/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: capwapclitdfsg.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $ 
*
* Description: This file contains data structures defined for Capwap module.
*********************************************************************/
/* Structure used by CLI to indicate which 
 all objects to be set in CapwapBaseAcNameListEntry */

typedef struct
{
 BOOL1  bCapwapBaseAcNameListId;
 BOOL1  bCapwapBaseAcNameListName;
 BOOL1  bCapwapBaseAcNameListPriority;
 BOOL1  bCapwapBaseAcNameListRowStatus;
} tCapwapIsSetCapwapBaseAcNameListEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in CapwapBaseMacAclEntry */

typedef struct
{
 BOOL1  bCapwapBaseMacAclId;
 BOOL1  bCapwapBaseMacAclStationId;
 BOOL1  bCapwapBaseMacAclRowStatus;
UINT1 u1Pad;
} tCapwapIsSetCapwapBaseMacAclEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in CapwapBaseWtpProfileEntry */

typedef struct
{
 BOOL1  bCapwapBaseWtpProfileId;
 BOOL1  bCapwapBaseWtpProfileName;
 BOOL1  bCapwapBaseWtpProfileWtpMacAddress;
 BOOL1  bCapwapBaseWtpProfileWtpModelNumber;
 BOOL1  bCapwapBaseWtpProfileWtpName;
 BOOL1  bCapwapBaseWtpProfileWtpLocation;
 BOOL1  bCapwapBaseWtpProfileWtpStaticIpEnable;
 BOOL1  bCapwapBaseWtpProfileWtpStaticIpType;
 BOOL1  bCapwapBaseWtpProfileWtpStaticIpAddress;
 BOOL1  bCapwapBaseWtpProfileWtpNetmask;
 BOOL1  bCapwapBaseWtpProfileWtpGateway;
 BOOL1  bCapwapBaseWtpProfileWtpFallbackEnable;
 BOOL1  bCapwapBaseWtpProfileWtpEchoInterval;
 BOOL1  bCapwapBaseWtpProfileWtpIdleTimeout;
 BOOL1  bCapwapBaseWtpProfileWtpMaxDiscoveryInterval;
 BOOL1  bCapwapBaseWtpProfileWtpReportInterval;
 BOOL1  bCapwapBaseWtpProfileWtpStatisticsTimer;
 BOOL1  bCapwapBaseWtpProfileWtpEcnSupport;
        BOOL1  bFsCapwapFsAcTimestampTrigger; 
 BOOL1  bCapwapBaseWtpProfileRowStatus;
} tCapwapIsSetCapwapBaseWtpProfileEntry;



/* Structure used by Capwap protocol for CapwapBaseAcNameListEntry */

typedef struct
{
 tRBNodeEmbd  CapwapBaseAcNameListTableNode;
 UINT4 u4CapwapBaseAcNameListId;
 UINT4 u4CapwapBaseAcNameListPriority;
 INT4 i4CapwapBaseAcNameListRowStatus;
 INT4 i4CapwapBaseAcNameListNameLen;
 UINT1 au1CapwapBaseAcNameListName[512];
} tCapwapMibCapwapBaseAcNameListEntry;

/* Structure used by Capwap protocol for CapwapBaseMacAclEntry */

typedef struct
{
 tRBNodeEmbd  CapwapBaseMacAclTableNode;
 UINT4 u4CapwapBaseMacAclId;
 INT4 i4CapwapBaseMacAclRowStatus;
 INT4 i4CapwapBaseMacAclStationIdLen;
 UINT1 au1CapwapBaseMacAclStationId[256];
} tCapwapMibCapwapBaseMacAclEntry;

/* Structure used by Capwap protocol for CapwapBaseWtpProfileEntry */

typedef struct
{
 tRBNodeEmbd  CapwapBaseWtpProfileTableNode;
 UINT4 u4CapwapBaseWtpProfileId;
 UINT4 u4CapwapBaseWtpProfileWtpEchoInterval;
 UINT4 u4CapwapBaseWtpProfileWtpIdleTimeout;
 UINT4 u4CapwapBaseWtpProfileWtpMaxDiscoveryInterval;
 UINT4 u4CapwapBaseWtpProfileWtpReportInterval;
 UINT4 u4CapwapBaseWtpProfileWtpStatisticsTimer;
 INT4 i4CapwapBaseWtpProfileWtpStaticIpEnable;
 INT4 i4CapwapBaseWtpProfileWtpStaticIpType;
 INT4 i4CapwapBaseWtpProfileWtpFallbackEnable;
 INT4 i4CapwapBaseWtpProfileWtpEcnSupport;
 INT4 i4CapwapBaseWtpProfileRowStatus;
 INT4 i4CapwapBaseWtpProfileNameLen;
 INT4 i4CapwapBaseWtpProfileWtpMacAddressLen;
 INT4 i4CapwapBaseWtpProfileWtpModelNumberLen;
 INT4 i4CapwapBaseWtpProfileWtpNameLen;
 INT4 i4CapwapBaseWtpProfileWtpLocationLen;
 INT4 i4CapwapBaseWtpProfileWtpStaticIpAddressLen;
 INT4 i4CapwapBaseWtpProfileWtpNetmaskLen;
 INT4 i4CapwapBaseWtpProfileWtpGatewayLen;
        INT4 i4FsCapwapClearApStats; 
        INT4 i4FsCapwapFsAcTimestampTrigger; 
 UINT1 au1CapwapBaseWtpProfileName[256];
 UINT1 au1CapwapBaseWtpProfileWtpMacAddress[20];
 UINT1 au1CapwapBaseWtpProfileWtpModelNumber[256];
 UINT1 au1CapwapBaseWtpProfileWtpName[512];
 UINT1 au1CapwapBaseWtpProfileWtpLocation[1024];
 UINT1 au1CapwapBaseWtpProfileWtpStaticIpAddress[4];
 UINT1 au1CapwapBaseWtpProfileWtpNetmask[4];
 UINT1 au1CapwapBaseWtpProfileWtpGateway[4];
} tCapwapMibCapwapBaseWtpProfileEntry;

/* Structure used by Capwap protocol for CapwapBaseWtpStateEntry */

typedef struct
{
 tRBNodeEmbd  CapwapBaseWtpStateTableNode;
 UINT4 u4CapwapBaseWtpStateWtpUpTime;
 UINT4 u4CapwapBaseWtpStateWtpCurrWtpProfileId;
 INT4 i4CapwapBaseWtpStateWtpIpAddressType;
 INT4 i4CapwapBaseWtpStateWtpLocalIpAddressType;
 INT4 i4CapwapBaseWtpState;
 INT4 i4CapwapBaseWtpStateWtpIdLen;
 INT4 i4CapwapBaseWtpStateWtpIpAddressLen;
 INT4 i4CapwapBaseWtpStateWtpLocalIpAddressLen;
 INT4 i4CapwapBaseWtpStateWtpBaseMacAddressLen;
 UINT1 au1CapwapBaseWtpStateWtpId[256];
 UINT1 au1CapwapBaseWtpStateWtpIpAddress[256];
 UINT1 au1CapwapBaseWtpStateWtpLocalIpAddress[256];
 UINT1 au1CapwapBaseWtpStateWtpBaseMacAddress[8];
} tCapwapMibCapwapBaseWtpStateEntry;

/* Structure used by Capwap protocol for CapwapBaseWtpEntry */

typedef struct
{
 tRBNodeEmbd  CapwapBaseWtpTableNode;
 UINT4 u4CapwapBaseWtpRadiosInUseNum;
 UINT4 u4CapwapBaseWtpRadioNumLimit;
 UINT4 u4CapwapBaseWtpRetransmitCount;
 INT4 i4CapwapBaseWtpPhyIndex;
 INT4 i4CapwapBaseWtpMacTypeOptions;
 INT4 i4CapwapBaseWtpDiscoveryType;
 INT4 i4CapwapBaseWtpCurrIdLen;
 INT4 i4CapwapBaseWtpBaseMacAddressLen;
 INT4 i4CapwapBaseWtpTunnelModeOptionsLen;
 UINT1 au1CapwapBaseWtpCurrId[256];
 UINT1 au1CapwapBaseWtpBaseMacAddress[8];
 UINT1 au1CapwapBaseWtpTunnelModeOptions[256];
} tCapwapMibCapwapBaseWtpEntry;

/* Structure used by Capwap protocol for CapwapBaseWirelessBindingEntry */

typedef struct
{
 tRBNodeEmbd  CapwapBaseWirelessBindingTableNode;
 UINT4 u4CapwapBaseWirelessBindingRadioId;
 UINT4 u4CapwapBaseWtpProfileId;
 INT4 i4CapwapBaseWirelessBindingVirtualRadioIfIndex;
 INT4 i4CapwapBaseWirelessBindingType;
} tCapwapMibCapwapBaseWirelessBindingEntry;

/* Structure used by Capwap protocol for CapwapBaseStationEntry */

typedef struct
{
 tRBNodeEmbd  CapwapBaseStationTableNode;
 UINT4 u4CapwapBaseStationWtpRadioId;
 INT4 i4CapwapBaseStationIdLen;
 INT4 i4CapwapBaseStationWtpIdLen;
 INT4 i4CapwapBaseStationAddedTimeLen;
 INT4 i4CapwapBaseStationVlanNameLen;
 UINT1 au1CapwapBaseStationId[256];
 UINT1 au1CapwapBaseStationWtpId[256];
 UINT1 au1CapwapBaseStationAddedTime[256];
 UINT1 au1CapwapBaseStationVlanName[32];
} tCapwapMibCapwapBaseStationEntry;

/* Structure used by Capwap protocol for CapwapBaseWtpEventsStatsEntry */

typedef struct
{
 tRBNodeEmbd  CapwapBaseWtpEventsStatsTableNode;
 UINT4 u4CapwapBaseWtpEventsStatsRebootCount;
 UINT4 u4CapwapBaseWtpEventsStatsInitCount;
 UINT4 u4CapwapBaseWtpEventsStatsLinkFailureCount;
 UINT4 u4CapwapBaseWtpEventsStatsSwFailureCount;
 UINT4 u4CapwapBaseWtpEventsStatsHwFailureCount;
 UINT4 u4CapwapBaseWtpEventsStatsOtherFailureCount;
 UINT4 u4CapwapBaseWtpEventsStatsUnknownFailureCount;
 INT4 i4CapwapBaseWtpEventsStatsLastFailureType;
 INT4 i4CapwapBaseWtpCurrIdLen;
 UINT1 au1CapwapBaseWtpCurrId[256];
} tCapwapMibCapwapBaseWtpEventsStatsEntry;

/* Structure used by Capwap protocol for CapwapBaseRadioEventsStatsEntry */

typedef struct
{
 tRBNodeEmbd  CapwapBaseRadioEventsStatsTableNode;
 UINT4 u4CapwapBaseRadioEventsWtpRadioId;
 UINT4 u4CapwapBaseRadioEventsStatsResetCount;
 UINT4 u4CapwapBaseRadioEventsStatsSwFailureCount;
 UINT4 u4CapwapBaseRadioEventsStatsHwFailureCount;
 UINT4 u4CapwapBaseRadioEventsStatsOtherFailureCount;
 UINT4 u4CapwapBaseRadioEventsStatsUnknownFailureCount;
 UINT4 u4CapwapBaseRadioEventsStatsConfigUpdateCount;
 UINT4 u4CapwapBaseRadioEventsStatsChannelChangeCount;
 UINT4 u4CapwapBaseRadioEventsStatsBandChangeCount;
 UINT4 u4CapwapBaseRadioEventsStatsDecryptErrorCount;
 INT4 i4CapwapBaseRadioEventsStatsCurrNoiseFloor;
 INT4 i4CapwapBaseRadioEventsStatsLastFailureType;
 INT4 i4CapwapBaseWtpCurrIdLen;
 UINT1 au1CapwapBaseWtpCurrId[256];
} tCapwapMibCapwapBaseRadioEventsStatsEntry;







/* Structure used by CLI to indicate which 
 all objects to be set in FsWtpModelEntry */

typedef struct
{
 BOOL1  bFsCapwapWtpModelNumber;
 BOOL1  bFsNoOfRadio;
 BOOL1  bFsCapwapWtpMacType;
 BOOL1  bFsCapwapWtpTunnelMode;
 BOOL1  bFsCapwapImageName;
 BOOL1  bFsCapwapQosProfileName;
 BOOL1  bFsMaxStations;
 BOOL1  bFsWtpModelRowStatus;
} tCapwapIsSetFsWtpModelEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsWtpRadioEntry */

typedef struct
{
 BOOL1  bFsNumOfRadio;
 BOOL1  bFsWtpRadioType;
 BOOL1  bFsRadioAdminStatus;
 BOOL1  bFsMaxSSIDSupported;
 BOOL1  bFsWtpRadioRowStatus;
 BOOL1  bFsCapwapWtpModelNumber;
UINT1 u1Pad[2];
} tCapwapIsSetFsWtpRadioEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsCapwapWhiteListEntry */

typedef struct
{
 BOOL1  bFsCapwapWhiteListId;
 BOOL1  bFsCapwapWhiteListWtpBaseMac;
 BOOL1  bFsCapwapWhiteListRowStatus;
UINT1 u1Pad;
} tCapwapIsSetFsCapwapWhiteListEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsCapwapBlackListEntry */

typedef struct
{
 BOOL1  bFsCapwapBlackListId;
 BOOL1  bFsCapwapBlackListWtpBaseMac;
 BOOL1  bFsCapwapBlackListRowStatus;
UINT1 u1Pad;
} tCapwapIsSetFsCapwapBlackListEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsCapwapWtpConfigEntry */

typedef struct
{
    BOOL1  bFsCapwapWtpReset;
    BOOL1  bFsCapwapClearConfig;
    BOOL1  bFsWtpDiscoveryType;
    BOOL1  bFsWtpCountryString;
    BOOL1  bFsWtpCrashDumpFileName;
    BOOL1  bFsWtpMemoryDumpFileName;
    BOOL1  bFsCapwapClearApStats; 
    BOOL1  bFsCapwapWtpConfigRowStatus;
    BOOL1  bFsWtpDeleteOperation;
    BOOL1  bCapwapBaseWtpProfileId;
    BOOL1  bFsWlcStaticIpAddress;
    UINT1  au1Pad[1];
} tCapwapIsSetFsCapwapWtpConfigEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsCapwapLinkEncryptionEntry */

typedef struct
{
 BOOL1  bFsCapwapEncryptChannel;
 BOOL1  bFsCapwapEncryptChannelStatus;
 BOOL1  bFsCapwapEncryptChannelRowStatus;
 BOOL1  bCapwapBaseWtpProfileId;
} tCapwapIsSetFsCapwapLinkEncryptionEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsCapwapDefaultWtpProfileEntry */

typedef struct
{
 BOOL1  bFsCapwapDefaultWtpProfileModelNumber;
 BOOL1  bFsCapwapDefaultWtpProfileWtpFallbackEnable;
 BOOL1  bFsCapwapDefaultWtpProfileWtpEchoInterval;
 BOOL1  bFsCapwapDefaultWtpProfileWtpIdleTimeout;

 BOOL1  bFsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval;
 BOOL1  bFsCapwapDefaultWtpProfileWtpReportInterval;
 BOOL1  bFsCapwapDefaultWtpProfileWtpStatisticsTimer;
 BOOL1  bFsCapwapDefaultWtpProfileWtpEcnSupport;

 BOOL1  bFsCapwapDefaultWtpProfileRowStatus;
UINT1 u1Pad[3];
} tCapwapIsSetFsCapwapDefaultWtpProfileEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsCapwapDnsProfileEntry */

typedef struct
{
 BOOL1  bFsCapwapDnsServerIp;
 BOOL1  bFsCapwapDnsDomainName;
 BOOL1  bFsCapwapDnsProfileRowStatus;
 BOOL1  bCapwapBaseWtpProfileId;
} tCapwapIsSetFsCapwapDnsProfileEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsWtpNativeVlanIdTable */

typedef struct
{
 BOOL1  bFsWtpNativeVlanId;
 BOOL1  bFsWtpNativeVlanIdRowStatus;
 BOOL1  bCapwapBaseWtpProfileId;
 UINT1 u1Pad;
} tCapwapIsSetFsWtpNativeVlanIdTable;

/* Structure used by CLI to indicate which 
 all objects to be set in FsWtpLocalRoutingTable */

typedef struct
{
 BOOL1  bFsWtpLocalRouting;
 BOOL1  bFsWtpLocalRoutingRowStatus;
 BOOL1  bCapwapBaseWtpProfileId;
 UINT1 u1Pad;
} tCapwapIsSetFsWtpLocalRoutingTable;


/* Structure used by CLI to indicate which 
 all objects to be set in FsCawapDiscStatsEntry */

typedef struct
{
 BOOL1  bFsCapwapDiscStatsRowStatus;
 BOOL1  bCapwapBaseWtpProfileId;
 UINT1 auPad[2];
} tCapwapIsSetFsCawapDiscStatsEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsCawapJoinStatsEntry */

typedef struct
{
 BOOL1  bFsCapwapJoinStatsRowStatus;
 BOOL1  bCapwapBaseWtpProfileId;
 UINT1 auPad[2];
} tCapwapIsSetFsCawapJoinStatsEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsCawapConfigStatsEntry */

typedef struct
{
 BOOL1  bFsCapwapConfigStatsRowStatus;
 BOOL1  bCapwapBaseWtpProfileId;
 UINT1 auPad[2];
} tCapwapIsSetFsCawapConfigStatsEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsCawapRunStatsEntry */

typedef struct
{
 BOOL1  bFsCapwapRunStatsRowStatus;
 BOOL1  bCapwapBaseWtpProfileId;
 UINT1 auPad[2];
} tCapwapIsSetFsCawapRunStatsEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsCapwapWirelessBindingEntry */

typedef struct
{
 BOOL1  bFsCapwapWirelessBindingVirtualRadioIfIndex;
 BOOL1  bFsCapwapWirelessBindingType;
 BOOL1  bFsCapwapWirelessBindingRowStatus;
 BOOL1  bCapwapBaseWtpProfileId;
 
 BOOL1  bCapwapBaseWirelessBindingRadioId;
 UINT1 u1Pad[3];
} tCapwapIsSetFsCapwapWirelessBindingEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsCapwapStationWhiteListEntry */

typedef struct
{
 BOOL1  bFsCapwapStationWhiteListId;
 BOOL1  bFsCapwapStationWhiteListStationId;
 BOOL1  bFsCapwapStationWhiteListRowStatus;
 UINT1 u1Pad;
} tCapwapIsSetFsCapwapStationWhiteListEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsCapwapWtpRebootStatisticsEntry */

typedef struct
{
 BOOL1  bFsCapwapWtpRebootStatisticsRebootCount;
 BOOL1  bFsCapwapWtpRebootStatisticsAcInitiatedCount;
 BOOL1  bFsCapwapWtpRebootStatisticsLinkFailureCount;
 BOOL1  bFsCapwapWtpRebootStatisticsSwFailureCount;

 BOOL1  bFsCapwapWtpRebootStatisticsHwFailureCount;
 BOOL1  bFsCapwapWtpRebootStatisticsOtherFailureCount;
 BOOL1  bFsCapwapWtpRebootStatisticsUnknownFailureCount;
 BOOL1  bFsCapwapWtpRebootStatisticsLastFailureType;

 BOOL1  bFsCapwapWtpRebootStatisticsRowStatus;
 BOOL1  bCapwapBaseWtpProfileId;
 UINT1 u1Pad[2];
} tCapwapIsSetFsCapwapWtpRebootStatisticsEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsCapwapWtpRadioStatisticsEntry */

typedef struct
{
 BOOL1  bFsCapwapWtpRadioLastFailType;
 BOOL1  bFsCapwapWtpRadioResetCount;
 BOOL1  bFsCapwapWtpRadioSwFailureCount;
 BOOL1  bFsCapwapWtpRadioHwFailureCount;

 BOOL1  bFsCapwapWtpRadioOtherFailureCount;
 BOOL1  bFsCapwapWtpRadioUnknownFailureCount;
 BOOL1  bFsCapwapWtpRadioConfigUpdateCount;
 BOOL1  bFsCapwapWtpRadioChannelChangeCount;

 BOOL1  bFsCapwapWtpRadioBandChangeCount;
 BOOL1  bFsCapwapWtpRadioCurrentNoiseFloor;
 BOOL1  bFsCapwapWtpRadioStatRowStatus;
 BOOL1  bRadioIfIndex;
} tCapwapIsSetFsCapwapWtpRadioStatisticsEntry;



/* Structure used by Capwap protocol for FsWtpModelEntry */

typedef struct
{
 tRBNodeEmbd  FsWtpModelTableNode;
 UINT4 u4FsNoOfRadio;
 INT4 i4FsCapwapWtpMacType;
 INT4 i4FsMaxStations;
 INT4 i4FsWtpModelRowStatus;
 INT4 i4FsCapwapWtpModelNumberLen;
 INT4 i4FsCapwapWtpTunnelModeLen;
 INT4 i4FsCapwapImageNameLen;
 INT4 i4FsCapwapQosProfileNameLen;
 UINT1 au1FsCapwapWtpModelNumber[256];
 UINT1 au1FsCapwapWtpTunnelMode[256];
 UINT1 au1FsCapwapImageName[23];
 UINT1 au1Align[1];
 UINT1 au1FsCapwapQosProfileName[256];
} tCapwapMibFsWtpModelEntry;

/* Structure used by Capwap protocol for FsWtpRadioEntry */

typedef struct
{
 tRBNodeEmbd  FsWtpRadioTableNode;
 UINT4 u4FsNumOfRadio;
 UINT4 u4FsWtpRadioType;
 INT4 i4FsRadioAdminStatus;
 INT4 i4FsMaxSSIDSupported;
 INT4 i4FsWtpRadioRowStatus;
 INT4 i4FsCapwapWtpModelNumberLen;
 UINT1 au1FsCapwapWtpModelNumber[256];
} tCapwapMibFsWtpRadioEntry;

/* Structure used by Capwap protocol for FsCapwapWhiteListEntry */

typedef struct
{
 tRBNodeEmbd  FsCapwapWhiteListTableNode;
 UINT4 u4FsCapwapWhiteListId;
 INT4 i4FsCapwapWhiteListRowStatus;
 INT4 i4FsCapwapWhiteListWtpBaseMacLen;
 UINT1 au1FsCapwapWhiteListWtpBaseMac[256];
} tCapwapMibFsCapwapWhiteListEntry;

/* Structure used by Capwap protocol for FsCapwapBlackListEntry */

typedef struct
{
 tRBNodeEmbd  FsCapwapBlackListNode;
 UINT4 u4FsCapwapBlackListId;
 INT4 i4FsCapwapBlackListRowStatus;
 INT4 i4FsCapwapBlackListWtpBaseMacLen;
 UINT1 au1FsCapwapBlackListWtpBaseMac[256];
} tCapwapMibFsCapwapBlackListEntry;

/* Structure used by Capwap protocol for FsCapwapWtpConfigEntry */

typedef struct
{
 tRBNodeEmbd  FsCapwapWtpConfigTableNode;
 UINT4 u4CapwapBaseWtpProfileId;
 INT4 i4FsCapwapWtpReset;
 INT4 i4FsCapwapClearConfig;
 INT4 i4FsWtpDiscoveryType;
 INT4 i4FsCapwapClearApStats;
 INT4 i4FsCapwapWtpConfigRowStatus;
 INT4 i4FsWtpCountryStringLen;
    INT4 i4FsWtpCrashDumpFileNameLen;
    INT4 i4FsWtpMemoryDumpFileNameLen;
    UINT4 u4FsWtpDeleteOperation;
    UINT4 u4FsWlcStaticIpAddress;
    UINT1 au1FsWtpCrashDumpFileName[256];
    UINT1 au1FsWtpMemoryDumpFileName[256];
 UINT1 au1FsWtpCountryString[3];
 UINT1 au1Align[1];
} tCapwapMibFsCapwapWtpConfigEntry;

/* Structure used by Capwap protocol for FsCapwapLinkEncryptionEntry */

typedef struct
{
 tRBNodeEmbd  FsCapwapLinkEncryptionTableNode;
 INT4 i4FsCapwapEncryptChannel;
 INT4 i4FsCapwapEncryptChannelStatus;
 INT4 i4FsCapwapEncryptChannelRowStatus;
 UINT4 u4CapwapBaseWtpProfileId;
 } tCapwapMibFsCapwapLinkEncryptionEntry;

/* Structure used by Capwap protocol for FsCapwapDefaultWtpProfileEntry */

typedef struct
{
 tRBNodeEmbd  FsCawapDefaultWtpProfileTableNode;
 UINT4 u4FsCapwapDefaultWtpProfileWtpEchoInterval;
 UINT4 u4FsCapwapDefaultWtpProfileWtpIdleTimeout;
 UINT4 u4FsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval;
 UINT4 u4FsCapwapDefaultWtpProfileWtpReportInterval;
 UINT4 u4FsCapwapDefaultWtpProfileWtpStatisticsTimer;
 INT4 i4FsCapwapDefaultWtpProfileWtpFallbackEnable;
 INT4 i4FsCapwapDefaultWtpProfileWtpEcnSupport;
 INT4 i4FsCapwapDefaultWtpProfileRowStatus;
 INT4 i4FsCapwapDefaultWtpProfileModelNumberLen;
 UINT1 au1FsCapwapDefaultWtpProfileModelNumber[256];
} tCapwapMibFsCapwapDefaultWtpProfileEntry;

/* Structure used by Capwap protocol for FsCapwapDnsProfileEntry */

typedef struct
{
 tRBNodeEmbd  FsCapwapDnsProfileTableNode;
 UINT4 u4CapwapBaseWtpProfileId;
 INT4 i4FsCapwapDnsProfileRowStatus;
 INT4 i4FsCapwapDnsServerIpLen;
 INT4 i4FsCapwapDnsDomainNameLen;
 UINT1 au1FsCapwapDnsServerIp[4];
 UINT1 au1FsCapwapDnsDomainName[32];
} tCapwapMibFsCapwapDnsProfileEntry;

/* Structure used by Capwap protocol for FsWtpNativeVlanIdTable */

typedef struct
{
 tRBNodeEmbd  FsWtpNativeVlanIdTableNode;
 UINT4 u4CapwapBaseWtpProfileId;
 INT4 i4FsWtpNativeVlanId;
 INT4 i4FsWtpNativeVlanIdRowStatus;
} tCapwapMibFsWtpNativeVlanIdTable;

/* Structure used by Capwap protocol for FsWtpLocalRoutingTable */

typedef struct
{
 tRBNodeEmbd  FsWtpLocalRoutingTableNode;
 UINT4 u4CapwapBaseWtpProfileId;
 INT4 i4FsWtpLocalRouting;
 INT4 i4FsWtpLocalRoutingRowStatus;
} tCapwapMibFsWtpLocalRoutingTable;


/* Structure used by Capwap protocol for FsCawapDiscStatsEntry */

typedef struct
{
 tRBNodeEmbd  FsCawapDiscStatsTableNode;
 UINT4 u4FsCapwapDiscReqReceived;
 UINT4 u4FsCapwapDiscRspReceived;
 UINT4 u4FsCapwapDiscReqTransmitted;
 UINT4 u4FsCapwapDiscRspTransmitted;
 UINT4 u4FsCapwapDiscunsuccessfulProcessed;
 UINT4 u4CapwapBaseWtpProfileId;
 INT4 i4FsCapwapDiscStatsRowStatus;
 INT4 i4FsCapwapDiscLastUnsuccAttemptReasonLen;
 INT4 i4FsCapwapDiscLastSuccAttemptTimeLen;
 INT4 i4FsCapwapDiscLastUnsuccessfulAttemptTimeLen;
 UINT1 au1FsCapwapDiscLastUnsuccAttemptReason[256];
 UINT1 au1FsCapwapDiscLastSuccAttemptTime[256];
 UINT1 au1FsCapwapDiscLastUnsuccessfulAttemptTime[256];
} tCapwapMibFsCawapDiscStatsEntry;

/* Structure used by Capwap protocol for FsCawapJoinStatsEntry */

typedef struct
{
 tRBNodeEmbd  FsCawapJoinStatsTableNode;
 UINT4 u4FsCapwapJoinReqReceived;
 UINT4 u4FsCapwapJoinRspReceived;
 UINT4 u4FsCapwapJoinReqTransmitted;
 UINT4 u4FsCapwapJoinRspTransmitted;
 UINT4 u4FsCapwapJoinunsuccessfulProcessed;
 UINT4 u4CapwapBaseWtpProfileId;
 INT4 i4FsCapwapJoinStatsRowStatus;
 INT4 i4FsCapwapJoinReasonLastUnsuccAttemptLen;
 INT4 i4FsCapwapJoinLastSuccAttemptTimeLen;
 INT4 i4FsCapwapJoinLastUnsuccAttemptTimeLen;
 UINT1 au1FsCapwapJoinReasonLastUnsuccAttempt[256];
 UINT1 au1FsCapwapJoinLastSuccAttemptTime[256];
 UINT1 au1FsCapwapJoinLastUnsuccAttemptTime[256];
} tCapwapMibFsCawapJoinStatsEntry;

/* Structure used by Capwap protocol for FsCawapConfigStatsEntry */

typedef struct
{
 tRBNodeEmbd  FsCawapConfigStatsTableNode;
 UINT4 u4FsCapwapConfigReqReceived;
 UINT4 u4FsCapwapConfigRspReceived;
 UINT4 u4FsCapwapConfigReqTransmitted;
 UINT4 u4FsCapwapConfigRspTransmitted;
 UINT4 u4FsCapwapConfigunsuccessfulProcessed;
 UINT4 u4CapwapBaseWtpProfileId;
 INT4 i4FsCapwapConfigStatsRowStatus;
 INT4 i4FsCapwapConfigReasonLastUnsuccAttemptLen;
 INT4 i4FsCapwapConfigLastSuccAttemptTimeLen;
 INT4 i4FsCapwapConfigLastUnsuccessfulAttemptTimeLen;
 UINT1 au1FsCapwapConfigReasonLastUnsuccAttempt[256];
 UINT1 au1FsCapwapConfigLastSuccAttemptTime[256];
 UINT1 au1FsCapwapConfigLastUnsuccessfulAttemptTime[256];
} tCapwapMibFsCawapConfigStatsEntry;

/* Structure used by Capwap protocol for FsCawapRunStatsEntry */

typedef struct
{
 tRBNodeEmbd  FsCawapRunStatsTableNode;
 UINT4 u4FsCapwapRunConfigUpdateReqReceived;
 UINT4 u4FsCapwapRunConfigUpdateRspReceived;
 UINT4 u4FsCapwapRunConfigUpdateReqTransmitted;
 UINT4 u4FsCapwapRunConfigUpdateRspTransmitted;
 UINT4 u4FsCapwapRunConfigUpdateunsuccessfulProcessed;
 UINT4 u4FsCapwapRunStationConfigReqReceived;
 UINT4 u4FsCapwapRunStationConfigRspReceived;
 UINT4 u4FsCapwapRunStationConfigReqTransmitted;
 UINT4 u4FsCapwapRunStationConfigRspTransmitted;
 UINT4 u4FsCapwapRunStationConfigunsuccessfulProcessed;
 UINT4 u4FsCapwapRunClearConfigReqReceived;
 UINT4 u4FsCapwapRunClearConfigRspReceived;
 UINT4 u4FsCapwapRunClearConfigReqTransmitted;
 UINT4 u4FsCapwapRunClearConfigRspTransmitted;
 UINT4 u4FsCapwapRunClearConfigunsuccessfulProcessed;
 UINT4 u4FsCapwapRunDataTransferReqReceived;
 UINT4 u4FsCapwapRunDataTransferRspReceived;
 UINT4 u4FsCapwapRunDataTransferReqTransmitted;
 UINT4 u4FsCapwapRunDataTransferRspTransmitted;
 UINT4 u4FsCapwapRunDataTransferunsuccessfulProcessed;
 UINT4 u4FsCapwapRunResetReqReceived;
 UINT4 u4FsCapwapRunResetRspReceived;
 UINT4 u4FsCapwapRunResetReqTransmitted;
 UINT4 u4FsCapwapRunResetRspTransmitted;
 UINT4 u4FsCapwapRunResetunsuccessfulProcessed;
 UINT4 u4FsCapwapRunPriDiscReqReceived;
 UINT4 u4FsCapwapRunPriDiscRspReceived;
 UINT4 u4FsCapwapRunPriDiscReqTransmitted;
 UINT4 u4FsCapwapRunPriDiscRspTransmitted;
 UINT4 u4FsCapwapRunPriDiscunsuccessfulProcessed;
 UINT4 u4FsCapwapRunEchoReqReceived;
 UINT4 u4FsCapwapRunEchoRspReceived;
 UINT4 u4FsCapwapRunEchoReqTransmitted;
 UINT4 u4FsCapwapRunEchoRspTransmitted;
 UINT4 u4FsCapwapRunEchounsuccessfulProcessed;
 UINT4 u4CapwapBaseWtpProfileId;
 INT4 i4FsCapwapRunStatsRowStatus;
 INT4 i4FsCapwapRunConfigUpdateReasonLastUnsuccAttemptLen;
 INT4 i4FsCapwapRunConfigUpdateLastSuccAttemptTimeLen;
 INT4 i4FsCapwapRunConfigUpdateLastUnsuccessfulAttemptTimeLen;
 INT4 i4FsCapwapRunStationConfigReasonLastUnsuccAttemptLen;
 INT4 i4FsCapwapRunStationConfigLastSuccAttemptTimeLen;
 INT4 i4FsCapwapRunStationConfigLastUnsuccessfulAttemptTimeLen;
 INT4 i4FsCapwapRunClearConfigReasonLastUnsuccAttemptLen;
 INT4 i4FsCapwapRunClearConfigLastSuccAttemptTimeLen;
 INT4 i4FsCapwapRunClearConfigLastUnsuccessfulAttemptTimeLen;
 INT4 i4FsCapwapRunDataTransferReasonLastUnsuccAttemptLen;
 INT4 i4FsCapwapRunDataTransferLastSuccAttemptTimeLen;
 INT4 i4FsCapwapRunDataTransferLastUnsuccessfulAttemptTimeLen;
 INT4 i4FsCapwapRunResetReasonLastUnsuccAttemptLen;
 INT4 i4FsCapwapRunResetLastSuccAttemptTimeLen;
 INT4 i4FsCapwapRunResetLastUnsuccessfulAttemptTimeLen;
 INT4 i4FsCapwapRunPriDiscReasonLastUnsuccAttemptLen;
 INT4 i4FsCapwapRunPriDiscLastSuccAttemptTimeLen;
 INT4 i4FsCapwapRunPriDiscLastUnsuccessfulAttemptTimeLen;
 INT4 i4FsCapwapRunEchoReasonLastUnsuccAttemptLen;
 INT4 i4FsCapwapRunEchoLastSuccAttemptTimeLen;
 INT4 i4FsCapwapRunEchoLastUnsuccessfulAttemptTimeLen;
 UINT1 au1FsCapwapRunConfigUpdateReasonLastUnsuccAttempt[256];
 UINT1 au1FsCapwapRunConfigUpdateLastSuccAttemptTime[256];
 UINT1 au1FsCapwapRunConfigUpdateLastUnsuccessfulAttemptTime[256];
 UINT1 au1FsCapwapRunStationConfigReasonLastUnsuccAttempt[256];
 UINT1 au1FsCapwapRunStationConfigLastSuccAttemptTime[256];
 UINT1 au1FsCapwapRunStationConfigLastUnsuccessfulAttemptTime[256];
 UINT1 au1FsCapwapRunClearConfigReasonLastUnsuccAttempt[256];
 UINT1 au1FsCapwapRunClearConfigLastSuccAttemptTime[256];
 UINT1 au1FsCapwapRunClearConfigLastUnsuccessfulAttemptTime[256];
 UINT1 au1FsCapwapRunDataTransferReasonLastUnsuccAttempt[256];
 UINT1 au1FsCapwapRunDataTransferLastSuccAttemptTime[256];
 UINT1 au1FsCapwapRunDataTransferLastUnsuccessfulAttemptTime[256];
 UINT1 au1FsCapwapRunResetReasonLastUnsuccAttempt[256];
 UINT1 au1FsCapwapRunResetLastSuccAttemptTime[256];
 UINT1 au1FsCapwapRunResetLastUnsuccessfulAttemptTime[256];
 UINT1 au1FsCapwapRunPriDiscReasonLastUnsuccAttempt[256];
 UINT1 au1FsCapwapRunPriDiscLastSuccAttemptTime[256];
 UINT1 au1FsCapwapRunPriDiscLastUnsuccessfulAttemptTime[256];
 UINT1 au1FsCapwapRunEchoReasonLastUnsuccAttempt[256];
 UINT1 au1FsCapwapRunEchoLastSuccAttemptTime[256];
 UINT1 au1FsCapwapRunEchoLastUnsuccessfulAttemptTime[256];
} tCapwapMibFsCawapRunStatsEntry;

/* Structure used by Capwap protocol for FsCapwapWirelessBindingEntry */

typedef struct
{
 tRBNodeEmbd  FsCapwapWirelessBindingTableNode;
 UINT4 u4CapwapBaseWtpProfileId;
 UINT4 u4CapwapBaseWirelessBindingRadioId;
 INT4 i4FsCapwapWirelessBindingVirtualRadioIfIndex;
 INT4 i4FsCapwapWirelessBindingType;
 INT4 i4FsCapwapWirelessBindingRowStatus;
} tCapwapMibFsCapwapWirelessBindingEntry;

/* Structure used by Capwap protocol for FsCapwapStationWhiteListEntry */

typedef struct
{
 tRBNodeEmbd  FsCapwapStationWhiteListNode;
 UINT4 u4FsCapwapStationWhiteListId;
 INT4 i4FsCapwapStationWhiteListRowStatus;
 INT4 i4FsCapwapStationWhiteListStationIdLen;
 UINT1 au1FsCapwapStationWhiteListStationId[256];
} tCapwapMibFsCapwapStationWhiteListEntry;

/* Structure used by Capwap protocol for FsCapwapWtpRebootStatisticsEntry */

typedef struct
{
 tRBNodeEmbd  FsCapwapWtpRebootStatisticsTableNode;
 UINT4 u4FsCapwapWtpRebootStatisticsRebootCount;
 UINT4 u4FsCapwapWtpRebootStatisticsAcInitiatedCount;
 UINT4 u4FsCapwapWtpRebootStatisticsLinkFailureCount;
 UINT4 u4FsCapwapWtpRebootStatisticsSwFailureCount;
 UINT4 u4FsCapwapWtpRebootStatisticsHwFailureCount;
 UINT4 u4FsCapwapWtpRebootStatisticsOtherFailureCount;
 UINT4 u4FsCapwapWtpRebootStatisticsUnknownFailureCount;
 UINT4 u4CapwapBaseWtpProfileId;
 INT4 i4FsCapwapWtpRebootStatisticsLastFailureType;
 INT4 i4FsCapwapWtpRebootStatisticsRowStatus;
} tCapwapMibFsCapwapWtpRebootStatisticsEntry;

/* Structure used by Capwap protocol for FsCapwapWtpRadioStatisticsEntry */

typedef struct
{
 tRBNodeEmbd  FsCapwapWtpRadioStatisticsTableNode;
 UINT4 u4FsCapwapWtpRadioResetCount;
 UINT4 u4FsCapwapWtpRadioSwFailureCount;
 UINT4 u4FsCapwapWtpRadioHwFailureCount;
 UINT4 u4FsCapwapWtpRadioOtherFailureCount;
 UINT4 u4FsCapwapWtpRadioUnknownFailureCount;
 UINT4 u4FsCapwapWtpRadioConfigUpdateCount;
 UINT4 u4FsCapwapWtpRadioChannelChangeCount;
 UINT4 u4FsCapwapWtpRadioBandChangeCount;
 UINT4 u4FsCapwapWtpRadioCurrentNoiseFloor;
 UINT4 u4FsCapwapWtpRadioStatRowStatus;
 INT4  i4RadioIfIndex;
 INT4 i4FsCapwapWtpRadioLastFailType;
} tCapwapMibFsCapwapWtpRadioStatisticsEntry;

typedef struct
{
    tRBNodeEmbd FsCapwapWtpDot11StatisticsTableNode;
    UINT4   u4Dot11TransmittedFragmentCount;
    UINT4   u4Dot11MulticastTransmittedFrameCount;
    UINT4   u4Dot11FailedCount;
    UINT4   u4Dot11RetryCount;
    UINT4   u4Dot11MultipleRetryCount;
    UINT4   u4Dot11FrameDuplicateCount;
    UINT4   u4Dot11RTSSuccessCount;
    UINT4   u4Dot11RTSFailureCount;
    UINT4   u4Dot11ACKFailureCount;
    UINT4   u4Dot11ReceivedFragmentCount;
    UINT4   u4Dot11MulticastReceivedFrameCount;
    UINT4   u4Dot11FCSErrorCount;
    UINT4   u4Dot11TransmittedFrameCount;
    UINT4   u4Dot11WEPUndecryptableCount;
    UINT4   u4Dot11QosDiscardedFragmentCount;
    UINT4   u4Dot11AssociatedStationCount;
    UINT4   u4Dot11QosCFPollsReceivedCount;
    UINT4   u4Dot11QosCFPollsUnusedCount;
    UINT4   u4Dot11QosCFPollsUnusableCount;
    UINT1   u1Dot11RadioId;
    UINT1 au1Pad[3]; 

}tCapwapMibFsCapwapWtpDot11StatisticsEntry;
