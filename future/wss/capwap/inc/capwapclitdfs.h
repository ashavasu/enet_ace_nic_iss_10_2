/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: capwapclitdfs.h,v 1.2 2017/11/24 10:37:01 siva Exp $ 
* Description: This file contains type definitions for Capwap module.
*********************************************************************/

typedef struct

{

 tRBTree   CapwapBaseAcNameListTable;
 tRBTree   CapwapBaseMacAclTable;
 tRBTree   CapwapBaseWtpProfileTable;
 tRBTree   CapwapBaseWtpStateTable;
 tRBTree   CapwapBaseWtpTable;
 tRBTree   CapwapBaseWirelessBindingTable;
 tRBTree   CapwapBaseStationTable;
 tRBTree   CapwapBaseWtpEventsStatsTable;
 tRBTree   CapwapBaseRadioEventsStatsTable;
    
    tRBTree   FsWtpModelTable;
    tRBTree   FsWtpRadioTable; 
 tRBTree   FsCapwapWhiteListTable;
 tRBTree   FsCapwapBlackList;
 tRBTree   FsCapwapWtpConfigTable;
 tRBTree   FsCapwapLinkEncryptionTable;
 tRBTree   FsCawapDefaultWtpProfileTable;
 tRBTree   FsCapwapDnsProfileTable;
 tRBTree   FsWtpNativeVlanIdTable;
 tRBTree   FsWtpLocalRoutingTable;
 tRBTree   FsCawapDiscStatsTable;
 tRBTree   FsCawapJoinStatsTable;
 tRBTree   FsCawapConfigStatsTable;
 tRBTree   FsCawapRunStatsTable;
 tRBTree   FsCapwapWirelessBindingTable; 
 tRBTree   FsCapwapStationWhiteList; 
 tRBTree   FsCapwapWtpRebootStatisticsTable;
 tRBTree   FsCapwapWtpRadioStatisticsTable; 
 tRBTree   FsCapwATPStatsTable;
 tRBTree   FsCapwapExtWtpConfigTable;
        INT4 i4FsCapwapEnable;
 INT4 i4FsCapwapShutdown;


    
    UINT4 u4CapwapBaseWtpSessions;
 UINT4 u4CapwapBaseWtpSessionsLimit;
 UINT4 u4CapwapBaseStationSessions;
 UINT4 u4CapwapBaseStationSessionsLimit;

 INT4 i4CapwapBaseDataChannelDTLSPolicyOptionsLen;
 UINT1 au1CapwapBaseDataChannelDTLSPolicyOptions[256];

 INT4 i4CapwapBaseControlChannelAuthenOptionsLen;
 UINT1 au1CapwapBaseControlChannelAuthenOptions[256];
 UINT4 u4CapwapBaseAcMaxRetransmit;
 UINT4 u4CapwapBaseAcChangeStatePendingTimer;
 UINT4 u4CapwapBaseAcDataCheckTimer;
 UINT4 u4CapwapBaseAcDTLSSessionDeleteTimer;
 UINT4 u4CapwapBaseAcEchoInterval;
 UINT4 u4CapwapBaseAcRetransmitInterval;
 UINT4 u4CapwapBaseAcSilentInterval;
 UINT4 u4CapwapBaseAcWaitDTLSTimer;
 UINT4 u4CapwapBaseAcWaitJoinTimer;
 INT4 i4CapwapBaseAcEcnSupport;
 UINT4 u4CapwapBaseFailedDTLSAuthFailureCount;
 UINT4 u4CapwapBaseFailedDTLSSessionCount;
    UINT4 u4FsDtlsEncAuthEncryptAlgorithm;
    UINT4 u4FsDtlsEncrption;
 INT4 i4CapwapBaseNtfWtpIdLen;
 UINT1 au1CapwapBaseNtfWtpId[256];
 UINT4 u4CapwapBaseNtfRadioId;
 INT4 i4CapwapBaseNtfChannelType;
 INT4 i4CapwapBaseNtfAuthenMethod;
 INT4 i4CapwapBaseNtfChannelDownReason;

 INT4 i4CapwapBaseNtfStationIdListLen;
 UINT1 au1CapwapBaseNtfStationIdList[1024];
 INT4 i4CapwapBaseNtfAuthenFailureReason;
 INT4 i4CapwapBaseNtfRadioOperStatusFlag;
 INT4 i4CapwapBaseNtfRadioStatusCause;
 INT4 i4CapwapBaseNtfJoinFailureReason;
 INT4 i4CapwapBaseNtfImageFailureReason;
 INT4 i4CapwapBaseNtfConfigMsgErrorType;

 INT4 i4CapwapBaseNtfMsgErrorElementsLen;
 UINT1 au1CapwapBaseNtfMsgErrorElements[256];
 INT4 i4CapwapBaseChannelUpDownNotifyEnable;
 INT4 i4CapwapBaseDecryptErrorNotifyEnable;
 INT4 i4CapwapBaseJoinFailureNotifyEnable;
 INT4 i4CapwapBaseImageUpgradeFailureNotifyEnable;
 INT4 i4CapwapBaseConfigMsgErrorNotifyEnable;
 INT4 i4CapwapBaseRadioOperableStatusNotifyEnable;
 INT4 i4CapwapBaseAuthenFailureNotifyEnable;


    UINT4 u4FsCapwapControlUdpPort;

    INT4 i4FsCapwapControlChannelDTLSPolicyOptionsLen;
 UINT1 au1FsCapwapControlChannelDTLSPolicyOptions[256];

 INT4 i4FsCapwapDataChannelDTLSPolicyOptionsLen;
 UINT1 au1FsCapwapDataChannelDTLSPolicyOptions[256];

 INT4 i4FsWlcDiscoveryModeLen;
 UINT1 au1FsWlcDiscoveryMode[256];
 INT4 i4FsCapwapWtpModeIgnore;
 INT4 i4FsCapwapDebugMask;
 INT4 i4FsDtlsDebugMask;
 INT4 i4FsDtlsEncryption;
 INT4 i4FsDtlsEncryptAlogritham;
 INT4 i4FsStationType;
        INT4 i4FsAcTimestampTrigger;
 INT4 i4FsCapwapWssDataDebugMask;
} tCapwapGlbMib;


typedef struct
{
            tCapwapMibFsWtpModelEntry       MibObject;
            UINT1                           au1LastUpdated[256];
            INT4                            i4LastUpdatedLen;
} tCapwapFsWtpModelEntry;


typedef struct
{
            tCapwapMibFsWtpRadioEntry       MibObject;
} tCapwapFsWtpRadioEntry;


typedef struct
{
 tCapwapMibFsCapwapWhiteListEntry       MibObject;
} tCapwapFsCapwapWhiteListEntry;


typedef struct
{
 tCapwapMibFsCapwapBlackListEntry       MibObject;
} tCapwapFsCapwapBlackListEntry;


typedef struct
{
 tCapwapMibFsCapwapWtpConfigEntry       MibObject;
} tCapwapFsCapwapWtpConfigEntry;


typedef struct
{
 tCapwapMibFsCapwapLinkEncryptionEntry       MibObject;
} tCapwapFsCapwapLinkEncryptionEntry;


typedef struct
{
 tCapwapMibFsCapwapDefaultWtpProfileEntry       MibObject;
} tCapwapFsCapwapDefaultWtpProfileEntry;


typedef struct
{
 tCapwapMibFsCapwapDnsProfileEntry       MibObject;
} tCapwapFsCapwapDnsProfileEntry;


typedef struct
{
 tCapwapMibFsWtpNativeVlanIdTable       MibObject;
} tCapwapFsWtpNativeVlanIdTable;

typedef struct
{
 tCapwapMibFsWtpLocalRoutingTable       MibObject;
} tCapwapFsWtpLocalRoutingTable;


typedef struct
{
 tCapwapMibFsCawapDiscStatsEntry       MibObject;
} tCapwapFsCawapDiscStatsEntry;


typedef struct
{
 tCapwapMibFsCawapJoinStatsEntry       MibObject;
} tCapwapFsCawapJoinStatsEntry;


typedef struct
{
 tCapwapMibFsCawapConfigStatsEntry       MibObject;
} tCapwapFsCawapConfigStatsEntry;


typedef struct
{
 tCapwapMibFsCawapRunStatsEntry       MibObject;
} tCapwapFsCawapRunStatsEntry;


typedef struct
{
 tCapwapMibFsCapwapWirelessBindingEntry       MibObject;
} tCapwapFsCapwapWirelessBindingEntry;


typedef struct
{
 tCapwapMibFsCapwapStationWhiteListEntry       MibObject;
} tCapwapFsCapwapStationWhiteListEntry;


typedef struct
{
 tCapwapMibFsCapwapWtpRebootStatisticsEntry       MibObject;
} tCapwapFsCapwapWtpRebootStatisticsEntry;


typedef struct
{
 tCapwapMibFsCapwapWtpRadioStatisticsEntry       MibObject;
} tCapwapFsCapwapWtpRadioStatisticsEntry;

typedef struct
{
 tCapwapMibFsCapwapWtpDot11StatisticsEntry       MibObject;
} tCapwapFsCapwapWtpDot11StatisticsEntry;


typedef struct
{
 tCapwapMibCapwapBaseAcNameListEntry       MibObject;
} tCapwapCapwapBaseAcNameListEntry;


typedef struct
{
 tCapwapMibCapwapBaseMacAclEntry       MibObject;
} tCapwapCapwapBaseMacAclEntry;


typedef struct
{
 tCapwapMibCapwapBaseWtpProfileEntry       MibObject;
    UINT4                                     u4RadioIndex;
    UINT4                                     u4SystemUpTime;
} tCapwapCapwapBaseWtpProfileEntry;


typedef struct
{
 tCapwapMibCapwapBaseWtpStateEntry       MibObject;
} tCapwapCapwapBaseWtpStateEntry;


typedef struct
{
 tCapwapMibCapwapBaseWtpEntry       MibObject;
} tCapwapCapwapBaseWtpEntry;


typedef struct
{
 tCapwapMibCapwapBaseWirelessBindingEntry       MibObject;
} tCapwapCapwapBaseWirelessBindingEntry;


typedef struct
{
 tCapwapMibCapwapBaseStationEntry       MibObject;
} tCapwapCapwapBaseStationEntry;


typedef struct
{
 tCapwapMibCapwapBaseWtpEventsStatsEntry       MibObject;
} tCapwapCapwapBaseWtpEventsStatsEntry;


typedef struct
{
 tCapwapMibCapwapBaseRadioEventsStatsEntry       MibObject;
} tCapwapCapwapBaseRadioEventsStatsEntry;




/* -------------------------------------------------------
 *
 *                CAPWAP   Data Structures
 *
 * ------------------------------------------------------*/


typedef struct CAPWAP_GLOBALS {
 tTimerListId        capwapTmrLst;
 UINT4               u4CapwapTrc;
 tOsixTaskId         capwapTaskId;
 UINT1               au1TaskSemName[8];
 tOsixSemId          capwapTaskSemId;
 tOsixQId            capwapQueId;
 tCapwapGlbMib       CapwapGlbMib;
 UINT4               u4CapwTrapStatus ;
 UINT4               u4CapwCriticalTrapStatus ;
 INT4                i4CapwApRunStateCount;
 INT4                i4CapwApIdleStateCount;
} tCapwapGlobals;


/*-----------------------------------------------------------------------*/


