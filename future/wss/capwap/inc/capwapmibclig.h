/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: capwapmibclig.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $ 
*
* Description: Extern Declaration of the Mib objects
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 CapwapBaseWtpSessionsLimit[220];

extern UINT4 CapwapBaseStationSessionsLimit[220];

extern UINT4 CapwapBaseAcNameListId[220];

extern UINT4 CapwapBaseAcNameListName[20];

extern UINT4 CapwapBaseAcNameListPriority[20];

extern UINT4 CapwapBaseAcNameListRowStatus[20];

extern UINT4 CapwapBaseMacAclId[20];

extern UINT4 CapwapBaseMacAclStationId[20];

extern UINT4 CapwapBaseMacAclRowStatus[20];

extern UINT4 CapwapBaseWtpProfileId[20];

extern UINT4 CapwapBaseWtpProfileName[20];

extern UINT4 CapwapBaseWtpProfileWtpMacAddress[20];

extern UINT4 CapwapBaseWtpProfileWtpModelNumber[20];

extern UINT4 CapwapBaseWtpProfileWtpName[20];

extern UINT4 CapwapBaseWtpProfileWtpLocation[20];

extern UINT4 CapwapBaseWtpProfileWtpStaticIpEnable[20];

extern UINT4 CapwapBaseWtpProfileWtpStaticIpType[20];

extern UINT4 CapwapBaseWtpProfileWtpStaticIpAddress[20];

extern UINT4 CapwapBaseWtpProfileWtpNetmask[20];

extern UINT4 CapwapBaseWtpProfileWtpGateway[20];

extern UINT4 CapwapBaseWtpProfileWtpFallbackEnable[20];

extern UINT4 CapwapBaseWtpProfileWtpEchoInterval[20];

extern UINT4 CapwapBaseWtpProfileWtpIdleTimeout[20];

extern UINT4 CapwapBaseWtpProfileWtpMaxDiscoveryInterval[20];

extern UINT4 CapwapBaseWtpProfileWtpReportInterval[20];

extern UINT4 CapwapBaseWtpProfileWtpStatisticsTimer[20];

extern UINT4 CapwapBaseWtpProfileWtpEcnSupport[20];

extern UINT4 CapwapBaseWtpProfileRowStatus[20];

extern UINT4 CapwapBaseWtpStateWtpId[20];


extern UINT4 CapwapBaseWirelessBindingRadioId[20];


extern UINT4 CapwapBaseStationId[20];

extern UINT4 CapwapBaseWtpCurrId[20];

extern UINT4 CapwapBaseRadioEventsWtpRadioId[20];


extern UINT4 CapwapBaseAcMaxRetransmit[20];

extern UINT4 CapwapBaseAcChangeStatePendingTimer[20];

extern UINT4 CapwapBaseAcDataCheckTimer[20];

extern UINT4 CapwapBaseAcDTLSSessionDeleteTimer[20];

extern UINT4 CapwapBaseAcEchoInterval[20];

extern UINT4 CapwapBaseAcRetransmitInterval[20];

extern UINT4 CapwapBaseAcSilentInterval[20];

extern UINT4 CapwapBaseAcWaitDTLSTimer[20];

extern UINT4 CapwapBaseAcWaitJoinTimer[20];

extern UINT4 CapwapBaseAcEcnSupport[20];

extern UINT4 CapwapBaseNtfWtpId[20];

extern UINT4 CapwapBaseNtfRadioId[20];

extern UINT4 CapwapBaseNtfChannelType[20];

extern UINT4 CapwapBaseNtfAuthenMethod[20];

extern UINT4 CapwapBaseNtfChannelDownReason[20];

extern UINT4 CapwapBaseNtfStationIdList[20];

extern UINT4 CapwapBaseNtfAuthenFailureReason[20];

extern UINT4 CapwapBaseNtfRadioOperStatusFlag[20];

extern UINT4 CapwapBaseNtfRadioStatusCause[20];

extern UINT4 CapwapBaseNtfJoinFailureReason[20];

extern UINT4 CapwapBaseNtfImageFailureReason[20];

extern UINT4 CapwapBaseNtfConfigMsgErrorType[20];

extern UINT4 CapwapBaseNtfMsgErrorElements[20];

extern UINT4 CapwapBaseChannelUpDownNotifyEnable[20];

extern UINT4 CapwapBaseDecryptErrorNotifyEnable[20];

extern UINT4 CapwapBaseJoinFailureNotifyEnable[20];

extern UINT4 CapwapBaseImageUpgradeFailureNotifyEnable[20];

extern UINT4 CapwapBaseConfigMsgErrorNotifyEnable[20];

extern UINT4 CapwapBaseRadioOperableStatusNotifyEnable[20];

extern UINT4 CapwapBaseAuthenFailureNotifyEnable[20];

extern UINT4 FsCapwapModuleStatus[0];

extern UINT4 FsCapwapSystemControl[0];

extern UINT4 FsCapwapControlUdpPort[0];

extern UINT4 FsCapwapControlChannelDTLSPolicyOptions[0];

extern UINT4 FsCapwapDataChannelDTLSPolicyOptions[0];

extern UINT4 FsWlcDiscoveryMode[0];

extern UINT4 FsCapwapWtpModeIgnore[0];

extern UINT4 FsCapwapDebugMask[0];

extern UINT4 FsDtlsDebugMask[0];

extern UINT4 FsDtlsEncryption[0];

extern UINT4 FsDtlsEncryptAlgorithm[0];

extern UINT4 FsStationType[0];

extern UINT4 FsAcTimestampTrigger[0];

extern UINT4 FsCapwapWssDataDebugMask[0];

extern UINT4 FsCapwapWtpModelNumber[0];

extern UINT4 FsNoOfRadio[0];

extern UINT4 FsCapwapWtpMacType[0];

extern UINT4 FsCapwapWtpTunnelMode[0];

extern UINT4 FsCapwapImageName[0];

extern UINT4 FsCapwapQosProfileName[0];

extern UINT4 FsMaxStations[0];

extern UINT4 FsWtpModelRowStatus[0];

extern UINT4 FsNumOfRadio[0];

extern UINT4 FsWtpRadioType[0];

extern UINT4 FsRadioAdminStatus[0];

extern UINT4 FsMaxSSIDSupported[0];

extern UINT4 FsWtpRadioRowStatus[0];


extern UINT4 FsCapwapWhiteListId[0];

extern UINT4 FsCapwapWhiteListWtpBaseMac[0];

extern UINT4 FsCapwapWhiteListRowStatus[0];

extern UINT4 FsCapwapBlackListId[0];

extern UINT4 FsCapwapBlackListWtpBaseMac[0];

extern UINT4 FsCapwapBlackListRowStatus[0];

extern UINT4 FsCapwapWtpReset[0];

extern UINT4 FsCapwapClearConfig[0];

extern UINT4 FsWtpDiscoveryType[0];

extern UINT4 FsWtpCountryString[0];

extern UINT4 FsWtpCrashDumpFileName[0];

extern UINT4 FsWtpMemoryDumpFileName[0];

extern UINT4 FsWtpDeleteOperation[0];

extern UINT4 FsCapwapClearApStats[0];

extern UINT4 FsCapwapWtpConfigRowStatus[0];

extern UINT4 FsCapwapEncryptChannel[0];

extern UINT4 FsCapwapEncryptChannelStatus[0];

extern UINT4 FsCapwapEncryptChannelRowStatus[0];

extern UINT4 FsCapwapDefaultWtpProfileModelNumber[0];

extern UINT4 FsCapwapDefaultWtpProfileWtpFallbackEnable[0];

extern UINT4 FsCapwapDefaultWtpProfileWtpEchoInterval[0];

extern UINT4 FsCapwapDefaultWtpProfileWtpIdleTimeout[0];

extern UINT4 FsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval[0];

extern UINT4 FsCapwapDefaultWtpProfileWtpReportInterval[0];

extern UINT4 FsCapwapDefaultWtpProfileWtpStatisticsTimer[0];

extern UINT4 FsCapwapDefaultWtpProfileWtpEcnSupport[0];

extern UINT4 FsCapwapDefaultWtpProfileRowStatus[0];

extern UINT4 FsCapwapDnsServerIp[0];

extern UINT4 FsCapwapDnsDomainName[0];

extern UINT4 FsCapwapDnsProfileRowStatus[0];

extern UINT4 FsWtpNativeVlanId[0];

extern UINT4 FsWtpNativeVlanIdRowStatus[0];

extern UINT4 FsWtpLocalRouting[0];

extern UINT4 FsWtpLocalRoutingRowStatus[0];

extern UINT4 FsCapwapFsAcTimestampTrigger[0];

extern UINT4 FsCapwapFragReassembleStatus[0];

extern UINT4 FsCapwapDiscStatsRowStatus[0];

extern UINT4 FsCapwapJoinStatsRowStatus[0];

extern UINT4 FsCapwapConfigStatsRowStatus[0];

extern UINT4 FsCapwapRunStatsRowStatus[0];

extern UINT4 FsCapwapWirelessBindingVirtualRadioIfIndex[0];

extern UINT4 FsCapwapWirelessBindingType[0];

extern UINT4 FsCapwapWirelessBindingRowStatus[0];

extern UINT4 FsCapwapStationWhiteListId[0];

extern UINT4 FsCapwapStationWhiteListStationId[0];

extern UINT4 FsCapwapStationWhiteListRowStatus[0];

extern UINT4 FsCapwapWtpRebootStatisticsRebootCount[0];

extern UINT4 FsCapwapWtpRebootStatisticsAcInitiatedCount[0];

extern UINT4 FsCapwapWtpRebootStatisticsLinkFailureCount[0];

extern UINT4 FsCapwapWtpRebootStatisticsSwFailureCount[0];

extern UINT4 FsCapwapWtpRebootStatisticsHwFailureCount[0];

extern UINT4 FsCapwapWtpRebootStatisticsOtherFailureCount[0];

extern UINT4 FsCapwapWtpRebootStatisticsUnknownFailureCount[0];

extern UINT4 FsCapwapWtpRebootStatisticsLastFailureType[0];

extern UINT4 FsCapwapWtpRebootStatisticsRowStatus[0];

extern UINT4 FsCapwapWtpRadioLastFailType[0];

extern UINT4 FsCapwapWtpRadioResetCount[0];

extern UINT4 FsCapwapWtpRadioSwFailureCount[0];

extern UINT4 FsCapwapWtpRadioHwFailureCount[0];

extern UINT4 FsCapwapWtpRadioOtherFailureCount[0];

extern UINT4 FsCapwapWtpRadioUnknownFailureCount[0];

extern UINT4 FsCapwapWtpRadioConfigUpdateCount[0];

extern UINT4 FsCapwapWtpRadioChannelChangeCount[0];

extern UINT4 FsCapwapWtpRadioBandChangeCount[0];

extern UINT4 FsCapwapWtpRadioCurrentNoiseFloor[0];

extern UINT4 FsCapwapWtpRadioStatRowStatus[0];
