/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: capwapclidefn.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 * Description: This file contains definitions for capwap module.
 *******************************************************************/

#ifndef __CAPWAPDEFN_H__
#define __CAPWAPDEFN_H__
/*
#define CAPWAP_SUCCESS        (OSIX_SUCCESS)
#define CAPWAP_FAILURE        (OSIX_FAILURE)
*/
#define  CAPWAP_TASK_PRIORITY              100
#define  CAPWAP_TASK_NAME                  "IAP1"
#define  CAPWAP_QUEUE_NAME                 (const UINT1 *) "CAPWAPQ"
#define  CAPWAP_MUT_EXCL_SEM_NAME          (const UINT1 *) "CAPWAPM"
#define  CAPWAP_QUEUE_DEPTH                100
#define  CAPWAP_SEM_CREATE_INIT_CNT        1


#define  CAPWAP_ENABLED                    1
#define  CAPWAP_DISABLED                   2

#define  CAPWAP_TIMER_EVENT             0x00000001 
#define  CAPWAP_QUEUE_EVENT             0x00000002
#define  MAX_CAPWAP_DUMMY                   1


#endif  /* __CAPWAPDEFN_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file capwapdefn.h                      */
/*-----------------------------------------------------------------------*/

