
/******************************************************************************/
/* Copyright (C) Future Software, 2005-2006                                   */
/* Licensee Future Communications Software, 2005-2006                         */
/* $Id: capwapprot.h,v 1.5 2017/11/24 10:37:02 siva Exp $               */
/*                                                                            */
/*  FILE NAME             :                                                   */
/*  PRINCIPAL AUTHOR      : FutureSoft                                        */
/*  SUBSYSTEM NAME        :                                                   */
/*  MODULE NAME           :                                                   */
/*  LANGUAGE              : C                                                 */
/*  TARGET ENVIRONMENT    : MontaVista Linux                                  */
/*  DATE OF FIRST RELEASE :                                                   */
/*  AUTHOR                :                                                   */
/*  FUNCTIONS DEFINED     :                                                   */
/*                                                                            */
/*  DESCRIPTION           :                                                   */
/******************************************************************************/
/*  Change History                                                            */
/*                                                                            */
/*  Version               :                                                   */
/*  Date(DD/MM/YYYY)      :                                                   */
/*  Modified by           :                                                   */
/*  Description of change :                                                   */
/******************************************************************************/

#ifndef _CAPWAP_PROT_H_
#define _CAPWAP_PROT_H_

#include "arp.h"
#include "wssifradioproto.h"
#include "wssifcapdb.h"
#include "wssifcapmacr.h"
#include "wssifcapproto.h"
/* Function prototypes */


INT4
CapwapGetPriAcNameWithPrioList(tACNameWithPrio *pAcNameWithPriolist,
                                UINT4 *pMsgLen, UINT2 u2IntProfileId);

PUBLIC UINT4 gu4CapwapSysLogId;

INT4
CapwapGetHash (tRemoteSessionManager *pSess);   
UINT1
CapwapCreateBinding (UINT1 *pu1WtpModelName,UINT4 u4ProfileId);   
INT4
CapwapDTLSInit ( VOID );

UINT1 CapwapCreateAutoProfile (tMacAddr , UINT1 *);

VOID
CapwapAssembleVendorDot11nCfg (UINT1 *pTxPkt, 
                                 tRadioIfDot11nCfg *);
VOID
capwapAssembleWtpDot11Statistics(UINT1 *pTxPkt, 
                                 tWtpdot11StatsElement *dot11StatsElement);
VOID
CapwapAssembleVendorSpecificPayloadWTPEventRequest(
                             UINT1 *pTxPkt,
                             tVendorSpecPayload *vendPld,
                             tWtpCapwapSessStatsElement *capwapStatsElement,
                             tBssIDMacHdlrStatsElement *MacHdlrStatsElement,
               tRadioClientStats   *RadioClientStatsElement,
        tApParamsStats *ApParamsElement);
#ifdef RFMGMT_WANTED
VOID 
CapwapAssembleVendSpecNeighAP(
    UINT1 * pTxPkt,
    tVendorSpecPayload *pvendPld);

VOID 
CapwapAssembleVendSpecClientScan(
    UINT1 * pTxPkt,
    tVendorSpecPayload *pvendPld);

VOID 
CapwapAssembleNeighborAPConfig (UINT1 * pTxPkt,
    tNeighApTableConfig *pvendPld);
VOID 
CapwapAssembleClientSNRConfig (UINT1 * pTxPkt,
    tClientTableConfig *pvendPld);
VOID 
CapwapAssembleChSwitchStatus (UINT1 * pTxPkt,
    tChSwitchStatusTable *pvendPld);

VOID 
CapwapAssembleVendorTpcSpectMgmt (UINT1 * pTxPkt,
    tTpcSpectMgmtTable *pvendPld);

VOID
CapwapAssembleVendorDfsParams (UINT1 * pTxPkt,
    tDfsParamsTable *pvendPld);   

#endif
UINT4 CapwapTmToSec (tUtlTm * tm);

VOID
CapwapAssembleVendSpecMultiDomainInfo (UINT1 *, UINT2, 
      UINT2 *, tRadioIfGetDB *);

INT4 CapwapProcessConfigStationResp (tSegment *,
                                     tCapwapControlPacket *, 
                                     tRemoteSessionManager  *);

INT4
CapwapGetStatisticsTimer (tStatisticsTimer *pStats, UINT4 *pMsgLen);

INT4
CapwapGetJoinRespVendorPayload (tVendorSpecPayload *pVendorPayload, 
                                  UINT4 *pMsgLen,UINT1 u1Index,UINT2 u2InternalId);
INT4
CapwapGetConfigStatVendorPayload (tVendorSpecPayload *pVendorPayload, 
                                  UINT4 *pMsgLen,UINT1 u1Index);

INT4
CapwapUtilParseSubIdNew (UINT1 **ppu1TempPtr);

tSNMP_OID_TYPE     *
CapwapMakeObjIdFromDotNew (INT1 *);

VOID CapwapProcessCapwapHdr PROTO ((UINT1 *, tCapwapHdr *,
                                    tCapwapCtrlHdr *, UINT4 *));
VOID capwapProcessCapwapCtrlhdr PROTO ((UINT1 *, tCapwapCtrlHdr *));
VOID  capwapProcessCapwapMsgElements PROTO ((UINT1 *, tCapwapMsgElement *,
                                             UINT2));

VOID CapwapSnmpifSendTrap PROTO ((UINT1 , VOID *));

/* Input will be the message buffer and the message type*/
typedef INT4 (*tCapwapParseCtrlMsg)(UINT1 *, tCapwapControlPacket *, UINT2);
typedef INT4 (*tIeeeParseCtrlMsg) (UINT1 *, tCapwapControlPacket *, UINT2);
INT4 CapwapProcessConfigUpdateResp (tSegment *,
                                    tCapwapControlPacket *,
                                    tRemoteSessionManager  *);
extern INT4 ApHdlrProcessDataTransferResp(tSegment*,
                                          tCapwapControlPacket*,
                                          tRemoteSessionManager  *);
INT4 CapwapProcessClearConfigResp(tSegment*,tCapwapControlPacket*,
                                  tRemoteSessionManager *);
extern INT4 CapwapProcessResetRequest(tSegment*,
                                      tCapwapControlPacket*,
                                      tRemoteSessionManager *);
INT4 CapwapProcessResetResponse(tSegment*,
                                tCapwapControlPacket*,
                                tRemoteSessionManager  *);
INT4 CapwapProcessPrimaryDiscoveryRequest (tSegment *, 
                                           tCapwapControlPacket *,     
                                           tRemoteSessionManager *);     
INT4 CapwapProcessPrimaryDiscoveryResponse (tSegment *,     
                                            tCapwapControlPacket *,     
                                            tRemoteSessionManager *); 
extern INT4 CapwapProcessClearConfigRequest(tSegment*,
                                            tCapwapControlPacket*,
                                            tRemoteSessionManager *);
extern INT4 CapwapProcessConfigStationReq (tSegment*,
                                           tCapwapControlPacket*,
                                           tRemoteSessionManager  *);
INT4    CapwapProcessWtpEventRequest (tSegment*,
                                      tCapwapControlPacket*,
                                      tRemoteSessionManager *);
INT4    CapwapProcessWtpEventResponse (tSegment*,
                                       tCapwapControlPacket*,
                                       tRemoteSessionManager  *);
INT4 CapwapProcessDataTransferResp(tSegment*,
                                   tCapwapControlPacket*,
                                   tRemoteSessionManager*);
INT4 CapwapProcessDataTransferRequest(tSegment*,
                                      tCapwapControlPacket*,
                                      tRemoteSessionManager*);
extern INT4 ApHdlrProcessDataTransferRequest (tSegment*,
                                              tCapwapControlPacket*,
                                              tRemoteSessionManager *); 
INT4
CapwapProcessWlanConfigReq (tCRU_BUF_CHAIN_HEADER *,
                            tCapwapControlPacket *,
                            tRemoteSessionManager *);
INT4
CapwapProcessWlanConfigRsp (tCRU_BUF_CHAIN_HEADER *,
                            tCapwapControlPacket *,
                            tRemoteSessionManager *);

/************************************************    ********************/

INT4 CapwapProcessWssIfMsg (UINT1 u1MsgType,
                            unCapwapMsgStruct *pWssMsgStruct);

/************************************************    ********************/

/**************************capwaputil.c****************************/
VOID CapwapPktDump (tSegment *);
INT4 CapwapGetMsgTypeFromBuf (UINT1 * , UINT4 *);
INT4 CapwapGetSeqNumFromPkt (UINT1 *, UINT1 *);
INT4 CapwapFragmentReceived  (tCRU_BUF_CHAIN_HEADER *);
INT4 CapwapGetWtpBoardDataFromBuf (UINT1 *, UINT4 ,tWtpBoardData *);
INT4 CapwapGetAcDescriptorFromBuf (UINT1 *, UINT4,tAcDescriptor *,UINT2);
INT4 CapwapGetAcNameFromBuf (UINT1 *, UINT4 ,tACName *,UINT2);
INT4 CapwapGetCtrlIP4AddrFromBuf (UINT1 *, UINT4 ,tCtrlIpAddr *,UINT2);
INT4 CapwapGetAcIp4ListFromBuf (UINT1 *, UINT4 ,tACIplist *,UINT2);
INT4 CapwapGetVendorInfoFromBuf (UINT1 *, UINT4,tVendorSpecPayload *,UINT2);
INT4 CapwapGetResultCodeFromBuf(UINT1 *, UINT4,tResCode *, UINT2);
INT4 CapwapGetEcnSupportFromBuf(UINT1 *, UINT4, tEcnSupport *, UINT2);
INT4 CapwapGetLocalIPAddrFromBuf(UINT1 *, UINT4, tLocalIpAddr *, UINT2);
INT4 CapwapGetTransportProtFromBuf(UINT1 *, UINT4, 
                                   tCapwapTransprotocol *, UINT2);
INT4 CapwapGetImageIdFromBuf(UINT1 *, UINT4,
                             tImageId  *, UINT2);
INT4 CapwapGetWtpProfile (tMacAddr ,UINT2 *);
INT4 CapwapConstructCpHeader (tCapwapHdr *);
INT4 CapwapGetWtpBaseMacAddr (tCRU_BUF_CHAIN_HEADER *,
                              tCapwapControlPacket *,
                              tMacAddr );
INT4
CapwapProcessKeepAlivePacket (tCRU_BUF_CHAIN_HEADER *,
                              tRemoteSessionManager  *);
INT4 CapwapUpdateResultCode (tResCode *, UINT4 , UINT4 *);
INT4 DbgDumpCapwapPkt (tCRU_BUF_CHAIN_HEADER *,UINT1 );
extern INT4
CapwapProcessConfigUpdateRequest (tSegment *,
                                  tCapwapControlPacket *, 
                                  tRemoteSessionManager *);

/*************************capservicemain.c**************************/

INT4
CapwapGetWtpRebootStats(tWtpRebootStatsElement *pWtpRebootStats, 
                        UINT4 *pMsgLen);

VOID tmrSerCallback (tTimerListId timerListId);
INT4 CapwapEventSend (VOID);
INT4 CapwapEngineEnable (VOID);
INT4 CapwapEngineDisable (VOID);

INT4 CapwapGetDiscResponseCount (UINT1 *);
INT4 CapwapGetDetailsFrmBestAC (tACInfoAfterDiscovery *pAcInfo);

INT4 CapwapSetVendSpecInfo (UINT1 *pRcvBuf,
                       tCapwapControlPacket *pCwMsg,
                       tConfigStatusRsp *pConfStatRsp,
                       UINT2 u2MsgIndex);

INT4 CapwapGetDiscResponseCountUpdateAcRef (UINT1 *pAcInfoCount, 
                                            UINT1 *pDiscReSend);
INT4 CapwapEnqueCtrlPkts (tCapwapRxQMsg *);
INT4 CapwapProcessCtrlRxMsg (VOID);
INT4 CapwapProcessFragRxMsg (VOID);
INT4 CapwapDataProcessFragRxMsg (VOID);
INT4 CapwapRetransmitPacket (tRemoteSessionManager  *);
INT4 CapwapConstructDiscReq (tDiscReq *);
INT4 CapwapConstructDiscAndSend(tRemoteSessionManager  *);
INT4 WtpEnterDiscoveryState (tRemoteSessionManager *);
INT4 CapwapConstructDiscandSent(tRemoteSessionManager  *);
INT4 WtpEnterJoinState (tACInfoAfterDiscovery *);
VOID CapwapGetNodeType (UINT1 *);
INT4 CapwapGetDiscoveryMode (UINT1 *);
VOID CapwapGetAcIpDetails(UINT4 *, UINT1 *);
INT4 CapwapGetACDescriptor (tAcDescriptor *, UINT4 *);
INT4 CapwapGetACName (tACName *, UINT4 *);
INT4 CapwapGetRadioAdminState(tRadioIfAdminStatus *, UINT4 *);
INT4 CapwapGetStatsTimer(tStatisticsTimer  *,  UINT4 *);
INT4 CapwapGetWtpRebootStatsElement(tWtpRebootStatsElement *, UINT4 *);
INT4 CapwapGetWtpRadioInfo (tWtpRadInfo *, UINT4 *);
INT4 CapwapGetControlIpAddr (tCtrlIpAddr *, UINT4 *);
INT4 CapwapGetVendorPayload (tVendorSpecPayload *, UINT4 *);
INT4 CapwapGetDiscRespVendorPayload (tVendorSpecPayload * pVendorPayload,
                                     UINT4 *pMsgLen, UINT1 u1Index,
                                     UINT2 u2InternalId);
INT4 CapwapGetWtpDiscPadding (tMtuDiscPad *, UINT4 *);
INT4 CapwapGetWtpMacType (tWtpMacType *,UINT4 *);
INT4 CapwapGetWtpFrameTunnelMode (tWtpFrameTunnel *,UINT4 *);
INT4 CapwapGetWtpDescriptor (tWtpDescriptor *,UINT4 *);
INT4 CapwapGetWtpBoardData (tWtpBoardData *,UINT4 *);
INT4 CapwapGetDiscoveryType (tWtpDiscType *, UINT4 *);
INT4 CapwapGetLocationData(tLocationData  *, UINT4 *);
INT4 CapwapGetWtpName(tWtpName *, UINT4 *);
INT4 CapwapGetJoinSessionId(UINT4 *, UINT4 *);
INT4 CapwapGetLocalIpAddr(tLocalIpAddr *, UINT4 *);
INT4 CapwapGetTransProtocol(tCapwapTransprotocol *, UINT4 *, UINT2);
INT4 CapwapGetMaxMessageLen(tMaxMsgLen *, UINT4 *);
INT4 CapwapGetRebootStats(tWtpRebootStatsElement *, UINT4 *);
INT4 CapwapUpdateKernelWlcIpaddr (UINT4 u4WlcIpAddr);
INT4 CapwapGetImageId(tImageId *, UINT4 *);
INT4 CapwapGetResultCode(tResCode *, UINT4 *);
INT4 CapwapGetEcnSupport(tEcnSupport *, UINT4 *);
INT4 CapwapGetCapwapTimers(tCapwapTimer *, UINT4 *, UINT2 );
INT4 CapwapGetDecryptErrReportPeriod(tDecryptErrReportPeriod  *,UINT4 *);
INT4 CapwapGetIdleTimeout(tIdleTimeout *, UINT4 *, UINT2);
INT4 CapwapGetWtpFallback(tWtpFallback *, UINT4 *, UINT2);
INT4 CapwapGetAcNameWithPrioList(tACNameWithPrio *pAcNameWithPriolist,
                                 UINT4 *pMsgLen, 
                                 UINT2 u2IntProfileId);
INT4 CapwapGetAcIpList(tACIplist *, UINT4 *);
INT4 CapwapGetPriAcNameWithPriority (tACNameWithPrio *, UINT4 *);
INT4 CapwapGetWtpStaticIpInfo (tWtpStaticIpAddr *, UINT4 *);
INT4 CapwapGetRadioOperationalState(tRadioIfOperStatus *,UINT4 *);
VOID CapwapPrintMacAddress (UINT1 *, UINT1 *);
INT4 CapwapProcessErrorHandler (tSegment *,
                                tCapwapControlPacket *,
                                tRemoteSessionManager  *);
INT4 CapwapProcessDataRxMsg (VOID);
INT4 CapwapDataKeepAlive (tCRU_BUF_CHAIN_HEADER *,tRemoteSessionManager  **);
INT4
CapwapProcessUnrecognisedRequest (tSegment *,
                                  tCapwapControlPacket *,
                                  tRemoteSessionManager *);

INT4 CapwapGetIeeeAntena (tRadioIfAntenna *, UINT2 *, UINT2);
INT4 CapwapGetIeeeDsc (tRadioIfDSSSPhy *, UINT2 *, UINT2);
INT4 CapwapGetIeeeInfoElem (tRadioIfInfoElement *, UINT2 *, UINT2, UINT1);
INT4 CapwapGetIeeeMacOperation (tRadioIfMacOperation *, UINT2 *,UINT2 );
INT4 CapwapGetIeeeMultiDomainCapability (tRadioIfMultiDomainCap *, 
                                         UINT2 *,
                                         UINT2);
INT4 CapwapGetIeeeOFDMControl (tRadioIfOFDMPhy *, UINT2 *,UINT2);
INT4 CapwapGetIeeeRateSet (tRadioIfRateSet *, UINT2 *,UINT2);
INT4 CapwapGetIeeeTxPower (tRadioIfTxPower *, UINT2 *,UINT2);
INT4 CapwapGetIeeeTxPowerLevel (tRadioIfTxPowerLevel *, UINT2 *,UINT2);
INT4 CapwapGetIeeeRadioConfiguration (tRadioIfConfig *, UINT2 *,UINT2);
INT4 CapwapGetIeeeRadioInformation (tRadioIfInfo *, UINT4 *, UINT2);
INT4 CapwapGetIeeeSupportedRates (tRadioIfSupportedRate *, UINT2 *,UINT2);
INT4 CapwapGetIeeeWtpQos (tRadioIfQos *, UINT2 *, UINT2);

INT4 CapwapCreateWtpProfile (UINT2 );
INT4 CapwapKeepAliveReceived  (tCRU_BUF_CHAIN_HEADER *);
INT4 CapwapDot3FrameReceived  (tCRU_BUF_CHAIN_HEADER *);
INT4 CapwapGetRadioInfoFromBuf (UINT1 *pRcvBuf,
                                UINT4 u4Offset,
                                tRadioIfInfo  *pWtpRadioInfo,
                                UINT2 u2NumMsgElems);
INT4 CapwapGetInternalId (UINT1 *, tCapwapControlPacket *, UINT2 *);
INT4 CapwapServiceMainTaskLock (VOID);
INT4 CapwapServiceMainTaskUnLock (VOID);
VOID CapwapCheckRelinquishCounters(VOID);
#ifdef BAND_SELECT_WANTED
UINT1
CapwapUpdateBandSelectStatus (tVendorSpecPayload * pVendor);
#endif
VOID CapwapCheckServRelinquishCounters(VOID);
#ifdef WLC_WANTED
VOID CapwapCheckDiscRelinquishCounters (VOID);
#endif

INT4 CapwapCheckModuleStatus (VOID);
UINT1 CapwapSendWebAuthStatusToSta (tVendorSpecPayload * pVendor);
INT4 CapwapServiceTaskLock (VOID);
INT4  CapwapServiceTaskUnLock (VOID);


/**********************************************************************/

INT4
CapwapGetStateEventRequestMsgElements(tChangeStateEvtReq *,
                                      tRemoteSessionManager  *);
INT4
CapwapSetIeeeAntna (UINT1 *,
                    tCapwapControlPacket *,
                    tConfigStatusRsp *,
                    UINT2 );
INT4
CapwapSetIeeeDSC (UINT1 *,
                    tCapwapControlPacket *,
                    tConfigStatusRsp *,
                    UINT2 );
INT4
CapwapSetIeeeInfoElem (UINT1 *,
                    tCapwapControlPacket *,
                    tConfigStatusRsp *,
                    UINT2 );
INT4
CapwapSetMacOperation (UINT1 *,
                    tCapwapControlPacket *,
                    tConfigStatusRsp *,
                    UINT2 );
INT4
CapwapSetIeeeMultiDomainCapability (UINT1 *,
                    tCapwapControlPacket *,
                    tConfigStatusRsp *,
                    UINT2 );
INT4
CapwapSetIeeeOFDMControl (UINT1 *,
                    tCapwapControlPacket *,
                    tConfigStatusRsp *,
                    UINT2 );

INT4
CapwapSetIeeeRateSet ( UINT1 *,
                    tCapwapControlPacket *,
                    tConfigStatusRsp *,
                    UINT2 );
INT4
CapwapSetIeeeSupportedRates ( UINT1 *,
                    tCapwapControlPacket *,
                    tConfigStatusRsp *,
                    UINT2 );
INT4
CapwapSetIeeeTxPower ( UINT1 *,
                    tCapwapControlPacket *,
                    tConfigStatusRsp *,
                    UINT2 );

INT4
CapwapSetIeeeTxPowerLevel  ( UINT1 *,
                    tCapwapControlPacket *,
                    tConfigStatusRsp *,
                    UINT2 );

INT4
CapwapSetIeeeWtpRadioConfig ( UINT1 *,
                    tCapwapControlPacket *,
                    tConfigStatusRsp *,
                    UINT2 );
INT4
CapwapSetIeeeRadioInfo (UINT1 *,
                    tCapwapControlPacket *,
                    tConfigStatusRsp *,
                    UINT2);
INT4
CapwapSetIeeeWtpQos (UINT1 *,
                    tCapwapControlPacket *,
                    tConfigStatusRsp *,
                    UINT2);
INT4
CapwapSetRadioAdminState(UINT1 *,tCapwapControlPacket *, 
                         tRadioIfAdminStatus *, UINT2 );
INT4
CapwapSetRadioOperState(UINT1 *,tCapwapControlPacket *,
                        tRadioIfOperStatus *, UINT2);

INT4 CapwapSetDeleteStation(UINT1 *,tCapwapControlPacket *, 
                            tStationConfRsp *, UINT2);
INT4 CapwapSetAddStation(UINT1 *,tCapwapControlPacket *, 
                         tStationConfRsp * , UINT2);
INT4 CapwapSetIeeeStation(UINT1 *,tCapwapControlPacket *, 
                          tStationConfRsp * , UINT2);
INT4 CapwapSetVendorPayload(UINT1 *,tCapwapControlPacket *, 
                             tStationConfRsp * , UINT2);
INT4 CapwapSetIeeeStaSessKey(UINT1 *,tCapwapControlPacket *, 
                             tStationConfRsp * , UINT2);

INT4
CapwapSetIeeeDSCUpdate (UINT1 *,
                        tCapwapControlPacket *,
                        tConfigUpdateRsp *,
                        UINT2 u2MsgIndex);

INT4
CapwapSetIeeeInfoElemUpdate (UINT1 *,
                        tCapwapControlPacket *,
                        tConfigUpdateRsp *,
                        UINT2 u2MsgIndex);
INT4
CapwapSetIeeeOFDMControlUpdate (UINT1 *,
                                tCapwapControlPacket *,
                                tConfigUpdateRsp *,
                                UINT2 u2MsgIndex);

INT4
CapwapSetIeeeTxPowerUpdate ( UINT1 *,
                             tCapwapControlPacket *,
                             tConfigUpdateRsp *,
                             UINT2 u2MsgIndex);


INT4
CapwapSetRadioAdminStateUpdate(UINT1 *,tCapwapControlPacket *, 
                               tConfigUpdateRsp *, UINT2 );


INT4
CapwapSetIeeeQualityUpdate(UINT1 *,tCapwapControlPacket *, 
                           tConfigUpdateRsp *, UINT2 );

INT4
CapwapSetIeeeRadioInfoUpdate(UINT1 *,tCapwapControlPacket *, 
                             tConfigUpdateRsp *, UINT2 );

INT4
CapwapSetIeeeRadioConfigUpdate(UINT1 *,tCapwapControlPacket *, 
                               tConfigUpdateRsp *, UINT2 );

INT4
CapwapSetIeeeRateSetUpdate(UINT1 *,tCapwapControlPacket *, 
                           tConfigUpdateRsp *, UINT2 );

INT4
CapwapSetIeeeMultiDomainCapabilityUpdate(UINT1 *,tCapwapControlPacket *, 
                                         tConfigUpdateRsp *, UINT2 );

INT4
CapwapSetMacOperationUpdate(UINT1 *,tCapwapControlPacket *, 
                            tConfigUpdateRsp *, UINT2 );

INT4
CapwapSetRadioAntennaUpdate(UINT1 *,tCapwapControlPacket *, 
                            tConfigUpdateRsp *, UINT2 );
/**************************capwaptmr.c******************************/
INT4 CapwapTmrInit(VOID);
VOID CapwapTmrInitTmrDesc (VOID);
VOID CapwapFsmTmrExpHandler (VOID);
VOID CapwapRunTmrExpHandler (VOID);
INT4 CapwapDiscTmrStart (UINT1 , UINT4);
INT4 CapwapTmrStart (tRemoteSessionManager *, UINT1 , UINT4 );
INT4 CapwapGlobalTmrStart (tTmrBlk *, UINT1 , UINT4 );
INT4 CapwapResetTmr (tRemoteSessionManager *, UINT1 , UINT4 );
INT4 CapwapTmrStop  (tRemoteSessionManager *,UINT1);
VOID CapwapDiscIntervalTmrExp (VOID *);
VOID CapwapMaxIntervalDiscTmrExp (VOID *pArg);
VOID CapwapSilentIntervalTmrExp (VOID *pArg);
VOID CapwapPrimaryDiscIntervalTmrExp (VOID *pArg);
VOID CapwapDhcpOfferIntervalTmrExp (VOID *pArg);
INT4 CapwapAcquireV4ACAddress(UINT1, UINT1 *,UINT1 );
INT4  CapwapGetDiscReqSentCount (UINT2 *);
INT4 CapwapAcquireV6ACAddress(UINT1, UINT1 *);
VOID CapwapRetransIntervalTmrExp (VOID *pArg);
VOID CapwapEchoIntervalTmrExp (VOID *pArg);
VOID CapwapDataChnlDeadIntTmrExp (VOID *pArg);
VOID CapwapDataChnlKeepAliveTmrExp (VOID *pArg);
VOID CapwapFsmTmrExp (VOID *pArg);
VOID CapwapReassemblyTmrExp (VOID *pArg);
VOID CapwapPathMtuUpdateTmrExp (VOID *pArg);
VOID CapwapImageDataStartTmrExp (VOID *pArg);
VOID CapwapServiceCPURelinquishTmrExp(VOID *pArg);
VOID CapwapDiscCPURelinquishTmrExp(VOID *pArg);
VOID CapwapRecvCPURelinquishTmrExp(VOID *pArg);
INT4 CapwapStopAllSessionTimers (tRemoteSessionManager *);
INT4 CapwapStopAllGlobalTimers (VOID);
INT4 CapwapStartAllGlobalTimers (VOID);
VOID CapwapStopKeepAlive(tRemoteSessionManager *);
/**************************capwaptx.c********************************/
INT4 CapwapTxMessage (tCRU_BUF_CHAIN_HEADER *, tRemoteSessionManager *,UINT4,UINT1);
INT4 CapwapTxPktAfterFrag (tCRU_BUF_CHAIN_HEADER *, 
                           tRemoteSessionManager *,
                           UINT4, UINT1);

INT4 CapwapDiscoveryTaskLock (VOID);
INT4 CapwapDiscoveryTaskUnLock (VOID);
/**************************capwaprx.c*******************************/



INT4 CapwapCtrlRevIpv6Packet(VOID);
INT4 CapwapDataRecvIpv6Packet(VOID);
INT4 CapwapEnable (VOID);
INT4 CapwapDisable (VOID);
VOID tmrRxCallback (tTimerListId timerListId);
INT4 CapwapUpdatePathMtu (UINT4 u2DestAddr ,
                          UINT4 u4DestPort, 
                          UINT4  *pu4PMTU);

UINT4 CapwapProcessCtrlPacket (tCRU_BUF_CHAIN_HEADER *);
UINT4 CapwapProcessReassemCtrlPacket (tCRU_BUF_CHAIN_HEADER *);
UINT4 CapwapProcessDataPacket (tCRU_BUF_CHAIN_HEADER *);
UINT4 CapwapRecvMemInit (VOID);
VOID  CapwapRecvMemClear (VOID);
INT4 CapwapCtrlUdpSockInit (VOID);
INT4 CapwapDataUdpSockInit (VOID);
INT4 CapwapUdpSock6Init (VOID);
INT4 CapwapDataUdpSock6Init(VOID);
INT4 CapwapDataSelAddFd (VOID);
INT4 CapwapShutdown (VOID);
INT4 CapwapNoShutdown (VOID);
VOID CapwapPacketOnSocket PROTO((INT4 ));
INT4 CapwapRecvCtrlIpv4Packet (VOID);
INT4 CapwapRecvCtrlIpv6Packet (VOID);
INT4 CapwapRecvDataIpv4Packet (VOID);
INT4 CapwapRecvDataIpv6Packet (VOID);
INT4 CapwapCtrlUdpTxPacket (tCRU_BUF_CHAIN_HEADER *,UINT4 ,UINT4 , UINT4);
INT4 CapwapDataUdpTxPacket (tCRU_BUF_CHAIN_HEADER *,UINT4 ,
                            UINT4 , UINT4, UINT1);
INT4  CapwapRxValidatePreamble(UINT1 *);
INT4 CapwapNoShutdownInitAllTimers (VOID);
INT4 CapwapEnableSockets (VOID);
INT4 CapwapDisableSockets (VOID);
INT4 CapwapShutdownStopAllTimers (VOID);
INT4 CapwapShutdownTearSessions (UINT4);
INT4 CapwapShutdownDeleteProfileAllDBs (VOID);
INT4 CapwapShutdownFreeMemory (VOID);
INT4 CapwapNoShutdownAllocMemory (VOID);
VOID CapwapReleaseMemBlock ( UINT2 , UINT1 *, UINT1 );

INT4 CapwapRecvMainTaskLock (VOID);
INT4 CapwapRecvMainTaskUnLock (VOID);


typedef INT4 (*tParseMsgElemEntry)(UINT1 *, UINT2, 
                                   tCapwapMsgElement *, UINT2 *);
typedef INT4 (*tParseIeeeMsgElemEntry)(UINT1 *, UINT2, 
                                       tCapwapMsgElement *, UINT2 *);

INT4 CapwapParseControlPacket PROTO ((tCRU_BUF_CHAIN_HEADER *, 
                                      tCapwapControlPacket  *));

INT4
CapwapParseCtrlMsgBlocks (UINT1 *,
                          tCapwapControlPacket *,
                          UINT2,
                          UINT4);

UINT1
CapwapIsMsgElemMandOrOptional(UINT4, 
                              UINT2, 
                              UINT2 *);

UINT1 
CapwapIsMultiElemOfSameType (UINT2);

INT4
CapwapParseMsgElement (UINT1 *,
                       UINT2,
                       tCapwapMsgElement *,
                       UINT2 *,
                       UINT2,
                       UINT1);

INT4
CapwapValidateMsgElemLen (UINT2,
                          UINT2);

INT4
CapwapValidateMsgElemVal (UINT2,
                          UINT2,
                          UINT1*);

INT4
DumpParsedData (tCapwapControlPacket *pCapwapCtrlMsg);

INT4
CapwapParseVendorSpecificPayload(UINT1 *, UINT2, tCapwapMsgElement *, UINT2 *);



INT4 CapwapParseStructureMemrelease(tCapwapControlPacket * );
INT4 PrintfParsedData (tCapwapControlPacket *);

VOID AssembleCapwapHeader PROTO ((UINT1 *, tCapwapHdr *)); 

VOID AssembleCapwapControlHeader PROTO((UINT1  *, UINT4,
                            UINT1, UINT2,UINT1));

INT4 CapwapAssembleJoinRequest PROTO ((UINT2,tJoinReq *, UINT1 *));
INT4 CapwapAssembleJoinResponse PROTO ((UINT2, tJoinRsp *, UINT1 *));
INT4 CapwapAssembleDiscoveryReq PROTO ((tDiscReq *, UINT1 *));
INT4 CapwapAssembleDiscoveryRsp PROTO ((UINT2, tDiscRsp *, UINT1 *));
INT4 CapwapAssemblePrimaryDiscoveryReq PROTO ((UINT2,tPriDiscReq *, UINT1 *));
INT4 CapwapAssemblePrimaryDiscoveryRsp PROTO ((UINT2, tPriDiscRsp *, UINT1 *));
INT4 CapwapAssembleConfigStatusRequest PROTO ((UINT2, 
                                               tConfigStatusReq *, 
                                               UINT1 *));
INT4 CapwapAssembleConfigStationResp PROTO ((UINT2, 
                                             tStationConfRsp *, 
                                             UINT1 *));
INT4 CapwapAssembleConfigStatusResponse(UINT2 ,tConfigStatusRsp *,UINT1 *);
INT4 CapwapAssembleChangeStateEventRequest(UINT2 ,
                                           tChangeStateEvtReq *,
                                           UINT1 *);

INT4 CapwapAssembleChangeStateEventResponse PROTO ((UINT2,
                                                    tChangeStateEvtRsp *,
                                                    UINT1 *));
INT4 CapwapAssembleEchoRequest PROTO ((tEchoReq *,UINT1 *));
INT4 CapwapAssembleEchoResponse PROTO ((tEchoRsp *,UINT1 *));
INT4 CapwapAssembleKeepAlivePkt PROTO ((tKeepAlivePkt *,
                                        tCRU_BUF_CHAIN_HEADER *));
INT4 CapwapAssembleUnknownRsp (tUnknownRsp *,tCRU_BUF_CHAIN_HEADER *);
VOID capwapAssembleDiscoveryType(UINT1 *, tWtpDiscType *);
VOID capwapAssembleMTUDiscoveryPadding(UINT1 *, tMtuDiscPad *);
VOID capwapAssembleAcDescriptor(UINT1 *, tAcDescriptor *);
VOID CapwapAssembleAcName(UINT1 *,tACName * );
VOID CapwapAssembleLocationData(UINT1 *, tLocationData *);
VOID CapwapAssembleWtpBoardData(UINT1 *, tWtpBoardData *);
VOID capwapAssembleWtpDescriptor(UINT1 *, tWtpDescriptor  *);
VOID capwapAssembleSessionId(UINT1 *, UINT4 *);
VOID capwapAssembleWtpFrameTunnelMode(UINT1 *, tWtpFrameTunnel *);
VOID capwapAssembleWtpName(UINT1 *, tWtpName  *);
VOID capwapAssembleWtpMacType(UINT1 *,tWtpMacType    *);
VOID capwapAssembleIeeeWtpRadioInformation(UINT1 *,tRadioIfInfo *);
VOID capwapAssembleEcnSupport(UINT1 *, tEcnSupport *);
VOID capwapAssembleLocalIPAddress(UINT1 *, tLocalIpAddr *);
VOID capwapAssembleControlIPAddress(UINT1 *, tCtrlIpAddr *);
VOID capwapAssembleTrasnportProtocol(UINT1 *, tCapwapTransprotocol *);
VOID capwapAssembleMaxMessageLength(UINT1 *, tMaxMsgLen *);
VOID capwapAssembleWtpRebootStatistics(UINT1 *,tWtpRebootStatsElement *);
VOID CapwapAssembleVendorSpecificPayload(UINT1 *,tVendorSpecPayload *);
VOID capwapAssembleResultCode(UINT1 *,tResCode *);
VOID capwapAssembleIpv4List(UINT1 *,tACIplist *);
VOID capwapAssembleIpv6List(UINT1 *,tACIplist *);
VOID capwapAssembleImageIdentifier(UINT1 *,tImageId *);

VOID CapwapAssembleImageInformation(UINT1 *pTxPkt, tImageInfo *pImageInfo);
VOID capwapAssembleImageData(UINT1 *pTxPkt, tImageData *pImageData);
VOID capwapAssembleInitiateDownload(UINT1 *pTxPkt,
                                tImageIntiateDownload *imageInitiateDownload);
VOID CapwapAssembleIeeeAssignedBssId (UINT1 *pTxPkt,
                                      tWssWlanRsp *pWssWlanRsp);
VOID capwapAssembleDot11MicCountermeasures(UINT1 *pTxPkt, 
                                     tDot11MicCountermeasures *Dot11MicCount);
VOID capwapAssembleDot11RSNAErrReport(UINT1 *pTxPkt, 
                                      tDot11RSNAErrReport *RSNAErrReport);
VOID capwapAssembleDot11Statistics(UINT1 *pTxPkt, 
                                   tDot11Statistics  *Dot11Statistics);
INT4 CapwapAssembleWTPImageDataReq (UINT2 mesgLen, 
                                    tImageDataReq *pImageDataReq, 
                                    UINT1 *pMesgOut);
INT4 CapwapAssembleWLCImageDataRsp (UINT2 mesgLen, 
                                    tImageDataRsp *pImageDataRsp, 
                                    UINT1 *pu1Buf);
INT4 CapGetResultCode (UINT1 *pRcvBuf, 
                       tCapwapControlPacket *pCpParsePkt, 
                       UINT4 u4Index, 
                       UINT4 *pRetCode);


VOID capwapAssembleRadioAdminState(UINT1 *,tRadioIfAdminStatus *);
VOID capwapAssembleDot11nParams (UINT1 *, tRadioIfDot11nParam *);
VOID CapwapAssembleStatisticsTimer(UINT1 *,tStatisticsTimer *);
VOID capwapAssembleAcnameWithPriority(UINT1 *,tACNameWithPrio *);
VOID capwapAssembleWtpStaticIpAddress(UINT1 *,tWtpStaticIpAddr *);
VOID capwapAssembleCapwapTimer(UINT1 *,tCapwapTimer *);
VOID capwapAssembleDecryptionErrorReport(UINT1 *,tDecryptErrReportPeriod *);
VOID capwapAssembleIdleTimeout(UINT1 *,tIdleTimeout *);
VOID capwapAssembleWtpFallback(UINT1 *,tWtpFallback*);
VOID capwapAssembleRadioOperState(UINT1 *, tRadioIfOperStatus *);

VOID capwapAssembleDeleteMacEntry (UINT1 *,tDeleteMacAclEntry *);
VOID capwapAssembleAddMacEntry (UINT1 *,tAddMacAclEntry *);
VOID capwapAssembleAcTimestamp (UINT1 *,tACtimestamp *);
VOID capwapAssembleAcNamePriority (UINT1 *,tACNameWithPrio *);

VOID CapwapAssembleRadarEventInfo(UINT1 *, tDFSRadarEventVendorInfo*);
VOID capwapAssembleAddstation(UINT1 *,tWssStaAddStation *);
VOID capwapAssembleDeletestation(UINT1 *,tWssStaDeleteStation *);
VOID capwapAssembleDecryptionErrReport(UINT1 *, tDecryptErrReport *);
VOID capwapAssembleDuplicateIpv4Address(UINT1 *, tDupIPV4Addr *);
VOID capwapAssembleDuplicateIpv6Address(UINT1 *, tDupIPV6Addr *);
VOID capwapAssembleDataTransferMode(UINT1 *, tDataTransferMode *);
VOID capwapAssembleDataTransferData(UINT1 *, tDataTransferData *);
VOID capwapAssembleWtpRadioStatistics(UINT1 *, tWtpRadioStatsElement *);
VOID capwapAssembleRadioOperationalState(UINT1 *, tRadioIfOperStatus *);
INT4 CapwapAssembleConfigStationReq PROTO ((UINT2, 
                                            tStationConfReq *, 
                                            UINT1 *));
INT4 CapwapAssembleConfigStationRsp PROTO ((UINT2, 
                                            tStationConfRsp *, 
                                            UINT1 *));
INT4 CapwapAssembleClearConfigReq PROTO ((UINT2, tClearconfigReq *, UINT1 *));
INT4 CapwapAssembleClearConfigResp PROTO ((UINT2, 
                                           tClearconfigRsp *, 
                                           UINT1 *));
INT4 CapwapAssembleImageDataReq PROTO ((UINT2, tImageDataReq *, UINT1 * ));
INT4 CapwapAssembleImageDataRsp PROTO ((UINT2, tImageDataRsp *, UINT1 *));
INT4 CapwapAssembleResetRequest PROTO ((UINT2, tResetReq *, UINT1 *));
INT4 CapwapAssembleResetResponse PROTO ((UINT2, tResetRsp *, UINT1 *));
INT4 CapwapAssembleWtpEventRequest PROTO ((UINT2, tWtpEveReq *, UINT1 *));
INT4 CapwapAssembleWtpEventResponse PROTO ((UINT2, tWtpEveRsp *, UINT1 *));
INT4 CapwapAssembleDataTransferReq PROTO ((UINT2, tDataReq *, UINT1 *));
INT4 CapwapAssembleDataTransferResp PROTO ((UINT2, tDataRsp *, UINT1 *));
INT4 CapwapAssembleConfigUpdateReq PROTO ((UINT2, 
                                           tConfigUpdateReq *, 
                                           UINT1 *));
INT4 CapwapAssembleConfigUpdateResp PROTO ((UINT2, 
                                            tConfigUpdateRsp *, 
                                            UINT1 *));



VOID CapwapAssembleIeeeAddWlan(UINT1 *,tWssWlanAddReq *, UINT2 *);
VOID CapwapAssembleIeeeAntena (UINT1 *,tRadioIfAntenna *);
VOID CapwapAssembleIeeeDeleteWlan (UINT1 *,tWssWlanDeleteReq *,UINT2 *);
VOID CapwapAssembleIeeeDirectSequeceControl(UINT1 *,tRadioIfDSSSPhy *);
VOID CapwapAssembleIeeeInfoElem(UINT1 *,tRadioIfInfoElement *);
VOID CapwapAssembleIeeeMacOperation (UINT1 *, tRadioIfMacOperation *);
VOID CapwapAssembleIeeeMultiDomainCapability (UINT1 *,
                                              tRadioIfMultiDomainCap *);
VOID CapwapAssembleIeeeOFDMControl (UINT1 *,tRadioIfOFDMPhy *);
VOID CapwapAssembleIeeeRateSet (UINT1 *,tRadioIfRateSet *);
VOID CapwapAssembleIeeeStation (UINT1 *,tWssStaDot11Station *);

VOID CapwapAssembleIeeeStationQosProfile (UINT1 *,tWssStaDot11QoSProfile *);
VOID CapwapAssembleIeeeStationSessionKey (UINT1 *,tWssStaDot11Sesskey *);
VOID CapwapAssembleStaDot11nParams (UINT1 *,tWssStaDot11nConfigInfo *);
VOID CapwapAssembleIeeeSupportedRates (UINT1 *,tRadioIfSupportedRate *);
VOID CapwapAssembleIeeeTxPower (UINT1 *,tRadioIfTxPower *);
VOID CapwapAssembleIeeeTxPowerLevel (UINT1 *,tRadioIfTxPowerLevel *);
VOID CapwapAssembleIeeeUpdateStatonQos(UINT1 *,tWssStaDot11UpdateStaQoS *);
VOID CapwapAssembleIeeeUpdateWlan(UINT1 *,tWssWlanUpdateReq *, UINT2 *);
VOID CapwapAssembleIeeeWtpQos (UINT1 *,tRadioIfQos *);
VOID CapwapAssembleIeeeWtpRadioConfiguration (UINT1 *,tRadioIfConfig *);
VOID CapwapAssembleIeeeWTPRadioFailAlarm (UINT1 *,tRadioIfFailAlarm *);
INT4 CapwapAssembIeeeWLANConfigurationReq (UINT2 ,
                                           tWssWlanConfigReq *,
                                           UINT1 *);
INT4 CapwapAssembleIeeeWLANConfigurationRsp (UINT2 ,
                                             tWssWlanConfigRsp *,
                                             UINT1 *);

/***************************capdmain.c************************/
VOID CapwapEnqueDiscPkts (tCapwapRxQMsg *);
VOID CapwapSendRestartDiscEvent(VOID);
INT4 CapwapValidateMessageElements(UINT1 *,
                              tCapwapControlPacket *,
                              UINT2);

INT4 CapwapValidateMacType (UINT1 *,tCapwapControlPacket *, UINT2);
INT4 CapwapValidateFrameTunnelMode (UINT1 *,tCapwapControlPacket *,UINT2);
INT4 CapwapValidateRadioInfo (UINT1 *,tCapwapControlPacket *,UINT2);
INT4 CapwapValidateDiscType (UINT1 *,tCapwapControlPacket * , UINT2);
INT4 CapwapValidateWtpBoardData (UINT1 *,tCapwapControlPacket *, UINT2);
INT4 CapwapValidateIpAddress(UINT4 );
VOID tmrDiscCallback (tTimerListId timerListId);
INT4 CapwapValidateAcNamePriority (UINT1 *pRcvBuf,
                              tCapwapControlPacket *pCwMsg,
                              UINT2 u2MsgIndex);

INT4 CapwapValidateImageId (UINT1 *pRcvBuf,
                       tCapwapControlPacket *cwMsg,
                       UINT2 u2MsgIndex);
INT4 CapwapSetAcName(UINT1 *pRcvBuf,
                tCapwapControlPacket *pCwMsg,
                UINT2 u2MsgIndex);

INT4 CapwapSetRadioInfo (UINT1 *pRcvBuf,
                tCapwapControlPacket *pCwMsg,
                UINT2 u2MsgIndex);

INT4 CapwapValidateAcTimeStamp (UINT1 *pRcvBuf,
                          tCapwapControlPacket *cwMsg,
                          UINT2 u2MsgIndex);

INT4 CapwapValidateAddMacEntry (UINT1 *pRcvBuf,
                          tCapwapControlPacket *cwMsg,
                          UINT2 u2MsgIndex);
INT4 CapwapValidateDeleteMacEntry (UINT1 *pRcvBuf,
                              tCapwapControlPacket *cwMsg,
                              UINT2 u2MsgIndex);

INT4 CapwapValidateWtpStaticIpAddress(UINT1 *pRcvBuf,
                                tCapwapControlPacket *cwMsg,
                                UINT2 u2MsgIndex);

INT4 CapwapValidateDiscReqMsgElems(UINT1 *,tCapwapControlPacket *);
INT4 CapwapValidateRadioOperState(UINT1 *, tCapwapControlPacket *, 
                                  tRadioIfOperStatus *, UINT2);
INT4 CapwapValidateResultCode(UINT1 *, tCapwapControlPacket *,
                              tResCode *,UINT2);
INT4 MakeDiscResponseEntry(tSegment *, tDiscRsp *);
INT4 CapwapClearDiscResponseEntry (VOID);
INT4 CapwapProcessDiscoveryResponse (tSegment *,
                                     tCapwapControlPacket *, 
                                     tRemoteSessionManager *);
INT4 CapwapProcessDiscoveryRequest (tSegment *,
                                    tCapwapControlPacket *, 
                                    tRemoteSessionManager *);
INT4 CapwapProcessChangeStateEvntRequest(tSegment *,
                                         tCapwapControlPacket *, 
                                         tRemoteSessionManager  *);
INT4 CapwapDiscProcessPkt (tSegment *);
UINT4 CapwapDiscValidatePkt (UINT1 *,UINT4 ,UINT4 );
INT4 CapDiscTmrInit(VOID);
VOID CapDiscInitTmrDesc (VOID);
VOID CapDiscTmrExpHandler (VOID);
INT4 CapwapValidateHdr (UINT1 *,UINT4,UINT4);
INT4 CapwapGetAcInfoCount (UINT1 *);
INT4 CapwapGetBestACDetails (tACInfoAfterDiscovery *);
INT4 CapwapValidateLocationData (UINT1 *,
                            tCapwapControlPacket *,
                            UINT2);

INT4
CapwapValidateMtuDiscPad(UINT1 *,
                         tCapwapControlPacket *,
                         UINT2);

INT4
CapwapValidateVendSpecPld(UINT1 *,
                       tCapwapControlPacket *, tVendorSpecPayload *,
                       UINT2);
INT4
CapwapValidateWtpName(UINT1 *,
                      tCapwapControlPacket *,
                      UINT2);

INT4
CapwapValidateWtpSessionId(UINT1 *,
                           tCapwapControlPacket *,
                           tRemoteSessionManager  *,
                           UINT2);
INT4
CapwapValidateEcnSupport(UINT1 *,
                           tCapwapControlPacket *,
                           UINT2);
INT4
CapwapValidateRebootStats(UINT1 *,
                     tCapwapControlPacket *,
                     UINT2);

INT4
CapwapValidateDot11Statistics(UINT1 *, tCapwapControlPacket *,
                     UINT2);
INT4
CapwapValidateStatsTimer(UINT1 *,
                     tCapwapControlPacket *,
                     UINT2);
INT4
CapwapValidateRadioAdminState(UINT1 *,
                     tCapwapControlPacket *,
                     tRadioIfAdminStatus *pRadioAdmin,
                     UINT2);
INT4
CapwapValidateAcName(UINT1 *,
                     tCapwapControlPacket *,
                     UINT2 );
INT4
CapwapValidateCapwapTimer(UINT1 *,
                          tCapwapControlPacket *,
                          UINT2);
INT4
CapwapValidateDecryptErrReportPeriod(UINT1 *,
                             tCapwapControlPacket *,
                             UINT2);
INT4
CapwapValidateIdleTimeout(UINT1 *,
                          tCapwapControlPacket *,
                          UINT2);

INT4
CapwapValidateWtpFallback(UINT1 *,
                          tCapwapControlPacket *,
                          UINT2);


INT4
CapwapValidateIpv4List(UINT1 *,
                       tCapwapControlPacket *,
                       UINT2);
INT4
CapwapValidateIpv6List(UINT1 *,
                       tCapwapControlPacket *,
                       UINT2);

INT4
CapwapValidateDataTransferMode(UINT1*,
                               tCapwapControlPacket *,
                               UINT2);

INT4
capwapValidateDataTransferData(UINT1 *,
                               tCapwapControlPacket *,
                               UINT2);

INT4
CapwapValidateDupIPV4Addr (UINT1 *,
                           tCapwapControlPacket *,
                           UINT2);

INT4
CapwapValidateDupIPV6Addr (UINT1 *,
                           tCapwapControlPacket *,
                           UINT2);

INT4
CapwapValidatePriAcNameWithPrio(UINT1 *,
                     tCapwapControlPacket *,
                     UINT2);

INT4
CapwapValidateDecryptErrReport (UINT1 *,
                                tCapwapControlPacket *,
                                UINT2);

INT4 
CapwapValidateRadioStats (UINT1 *, 
                          tCapwapControlPacket *, 
                          UINT2);

INT4
CapwapGetAcNameWithPriority (tACNameWithPrio *, UINT4 *);

INT4
WssApGetUdpPort (UINT4 *);

/**************************capwapjoin.c*******************************/
INT4 CapwapProcessJoinRequest(tSegment *,
                              tCapwapControlPacket *, 
                              tRemoteSessionManager *);
INT4 CapwapProcessJoinResponse(tSegment *,
                               tCapwapControlPacket *, 
                               tRemoteSessionManager *);
INT4 CapwapValidateJoinResponseMsgElems(UINT1 *,
                                        tCapwapControlPacket *,
                                        tRemoteSessionManager *);
INT4 CapwapGetConfigStatusRequestMsgElements (tConfigStatusReq *, 
                                              tRemoteSessionManager *);
INT4 CapwapValidateJoinRequestMsgElems(UINT1 *,
                                       tCapwapControlPacket *, 
                                       tRemoteSessionManager *);
INT4 CapwapValidateConfigStatusRequestMsgElems(UINT1 *,
                                               tCapwapControlPacket *,
                                               tConfigStatusRsp *);
INT4 CapwapProcessConfigStatusResponse (tSegment *,
                                        tCapwapControlPacket *, 
                                        tRemoteSessionManager *);
INT4 CapwapConstructJoinReq (tJoinReq *);
INT4 CapwapProcessConfigStatusRequest (tSegment *,
                                  tCapwapControlPacket *, 
                                  tRemoteSessionManager *);

INT4
CapProcessConfigUpdateReq (tCapwapConfigUpdateReq  *pCapUpdteReq, 
                           tRemoteSessionManager *);

INT4
CapwapValidateConfigStatusResponseMsgElems(UINT1 *,
                                           tCapwapControlPacket *,
                                           tRemoteSessionManager *);
INT4
CapwapValidateChangeStateEventReqMsg (UINT1 *,
                               tCapwapControlPacket *, 
                               tRemoteSessionManager  *);
INT4
CapwapGetStateEventResponseMsgElements(tChangeStateEvtRsp *);
INT4
CapwapValidateStateEventRespMsg(UINT1 *,
                               tCapwapControlPacket *);
INT4
CapwapProcessChangeStateEvntResponse(tSegment *,
                                     tCapwapControlPacket *,
                                     tRemoteSessionManager *);

INT4
CapwapGetJoinRspMsgElements (tJoinRsp  *, UINT4 *, 
                             tRemoteSessionManager *, UINT2);
/*************************cpwaprun.c***************************************/
INT4 CapwapProcessEchoRequest (tSegment *,
                               tCapwapControlPacket *, 
                               tRemoteSessionManager *);
INT4 CapwapProcessEchoResponse (tSegment *,
                                tCapwapControlPacket *,
                                tRemoteSessionManager *);
INT4 CapwapValidateEchoRequestMsgElems(UINT1 *,tCapwapControlPacket *);
INT4 CapwapValidateEchoResponseMsgElems(UINT1 *pRcvBuf,
                                        tCapwapControlPacket *,
                                        UINT2 *);
INT4 CapwapProcessDataTxMsg (VOID);
INT4 CapwapTransmitKeepAlivePkt (tRemoteSessionManager  *);
INT4 CapwapInitiateDataChannel (tRemoteSessionManager  *);
INT4 CapwapTransmitEchoRequest (tRemoteSessionManager  *);
/*************************cpwapfrag.c***************************************/
tCapFragNode * CapwapCreateFrag (UINT2 , UINT2 , tCRU_BUF_CHAIN_HEADER *,UINT2);
VOID CapwapDeleteFrag (tCapFragNode *);
INT4 CapwapFragReassemble (tCRU_BUF_CHAIN_HEADER *, 
                           tRemoteSessionManager *,UINT2,UINT1 );
INT4 CapwapDataFragReassemble (tCRU_BUF_CHAIN_HEADER *, 
                               tRemoteSessionManager *,
                               UINT2,
                               tCRU_BUF_CHAIN_HEADER **);
INT4 CapwapFreeReAssemble  (tCapFragment*);
VOID CapwapPutHdr (tCRU_BUF_CHAIN_HEADER *, tCapwapHdr *);
VOID CapwapPutCtrlHdr (tCRU_BUF_CHAIN_HEADER *, tCapwapCtrlHdr *);
INT4 CapwapExtractHdr (tCRU_BUF_CHAIN_HEADER *, tCapwapHdr *);
INT4 CapwapExtractCtrlHdr (tCRU_BUF_CHAIN_HEADER *, tCapwapCtrlHdr *);
INT4 CapwapFragmentAndSend ( tCRU_BUF_CHAIN_HEADER *, UINT4 , 
                             tRemoteSessionManager  *,INT2 ,UINT1);
INT4
CapwapDataFreeReAssemble (tCapFragment * , UINT2 );

/* #ifdef DTLS_WANTED */
VOID CapwapDtlsSeesionEstdNotify (UINT4 , UINT4 , UINT4);
VOID CapwapDtlsPeerDisconnectNotify (UINT4,  UINT4 , UINT4);


INT4 CapwapDtlsDecryptNotify (tQueData *pDtlsData);

INT4 CapwapEnableDTLSInit (VOID);
INT4 CapwapStartDTLS ( UINT4 );
INT4 CapwapEnableDTLS (VOID);
INT4 CapwapDisableDTLS (VOID);
INT4 CapwapShutDownDTLS (tRemoteSessionManager *);
INT4 CapwapValidateWtpDeleteStation (UINT1 *pRcvBuf,
                          tCapwapControlPacket *cwMsg,
                          UINT2 u2MsgIndex);
/* #endif  */

VOID CapwapDtlsDataNotify (tQueData *);

VOID
CapwapSetDot11nHTOperBooleanEnable (tRadioIfGetDB * pRadioIfGetDB);

VOID
CapwapSetDot11nHTCapBooleanEnable (tRadioIfGetDB * pRadioIfGetDB);
#ifdef ROGUEAP_WANTED
VOID CapwapAssembleVendorRogueMgmt(UINT1 *pTxPkt, tRogueMgmt * pvendPld);
#endif 

/**********************capwapwtputil.c***************************************/

#ifdef WTP_WANTED

INT4
CapwapValidateConfigUpdateVendSpecPld (UINT1 *pRcvBuf,
                                       tCapwapControlPacket *cwMsg,
                                       tVendorSpecPayload *pVendor,
                                       UINT2 u2MsgIndex);

#endif
/**********************capwapwtpimage.c and capwapwlcimage.c **************/

#ifdef WTP_WANTED

INT4
CapwapProcessImageDataRsp(tSegment *,
                          tCapwapControlPacket *,
                          tRemoteSessionManager  *);

INT4
CapwapProcessImageDataReq (tSegment *,
                           tCapwapControlPacket *,
                           tRemoteSessionManager  *);
INT4
CapwapGetWTPImageDataRspMsgElements(tImageDataRsp *, UINT4 *);

INT4
CapwapGetImageDataReqElements (tImageDataReq *, UINT4 *);

INT4
CapwapGetWTPImageId(tImageId  *, UINT4 *);

INT4
CapwapOpenImage(tRemoteSessionManager * pSess);

INT4
CapwapGetInitiateDownload (tImageIntiateDownload *, UINT4 *);

INT4
CapwapWriteToImage(UINT1 *,
                   tRemoteSessionManager  *,
                   UINT2  ); 

INT4
CapwapOpenAndWriteToImage(UINT1 *,
                          tRemoteSessionManager  *,
                          UINT2  ); 
 

INT4
CapwapValidateWTPImageDataReqMsgElems (UINT1 *,
                                       tCapwapControlPacket *,
                                       tRemoteSessionManager *);

INT4
CapwapValidateWTPImageDataRespMsgElems (UINT1 *,
                                        tCapwapControlPacket *,
                                        tImageDataRsp        *);

#else

INT4
CapwapProcessImageDataReq (tSegment *,
                           tCapwapControlPacket *,
                           tRemoteSessionManager  *); 
INT4
CapwapProcessImageDataRsp (tSegment *,
                          tCapwapControlPacket *,
                          tRemoteSessionManager *);
INT4
CapwapGetWLCImageDataRspMsgElements(tImageDataRsp *, 
                                 UINT4 *,
                                 tRemoteSessionManager *);
INT4
CapwapValidateImageDataReqMsgElems (UINT1 *,
                                    tCapwapControlPacket *,
                                    tImageDataReq        *);
INT4
CapwapGetImageInfo(tImageInfo *, UINT4 *, tRemoteSessionManager *);

INT4
CapwapGetImageSize (tRemoteSessionManager *pSess);


INT4
CapwapGetImageData(tImageData  *, 
                   UINT4 *, 
                   tRemoteSessionManager  *);
INT4
CapwapGetWLCImageDataRequestMsgElements (tImageDataReq *,
                                         UINT4 *,
                                         tRemoteSessionManager  *);
INT4
CapwapReadFromImage(UINT1 *,
                    tRemoteSessionManager  *);

INT4 CapwapGetWLCResultCode(tResCode *pResCode,UINT4 *pMsgLen);
INT4
CapwapOpenAndReadFromImage(UINT1 *,
                           tRemoteSessionManager  *);
INT4
CapwapValidateImageDataRespMsgElems (UINT1 *,
                                     tCapwapControlPacket *,
                                     tRemoteSessionManager *);

INT4
CapwapNotifyRunStateToModules (UINT2);

VOID
CapwapRegisterVlanInfo PROTO ((tVlanBasicInfo *pVlanBasicInfo));

#endif
VOID
CapwapRegisterArpInfo PROTO ((tArpBasicInfo *pArpBasicInfo));

INT4 CapwapGetWLCImageId(tImageId *pImageId, UINT4 *pMsgLen);

const char *CapwapGetFilenameExt(const char *filename);

INT4
CapwapCleanUpSessionDetails (tRemoteSessionManager *);

INT4
CapwapAssembleGenRadioParam (UINT2, UINT1, UINT1 **, UINT2 *, UINT2);

INT4
CapwapConstructGenReq (UINT4, UINT1 **);

INT4
CapwapFetchGenElem(UINT2, UINT1, UINT1 *, UINT2 *, UINT4, UINT1, UINT1);

VOID
CapwapSetMsgElemBoolRadioValue(UINT2,UINT1,tRadioIfGetDB *);

INT1
CapwapFetchMsgElemRadioValue(UINT2, UINT1, tRadioIfGetDB *, UINT1 *, 
                             UINT2 *, UINT1, UINT1);

INT4
CapwapConfigNoOfRadio(UINT2, UINT2 *);

INT4
CapwapDispGenRadioParam (VOID);
#ifdef LNXIP4_WANTED
INT4 
CapwapJoinMulticastGroup (INT4);
#endif
#ifdef KERNEL_CAPWAP_WANTED
INT4
CapwapUpdateWtpKernelDB (VOID);

INT4
CapwapRemoveWtpKernelDB (VOID);

INT4
CapwapAddDeviceInKernel (UINT1 *pu1DeviceName);

INT4
CapwapRemoveDeviceInKernel (UINT1 *pu1DeviceName);
#endif
#endif

