/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: capwapcliglob.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 * Description: This file contains declaration of global variables 
 *              of capwap module.
 *******************************************************************/

#ifndef __CAPWAPGLOBAL_H__
#define __CAPWAPGLOBAL_H__

tCapwapGlobals gCapwapGlobals;

#endif  /* __CAPWAPGLOBAL_H__ */


/*-----------------------------------------------------------------------*/
/*                       End of the file capwapglob.h                      */
/*-----------------------------------------------------------------------*/
