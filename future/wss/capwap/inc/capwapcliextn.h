/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: capwapcliextn.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
 * Description: This file contains extern declaration of global 
 *              variables of the capwap module.
 *******************************************************************/

#ifndef __CAPWAPEXTN_H__
#define __CAPWAPEXTN_H__

PUBLIC tCapwapGlobals gCapwapGlobals;

#endif/*__CAPWAPEXTN_H__*/
   
/*-----------------------------------------------------------------------*/
/*                       End of the file capwapextn.h                      */
/*-----------------------------------------------------------------------*/

