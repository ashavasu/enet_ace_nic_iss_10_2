/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
 Aricent Inc . All Rights Reserved
*
* $Id: capwapcliprotg.h,v 1.3 2017/11/24 10:37:01 siva Exp $ 
*
* This file contains prototypes for functions defined in Capwap.
*********************************************************************/
#ifndef __CAPWAP_CLI_PROTG_
#define __CAPWAP_CLI_PROTG_

#include "cli.h"
#include "wssifcapdb.h"


INT4 CapwapCheckForAlphaNumeric(UINT1 *);

INT4 CapwapCliSetCapwapBaseWtpSessionsLimit (tCliHandle CliHandle,UINT4 *pCapwapBaseWtpSessionsLimit);

INT4 CapwapShowGlobals (tCliHandle CliHandle );

INT4 CapwapCliSetCapwapBaseStationSessionsLimit (tCliHandle CliHandle,UINT4 *pCapwapBaseStationSessionsLimit);

INT4 nmhTestv2MacAddress (UINT4 *pu4ErrorCode, tMacAddr TestValIfMainExtMacAddress);
INT4 CapwapCliSetCapwapBaseAcNameListTable (tCliHandle CliHandle,tCapwapCapwapBaseAcNameListEntry * pCapwapSetCapwapBaseAcNameListEntry,tCapwapIsSetCapwapBaseAcNameListEntry * pCapwapIsSetCapwapBaseAcNameListEntry);

INT4 CapwapCliSetCapwapBaseMacAclTable (tCliHandle CliHandle,tCapwapCapwapBaseMacAclEntry * pCapwapSetCapwapBaseMacAclEntry,tCapwapIsSetCapwapBaseMacAclEntry * pCapwapIsSetCapwapBaseMacAclEntry);

INT4 CapwapCliSetCapwapBaseWtpProfileTable (tCliHandle CliHandle,tCapwapCapwapBaseWtpProfileEntry * pCapwapSetCapwapBaseWtpProfileEntry,tCapwapIsSetCapwapBaseWtpProfileEntry * pCapwapIsSetCapwapBaseWtpProfileEntry);

INT4 CapwapCliSetCapwapBaseAcMaxRetransmit (tCliHandle CliHandle,UINT4 *pCapwapBaseAcMaxRetransmit);


INT4 CapwapCliSetCapwapBaseAcChangeStatePendingTimer (tCliHandle CliHandle,UINT4 *pCapwapBaseAcChangeStatePendingTimer);


INT4 CapwapCliSetCapwapBaseAcDataCheckTimer (tCliHandle CliHandle,UINT4 *pCapwapBaseAcDataCheckTimer);


INT4 CapwapCliSetCapwapBaseAcDTLSSessionDeleteTimer (tCliHandle CliHandle,UINT4 *pCapwapBaseAcDTLSSessionDeleteTimer);


INT4 CapwapCliSetCapwapBaseAcEchoInterval (tCliHandle CliHandle,UINT4 *pCapwapBaseAcEchoInterval);


INT4 CapwapCliSetCapwapBaseAcRetransmitInterval (tCliHandle CliHandle,UINT4 *pCapwapBaseAcRetransmitInterval);


INT4 CapwapCliSetCapwapBaseAcSilentInterval (tCliHandle CliHandle,UINT4 *pCapwapBaseAcSilentInterval);


INT4 CapwapCliSetCapwapBaseAcWaitDTLSTimer (tCliHandle CliHandle,UINT4 *pCapwapBaseAcWaitDTLSTimer);


INT4 CapwapCliSetCapwapBaseAcWaitJoinTimer (tCliHandle CliHandle,UINT4 *pCapwapBaseAcWaitJoinTimer);


INT4 CapwapCliSetCapwapBaseAcEcnSupport (tCliHandle CliHandle,UINT4 *pCapwapBaseAcEcnSupport);


INT4 CapwapCliSetCapwapBaseChannelUpDownNotifyEnable (tCliHandle CliHandle,UINT4 *pCapwapBaseChannelUpDownNotifyEnable);


INT4 CapwapCliSetCapwapBaseDecryptErrorNotifyEnable (tCliHandle CliHandle,UINT4 *pCapwapBaseDecryptErrorNotifyEnable);


INT4 CapwapCliSetCapwapBaseJoinFailureNotifyEnable (tCliHandle CliHandle,UINT4 *pCapwapBaseJoinFailureNotifyEnable);


INT4 CapwapCliSetCapwapBaseImageUpgradeFailureNotifyEnable (tCliHandle CliHandle,UINT4 *pCapwapBaseImageUpgradeFailureNotifyEnable);


INT4 CapwapCliSetCapwapBaseConfigMsgErrorNotifyEnable (tCliHandle CliHandle,UINT4 *pCapwapBaseConfigMsgErrorNotifyEnable);


INT4 CapwapCliSetCapwapBaseRadioOperableStatusNotifyEnable (tCliHandle CliHandle,UINT4 *pCapwapBaseRadioOperableStatusNotifyEnable);


INT4 CapwapCliSetCapwapBaseAuthenFailureNotifyEnable (tCliHandle CliHandle,UINT4 *pCapwapBaseAuthenFailureNotifyEnable);



INT4 CapwapCliSetFsCapwapTrasnportProtocol (tCliHandle CliHandle,UINT4 *pFsCapwapTrasnportProtocol);


INT4 CapwapCliSetFsCapwapDataUdpPort (tCliHandle CliHandle,UINT4
        *pFsCapwapDataUdpPort);

INT4 CapwapCliSetFsCapwapMaxFragmentsPerPacket (tCliHandle CliHandle,UINT4
        *pFsCapwapMaxFragmentsPerPacket);

INT4 CapwapCliSetFsCapwapEnableFsmOptimization (tCliHandle CliHandle,UINT4
        *pFsCapwapEnableFsmOptimization);


INT4 CapwapCliSetFsDiscoveryMode (tCliHandle CliHandle,UINT4 *pFsDiscoveryMode);


INT4 CapwapCliSetFsWtpDiscoveryType (tCliHandle CliHandle,UINT4
        *pFsWtpDiscoveryType);


INT4 CapwapCliSetFsManagmentSSID (tCliHandle CliHandle,UINT4 *pFsManagmentSSID);

INT4 CapwapCliSetFsCapwapEnable (tCliHandle CliHandle,UINT4 *pFsCapwapEnable);


INT4 CapwapCliSetFsCapwapShutdown (tCliHandle CliHandle,UINT4 *pFsCapwapShutdown);


INT4 CapwapCliSetFsCapwapControlUdpPort (tCliHandle CliHandle,UINT4 *pFsCapwapControlUdpPort);


INT4 CapwapCliSetFsCapwapControlChannelDTLSPolicyOptions (tCliHandle CliHandle,UINT4 *pFsCapwapControlChannelDTLSPolicyOptions);


INT4 CapwapCliSetFsCapwapDataChannelDTLSPolicyOptions (tCliHandle CliHandle,UINT4 *pFsCapwapDataChannelDTLSPolicyOptions);


INT4 CapwapCliSetFsWlcDiscoveryMode (tCliHandle CliHandle,UINT1 *pFsWlcDiscoveryMode);


INT4 CapwapCliSetFsCapwapWtpModeIgnore (tCliHandle CliHandle,UINT4 *pFsCapwapWtpModeIgnore);


INT4 CapwapCliSetFsCapwapDebugMask (tCliHandle CliHandle,UINT4 *pFsCapwapDebugMask, UINT4 *, UINT4 *);

INT4 CapwapCliSetFsCapwapNoDebugMask (tCliHandle CliHandle,UINT4 *pFsCapwapNoDebugMask, UINT4 *, UINT4 *);

INT4 CapwapCliSetFsCapwapWssDataDebugMask (tCliHandle CliHandle,UINT4 *pFsCapwapWssDataDebugMask);

INT4 CapwapCliSetFsCapwapNoWssDataDebugMask (tCliHandle CliHandle,UINT4 *pFsCapwapNoWssDataDebugMask);

INT4
CapwapCliSetStationConfigTimer (tCliHandle CliHandle, UINT4 u4Status,UINT4 u4TimerValue);


INT4 CapwapCliSetFsDtlsDebugMask (tCliHandle CliHandle,UINT4 *pFsDtlsDebugMask);


INT4 CapwapCliSetFsDtlsEncryption (tCliHandle CliHandle,UINT4 *pFsDtlsEncryption);


INT4 CapwapCliSetFsDtlsEncryptAlogritham (tCliHandle CliHandle,UINT4 *pFsDtlsEncryptAlogritham);


INT4 CapwapCliSetFsStationType (tCliHandle CliHandle,UINT4 *pFsStationType);
INT4 CapwapCliSetFsAcTimestampTrigger (tCliHandle CliHandle,UINT4 *);

INT4 CapwapShowApSummary (tCliHandle CliHandle, UINT1 *pu1CapwapBaseWtpProfileName);
INT4 CapwapShowApSummaryAll (tCliHandle CliHandle);
INT4 CapwapShowWtpModel (tCliHandle CliHandle, UINT1 *pau1FsCapwapWtpModelNumber);
INT4 CapwapShowApConfigGeneral (tCliHandle CliHandle, UINT1 *pu1CapwapBaseWtpProfileName);
INT4 CapwapShowDtlsConnectons (tCliHandle CliHandle);
INT4 CapwapShowLinkEncryption (tCliHandle CliHandle , UINT1 *pu1CapwapBaseWtpProfileName);

INT4 CapwapCliSetFsWtpModelTable (tCliHandle CliHandle,tCapwapFsWtpModelEntry * pCapwapSetFsWtpModelEntry,tCapwapIsSetFsWtpModelEntry * pCapwapIsSetFsWtpModelEntry);

INT4 CapwapCliSetFsWtpRadioTable (tCliHandle CliHandle,tCapwapFsWtpRadioEntry * pCapwapSetFsWtpRadioEntry,tCapwapIsSetFsWtpRadioEntry * pCapwapIsSetFsWtpRadioEntry);

INT4 CapwapCliSetFsCapwapWhiteListTable (tCliHandle CliHandle,tCapwapFsCapwapWhiteListEntry * pCapwapSetFsCapwapWhiteListEntry,tCapwapIsSetFsCapwapWhiteListEntry * pCapwapIsSetFsCapwapWhiteListEntry);

INT4 CapwapCliSetFsCapwapBlackList (tCliHandle CliHandle,tCapwapFsCapwapBlackListEntry * pCapwapSetFsCapwapBlackListEntry,tCapwapIsSetFsCapwapBlackListEntry * pCapwapIsSetFsCapwapBlackListEntry);

INT4 CapwapCliSetFsCapwapWtpConfigTable (tCliHandle CliHandle,tCapwapFsCapwapWtpConfigEntry * pCapwapSetFsCapwapWtpConfigEntry,tCapwapIsSetFsCapwapWtpConfigEntry * pCapwapIsSetFsCapwapWtpConfigEntry);

INT4 CapwapCliSetFsCapwapLinkEncryptionTable (tCliHandle CliHandle,tCapwapFsCapwapLinkEncryptionEntry * pCapwapSetFsCapwapLinkEncryptionEntry,tCapwapIsSetFsCapwapLinkEncryptionEntry * pCapwapIsSetFsCapwapLinkEncryptionEntry);

INT4 CapwapCliSetFsCawapDefaultWtpProfileTable (tCliHandle CliHandle,tCapwapFsCapwapDefaultWtpProfileEntry * pCapwapSetFsCapwapDefaultWtpProfileEntry,tCapwapIsSetFsCapwapDefaultWtpProfileEntry * pCapwapIsSetFsCapwapDefaultWtpProfileEntry);

INT4 CapwapCliSetFsCapwapDnsProfileTable (tCliHandle CliHandle,tCapwapFsCapwapDnsProfileEntry * pCapwapSetFsCapwapDnsProfileEntry,tCapwapIsSetFsCapwapDnsProfileEntry * pCapwapIsSetFsCapwapDnsProfileEntry);

INT4 CapwapCliSetFsWtpNativeVlanIdTable (tCliHandle CliHandle,tCapwapFsWtpNativeVlanIdTable * pCapwapSetFsWtpNativeVlanIdTable,tCapwapIsSetFsWtpNativeVlanIdTable * pCapwapIsSetFsWtpNativeVlanIdTable);

INT4 CapwapCliSetFsWtpLocalRoutingTable (tCliHandle CliHandle,tCapwapFsWtpLocalRoutingTable * pCapwapSetFsWtpLocalRoutingTable,tCapwapIsSetFsWtpLocalRoutingTable * pCapwapIsSetFsWtpLocalRoutingTable);

INT4 CapwapCliSetFsCawapDiscStatsTable (tCliHandle CliHandle,tCapwapFsCawapDiscStatsEntry * pCapwapSetFsCawapDiscStatsEntry,tCapwapIsSetFsCawapDiscStatsEntry * pCapwapIsSetFsCawapDiscStatsEntry);

INT4 CapwapCliSetFsCawapJoinStatsTable (tCliHandle CliHandle,tCapwapFsCawapJoinStatsEntry * pCapwapSetFsCawapJoinStatsEntry,tCapwapIsSetFsCawapJoinStatsEntry * pCapwapIsSetFsCawapJoinStatsEntry);

INT4 CapwapCliSetFsCawapConfigStatsTable (tCliHandle CliHandle,tCapwapFsCawapConfigStatsEntry * pCapwapSetFsCawapConfigStatsEntry,tCapwapIsSetFsCawapConfigStatsEntry * pCapwapIsSetFsCawapConfigStatsEntry);

INT4 CapwapCliSetFsCawapRunStatsTable (tCliHandle CliHandle,tCapwapFsCawapRunStatsEntry * pCapwapSetFsCawapRunStatsEntry,tCapwapIsSetFsCawapRunStatsEntry * pCapwapIsSetFsCawapRunStatsEntry);

INT4 CapwapCliSetFsCapwapWirelessBindingTable (tCliHandle CliHandle,tCapwapFsCapwapWirelessBindingEntry * pCapwapSetFsCapwapWirelessBindingEntry,tCapwapIsSetFsCapwapWirelessBindingEntry * pCapwapIsSetFsCapwapWirelessBindingEntry);

INT4 CapwapCliSetFsCapwapStationWhiteList (tCliHandle CliHandle,tCapwapFsCapwapStationWhiteListEntry * pCapwapSetFsCapwapStationWhiteListEntry,tCapwapIsSetFsCapwapStationWhiteListEntry * pCapwapIsSetFsCapwapStationWhiteListEntry);

INT4 CapwapCliSetFsCapwapWtpRebootStatisticsTable (tCliHandle CliHandle,tCapwapFsCapwapWtpRebootStatisticsEntry * pCapwapSetFsCapwapWtpRebootStatisticsEntry,tCapwapIsSetFsCapwapWtpRebootStatisticsEntry * pCapwapIsSetFsCapwapWtpRebootStatisticsEntry);

INT4 CapwapCliSetFsCapwapWtpRadioStatisticsTable (tCliHandle CliHandle,tCapwapFsCapwapWtpRadioStatisticsEntry * pCapwapSetFsCapwapWtpRadioStatisticsEntry,tCapwapIsSetFsCapwapWtpRadioStatisticsEntry * pCapwapIsSetFsCapwapWtpRadioStatisticsEntry);


PUBLIC INT4 CapwapUtlCreateRBTree PROTO ((VOID));

PUBLIC INT4 CapwapUtlDeleteRBTree PROTO ((VOID));

PUBLIC INT4 CapwapLock (VOID);

PUBLIC INT4 CapwapUnLock (VOID);

PUBLIC INT4 CapwapBaseAcNameListTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapCapwapBaseAcNameListTableCreate PROTO ((VOID));

tCapwapCapwapBaseAcNameListEntry * CapwapCapwapBaseAcNameListTableCreateApi PROTO ((tCapwapCapwapBaseAcNameListEntry *));

PUBLIC INT4 CapwapBaseMacAclTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapCapwapBaseMacAclTableCreate PROTO ((VOID));

tCapwapCapwapBaseMacAclEntry * CapwapCapwapBaseMacAclTableCreateApi PROTO ((tCapwapCapwapBaseMacAclEntry *));

PUBLIC INT4 CapwapBaseWtpProfileTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapCapwapBaseWtpProfileTableCreate PROTO ((VOID));

tCapwapCapwapBaseWtpProfileEntry * CapwapCapwapBaseWtpProfileTableCreateApi PROTO ((tCapwapCapwapBaseWtpProfileEntry *));

PUBLIC INT4 CapwapBaseWtpStateTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapCapwapBaseWtpStateTableCreate PROTO ((VOID));

tCapwapCapwapBaseWtpStateEntry * CapwapCapwapBaseWtpStateTableCreateApi PROTO ((tCapwapCapwapBaseWtpStateEntry *));

PUBLIC INT4 CapwapBaseWtpTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapCapwapBaseWtpTableCreate PROTO ((VOID));

tCapwapCapwapBaseWtpEntry * CapwapCapwapBaseWtpTableCreateApi PROTO ((tCapwapCapwapBaseWtpEntry *));

PUBLIC INT4 CapwapBaseWirelessBindingTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapCapwapBaseWirelessBindingTableCreate PROTO ((VOID));

tCapwapCapwapBaseWirelessBindingEntry * CapwapCapwapBaseWirelessBindingTableCreateApi PROTO ((tCapwapCapwapBaseWirelessBindingEntry *));

PUBLIC INT4 CapwapBaseStationTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapCapwapBaseStationTableCreate PROTO ((VOID));

tCapwapCapwapBaseStationEntry * CapwapCapwapBaseStationTableCreateApi PROTO ((tCapwapCapwapBaseStationEntry *));

PUBLIC INT4 CapwapBaseWtpEventsStatsTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapCapwapBaseWtpEventsStatsTableCreate PROTO ((VOID));

tCapwapCapwapBaseWtpEventsStatsEntry * CapwapCapwapBaseWtpEventsStatsTableCreateApi PROTO ((tCapwapCapwapBaseWtpEventsStatsEntry *));

PUBLIC INT4 CapwapBaseRadioEventsStatsTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapCapwapBaseRadioEventsStatsTableCreate PROTO ((VOID));

PUBLIC INT4 FsWtpModelTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapFsWtpModelTableCreate PROTO ((VOID));

tCapwapFsWtpModelEntry * CapwapFsWtpModelTableCreateApi PROTO ((tCapwapFsWtpModelEntry *));

PUBLIC INT4 FsWtpRadioTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapFsWtpRadioTableCreate PROTO ((VOID));

tCapwapFsWtpRadioEntry * CapwapFsWtpRadioTableCreateApi PROTO ((tCapwapFsWtpRadioEntry *));

PUBLIC INT4 FsCapwapWhiteListTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapFsCapwapWhiteListTableCreate PROTO ((VOID));

tCapwapFsCapwapWhiteListEntry * CapwapFsCapwapWhiteListTableCreateApi PROTO ((tCapwapFsCapwapWhiteListEntry *));

PUBLIC INT4 FsCapwapBlackListRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapFsCapwapBlackListCreate PROTO ((VOID));

tCapwapFsCapwapBlackListEntry * CapwapFsCapwapBlackListCreateApi PROTO ((tCapwapFsCapwapBlackListEntry *));

PUBLIC INT4 FsCapwapWtpConfigTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapFsCapwapWtpConfigTableCreate PROTO ((VOID));

tCapwapFsCapwapWtpConfigEntry * CapwapFsCapwapWtpConfigTableCreateApi PROTO ((tCapwapFsCapwapWtpConfigEntry *));

PUBLIC INT4 FsCapwapLinkEncryptionTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapFsCapwapLinkEncryptionTableCreate PROTO ((VOID));

tCapwapFsCapwapLinkEncryptionEntry * CapwapFsCapwapLinkEncryptionTableCreateApi PROTO ((tCapwapFsCapwapLinkEncryptionEntry *));

PUBLIC INT4 FsCawapDefaultWtpProfileTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapFsCawapDefaultWtpProfileTableCreate PROTO ((VOID));

tCapwapFsCapwapDefaultWtpProfileEntry * CapwapFsCawapDefaultWtpProfileTableCreateApi PROTO ((tCapwapFsCapwapDefaultWtpProfileEntry *));

PUBLIC INT4 FsCapwapDnsProfileTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapFsCapwapDnsProfileTableCreate PROTO ((VOID));

tCapwapFsCapwapDnsProfileEntry * CapwapFsCapwapDnsProfileTableCreateApi PROTO ((tCapwapFsCapwapDnsProfileEntry *));

PUBLIC INT4 FsWtpNativeVlanIdTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapFsWtpNativeVlanIdTableCreate PROTO ((VOID));

tCapwapFsWtpNativeVlanIdTable * CapwapFsWtpNativeVlanIdTableCreateApi PROTO ((tCapwapFsWtpNativeVlanIdTable *));

PUBLIC INT4 FsWtpLocalRoutingTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapFsWtpLocalRoutingTableCreate PROTO ((VOID));

tCapwapFsWtpLocalRoutingTable * CapwapFsWtpLocalRoutingTableCreateApi PROTO ((tCapwapFsWtpLocalRoutingTable *));

PUBLIC INT4 FsCawapDiscStatsTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapFsCawapDiscStatsTableCreate PROTO ((VOID));

tCapwapFsCawapDiscStatsEntry * CapwapFsCawapDiscStatsTableCreateApi PROTO ((tCapwapFsCawapDiscStatsEntry *));

PUBLIC INT4 FsCawapJoinStatsTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapFsCawapJoinStatsTableCreate PROTO ((VOID));

tCapwapFsCawapJoinStatsEntry * CapwapFsCawapJoinStatsTableCreateApi PROTO ((tCapwapFsCawapJoinStatsEntry *));

PUBLIC INT4 FsCawapConfigStatsTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapFsCawapConfigStatsTableCreate PROTO ((VOID));

tCapwapFsCawapConfigStatsEntry * CapwapFsCawapConfigStatsTableCreateApi PROTO ((tCapwapFsCawapConfigStatsEntry *));

PUBLIC INT4 FsCawapRunStatsTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapFsCawapRunStatsTableCreate PROTO ((VOID));

tCapwapFsCawapRunStatsEntry * CapwapFsCawapRunStatsTableCreateApi PROTO ((tCapwapFsCawapRunStatsEntry *));

PUBLIC INT4 FsCapwapWirelessBindingTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapFsCapwapWirelessBindingTableCreate PROTO ((VOID));

tCapwapFsCapwapWirelessBindingEntry * CapwapFsCapwapWirelessBindingTableCreateApi PROTO ((tCapwapFsCapwapWirelessBindingEntry *));

PUBLIC INT4 FsCapwapStationWhiteListRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapFsCapwapStationWhiteListCreate PROTO ((VOID));

tCapwapFsCapwapStationWhiteListEntry * CapwapFsCapwapStationWhiteListCreateApi PROTO ((tCapwapFsCapwapStationWhiteListEntry *));

PUBLIC INT4 FsCapwapWtpRebootStatisticsTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapFsCapwapWtpRebootStatisticsTableCreate PROTO ((VOID));

tCapwapFsCapwapWtpRebootStatisticsEntry * CapwapFsCapwapWtpRebootStatisticsTableCreateApi PROTO ((tCapwapFsCapwapWtpRebootStatisticsEntry *));

PUBLIC INT4 FsCapwapWtpRadioStatisticsTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 CapwapFsCapwapWtpRadioStatisticsTableCreate PROTO ((VOID));

tCapwapFsCapwapWtpRadioStatisticsEntry * CapwapFsCapwapWtpRadioStatisticsTableCreateApi PROTO ((tCapwapFsCapwapWtpRadioStatisticsEntry *));
INT4 CapwapGetFsCapwapEnable PROTO ((INT4 *));
INT4 CapwapGetFsCapwapShutdown PROTO ((INT4 *));
INT4 CapwapGetFsCapwapControlUdpPort PROTO ((UINT4 *));
INT4 CapwapGetFsCapwapControlChannelDTLSPolicyOptions PROTO ((UINT1 *));
INT4 CapwapGetFsCapwapDataChannelDTLSPolicyOptions PROTO ((UINT1 *));
INT4 CapwapGetFsWlcDiscoveryMode PROTO ((UINT1 *));
INT4 CapwapGetFsCapwapWtpModeIgnore PROTO ((INT4 *));
INT4 CapwapGetFsCapwapDebugMask PROTO ((INT4 *));
INT4 CapwapGetFsDtlsDebugMask PROTO ((INT4 *));
INT4 CapwapGetFsDtlsEncryption PROTO ((INT4 *));
INT4 CapwapGetFsDtlsEncryptAlogritham PROTO ((INT4 *));
INT4 CapwapGetFsStationType PROTO ((INT4 *));
INT4 CapwapGetFsAcTimestampTrigger PROTO ((INT4 *));
INT4 CapwapGetFsCapwapFsAcTimestampTrigger PROTO ((UINT4 , INT4 *));
INT4 CapwapGetAllFsWtpModelTable PROTO ((tCapwapFsWtpModelEntry *));
INT4 CapwapGetAllUtlFsWtpModelTable PROTO((tCapwapFsWtpModelEntry *, tCapwapFsWtpModelEntry *));
INT4 CapwapGetAllFsWtpRadioTable PROTO ((tCapwapFsWtpRadioEntry *));
INT4 CapwapGetAllUtlFsWtpRadioTable PROTO((tCapwapFsWtpRadioEntry *, tCapwapFsWtpRadioEntry *));
INT4 CapwapGetAllFsCapwapWhiteListTable PROTO ((tCapwapFsCapwapWhiteListEntry *));
INT4 CapwapGetAllUtlFsCapwapWhiteListTable PROTO((tCapwapFsCapwapWhiteListEntry *, tCapwapFsCapwapWhiteListEntry *));
INT4 CapwapGetAllFsCapwapBlackList PROTO ((tCapwapFsCapwapBlackListEntry *));
INT4 CapwapGetAllUtlFsCapwapBlackList PROTO((tCapwapFsCapwapBlackListEntry *, tCapwapFsCapwapBlackListEntry *));
INT4 CapwapGetAllFsCapwapWtpConfigTable PROTO ((tCapwapFsCapwapWtpConfigEntry *));
INT4 CapwapGetAllUtlFsCapwapWtpConfigTable PROTO((tCapwapFsCapwapWtpConfigEntry *, tCapwapFsCapwapWtpConfigEntry *));
INT4 CapwapGetAllFsCapwapLinkEncryptionTable PROTO ((tCapwapFsCapwapLinkEncryptionEntry *));
INT4 CapwapGetAllUtlFsCapwapLinkEncryptionTable PROTO((tCapwapFsCapwapLinkEncryptionEntry *, tCapwapFsCapwapLinkEncryptionEntry *));
INT4 CapwapGetAllFsCawapDefaultWtpProfileTable PROTO ((tCapwapFsCapwapDefaultWtpProfileEntry *));
INT4 CapwapGetAllUtlFsCawapDefaultWtpProfileTable PROTO((tCapwapFsCapwapDefaultWtpProfileEntry *, tCapwapFsCapwapDefaultWtpProfileEntry *));
INT4 CapwapGetAllFsCapwapDnsProfileTable PROTO ((tCapwapFsCapwapDnsProfileEntry *));
INT4 CapwapGetAllUtlFsCapwapDnsProfileTable PROTO((tCapwapFsCapwapDnsProfileEntry *, tCapwapFsCapwapDnsProfileEntry *));
INT4 CapwapGetAllFsWtpNativeVlanIdTable PROTO ((tCapwapFsWtpNativeVlanIdTable *));
INT4 CapwapGetAllUtlFsWtpNativeVlanIdTable PROTO((tCapwapFsWtpNativeVlanIdTable *, tCapwapFsWtpNativeVlanIdTable *));
INT4 CapwapGetAllFsWtpLocalRoutingTable PROTO ((tCapwapFsWtpLocalRoutingTable *));
INT4 CapwapGetAllUtlFsWtpLocalRoutingTable PROTO((tCapwapFsWtpLocalRoutingTable *, tCapwapFsWtpLocalRoutingTable *));

INT4 CapwapGetAllFsCawapDiscStatsTable PROTO ((tCapwapFsCawapDiscStatsEntry *));
INT4 CapwapGetAllUtlFsCawapDiscStatsTable PROTO((tCapwapFsCawapDiscStatsEntry *, tCapwapFsCawapDiscStatsEntry *));
INT4 CapwapGetAllFsCawapJoinStatsTable PROTO ((tCapwapFsCawapJoinStatsEntry *));
INT4 CapwapGetAllUtlFsCawapJoinStatsTable PROTO((tCapwapFsCawapJoinStatsEntry *, tCapwapFsCawapJoinStatsEntry *));
INT4 CapwapGetAllFsCawapConfigStatsTable PROTO ((tCapwapFsCawapConfigStatsEntry *));
INT4 CapwapGetAllUtlFsCawapConfigStatsTable PROTO((tCapwapFsCawapConfigStatsEntry *, tCapwapFsCawapConfigStatsEntry *));
INT4 CapwapGetAllFsCawapRunStatsTable PROTO ((tCapwapFsCawapRunStatsEntry *));
INT4 CapwapGetAllUtlFsCawapRunStatsTable PROTO((tCapwapFsCawapRunStatsEntry *, tCapwapFsCawapRunStatsEntry *));
INT4 CapwapGetAllFsCapwapWirelessBindingTable PROTO ((tCapwapFsCapwapWirelessBindingEntry *));
INT4 CapwapGetAllUtlFsCapwapWirelessBindingTable PROTO((tCapwapFsCapwapWirelessBindingEntry *, tCapwapFsCapwapWirelessBindingEntry *));
INT4 CapwapGetAllFsCapwapStationWhiteList PROTO ((tCapwapFsCapwapStationWhiteListEntry *));
INT4 CapwapGetAllUtlFsCapwapStationWhiteList PROTO((tCapwapFsCapwapStationWhiteListEntry *, tCapwapFsCapwapStationWhiteListEntry *));
INT4 CapwapGetAllFsCapwapWtpRebootStatisticsTable PROTO ((tCapwapFsCapwapWtpRebootStatisticsEntry *));
INT4 CapwapGetAllUtlFsCapwapWtpRebootStatisticsTable PROTO((tCapwapFsCapwapWtpRebootStatisticsEntry *, tCapwapFsCapwapWtpRebootStatisticsEntry *));
INT4 CapwapGetAllFsCapwapWtpRadioStatisticsTable PROTO ((tCapwapFsCapwapWtpRadioStatisticsEntry *));
INT4 CapwapGetAllUtlFsCapwapWtpRadioStatisticsTable PROTO((tCapwapFsCapwapWtpRadioStatisticsEntry *, tCapwapFsCapwapWtpRadioStatisticsEntry *));
INT4 CapwapGetAllUtlFsCapwapDot11StatisticsTable PROTO ((tCapwapFsCapwapWtpDot11StatisticsEntry *));
INT4 CapwapSetFsCapwapEnable PROTO ((INT4 ));
INT4 CapwapSetFsCapwapShutdown PROTO ((INT4 ));
INT4 CapwapSetFsCapwapControlUdpPort PROTO ((UINT4 ));
INT4 CapwapSetFsCapwapControlChannelDTLSPolicyOptions PROTO ((UINT1 *));
INT4 CapwapSetFsCapwapDataChannelDTLSPolicyOptions PROTO ((UINT1 *));
INT4 CapwapSetFsWlcDiscoveryMode PROTO ((UINT1 *));
INT4 CapwapSetFsCapwapWtpModeIgnore PROTO ((INT4 ));
INT4 CapwapSetFsCapwapDebugMask PROTO ((INT4 ));
INT4 CapwapSetFsDtlsDebugMask PROTO ((INT4 ));
INT4 CapwapSetFsDtlsEncryption PROTO ((INT4 ));
INT4 CapwapSetFsDtlsEncryptAlogritham PROTO ((INT4 ));
INT4 CapwapSetFsStationType PROTO ((INT4 ));
INT4 CapwapSetFsAcTimestampTrigger PROTO ((INT4 ));
INT4 CapwapSetFsCapwapFsAcTimestampTrigger PROTO ((INT4 , UINT4 ));
INT4 CapwapSetAllFsWtpModelTable PROTO ((tCapwapFsWtpModelEntry *, tCapwapIsSetFsWtpModelEntry *, INT4 , INT4 ));
INT4 CapwapSetAllFsWtpRadioTable PROTO ((tCapwapFsWtpRadioEntry *, tCapwapIsSetFsWtpRadioEntry *, INT4 , INT4 ));
INT4 CapwapSetAllFsCapwapWhiteListTable PROTO ((tCapwapFsCapwapWhiteListEntry *, tCapwapIsSetFsCapwapWhiteListEntry *, INT4 , INT4 ));
INT4 CapwapSetAllFsCapwapBlackList PROTO ((tCapwapFsCapwapBlackListEntry *, tCapwapIsSetFsCapwapBlackListEntry *, INT4 , INT4 ));
INT4 CapwapSetAllFsCapwapWtpConfigTable PROTO ((tCapwapFsCapwapWtpConfigEntry *, tCapwapIsSetFsCapwapWtpConfigEntry *, INT4 , INT4 ));
INT4 CapwapSetAllFsCapwapLinkEncryptionTable PROTO ((tCapwapFsCapwapLinkEncryptionEntry *, tCapwapIsSetFsCapwapLinkEncryptionEntry *, INT4 , INT4 ));
INT4 CapwapSetAllFsCawapDefaultWtpProfileTable PROTO ((tCapwapFsCapwapDefaultWtpProfileEntry *, tCapwapIsSetFsCapwapDefaultWtpProfileEntry *, INT4 , INT4 ));
INT4 CapwapSetAllFsCapwapDnsProfileTable PROTO ((tCapwapFsCapwapDnsProfileEntry *, tCapwapIsSetFsCapwapDnsProfileEntry *, INT4 , INT4 ));
INT4 CapwapSetAllFsWtpNativeVlanIdTable PROTO ((tCapwapFsWtpNativeVlanIdTable *, tCapwapIsSetFsWtpNativeVlanIdTable *, INT4 , INT4 ));
INT4 CapwapSetAllFsWtpLocalRoutingTable PROTO ((tCapwapFsWtpLocalRoutingTable *, tCapwapIsSetFsWtpLocalRoutingTable *, INT4 , INT4 ));
INT4 CapwapSetAllFsCawapDiscStatsTable PROTO ((tCapwapFsCawapDiscStatsEntry *, tCapwapIsSetFsCawapDiscStatsEntry *, INT4 , INT4 ));
INT4 CapwapSetAllFsCawapJoinStatsTable PROTO ((tCapwapFsCawapJoinStatsEntry *, tCapwapIsSetFsCawapJoinStatsEntry *, INT4 , INT4 ));
INT4 CapwapSetAllFsCawapConfigStatsTable PROTO ((tCapwapFsCawapConfigStatsEntry *, tCapwapIsSetFsCawapConfigStatsEntry *, INT4 , INT4 ));
INT4 CapwapSetAllFsCawapRunStatsTable PROTO ((tCapwapFsCawapRunStatsEntry *, tCapwapIsSetFsCawapRunStatsEntry *, INT4 , INT4 ));
INT4 CapwapSetAllFsCapwapWirelessBindingTable PROTO ((tCapwapFsCapwapWirelessBindingEntry *, tCapwapIsSetFsCapwapWirelessBindingEntry *, INT4 , INT4 ));
INT4 CapwapSetAllFsCapwapStationWhiteList PROTO ((tCapwapFsCapwapStationWhiteListEntry *, tCapwapIsSetFsCapwapStationWhiteListEntry *, INT4 , INT4 ));
INT4 CapwapSetAllFsCapwapWtpRebootStatisticsTable PROTO ((tCapwapFsCapwapWtpRebootStatisticsEntry *, tCapwapIsSetFsCapwapWtpRebootStatisticsEntry *, INT4 , INT4 ));
INT4 CapwapSetAllFsCapwapWtpRadioStatisticsTable PROTO ((tCapwapFsCapwapWtpRadioStatisticsEntry *, tCapwapIsSetFsCapwapWtpRadioStatisticsEntry *));

INT4 CapwapInitializeFsWtpModelTable PROTO ((tCapwapFsWtpModelEntry *));

INT4 CapwapInitializeMibFsWtpModelTable PROTO ((tCapwapFsWtpModelEntry *));

INT4 CapwapInitializeFsWtpRadioTable PROTO ((tCapwapFsWtpRadioEntry *));

INT4 CapwapInitializeMibFsWtpRadioTable PROTO ((tCapwapFsWtpRadioEntry *));

INT4 CapwapInitializeFsCapwapWhiteListTable PROTO ((tCapwapFsCapwapWhiteListEntry *));

INT4 CapwapInitializeMibFsCapwapWhiteListTable PROTO ((tCapwapFsCapwapWhiteListEntry *));

INT4 CapwapInitializeFsCapwapBlackList PROTO ((tCapwapFsCapwapBlackListEntry *));

INT4 CapwapInitializeMibFsCapwapBlackList PROTO ((tCapwapFsCapwapBlackListEntry *));

INT4 CapwapInitializeFsCapwapWtpConfigTable PROTO ((tCapwapFsCapwapWtpConfigEntry *));

INT4 CapwapInitializeMibFsCapwapWtpConfigTable PROTO ((tCapwapFsCapwapWtpConfigEntry *));

INT4 CapwapInitializeFsCapwapLinkEncryptionTable PROTO ((tCapwapFsCapwapLinkEncryptionEntry *));

INT4 CapwapInitializeMibFsCapwapLinkEncryptionTable PROTO ((tCapwapFsCapwapLinkEncryptionEntry *));

INT4 CapwapInitializeFsCawapDefaultWtpProfileTable PROTO ((tCapwapFsCapwapDefaultWtpProfileEntry *));

INT4 CapwapInitializeMibFsCawapDefaultWtpProfileTable PROTO ((tCapwapFsCapwapDefaultWtpProfileEntry *));

INT4 CapwapInitializeFsCapwapDnsProfileTable PROTO ((tCapwapFsCapwapDnsProfileEntry *));

INT4 CapwapInitializeMibFsCapwapDnsProfileTable PROTO ((tCapwapFsCapwapDnsProfileEntry *));

INT4 CapwapInitializeFsWtpNativeVlanIdTable PROTO ((tCapwapFsWtpNativeVlanIdTable *));

INT4 CapwapInitializeMibFsWtpNativeVlanIdTable PROTO ((tCapwapFsWtpNativeVlanIdTable *));

INT4 CapwapInitializeFsWtpLocalRoutingTable PROTO ((tCapwapFsWtpLocalRoutingTable *));

INT4 CapwapInitializeMibFsWtpLocalRoutingTable PROTO ((tCapwapFsWtpLocalRoutingTable *));


INT4 CapwapInitializeFsCawapDiscStatsTable PROTO ((tCapwapFsCawapDiscStatsEntry *));

INT4 CapwapInitializeMibFsCawapDiscStatsTable PROTO ((tCapwapFsCawapDiscStatsEntry *));

INT4 CapwapInitializeFsCawapJoinStatsTable PROTO ((tCapwapFsCawapJoinStatsEntry *));

INT4 CapwapInitializeMibFsCawapJoinStatsTable PROTO ((tCapwapFsCawapJoinStatsEntry *));

INT4 CapwapInitializeFsCawapConfigStatsTable PROTO ((tCapwapFsCawapConfigStatsEntry *));

INT4 CapwapInitializeMibFsCawapConfigStatsTable PROTO ((tCapwapFsCawapConfigStatsEntry *));

INT4 CapwapInitializeFsCawapRunStatsTable PROTO ((tCapwapFsCawapRunStatsEntry *));

INT4 CapwapInitializeMibFsCawapRunStatsTable PROTO ((tCapwapFsCawapRunStatsEntry *));

INT4 CapwapInitializeFsCapwapWirelessBindingTable PROTO ((tCapwapFsCapwapWirelessBindingEntry *));

INT4 CapwapInitializeMibFsCapwapWirelessBindingTable PROTO ((tCapwapFsCapwapWirelessBindingEntry *));

INT4 CapwapInitializeFsCapwapStationWhiteList PROTO ((tCapwapFsCapwapStationWhiteListEntry *));

INT4 CapwapInitializeMibFsCapwapStationWhiteList PROTO ((tCapwapFsCapwapStationWhiteListEntry *));

INT4 CapwapInitializeFsCapwapWtpRebootStatisticsTable PROTO ((tCapwapFsCapwapWtpRebootStatisticsEntry *));

INT4 CapwapInitializeMibFsCapwapWtpRebootStatisticsTable PROTO ((tCapwapFsCapwapWtpRebootStatisticsEntry *));
INT4 CapwapTestFsCapwapEnable PROTO ((UINT4 *,INT4 ));
INT4 CapwapTestFsCapwapShutdown PROTO ((UINT4 *,INT4 ));
INT4 CapwapTestFsCapwapControlUdpPort PROTO ((UINT4 *,UINT4 ));
INT4 CapwapTestFsCapwapControlChannelDTLSPolicyOptions PROTO ((UINT4 *,UINT1 *));
INT4 CapwapTestFsCapwapDataChannelDTLSPolicyOptions PROTO ((UINT4 *,UINT1 *));
INT4 CapwapTestFsWlcDiscoveryMode PROTO ((UINT4 *,UINT1 *));
INT4 CapwapTestFsCapwapWtpModeIgnore PROTO ((UINT4 *,INT4 ));
INT4 CapwapTestFsCapwapDebugMask PROTO ((UINT4 *,INT4 ));
INT4 CapwapTestFsDtlsDebugMask PROTO ((UINT4 *,INT4 ));
INT4 CapwapTestFsDtlsEncryption PROTO ((UINT4 *,INT4 ));
INT4 CapwapTestFsDtlsEncryptAlogritham PROTO ((UINT4 *,INT4 ));
INT4 CapwapTestFsStationType PROTO ((UINT4 *,INT4 ));
INT4 CapwapTestFsAcTimestampTrigger PROTO ((UINT4 *, INT4));
INT4 CapwapTestFsCapwapFsAcTimestampTrigger PROTO ((UINT4 *, UINT4 , INT4));
INT4 CapwapTestAllFsWtpModelTable PROTO ((UINT4 *, tCapwapFsWtpModelEntry *, tCapwapIsSetFsWtpModelEntry *, INT4 , INT4));
INT4 CapwapTestAllFsWtpRadioTable PROTO ((UINT4 *, tCapwapFsWtpRadioEntry *, tCapwapIsSetFsWtpRadioEntry *, INT4 , INT4));
INT4 CapwapTestAllFsCapwapWhiteListTable PROTO ((UINT4 *, tCapwapFsCapwapWhiteListEntry *, tCapwapIsSetFsCapwapWhiteListEntry *, INT4 , INT4));
INT4 CapwapTestAllFsCapwapBlackList PROTO ((UINT4 *, tCapwapFsCapwapBlackListEntry *, tCapwapIsSetFsCapwapBlackListEntry *, INT4 , INT4));
INT4 CapwapTestAllFsCapwapWtpConfigTable PROTO ((UINT4 *, tCapwapFsCapwapWtpConfigEntry *, tCapwapIsSetFsCapwapWtpConfigEntry *, INT4 , INT4));
INT4 CapwapTestAllFsCapwapLinkEncryptionTable PROTO ((UINT4 *, tCapwapFsCapwapLinkEncryptionEntry *, tCapwapIsSetFsCapwapLinkEncryptionEntry *, INT4 , INT4));
INT4 CapwapTestAllFsCawapDefaultWtpProfileTable PROTO ((UINT4 *, tCapwapFsCapwapDefaultWtpProfileEntry *, tCapwapIsSetFsCapwapDefaultWtpProfileEntry *, INT4 , INT4));
INT4 CapwapTestAllFsCapwapDnsProfileTable PROTO ((UINT4 *, tCapwapFsCapwapDnsProfileEntry *, tCapwapIsSetFsCapwapDnsProfileEntry *, INT4 , INT4));
INT4 CapwapTestAllFsWtpNativeVlanIdTable PROTO ((UINT4 *, tCapwapFsWtpNativeVlanIdTable *, tCapwapIsSetFsWtpNativeVlanIdTable *, INT4 , INT4));
INT4 CapwapTestAllFsWtpLocalRoutingTable PROTO ((UINT4 *, tCapwapFsWtpLocalRoutingTable *, tCapwapIsSetFsWtpLocalRoutingTable *, INT4 , INT4));
INT4 CapwapTestAllFsCawapDiscStatsTable PROTO ((UINT4 *, tCapwapFsCawapDiscStatsEntry *, tCapwapIsSetFsCawapDiscStatsEntry *, INT4 , INT4));
INT4 CapwapTestAllFsCawapJoinStatsTable PROTO ((UINT4 *, tCapwapFsCawapJoinStatsEntry *, tCapwapIsSetFsCawapJoinStatsEntry *, INT4 , INT4));
INT4 CapwapTestAllFsCawapConfigStatsTable PROTO ((UINT4 *, tCapwapFsCawapConfigStatsEntry *, tCapwapIsSetFsCawapConfigStatsEntry *, INT4 , INT4));
INT4 CapwapTestAllFsCawapRunStatsTable PROTO ((UINT4 *, tCapwapFsCawapRunStatsEntry *, tCapwapIsSetFsCawapRunStatsEntry *, INT4 , INT4));
INT4 CapwapTestAllFsCapwapWirelessBindingTable PROTO ((UINT4 *, tCapwapFsCapwapWirelessBindingEntry *, tCapwapIsSetFsCapwapWirelessBindingEntry *, INT4 , INT4));
INT4 CapwapTestAllFsCapwapStationWhiteList PROTO ((UINT4 *, tCapwapFsCapwapStationWhiteListEntry *, tCapwapIsSetFsCapwapStationWhiteListEntry *, INT4 , INT4));
INT4 CapwapTestAllFsCapwapWtpRebootStatisticsTable PROTO ((UINT4 *, tCapwapFsCapwapWtpRebootStatisticsEntry *, tCapwapIsSetFsCapwapWtpRebootStatisticsEntry *, INT4 , INT4));
INT4 CapwapTestAllFsCapwapWtpRadioStatisticsTable PROTO ((UINT4 *, tCapwapFsCapwapWtpRadioStatisticsEntry *, tCapwapIsSetFsCapwapWtpRadioStatisticsEntry *));
tCapwapFsWtpModelEntry * CapwapGetFsWtpModelTable PROTO ((tCapwapFsWtpModelEntry *));
tCapwapFsWtpRadioEntry * CapwapGetFsWtpRadioTable PROTO ((tCapwapFsWtpRadioEntry *));
tCapwapFsCapwapWhiteListEntry * CapwapGetFsCapwapWhiteListTable PROTO ((tCapwapFsCapwapWhiteListEntry *));
tCapwapFsCapwapBlackListEntry * CapwapGetFsCapwapBlackList PROTO ((tCapwapFsCapwapBlackListEntry *));
tCapwapFsCapwapWtpConfigEntry * CapwapGetFsCapwapWtpConfigTable PROTO ((tCapwapFsCapwapWtpConfigEntry *));
tCapwapFsCapwapLinkEncryptionEntry * CapwapGetFsCapwapLinkEncryptionTable PROTO ((tCapwapFsCapwapLinkEncryptionEntry *));
tCapwapFsCapwapDefaultWtpProfileEntry * CapwapGetFsCawapDefaultWtpProfileTable PROTO ((tCapwapFsCapwapDefaultWtpProfileEntry *));
tCapwapFsCapwapDnsProfileEntry * CapwapGetFsCapwapDnsProfileTable PROTO ((tCapwapFsCapwapDnsProfileEntry *));
tCapwapFsWtpNativeVlanIdTable * CapwapGetFsWtpNativeVlanIdTable PROTO ((tCapwapFsWtpNativeVlanIdTable *));
tCapwapFsWtpLocalRoutingTable * CapwapGetFsWtpLocalRoutingTable PROTO ((tCapwapFsWtpLocalRoutingTable *));
tCapwapFsCawapDiscStatsEntry * CapwapGetFsCawapDiscStatsTable PROTO ((tCapwapFsCawapDiscStatsEntry *));
tCapwapFsCawapJoinStatsEntry * CapwapGetFsCawapJoinStatsTable PROTO ((tCapwapFsCawapJoinStatsEntry *));
tCapwapFsCawapConfigStatsEntry * CapwapGetFsCawapConfigStatsTable PROTO ((tCapwapFsCawapConfigStatsEntry *));
tCapwapFsCawapRunStatsEntry * CapwapGetFsCawapRunStatsTable PROTO ((tCapwapFsCawapRunStatsEntry *));
tCapwapFsCapwapWirelessBindingEntry * CapwapGetFsCapwapWirelessBindingTable PROTO ((tCapwapFsCapwapWirelessBindingEntry *));
tCapwapFsCapwapStationWhiteListEntry * CapwapGetFsCapwapStationWhiteList PROTO ((tCapwapFsCapwapStationWhiteListEntry *));
tCapwapFsCapwapWtpRebootStatisticsEntry * CapwapGetFsCapwapWtpRebootStatisticsTable PROTO ((tCapwapFsCapwapWtpRebootStatisticsEntry *));
tCapwapFsCapwapWtpRadioStatisticsEntry * CapwapGetFsCapwapWtpRadioStatisticsTable PROTO ((tCapwapFsCapwapWtpRadioStatisticsEntry *));
tCapwapFsWtpModelEntry * CapwapGetFirstFsWtpModelTable PROTO ((VOID));
tCapwapFsWtpRadioEntry * CapwapGetFirstFsWtpRadioTable PROTO ((VOID));
tCapwapFsCapwapWhiteListEntry * CapwapGetFirstFsCapwapWhiteListTable PROTO ((VOID));
tCapwapFsCapwapBlackListEntry * CapwapGetFirstFsCapwapBlackList PROTO ((VOID));
tCapwapFsCapwapWtpConfigEntry * CapwapGetFirstFsCapwapWtpConfigTable PROTO ((VOID));
tCapwapFsCapwapLinkEncryptionEntry * CapwapGetFirstFsCapwapLinkEncryptionTable PROTO ((VOID));
tCapwapFsCapwapDefaultWtpProfileEntry * CapwapGetFirstFsCawapDefaultWtpProfileTable PROTO ((VOID));
tCapwapFsCapwapDnsProfileEntry * CapwapGetFirstFsCapwapDnsProfileTable PROTO ((VOID));
tCapwapFsWtpNativeVlanIdTable * CapwapGetFirstFsWtpNativeVlanIdTable PROTO ((VOID));
tCapwapFsWtpLocalRoutingTable * CapwapGetFirstFsWtpLocalRoutingTable PROTO ((VOID));
tCapwapFsCawapDiscStatsEntry * CapwapGetFirstFsCawapDiscStatsTable PROTO ((VOID));
tCapwapFsCawapJoinStatsEntry * CapwapGetFirstFsCawapJoinStatsTable PROTO ((VOID));
tCapwapFsCawapConfigStatsEntry * CapwapGetFirstFsCawapConfigStatsTable PROTO ((VOID));
INT1 CapwapGetFirstFsCawapRunStatsTable PROTO ((UINT4 *));
tCapwapFsCapwapWirelessBindingEntry * CapwapGetFirstFsCapwapWirelessBindingTable PROTO ((VOID));
tCapwapFsCapwapStationWhiteListEntry * CapwapGetFirstFsCapwapStationWhiteList PROTO ((VOID));
INT1 CapwapGetFirstFsCapwapWtpRebootStatisticsTable PROTO ((UINT4 *));
INT1 CapwapGetFirstFsCapwapWtpRadioStatisticsTable PROTO ((INT4 *));
tCapwapFsWtpModelEntry * CapwapGetNextFsWtpModelTable PROTO ((tCapwapFsWtpModelEntry *));
tCapwapFsWtpRadioEntry * CapwapGetNextFsWtpRadioTable PROTO ((tCapwapFsWtpRadioEntry *));
tCapwapFsCapwapWhiteListEntry * CapwapGetNextFsCapwapWhiteListTable PROTO ((tCapwapFsCapwapWhiteListEntry *));
tCapwapFsCapwapBlackListEntry * CapwapGetNextFsCapwapBlackList PROTO ((tCapwapFsCapwapBlackListEntry *));
tCapwapFsCapwapWtpConfigEntry * CapwapGetNextFsCapwapWtpConfigTable PROTO ((tCapwapFsCapwapWtpConfigEntry *));
tCapwapFsCapwapLinkEncryptionEntry * CapwapGetNextFsCapwapLinkEncryptionTable PROTO ((tCapwapFsCapwapLinkEncryptionEntry *));
tCapwapFsCapwapDefaultWtpProfileEntry * CapwapGetNextFsCawapDefaultWtpProfileTable PROTO ((tCapwapFsCapwapDefaultWtpProfileEntry *));
tCapwapFsCapwapDnsProfileEntry * CapwapGetNextFsCapwapDnsProfileTable PROTO ((tCapwapFsCapwapDnsProfileEntry *));
tCapwapFsWtpNativeVlanIdTable * CapwapGetNextFsWtpNativeVlanIdTable PROTO ((tCapwapFsWtpNativeVlanIdTable *));
tCapwapFsWtpLocalRoutingTable * CapwapGetNextFsWtpLocalRoutingTable PROTO ((tCapwapFsWtpLocalRoutingTable *));
tCapwapFsCawapDiscStatsEntry * CapwapGetNextFsCawapDiscStatsTable PROTO ((tCapwapFsCawapDiscStatsEntry *));
tCapwapFsCawapJoinStatsEntry * CapwapGetNextFsCawapJoinStatsTable PROTO ((tCapwapFsCawapJoinStatsEntry *));
tCapwapFsCawapConfigStatsEntry * CapwapGetNextFsCawapConfigStatsTable PROTO ((tCapwapFsCawapConfigStatsEntry *));
INT1 CapwapGetNextFsCawapRunStatsTable PROTO ((UINT4 , UINT4*));
tCapwapFsCapwapWirelessBindingEntry * CapwapGetNextFsCapwapWirelessBindingTable PROTO ((tCapwapFsCapwapWirelessBindingEntry *));
tCapwapFsCapwapStationWhiteListEntry * CapwapGetNextFsCapwapStationWhiteList PROTO ((tCapwapFsCapwapStationWhiteListEntry *));
INT1 CapwapGetNextFsCapwapWtpRebootStatisticsTable PROTO ((UINT4, UINT4 *));
INT1 CapwapGetNextFsCapwapWtpRadioStatisticsTable PROTO ((INT4, INT4 *));
INT1 CapwapGetNextCapwATPStatsTable PROTO ((UINT4, UINT4 *));
INT1 CapwapGetFirstCapwATPStatsTable PROTO((UINT4 *));
INT4 FsWtpModelTableFilterInputs PROTO ((tCapwapFsWtpModelEntry *, tCapwapFsWtpModelEntry *, tCapwapIsSetFsWtpModelEntry *));
INT4 FsWtpRadioTableFilterInputs PROTO ((tCapwapFsWtpRadioEntry *, tCapwapFsWtpRadioEntry *, tCapwapIsSetFsWtpRadioEntry *));
INT4 FsCapwapWhiteListTableFilterInputs PROTO ((tCapwapFsCapwapWhiteListEntry *, tCapwapFsCapwapWhiteListEntry *, tCapwapIsSetFsCapwapWhiteListEntry *));
INT4 FsCapwapBlackListFilterInputs PROTO ((tCapwapFsCapwapBlackListEntry *, tCapwapFsCapwapBlackListEntry *, tCapwapIsSetFsCapwapBlackListEntry *));
INT4 FsCapwapWtpConfigTableFilterInputs PROTO ((tCapwapFsCapwapWtpConfigEntry *, tCapwapFsCapwapWtpConfigEntry *, tCapwapIsSetFsCapwapWtpConfigEntry *));
INT4 FsCapwapLinkEncryptionTableFilterInputs PROTO ((tCapwapFsCapwapLinkEncryptionEntry *, tCapwapFsCapwapLinkEncryptionEntry *, tCapwapIsSetFsCapwapLinkEncryptionEntry *));
INT4 FsCawapDefaultWtpProfileTableFilterInputs PROTO ((tCapwapFsCapwapDefaultWtpProfileEntry *, tCapwapFsCapwapDefaultWtpProfileEntry *, tCapwapIsSetFsCapwapDefaultWtpProfileEntry *));
INT4 FsCapwapDnsProfileTableFilterInputs PROTO ((tCapwapFsCapwapDnsProfileEntry *, tCapwapFsCapwapDnsProfileEntry *, tCapwapIsSetFsCapwapDnsProfileEntry *));
INT4 FsWtpNativeVlanIdTableFilterInputs PROTO ((tCapwapFsWtpNativeVlanIdTable *, tCapwapFsWtpNativeVlanIdTable *, tCapwapIsSetFsWtpNativeVlanIdTable *));
INT4 FsWtpLocalRoutingTableFilterInputs PROTO ((tCapwapFsWtpLocalRoutingTable *, tCapwapFsWtpLocalRoutingTable *, tCapwapIsSetFsWtpLocalRoutingTable *));
INT4 FsCawapDiscStatsTableFilterInputs PROTO ((tCapwapFsCawapDiscStatsEntry *, tCapwapFsCawapDiscStatsEntry *, tCapwapIsSetFsCawapDiscStatsEntry *));
INT4 FsCawapJoinStatsTableFilterInputs PROTO ((tCapwapFsCawapJoinStatsEntry *, tCapwapFsCawapJoinStatsEntry *, tCapwapIsSetFsCawapJoinStatsEntry *));
INT4 FsCawapConfigStatsTableFilterInputs PROTO ((tCapwapFsCawapConfigStatsEntry *, tCapwapFsCawapConfigStatsEntry *, tCapwapIsSetFsCawapConfigStatsEntry *));
INT4 FsCawapRunStatsTableFilterInputs PROTO ((tCapwapFsCawapRunStatsEntry *, tCapwapFsCawapRunStatsEntry *, tCapwapIsSetFsCawapRunStatsEntry *));
INT4 FsCapwapWirelessBindingTableFilterInputs PROTO ((tCapwapFsCapwapWirelessBindingEntry *, tCapwapFsCapwapWirelessBindingEntry *, tCapwapIsSetFsCapwapWirelessBindingEntry *));
INT4 FsCapwapStationWhiteListFilterInputs PROTO ((tCapwapFsCapwapStationWhiteListEntry *, tCapwapFsCapwapStationWhiteListEntry *, tCapwapIsSetFsCapwapStationWhiteListEntry *));
INT4 FsCapwapWtpRebootStatisticsTableFilterInputs PROTO ((tCapwapFsCapwapWtpRebootStatisticsEntry *, tCapwapFsCapwapWtpRebootStatisticsEntry *, tCapwapIsSetFsCapwapWtpRebootStatisticsEntry *));
INT4 FsCapwapWtpRadioStatisticsTableFilterInputs PROTO ((tCapwapFsCapwapWtpRadioStatisticsEntry *, tCapwapFsCapwapWtpRadioStatisticsEntry *, tCapwapIsSetFsCapwapWtpRadioStatisticsEntry *));
INT4 CapwapUtilUpdateFsWtpModelTable PROTO ((tCapwapFsWtpModelEntry *, tCapwapFsWtpModelEntry *, tCapwapIsSetFsWtpModelEntry *));
INT4 CapwapUtilUpdateFsWtpRadioTable PROTO ((tCapwapFsWtpRadioEntry *, tCapwapFsWtpRadioEntry *, tCapwapIsSetFsWtpRadioEntry *));
INT4 CapwapUtilUpdateFsCapwapWhiteListTable PROTO ((tCapwapFsCapwapWhiteListEntry *, tCapwapFsCapwapWhiteListEntry *, tCapwapIsSetFsCapwapWhiteListEntry *));
INT4 CapwapUtilUpdateFsCapwapBlackList PROTO ((tCapwapFsCapwapBlackListEntry *, tCapwapFsCapwapBlackListEntry *, tCapwapIsSetFsCapwapBlackListEntry *));
INT4 CapwapUtilUpdateFsCapwapWtpConfigTable PROTO ((tCapwapFsCapwapWtpConfigEntry *, tCapwapFsCapwapWtpConfigEntry *, tCapwapIsSetFsCapwapWtpConfigEntry *));
INT4 CapwapUtilUpdateFsCapwapLinkEncryptionTable PROTO ((tCapwapFsCapwapLinkEncryptionEntry *, tCapwapFsCapwapLinkEncryptionEntry *, tCapwapIsSetFsCapwapLinkEncryptionEntry *));
INT4 CapwapUtilUpdateFsCawapDefaultWtpProfileTable PROTO ((tCapwapFsCapwapDefaultWtpProfileEntry *, tCapwapFsCapwapDefaultWtpProfileEntry *, tCapwapIsSetFsCapwapDefaultWtpProfileEntry *));
INT4 CapwapUtilUpdateFsCapwapDnsProfileTable PROTO ((tCapwapFsCapwapDnsProfileEntry *, tCapwapFsCapwapDnsProfileEntry *, tCapwapIsSetFsCapwapDnsProfileEntry *));
INT4 CapwapUtilUpdateFsWtpNativeVlanIdTable PROTO ((tCapwapFsWtpNativeVlanIdTable *, tCapwapFsWtpNativeVlanIdTable *, tCapwapIsSetFsWtpNativeVlanIdTable *));
INT4 CapwapUtilUpdateFsWtpLocalRoutingTable PROTO ((tCapwapFsWtpLocalRoutingTable *, tCapwapFsWtpLocalRoutingTable *, tCapwapIsSetFsWtpLocalRoutingTable *));
INT4 CapwapUtilUpdateFsCawapDiscStatsTable PROTO ((tCapwapFsCawapDiscStatsEntry *, tCapwapFsCawapDiscStatsEntry *, tCapwapIsSetFsCawapDiscStatsEntry *));
INT4 CapwapUtilUpdateFsCawapJoinStatsTable PROTO ((tCapwapFsCawapJoinStatsEntry *, tCapwapFsCawapJoinStatsEntry *, tCapwapIsSetFsCawapJoinStatsEntry *));
INT4 CapwapUtilUpdateFsCawapConfigStatsTable PROTO ((tCapwapFsCawapConfigStatsEntry *, tCapwapFsCawapConfigStatsEntry *, tCapwapIsSetFsCawapConfigStatsEntry *));
INT4 CapwapUtilUpdateFsCawapRunStatsTable PROTO ((tCapwapFsCawapRunStatsEntry *, tCapwapFsCawapRunStatsEntry *, tCapwapIsSetFsCawapRunStatsEntry *));
INT4 CapwapUtilUpdateFsCapwapWirelessBindingTable PROTO ((tCapwapFsCapwapWirelessBindingEntry *, tCapwapFsCapwapWirelessBindingEntry *, tCapwapIsSetFsCapwapWirelessBindingEntry *));
INT4 CapwapUtilUpdateFsCapwapStationWhiteList PROTO ((tCapwapFsCapwapStationWhiteListEntry *, tCapwapFsCapwapStationWhiteListEntry *, tCapwapIsSetFsCapwapStationWhiteListEntry *));
INT4 CapwapUtilUpdateFsCapwapWtpRebootStatisticsTable PROTO ((tCapwapFsCapwapWtpRebootStatisticsEntry *, tCapwapFsCapwapWtpRebootStatisticsEntry *, tCapwapIsSetFsCapwapWtpRebootStatisticsEntry *));
INT4 CapwapUtilUpdateFsCapwapWtpRadioStatisticsTable PROTO ((tCapwapFsCapwapWtpRadioStatisticsEntry *, tCapwapFsCapwapWtpRadioStatisticsEntry *, tCapwapIsSetFsCapwapWtpRadioStatisticsEntry *));
INT4 CapwapUtilGetBaseWtpProfileName(UINT4, tSNMP_OCTET_STRING_TYPE *, UINT1 *);
INT4 CapwapSetAllFsWtpModelTableTrigger PROTO ((tCapwapFsWtpModelEntry *, tCapwapIsSetFsWtpModelEntry *, INT4));
INT4 CapwapSetAllFsWtpRadioTableTrigger PROTO ((tCapwapFsWtpRadioEntry *, tCapwapIsSetFsWtpRadioEntry *, INT4));
INT4 CapwapSetAllFsCapwapWhiteListTableTrigger PROTO ((tCapwapFsCapwapWhiteListEntry *, tCapwapIsSetFsCapwapWhiteListEntry *, INT4));
INT4 CapwapSetAllFsCapwapBlackListTrigger PROTO ((tCapwapFsCapwapBlackListEntry *, tCapwapIsSetFsCapwapBlackListEntry *, INT4));
INT4 CapwapSetAllFsCapwapWtpConfigTableTrigger PROTO ((tCapwapFsCapwapWtpConfigEntry *, tCapwapIsSetFsCapwapWtpConfigEntry *, INT4));
INT4 CapwapSetAllFsCapwapLinkEncryptionTableTrigger PROTO ((tCapwapFsCapwapLinkEncryptionEntry *, tCapwapIsSetFsCapwapLinkEncryptionEntry *, INT4));
INT4 CapwapSetAllFsCawapDefaultWtpProfileTableTrigger PROTO ((tCapwapFsCapwapDefaultWtpProfileEntry *, tCapwapIsSetFsCapwapDefaultWtpProfileEntry *, INT4));
INT4 CapwapSetAllFsCapwapDnsProfileTableTrigger PROTO ((tCapwapFsCapwapDnsProfileEntry *, tCapwapIsSetFsCapwapDnsProfileEntry *, INT4));
INT4 CapwapSetAllFsWtpNativeVlanIdTableTrigger PROTO ((tCapwapFsWtpNativeVlanIdTable *, tCapwapIsSetFsWtpNativeVlanIdTable *, INT4));
INT4 CapwapSetAllFsWtpLocalRoutingTableTrigger PROTO ((tCapwapFsWtpLocalRoutingTable *, tCapwapIsSetFsWtpLocalRoutingTable *, INT4));
INT4 CapwapSetAllFsCawapDiscStatsTableTrigger PROTO ((tCapwapFsCawapDiscStatsEntry *, tCapwapIsSetFsCawapDiscStatsEntry *, INT4));
INT4 CapwapSetAllFsCawapJoinStatsTableTrigger PROTO ((tCapwapFsCawapJoinStatsEntry *, tCapwapIsSetFsCawapJoinStatsEntry *, INT4));
INT4 CapwapSetAllFsCawapConfigStatsTableTrigger PROTO ((tCapwapFsCawapConfigStatsEntry *, tCapwapIsSetFsCawapConfigStatsEntry *, INT4));
INT4 CapwapSetAllFsCawapRunStatsTableTrigger PROTO ((tCapwapFsCawapRunStatsEntry *, tCapwapIsSetFsCawapRunStatsEntry *, INT4));
INT4 CapwapSetAllFsCapwapWirelessBindingTableTrigger PROTO ((tCapwapFsCapwapWirelessBindingEntry *, tCapwapIsSetFsCapwapWirelessBindingEntry *, INT4));
INT4 CapwapSetAllFsCapwapStationWhiteListTrigger PROTO ((tCapwapFsCapwapStationWhiteListEntry *, tCapwapIsSetFsCapwapStationWhiteListEntry *, INT4));
INT4 CapwapSetAllFsCapwapWtpRebootStatisticsTableTrigger PROTO ((tCapwapFsCapwapWtpRebootStatisticsEntry *, tCapwapIsSetFsCapwapWtpRebootStatisticsEntry *, INT4));
INT4 CapwapSetAllFsCapwapWtpRadioStatisticsTableTrigger PROTO ((tCapwapFsCapwapWtpRadioStatisticsEntry *, tCapwapIsSetFsCapwapWtpRadioStatisticsEntry *, INT4));

tCapwapCapwapBaseRadioEventsStatsEntry * CapwapCapwapBaseRadioEventsStatsTableCreateApi PROTO ((tCapwapCapwapBaseRadioEventsStatsEntry *));
INT4 CapwapGetCapwapBaseWtpSessions PROTO ((UINT4 *));
INT4 CapwapGetCapwapBaseWtpSessionsLimit PROTO ((UINT4 *));
INT4 CapwapGetCapwapBaseStationSessions PROTO ((UINT4 *));
INT4 CapwapGetCapwapBaseStationSessionsLimit PROTO ((UINT4 *));
INT4 CapwapGetCapwapBaseDataChannelDTLSPolicyOptions PROTO ((UINT1 *));
INT4 CapwapGetCapwapBaseControlChannelAuthenOptions PROTO ((UINT1 *));
INT4 CapwapGetAllCapwapBaseAcNameListTable PROTO ((tCapwapCapwapBaseAcNameListEntry *));
INT4 CapwapGetAllUtlCapwapBaseAcNameListTable PROTO((tCapwapCapwapBaseAcNameListEntry *, tCapwapCapwapBaseAcNameListEntry *));
INT4 CapwapGetAllCapwapBaseMacAclTable PROTO ((tCapwapCapwapBaseMacAclEntry *));
INT4 CapwapGetAllUtlCapwapBaseMacAclTable PROTO((tCapwapCapwapBaseMacAclEntry *, tCapwapCapwapBaseMacAclEntry *));
INT4 CapwapGetAllCapwapBaseWtpProfileTable PROTO ((tCapwapCapwapBaseWtpProfileEntry *));
INT4 CapwapGetAllUtlCapwapBaseWtpProfileTable PROTO((tCapwapCapwapBaseWtpProfileEntry *, tCapwapCapwapBaseWtpProfileEntry *));
INT4 CapwapGetAllCapwapBaseWtpStateTable PROTO ((tCapwapCapwapBaseWtpStateEntry *));
INT4 CapwapGetAllUtlCapwapBaseWtpStateTable PROTO((tCapwapCapwapBaseWtpStateEntry *, tCapwapCapwapBaseWtpStateEntry *));
INT4 CapwapGetAllCapwapBaseWtpTable PROTO ((tCapwapCapwapBaseWtpEntry *));
INT4 CapwapGetAllUtlCapwapBaseWtpTable PROTO((tCapwapCapwapBaseWtpEntry *, tCapwapCapwapBaseWtpEntry *));
INT4 CapwapGetAllCapwapBaseWirelessBindingTable PROTO ((tCapwapCapwapBaseWirelessBindingEntry *));
INT4 CapwapGetAllUtlCapwapBaseWirelessBindingTable PROTO((tCapwapCapwapBaseWirelessBindingEntry *, tCapwapCapwapBaseWirelessBindingEntry *));
INT4 CapwapGetAllCapwapBaseStationTable PROTO ((tCapwapCapwapBaseStationEntry *));
INT4 CapwapGetAllUtlCapwapBaseStationTable PROTO((tCapwapCapwapBaseStationEntry *, tCapwapCapwapBaseStationEntry *));
INT4 CapwapGetAllCapwapBaseWtpEventsStatsTable PROTO ((tCapwapCapwapBaseWtpEventsStatsEntry *));
INT4 CapwapGetAllUtlCapwapBaseWtpEventsStatsTable PROTO((tCapwapCapwapBaseWtpEventsStatsEntry *, tCapwapCapwapBaseWtpEventsStatsEntry *));
INT4 CapwapGetAllCapwapBaseRadioEventsStatsTable PROTO ((tCapwapCapwapBaseRadioEventsStatsEntry *));
INT4 CapwapGetAllUtlCapwapBaseRadioEventsStatsTable PROTO((tCapwapCapwapBaseRadioEventsStatsEntry *, tCapwapCapwapBaseRadioEventsStatsEntry *));
INT4 CapwapGetCapwapBaseAcMaxRetransmit PROTO ((UINT4 *));
INT4 CapwapGetCapwapBaseAcChangeStatePendingTimer PROTO ((UINT4 *));
INT4 CapwapGetCapwapBaseAcDataCheckTimer PROTO ((UINT4 *));
INT4 CapwapGetCapwapBaseAcDTLSSessionDeleteTimer PROTO ((UINT4 *));
INT4 CapwapGetCapwapBaseAcEchoInterval PROTO ((UINT4 *));
INT4 CapwapGetCapwapBaseAcRetransmitInterval PROTO ((UINT4 *));
INT4 CapwapGetCapwapBaseAcSilentInterval PROTO ((UINT4 *));
INT4 CapwapGetCapwapBaseAcWaitDTLSTimer PROTO ((UINT4 *));
INT4 CapwapGetCapwapBaseAcWaitJoinTimer PROTO ((UINT4 *));
INT4 CapwapGetCapwapBaseAcEcnSupport PROTO ((INT4 *));
INT4 CapwapGetCapwapBaseFailedDTLSAuthFailureCount PROTO ((UINT4 *));
INT4 CapwapGetCapwapBaseFailedDTLSSessionCount PROTO ((UINT4 *));
INT4 CapwapGetCapwapBaseNtfWtpId PROTO ((UINT1 *));
INT4 CapwapGetCapwapBaseNtfRadioId PROTO ((UINT4 *));
INT4 CapwapGetCapwapBaseNtfChannelType PROTO ((INT4 *));
INT4 CapwapGetCapwapBaseNtfAuthenMethod PROTO ((INT4 *));
INT4 CapwapGetCapwapBaseNtfChannelDownReason PROTO ((INT4 *));
INT4 CapwapGetCapwapBaseNtfStationIdList PROTO ((UINT1 *));
INT4 CapwapGetCapwapBaseNtfAuthenFailureReason PROTO ((INT4 *));
INT4 CapwapGetCapwapBaseNtfRadioOperStatusFlag PROTO ((INT4 *));
INT4 CapwapGetCapwapBaseNtfRadioStatusCause PROTO ((INT4 *));
INT4 CapwapGetCapwapBaseNtfJoinFailureReason PROTO ((INT4 *));
INT4 CapwapGetCapwapBaseNtfImageFailureReason PROTO ((INT4 *));
INT4 CapwapGetCapwapBaseNtfConfigMsgErrorType PROTO ((INT4 *));
INT4 CapwapGetCapwapBaseNtfMsgErrorElements PROTO ((UINT1 *));
INT4 CapwapGetCapwapBaseChannelUpDownNotifyEnable PROTO ((INT4 *));
INT4 CapwapGetCapwapBaseDecryptErrorNotifyEnable PROTO ((INT4 *));
INT4 CapwapGetCapwapBaseJoinFailureNotifyEnable PROTO ((INT4 *));
INT4 CapwapGetCapwapBaseImageUpgradeFailureNotifyEnable PROTO ((INT4 *));
INT4 CapwapGetCapwapBaseConfigMsgErrorNotifyEnable PROTO ((INT4 *));
INT4 CapwapGetCapwapBaseRadioOperableStatusNotifyEnable PROTO ((INT4 *));
INT4 CapwapGetCapwapBaseAuthenFailureNotifyEnable PROTO ((INT4 *));
INT4 CapwapSetCapwapBaseWtpSessionsLimit PROTO ((UINT4 ));
INT4 CapwapSetCapwapBaseStationSessionsLimit PROTO ((UINT4 ));
INT4 CapwapSetAllCapwapBaseAcNameListTable PROTO ((tCapwapCapwapBaseAcNameListEntry *, tCapwapIsSetCapwapBaseAcNameListEntry *, INT4 , INT4 ));
INT4 CapwapSetAllCapwapBaseMacAclTable PROTO ((tCapwapCapwapBaseMacAclEntry *, tCapwapIsSetCapwapBaseMacAclEntry *, INT4 , INT4 ));
INT4 CapwapSetAllCapwapBaseWtpProfileTable PROTO ((tCapwapCapwapBaseWtpProfileEntry *, tCapwapIsSetCapwapBaseWtpProfileEntry *, INT4 , INT4 ));
INT4 CapwapSetCapwapBaseAcMaxRetransmit PROTO ((UINT4 ));
INT4 CapwapSetCapwapBaseAcChangeStatePendingTimer PROTO ((UINT4 ));
INT4 CapwapSetCapwapBaseAcDataCheckTimer PROTO ((UINT4 ));
INT4 CapwapSetCapwapBaseAcDTLSSessionDeleteTimer PROTO ((UINT4 ));
INT4 CapwapSetCapwapBaseAcEchoInterval PROTO ((UINT4 ));
INT4 CapwapSetCapwapBaseAcRetransmitInterval PROTO ((UINT4 ));
INT4 CapwapSetCapwapBaseAcSilentInterval PROTO ((UINT4 ));
INT4 CapwapSetCapwapBaseAcWaitDTLSTimer PROTO ((UINT4 ));
INT4 CapwapSetCapwapBaseAcWaitJoinTimer PROTO ((UINT4 ));
INT4 CapwapSetCapwapBaseAcEcnSupport PROTO ((INT4 ));
INT4 CapwapSetCapwapBaseChannelUpDownNotifyEnable PROTO ((INT4 ));
INT4 CapwapSetCapwapBaseDecryptErrorNotifyEnable PROTO ((INT4 ));
INT4 CapwapSetCapwapBaseJoinFailureNotifyEnable PROTO ((INT4 ));
INT4 CapwapSetCapwapBaseImageUpgradeFailureNotifyEnable PROTO ((INT4 ));
INT4 CapwapSetCapwapBaseConfigMsgErrorNotifyEnable PROTO ((INT4 ));
INT4 CapwapSetCapwapBaseRadioOperableStatusNotifyEnable PROTO ((INT4 ));
INT4 CapwapSetCapwapBaseAuthenFailureNotifyEnable PROTO ((INT4 ));

INT4 CapwapInitializeCapwapBaseAcNameListTable PROTO ((tCapwapCapwapBaseAcNameListEntry *));

INT4 CapwapInitializeMibCapwapBaseAcNameListTable PROTO ((tCapwapCapwapBaseAcNameListEntry *));

INT4 CapwapInitializeCapwapBaseMacAclTable PROTO ((tCapwapCapwapBaseMacAclEntry *));

INT4 CapwapInitializeMibCapwapBaseMacAclTable PROTO ((tCapwapCapwapBaseMacAclEntry *));

INT4 CapwapInitializeCapwapBaseWtpProfileTable PROTO ((tCapwapCapwapBaseWtpProfileEntry *));

INT4 CapwapInitializeMibCapwapBaseWtpProfileTable PROTO ((tCapwapCapwapBaseWtpProfileEntry *));
INT4 CapwapTestCapwapBaseWtpSessionsLimit PROTO ((UINT4 *,UINT4 ));


INT4 CapwapTestFsDtlsEncrption (UINT4 *pu4ErrorCode,
                           UINT4 u4FsDtlsEncrption);
INT4 CapwapTestFsDtlsEncAuthEncryptAlgorithm (UINT4 *pu4ErrorCode, UINT4
        u4FsDtlsEncAuthEncryptAlgorithm);

INT4 CapwapTestCapwapBaseStationSessionsLimit PROTO ((UINT4 *,UINT4 ));
INT4 CapwapTestAllCapwapBaseAcNameListTable PROTO ((UINT4 *, tCapwapCapwapBaseAcNameListEntry *, tCapwapIsSetCapwapBaseAcNameListEntry *, INT4 , INT4));
INT4 CapwapTestAllCapwapBaseMacAclTable PROTO ((UINT4 *, tCapwapCapwapBaseMacAclEntry *, tCapwapIsSetCapwapBaseMacAclEntry *, INT4 , INT4));
INT4 CapwapTestAllCapwapBaseWtpProfileTable PROTO ((UINT4 *, tCapwapCapwapBaseWtpProfileEntry *, tCapwapIsSetCapwapBaseWtpProfileEntry *, INT4 , INT4));
INT4 CapwapTestCapwapBaseAcMaxRetransmit PROTO ((UINT4 *,UINT4 ));
INT4 CapwapTestCapwapBaseAcChangeStatePendingTimer PROTO ((UINT4 *,UINT4 ));
INT4 CapwapTestCapwapBaseAcDataCheckTimer PROTO ((UINT4 *,UINT4 ));
INT4 CapwapTestCapwapBaseAcDTLSSessionDeleteTimer PROTO ((UINT4 *,UINT4 ));
INT4 CapwapTestCapwapBaseAcEchoInterval PROTO ((UINT4 *,UINT4 ));
INT4 CapwapTestCapwapBaseAcRetransmitInterval PROTO ((UINT4 *,UINT4 ));
INT4 CapwapTestCapwapBaseAcSilentInterval PROTO ((UINT4 *,UINT4 ));
INT4 CapwapTestCapwapBaseAcWaitDTLSTimer PROTO ((UINT4 *,UINT4 ));
INT4 CapwapTestCapwapBaseAcWaitJoinTimer PROTO ((UINT4 *,UINT4 ));
INT4 CapwapTestCapwapBaseAcEcnSupport PROTO ((UINT4 *,INT4 ));
INT4 CapwapTestCapwapBaseChannelUpDownNotifyEnable PROTO ((UINT4 *,INT4 ));
INT4 CapwapTestCapwapBaseDecryptErrorNotifyEnable PROTO ((UINT4 *,INT4 ));
INT4 CapwapTestCapwapBaseJoinFailureNotifyEnable PROTO ((UINT4 *,INT4 ));
INT4 CapwapTestCapwapBaseImageUpgradeFailureNotifyEnable PROTO ((UINT4 *,INT4 ));
INT4 CapwapTestCapwapBaseConfigMsgErrorNotifyEnable PROTO ((UINT4 *,INT4 ));
INT4 CapwapTestCapwapBaseRadioOperableStatusNotifyEnable PROTO ((UINT4 *,INT4 ));
INT4 CapwapTestCapwapBaseAuthenFailureNotifyEnable PROTO ((UINT4 *,INT4 ));
tCapwapCapwapBaseAcNameListEntry * CapwapGetCapwapBaseAcNameListTable PROTO ((tCapwapCapwapBaseAcNameListEntry *));
tCapwapCapwapBaseMacAclEntry * CapwapGetCapwapBaseMacAclTable PROTO ((tCapwapCapwapBaseMacAclEntry *));
tCapwapCapwapBaseWtpProfileEntry * CapwapGetCapwapBaseWtpProfileTable PROTO ((tCapwapCapwapBaseWtpProfileEntry *));
tCapwapCapwapBaseWtpStateEntry * CapwapGetCapwapBaseWtpStateTable PROTO ((tCapwapCapwapBaseWtpStateEntry *));
tWssIfProfileMacMap * CapwapGetCapwapBaseWtpTable PROTO ((UINT1 *));
tCapwapCapwapBaseWirelessBindingEntry * CapwapGetCapwapBaseWirelessBindingTable PROTO ((tCapwapCapwapBaseWirelessBindingEntry *));
tCapwapCapwapBaseStationEntry * CapwapGetCapwapBaseStationTable PROTO ((tCapwapCapwapBaseStationEntry *));
tCapwapCapwapBaseWtpEventsStatsEntry * CapwapGetCapwapBaseWtpEventsStatsTable PROTO ((tCapwapCapwapBaseWtpEventsStatsEntry *));
tCapwapCapwapBaseRadioEventsStatsEntry * CapwapGetCapwapBaseRadioEventsStatsTable PROTO ((tCapwapCapwapBaseRadioEventsStatsEntry *));
tCapwapCapwapBaseAcNameListEntry * CapwapGetFirstCapwapBaseAcNameListTable PROTO ((VOID));
tCapwapCapwapBaseMacAclEntry * CapwapGetFirstCapwapBaseMacAclTable PROTO ((VOID));
tCapwapCapwapBaseWtpProfileEntry * CapwapGetFirstCapwapBaseWtpProfileTable PROTO ((VOID));
tCapwapCapwapBaseWtpStateEntry * CapwapGetFirstCapwapBaseWtpStateTable PROTO ((VOID));
tWssIfProfileMacMap * CapwapGetFirstCapwapBaseWtpTable PROTO ((VOID));
tCapwapCapwapBaseWirelessBindingEntry * CapwapGetFirstCapwapBaseWirelessBindingTable PROTO ((VOID));
INT1 CapwapGetFirstCapwapBaseStationTable PROTO ((tCapwapCapwapBaseStationEntry *));
tWssIfProfileMacMap * CapwapGetFirstCapwapBaseWtpEventsStatsTable PROTO ((tSNMP_OCTET_STRING_TYPE *));
tWssIfProfileMacMap * CapwapGetFirstCapwapBaseRadioEventsStatsTable PROTO ((tSNMP_OCTET_STRING_TYPE *,UINT4 * ));
tCapwapCapwapBaseAcNameListEntry * CapwapGetNextCapwapBaseAcNameListTable PROTO ((tCapwapCapwapBaseAcNameListEntry *));
tCapwapCapwapBaseMacAclEntry * CapwapGetNextCapwapBaseMacAclTable PROTO ((tCapwapCapwapBaseMacAclEntry *));
tCapwapCapwapBaseWtpProfileEntry * CapwapGetNextCapwapBaseWtpProfileTable PROTO ((tCapwapCapwapBaseWtpProfileEntry *));
tCapwapCapwapBaseWtpStateEntry * CapwapGetNextCapwapBaseWtpStateTable PROTO ((tCapwapCapwapBaseWtpStateEntry *));
tWssIfProfileMacMap * CapwapGetNextCapwapBaseWtpTable PROTO ((tWssIfProfileMacMap *));
tCapwapCapwapBaseWirelessBindingEntry * CapwapGetNextCapwapBaseWirelessBindingTable PROTO ((tCapwapCapwapBaseWirelessBindingEntry *));
INT1  CapwapGetNextCapwapBaseStationTable PROTO ((tCapwapCapwapBaseStationEntry *, tCapwapCapwapBaseStationEntry *));
tWssIfProfileMacMap * CapwapGetNextCapwapBaseWtpEventsStatsTable PROTO ((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1 CapwapGetNextCapwapBaseRadioEventsStatsTable PROTO ((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE *,UINT4 ,UINT4 * )); 
INT4 CapwapBaseAcNameListTableFilterInputs PROTO ((tCapwapCapwapBaseAcNameListEntry *, tCapwapCapwapBaseAcNameListEntry *, tCapwapIsSetCapwapBaseAcNameListEntry *));
INT4 CapwapBaseMacAclTableFilterInputs PROTO ((tCapwapCapwapBaseMacAclEntry *, tCapwapCapwapBaseMacAclEntry *, tCapwapIsSetCapwapBaseMacAclEntry *));
INT4 CapwapBaseWtpProfileTableFilterInputs PROTO ((tCapwapCapwapBaseWtpProfileEntry *, tCapwapCapwapBaseWtpProfileEntry *, tCapwapIsSetCapwapBaseWtpProfileEntry *));
INT4 CapwapUtilUpdateCapwapBaseMacAclTable PROTO ((tCapwapCapwapBaseMacAclEntry *, tCapwapCapwapBaseMacAclEntry *, tCapwapIsSetCapwapBaseMacAclEntry *));
INT4 CapwapUtilUpdateCapwapBaseWtpProfileTable PROTO ((tCapwapCapwapBaseWtpProfileEntry *, tCapwapCapwapBaseWtpProfileEntry *, tCapwapIsSetCapwapBaseWtpProfileEntry *));
INT4 CapwapSetAllCapwapBaseAcNameListTableTrigger PROTO ((tCapwapCapwapBaseAcNameListEntry *, tCapwapIsSetCapwapBaseAcNameListEntry *, INT4));
INT4 CapwapSetAllCapwapBaseMacAclTableTrigger PROTO ((tCapwapCapwapBaseMacAclEntry *, tCapwapIsSetCapwapBaseMacAclEntry *, INT4));
INT4 CapwapSetAllCapwapBaseWtpProfileTableTrigger PROTO ((tCapwapCapwapBaseWtpProfileEntry *, tCapwapIsSetCapwapBaseWtpProfileEntry *, INT4));
INT4 CapwapClearAllAPStats PROTO((tCliHandle CliHandle));
INT4 CapwapShowApConfiguration PROTO((tCliHandle CliHandle, UINT4, UINT1*, UINT4));
INT4 CapwapShowApTraffic PROTO ((tCliHandle CliHandle,UINT4 *));
INT4 CapwapUtilGetReportInterval  PROTO((UINT4, UINT2 *));
INT4 CapwapUtilGetTimeForTicks  PROTO((UINT4, UINT1 *));
INT4 CapwapShowApRunCount (VOID);

INT4
CapwapTestFsCapwapFragReassembleStatus (UINT4 *pu4ErrorCode,
                                        UINT4 u4CapwapBaseWtpProfileId,
                                        UINT1 u1FragReassembleStatus);
INT4
CapwapGetFragReassembleStatus (UINT4 u4CapwapBaseWtpProfileId,
                               UINT1 *pu1FragReassembleStatus);

INT4
CapwapSetFragReassembleStatus (UINT4 u4CapwapBaseWtpProfileId,
                               UINT1 u1FragReassembleStatus);

#endif
