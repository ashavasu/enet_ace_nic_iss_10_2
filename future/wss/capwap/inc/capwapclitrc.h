/********************************************************************
 * Copyright (C) Future Software Limited,1997-98,2001
 * $Id: capwapclitrc.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $ 
 * Description:This file contains procedures and definitions 
 *             used for debugging.
 *******************************************************************/


#ifndef __CAPWAPTRC_H__
#define __CAPWAPTRC_H__

#define  CAPWAP_TRC_FLAG  gCapwapGlobals.u4CapwapTrc
#define  CAPWAP_NAME      "CAPWAP"               

#ifdef CAPWAP_TRACE_WANTED


#if 0
#define  CAPWAP_TRC(x)       CapwapTrcPrint( __FILE__, __LINE__, CapwapTrc x)
#endif
#define  CAPWAP_TRC_FUNC(x)  CapwapTrcPrint( __FILE__, __LINE__, CapwapTrc x)
#define  CAPWAP_TRC_CRIT(x)  CapwapTrcPrint( __FILE__, __LINE__, CapwapTrc x)
#define  CAPWAP_TRC_PKT(x)   CapwapTrcWrite( CapwapTrc x)




#else /* CAPWAP_TRACE_WANTED */
#if 0
#define  CAPWAP_TRC(x) 
#define  CAPWAP_TRC_FUNC(x)
#define  CAPWAP_TRC_CRIT(x)
#define  CAPWAP_TRC_PKT(x)

#endif
#endif /* CAPWAP_TRACE_WANTED */

#if 0
#define  CAPWAP_FN_ENTRY  (0x1 << 9)
#define  CAPWAP_FN_EXIT   (0x1 << 10)
#define  CAPWAP_CLI_TRC   (0x1 << 11)
#define  CAPWAP_MAIN_TRC  (0x1 << 12)
#define  CAPWAP_PKT_TRC   (0x1 << 13)
#define  CAPWAP_QUE_TRC   (0x1 << 14)
#define  CAPWAP_TASK_TRC  (0x1 << 15)
#define  CAPWAP_TMR_TRC   (0x1 << 16)
#define  CAPWAP_UTIL_TRC  (0x1 << 17)
#define  CAPWAP_ALL_TRC CAPWAP_CLI_TRC  |\
                        CAPWAP_MAIN_TRC |\
                        CAPWAP_PKT_TRC  |\
                        CAPWAP_QUE_TRC  |\
                        CAPWAP_TASK_TRC |\
                        CAPWAP_TMR_TRC  |\
                        CAPWAP_UTIL_TRC


#endif
#endif /* _CAPWAPTRC_H */


/*-----------------------------------------------------------------------*/
/*                       End of the file  capwaptrc.h                      */
/*-----------------------------------------------------------------------*/
