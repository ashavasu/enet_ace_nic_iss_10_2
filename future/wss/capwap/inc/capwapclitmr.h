/********************************************************************
 * Copyright (C) Future Software Limited, 2001
 * $Id: capwapclitmr.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $ 
 * Description: This file contains definitions for capwap Timer
 *******************************************************************/

#ifndef __CAPWAPTMR_H__
#define __CAPWAPTMR_H__



/* constants for timer types */
typedef enum {
	CAPWAP_TMR1 =0,
	CAPWAP_MAX_TMRS = 1 
} enCapwapTmrId;

#define     NO_OF_TICKS_PER_SEC             SYS_NUM_OF_TIME_UNITS_IN_A_SEC

typedef struct _CAPWAP_TMR_DESC {
    VOID                (*pTmrExpFunc)(VOID *);
    INT2                i2Offset;
                            /* If this field is -1 then the fn takes no
                             * parameter
                             */
    UINT2               u2Pad;
                            /* Included for 4-byte Alignment
                             */
} tCapwapTmrDesc;

typedef struct _CAPWAP_TIMER {
       tTmrAppTimer    tmrNode;
       enCapwapTmrId     eCapwapTmrId;
} tCapwapTmr;




#endif  /* __CAPWAPTMR_H__  */


/*-----------------------------------------------------------------------*/
/*                       End of the file  capwaptmr.h                      */
/*-----------------------------------------------------------------------*/
