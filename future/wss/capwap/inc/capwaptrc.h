/*****************************************************************************/
/* Copyright (C) 2012 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc.,                                                    */
/*****************************************************************************/
/*    FILE  NAME            : capwaptrc.h                                    */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : CAPWAP module                                  */
/*    MODULE NAME           : CAPWAP module                                  */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains declarations of traces used */
/*                            in CAPWAP Module.                              */
/* $Id: capwaptrc.h,v 1.2 2017/11/24 10:37:02 siva Exp $                      */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                      DESCRIPTION OF CHANGE/              */
/*            MODIFIED BY                FAULT REPORT NO                     */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 MAR 2002 / BridgeTeam   Initial Create.                     */
/*---------------------------------------------------------------------------*/
#ifndef _CAPWAPTRC_H_
#define _CAPWAPTRC_H_

#define  CAPWAP_MOD                ((const char *)"CAPWAP")

/* Trace and debug flags */
#define  CAPWAP_MASK gu4CapwapDebugMask 

#define CAPWAP_PKT_FLOW_IN          1
#define CAPWAP_PKT_FLOW_OUT         2

#define CAPWAP_FN_ENTRY() \
        MOD_FN_ENTRY (CAPWAP_MASK, CAPWAP_ENTRY_TRC,CAPWAP_MOD)

#define CAPWAP_FN_EXIT() MOD_FN_EXIT (CAPWAP_MASK, CAPWAP_EXIT_TRC,CAPWAP_MOD)

#define CAPWAP_PKT_DUMP(mask, pBuf, Length, fmt)                           \
        MOD_PKT_DUMP(DUMP_TRC,mask, CAPWAP_MOD, pBuf, Length, fmt)
#define CAPWAP_TRC(mask, fmt)\
      MOD_TRC(CAPWAP_MASK, mask, CAPWAP_MOD, fmt)
#define CAPWAP_TRC1(mask,fmt,arg1)\
      MOD_TRC_ARG1(CAPWAP_MASK,mask,CAPWAP_MOD,fmt,arg1)
#define CAPWAP_TRC2(mask,fmt,arg1,arg2)\
      MOD_TRC_ARG2(CAPWAP_MASK,mask,CAPWAP_MOD,fmt,arg1,arg2)
#define CAPWAP_TRC3(mask,fmt,arg1,arg2,arg3)\
      MOD_TRC_ARG3(CAPWAP_MASK,mask,CAPWAP_MOD,fmt,arg1,arg2,arg3)
#define CAPWAP_TRC4(mask,fmt,arg1,arg2,arg3,arg4)\
      MOD_TRC_ARG4(CAPWAP_MASK,mask,CAPWAP_MOD,fmt,arg1,arg2,arg3,arg4)
#define CAPWAP_TRC5(mask,fmt,arg1,arg2,arg3,arg4,arg5)\
      MOD_TRC_ARG5(CAPWAP_MASK, mask, CAPWAP_MOD,fmt,arg1,arg2,arg3,arg4,arg5)
#define CAPWAP_TRC6(mask,fmt,arg1,arg2,arg3,arg4,arg5,arg6)\
      MOD_TRC_ARG6(CAPWAP_MASK, mask, CAPWAP_MOD,fmt,arg1,arg2,arg3,arg4,arg5,arg6)
#define CAPWAP_TRC7(mask,fmt,arg1,arg2,arg3,arg4,arg5,arg6,arg7)\
      MOD_TRC_ARG7(CAPWAP_MASK, mask, CAPWAP_MOD,fmt,arg1,arg2,arg3,arg4,arg5,arg6,arg7)
#define CAPWAP_TRC8(mask,fmt,arg1,arg2,arg3,arg4,arg5,arg6,arg7,arg8)\
      MOD_TRC_ARG8(CAPWAP_MASK, mask, CAPWAP_MOD,fmt,arg1,arg2,arg3,arg4,arg5,arg6,arg7,arg8)

#endif
