/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: capwapclidefg.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $ capwapdefg.h 
*
* Description: Macros used to fill the CLI structure and 
              index value in the respective structure.
*********************************************************************/
#include "capwaptrc.h"

#define CAPWAP_CAPWAPBASEACNAMELISTTABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_CAPWAPBASEACNAMELISTTABLE_SIZING_ID]


#define CAPWAP_CAPWAPBASEACNAMELISTTABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_CAPWAPBASEACNAMELISTTABLE_ISSET_SIZING_ID]


#define CAPWAP_CAPWAPBASEMACACLTABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_CAPWAPBASEMACACLTABLE_SIZING_ID]


#define CAPWAP_CAPWAPBASEMACACLTABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_CAPWAPBASEMACACLTABLE_ISSET_SIZING_ID]


#define CAPWAP_CAPWAPBASEWTPPROFILETABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_CAPWAPBASEWTPPROFILETABLE_SIZING_ID]


#define CAPWAP_CAPWAPBASEWTPPROFILETABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_CAPWAPBASEWTPPROFILETABLE_ISSET_SIZING_ID]


#define CAPWAP_CAPWAPBASEWTPSTATETABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_CAPWAPBASEWTPSTATETABLE_SIZING_ID]


#define CAPWAP_CAPWAPBASEWTPSTATETABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_CAPWAPBASEWTPSTATETABLE_ISSET_SIZING_ID]


#define CAPWAP_CAPWAPBASEWTPTABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_CAPWAPBASEWTPTABLE_SIZING_ID]


#define CAPWAP_CAPWAPBASEWTPTABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_CAPWAPBASEWTPTABLE_ISSET_SIZING_ID]


#define CAPWAP_CAPWAPBASEWIRELESSBINDINGTABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_CAPWAPBASEWIRELESSBINDINGTABLE_SIZING_ID]


#define CAPWAP_CAPWAPBASEWIRELESSBINDINGTABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_CAPWAPBASEWIRELESSBINDINGTABLE_ISSET_SIZING_ID]


#define CAPWAP_CAPWAPBASESTATIONTABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_CAPWAPBASESTATIONTABLE_SIZING_ID]


#define CAPWAP_CAPWAPBASESTATIONTABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_CAPWAPBASESTATIONTABLE_ISSET_SIZING_ID]


#define CAPWAP_CAPWAPBASEWTPEVENTSSTATSTABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_CAPWAPBASEWTPEVENTSSTATSTABLE_SIZING_ID]


#define CAPWAP_CAPWAPBASEWTPEVENTSSTATSTABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_CAPWAPBASEWTPEVENTSSTATSTABLE_ISSET_SIZING_ID]


#define CAPWAP_CAPWAPBASERADIOEVENTSSTATSTABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_CAPWAPBASERADIOEVENTSSTATSTABLE_SIZING_ID]


#define CAPWAP_CAPWAPBASERADIOEVENTSSTATSTABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_CAPWAPBASERADIOEVENTSSTATSTABLE_ISSET_SIZING_ID]


#define CAPWAP_FSWTPMODELTABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSWTPMODELTABLE_SIZING_ID]


#define CAPWAP_FSWTPMODELTABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSWTPMODELTABLE_ISSET_SIZING_ID]


#define CAPWAP_FSWTPRADIOTABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSWTPRADIOTABLE_SIZING_ID]


#define CAPWAP_FSWTPRADIOTABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSWTPRADIOTABLE_ISSET_SIZING_ID]


#define CAPWAP_FSCAPWAPWHITELISTTABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAPWAPWHITELISTTABLE_SIZING_ID]


#define CAPWAP_FSCAPWAPWHITELISTTABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAPWAPWHITELISTTABLE_ISSET_SIZING_ID]


#define CAPWAP_FSCAPWAPBLACKLIST_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAPWAPBLACKLIST_SIZING_ID]


#define CAPWAP_FSCAPWAPBLACKLIST_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAPWAPBLACKLIST_ISSET_SIZING_ID]


#define CAPWAP_FSCAPWAPWTPCONFIGTABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAPWAPWTPCONFIGTABLE_SIZING_ID]


#define CAPWAP_FSCAPWAPWTPCONFIGTABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAPWAPWTPCONFIGTABLE_ISSET_SIZING_ID]


#define CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_SIZING_ID]


#define CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAPWAPLINKENCRYPTIONTABLE_ISSET_SIZING_ID]


#define CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_SIZING_ID]


#define CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAWAPDEFAULTWTPPROFILETABLE_ISSET_SIZING_ID]


#define CAPWAP_FSCAPWAPDNSPROFILETABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAPWAPDNSPROFILETABLE_SIZING_ID]


#define CAPWAP_FSCAPWAPDNSPROFILETABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAPWAPDNSPROFILETABLE_ISSET_SIZING_ID]


#define CAPWAP_FSWTPNATIVEVLANIDTABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSWTPNATIVEVLANIDTABLE_SIZING_ID]


#define CAPWAP_FSWTPNATIVEVLANIDTABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSWTPNATIVEVLANIDTABLE_ISSET_SIZING_ID]



#define CAPWAP_FSWTPLOCALROUTINGTABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSWTPLOCALROUTINGTABLE_SIZING_ID]


#define CAPWAP_FSWTPLOCALROUTINGTABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSWTPLOCALROUTINGTABLE_ISSET_SIZING_ID]


#define CAPWAP_FSCAWAPDISCSTATSTABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAWAPDISCSTATSTABLE_SIZING_ID]


#define CAPWAP_FSCAWAPDISCSTATSTABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAWAPDISCSTATSTABLE_ISSET_SIZING_ID]


#define CAPWAP_FSCAWAPJOINSTATSTABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAWAPJOINSTATSTABLE_SIZING_ID]


#define CAPWAP_FSCAWAPJOINSTATSTABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAWAPJOINSTATSTABLE_ISSET_SIZING_ID]


#define CAPWAP_FSCAWAPCONFIGSTATSTABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAWAPCONFIGSTATSTABLE_SIZING_ID]


#define CAPWAP_FSCAWAPCONFIGSTATSTABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAWAPCONFIGSTATSTABLE_ISSET_SIZING_ID]


#define CAPWAP_FSCAWAPRUNSTATSTABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAWAPRUNSTATSTABLE_SIZING_ID]


#define CAPWAP_FSCAWAPRUNSTATSTABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAWAPRUNSTATSTABLE_ISSET_SIZING_ID]


#define CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_SIZING_ID]


#define CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAPWAPWIRELESSBINDINGTABLE_ISSET_SIZING_ID]


#define CAPWAP_FSCAPWAPSTATIONWHITELIST_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAPWAPSTATIONWHITELIST_SIZING_ID]


#define CAPWAP_FSCAPWAPSTATIONWHITELIST_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAPWAPSTATIONWHITELIST_ISSET_SIZING_ID]


#define CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_SIZING_ID]


#define CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAPWAPWTPREBOOTSTATISTICSTABLE_ISSET_SIZING_ID]


#define CAPWAP_FSCAPWAPWTPRADIOSTATISTICSTABLE_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAPWAPWTPRADIOSTATISTICSTABLE_SIZING_ID]


#define CAPWAP_FSCAPWAPWTPRADIOSTATISTICSTABLE_ISSET_POOLID   CAPWAPMemPoolIds[MAX_CAPWAP_FSCAPWAPWTPRADIOSTATISTICSTABLE_ISSET_SIZING_ID]
/* Macro used to fill the CLI structure for FsWtpModelEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSWTPMODELTABLE_ARGS(pCapwapFsWtpModelEntry,\
   pCapwapIsSetFsWtpModelEntry,\
   pau1FsCapwapWtpModelNumber,\
   pau1FsCapwapWtpModelNumberLen,\
   pau1FsNoOfRadio,\
   pau1FsCapwapWtpMacType,\
   pau1FsCapwapWtpTunnelMode,\
   pau1FsCapwapWtpTunnelModeLen,\
   pau1FsCapwapSwVersion,\
   pau1FsCapwapSwVersionLen,\
   pau1FsCapwapImageName,\
   pau1FsCapwapImageNameLen,\
   pau1FsCapwapQosProfileName,\
   pau1FsCapwapQosProfileNameLen,\
   pau1FsMaxStations,\
   pau1FsWtpModelRowStatus)\
  {\
  if (pau1FsCapwapWtpModelNumber != NULL)\
  {\
   MEMCPY (pCapwapFsWtpModelEntry->MibObject.au1FsCapwapWtpModelNumber, pau1FsCapwapWtpModelNumber, *(INT4 *)pau1FsCapwapWtpModelNumberLen);\
   pCapwapFsWtpModelEntry->MibObject.i4FsCapwapWtpModelNumberLen = *(INT4 *)pau1FsCapwapWtpModelNumberLen;\
   pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpModelNumber = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpModelNumber = OSIX_FALSE;\
  }\
  if (pau1FsNoOfRadio != NULL)\
  {\
   pCapwapFsWtpModelEntry->MibObject.u4FsNoOfRadio = *(UINT4 *) (pau1FsNoOfRadio);\
   pCapwapIsSetFsWtpModelEntry->bFsNoOfRadio = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsWtpModelEntry->bFsNoOfRadio = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWtpMacType != NULL)\
  {\
   pCapwapFsWtpModelEntry->MibObject.i4FsCapwapWtpMacType = *(INT4 *) (pau1FsCapwapWtpMacType);\
   pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpMacType = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpMacType = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWtpTunnelMode != NULL)\
  {\
   MEMCPY (pCapwapFsWtpModelEntry->MibObject.au1FsCapwapWtpTunnelMode, pau1FsCapwapWtpTunnelMode, *(INT4 *)pau1FsCapwapWtpTunnelModeLen);\
   pCapwapFsWtpModelEntry->MibObject.i4FsCapwapWtpTunnelModeLen = *(INT4 *)pau1FsCapwapWtpTunnelModeLen;\
   pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpTunnelMode = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsWtpModelEntry->bFsCapwapWtpTunnelMode = OSIX_FALSE;\
  }\
  if (pau1FsCapwapImageName != NULL)\
  {\
   MEMCPY (pCapwapFsWtpModelEntry->MibObject.au1FsCapwapImageName, pau1FsCapwapImageName, *(INT4 *)pau1FsCapwapImageNameLen);\
   pCapwapFsWtpModelEntry->MibObject.i4FsCapwapImageNameLen = *(INT4 *)pau1FsCapwapImageNameLen;\
   pCapwapIsSetFsWtpModelEntry->bFsCapwapImageName = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsWtpModelEntry->bFsCapwapImageName = OSIX_FALSE;\
  }\
  if (pau1FsCapwapQosProfileName != NULL)\
  {\
   MEMCPY (pCapwapFsWtpModelEntry->MibObject.au1FsCapwapQosProfileName, pau1FsCapwapQosProfileName, *(INT4 *)pau1FsCapwapQosProfileNameLen);\
   pCapwapFsWtpModelEntry->MibObject.i4FsCapwapQosProfileNameLen = *(INT4 *)pau1FsCapwapQosProfileNameLen;\
   pCapwapIsSetFsWtpModelEntry->bFsCapwapQosProfileName = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsWtpModelEntry->bFsCapwapQosProfileName = OSIX_FALSE;\
  }\
  if (pau1FsMaxStations != NULL)\
  {\
   pCapwapFsWtpModelEntry->MibObject.i4FsMaxStations = *(INT4 *) (pau1FsMaxStations);\
   pCapwapIsSetFsWtpModelEntry->bFsMaxStations = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsWtpModelEntry->bFsMaxStations = OSIX_FALSE;\
  }\
  if (pau1FsWtpModelRowStatus != NULL)\
  {\
   pCapwapFsWtpModelEntry->MibObject.i4FsWtpModelRowStatus = *(INT4 *) (pau1FsWtpModelRowStatus);\
   pCapwapIsSetFsWtpModelEntry->bFsWtpModelRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsWtpModelEntry->bFsWtpModelRowStatus = OSIX_FALSE;\
  }\
  }

/* Macro used to fill the CLI structure for FsWtpRadioEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSWTPRADIOTABLE_ARGS(pCapwapFsWtpRadioEntry,\
   pCapwapIsSetFsWtpRadioEntry,\
   pau1FsNumOfRadio,\
   pau1FsWtpRadioType,\
   pau1FsRadioAdminStatus,\
   pau1FsMaxSSIDSupported,\
   pau1FsWtpRadioRowStatus,\
   pau1FsCapwapWtpModelNumber,\
   pau1FsCapwapWtpModelNumberLen)\
  {\
  if (pau1FsNumOfRadio != NULL)\
  {\
   pCapwapFsWtpRadioEntry->MibObject.u4FsNumOfRadio = *(UINT4 *) (pau1FsNumOfRadio);\
   pCapwapIsSetFsWtpRadioEntry->bFsNumOfRadio = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsWtpRadioEntry->bFsNumOfRadio = OSIX_FALSE;\
  }\
  if (pau1FsWtpRadioType != NULL)\
  {\
   pCapwapFsWtpRadioEntry->MibObject.u4FsWtpRadioType = *(UINT4 *) (pau1FsWtpRadioType);\
   pCapwapIsSetFsWtpRadioEntry->bFsWtpRadioType = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsWtpRadioEntry->bFsWtpRadioType = OSIX_FALSE;\
  }\
  if (pau1FsRadioAdminStatus != NULL)\
  {\
   pCapwapFsWtpRadioEntry->MibObject.i4FsRadioAdminStatus = *(INT4 *) (pau1FsRadioAdminStatus);\
   pCapwapIsSetFsWtpRadioEntry->bFsRadioAdminStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsWtpRadioEntry->bFsRadioAdminStatus = OSIX_FALSE;\
  }\
  if (pau1FsMaxSSIDSupported != NULL)\
  {\
   pCapwapFsWtpRadioEntry->MibObject.i4FsMaxSSIDSupported = *(INT4 *) (pau1FsMaxSSIDSupported);\
   pCapwapIsSetFsWtpRadioEntry->bFsMaxSSIDSupported = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsWtpRadioEntry->bFsMaxSSIDSupported = OSIX_FALSE;\
  }\
  if (pau1FsWtpRadioRowStatus != NULL)\
  {\
   pCapwapFsWtpRadioEntry->MibObject.i4FsWtpRadioRowStatus = *(INT4 *) (pau1FsWtpRadioRowStatus);\
   pCapwapIsSetFsWtpRadioEntry->bFsWtpRadioRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsWtpRadioEntry->bFsWtpRadioRowStatus = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWtpModelNumber != NULL)\
  {\
   MEMCPY (pCapwapFsWtpRadioEntry->MibObject.au1FsCapwapWtpModelNumber, pau1FsCapwapWtpModelNumber, *(INT4 *)pau1FsCapwapWtpModelNumberLen);\
   pCapwapFsWtpRadioEntry->MibObject.i4FsCapwapWtpModelNumberLen = *(INT4 *)pau1FsCapwapWtpModelNumberLen;\
   pCapwapIsSetFsWtpRadioEntry->bFsCapwapWtpModelNumber = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsWtpRadioEntry->bFsCapwapWtpModelNumber = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsCapwapWhiteListEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAPWAPWHITELISTTABLE_ARGS(pCapwapFsCapwapWhiteListEntry,\
   pCapwapIsSetFsCapwapWhiteListEntry,\
   pau1FsCapwapWhiteListId,\
   pau1FsCapwapWhiteListWtpBaseMac,\
   pau1FsCapwapWhiteListWtpBaseMacLen,\
   pau1FsCapwapWhiteListRowStatus)\
  {\
  if (pau1FsCapwapWhiteListId != NULL)\
  {\
   pCapwapFsCapwapWhiteListEntry->MibObject.u4FsCapwapWhiteListId = *(UINT4 *) (pau1FsCapwapWhiteListId);\
   pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListId = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListId = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWhiteListWtpBaseMac != NULL)\
  {\
   MEMCPY (pCapwapFsCapwapWhiteListEntry->MibObject.au1FsCapwapWhiteListWtpBaseMac, pau1FsCapwapWhiteListWtpBaseMac, *(INT4 *)pau1FsCapwapWhiteListWtpBaseMacLen);\
   pCapwapFsCapwapWhiteListEntry->MibObject.i4FsCapwapWhiteListWtpBaseMacLen\
            = *(INT4 *) (pau1FsCapwapWhiteListWtpBaseMacLen);\
   pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListWtpBaseMac = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListWtpBaseMac = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWhiteListRowStatus != NULL)\
  {\
   pCapwapFsCapwapWhiteListEntry->MibObject.i4FsCapwapWhiteListRowStatus = *(INT4 *) (pau1FsCapwapWhiteListRowStatus);\
   pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWhiteListEntry->bFsCapwapWhiteListRowStatus = OSIX_FALSE;\
  }\
  }

/* Macro used to fill the CLI structure for FsCapwapBlackListEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAPWAPBLACKLIST_ARGS(pCapwapFsCapwapBlackListEntry,\
   pCapwapIsSetFsCapwapBlackListEntry,\
   pau1FsCapwapBlackListId,\
   pau1FsCapwapBlackListWtpBaseMac,\
   pau1FsCapwapBlackListWtpBaseMacLen,\
   pau1FsCapwapBlackListRowStatus)\
  {\
  if (pau1FsCapwapBlackListId != NULL)\
  {\
   pCapwapFsCapwapBlackListEntry->MibObject.u4FsCapwapBlackListId = *(UINT4 *) (pau1FsCapwapBlackListId);\
   pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListId = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListId = OSIX_FALSE;\
  }\
  if (pau1FsCapwapBlackListWtpBaseMac != NULL)\
  {\
   MEMCPY (pCapwapFsCapwapBlackListEntry->MibObject.au1FsCapwapBlackListWtpBaseMac, pau1FsCapwapBlackListWtpBaseMac, *(INT4 *)pau1FsCapwapBlackListWtpBaseMacLen);\
   pCapwapFsCapwapBlackListEntry->MibObject.i4FsCapwapBlackListWtpBaseMacLen\
            = *(INT4 *) (pau1FsCapwapBlackListWtpBaseMacLen);\
   pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListWtpBaseMac = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListWtpBaseMac = OSIX_FALSE;\
  }\
  if (pau1FsCapwapBlackListRowStatus != NULL)\
  {\
   pCapwapFsCapwapBlackListEntry->MibObject.i4FsCapwapBlackListRowStatus = *(INT4 *) (pau1FsCapwapBlackListRowStatus);\
   pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapBlackListEntry->bFsCapwapBlackListRowStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsCapwapWtpConfigEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAPWAPWTPCONFIGTABLE_ARGS(pCapwapFsCapwapWtpConfigEntry,\
   pCapwapIsSetFsCapwapWtpConfigEntry,\
   pau1FsCapwapWtpReset,\
   pau1FsCapwapClearConfig,\
   pau1FsWtpDiscoveryType,\
   pau1FsWtpCountryString,\
   pau1FsWtpCountryStringLen,\
            pau1FsWtpCrashDumpFileName,\
            pau1FsWtpCrashDumpFileNameLen,\
            pau1FsWtpMemoryDumpFileName,\
            pau1FsWtpMemoryDumpFileNameLen,\
            pau1FsCapwapClearApStats,\
   pau1FsCapwapWtpConfigRowStatus,\
            pu4FsWtpDeleteOperation,\
   pau1CapwapBaseWtpProfileId,\
            pu4FsWlcStaticIpAddress)\
  {\
  if (pau1FsCapwapWtpReset != NULL)\
  {\
   pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapWtpReset = *(INT4 *) (pau1FsCapwapWtpReset);\
   pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapWtpReset = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapWtpReset = OSIX_FALSE;\
  }\
  if (pau1FsCapwapClearConfig != NULL)\
  {\
   pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapClearConfig = *(INT4 *) (pau1FsCapwapClearConfig);\
   pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapClearConfig = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapClearConfig = OSIX_FALSE;\
  }\
  if (pau1FsWtpDiscoveryType != NULL)\
  {\
   pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsWtpDiscoveryType = *(INT4 *) (pau1FsWtpDiscoveryType);\
   pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpDiscoveryType = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpDiscoveryType = OSIX_FALSE;\
  }\
  if (pau1FsWtpCountryString != NULL)\
  {\
   MEMCPY (pCapwapFsCapwapWtpConfigEntry->MibObject.au1FsWtpCountryString, pau1FsWtpCountryString, *(INT4 *)pau1FsWtpCountryStringLen);\
   pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsWtpCountryStringLen = *(INT4 *)pau1FsWtpCountryStringLen;\
   pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCountryString = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCountryString = OSIX_FALSE;\
  }\
        if (pau1FsWtpCrashDumpFileName != NULL)\
        {\
            MEMCPY (pCapwapFsCapwapWtpConfigEntry->MibObject.au1FsWtpCrashDumpFileName, pau1FsWtpCrashDumpFileName, *(INT4 *)pau1FsWtpCrashDumpFileNameLen);\
            pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsWtpCrashDumpFileNameLen = *(INT4 *)pau1FsWtpCrashDumpFileNameLen;\
            pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCrashDumpFileName = OSIX_TRUE;\
        }\
        else\
        {\
            pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpCrashDumpFileName = OSIX_FALSE;\
        }\
        if (pau1FsWtpMemoryDumpFileName != NULL)\
        {\
            MEMCPY (pCapwapFsCapwapWtpConfigEntry->MibObject.au1FsWtpMemoryDumpFileName, pau1FsWtpMemoryDumpFileName, *(INT4 *)pau1FsWtpMemoryDumpFileNameLen);\
            pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsWtpMemoryDumpFileNameLen = *(INT4 *)pau1FsWtpMemoryDumpFileNameLen;\
            pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpMemoryDumpFileName = OSIX_TRUE;\
        }\
        else\
        {\
            pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpMemoryDumpFileName = OSIX_FALSE;\
        }\
        if (pau1FsCapwapClearApStats != NULL)\
         {\
             pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapClearApStats = *(INT4 *) (pau1FsCapwapClearApStats);\
              pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapClearApStats = OSIX_TRUE;\
         }\
        else\
        {\
             pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapClearApStats = OSIX_FALSE;\
        }\
  if (pau1FsCapwapWtpConfigRowStatus != NULL)\
  {\
   pCapwapFsCapwapWtpConfigEntry->MibObject.i4FsCapwapWtpConfigRowStatus = *(INT4 *) (pau1FsCapwapWtpConfigRowStatus);\
   pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapWtpConfigRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpConfigEntry->bFsCapwapWtpConfigRowStatus = OSIX_FALSE;\
  }\
  if (pu4FsWtpDeleteOperation != NULL)\
  {\
   pCapwapFsCapwapWtpConfigEntry->MibObject.u4FsWtpDeleteOperation = *(UINT4 *) (pu4FsWtpDeleteOperation);\
   pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpDeleteOperation = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpConfigEntry->bFsWtpDeleteOperation = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapFsCapwapWtpConfigEntry->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
   pCapwapIsSetFsCapwapWtpConfigEntry->bCapwapBaseWtpProfileId = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpConfigEntry->bCapwapBaseWtpProfileId = OSIX_FALSE;\
  }\
         if(pu4FsWlcStaticIpAddress != NULL)\
         {\
             pCapwapFsCapwapWtpConfigEntry->MibObject.u4FsWlcStaticIpAddress = *(pu4FsWlcStaticIpAddress);\
             pCapwapIsSetFsCapwapWtpConfigEntry->bFsWlcStaticIpAddress = OSIX_TRUE;\
         }\
         else\
         {\
             pCapwapIsSetFsCapwapWtpConfigEntry->bFsWlcStaticIpAddress = OSIX_FALSE;\
         }\
  }
/* Macro used to fill the CLI structure for FsCapwapLinkEncryptionEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAPWAPLINKENCRYPTIONTABLE_ARGS(pCapwapFsCapwapLinkEncryptionEntry,\
   pCapwapIsSetFsCapwapLinkEncryptionEntry,\
   pau1FsCapwapEncryptChannel,\
   pau1FsCapwapEncryptChannelStatus,\
   pau1FsCapwapEncryptChannelRowStatus,\
   pau1CapwapBaseWtpProfileId)\
  {\
  if (pau1FsCapwapEncryptChannel != NULL)\
  {\
   pCapwapFsCapwapLinkEncryptionEntry->MibObject.i4FsCapwapEncryptChannel = *(INT4 *) (pau1FsCapwapEncryptChannel);\
   pCapwapIsSetFsCapwapLinkEncryptionEntry->bFsCapwapEncryptChannel = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapLinkEncryptionEntry->bFsCapwapEncryptChannel = OSIX_FALSE;\
  }\
  if (pau1FsCapwapEncryptChannelStatus != NULL)\
  {\
   pCapwapFsCapwapLinkEncryptionEntry->MibObject.i4FsCapwapEncryptChannelStatus = *(INT4 *) (pau1FsCapwapEncryptChannelStatus);\
   pCapwapIsSetFsCapwapLinkEncryptionEntry->bFsCapwapEncryptChannelStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapLinkEncryptionEntry->bFsCapwapEncryptChannelStatus = OSIX_FALSE;\
  }\
  if (pau1FsCapwapEncryptChannelRowStatus != NULL)\
  {\
   pCapwapFsCapwapLinkEncryptionEntry->MibObject.i4FsCapwapEncryptChannelRowStatus = *(INT4 *) (pau1FsCapwapEncryptChannelRowStatus);\
   pCapwapIsSetFsCapwapLinkEncryptionEntry->bFsCapwapEncryptChannelRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapLinkEncryptionEntry->bFsCapwapEncryptChannelRowStatus = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileId != NULL)\
         {\
             pCapwapFsCapwapLinkEncryptionEntry->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
              pCapwapIsSetFsCapwapLinkEncryptionEntry->bCapwapBaseWtpProfileId = OSIX_TRUE;\
         }\
        else\
         {\
             pCapwapIsSetFsCapwapLinkEncryptionEntry->bCapwapBaseWtpProfileId = OSIX_FALSE;\
         }\
  }
/* Macro used to fill the CLI structure for FsCapwapDefaultWtpProfileEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAWAPDEFAULTWTPPROFILETABLE_ARGS(pCapwapFsCapwapDefaultWtpProfileEntry,\
   pCapwapIsSetFsCapwapDefaultWtpProfileEntry,\
   pau1FsCapwapDefaultWtpProfileModelNumber,\
   pau1FsCapwapDefaultWtpProfileModelNumberLen,\
   pau1FsCapwapDefaultWtpProfileWtpFallbackEnable,\
   pau1FsCapwapDefaultWtpProfileWtpEchoInterval,\
   pau1FsCapwapDefaultWtpProfileWtpIdleTimeout,\
   pau1FsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval,\
   pau1FsCapwapDefaultWtpProfileWtpReportInterval,\
   pau1FsCapwapDefaultWtpProfileWtpStatisticsTimer,\
   pau1FsCapwapDefaultWtpProfileWtpEcnSupport,\
   pau1FsCapwapDefaultWtpProfileRowStatus)\
  {\
  if (pau1FsCapwapDefaultWtpProfileModelNumber != NULL)\
  {\
   MEMCPY (pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.au1FsCapwapDefaultWtpProfileModelNumber, pau1FsCapwapDefaultWtpProfileModelNumber, *(INT4 *)pau1FsCapwapDefaultWtpProfileModelNumberLen);\
   pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.i4FsCapwapDefaultWtpProfileModelNumberLen = *(INT4 *)pau1FsCapwapDefaultWtpProfileModelNumberLen;\
   pCapwapIsSetFsCapwapDefaultWtpProfileEntry->bFsCapwapDefaultWtpProfileModelNumber = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapDefaultWtpProfileEntry->bFsCapwapDefaultWtpProfileModelNumber = OSIX_FALSE;\
  }\
  if (pau1FsCapwapDefaultWtpProfileWtpFallbackEnable != NULL)\
  {\
   pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.i4FsCapwapDefaultWtpProfileWtpFallbackEnable = *(INT4 *) (pau1FsCapwapDefaultWtpProfileWtpFallbackEnable);\
   pCapwapIsSetFsCapwapDefaultWtpProfileEntry->bFsCapwapDefaultWtpProfileWtpFallbackEnable = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapDefaultWtpProfileEntry->bFsCapwapDefaultWtpProfileWtpFallbackEnable = OSIX_FALSE;\
  }\
  if (pau1FsCapwapDefaultWtpProfileWtpEchoInterval != NULL)\
  {\
   pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.u4FsCapwapDefaultWtpProfileWtpEchoInterval = *(UINT4 *) (pau1FsCapwapDefaultWtpProfileWtpEchoInterval);\
   pCapwapIsSetFsCapwapDefaultWtpProfileEntry->bFsCapwapDefaultWtpProfileWtpEchoInterval = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapDefaultWtpProfileEntry->bFsCapwapDefaultWtpProfileWtpEchoInterval = OSIX_FALSE;\
  }\
  if (pau1FsCapwapDefaultWtpProfileWtpIdleTimeout != NULL)\
  {\
   pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.u4FsCapwapDefaultWtpProfileWtpIdleTimeout = *(UINT4 *) (pau1FsCapwapDefaultWtpProfileWtpIdleTimeout);\
   pCapwapIsSetFsCapwapDefaultWtpProfileEntry->bFsCapwapDefaultWtpProfileWtpIdleTimeout = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapDefaultWtpProfileEntry->bFsCapwapDefaultWtpProfileWtpIdleTimeout = OSIX_FALSE;\
  }\
  if (pau1FsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval != NULL)\
  {\
   pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.u4FsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval = *(UINT4 *) (pau1FsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval);\
   pCapwapIsSetFsCapwapDefaultWtpProfileEntry->bFsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapDefaultWtpProfileEntry->bFsCapwapDefaultWtpProfileWtpMaxDiscoveryInterval = OSIX_FALSE;\
  }\
  if (pau1FsCapwapDefaultWtpProfileWtpReportInterval != NULL)\
  {\
   pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.u4FsCapwapDefaultWtpProfileWtpReportInterval = *(UINT4 *) (pau1FsCapwapDefaultWtpProfileWtpReportInterval);\
   pCapwapIsSetFsCapwapDefaultWtpProfileEntry->bFsCapwapDefaultWtpProfileWtpReportInterval = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapDefaultWtpProfileEntry->bFsCapwapDefaultWtpProfileWtpReportInterval = OSIX_FALSE;\
  }\
  if (pau1FsCapwapDefaultWtpProfileWtpStatisticsTimer != NULL)\
  {\
   pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.u4FsCapwapDefaultWtpProfileWtpStatisticsTimer = *(UINT4 *) (pau1FsCapwapDefaultWtpProfileWtpStatisticsTimer);\
   pCapwapIsSetFsCapwapDefaultWtpProfileEntry->bFsCapwapDefaultWtpProfileWtpStatisticsTimer = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapDefaultWtpProfileEntry->bFsCapwapDefaultWtpProfileWtpStatisticsTimer = OSIX_FALSE;\
  }\
  if (pau1FsCapwapDefaultWtpProfileWtpEcnSupport != NULL)\
  {\
   pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.i4FsCapwapDefaultWtpProfileWtpEcnSupport = *(INT4 *) (pau1FsCapwapDefaultWtpProfileWtpEcnSupport);\
   pCapwapIsSetFsCapwapDefaultWtpProfileEntry->bFsCapwapDefaultWtpProfileWtpEcnSupport = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapDefaultWtpProfileEntry->bFsCapwapDefaultWtpProfileWtpEcnSupport = OSIX_FALSE;\
  }\
  if (pau1FsCapwapDefaultWtpProfileRowStatus != NULL)\
  {\
   pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.i4FsCapwapDefaultWtpProfileRowStatus = *(INT4 *) (pau1FsCapwapDefaultWtpProfileRowStatus);\
   pCapwapIsSetFsCapwapDefaultWtpProfileEntry->bFsCapwapDefaultWtpProfileRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapDefaultWtpProfileEntry->bFsCapwapDefaultWtpProfileRowStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsCapwapDnsProfileEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAPWAPDNSPROFILETABLE_ARGS(pCapwapFsCapwapDnsProfileEntry,\
   pCapwapIsSetFsCapwapDnsProfileEntry,\
   pau1FsCapwapDnsServerIp,\
   pau1FsCapwapDnsServerIpLen,\
   pau1FsCapwapDnsDomainName,\
   pau1FsCapwapDnsDomainNameLen,\
   pau1FsCapwapDnsProfileRowStatus,\
   pau1CapwapBaseWtpProfileId)\
  {\
  if (pau1FsCapwapDnsServerIp != NULL)\
  {\
   MEMCPY (pCapwapFsCapwapDnsProfileEntry->MibObject.au1FsCapwapDnsServerIp, pau1FsCapwapDnsServerIp, *(INT4 *)pau1FsCapwapDnsServerIpLen);\
   pCapwapFsCapwapDnsProfileEntry->MibObject.i4FsCapwapDnsServerIpLen = *(INT4 *)pau1FsCapwapDnsServerIpLen;\
   pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsServerIp = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsServerIp = OSIX_FALSE;\
  }\
  if (pau1FsCapwapDnsDomainName != NULL)\
  {\
   MEMCPY (pCapwapFsCapwapDnsProfileEntry->MibObject.au1FsCapwapDnsDomainName, pau1FsCapwapDnsDomainName, *(INT4 *)pau1FsCapwapDnsDomainNameLen);\
   pCapwapFsCapwapDnsProfileEntry->MibObject.i4FsCapwapDnsDomainNameLen = *(INT4 *)pau1FsCapwapDnsDomainNameLen;\
   pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsDomainName = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsDomainName = OSIX_FALSE;\
  }\
  if (pau1FsCapwapDnsProfileRowStatus != NULL)\
  {\
   pCapwapFsCapwapDnsProfileEntry->MibObject.i4FsCapwapDnsProfileRowStatus = *(INT4 *) (pau1FsCapwapDnsProfileRowStatus);\
   pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsProfileRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapDnsProfileEntry->bFsCapwapDnsProfileRowStatus = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapFsCapwapDnsProfileEntry->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
   pCapwapIsSetFsCapwapDnsProfileEntry->bCapwapBaseWtpProfileId = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapDnsProfileEntry->bCapwapBaseWtpProfileId = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsWtpNativeVlanIdTable 
 using the input given in def file */

#define  CAPWAP_FILL_FSWTPNATIVEVLANIDTABLE_ARGS(pCapwapFsWtpNativeVlanIdTable,\
   pCapwapIsSetFsWtpNativeVlanIdTable,\
   pau1FsWtpNativeVlanId,\
   pau1FsWtpNativeVlanIdRowStatus,\
   pau1CapwapBaseWtpProfileId)\
  {\
  if (pau1FsWtpNativeVlanId != NULL)\
  {\
   pCapwapFsWtpNativeVlanIdTable->MibObject.i4FsWtpNativeVlanId = *(INT4 *) (pau1FsWtpNativeVlanId);\
   pCapwapIsSetFsWtpNativeVlanIdTable->bFsWtpNativeVlanId = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsWtpNativeVlanIdTable->bFsWtpNativeVlanId = OSIX_FALSE;\
  }\
  if (pau1FsWtpNativeVlanIdRowStatus != NULL)\
  {\
   pCapwapFsWtpNativeVlanIdTable->MibObject.i4FsWtpNativeVlanIdRowStatus = *(INT4 *) (pau1FsWtpNativeVlanIdRowStatus);\
   pCapwapIsSetFsWtpNativeVlanIdTable->bFsWtpNativeVlanIdRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsWtpNativeVlanIdTable->bFsWtpNativeVlanIdRowStatus = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapFsWtpNativeVlanIdTable->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
   pCapwapIsSetFsWtpNativeVlanIdTable->bCapwapBaseWtpProfileId = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsWtpNativeVlanIdTable->bCapwapBaseWtpProfileId = OSIX_FALSE;\
  }\
  }

/* Macro used to fill the CLI structure for FsWtpLocalRoutingTable 
 using the input given in def file */

#define  CAPWAP_FILL_FSWTPLOCALROUTINGTABLE_ARGS(pCapwapFsWtpLocalRoutingTable,\
   pCapwapIsSetFsWtpLocalRoutingTable,\
   pau1FsWtpLocalRouting,\
   pau1FsWtpLocalRoutingRowStatus,\
   pau1CapwapBaseWtpProfileId)\
  {\
  if (pau1FsWtpLocalRouting != NULL)\
  {\
   pCapwapFsWtpLocalRoutingTable->MibObject.i4FsWtpLocalRouting = *(INT4 *) (pau1FsWtpLocalRouting);\
   pCapwapIsSetFsWtpLocalRoutingTable->bFsWtpLocalRouting = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsWtpLocalRoutingTable->bFsWtpLocalRouting = OSIX_FALSE;\
  }\
  if (pau1FsWtpLocalRoutingRowStatus != NULL)\
  {\
   pCapwapFsWtpLocalRoutingTable->MibObject.i4FsWtpLocalRoutingRowStatus = *(INT4 *) (pau1FsWtpLocalRoutingRowStatus);\
   pCapwapIsSetFsWtpLocalRoutingTable->bFsWtpLocalRoutingRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsWtpLocalRoutingTable->bFsWtpLocalRoutingRowStatus = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapFsWtpLocalRoutingTable->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
   pCapwapIsSetFsWtpLocalRoutingTable->bCapwapBaseWtpProfileId = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsWtpLocalRoutingTable->bCapwapBaseWtpProfileId = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsCawapDiscStatsEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAWAPDISCSTATSTABLE_ARGS(pCapwapFsCawapDiscStatsEntry,\
   pCapwapIsSetFsCawapDiscStatsEntry,\
   pau1FsCapwapDiscStatsRowStatus,\
   pau1CapwapBaseWtpProfileId)\
  {\
  if (pau1FsCapwapDiscStatsRowStatus != NULL)\
  {\
   pCapwapFsCawapDiscStatsEntry->MibObject.i4FsCapwapDiscStatsRowStatus = *(INT4 *) (pau1FsCapwapDiscStatsRowStatus);\
   pCapwapIsSetFsCawapDiscStatsEntry->bFsCapwapDiscStatsRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCawapDiscStatsEntry->bFsCapwapDiscStatsRowStatus = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapFsCawapDiscStatsEntry->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
   pCapwapIsSetFsCawapDiscStatsEntry->bCapwapBaseWtpProfileId = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCawapDiscStatsEntry->bCapwapBaseWtpProfileId = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsCawapJoinStatsEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAWAPJOINSTATSTABLE_ARGS(pCapwapFsCawapJoinStatsEntry,\
   pCapwapIsSetFsCawapJoinStatsEntry,\
   pau1FsCapwapJoinStatsRowStatus,\
   pau1CapwapBaseWtpProfileId)\
  {\
  if (pau1FsCapwapJoinStatsRowStatus != NULL)\
  {\
   pCapwapFsCawapJoinStatsEntry->MibObject.i4FsCapwapJoinStatsRowStatus = *(INT4 *) (pau1FsCapwapJoinStatsRowStatus);\
   pCapwapIsSetFsCawapJoinStatsEntry->bFsCapwapJoinStatsRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCawapJoinStatsEntry->bFsCapwapJoinStatsRowStatus = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapFsCawapJoinStatsEntry->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
   pCapwapIsSetFsCawapJoinStatsEntry->bCapwapBaseWtpProfileId = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCawapJoinStatsEntry->bCapwapBaseWtpProfileId = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsCawapConfigStatsEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAWAPCONFIGSTATSTABLE_ARGS(pCapwapFsCawapConfigStatsEntry,\
   pCapwapIsSetFsCawapConfigStatsEntry,\
   pau1FsCapwapConfigStatsRowStatus,\
   pau1CapwapBaseWtpProfileId)\
  {\
  if (pau1FsCapwapConfigStatsRowStatus != NULL)\
  {\
   pCapwapFsCawapConfigStatsEntry->MibObject.i4FsCapwapConfigStatsRowStatus = *(INT4 *) (pau1FsCapwapConfigStatsRowStatus);\
   pCapwapIsSetFsCawapConfigStatsEntry->bFsCapwapConfigStatsRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCawapConfigStatsEntry->bFsCapwapConfigStatsRowStatus = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapFsCawapConfigStatsEntry->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
   pCapwapIsSetFsCawapConfigStatsEntry->bCapwapBaseWtpProfileId = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCawapConfigStatsEntry->bCapwapBaseWtpProfileId = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsCawapRunStatsEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAWAPRUNSTATSTABLE_ARGS(pCapwapFsCawapRunStatsEntry,\
   pCapwapIsSetFsCawapRunStatsEntry,\
   pau1FsCapwapRunStatsRowStatus,\
   pau1CapwapBaseWtpProfileId)\
  {\
  if (pau1FsCapwapRunStatsRowStatus != NULL)\
  {\
   pCapwapFsCawapRunStatsEntry->MibObject.i4FsCapwapRunStatsRowStatus = *(INT4 *) (pau1FsCapwapRunStatsRowStatus);\
   pCapwapIsSetFsCawapRunStatsEntry->bFsCapwapRunStatsRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCawapRunStatsEntry->bFsCapwapRunStatsRowStatus = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapFsCawapRunStatsEntry->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
   pCapwapIsSetFsCawapRunStatsEntry->bCapwapBaseWtpProfileId = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCawapRunStatsEntry->bCapwapBaseWtpProfileId = OSIX_FALSE;\
  }\
  }

/* Macro used to fill the CLI structure for FsCapwapWirelessBindingEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAPWAPWIRELESSBINDINGTABLE_ARGS(pCapwapFsCapwapWirelessBindingEntry,\
   pCapwapIsSetFsCapwapWirelessBindingEntry,\
   pau1FsCapwapWirelessBindingVirtualRadioIfIndex,\
   pau1FsCapwapWirelessBindingType,\
   pau1FsCapwapWirelessBindingRowStatus,\
   pau1CapwapBaseWtpProfileId,\
   pau1CapwapBaseWirelessBindingRadioId)\
  {\
  if (pau1FsCapwapWirelessBindingVirtualRadioIfIndex != NULL)\
  {\
   pCapwapFsCapwapWirelessBindingEntry->MibObject.i4FsCapwapWirelessBindingVirtualRadioIfIndex = *(INT4 *) (pau1FsCapwapWirelessBindingVirtualRadioIfIndex);\
   pCapwapIsSetFsCapwapWirelessBindingEntry->bFsCapwapWirelessBindingVirtualRadioIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWirelessBindingEntry->bFsCapwapWirelessBindingVirtualRadioIfIndex = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWirelessBindingType != NULL)\
  {\
   pCapwapFsCapwapWirelessBindingEntry->MibObject.i4FsCapwapWirelessBindingType = *(INT4 *) (pau1FsCapwapWirelessBindingType);\
   pCapwapIsSetFsCapwapWirelessBindingEntry->bFsCapwapWirelessBindingType = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWirelessBindingEntry->bFsCapwapWirelessBindingType = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWirelessBindingRowStatus != NULL)\
  {\
   pCapwapFsCapwapWirelessBindingEntry->MibObject.i4FsCapwapWirelessBindingRowStatus = *(INT4 *) (pau1FsCapwapWirelessBindingRowStatus);\
   pCapwapIsSetFsCapwapWirelessBindingEntry->bFsCapwapWirelessBindingRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWirelessBindingEntry->bFsCapwapWirelessBindingRowStatus = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapFsCapwapWirelessBindingEntry->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
   pCapwapIsSetFsCapwapWirelessBindingEntry->bCapwapBaseWtpProfileId = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWirelessBindingEntry->bCapwapBaseWtpProfileId = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWirelessBindingRadioId != NULL)\
  {\
   pCapwapFsCapwapWirelessBindingEntry->MibObject.u4CapwapBaseWirelessBindingRadioId = *(UINT4 *) (pau1CapwapBaseWirelessBindingRadioId);\
   pCapwapIsSetFsCapwapWirelessBindingEntry->bCapwapBaseWirelessBindingRadioId = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWirelessBindingEntry->bCapwapBaseWirelessBindingRadioId = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsCapwapStationWhiteListEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAPWAPSTATIONWHITELIST_ARGS(pCapwapFsCapwapStationWhiteListEntry,\
   pCapwapIsSetFsCapwapStationWhiteListEntry,\
   pau1FsCapwapStationWhiteListId,\
   pau1FsCapwapStationWhiteListStationId,\
   pau1FsCapwapStationWhiteListStationIdLen,\
   pau1FsCapwapStationWhiteListRowStatus)\
  {\
  if (pau1FsCapwapStationWhiteListId != NULL)\
  {\
   pCapwapFsCapwapStationWhiteListEntry->MibObject.u4FsCapwapStationWhiteListId = *(UINT4 *) (pau1FsCapwapStationWhiteListId);\
   pCapwapIsSetFsCapwapStationWhiteListEntry->bFsCapwapStationWhiteListId = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapStationWhiteListEntry->bFsCapwapStationWhiteListId = OSIX_FALSE;\
  }\
  if (pau1FsCapwapStationWhiteListStationId != NULL)\
  {\
   MEMCPY (pCapwapFsCapwapStationWhiteListEntry->MibObject.au1FsCapwapStationWhiteListStationId, pau1FsCapwapStationWhiteListStationId, *(INT4 *)pau1FsCapwapStationWhiteListStationIdLen);\
   pCapwapFsCapwapStationWhiteListEntry->MibObject.i4FsCapwapStationWhiteListStationIdLen = *(INT4 *)pau1FsCapwapStationWhiteListStationIdLen;\
   pCapwapIsSetFsCapwapStationWhiteListEntry->bFsCapwapStationWhiteListStationId = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapStationWhiteListEntry->bFsCapwapStationWhiteListStationId = OSIX_FALSE;\
  }\
  if (pau1FsCapwapStationWhiteListRowStatus != NULL)\
  {\
   pCapwapFsCapwapStationWhiteListEntry->MibObject.i4FsCapwapStationWhiteListRowStatus = *(INT4 *) (pau1FsCapwapStationWhiteListRowStatus);\
   pCapwapIsSetFsCapwapStationWhiteListEntry->bFsCapwapStationWhiteListRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapStationWhiteListEntry->bFsCapwapStationWhiteListRowStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsCapwapWtpRebootStatisticsEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAPWAPWTPREBOOTSTATISTICSTABLE_ARGS(pCapwapFsCapwapWtpRebootStatisticsEntry,\
   pCapwapIsSetFsCapwapWtpRebootStatisticsEntry,\
   pau1FsCapwapWtpRebootStatisticsRebootCount,\
   pau1FsCapwapWtpRebootStatisticsAcInitiatedCount,\
   pau1FsCapwapWtpRebootStatisticsLinkFailureCount,\
   pau1FsCapwapWtpRebootStatisticsSwFailureCount,\
   pau1FsCapwapWtpRebootStatisticsHwFailureCount,\
   pau1FsCapwapWtpRebootStatisticsOtherFailureCount,\
   pau1FsCapwapWtpRebootStatisticsUnknownFailureCount,\
   pau1FsCapwapWtpRebootStatisticsLastFailureType,\
   pau1FsCapwapWtpRebootStatisticsRowStatus,\
   pau1CapwapBaseWtpProfileId)\
  {\
  if (pau1FsCapwapWtpRebootStatisticsRebootCount != NULL)\
  {\
   pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.u4FsCapwapWtpRebootStatisticsRebootCount = *(UINT4 *) (pau1FsCapwapWtpRebootStatisticsRebootCount);\
   pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->bFsCapwapWtpRebootStatisticsRebootCount = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->bFsCapwapWtpRebootStatisticsRebootCount = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWtpRebootStatisticsAcInitiatedCount != NULL)\
  {\
   pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.u4FsCapwapWtpRebootStatisticsAcInitiatedCount = *(UINT4 *) (pau1FsCapwapWtpRebootStatisticsAcInitiatedCount);\
   pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->bFsCapwapWtpRebootStatisticsAcInitiatedCount = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->bFsCapwapWtpRebootStatisticsAcInitiatedCount = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWtpRebootStatisticsLinkFailureCount != NULL)\
  {\
   pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.u4FsCapwapWtpRebootStatisticsLinkFailureCount = *(UINT4 *) (pau1FsCapwapWtpRebootStatisticsLinkFailureCount);\
   pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->bFsCapwapWtpRebootStatisticsLinkFailureCount = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->bFsCapwapWtpRebootStatisticsLinkFailureCount = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWtpRebootStatisticsSwFailureCount != NULL)\
  {\
   pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.u4FsCapwapWtpRebootStatisticsSwFailureCount = *(UINT4 *) (pau1FsCapwapWtpRebootStatisticsSwFailureCount);\
   pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->bFsCapwapWtpRebootStatisticsSwFailureCount = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->bFsCapwapWtpRebootStatisticsSwFailureCount = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWtpRebootStatisticsHwFailureCount != NULL)\
  {\
   pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.u4FsCapwapWtpRebootStatisticsHwFailureCount = *(UINT4 *) (pau1FsCapwapWtpRebootStatisticsHwFailureCount);\
   pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->bFsCapwapWtpRebootStatisticsHwFailureCount = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->bFsCapwapWtpRebootStatisticsHwFailureCount = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWtpRebootStatisticsOtherFailureCount != NULL)\
  {\
   pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.u4FsCapwapWtpRebootStatisticsOtherFailureCount = *(UINT4 *) (pau1FsCapwapWtpRebootStatisticsOtherFailureCount);\
   pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->bFsCapwapWtpRebootStatisticsOtherFailureCount = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->bFsCapwapWtpRebootStatisticsOtherFailureCount = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWtpRebootStatisticsUnknownFailureCount != NULL)\
  {\
   pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.u4FsCapwapWtpRebootStatisticsUnknownFailureCount = *(UINT4 *) (pau1FsCapwapWtpRebootStatisticsUnknownFailureCount);\
   pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->bFsCapwapWtpRebootStatisticsUnknownFailureCount = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->bFsCapwapWtpRebootStatisticsUnknownFailureCount = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWtpRebootStatisticsLastFailureType != NULL)\
  {\
   pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.i4FsCapwapWtpRebootStatisticsLastFailureType = *(INT4 *) (pau1FsCapwapWtpRebootStatisticsLastFailureType);\
   pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->bFsCapwapWtpRebootStatisticsLastFailureType = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->bFsCapwapWtpRebootStatisticsLastFailureType = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWtpRebootStatisticsRowStatus != NULL)\
  {\
   pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.i4FsCapwapWtpRebootStatisticsRowStatus = *(INT4 *) (pau1FsCapwapWtpRebootStatisticsRowStatus);\
   pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->bFsCapwapWtpRebootStatisticsRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->bFsCapwapWtpRebootStatisticsRowStatus = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
   pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->bCapwapBaseWtpProfileId = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpRebootStatisticsEntry->bCapwapBaseWtpProfileId = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsCapwapWtpRadioStatisticsEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAPWAPWTPRADIOSTATISTICSTABLE_ARGS(pCapwapFsCapwapWtpRadioStatisticsEntry,\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry,\
   pau1FsCapwapWtpRadioLastFailType,\
   pau1FsCapwapWtpRadioResetCount,\
   pau1FsCapwapWtpRadioSwFailureCount,\
   pau1FsCapwapWtpRadioHwFailureCount,\
   pau1FsCapwapWtpRadioOtherFailureCount,\
   pau1FsCapwapWtpRadioUnknownFailureCount,\
   pau1FsCapwapWtpRadioConfigUpdateCount,\
   pau1FsCapwapWtpRadioChannelChangeCount,\
   pau1FsCapwapWtpRadioBandChangeCount,\
   pau1FsCapwapWtpRadioCurrentNoiseFloor,\
   pau1FsCapwapWtpRadioStatRowStatus,\
   pau1RadioIfIndex)\
  {\
  if (pau1FsCapwapWtpRadioLastFailType != NULL)\
  {\
   pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.i4FsCapwapWtpRadioLastFailType = *(INT4 *) (pau1FsCapwapWtpRadioLastFailType);\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bFsCapwapWtpRadioLastFailType = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bFsCapwapWtpRadioLastFailType = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWtpRadioResetCount != NULL)\
  {\
   pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.u4FsCapwapWtpRadioResetCount = *(UINT4 *) (pau1FsCapwapWtpRadioResetCount);\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bFsCapwapWtpRadioResetCount = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bFsCapwapWtpRadioResetCount = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWtpRadioSwFailureCount != NULL)\
  {\
   pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.u4FsCapwapWtpRadioSwFailureCount = *(UINT4 *) (pau1FsCapwapWtpRadioSwFailureCount);\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bFsCapwapWtpRadioSwFailureCount = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bFsCapwapWtpRadioSwFailureCount = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWtpRadioHwFailureCount != NULL)\
  {\
   pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.u4FsCapwapWtpRadioHwFailureCount = *(UINT4 *) (pau1FsCapwapWtpRadioHwFailureCount);\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bFsCapwapWtpRadioHwFailureCount = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bFsCapwapWtpRadioHwFailureCount = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWtpRadioOtherFailureCount != NULL)\
  {\
   pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.u4FsCapwapWtpRadioOtherFailureCount = *(UINT4 *) (pau1FsCapwapWtpRadioOtherFailureCount);\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bFsCapwapWtpRadioOtherFailureCount = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bFsCapwapWtpRadioOtherFailureCount = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWtpRadioUnknownFailureCount != NULL)\
  {\
   pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.u4FsCapwapWtpRadioUnknownFailureCount = *(UINT4 *) (pau1FsCapwapWtpRadioUnknownFailureCount);\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bFsCapwapWtpRadioUnknownFailureCount = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bFsCapwapWtpRadioUnknownFailureCount = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWtpRadioConfigUpdateCount != NULL)\
  {\
   pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.u4FsCapwapWtpRadioConfigUpdateCount = *(UINT4 *) (pau1FsCapwapWtpRadioConfigUpdateCount);\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bFsCapwapWtpRadioConfigUpdateCount = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bFsCapwapWtpRadioConfigUpdateCount = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWtpRadioChannelChangeCount != NULL)\
  {\
   pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.u4FsCapwapWtpRadioChannelChangeCount = *(UINT4 *) (pau1FsCapwapWtpRadioChannelChangeCount);\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bFsCapwapWtpRadioChannelChangeCount = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bFsCapwapWtpRadioChannelChangeCount = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWtpRadioBandChangeCount != NULL)\
  {\
   pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.u4FsCapwapWtpRadioBandChangeCount = *(UINT4 *) (pau1FsCapwapWtpRadioBandChangeCount);\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bFsCapwapWtpRadioBandChangeCount = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bFsCapwapWtpRadioBandChangeCount = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWtpRadioCurrentNoiseFloor != NULL)\
  {\
   pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.u4FsCapwapWtpRadioCurrentNoiseFloor = *(UINT4 *) (pau1FsCapwapWtpRadioCurrentNoiseFloor);\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bFsCapwapWtpRadioCurrentNoiseFloor = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bFsCapwapWtpRadioCurrentNoiseFloor = OSIX_FALSE;\
  }\
  if (pau1FsCapwapWtpRadioStatRowStatus != NULL)\
  {\
   pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.u4FsCapwapWtpRadioStatRowStatus = *(UINT4 *) (pau1FsCapwapWtpRadioStatRowStatus);\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bFsCapwapWtpRadioStatRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bFsCapwapWtpRadioStatRowStatus = OSIX_FALSE;\
  }\
  if (pau1RadioIfIndex != NULL)\
  {\
   pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.i4RadioIfIndex = *(INT4 *) (pau1RadioIfIndex);\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bRadioIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetFsCapwapWtpRadioStatisticsEntry->bRadioIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the index values in  structure for FsWtpModelEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSWTPMODELTABLE_INDEX(pCapwapFsWtpModelEntry,\
   pau1FsCapwapWtpModelNumber)\
  {\
  if (pau1FsCapwapWtpModelNumber != NULL)\
  {\
   MEMCPY (pCapwapFsWtpModelEntry->MibObject.au1FsCapwapWtpModelNumber, pau1FsCapwapWtpModelNumber, *(INT4 *)pau1FsCapwapWtpModelNumberLen);\
   pCapwapFsWtpModelEntry->MibObject.i4FsCapwapWtpModelNumberLen = *(INT4 *)pau1FsCapwapWtpModelNumberLen;\
  }\
 }
/* Macro used to fill the index values in  structure for FsWtpRadioEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSWTPRADIOTABLE_INDEX(pCapwapFsWtpRadioEntry,\
   pau1FsCapwapWtpModelNumber,\
   pau1FsNumOfRadio)\
  {\
  if (pau1FsCapwapWtpModelNumber != NULL)\
  {\
   MEMCPY (pCapwapFsWtpRadioEntry->MibObject.au1FsCapwapWtpModelNumber, pau1FsCapwapWtpModelNumber, *(INT4 *)pau1FsCapwapWtpModelNumberLen);\
   pCapwapFsWtpRadioEntry->MibObject.i4FsCapwapWtpModelNumberLen = *(INT4 *)pau1FsCapwapWtpModelNumberLen;\
  }\
  if (pau1FsNumOfRadio != NULL)\
  {\
   pCapwapFsWtpRadioEntry->MibObject.u4FsNumOfRadio = *(UINT4 *) (pau1FsNumOfRadio);\
  }\
 }
/* Macro used to fill the index values in  structure for FsCapwapWhiteListEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAPWAPWHITELISTTABLE_INDEX(pCapwapFsCapwapWhiteListEntry,\
   pau1FsCapwapWhiteListId)\
  {\
  if (pau1FsCapwapWhiteListId != NULL)\
  {\
   pCapwapFsCapwapWhiteListEntry->MibObject.u4FsCapwapWhiteListId = *(UINT4 *) (pau1FsCapwapWhiteListId);\
  }\
 }
/* Macro used to fill the index values in  structure for FsCapwapBlackListEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAPWAPBLACKLIST_INDEX(pCapwapFsCapwapBlackListEntry,\
   pau1FsCapwapBlackListId)\
  {\
  if (pau1FsCapwapBlackListId != NULL)\
  {\
   pCapwapFsCapwapBlackListEntry->MibObject.u4FsCapwapBlackListId = *(UINT4 *) (pau1FsCapwapBlackListId);\
  }\
 }
/* Macro used to fill the index values in  structure for FsCapwapWtpConfigEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAPWAPWTPCONFIGTABLE_INDEX(pCapwapFsCapwapWtpConfigEntry,\
   pau1CapwapBaseWtpProfileId)\
  {\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapFsCapwapWtpConfigEntry->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
  }\
 }
/* Macro used to fill the index values in  structure for FsCapwapLinkEncryptionEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAPWAPLINKENCRYPTIONTABLE_INDEX(pCapwapFsCapwapLinkEncryptionEntry,\
   pau1CapwapBaseWtpProfileId,\
   pau1FsCapwapEncryptChannel)\
  {\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapFsCapwapLinkEncryptionEntry->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
  }\
  if (pau1FsCapwapEncryptChannel != NULL)\
  {\
   pCapwapFsCapwapLinkEncryptionEntry->MibObject.i4FsCapwapEncryptChannel = *(INT4 *) (pau1FsCapwapEncryptChannel);\
  }\
 }
/* Macro used to fill the index values in  structure for FsCapwapDefaultWtpProfileEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAWAPDEFAULTWTPPROFILETABLE_INDEX(pCapwapFsCapwapDefaultWtpProfileEntry,\
   pau1FsCapwapDefaultWtpProfileModelNumber)\
  {\
  if (pau1FsCapwapDefaultWtpProfileModelNumber != NULL)\
  {\
   MEMCPY (pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.au1FsCapwapDefaultWtpProfileModelNumber, pau1FsCapwapDefaultWtpProfileModelNumber, *(INT4 *)pau1FsCapwapDefaultWtpProfileModelNumberLen);\
   pCapwapFsCapwapDefaultWtpProfileEntry->MibObject.i4FsCapwapDefaultWtpProfileModelNumberLen = *(INT4 *)pau1FsCapwapDefaultWtpProfileModelNumberLen;\
  }\
 }
/* Macro used to fill the index values in  structure for FsCapwapDnsProfileEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAPWAPDNSPROFILETABLE_INDEX(pCapwapFsCapwapDnsProfileEntry,\
   pau1CapwapBaseWtpProfileId)\
  {\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapFsCapwapDnsProfileEntry->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
  }\
 }
/* Macro used to fill the index values in  structure for FsWtpNativeVlanIdTable 
 using the input given in def file */

#define  CAPWAP_FILL_FSWTPNATIVEVLANIDTABLE_INDEX(pCapwapFsWtpNativeVlanIdTable,\
   pau1CapwapBaseWtpProfileId)\
  {\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapFsWtpNativeVlanIdTable->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
  }\
 }
/* Macro used to fill the index values in  structure for FsCawapDiscStatsEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAWAPDISCSTATSTABLE_INDEX(pCapwapFsCawapDiscStatsEntry,\
   pau1CapwapBaseWtpProfileId)\
  {\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapFsCawapDiscStatsEntry->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
  }\
 }
/* Macro used to fill the index values in  structure for FsCawapJoinStatsEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAWAPJOINSTATSTABLE_INDEX(pCapwapFsCawapJoinStatsEntry,\
   pau1CapwapBaseWtpProfileId)\
  {\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapFsCawapJoinStatsEntry->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
  }\
 }
/* Macro used to fill the index values in  structure for FsCawapConfigStatsEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAWAPCONFIGSTATSTABLE_INDEX(pCapwapFsCawapConfigStatsEntry,\
   pau1CapwapBaseWtpProfileId)\
  {\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapFsCawapConfigStatsEntry->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
  }\
 }
/* Macro used to fill the index values in  structure for FsCawapRunStatsEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAWAPRUNSTATSTABLE_INDEX(pCapwapFsCawapRunStatsEntry,\
   pau1CapwapBaseWtpProfileId)\
  {\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapFsCawapRunStatsEntry->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
  }\
 }
/* Macro used to fill the index values in  structure for FsCapwapWirelessBindingEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAPWAPWIRELESSBINDINGTABLE_INDEX(pCapwapFsCapwapWirelessBindingEntry,\
   pau1CapwapBaseWtpProfileId,\
   pau1CapwapBaseWirelessBindingRadioId)\
  {\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapFsCapwapWirelessBindingEntry->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
  }\
  if (pau1CapwapBaseWirelessBindingRadioId != NULL)\
  {\
   pCapwapFsCapwapWirelessBindingEntry->MibObject.u4CapwapBaseWirelessBindingRadioId = *(UINT4 *) (pau1CapwapBaseWirelessBindingRadioId);\
  }\
 }
/* Macro used to fill the index values in  structure for FsCapwapStationWhiteListEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAPWAPSTATIONWHITELIST_INDEX(pCapwapFsCapwapStationWhiteListEntry,\
   pau1FsCapwapStationWhiteListId)\
  {\
  if (pau1FsCapwapStationWhiteListId != NULL)\
  {\
   pCapwapFsCapwapStationWhiteListEntry->MibObject.u4FsCapwapStationWhiteListId = *(UINT4 *) (pau1FsCapwapStationWhiteListId);\
  }\
 }
/* Macro used to fill the index values in  structure for FsCapwapWtpRebootStatisticsEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAPWAPWTPREBOOTSTATISTICSTABLE_INDEX(pCapwapFsCapwapWtpRebootStatisticsEntry,\
   pau1CapwapBaseWtpProfileId)\
  {\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapFsCapwapWtpRebootStatisticsEntry->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
  }\
 }
/* Macro used to fill the index values in  structure for FsCapwapWtpRadioStatisticsEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSCAPWAPWTPRADIOSTATISTICSTABLE_INDEX(pCapwapFsCapwapWtpRadioStatisticsEntry,\
   pau1CapwapBaseWtpProfileId)\
  {\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapFsCapwapWtpRadioStatisticsEntry->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
  }\
 }
/* Macro used to convert the input 
  to required datatype for FsCapwapEnable*/

#define  CAPWAP_FILL_FSCAPWAPENABLE(i4FsCapwapEnable,\
   pau1FsCapwapEnable)\
  {\
  if (pau1FsCapwapEnable != NULL)\
  {\
   i4FsCapwapEnable = *(INT4 *) (pau1FsCapwapEnable);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsCapwapShutdown*/

#define  CAPWAP_FILL_FSCAPWAPSHUTDOWN(i4FsCapwapShutdown,\
   pau1FsCapwapShutdown)\
  {\
  if (pau1FsCapwapShutdown != NULL)\
  {\
   i4FsCapwapShutdown = *(INT4 *) (pau1FsCapwapShutdown);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsCapwapControlUdpPort*/

#define  CAPWAP_FILL_FSCAPWAPCONTROLUDPPORT(u4FsCapwapControlUdpPort,\
   pau1FsCapwapControlUdpPort)\
  {\
  if (pau1FsCapwapControlUdpPort != NULL)\
  {\
   u4FsCapwapControlUdpPort = *(UINT4 *) (pau1FsCapwapControlUdpPort);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsCapwapControlChannelDTLSPolicyOptions*/

#define  CAPWAP_FILL_FSCAPWAPCONTROLCHANNELDTLSPOLICYOPTIONS(pFsCapwapControlChannelDTLSPolicyOptions,\
   pau1FsCapwapControlChannelDTLSPolicyOptions)\
  {\
  if (pau1FsCapwapControlChannelDTLSPolicyOptions != NULL)\
  {\
   STRCPY (pFsCapwapControlChannelDTLSPolicyOptions, pau1FsCapwapControlChannelDTLSPolicyOptions);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsCapwapDataChannelDTLSPolicyOptions*/

#define  CAPWAP_FILL_FSCAPWAPDATACHANNELDTLSPOLICYOPTIONS(pFsCapwapDataChannelDTLSPolicyOptions,\
   pau1FsCapwapDataChannelDTLSPolicyOptions)\
  {\
  if (pau1FsCapwapDataChannelDTLSPolicyOptions != NULL)\
  {\
   STRCPY (pFsCapwapDataChannelDTLSPolicyOptions, pau1FsCapwapDataChannelDTLSPolicyOptions);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsWlcDiscoveryMode*/

#define  CAPWAP_FILL_FSWLCDISCOVERYMODE(pFsWlcDiscoveryMode,\
   pau1FsWlcDiscoveryMode)\
  {\
  if (pau1FsWlcDiscoveryMode != NULL)\
  {\
   STRCPY (pFsWlcDiscoveryMode, pau1FsWlcDiscoveryMode);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsCapwapWtpModeIgnore*/

#define  CAPWAP_FILL_FSCAPWAPWTPMODEIGNORE(i4FsCapwapWtpModeIgnore,\
   pau1FsCapwapWtpModeIgnore)\
  {\
  if (pau1FsCapwapWtpModeIgnore != NULL)\
  {\
   i4FsCapwapWtpModeIgnore = *(INT4 *) (pau1FsCapwapWtpModeIgnore);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsCapwapDebugMask*/

#define  CAPWAP_FILL_FSCAPWAPDEBUGMASK(i4FsCapwapDebugMask,\
   pau1FsCapwapDebugMask)\
  {\
  if (pau1FsCapwapDebugMask != NULL)\
  {\
   i4FsCapwapDebugMask = *(INT4 *) (pau1FsCapwapDebugMask);\
  }\
  }

/* Macro used to convert the input 
  to required datatype for FsCapwapWssDataDebugMask*/

#define  CAPWAP_FILL_FSCAPWAPWSSDATA(i4FsCapwapWssDataDebugMask,\
   pau1FsCapwapWssDataDebugMask)\
  {\
  if (pau1FsCapwapWssDataDebugMask != NULL)\
  {\
   i4FsCapwapWssDataDebugMask = *(INT4 *) (pau1FsCapwapWssDataDebugMask);\
  }\
  }

/* Macro used to convert the input 
  to required datatype for FsDtlsDebugMask*/

#define  CAPWAP_FILL_FSDTLSDEBUGMASK(i4FsDtlsDebugMask,\
   pau1FsDtlsDebugMask)\
  {\
  if (pau1FsDtlsDebugMask != NULL)\
  {\
   i4FsDtlsDebugMask = *(INT4 *) (pau1FsDtlsDebugMask);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsDtlsEncryption*/

#define  CAPWAP_FILL_FSDTLSENCRYPTION(i4FsDtlsEncryption,\
   pau1FsDtlsEncryption)\
  {\
  if (pau1FsDtlsEncryption != NULL)\
  {\
   i4FsDtlsEncryption = *(INT4 *) (pau1FsDtlsEncryption);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsDtlsEncryptAlogritham*/

#define  CAPWAP_FILL_FSDTLSENCRYPTALOGRITHAM(i4FsDtlsEncryptAlogritham,\
   pau1FsDtlsEncryptAlogritham)\
  {\
  if (pau1FsDtlsEncryptAlogritham != NULL)\
  {\
   i4FsDtlsEncryptAlogritham = *(INT4 *) (pau1FsDtlsEncryptAlogritham);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsStationType*/

#define  CAPWAP_FILL_FSSTATIONTYPE(i4FsStationType,\
   pau1FsStationType)\
  {\
  if (pau1FsStationType != NULL)\
  {\
   i4FsStationType = *(INT4 *) (pau1FsStationType);\
  }\
  }


/* Macro used to fill the CLI structure for CapwapBaseAcNameListEntry 
 using the input given in def file */

#define  CAPWAP_FILL_CAPWAPBASEACNAMELISTTABLE_ARGS(pCapwapCapwapBaseAcNameListEntry,\
   pCapwapIsSetCapwapBaseAcNameListEntry,\
   pau1CapwapBaseAcNameListId,\
   pau1CapwapBaseAcNameListName,\
   pau1CapwapBaseAcNameListNameLen,\
   pau1CapwapBaseAcNameListPriority,\
   pau1CapwapBaseAcNameListRowStatus)\
  {\
  if (pau1CapwapBaseAcNameListId != NULL)\
  {\
   pCapwapCapwapBaseAcNameListEntry->MibObject.u4CapwapBaseAcNameListId = *(UINT4 *) (pau1CapwapBaseAcNameListId);\
   pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListId = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListId = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseAcNameListName != NULL)\
  {\
   MEMCPY (pCapwapCapwapBaseAcNameListEntry->MibObject.au1CapwapBaseAcNameListName, pau1CapwapBaseAcNameListName, *(INT4 *)pau1CapwapBaseAcNameListNameLen);\
   pCapwapCapwapBaseAcNameListEntry->MibObject.i4CapwapBaseAcNameListNameLen = *(INT4 *)pau1CapwapBaseAcNameListNameLen;\
   pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListName = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListName = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseAcNameListPriority != NULL)\
  {\
   pCapwapCapwapBaseAcNameListEntry->MibObject.u4CapwapBaseAcNameListPriority = *(UINT4 *) (pau1CapwapBaseAcNameListPriority);\
   pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListPriority = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListPriority = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseAcNameListRowStatus != NULL)\
  {\
   pCapwapCapwapBaseAcNameListEntry->MibObject.i4CapwapBaseAcNameListRowStatus = *(INT4 *) (pau1CapwapBaseAcNameListRowStatus);\
   pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseAcNameListEntry->bCapwapBaseAcNameListRowStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for CapwapBaseMacAclEntry 
 using the input given in def file */

#define  CAPWAP_FILL_CAPWAPBASEMACACLTABLE_ARGS(pCapwapCapwapBaseMacAclEntry,\
   pCapwapIsSetCapwapBaseMacAclEntry,\
   pau1CapwapBaseMacAclId,\
   pau1CapwapBaseMacAclStationId,\
   pau1CapwapBaseMacAclStationIdLen,\
   pau1CapwapBaseMacAclRowStatus)\
  {\
  if (pau1CapwapBaseMacAclId != NULL)\
  {\
   pCapwapCapwapBaseMacAclEntry->MibObject.u4CapwapBaseMacAclId = *(UINT4 *) (pau1CapwapBaseMacAclId);\
   pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclId = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclId = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseMacAclStationId != NULL)\
  {\
   MEMCPY (pCapwapCapwapBaseMacAclEntry->MibObject.au1CapwapBaseMacAclStationId, pau1CapwapBaseMacAclStationId, *(INT4 *)pau1CapwapBaseMacAclStationIdLen);\
   pCapwapCapwapBaseMacAclEntry->MibObject.i4CapwapBaseMacAclStationIdLen = *(INT4 *)pau1CapwapBaseMacAclStationIdLen;\
   pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclStationId = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclStationId = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseMacAclRowStatus != NULL)\
  {\
   pCapwapCapwapBaseMacAclEntry->MibObject.i4CapwapBaseMacAclRowStatus = *(INT4 *) (pau1CapwapBaseMacAclRowStatus);\
   pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseMacAclEntry->bCapwapBaseMacAclRowStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for CapwapBaseWtpProfileEntry 
 using the input given in def file */

#define  CAPWAP_FILL_CAPWAPBASEWTPPROFILETABLE_ARGS(pCapwapCapwapBaseWtpProfileEntry,\
   pCapwapIsSetCapwapBaseWtpProfileEntry,\
   pau1CapwapBaseWtpProfileId,\
   pau1CapwapBaseWtpProfileName,\
   pau1CapwapBaseWtpProfileNameLen,\
   pau1CapwapBaseWtpProfileWtpMacAddress,\
   pau1CapwapBaseWtpProfileWtpMacAddressLen,\
   pau1CapwapBaseWtpProfileWtpModelNumber,\
   pau1CapwapBaseWtpProfileWtpModelNumberLen,\
   pau1CapwapBaseWtpProfileWtpName,\
   pau1CapwapBaseWtpProfileWtpNameLen,\
   pau1CapwapBaseWtpProfileWtpLocation,\
   pau1CapwapBaseWtpProfileWtpLocationLen,\
   pau1CapwapBaseWtpProfileWtpStaticIpEnable,\
   pau1CapwapBaseWtpProfileWtpStaticIpType,\
   pau1CapwapBaseWtpProfileWtpStaticIpAddress,\
   pau1CapwapBaseWtpProfileWtpStaticIpAddressLen,\
   pau1CapwapBaseWtpProfileWtpNetmask,\
   pau1CapwapBaseWtpProfileWtpNetmaskLen,\
   pau1CapwapBaseWtpProfileWtpGateway,\
   pau1CapwapBaseWtpProfileWtpGatewayLen,\
   pau1CapwapBaseWtpProfileWtpFallbackEnable,\
   pau1CapwapBaseWtpProfileWtpEchoInterval,\
   pau1CapwapBaseWtpProfileWtpIdleTimeout,\
   pau1CapwapBaseWtpProfileWtpMaxDiscoveryInterval,\
   pau1CapwapBaseWtpProfileWtpReportInterval,\
   pau1CapwapBaseWtpProfileWtpStatisticsTimer,\
   pau1CapwapBaseWtpProfileWtpEcnSupport,\
   pau1CapwapBaseWtpProfileRowStatus)\
  {\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapCapwapBaseWtpProfileEntry->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileId = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileId = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileName != NULL)\
  {\
   MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.au1CapwapBaseWtpProfileName, pau1CapwapBaseWtpProfileName, *(INT4 *)pau1CapwapBaseWtpProfileNameLen);\
   pCapwapCapwapBaseWtpProfileEntry->MibObject.i4CapwapBaseWtpProfileNameLen = *(INT4 *)pau1CapwapBaseWtpProfileNameLen;\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileName = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileName = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileWtpMacAddress != NULL)\
  {\
   MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.au1CapwapBaseWtpProfileWtpMacAddress, pau1CapwapBaseWtpProfileWtpMacAddress, *(INT4 *)pau1CapwapBaseWtpProfileWtpMacAddressLen);\
   pCapwapCapwapBaseWtpProfileEntry->MibObject.i4CapwapBaseWtpProfileWtpMacAddressLen = *(INT4 *)pau1CapwapBaseWtpProfileWtpMacAddressLen;\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpMacAddress = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpMacAddress = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileWtpModelNumber != NULL)\
  {\
   MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.au1CapwapBaseWtpProfileWtpModelNumber, pau1CapwapBaseWtpProfileWtpModelNumber, *(INT4 *)pau1CapwapBaseWtpProfileWtpModelNumberLen);\
   pCapwapCapwapBaseWtpProfileEntry->MibObject.i4CapwapBaseWtpProfileWtpModelNumberLen = *(INT4 *)pau1CapwapBaseWtpProfileWtpModelNumberLen;\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpModelNumber = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpModelNumber = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileWtpName != NULL)\
  {\
   MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.au1CapwapBaseWtpProfileWtpName, pau1CapwapBaseWtpProfileWtpName, *(INT4 *)pau1CapwapBaseWtpProfileWtpNameLen);\
   pCapwapCapwapBaseWtpProfileEntry->MibObject.i4CapwapBaseWtpProfileWtpNameLen = *(INT4 *)pau1CapwapBaseWtpProfileWtpNameLen;\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpName = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpName = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileWtpLocation != NULL)\
  {\
   MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.au1CapwapBaseWtpProfileWtpLocation, pau1CapwapBaseWtpProfileWtpLocation, *(INT4 *)pau1CapwapBaseWtpProfileWtpLocationLen);\
   pCapwapCapwapBaseWtpProfileEntry->MibObject.i4CapwapBaseWtpProfileWtpLocationLen = *(INT4 *)pau1CapwapBaseWtpProfileWtpLocationLen;\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpLocation = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpLocation = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileWtpStaticIpEnable != NULL)\
  {\
   pCapwapCapwapBaseWtpProfileEntry->MibObject.i4CapwapBaseWtpProfileWtpStaticIpEnable = *(INT4 *) (pau1CapwapBaseWtpProfileWtpStaticIpEnable);\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpStaticIpEnable = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpStaticIpEnable = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileWtpStaticIpType != NULL)\
  {\
   pCapwapCapwapBaseWtpProfileEntry->MibObject.i4CapwapBaseWtpProfileWtpStaticIpType = *(INT4 *) (pau1CapwapBaseWtpProfileWtpStaticIpType);\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpStaticIpType = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpStaticIpType = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileWtpStaticIpAddress != NULL)\
  {\
   MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.au1CapwapBaseWtpProfileWtpStaticIpAddress, pau1CapwapBaseWtpProfileWtpStaticIpAddress, *(INT4 *)pau1CapwapBaseWtpProfileWtpStaticIpAddressLen);\
   pCapwapCapwapBaseWtpProfileEntry->MibObject.i4CapwapBaseWtpProfileWtpStaticIpAddressLen = *(INT4 *)pau1CapwapBaseWtpProfileWtpStaticIpAddressLen;\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpStaticIpAddress = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpStaticIpAddress = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileWtpNetmask != NULL)\
  {\
   MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.au1CapwapBaseWtpProfileWtpNetmask, pau1CapwapBaseWtpProfileWtpNetmask, *(INT4 *)pau1CapwapBaseWtpProfileWtpNetmaskLen);\
   pCapwapCapwapBaseWtpProfileEntry->MibObject.i4CapwapBaseWtpProfileWtpNetmaskLen = *(INT4 *)pau1CapwapBaseWtpProfileWtpNetmaskLen;\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpNetmask = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpNetmask = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileWtpGateway != NULL)\
  {\
   MEMCPY (pCapwapCapwapBaseWtpProfileEntry->MibObject.au1CapwapBaseWtpProfileWtpGateway, pau1CapwapBaseWtpProfileWtpGateway, *(INT4 *)pau1CapwapBaseWtpProfileWtpGatewayLen);\
   pCapwapCapwapBaseWtpProfileEntry->MibObject.i4CapwapBaseWtpProfileWtpGatewayLen = *(INT4 *)pau1CapwapBaseWtpProfileWtpGatewayLen;\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpGateway = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpGateway = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileWtpFallbackEnable != NULL)\
  {\
   pCapwapCapwapBaseWtpProfileEntry->MibObject.i4CapwapBaseWtpProfileWtpFallbackEnable = *(INT4 *) (pau1CapwapBaseWtpProfileWtpFallbackEnable);\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpFallbackEnable = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpFallbackEnable = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileWtpEchoInterval != NULL)\
  {\
   pCapwapCapwapBaseWtpProfileEntry->MibObject.u4CapwapBaseWtpProfileWtpEchoInterval = *(UINT4 *) (pau1CapwapBaseWtpProfileWtpEchoInterval);\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpEchoInterval = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpEchoInterval = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileWtpIdleTimeout != NULL)\
  {\
   pCapwapCapwapBaseWtpProfileEntry->MibObject.u4CapwapBaseWtpProfileWtpIdleTimeout = *(UINT4 *) (pau1CapwapBaseWtpProfileWtpIdleTimeout);\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpIdleTimeout = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpIdleTimeout = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileWtpMaxDiscoveryInterval != NULL)\
  {\
   pCapwapCapwapBaseWtpProfileEntry->MibObject.u4CapwapBaseWtpProfileWtpMaxDiscoveryInterval = *(UINT4 *) (pau1CapwapBaseWtpProfileWtpMaxDiscoveryInterval);\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpMaxDiscoveryInterval = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpMaxDiscoveryInterval = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileWtpReportInterval != NULL)\
  {\
   pCapwapCapwapBaseWtpProfileEntry->MibObject.u4CapwapBaseWtpProfileWtpReportInterval = *(UINT4 *) (pau1CapwapBaseWtpProfileWtpReportInterval);\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpReportInterval = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpReportInterval = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileWtpStatisticsTimer != NULL)\
  {\
   pCapwapCapwapBaseWtpProfileEntry->MibObject.u4CapwapBaseWtpProfileWtpStatisticsTimer = *(UINT4 *) (pau1CapwapBaseWtpProfileWtpStatisticsTimer);\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpStatisticsTimer = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpStatisticsTimer = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileWtpEcnSupport != NULL)\
  {\
   pCapwapCapwapBaseWtpProfileEntry->MibObject.i4CapwapBaseWtpProfileWtpEcnSupport = *(INT4 *) (pau1CapwapBaseWtpProfileWtpEcnSupport);\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpEcnSupport = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileWtpEcnSupport = OSIX_FALSE;\
  }\
  if (pau1CapwapBaseWtpProfileRowStatus != NULL)\
  {\
   pCapwapCapwapBaseWtpProfileEntry->MibObject.i4CapwapBaseWtpProfileRowStatus = *(INT4 *) (pau1CapwapBaseWtpProfileRowStatus);\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pCapwapIsSetCapwapBaseWtpProfileEntry->bCapwapBaseWtpProfileRowStatus = OSIX_FALSE;\
  }\
                }






/* Macro used to fill the index values in  structure for CapwapBaseAcNameListEntry 
 using the input given in def file */

#define  CAPWAP_FILL_CAPWAPBASEACNAMELISTTABLE_INDEX(pCapwapCapwapBaseAcNameListEntry,\
   pau1CapwapBaseAcNameListId)\
  {\
  if (pau1CapwapBaseAcNameListId != NULL)\
  {\
   pCapwapCapwapBaseAcNameListEntry->MibObject.u4CapwapBaseAcNameListId = *(UINT4 *) (pau1CapwapBaseAcNameListId);\
  }\
 }
/* Macro used to fill the index values in  structure for CapwapBaseMacAclEntry 
 using the input given in def file */

#define  CAPWAP_FILL_CAPWAPBASEMACACLTABLE_INDEX(pCapwapCapwapBaseMacAclEntry,\
   pau1CapwapBaseMacAclId)\
  {\
  if (pau1CapwapBaseMacAclId != NULL)\
  {\
   pCapwapCapwapBaseMacAclEntry->MibObject.u4CapwapBaseMacAclId = *(UINT4 *) (pau1CapwapBaseMacAclId);\
  }\
 }
/* Macro used to fill the index values in  structure for CapwapBaseWtpProfileEntry 
 using the input given in def file */

#define  CAPWAP_FILL_CAPWAPBASEWTPPROFILETABLE_INDEX(pCapwapCapwapBaseWtpProfileEntry,\
   pau1CapwapBaseWtpProfileId)\
  {\
  if (pau1CapwapBaseWtpProfileId != NULL)\
  {\
   pCapwapCapwapBaseWtpProfileEntry->MibObject.u4CapwapBaseWtpProfileId = *(UINT4 *) (pau1CapwapBaseWtpProfileId);\
  }\
 }


/* Macro used to convert the input 
  to required datatype for CapwapBaseWtpSessionsLimit*/

#define  CAPWAP_FILL_CAPWAPBASEWTPSESSIONSLIMIT(u4CapwapBaseWtpSessionsLimit,\
   pau1CapwapBaseWtpSessionsLimit)\
  {\
  if (pau1CapwapBaseWtpSessionsLimit != NULL)\
  {\
   u4CapwapBaseWtpSessionsLimit = *(UINT4 *) (pau1CapwapBaseWtpSessionsLimit);\
  }\
  }




/* Macro used to convert the input 
  to required datatype for CapwapBaseStationSessionsLimit*/

#define  CAPWAP_FILL_CAPWAPBASESTATIONSESSIONSLIMIT(u4CapwapBaseStationSessionsLimit,\
   pau1CapwapBaseStationSessionsLimit)\
  {\
  if (pau1CapwapBaseStationSessionsLimit != NULL)\
  {\
   u4CapwapBaseStationSessionsLimit = *(UINT4 *) (pau1CapwapBaseStationSessionsLimit);\
  }\
  }


/* Macro used to convert the input to required datatype for FsDtlsEncAuthEncryptAlgorithm */
#define  CAPWAP_FILL_FSDTLSENCAUTHENCRYPTALGORITHM(u4FsDtlsEncAuthEncryptAlgorithm,\
        pau1FsDtlsEncAuthEncryptAlgorithm)\
{\
    if (pau1FsDtlsEncAuthEncryptAlgorithm != NULL)\
    {\
        u4FsDtlsEncAuthEncryptAlgorithm = *(UINT4 *) (pau1FsDtlsEncAuthEncryptAlgorithm);\
    }\
}

/* Macro used to convert the input to required datatype for Fsdtlsencrption*/
#define  CAPWAP_FILL_FSDTLSENCRPTION(u4FsDtlsEncrption,\
        pau1FsDtlsEncrption)\
{\
    if (pau1FsDtlsEncrption != NULL)\
    {\
        u4FsDtlsEncrption = *(UINT4 *) (pau1FsDtlsEncrption);\
    }\
}





/* Macro used to convert the input 
  to required datatype for CapwapBaseAcMaxRetransmit*/

#define  CAPWAP_FILL_CAPWAPBASEACMAXRETRANSMIT(u4CapwapBaseAcMaxRetransmit,\
   pau1CapwapBaseAcMaxRetransmit)\
  {\
  if (pau1CapwapBaseAcMaxRetransmit != NULL)\
  {\
   u4CapwapBaseAcMaxRetransmit = *(UINT4 *) (pau1CapwapBaseAcMaxRetransmit);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseAcChangeStatePendingTimer*/

#define  CAPWAP_FILL_CAPWAPBASEACCHANGESTATEPENDINGTIMER(u4CapwapBaseAcChangeStatePendingTimer,\
   pau1CapwapBaseAcChangeStatePendingTimer)\
  {\
  if (pau1CapwapBaseAcChangeStatePendingTimer != NULL)\
  {\
   u4CapwapBaseAcChangeStatePendingTimer = *(UINT4 *) (pau1CapwapBaseAcChangeStatePendingTimer);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseAcDataCheckTimer*/

#define  CAPWAP_FILL_CAPWAPBASEACDATACHECKTIMER(u4CapwapBaseAcDataCheckTimer,\
   pau1CapwapBaseAcDataCheckTimer)\
  {\
  if (pau1CapwapBaseAcDataCheckTimer != NULL)\
  {\
   u4CapwapBaseAcDataCheckTimer = *(UINT4 *) (pau1CapwapBaseAcDataCheckTimer);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseAcDTLSSessionDeleteTimer*/

#define  CAPWAP_FILL_CAPWAPBASEACDTLSSESSIONDELETETIMER(u4CapwapBaseAcDTLSSessionDeleteTimer,\
   pau1CapwapBaseAcDTLSSessionDeleteTimer)\
  {\
  if (pau1CapwapBaseAcDTLSSessionDeleteTimer != NULL)\
  {\
   u4CapwapBaseAcDTLSSessionDeleteTimer = *(UINT4 *) (pau1CapwapBaseAcDTLSSessionDeleteTimer);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseAcEchoInterval*/

#define  CAPWAP_FILL_CAPWAPBASEACECHOINTERVAL(u4CapwapBaseAcEchoInterval,\
   pau1CapwapBaseAcEchoInterval)\
  {\
  if (pau1CapwapBaseAcEchoInterval != NULL)\
  {\
   u4CapwapBaseAcEchoInterval = *(UINT4 *) (pau1CapwapBaseAcEchoInterval);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseAcRetransmitInterval*/

#define  CAPWAP_FILL_CAPWAPBASEACRETRANSMITINTERVAL(u4CapwapBaseAcRetransmitInterval,\
   pau1CapwapBaseAcRetransmitInterval)\
  {\
  if (pau1CapwapBaseAcRetransmitInterval != NULL)\
  {\
   u4CapwapBaseAcRetransmitInterval = *(UINT4 *) (pau1CapwapBaseAcRetransmitInterval);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseAcSilentInterval*/

#define  CAPWAP_FILL_CAPWAPBASEACSILENTINTERVAL(u4CapwapBaseAcSilentInterval,\
   pau1CapwapBaseAcSilentInterval)\
  {\
  if (pau1CapwapBaseAcSilentInterval != NULL)\
  {\
   u4CapwapBaseAcSilentInterval = *(UINT4 *) (pau1CapwapBaseAcSilentInterval);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseAcWaitDTLSTimer*/

#define  CAPWAP_FILL_CAPWAPBASEACWAITDTLSTIMER(u4CapwapBaseAcWaitDTLSTimer,\
   pau1CapwapBaseAcWaitDTLSTimer)\
  {\
  if (pau1CapwapBaseAcWaitDTLSTimer != NULL)\
  {\
   u4CapwapBaseAcWaitDTLSTimer = *(UINT4 *) (pau1CapwapBaseAcWaitDTLSTimer);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseAcWaitJoinTimer*/

#define  CAPWAP_FILL_CAPWAPBASEACWAITJOINTIMER(u4CapwapBaseAcWaitJoinTimer,\
   pau1CapwapBaseAcWaitJoinTimer)\
  {\
  if (pau1CapwapBaseAcWaitJoinTimer != NULL)\
  {\
   u4CapwapBaseAcWaitJoinTimer = *(UINT4 *) (pau1CapwapBaseAcWaitJoinTimer);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseAcEcnSupport*/

#define  CAPWAP_FILL_CAPWAPBASEACECNSUPPORT(i4CapwapBaseAcEcnSupport,\
   pau1CapwapBaseAcEcnSupport)\
  {\
  if (pau1CapwapBaseAcEcnSupport != NULL)\
  {\
   i4CapwapBaseAcEcnSupport = *(INT4 *) (pau1CapwapBaseAcEcnSupport);\
  }\
  }






/* Macro used to convert the input 
  to required datatype for CapwapBaseNtfWtpId*/

#define  CAPWAP_FILL_CAPWAPBASENTFWTPID(pCapwapBaseNtfWtpId,\
   pau1CapwapBaseNtfWtpId)\
  {\
  if (pau1CapwapBaseNtfWtpId != NULL)\
  {\
   STRCPY (pCapwapBaseNtfWtpId, pau1CapwapBaseNtfWtpId);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseNtfRadioId*/

#define  CAPWAP_FILL_CAPWAPBASENTFRADIOID(u4CapwapBaseNtfRadioId,\
   pau1CapwapBaseNtfRadioId)\
  {\
  if (pau1CapwapBaseNtfRadioId != NULL)\
  {\
   u4CapwapBaseNtfRadioId = *(UINT4 *) (pau1CapwapBaseNtfRadioId);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseNtfChannelType*/

#define  CAPWAP_FILL_CAPWAPBASENTFCHANNELTYPE(i4CapwapBaseNtfChannelType,\
   pau1CapwapBaseNtfChannelType)\
  {\
  if (pau1CapwapBaseNtfChannelType != NULL)\
  {\
   i4CapwapBaseNtfChannelType = *(INT4 *) (pau1CapwapBaseNtfChannelType);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseNtfAuthenMethod*/

#define  CAPWAP_FILL_CAPWAPBASENTFAUTHENMETHOD(i4CapwapBaseNtfAuthenMethod,\
   pau1CapwapBaseNtfAuthenMethod)\
  {\
  if (pau1CapwapBaseNtfAuthenMethod != NULL)\
  {\
   i4CapwapBaseNtfAuthenMethod = *(INT4 *) (pau1CapwapBaseNtfAuthenMethod);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseNtfChannelDownReason*/

#define  CAPWAP_FILL_CAPWAPBASENTFCHANNELDOWNREASON(i4CapwapBaseNtfChannelDownReason,\
   pau1CapwapBaseNtfChannelDownReason)\
  {\
  if (pau1CapwapBaseNtfChannelDownReason != NULL)\
  {\
   i4CapwapBaseNtfChannelDownReason = *(INT4 *) (pau1CapwapBaseNtfChannelDownReason);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseNtfStationIdList*/

#define  CAPWAP_FILL_CAPWAPBASENTFSTATIONIDLIST(pCapwapBaseNtfStationIdList,\
   pau1CapwapBaseNtfStationIdList)\
  {\
  if (pau1CapwapBaseNtfStationIdList != NULL)\
  {\
   STRCPY (pCapwapBaseNtfStationIdList, pau1CapwapBaseNtfStationIdList);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseNtfAuthenFailureReason*/

#define  CAPWAP_FILL_CAPWAPBASENTFAUTHENFAILUREREASON(i4CapwapBaseNtfAuthenFailureReason,\
   pau1CapwapBaseNtfAuthenFailureReason)\
  {\
  if (pau1CapwapBaseNtfAuthenFailureReason != NULL)\
  {\
   i4CapwapBaseNtfAuthenFailureReason = *(INT4 *) (pau1CapwapBaseNtfAuthenFailureReason);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseNtfRadioOperStatusFlag*/

#define  CAPWAP_FILL_CAPWAPBASENTFRADIOOPERSTATUSFLAG(i4CapwapBaseNtfRadioOperStatusFlag,\
   pau1CapwapBaseNtfRadioOperStatusFlag)\
  {\
  if (pau1CapwapBaseNtfRadioOperStatusFlag != NULL)\
  {\
   i4CapwapBaseNtfRadioOperStatusFlag = *(INT4 *) (pau1CapwapBaseNtfRadioOperStatusFlag);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseNtfRadioStatusCause*/

#define  CAPWAP_FILL_CAPWAPBASENTFRADIOSTATUSCAUSE(i4CapwapBaseNtfRadioStatusCause,\
   pau1CapwapBaseNtfRadioStatusCause)\
  {\
  if (pau1CapwapBaseNtfRadioStatusCause != NULL)\
  {\
   i4CapwapBaseNtfRadioStatusCause = *(INT4 *) (pau1CapwapBaseNtfRadioStatusCause);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseNtfJoinFailureReason*/

#define  CAPWAP_FILL_CAPWAPBASENTFJOINFAILUREREASON(i4CapwapBaseNtfJoinFailureReason,\
   pau1CapwapBaseNtfJoinFailureReason)\
  {\
  if (pau1CapwapBaseNtfJoinFailureReason != NULL)\
  {\
   i4CapwapBaseNtfJoinFailureReason = *(INT4 *) (pau1CapwapBaseNtfJoinFailureReason);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseNtfImageFailureReason*/

#define  CAPWAP_FILL_CAPWAPBASENTFIMAGEFAILUREREASON(i4CapwapBaseNtfImageFailureReason,\
   pau1CapwapBaseNtfImageFailureReason)\
  {\
  if (pau1CapwapBaseNtfImageFailureReason != NULL)\
  {\
   i4CapwapBaseNtfImageFailureReason = *(INT4 *) (pau1CapwapBaseNtfImageFailureReason);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseNtfConfigMsgErrorType*/

#define  CAPWAP_FILL_CAPWAPBASENTFCONFIGMSGERRORTYPE(i4CapwapBaseNtfConfigMsgErrorType,\
   pau1CapwapBaseNtfConfigMsgErrorType)\
  {\
  if (pau1CapwapBaseNtfConfigMsgErrorType != NULL)\
  {\
   i4CapwapBaseNtfConfigMsgErrorType = *(INT4 *) (pau1CapwapBaseNtfConfigMsgErrorType);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseNtfMsgErrorElements*/

#define  CAPWAP_FILL_CAPWAPBASENTFMSGERRORELEMENTS(pCapwapBaseNtfMsgErrorElements,\
   pau1CapwapBaseNtfMsgErrorElements)\
  {\
  if (pau1CapwapBaseNtfMsgErrorElements != NULL)\
  {\
   STRCPY (pCapwapBaseNtfMsgErrorElements, pau1CapwapBaseNtfMsgErrorElements);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseChannelUpDownNotifyEnable*/

#define  CAPWAP_FILL_CAPWAPBASECHANNELUPDOWNNOTIFYENABLE(i4CapwapBaseChannelUpDownNotifyEnable,\
   pau1CapwapBaseChannelUpDownNotifyEnable)\
  {\
  if (pau1CapwapBaseChannelUpDownNotifyEnable != NULL)\
  {\
   i4CapwapBaseChannelUpDownNotifyEnable = *(INT4 *) (pau1CapwapBaseChannelUpDownNotifyEnable);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseDecryptErrorNotifyEnable*/

#define  CAPWAP_FILL_CAPWAPBASEDECRYPTERRORNOTIFYENABLE(i4CapwapBaseDecryptErrorNotifyEnable,\
   pau1CapwapBaseDecryptErrorNotifyEnable)\
  {\
  if (pau1CapwapBaseDecryptErrorNotifyEnable != NULL)\
  {\
   i4CapwapBaseDecryptErrorNotifyEnable = *(INT4 *) (pau1CapwapBaseDecryptErrorNotifyEnable);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseJoinFailureNotifyEnable*/

#define  CAPWAP_FILL_CAPWAPBASEJOINFAILURENOTIFYENABLE(i4CapwapBaseJoinFailureNotifyEnable,\
   pau1CapwapBaseJoinFailureNotifyEnable)\
  {\
  if (pau1CapwapBaseJoinFailureNotifyEnable != NULL)\
  {\
   i4CapwapBaseJoinFailureNotifyEnable = *(INT4 *) (pau1CapwapBaseJoinFailureNotifyEnable);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseImageUpgradeFailureNotifyEnable*/

#define  CAPWAP_FILL_CAPWAPBASEIMAGEUPGRADEFAILURENOTIFYENABLE(i4CapwapBaseImageUpgradeFailureNotifyEnable,\
   pau1CapwapBaseImageUpgradeFailureNotifyEnable)\
  {\
  if (pau1CapwapBaseImageUpgradeFailureNotifyEnable != NULL)\
  {\
   i4CapwapBaseImageUpgradeFailureNotifyEnable = *(INT4 *) (pau1CapwapBaseImageUpgradeFailureNotifyEnable);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseConfigMsgErrorNotifyEnable*/

#define  CAPWAP_FILL_CAPWAPBASECONFIGMSGERRORNOTIFYENABLE(i4CapwapBaseConfigMsgErrorNotifyEnable,\
   pau1CapwapBaseConfigMsgErrorNotifyEnable)\
  {\
  if (pau1CapwapBaseConfigMsgErrorNotifyEnable != NULL)\
  {\
   i4CapwapBaseConfigMsgErrorNotifyEnable = *(INT4 *) (pau1CapwapBaseConfigMsgErrorNotifyEnable);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseRadioOperableStatusNotifyEnable*/

#define  CAPWAP_FILL_CAPWAPBASERADIOOPERABLESTATUSNOTIFYENABLE(i4CapwapBaseRadioOperableStatusNotifyEnable,\
   pau1CapwapBaseRadioOperableStatusNotifyEnable)\
  {\
  if (pau1CapwapBaseRadioOperableStatusNotifyEnable != NULL)\
  {\
   i4CapwapBaseRadioOperableStatusNotifyEnable = *(INT4 *) (pau1CapwapBaseRadioOperableStatusNotifyEnable);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for CapwapBaseAuthenFailureNotifyEnable*/

#define  CAPWAP_FILL_CAPWAPBASEAUTHENFAILURENOTIFYENABLE(i4CapwapBaseAuthenFailureNotifyEnable,\
   pau1CapwapBaseAuthenFailureNotifyEnable)\
  {\
  if (pau1CapwapBaseAuthenFailureNotifyEnable != NULL)\
  {\
   i4CapwapBaseAuthenFailureNotifyEnable = *(INT4 *) (pau1CapwapBaseAuthenFailureNotifyEnable);\
  }\
  }
/* Macro used to fill the index values in  structure for FsWtpModelEntry 
 using the input given in def file */

#define  CAPWAP_FILL_FSWTPMODELTABLE_INDEX(pCapwapFsWtpModelEntry,\
   pau1FsCapwapWtpModelNumber)\
  {\
  if (pau1FsCapwapWtpModelNumber != NULL)\
  {\
   MEMCPY (pCapwapFsWtpModelEntry->MibObject.au1FsCapwapWtpModelNumber, pau1FsCapwapWtpModelNumber, *(INT4 *)pau1FsCapwapWtpModelNumberLen);\
   pCapwapFsWtpModelEntry->MibObject.i4FsCapwapWtpModelNumberLen = *(INT4 *)pau1FsCapwapWtpModelNumberLen;\
  }\
 }
/* Macro used to fill the index values in  structure for FsWtpRadioEntry 
 using the input given in def file 

#define  CAPWAP_FILL_FSWTPRADIOTABLE_INDEX(pCapwapFsWtpRadioEntry,\
   pau1FsCapwapWtpModelNum,\
   pau1FsNoOfRadio)\
  {\
  if (pau1FsCapwapWtpModelNum != NULL)\
  {\
   MEMCPY (pCapwapFsWtpRadioEntry->MibObject.au1FsCapwapWtpModelNum, pau1FsCapwapWtpModelNum, *(INT4 *)pau1FsCapwapWtpModelNumLen);\
   pCapwapFsWtpRadioEntry->MibObject.i4FsCapwapWtpModelNumLen = *(INT4 *)pau1FsCapwapWtpModelNumLen;\
  }\
  if (pau1FsNoOfRadio != NULL)\
  {\
   pCapwapFsWtpRadioEntry->MibObject.u4FsNoOfRadio = *(UINT4 *) (pau1FsNoOfRadio);\
  }\
 }*/
/* Macro used to convert the input 
  to required datatype for FsCapwapTrasnportProtocol*/

#define  CAPWAP_FILL_FSCAPWAPTRASNPORTPROTOCOL(pFsCapwapTrasnportProtocol,\
   pau1FsCapwapTrasnportProtocol)\
  {\
  if (pau1FsCapwapTrasnportProtocol != NULL)\
  {\
   STRCPY (pFsCapwapTrasnportProtocol, pau1FsCapwapTrasnportProtocol);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsCapwapControlUdpPort*/

#define  CAPWAP_FILL_FSCAPWAPCONTROLUDPPORT(u4FsCapwapControlUdpPort,\
   pau1FsCapwapControlUdpPort)\
  {\
  if (pau1FsCapwapControlUdpPort != NULL)\
  {\
   u4FsCapwapControlUdpPort = *(UINT4 *) (pau1FsCapwapControlUdpPort);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsCapwapDataUdpPort*/

#define  CAPWAP_FILL_FSCAPWAPDATAUDPPORT(u4FsCapwapDataUdpPort,\
   pau1FsCapwapDataUdpPort)\
  {\
  if (pau1FsCapwapDataUdpPort != NULL)\
  {\
   u4FsCapwapDataUdpPort = *(UINT4 *) (pau1FsCapwapDataUdpPort);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsCapwapControlChannelDTLSPolicyOptions*/

#define  CAPWAP_FILL_FSCAPWAPCONTROLCHANNELDTLSPOLICYOPTIONS(pFsCapwapControlChannelDTLSPolicyOptions,\
   pau1FsCapwapControlChannelDTLSPolicyOptions)\
  {\
  if (pau1FsCapwapControlChannelDTLSPolicyOptions != NULL)\
  {\
   STRCPY (pFsCapwapControlChannelDTLSPolicyOptions, pau1FsCapwapControlChannelDTLSPolicyOptions);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsCapwapMaxFragmentsPerPacket*/

#define  CAPWAP_FILL_FSCAPWAPMAXFRAGMENTSPERPACKET(i4FsCapwapMaxFragmentsPerPacket,\
   pau1FsCapwapMaxFragmentsPerPacket)\
  {\
  if (pau1FsCapwapMaxFragmentsPerPacket != NULL)\
  {\
   i4FsCapwapMaxFragmentsPerPacket = *(INT4 *) (pau1FsCapwapMaxFragmentsPerPacket);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsCapwapEnableFsmOptimization*/

#define  CAPWAP_FILL_FSCAPWAPENABLEFSMOPTIMIZATION(i4FsCapwapEnableFsmOptimization,\
   pau1FsCapwapEnableFsmOptimization)\
  {\
  if (pau1FsCapwapEnableFsmOptimization != NULL)\
  {\
   i4FsCapwapEnableFsmOptimization = *(INT4 *) (pau1FsCapwapEnableFsmOptimization);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsDiscoveryMode*/

#define  CAPWAP_FILL_FSDISCOVERYMODE(pFsDiscoveryMode,\
   pau1FsDiscoveryMode)\
  {\
  if (pau1FsDiscoveryMode != NULL)\
  {\
   STRCPY (pFsDiscoveryMode, pau1FsDiscoveryMode);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsWtpDiscoveryType*/

#define  CAPWAP_FILL_FSWTPDISCOVERYTYPE(i4FsWtpDiscoveryType,\
   pau1FsWtpDiscoveryType)\
  {\
  if (pau1FsWtpDiscoveryType != NULL)\
  {\
   i4FsWtpDiscoveryType = *(INT4 *) (pau1FsWtpDiscoveryType);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsCapwapWtpModeIgnore*/

#define  CAPWAP_FILL_FSCAPWAPWTPMODEIGNORE(i4FsCapwapWtpModeIgnore,\
   pau1FsCapwapWtpModeIgnore)\
  {\
  if (pau1FsCapwapWtpModeIgnore != NULL)\
  {\
   i4FsCapwapWtpModeIgnore = *(INT4 *) (pau1FsCapwapWtpModeIgnore);\
  }\
  }


