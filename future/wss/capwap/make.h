#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : MAKE.H                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                     |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX ( Slackware 1.2.1 )                     |
# |                                                                          |
# |   $Id: make.h,v 1.2 2017/05/23 14:16:47 siva Exp $                                                                  |
# |                                                                           
# |   DATE                   : 07th Mar 2002                                 |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

CAPWAP_SWITCHES = -DRAD_TEST

TOTAL_OPNS = ${CAPWAP_SWITCHES} $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

CFA_BASE_DIR   = ${BASE_DIR}/cfa2

FCAP_INC             = ${BASE_DIR}/wss/fcapwap/inc
CAPWAP_BASE_DIR      = ${BASE_DIR}/wss/capwap
CAPWAP_SRC_DIR       = ${CAPWAP_BASE_DIR}/src
CAPWAP_INC_DIR       = ${CAPWAP_BASE_DIR}/inc
CAPWAP_OBJ_DIR       = ${CAPWAP_BASE_DIR}/obj
WSSCFG_INC_DIR       = ${BASE_DIR}/wss/wsscfg/inc
WSSPM_INC_DIR       = ${BASE_DIR}/wss/wsspm/inc
WSSIF_INC_DIR        = ${BASE_DIR}/wss/wssif/inc
APHDLR_INC_DIR      = ${BASE_DIR}/wss/aphdlr/inc
WLAN_INC_DIR       = ${BASE_DIR}/wss/wsswlan/inc
WLCHDLR_INC_DIR    = ${BASE_DIR}/wss/wlchdlr/inc
DTLS_INC_DIR       = ${BASE_DIR}/wss/dtls/inc
SNMP_INCL_DIR      = ${BASE_DIR}/inc/snmp
CFA_INCD           = ${CFA_BASE_DIR}/inc
CMN_INC_DIR        = ${BASE_DIR}/inc
WSSPM_INC_DIR    = ${BASE_DIR}/wss/wsspm/inc
RADIOIF_INC_DIR       = ${BASE_DIR}/wss/radioif/inc
RFMGMT_INC_DIR       = ${BASE_DIR}/wss/rfmgmt/inc
COMMON_INC_DIR   = ${BASE_DIR}/ISS/common/system/inc
DNS_INC_DIR        = ${BASE_DIR}/dns/inc

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################
GLOBAL_INCLUDES  = -I${DTLS_INC_DIR} -I${CAPWAP_INC_DIR} -I${CFA_INCD} -I${CMN_INC_DIR} -I${WSSIF_INC_DIR} -I${APHDLR_INC_DIR}     -I${WLCHDLR_INC_DIR} -I${WSSPM_INC_DIR} -I${WSSCFG_INC_DIR} -I${WLAN_INC_DIR}   -I${COMMON_INC_DIR} -I${RFMGMT_INC_DIR}  -I${RADIOIF_INC_DIR} -I${FCAP_INC} -I${DNS_INC_DIR}

INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
