/********************************************************************
* Copyright PROTO ((C) 2006 Aricent Inc . All Rights Reserved]
*
* $Id: wssifproto.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
*
* Description: This file contains all the prototypes used by the
*              WSS Modules.
*
********************************************************************/

#ifndef __WSSIF_PROTO_H__
#define __WSSIF_PROTO_H__


UINT1 WssIfInit (VOID);
UINT1 WssIfDeInit (VOID);
UINT1 WssIfLock (VOID);
UINT1 WssIfUnLock (VOID);

VOID Dot11nCalculateRxStbc(tRadioIfGetDB *);

UINT1
WssIfProcessCfaMsg(UINT4 eMsgType, tCfaMsgStruct *pCfaMsgStruct);

INT4 WssifUpdateBeacon ( UINT4 u4RadioIfIndex, UINT4 u4WlanIfIndex );

INT4 WssifUpdateBeaconParamForAllBssId ( tRadioIfGetDB *pRadioIfGetDB);
UINT4 WssifAllocateTCFilterHandle (UINT4 *pHandle);
VOID WssifFreeTCFilterHandle (UINT4 *pu4Handle);

#if 0
/* #include "wssifinc.h" */
#include "radioif.h"
#include "wsswlan.h"

/* RADIO IF Module */
UINT1 WssIfProcessRadioIfMsg PROTO ((UINT4, tRadioIfMsgStruct *));

/* WSS WLAN Module */
UINT1 WssIfProcessWssWlanMsg PROTO ((UINT4, tWssWlanMsgStruct *));

/* RFMGMT Module */
UINT1 WssIfProcessRfmgmtMsg PROTO ((UINT4, tRfMgmtMsgStruct *));
#endif
#endif
