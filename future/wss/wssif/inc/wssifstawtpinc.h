/******************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                       *
 * $Id: wssifstawtpinc.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $                                                                           *
 * Description: This file contains the include files for WSSIFSTA             *
 ******************************************************************************/
#ifndef __WSSSTA_WTP_INC_H__
#define __WSSSTA_WTP_INC_H__

#include "lr.h"
#include "cfa.h"
#include "ipv6.h"
#include "tcp.h"
#include "cli.h"
#include "trace.h"
#include "fssocket.h"
#include "fssyslog.h"
#include "capwap.h"
#ifdef WTP_WANTED
#include "aphdlr.h"
#include "wssifconst.h"
#include "wssiftdfs.h"
#endif
#include "wssmac.h"
#include "wsssta.h"
#include "wssifstawtptdfs.h"
#include "wssifstawtpdef.h"
#include "wssifstawtptrc.h"
#include "wssifstawtpsz.h"
#include "wssifstawtpprot.h"

#endif
