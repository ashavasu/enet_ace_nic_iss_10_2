/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *   $Id: wssifwlcwlanproto.h,v 1.3 2017/11/24 10:37:10 siva Exp $ 
 *  Description: This file contains prototypes for functions used for
 *               wlc wsswlan module.
 *
 *  *********************************************************************/



#ifndef _WSSIFWLANPROTO_H
#define _WSSIFWLANPROTO_H


INT4 WssWlanIfIndexDBRBCmp PROTO ((tRBElem*, tRBElem*));

INT4 WssWlanBssIfIndexDBRBCmp PROTO ((tRBElem* , tRBElem*));

INT4 WssWlanBssInterfaceDBRBCmp PROTO ((tRBElem* , tRBElem* ));

INT4 WssWlanBssIdMappingDBRBCmp PROTO ((tRBElem* , tRBElem* ));

INT4 WssWlanSSIDMappingDBRBCmp PROTO ((tRBElem * e1, tRBElem * e2));

UINT1 WssWlanSSIDMappingDBCreate PROTO ((VOID));

UINT1 WssWlanBssIfIndexDBCreate PROTO ((VOID));

UINT1 WssWlanIfIndexDBCreate PROTO ((VOID));

UINT1 WssWlanBssInterfaceDBCreate PROTO ((VOID));

UINT1 WssWlanBssIdMappingDBCreate PROTO ((VOID));

UINT1 WssIfProcessWssWlanDBMsg PROTO ((UINT1 , tWssWlanDB*));

UINT1 WssWlanInit PROTO ((VOID));
UINT1
WssIfGetBandSelectStatus PROTO((UINT1 *));

UINT1
WssIfSetBandSelectStatus PROTO((UINT1 ));

UINT1
WssIfGetLegacyRateStatus PROTO((UINT1 *));

INT4
WssIfSetLegacyRateStatus PROTO((UINT1 ));
#endif

