/** $Id: wssifconst.h,v 1.2 2017/05/23 14:16:57 siva Exp $ **/
#ifndef __WSSIFCONST_H__
#define __WSSIFCONST_H__

#define  WSSIF_LOCK_SEM_NAME              (const UINT1 *) "WSSIFM"
#define  WSSIF_SEM_CREATE_INIT_CNT        1
#define  PMF_TWO_BYTES     2

#define WSSIF_LOCK  WssIfLock ()
#define WSSIF_UNLOCK  WssIfUnLock ()

#define WSS_MIN_USER_TC_FILTER_ID           1
#define WSS_MAX_USER_TC_FILTER_ID           MAX_WSS_USERSESSION_INFO
#define WSS_USER_TC_HANDLE_LIST_SIZE        ((WSS_MAX_USER_TC_FILTER_ID + 31)/32 * 4)

#define MAX_WSS_TCFILTER_HANDLE           MAX_WSS_USERSESSION_INFO
#define AP_NOT_PRESENT   2

#define AP_NETWORK_TYPE_WAN NETWORK_TYPE_WAN
#define AP_NETWORK_TYPE_LAN NETWORK_TYPE_LAN

#define AP_IF_UP  1
#define AP_IF_DOWN  2

#endif
