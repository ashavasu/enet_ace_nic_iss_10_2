/*  $Id: wssifcapproto.h,v 1.2 2017/05/23 14:16:57 siva Exp $ */

#ifndef _WSSIFCAP_PROTO_H_
#define _WSSIFCAP_PROTO_H_

/* extern INT4  IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams); */

INT4
WssIfCompareWtpProfileRBTree (tRBElem *, tRBElem *);

tCapwapBindingTable *
CapwapGetNextFsWlanBindingTable(tCapwapBindingTable *);

tCapwapBindingTable *
CapwapGetFirstFsWlanBindingTable(VOID);

INT4
CapwapValidateWlanTable(UINT4 u4CapwapDot11WlanProfileId);
INT4
WssIfCompareWtpProfildIdRBTree (tRBElem *, tRBElem *);

INT4
WssIfCompareWtpBindingTableRBTree (tRBElem *, tRBElem *);

INT4
WssIfCompareWtpProfileNameMapRBTree (tRBElem *, tRBElem *);

INT4
WssIfCompareWtpProfileMacMapRBTree (tRBElem *, tRBElem *);

INT4 
WssIfCompareWlcBlackListRBTree PROTO ((tRBElem *, tRBElem *));

INT4 
WssIfCompareWlcWhiteListRBTree PROTO ((tRBElem *, tRBElem *));

INT4
WssIfCompareStaWhiteListRBTree PROTO ((tRBElem *, tRBElem *));

INT4
WssIfCompareStaBlackListRBTree PROTO ((tRBElem *, tRBElem *));

INT4
WssIfDeleteRSMFromJoinSessionId PROTO ((tWssIfCapDB *));

INT4
WssIfCompareDhcpPoolRBTree (tRBElem *, tRBElem *);

INT4
WssIfCompareWtpDhcpPoolRBTree (tRBElem *, tRBElem *);

INT4
WssIfCompareNATConfigEntryRBTree (tRBElem *, tRBElem *);

INT4
WssIfCompareWtpLRConfigRBTree (tRBElem *, tRBElem *);

INT4
WssIfCompareWtpDhcpConfigRBTree (tRBElem *, tRBElem *);

INT4
WssIfCompareWtpIpRouteConfigRBTree (tRBElem *, tRBElem *);


INT4 WssIfCapwapDBInit PROTO ((VOID));

tWssIfProfileIdMap *
WssIfWtpProfileIdEntryGet (UINT4 );
tWssIfWtpModelMap *
WssIfWtpModelEntryGet  (UINT1 *);
tCapwapDtlsSessEntry *
WssIfCapwapDtlsEntryGet (UINT4 );
INT4
WssIfCompareCapwapDtlsRBTree (tRBElem *, tRBElem *);
INT4
WssIfCapwapDtlsEntryDelete (UINT4, UINT4);
INT4
WssIfCapwapDtlsEntryCreate  (UINT4 , UINT4 , tMacAddr );

INT4 WssIfRemoteSessionInit (VOID);
tRemoteSessionManager *
WssIfRemoteSessionCreate (UINT4 , UINT4 );
INT4
WssIfRemoteSessionDelete (UINT4 , UINT4);
tRemoteSessionManager *
WssIfRemoteSessionGet (UINT4 , UINT4 );
INT4
WssIfPortSessDBCreate (VOID);
INT4
WssIfPortSessDBDelete (VOID);
INT4
WssIfWtpProfileIdSessDBCreate (VOID);
INT4
WssIfWtpProfileIdSessDBDelete (VOID);
INT4
WssIfSessionPortEntryCreate (UINT4 , UINT4, UINT2 );
INT4
WssIfSessionProfileIdEntryCreate  (UINT2, UINT2);
INT4
WssIfSessionPortEntryDelete (UINT4 , UINT2 );
INT4
WssIfSessionProfileIdEntryDelete (UINT2);
tCapwapSessDestIpPortMapTable *
WssIfSessionPortEntryGet (UINT4 , UINT4 );
tCapwapSessProfileIdMapTable *
WssIfSessionProfileIdEntryGet (UINT2);
INT4
WssIfCompareSessionPortRBTree (tRBElem *, tRBElem *);
INT4
WssIfCompareSessionProfileIdRBTree (tRBElem *, tRBElem * );
INT4
WssIfCompareWtpModelRBTree (tRBElem *, tRBElem *);

INT4
WssIfWtpDhcpConfigDBCreate (VOID);

INT4
WssIfDhcpGlobalDBCreate (VOID);

INT4
WssIfDhcpGlobalDBDelete (VOID);

INT4
WssIfWtpDhcpDBCreate (VOID);

INT4
WssIfWtpDhcpDBDelete (VOID);

INT4
WssIfNATConfigEntryDBCreate (VOID);

INT4
WssIfWtpLRConfigDBCreate (VOID);

INT4
WssIfWtpIpRouteConfigDBCreate (VOID);

INT4
WssIfNATConfigEntryDBDelete (VOID);

INT4
WssIfWtpLRConfigDBDelete (VOID);

INT4
WssIfWtpIpRouteConfigDBDelete (VOID);

INT4
WssIfProcessCapwapDBMsg (UINT1, tWssIfCapDB *);

INT4
WssIfWtpBindingTableDBDelete (UINT1 *pu1ModelNumber,UINT1 u1RadioId,UINT2 u2WlanProfileId);

INT4
WssIfWtpBindingTableDBCreate (VOID);

INT4
WssIfWtpBindingTableDBGet(UINT1 *pu1ModelNumber,UINT1 u1RadioId,UINT2 u2WlanProfileId, INT4 *);

INT4
WssIfWtpBindingTableDBAdd (UINT1 *pu1ModelNumber,UINT1 u1RadioId,UINT2 u2WlanProfileId, UINT1 u1RowStatus);

INT4
ValidateWlanTable(UINT4 u4CapwapDot11WlanProfileId);

INT4
WssIfWtpBindingTableDBSet(UINT1 *pu1ModelNumber,UINT1 u1RadioId,UINT2 u2WlanProfileId, UINT1 u1RowStatus);

INT4
WssifStartBinding(tCapwapBindingTable *,tWssIfCapDB *);

INT4
WssIfProcessApHdlrDBMsg (UINT1 , tWssIfApHdlrDB *);
INT4
WssIfProcessWlcHdlrDBMsg (UINT1 , tWssIfWlcHdlrDB *);
INT4
WssIfProcessStaticNameinWlcHdlrMsg(tWssIfCapDB *);
INT4
CapwapGetInternalProfildId (UINT2  *pProfildId, UINT2 *pWtpInternalId);
INT4
CapwapGetProfileIdFromInternalProfId (UINT2  wtpInternalId, UINT2 *pWtpProfileId);
INT4
CapwapGetFreeInternalId (UINT2  *pProfildId, UINT2 *pWtpInternalId);
INT4
WssIfGetProfileIndex (UINT2 u2WtpProfileId, UINT2 *pInternalId);
INT4
WssIfDeleteProfileIndex (UINT2 u2WtpProfileId);

INT4 WssIfGetProfildIndex (UINT2 , UINT2   *);
INT4 WssIfWtpProfildIdEntryDelete (UINT4 );
INT4 WssIfWtpModelEntryDelete (UINT1 *);
tRemoteSessionManager * 
WssIfGetRsmFromProfileId (tCapwapSessProfileIdMapTable *);
tRemoteSessionManager *
WssIfGetRsmFromCapwapId (UINT2);
INT4 WssIfSetProfildIndex (UINT2 );

INT4 WssIfCapwapDtlsDBCreate (VOID);
INT4 WssIfCapwapDtlsDBDelete (VOID);
INT4 WssIfWtpProfileDBCreate (VOID);
INT4 WssIfWtpProfildIdDBCreate (VOID);
INT4 WssIfWtpProfildIdDBDelete (VOID);
INT4 WssIfWtpProfileNameMapDBCreate (VOID);
INT4 WssIfWtpProfileNameMapDBDelete (VOID);
INT4 WssIfWtpProfileMacMapDBCreate (VOID);
INT4 WssIfWtpProfileMacMapDBDelete (VOID);
INT4 WssIfWtpProfildIdEntryCreate  (UINT4, UINT2);
INT4 WssIfGetRSMFromJoinSessionId (tWssIfCapDB *);
INT4 WssIfWtpModelDBCreate (VOID);
INT4 WssIfWtpModelDBDelete (VOID);
INT4 WssIfWtpModelEntryCreate  (UINT1 *, UINT2);
INT4 WssIfWlcWhiteListDBCreate PROTO ((void));
INT4 WssIfWlcWhiteListDBDelete PROTO ((void));
INT4 WssIfWlcBlackListDBCreate PROTO ((void));
INT4 WssIfWlcBlackListDBDelete PROTO ((void));
INT4 WssIfStaWhiteListDBCreate PROTO ((void));
INT4 WssIfStaBlackListDBCreate PROTO ((void));
INT4 WssIfStaWhiteListDBDelete PROTO ((void));
INT4 WssIfStaBlackListDBDelete PROTO ((void));
INT4 WssIfGetIfIpInfo (UINT1 *pu1IfName, UINT4 *pNodeIpAddr);

INT4
WssIfCapDscpMapDBCreate (VOID);
INT4
WssIfCapDscpMapDBDelete (VOID);

#ifdef WLC_WANTED
INT4
WssIfCapwapDscpMapEntryCreate (tCapwapDscpMapTable *);
INT4
WssIfCapwapDscpMapEntryDelete (tCapwapDscpMapTable *);
INT4
WssIfCapwapDscpMapEntryGet (tCapwapDscpMapTable *);
INT4
WssIfCapwapDscpMapEntrySet (tCapwapDscpMapTable *);

#endif

#ifdef WTP_WANTED
INT4
WssIfCapwapDscpMapEntryCreate (UINT4 , UINT1, UINT1 );
INT4
WssIfCapwapDscpMapEntryDelete (UINT4 , UINT1 );
tCapwapDscpMapTable *
WssIfCapwapDscpMapEntryGet (UINT4 , UINT1 );
#endif
INT4
WssIfDscpConfigTableGetFirstEntry(INT4 *, UINT4 *);
INT4
WssIfDscpConfigTableGetNextEntry(INT4 *, UINT4 *, INT4 *,
     UINT4 *);
INT4
WssIfCompareDscpMapRBTree (tRBElem *, tRBElem *);
    
INT4
WssIfDhcpGlobalAddEntry (tWssIfDhcpPoolDB *pDhcpPoolDB);
INT4
WssIfWtpLRConfigAddEntry (tWssIfWtpLRConfigDB *pConfigEntry);
INT4
WssIfWtpIpRouteConfigAddEntry (tWssIfWtpIpRouteConfigDB *pConfigEntry);

INT4
WssIfNATConfigAddEntry (tWssIfNATConfigEntryDB *pConfigEntry);
INT4
WssIfWtpDhcpPoolAddEntry (tWssIfWtpDhcpPoolDB *pWtpDhcpPoolDB);
INT4
WssIfDhcpGlobalDeleteEntry (tWssIfDhcpPoolDB *pDhcpPoolDB);
INT4
WssIfWtpLRConfigDeleteEntry (tWssIfWtpLRConfigDB *pConfigEntry);
INT4
WssIfWtpIpRouteConfigDeleteEntry (tWssIfWtpIpRouteConfigDB *pConfigEntry);
INT4
WssIfNATConfigDeleteEntry (tWssIfNATConfigEntryDB *pConfigEntry);
INT4
WssIfWtpDhcpPoolDeleteEntry (tWssIfWtpDhcpPoolDB *pWtpDhcpPoolDB);
INT4
WssIfDhcpGlobalGetFirstEntry (VOID);
INT4
WssIfNATConfigGetFirstEntry (UINT4 *,INT4 *);
INT4
WssIfWtpLRConfigGetFirstEntry(UINT4 *);
INT4
WssIfWtpIpRouteConfigGetFirstEntry(UINT4 *, UINT4 *, UINT4 *, UINT4 *);
INT4
WssIfWtpDhcpPoolGetFirstEntry (UINT4 *, INT4 *);
INT4
WssIfDhcpGlobalGetNextEntry (INT4 *);
INT4
WssIfNATConfigGetNextEntry (INT4 *,UINT4 *,UINT4*, INT4*);
INT4
WssIfWtpLRConfigGetNextEntry(UINT4 *, UINT4 *);
INT4
WssIfWtpIpRouteConfigGetNextEntry(UINT4 *, UINT4 *, UINT4 *,UINT4 *, UINT4 *,UINT4 *, UINT4 *,UINT4 *);
INT4
WssIfWtpDhcpPoolGetNextEntry (UINT4, UINT4 *,  UINT4, UINT4 *);
INT4
WssIfDhcpGlobalGetEntry (tWssIfDhcpPoolDB *pDhcpPoolDB);
INT4
WssIfNATConfigGetEntry (tWssIfNATConfigEntryDB *pConfigEntry);
INT4
WssIfWtpLRConfigGetEntry (tWssIfWtpLRConfigDB *pWtpLRConfig);
INT4
WssIfWtpIpRouteConfigGetEntry (tWssIfWtpIpRouteConfigDB *pWtpIpRouteConfig);
INT4
WssIfWtpDhcpPoolGetEntry (tWssIfWtpDhcpPoolDB *pWtpDhcpPoolDB);
INT4
WssIfDhcpGlobalSetEntry (tWssIfDhcpPoolDB *pDhcpPoolDB);
INT4
WssIfWtpLRConfigSetEntry (tWssIfWtpLRConfigDB *pWtpLRConfig);
INT4
WssIfWtpIpRouteConfigSetEntry (tWssIfWtpIpRouteConfigDB *pWtpIpRouteConfig);
INT4
WssIfNATConfigSetEntry (tWssIfNATConfigEntryDB *pConfigEntry);
INT4
WssIfWtpDhcpPoolSetEntry (tWssIfWtpDhcpPoolDB *pWtpDhcpPoolDB);
INT4
WssIfDhcpServerStatusGet (VOID);
INT4
WssIfDhcpServerStatusSet (INT4);
INT4
WssIfDhcpRelayStatusGet (VOID);
INT4
WssIfDhcpRelayStatusSet (INT4);
INT4
WssIfDhcpNextSrvIpAddrGet (VOID);
INT4
WssIfDhcpNextSrvIpAddrSet (UINT4);
INT4
WssIfWlanDNSServerIpAddrGet (VOID);
INT4
WssIfWlanDNSServerIpAddrSet (UINT4);
INT4
WssIfWlanDefaultRouterIpAddrGet (VOID);
INT4
WssIfWlanDefaultRouterIpAddrSet (UINT4);
INT4
WssIfWtpFirewallStatusGet (VOID);
INT4
WssIfWtpFirewallStatusSet (INT4);
INT4
WssIfWtpNATStatusGet (VOID);
INT4
WssIfWtpNATStatusSet (INT4);
INT4
WssifStartDhcpRelayUpdateReq (UINT4, UINT1, UINT4);
INT4
WssifStartDhcpPoolUpdateReq (UINT4, UINT1, INT4, UINT1);
UINT4
WssIfWlanRadioIfIpCheck (INT4, UINT4);
UINT4
WssIfWlanUpdateDiffServ(INT4, UINT4 );
INT4
WssIfWtpDhcpConfigAddEntry (tWssIfWtpDhcpConfigDB *);
INT4
WssIfWtpDhcpConfigDeleteEntry (tWssIfWtpDhcpConfigDB *);
INT4
WssIfWtpDhcpConfigGetFirstEntry(UINT4 *);
INT4
WssIfWtpDhcpConfigGetNextEntry(UINT4 *, UINT4 *);
INT4
WssIfWtpDhcpConfigGetEntry (tWssIfWtpDhcpConfigDB *);
INT4
WssIfWtpDhcpConfigSetEntry (tWssIfWtpDhcpConfigDB *);
INT4
WssifFireWallUpdateReq (UINT4, UINT4 );
INT4
WssifNatUpdateReq(UINT4 ,INT4, UINT4 );
INT4
WssifRouteTable(UINT4  u4WtpProfileId, UINT4 u4Subnet,
  UINT4 u4Mask, UINT4 u4Gateway, UINT4 u4RowStatus);
INT4
WssifFirewallFilterTable(UINT4, UINT1*,
        UINT1*, UINT1 *, UINT1, UINT1 *,
        UINT1 *, UINT2, UINT4);
INT4
WssifFirewallAclTable(UINT4,UINT1 *, INT4, UINT1 ,
                      UINT1, UINT2, INT4);

INT4
WssIfWtpDhcpConfigDBDelete (VOID);
UINT4
WssIfRadioUpdateDiffServ(INT4 i4IfIndex);
 
INT4
WssIfWtpFwlFilterDBCreate (VOID);
INT4
WssIfWtpFwlRuleDBCreate (VOID);
INT4
WssIfWtpFwlAclDBCreate (VOID);
INT4
WssIfWtpFwlFilterDBDelete (VOID);
INT4
WssIfWtpFwlRuleDBDelete (VOID);
INT4
WssIfWtpFwlAclDBDelete (VOID);
INT4
WssIfCompareWtpFwlFilterRBTree (tRBElem * e1, tRBElem * e2);
INT4
WssIfCompareWtpFwlRuleRBTree (tRBElem * e1, tRBElem * e2);
INT4
WssIfCompareWtpFwlAclRBTree (tRBElem * e1, tRBElem * e2);
INT4
WssIfWtpFwlFilterAddEntry (tWssIfWtpFwlFilterDB *);
VOID
ApFwlAddDefaultRules (UINT4);
INT1
ApFwlInitDefaultRule (UINT4, INT1 *, INT1 *,
                     UINT4, UINT4, UINT1,
                     UINT1);
PUBLIC VOID
ApFwlConcatAddrAndMask (UINT4,UINT4,UINT1*);
INT4
WssIfWtpFwlFilterAddDefEntry (tWssIfWtpFwlFilterDB *);
INT4
WssIfWtpFwlRuleAddEntry (tWssIfWtpFwlRuleDB *);
INT4
WssIfWtpFwlAclAddEntry (tWssIfWtpFwlAclDB *);
INT4
WssIfWtpFwlFilterDeleteEntry (tWssIfWtpFwlFilterDB *);
INT4
WssIfWtpFwlRuleDeleteEntry (tWssIfWtpFwlRuleDB *);
INT4
WssIfWtpFwlAclDeleteEntry (tWssIfWtpFwlAclDB *);
INT4
WssIfWtpFwlFilterGetFirstEntry(UINT4 *, UINT1 *);
INT4
WssIfWtpFwlRuleGetFirstEntry(UINT4 *, UINT1 *);
INT4
WssIfWtpFwlAclGetFirstEntry(UINT4 *, UINT1 *,INT4 *,UINT1 *);
INT4
WssIfWtpFwlFilterGetNextEntry(UINT4, UINT4 *,UINT1 *, UINT1 *);
INT4
WssIfWtpFwlRuleGetNextEntry(UINT4, UINT4 *,UINT1 *, UINT1 *);
INT4
WssIfWtpFwlAclGetNextEntry(UINT4, UINT4 *,UINT1 *, UINT1 *,INT4, INT4 *,UINT1, UINT1*);
INT4
WssIfWtpFwlFilterGetEntry (tWssIfWtpFwlFilterDB *);
INT4
WssIfWtpFwlFilterSetEntry (tWssIfWtpFwlFilterDB *);
INT4
WssIfWtpFwlRuleGetEntry (tWssIfWtpFwlRuleDB *);
INT4
WssIfWtpFwlRuleSetEntry (tWssIfWtpFwlRuleDB *);
INT4
WssIfWtpFwlAclGetEntry (tWssIfWtpFwlAclDB *);
INT4
WssIfWtpFwlAclSetEntry (tWssIfWtpFwlAclDB *);

INT4
WssIfWtpDynamicNatDBCreate (VOID);
INT4
WssIfWtpDynamicNatDBDelete (VOID);
INT4
WssIfCompareWtpDynamicNatRBTree (tRBElem * e1, tRBElem * e2);
INT4
WssIfWtpStaticNatDBCreate (VOID);
INT4
WssIfWtpStaticNatDBDelete (VOID);
INT4
WssIfCompareWtpStaticNatRBTree (tRBElem * e1, tRBElem * e2);
INT4
WssIfWtpStaticNatLocalAddrDBCreate (VOID);
INT4
WssIfWtpNatLocalAddrDBDelete (VOID);
INT4
WssIfCompareWtpNatLocalAddrRBTree (tRBElem * e1, tRBElem * e2);
INT4
WssIfWtpStaticNatGlobalAddrDBCreate (VOID);
INT4
WssIfWtpNatGlobalAddrDBDelete (VOID);
INT4
WssIfCompareWtpNatGlobalAddrRBTree (tRBElem * e1, tRBElem * e2);
INT4
WssIfWtpDynamicNatRBTreeAdd (tWssIfWtpDynamicNatDB  *);
INT4
WssIfWtpDynamicNatRBTreeGet(tWssIfWtpDynamicNatDB  *);
INT4
WssIfWtpDynamicNatRBTreeSet (tWssIfWtpDynamicNatDB  *);
INT4
WssIfWtpDynamicNatRBTreeDelete (tWssIfWtpDynamicNatDB  *);
INT4
WssIfWtpStaticNatRBTreeAdd (tWssIfWtpStaticNatDB  *);
INT4
WssIfWtpStaticNatRBTreeGet (tWssIfWtpStaticNatDB  *);
INT4
WssIfWtpStaticNatRBTreeSet (tWssIfWtpStaticNatDB  *);
INT4
WssIfWtpStaticNatRBTreeDelete (tWssIfWtpStaticNatDB  *);
INT4 WssIfWtpL3SubIfDBCreate (VOID);
INT4 WssIfWtpL3SubIfDBDelete (VOID);
INT4 WssIfWtpCompareL3SubIfRBTree (tRBElem * e1, tRBElem * e2);
INT4 WssIfWtpL3SubIfAddEntry (tWssIfWtpL3SubIfDB *);
INT4 WssIfWtpL3SubIfDeleteEntry (tWssIfWtpL3SubIfDB *);
INT4 WssIfWtpL3SubIfGetEntry (tWssIfWtpL3SubIfDB *);
INT4 WssIfWtpL3SubIfSetEntry (tWssIfWtpL3SubIfDB *);
INT4 WssIfWtpL3SubIfGetFirstEntry (UINT4 *, UINT4 *);
INT4 WssIfWtpL3SubIfGetNextEntry (UINT4, UINT4 *, UINT4, UINT4 *);
INT4 WssIfL3SubIfUpdateReq (tWssIfWtpL3SubIfDB *);
INT4 WssIfWtpL3SubIfDeleteEntryOnProfile (UINT4 u4WtpProfileId);
#endif 
