/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *   $Id: wssifwtpwlanproto.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ 
 *  Description: This file contains prototypes for functions used for
 *               wlc wsswlan module.
 *
 *  *********************************************************************/



#ifndef _WSSIFWTPWLANPROTO_H
#define _WSSIFWTPWLANPROTO_H


UINT1 WssWlanInit PROTO((VOID));

INT4 WssWlanIfIndexDBRBCmp PROTO ((tRBElem*, tRBElem*));

UINT1 WssWlanIfIndexDBCreate PROTO ((VOID));

UINT1 WssIfProcessWssWlanDBMsg PROTO ((UINT1 , tWssWlanDB*));

INT4 WssWlanInterfaceDBRBCmp PROTO ((tRBElem * e1, tRBElem * e2));

PUBLIC UINT1 WssWlanInterfaceDBCreate PROTO ((VOID));

INT4 WssWlanBSSIDMappingDBRBCmp PROTO ((tRBElem * e1, tRBElem * e2));

UINT1 WssWlanBSSIDMappingDBCreate PROTO ((VOID));

INT4 WssIfGetSsid PROTO ((UINT2 au2WlanIfIndex,BOOL1 bFlag,UINT1 * pu1GetEssid));

INT4 WssIfGetHideSsid PROTO ((UINT2 au2WlanIfIndex,BOOL1 bFlag,INT4 * pu1GetHdEssid));

INT4 WssIfGetHwAddress PROTO ((UINT2 au2WlanIfIndex,BOOL1 bFlag,UINT1 * pu1GetHwAddress));

INT4 WssIfGetDtimPeriod PROTO((UINT2 au2WlanIfIndex,BOOL1 bFlag,INT4 * pi4GetDtimPeriod));


#endif
