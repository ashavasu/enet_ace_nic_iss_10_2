/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 *   $Id: wssifwlcwlanextn.h,v 1.2 2017/11/24 10:37:10 siva Exp $ 

 *  Description: This file contains extern of global vaiables used
 *               for wsswlan module.
 *  *********************************************************************/


#ifndef  __WSSIF_WLCWLAN_EXTN_H
#define  __WSSIF_WLCWLAN_EXTN_H




PUBLIC UINT2 gu2WlanInternalIdCount;
PUBLIC tWssWlanFreeIndex gWssWlanFreeIndex[MAX_NUM_OF_SSID_PER_WLC];
PUBLIC tWssWlanGlobals gWssWlanGlobals;

PUBLIC tWssWlanProfileDB   gaWssWlanProfileDB[MAX_SSID_PROFILE_INDEX];
PUBLIC tWssWlanInternalProfileDB gaWssWlanInternalProfileDB[MAX_NUM_OF_SSID_PER_WLC];
PUBLIC UINT1 gu1LegacyStatus;
#ifdef BAND_SELECT_WANTED
PUBLIC UINT1 gu1BandSelectStatus;
#endif

#endif

