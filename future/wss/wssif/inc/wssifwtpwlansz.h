/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *   $Id: wssifwtpwlansz.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ 
 *  Description: This file contains WSSWLAN Sizing parameters
 *      *******************************************************************/


enum {
    WLAN_INTERFACE_DB_MAX_SIZING_ID,
    WTPWLAN_BSSID_MAPPING_DB_MAX_SIZING_ID,
    WTPWLAN_IFINDEX_DB_MAX_SIZING_ID,
    WSSIFWTPWLAN_MAX_SIZING_ID
};


#ifdef  _WSSIFWTPWLANSZ_C
tMemPoolId WSSIFWTPWLANMemPoolIds[ WSSIFWTPWLAN_MAX_SIZING_ID];
INT4  WssifwtpwlanSizingMemCreateMemPools(VOID);
VOID  WssifwtpwlanSizingMemDeleteMemPools(VOID);
INT4  WssifwtpwlanSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _WSSIFWTPWLANSZ_C  */
extern tMemPoolId WSSIFWTPWLANMemPoolIds[ ];
extern INT4  WssifwtpwlanSizingMemCreateMemPools(VOID);
extern VOID  WssifwtpwlanSizingMemDeleteMemPools(VOID);
#endif /*  _WSSIFWTPWLANSZ_C  */


#ifdef  _WSSIFWTPWLANSZ_C
tFsModSizingParams FsWSSIFWTPWLANSizingParams [] = {
{ "tWssWlanInterfaceDB", "WLAN_INTERFACE_DB_MAX", sizeof(tWssWlanInterfaceDB),WLAN_INTERFACE_DB_MAX, WLAN_INTERFACE_DB_MAX,0 },
{ "tWssWlanBSSIDMappingDB", "WTPWLAN_BSSID_MAPPING_DB_MAX", sizeof(tWssWlanBSSIDMappingDB),WTPWLAN_BSSID_MAPPING_DB_MAX, WTPWLAN_BSSID_MAPPING_DB_MAX,0 },
{ "tWssWlanIfIndexDB", "WTPWLAN_IFINDEX_DB_MAX", sizeof(tWssWlanIfIndexDB),WTPWLAN_IFINDEX_DB_MAX, WTPWLAN_IFINDEX_DB_MAX,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _WSSIFWTPWLANSZ_C  */
extern tFsModSizingParams FsWSSIFWTPWLANSizingParams [];
#endif /*  _WSSIFWTPWLANSZ_C  */


