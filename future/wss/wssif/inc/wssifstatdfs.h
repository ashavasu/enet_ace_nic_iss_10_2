/*  $Id: wssifstatdfs.h,v 1.3 2017/11/24 10:37:10 siva Exp $ */

#ifndef __WSSIFAUTH_TDFS_H__
#define __WSSIFAUTH_TDFS_H__

#include "wsssta.h"
#include "wlchdlr.h"
#define MAX_MSG_ELEM 6
#define WSSAUTH_MAX_BSS 5

#define WSS_STA_AUTH_ALGO_OPEN   1
#define WSS_STA_AUTH_ALGO_SHARED 2
#define WSS_STA_AUTH_ALGO_WEB    3

#define MAX_WEP_KEY_LENGTH       104
#define MAX_USER_BANDWIDTH       2048
#define MIN_USER_BANDWIDTH       256


#define WSS_RX_AUTH_ALGO_OPEN   0x0000
#define WSS_RX_AUTH_ALGO_SHARED 0x0100

typedef struct {
    UINT4          u4BssIfIndex;
    tMacAddr       staMacAddr;
    UINT2          u2SessId;
    eWssStaState   stationState;
    BOOL1          isPresent;
    UINT1          au1pad[3];
}tWssStaBssDB;

/* Stores the station's authentication and association status */
typedef struct {
    UINT4          u4RuleNumber;
    UINT4          u4StaConnTime;
    tMacAddr       stationMacAddress;
    UINT2          u2AssociationID;       /* this shall be provided by the
                                           *station module once the association
                                           *is established*/

    UINT1     au1StationAddedTime[256]; /*Station Added Time*/
    tRBNodeEmbd    nextWssIfAuthStateDB;
    tWssStaParams  staParams; /* Contains the Station association Info */
    tWssStaBssDB   aWssStaBSS[5];
    UINT1          au1StaRedirectionURL[STA_REDIRECTION_URL_LENGTH];
    UINT1          au1LoggedUserName[WSS_STA_WEBUSER_LEN_MAX];
    UINT4          u4LastUpdatedTime;
    UINT1          u1Index;
    UINT1          u1StaAuthFinished;
    UINT1          u1StaAuthRedirected;
    BOOL1          bStaAuthRedirected;
    BOOL1          bStaRedirectionURL;
    UINT1          au1pad[3];
}tWssIfAuthStateDB;

/* Stores the list of stations belonging to the Wlan */
typedef struct {
    UINT4              u4BssIfIndex;
    tWssIfAuthStateDB    *stationStateInfo;
}tWssStaRadioIfMapTable;

typedef struct {
    tRBNodeEmbd    nextWtpAssocCountInfo;
    UINT4          u4AssoCount;/* number of Stations associated*/
    UINT2          u2WtpId;
    UINT1          au1pad[2];
}tWssIfAuthWtpAssocCountInfo;


/* Station update action */
typedef enum{
   WSS_STA_NEW_STA_REQ = 0,
   WSS_STA_AUTHENTICATED,
   WSS_STA_OPEN_AUTHENTICATED,
   WSS_STA_ASSOCIATED,
   WSS_STA_DIASSOCIATED,
   WSS_STA_DEAUTHENTICATED
}eWssStaUpdateAction;

/* Station load balance info */
typedef struct {
    tRBNodeEmbd    nextStaLoadBalanceDB;
    UINT4          u4BssIfIndex;
    UINT4          u4AssocRejectionCount; /* number of times the
                                           * association is rejected */
    tMacAddr       stationMacAddress;
    UINT1          au1pad[2];
}tWssIfAuthLoadBalanceDB;

/* Stores info on the QoS Info configured by the operator
 * for a particular station belonging to a radioIfIndex */
typedef struct {
    UINT4        u4RadioIfIndex;
    tMacAddr     stationMacAddress;
    UINT1        u1Qospriority;
    UINT1        dscp;
}tWssStaQoSInfo;

/* For Add/delete station message element */ 
typedef struct {
    tMacAddr     stationMacAddress[MAX_MSG_ELEM];
    UINT1   u1MessageLength[MAX_MSG_ELEM];
    UINT2  u2MessageType;
    BOOL1        isPresent;
    UINT1        u1NumStations;
    UINT1        u1RadioId;
    UINT1          au1pad[1];
}tAddDelStation;

/* For vendor specific payload message element.
   This is not used currently. Still we provide the structure for it */
typedef struct {
    UINT4  u4VendorId;
    UINT2  u2MessageType;
    UINT2  u2ElementId;
    BOOL1  isPresent;
    UINT1  u1MessageLength;
    UINT1  au1Data[2048];
    UINT1 au1pad[2];
}tVendSpecPayload;

/* For IEEE 802.11 Station message element */
typedef struct {    
    UINT2   u2MessageType;
    UINT2         u2Capab;
    UINT2         u2AssocId;
    UINT1         u1MessageLength;
    UINT1         u1RadioId;
    UINT1         u1Flags;
    tMacAddr      stationMacAddress;
    BOOL1         isPresent;
    UINT1         u1WlanId;
    UINT1         au1SuppRates[126];
    UINT1         au1pad[1];
}tDot11Station;

/* For 802.11 Station Session Key message element */
typedef struct {
    tMacAddr     stationMacAddress;
    UINT2     u2MessageType;
    UINT2  u2Flags;
    UINT1  u1MessageLength;
    UINT1        au1PairwiseTSC[6];
    UINT1        au1PairwiseRSC[6];
    UINT1        au1Key[256];
    BOOL1        isPresent;
}tDot11Sesskey;

/* For 802.11 Station QoS Profile */
typedef struct {
    tMacAddr    stationMacAddress;
    UINT2  u2MessageType;
    UINT1  u1MessageLength;
    BOOL1       isPresent;
    UINT1  u180211Priority;
    UINT1       au1pad[1];
}tDot11QoSProfile;

/* For 802.11 Update Station QoS */
typedef struct {
    tMacAddr    stationMacAddress;
    UINT2     u2QoSElement;
    UINT2  u2MessageType;
    UINT1  u1MessageLength;
    UINT1        u1RadioId;
    BOOL1       isPresent;
    UINT1       au1pad[3];
}tDot11UpdateStaQoS;

typedef enum{
    /* Authentication module */
    WSS_AUTH_PROCESS_AUTH = 0,
    WSS_AUTH_PROCESS_ASSOC,
    WSS_AUTH_PROCESS_DEAUTH,
    WSS_AUTH_DEL_STATION,
    /* Station module */
    WSS_STA_UPDATE_STATE_DB,
    WSS_STA_DELETE_STATION,
    WSS_STA_UPDATE_LOADBAL_DB
}eWssIfAuthMsgTypes;

/* For Add/delete station message element */
typedef struct {
    tMacAddr     stationMacAddress;
    UINT2        u2MessageType;
    UINT1        u1MessageLength;
    UINT1        u1RadioId;
    UINT1        u1MacAddrLen;
    UINT1        au1VlanName[512];
    BOOL1        isPresent;
}tWssAuthAddDelStation;

/* Station config info */
typedef struct {
    tWssAuthAddDelStation     addDelSta;
    tVendSpecPayload          vendSpec;
    tDot11Station             staMsg;
    tDot11Sesskey             staSessKey;
    tDot11QoSProfile          staQosProfile;
    tDot11UpdateStaQoS        staQosUpdate;
}tWssStaConfigInfo;

typedef enum {
    WSS_AUTH_UPDATE_DB,
    WSS_AUTH_SET_DB,
    WSS_AUTH_GET_STATION_DB,
    WSS_AUTH_SET_ASSOC_REJECT_COUNT_DB,
    WSS_AUTH_SET_WTP_ASSOC_COUNT_DB,
    WSS_AUTH_GET_WTP_ASSOC_COUNT_DB,
    WSS_AUTH_GET_STA_ASSOC_REJECT_COUNT_DB,
    WSS_AUTH_DELETE_DB,
    WSS_STA_WEBAUTH_INIT_DB,
    WSS_STA_WEBAUTH_CREATE_DB,
    WSS_STA_WEBAUTH_SET_DB,
    WSS_STA_WEBAUTH_GET_USER,
    WSS_STA_WEBAUTH_AUTHENTICATE_USER,
    WSS_STA_WEB_AUTH_DB_WALK,
    WSS_STA_WEBAUTH_DELETE_DB,
    WSS_AUTH_DB_UPDATE_RULENUM,
    WSS_AUTH_GET_BSSSID_STATION_STATS,
    WSS_AUTH_ADD_BSSSID_STATION_STATS,
    WSS_AUTH_DEL_BSSSID_STATION_STATS,
    WSS_STA_GET_CLIENT_COUNT_PER_BSSSID,
    WSS_STA_GET_CLIENT_MAC_PER_BSSID,
    WSS_STA_WEBAUTH_UPDATE_LIFESPAN,
    WSS_STA_RADIO_DEAUTH
}eWssDBMsgType;

typedef struct {
    UINT4 u4BssIfIndex;
    tWssStaParams WssStaParams;
    eWssStaUpdateAction action;
    tMacAddr staMacAddr;
    UINT2 u2Aid;
    UINT2 u2SessId;
    UINT1 au1pad[2];
}tWssStaDB;

/* WebAuthenticatio DB */
/* Internal DB to store the user details */
typedef struct{
    tRBNodeEmbd    nextWebAuthDB;
    UINT4 u4MaxUserLifetime; /* In minutes */
    UINT4 u4WlanId; /* Wlan profile Id */
    UINT4 u4TimerTickCount;/* Increased for every time tick */
    UINT4 u4RuleNumber;
    UINT2 u2AuthState; /* Either 0 or 1 */
    UINT1 au1UserName[WSS_STA_WEBUSER_LEN_MAX];
    UINT1 au1UserPwd[WSS_STA_WEBUSER_LEN_MAX];
    UINT1 au1UserEmailId[256];
    UINT1 u1RowStatus;
    UINT1 au1pad[1];
}tWssIfWebAuthDB;
/* Structure to say if the User config params are set */
typedef struct{
    BOOL1 bUserName;
    BOOL1 bMaxUserLifetime; 
    BOOL1 bWlanProfileId;
    BOOL1 bRowStatus;
    BOOL1 bUserEmailId;
    BOOL1 bUserPwd;
    UINT1 au1pad[2];
}tWssIfIsSetWebAuthDB;

typedef struct {
UINT4    authReqRcvd;
UINT4    deAuthReqSent;
UINT4    unknownAuthReqRcvd;
UINT4    invalidAuthReqRcvd;
UINT4    authFailureCount;
UINT4    unknownReAuthReqRcvd;
UINT4    invalidReAuthReqRcvd;
UINT4    asscReqRcvd;
UINT4    reAsscReqRcvd;
UINT4    asscFailure;
UINT4    unknownReAsscReqRcvd;
UINT4    invalidReAssocReqRcvd;                 
UINT4    handOffReqRcvd;
UINT4    handOffReqSucc;
UINT4    handOffReqFailure;                     /* Mobility Related Stats */
UINT4    stnReqRejAbvLimit;
UINT4    stnReqAccAbvLimit;                     /* Load Balancing Related Stats*/
}tBSSIDStnStat;                                /* WLAN BSSID Station Stats collected from WLC context */


/* Stores the station's authentication and association status */
typedef struct {
    tRBNodeEmbd    nextWssIfBssIdStatDB;
    UINT4          u4BssIfIndex;
    tBSSIDStnStat  bssIdStnStats;
}tWssIfBssIdStatsDB;

/* Stores the station's authentication and association status */
typedef struct {
    UINT4          u4BssIfIndex;
    UINT1          u1ClientCount;
    UINT1          au1pad[3];

}tWssIfBssIdStaCountDB;

typedef enum{
    WSSSTA_WEB_USER_AUTHENTICATED = 1
}eWssIfWebAuthStatus;

/* Structure to store the list of usernames temporarily */
typedef struct {
        UINT1 au1UserName[WSS_STA_WEBUSER_LEN_MAX];
}tWssStaWebAuthDBWalk;

/* Internal DB to store the original URL the user has requested for */
typedef struct{
    tRBNodeEmbd    nextWebAuthTempDB;
    tMacAddr StaMacAddr;
    UINT1    au1ReqUrl[512]; /* The URL which the Station has requested for */
    UINT1    au1pad[2];
}tWssIfWebAuthTempDB;

typedef struct {
    tWssStaDB                     WssStaDB;
    tWssIfAuthStateDB             WssIfAuthStateDB;
    tWssIfAuthWtpAssocCountInfo   WssStaWtpAssocCountInfo;
    tWssIfAuthLoadBalanceDB       WssStaLoadBalanceDB;
    tWssIfWebAuthDB               WssStaWebAuthDB;
    tWssIfWebAuthTempDB           WssStaWebAuthTempDB;
    tWssIfBssIdStatsDB            WssBssStnStatsDB;
    tWssIfBssIdStaCountDB         WssBssStnCountDB;
}tWssifauthDBMsgStruct;

typedef struct {
    tMacAddr       stationMacAddress;
    UINT2          u2WlanProfileId;
    tRBNodeEmbd    nextWssStaAuthSessionDB;
    UINT4          u4RuleNumber;
    UINT4          u4BssIfIndex;
    UINT4          u4LastUpdatedTime;
    UINT4          u4UserLifeTime;
    UINT4          u4StaDeAuthTime;
    UINT1          u1StaAuthFinished;
    UINT1          au1pad[3];
    UINT1          au1StaRedirectionURL[STA_REDIRECTION_URL_LENGTH];
    UINT1          au1LoggedUserName[WSS_STA_WEBUSER_LEN_MAX];
}tWssStaAuthSessionDB;

typedef struct {
    tMacAddr        stationMacAddress;
    UINT2           u2WlanProfileId;
}tWssStaAuthSessionDBWalk;

#define WEBAUTH_INTERNAL        1
#define WEBAUTH_EXTERNAL        2

#define WEBAUTH_HTTP_GET        1
#define WEBAUTH_HTTP_POST       2

#endif
