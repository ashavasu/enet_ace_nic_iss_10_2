/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved             *
 *  $Id: wssifstawtpsz.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $                                                                  *
 * Description: MempoolIds, Block size information for all          *
 *              the pools in WSSSTA                                 *
 ********************************************************************/

enum {
#ifdef BAND_SELECT_WANTED
    MAX_STA_PER_AP_SIZING_ID,
#endif
    MAX_WSSSTA_AP_STATION_SIZING_ID,
    MAX_WSSSTA_AP_VLAN_SIZING_ID,
    MAX_WSS_TCFILTER_HANDLE_SIZING_ID,
    WSSIFSTAWTP_MAX_SIZING_ID
};


#ifdef  _WSSIFSTAWTPSZ_C
tMemPoolId WSSIFSTAWTPMemPoolIds[ WSSIFSTAWTP_MAX_SIZING_ID];
INT4  WssifstawtpSizingMemCreateMemPools(VOID);
VOID  WssifstawtpSizingMemDeleteMemPools(VOID);
INT4  WssifstawtpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _WSSIFSTAWTPSZ_C  */
extern tMemPoolId WSSIFSTAWTPMemPoolIds[ ];
extern INT4  WssifstawtpSizingMemCreateMemPools(VOID);
extern VOID  WssifstawtpSizingMemDeleteMemPools(VOID);
#endif /*  _WSSIFSTAWTPSZ_C  */


#ifdef  _WSSIFSTAWTPSZ_C
tFsModSizingParams FsWSSIFSTAWTPSizingParams [] = {
#ifdef BAND_SELECT_WANTED
{ "tWssStaBandSteerDB", "MAX_STA_PER_AP", sizeof(tWssStaBandSteerDB),MAX_STA_PER_AP, MAX_STA_PER_AP,0 },
#endif
{ "tWssStaStateDB", "MAX_WSSSTA_AP_STATION", sizeof(tWssStaStateDB),MAX_WSSSTA_AP_STATION, MAX_WSSSTA_AP_STATION,0 },
{ "tWssStaVlanDB", "MAX_WSSSTA_AP_VLAN", sizeof(tWssStaVlanDB),MAX_WSSSTA_AP_VLAN, MAX_WSSSTA_AP_VLAN,0 },
{ "tWssStaTCFilterHandle", "MAX_WSS_TCFILTER_HANDLE", sizeof(tWssStaTCFilterHandle),MAX_WSS_TCFILTER_HANDLE, MAX_WSS_TCFILTER_HANDLE,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _WSSIFSTAWTPSZ_C  */
extern tFsModSizingParams FsWSSIFSTAWTPSizingParams [];
#endif /*  _WSSIFSTAWTPSZ_C  */


