/*****************************************************************************/
/* Copyright (C) 2013 Aricent Inc . All Rights Reserved                      */
/* $Id: wssifstatrc.h,v 1.2 2017/05/23 14:16:57 siva Exp $                                                                          */
/* Licensee Aricent Inc.,                                                    */
/*****************************************************************************/
/*    FILE  NAME            : Wssstatrc.h                                    */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : WSSSTA module                                  */
/*    MODULE NAME           : WSSSTA module                                  */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains declarations of traces used */
/*                            in WSSSTA Module.                              */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                      DESCRIPTION OF CHANGE/              */
/*            MODIFIED BY                FAULT REPORT NO                     */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*---------------------------------------------------------------------------*/
#ifndef __WSSIFAUTH_TRC_H__
#define __WSSIFAUTH_TRC_H__

#define  WSSSTA_MOD                ((const char *)"WSSSTA")
#define WSSSTA_MASK                gu4StationBandTrace
#define WSSSTAWEBAUTH_MASK         gu4WssStaWebAuthDebugMask 

/* if WSSSTA Trace/Debug messages wanted this flags can be enabled
 *  by repacing the above WSSWLAN_MASK macro value with the below
 * #define WSSSTA_MASK  \
   WSSSTA_MGMT_TRC | WSSSTA_FAILURE_TRC | WSSSTA_ENTRY_TRC | WSSSTA_EXIT_TRC
 */
/* Trace and debug flags */

#define WSSSTA_FN_ENTRY() \
    MOD_FN_ENTRY (WSSSTA_MASK, WSSSTA_ENTRY_TRC,WSSSTA_MOD)

#define WSSSTA_FN_EXIT() MOD_FN_EXIT (WSSSTA_MASK, WSSSTA_EXIT_TRC,WSSSTA_MOD)

#define WSSSTA_PKT_DUMP(mask, pBuf, Length, fmt)                           \
        MOD_PKT_DUMP(WSSSTA_PKT_DUMP_TRC,mask, WSSSTA_MOD, pBuf, Length, fmt)
#define WSSSTA_TRC(mask, fmt)\
      MOD_TRC(WSSSTA_MASK, mask, WSSSTA_MOD, fmt)
#define WSSSTA_TRC1(mask,fmt,arg1)\
      MOD_TRC_ARG1(WSSSTA_MASK,mask,WSSSTA_MOD,fmt,arg1)
#define WSSSTA_TRC2(mask,fmt,arg1,arg2)\
      MOD_TRC_ARG2(WSSSTA_MASK,mask,WSSSTA_MOD,fmt,arg1,arg2)
#define WSSSTA_TRC3(mask,fmt,arg1,arg2,arg3)\
      MOD_TRC_ARG3(WSSSTA_MASK,mask,WSSSTA_MOD,fmt,arg1,arg2,arg3)
#define WSSSTA_TRC4(mask,fmt,arg1,arg2,arg3,arg4)\
      MOD_TRC_ARG4(WSSSTA_MASK,mask,WSSSTA_MOD,fmt,arg1,arg2,arg3,arg4)

#ifdef BAND_SELECT_WANTED
#define WSSSTA_TRC5(mask,fmt,arg1,arg2,arg3,arg4,arg5)\
      MOD_TRC_ARG5(WSSSTA_MASK,mask,WSSSTA_MOD,fmt,arg1,arg2,arg3,arg4,arg5)

#define WSSSTA_TRC6(mask,fmt,arg1,arg2,arg3,arg4,arg5,arg6)\
      MOD_TRC_ARG6(WSSSTA_MASK,mask,WSSSTA_MOD,fmt,arg1,arg2,arg3,arg4,arg5,arg6)

#endif

#define WSSSTA_WEBAUTH_TRC(mask, fmt)\
      MOD_TRC(WSSSTAWEBAUTH_MASK, mask, WSSSTA_MOD, fmt)
#define WSSSTA_WEBAUTH_TRC1(mask,fmt,arg1)\
      MOD_TRC_ARG1(WSSSTAWEBAUTH_MASK,mask,WSSSTA_MOD,fmt,arg1)
#define WSSSTA_WEBAUTH_TRC2(mask,fmt,arg1,arg2)\
      MOD_TRC_ARG2(WSSSTAWEBAUTH_MASK,mask,WSSSTA_MOD,fmt,arg1,arg2)
#define WSSSTA_WEBAUTH_TRC3(mask,fmt,arg1,arg2,arg3)\
      MOD_TRC_ARG3(WSSSTAWEBAUTH_MASK,mask,WSSSTA_MOD,fmt,arg1,arg2,arg3)
#define WSSSTA_WEBAUTH_TRC4(mask,fmt,arg1,arg2,arg3,arg4)\
      MOD_TRC_ARG4(WSSSTAWEBAUTH_MASK,mask,WSSSTA_MOD,fmt,arg1,arg2,arg3,arg4)
#define WSSSTA_WEBAUTH_TRC5(mask,fmt,arg1,arg2,arg3,arg4,arg5)\
      MOD_TRC_ARG5(WSSSTAWEBAUTH_MASK,mask,WSSSTA_MOD,fmt,arg1,arg2,arg3,arg4,arg5)
#define WSSSTA_WEBAUTH_TRC6(mask,fmt,arg1,arg2,arg3,arg4,arg5,arg6)\
      MOD_TRC_ARG6(WSSSTAWEBAUTH_MASK,mask,WSSSTA_MOD,fmt,arg1,arg2,arg3,arg4,arg5,arg6)

#endif


