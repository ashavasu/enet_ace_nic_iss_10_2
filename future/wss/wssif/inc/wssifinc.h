/** $Id: wssifinc.h,v 1.3 2017/11/24 10:37:09 siva Exp $ **/
#ifndef __WSSIF_INC_H__
#define __WSSIF_INC_H__

#include "lr.h"
#include "cli.h"
#include "cfa.h"
#include "trace.h"
#include "secmod.h"
#include "secidscli.h"

#include "radioif.h"
#include "wsswlan.h"
#include "wssifwlanconst.h"

#ifdef RFMGMT_WANTED
#include "rfmgmt.h"
#endif

#ifdef WLC_WANTED
#include "wssifwlcwlanmacr.h"
#include "wssifwlcwlandb.h"
#include "wssifwlcwlansz.h"
#define DEFAULT_MULTICAST_TABLE_LENGTH 64
#define DEFAULT_MULTICAST_SNOOP_TIMER 30000
#define DEFAULT_MULTICAST_SNOOP_TIMEOUT 120000
#define APGROUP_ENABLE 1
#define APGROUP_DISABLE 2
#endif

#ifdef WTP_WANTED
#include "wssifwtpwlanmacr.h"
#include "wssifwtpwlandb.h"
#include "wssifwtpwlansz.h"
#include "wssifstawtpsz.h"
#endif

#include "wssifconst.h"
#include "wssiftdfs.h"

#ifdef _WSSIF_MAIN_C
#include "wssifglob.h"
#else
#include "wssifextn.h"
#endif

#include "wssifradioconst.h"
#include "wssifradiodb.h"
#include "wssifradioproto.h"
#include "wssifproto.h"
#include "wssiftrc.h"
#include "iss.h"

#ifdef WLC_WANTED
#include "wssifwlcwlanproto.h"
#ifdef __WSSIFWLCWLANDB_C__ 
#include "wssifwlcwlanglob.h"
#else
#include "wssifwlcwlanextn.h"
#endif
#endif

#ifdef WTP_WANTED
#include "wssifwtpwlanproto.h"
#ifdef __WSSIFWTPWLANDB_C__ 
#include "wssifwtpwlanglob.h"
#else
#include "wssifwtpwlanextn.h"
#endif
#endif
#include "wssifradiosz.h"
#ifdef __WSSIFRADIOIFDB_C__
#include "wssifradioglob.h"
#else
#include "wssifradioextn.h"
#endif

#include "wssifcapdb.h"
#include "wssifcapmacr.h"
#include "wssifcapproto.h"

#ifdef WSSMAC_WANTED
#include "wssmac.h"
#endif

#ifdef BCNMGR_WANTED
#include "bcnmgr.h"
#endif

#ifdef CAPWAP_WANTED
#include "capwap.h"
#endif 

#ifdef APHDLR_WANTED
#include "aphdlr.h"
#include "cfaport.h"
#endif

#ifdef WLCHDLR_WANTED
#include "wlchdlr.h"
#endif
#ifdef WLC_WANTED
#include "wssifstaextn.h"
#include "wssifstaconst.h"
#include "wssifstatdfs.h"
#include "wssifstadef.h"
#include "wssifstamacr.h"
#include "wssifstatrc.h"
#include "wssifstaproto.h"
#include "wsssta.h"
#include "wsspm.h"
#include "wssifauthsz.h"
#include "wssifwebauthsz.h"
#include "wsscfgcli.h"
#endif

#ifdef WSSSTA_WANTED
#include "wsssta.h"
#endif
#define REBOOT_COUNT_NOT_AVAILABLE 65535
#include "wssifcapsz.h"

#define FS11AC_MCS_HALFVALUE 4
#define FS11AC_MCS_SPATIALVALUE 8
#define FS11AC_MCS_SHIFTVALUE 6
#define FS11AC_MCS_SHIFTMINUS 2
#define FS11AC_MCS_SHIFTLEFT 0
#define FS11AC_MCS_CONFIGURABLESS 3
#define FS11AC_MCS_SSRESERVED 3
#define FS11AC_SIZE8 8
#define FS11AC_SIZE2 2
#define FS11AC_RATE_SHIFT8 8
#define FS11AC_RATE_SHIFT3 3
#define FS11AC_MPDU_ZERO 0
#define FS11AC_MPDU_ONE 1
#define FS11AC_MPDU_TWO 2
#define FS11AC_AMPDU_MAX 7
#define FS11AC_AMPDU_MIN 0
#define FS11AC_CHAN_MAX 4
#define FS11AC_CHAN_MIN 0
#define FS11AC_CENTERFRQ_MAX 200
#define FS11AC_MCS_RATE7 0
#define FS11AC_MCS_RATE8 1
#define FS11AC_MCS_RATE9 2

#endif

