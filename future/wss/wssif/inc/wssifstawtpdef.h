/******************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                       *
 *  $Id: wssifstawtpdef.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *  *
 * Description: This file contains the macro for WSSIFSTA                     *
 ******************************************************************************/
#ifndef __WSSSTA_WTP_DEF_H__
#define __WSSSTA_WTP_DEF_H__

#define MAX_WSSSTA_AP_STATION   MAX_STA_SUPP_PER_AP
#define MAX_WSSSTA_AP_VLAN    4096
#define WSSSTA_AP_STADB_POOLID \
    WSSIFSTAWTPMemPoolIds[MAX_WSSSTA_AP_STATION_SIZING_ID]
#define WSSSTA_AP_VLANDB_POOLID \
    WSSIFSTAWTPMemPoolIds[MAX_WSSSTA_AP_VLAN_SIZING_ID]
#define WSSSTA_TCFILTER_HANDLE_POOLID \
    WSSIFSTAWTPMemPoolIds[MAX_WSS_TCFILTER_HANDLE_SIZING_ID]
#ifdef BAND_SELECT_WANTED
#define WSSSTA_AP_BANDSTEERDB_POOLID \
    WSSIFSTAWTPMemPoolIds[MAX_STA_PER_AP_SIZING_ID]
#endif

/*Trace Level*/
#define WSSSTA_MGMT_TRC                    0x00000001
#define WSSSTA_INIT_TRC                    0x00000002
#define WSSSTA_ENTRY_TRC                   0x00000004
#define WSSSTA_EXIT_TRC                    0x00000008
#define WSSSTA_FAILURE_TRC                 0x00000010
#define WSSSTA_BUF_TRC                     0x00000020
#define WSSSTA_SESS_TRC                    0x00000040
#define WSSSTA_INFO_TRC                    0x00000080

#define WSSSTA_RB_GREATER 1
#define WSSSTA_RB_EQUAL   0
#define WSSSTA_RB_LESS    -1

#endif
