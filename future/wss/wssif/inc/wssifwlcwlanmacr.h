/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *  
 *  $Id: wssifwlcwlanmacr.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ wssifwlcwlanmacr.h 
 *  
 *  Description: This file contains macros used for wsswlan module.
 *
 *  *********************************************************************/



#ifndef _WSSIF_WLAN_WLCMACR_H_
#define _WSSIF_WLAN_WLCMACR_H_

#define WSSWLAN_IF_INDEX_DB_POOLID \
    WSSIFWLCWLANMemPoolIds[WLAN_IFINDEX_DB_MAX_SIZING_ID]

#define WSSWLAN_BSS_INTERFACE_DB_POOLID \
    WSSIFWLCWLANMemPoolIds[WLAN_BSSINTERFACE_DB_MAX_SIZING_ID]

#define WSSWLAN_BSS_IFINDEX_DB_POOLID \
    WSSIFWLCWLANMemPoolIds[WLAN_BSSIFINDEX_DB_MAX_SIZING_ID]

#define WSSWLAN_BSSID_MAPPING_DB_POOLID \
    WSSIFWLCWLANMemPoolIds[WLAN_BSSID_MAPPING_DB_MAX_SIZING_ID]

#define WSSWLAN_SSID_MAPPING_DB_POOLID \
    WSSIFWLCWLANMemPoolIds[WLAN_SSID_MAPPING_DB_MAX_SIZING_ID]

#endif

