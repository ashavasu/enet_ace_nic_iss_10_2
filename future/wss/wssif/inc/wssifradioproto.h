/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wssifradioproto.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains RadioIf function prototypes
 *******************************************************************/

#ifndef _WSSIFRADIOIFPROTO_H
#define _WSSIFRADIOIFPROTO_H


PUBLIC UINT1 RadioIfDBCreate PROTO ((VOID));

INT4 RadioIfDBRBCmp PROTO ((tRBElem * e1, tRBElem * e2)); 

UINT1 WssIfProcessRadioIfDBMsg  PROTO ((UINT1 , tRadioIfGetDB *));

UINT1 RadioIfInit PROTO ((VOID));

UINT1 RadioIfGetTxPowerTable PROTO ((tRadioIfGetDB *));

UINT1 RadioIfGetAllParams PROTO ((tRadioIfGetDB *));

INT4 WssIfGetChannel PROTO((UINT4 u4radioindex,BOOL1 bFlag,UINT2 * u2channel));

INT4 WssIfGetTxpower PROTO((UINT4 u4radioindex,BOOL1 bFlag,INT4 * pi4PwrLevel));

INT4 WssIfGetBeaconInterval PROTO((UINT4 u4radioindex,BOOL1 bFlag,UINT4 *pu4BeaconItrval));

INT4 WssIfGetAifs PROTO((UINT4 u4radioindex,BOOL1 bFlag,INT4 * pi4ValDot11EDCATbleAifs,
                                                                       INT4  i4EDCATbleIndex));

INT4 WssIfGetCwmin PROTO((UINT4 u4radioindex,BOOL1 bFlag,INT4 * i4ValEDCATbleCWmin,
                                                                      INT4  i4EDCATbleIndex));

INT4 WssIfGetCwmax PROTO((UINT4 u4radioindex,BOOL1 bFlag,INT4 * i4ValEDCATbleCWmax,
                                                                      INT4  i4EDCATbleIndex));

INT4 WssIfGetMaxpower PROTO((UINT4 u4radioindex,BOOL1 bFlag,UINT4 * pu4PwrLevel));

INT4 WssIfGetDot11Frequency PROTO((UINT4 ,UINT1 *));
INT4 WssIfGetDot11FragThreshold PROTO((UINT4 ,UINT2 *));
INT4 WssIfGetDot11RTSThreshold PROTO((UINT4 ,UINT2 *));
INT4 WssIfGetDot11Country PROTO((UINT4 ,UINT1 *));
INT4 WssIfGetAntennaDiversity PROTO((UINT4 ,UINT4 *));
VOID WssIfSetDefault11AcParams PROTO((tRadioIfDB *));
VOID WssIfAssembleOperModeNotify PROTO((tRadioIfGetDB *));
VOID WssIfAssembleVhtPowerEnvelope PROTO((tRadioIfGetDB *));
UINT1 WssIfComputeBandWidth PROTO((tRadioIfGetDB *));
VOID WssIfVhtHighestSuppDataRate PROTO((tRadioIfGetDB *));
UINT1
WssIfObtainOperClass PROTO ((UINT1, UINT1));
VOID
WssIfSetDefault11nParams PROTO ((tRadioIfDB *pRadioIfDB));

VOID
WssIfDot11nSplStreamAndUnequal PROTO ((tRadioIfGetDB *pRadioIfGetDB));

VOID
WssIfDot11nHighestSuppDataRate PROTO ((tRadioIfGetDB *pRadioIfGetDB));

VOID
WssIfVhtSts PROTO ((tRadioIfGetDB *pRadioIfGetDB, UINT1 *u1Sts));

#endif
