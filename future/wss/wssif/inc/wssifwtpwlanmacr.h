/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *   $Id: wssifwtpwlanmacr.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ 
 *  Description: This file contains macros used for wsswlan module.
 *
 *  *********************************************************************/



#ifndef _WSSIF_WLAN_WTPMACR_H_
#define _WSSIF_WLAN_WTPMACR_H_

#include "wssifinc.h"

#define  WSSWLAN_INTERFACE_DB_POOLID \
    WSSIFWTPWLANMemPoolIds[WLAN_INTERFACE_DB_MAX_SIZING_ID]

#define WSSWLAN_IF_INDEX_DB_POOLID \
    WSSIFWTPWLANMemPoolIds[WTPWLAN_IFINDEX_DB_MAX_SIZING_ID]

#define WSSWLAN_BSSID_MAPPING_DB_POOLID \
    WSSIFWTPWLANMemPoolIds[WTPWLAN_BSSID_MAPPING_DB_MAX_SIZING_ID]
#endif

