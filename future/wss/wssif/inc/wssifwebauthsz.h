/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved             *
 *  $Id: wssifwebauthsz.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $                                                                  *
 * Description: MempoolIds, Block size information for all          *
 *              the pools in WSSSTA                                 *
 ********************************************************************/

enum {
    MAX_WSSIFWEBAUTH_STATION_SIZING_ID,
    MAX_WSSIFWEBAUTH_TEMP_STATION_SIZING_ID,
    WSSIFWEBAUTH_MAX_SIZING_ID
};


#ifdef  _WSSIFWEBAUTHSZ_C
tMemPoolId WSSIFWEBAUTHMemPoolIds[ WSSIFWEBAUTH_MAX_SIZING_ID];
INT4  WssifwebauthSizingMemCreateMemPools(VOID);
VOID  WssifwebauthSizingMemDeleteMemPools(VOID);
INT4  WssifwebauthSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _WSSIFWEBAUTHSZ_C  */
extern tMemPoolId WSSIFWEBAUTHMemPoolIds[ ];
extern INT4  WssifwebauthSizingMemCreateMemPools(VOID);
extern VOID  WssifwebauthSizingMemDeleteMemPools(VOID);
#endif /*  _WSSIFWEBAUTHSZ_C  */


#ifdef  _WSSIFWEBAUTHSZ_C
tFsModSizingParams FsWSSIFWEBAUTHSizingParams [] = {
{ "tWssIfWebAuthDB", "MAX_WSSIFWEBAUTH_STATION", sizeof(tWssIfWebAuthDB),MAX_WSSIFWEBAUTH_STATION, MAX_WSSIFWEBAUTH_STATION,0 },
{ "tWssIfWebAuthTempDB", "MAX_WSSIFWEBAUTH_TEMP_STATION", sizeof(tWssIfWebAuthTempDB),MAX_WSSIFWEBAUTH_TEMP_STATION, MAX_WSSIFWEBAUTH_TEMP_STATION,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _WSSIFWEBAUTHSZ_C  */
extern tFsModSizingParams FsWSSIFWEBAUTHSizingParams [];
#endif /*  _WSSIFWEBAUTHSZ_C  */


