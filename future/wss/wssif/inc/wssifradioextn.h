/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wssifradioextn.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains RadioIf extern parameters
 *******************************************************************/

#ifndef  __WSSIF_RADIO_EXTN_H
#define  __WSSIF_RADIO_EXTN_H

PUBLIC tRadioIfGlobals gRadioIfGlobals;


#endif

