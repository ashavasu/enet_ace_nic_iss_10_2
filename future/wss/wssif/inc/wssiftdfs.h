/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wssiftdfs.h,v 1.2 2017/05/23 14:16:57 siva Exp $
 *
 * Description: This file contains the typedefs for WSS Modules.
 *******************************************************************/


#ifndef __WSSIF_TDFS_H__
#define __WSSIF_TDFS_H__

#include "wssifconst.h"

typedef struct WSSIF_GLOBALS {
    UINT4               u4WsscfgTrc;
    UINT1               au1TaskSemName[8];
    tOsixSemId          wssifTaskSemId;
} tWssIfGlobals;

typedef struct{
    tRBNodeEmbd     nextWssStaTCFilterHandle;
    tMacAddr        StationMacAddr;
    UINT1           au1pad[2];
    UINT4           u4TCFilterHandle;
    UINT4           u4StationIp;
}tWssStaTCFilterHandle;

typedef UINT1 tTCFilterHandleList[WSS_USER_TC_HANDLE_LIST_SIZE];

#endif
