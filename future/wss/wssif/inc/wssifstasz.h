/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved             *
 * $Id: wssifstasz.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $                                                                 *
 * Description: MempoolIds, Block size information                  *
 * for all the pools in WSSIFSTA                                    *
 ********************************************************************/


#if 0/* This file is no longer needed.Because content of this file is present in wssifauthsz.h and wssifwebauthsz.h*/
enum {
    MAX_WSSIFAUTH_STATION_SIZING_ID,
    MAX_WSSIFAUTH_LOADSTATION_SIZING_ID,
    MAX_WSSIFAUTH_ASSOC_COUNT_SIZING_ID,
    MAX_WSSIFAUTH_BSSID_STATS_SIZING_ID,
    WSSIFAUTH_MAX_SIZING_ID
};

#ifdef  __WSSIFAUTH_UTIL__C
tMemPoolId gaWSSIFAUTHMemPoolIds[WSSIFAUTH_MAX_SIZING_ID];
INT4  WssIfAuthSizingMemCreateMemPools(VOID);
VOID  WssIfAuthSizingMemDeleteMemPools(VOID);
INT4  WssIfAuthSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  __WSSIFAUTH_UTIL__C  */
extern tMemPoolId gaWSSIFAUTHMemPoolIds[WSSIFAUTH_MAX_SIZING_ID];
extern INT4  WssIfAuthSizingMemCreateMemPools(VOID);
extern VOID  WssIfAuthSizingMemDeleteMemPools(VOID);
#endif /*  __WSSIFAUTH_UTIL__C  */

#ifdef  __WSSIFAUTH_UTIL__C
tFsModSizingParams gaFsWSSIFAUTHSizingParams [] = {
{ "tWssIfAuthStateDB", "MAX_WSSIFAUTH_STATION", sizeof(tWssIfAuthStateDB),
    MAX_WSSIFAUTH_STATION, MAX_WSSIFAUTH_STATION, 0},
{"tWssIfAuthLoadBalanceDB", "MAX_WSSIFAUTH_LOADSTATION",
    sizeof(tWssIfAuthLoadBalanceDB), MAX_WSSIFAUTH_LOADSTATION,
    MAX_WSSIFAUTH_LOADSTATION, 0},
{"tWssIfAuthWtpAssocCountInfo", "MAX_WSSIFAUTH_ASSOC_COUNT", 
    sizeof(tWssIfAuthWtpAssocCountInfo), MAX_WSSIFAUTH_ASSOC_COUNT,
    MAX_WSSIFAUTH_ASSOC_COUNT, 0},
{"tWssIfBssIdStatsDB", "MAX_WSSIFAUTH_LOADSTATION", sizeof(tWssIfBssIdStatsDB),
    MAX_WSSIFAUTH_STATION, MAX_WSSIFAUTH_STATION, 0},
};
#else  /*  __WSSIFAUTH_UTIL__C  */
extern tFsModSizingParams gaFsWSSIFAUTHSizingParams [];
#endif /*  __WSSIFAUTH_UTIL__C  */

/* Web Authentication */
enum {
    MAX_WSSIFWEBAUTH_SIZING_ID,
    MAX_WSSIFWEBAUTHTEMP_SIZING_ID,
    WSSIFWEBAUTH_MAX_SIZING_ID
};

INT4  WssIfWebAuthSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#ifdef  __WSSIFAUTH_UTIL__C
tMemPoolId gaWSSIFWEBAUTHMemPoolIds[WSSIFWEBAUTH_MAX_SIZING_ID];
INT4  WssIfWebAuthSizingMemCreateMemPools(VOID);
VOID  WssIfWebAuthSizingMemDeleteMemPools(VOID);
#else  /*  __WSSIFAUTH_UTIL__C  */
extern tMemPoolId gaWSSIFWEBAUTHMemPoolIds[WSSIFWEBAUTH_MAX_SIZING_ID];
extern INT4  WssIfWebAuthSizingMemCreateMemPools(VOID);
extern VOID  WssIfWebAuthSizingMemDeleteMemPools(VOID);
#endif /*  __WSSIFAUTH_UTIL__C  */

#ifdef  __WSSIFAUTH_UTIL__C
tFsModSizingParams gaFsWSSIFWEBAUTHSizingParams [] = {
    { "tWssIfWebAuthDB", "MAX_WSSIFWEBAUTH_STATION", sizeof(tWssIfWebAuthDB),
        MAX_WSSIFWEBAUTH_STATION, MAX_WSSIFWEBAUTH_STATION, 0},
    { "tWssIfWebAuthTempDB", "MAX_WSSIFWEBAUTH_STATION",
        sizeof(tWssIfWebAuthTempDB), MAX_WSSIFWEBAUTH_STATION,
        MAX_WSSIFWEBAUTH_STATION, 0}
};
#else  /*  __WSSIFAUTH_UTIL__C  */
extern tFsModSizingParams gaFsWSSIFWEBAUTHSizingParams [];
#endif /*  __WSSIFAUTH_UTIL__C  */
#endif

