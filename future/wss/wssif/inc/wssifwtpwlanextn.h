/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *   $Id: wssifwtpwlanextn.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ 
 *  Description: This file contains extern of global vaiables used
 *               for wsswlan module.
 *  *********************************************************************/


#ifndef  __WSSIF_WTPWLAN_EXTN_H
#define  __WSSIF_WTPWLAN_EXTN_H

#include "wssifinc.h"


PUBLIC tWssWlanGlobals gWssWlanGlobals;

#endif

