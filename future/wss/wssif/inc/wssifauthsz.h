/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 *  $Id: wssifauthsz.h,v 1.2 2017/11/24 10:37:09 siva Exp $
 * Description: This file contains WSS IF Sizing parameters
 *******************************************************************/


enum {
    MAX_WSSIFAUTH_ASSOC_COUNT_SIZING_ID,
    MAX_WSSIFAUTH_BSSID_STATS_SIZING_ID,
    MAX_WSSIFAUTH_LOADSTATION_SIZING_ID,
    MAX_WSSIFAUTH_STATION_SIZING_ID,
    MAX_WSSIFAUTH_STATION_SESSION_SIZING_ID, /* ZEUS _ ICP LIFETIME Chg */
    WSSIFAUTH_MAX_SIZING_ID
};


#ifdef  _WSSIFAUTHSZ_C
tMemPoolId WSSIFAUTHMemPoolIds[ WSSIFAUTH_MAX_SIZING_ID];
INT4  WssifauthSizingMemCreateMemPools(VOID);
VOID  WssifauthSizingMemDeleteMemPools(VOID);
INT4  WssifauthSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _WSSIFAUTHSZ_C  */
extern tMemPoolId WSSIFAUTHMemPoolIds[ ];
extern INT4  WssifauthSizingMemCreateMemPools(VOID);
extern VOID  WssifauthSizingMemDeleteMemPools(VOID);
#endif /*  _WSSIFAUTHSZ_C  */


#ifdef  _WSSIFAUTHSZ_C
tFsModSizingParams FsWSSIFAUTHSizingParams [] = {
{ "tWssIfAuthWtpAssocCountInfo", "MAX_WSSIFAUTH_ASSOC_COUNT", sizeof(tWssIfAuthWtpAssocCountInfo),MAX_WSSIFAUTH_ASSOC_COUNT, MAX_WSSIFAUTH_ASSOC_COUNT,0 },
{ "tWssIfBssIdStatsDB", "MAX_WSSIFAUTH_BSSID_STATS", sizeof(tWssIfBssIdStatsDB),MAX_WSSIFAUTH_BSSID_STATS, MAX_WSSIFAUTH_BSSID_STATS,0 },
{ "tWssIfAuthLoadBalanceDB", "MAX_WSSIFAUTH_LOADSTATION", sizeof(tWssIfAuthLoadBalanceDB),MAX_WSSIFAUTH_LOADSTATION, MAX_WSSIFAUTH_LOADSTATION,0 },
{ "tWssIfAuthStateDB", "MAX_WSSIFAUTH_STATION", sizeof(tWssIfAuthStateDB),MAX_WSSIFAUTH_STATION, MAX_WSSIFAUTH_STATION,0 },
{ "tWssStaAuthSessionDB", "MAX_WSSIFAUTH_STATION_SESSION", sizeof(tWssStaAuthSessionDB), MAX_WSSIFAUTH_STATION_SESSION, MAX_WSSIFAUTH_STATION_SESSION,0 }, /* ZEUS _ ICP LIFETIME Chg */
{"\0","\0",0,0,0,0}
};
#else  /*  _WSSIFAUTHSZ_C  */
extern tFsModSizingParams FsWSSIFAUTHSizingParams [];
#endif /*  _WSSIFAUTHSZ_C  */


