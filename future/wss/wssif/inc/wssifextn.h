/** $Id: wssifextn.h,v 1.2 2017/05/23 14:16:57 siva Exp $ **/

#ifndef __WSSIFEXTN_H__
#define __WSSIFEXTN_H__

PUBLIC tWssIfGlobals        gWssIfGlobals;
PUBLIC tTCFilterHandleList  gTCFilterHandleList;
PUBLIC tRBTree              gWssStaTCFilterHandleDB;
PUBLIC INT4       gDhcpServerStatus;
PUBLIC INT4       gDhcpRelayStatus;
PUBLIC UINT4       gNextDhcpSrvAddress;
PUBLIC UINT4       gWlanDNSServerIpAddr;
PUBLIC UINT4       gWlanDefaultRouterIpAddr;
PUBLIC INT4       gWtpFirewallStatus;
PUBLIC INT4       gWtpNATStatus; 
PUBLIC UINT4                gu4StationBandTrace;
#endif /* __WSSIFEXTN_H__ */

