/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *   $Id: wssifwtpwlanglob.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ 
 *  Description: This file contains global vaiables used
 *               for wsswlan module.
 *  *********************************************************************/



#ifndef  __WSSIF_WTPWLAN_GLOB_H
#define  __WSSIF_WTPWLAN_GLOB_H


tWssWlanGlobals gWssWlanGlobals;

#endif

