/************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wssifradioglob.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains RadioIf Global parameters
 ***********************************************************************/

#ifndef  __WSSIF_RADIOGLOB_H
#define  __WSSIF_RADIOGLOB_H

tRadioIfGlobals gRadioIfGlobals;


#endif
