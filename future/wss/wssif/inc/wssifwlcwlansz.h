/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *  $Id: wssifwlcwlansz.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $  
 *  Description: This file contains WSSWLAN Sizing parameters
 *      *******************************************************************/

enum {
    WLAN_BSSID_MAPPING_DB_MAX_SIZING_ID,
    WLAN_BSSIFINDEX_DB_MAX_SIZING_ID,
    WLAN_BSSINTERFACE_DB_MAX_SIZING_ID,
    WLAN_IFINDEX_DB_MAX_SIZING_ID,
    WLAN_SSID_MAPPING_DB_MAX_SIZING_ID,
    WSSIFWLCWLAN_MAX_SIZING_ID
};


#ifdef  _WSSIFWLCWLANSZ_C
tMemPoolId WSSIFWLCWLANMemPoolIds[ WSSIFWLCWLAN_MAX_SIZING_ID];
INT4  WssifwlcwlanSizingMemCreateMemPools(VOID);
VOID  WssifwlcwlanSizingMemDeleteMemPools(VOID);
INT4  WssifwlcwlanSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _WSSIFWLCWLANSZ_C  */
extern tMemPoolId WSSIFWLCWLANMemPoolIds[ ];
extern INT4  WssifwlcwlanSizingMemCreateMemPools(VOID);
extern VOID  WssifwlcwlanSizingMemDeleteMemPools(VOID);
#endif /*  _WSSIFWLCWLANSZ_C  */


#ifdef  _WSSIFWLCWLANSZ_C
tFsModSizingParams FsWSSIFWLCWLANSizingParams [] = {
{ "tWssWlanBssIdMappingDB", "WLAN_BSSID_MAPPING_DB_MAX", sizeof(tWssWlanBssIdMappingDB),WLAN_BSSID_MAPPING_DB_MAX, WLAN_BSSID_MAPPING_DB_MAX,0 },
{ "tWssWlanBssIfIndexDB", "WLAN_BSSIFINDEX_DB_MAX", sizeof(tWssWlanBssIfIndexDB),WLAN_BSSIFINDEX_DB_MAX, WLAN_BSSIFINDEX_DB_MAX,0 },
{ "tWssWlanBssInterfaceDB", "WLAN_BSSINTERFACE_DB_MAX", sizeof(tWssWlanBssInterfaceDB),WLAN_BSSINTERFACE_DB_MAX, WLAN_BSSINTERFACE_DB_MAX,0 },
{ "tWssWlanIfIndexDB", "WLAN_IFINDEX_DB_MAX", sizeof(tWssWlanIfIndexDB),WLAN_IFINDEX_DB_MAX, WLAN_IFINDEX_DB_MAX,0 },
{ "tWssWlanSSIDMappingDB", "WLAN_SSID_MAPPING_DB_MAX", sizeof(tWssWlanSSIDMappingDB),WLAN_SSID_MAPPING_DB_MAX, WLAN_SSID_MAPPING_DB_MAX,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _WSSIFWLCWLANSZ_C  */
extern tFsModSizingParams FsWSSIFWLCWLANSizingParams [];
#endif /*  _WSSIFWLCWLANSZ_C  */


