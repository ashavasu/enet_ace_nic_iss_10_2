/***$Id: wssifstaconst.h,v 1.2 2017/11/24 10:37:09 siva Exp $**/
#ifndef __WSSIFAUTH_CONST_H__
#define __WSSIFAUTH_CONST_H__


#define MAX_WSSIFAUTH_STATION   MAX_STA_SUPP_PER_WLC

#define MAX_WSSIFAUTH_LOADSTATION   MAX_STA_SUPP_PER_WLC
#define MAX_WSSIFAUTH_BSSID_STATS   MAX_NUM_OF_RADIOS * MAX_NUM_OF_BSSID_PER_RADIO
#define MAX_WSSIFAUTH_ASSOC_COUNT   NUM_OF_AP_SUPPORTED

#define MAX_WSSIFWEBAUTH_STATION    MAX_STA_SUPP_PER_WLC
#define MAX_WSSIFWEBAUTH_TEMP_STATION MAX_STA_SUPP_PER_WLC
#define MAX_WSSIFAUTH_STATION_SESSION MAX_STA_SUPP_PER_WLC 
#endif
