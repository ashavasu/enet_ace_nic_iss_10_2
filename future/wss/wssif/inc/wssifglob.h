/** $Id: wssifglob.h,v 1.2 2017/05/23 14:16:57 siva Exp $ **/

#ifndef __WSSIFGLOBAL_H__
#define __WSSIFGLOBAL_H__
#define ENABLED 1
#define DISABLED 0
tWssIfGlobals        gWssIfGlobals;
tTCFilterHandleList  gTCFilterHandleList;
tRBTree              gWssStaTCFilterHandleDB;
INT4       gDhcpServerStatus = ENABLED;
INT4       gDhcpRelayStatus = DISABLED;
UINT4       gNextDhcpSrvAddress = 0;
UINT4       gWlanDNSServerIpAddr = 0;
UINT4       gWlanDefaultRouterIpAddr = 0; 
INT4       gWtpFirewallStatus = DISABLED;
INT4       gWtpNATStatus = ENABLED;
UINT4                gu4StationBandTrace;
#endif /* __WSSIFGLOBAL_H__ */
