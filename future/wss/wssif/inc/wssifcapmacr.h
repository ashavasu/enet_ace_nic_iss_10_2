/**********************************************************************
 *Copyright (C) 2013 Aricent Inc . All Rights Reserved                 *
 *  $Id: wssifcapmacr.h,v 1.2 2017/11/24 10:37:09 siva Exp $                                                              *
 * DESCRIPTION    : This file contains the  macros  used for        *
 *                  WSSIF module                                       *
 ************************************************************************/
#ifndef _WSSIFMACR_H_
#define _WSSIFMACR_H_

#define  WSSIF_MOD                ((const char *)"WSSIF")

#define     WSSIF_INIT_TRC     0x00000001
/* #define  WSSIF_MGMT_TRC     0x00000002 */
/* #define  WSSIF_FAILURE_TRC 0x00000004 */
/* #define  WSSIF_ENTRY_TRC  0x00000008 */
/* #define  WSSIF_EXIT_TRC  0x00000010 */

#define  WSSIF_RB_LESS  -1
#define  WSSIF_RB_EQUAL  0  
#define  WSSIF_RB_GREATER 1


#define  WSSIF_BUF_SIZE      100
#define  WSSIF_ARP_OFFSET    12
#define  WSSIF_TWO_BYTE       2
#define  WSSIF_ARP_ETHERTYPE 0x806
#define WSSIF_CAPWAP_AID_DEPLETED 0xFFFFFFFF
#define WSSIF_CAPWAP_VALUE_32    32


#define     MAX_RSM_SUPPORTED       NUM_OF_AP_SUPPORTED

/* #define WSSIF_MASK  WSSIF_INIT_TRC | WSSIF_MGMT_TRC | WSSIF_FAILURE_TRC |
 * WSSIF_ENTRY_TRC | WSSIF_EXIT_TRC */

/*#define WSSIF_FN_ENTRY() MOD_FN_ENTRY(WSSIF_MASK, WSSIF_ENTRY_TRC,WSSIF_MOD)*/


/* #define WSSIF_FN_EXIT() MOD_FN_EXIT (WSSIF_MASK, WSSIF_EXIT_TRC,WSSIF_MOD) */

#define WSSIF_PKT_DUMP(mask, pBuf, Length, fmt)                           \
        MOD_PKT_DUMP(WSSIF_PKT_DUMP_TRC,mask, WSSIF_MOD, pBuf, Length, fmt)
#define WSSIF_TRC(mask, fmt)\
      MOD_TRC(WSSIF_MASK, mask, WSSIF_MOD, fmt)
#define WSSIF_TRC1(mask,fmt,arg1)\
      MOD_TRC_ARG1(WSSIF_MASK,mask,WSSIF_MOD,fmt,arg1)
#define WSSIF_TRC2(mask,fmt,arg1,arg2)\
      MOD_TRC_ARG2(WSSIF_MASK,mask,WSSIF_MOD,fmt,arg1,arg2)
#define WSSIF_TRC3(mask,fmt,arg1,arg2,arg3)\
      MOD_TRC_ARG3(WSSIF_MASK,mask,WSSIF_MOD,fmt,arg1,arg2,arg3)
#define WSSIF_TRC4(mask,fmt,arg1,arg2,arg3,arg4)\
      MOD_TRC_ARG4(WSSIF_MASK,mask,WSSIF_MOD,fmt,arg1,arg2,arg3,arg4)
#define WSSIF_TRC5(mask,fmt,arg1,arg2,arg3,arg4,arg5)\
      MOD_TRC_ARG5(WSSIF_MASK,mask,WSSIF_MOD,fmt,arg1,arg2,arg3,arg4,arg5)
#define WSSIF_TRC6(mask,fmt,arg1,arg2,arg3,arg4,arg5,arg6)\
      MOD_TRC_ARG6(WSSIF_MASK,mask,WSSIF_MOD,fmt,arg1,arg2,arg3,arg4,arg5,arg6)




#endif 

