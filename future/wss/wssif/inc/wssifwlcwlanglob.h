/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *  
 *  $Id: wssifwlcwlanglob.h,v 1.2 2017/11/24 10:37:10 siva Exp $
 *  Description: This file contains global vaiables used
 *               for wsswlan module.
 *  *********************************************************************/


#ifndef  __WSSIF_WLCWLAN_GLOB_H
#define  __WSSIF_WLCWLAN_GLOB_H



UINT2 gu2WlanInternalIdCount;
#ifdef BAND_SELECT_WANTED
UINT1 gu1BandSelectStatus = WLAN_DISABLE ;
#endif
UINT1 gu1LegacyStatus;
tWssWlanFreeIndex gWssWlanFreeIndex [MAX_NUM_OF_SSID_PER_WLC];
tWssWlanGlobals gWssWlanGlobals;

tWssWlanProfileDB          gaWssWlanProfileDB[MAX_SSID_PROFILE_INDEX];
tWssWlanInternalProfileDB gaWssWlanInternalProfileDB[MAX_NUM_OF_SSID_PER_WLC];

#endif
