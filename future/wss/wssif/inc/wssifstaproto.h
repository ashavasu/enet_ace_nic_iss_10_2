/*$Id: wssifstaproto.h,v 1.3 2017/11/24 10:37:10 siva Exp $*/
#ifndef __WSSIFAUTH_PROTO_H__
#define __WSSIFAUTH_PROTO_H__

UINT4 WssIfAuthInit (VOID);
UINT4
WssAuthUpdateDB(eWssStaUpdateAction, UINT4, tMacAddr, UINT2, UINT2,
        tWssStaParams *);
INT4  WssstaCompareStaDBRBTree(tRBElem *, tRBElem *);
VOID  WssstaRecvMemClear (VOID);
INT4  WssstaSizingMemCreateMemPools(VOID);
INT4  WssstaSzRegisterModuleSizingParams(CHR1 *);
VOID  WssstaSizingMemDeleteMemPools(VOID);
INT4  StaDBWalkFn(tRBElem *, eRBVisit, UINT4, void *, void *);
UINT4 WssStaUpdateLdDB(tWssIfAuthLoadBalanceDB *);
UINT4 WssStaSetLdDB(tWssIfAuthLoadBalanceDB *);
UINT4 WssStaAssocCntDB(tWssIfAuthWtpAssocCountInfo *);
UINT4 WssStaSetAssocCount(tWssIfAuthWtpAssocCountInfo *);
tWssIfAuthStateDB* WssStaStationEntryGet(tMacAddr);
INT4 WssStaCompareStaDBRBTree(tRBElem *, tRBElem *);
INT4 WssStaCompareStaLOADRBTree(tRBElem *, tRBElem *);
INT4 WssStaCompareAssoCntRBTree(tRBElem *, tRBElem *);
INT4 WssStaCompareBSSIDRBTree (tRBElem * , tRBElem * );
INT4 WssStaCompareAuthSessionRBTree (tRBElem * , tRBElem * );
INT4 WssIfUpdateBSSIDStnStatsDB(UINT4 u4BssIfIndex);
tWssIfBssIdStatsDB*  WssIfGetBssIdStnStatsEntry (UINT4 u4BssIfIndex);
INT4 WssIfDelBssIdStnStatsEntry (UINT4 u4BssIfIndex);

INT4 WssStaShowClient(VOID);
INT4 ClientDBWalkFn(tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
            void *pArg, void *pOut);

INT4 WebAuthSessionDBWalkFn (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
            void *pArg, void *pOut);
VOID WssIfAuthRecvMemClear(VOID);
UINT4 WssIfProcessWssAuthDBMsg (UINT1, tWssifauthDBMsgStruct *);
UINT4 WssIfStaDelStation (tWssStaDelProfile);
UINT1 WssStaDeleteStation(UINT4 u4BssIfIndex);
UINT4 WssStaDeAuthStation (tWssStaDeleteStation);
INT4 StaDBWalkFnForDeauth (tRBElem *, eRBVisit, UINT4, void *, void *);
/* Web Authentication */
INT4 WssIfCompareWebAuthRBTree(tRBElem *, tRBElem *);
INT4 WssIfWebAuthDBCreate(VOID);
INT4 WssIfCompareWebAuthTempRBTree(tRBElem *, tRBElem *);
INT4 WssIfWebAuthTempDBCreate(VOID);
UINT4 WssStaWebAuthTreeAdd (
        tWssIfIsSetWebAuthDB * pWssStaWebAuthIsSetDB,
        tWssIfWebAuthDB * pWebAuthDB);

INT4 WssStaWebAuthDBWalkFn PROTO((tRBElem *, eRBVisit, UINT4, void *, void *));
UINT4 WssStaUpdateWebAuthDB(UINT4, tWssIfWebAuthDB  *);
INT4 WssStaDelEntryFromWebAuthDb (VOID);

UINT1 
WssIfGetStationIpAddr (UINT4 *pu4BssIfIndex, UINT4 u4IpAddr);

UINT1
WssIfGetIsolatedStationIpAddr (UINT4 *pu4BssIfIndex);

UINT1
WssIfGetBSSIDofConnectedStation (tMacAddr StaMac, UINT1 *pu1BssId);

INT4
WssStaGetClientPerRadio PROTO ((UINT4 u4BssIfIndex, UINT1 *pu1ClientCount));
INT4
StaDBClientWalkFn PROTO ((tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
            void *pArg, void *pOut));
INT4
StaDBClientUserDeleteWalkFn (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                           void *pArg, void *pOut);
INT4
WssStaGetClientPerSSID PROTO ((UINT4 u4BssIfIndex, tMacAddr *pu1staMacAddr));
INT4
StaDBClientMacWalkFn PROTO ((tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
            void *pArg, void *pOut));
UINT1 WssIfGetCapwapDB PROTO ((tMacAddr StaMacAddr,tWssIfCapDB *pWssIfCapwapDB));
INT4
WssStaDeAuthFn PROTO ((UINT4 u4BssIfIndex));
INT4
StaDBDeAuthWalkFn PROTO ((tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
            void *pArg, void *pOut));

INT4 WssStaDelEntryFromAuthSessionDb PROTO ((VOID));
INT4 WssStaDelEntryFromAuthUsrSesDb PROTO ((UINT1 *pu1UserName));
#endif
