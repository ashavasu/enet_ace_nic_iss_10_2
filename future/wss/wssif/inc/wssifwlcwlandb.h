/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 *   $Id: wssifwlcwlandb.h,v 1.3 2017/11/24 10:37:10 siva Exp $ 
 *
 *  Description: This file contains type definitions for wsswlan module.
 *  *********************************************************************/
#ifndef __WSSIF_WLCWLANDB_H__
#define __WSSIF_WLCWLANDB_H__
#include "wssifwlanconst.h" 

/* Enum for Database Operations */

typedef enum {

     WSS_WLAN_CREATE_BSS_ENTRY,
     WSS_WLAN_SET_BSS_ENTRY,
     WSS_WLAN_GET_BSS_ENTRY,
     WSS_WLAN_DESTROY_BSS_ENTRY,

     WSS_WLAN_CREATE_BSS_IFINDEX_ENTRY,
     WSS_WLAN_SET_BSS_IFINDEX_ENTRY,
     WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
     WSS_WLAN_DESTROY_BSS_IFINDEX_ENTRY,
     
     WSS_WLAN_CREATE_PROFILE_ENTRY,
     WSS_WLAN_SET_PROFILE_ENTRY,
     WSS_WLAN_GET_PROFILE_ENTRY,
     WSS_WLAN_DESTROY_PROFILE_ENTRY,
     
     WSS_WLAN_SET_INTERNAL_PROFILE_ENTRY,
     WSS_WLAN_GET_INTERNAL_PROFILE_ENTRY,

     WSS_WLAN_CREATE_IFINDEX_ENTRY,
     WSS_WLAN_SET_IFINDEX_ENTRY,
     WSS_WLAN_GET_IFINDEX_ENTRY,
     WSS_WLAN_DESTROY_IFINDEX_ENTRY,

     WSS_WLAN_CREATE_SSID_MAPPING_ENTRY,
     WSS_WLAN_DESTROY_SSID_MAPPING_ENTRY,
     WSS_WLAN_SET_SSID_MAPPING_ENTRY,
     WSS_WLAN_GET_SSID_MAPPING_ENTRY,

     WSS_WLAN_CREATE_BSSID_MAPPING_ENTRY,
     WSS_WLAN_DESTROY_BSSID_MAPPING_ENTRY,
     WSS_WLAN_SET_BSSID_MAPPING_ENTRY,
     WSS_WLAN_GET_BSSID_MAPPING_ENTRY,


     WSS_WLAN_SET_MANAGMENT_SSID,
     WSS_WLAN_GET_MANAGMENT_SSID
} eWssWlanDBMsgType;

typedef enum {
    
    WSS_WLAN_BIND_STAT_UP,
    WSS_WLAN_BIND_STAT_DOWN,
    WSS_WLAN_BIND_FROM_CAPWAP,
    WSS_WLAN_BIND_FROM_USERIF,

} eWssWlanBindStatus;

/* The below data structure will be created during WLAN Profile creation. An
 * internal id will be created for each WLAN Profile */

typedef struct {
    UINT2       u2WlanInternalId;
    UINT1       au1Pad[2];
} tWssWlanProfileDB;

typedef struct {
   UINT4        u4WebAuthMode;
   UINT4        u4WebServIPAddr;
   UINT4        u4WebAuthType;    
   UINT1        u1WebAuthUrl;
   UINT1        au1pad[3];
}tWssWlanWebParamsTable;

typedef struct {
    UINT4        u4WebAuthUserLifetime;
    UINT1        u1WebAuthUName[WSS_WLAN_MAX_USER_CHAR];
    UINT1        u1WebAuthPasswd[WSS_WLAN_MAX_PWD_CHAR];
    BOOL1        bWebAuthUserStatus;
    UINT1        au1pad[3];
}tWssWlanWebUserCred;


typedef struct {
    UINT1       u1QosInfo;
    UINT1       au1pad[3];
}tWssWlanQos;

typedef struct {
  UINT4              u4IfIndex;
  INT4               i4RowStatus;
  UINT2              u2WlanProfileId;
  UINT1              u1InPriority;
  UINT1              u1OutDscp;
}tWssWlanDiffServ;

typedef struct {
   UINT4 u4IpAddr;
   UINT4 u4SubMask;
   UINT1 u1AdminStatus;
   UINT1 u1RowStatus;
   UINT1 au1Pad[2];
}tWlanInterfaceIp;


typedef struct 
{
    UINT1 u1CfPollable;
    UINT1 u1CfPollRequest;
    UINT1 u1PrivacyOptionImplemented;
    UINT1 u1ShortPreambleOptionImplemented;
    UINT1 u1PBCCOptionImplemented;
    UINT1 u1ChannelAgilityPresent;
    UINT1 u1SpectrumManagementRequired;
    UINT1 u1QosOptionImplemented;
    UINT1 u1ShortSlotTimeOptionImplemented;
    UINT1 u1APSDOptionImplemented;
    UINT1 u1DSSSOFDMOptionEnabled;
    UINT1 u1DelayedBlockAckOptionImplemented;
    UINT1 u1ImmediateBlockAckOptionImplemented;

    UINT1 u1QAckOptionImplemented;
    UINT1 u1QueueRequestOptionImplemented;
    UINT1 u1TXOPRequestOptionImplemented;
    UINT1 u1RSNAPreauthenticationImplemented;
    UINT1 u1RSNAOptionImplemented;
#ifdef WPS_WANTED    
    UINT1 u1WPSEnabled;
    UINT1 u1WPSAdditionalIE;
    UINT1 u1WPSConfigMethod;
    UINT1 au1Wpspad[1];
#endif    
    UINT1 au1pad[2];
}tWssWlanCapabilityProfileDB; 
/* 18 elements */


/* The below structure will get created internally 
 * when a WLAN Profile is created.
 * This will be created as an RBTree with u4WlanIfIndex as the key */
typedef struct {
    tRBNodeEmbd             IfIndexDBNode; 
    tWssWlanCapabilityProfileDB WssWlanCapabilityProfileDB;
    tWssWlanEdcaParam       WssWlanEdcaParam;
    tWssWlanQos             WssWlanQosCapab;
    tWssWlanWebParamsTable  WssWlanWebParamsTable;
    tWssWlanWebUserCred     WssWlanWebUserCred;
    UINT4                   u4WlanIfIndex;
    UINT4      u4LoginAuthMode;
    UINT4                   u4QosUpStreamCIR;
    UINT4                   u4QosUpStreamCBS;
    UINT4                   u4QosUpStreamEIR;
    UINT4                   u4QosUpStreamEBS;
    UINT4                   u4QosDownStreamCIR;
    UINT4                   u4QosDownStreamCBS;
    UINT4                   u4QosDownStreamEIR;
    UINT4                   u4QosDownStreamEBS;
    UINT4                   u4MaxClientCount;
    UINT4                   u4BandwidthThresh;
    UINT4                   u4WlanSnoopTimer;
    UINT4                   u4WlanSnoopTimeout;
    UINT2                   u2VlanId;
    UINT2                   u2WlanInternalId;
    UINT2                   u2WlanMulticastMode;
    UINT2                   u2WlanSnoopTableLength;
    UINT2                   u2Capability;
    INT2                    i2DtimPeriod;
    UINT1                           au1GroupTsc [WSSWLAN_GROUP_TSC_LEN];
    UINT1                   u1WlanIfType;
    UINT1                   u1KeyIndex;
    UINT1                           au1DesiredSsid [WSSWLAN_SSID_NAME_LEN];
    UINT1                   u1KeyType;
    UINT1                   u1KeyStatus;        
    UINT1                   u1AuthenticationIndex;
    UINT1                   u1AuthAlgorithm;
    UINT1                   au1WepKey[WSS_WLAN_KEY_LENGTH];
    UINT1                   u1WebAuthStatus;
    UINT1                   u1AuthMethod;
    UINT1                   u1QosProfileId;
    UINT1                   u1CapabilityId;
    UINT1                   u1AdminStatus;
    UINT1                   u1EncryptDecryptCapab;
    UINT1                   u1PowerConstraint;
    UINT1                   u1SupressSsid;
    UINT1                   u1SsidIsolation;
    UINT1                   u1QosSelection;
    UINT1                   u1FsPreAuth;
    UINT1                   u1PassengerTrustMode;
    UINT1                   u1QosTraffic;
    UINT1                   u1MitigationRequirement;
    UINT1                   u1QosRateLimit;
    UINT1                   u1ExternalWebAuthMethod;
    UINT1                   au1ExternalWebAuthUrl[WSSWLAN_EXTERNAL_URL_LENGTH];
#ifdef BAND_SELECT_WANTED
    UINT1                   u1BandSelectStatus;
    UINT1                   u1AgeOutSuppression;
    UINT1                   u1AssocRejectCount;
    UINT1                   u1AssocResetTime;
#endif
    UINT1      au1FsDot11RedirectFileName[32];    
}tWssWlanIfIndexDB;

/* The below data structure will be created during WLAN Profile creation. 
 * All WLAN Profile related info will be maintained in this table. There will be
 * a mapping to WLAN Profile */
typedef struct 
{
    tWssWlanIfIndexDB   *pWssWlanIfIndexDB;
    UINT2       u2WlanProfileId;
    UINT2       u2WlanMulticastMode;
    UINT2       u2WlanSnoopTableLength;
    UINT2       u2WlanSnoopTimer;
    UINT2       u2WlanSnoopTimeout;
    UINT1       u1MacType;
    UINT1       u1TunnelMode;
    UINT1       u1WlanRadioMappingCount;
    UINT1       u1RowStatus;
    UINT1       au1Pad[2];
} tWssWlanInternalProfileDB;

/* The below structure will get created internally when operator binds the WLAN 
 * profile to a radio interface. An interface index will be created u4BssIfIndex
 * and that will the key for this RBTree */
struct tRadioIFDB;

typedef struct {  
    tRBNodeEmbd     BssIfIndexDBNode; 
    struct tRadioIFDB      *pRadioIfDB;
    tWssWlanDiffServ  WssWlanDiffServ;
    tWlanInterfaceIp     WlanInterfaceIp;
    tMacAddr        BssId;
    UINT2                   u2WlanInternalId;
    UINT4           u4BssIfIndex;
    UINT4           u1IfType;
    UINT2           u2VlanId;
    UINT1           u1WlanId;
    UINT1           u1AdminStatus;
    UINT1           u1WlanBindStatus;
    UINT1       au1Pad[3];
}tWssWlanBssIfIndexDB;

/* The below structure will get created when operator binds a wlan profile to
 * a Radio Interface. This will an RBTree with u4RadioIfIndex and
 * u2WlanProfileId as the key */
typedef struct  tWssWlanBssInterfaceDb
{
    tRBNodeEmbd             BssInterfaceDBNode; 
    tWssWlanBssIfIndexDB   *pWssWlanBssIfIndexDB;
    UINT4                   u4RadioIfIndex;
    UINT2                   u2WlanProfileId;
    UINT1                   u1WlanId;
    UINT1                   u1RowStatus;        
} tWssWlanBssInterfaceDB;

/* Mapping for BSS Mac address to BSS IfIndex */
typedef struct {
    tRBNodeEmbd BssIdMappingDBNode; 
 UINT4      u4BssIfIndex;
    tMacAddr    BssId;
 UINT2  u2RuleId;
 
}tWssWlanBssIdMappingDB;

typedef struct {
        tWssWlanBssIdMappingDB   WssWlanBssId;
        tWssWlanBssIfIndexDB     WssWlanBssIfIndexDB;
}tWssWlanBssIdDB;

typedef struct {
 
    tWssWlanCapabilityProfileDB WssWlanCapabilityProfileDB;
    tWssWlanEdcaParam            WssWlanEdcaParam;
    tWssWlanQos             WssWlanQosCapab;
    tWssWlanDiffServ        WssWlanDiffServ;
    tWlanInterfaceIp        WlanInterfaceIp;
    tWssWlanWebParamsTable  WssWlanWebParamsTable;
    tWssWlanWebUserCred     WssWlanWebUserCred;
#ifdef WPS_WANTED    
    tWpsIEElements          WpsIEElements;
#endif    
    tRsnaIEElements         RsnaIEElements;
    tWpaIEElements          WpaIEElements;
    tMacAddr                BssId;
    UINT2                   u2RuleId;
    UINT4                   u4BssIfIndex;
    UINT4                   u4WlanIfIndex;
    UINT4      u4LoginAuthMode;
    UINT4                   u4RadioIfIndex;
    UINT4                   u4QosUpStreamCIR;
    UINT4                   u4QosUpStreamCBS;
    UINT4                   u4QosUpStreamEIR;
    UINT4                   u4QosUpStreamEBS;
    UINT4                   u4QosDownStreamCIR;
    UINT4                   u4QosDownStreamCBS;
    UINT4                   u4QosDownStreamEIR;
    UINT4                   u4QosDownStreamEBS;
    UINT4                   u1WlanIfType;
    UINT4      u4MaxClientCount;
    UINT4                   u4BandwidthThresh;
    UINT4                   u4WlanSnoopTimer;
    UINT4                   u4WlanSnoopTimeout;
    UINT2                   u2WtpInternalId;
    UINT2                   u2WlanInternalId;
    UINT2                   u2WlanProfileId;
    UINT2                   u2WlanMulticastMode;
    UINT2                   u2WlanSnoopTableLength;
    UINT2                   u2VlanId;
    UINT2                   u2Capability;
    INT2                    i2DtimPeriod;

    UINT1                          au1DesiredSsid [WSSWLAN_SSID_NAME_LEN];
    UINT1                          au1GroupTsc [WSSWLAN_GROUP_TSC_LEN];
    UINT1                   u1WlanId;
    UINT1                   u1KeyIndex;
    UINT1                   au1WepKey[WSS_WLAN_KEY_LENGTH];
    UINT1                   u1KeyType;
    UINT1                   u1KeyStatus;        
    UINT1                   u1AuthenticationIndex;
    UINT1                   u1AuthAlgorithm;
    UINT1                   u1WebAuthStatus;
    UINT1                   u1AuthMethod;
    UINT1                   u1QosProfileId;
    UINT1                   u1CapabilityId;
    UINT1                   u1AdminStatus;
    UINT1                   u1EncryptDecryptCapab;
    UINT1                   u1PowerConstraint;
    UINT1                   u1SupressSsid;
    UINT1                   u1SsidIsolation;
    UINT1                   u1QosSelection;
    UINT1                   u1FsPreAuth;
    UINT1                   u1MacType;
    UINT1                   u1TunnelMode;
    UINT1                   u1WlanRadioMappingCount;
    UINT1                   u1ExternalWebAuthMethod;
    UINT1                   u1RowStatus;
    UINT1                   au1ExternalWebAuthUrl[WSSWLAN_EXTERNAL_URL_LENGTH];
    UINT1      au1InterfaceName[24];

    /* Capability profile elements - start */
    UINT1 u1CfPollable;
    UINT1 u1CfPollRequest;
    UINT1 u1PrivacyOptionImplemented;
    UINT1 u1ShortPreambleOptionImplemented;
    UINT1 u1PBCCOptionImplemented;
    UINT1 u1ChannelAgilityPresent;
    UINT1 u1SpectrumManagementRequired;
    UINT1 u1QosOptionImplemented;
    UINT1 u1ShortSlotTimeOptionImplemented;
    UINT1 u1APSDOptionImplemented;
    UINT1 u1DSSSOFDMOptionEnabled;
    UINT1 u1DelayedBlockAckOptionImplemented;
    UINT1 u1ImmediateBlockAckOptionImplemented;
    /* Capability profile elements - end */
    UINT1 u1QAckOptionImplemented;
    UINT1 u1QueueRequestOptionImplemented;
    UINT1 u1TXOPRequestOptionImplemented;
    UINT1 u1RSNAPreauthenticationImplemented;
    UINT1 u1RSNAOptionImplemented;
#ifdef WPS_WANTED    
    UINT1 u1WPSEnabled;
    UINT1 u1WPSAdditionalIE;
    UINT1 au1WpsPad[2];
#endif    

    /* Qos Elements - start */
     UINT1                   u1PassengerTrustMode;
     UINT1                   u1QosTraffic;
     UINT1                   u1QosRateLimit;

    /* Qos Elements - end */

     /* Spectrum table - start */
    UINT1                   u1MitigationRequirement;
    UINT1                          u1WlanBindStatus;
    UINT1                          u1IfType;
    UINT1                   au1ManagmentSSID[WSSWLAN_SSID_NAME_LEN];
     /* Spectrum table - end */

#ifdef BAND_SELECT_WANTED
    UINT1   u1BandSelectStatus;
    UINT1   u1AgeOutSuppression;
    UINT1   u1AssocRejectCount;
    UINT1   u1AssocResetTime;
#endif
    UINT1      au1FsDot11RedirectFileName[32];
}tWssWlanAttributeDB;



typedef struct {
    BOOL1       bWssWlanEdcaParam;
    BOOL1       bWssWlanQosCapab;
    BOOL1       bWssWlanWebParamsTable;
    BOOL1       bWssWlanWebUserCred;
    BOOL1       bWlanDiffServId;
    BOOL1       bBssIfIndex;
    BOOL1       bWlanIfIndex;
    BOOL1       bWlanIfType;
    BOOL1       bRadioIfIndex;
    BOOL1       bIfType;
    BOOL1       bWtpInternalId;
    BOOL1       bWlanInternalId;
    BOOL1       bWlanProfileId;
    BOOL1       bVlanId;
    BOOL1       bCapability;
    BOOL1       bWlanId;
    BOOL1       bDtimPeriod;
    BOOL1       bGroupTsc;
    BOOL1       bDesiredSsid;
    BOOL1       bKeyIndex;
    BOOL1       bKeyType;
    BOOL1       bKeyStatus;        
    BOOL1       bAuthenticationIndex;
    BOOL1       bBssId;
    BOOL1       bRuleId;
    BOOL1       bWepKey;
    BOOL1       bAuthAlgorithm;
    BOOL1       bWebAuthStatus;
    BOOL1       bAuthMethod;
    BOOL1       bQosProfileId;
    BOOL1       bCapabilityId;
    BOOL1       bAdminStatus;
    BOOL1       bEncryptDecryptCapab;
    BOOL1       bPowerConstraint;
    BOOL1       bSupressSsid;
    BOOL1       bSsidIsolation;
    BOOL1       bQosSelection;
    BOOL1       bFsPreAuth;
    BOOL1       bWlanMacType;
    BOOL1       bWlanTunnelMode;
    BOOL1       bShortPreamble;
    BOOL1       bWlanRadioMappingCount;
    BOOL1       bExternalWebAuthMethod;
    BOOL1       bExternalWebAuthUrl;
    BOOL1       bRowStatus;
    BOOL1       bWlanMulticastMode;
    BOOL1       bWlanSnoopTableLength;
    BOOL1       bWlanSnoopTimer;
    BOOL1       bWlanSnoopTimeout;
    BOOL1       bInterfaceName;
    /* Capability profile elements - start */
    BOOL1 bCfPollable;
    BOOL1 bCfPollRequest;
    BOOL1 bPrivacyOptionImplemented;
    BOOL1 bShortPreambleOptionImplemented;
    BOOL1 bPBCCOptionImplemented;
    BOOL1 bChannelAgilityPresent;
    BOOL1 bSpectrumManagementRequired;
    BOOL1 bQosOptionImplemented;
    BOOL1 bShortSlotTimeOptionImplemented;
    BOOL1 bAPSDOptionImplemented;
    BOOL1 bDSSSOFDMOptionEnabled;
    BOOL1 bDelayedBlockAckOptionImplemented;
    BOOL1 bImmediateBlockAckOptionImplemented;
    /* Capability profile elements - end */

    BOOL1 bQAckOptionImplemented;
    BOOL1 bQueueRequestOptionImplemented;
    BOOL1 bTXOPRequestOptionImplemented;
    BOOL1 bRSNAPreauthenticationImplemented;
    BOOL1 bRSNAOptionImplemented;
    BOOL1 bWPSEnabled;
    BOOL1 bWPSAdditionalIE;

    /* Qos elements - start */

    BOOL1 bPassengerTrustMode;
    BOOL1 bQosTraffic;
    BOOL1 bQosRateLimit;
    BOOL1 bQosUpStreamCIR;
    BOOL1 bQosUpStreamCBS;
    BOOL1 bQosUpStreamEIR;
    BOOL1 bQosUpStreamEBS;
    BOOL1 bQosDownStreamCIR;
    BOOL1 bQosDownStreamCBS;
    BOOL1 bQosDownStreamEIR;
    BOOL1 bQosDownStreamEBS;
        /* Qos elements - end */
    BOOL1 bMitigationRequirement;
    BOOL1 bManagmentSSID;
    BOOL1 bWlanBindStatus;
    BOOL1 bMaxClientCount;
#ifdef BAND_SELECT_WANTED
    BOOL1 bBandSelectStatus;
    BOOL1 bAgeOutSuppression;
    BOOL1 bAssocRejectCount;
    BOOL1 bAssocResetTime;
#endif
    UINT1       au1pad[3];
    BOOL1 bFsDot11RedirectFileName;
    BOOL1 bWlanInterfaceIp;
    BOOL1 bVendorWlanInterfaceIp;
    BOOL1 bFsLoginAuthMode;
    BOOL1 bBandwidthThresh;
    UINT1       au1pad2[3];
}tWssWlanIsPresentDB;

typedef struct {
    tWssWlanIsPresentDB             WssWlanIsPresentDB;               
    tWssWlanAttributeDB            WssWlanAttributeDB;
}tWssWlanDB;


/* Mapping for SSID name to BSS IfIndex */
typedef struct {
        tRBNodeEmbd    SsidMappingDBNode;
 UINT4  u4WlanIfIndex;
     UINT1     au1SSID[WSSWLAN_SSID_NAME_LEN];
}tWssWlanSSIDMappingDB;

typedef struct 
{
    tRBTree                     WssWlanBssIfIndexDB;
    tRBTree                     WssWlanIfIndexDB;
    tRBTree                     WssWlanBssInterfaceDB;
    tRBTree                     WssWlanBssIdMappingDB;
    tRBTree                     WssWlanSSIDMappingDB;
    UINT1                       au1ManagmentSSID[WSSWLAN_SSID_NAME_LEN];
 } tWssWlanGlbMib;

typedef struct WSSWLAN_GLOBALS {
     tWssWlanGlbMib       WssWlanGlbMib;
 } tWssWlanGlobals;

typedef struct {
    UINT2   u2WlanProfileId;
    UINT1   u1Used;
    UINT1   u1Pad;
}tWssWlanFreeIndex;

#endif  /* __WSSIFWLCWLANDB_H__ */

