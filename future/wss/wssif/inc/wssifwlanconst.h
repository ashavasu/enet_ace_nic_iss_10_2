/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *  $Id: wssifwlanconst.h,v 1.4 2017/11/24 10:37:10 siva Exp $
 *  
 *  Description: This file contains constant definitions for wsswlan module.
 *  *********************************************************************/


#ifndef  __WSSIF_WLAN_CONST_H__
#define  __WSSIF_WLAN_CONST_H__


#define WSS_WLAN_GROUP_TSC_LEN  6
#define WSS_WLAN_SSID_NAME_LEN  32
#define WSS_WLAN_MAX_USER_CHAR   100
#define WSS_WLAN_MAX_PWD_CHAR    100
#define WSS_WLAN_KEY_LENGTH      104
#define WSS_WEP_KEY_SIZE      104
#define MAX_SSID_PROFILE_INDEX  512 
#define MAX_NUM_OF_SSID_PER_AP 16
#define MAX_WSSIFWSSUSER_BUFFER_SIZE   256
#define MAX_WSSIFWSSUSER_ONE_MASK   255
#define WSS_WLAN_MAC_SIZE 6
#define WSS_WLAN_NAME_MAX_LEN CFA_MAX_PORT_NAME_LENGTH 

#define WSS_WLAN_WEBAUTH_URL_LENGTH 120
#define WSS_WLAN_DEFAULT_DTIM 5
#define WSS_WLAN_DEFAULT_REDIRECT_FILE_LENGTH 32
#define WSS_WLAN_AUTH_ALGO_OPEN 1
#define WSS_WLAN_AUTH_ALGO_SHARED 2

#define WSS_WLAN_POWER_CONSTRAINT 3
#define WLAN_IFINDEX_DB_MAX MAX_NUM_OF_SSID_PER_WLC
#define WLAN_BSSINTERFACE_DB_MAX ( MAX_NUM_OF_RADIO_PER_AP * \
                               NUM_OF_AP_SUPPORTED * MAX_NUM_OF_SSID_PER_WLC)
/* 31 radios, 4 ap, MAX_NUM_OF_SSID_PER_WLC profiles */

#define WLAN_BSSIFINDEX_DB_MAX ( MAX_NUM_OF_RADIO_PER_AP * \
                               NUM_OF_AP_SUPPORTED * MAX_NUM_OF_SSID_PER_WLC)

#define WLAN_BSSID_MAPPING_DB_MAX ( MAX_NUM_OF_RADIO_PER_AP * \
                                   NUM_OF_AP_SUPPORTED * MAX_NUM_OF_SSID_PER_WLC)
#define WLAN_SSID_MAPPING_DB_MAX MAX_NUM_OF_SSID_PER_WLC
#define WTPWLAN_IFINDEX_DB_MAX 16
#define WLAN_INTERFACE_DB_MAX 16
#define WTPWLAN_BSSID_MAPPING_DB_MAX 16
#define WSS_WLAN_SPEC_MGMT      0x0100
#ifdef BAND_SELECT_WANTED
#define WSS_WLAN_DEF_BANDSELECT 0
#define WSS_WLAN_DEF_AGEOUT_SUPPRESION 5
#define WSS_WLAN_DEF_ASSOC_COUNT_FLUSH 5
#define WSS_WLAN_DEF_ASSOC_REJ_COUNT 5
#endif
#define WSS_WLAN_DEF_BANDWIDTH_THRESH   0

#endif
