/*******************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *  $Id: wssifcapdb.h,v 1.4 2018/02/20 10:10:21 siva Exp $
 *
 * Description: This file contains all the typedefs used by the WLC
 *              Handler module
 *******************************************************************/
#ifndef __WSSIFTDFS_H__
#define __WSSIFTDFS_H__

#include "tcp.h"
#include "wssifradiodb.h"
#include "capwap.h"

#define CAPWAP_WTPSESSPORT_POOLID       WSSIFCAPMemPoolIds[MAX_CAPWAP_DESTIPPORTMAPTABLE_SIZING_ID]
#define CAPWAP_WTPPROFILE_POOLID        WSSIFCAPMemPoolIds[MAX_CAPWAP_WTPPROFTABLE_SIZING_ID]
#define CAPWAP_WTPPROFILENAME_POOLID    WSSIFCAPMemPoolIds[MAX_CAPWAP_WTPNAMETABLE_SIZING_ID]
#define CAPWAP_WTPPROFILEMAC_POOLID     WSSIFCAPMemPoolIds[MAX_CAPWAP_WTPMACTABLE_SIZING_ID]
#define CAPWAP_WTPSESSPROFILE_POOLID    WSSIFCAPMemPoolIds[MAX_CAPWAP_PROFILEIDMAPTABLE_SIZING_ID]
#define CAPWAP_DTLSSESS_POOLID          WSSIFCAPMemPoolIds[MAX_CAPWAP_DTLSSESSENTRY_SIZING_ID]
#define CAPWAP_WTPMODEL_POOLID  WSSIFCAPMemPoolIds[MAX_CAPWAP_WTPMODELMAP_SIZING_ID]
#define CAPWAP_WLCBLACKLIST_POOLID      WSSIFCAPMemPoolIds[MAX_CAPWAP_WLCBLACKLIST_SIZING_ID]
#define CAPWAP_WLCWHITELIST_POOLID      WSSIFCAPMemPoolIds[MAX_CAPWAP_WLCWHITELIST_SIZING_ID]
#define CAPWAP_STAWHITELIST_POOLID      WSSIFCAPMemPoolIds[MAX_CAPWAP_STAWHITELIST_SIZING_ID]
#define CAPWAP_STABLACKLIST_POOLID WSSIFCAPMemPoolIds[MAX_CAPWAP_STABLACKLIST_SIZING_ID]
#define CAPWAP_CAPWAP_IF_CAP_DB_POOLID  WSSIFCAPMemPoolIds[MAX_CAPWAP_IF_CAP_DB_SIZING_ID]
#define CAPWAP_DSCP_MAP_DB_POOLID       WSSIFCAPMemPoolIds[MAX_CAPWAP_DSCP_MAP_DB_SIZING_ID]
#define CAPWAP_MODEL_TABLE_DB_POOLID       WSSIFCAPMemPoolIds[MAX_MODEL_TABLE_SIZE_SIZING_ID]
#define CAPWAP_DHCP_GLOBAL_DB_POOLID       WSSIFCAPMemPoolIds[MAX_CAPWAP_DHCP_POOL_SIZING_ID]
#define CAPWAP_WTP_DHCP_DB_POOLID          WSSIFCAPMemPoolIds[MAX_CAPWAP_WTP_DHCP_POOL_SIZING_ID]
#define CAPWAP_NAT_CONFIG_ENTRY_DB_POOLID  WSSIFCAPMemPoolIds[MAX_NAT_CONFIG_POOL_SIZING_ID]
#define CAPWAP_WTP_LR_CONFIG_DB_POOLID     WSSIFCAPMemPoolIds[MAX_WTP_LR_CONFIG_POOL_SIZING_ID]
#define CAPWAP_WTP_IP_ROUTE_CONFIG_DB_POOLID WSSIFCAPMemPoolIds[MAX_WTP_IP_ROUTE_CONFIG_POOL_SIZING_ID]
#define CAPWAP_WTP_DHCP_CONFIG_DB_POOLID   WSSIFCAPMemPoolIds[MAX_WTP_DHCP_CONFIG_POOL_SIZING_ID]
#define CAPWAP_WTP_FWL_FILTER_DB_POOLID    WSSIFCAPMemPoolIds[MAX_WTP_FWL_FILTER_POOL_SIZING_ID]
#define CAPWAP_WTP_FWL_RULE_DB_POOLID      WSSIFCAPMemPoolIds[MAX_WTP_FWL_RULE_POOL_SIZING_ID]
#define CAPWAP_WTP_FWL_ACL_DB_POOLID       WSSIFCAPMemPoolIds[MAX_WTP_FWL_ACL_POOL_SIZING_ID]
#define CAPWAP_NAT_DYNAMIC_DB_POOLID       WSSIFCAPMemPoolIds[MAX_WTP_DYNAMIC_NAT_POOL_SIZING_ID]
#define CAPWAP_NAT_STATIC_DB_POOLID        WSSIFCAPMemPoolIds[MAX_WTP_STATIC_NAT_POOL_SIZING_ID]
#define CAPWAP_NAPT_STATIC_DB_POOLID       WSSIFCAPMemPoolIds[MAX_WTP_STATIC_NAPT_POOL_SIZING_ID]
#define CAPWAP_NAT_GLOBAL_ADDR_DB_POOLID   WSSIFCAPMemPoolIds[MAX_WTP_NAT_GLOBAL_ADDR_POOL_SIZING_ID]
#define CAPWAP_NAT_LOCAL_ADDR_DB_POOLID    WSSIFCAPMemPoolIds[MAX_WTP_NAT_LOCAL_ADDR_POOL_SIZING_ID]
#define CAPWAP_L3_SUB_IF_DB_POOLID        WSSIFCAPMemPoolIds[MAX_WTP_L3_SUB_IF_POOL_SIZING_ID]

/* Macros for allocation/deallocation from pool - CAPWAP_CAPWAP_IF_CAP_DB_POOLID */
#define CAPWAP_IF_CAP_DB_ALLOC(p)       p = (tWssIfCapDB *) \
                                             MemAllocMemBlk (CAPWAP_CAPWAP_IF_CAP_DB_POOLID)
#define CAPWAP_IF_CAP_DB_RELEASE(p)     MemReleaseMemBlock (CAPWAP_CAPWAP_IF_CAP_DB_POOLID,\
                                                                                 (UINT1 *) p)

/* This are macros added for the purpose of accesing the UTIL memory pool
 * created for tWssIfCapDB structure - To use it as a local structure 
 */
#define WSS_IF_DB_TEMP_MEM_POOL_ID UTILMemPoolIds[MAX_WSS_TEMP_IF_DB_SIZING_ID]

/* Trace message to trace problem with this pool
   PRINTF ("+++Function = %s ; Line No = %d Alloc - WSS_IF_DB_TEMP_MEM_POOL_ID\n", __FUNCTION__, __LINE__);\
*/
#define WSS_IF_DB_TEMP_MEM_ALLOC(p, ret_value) \
              MemAllocateMemBlock (WSS_IF_DB_TEMP_MEM_POOL_ID, (UINT1 **) (VOID *) &p); \
/*   PRINTF ("+++Function = %s ; Line No = %d Alloc - WSS_IF_DB_TEMP_MEM_POOL_ID\n", __FUNCTION__, __LINE__);\
*/ \
              if (p == NULL) \
              {\
                  PRINTF ("!!!Function = %s ; Line No = %d Alloc Failed - WSS_IF_DB_TEMP_MEM_POOL_ID\n", __FUNCTION__, __LINE__);\
                  return (ret_value);\
              }

#define WSS_IF_DB_TEMP_MEM_ALLOC_VOID_RETURN(p) \
              MemAllocateMemBlock (WSS_IF_DB_TEMP_MEM_POOL_ID, (UINT1 **) (VOID *) &p); \
/*   PRINTF ("+++Function = %s ; Line No = %d Alloc - WSS_IF_DB_TEMP_MEM_POOL_ID\n", __FUNCTION__, __LINE__);\
*/ \
              if (p == NULL) \
              {\
                  PRINTF ("!!!Function = %s ; Line No = %d Alloc Failed - WSS_IF_DB_TEMP_MEM_POOL_ID\n", __FUNCTION__, __LINE__);\
                  return;\
              }

             
#define WSS_IF_DB_TEMP_MEM_RELEASE(p) MemReleaseMemBlock(WSS_IF_DB_TEMP_MEM_POOL_ID, (UINT1 *) p);
/* 
 * PRINTF ("---Function = %s ; Line No = %d Release - WSS_IF_DB_TEMP_MEM_POOL_ID\n", __FUNCTION__, __LINE__);
*/


#define WSS_IF_DB_NEIGHBOR_AP_SCAN_POOL_ID UTILMemPoolIds[MAX_NEIGHBOR_SCAN_SIZING_ID]

#define WSS_IF_DB_NEIGHBOR_AP_SCAN_ALLOC(p) \
              MemAllocateMemBlock (WSS_IF_DB_NEIGHBOR_AP_SCAN_POOL_ID, (UINT1 **) (VOID *) &p); \
/*   PRINTF ("+++Function = %s ; Line No = %d Alloc - WSS_IF_DB_NEIGHBOR_AP_SCAN_POOL_ID \n", __FUNCTION__, __LINE__);\
*/ \
              if (p == NULL) \
              {\
                  PRINTF ("!!!Function = %s ; Line No = %d Alloc Failed - MAX_SCANNING_SIZING_ID\n", __FUNCTION__, __LINE__);\
              }

#define WSS_IF_DB_NEIGHBOR_AP_SCAN_RELEASE(p) MemReleaseMemBlock(WSS_IF_DB_NEIGHBOR_AP_SCAN_POOL_ID, (UINT1 *) p);



#define WSSIF_CAPWAP_PROFILE_NAME_LEN   256
#define    MAX_WTP_MODELNUMBER_LEN    32
#define    MAX_IN_PRIORITY_IP_DSCP    64
#define MAX_CAPWAP_WTPPROFTABLE            NUM_OF_AP_SUPPORTED * 2
#define MAX_CAPWAP_WTPMAPTABLE            MAX_CAPWAP_WTPPROFTABLE
#define MAX_CAPWAP_WTPMACTABLE            MAX_CAPWAP_WTPPROFTABLE
#define MAX_CAPWAP_WTPNAMETABLE            MAX_CAPWAP_WTPPROFTABLE
#define MAX_CAPWAP_IF_CAP_DB               7
#define MAX_CAPWAP_DESTIPPORTMAPTABLE           MAX_CAPWAP_WTPPROFTABLE
#define MAX_CAPWAP_PROFILEIDMAPTABLE            MAX_CAPWAP_WTPPROFTABLE
#define MAX_CAPWAP_DTLSSESSENTRY                MAX_CAPWAP_WTPPROFTABLE
#define MAX_CAPWAP_WLCBLACKLIST                 MAX_CAPWAP_WTPPROFTABLE
#define MAX_CAPWAP_WLCWHITELIST                 MAX_CAPWAP_WTPPROFTABLE
#define MAX_CAPWAP_STAWHITELIST                 NUM_OF_AP_SUPPORTED * 16
#define MAX_CAPWAP_STABLACKLIST                 NUM_OF_AP_SUPPORTED * 16
#define MAX_CAPWAP_WTPMODELMAP                  MAX_CAPWAP_WTPPROFTABLE
#define MAX_MODEL_TABLE_SIZE                 128
#define MAX_CAPWAP_DSCP_MAP_DB                  MAX_IN_PRIORITY_IP_DSCP * \
                                                SYS_DEF_MAX_PHYSICAL_INTERFACES
#define MAX_CAPWAP_DHCP_POOL                    16
#define MAX_CAPWAP_WTP_DHCP_POOL                NUM_OF_AP_SUPPORTED * MAX_CAPWAP_DHCP_POOL
#define MAX_NAT_CONFIG_POOL                     NUM_OF_AP_SUPPORTED * 2
#define MAX_WTP_LR_CONFIG_POOL           NUM_OF_AP_SUPPORTED * 16
#define MAX_WTP_IP_ROUTE_CONFIG_POOL      NUM_OF_AP_SUPPORTED * 10
#define MAX_WTP_DHCP_CONFIG_POOL          NUM_OF_AP_SUPPORTED * 5
#define MAX_WTP_FWL_FILTER_POOL                 NUM_OF_AP_SUPPORTED * 100
#define MAX_WTP_FWL_RULE_POOL                   NUM_OF_AP_SUPPORTED * 50
#define MAX_WTP_FWL_ACL_POOL                    NUM_OF_AP_SUPPORTED * 50
#define MAX_WTP_DYNAMIC_NAT_POOL                NUM_OF_AP_SUPPORTED * 9000
#define MAX_WTP_NAT_LOCAL_ADDR_POOL             NUM_OF_AP_SUPPORTED * 10
#define MAX_WTP_STATIC_NAT_POOL                 NUM_OF_AP_SUPPORTED * 100
#define MAX_WTP_STATIC_NAPT_POOL                NUM_OF_AP_SUPPORTED * 100
#define MAX_WTP_NAT_GLOBAL_ADDR_POOL            NUM_OF_AP_SUPPORTED * 100
#define MAX_WTP_NAT_IF_POOL                     NUM_OF_AP_SUPPORTED * 4497
#define MAX_WTP_L3_SUB_IF_POOL   NUM_OF_AP_SUPPORTED * 10


#define MAX_WTP_PROFILE_SUPPORTED   NUM_OF_AP_SUPPORTED
#define CAPWAP_WTP_DEF_MAC_TYPE   0
#define CAPWAP_WTP_DEF_TUNN_MODE  4


#define DTLS_CFG_ENABLE   1
#define DTLS_CFG_DISABLE  2

#define WSSIF_CAPWAP_STA_WHITELIST 2
#define WSSIF_CAPWAP_STA_BLACKLIST 1

#define AP_FWL_MAX_FILTER_SET_LEN      252
#define AP_FWL_MAX_RULE_NAME_LEN       36
#define AP_FWL_MAX_FILTER_NAME_LEN     36
#define AP_FWL_MAX_ACL_NAME_LEN        36
#define AP_FWL_MAX_ADDR_LEN            85
#define AP_FWL_MAX_PORT_LEN            12
#define AP_FWL_MIN_PORT_LEN            1
#define AP_FWL_MAX_URL_FILTER_STRING_LEN    100
#define AP_FWL_FLAG_NOT_SET             0
#define AP_FWL_FLAG_SET                 1
#define AP_FWL_SET                      1
#define AP_FWL_NOT_SET                  0
#define AP_FWL_TRUE                     1
#define AP_FWL_FALSE                    0
#define AP_FWL_ZERO                  0
#define AP_FWL_ONE                   1
#define AP_FWL_ACTIVE                  1
#define AP_FWL_SUCCESS    0
#define AP_FWL_FAILURE    1
#define AP_FWL_INVALID      -1
#define AP_FWL_NOT_IN_SERVICE          2
#define AP_FWL_PERMIT                  1
#define AP_FWL_DENY                    2
#define AP_FWL_END_OF_STRING           '\0'
#define AP_FWL_NULL_STRING               "\0"
#define AP_FWL_STRING_EQUAL            0
#define AP_FWL_TCP                      6
#define AP_FWL_UDP                      17
#define AP_FWL_TCP_ACK_ESTABLISH                         1
#define AP_FWL_TCP_ACK_NOT_ESTABLISH                     2
#define AP_FWL_TCP_ACK_ANY                               3
#define AP_FWL_INTERFACE_NAME_LEN_MAX   20

#define AP_FWL_TCP_RST_SET                               1
#define AP_FWL_TCP_RST_NOT_SET                           2
#define AP_FWL_TCP_RST_ANY                               3
#define AP_FWL_URG_SET                                   1
#define AP_FWL_SHIFT_VALUE                     24
#define AP_FWL_MASK_VALUE              0xff000000
#define AP_FWL_REMAINING_SHIFT_VALUE            8
#define AP_FWL_ASCII_VALUE                     48
#define AP_FWL_DIV_FACTOR_100               100
#define AP_FWL_DIV_FACTOR_10                10
#define AP_FWL_MASK_1_BIT                   0x00000001
#define AP_FWL_CHECK_VAL_9                  9
#define AP_FWL_MAX_MASK_VAL                 32
#define AP_FWL_MIN_MASK_VAL                 3
#define AP_FWL_FTP_CTRL_PORT              21
#define AP_FWL_TELNET_PORT                23
#define AP_FWL_SMTP_PORT                  25
#define AP_FWL_DNS_PORT                   53
#define AP_FWL_HTTP_PORT                  80
#define AP_FWL_POP3_PORT                  110
#define AP_FWL_IMAP_PORT                  143
#define AP_FWL_HTTPS_PORT                 443  /*  HTTPS  */
#define AP_FWL_SNMP_PORT                  161
#define AP_FWL_SNMP_TRAP_PORT             162
#define AP_FWL_SNTP_PORT                  123
#define AP_FWL_DHCP_CLIENT_PORT           68
#define AP_FWL_DHCP_SERVER_PORT           67
#define AP_FWL_UDP_RIP_PORT               520
#define AP_FWL_UDP_RIPng_PORT             521
#define  AP_FWL_MAX_PORT_VALUE                         65535
#define  AP_FWL_MIN_PORT_VALUE                         1
#define  AP_FWL_MAX_PORT                               65535
#define  AP_FWL_MIN_PORT                               1

#define AP_FWL_IP_VERSION_4          4     /* Ip Header Version Field valid value. */
#define AP_FWL_IP6_HEADER_LEN        40 /* IPv6 Header length of a packet excluding nH */
#define AP_FWL_IP_VERSION_6          6  /* Ip Header Version Field valid value. */
#define AP_FWL_DEFAULT_ADDRESS             0
#define AP_FWL_DEFAULT_PORT                0
#define AP_FWL_DEFAULT_PROTO             255
#define AP_FWL_DEFAULT_MASK       0xffffffff
#define AP_FWL_DEFAULT_COUNT               0
#define AP_FWL_DEFAULT_OFFSET              0
#define AP_FWL_DEFAULT_LENGTH              0
#define AP_FWL_DEFAULT_SEQ_NUM             0
#define AP_FWL_DEFAULT_ACL_TYPE            1
#define AP_FWL_DEFAULT_ACTION              0
#define AP_FWL_DEFAULT_SERVICES            0
#define AP_FWL_DEFAULT_TIMEOUT             0
#define AP_FWL_DEFAULT_DSCP                0
#define AP_FWL_DEFAULT_FLOWID              0
#define AP_FWL_ENABLE                                    1
#define AP_FWL_DISABLE                                   2
#define AP_FWL_FILTERING_ENABLED                         1
#define AP_FWL_FILTERING_DISABLED                        2
#define AP_FWL_TOS_ANY               0
#define AP_FWL_TOS_MAX_VALUE         7

/* Interface Type Value */
#define AP_L3SUBIF CFA_L3SUB_INTF
#define AP_L3_UPDATEREQ_MSGLEN(u2Index)(11 + (u2Index * 17))

#define tIp4Addr    UINT4
/*****************************************************************************/
/*   The maximum number of Filter pointers can be stored in a Rule structure */ 
/*****************************************************************************/

#define  AP_FWL_MAX_FILTERS_IN_RULE   8

/*****************************************************************************/
/*   The Typical number of Rules, filters and Acl filters values             */ 
/*****************************************************************************/
#define  AP_FWL_MAX_NUM_OF_RULES  50

/* 
 * This specifies the Typical num of Rules that can be  configured. This value 
 * also be changed in run time through MIB Object.  
 */

#define  AP_FWL_MAX_NUM_OF_FILTERS  100
/* 
 * This is to configure the Filters and this also can be changed during
 * Run time through MIB object.  
 */

#define  AP_FWL_MAX_NUM_OF_ACL_FILTER  50

#define LOCAL_ROUTING_DISABLE 2
enum {
 WSS_WLCHDLR_GET_VENDOR_PAYLOAD,
 WSS_WLCHDLR_GET_LOCAL_IPV4,
 WSS_WLCHDLR_GET_CTRL_IP4_ADDR,
 WSS_WLCHDLR_GET_AC_IPV4_LIST,
 WSS_WLCHDLR_GET_AC_IPV6_LIST
};

typedef struct {
    UINT2          u2ProfileId;  /* range 0 - 4094 */
    UINT2          u2InternalProfileId; /* range - Compile time configurable */
    UINT1          u1Used;
    UINT1           au1pad[3];
}tWtpProfile;

typedef struct {
    UINT2          u2ProfileId;  /* range 0 - 4094 */
    UINT1          u1Used;
    UINT1          au1pad;
}tWtpIntProfile;

typedef struct {
   UINT4                au4WtpRadioType[31];
   UINT2                u2MaxStationsSupported;
   UINT1                u1MacType;
   UINT1                u1TunnelMode;
   UINT1    au1WtpSwVersion[128];
   UINT1    au1WtpHwVersion[128];
   UINT1    au1WtpQosProfildName[128];
   UINT1                au1WtpMaxSSIDSupported[31];
   UINT1                u1NumOfRadio;
   UINT1                au1WtpRadioAdminStatus[31];
   UINT1    u1RowStatus;
   UINT1                au1ModelNumber[512];
   UINT1                au1WtpImageName[128];
   UINT1                u1WtpImageLen;
   UINT1                u1ModelCount;
   UINT1                u1LocalRouting;
   UINT1                au1pad[1];
}tWtpModelEntry;

typedef struct{
    UINT4 ipAddr[128]; /* the latest list of AC.s IP available for the */
}tACIpv4List;

typedef struct{
    tIpAddr ipAddr[128];  /* the latest list of AC.s IP available for the WTP to join */
}tACIpv6List;

typedef struct {
     UINT4    u4LocalIpAddr;
     UINT4         u4VendorSMI;
     tACIpv4List        ACIpv4list;
     tACIpv6List        ACIpv6list;
     UINT1              u1NumOfIPv6List;
     UINT1              u1NumOfIPv4List;
     UINT1              au1pad[2];
}tWssIfWlcHdlrDB;

enum {
 WSS_APHDLR_DB_INIT,
 WSS_APHDLR_GET_WTPREBOOT_STATS,
 WSS_APHDLR_SET_WTPREBOOT_STATS,
 WSS_APHDLR_GET_LOCAL_IPV4,
 WSS_APHDLR_SET_AP_STATS,
 WSS_APHDLR_GET_AP_STATS
};

typedef struct{
    UINT1 wlcName[512];
    UINT1 u1Priority;
    UINT1 au1pad[3];
}tACNameWithPrioList;


typedef struct {
    tApParams  ApStats;
    UINT4  u4LocalIpAddr;
    UINT2         u2RebootCount;
    UINT2  u2AcInitiatedCount;
    UINT2  u2LinkFailCount;
    UINT2  u2SwFailCount;
    UINT2  u2HwFailCount;
    UINT2  u2OtherFailCount;
    UINT2  u2UnknownFailCount;
    UINT2  u1LastFailureType;
    UINT1  u1ProtocolType;
    UINT1  u2MaxMsgLen;
    UINT1       au1pad[2];
}tWssIfApHdlrDB;

typedef enum {
        CAPWAP_START=0,
        CAPWAP_IDLE,
        CAPWAP_DISCOVERY,
        CAPWAP_DTLS,
        CAPWAP_JOIN,
        CAPWAP_CONFIGURE,
        CAPWAP_DATACHECK,
        CAPWAP_RUN,
        CAPWAP_IMAGEDATA,
        CAPWAP_RESET,
        CAPWAP_SULKING,
        CAPWAP_DTLSTD,
        CAPWAP_DEAD,
        CAPWAP_MAX_STATE
} eState;

/* Table to maintain the mapping between the Incoming IP/UDP port number and CAPWAP Remote session Index */
typedef struct
{
  tRBNodeEmbd        nextSessionPortMap;
  tIpAddr            DestIp;
  UINT4              u4DestPort;
  UINT2              u2CapwapRSMIndex;
  UINT1              au1pad[2];
}tCapwapSessDestIpPortMapTable;

/* Table to maintain the mapping between the WTP internal Profile and CAPWAP Remote session Index */
typedef struct
{
  tRBNodeEmbd        nextSessionProfileIdMap;
  UINT2              u2WtpProfileInternalId;
  UINT2              u2CapwapRSMIndex;

}tCapwapSessProfileIdMapTable;

#ifdef WLC_WANTED
/*Table for mapping priority type with Dscp value*/
typedef struct
{
  tRBNodeEmbd        nextDscpValueMap;
  UINT4           u4IfIndex;
  UINT2              u2WlanProfileId;
  UINT1              u1InPriority;
  UINT1              u1OutDscp;
  INT4           i4RowStatus;
}tCapwapDscpMapTable;
#endif

#ifdef WTP_WANTED/*Table for mapping priority type with Dscp value*/
typedef struct
{
  tRBNodeEmbd        nextDscpValueMap;
  UINT1              au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
  UINT1              u1InPriority;
  UINT1              u1OutDscp;
  UINT1              au1pad[2];
}tCapwapDscpMapTable;

#endif
typedef struct
{
  tRBNodeEmbd nextBindingTableEntry;
  UINT2         u2WlanProfileId;
  UINT1  au1ModelNumber[MAX_WTP_MODELNUMBER_LEN];
  UINT1  u1RadioId;
  UINT1  u1RowStatus;
}tCapwapBindingTable;

typedef struct {
    tTmrBlk  ReassemblyTimer;
    tTMO_DLL       FragList;         /* List of fragments collected so far */
    UINT4  u4FirstFragTime;
    UINT2  u2FragmentId;
    UINT2  u2TotalLen;
    UINT1  u1NumberOfFragments;
    UINT1       au1pad[3];
}tCapFragment;



/* ------------------------------------------------------------------
 *                Remote Session Module
 * This structure contains all the Remote session module information
 * for CAPWAP sessions .
 * ----------------------------------------------------------------- */

typedef struct {
    tCapFragment                CapFragStream;
    tCapFragment                CapDataFragStream[CAPWAP_MAX_DATA_FRAG_INSTANCE];
    tIpAddr                     remoteIpAddr;
    eState                      eCurrentState;
    eState                     ePrevState;
    tTmrBlk                     EchoInterval;
    tTmrBlk                     DataChnlKeepAliveTmr;
    tTmrBlk                     PrimaryDiscIntervalTmr;
    tTmrBlk                     DataChnlDeadIntTmr;
    tTmrBlk                     RetransIntervalTmr;
    tTmrBlk                     IdleIntervalTmr;
    tTmrBlk                     WaitDtlsTmr;
    tTmrBlk                     WaitJoinTmr;
    tTmrBlk                     ChangeStatePendingTmr;
    tTmrBlk                     DataCheckTmr;
    tTmrBlk               ReassemblyTmr;
    tTmrBlk               DataTransferStartTmr;

    tCRU_BUF_CHAIN_HEADER      *lastTransmittedPkt;
    tUtlDynSll                  CapwapTxList;
                              /* This filed contain the pointer for
                                * Requests messages which needs to be processed
                                * by CAPWAP module. After sending each request message
                                * wait for the reponse message.*/
    FILE                        *writeFd;
    FILE                        *readFd;
    UINT4                       u4DtlsSessId;
    UINT4                       sessionId;     
    UINT4                       u4RemoteCtrlPort;
    UINT4                       u4RemoteDataPort;
    UINT4                       JoinSessionID[4];
    UINT4                       PMTU;
    UINT4                       lastTransmittedPktLen;
    UINT4                       u4TunnelId; /*Tunnel Id in NPAPI */
    UINT4                       wtpKeepAlivePktsSent;    
    UINT4                       u4FileReadOffset;
    UINT2                       u2IntProfileId;
    UINT2          u2CtrllastfragID;
    UINT2                       u2CapwapRSMIndex;
    UINT1                       lastProcesseddSeqNum;
    UINT1                       lastTransmitedSeqNum;
    /* Data Path */
    /* tFragment                 *FragmentData; */
    UINT4                       WtpDataIfIndex;         
    UINT4                       u4ImgDataFileWriteOffset;
    /* Retransmit */
    UINT4                       u4FileWriteOffset;
    tTmrBlk                     ImageDataStartTmr;
    tTmrBlk                     StatsIntervalTmr;
    BOOL1                       isRequestMsg;
    BOOL1                       isResponsePending;

    UINT1       au1pad[2];
    /* Data Transfer Info */
    FILE                        *readCrashFd;
    FILE                        *writeCrashFd;
    UINT1                       u1CrashEndofFileReached;
    UINT1                       u1CrashWriteEndofFileReached;
    UINT1                       u1CrashWriteFileOpened;
    UINT1                       u1CrashReadFileOpened;
    INT4                        i4CrashFileSize;
    UINT4                       u4CrashFileReadOffset;
    UINT1                       au1CrashFileName[280];
    UINT1                       u1CrashFileNameSet;
    UINT1                       u1DataTransferTimerRunning;
    /* Image Data Info */
    UINT1                       u1EndofFileReached;
    UINT1                       u1ImageDataTimerRunning;
    INT4                        i4FileSize;
    UINT1                       u1WriteFileOpened;
    UINT1                       u1ReadFileOpened;
    UINT1                       u1RetransmitCount;
    UINT1                       u1Used;
    UINT1   au1Name[512];
    tMacAddr                    WtpMacAddress;
    UINT1                       u1ModuleId;
    UINT1                       u1DtlsFlag;
    tImageId                    ImageId;
    tWtpCapwapSessStats         capwapSessStats;
    tWtpCapwapSessDiscStats     wtpCapwapSessDiscStats;
    tWtpCapwapSessJoinStats     wtpCapwapSessJoinStats;
    tWtpCapwapSessConfigStats   wtpCapwapSessConfigStats;
    tWtpCPWPWtpStats            wtpCapwapStats;    
    UINT1                       u1LastImgDataSeqNum;
    UINT1                       u1EchoCount;
    UINT1                       u1KeepAliveCount;
    UINT1                       u1DataDtlsFlag;
}tRemoteSessionManager;


enum {
    WSS_CAPWAP_MODULE=1,
    WSS_RADIOIF_MODULE
};

enum {
 WSS_CAPWAP_INIT_DB,
 WSS_CAPWAP_SHUTDOWN_DB,
 WSS_CAPWAP_GET_DB,
 WSS_CAPWAP_SET_DB,
 WSS_CAPWAP_VALIDATE_DB,
 WSS_CAPWAP_GET_TMR,
 WSS_CAPWAP_GET_RSM,
 WSS_CAPWAP_GET_RSM_FROM_CAPWAPID,
   WSS_CAPWAP_GET_RSM_WTP,
 WSS_CAPWAP_CREATE_RSM_WTP,
 WSS_CAPWAP_GET_RSM_FROM_INDEX,
 WSS_CAPWAP_GET_WTP_RSM,
 WSS_CAPWAP_DELETE_RSM,
 WSS_CAPWAP_CREATE_RSM,
        WSS_CAPWAP_CREATE_SESSION_PORT_ENTRY,
        WSS_CAPWAP_DELETE_SESSION_PORT_ENTRY,
 WSS_CAPWAP_GET_SESSION_PORT_ENTRY,
 WSS_CAPWAP_CREATE_TEMPDTLS_ENTRY,
        WSS_CAPWAP_GET_RSM_FROM_SESS_IDX,
        WSS_CAPWAP_DELETE_TEMPDTLS_ENTRY,
 WSS_CAPWAP_GET_TEMPDTLS_ENTRY,
 WSS_CAPWAP_CREATE_SESSPROFID_ENTRY,
 WSS_CAPWAP_DELETE_SESSPROFID_ENTRY,
 WSS_CAPWAP_GET_SESSPROFID_ENTRY,
 WSS_CAPWAP_GET_CTRLUDP_PORT,
 WSS_CAPWAP_GET_MODEL_TABLE,
 WSS_CAPWAP_GET_CTRL_IP4_ADDR,
 WSS_CAPWAP_GET_AC_IPV6_LIST,
 WSS_CAPWAP_GET_WTP_FALLBACK,
        WSS_CAPWAP_GET_RSM_FROM_SESSIONID,
        WSS_CAPWAP_DELETE_RSM_FROM_SESSIONID,
 WSS_CAPWAP_SET_PROFILE_ENTRY,
 WSS_CAPWAP_GET_PROFILE_ENTRY,
 WSS_CAPWAP_DESTROY_PROFILE_ENTRY,
    WSS_CAPWAP_CREATE_PROFILE_ENTRY,
 WSS_CAPWAP_GET_INDEX_FROM_MAC,   /*30*/
    WSS_CAPWAP_CREATE_PROFILE_MAPPING_ENTRY,
    WSS_CAPWAP_GET_PROFILE_MAPPING_ENTRY,
    WSS_CAPWAP_GET_PROFILEID_MAPPING_ENTRY,
    WSS_CAPWAP_DELETE_PROFILE_MAPPING_ENTRY,
    WSS_CAPWAP_SET_WTP_MODEL_ENTRY,
    WSS_CAPWAP_GET_WTP_MODEL_ENTRY,
    WSS_CAPWAP_DESTROY_WTP_MODEL_ENTRY,
    WSS_CAPWAP_CREATE_WTP_MODEL_ENTRY,
    WSS_CAPWAP_DELETE_WTP_MODEL_MAPPING_ENTRY,
    WSS_CAPWAP_SET_WTP_RADIO_ENTRY,
    WSS_CAPWAP_CREATE_WTP_RADIO_ENTRY,
    WSS_CAPWAP_DESTROY_WTP_RADIO_ENTRY,
    WSS_CAPWAP_SHOW_WTPSTATE_TABLE,
    WSS_CAPWAP_GET_INTERNAL_ID,
    WSS_CAPWAP_GET_CAPW_ATP_ID,
    WSS_CAPWAP_GET_SESS_STATS,
    WSS_CAPWAP_CLEAR_SESS_STATS,
    WSS_CAPWAP_GET_ACDESCRIPTOR,
    WSS_CAPWAP_DESTROY_BASEACNAMELIST_ENTRY,
    WSS_CAPWAP_CREATE_DNS_PROFILE_ENTRY,
    WSS_CAPWAP_SET_DNS_PROFILE_ENTRY,
    WSS_CAPWAP_GET_DNS_PROFILE_ENTRY,
    WSS_CAPWAP_CREATE_BASEACNAMELIST_ENTRY,
    WSS_CAPWAP_MAC_FILTER_SET,
    WSS_CAPWAP_MAC_FILTER_DELETE,
    WSS_CAPWAP_MAC_FILTER_CREATE,
    WSS_CAPWAP_CREATE_MACADDR_MAPPING_ENTRY,
    WSS_CAPWAP_GET_MACADDR_MAPPING_ENTRY,
    WSS_CAPWAP_DELETE_MACADDR_MAPPING_ENTRY,
    WSS_CAPWAP_CREATE_BLACKLIST_ENTRY,
    WSS_CAPWAP_DELETE_BLACKLIST_ENTRY,
    WSS_CAPWAP_GET_BLACKLIST_ENTRY,
    WSS_CAPWAP_CREATE_WHITELIST_ENTRY,
    WSS_CAPWAP_DELETE_WHITELIST_ENTRY,
    WSS_CAPWAP_GET_WHITELIST_ENTRY,
    WSS_CAPWAP_GET_STA_WHITELIST_ENTRY,
    WSS_CAPWAP_SET_STA_WHITELIST_ENTRY,
    WSS_CAPWAP_CREATE_STA_WHITELIST_ENTRY,
    WSS_CAPWAP_DELETE_STA_WHITELIST_ENTRY,
    WSS_CAPWAP_GETFIRST_STA_WHITELIST_ENTRY,
    WSS_CAPWAP_GETNEXT_STA_WHITELIST_ENTRY,
    WSS_CAPWAP_GET_STA_BLACKLIST_ENTRY,
    WSS_CAPWAP_SET_STA_BLACKLIST_ENTRY,
    WSS_CAPWAP_CREATE_STA_BLACKLIST_ENTRY,
    WSS_CAPWAP_DELETE_STA_BLACKLIST_ENTRY,
    WSS_CAPWAP_GETFIRST_STA_BLACKLIST_ENTRY,
    WSS_CAPWAP_GETNEXT_STA_BLACKLIST_ENTRY,
    WSS_CAPWAP_GET_MODEL_COUNT,
    WSS_CAPWAP_CREATE_DSCP_MAP_ENTRY,
    WSS_CAPWAP_DELETE_DSCP_MAP_ENTRY
};

/* If the CAPWAP receives the DTLS notitfications enrty created in the below table */
typedef struct {
     tRBNodeEmbd     nextCapDtlsSessIdMap;
     UINT4           u4IpAddr;
     UINT4           u4DtlsId;
     tMacAddr        MacAddress;
     UINT1           au1pad[2];

}tCapwapDtlsSessEntry;

typedef struct {
    UINT4       ReqRxd;
    UINT4       RespSent;
    UINT4       UnsuccReqProcessed;
    UINT4       ReasonLastUnsuccAttempt;
    UINT4       LastSuccAttemptTime;
    UINT4       LastUnsuccAttemptTime;
} tWssIfSessStats;

typedef struct {
   tWssIfSessStats    CapDisc;
   tWssIfSessStats    CapJoin;
   tWssIfSessStats    CapConfig;
   tWssIfSessStats    CapEcho;
   tWssIfSessStats    CapKeeAlive;
   tWssIfSessStats    CapConfUpdate;
   tWssIfSessStats    CapStaUpdate;
   tWssIfSessStats    CapData;
   tWssIfSessStats    CapWlanUpdate;
   tWssIfSessStats    CapPriDisc;
   tWssIfSessStats    CapClearConf;
   tWssIfSessStats    CapReset;
   tWssIfSessStats    CapRun;
}tWssIfCapwapStats;

typedef struct {
    UINT2       u2WtpRebootCount;
    UINT2       u2AcIntiatedCount;
    UINT2       u2WtpLinkFailureCount;
    UINT2       u2WtpSwFailureCount;
    UINT2       u2WtpHwFailureCount;
    UINT2       u2WtpOtherFailCount;
    UINT2       u2WtpUnknownFailCount;
    UINT1       u1WtpLastFailureType;
    UINT1       au1pad[1];
}tApRebootStats;

typedef struct
{
    BOOL1       bWtpDefaultProfile;
    BOOL1 bWlcDescriptor;
    BOOL1  bWtpDescriptor;
    BOOL1 bWtpPhyIndex;
    BOOL1       bMaxStationsSupported;
    BOOL1       bMacType;
    BOOL1       bTunnelMode;
    BOOL1 bAcNamewithPriority;
    BOOL1       bAcName;
    BOOL1 bDiscPadding;
    BOOL1 bRadioId;
    BOOL1       bRadioIfDB;
    BOOL1       bWtpInternalId;
    BOOL1 bTransportProtocol;
    BOOL1       bProfileName;
    BOOL1       bWtpProfileId;
    BOOL1       bWtpMacAddress;
    BOOL1       bWtpModelNumber;
    BOOL1 bWtpSerialNumber;
    BOOL1 bWtpBoardId;
    BOOL1       bWtpBoardRivision;
    BOOL1 bVendorSMI;
    BOOL1       bWtpName;
    BOOL1       bWtpLocation;
    BOOL1       bWtpLocalRouting;
    BOOL1       bFragReassembleStatus;
    BOOL1       bPathMTU;
    BOOL1       bWtpStaticIpEnable;
    BOOL1       bWtpStaticIpType;
    BOOL1       bWtpStaticIpAddress;
    BOOL1       bWtpNetmask;
    BOOL1       bWtpGateway;
    BOOL1       bWtpFallbackEnable;
    BOOL1       bWtpEchoInterval;
    BOOL1       bWtpIdleTimeout;
    BOOL1       bWtpMaxDiscoveryInterval;
    BOOL1       bWtpReportInterval;
    BOOL1       bWtpStatisticsTimer;
    BOOL1       bWtpEcnSupport;
    BOOL1 bWlcEcnSupport;
    BOOL1       bWtpDiscType;
    BOOL1       bRowStatus;
    BOOL1       bCtrlDTLSStatus;
    BOOL1 bDTLSAuth;
    BOOL1 bDTLSEncrypt;
    BOOL1       bIpAddressType;
    BOOL1       bIpAddress;
    BOOL1 bCapwapState;
    BOOL1 bNoOfRadio;
    BOOL1 bRadioCount;
    BOOL1 bNoOfRadioLimit;
    BOOL1 bWtpMacType;
    BOOL1 bWtpTunnelMode;  
    BOOL1 bWtpRadioType;
    BOOL1 bRadioAdminStatus;
    BOOL1 bMaxSSIDSupported; 
    BOOL1 bDiscStats;
    BOOL1 bJoinStats;
    BOOL1 bConfigStats;
    BOOL1 bRunStats; 
    BOOL1 bModelCheck;
    BOOL1 bWtpQosPofile;
    BOOL1 bSwVersion;
    BOOL1 bHwVersion;
    BOOL1 bBootVersion; 
    BOOL1 bMaxMsgLen;
    BOOL1 bWtpRebootStats;
    BOOL1 bDataDTLSStatus;
    BOOL1 bActiveWtps;
    BOOL1 bMaxWtps;
    BOOL1 bNativeVlan;
    BOOL1 bLocalIpAddr;
    BOOL1       bCapwapDnsServerIp;
    BOOL1       bCapwapDnsDomainName;
    BOOL1       bPrimaryBaseController;
    BOOL1       bSecondaryBaseController;
    BOOL1       bTeritaryBaseController;
    BOOL1       bWtpCtrlDTLSStatus;
    BOOL1       bWtpCtrlDTLSCount;
    BOOL1       bWtpDataDTLSStatus;
    BOOL1       bDiscMode;
    BOOL1       bCtrlUdpPort;
    BOOL1       bImageName; 
    BOOL1       bModelCount;
    BOOL1       bWtpUpTime;
    BOOL1       bWlcStaticIp;
    BOOL1       bWlcIpAddress;
    BOOL1       bWlcCtrlPort;
    BOOL1       bWtpDataPort;
    BOOL1       bHtFlag;
    BOOL1       bMCSRateSet;
    BOOL1       bImageTransfer;
    BOOL1 bIntfName;
    BOOL1       bFreeAssocId;
    BOOL1 bLocalRouting;
    UINT1   au1Pad;
}tCapwapIsGetAllDB;

typedef struct
{
    tRadioIfDB  *pRadioIFDB[31];
    tWtpModelEntry      WtpModelEntry;
    tIpAddr             WtpStaticIpAddress;
    tIpAddr             WtpNetmask;
    tIpAddr             WtpGateway;
    tACNameWithPrioList AcNameWithPri[MAX_NUM_AC_FALLBACK];
    tWssIfCapwapStats CapwaStats;
    tApRebootStats      WtpRebootStats;
    tIpAddr          IpAddress;
    UINT4          u4VendorSMI;
    UINT4               u4NativeVlan;
    UINT4          u4WtpPhyIndex;
    UINT4               u4WtpIdleTimeout;
    UINT4               WtpCtrlIfIndex;
    UINT4               WtpDataIfIndex;
    UINT4          u4IfIndex;
    UINT4            u4BaseAcNameListId;
    UINT4          u4CapwapState; 
    UINT4               u4WtpUpTime;
    UINT4               u4WlcStaticIp;
    UINT4               u4WlcCtrlPort;
    UINT4               u4WlcIpAddress;
    UINT4               u4WtpDataPort;
    UINT4               u4WtpCtrlDTLSCount; 
    UINT4               u4PathMTU;
    UINT4               u4AssocIdMapping[MAX_AID_INDEX]; /*This is used to store the mapping     of */
                                                          /*whether AID is used or free*/
    UINT4               u4FreeAssocId;

    tMacAddr            WtpMacAddress;
    UINT2               u2ProfileId;
    UINT2          u2NoOfRadioLimit;
    UINT2               u2ProfildId;
    UINT2            u2MaxStationsSupported;
    UINT2               u2WtpProfileId;  /* range 0 - 4096 */
    UINT2               u2WtpInternalId;  /* range 1 - MAX_AP */
    UINT2               u2WtpEchoInterval;
    UINT2               u2WtpMaxDiscoveryInterval;
    UINT2               u2WtpReportInterval;
    UINT2               u2WtpStatisticsTimer;
    UINT2               u2MaxMsgLen;

    UINT1               au1ProfileName[1024];
    UINT1               u1WtpModelNumber[1024];
    UINT1               au1WtpName[512];
    UINT1               au1WtpLocation[512];
    UINT1          au1DiscPadding[1500];
    UINT1            u1RadioType[31];
    UINT1          u1ProtocolType;
    UINT1          au1WtpModelNumber[MAX_WTP_MODELNUMBER_LEN];
    UINT1          au1WtpTunnelMode[256];
    UINT1               au1WtpSerialNumber[1024];
    UINT1          u1WtpRadioType[31];
    UINT1          u1WtpMacType;
    UINT1               au1WtpBoardId[1024];
    UINT1               au1WtpBoardRivision[1024];
    UINT1               au1WtpImageIdentifier[1024]; 
    UINT1               au1WtpHWversion[1024];
    UINT1               au1WtpSWversion[1024];
    UINT1               au1WtpBootversion[1024];
    UINT4          au4WtpRadioType[31]; 
    UINT1          u1WtpNoofRadio;
    UINT1          au1WtpRadioAdminStatus[31];
    UINT1            u1WtpCtrlDTLSStatus;
    UINT1          au1WtpMaxSSIDSupported[31];
    UINT1          u1ModelCheck;
    UINT1               u1RadioId;
    UINT1               u1MacType;
    UINT1               u1NumOfRadio;
    UINT1               u1WtpLocalRouting;
    UINT1               u1WtpStaticIpEnable;
    UINT1               u1WtpTunnelMode;
    UINT1               u1TunnelMode;
    UINT1               u1WtpStaticIpType;
    UINT1               u1WtpAddrType;
    UINT1               u1WtpFallbackEnable;
    UINT1               u1WtpEcnSupport;
    UINT1               u1RowStatus;
    UINT1               u1WtpDiscType;
    UINT1            u1WtpDataDTLSStatus;
    UINT1               au1WtpImageIdLen;
    UINT1               u1HtFlag;
    UINT1               au1MCSRateSet[16];
    UINT1               u1FragReassembleStatus;
    UINT1               u1ImageTransferFlag;
    UINT1              au1IntfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1              au1Pad;
}tCapwapGetAllDB;

typedef struct {
   tCapwapGetAllDB      CapwapGetDB[NUM_OF_AP_SUPPORTED];
   tWtpModelEntry    WtpModelEntry[10];
   tIpAddr    LocalIpAddr; /* Local IP Address or WLC or WTP */
   UINT4                u4CtrludpPort;
   UINT4                u4DtlsId;
   UINT4    u4VendorSMI;
   UINT1                au1WlcHWversion[1024];
   UINT2    u2ActiveStation;
   UINT2    u2MaxStationLimit;
   UINT2    u2ActiveWtps;
   UINT2    u2MaxWtps;
   UINT1               au1WlcSWversion[1024];
   UINT1    au1WlcName[512];
   UINT1    u1Security;  
   UINT1    u1RMAC;
   UINT1                u1DiscMode;
   UINT1                u1AcRefOption;
   UINT1                u1CtrlDTLSStatus;
   UINT1                u1DataDTLSStatus;
   UINT1    u1ModelCheck;
   UINT1    u1WlcEcnSupport;
}tWssIfCapwapDB;

typedef struct {
    UINT4 au1CapwapBaseWtpProfileId;
    INT4 i4FsCapwapDnsProfileRowStatus;
    INT4 i4FsCapwapDnsServerIpLen;
    INT4 i4FsCapwapDnsDomainNameLen;
    UINT1 au1FsCapwapDnsServerIp[4];
    UINT1 au1FsCapwapDnsDomainName[32];
}tCapwapDnsProfileEntry;

typedef struct {
    UINT4        u4WhitelistId;
    INT4         i4BaseMac;
    INT4         i4RowStatus;
    UINT4        u4BlacklistId;
    INT4         i4BlacklistBaseMac;
    INT4         i4BlacklistRowStatus;
}tMacFilterGetDB;

typedef struct {
   tCapwapIsGetAllDB                     CapwapIsGetAllDB;
   tCapwapGetAllDB                       CapwapGetDB;
   tMacFilterGetDB                       CapwapBaseDB;
   tIpAddr                               LocalIpAddr;
   tRemoteSessionManager                 *pSessEntry;
   tCapwapSessProfileIdMapTable          *pSessProfileIdEntry;
   tCapwapDtlsSessEntry                  *pDtlsEntry;
   tCapwapDnsProfileEntry                 DnsProfileEntry;
   tCapwapSessDestIpPortMapTable          *pSessDestIpEntry;

   UINT1                 au1WlcName[512];
   UINT1                 au1WlcHWversion[1024];
   UINT1                 au1WlcSWversion[1024];
   UINT4                 u4DestIp;
   UINT4                 u4DestPort;
   UINT4                 u4CtrludpPort;
   UINT4                 u4DtlsId;
   UINT4                 u4VendorSMI;
   UINT4     u4DTLSAuth;
   UINT4     u4DTLSEncrypt;
   UINT4                 JoinSessionID[4];
   tMacAddr              BaseMacAddress;
   UINT2     u2ActiveWtps;
   UINT2                 u2ActiveStation;
   UINT2                 u2MaxStationLimit;
   UINT2                 u2MaxWtps;
   /*UINT2               u2WtpInternalId;
   UINT2     u2ProfildId;*/
   UINT2                 u2CapwapRSMId;
   UINT1                 u1AcRefOption;
   UINT1                 u1CtrlDTLSStatus;
   UINT1                 u1DataDTLSStatus;
   UINT1                 u1Security;
   UINT1                 u1RMAC;
   UINT1                 u1DiscMode;
   UINT1    u1HtFlag;
   UINT1   au1MCSRateSet[16];
   UINT1                 u1pad;
   UINT4                 u4IfIndex;
   UINT1                 u1InPriority;
   UINT1                 u1OutDscp;
   UINT1                 au1pad[2];
   INT4                  i4stationRowStatus;
}tWssIfCapDB;

typedef struct {
    tRBNodeEmbd        nextProfile;
    UINT4              u4WtpProfileId;
    UINT2              u2WtpInternalId;
    UINT1              au1pad[2];
}tWssIfProfile;

typedef struct {
    tRBNodeEmbd        nextProfileIdMap;
    UINT4              u4WtpProfildId;
    UINT2              u2WtpProfileInternalId;
    UINT1               au1pad[2];
}tWssIfProfileIdMap;

typedef struct {
    tRBNodeEmbd        nextProfileMacMap;
    UINT2              u2WtpProfileId;
    UINT2              u2WtpInternalId;
    tMacAddr           MacAddr; 
    UINT1              au1Pad[2];
}tWssIfProfileMacMap;

typedef struct
{
        tRBNodeEmbd FsCapwATPStatsTableNode;
        UINT4 u4CapwapBaseWtpProfileId;
        UINT4 u4FsCapwATPStatsSentBytes;
        UINT4 u4FsCapwATPStatsRcvdBytes;
        UINT4 u4FsCapwATPStatsWiredSentBytes;
        UINT4 u4FsCapwATPStatsWirelessSentBytes;
        UINT4 u4FsCapwATPStatsWiredRcvdBytes;
        UINT4 u4FsCapwATPStatsWirelessRcvdBytes;
        UINT4 u4FsCapwATPStatsSentTrafficRate;
        UINT4 u4FsCapwATPStatsRcvdTrafficRate;
        UINT4 u4FsCapwATPStatsTotalClient;
        UINT4 u4FsCapwATPStatsMaxClientPerAPRadio;
        UINT4 u4FsCapwATPStatsMaxClientPerSSID;

}tWssifATPStatsMap;

typedef struct {
    tRBNodeEmbd        nextSSIDMap;
    UINT4 u4FsDot11WlanProfileId;
    UINT4 u4FsWlanSSIDStatsMaxClientCount;
    UINT4 u4FsWlanSSIDStatsAssocClientCount;
}tWssIfSSIDMap;

typedef struct {
    tRBNodeEmbd        nextProfileNameMap;
    UINT1              au1WtpProfileName [256];
    UINT2              u2WtpProfileId;
    UINT1              au1Pad[2];
}tWssIfProfileNameMap;

typedef struct {
 tRBNodeEmbd         nextWtpModelEntry;
 UINT1    au1ModelNumber[MAX_WTP_MODELNUMBER_LEN];
 UINT1    u2ModelIndex;
 UINT1               au1pad[3];
}tWssIfWtpModelMap;

typedef struct {
    tRBNodeEmbd         nextWlcMacEntry;
    tMacAddr            MacAddr;
    UINT1               au1pad[2];
}tWssIfWlcWhiteListDB;
typedef struct {
    tRBNodeEmbd         nextWlcMacEntry;
    tMacAddr            MacAddr;
    UINT1               au1pad[2];
}tWssIfWlcBlackListDB;

typedef struct {
    tRBNodeEmbd         nextStaMacEntry;
    tMacAddr            MacAddr;
    UINT1               au1pad[2];
    INT4                i4RowStatus;
}tWssIfStaWhiteListDB;

typedef struct {
    tRBNodeEmbd         nextStaMacEntry;
    tMacAddr            MacAddr;
    UINT1               au1pad[2];
    INT4                i4RowStatus;
}tWssIfStaBlackListDB;
 
typedef struct {
    tRBNodeEmbd         nextDhcpPoolId;
    UINT4               u4DhcpPoolId;
    UINT4               u4Subnet;
    UINT4               u4Mask;
    UINT4               u4StartIp;
    UINT4               u4EndIp;
    UINT4               u4LeaseExprTime;  
    UINT4               u4DefaultIp;  
    UINT4               u4DnsServerIp;  
    INT4                i4RowStatus;
}tWssIfDhcpPoolDB;
 
typedef struct {
    tRBNodeEmbd         nextWtpDhcpPoolId;
    UINT4               u4DhcpPoolId;
    UINT4               u4Subnet;
    UINT4               u4Mask;
    UINT4               u4StartIp;
    UINT4               u4EndIp;
    UINT4               u4LeaseExprTime;  
    UINT4               u4DefaultIp;  
    UINT4               u4DnsServerIp;  
    INT4                i4RowStatus;
    UINT4               u4WtpProfileId;
}tWssIfWtpDhcpPoolDB;

typedef struct {
    tRBNodeEmbd         nextNATConfigEntry;
    UINT4               u4WtpProfileId;
    INT4  i4NATConfigEntryIndex;
    INT4  i4RowStatus;
    INT1  i1WanType;
    UINT1  au1Pad[3];
}tWssIfNATConfigEntryDB;

typedef struct {
    tRBNodeEmbd         nextWtpLRConfigEntry;
    UINT4  u4WtpProfileId;
    INT4  i4WtpDhcpServerStatus;
    INT4  i4WtpDhcpRelayStatus;
    INT4  i4WtpFirewallStatus;
    INT4  i4RowStatus;
}tWssIfWtpLRConfigDB;

typedef struct {
    tRBNodeEmbd  nextWtpIpRouteConfigEntry;
    UINT4  u4WtpProfileId;
    UINT4  u4Subnet;
    UINT4  u4Mask;
    UINT4  u4Gateway;
    INT4  i4RowStatus;
}tWssIfWtpIpRouteConfigDB;

typedef struct {
    tRBNodeEmbd  nextWtpDhcpConfigEntry;
    UINT4  u4WtpProfileId;
    INT4  i4WtpDhcpServerStatus;
    INT4  i4WtpDhcpRelayStatus;
}tWssIfWtpDhcpConfigDB;

typedef struct {
    union {
    tIp4Addr    Ip4Addr;
    tIp6Addr    Ip6Addr;
    } uIpAddr;
    UINT4       u4AddrType;
#define v4Addr   uIpAddr.Ip4Addr
#define v6Addr   uIpAddr.Ip6Addr
} tWtpFwlIpAddr;
typedef UINT4         COUNTER32;
typedef UINT2         COUNTER16;
typedef UINT1         GAUGE;
typedef struct {
    tRBNodeEmbd     nextWtpFwlFilterEntry;
    UINT4          u4WtpProfileId;
    UINT4          u4FlowId;
    UINT1          au1FilterName [AP_FWL_MAX_FILTER_NAME_LEN + 1];
    UINT1          u1Pad[3];
  /* The following four members are used during SNMP GET operation. If not
  * present the ports and the IP address will not be in format as in SET. */
    UINT1          au1SrcPort[AP_FWL_MAX_PORT_LEN];
    UINT1          au1DestPort[AP_FWL_MAX_PORT_LEN];
    UINT1          au1SrcAddr[AP_FWL_MAX_ADDR_LEN];
    UINT1          au1DestAddr[AP_FWL_MAX_ADDR_LEN];
    UINT1          u1Pad1[2];
    tWtpFwlIpAddr  SrcStartAddr;
    tWtpFwlIpAddr  SrcEndAddr;
    tWtpFwlIpAddr  DestStartAddr;
    tWtpFwlIpAddr  DestEndAddr;
    COUNTER32      u4FilterHitCount;
    INT4           i4RowStatus;
    INT4           i4Dscp;
    UINT2          u2SrcMaxPort;
    UINT2          u2SrcMinPort;
    UINT2          u2DestMaxPort;
    UINT2          u2DestMinPort;
    UINT2          u2AddrType;
    UINT1          u1Proto;
    UINT1          u1Tos;     /*  TOS support */
    UINT1          u1TcpAck;
    UINT1          u1TcpRst;
    GAUGE          u1FilterRefCount;
    UINT1          u1FilterAccounting; /* enable or disable filter accounting*/
    UINT1          u1Reserved;
    UINT1          au1Padding[3];
}tWssIfWtpFwlFilterDB;
typedef struct {
    tRBNodeEmbd     nextWtpFwlRuleEntry;
    tWssIfWtpFwlFilterDB  *apFilterInfo [AP_FWL_MAX_FILTERS_IN_RULE];
    UINT4          u4WtpProfileId;
    UINT1          au1RuleName [AP_FWL_MAX_RULE_NAME_LEN];
    UINT1          au1FilterSet [AP_FWL_MAX_FILTER_SET_LEN]; /* This string is  
                                                           * used during 
                                                           * SNMP GET. 
                                                           */
    INT4           i4RowStatus;
    UINT2          u2MinSrcPort;
    UINT2          u2MaxSrcPort;
    UINT2          u2MinDestPort;
    UINT2          u2MaxDestPort;
    UINT1          u1FilterConditionFlag;        /* Specifies, the filter
                                                  * combination is "&" or
                                                  * "Or" operator.
                                                  */
    GAUGE          u1RuleRefCount;
    UINT1          u1Reserved;
    UINT1          au1Pad;
}tWssIfWtpFwlRuleDB;

typedef struct {
    tRBNodeEmbd    nextWtpFwlAclEntry;
    UINT4          u4WtpProfileId;
    UINT1          au1AclName[AP_FWL_MAX_ACL_NAME_LEN];
/*
    //tWssIfWtpFwlAclDB  *inFilterList [AP_FWL_MAX_FILTERS_IN_RULE];
    //tWssIfWtpFwlAclDB  *outFilterList [AP_FWL_MAX_FILTERS_IN_RULE];
   */
    INT4           i4AclIfIndex;
    UINT4          u4StartTime;
    UINT4          u4EndTime;
    UINT4          u4Scheduled;  /* FWL_ENABLE -or- FWL_DISABLE */ 
    INT4           i4RowStatus;                          
    UINT2          u2MaxFragmentSize; 
    UINT2          u2SeqNum;
    UINT1          u1AclDirection;
    UINT1          u1IfaceType;
    UINT1          u1Fragment;
    UINT1          u1Action;
    UINT1          u1LogTrigger;  
    UINT1          u1FragAction;  
    UINT1          u1AclType;
    UINT1          u1IpOption;
    UINT1          u1IcmpType;
    UINT1          u1IcmpCode;
    UINT1          u1Pad[2];   
    VOID           *pAclName;
} tWssIfWtpFwlAclDB;

#ifdef DEBUG_WANTED
struct NatAppDefn {
    INT1            AppName[8]; /* Application Name */
    UINT4           u4AppId;     /* Unique Key that identifies the Appln */
    UINT4           u4AppUseCount; /* No of active sessions */
    INT4           (*pProcessPkt) (tCRU_BUF_CHAIN_HEADER *, tHeaderInfo *);
};


typedef struct  {
    UINT4         u4InSeq;
    UINT4         u4InSeqDelta;
    UINT4         u4OutSeq;
    UINT4         u4OutSeqDelta;
    INT4          i4InDelta;
    INT4          i4OutDelta;
    UINT1         u1Status;
    UINT1         au1Pad[3]; /* For Padding only */
    UINT4         u4Direction;
}tNatSeqAckHistory;
#endif

typedef struct {
     tRBNodeEmbd     nextWtpDynamicNatEntry;
     UINT4          u4WtpProfileId;   
     UINT4          u4IfNum;
     UINT4          u4LocIpAddr;
     UINT4          u4TranslatedLocIpAddr;
     UINT4          u4OutIpAddr;
     UINT4          u4TimeStamp;
     UINT2          u2LocPort;
     UINT2          u2TranslatedLocPort;
     UINT2          u2OutPort;
     UINT1          u1PktType;
     UINT1          u1NaptEnable;
#ifdef DEBUG_WANTED
     struct NatAppDefn *pAppRec;
     tNatSeqAckHistory aNatSeqAckHistory[25];
#endif     
     UINT1          u1DeltaChangeFlag;
     UINT1          u1DeltaIndex;
     UINT1          u1AppCallStatus;
     UINT1          u1Type;
     UINT1          u1UsedFlag;
     UINT1          u1Direction;
     UINT1          u1RetainOnTmrExpiry;
     UINT1          u1Pad;
} tWssIfWtpDynamicNatDB;

/**** Type Definition for Local Address Table *****/

typedef struct {
     tRBNodeEmbd    nextWtpNatLocalAddrEntry;
     UINT4          u4WtpProfileId;
     UINT4          u4InterfaceNumber;
     UINT4          u4LocIpNum;
     UINT4          u4Mask;
     INT4           i4RowStatus;
}tWssIfWtpNatLocalAddrDB;

/**** Type Definition for Static Table *****/

typedef struct {
     tRBNodeEmbd    nextWtpStaticNatEntry;
     UINT4          u4WtpProfileId;
     UINT4          u4InterfaceNumber;
     UINT4          u4LocIpAddr;
     UINT4          u4TranslatedLocIpAddr;
     INT4           i4RowStatus;
}tWssIfWtpStaticNatDB;

/* UPNP NAPT TIMER */
/* Timer Structure ForEach Entries in NAPT Table */
/*typedef struct NaptTimerNode {
       tTmrAppTimer            TimerNode;
       UINT1                   u1TimerId;
       UINT1                   u1TimerStatus;
       UINT2                   u2Allignment;
       struct StaticNaptTable  *pNaptBackPtr;
       INT4                    i4IfIndex;
}tNaptTimer;
*/
/**** Type Definition for Static-Napt Table *****/

typedef struct {
    tRBNodeEmbd    nextWtpStaticNaptEntry;
    UINT4          u4WtpProfileId;
    /*tNaptTimer     NaptTimerNode; *//* Added Timer Field */
    UINT4          u4LocIpAddr;
    UINT4          u4TranslatedLocIpAddr;
    INT4           i4RowStatus;
    UINT2          u2StartLocPort;/* Start of port range */
    UINT2          u2EndLocPort; /* End of port range */
    UINT2          u2TranslatedLocPort;
    UINT2          u2ProtocolNumber;
    INT1           i1Description[255];
    UINT1          u1AppCallStatus; /*  HOLD or UNHOLD */
}tWssIfWtpStaticNaptDB;

/**** Type Definition for Global Address Table *****/


typedef struct {
    tRBNodeEmbd    nextWtpNatGlobalAddrEntry;
    UINT4          u4WtpProfileId;
    UINT4          u4InterfaceNumber;
    UINT4          u4TranslatedLocIpAddr;
    UINT4          u4Mask;
    INT4           i4RowStatus;
}tWssIfWtpNatGlobalAddrDB;

/**** Type Definition for Interface Information *****/

 typedef struct {
     tRBNodeEmbd    nextWtpNatIfTableEntry;
     UINT4          u4WtpProfileId;
     UINT1     u1NatEnable;
     UINT1     u1NaptEnable;
     UINT1     u1TwoWayNatEnable;
     UINT1     u1Reserved;
     tWssIfWtpStaticNatDB  StaticList;
     tWssIfWtpStaticNaptDB  StaticNaptList;
     tWssIfWtpNatGlobalAddrDB  TranslatedLocIpList;
     /*tTMO_SLL  NatFreeGipList;*/
     UINT4     u4NumOfTranslation;
     UINT4     u4NumOfActiveSessions;
     UINT4     u4NumOfPktsDropped;
     UINT4     u4NumOfSessionsClosed;
     UINT4     u4CurrTranslatedLocIpAddr;
     UINT4     u4TranslatedLocIpRangeEnd;
     UINT4     u4CurrDnsTranslatedLocIpInUse;
     UINT4     u4CurrDnsTranslatedLocIpRangeEnd;
     INT4      i4RowStatus;
}tWssIfWtpNatIfDB;                                                                     

/**** Type Definition for L3 SUB interface Information */

typedef struct {
   tRBNodeEmbd   nextWtpL3SubIfEntry;
  UINT4         u4WtpProfileId;
  UINT4         u4IfIndex;
  UINT4        u4PhyPort;
  UINT4        u4IpAddr;
  UINT4        u4SubnetMask;
  UINT4        u4BroadcastAddr;
  INT4        i4RowStatus;     
  INT4        i4IfType;
  INT4        i4IfNwType;
  UINT2        u2VlanId;
  UINT1        u1IfAdminStatus;
  UINT1        u1Reserved;
}tWssIfWtpL3SubIfDB;
#endif
