/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wssifpmdb.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains db structures for WSSPM module.
 ****************************************************************************/
#ifndef __WSSIFPM_H__
#define __WSSIFPM_H__


#include "wsspmdb.h"

typedef struct {
    BOOL1       bWssPmWLCCapwapDiscReqRxd;
    BOOL1       bWssPmWLCCapwapDiscRespSent;
    BOOL1       bWssPmWLCCapwapDiscUnsuccReqProcessed;
    BOOL1       bWssPmWLCCapwapDiscReasonLastUnsuccAtt;
    BOOL1       bWssPmWLCCapwapDiscLastSuccAttTime;
    BOOL1       bWssPmWLCCapwapDiscLastUnsuccAttTime;
    BOOL1       bWssPmWLCCapwapJoinReqRxd;
    BOOL1       bWssPmWLCCapwapJoinRespSent;
    BOOL1       bWssPmWLCCapwapJoinUnsuccReqProcessed;
    BOOL1       bWssPmWLCCapwapJoinReasonForLastUnsuccAtt;
    BOOL1       bWssPmWLCCapwapJoinLastSuccAttTime;
    BOOL1       bWssPmWLCCapwapJoinLastUnsuccAttTime;
    BOOL1       bWssPmWLCCapwapCfgRespRxd;
    BOOL1       bWssPmWLCCapwapCfgReqSent;
    BOOL1       bWssPmWLCCapwapCfgUnsuccRespProcessed;
    BOOL1       bWssPmWLCCapwapCfgReasonForLastUnsuccAtt;
    BOOL1       bWssPmWLCCapwapCfgLastSuccAttTime;
    BOOL1       bWssPmWLCCapwapCfgLastUnsuccAttTime;
    BOOL1       bWssPmWLCCapwapKeepAlivePkts;
    BOOL1       bWssPmWLCCapwapClearApStats;
}tWssPmWLCStatsSetAttributeDB;

typedef struct {
    tWssPmWLCStatsSetAttributeDB wssPmWLCStatsSetDB;
    BOOL1       bWssPmWtpAdd;
    BOOL1       bWssPmWtpDel;
    BOOL1       bWssPmBSSIDAdd;
    BOOL1       bWssPmBSSIDDel;
    BOOL1 bWssPmRadioAdd;
    BOOL1       bWssPmRadioDel;
    BOOL1       bWssPmSSIDAdd;
    BOOL1       bWssPmSSIDDel;
    BOOL1       bWssPmClientAdd;
    BOOL1       bWssPmClientDel;
    BOOL1       bWssPmCapwATPAdd;
    BOOL1       bWssPmCapwATPDel;
    BOOL1       bWssPmStatTmrReset;
    BOOL1       bWssPmRbtStatsSet;
    BOOL1       bWssPmRadStatsSet;
    BOOL1       bWssPmIEEE80211StatsSet;
    BOOL1       bWssPmVSPBSSIDStatsSet;
    BOOL1       bWssPmVSPSSIDStatsSet;
    BOOL1       bWssPmWtpEvtWtpCWStatsSet;
    BOOL1       bWssPmRbtStatsGet;
    BOOL1       bWssPmRadStatsGet;
    BOOL1       bWssPmIEEE80211StatsGet;
    BOOL1       bWssPmCLIWtpIdStatsGet;
    BOOL1       bWssPmCLIBSSIDStatsGet;
    BOOL1       bWssPmRadioClientStatsSet;
    BOOL1     bWssPmCapwATPStatsSet;
    BOOL1     bWssPmRadioStatsSet;
    BOOL1     bWssPmClientStatsSet;
    BOOL1     bWssPmSSIDStatsSet;
    BOOL1 bWssPmVSPRadioStatsSet;
    BOOL1 bWssPmVSPClientStatsSet;
    BOOL1 bWssPmVSPApStatsSet;
}tWssPmAttributeDB;

typedef struct {
    UINT4                 u4RadioIfIndex;
    UINT4                 u2ProfileBSSID;
    UINT4                 u4FsDot11WlanProfileId;
    tMacAddr        FsWlanClientStatsMACAddress;
    UINT1                 au1Pad[2];
    UINT4                 u4CapwapBaseWtpProfileId;
    UINT2                 stats_timer;
    UINT2                 u2ProfileId;
    tWtpRebootStats       tWtpRebootStatistics;
    tWtpRadioStats        tRadioStatistics;
    tDot11Statistics      tdot11Statistics;
    tWtpCapwapSessStats   tWtpCWSessStats;
    tBSSIDStnStats        wtpBSSIDStnStats;
    tBSSIDMacHndlrStats   tWtpVSPBSSIDStats;
    tWssPmAttributeDB     tWtpPmAttState; 
    tWssPmWlanSSIDStatsProcessRBDB SSIDStats;
    tWssPmWlanRadioStatsProcessRBDB RadioStats;
    tWssPmWlanClientStatsProcessRBDB ClientStats;
    tWssPmCapwATPStatsProcessRBDB CapwATPStats;
    tWssPmWLCCapwapStatsSetDB WssPmWLCCapwapStatsSetFlag; 
    /* need to be updated by diff. modules to set 
     * wht kind of stats are to be passed to PM */
    tWtpCPWPWtpStats      wtpVSPCPWPWtpStats;
    tWtpRCStats           radioClientStats;
    tWtpStaStats          clientStats;
    tApParams             ApElement;
}tWssIfPMDB;

INT4 WssIfProcessPMDBMsg PROTO ((UINT2 u1MsgType, tWssIfPMDB *));

#endif
