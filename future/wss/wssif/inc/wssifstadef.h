/** $Id: wssifstadef.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ **/
#ifndef __WSSIFAUTH_DEF_H__
#define __WSSIFAUTH_DEF_H__

#define WSSSTA_RB_GREATER 1
#define WSSSTA_RB_EQUAL   0
#define WSSSTA_RB_LESS    -1

/*Trace Level*/
#define WSSSTA_MGMT_TRC                    0x00000001
#define WSSSTA_INIT_TRC                    0x00000002
#define WSSSTA_ENTRY_TRC                   0x00000004
#define WSSSTA_EXIT_TRC                    0x00000008
#define WSSSTA_FAILURE_TRC                 0x00000010
#define WSSSTA_BUF_TRC                     0x00000020
#define WSSSTA_SESS_TRC                    0x00000040
#define WSSSTA_PKTDUMP_TRC                 0x00000080
#define WSSSTA_DEBUG_TRC                   0x00000100
#define WSSSTA_INFO_TRC                    0x00000200
#ifdef BAND_SELECT_WANTED
#define WSSSTA_BANDSELECT_TRC              0x00000400
#endif
#define WSSSTA_DEFAULT_USR_LIFETIME      60 /*1 day - in minutes */
#define WSSSTA_TIME_IN_MINS              60 /* Minutes to seconds multiplier */
#endif
