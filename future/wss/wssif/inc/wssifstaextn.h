/*******************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved            *
 *                                                                 *
 * $Id: wssifstaextn.h,v 1.2 2017/11/24 10:37:09 siva Exp $     *
 *                                                                 *
 * Description: This file contains RBTree for the station DB       *
 *******************************************************************/

#ifndef __WSSIFAUTH_EXTN_H__
#define __WSSIFAUTH_EXTn_H__
extern tRBTree gWssStaStateDB;
extern tRBTree gWssStaAuthSessionDB;
#endif
