/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *   $Id: wssifwtpwlandb.h,v 1.3 2017/11/24 10:37:10 siva Exp $ 
 *  Description: This file contains type definitions for wsswlan module.
 *  *********************************************************************/

#ifndef __WSSIF_WLANDB_H__
#define __WSSIF_WLANDB_H__

#include "wssifwlanconst.h"
#ifdef WPS_WTP_WANTED
#include "wps.h"
#endif

/* Enum for Database Operations */

typedef enum {
     
     WSS_WLAN_CREATE_INTERFACE_ENTRY,
     WSS_WLAN_SET_INTERFACE_ENTRY,
     WSS_WLAN_GET_INTERFACE_ENTRY,
     WSS_WLAN_DESTROY_INTERFACE_ENTRY,
     
     WSS_WLAN_CREATE_IFINDEX_ENTRY,
     WSS_WLAN_SET_IFINDEX_ENTRY,
     WSS_WLAN_GET_IFINDEX_ENTRY,
     WSS_WLAN_DESTROY_IFINDEX_ENTRY,

     WSS_WLAN_CREATE_BSSID_MAPPING_ENTRY,
     WSS_WLAN_DESTROY_BSSID_MAPPING_ENTRY,
     WSS_WLAN_GET_BSSID_MAPPING_ENTRY,

     WSS_WLAN_SET_MANAGMENT_SSID,
     WSS_WLAN_GET_MANAGMENT_SSID,
     WSS_WLAN_GET_BSS_IFINDEX_ENTRY
} eWssWlanDBMsgType;


typedef struct {
   UINT4        u4WebAuthMode;
   UINT4        u4WebServIPAddr;
   UINT4        u4WebAuthType;    
   UINT1        u1WebAuthUrl;
   UINT1        au1Pad[3];
}tWssWlanWebParamsTable;

typedef struct {
    UINT4        u4WebAuthUserLifetime;
    UINT1        u1WebAuthUName[WSS_WLAN_MAX_USER_CHAR];
    UINT1        u1WebAuthPasswd[WSS_WLAN_MAX_PWD_CHAR];
    BOOL1        bWebAuthUserStatus;
    UINT1        au1Pad[3];
}tWssWlanWebUserCred;

typedef struct {
    UINT2       u2TxOpLimit;    
    UINT1       u1ACI_AIFSN;
    UINT1       u1ECWmin_ECWmax;
}tAC_BE;

typedef struct  {
    UINT2       u2TxOpLimit;    
    UINT1       u1ACI_AIFSN;
    UINT1       u1ECWmin_ECWmax;
}tAC_BK;

typedef struct  {
    UINT2       u2TxOpLimit;    
    UINT1       u1ACI_AIFSN;
    UINT1       u1ECWmin_ECWmax;
}tAC_VI;

typedef struct  {
    UINT2       u2TxOpLimit;    
    UINT1       u1ACI_AIFSN;
    UINT1       u1ECWmin_ECWmax;
}tAC_VO;

typedef struct {
    tAC_BE AC_BE_ParameterRecord;
    tAC_BK AC_BK_ParameterRecord;
    tAC_VI AC_VI_ParameterRecord;
    tAC_VO AC_VO_ParameterRecord;
    UINT1       u1QosInfo;
    UINT1       au1Pad[3];
}tWssWlanEdca;

typedef struct {
    UINT1       u1QosInfo;
    UINT1       au1Pad[3];
}tWssWlanQos;

/* The below structure will get created internally 
 * when a WLAN Profile is created.
 * This will be created as an RBTree 
 * with u4WlanIfIndex as the key */
typedef struct {
    tRBNodeEmbd             IfIndexDBNode; 
    tWssWlanEdca            WssWlanEdcaParam;
    tWssWlanQos             WssWlanQosCapab;
    tWssWlanWebParamsTable  WssWlanWebParamsTable;
    tWssWlanWebUserCred     WssWlanWebUserCred;
    UINT4                   u4WlanIfIndex;
    UINT4                   u4WlanIfType;
    UINT4      u4MaxClientCount;
#ifdef WPS_WTP_WANTED    
    UINT4 au4WtpRadioType[31];
#endif    
    /* QosRateLimit - Begin*/
    UINT4                   u4QosUpStreamCIR;
    UINT4                   u4QosUpStreamCBS;
    UINT4                   u4QosUpStreamEIR;
    UINT4                   u4QosUpStreamEBS;
    UINT4                   u4QosDownStreamCIR;
    UINT4                   u4QosDownStreamCBS;
    UINT4                   u4QosDownStreamEIR;
    UINT4                   u4QosDownStreamEBS;
    /* QosRateLimit - End*/

    UINT2                   u2VlanId;
    UINT2                   u2Capability;
    INT2                    i2DtimPeriod;

    UINT1                   u1RadioId;
    UINT1                   u1KeyIndex;
    UINT1                   au1WepKey[WSSWLAN_WEP_KEY_LEN];
    UINT1                   au1DesiredSsid [WSSWLAN_SSID_NAME_LEN];
    UINT1                   au1GroupTsc [WSSWLAN_GROUP_TSC_LEN];

    UINT1                   u1KeyType;
    UINT1                   u1KeyStatus;        
    UINT1                   u1AuthenticationIndex;
    UINT1                   u1AuthAlgorithm;
    UINT1                   u1AuthMethod;
    UINT1                   u1WebAuthStatus;
    UINT1                   u1QosProfileId;
    UINT1                   u1CapabilityId;
    UINT1                   u1AdminStatus;
    UINT1                   u1EncryptDecryptCapab;
    UINT1                   u1PowerConstraint;
    UINT1                   u1SupressSsid;
    UINT1                   u1SsidIsolation;
    UINT1                   u1QosSelection;
    UINT1                   u1FsPreAuth;
    UINT1                   u1WlanId;
    UINT1                   u1MacType;
    UINT1                   u1TunnelMode;
    tMacAddr                BssId;
    UINT1                   u1MitigationRequirement;
    UINT1                   u1VlanFlag;
   /* RSNA Elements - Set */
    tPmkId aPmkidList[RSNA_MAX_PMKID_IDS];
    tRsnaPwCipherSuiteDB aRsnaPwCipherDB[RSNA_PAIRWISE_CIPHER_COUNT];
    tRsnaAkmSuiteDB aRsnaAkmDB[RSNA_AKM_SUITE_COUNT];
    tWpaPwCipherSuiteDB aWpaPwCipherDB[RSNA_PAIRWISE_CIPHER_COUNT];
    tWpaAkmSuiteDB aWpaAkmDB[RSNA_AKM_SUITE_COUNT];
    UINT1 au1GroupCipherSuite[RSNA_CIPHER_SUITE_LEN];
    UINT2 u2RsnCapab;
    UINT2 u2PairwiseCipherSuiteCount;
    UINT2 u2AKMSuiteCount;
    UINT2 u2PmkidCount;
    UINT2 u2Ver; 
    UINT1 u1ElemId;
    UINT1 u1Len;
    UINT1 au1GroupMgmtCipherSuite[RSNA_CIPHER_SUITE_LEN];
    
    /* RSNA IE ELEMENTS -> End */
#ifdef WPS_WTP_WANTED    
    /*WPS elements Set */
    UINT1 u1WpsElemId;
    UINT1 u1WpsLen;
    BOOL1 bWpsEnabled;
    UINT1 wscState;
    BOOL1 bWpsAdditionalIE;
    UINT1 authorized_macs[WPS_MAX_AUTHORIZED_MACS][WPS_ETH_ALEN];
    UINT1 noOfAuthMac; 
    UINT1 uuid_e[16];
    UINT1 configMethods; 
    UINT1 au1WtpModelNumber[32];
    UINT1 au1WtpSerialNumber[1024];
    UINT1 u1WtpNoofRadio;
    UINT1 au1WtpName[512];
    /*WPS elements Set end */
    UINT1 au1WpsPad[2];
#endif    
 /* QosRateLimit - Begin*/
    UINT1 u1QosRateLimit;
    UINT1 u1PassengerTrustMode;
    UINT1           au1Pad[2];
#ifdef BAND_SELECT_WANTED
    UINT1 u1BandSelectStatus;
    UINT1 u1AgeOutSuppression;
    UINT1 u1AssocResetTime;
    UINT1 u1AssocRejectCount;
#endif
    UINT2 u2WlanMulticastMode;
    UINT2 u2WlanSnoopTableLength;
    UINT4 u4WlanSnoopTimer;
    UINT4 u4WlanSnoopTimeout;
    UINT1 u1ExternalWebAuthMethod;
    UINT1 u1Pad;
    UINT1           au1Pad1[2];
    /* QosRateLimit - End*/

}tWssWlanIfIndexDB;


/* The below structure will get created when operator binds a wlan profile to
 * a Radio Interface. This will an RBTree with u4RadioIfIndex and
 * u2WlanProfileId as the key */
typedef struct 
{
    tRBNodeEmbd             WlanInterfaceDBNode; 
    tWssWlanIfIndexDB     *pWssWlanIfIndexDB;
    UINT1                   u1RadioId;
    UINT1                   u1WlanId;
    UINT1                   u1RowStatus;        
    UINT1      au1InterfaceName[WSSWLAN_BR_INTERFACE_NAME_LEN];
    UINT1                   u1Pad;
} tWssWlanInterfaceDB;

typedef struct {
    tWssWlanEdca     WssWlanEdcaParam;
    tWssWlanQos             WssWlanQosCapab;
    tRsnaIEElements         RsnaIEElements;
    tWpaIEElements         WpaIEElements;
#ifdef WPS_WTP_WANTED    
    tWpsIEElements          WpsIEElements;
#endif    
    tWssWlanWebParamsTable  WssWlanWebParamsTable;
    tWssWlanWebUserCred     WssWlanWebUserCred;
    /*802.11n*/
    tHTCapabilities         HTCapabilities;
    tHTOperation            HTOperation;
    tVHTCapability          VHTCapabilities;
    tVHTOperation           VHTOperation;
    tVhtTxPower             VhtTxPower;
    tChanSwitchWrapper      ChanSwitchWrapper;
    tOperModeNotify         OperModeNotify;
    tMacAddr                BssId;
    /*802.11n*/
    INT2                    i2DtimPeriod;
    UINT4                   u4IfIndex;
    UINT4                   u4WlanIfIndex;
    UINT4                   u4RadioIfIndex;
    UINT4                   u4BssIfIndex;
    UINT4                   u4WlanIfType;
    UINT4                   u4IfType;
    UINT4                   u4MaxClientCount;
   /* QosRateLimit - Begin*/
    UINT4                   u4QosUpStreamCIR;
    UINT4                   u4QosUpStreamCBS;
    UINT4                   u4QosUpStreamEIR;
    UINT4                   u4QosUpStreamEBS;
    UINT4                   u4QosDownStreamCIR;
    UINT4                   u4QosDownStreamCBS;
    UINT4                   u4QosDownStreamEIR;
    UINT4                   u4QosDownStreamEBS;
    /* QosRateLimit - End*/

    UINT2                   u2WtpInternalId;
    UINT2                   u2WlanInternalId;
    UINT2                   u2WlanProfileId;
    UINT2                   u2VlanId;
    UINT2                   u2Capability;
    UINT2                   u2KeyLength;

    UINT1                   au1DesiredSsid [WSSWLAN_SSID_NAME_LEN];
    UINT1                   au1ManagmentSSID[WSSWLAN_SSID_NAME_LEN];
    UINT1                   au1WepKey[WSSWLAN_WEP_KEY_LEN];
    UINT1                   au1GtkKey[WSSWLAN_RSNA_KEY_LEN];
    UINT1                   au1WlanIntfName[WSS_WLAN_NAME_MAX_LEN];
    UINT1                   au1GroupTsc [WSSWLAN_GROUP_TSC_LEN];
    UINT1                   u1RadioId;
    UINT1                   u1WlanId;
    UINT1                   u1KeyIndex;
    UINT1                   u1KeyType;
    UINT1                   u1KeyStatus;        
    UINT1                   u1AuthenticationIndex;
    UINT1                   u1AuthAlgorithm;
    UINT1                   u1AuthMethod;
    UINT1                   u1WebAuthStatus;
    UINT1                   u1QosProfileId;
    UINT1                   u1CapabilityId;
    UINT1                   u1AdminStatus;
    UINT1                   u1EncryptDecryptCapab;
    UINT1                   u1PowerConstraint;
    UINT1                   u1SupressSsid;
    UINT1                   u1SsidIsolation;
    UINT1                   u1QosSelection;
    UINT1                   u1FsPreAuth;
    UINT1                   u1MacType;
    UINT1                   u1TunnelMode;
    UINT1                   u1WlanRadioMappingCount;
    UINT1                   u1MitigationRequirement;
    UINT1                   u1RowStatus;
     /* QosRateLimit - Begin*/
    UINT1                   u1PassengerTrustMode;
    UINT1                   u1QosRateLimit;
    /* QosRateLimit - End*/
    UINT1                   u1VlanFlag; 
    UINT1                   u1Dot11nSupport;
    UINT1      u1Dot11acSupport;
    UINT1                   u1HTCapEnable;
    UINT1                   u1HTOpeEnable;
#ifdef BAND_SELECT_WANTED
    UINT1                   u1BandSelectStatus;
    UINT1                   u1AgeOutSuppression;
    UINT1                   u1AssocResetTime;
    UINT1                   u1AssocRejectCount;
#endif
    UINT2                   u2WlanMulticastMode;
    UINT2                   u2WlanSnoopTableLength;
    UINT4                   u4WlanSnoopTimer;
    UINT4                   u4WlanSnoopTimeout;
    UINT1                   au1InterfaceName[WSSWLAN_BR_INTERFACE_NAME_LEN];
    UINT1                   u1BrInterfaceStatus;
    UINT1                   u1ExternalWebAuthMethod;
    UINT1      au1Pad[2];
}tWssWlanAttributeDB;

typedef struct {
    BOOL1       bWssWlanEdcaParam;
    BOOL1       bWssWlanQosCapab;
    BOOL1       bWssWlanWebParamsTable;
    BOOL1       bWssWlanWebUserCred;
    BOOL1       bBssIfIndex;
    BOOL1       bWlanIfType;
    BOOL1       bRadioId;
    BOOL1       bRadioIfIndex;
    BOOL1       bIfType;
    BOOL1       bWtpInternalId;
    BOOL1       bWlanInternalId;
    BOOL1       bWlanProfileId;
    BOOL1       bIfIndex;
    BOOL1       bWlanIfIndex;
    BOOL1       bVlanId;
    BOOL1       bCapability;
    BOOL1       bWlanId;
    BOOL1       bDtimPeriod;
    BOOL1       bBssId;
    BOOL1       bGroupTsc;
    BOOL1       bDesiredSsid;
    BOOL1       bKeyIndex;
    BOOL1       bKeyType;
    BOOL1       bKeyStatus;        
    BOOL1       bAuthenticationIndex;
 
    BOOL1       bWepKey;
    BOOL1       bAuthAlgorithm;
    BOOL1       bAuthMethod;
    BOOL1       bWebAuthStatus;
    BOOL1       bQosProfileId;
    BOOL1       bCapabilityId;
    BOOL1       bAdminStatus;
    BOOL1       bEncryptDecryptCapab;
    BOOL1       bPowerConstraint;
    BOOL1       bSupressSsid;
    BOOL1       bSsidIsolation;
    BOOL1       bQosSelection;
    BOOL1       bFsPreAuth;
    BOOL1       bMacType;
    BOOL1       bTunnelMode;
    BOOL1       bWlanRadioMappingCount;
    BOOL1      bMitigationRequirement;
    BOOL1      bManagmentSSID;
    BOOL1       bRowStatus;
   /* QosRateLimit - Begin*/
    BOOL1                   bQosRateLimit;
    BOOL1                   bQosUpStreamCIR;
    BOOL1                   bQosUpStreamCBS;
    BOOL1                   bQosUpStreamEIR;
    BOOL1                   bQosUpStreamEBS;
    BOOL1                   bQosDownStreamCIR;
    BOOL1                   bQosDownStreamCBS;
    BOOL1                   bQosDownStreamEIR;
    BOOL1                   bQosDownStreamEBS;
    BOOL1                   bPassengerTrustMode;

    /* QosRateLimit - End*/
    BOOL1                   bRSNAIEElements;
    BOOL1                   bWpaIEElements;
#ifdef WPS_WTP_WANTED    
    BOOL1                   bWPSEnabled;
    BOOL1                   bWPSAdditionalIE;
    UINT1                   au1Pad[2];
#endif    
    
    BOOL1        bVlanFlag;  
    BOOL1        bWlanIntfName;
    BOOL1        bMaxClientCount;
#ifdef BAND_SELECT_WANTED
    BOOL1        bBandSelectStatus;
    BOOL1        bAgeOutSuppression;
    BOOL1        bAssocRejectCount;
    BOOL1        bAssocResetTime;
#endif
    BOOL1       bWlanMulticastMode;
    BOOL1       bWlanSnoopTableLength;
    BOOL1       bWlanSnoopTimer;
    BOOL1       bWlanSnoopTimeout;
    BOOL1       bInterfaceName;
    BOOL1       bExternalWebAuthMethod;
    UINT1                   au1Pad1[3];
}tWssWlanIsPresentDB;

typedef struct {
    tWssWlanAttributeDB            WssWlanAttributeDB;
    tWssWlanIsPresentDB             WssWlanIsPresentDB;               
}tWssWlanDB;


/* Mapping for SSID name to BSS IfIndex */
typedef struct {
        tRBNodeEmbd    BssidMappingDBNode;
        UINT4          u4WlanIfIndex;
        tMacAddr           BssId;
        UINT1           au1WlanIntfName[WSS_WLAN_NAME_MAX_LEN];
        UINT1       au1Pad[2];
}tWssWlanBSSIDMappingDB;

typedef struct 
{
    tRBTree                     WssWlanInterfaceDB;
    tRBTree                     WssWlanIfIndexDB;
    tRBTree                     WssWlanBSSIDMappingDB;
    UINT1                       au1ManagmentSSID[WSSWLAN_SSID_NAME_LEN];
 } tWssWlanGlbMib;

typedef struct WSSWLAN_GLOBALS {
     tWssWlanGlbMib       WssWlanGlbMib;
 } tWssWlanGlobals;

/* Mapping for BSS Mac address to BSS IfIndex */
typedef struct {
    tRBNodeEmbd BssIdMappingDBNode;
        UINT4               u4BssIfIndex;
    tMacAddr    BssId;
        UINT2           u2RuleId;

}tWssWlanBssIdMappingDB;

typedef struct {
    tRBNodeEmbd     BssIfIndexDBNode;
    struct tRadioIFDB      *pRadioIfDB;
    tMacAddr        BssId;
    UINT2                   u2WlanInternalId;
    UINT4           u4BssIfIndex;
    UINT4           u1IfType;
    UINT1           u1WlanId;
    UINT1           u1AdminStatus;
    UINT1           u1WlanBindStatus;
    UINT1       au1Pad[1];
}tWssWlanBssIfIndexDB;

typedef struct  tWssWlanBssInterfaceDb
{
    tRBNodeEmbd             BssInterfaceDBNode;
    tWssWlanBssIfIndexDB   *pWssWlanBssIfIndexDB;
    UINT4                   u4RadioIfIndex;
    UINT2                   u2WlanProfileId;
    UINT1                   u1WlanId;
    UINT1                   u1RowStatus;
} tWssWlanBssInterfaceDB;

/* Mapping for SSID name to BSS IfIndex */
typedef struct {
        tRBNodeEmbd    SsidMappingDBNode;
        UINT4           u4WlanIfIndex;
            UINT1                  au1SSID[WSSWLAN_SSID_NAME_LEN];
}tWssWlanSSIDMappingDB;


#endif  /* __WSSWLANTDFS_H__ */

