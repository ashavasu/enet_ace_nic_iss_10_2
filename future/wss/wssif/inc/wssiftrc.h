/*****************************************************************************/
/* Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *  $Id: wssiftrc.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $                                                                           */
/* Licensee Aricent Inc.,                         */
/*****************************************************************************/
/*    FILE  NAME            : wssiftrc.h
 *    */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : WSSIF module
 *    */
/*    MODULE NAME           : WSSIF module
 *    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                     */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains declarations of traces used */
/*                            in WSSIF Module.
 *                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                      DESCRIPTION OF CHANGE/              */
/*            MODIFIED BY                FAULT REPORT NO                     */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 MAR 2002 / BridgeTeam   Initial Create.                     */
/*---------------------------------------------------------------------------*/
#ifndef _WSSIFTRC_H_
#define _WSSIFTRC_H_


#define  WSSIF_MOD                ((const char *)"WSSIF")

#define WSSIF_MGMT_TRC 1
#define WSSIF_FAILURE_TRC 2
#define WSSIF_ENTRY_TRC 4
#define WSSIF_EXIT_TRC 8

#define WSSIF_MASK                0x0 

/* if WSSSIF Trace/Debug messages wanted this flags can be enabled
 * by repacing the above WSSWLAN_MASK macro value with the below
 * #define WSSSTA_MASK  \
   WSSSTA_MGMT_TRC | WSSSTA_FAILURE_TRC | WSSSTA_ENTRY_TRC | WSSSTA_EXIT_TRC
 */

/* Trace and debug flags */

#define WSSIF_FN_ENTRY() MOD_FN_ENTRY (WSSIF_MASK,WSSIF_ENTRY_TRC,WSSIF_MOD)

#define WSSIF_FN_EXIT() MOD_FN_EXIT (WSSIF_MASK, WSSIF_EXIT_TRC,WSSIF_MOD)

#define WSSIF_PKT_DUMP(mask, pBuf, Length, fmt)                           \
        MOD_PKT_DUMP(WSSIF_PKT_DUMP_TRC,mask, WSSIF_MOD, pBuf, Length, fmt)
#define WSSIF_TRC(mask, fmt)\
        MOD_TRC(WSSIF_MASK, mask, WSSIF_MOD, fmt)
#define WSSIF_TRC1(mask,fmt,arg1)\
        MOD_TRC_ARG1(WSSIF_MASK,mask,WSSIF_MOD,fmt,arg1)
#define WSSIF_TRC2(mask,fmt,arg1,arg2)\
        MOD_TRC_ARG2(WSSIF_MASK,mask,WSSIF_MOD,fmt,arg1,arg2)
#define WSSIF_TRC3(mask,fmt,arg1,arg2,arg3)\
        MOD_TRC_ARG3(WSSIF_MASK,mask,WSSIF_MOD,fmt,arg1,arg2,arg3)
#define WSSIF_TRC4(mask,fmt,arg1,arg2,arg3,arg4)\
        MOD_TRC_ARG4(WSSIF_MASK,mask,WSSIF_MOD,fmt,arg1,arg2,arg3,arg4)
#define WSSIF_TRC5(mask,fmt,arg1,arg2,arg3,arg4,arg5)\
        MOD_TRC_ARG5(WSSIF_MASK,mask,WSSIF_MOD,fmt,arg1,arg2,arg3,arg4,arg5)



#endif

