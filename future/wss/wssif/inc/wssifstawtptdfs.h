/******************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                       *
 *  $Id: wssifstawtptdfs.h,v 1.2 2017/11/24 10:37:10 siva Exp $                                                                            *
 * Description: This file contains the typedef for WSSIFSTA                   *
 ******************************************************************************/
#ifndef __WSSSTA_WTP_TDFS_H__
#define __WSSSTA_WTP_TDFS_H__

#if 0
/* Station update action */
typedef enum{
   WSS_STA_NEW_STA_REQ = 0,
   WSS_STA_DELETE,
   WSS_STA_UPDATE
}eWssStaUpdateAction;
#endif
#endif

