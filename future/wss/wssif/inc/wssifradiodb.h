/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *  
 *  $Id: wssifradiodb.h,v 1.2 2017/05/23 14:16:57 siva Exp $
 *  
 *  Description: This file contains type definitions for RadioIf module.
 **********************************************************************/

#ifndef __WSSIF_RADIODB_H__
#define __WSSIF_RADIODB_H__

#include "wssifradioconst.h"
#include "radioif.h"
#include "capwap.h"

typedef struct {
    UINT2       u2CwMin;
    UINT2       u2CwMax;
    UINT2       u2TxOpLimit;
    UINT1       u1Aifsn;
    UINT1       u1AdmissionControl;
    UINT1       u1QueueDepth;
    UINT1       u1QosPrio;
    UINT1       u1Dscp;
    UINT1       u1TaggingPolicy;
}tRadioIfQosConfig;

typedef struct {
    tRadioIfQosConfig   RadioIfQosConfig[WSSIF_RADIOIF_QOS_CONFIG_LEN];
    tMultiDomainInfo    MultiDomainInfo[MAX_MULTIDOMAIN_INFO_ELEMENT];
    UINT4               u4TxMsduLifetime;
    UINT4               u4RxMsduLifetime;
    UINT4               u4Dot11RadioType; 
    UINT4      u4MaxClientCount;
    UINT4  u4RadioInOctets;
    UINT4  u4RadioOutOctets;
    UINT2               u2FragmentationThreshold;
    UINT2               u2DtimPeriod; 
    UINT2               u2RTSThreshold;
    UINT2               u2BeaconPeriod;
    UINT2               u2ReportInterval;
    UINT2               u2HTCapInfo;
    UINT2               u2Capability;
 UINT1               u1AntennaMode;
 UINT1               u1CurrentTxAntenna;
    UINT1               u1ShortPreamble;
    UINT1               u1DiversitySupport;
    UINT1               u1ShortRetry;
    UINT1               u1LongRetry;
    UINT1               au1AntennaSelection[RADIOIF_ANTENNA_LIST_INDEX];
    UINT1               u1NoOfBssId;
    UINT1               au1MCSRateSet[RADIOIF_11N_MCS_RATE_LEN];
 UINT1               au1OperationalRate [RADIOIF_OPER_RATE];
    UINT1               au1SupportedRate [RADIOIF_OPER_RATE];
    UINT1               au1CountryString[RADIO_COUNTRY_LENGTH];
    UINT1               u1ProbeInclude;
    UINT1               u1BeaconInclude;
    UINT1               u1HtFlag;
    UINT1               u1MaxSuppMCS;
    UINT1               u1MaxManMCS;
    UINT1               u1TxAntenna;
    UINT1               u1RxAntenna; 
    UINT1               u1ChannelWidth;
    UINT1               u1ShortGi20;
    UINT1               u1ShortGi40;
    UINT1               u1AMPDUSupport;
    UINT1               u1MaxMpduInAmpdu;
    UINT1               u1MaxMsdu;
    UINT1               u1HTMaxAmpduLengthExponent;
    UINT1               u1VHTMaxAmpduLengthExponent;
    UINT1               u1BPFlag;
    UINT1           u1AMPDUParam;
    UINT1           u1HTInfoSecChanAbove;
    UINT1           u1HTInfoSecChanBelow;
    UINT1           au1Pad[2];
    UINT1           au1SuppMCSSet[RADIOIF_11N_MCS_RATE_LEN_PKT];
    UINT1           au1ManMCSSet[RADIOIF_11N_MCS_RATE_LEN_PKT];
    UINT4           u4SuppRadioType;
    UINT1           u1TxSupportMcsSet;
    UINT1            u1MultiDomainCapabilityEnabled;
    UINT1            u1HTOption;
    UINT1             u1AllowChannelWidth; 
 UINT1            u1RadarFound;
    UINT1            u1ChanlSwitStatus;  
    UINT1            u111hDfsStatus;
    UINT1            u1Pad[1];
}tRadioIfConfigParam;


typedef struct {
    UINT4           u4OFDMChannelWidth;
    UINT4           u4T1Threshold;
    INT4            i4RssiThreshold;
    INT4            i4EDThreshold;
    UINT2           au2TxPowerLevel[RADIOIF_MAX_POWER_LEVEL];
    UINT2           u2FirstChannelNumber;
    UINT2           u2NoOfChannels;
    UINT2           u2MaxTxPowerLevel;
    UINT1           u1SupportedBand;
    UINT1           u1CurrentCCAMode;
    UINT1           u1CurrentChannel;
    UINT1           u1CurrentFrequency;
    UINT1           u1NoOfSupportedPowerLevels;
    UINT1           u1Pad;
}tRadioIfChannelInfo;

struct  tWssWlanBssInterfaceDb;

typedef struct {
 UINT4 u4SuppVhtCapInfo;
 UINT4 u4VhtCapInfo;
 UINT2 u2RxDataRate;
 UINT2 u2TxDataRate;
 UINT2 u2SuppRxDataRate;
 UINT2 u2SuppTxDataRate;
 UINT1 au1VhtCapaMcs[DOT11AC_VHT_CAP_MCS_LEN];
 UINT1 au1SuppVhtCapaMcs[DOT11AC_VHT_CAP_MCS_LEN];
 UINT1 u1MaxMPDULen;
 UINT1 u1SuppMaxMPDULen;
 UINT1 u1SuppChanWidth;
 UINT1 u1RxLpdc;
 UINT1 u1SuppRxLpdc;
 UINT1 u1ShortGi80;
 UINT1 u1SuppShortGi80;
 UINT1 u1ShortGi160;
 UINT1 u1SuppTxStbc;
 UINT1 u1TxStbc;
 UINT1 u1SuppRxStbc;
 UINT1 u1SuppRxStbcStatus;
 UINT1 u1RxStbc;
 UINT1 u1RxStbcStatus;
 UINT1 u1SuBeamFormer;
 UINT1 u1SuBeamFormee;
 UINT1 u1SuppBeamFormAnt;
 UINT1 u1SoundDimension;
 UINT1 u1MuBeamFormer;
 UINT1 u1MuBeamFormee;
 UINT1 u1VhtTxOpPs;
 UINT1 u1HtcVhtCapable;
 UINT1 u1VhtMaxAMPDU;
 UINT1 u1SuppVhtMaxAMPDU;
 UINT1 u1VhtLinkAdaption;
 UINT1 u1VhtRxAntenna;
 UINT1 u1VhtTxAntenna;
 UINT1 u1VhtReserved;
 UINT1 u1SuppVhtSTS;
 UINT1 u1VhtSTS;
 UINT1 u1SpecMgmt;
 UINT1 u1Pad;
}tRadioDot11AcCapaParams;

typedef struct {
 UINT2 u2SuppHtCapInfo;
 UINT2 u2HtCapInfo;
 UINT1 u1SuppLdpcCoding;
 UINT1 u1LdpcCoding;
 UINT1 u1SuppChannelWidth;
 UINT1 u1ChannelWidth;
 UINT1 u1SuppSmPowerSave; 
 UINT1 u1SmPowerSave; 
 UINT1 u1SuppHtGreenField;
 UINT1 u1HtGreenField;
 UINT1 u1SuppShortGi20;
 UINT1 u1ShortGi20;
 UINT1 u1SuppShortGi40;
 UINT1 u1ShortGi40;
 UINT1 u1SuppTxStbc; 
 UINT1 u1TxStbc; 
 UINT1 u1SuppRxStbc; 
 UINT1 u1RxStbc; 
 UINT1 u1SuppDelayedBlockack;
 UINT1 u1DelayedBlockack;
 UINT1 u1SuppMaxAmsduLen;
 UINT1 u1MaxAmsduLen;
 UINT1 u1SuppDsssCck40;
 UINT1 u1DsssCck40;
 UINT1 u1Reserved;
 UINT1 u1SuppFortyInto;
 UINT1 u1FortyInto;
 UINT1 u1SuppLsigTxopProtFulSup;
 UINT1 u1LsigTxopProtFulSup;
 UINT1 u1AMPDUStatus;
 UINT1 u1AMPDUSubFrame;
 UINT1 u1AMSDUStatus;
 UINT2 u2AMPDULimit;
 UINT2 u2AMSDULimit;
 UINT1 au1Pad[2];
}tRadioDot11nHtCapaParams;

typedef struct {
 UINT1 u1SuppAmpduParam;
 UINT1 u1AmpduParam;
 UINT1 u1SuppMaxAmpduLen;
 UINT1 u1MaxAmpduLen;
 UINT1 u1SuppMinAmpduStart;
 UINT1 u1MinAmpduStart;
 UINT1 u1Reserved;
 UINT1 au1Pad[1];
}tRadioDot11nAmpduParams;

typedef struct {
 UINT1 au1SuppHtCapaMcs[DOT11N_HT_CAP_MCS_LEN];
 UINT1 au1HtCapaMcs[DOT11N_HT_CAP_MCS_LEN];
 UINT1 au1SuppRxMcsBitmask[10];
 UINT1 au1RxMcsBitmask[10];
 UINT2 u2SuppHighSuppDataRate;
 UINT2 u2HighSuppDataRate;
 UINT1 u1Reserved1;
 UINT1 u1Reserved2;
 UINT1 u1SuppTxMcsSetDefined; 
 UINT1 u1TxMcsSetDefined; 
 UINT1 u1SuppTxRxMcsSetNotEqual;
 UINT1 u1TxRxMcsSetNotEqual;
 UINT1 u1SuppTxMaxNoSpatStrm;
 UINT1 u1TxMaxNoSpatStrm;
 UINT1 u1SuppTxUnequalModSupp;
 UINT1 u1TxUnequalModSupp;
 UINT1 au1Pad[2];
 UINT1 au1Reserved3[4];
}tRadioDot11nSuppMcsParams;

typedef struct {
 UINT2 u2SuppHtExtCap;
 UINT2 u2HtExtCap;
 UINT1 u1SuppPco;
 UINT1 u1Pco;
 UINT1 u1SuppPcoTransTime;
 UINT1 u1PcoTransTime;
 UINT1 u1Reserved1;
 UINT1 u1SuppMcsFeedback;
 UINT1 u1McsFeedback;
 UINT1 u1SuppHtcSupp;
 UINT1 u1HtcSupp;
 UINT1 u1SuppRdResponder;
 UINT1 u1RdResponder;
 UINT1 u1Reserved2;
}tRadioDot11nHtExtCapParams;

typedef struct {
 UINT4 u4SuppTxBeamCapParam;
 UINT4 u4TxBeamCapParam;
 UINT1 u1SuppImpTranBeamRecCapable;
 UINT1 u1ImpTranBeamRecCapable;
 UINT1 u1SuppRecStagSoundCapable;
 UINT1 u1RecStagSoundCapable;
 UINT1 u1SuppTransStagSoundCapable;
 UINT1 u1TransStagSoundCapable;
 UINT1 u1SuppRecNdpCapable;
 UINT1 u1RecNdpCapable;
 UINT1 u1SuppTransNdpCapable;
 UINT1 u1TransNdpCapable;
 UINT1 u1SuppImpTranBeamCapable;
 UINT1 u1ImpTranBeamCapable;
 UINT1 u1SuppCalibration;
 UINT1 u1Calibration;
 UINT1 u1SuppExplCsiTranBeamCapable;
 UINT1 u1ExplCsiTranBeamCapable;
 UINT1 u1SuppExplNoncompSteCapable;
 UINT1 u1ExplNoncompSteCapable;
 UINT1 u1SuppExplCompSteCapable;
 UINT1 u1ExplCompSteCapable;
 UINT1 u1SuppExplTranBeamCsiFeedback;
 UINT1 u1ExplTranBeamCsiFeedback;
 UINT1 u1SuppExplNoncompBeamFbCapable;
 UINT1 u1ExplNoncompBeamFbCapable;
 UINT1 u1SuppExplCompBeamFbCapable;
 UINT1 u1ExplCompBeamFbCapable;
 UINT1 u1SuppMinGrouping;
 UINT1 u1MinGrouping;
 UINT1 u1SuppCsiNoofBeamAntSupported;
 UINT1 u1CsiNoofBeamAntSupported;
 UINT1 u1SuppNoncompSteNoBeamformer;
 UINT1 u1NoncompSteNoBeamformerAntSupp;
 UINT1 u1SuppCompSteNoBeamformerAntSupp;
 UINT1 u1CompSteNoBeamformerAntSupp;
 UINT1 u1SuppCSIMaxNoRowsBeamformerSupp;
 UINT1 u1CSIMaxNoRowsBeamformerSupp;
 UINT1 u1SuppChannelEstCapability;
 UINT1 u1ChannelEstCapability;
 UINT1 au1Pad[2];
}tRadioDot11nHtTxBeamCapParams;

typedef struct {
    UINT1       au1SuppHTOpeInfo[WSSMAC_HTOPE_INFO];
    UINT1       au1HTOpeInfo[WSSMAC_HTOPE_INFO];
    UINT1  au1Pad[2];
    UINT1       au1SuppBasicMCSSet[WSSMAC_BASIC_MCS_SET];
    UINT1       au1BasicMCSSet[WSSMAC_BASIC_MCS_SET];
    UINT1       u1SuppPrimaryChannel;
    UINT1       u1PrimaryChannel;
    UINT1       u1SuppSecondaryChannel; 
    UINT1       u1SecondaryChannel; 
    UINT1       u1SuppStaChanWidth;
    UINT1       u1StaChanWidth;
    UINT1       u1SuppRifsMode;
    UINT1       u1RifsMode;
    UINT1       u1Reserved1;
    UINT1       u1SuppHtProtection; 
    UINT1       u1HtProtection; 
    UINT1       u1SuppNongfHtStasPresent;
    UINT1       u1NongfHtStasPresent;
    UINT1       u1Reserved2;
    UINT1       u1SuppObssNonHtStasPresent;
    UINT1       u1ObssNonHtStasPresent;
    UINT1       u1Reserved3;
    UINT1       u1Reserved4;
    UINT1       u1SuppDualBeacon;
    UINT1       u1DualBeacon;
    UINT1       u1SuppDualCtsProtection;
    UINT1       u1DualCtsProtection;
    UINT1       u1SuppStbcBeacon;
    UINT1       u1StbcBeacon;
    UINT1       u1SuppLsigTxopProtFulSup;
    UINT1       u1LsigTxopProtFulSup;
    UINT1       u1SuppPcoActive;
    UINT1       u1PcoActive;
    UINT1       u1SuppPcoPhase;
    UINT1       u1PcoPhase;
    UINT1       u1Reserved5;
    UINT1       u1IsGreenFieldPresent;
}tRadioDot11nHtOperation;

typedef struct {
 UINT2 u2VhtOperMcsSet;
 UINT1 u1VhtChannelWidth;
 UINT1 u1SuppVhtChannelWidth;
 UINT1 au1VhtOperInfo[DOT11AC_VHT_OPER_INFO_LEN];
 UINT1 u1CenterFcy0;
 UINT1 u1SuppCenterFcy0;
 UINT1 u1CenterFcy1;
 UINT1 u1VhtOperModeNotify;
 UINT1 u1VhtOperModeElem;
 UINT1 u1VhtOption;
 UINT1 u1Action;
 UINT1 au1Pad[2];
}tRadioDot11AcOperparams;

typedef struct {
 UINT1 u1TransPower;
 UINT1 u1MaxTxPowerCount;
 UINT1 u1MaxTxPowerUnit;
 UINT1 u1TxPower20;
 UINT1 u1TxPower40;
 UINT1 u1TxPower80;
 UINT1 u1TxPower160;
 UINT1 u1Pad;
}tRadioVhtTransmitPower;

typedef struct tRadioIFDB { 
    tRBNodeEmbd                     RadioIfIndexDBNode;
    struct  tWssWlanBssInterfaceDb  *pu1WlanBssId[WSSIF_MAX_BSSID_LEN];
    tRadioIfConfigParam             RadioIfConfigParam;
    tRadioIfChannelInfo             RadioIfChannelInfo;
    tDot11Statistics                ieee80211Stats;
    tWtpRadioStats                  radioStats;
    tRadioDot11AcCapaParams      Dot11AcCapaParams;
    tRadioDot11AcOperparams      Dot11AcOperParams;
    tRadioVhtTransmitPower       VhtTransmitPower;
    tRadioDot11nHtCapaParams          Dot11NCapaParams;
    tRadioDot11nAmpduParams      Dot11NampduParams;
    tRadioDot11nSuppMcsParams        Dot11NsuppMcsParams;
    tRadioDot11nHtExtCapParams      Dot11NhtExtCapParams;
    tRadioDot11nHtTxBeamCapParams    Dot11NhtTxBeamCapParams;
    tRadioDot11nHtOperation          Dot11NhtOperation;
    UINT4                      au4BssIfIndex[WSSIF_MAX_BSSID_LEN];
    UINT4                           u4VirtualRadioIfIndex;
    UINT2                           u2WtpInternalId; 
    UINT2                           u2CurrentTxPowerLevel;
    INT2                           i2CurrentTxPower;
    UINT1                           u1RadioId;
    UINT1                           u1BindingType;
    UINT1                           au1InfoElement[RADIOIF_INFO_ELEMENT];
    tMacAddr                        MacAddr;
    UINT1                           u1AdminStatus;
    UINT1                           u1OperStatus;
    UINT1                           u1IfType;
    UINT1                           u1BssIdCount;
    UINT1                           u1RadioAntennaType;
    UINT1                           u1FailureStatus;
    UINT1                           u1DTIMPeriod;
    UINT1                           u1AllowChannelWidth;
    UINT1                           au1pad[2];
} tRadioIfDB;

typedef struct {
    tRadioIfQosConfig   RadioIfQosConfig[WSSIF_RADIOIF_QOS_CONFIG_LEN];
    tDot11Statistics    ieee80211Stats;
    tWtpRadioStats      radioStats;
    tRadioDot11AcCapaParams  Dot11AcCapaParams;
    tRadioDot11AcOperparams  Dot11AcOperParams;
    tRadioVhtTransmitPower   VhtTransmitPower;
    tMultiDomainInfo         MultiDomainInfo[MAX_MULTIDOMAIN_INFO_ELEMENT];
    tRadioDot11nHtCapaParams   Dot11NCapaParams;
    tRadioDot11nAmpduParams      Dot11NampduParams;
    tRadioDot11nSuppMcsParams        Dot11NsuppMcsParams;
    tRadioDot11nHtExtCapParams      Dot11NhtExtCapParams;
    tRadioDot11nHtTxBeamCapParams    Dot11NhtTxBeamCapParams;
    tRadioDot11nHtOperation          Dot11NhtOperation;
    UINT1               *pu1AntennaSelection;
    UINT4               u4SuppRadioType;
    UINT4          u4RadioIfIndex;
    UINT4          u4BssIfIndex;
    UINT4               u4TxMsduLifetime;
    UINT4               u4RxMsduLifetime;
    UINT4               u4Dot11RadioType; 
    UINT4               u4OFDMChannelWidth;
    UINT4               u4T1Threshold;
    UINT4               u4RowStatus;
    UINT4               u4MaxClientCount;
    UINT4               u4RadioInOctets;
    UINT4               u4RadioOutOctets;
    INT4                i4RssiThreshold;
    INT4                i4EDThreshold;
    INT2                i2CurrentTxPower;
    UINT2               u2MessageLength;
    UINT2               u2MessageType; 
    UINT2               u2WtpInternalId; 
    UINT2               u2CurrentTxPowerLevel;
    UINT2               u2RTSThreshold;
    UINT2               u2FragmentationThreshold;
    UINT2               u2FirstChannelNumber;
    UINT2               au2CwMin[WSSIF_RADIOIF_QOS_CONFIG_LEN];
    UINT2               au2CwMax[WSSIF_RADIOIF_QOS_CONFIG_LEN];
    UINT2               au2TxOpLimit[WSSIF_RADIOIF_QOS_CONFIG_LEN];
    UINT2               au2TxPowerLevel[RADIOIF_OPER_RATE];
    UINT2               u2MaxTxPowerLevel ;
    UINT2               u2BeaconPeriod;
    UINT2               u2NoOfChannels;
    UINT2               u2ReportInterval;
    UINT2               u2HTCapInfo;
    UINT2               u2Capability;
    tMacAddr            MacAddr;
    UINT1               u1RadioId;
    UINT1               u1BindingType;
    UINT1               u1AdminStatus;
    UINT1               u1OperStatus;
    UINT1               u1IfType;
    UINT1               u1BssIdCount;
    UINT1               u1RadioAntennaType;
    UINT1               u1FailureStatus;
    UINT1               u1DiversitySupport;
    UINT1               u1CurrentTxAntenna;
    UINT1               u1ShortRetry;
    UINT1               u1AntennaMode;
    UINT1               u1LongRetry;
    UINT1               au1Pad[1];
    UINT1               au1OperationalRate [RADIOIF_OPER_RATE];
    UINT1               au1SupportedRate [RADIOIF_OPER_RATE];
    UINT1               u1TaggingPolicy;
    UINT1               u1ShortPreamble;
    UINT1               u1NoOfBssId;
    UINT1               u1ProbeInclude;
    UINT1               u1BeaconInclude;
    UINT1               u1SupportedBand;
    UINT1               u1CurrentFrequency;
    UINT1               u1CurrentCCAMode;
    UINT1               u1CurrentChannel;
    UINT1               u1NoOfSupportedPowerLevels;
    UINT1               u1HtFlag;
    UINT1               u1MaxSuppMCS;
    UINT1               u1MaxManMCS;
    UINT1               u1TxAntenna;
    UINT1               u1RxAntenna;
    UINT1               u1ChannelWidth;
    UINT1               u1ShortGi20;
    UINT1               u1ShortGi40;
    UINT1               u1AMPDUSupport;
    UINT1               u1MaxMpduInAmpdu;
    UINT1               au1SuppMCSSet[RADIOIF_11N_MCS_RATE_LEN_PKT];
    UINT1               au1ManMCSSet[RADIOIF_11N_MCS_RATE_LEN_PKT];
    UINT1               au1CountryString[RADIO_COUNTRY_LENGTH];
    UINT1               u1MaxMsdu;
    UINT1               au1MCSRateSet[RADIOIF_11N_MCS_RATE_LEN];
    UINT1               au1QueueDepth[WSSIF_RADIOIF_QOS_CONFIG_LEN];
    UINT1               au1Aifsn[WSSIF_RADIOIF_QOS_CONFIG_LEN];
    UINT1               au1AdmissionControl[WSSIF_RADIOIF_QOS_CONFIG_LEN];
    UINT1               au1QosPrio[WSSIF_RADIOIF_QOS_CONFIG_LEN];
    UINT1               au1Dscp[WSSIF_RADIOIF_QOS_CONFIG_LEN];
    UINT1               u1HTMaxAmpduLengthExponent;
    UINT1               u1VHTMaxAmpduLengthExponent;
    UINT1               u1WlanId;
    UINT1               u1QosIndex;
    UINT1               u1DTIMPeriod;
    UINT1               u1BPFlag;
    UINT1      u1AMPDUParam;
    UINT1      u1HTInfoSecChanAbove;
    UINT1      u1HTInfoSecChanBelow;
    UINT1      u1TxSupportMcsSet;
    INT1                i1LocalTxPower; /*This value has to filled later*/
    UINT1               u1HTOption;
    UINT1               u1RadarFound;
    UINT1               u1ChanlSwitStatus;
    UINT1               u111hDfsStatus;
    UINT1               u1ExChanSwitch;
    UINT1      u1AllowChannelWidth;
    UINT1               u1HTCapEnable;
    UINT1               u1HTOpeEnable;
    UINT1               u1AMSDUStatus;
    UINT4               u4QosInfo;
    UINT2               u2AMPDULimit;
    UINT2               u2AMSDULimit;
    UINT1               u1AMPDUStatus;
    UINT1               u1AMPDUSubFrame;
    UINT1               au1Pad1[2];
}tRadioIfGetAllDB;

typedef struct {
    BOOL1       bRadioIfQosConfig;
    BOOL1   bRadioIfIndex;
    BOOL1   bBssIfIndex;
    BOOL1       bWtpInternalId; 
    BOOL1       bCurrentTxPowerLevel;
    BOOL1       bMacAddr;
    BOOL1       bRadioId;
    BOOL1       bBindingType;
    BOOL1       bAdminStatus;
    BOOL1       bOperStatus;
    BOOL1       bIfType;
    BOOL1       bBssIdCount;
    BOOL1       bRadioAntennaType;
    BOOL1       bFailureStatus;
    BOOL1       bDiversitySupport;
    BOOL1       bAntennaMode;
    BOOL1       bAntennaSelection[RADIOIF_ANTENNA_LIST_INDEX];
    BOOL1       bCurrentTxAntenna;
    BOOL1       bRTSThreshold;
    BOOL1       bShortRetry;
    BOOL1       bLongRetry;
    BOOL1       bFragmentationThreshold;
    BOOL1       bTxMsduLifetime;
    BOOL1       bRxMsduLifetime;
    BOOL1       bBeaconPeriod;
    BOOL1       bCountryString;
    BOOL1       bOperationalRate;
    BOOL1       bSupportedRate;
    BOOL1       bAdmissionControl;
    BOOL1       bDot11RadioType; 
    BOOL1       bShortPreamble;
    BOOL1       bNoOfBssId;
    BOOL1       bProbeInclude;
    BOOL1       bBeaconInclude;
    BOOL1       bFirstChannelNumber;
    BOOL1       bNoOfChannels;
    BOOL1       bMaxTxPowerLevel;
    BOOL1       bSupportedBand;
    BOOL1       bOFDMChannelWidth;
    BOOL1       bRssiThreshold;
    BOOL1       bT1Threshold;
    BOOL1       bEDThreshold;
    BOOL1       bCurrentFrequency;
    BOOL1       bCurrentCCAMode;
    BOOL1       bCurrentChannel;
    BOOL1       bCurrentTxPower;
    BOOL1       bNoOfSupportedPowerLevels;
    BOOL1       bTxPowerLevel;
    BOOL1       bHtFlag;
    BOOL1       bMaxSuppMCS;
    BOOL1       bMaxManMCS;
    BOOL1       bTxAntenna;
    BOOL1       bRxAntenna;
    BOOL1       bChannelWidth;
    BOOL1       bShortGi20;
    BOOL1       bShortGi40;
    BOOL1       bMCSRateSet;
    BOOL1       bAMPDUSupport;
    BOOL1       bMaxMpduInAmpdu;
    BOOL1       bMaxMsdu;
    BOOL1       bHTMaxAmpduLengthExponent;
    BOOL1       bVHTMaxAmpduLengthExponent;
    BOOL1       bWlanId;
    BOOL1       bTaggingPolicy;
    BOOL1       bQosIndex;
    BOOL1       bQueueDepth;
    BOOL1       bCwMin;
    BOOL1       bCwMax;
    BOOL1       bAifsn;
    BOOL1       bTxOpLimit;
    BOOL1       bQosPrio;
    BOOL1       bDscp;
    BOOL1       bDTIMPeriod;
    BOOL1       bRadioStats;
    BOOL1       bIeee80211Stats;
    BOOL1       bNoOfRadio;
    BOOL1       bMessageType;
    BOOL1       bMessageLength;
    BOOL1       bRowStatus;
    BOOL1       bReportInterval;
    BOOL1       bMaxClientCount;
    BOOL1       bRadioCounters;
    BOOL1       bHTCapInfo;
    BOOL1       bSuppHTCapInfo;
    BOOL1 bSuppPrimaryCh;
    BOOL1 bSuppHTOpeInfo;
    BOOL1       bSuppMCSSet;
    BOOL1       bManMCSSet;
    BOOL1       bBPFlag;
    BOOL1       bAMPDUParam;
    BOOL1       bHTInfoSecChanAbove;
    BOOL1       bHTInfoSecChanBelow;
    BOOL1       bMaxMPDULen;
    BOOL1       bSuppMaxMPDULen;
    BOOL1       bSuppChanWidth;
    BOOL1       bRxLpdc;
    BOOL1       bSuppRxLpdc;
    BOOL1       bShortGi80;
    BOOL1       bSuppShortGi80;
    BOOL1       bShortGi160;
    BOOL1       bSuppVhtTxStbc;
    BOOL1       bTxStbc;
    BOOL1       bSuppVhtRxStbc;
    BOOL1       bSuppRxStbcStatus;
    BOOL1       bRxStbc;
    BOOL1       bRxStbcStatus;
    BOOL1       bSuBeamFormer;
    BOOL1       bSuBeamFormee;
    BOOL1       bSuppBeamFormAnt;
    BOOL1       bSoundDimension;
    BOOL1       bMuBeamFormer;
    BOOL1       bMuBeamFormee;
    BOOL1       bVhtTxOpPs;
    BOOL1       bHtcVhtCapable;
    BOOL1       bVhtMaxAMPDU;
    BOOL1       bSuppVhtMaxAMPDU;
    BOOL1       bVhtLinkAdaption;
    BOOL1       bVhtRxAntenna;
    BOOL1       bVhtTxAntenna;
    BOOL1       bVhtReserved;
    BOOL1       bVhtCapaMcs;
    BOOL1       bSuppVhtCapaMcs;
    BOOL1       bVhtCapInfo;
    BOOL1       bVhtChannelWidth;
    BOOL1       bSuppVhtChannelWidth;
    BOOL1       bCenterFcy0;
    BOOL1       bSuppCenterFcy0;
    BOOL1       bCenterFcy1;
    BOOL1       bVhtOperMcsSet;
    BOOL1       bVhtOperInfo;
    BOOL1       bSuppRadioType;
    BOOL1       bSuppVhtCapInfo;
    BOOL1       bTxSupportMcsSet;
    BOOL1 bMultiDomainInfo;
    BOOL1       bSuppVhtSTS;
    BOOL1       bVhtOption;
    BOOL1       bHTOption;
    BOOL1       bVhtSTS;
    BOOL1       bVhtOperModeNotify;
    BOOL1       bVhtOperModeElem;
    BOOL1       bTransPower;
    BOOL1       bMaxTxPowerCount;
    BOOL1       bMaxTxPowerUnit;
    BOOL1       bTxPower20;
    BOOL1       bTxPower40;
    BOOL1       bTxPower80;
    BOOL1       bTxPower160;
    BOOL1       bSpecMgmt;
    BOOL1       bLocalTxPower;
    BOOL1       bRxDataRate;
    BOOL1       bTxDataRate;
    BOOL1       bSuppRxDataRate;
    BOOL1       bSuppTxDataRate;
    BOOL1       bCapability;
    BOOL1       bRadarFound;
    BOOL1       bChanlSwitStatus;
    BOOL1       b11hDfsStatus;
    BOOL1       bSuppHtCapInfo;
    BOOL1       bHtCapInfo;
    BOOL1       bSuppLdpcCoding;
    BOOL1  bLdpcCoding;
    BOOL1       bSuppSmPowerSave;
    BOOL1  bSmPowerSave;
    BOOL1       bSuppHtGreenField;
    BOOL1  bHtGreenField;
    BOOL1       bSuppShortGi20;
    BOOL1       bSuppShortGi40;
    BOOL1       bSuppTxStbc;
    BOOL1       bSuppRxStbc;
    BOOL1       bSuppDelayedBlockack;
    BOOL1  bDelayedBlockack;
    BOOL1       bSuppMaxAmsduLen;
    BOOL1  bMaxAmsduLen;
    BOOL1       bSuppDsssCck40;
    BOOL1  bDsssCck40;
    BOOL1       bSuppFortyInto;
    BOOL1  bFortyInto;

    BOOL1       bAmpduParam;
    BOOL1 bSuppAmpduParam;
    BOOL1       bHtCapaMcs;
    BOOL1       bHtExtCap;
    BOOL1       bSuppHtExtCap;
    BOOL1       bTxBeamCapParam;
    BOOL1       bSuppTxBeamCapParam;
    BOOL1 bSuppBasicMCSSet;

    BOOL1 bSuppHtCapaMcs;
    BOOL1  bRxMcsBitmask;
    BOOL1  bHighSuppDataRate;
    BOOL1       bSuppTxMcsSetDefined;
    BOOL1  bTxMcsSetDefined;
    BOOL1       bSuppTxRxMcsSetNotEqual;
    BOOL1  bTxRxMcsSetNotEqual;
    BOOL1       bSuppTxMaxNoSpatStrm;
    BOOL1  bTxMaxNoSpatStrm;
    BOOL1       bSuppTxUnequalModSupp;
    BOOL1  bTxUnequalModSupp;
    BOOL1  bSuppMaxAmpduLen;
    BOOL1  bSuppMinAmpduStart;
    BOOL1  bMaxAmpduLen;
    BOOL1  bMinAmpduStart;
    BOOL1       bSuppPco;
    BOOL1  bPco;
    BOOL1       bSuppPcoTransTime;
    BOOL1  bPcoTransTime;
    BOOL1       bSuppMcsFeedback;
    BOOL1  bMcsFeedback;
    BOOL1       bSuppHtcSupp;
    BOOL1  bHtcSupp;
    BOOL1       bSuppRdResponder;
    BOOL1  bRdResponder;
    BOOL1       bSuppImpTranBeamRecCapable;
    BOOL1  bImpTranBeamRecCapable;
    BOOL1       bSuppRecStagSoundCapable;
    BOOL1  bRecStagSoundCapable;
    BOOL1       bSuppTransStagSoundCapable;
    BOOL1  bTransStagSoundCapable;
    BOOL1       bSuppRecNdpCapable;
    BOOL1  bRecNdpCapable;
    BOOL1       bSuppTransNdpCapable;
    BOOL1  bTransNdpCapable;
    BOOL1       bSuppImpTranBeamCapable;
    BOOL1  bImpTranBeamCapable;
    BOOL1       bSuppCalibration;
    BOOL1  bCalibration;
    BOOL1  bSuppExplCsiTranBeamCapable;
    BOOL1  bExplCsiTranBeamCapable;
    BOOL1  bSuppExplNoncompSteCapable;
    BOOL1  bExplNoncompSteCapable;
    BOOL1  bSuppExplCompSteCapable;
    BOOL1  bExplCompSteCapable;
    BOOL1  bSuppExplTranBeamCsiFeedback;
    BOOL1  bExplTranBeamCsiFeedback;
    BOOL1  bSuppExplNoncompBeamFbCapable;
    BOOL1  bExplNoncompBeamFbCapable;
    BOOL1  bSuppExplCompBeamFbCapable;
    BOOL1  bExplCompBeamFbCapable;
    BOOL1  bSuppMinGrouping;
    BOOL1  bMinGrouping;
    BOOL1  bSuppCsiNoofBeamAntSupported;
    BOOL1  bCsiNoofBeamAntSupported;
    BOOL1       bSuppNoncompSteNoBeamformer;
    BOOL1       bNoncompSteNoBeamformer;
    BOOL1       bSuppCompSteNoBeamformer;
    BOOL1  bCompSteNoBeamformer;
    BOOL1       bSuppCSIMaxNoRowsBeamformer;
    BOOL1  bCSIMaxNoRowsBeamformer;
    BOOL1  bSuppChannelEstCapability;
    BOOL1  bChannelEstCapability;
    BOOL1       bSuppPrimaryChannel;
    BOOL1 bPrimaryChannel;
    BOOL1       bSuppSecondaryChannel;
    BOOL1       bSecondaryChannel;
    BOOL1 bHTOpeInfo;
    BOOL1 bBasicMCSSet;
    BOOL1       bSuppStaChanWidth;
    BOOL1       bStaChanWidth;
    BOOL1       bSuppRifsMode;
    BOOL1       bRifsMode;
    BOOL1       bSuppHtProtection;
    BOOL1       bHtProtection;
    BOOL1       bSuppNongfHtStasPresent;
    BOOL1       bNongfHtStasPresent;
    BOOL1       bSuppObssNonHtStasPresent;
    BOOL1       bObssNonHtStasPresent;
    BOOL1       bSuppDualBeacon;
    BOOL1       bDualBeacon;
    BOOL1       bSuppDualCtsProtection;
    BOOL1       bDualCtsProtection;
    BOOL1       bSuppStbcBeacon;
    BOOL1       bStbcBeacon;
    BOOL1       bSuppLsigTxopProtSupp;
    BOOL1       bSuppLsigTxopProtFulSup;
    BOOL1       bLsigTxopProtFulSup;
    BOOL1       bSuppPcoActive;
    BOOL1       bPcoActive;
    BOOL1       bSuppPcoPhase;
    BOOL1       bPcoPhase;
    BOOL1  bSuppChannelWidth;
    BOOL1  bSuppRxMcsBitmask;
    BOOL1  bHighDataRate;
    BOOL1  bSuppSuppHtCapInfo;
    BOOL1  bSuppHighSuppDataRate;
    BOOL1       bAllowChannelWidth;
    BOOL1       bAMPDUStatus;
    BOOL1       bAMPDUSubFrame;
    BOOL1       bAMPDULimit;
    BOOL1       bAMSDUStatus;
    BOOL1       bAMSDULimit;
    UINT1       au1Pad[2];
}tRadioIsGetAllDB;

typedef struct {
    tRadioIfGetAllDB    RadioIfGetAllDB;
    tRadioIsGetAllDB    RadioIfIsGetAllDB;
}tRadioIfGetDB;

typedef struct 
{
    tRBTree     RadioIfDB;
} tRadioIfGlbMib;


typedef struct RADIOIF_GLOBALS 
{
    tRadioIfGlbMib       RadioIfGlbMib;
} tRadioIfGlobals;


typedef struct {
    UINT4       u4ResultCode;
    UINT1       u1MessageType;
    UINT1       u1Length;
    UINT1       au1pad[2];
} tRadioIfResultCode;

typedef enum {
    WSS_ADD_RADIO_IF_DB_PHY,
    WSS_ADD_RADIO_IF_DB,
    WSS_DEL_RADIO_IF_DB,
    WSS_GET_RADIO_IF_DB,
    WSS_GET_PHY_RADIO_IF_DB,
    WSS_SET_RADIO_IF_DB,
    WSS_SET_PHY_RADIO_IF_DB,
    WSS_GET_WLAN_INTERNAL_DB,
    WSS_GET_WLAN_IFINDEX_DB,
    WSS_GET_WLAN_BSS_IFINDEX_DB,
    WSS_GET_NEXT_WLAN_BSS_IFINDEX_DB,
    WSS_GET_WLAN_BSSID_DB,
    WSS_GET_RADIO_QOS_CONFIG_DB,
    WSS_SET_RADIO_QOS_CONFIG_DB_WLC,
    WSS_SET_RADIO_QOS_CONFIG_DB_WTP,
    WSS_DESTROY_WLAN_BSS_IFINDEX_DB,
    WSS_GET_RADIO_IF_DB_11AC,
    WSS_SET_RADIO_IF_DB_11AC,
    WSS_ASSEMBLE_11AC_CAPABILITY_INFO,
    WSS_ASSEMBLE_11AC_OPER_INFO,
    WSS_PARSE_11AC_CAPABILITY_INFO,
    WSS_PARSE_11AC_OPER_INFO,
    WSS_GET_RADIO_IF_DB_11N,
    WSS_SET_RADIO_IF_DB_11N,
    WSS_ASSEMBLE_11N_CAPABILITY_INFO,
    WSS_ASSEMBLE_11N_AMPDU_INFO,
    WSS_ASSEMBLE_11N_MCS_INFO,
    WSS_ASSEMBLE_11N_EXTCAP_INFO,
    WSS_ASSEMBLE_11N_BEAMFORMING_INFO,
    WSS_ASSEMBLE_11N_OPER_INFO,
    WSS_PARSE_11N_CAPABILITY_INFO,
    WSS_PARSE_11N_AMPDU_INFO,
    WSS_PARSE_11N_MCS_INFO,
    WSS_PARSE_11N_EXTCAP_INFO,
    WSS_PARSE_11N_BEAMFORMING_INFO,
    WSS_PARSE_11N_OPER_INFO,
  
}eRadioIfDBMsgType;

#endif  /* __RADIOIFDB_H__ */
