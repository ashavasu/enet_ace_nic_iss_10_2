/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wssifradiosz.h,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains RadioIf Sizing parameters
 *******************************************************************/


enum {
    WSSIF_RADIOIF_DB_MAX_SIZING_ID,
    WSSIFRADIO_MAX_SIZING_ID
};


#ifdef  _WSSIFRADIOSZ_C
tMemPoolId WSSIFRADIOMemPoolIds[ WSSIFRADIO_MAX_SIZING_ID];
INT4  WssifradioSizingMemCreateMemPools(VOID);
VOID  WssifradioSizingMemDeleteMemPools(VOID);
INT4  WssifradioSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _WSSIFRADIOSZ_C  */
extern tMemPoolId WSSIFRADIOMemPoolIds[ ];
extern INT4  WssifradioSizingMemCreateMemPools(VOID);
extern VOID  WssifradioSizingMemDeleteMemPools(VOID);
#endif /*  _WSSIFRADIOSZ_C  */


#ifdef  _WSSIFRADIOSZ_C
tFsModSizingParams FsWSSIFRADIOSizingParams [] = {
{ "tRadioIfDB", "WSSIF_RADIOIF_DB_MAX", sizeof(tRadioIfDB),WSSIF_RADIOIF_DB_MAX, WSSIF_RADIOIF_DB_MAX,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _WSSIFRADIOSZ_C  */
extern tFsModSizingParams FsWSSIFRADIOSizingParams [];
#endif /*  _WSSIFRADIOSZ_C  */


