/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *  $Id: wssifstawtpprot.h,v 1.2 2017/05/23 14:16:57 siva Exp $ 
 * Description:This file contains the exported definitions and
 *             macros of WSSSTA module
 *
 *******************************************************************/
#ifndef __WSSSTA_WTP_PROT_H__
#define __WSSSTA_WTP_PROT_H__

#include "wssifradiodb.h"

UINT4 WssStaInit PROTO ((VOID));
UINT4 WssStaUpdateDB PROTO ((tWssStaConfigReqInfo*));
UINT4 WssStaIdleTimeoutExpiry PROTO ((VOID));
INT4 WssStaApCompareStaDBRBTree(tRBElem *, tRBElem *);
INT4 WssStaCompareTCFilterHandleRBTree(tRBElem *, tRBElem *);
INT4 WssStaApCompareVlanDBRBTree(tRBElem *, tRBElem *);
VOID WssStaApRecvMemClear PROTO ((VOID));
INT4 StaApDBWalkFn PROTO ((tRBElem *, eRBVisit, UINT4, void *, void *));
UINT4 WssStaGetStationDB PROTO ((tWssStaStateDB *));
INT4 WssStaValidateConfigReq PROTO ((VOID));
INT4 StaDBWalkFnForDeauth(tRBElem*, eRBVisit, UINT4, void *, void *);
UINT4 WssStaDeauthMsg PROTO ((unWssMsgStructs *));
UINT4 WssStaSetRsnaPairwiseParams PROTO ((tWssStaStateDB *));
INT4 WssStaWtpShowClient PROTO ((VOID));
INT4 StaDBWalkFnForGetStation PROTO((tRBElem *, eRBVisit, UINT4, 
            void *, void *));
UINT1 WssStaGetBssid PROTO((tMacAddr * ,tMacAddr *));
UINT1 WssStaCheckIsLinkMarginUpdated PROTO ((tMacAddr *, INT1));

INT4
WssStaGetRadioInfo PROTO((UINT1 * pu1StaMac, UINT1 * pu1RadioId, UINT1 * u1WlanId));

UINT4 WssStaConstructTpcRequest PROTO ((tWssSta11hTpcMsg *));
UINT1 WssStaConstructChanlSwitchAnnounce PROTO ((UINT1, tRadioIfGetDB *));
UINT4 WssStaConstructMeasRequest PROTO ((UINT1));
VOID WssIfStaUpdateIpAddress PROTO ((UINT4, UINT1 *));
VOID WssIfStaUpdateIpAddressDelete PROTO ((UINT4, UINT1 *));
VOID WssifStaGetWtpInterfaceName PROTO ((UINT1 *));
#ifdef BAND_SELECT_WANTED
INT4
WssStaCompareWtpBandSteerDBRBTree PROTO((tRBElem * e1, tRBElem * e2));

UINT4
WssStaUpdateAPBandSteerProcessDB PROTO((eProcessDB action,
                          tWssStaBandSteerDB* pWssStaBandSteerDB));

#endif
#ifdef KERNEL_CAPWAP_WANTED
UINT1 WssStaUpdateVlanDB PROTO ((tWssStaVlanDB  *, UINT1 ));
UINT1  WssStaCreateandMapVlanToWlanIntf PROTO ((UINT2  u2VlanId));
UINT1  WssStaModifyandMapVlanToWlanIntf PROTO ((UINT2  u2VlanId));
UINT1  WssStaDeleteVlanToWlanIntf PROTO ((UINT2  u2VlanId));
INT4   WssStaGetClientPerSSID PROTO ((UINT4 u4BssIfIndex));
INT4   StaDBClientMacWalkFn PROTO ((tRBElem *, eRBVisit, UINT4, void *, void *));
#endif

UINT1 
WssStaConstructExChanlSwitchAnnounce PROTO ((UINT1 ,tRadioIfGetDB *));

UINT1
WssIfStaGetIpAddr PROTO ((UINT4 *, UINT1 *));


#endif

