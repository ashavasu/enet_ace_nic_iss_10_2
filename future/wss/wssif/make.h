#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# /*$Id: make.h,v 1.2 2017/05/23 14:16:57 siva Exp $ */
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                                  |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : ANY                                           |
# |                                                                          |   
# |   DATE                   : 04 Mar 2013                                   |
# |                                                                          |  
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


TOTAL_OPNS =  $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

FCAP_INC     = ${BASE_DIR}/wss/fcapwap/inc
WSSIF_BASE_DIR = ${BASE_DIR}/wss/wssif
WSSIF_INC_DIR  = ${WSSIF_BASE_DIR}/inc
WSSIF_SRC_DIR  = ${WSSIF_BASE_DIR}/src
WSSIF_OBJ_DIR  = ${WSSIF_BASE_DIR}/obj
RADIOIF_INC_DIR  = ${BASE_DIR}/wss/radioif/inc
WSSWLAN_INC_DIR  = ${BASE_DIR}/wss/wsswlan/inc
WSSMAC_INC_DIR  = ${BASE_DIR}/wss/wssmac/inc
RFMGMT_INC_DIR  = ${BASE_DIR}/wss/rfmgmt/inc
BCNMGR_INC_DIR  = ${BASE_DIR}/wss/bcnmgr/inc
APHDLR_INC_DIR = ${BASE_DIR}/wss/aphdlr/inc
WLCHDLR_INC_DIR = ${BASE_DIR}/wss/wlchdlr/inc
WSSUSER_INC_DIR = ${BASE_DIR}/wss/wssuser/inc
CAPWAP_INC_DIR = ${BASE_DIR}/wss/capwap/inc
WSSAUTH_INC_DIR = ${BASE_DIR}/wss/wssauth/inc
WSSWLAN_INC_DIR = ${BASE_DIR}/wss/wsswlan/inc
WSSSTA_INC_DIR = ${BASE_DIR}/wss/wsssta/inc
CFA_INC_DIR = ${BASE_DIR}/cfa2/inc
WSSPM_INC_DIR = ${BASE_DIR}/wss/wsspm/inc
COMMON_INC_DIR = ${BASE_DIR}/ISS/common/system/inc
WSSWLAN_INC_DIR = ${BASE_DIR}/wss/wsswlan/inc
VLAN_INC_DIR = ${BASE_DIR}/vlangarp/vlan/inc
WSSCFG_INC_DIR = ${BASE_DIR}/wss/wsscfg/inc
PNAC_INC_DIR = ${BASE_DIR}/pnac/inc
NETIP_INC_DIR    = ${BASE_DIR}/netip/ipmgmt/inc

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${CFA_INC_DIR}  -I${WSSIF_INC_DIR} -I${WSSWLAN_INC_DIR} -I${WSSMAC_INC_DIR} -I${RADIOIF_INC_DIR} -I${BCNMGR_INC_DIR} -I${APHDLR_INC_DIR} -I${WLCHDLR_INC_DIR} -I${CAPWAP_INC_DIR} -I${WSSAUTH_INC_DIR} -I${WSSSTA_INC_DIR} -I${COMMON_INC_DIR} -I${WSSPM_INC_DIR} -I${WSSWLAN_INC_DIR} -I${WSSWLAN_INC_DIR} -I${VLAN_INC_DIR} -I${RFMGMT_INC_DIR} -I${FCAP_INC} -I${WSSUSER_INC_DIR} -I${WSSCFG_INC_DIR} -I${PNAC_INC_DIR} -I${NETIP_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
