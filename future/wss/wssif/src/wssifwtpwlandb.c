
/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 *  $Id: wssifwtpwlandb.c,v 1.3 2017/11/24 10:37:10 siva Exp $
 *                                                                             *
 *  Description: This file contains the WSSWLAN util module APIs in WTP       *
 *                                                                            *
 ******************************************************************************/
#ifndef __WSSIFWTPWLANDB_C__
#define __WSSIFWTPWLANDB_C__

#include "wssifinc.h"
#include "sizereg.h"

/*****************************************************************************
 *  Function Name   : WlanWssInit                                            *
 *  Description     : This function used to initialize the WssWlan session   *
 *                    profile RB Tree                                        *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 * **************************************************************************/

UINT1
WssWlanInit (VOID)
{

    WSSIF_FN_ENTRY ();
    if (WssifwtpwlanSizingMemCreateMemPools () != OSIX_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "\nwlanWssInit: MemPoolCreation Failed\r\n");
        return OSIX_FAILURE;

    }

    /* Create RB tree for WssWlanInterfaceDBCreate */
    if (WssWlanInterfaceDBCreate () != OSIX_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "wlanWssAPInit: RB Tree Creation Failed\
                    for tWssWlanInterfaceDB\r\n");
        return OSIX_FAILURE;
    }
    if (WssWlanIfIndexDBCreate () != OSIX_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "wlanWssInit: RB Tree Creation Failed for\
                    tWssWlanIfIndexDB\r\n");
        return OSIX_FAILURE;

    }
    if (WssWlanBSSIDMappingDBCreate () != OSIX_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "wlanWssInit: RB Tree Creation Failed for\
                    tWssWlanBSSIDMappingDB\r\n");
        return OSIX_FAILURE;

    }

    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : WssWlanIfIndexDBRBCmp                                */
/*                                                                           */
/* Description        : This function compares the two WLAN  Interface Index */
/*                       RB Tree's based on the Wlan interface index         */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS or OSIX_FAILURE                         */
/*****************************************************************************/

INT4
WssWlanIfIndexDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tWssWlanIfIndexDB  *pNode1 = e1;
    tWssWlanIfIndexDB  *pNode2 = e2;

    WSSIF_FN_ENTRY ();

    if (pNode1->u4WlanIfIndex > pNode2->u4WlanIfIndex)
    {
        return 1;
    }
    else if (pNode1->u4WlanIfIndex < pNode2->u4WlanIfIndex)
    {
        return -1;
    }
    WSSIF_FN_EXIT ();
    return 0;
}

/*****************************************************************************/
/* Function Name      : WssWlanInterfaceDBRBCmp                              */
/*                                                                           */
/* Description        : This function compares the two WLAN  Interface Index */
/*                       RB Tree's based on the Wlan interface index         */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 , 1 or -1                                          */
/*****************************************************************************/
INT4
WssWlanInterfaceDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tWssWlanInterfaceDB *pNode1 = e1;
    tWssWlanInterfaceDB *pNode2 = e2;

    WSSIF_FN_ENTRY ();

    if (pNode1->u1RadioId > pNode2->u1RadioId)
    {
        return 1;
    }
    else if (pNode1->u1RadioId < pNode2->u1RadioId)
    {
        return -1;
    }
    if (pNode1->u1WlanId > pNode2->u1WlanId)
    {
        return 1;
    }
    else if (pNode1->u1WlanId < pNode2->u1WlanId)
    {
        return -1;
    }
    WSSIF_FN_EXIT ();
    return 0;
}

/*****************************************************************************/
/* Function Name      : WssWlanSsidMappingDBRBCmp                            */
/*                                                                           */
/* Description        : This function compares the two WLAN BSS ID MappingDB */
/*                       RB Tree's based on the BSS Mac Address              */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 , 1 or -1                                          */
/*****************************************************************************/
INT4
WssWlanBSSIDMappingDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tWssWlanBSSIDMappingDB *pNode1 = e1;
    tWssWlanBSSIDMappingDB *pNode2 = e2;

    INT4                i4RetVal = 0;

    WSSIF_FN_ENTRY ();
    i4RetVal = MEMCMP (pNode1->BssId, pNode2->BssId, MAC_ADDR_LEN);

    if (i4RetVal > 0)
    {
        return 1;
    }
    else if (i4RetVal < 0)
    {
        return -1;
    }
    WSSIF_FN_EXIT ();
    return 0;
}

/****************************************************************************
 *  Function    :  WssWlanIfIndexDBCreate
 *  Input       :  None
 *  Description :  This function creates the WssWlanIfIndexDB
 *                 i.e. an RBTree.
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/

PUBLIC UINT1
WssWlanIfIndexDBCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    WSSIF_FN_ENTRY ();
    u4RBNodeOffset = FSAP_OFFSETOF (tWssWlanIfIndexDB, IfIndexDBNode);

    if ((gWssWlanGlobals.WssWlanGlbMib.WssWlanIfIndexDB =
         RBTreeCreateEmbedded (u4RBNodeOffset, WssWlanIfIndexDBRBCmp)) == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "WssWlanIfIndexDBCreate failed\r\n");

        return OSIX_FAILURE;
    }
    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 *  Function    :  WssWlanInterfaceDBCreate
 *  Input       :  None
 *  Description :  This function creates the WssWlanInterfaceDB
 *                 i.e. a RBTree.
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/

PUBLIC UINT1
WssWlanInterfaceDBCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    WSSIF_FN_ENTRY ();
    u4RBNodeOffset = FSAP_OFFSETOF (tWssWlanInterfaceDB, WlanInterfaceDBNode);

    if ((gWssWlanGlobals.WssWlanGlbMib.WssWlanInterfaceDB =
         RBTreeCreateEmbedded (u4RBNodeOffset, WssWlanInterfaceDBRBCmp))
        == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "WssWlanInterfaceDBCreate failed\r\n");

        return OSIX_FAILURE;
    }
    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 *  Function    :  WssWlanBSSIDMappingDBCreate
 *  Input       :  None
 *  Description :  This function creates the WssWlanBssIdMappingDB
 *                 i.e. a RBTree.
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/

PUBLIC UINT1
WssWlanBSSIDMappingDBCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    WSSIF_FN_ENTRY ();
    u4RBNodeOffset = FSAP_OFFSETOF (tWssWlanBSSIDMappingDB, BssidMappingDBNode);

    if ((gWssWlanGlobals.WssWlanGlbMib.WssWlanBSSIDMappingDB =
         RBTreeCreateEmbedded (u4RBNodeOffset, WssWlanBSSIDMappingDBRBCmp))
        == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "WssWlanBSSIDMappingDBCreate failed\r\n");
        return OSIX_FAILURE;
    }
    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 *  Function    :  WssIfGetDtimPeriod
 *  Input       :  u4RadioIfIndex,au2WlanIfIndex
 *  Description :  Invoke WssIfProcessWssWlanDBMsg for getting hardware address
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/

INT4
WssIfGetDtimPeriod (UINT2 au2WlanIfIndex, BOOL1 bFlag, INT4 *pi4GetDtimPeriod)
{
    UINT1               u1RetVal;
    tWssWlanDB          pWssWlanDB;
    pWssWlanDB.WssWlanAttributeDB.u4WlanIfIndex = au2WlanIfIndex;
    pWssWlanDB.WssWlanIsPresentDB.bDtimPeriod = bFlag;
    u1RetVal =
        WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &pWssWlanDB);
    if (u1RetVal == OSIX_SUCCESS)
    {
        *pi4GetDtimPeriod = (INT4) pWssWlanDB.WssWlanAttributeDB.i2DtimPeriod;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *  Function    :  wsswlanGetHwAddress
 *  Input       :  u4RadioIfIndex,au2WlanIfIndex
 *  Description :  Invoke WssIfProcessWssWlanDBMsg for getting hardware address
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/

INT4
WssIfGetHwAddress (UINT2 au2WlanIfIndex, BOOL1 bFlag, UINT1 *pu1GetHwAddress)
{
    UINT1               u1RetVal;
    tWssWlanDB          pWssWlanDB;
    pWssWlanDB.WssWlanAttributeDB.u4WlanIfIndex = au2WlanIfIndex;
    pWssWlanDB.WssWlanIsPresentDB.bBssId = bFlag;
    u1RetVal =
        WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &pWssWlanDB);
    if (u1RetVal == OSIX_SUCCESS)
    {
        MEMCPY (pu1GetHwAddress, pWssWlanDB.WssWlanAttributeDB.BssId,
                MAC_ADDR_LEN);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *  Function    :  wsswlanGetHideSsid
 *  Input       :  u4RadioIfIndex,au2WlanIfIndex
 *  Description :  Invoke WssIfProcessWssWlanDBMsg for getting ssid
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
INT4
WssIfGetHideSsid (UINT2 au2WlanIfIndex, BOOL1 bFlag, INT4 *pu1GetHdEssid)
{
    UINT1               u1RetVal;
    tWssWlanDB          pWssWlanDB;
    pWssWlanDB.WssWlanAttributeDB.u4WlanIfIndex = au2WlanIfIndex;
    pWssWlanDB.WssWlanIsPresentDB.bSupressSsid = bFlag;
    u1RetVal =
        WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY, &pWssWlanDB);
    if (u1RetVal == OSIX_SUCCESS)
    {
        *pu1GetHdEssid = (INT4) pWssWlanDB.WssWlanAttributeDB.u1SupressSsid;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *  Function    :  wsswlanGetSsid
 *  Input       :  u4RadioIfIndex,au2WlanIfIndex
 *  Description :  Invoke WssIfProcessWssWlanDBMsg for getting ssid
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
INT4
WssIfGetSsid (UINT2 au2WlanIfIndex, BOOL1 bFlag, UINT1 *pu1GetEssid)
{
    UINT1               u1RetVal;
    tWssWlanDB          pWssWlanDB;
    pWssWlanDB.WssWlanAttributeDB.u4WlanIfIndex = au2WlanIfIndex;
    pWssWlanDB.WssWlanIsPresentDB.bDesiredSsid = bFlag;
    u1RetVal =
        WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY, &pWssWlanDB);
    if (u1RetVal == OSIX_SUCCESS)
    {
        MEMCPY (pu1GetEssid, pWssWlanDB.WssWlanAttributeDB.au1DesiredSsid,
                WSSWLAN_SSID_NAME_LEN);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *  Function    :  WssIfProcessWssWlanDBMsg
 *  Input       :  pRBElem1 - Pointer to the node1 for comparision
 *  Description :  Invoke he correponding DB Table to set/get DB information
 *                 CapwapDot11WlanBindTable table
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
UINT1
WssIfProcessWssWlanDBMsg (UINT1 u1OpCode, tWssWlanDB * pWssWlanDB)
{
    tWssWlanIfIndexDB   WssWlanIfIndexDB;
    tWssWlanIfIndexDB  *pWssWlanIfIndexDB = NULL;
    tWssWlanInterfaceDB WssWlanInterfaceDB;
    tWssWlanInterfaceDB *pWssWlanInterfaceDB = NULL;
    tWssWlanBSSIDMappingDB WssWlanBSSIDMappingDB;
    tWssWlanBSSIDMappingDB *pWssWlanBSSIDMappingDB = NULL;
    tRadioIfDB         *pRadioIfDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    tRadioIfDB          RadioIfDB;
    UINT1               u1Count = 0;
    tMacAddr            BssId;

    MEMSET (&WssWlanIfIndexDB, 0, sizeof (tWssWlanIfIndexDB));
    MEMSET (&WssWlanInterfaceDB, 0, sizeof (tWssWlanInterfaceDB));
    MEMSET (&WssWlanBSSIDMappingDB, 0, sizeof (tWssWlanBSSIDMappingDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));

    WSSIF_FN_ENTRY ();
    WSSIF_LOCK;
    switch (u1OpCode)
    {

        case WSS_WLAN_CREATE_IFINDEX_ENTRY:
            if (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:IfIndex is empty\
                        WlanIfIndex creation failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            if ((pWssWlanIfIndexDB =
                 (tWssWlanIfIndexDB *)
                 MemAllocMemBlk (WSSWLAN_IF_INDEX_DB_POOLID)) == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:WlanIfIndex Memory \
                        allocation failed \r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            MEMSET (pWssWlanIfIndexDB, 0, sizeof (tWssWlanIfIndexDB));
            pWssWlanIfIndexDB->u4WlanIfIndex =
                pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;

            if (RBTreeAdd
                (gWssWlanGlobals.WssWlanGlbMib.WssWlanIfIndexDB,
                 (tRBElem *) pWssWlanIfIndexDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (WSSWLAN_IF_INDEX_DB_POOLID,
                                    (UINT1 *) pWssWlanIfIndexDB);
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:RBTreeAdd failed\
                        for WlanIfIndex\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            break;

        case WSS_WLAN_DESTROY_IFINDEX_ENTRY:
            if (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:IfIndex is empty\
                        deletion of WlanIfIndexDB failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            WssWlanIfIndexDB.u4WlanIfIndex =
                pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;

            pWssWlanIfIndexDB =
                RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.WssWlanIfIndexDB,
                           (tRBElem *) & WssWlanIfIndexDB);

            if (pWssWlanIfIndexDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        RBTreeGet wlanIfIndex failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;

            }
            RBTreeRem (gWssWlanGlobals.WssWlanGlbMib.WssWlanIfIndexDB,
                       &WssWlanIfIndexDB);
            MemReleaseMemBlock (WSSWLAN_IF_INDEX_DB_POOLID,
                                (UINT1 *) pWssWlanIfIndexDB);

            break;

        case WSS_WLAN_GET_IFINDEX_ENTRY:

            MEMSET (&WssWlanIfIndexDB, 0, sizeof (tWssWlanIfIndexDB));

            if (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex != 0)
            {
                WssWlanIfIndexDB.u4WlanIfIndex =
                    pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
            }
            else
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        Key value WlanIfIndex is empty\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            WssWlanIfIndexDB.u4WlanIfIndex =
                pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;

            pWssWlanIfIndexDB =
                RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.WssWlanIfIndexDB,
                           (tRBElem *) & WssWlanIfIndexDB);

            if (pWssWlanIfIndexDB == NULL)
            {

                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        RBTreeGet from WssWlanIfIndexDB returned NULL\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bVlanId != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u2VlanId =
                    pWssWlanIfIndexDB->u2VlanId;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bVlanFlag != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1VlanFlag =
                    pWssWlanIfIndexDB->u1VlanFlag;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bDtimPeriod != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.i2DtimPeriod =
                    pWssWlanIfIndexDB->i2DtimPeriod;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bGroupTsc != OSIX_FALSE)
            {
                MEMCPY ((pWssWlanDB->WssWlanAttributeDB.au1GroupTsc),
                        (pWssWlanIfIndexDB->au1GroupTsc),
                        WSS_WLAN_GROUP_TSC_LEN);
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bDesiredSsid != OSIX_FALSE)
            {
                MEMCPY ((pWssWlanDB->WssWlanAttributeDB.au1DesiredSsid),
                        (pWssWlanIfIndexDB->au1DesiredSsid),
                        WSSWLAN_SSID_NAME_LEN);
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bCapability != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u2Capability =
                    pWssWlanIfIndexDB->u2Capability;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanMulticastMode != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u2WlanMulticastMode =
                    pWssWlanIfIndexDB->u2WlanMulticastMode;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanSnoopTableLength !=
                OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u2WlanSnoopTableLength =
                    pWssWlanIfIndexDB->u2WlanSnoopTableLength;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanSnoopTimer != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4WlanSnoopTimer =
                    pWssWlanIfIndexDB->u4WlanSnoopTimer;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanSnoopTimeout != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4WlanSnoopTimeout =
                    pWssWlanIfIndexDB->u4WlanSnoopTimeout;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosProfileId != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1QosProfileId =
                    pWssWlanIfIndexDB->u1QosProfileId;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bEncryptDecryptCapab
                != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1EncryptDecryptCapab =
                    pWssWlanIfIndexDB->u1EncryptDecryptCapab;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bPowerConstraint != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1PowerConstraint =
                    pWssWlanIfIndexDB->u1PowerConstraint;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bSupressSsid != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1SupressSsid =
                    pWssWlanIfIndexDB->u1SupressSsid;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bSsidIsolation != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1SsidIsolation =
                    pWssWlanIfIndexDB->u1SsidIsolation;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanIfType != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4WlanIfType =
                    pWssWlanIfIndexDB->u4WlanIfType;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bKeyIndex != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1KeyIndex =
                    pWssWlanIfIndexDB->u1KeyIndex;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bKeyType != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1KeyType =
                    pWssWlanIfIndexDB->u1KeyType;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bKeyStatus != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1KeyStatus =
                    pWssWlanIfIndexDB->u1KeyStatus;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bBssId != OSIX_FALSE)
            {
                MEMCPY ((pWssWlanDB->WssWlanAttributeDB.BssId),
                        (pWssWlanIfIndexDB->BssId), MAC_ADDR_LEN);
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWepKey != OSIX_FALSE)
            {
                MEMCPY ((pWssWlanDB->WssWlanAttributeDB.au1WepKey),
                        (pWssWlanIfIndexDB->au1WepKey),
                        STRLEN ((pWssWlanIfIndexDB->au1WepKey)));
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bAuthAlgorithm != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1AuthAlgorithm =
                    pWssWlanIfIndexDB->u1AuthAlgorithm;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bRadioId != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1RadioId =
                    pWssWlanIfIndexDB->u1RadioId;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanId != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1WlanId =
                    pWssWlanIfIndexDB->u1WlanId;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bAdminStatus != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1AdminStatus =
                    pWssWlanIfIndexDB->u1AdminStatus;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bAuthMethod != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1AuthMethod =
                    pWssWlanIfIndexDB->u1AuthMethod;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWebAuthStatus != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1WebAuthStatus =
                    pWssWlanIfIndexDB->u1WebAuthStatus;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bExternalWebAuthMethod !=
                OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1ExternalWebAuthMethod =
                    pWssWlanIfIndexDB->u1ExternalWebAuthMethod;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosSelection != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1QosSelection =
                    pWssWlanIfIndexDB->u1QosSelection;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bMaxClientCount != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4MaxClientCount =
                    pWssWlanIfIndexDB->u4MaxClientCount;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bMacType != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1MacType =
                    pWssWlanIfIndexDB->u1MacType;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bTunnelMode != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1TunnelMode =
                    pWssWlanIfIndexDB->u1TunnelMode;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bMitigationRequirement
                != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1MitigationRequirement =
                    pWssWlanIfIndexDB->u1MitigationRequirement;
            }
            /* QosRatelimit */

            if (pWssWlanDB->WssWlanIsPresentDB.bQosRateLimit != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1QosRateLimit =
                    pWssWlanIfIndexDB->u1QosRateLimit;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamCIR != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamCIR =
                    pWssWlanIfIndexDB->u4QosUpStreamCIR;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamCBS != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamCBS =
                    pWssWlanIfIndexDB->u4QosUpStreamCBS;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamEIR != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamEIR =
                    pWssWlanIfIndexDB->u4QosUpStreamEIR;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamEBS != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamEBS =
                    pWssWlanIfIndexDB->u4QosUpStreamEBS;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamCIR != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamCIR =
                    pWssWlanIfIndexDB->u4QosDownStreamCIR;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamCBS != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamCBS =
                    pWssWlanIfIndexDB->u4QosDownStreamCBS;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamEIR != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamEIR =
                    pWssWlanIfIndexDB->u4QosDownStreamEIR;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamEBS != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamEBS =
                    pWssWlanIfIndexDB->u4QosDownStreamEBS;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bPassengerTrustMode !=
                OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1PassengerTrustMode =
                    pWssWlanIfIndexDB->u1PassengerTrustMode;
            }

            /* RSNA IE Elements -> GET */

            if (pWssWlanDB->WssWlanIsPresentDB.bRSNAIEElements != OSIX_FALSE)
            {
                /* RSN elements */
                pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.u1ElemId =
                    pWssWlanIfIndexDB->u1ElemId;

                pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.u1Len =
                    pWssWlanIfIndexDB->u1Len;

                pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.u2Ver =
                    pWssWlanIfIndexDB->u2Ver;

                MEMCPY (&(pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                          au1GroupCipherSuite),
                        &(pWssWlanIfIndexDB->au1GroupCipherSuite),
                        RSNA_CIPHER_SUITE_LEN);

                pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                    u2PairwiseCipherSuiteCount =
                    pWssWlanIfIndexDB->u2PairwiseCipherSuiteCount;

                for (u1Count = 0;
                     u1Count <
                     pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                     u2PairwiseCipherSuiteCount; u1Count++)
                {
                    MEMCPY (&(pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                              aRsnaPwCipherDB[u1Count].au1PairwiseCipher),
                            &(pWssWlanIfIndexDB->
                              aRsnaPwCipherDB[u1Count].au1PairwiseCipher),
                            RSNA_CIPHER_SUITE_LEN);
                }

                pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                    u2AKMSuiteCount = pWssWlanIfIndexDB->u2AKMSuiteCount;

                for (u1Count = 0;
                     u1Count <
                     pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                     u2AKMSuiteCount; u1Count++)
                {
                    MEMCPY (&(pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                              aRsnaAkmDB[u1Count].au1AKMSuite),
                            &(pWssWlanIfIndexDB->aRsnaAkmDB[u1Count].
                              au1AKMSuite), RSNA_AKM_SELECTOR_LEN);
                }

                pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.u2RsnCapab =
                    pWssWlanIfIndexDB->u2RsnCapab;

                pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.u2PmkidCount =
                    pWssWlanIfIndexDB->u2PmkidCount;

                MEMCPY (&(pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                          aPmkidList),
                        &(pWssWlanIfIndexDB->aPmkidList), RSNA_PMKID_LEN);

#ifdef PMF_WANTED
                if (((pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.u2RsnCapab &
                      RSNA_CAPABILITY_MFPR) == RSNA_CAPABILITY_MFPR) ||
                    ((pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.u2RsnCapab &
                      RSNA_CAPABILITY_MFPC) == RSNA_CAPABILITY_MFPC))
                {
                    MEMCPY (&(pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                              au1GroupMgmtCipherSuite),
                            &(pWssWlanIfIndexDB->au1GroupMgmtCipherSuite),
                            RSNA_CIPHER_SUITE_LEN);
                }
#endif

            }
#ifdef BAND_SELECT_WANTED
            if (pWssWlanDB->WssWlanIsPresentDB.bBandSelectStatus != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1BandSelectStatus =
                    pWssWlanIfIndexDB->u1BandSelectStatus;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bAgeOutSuppression != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1AgeOutSuppression =
                    pWssWlanIfIndexDB->u1AgeOutSuppression;
            }
#endif

            /* RSNA IE Elements -> End */
#ifdef WPS_WANTED
            /* WPS IE Elements -> GET */

            if ((pWssWlanDB->WssWlanIsPresentDB.bWPSEnabled != OSIX_FALSE) ||
                (pWssWlanDB->WssWlanIsPresentDB.bWPSAdditionalIE != OSIX_FALSE))
            {
                pWssWlanDB->WssWlanAttributeDB.WpsIEElements.u1ElemId =
                    pWssWlanIfIndexDB->u1WpsElemId;

                pWssWlanDB->WssWlanAttributeDB.WpsIEElements.u1Len =
                    pWssWlanIfIndexDB->u1WpsLen;

                pWssWlanDB->WssWlanAttributeDB.WpsIEElements.bWpsEnabled =
                    pWssWlanIfIndexDB->bWpsEnabled;

                pWssWlanDB->WssWlanAttributeDB.WpsIEElements.wscState =
                    pWssWlanIfIndexDB->wscState;

                pWssWlanDB->WssWlanAttributeDB.WpsIEElements.bWpsAdditionalIE =
                    pWssWlanIfIndexDB->bWpsAdditionalIE;

                pWssWlanDB->WssWlanAttributeDB.WpsIEElements.noOfAuthMac =
                    pWssWlanIfIndexDB->noOfAuthMac;

                for (u1Count = 0;
                     u1Count <
                     pWssWlanDB->WssWlanAttributeDB.WpsIEElements.
                     noOfAuthMac; u1Count++)
                {
                    MEMCPY (&(pWssWlanDB->WssWlanAttributeDB.WpsIEElements.
                              authorized_macs[u1Count]),
                            &(pWssWlanIfIndexDB->
                              authorized_macs[u1Count]), WPS_ETH_ALEN);
                }

                MEMCPY (pWssWlanDB->WssWlanAttributeDB.WpsIEElements.uuid_e,
                        pWssWlanIfIndexDB->uuid_e, WPS_UUID_LEN);

                pWssWlanDB->WssWlanAttributeDB.WpsIEElements.configMethods =
                    pWssWlanIfIndexDB->configMethods;
                MEMCPY (pWssWlanDB->WssWlanAttributeDB.WpsIEElements.
                        au1WtpModelNumber, pWssWlanIfIndexDB->au1WtpModelNumber,
                        STRLEN (pWssWlanIfIndexDB->au1WtpModelNumber));
                MEMCPY (pWssWlanDB->WssWlanAttributeDB.WpsIEElements.
                        au1WtpSerialNumber,
                        pWssWlanIfIndexDB->au1WtpSerialNumber,
                        STRLEN (pWssWlanIfIndexDB->au1WtpSerialNumber));
                MEMCPY (pWssWlanDB->WssWlanAttributeDB.WpsIEElements.au1WtpName,
                        pWssWlanIfIndexDB->au1WtpName,
                        STRLEN (pWssWlanIfIndexDB->au1WtpName));

            }
            /* WPS IE Elements -> End */
#endif
            break;
        case WSS_WLAN_SET_IFINDEX_ENTRY:
            MEMSET (&WssWlanIfIndexDB, 0, sizeof (tWssWlanIfIndexDB));

            if (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex != 0)
            {
                WssWlanIfIndexDB.u4WlanIfIndex =
                    pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
            }
            else
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        Key value WlanIfIndex is empty\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            WssWlanIfIndexDB.u4WlanIfIndex =
                pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;

            pWssWlanIfIndexDB =
                RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.WssWlanIfIndexDB,
                           (tRBElem *) & WssWlanIfIndexDB);

            if (pWssWlanIfIndexDB == NULL)
            {

                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        RBTreeGet from WssWlanIfIndexDB returned NULL\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bVlanId != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u2VlanId =
                    pWssWlanDB->WssWlanAttributeDB.u2VlanId;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bVlanFlag != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1VlanFlag =
                    pWssWlanDB->WssWlanAttributeDB.u1VlanFlag;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bDtimPeriod != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->i2DtimPeriod =
                    pWssWlanDB->WssWlanAttributeDB.i2DtimPeriod;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bGroupTsc != OSIX_FALSE)
            {
                MEMCPY ((pWssWlanIfIndexDB->au1GroupTsc),
                        (pWssWlanDB->WssWlanAttributeDB.au1GroupTsc),
                        WSS_WLAN_GROUP_TSC_LEN);
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bDesiredSsid != OSIX_FALSE)
            {
                MEMCPY ((pWssWlanIfIndexDB->au1DesiredSsid),
                        (pWssWlanDB->WssWlanAttributeDB.au1DesiredSsid),
                        WSSWLAN_SSID_NAME_LEN);
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bQosProfileId != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1QosProfileId =
                    pWssWlanDB->WssWlanAttributeDB.u1QosProfileId;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bCapability != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u2Capability =
                    pWssWlanDB->WssWlanAttributeDB.u2Capability;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanMulticastMode != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u2WlanMulticastMode =
                    pWssWlanDB->WssWlanAttributeDB.u2WlanMulticastMode;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanSnoopTableLength !=
                OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u2WlanSnoopTableLength =
                    pWssWlanDB->WssWlanAttributeDB.u2WlanSnoopTableLength;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanSnoopTimer != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4WlanSnoopTimer =
                    pWssWlanDB->WssWlanAttributeDB.u4WlanSnoopTimer;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanSnoopTimeout != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4WlanSnoopTimeout =
                    pWssWlanDB->WssWlanAttributeDB.u4WlanSnoopTimeout;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bEncryptDecryptCapab
                != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1EncryptDecryptCapab =
                    pWssWlanDB->WssWlanAttributeDB.u1EncryptDecryptCapab;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bPowerConstraint != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1PowerConstraint =
                    pWssWlanDB->WssWlanAttributeDB.u1PowerConstraint;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bSupressSsid != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1SupressSsid =
                    pWssWlanDB->WssWlanAttributeDB.u1SupressSsid;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bSsidIsolation != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1SsidIsolation =
                    pWssWlanDB->WssWlanAttributeDB.u1SsidIsolation;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanIfType != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4WlanIfType =
                    pWssWlanDB->WssWlanAttributeDB.u4WlanIfType;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bKeyIndex != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1KeyIndex =
                    pWssWlanDB->WssWlanAttributeDB.u1KeyIndex;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bKeyType != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1KeyType =
                    pWssWlanDB->WssWlanAttributeDB.u1KeyType;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bKeyStatus != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1KeyStatus =
                    pWssWlanDB->WssWlanAttributeDB.u1KeyStatus;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bBssId != OSIX_FALSE)
            {
                MEMCPY ((pWssWlanIfIndexDB->BssId),
                        (pWssWlanDB->WssWlanAttributeDB.BssId), MAC_ADDR_LEN);
                WSSIF_TRC5 (WSSIF_MGMT_TRC,
                            "\n Mac Address %d %d %d %d %d \r\n",
                            pWssWlanIfIndexDB->BssId[0],
                            pWssWlanIfIndexDB->BssId[1],
                            pWssWlanIfIndexDB->BssId[3],
                            pWssWlanIfIndexDB->BssId[4],
                            pWssWlanIfIndexDB->BssId[5]);

            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWepKey != OSIX_FALSE)
            {
                MEMCPY ((pWssWlanIfIndexDB->au1WepKey),
                        (pWssWlanDB->WssWlanAttributeDB.au1WepKey),
                        STRLEN ((pWssWlanDB->WssWlanAttributeDB.au1WepKey)));
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bAuthAlgorithm != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1AuthAlgorithm =
                    pWssWlanDB->WssWlanAttributeDB.u1AuthAlgorithm;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bRadioId != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1RadioId =
                    pWssWlanDB->WssWlanAttributeDB.u1RadioId;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanId != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1WlanId =
                    pWssWlanDB->WssWlanAttributeDB.u1WlanId;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bAdminStatus != OSIX_FALSE)
            {

                pWssWlanIfIndexDB->u1AdminStatus =
                    pWssWlanDB->WssWlanAttributeDB.u1AdminStatus;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bMaxClientCount != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4MaxClientCount =
                    pWssWlanDB->WssWlanAttributeDB.u4MaxClientCount;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bAuthMethod != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1AuthMethod =
                    pWssWlanDB->WssWlanAttributeDB.u1AuthMethod;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWebAuthStatus != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1WebAuthStatus =
                    pWssWlanDB->WssWlanAttributeDB.u1WebAuthStatus;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bExternalWebAuthMethod !=
                OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1ExternalWebAuthMethod =
                    pWssWlanDB->WssWlanAttributeDB.u1ExternalWebAuthMethod;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosSelection != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1QosSelection =
                    pWssWlanDB->WssWlanAttributeDB.u1QosSelection;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bMacType != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1MacType =
                    pWssWlanDB->WssWlanAttributeDB.u1MacType;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bTunnelMode != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1TunnelMode =
                    pWssWlanDB->WssWlanAttributeDB.u1TunnelMode;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bMitigationRequirement
                != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1MitigationRequirement =
                    pWssWlanDB->WssWlanAttributeDB.u1MitigationRequirement;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWssWlanQosCapab != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanQosCapab.u1QosInfo =
                    pWssWlanDB->WssWlanAttributeDB.WssWlanQosCapab.u1QosInfo;
            }
            /* QosRateLimit */
            if (pWssWlanDB->WssWlanIsPresentDB.bQosRateLimit != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1QosRateLimit =
                    pWssWlanDB->WssWlanAttributeDB.u1QosRateLimit;

            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamCIR != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4QosUpStreamCIR =
                    pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamCIR;

            }
            if (pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamCBS != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4QosUpStreamCBS =
                    pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamCBS;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamEIR != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4QosUpStreamEIR =
                    pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamEIR;

            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamEBS != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4QosUpStreamEBS =
                    pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamEBS;

            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamCIR != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4QosDownStreamCIR =
                    pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamCIR;

            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamCBS != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4QosDownStreamCBS =
                    pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamCBS;

            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamEIR != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4QosDownStreamEIR =
                    pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamEIR;

            }
            if (pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamEBS != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4QosDownStreamEBS =
                    pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamEBS;

            }
            if (pWssWlanDB->WssWlanIsPresentDB.bPassengerTrustMode !=
                OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1PassengerTrustMode =
                    pWssWlanDB->WssWlanAttributeDB.u1PassengerTrustMode;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWpaIEElements != OSIX_FALSE)
            {
                /* RSN elements */
                pWssWlanIfIndexDB->u1ElemId =
                    pWssWlanDB->WssWlanAttributeDB.WpaIEElements.u1ElemId;

                pWssWlanIfIndexDB->u1Len =
                    pWssWlanDB->WssWlanAttributeDB.WpaIEElements.u1Len;

                pWssWlanIfIndexDB->u2Ver =
                    pWssWlanDB->WssWlanAttributeDB.WpaIEElements.u2Ver;

                MEMCPY (&(pWssWlanIfIndexDB->au1GroupCipherSuite),
                        &(pWssWlanDB->WssWlanAttributeDB.WpaIEElements.
                          au1GroupCipherSuite), RSNA_CIPHER_SUITE_LEN);

                pWssWlanIfIndexDB->u2PairwiseCipherSuiteCount =
                    pWssWlanDB->WssWlanAttributeDB.WpaIEElements.
                    u2PairwiseCipherSuiteCount;

                for (u1Count = 0;
                     u1Count <
                     pWssWlanDB->WssWlanAttributeDB.WpaIEElements.
                     u2PairwiseCipherSuiteCount; u1Count++)
                {
                    MEMCPY (&
                            (pWssWlanIfIndexDB->aWpaPwCipherDB[u1Count].
                             au1PairwiseCipher),
                            &(pWssWlanDB->WssWlanAttributeDB.WpaIEElements.
                              aWpaPwCipherDB[u1Count].au1PairwiseCipher),
                            RSNA_CIPHER_SUITE_LEN);
                }

                pWssWlanIfIndexDB->u2AKMSuiteCount =
                    pWssWlanDB->WssWlanAttributeDB.WpaIEElements.
                    u2AKMSuiteCount;

                for (u1Count = 0;
                     u1Count <
                     pWssWlanDB->WssWlanAttributeDB.WpaIEElements.
                     u2AKMSuiteCount; u1Count++)
                {
                    MEMCPY (&
                            (pWssWlanIfIndexDB->aWpaAkmDB[u1Count].
                             au1AKMSuite),
                            &(pWssWlanDB->WssWlanAttributeDB.WpaIEElements.
                              aWpaAkmDB[u1Count].au1AKMSuite),
                            RSNA_AKM_SELECTOR_LEN);
                }
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bRSNAIEElements != OSIX_FALSE)
            {
                /* RSN elements */
                pWssWlanIfIndexDB->u1ElemId =
                    pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.u1ElemId;

                pWssWlanIfIndexDB->u1Len =
                    pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.u1Len;

                pWssWlanIfIndexDB->u2Ver =
                    pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.u2Ver;

                MEMCPY (&(pWssWlanIfIndexDB->au1GroupCipherSuite),
                        &(pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                          au1GroupCipherSuite), RSNA_CIPHER_SUITE_LEN);

                pWssWlanIfIndexDB->u2PairwiseCipherSuiteCount =
                    pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                    u2PairwiseCipherSuiteCount;

                for (u1Count = 0;
                     u1Count <
                     pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                     u2PairwiseCipherSuiteCount; u1Count++)
                {
                    MEMCPY (&
                            (pWssWlanIfIndexDB->aRsnaPwCipherDB[u1Count].
                             au1PairwiseCipher),
                            &(pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                              aRsnaPwCipherDB[u1Count].au1PairwiseCipher),
                            RSNA_CIPHER_SUITE_LEN);
                }

                pWssWlanIfIndexDB->u2AKMSuiteCount =
                    pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                    u2AKMSuiteCount;

                for (u1Count = 0;
                     u1Count <
                     pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                     u2AKMSuiteCount; u1Count++)
                {
                    MEMCPY (&
                            (pWssWlanIfIndexDB->aRsnaAkmDB[u1Count].
                             au1AKMSuite),
                            &(pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                              aRsnaAkmDB[u1Count].au1AKMSuite),
                            RSNA_AKM_SELECTOR_LEN);
                }

                pWssWlanIfIndexDB->u2RsnCapab =
                    pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.u2RsnCapab;

                pWssWlanIfIndexDB->u2PmkidCount =
                    pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.u2PmkidCount;

                MEMCPY (&(pWssWlanIfIndexDB->aPmkidList),
                        &(pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                          aPmkidList), RSNA_PMKID_LEN);
#ifdef PMF_WANTED
                if (((pWssWlanIfIndexDB->u2RsnCapab & RSNA_CAPABILITY_MFPR) ==
                     RSNA_CAPABILITY_MFPR) || ((pWssWlanIfIndexDB->u2RsnCapab &
                                                RSNA_CAPABILITY_MFPC) ==
                                               RSNA_CAPABILITY_MFPC))
                {
                    MEMCPY (&(pWssWlanIfIndexDB->au1GroupMgmtCipherSuite),
                            &(pWssWlanDB->WssWlanAttributeDB.RsnaIEElements.
                              au1GroupMgmtCipherSuite), RSNA_CIPHER_SUITE_LEN);
                }
#endif
            }                    /* if - rsna ie - end */
#ifdef WPS_WANTED
            /* WPS IE Elements -> SET */

            if ((pWssWlanDB->WssWlanIsPresentDB.bWPSEnabled != OSIX_FALSE) ||
                (pWssWlanDB->WssWlanIsPresentDB.bWPSAdditionalIE != OSIX_FALSE))
            {
                pWssWlanIfIndexDB->u1WpsElemId =
                    pWssWlanDB->WssWlanAttributeDB.WpsIEElements.u1ElemId;

                pWssWlanIfIndexDB->u1WpsLen =
                    pWssWlanDB->WssWlanAttributeDB.WpsIEElements.u1Len;

                pWssWlanIfIndexDB->bWpsEnabled =
                    pWssWlanDB->WssWlanAttributeDB.WpsIEElements.bWpsEnabled;

                pWssWlanIfIndexDB->wscState =
                    pWssWlanDB->WssWlanAttributeDB.WpsIEElements.wscState;

                pWssWlanIfIndexDB->bWpsAdditionalIE =
                    pWssWlanDB->WssWlanAttributeDB.WpsIEElements.
                    bWpsAdditionalIE;

                pWssWlanIfIndexDB->noOfAuthMac =
                    pWssWlanDB->WssWlanAttributeDB.WpsIEElements.noOfAuthMac;

                for (u1Count = 0;
                     u1Count <
                     pWssWlanDB->WssWlanAttributeDB.WpsIEElements.
                     noOfAuthMac; u1Count++)
                {
                    MEMCPY (&(pWssWlanIfIndexDB->
                              authorized_macs[u1Count]),
                            (pWssWlanDB->WssWlanAttributeDB.WpsIEElements.
                             authorized_macs[u1Count]), WPS_ETH_ALEN);
                }

                MEMCPY (pWssWlanIfIndexDB->uuid_e,
                        pWssWlanDB->WssWlanAttributeDB.WpsIEElements.uuid_e,
                        WPS_UUID_LEN);

                pWssWlanIfIndexDB->configMethods =
                    pWssWlanDB->WssWlanAttributeDB.WpsIEElements.configMethods;
                MEMCPY (pWssWlanIfIndexDB->au1WtpModelNumber,
                        pWssWlanDB->WssWlanAttributeDB.WpsIEElements.
                        au1WtpModelNumber,
                        STRLEN (pWssWlanDB->WssWlanAttributeDB.WpsIEElements.
                                au1WtpModelNumber));
                MEMCPY (pWssWlanIfIndexDB->au1WtpSerialNumber,
                        pWssWlanDB->WssWlanAttributeDB.WpsIEElements.
                        au1WtpSerialNumber,
                        STRLEN (pWssWlanDB->WssWlanAttributeDB.WpsIEElements.
                                au1WtpSerialNumber));
                MEMCPY (pWssWlanIfIndexDB->au1WtpName,
                        pWssWlanDB->WssWlanAttributeDB.WpsIEElements.au1WtpName,
                        STRLEN (pWssWlanDB->WssWlanAttributeDB.WpsIEElements.
                                au1WtpName));
            }
            /* WPS IE Elements -> End */
#endif
            if (pWssWlanDB->WssWlanIsPresentDB.bWssWlanEdcaParam != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanEdcaParam.u1QosInfo =
                    pWssWlanDB->WssWlanAttributeDB.WssWlanEdcaParam.u1QosInfo;
                if (pWssWlanDB->WssWlanIsPresentDB.bQosProfileId != OSIX_FALSE)
                {
                    switch (pWssWlanDB->WssWlanAttributeDB.u1QosProfileId)
                    {
                        case BEST_EFFORT_ACI:
                            MEMCPY (&(pWssWlanIfIndexDB->WssWlanEdcaParam.
                                      AC_BE_ParameterRecord),
                                    &(pWssWlanDB->WssWlanAttributeDB.
                                      WssWlanEdcaParam.AC_BE_ParameterRecord),
                                    sizeof (pWssWlanDB->WssWlanAttributeDB.
                                            WssWlanEdcaParam.
                                            AC_BE_ParameterRecord));
                            break;
                        case VIDEO_ACI:
                            MEMCPY (&(pWssWlanIfIndexDB->WssWlanEdcaParam.
                                      AC_VI_ParameterRecord),
                                    &(pWssWlanDB->WssWlanAttributeDB.
                                      WssWlanEdcaParam.AC_VI_ParameterRecord),
                                    sizeof (pWssWlanDB->WssWlanAttributeDB.
                                            WssWlanEdcaParam.
                                            AC_VI_ParameterRecord));
                            break;
                        case VOICE_ACI:
                            MEMCPY (&(pWssWlanIfIndexDB->WssWlanEdcaParam.
                                      AC_VO_ParameterRecord),
                                    &(pWssWlanDB->WssWlanAttributeDB.
                                      WssWlanEdcaParam.AC_VO_ParameterRecord),
                                    sizeof (pWssWlanDB->WssWlanAttributeDB.
                                            WssWlanEdcaParam.
                                            AC_VO_ParameterRecord));
                            break;
                        case BACKGROUND_ACI:
                            MEMCPY (&(pWssWlanIfIndexDB->WssWlanEdcaParam.
                                      AC_BK_ParameterRecord),
                                    &(pWssWlanDB->WssWlanAttributeDB.
                                      WssWlanEdcaParam.AC_BK_ParameterRecord),
                                    sizeof (pWssWlanDB->WssWlanAttributeDB.
                                            WssWlanEdcaParam.
                                            AC_BK_ParameterRecord));
                            break;
                        default:
                            WSSIF_TRC (WSSIF_FAILURE_TRC,
                                       "WssIfProcessWssWlanDBMsg:Wlan EDCA Db\
                                    set failed, return failure \r\n");
                            break;

                    }
                }
            }
#ifdef BAND_SELECT_WANTED
            if (pWssWlanDB->WssWlanIsPresentDB.bBandSelectStatus != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1BandSelectStatus =
                    pWssWlanDB->WssWlanAttributeDB.u1BandSelectStatus;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bAgeOutSuppression != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1AgeOutSuppression =
                    pWssWlanDB->WssWlanAttributeDB.u1AgeOutSuppression;
            }

#endif
            break;

        case WSS_WLAN_CREATE_INTERFACE_ENTRY:
            if ((pWssWlanDB->WssWlanAttributeDB.u1RadioId <
                 RADIOIF_START_RADIOID) ||
                (pWssWlanDB->WssWlanAttributeDB.u1RadioId >
                 RADIOIF_END_RADIOID))
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:CREATE: RadioId is invalid;\
                        should be of range (1-31)\
                        Wlan Interface creation failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if ((pWssWlanDB->WssWlanAttributeDB.u1WlanId <
                 WSSWLAN_START_WLANID_PER_RADIO) ||
                (pWssWlanDB->WssWlanAttributeDB.u1WlanId >
                 WSSWLAN_END_WLANID_PER_RADIO))
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:WlanId is invalid \
                        should be of range (1-16) \
                        creation of Wlan Interface failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if ((pWssWlanInterfaceDB =
                 (tWssWlanInterfaceDB *)
                 MemAllocMemBlk (WSSWLAN_INTERFACE_DB_POOLID)) == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:Wlan Interface Memory\
                        allocation failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            MEMSET (pWssWlanInterfaceDB, 0, sizeof (tWssWlanInterfaceDB));
            pWssWlanInterfaceDB->u1RadioId =
                pWssWlanDB->WssWlanAttributeDB.u1RadioId;
            pWssWlanInterfaceDB->u1WlanId =
                pWssWlanDB->WssWlanAttributeDB.u1WlanId;

            WSSIF_TRC2 (WSSIF_MGMT_TRC, "\r\n Creating Wlan Interface with\
                    radioid %d wlanid %d\n", pWssWlanInterfaceDB->u1RadioId, pWssWlanInterfaceDB->u1WlanId);
            if (RBTreeAdd (gWssWlanGlobals.WssWlanGlbMib.WssWlanInterfaceDB,
                           (tRBElem *) pWssWlanInterfaceDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (WSSWLAN_INTERFACE_DB_POOLID,
                                    (UINT1 *) pWssWlanInterfaceDB);
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:RBTreeAdd failed\
                        for Wlan Interface DB\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            RadioIfDB.u4VirtualRadioIfIndex = (UINT4)
                (pWssWlanInterfaceDB->u1RadioId + SYS_DEF_MAX_ENET_INTERFACES);
            pRadioIfDB = RBTreeGet (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                                    (tRBElem *) & RadioIfDB);
            if (pRadioIfDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        Updation of pRadioIfDB failed,\
                        No such RadioIfIndex\r\n");
                return OSIX_FAILURE;
            }

            pRadioIfDB->au4BssIfIndex[pWssWlanDB->WssWlanAttributeDB.u1WlanId -
                                      1] = pWssWlanInterfaceDB->u1WlanId;
            break;

        case WSS_WLAN_DESTROY_INTERFACE_ENTRY:
            if ((pWssWlanDB->WssWlanAttributeDB.u1RadioId <
                 RADIOIF_START_RADIOID) ||
                (pWssWlanDB->WssWlanAttributeDB.u1RadioId >
                 RADIOIF_END_RADIOID))
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:RadioId is invalid;\
                        should be of range (1-31)\
                        Wlan Interface deletion failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if ((pWssWlanDB->WssWlanAttributeDB.u1WlanId <
                 WSSWLAN_START_WLANID_PER_RADIO) ||
                (pWssWlanDB->WssWlanAttributeDB.u1WlanId >
                 WSSWLAN_END_WLANID_PER_RADIO))
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:WlanId is invalid \
                        should be of range (1-16) \
                        deeltion of Wlan Interface failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            WssWlanInterfaceDB.u1RadioId =
                pWssWlanDB->WssWlanAttributeDB.u1RadioId;
            WssWlanInterfaceDB.u1WlanId =
                pWssWlanDB->WssWlanAttributeDB.u1WlanId;

            pWssWlanInterfaceDB =
                RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.WssWlanInterfaceDB,
                           (tRBElem *) & WssWlanInterfaceDB);
            if (pWssWlanInterfaceDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:RBTreeGet failed from\
                        WlanInterface, return failure \r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;

            }
            RBTreeRem (gWssWlanGlobals.WssWlanGlbMib.WssWlanInterfaceDB,
                       &WssWlanInterfaceDB);
            MemReleaseMemBlock (WSSWLAN_INTERFACE_DB_POOLID,
                                (UINT1 *) pWssWlanInterfaceDB);
            WSSIF_TRC2 (WSSIF_MGMT_TRC,
                        "WssIfProcessWssWlanDBMsg:Deletion success for\
                    Wlan Interface RadioId %d WlanId %d\r\n", pWssWlanInterfaceDB->u1RadioId, pWssWlanInterfaceDB->u1WlanId);

            break;
        case WSS_WLAN_GET_INTERFACE_ENTRY:
            MEMSET (&WssWlanInterfaceDB, 0, sizeof (tWssWlanInterfaceDB));
            if ((pWssWlanDB->WssWlanAttributeDB.u1RadioId <
                 RADIOIF_START_RADIOID) ||
                (pWssWlanDB->WssWlanAttributeDB.u1RadioId >
                 RADIOIF_END_RADIOID))
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:RadioId is invalid;\
                        should be of range (1-31)\
                        Wlan Interface deletion failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if ((pWssWlanDB->WssWlanAttributeDB.u1WlanId <
                 WSSWLAN_START_WLANID_PER_RADIO) ||
                (pWssWlanDB->WssWlanAttributeDB.u1WlanId >
                 WSSWLAN_END_WLANID_PER_RADIO))
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:WlanId is invalid \
                        should be of range (1-16) \
                        deeltion of Wlan Interface failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            WssWlanInterfaceDB.u1WlanId =
                pWssWlanDB->WssWlanAttributeDB.u1WlanId;
            WssWlanInterfaceDB.u1RadioId =
                pWssWlanDB->WssWlanAttributeDB.u1RadioId;

            pWssWlanInterfaceDB =
                RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.WssWlanInterfaceDB,
                           (tRBElem *) & WssWlanInterfaceDB);
            if (pWssWlanInterfaceDB == NULL)
            {
                WSSIF_TRC2 (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        RBTreeGet from WlanIfIndexDB failed, \
                        for u1WlanId %d , u1RadioId %d\r\n", WssWlanInterfaceDB.u1WlanId, WssWlanInterfaceDB.u1RadioId);
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bRowStatus != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1RowStatus =
                    pWssWlanInterfaceDB->u1RowStatus;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bInterfaceName != OSIX_FALSE)
            {
                MEMCPY (pWssWlanDB->WssWlanAttributeDB.au1InterfaceName,
                        pWssWlanInterfaceDB->au1InterfaceName, 24);
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex =
                    pWssWlanInterfaceDB->pWssWlanIfIndexDB->u4WlanIfIndex;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bBssId != OSIX_FALSE)
            {
                if (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex != 0)
                {
                    WssWlanIfIndexDB.u4WlanIfIndex =
                        pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;

                    pWssWlanIfIndexDB =
                        RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.
                                   WssWlanIfIndexDB,
                                   (tRBElem *) & WssWlanIfIndexDB);

                    if (pWssWlanIfIndexDB == NULL)
                    {

                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "WssIfProcessWssWlanDBMsg:\
                                RBTreeGet from WssWlanIfIndexDB returned\
                                NULL\r\n");
                        return OSIX_FAILURE;
                    }

                    MEMCPY ((pWssWlanDB->WssWlanAttributeDB.BssId),
                            (pWssWlanIfIndexDB->BssId), MAC_ADDR_LEN);
                }
                else
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                            No Entry found , WlanIfIndex is empty\r\n");
                    return OSIX_FAILURE;
                }
            }
            break;
        case WSS_WLAN_SET_INTERFACE_ENTRY:
            MEMSET (&WssWlanInterfaceDB, 0, sizeof (tWssWlanInterfaceDB));
            if ((pWssWlanDB->WssWlanAttributeDB.u1RadioId <
                 RADIOIF_START_RADIOID) ||
                (pWssWlanDB->WssWlanAttributeDB.u1RadioId >
                 RADIOIF_END_RADIOID))
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:CREATE: RadioId is invalid;\
                        should be of range (1-31)\
                        Wlan Interface creation failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if ((pWssWlanDB->WssWlanAttributeDB.u1WlanId <
                 WSSWLAN_START_WLANID_PER_RADIO) ||
                (pWssWlanDB->WssWlanAttributeDB.u1WlanId >
                 WSSWLAN_END_WLANID_PER_RADIO))
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:WlanId is invalid \
                        should be of range (1-16) \
                        creation of Wlan Interface failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            WssWlanInterfaceDB.u1WlanId =
                pWssWlanDB->WssWlanAttributeDB.u1WlanId;
            WssWlanInterfaceDB.u1RadioId =
                pWssWlanDB->WssWlanAttributeDB.u1RadioId;

            pWssWlanInterfaceDB =
                RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.WssWlanInterfaceDB,
                           (tRBElem *) & WssWlanInterfaceDB);
            if (pWssWlanInterfaceDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        RBTreeGet from WlanInterfaceDB failed \r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bRowStatus != OSIX_FALSE)
            {
                pWssWlanInterfaceDB->u1RowStatus =
                    pWssWlanDB->WssWlanAttributeDB.u1RowStatus;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bInterfaceName != OSIX_FALSE)
            {
                MEMCPY (pWssWlanInterfaceDB->au1InterfaceName,
                        pWssWlanDB->WssWlanAttributeDB.au1InterfaceName, 24);

            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex != OSIX_FALSE)
            {
                if (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex != 0)
                {
                    WssWlanIfIndexDB.u4WlanIfIndex =
                        pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
                    pWssWlanIfIndexDB =
                        RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.
                                   WssWlanIfIndexDB,
                                   (tRBElem *) & WssWlanIfIndexDB);
                    if (pWssWlanIfIndexDB == NULL)
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "WssIfProcessWssWlanDBMsg: RBTreeGet\
                                from WlanIfIndexDB failed \r\n");
                        WSSIF_UNLOCK;
                        return OSIX_FAILURE;
                    }

                }
                else
                {
                    pWssWlanIfIndexDB = NULL;
                }

                pWssWlanInterfaceDB->pWssWlanIfIndexDB = pWssWlanIfIndexDB;
            }
            break;

        case WSS_WLAN_CREATE_BSSID_MAPPING_ENTRY:
            MEMSET (&BssId, 0, sizeof (BssId));
            if (MEMCMP (pWssWlanDB->WssWlanAttributeDB.BssId, BssId,
                        sizeof (BssId)) == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:BSSID is empty , BssId "
                           "mapping entry creation failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            if (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:WlanIfIndex empty, BSSID\
                        mapping entry creation failed\r\n");

            }
            if ((pWssWlanBSSIDMappingDB =
                 (tWssWlanBSSIDMappingDB *)
                 MemAllocMemBlk (WSSWLAN_BSSID_MAPPING_DB_POOLID)) == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        Memory allocation failed for BssIdMappingDB\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            MEMSET (pWssWlanBSSIDMappingDB, 0, sizeof (tWssWlanBSSIDMappingDB));
            MEMCPY (pWssWlanBSSIDMappingDB->BssId,
                    pWssWlanDB->WssWlanAttributeDB.BssId, MAC_ADDR_LEN);

            if (RBTreeAdd
                (gWssWlanGlobals.WssWlanGlbMib.WssWlanBSSIDMappingDB,
                 (tRBElem *) pWssWlanBSSIDMappingDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (WSSWLAN_BSSID_MAPPING_DB_POOLID,
                                    (UINT1 *) pWssWlanBSSIDMappingDB);
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        BSSID Mapping DB RBTreeAdd failed \r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            pWssWlanBSSIDMappingDB->u4WlanIfIndex =
                pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;

            MEMCPY (pWssWlanBSSIDMappingDB->au1WlanIntfName,
                    pWssWlanDB->WssWlanAttributeDB.au1WlanIntfName,
                    WSS_WLAN_NAME_MAX_LEN);

            break;
        case WSS_WLAN_DESTROY_BSSID_MAPPING_ENTRY:
            MEMSET (&BssId, 0, sizeof (BssId));
            if (MEMCMP (pWssWlanDB->WssWlanAttributeDB.BssId, BssId,
                        sizeof (BssId)) == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:BSSID is empty , "
                           "BssIdMappingDB deletion failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            MEMCPY (WssWlanBSSIDMappingDB.BssId,
                    pWssWlanDB->WssWlanAttributeDB.BssId, MAC_ADDR_LEN);
            pWssWlanBSSIDMappingDB =
                RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.WssWlanBSSIDMappingDB,
                           (tRBElem *) & WssWlanBSSIDMappingDB);

            if (pWssWlanBSSIDMappingDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:WssWlanBSSIDMappingDB\
                        RBTreeGet failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;

            }
            RBTreeRem (gWssWlanGlobals.WssWlanGlbMib.WssWlanBSSIDMappingDB,
                       &WssWlanBSSIDMappingDB);
            MemReleaseMemBlock (WSSWLAN_BSSID_MAPPING_DB_POOLID,
                                (UINT1 *) pWssWlanBSSIDMappingDB);

            break;
        case WSS_WLAN_GET_BSSID_MAPPING_ENTRY:
            MEMSET (&WssWlanBSSIDMappingDB, 0, sizeof (tWssWlanBSSIDMappingDB));

            if ((MEMCMP (pWssWlanDB->WssWlanAttributeDB.BssId,
                         "\0\0\0\0\0\0", sizeof (tMacAddr))) != 0)
            {
                MEMCPY (WssWlanBSSIDMappingDB.BssId,
                        pWssWlanDB->WssWlanAttributeDB.BssId, MAC_ADDR_LEN);

                pWssWlanBSSIDMappingDB =
                    RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.
                               WssWlanBSSIDMappingDB,
                               (tRBElem *) & WssWlanBSSIDMappingDB);

                if (pWssWlanBSSIDMappingDB == NULL)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC,
                               "WssIfProcessWssWlanDBMsg:RBTreeGet from \
                            WlanBSSIDMappingDB failed\r\n");
                    WSSIF_UNLOCK;
                    return OSIX_FAILURE;
                }

            }
            else
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        Key value BSSID is not set\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex =
                    pWssWlanBSSIDMappingDB->u4WlanIfIndex;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanIntfName != OSIX_FALSE)
            {
                MEMCPY (pWssWlanDB->WssWlanAttributeDB.au1WlanIntfName,
                        pWssWlanBSSIDMappingDB->au1WlanIntfName,
                        WSS_WLAN_NAME_MAX_LEN);
            }
            break;

        case WSS_WLAN_SET_MANAGMENT_SSID:
            if (pWssWlanDB->WssWlanAttributeDB.au1ManagmentSSID[0] != '\0')
            {
                MEMCPY (gWssWlanGlobals.WssWlanGlbMib.au1ManagmentSSID,
                        pWssWlanDB->WssWlanAttributeDB.au1ManagmentSSID,
                        WSSWLAN_SSID_NAME_LEN);
            }
            else
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "\nWssIfProcessWssWlanDBMsg:"
                           "ManagmentSSID Entry not valid \r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            break;
        case WSS_WLAN_GET_MANAGMENT_SSID:
            if (gWssWlanGlobals.WssWlanGlbMib.au1ManagmentSSID[0] == '\0')
            {
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            MEMCPY (pWssWlanDB->WssWlanAttributeDB.au1ManagmentSSID,
                    gWssWlanGlobals.WssWlanGlbMib.au1ManagmentSSID,
                    WSSWLAN_SSID_NAME_LEN);

            break;

        default:
            WSSIF_TRC (WSSIF_FAILURE_TRC, "\n WssIfProcessWssWlanDBMsg:\
                    Invalid Opcode\r\n");
            WSSIF_UNLOCK;
            return OSIX_FAILURE;
            break;

    }
    WSSIF_UNLOCK;
    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

#endif
