/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 *   $Id: wssifwlan.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $                                                                           *
 *  Description: This file contains the WSSWLAN AP module APIs in WTP         *
 *                                                                            *
 ******************************************************************************/

#ifndef __WSSIF_WLAN_C__
#define __WSSIF_WLAN_C__

#include "wssifinc.h"
#ifdef WLC_WANTED
#include "wsswlanwlcproto.h"
#else
#include "wsswlanwtpproto.h"
#endif

/*******************************************************************************
 * *                                                                           *
 * * Function     : WssIfProcessWssWlanMsg                                     *
 * *                                                                           *
 * * Description  : Calls WLAN module to process message                       *
 * *                                                                           *
 * * Input        : None                                                       *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                OSIX_FAILURE, otherwise                                    *
 * *                                                                           *
 * ****************************************************************************/

UINT1
WssIfProcessWssWlanMsg(UINT4 eMsgType, tWssWlanMsgStruct *pWssWlanMsgStruct)
{
    if(WssWlanProcessWssIfMsg((UINT1)eMsgType , pWssWlanMsgStruct) != OSIX_SUCCESS)
    {
        WSSIF_TRC(WSSIF_FAILURE_TRC , "WssIfProcessWssWlanMsg: \
                WssWlanProcessWssIfMsg returns failure\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

#endif
