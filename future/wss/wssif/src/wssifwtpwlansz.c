/******************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                       *
 *  $Id: wssifwtpwlansz.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $                                                                            *
 * Description: This file contains the MEMPOOL related API for WSSSTA MODULE  *
 ******************************************************************************/

#define _WSSIFWTPWLANSZ_C
#include "wssifinc.h"
extern INT4  IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams); 
extern INT4  IssSzRegisterModulePoolId( CHR1 *pu1ModName, tMemPoolId *pModPoolId); 
INT4  WssifwtpwlanSizingMemCreateMemPools()
{
    INT4 i4RetVal;
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < WSSIFWTPWLAN_MAX_SIZING_ID; i4SizingId++) {
        i4RetVal = MemCreateMemPool( 
                          FsWSSIFWTPWLANSizingParams[i4SizingId].u4StructSize,
                          FsWSSIFWTPWLANSizingParams[i4SizingId].u4PreAllocatedUnits,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(WSSIFWTPWLANMemPoolIds[ i4SizingId]));
        if( i4RetVal == (INT4) MEM_FAILURE) {
            WssifwtpwlanSizingMemDeleteMemPools();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4   WssifwtpwlanSzRegisterModuleSizingParams( CHR1 *pu1ModName)
{
      /* Copy the Module Name */ 
       IssSzRegisterModuleSizingParams( pu1ModName, FsWSSIFWTPWLANSizingParams); 
      IssSzRegisterModulePoolId( pu1ModName, WSSIFWTPWLANMemPoolIds); 
      return OSIX_SUCCESS; 
}


VOID  WssifwtpwlanSizingMemDeleteMemPools()
{
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < WSSIFWTPWLAN_MAX_SIZING_ID; i4SizingId++) {
        if(WSSIFWTPWLANMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool( WSSIFWTPWLANMemPoolIds[ i4SizingId] );
            WSSIFWTPWLANMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
