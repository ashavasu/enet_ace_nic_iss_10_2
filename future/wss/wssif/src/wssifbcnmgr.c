/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                     *
 *  $Id: wssifbcnmgr.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $                                                                           *
 *  Description: This file contains the WSS MAC module APIs.                 *
 *                                                                           *
 *****************************************************************************/

#ifndef _WSSIF_BCNMGR_C_
#define _WSSIF_BCNMGR_C_

#include "wssifinc.h"

/****************************************************************************
 * *                                                                        *
 * * Function     : WssIfProcessBcnMgrMsg                                   *
 * *                                                                        *
 * * Description  : Calls BCNMGR module API to process fn calls from other *
 * *                WSS modules.                                            *
 * *                                                                        *
 * * Input        : eMsgType - Msg opcode                                   *
 * *                pBcnMgrMsgStruct - Pointer to WSS MAC msg struct        *
 * *                                                                        *
 * * Output       : None                                                    *
 * *                                                                        *
 * * Returns      : OSIX_SUCCESS on success                                 *
 * *                OSIX_FAILURE otherwise                                  *
 * *                                                                        *
 * **************************************************************************/

UINT1
WssIfProcessBcnMgrMsg(UINT4 eMsgType, tBcnMgrMsgStruct *pBcnMgrMsgStruct)
{
    if (BcnMgrProcessWssIfMsg (eMsgType, pBcnMgrMsgStruct) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

#endif
