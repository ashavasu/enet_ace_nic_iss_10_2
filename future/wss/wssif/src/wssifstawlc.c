/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                     *
 *  $Id: wssifstawlc.c,v 1.3 2017/11/24 10:37:10 siva Exp $
 *  Description: This file contains the station update related api           *
 *                                                                           *
 *****************************************************************************/
#ifndef __WSSIFSTA__C
#define __WSSIFSTA__C
#include "wssstawlcprot.h"
#include "wssifinc.h"
#include "wsspmdb.h"

#include "wssstawlcinc.h"
#include "capwapglob.h"
#ifdef KERNEL_CAPWAP_WANTED
#include "fcusprot.h"
#include "fcusglob.h"
#endif
#define WEBAUTH_DEFAULT_PASSWORD "Password123#"

tRBTree             gWssStaWebAuthDB;
static tRBTree      gWssStaWebAuthTempDB;
tWssStaWebAuthDBWalk gaWssStaWebAuthDB[MAX_WSSIFWEBAUTH_STATION];
static INT1         gi1DBWalkCnt;

INT4                gu4StaAuthSessionIndex = 0;
tWssStaAuthSessionDBWalk gaWssStaAuthSessionDB[MAX_STA_SUPP_PER_WLC];

INT1                gUserCnt = 0;
extern INT4         pmWTPInfoNotify (UINT1, tWssPMInfo *);
extern tWssClientSummary gaWssClientSummary[MAX_STA_SUPP_PER_WLC];
extern UINT4        gu4ClientWalkIndex;
tWssStaClientWalk   gClient;
extern tRBTree      gWssStaWepProcessDB;

extern UINT4        gu4WssStaWebAuthDebugMask;
extern INT4
 
 
 
  CapwapKernelStaTableUpdate (tMacAddr staMac, tWssIfAuthStateDB * pStaStateDB);

/*****************************************************************************
 *                                                                           *
 * Function     : WssIfProcessWssStaMsg                                     *
 *                                                                           *
 * Description  : This function will invoke the functions of WSS STA module *
 *                                                                           *
 * Input        : OpCode - Opcode value indicating the type of operation to  *
 *                be executed                                                *
 *                pWssMsgStruct - Pointer to the WSS STA process             *
 *                Request struct                                             *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                          *
 *****************************************************************************/

/* Sends Request to the WssSta Module */
INT4
WssIfProcessWssStaMsg (UINT4 WssMsgType, tWssStaMsgStruct * pWssStaMsgStruct)
{
    if (WssStaProcessWssIfMsg (WssMsgType, pWssStaMsgStruct) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *  Function    :  WssIfProcessWssStaDBMsg
 *  Input       :  u1Opcode - The Code based on which the Db will be updated
 *                 tWssMsgDBStruct - The message DB structure.
 *  Description :  Invoke the correponding DB Table to set/get DB information
 *                 CapwapDot11WlanBindTable table
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
UINT4
WssIfProcessWssAuthDBMsg (UINT1 u1OpCode, tWssifauthDBMsgStruct * pWssStaMsg)
{

    tWssIfAuthStateDB   staDB;
    tWssIfAuthStateDB  *pStaDB = NULL;
    tWssIfWebAuthDB     WebAuthDB;
    tWssIfWebAuthDB    *pWebAuthDB = NULL;
    INT1                i1index = 0;
    UINT4               u4index = 0;
    UINT1               u1Loop = 0;
    tWssIfBssIdStatsDB *pBssIdStnStatsEntry = NULL;
    UINT4               u4BssIfIndex = 0;
    UINT4               u4CurrentTime = 0;
    tWssWlanDB         *pWssWlanDB = NULL;
    tWssMacMsgStruct   *pWssDisassocMacFrame = NULL;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssWlanDB          wssWlanDB;
    UINT4               u4RadioIndex = 0;
    tRadioIfGetDB       RadioIfGetDB;

    switch (u1OpCode)
    {
        case WSS_AUTH_UPDATE_DB:
            WssAuthUpdateDB (pWssStaMsg->WssStaDB.action,
                             pWssStaMsg->WssStaDB.u4BssIfIndex,
                             pWssStaMsg->WssStaDB.staMacAddr,
                             pWssStaMsg->WssStaDB.u2Aid,
                             pWssStaMsg->WssStaDB.u2SessId,
                             &(pWssStaMsg->WssStaDB.WssStaParams));
            break;
        case WSS_AUTH_DB_UPDATE_RULENUM:
            /* The Station DB is update with rule number added
             * after WEB Authentication */
            MEMSET (&staDB, 0, sizeof (tWssIfAuthStateDB));
            MEMCPY (staDB.stationMacAddress,
                    pWssStaMsg->WssIfAuthStateDB.stationMacAddress,
                    sizeof (tMacAddr));
            pStaDB = ((tWssIfAuthStateDB *) RBTreeGet (gWssStaStateDB,
                                                       (tRBElem *) & staDB));
            if (pStaDB == NULL)
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "ASSOC Case - Entry is not present in the "
                            "Station DB \r\n");
                return OSIX_FAILURE;
            }
            if (pWssStaMsg->WssIfAuthStateDB.bStaRedirectionURL == OSIX_TRUE)
            {
                MEMSET (pStaDB->au1StaRedirectionURL, 0, 100);
                MEMCPY (pStaDB->au1StaRedirectionURL,
                        pWssStaMsg->WssIfAuthStateDB.au1StaRedirectionURL,
                        STRLEN (pWssStaMsg->WssIfAuthStateDB.
                                au1StaRedirectionURL));
            }
            if (pWssStaMsg->WssIfAuthStateDB.bStaAuthRedirected == OSIX_TRUE)
            {
                pStaDB->u1StaAuthRedirected = pWssStaMsg->WssIfAuthStateDB.
                    u1StaAuthRedirected;
                return OSIX_SUCCESS;
            }
            pStaDB->u4RuleNumber = pWssStaMsg->WssIfAuthStateDB.u4RuleNumber;
            pStaDB->u1StaAuthFinished = pWssStaMsg->WssIfAuthStateDB.
                u1StaAuthFinished;
            pStaDB->u4LastUpdatedTime = pWssStaMsg->WssIfAuthStateDB.
                u4LastUpdatedTime;
            MEMCPY (pStaDB->au1LoggedUserName,
                    pWssStaMsg->WssIfAuthStateDB.au1LoggedUserName,
                    WSS_STA_WEBUSER_LEN_MAX);
            break;
        case WSS_STA_WEBAUTH_UPDATE_LIFESPAN:
            WssStaShowClient ();
            for (u4index = 0; u4index < gu4ClientWalkIndex; u4index++)
            {
                if (gaWssClientSummary[u4index].stationState == AUTH_ASSOC)
                {

                    MEMSET (&staDB, 0, sizeof (tWssIfAuthStateDB));
                    MEMCPY (staDB.stationMacAddress,
                            gaWssClientSummary[u4index].staMacAddr,
                            sizeof (tMacAddr));
                    pStaDB = ((tWssIfAuthStateDB *) RBTreeGet (gWssStaStateDB,
                                                               (tRBElem *) &
                                                               staDB));
                    if (pStaDB == NULL)
                    {
                        continue;
                    }

                    if (pStaDB->u1StaAuthFinished == OSIX_TRUE)
                    {
                        /* Get the lifetime of that user */
                        MEMSET (&WebAuthDB, 0, sizeof (tWssIfWebAuthDB));
                        MEMCPY (WebAuthDB.au1UserName,
                                pStaDB->au1LoggedUserName,
                                STRLEN (pStaDB->au1LoggedUserName));
                        pWebAuthDB = ((tWssIfWebAuthDB *)
                                      RBTreeGet (gWssStaWebAuthDB,
                                                 (tRBElem *) & WebAuthDB));
                        if (pWebAuthDB != NULL)
                        {

                            if (pWebAuthDB->u4MaxUserLifetime != 0)
                            {
                                u4CurrentTime = OsixGetSysUpTime ();
                                if ((u4CurrentTime -
                                     pStaDB->u4LastUpdatedTime) >=
                                    (pWebAuthDB->u4MaxUserLifetime *
                                     WSSSTA_TIME_IN_MINS))
                                {
                                    pStaDB->u1StaAuthFinished = OSIX_FALSE;
                                    pStaDB->u1StaAuthRedirected = OSIX_FALSE;
#ifdef KERNEL_CAPWAP_WANTED
                                    /* Update the Kernel module */
                                    if (CapwapUpdateKernelUsrDeletionStaTable
                                        (pStaDB->stationMacAddress) !=
                                        OSIX_SUCCESS)
                                    {
                                        continue;
                                    }
#endif
                                    if (CapwapSendWebAuthStatus
                                        (pStaDB->stationMacAddress,
                                         pStaDB->u1StaAuthFinished,
                                         pStaDB->aWssStaBSS[0].u2SessId) !=
                                        OSIX_SUCCESS)
                                    {
                                        continue;
                                    }

                                }
                            }
                        }
                    }

                }
            }

            /* Remove entry added with web auth timer if timer is expired and
             * the station is in not-connected state as well connected */
            RBTreeWalk (gWssStaAuthSessionDB,
                        (tRBWalkFn) WebAuthSessionDBWalkFn, 0, 0);
            WssStaDelEntryFromAuthSessionDb ();

            break;
        case WSS_STA_WEBAUTH_INIT_DB:
            if (WssifwebauthSizingMemCreateMemPools () == OSIX_FAILURE)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WssifwebauthSizingMemCreateMemPools: "
                            "Mem Pool Creation Failed \r\n");
                return OSIX_FAILURE;
            }

            if (WssIfWebAuthDBCreate () == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, " WssIfWebAuthDBCreate Returned "
                           "Failure \r\n");
                return OSIX_FAILURE;
            }
            if (WssIfWebAuthTempDBCreate () == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfWebAuthTempDBCreate "
                           "Returned Failure \r\n");
                return OSIX_FAILURE;
            }
            break;
        case WSS_STA_WEB_AUTH_DB_WALK:
            /* Do a RB tree Walk of WebAuth DB */
            RBTreeWalk (gWssStaWebAuthDB, (tRBWalkFn) WssStaWebAuthDBWalkFn, 0,
                        0);
            /* Delete the entries from Web Auth DB */
            WssStaDelEntryFromWebAuthDb ();
            break;
        case WSS_STA_WEBAUTH_SET_DB:
            break;
        case WSS_STA_WEBAUTH_GET_USER:
            /* This opcode is used when an user name is submitted for 
             * validation.
             * Do the validation only if the username is present 
             * in the Web Auth RB Tree   */
            MEMSET (&WebAuthDB, 0, sizeof (tWssIfWebAuthDB));
            MEMCPY (WebAuthDB.au1UserName,
                    pWssStaMsg->WssStaWebAuthDB.au1UserName,
                    STRLEN (pWssStaMsg->WssStaWebAuthDB.au1UserName));
            pWebAuthDB = ((tWssIfWebAuthDB *)
                          RBTreeGet (gWssStaWebAuthDB,
                                     (tRBElem *) & WebAuthDB));
            if (pWebAuthDB == NULL)
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "Entry is not present in Web Auth DB \r\n");
                return OSIX_FAILURE;
            }
            pWssStaMsg->WssStaWebAuthDB.u4WlanId = pWebAuthDB->u4WlanId;
            pWssStaMsg->WssStaWebAuthDB.u4MaxUserLifetime =
                pWebAuthDB->u4MaxUserLifetime;
            MEMCPY (pWssStaMsg->WssStaWebAuthDB.au1UserEmailId,
                    pWebAuthDB->au1UserEmailId,
                    sizeof (pWebAuthDB->au1UserEmailId));
            pWssStaMsg->WssStaWebAuthDB.u1RowStatus = pWebAuthDB->u1RowStatus;
            MEMCPY (pWssStaMsg->WssStaWebAuthDB.au1UserPwd,
                    pWebAuthDB->au1UserPwd, sizeof (pWebAuthDB->au1UserPwd));

            break;
        case WSS_STA_WEBAUTH_AUTHENTICATE_USER:
            /* Get the RB Tree entry and update the user's state */
            MEMSET (&WebAuthDB, 0, sizeof (tWssIfWebAuthDB));
            MEMCPY (WebAuthDB.au1UserName,
                    pWssStaMsg->WssStaWebAuthDB.au1UserName,
                    STRLEN (pWssStaMsg->WssStaWebAuthDB.au1UserName));
            pWebAuthDB = ((tWssIfWebAuthDB *)
                          RBTreeGet (gWssStaWebAuthDB,
                                     (tRBElem *) & WebAuthDB));
            if (pWebAuthDB == NULL)
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "Entry is not present in Web Auth DB \r\n");
                return OSIX_FAILURE;
            }
            /* if the Wlan ID matches with Wlan ID of the received station, then
             * change the state to authenticated */
            if (pWebAuthDB->u1RowStatus != ACTIVE)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "\r\nTrying to authenticate using inactive"
                            "username");
                return OSIX_FAILURE;
            }
            if (pWebAuthDB->u4WlanId == pWssStaMsg->WssStaWebAuthDB.u4WlanId)
            {
                if (MEMCMP (pWssStaMsg->WssStaWebAuthDB.au1UserPwd,
                            pWebAuthDB->au1UserPwd,
                            WSS_STA_WEBUSER_LEN_MAX) == 0)
                {

                    pWebAuthDB->u2AuthState = WSSSTA_WEB_USER_AUTHENTICATED;
                    return OSIX_SUCCESS;
                }
                else
                {

                    WSSSTA_TRC (WSSSTA_FAILURE_TRC, "Incorrect password\r\n");
                }
            }
            else
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "No SSID to Username exists\r\n");
            }
            return OSIX_FAILURE;
            break;
        case WSS_AUTH_SET_DB:
            break;
        case WSS_AUTH_SET_ASSOC_REJECT_COUNT_DB:
            WssStaSetLdDB (&(pWssStaMsg->WssStaLoadBalanceDB));
            break;
        case WSS_AUTH_SET_WTP_ASSOC_COUNT_DB:
            WssStaSetAssocCount (&(pWssStaMsg->WssStaWtpAssocCountInfo));
            break;
        case WSS_AUTH_GET_STATION_DB:
            MEMSET (&staDB, 0, sizeof (tWssIfAuthStateDB));
            MEMCPY (staDB.stationMacAddress,
                    pWssStaMsg->WssIfAuthStateDB.stationMacAddress,
                    sizeof (tMacAddr));

            pStaDB = ((tWssIfAuthStateDB *) RBTreeGet (gWssStaStateDB,
                                                       (tRBElem *) & staDB));
            if (pStaDB == NULL)
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "Entry is not present in Station DB \r\n");
                return OSIX_FAILURE;
            }
            pWssStaMsg->WssIfAuthStateDB.u1Index = pStaDB->u1Index;
            pWssStaMsg->WssIfAuthStateDB.u1StaAuthFinished =
                pStaDB->u1StaAuthFinished;
            pWssStaMsg->WssIfAuthStateDB.u1StaAuthRedirected =
                pStaDB->u1StaAuthRedirected;
            pWssStaMsg->WssIfAuthStateDB.u4LastUpdatedTime =
                pStaDB->u4LastUpdatedTime;
            MEMSET (pWssStaMsg->WssIfAuthStateDB.au1StaRedirectionURL,
                    0,
                    sizeof (pWssStaMsg->WssIfAuthStateDB.au1StaRedirectionURL));
            MEMCPY (pWssStaMsg->WssIfAuthStateDB.au1StaRedirectionURL,
                    pStaDB->au1StaRedirectionURL,
                    STRLEN (pStaDB->au1StaRedirectionURL));

            i1index = 0;
            if (pStaDB->aWssStaBSS[i1index].stationState == AUTH_ASSOC)
            {

                pWssStaMsg->WssIfAuthStateDB.u4StaConnTime =
                    pStaDB->u4StaConnTime;
                MEMCPY (pWssStaMsg->WssIfAuthStateDB.au1StationAddedTime,
                        pStaDB->au1StationAddedTime,
                        sizeof (pStaDB->au1StationAddedTime));
                pWssStaMsg->WssIfAuthStateDB.u1Index = 0;
                pWssStaMsg->WssIfAuthStateDB.u2AssociationID =
                    pStaDB->u2AssociationID;
                pWssStaMsg->WssIfAuthStateDB.aWssStaBSS[i1index].u4BssIfIndex =
                    pStaDB->aWssStaBSS[i1index].u4BssIfIndex;
                pWssStaMsg->WssIfAuthStateDB.aWssStaBSS[i1index].stationState =
                    pStaDB->aWssStaBSS[i1index].stationState;
                pWssStaMsg->WssIfAuthStateDB.aWssStaBSS[i1index].u2SessId =
                    pStaDB->aWssStaBSS[i1index].u2SessId;
                MEMCPY (pWssStaMsg->WssIfAuthStateDB.
                        aWssStaBSS[i1index].staMacAddr,
                        pWssStaMsg->WssIfAuthStateDB.stationMacAddress,
                        sizeof (tMacAddr));
                return OSIX_SUCCESS;
            }
            break;
        case WSS_AUTH_GET_WTP_ASSOC_COUNT_DB:
            WssStaAssocCntDB (&(pWssStaMsg->WssStaWtpAssocCountInfo));
            break;
        case WSS_AUTH_GET_STA_ASSOC_REJECT_COUNT_DB:
            WssStaUpdateLdDB (&(pWssStaMsg->WssStaLoadBalanceDB));
            break;
        case WSS_AUTH_DELETE_DB:
            MEMSET (&staDB, 0, sizeof (tWssIfAuthStateDB));
            MEMCPY (staDB.stationMacAddress,
                    pWssStaMsg->WssIfAuthStateDB.stationMacAddress,
                    sizeof (tMacAddr));
            pStaDB = ((tWssIfAuthStateDB *) RBTreeGet (gWssStaStateDB,
                                                       (tRBElem *) & staDB));
            if (pStaDB == NULL)
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "Entry is not present in Station DB \r\n");
                return OSIX_FAILURE;
            }
            if (pStaDB->u1Index == 1)
            {
                RBTreeRem (gWssStaStateDB, (tRBElem *) pStaDB);
                MemReleaseMemBlock (WSSIFAUTH_STADB_POOLID, (UINT1 *) pStaDB);
                pWssStaWepProcessDB =
                    WssStaProcessEntryGet (staDB.stationMacAddress);
                if (pWssStaWepProcessDB != NULL)
                {
                    RBTreeRem (gWssStaWepProcessDB,
                               (tRBElem *) pWssStaWepProcessDB);
                    MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                        (UINT1 *) pWssStaWepProcessDB);
                }
                break;
            }
            i1index = (INT1) pStaDB->u1Index;
            if (i1index == WSSAUTH_MAX_BSS)
            {
                i1index--;
            }
            /* Kloc Fix Start */
            if (i1index > WSSAUTH_MAX_BSS)
            {
                return OSIX_FAILURE;
            }
            /* Kloc Fix Ends */
            for (; i1index >= 0; i1index--)
            {
                if (pWssStaMsg->WssIfAuthStateDB.aWssStaBSS[0].u4BssIfIndex ==
                    pStaDB->aWssStaBSS[i1index].u4BssIfIndex)
                {
                    pStaDB->aWssStaBSS[i1index].stationState = UNAUTH_UNASSOC;
                    pStaDB->aWssStaBSS[i1index].isPresent = FALSE;
                    pStaDB->aWssStaBSS[i1index].u4BssIfIndex = 0;
                    pStaDB->u2AssociationID = 0;
                    break;
                }
            }
            break;
        case WSS_AUTH_GET_BSSSID_STATION_STATS:
            u4BssIfIndex = pWssStaMsg->WssBssStnStatsDB.u4BssIfIndex;
            pBssIdStnStatsEntry =
                (tWssIfBssIdStatsDB *)
                WssIfGetBssIdStnStatsEntry (u4BssIfIndex);
            if (pBssIdStnStatsEntry != NULL)
            {
                MEMCPY (&pWssStaMsg->WssBssStnStatsDB.bssIdStnStats,
                        &pBssIdStnStatsEntry->bssIdStnStats,
                        sizeof (tBSSIDStnStat));
            }
            else
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "Entry is not present in BssIDStation DB \r\n");
                return OSIX_FAILURE;

            }

            break;
        case WSS_AUTH_ADD_BSSSID_STATION_STATS:
            u4BssIfIndex = pWssStaMsg->WssBssStnStatsDB.u4BssIfIndex;
            WssIfUpdateBSSIDStnStatsDB (u4BssIfIndex);
            break;
        case WSS_AUTH_DEL_BSSSID_STATION_STATS:
            u4BssIfIndex = pWssStaMsg->WssBssStnStatsDB.u4BssIfIndex;
            WssIfDelBssIdStnStatsEntry (u4BssIfIndex);
            break;
        case WSS_STA_GET_CLIENT_COUNT_PER_BSSSID:
            u4BssIfIndex = pWssStaMsg->WssBssStnCountDB.u4BssIfIndex;
            WssStaGetClientPerRadio (u4BssIfIndex,
                                     &pWssStaMsg->WssBssStnCountDB.
                                     u1ClientCount);
            break;
        case WSS_STA_GET_CLIENT_MAC_PER_BSSID:
            u4BssIfIndex = pWssStaMsg->WssStaDB.u4BssIfIndex;
            WssStaGetClientPerSSID (u4BssIfIndex,
                                    &pWssStaMsg->WssStaDB.staMacAddr);
            break;

        case WSS_STA_RADIO_DEAUTH:
            pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();

            if (pWssWlanDB == NULL)
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC, "Failed to allocate memory "
                            "UtlShMemAllocWlanDbBuf returned failure\n");
                return OSIX_FAILURE;
            }
            MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
            MEMSET (&gClient, 0, sizeof (tWssStaClientWalk));

            WssStaDeAuthFn (pWssStaMsg->WssStaDB.u4BssIfIndex);
            pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex =
                pWssStaMsg->WssStaDB.u4BssIfIndex;
            pWssWlanDB->WssWlanIsPresentDB.bBssId = OSIX_TRUE;
            /* Gets the BSSId from WssWlanDB by passing the BSS interface index */
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC, "Failed to obtain the bssid "
                            "from wlan db \r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
                /*continue; */
            }
            pWlcHdlrMsgStruct =
                (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
            if (pWlcHdlrMsgStruct == NULL)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WssIfProcessWssAuthDBMsg :-"
                            "UtlShMemAllocWlcBuf returned failure\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }
            MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
            pWssDisassocMacFrame =
                (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
            if (pWssDisassocMacFrame == NULL)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WssIfProcessWssAuthDBMsg :-"
                            "Failed to allocate "
                            "memory for disassoc message frame \r\n");
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
                return OSIX_FAILURE;
            }
            MEMSET (pWssDisassocMacFrame, 0, sizeof (tWssMacMsgStruct));

            for (u1Loop = 0; u1Loop < gClient.u1Count; u1Loop++)
            {
                pWssStaWepProcessDB =
                    WssStaProcessEntryGet (gClient.aStaMacAddr[u1Loop]);
                if (pWssStaWepProcessDB == NULL)
                {
                    WSSSTA_TRC (WSSSTA_MGMT_TRC, "Failed to obtain the station "
                                "mac address \r\n");
                    continue;
                }
                MEMCPY (pWssDisassocMacFrame->unMacMsg.DisassocMacFrame.
                        MacMgmtFrmHdr.u1SA, pWssWlanDB->
                        WssWlanAttributeDB.BssId, sizeof (tMacAddr));
                MEMCPY (pWssDisassocMacFrame->unMacMsg.DisassocMacFrame.
                        MacMgmtFrmHdr.u1DA, gClient.aStaMacAddr[u1Loop],
                        sizeof (tMacAddr));
                MEMCPY (pWssDisassocMacFrame->unMacMsg.DisassocMacFrame.
                        MacMgmtFrmHdr.u1BssId, pWssWlanDB->
                        WssWlanAttributeDB.BssId, sizeof (tMacAddr));
                pWssDisassocMacFrame->unMacMsg.DisassocMacFrame.u2SessId =
                    pWssStaWepProcessDB->u2Sessid;
                pWssDisassocMacFrame->unMacMsg.DisassocMacFrame.
                    ReasonCode.u2MacFrameReasonCode = WSSSTA_DISASS_TOO_MANY_MS;

                MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct),
                        pWssDisassocMacFrame, sizeof (tWssMacMsgStruct));
                pWlcHdlrMsgStruct->WssMacMsgStruct.msgType = 0x07;
                if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_MAC_DISASSOC_MSG,
                                            pWlcHdlrMsgStruct) == OSIX_FAILURE)
                {
                    WSSSTA_TRC (WSSSTA_MGMT_TRC, "Station is not connected "
                                "at present\r\n");
                    continue;
                }
                MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));
                wssWlanDB.WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
                wssWlanDB.WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
                MEMCPY (wssWlanDB.WssWlanAttributeDB.BssId,
                        pWssStaWepProcessDB->BssIdMacAddr, sizeof (tMacAddr));

                if (WssIfProcessWssWlanDBMsg
                    (WSS_WLAN_GET_BSSID_MAPPING_ENTRY, &wssWlanDB))
                {
                    continue;
                }
                u4RadioIndex = wssWlanDB.WssWlanAttributeDB.u4RadioIfIndex;

                MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIndex;
                RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

                if (WssIfProcessRadioIfDBMsg
                    (WSS_GET_RADIO_IF_DB, (&RadioIfGetDB)) != OSIX_SUCCESS)
                {
                    continue;
                }
                pWlcHdlrMsgStruct->StationConfReq.wssStaConfig.u2SessId =
                    pWssStaWepProcessDB->u2Sessid;
                pWlcHdlrMsgStruct->StationConfReq.wssStaConfig.DelSta.
                    isPresent = TRUE;
                pWlcHdlrMsgStruct->StationConfReq.wssStaConfig.DelSta.
                    u2MessageType = DELETE_STATION;
                pWlcHdlrMsgStruct->StationConfReq.wssStaConfig.DelSta.
                    u2MessageLength = WSSSTA_DELSTA_LEN;
                pWlcHdlrMsgStruct->StationConfReq.wssStaConfig.DelSta.
                    u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
                pWlcHdlrMsgStruct->StationConfReq.wssStaConfig.DelSta.
                    u1MacAddrLen = sizeof (tMacAddr);
                MEMCPY (pWlcHdlrMsgStruct->StationConfReq.wssStaConfig.DelSta.
                        StaMacAddr, gClient.aStaMacAddr[u1Loop],
                        sizeof (tMacAddr));
                if (WssIfProcessWlcHdlrMsg
                    (WSS_WLCHDLR_STATION_CONF_REQ,
                     pWlcHdlrMsgStruct) == OSIX_FAILURE)
                {
                    break;
                }

            }
            UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssDisassocMacFrame);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            break;
        default:
            break;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : WssStaDelEntryFromWebAuthDb
 * Description  :  This function is called whenever a WebAuth Timer expires.
 * Input        : None
 * Output       : None
 * Returns      : None
 *****************************************************************************/

INT4
WssStaDelEntryFromWebAuthDb (VOID)
{

    tWssIfWebAuthDB     WssStaWebAuthDB;

    while (gi1DBWalkCnt >= 0)
    {
        MEMSET (&WssStaWebAuthDB, 0, sizeof (tWssIfWebAuthDB));
        MEMCPY (WssStaWebAuthDB.au1UserName,
                gaWssStaWebAuthDB[gi1DBWalkCnt].au1UserName,
                STRLEN (gaWssStaWebAuthDB[gi1DBWalkCnt].au1UserName));
        /* Delete the entry from Web Auth DB */
        if (WssStaUpdateWebAuthDB (WSS_STA_WEBAUTH_DELETE_DB,
                                   &WssStaWebAuthDB) != OSIX_SUCCESS)
        {
            WSSSTA_TRC (WSSSTA_MGMT_TRC,
                        "WssStaDelEntryFromWebAuthDb: Failed to "
                        "delete the entry of timeout station "
                        "from Web Auth DB  \r\n");
            return OSIX_FAILURE;
        }
        gi1DBWalkCnt--;
    }
    gi1DBWalkCnt = 0;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : WssStaUpdateWebAuthDB
 * Description  : This function is called whenever a WebAuth username's lifetime
 *                expires.
 * Input        : None
 * Output       : None
 * Returns      : None
 *****************************************************************************/
UINT4
WssStaUpdateWebAuthDB (UINT4 u4Action, tWssIfWebAuthDB * pWssIfStaWebAuthDB)
{
    tWssIfWebAuthDB    *pWebAuthDB = NULL;
    UINT1               u1Count = 0;

    switch (u4Action)
    {
        case WSS_STA_WEBAUTH_DELETE_DB:
            pWebAuthDB = ((tWssIfWebAuthDB *) RBTreeGet (gWssStaWebAuthDB,
                                                         (tRBElem *)
                                                         pWssIfStaWebAuthDB));
            if (pWebAuthDB == NULL)
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "Entry is not present in the WebAuth DB \r\n");
                return OSIX_FAILURE;
            }

            RBTreeWalk (gWssStaStateDB, (tRBWalkFn) StaDBClientUserDeleteWalkFn,
                        pWebAuthDB->au1UserName, &u1Count);
            RBTreeRem (gWssStaWebAuthDB, (tRBElem *) pWebAuthDB);
            MemReleaseMemBlock (WSSIFWEBAUTH_DB_POOLID, (UINT1 *) pWebAuthDB);

            break;
        default:
            break;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : WssStaWebAuthDBWalkFn
 * Description  :  This function is called whenever a WebAuth Timer expires.
 * Input        : None
 * Output       : None
 * Returns      : None
 *****************************************************************************/

INT4
WssStaWebAuthDBWalkFn (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                       void *pArg, void *pOut)
{
    tWssIfWebAuthDB    *pWssStaWebAuthDB = NULL;
    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (pArg);
    UNUSED_PARAM (pOut);
    if ((visit == postorder) || (visit == leaf))
    {
        if (pRBElem != NULL)
        {
            pWssStaWebAuthDB = (tWssIfWebAuthDB *) pRBElem;
            /* If the timer tick count exceed the Max user lifetime, the entry
             * should be deleted */
            if ((pWssStaWebAuthDB->u4TimerTickCount) >=
                (pWssStaWebAuthDB->u4MaxUserLifetime))
            {
                MEMCPY (gaWssStaWebAuthDB[gi1DBWalkCnt].au1UserName,
                        pWssStaWebAuthDB->au1UserName,
                        STRLEN (pWssStaWebAuthDB->au1UserName));
                gi1DBWalkCnt++;
            }
            else
            {
                pWssStaWebAuthDB->u4TimerTickCount++;
            }
        }
    }

    return RB_WALK_CONT;
}

/*****************************************************************************
 *  Function    :  WssAuthUpdateDB                                           *
 *  Input       :                                                            *
 *                                                                           *
 *  Description :  This API is use to update the Station DB                  *
 *                 CapwapDot11WlanBindTable table                            *
 *  Output      :  None                                                      *
 *  Returns     :  OSIX_SUCCESS/OSIX_FAILURE                                 *
 *****************************************************************************/
UINT4
WssAuthUpdateDB (eWssStaUpdateAction action,
                 UINT4 u4BssIfIndex,
                 tMacAddr staMacAddr,
                 UINT2 u2Aid, UINT2 u2SessId, tWssStaParams * pWssStaParams)
{

    UNUSED_PARAM (pWssStaParams);

    tWssIfAuthStateDB  *pWssStaStateDB = NULL;
    tWssIfAuthStateDB   wssStaStateDB;
    UINT1               u1AddFlag = 0;
    INT1                i1Index = 0;
    tWssWlanDB          wssWlanDB;
    UINT1               au1CurrentTime[256];
    UINT4               u4CurrentTime = 0;
    tWssPMInfo          WssPmInfo;
    tWssStaWepProcessDB *pWssStaWepProcessDB;
    tWssStaWepProcessDB WssStaWepProcessDB;
    tWssStaAuthSessionDB *pWssStaAuthSessionDB = NULL;
    tWssStaAuthSessionDB WssStaAuthSessionDB;
    tWssIfWebAuthDB     staWebAuthDB;
    tWssIfWebAuthDB    *pWssStaWebAuthDB;

    UNUSED_PARAM (pWssStaParams);
    UNUSED_PARAM (wssWlanDB);

    MEMSET (&wssWlanDB, 0, sizeof (wssWlanDB));
    MEMSET (&wssStaStateDB, 0, sizeof (tWssIfAuthStateDB));
    MEMSET (&WssStaAuthSessionDB, 0, sizeof (tWssStaAuthSessionDB));
    MEMSET (&staWebAuthDB, 0, sizeof (tWssIfWebAuthDB));
    MEMSET (&au1CurrentTime, 0, 256);

    time_t              t = time (NULL);
    struct tm          *tm = NULL;

    tm = localtime (&t);

    if (tm == NULL)
    {
        return OSIX_FAILURE;
    }

    switch (action)
    {
#ifdef WSSSTA_WANTED
        case WSS_STA_ASSOCIATED:
            MEMCPY (wssStaStateDB.stationMacAddress, staMacAddr,
                    sizeof (tMacAddr));
            pWssStaStateDB = ((tWssIfAuthStateDB *) RBTreeGet (gWssStaStateDB,
                                                               (tRBElem *) &
                                                               wssStaStateDB));
            if (pWssStaStateDB == NULL)
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "ASSOC Case - Entry is not present in the Station "
                            "DB. Creating It \r\n");

                pWssStaStateDB = (tWssIfAuthStateDB *)
                    MemAllocMemBlk (WSSIFAUTH_STADB_POOLID);
                MEMSET (pWssStaStateDB, 0, sizeof (tWssIfAuthStateDB));
                if (pWssStaStateDB == NULL)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaUpdateDB-ASSOC: "
                                "Memory allocation Failed For StationDB \r\n");
                    return OSIX_FAILURE;
                }
                u1AddFlag = 1;
                pWssStaStateDB->u1StaAuthFinished = OSIX_FALSE;
                pWssStaStateDB->u1StaAuthRedirected = OSIX_FALSE;

                SPRINTF ((CHR1 *) au1CurrentTime, "%d-%02d-%02d %02d:%02d:%02d",
                         tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
                         tm->tm_hour, tm->tm_min, tm->tm_sec);

                MEMCPY (pWssStaStateDB->au1StationAddedTime, au1CurrentTime,
                        sizeof (au1CurrentTime));
                pWssStaStateDB->u4StaConnTime = OsixGetSysUpTime ();

                /* update last updated time from the timer DB */
                wssWlanDB.WssWlanAttributeDB.u4BssIfIndex = u4BssIfIndex;
                wssWlanDB.WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
                wssWlanDB.WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                              &wssWlanDB) == OSIX_SUCCESS)
                {
                    MEMCPY (WssStaAuthSessionDB.stationMacAddress, staMacAddr,
                            sizeof (tMacAddr));
                    WssStaAuthSessionDB.u2WlanProfileId =
                        wssWlanDB.WssWlanAttributeDB.u2WlanProfileId;

                    pWssStaAuthSessionDB =
                        (tWssStaAuthSessionDB *)
                        RBTreeGet (gWssStaAuthSessionDB,
                                   (tRBElem *) & WssStaAuthSessionDB);

                    if (pWssStaAuthSessionDB != NULL)
                    {
                        /* Update station web auth status as authenticated if
                         * the station is disconnected and re-connected before
                         * lifetime expiry */
                        u4CurrentTime = OsixGetSysUpTime ();
                        WSSSTA_WEBAUTH_TRC3 (WSSSTAWEBAUTH_MASK,
                                             "\n WEB AUTH DB : LAST Updated time : %d, user life time : %d Current Time : %d\n",
                                             pWssStaAuthSessionDB->
                                             u4LastUpdatedTime,
                                             pWssStaAuthSessionDB->
                                             u4UserLifeTime, u4CurrentTime);
                        if ((u4CurrentTime -
                             pWssStaAuthSessionDB->u4LastUpdatedTime) <
                            (pWssStaAuthSessionDB->u4UserLifeTime *
                             WSSSTA_TIME_IN_MINS))
                        {

                            pWssStaStateDB->u1StaAuthFinished = OSIX_TRUE;
                            if (CapwapSendWebAuthStatus (staMacAddr,
                                                         pWssStaStateDB->
                                                         u1StaAuthFinished,
                                                         u2SessId) !=
                                OSIX_SUCCESS)
                            {
                                pWssStaStateDB->u1StaAuthFinished = OSIX_FALSE;
                                WSSSTA_WEBAUTH_TRC3 (WSSSTAWEBAUTH_MASK,
                                                     "\n Failed to send web auth status : Wlan : %d BSS : %d Old BSS : %d\n",
                                                     WssStaAuthSessionDB.
                                                     u2WlanProfileId, u2SessId,
                                                     pWssStaStateDB->
                                                     aWssStaBSS[0].u2SessId);
                            }
                            else
                            {
                                pWssStaStateDB->u4LastUpdatedTime =
                                    pWssStaAuthSessionDB->u4LastUpdatedTime;
                                pWssStaStateDB->u1StaAuthRedirected = OSIX_TRUE;
                                pWssStaStateDB->u4RuleNumber =
                                    pWssStaAuthSessionDB->u4RuleNumber;
                                MEMCPY (pWssStaStateDB->au1StaRedirectionURL,
                                        pWssStaAuthSessionDB->
                                        au1StaRedirectionURL,
                                        STRLEN (pWssStaAuthSessionDB->
                                                au1StaRedirectionURL));
                                MEMCPY (pWssStaStateDB->au1LoggedUserName,
                                        pWssStaAuthSessionDB->au1LoggedUserName,
                                        WSS_STA_WEBUSER_LEN_MAX);
                            }
                            WSSSTA_WEBAUTH_TRC6 (WSSSTAWEBAUTH_MASK,
                                                 "\nStation :: %2x%2x%2x%2x%2x%2x",
                                                 staMacAddr[0], staMacAddr[1],
                                                 staMacAddr[2], staMacAddr[3],
                                                 staMacAddr[4], staMacAddr[5]);
                            WSSSTA_WEBAUTH_TRC3 (WSSSTAWEBAUTH_MASK,
                                                 " authenticated with User : %s, URL : %s Rule : %d\r\n",
                                                 pWssStaStateDB->
                                                 au1LoggedUserName,
                                                 pWssStaStateDB->
                                                 au1StaRedirectionURL,
                                                 pWssStaStateDB->u4RuleNumber);
                        }
                    }
                }
            }

            i1Index = 0;
            MEMCPY (pWssStaStateDB->stationMacAddress, staMacAddr,
                    sizeof (tMacAddr));
            pWssStaStateDB->aWssStaBSS[i1Index].u4BssIfIndex = u4BssIfIndex;
            pWssStaStateDB->aWssStaBSS[i1Index].u2SessId = u2SessId;
            pWssStaStateDB->u1Index = 0;
            pWssStaStateDB->aWssStaBSS[i1Index].stationState = AUTH_ASSOC;
            /* Store the Association parameters provided by the Station */
            pWssStaStateDB->u2AssociationID = u2Aid;
            pWssStaStateDB->staParams.u2CapabilityInfo =
                pWssStaParams->u2CapabilityInfo;

            if (u1AddFlag)
            {
                /*  pWssStaStateDB->pWssStaParams->u2ListenInterval = ; */
                if (RBTreeAdd (gWssStaStateDB, pWssStaStateDB) != RB_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WssStaUpdateDB-ASSOC: Failed to add entry "
                                "to the RB Tree \r\n");
                    MemReleaseMemBlock (WSSIFAUTH_STADB_POOLID,
                                        (UINT1 *) pWssStaStateDB);
                    pWssStaWepProcessDB =
                        WssStaProcessEntryGet (pWssStaStateDB->
                                               stationMacAddress);
                    if (pWssStaWepProcessDB != NULL)
                    {
                        RBTreeRem (gWssStaWepProcessDB,
                                   (tRBElem *) pWssStaWepProcessDB);
                        MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                            (UINT1 *) pWssStaWepProcessDB);
                    }
                    return OSIX_FAILURE;
                }
                MEMSET (&WssPmInfo, 0, sizeof (tWssPMInfo));
                MEMCPY (&WssPmInfo.FsWlanClientStatsMACAddress, staMacAddr,
                        sizeof (tMacAddr));
                if (pmWTPInfoNotify (CLIENT_ADD, &WssPmInfo) != OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "Create Client entry : Update PM Stats DB Failed \r\n");
                    return OSIX_FAILURE;
                }
            }
            break;

        case WSS_STA_DIASSOCIATED:

        case WSS_STA_DEAUTHENTICATED:
            MEMCPY (wssStaStateDB.stationMacAddress, staMacAddr,
                    sizeof (tMacAddr));
            pWssStaStateDB = ((tWssIfAuthStateDB *) RBTreeGet (gWssStaStateDB,
                                                               (tRBElem *) &
                                                               wssStaStateDB));
            if (pWssStaStateDB == NULL)
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "DEAUTH - Entry is not present in the Station DB \r\n");
            }
            pWssStaWepProcessDB =
                WssStaProcessEntryGet (wssStaStateDB.stationMacAddress);
            if (pWssStaWepProcessDB == NULL)
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "Entry is not present in Station DB \r\n");
                if (pWssStaStateDB != NULL)
                {
                    CAPWAP_TRC (CAPWAP_STATION_TRC,
                                "\nWssAuthUpdateDB : DEAUTH ::: WEP Process DB NULL.. Deleting StaStateDB DB\n");
                    RBTreeRem (gWssStaStateDB, (tRBElem *) pWssStaStateDB);
                    MemReleaseMemBlock (WSSIFAUTH_STADB_POOLID,
                                        (UINT1 *) pWssStaStateDB);
                }
                return OSIX_FAILURE;

            }
            if (pWssStaStateDB == NULL)
            {
                if (pWssStaWepProcessDB != NULL)
                {
                    CAPWAP_TRC (CAPWAP_STATION_TRC,
                                "\nWssAuthUpdateDB : DEAUTH :::StateDB NULL.. Deleting WEPProcess DB\n");
                    RBTreeRem (gWssStaWepProcessDB,
                               (tRBElem *) pWssStaWepProcessDB);
                    MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                        (UINT1 *) pWssStaWepProcessDB);
                }
                return OSIX_FAILURE;
            }
            i1Index = (INT1) (pWssStaStateDB->u1Index);

            MEMCPY (staWebAuthDB.au1UserName, pWssStaStateDB->au1LoggedUserName,
                    STRLEN (pWssStaStateDB->au1LoggedUserName));

            pWssStaWebAuthDB = ((tWssIfWebAuthDB *) RBTreeGet (gWssStaWebAuthDB,
                                                               (tRBElem *) &
                                                               staWebAuthDB));
            /* An entry with Username and Wlan ID will be added */
            if (pWssStaWebAuthDB != NULL)
            {
                u4CurrentTime = OsixGetSysUpTime ();
                if ((u4CurrentTime - pWssStaStateDB->u4LastUpdatedTime) <
                    (pWssStaWebAuthDB->u4MaxUserLifetime * WSSSTA_TIME_IN_MINS))
                {
                    /* Add entry WEB Auth Timer DB to update the station last
                     * updated time and disconnected time */
                    wssWlanDB.WssWlanAttributeDB.u4BssIfIndex = u4BssIfIndex;
                    wssWlanDB.WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
                    wssWlanDB.WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                         &wssWlanDB) != OSIX_SUCCESS)
                    {
                        return OSIX_FAILURE;
                    }

                    MEMCPY (WssStaAuthSessionDB.stationMacAddress, staMacAddr,
                            sizeof (tMacAddr));
                    MEMCPY (WssStaAuthSessionDB.au1LoggedUserName,
                            pWssStaStateDB->au1LoggedUserName,
                            STRLEN (pWssStaStateDB->au1LoggedUserName));
                    WssStaAuthSessionDB.u2WlanProfileId =
                        wssWlanDB.WssWlanAttributeDB.u2WlanProfileId;

                    pWssStaAuthSessionDB =
                        (tWssStaAuthSessionDB *)
                        RBTreeGet (gWssStaAuthSessionDB,
                                   (tRBElem *) & WssStaAuthSessionDB);

                    WSSSTA_WEBAUTH_TRC6 (WSSSTAWEBAUTH_MASK,
                                         "\nWssAuthUpdateDB DE AUTH () ::: station  %2x%2x%2x%2x%2x%2x",
                                         staMacAddr[0], staMacAddr[1],
                                         staMacAddr[2], staMacAddr[3],
                                         staMacAddr[4], staMacAddr[5]);
                    WSSSTA_WEBAUTH_TRC3 (WSSSTAWEBAUTH_MASK,
                                         " BSSID : %d User name : %s Wlan : %d \n",
                                         u4BssIfIndex,
                                         pWssStaStateDB->au1LoggedUserName,
                                         wssWlanDB.WssWlanAttributeDB.
                                         u2WlanProfileId);
                    if (pWssStaAuthSessionDB == NULL)
                    {
                        pWssStaAuthSessionDB = (tWssStaAuthSessionDB *)
                            MemAllocMemBlk (WSSIFAUTH_STATION_SESSION_POOLID);

                        if (pWssStaAuthSessionDB != NULL)
                        {
                            MEMSET (pWssStaAuthSessionDB, 0,
                                    sizeof (tWssStaAuthSessionDB));

                            MEMCPY (pWssStaAuthSessionDB->stationMacAddress,
                                    staMacAddr, sizeof (tMacAddr));
                            MEMCPY (pWssStaAuthSessionDB->au1LoggedUserName,
                                    pWssStaStateDB->au1LoggedUserName,
                                    STRLEN (pWssStaStateDB->au1LoggedUserName));
                            pWssStaAuthSessionDB->u4BssIfIndex = u4BssIfIndex;

                            pWssStaAuthSessionDB->u2WlanProfileId =
                                wssWlanDB.WssWlanAttributeDB.u2WlanProfileId;
                            pWssStaAuthSessionDB->u4StaDeAuthTime =
                                u4CurrentTime;
                            pWssStaAuthSessionDB->u4LastUpdatedTime =
                                pWssStaStateDB->u4LastUpdatedTime;
                            pWssStaAuthSessionDB->u4UserLifeTime =
                                pWssStaWebAuthDB->u4MaxUserLifetime;
                            pWssStaAuthSessionDB->u4RuleNumber =
                                pWssStaStateDB->u4RuleNumber;
                            MEMSET (pWssStaAuthSessionDB->au1StaRedirectionURL,
                                    0,
                                    sizeof (pWssStaAuthSessionDB->
                                            au1StaRedirectionURL));
                            MEMCPY (pWssStaAuthSessionDB->au1StaRedirectionURL,
                                    pWssStaStateDB->au1StaRedirectionURL,
                                    STRLEN (pWssStaStateDB->
                                            au1StaRedirectionURL));

                            if (RBTreeAdd
                                (gWssStaAuthSessionDB,
                                 pWssStaAuthSessionDB) != RB_SUCCESS)
                            {
                                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                                            "DEAUTH - Failed to add entry in WEB auth Timer DB \r\n");
                                /* return OSIX_FAILURE; */
                            }
                            WSSSTA_WEBAUTH_TRC2 (WSSSTAWEBAUTH_MASK,
                                                 "\nEntry added in Timer DB with Last update : %d lifetime : %d\n",
                                                 pWssStaAuthSessionDB->
                                                 u4LastUpdatedTime,
                                                 pWssStaAuthSessionDB->
                                                 u4UserLifeTime);
                        }

                    }
                    else
                    {
                        MEMCPY (pWssStaAuthSessionDB->au1LoggedUserName,
                                pWssStaStateDB->au1LoggedUserName,
                                STRLEN (pWssStaStateDB->au1LoggedUserName));
                        pWssStaAuthSessionDB->u4BssIfIndex = u4BssIfIndex;
                        pWssStaAuthSessionDB->u4StaDeAuthTime = u4CurrentTime;
                        pWssStaAuthSessionDB->u4LastUpdatedTime =
                            pWssStaStateDB->u4LastUpdatedTime;
                        pWssStaAuthSessionDB->u4UserLifeTime =
                            pWssStaWebAuthDB->u4MaxUserLifetime;
                    }
                }
            }

            if (pWssStaStateDB->aWssStaBSS[i1Index].u4BssIfIndex ==
                u4BssIfIndex)
            {
                CAPWAP_TRC (CAPWAP_STATION_TRC,
                            "\nWssAuthUpdateDB : DEAUTH :::.. Deleting WEPProcess DB, Stat StateDB \r\n");
                RBTreeRem (gWssStaStateDB, (tRBElem *) pWssStaStateDB);
                MemReleaseMemBlock (WSSIFAUTH_STADB_POOLID,
                                    (UINT1 *) pWssStaStateDB);
                if (WssStaUpdateWepProcessDB (WSSSTA_DELETE_PROCESS_DB,
                                              pWssStaWepProcessDB) !=
                    OSIX_SUCCESS)
                {
                }
                MEMSET (&WssPmInfo, 0, sizeof (tWssPMInfo));
                MEMCPY (&WssPmInfo.FsWlanClientStatsMACAddress, staMacAddr,
                        sizeof (tMacAddr));
                if (pmWTPInfoNotify (CLIENT_DEL, &WssPmInfo) != OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "Delete Client entry : Update PM Stats DB Failed \r\n");
                    return OSIX_FAILURE;
                }
                MEMSET (&WssStaWepProcessDB, 0, sizeof (tWssStaWepProcessDB));
                MEMCPY (WssStaWepProcessDB.stationMacAddress, staMacAddr,
                        sizeof (tMacAddr));
                pWssStaWepProcessDB =
                    ((tWssStaWepProcessDB *)
                     RBTreeGet (gWssStaWepProcessDB,
                                (tRBElem *) & WssStaWepProcessDB));
                if (pWssStaWepProcessDB != NULL)
                {
                    if (WssStaUpdateWepProcessDB (WSSSTA_DELETE_PROCESS_DB,
                                                  &WssStaWepProcessDB) !=
                        OSIX_SUCCESS)
                    {
                        return OSIX_FAILURE;
                    }
                }
            }
            break;
        case WSS_STA_NEW_STA_REQ:
            break;
        case WSS_STA_AUTHENTICATED:
            break;
        case WSS_STA_OPEN_AUTHENTICATED:
            break;
#endif
        default:
            WSSSTA_TRC (WSSSTA_MGMT_TRC, "Received unknown action\n ");
            return OSIX_FAILURE;
            break;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfCompareWebAuthRBTree
 *  Description     : This function is used to compare the entries in the Web
 *   Authentication DB RB Tree.
 *  Input(s)        : None
 *  Output(s)       : None
 *  Returns         : OSIX_FAILURE on failure
 *                    OSIX_SUCCESS on success
 *****************************************************************************/

INT4
WssIfCompareWebAuthRBTree (tRBElem * e1, tRBElem * e2)
{

    tWssIfWebAuthDB    *pNode1 = e1;
    tWssIfWebAuthDB    *pNode2 = e2;
    INT4                i4RetVal = 0;

    i4RetVal = MEMCMP (pNode1->au1UserName, pNode2->au1UserName,
                       WSS_STA_WEBUSER_LEN_MAX);

    if (i4RetVal > 0)
    {
        return 1;
    }
    else if (i4RetVal < 0)
    {
        return -1;
    }
    return 0;

}

/*****************************************************************************
 *  Function Name   : WssIfCompareWebAuthTempRBTree
 *  Description     : This function is used to compare the entries in the
 *       Web Authentication Temp DB RB Tree.
 *  Input(s)        : None
 *  Output(s)       : None
 * Returns         : OSIX_FAILURE on failure
 *                    OSIX_SUCCESS on success
 *****************************************************************************/

INT4
WssIfCompareWebAuthTempRBTree (tRBElem * e1, tRBElem * e2)
{

    tWssIfWebAuthTempDB *pNode1 = e1;
    tWssIfWebAuthTempDB *pNode2 = e2;
    INT4                i4RetVal = 0;

    i4RetVal = MEMCMP (pNode1->StaMacAddr, pNode2->StaMacAddr,
                       STRLEN (pNode2->StaMacAddr));

    if (i4RetVal > 0)
    {
        return 1;
    }
    else if (i4RetVal < 0)
    {
        return -1;
    }
    return 0;

}

/*****************************************************************************
 *  Function Name   : WssIfWebAuthDBCreate
 *  Description     : This function used to initialize the Web Authentication DB
 *                    RB Tree.
 *  Input(s)        : None
 *  Output(s)       : None
 *  Returns         : OSIX_FAILURE on failure
 *                    OSIX_SUCCESS on success
 *****************************************************************************/
INT4
WssIfWebAuthDBCreate ()
{
    gWssStaWebAuthDB = RBTreeCreateEmbedded ((FSAP_OFFSETOF (tWssIfWebAuthDB,
                                                             nextWebAuthDB)),
                                             WssIfCompareWebAuthRBTree);
    if (gWssStaWebAuthDB == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWebAuthDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWebAuthTempDBCreate
 *  Description     : This function used to initialize the Web
 *   Authentication DB
 *                    RB Tree.
 *  Input(s)        : None
 *  Output(s)       : None
 *  Returns         : OSIX_FAILURE on failure
 *                    OSIX_SUCCESS on success
 *****************************************************************************/
INT4
WssIfWebAuthTempDBCreate ()
{
    gWssStaWebAuthTempDB = RBTreeCreateEmbedded ((FSAP_OFFSETOF
                                                  (tWssIfWebAuthTempDB,
                                                   nextWebAuthTempDB)),
                                                 WssIfCompareWebAuthTempRBTree);
    if (gWssStaWebAuthTempDB == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWebAuthTempDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaWebAuthTreeAdd
 *  Description     : This function is used to Add an entry to the WebAuth
 *    DB  Entry will be added to this tree when lifetime to the  user
 *                    is configured.
 *  Input(s)        : tWssIfAuthLoadBalanceDB 
 *  Output(s)       : None
 *  Returns         : OSIX_FAILURE on failure
 *                    OSIX_SUCCESS on success
 *****************************************************************************/
UINT4
WssStaWebAuthTreeAdd (tWssIfIsSetWebAuthDB * pWssStaWebAuthIsSetDB,
                      tWssIfWebAuthDB * pWebAuthDB)
{
    tWssIfWebAuthDB    *pWssStaWebAuthDB = NULL;
    tWssIfWebAuthDB     staWebAuthDB;

    UINT1               au1Temp[WSS_STA_WEBUSER_LEN_MAX];
    MEMSET (au1Temp, 0, WSS_STA_WEBUSER_LEN_MAX);
    MEMSET (&staWebAuthDB, 0, sizeof (tWssIfWebAuthDB));
    if (STRLEN (pWebAuthDB->au1UserName) == 0)
    {

        return OSIX_FAILURE;
    }
    MEMCPY (staWebAuthDB.au1UserName, pWebAuthDB->au1UserName,
            STRLEN (pWebAuthDB->au1UserName));

    pWssStaWebAuthDB = ((tWssIfWebAuthDB *) RBTreeGet (gWssStaWebAuthDB,
                                                       (tRBElem *) &
                                                       staWebAuthDB));
    /* An entry with Username and Wlan ID will be added */
    if (pWssStaWebAuthDB == NULL)
    {
        /* Creating a node */
        pWssStaWebAuthDB = (tWssIfWebAuthDB *)
            MemAllocMemBlk (WSSIFWEBAUTH_DB_POOLID);
        MEMSET (pWssStaWebAuthDB, 0, sizeof (tWssIfWebAuthDB));
        if (pWssStaWebAuthDB == NULL)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaWebAuthTreeAdd: "
                        "Memory Allocation Failed For WebAuth DB \r\n");
            return OSIX_FAILURE;
        }
        else
        {
            MEMSET (pWssStaWebAuthDB, 0, sizeof (tWssIfWebAuthDB));
            MEMCPY (pWssStaWebAuthDB->au1UserName, pWebAuthDB->au1UserName,
                    STRLEN (pWebAuthDB->au1UserName));

            if (pWssStaWebAuthIsSetDB->bUserPwd == OSIX_TRUE)
            {
                MEMCPY (pWssStaWebAuthDB->au1UserPwd, pWebAuthDB->au1UserPwd,
                        WSS_STA_WEBUSER_LEN_MAX);
            }
            else
            {
                MEMCPY (pWssStaWebAuthDB->au1UserPwd, WEBAUTH_DEFAULT_PASSWORD,
                        STRLEN (WEBAUTH_DEFAULT_PASSWORD));

            }

            if (pWssStaWebAuthIsSetDB->bWlanProfileId == OSIX_TRUE)
            {
                /* Tjis will be hit in case of cli or gui only */
                pWssStaWebAuthDB->u4WlanId = pWebAuthDB->u4WlanId;
            }
            /* only if password and wlan mappinh is there, 
             * rowstatus can be ACTIVE*/
            if ((pWssStaWebAuthDB->u4WlanId != 0) &&
                MEMCMP (pWssStaWebAuthDB->au1UserPwd, au1Temp,
                        WSS_STA_WEBUSER_LEN_MAX) != 0)
            {
                pWssStaWebAuthDB->u1RowStatus = ACTIVE;
            }
            else
            {
                /* This will be hit in case of snmp only */
                pWssStaWebAuthDB->u1RowStatus = NOT_READY;
            }
            /* By default fill the lifetime with assigned or default value */
            if (pWssStaWebAuthIsSetDB->bMaxUserLifetime == OSIX_TRUE)
            {
                pWssStaWebAuthDB->u4MaxUserLifetime =
                    (pWebAuthDB->u4MaxUserLifetime);
            }
            else
            {
                pWssStaWebAuthDB->u4MaxUserLifetime =
                    WSSSTA_DEFAULT_USR_LIFETIME;
            }
            if (RBTreeAdd (gWssStaWebAuthDB, pWssStaWebAuthDB) != RB_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WssStaWebAuthTreeAdd: Failed to add entry to "
                            "Web Auth RB Tree \r\n");
                MemReleaseMemBlock (WSSIFWEBAUTH_DB_POOLID,
                                    (UINT1 *) pWssStaWebAuthDB);
                return OSIX_FAILURE;
            }

            gUserCnt = (INT1) (gUserCnt + 1);
        }
    }
    else                        /* The entry is updated with User lifetime */
    {
        if (pWssStaWebAuthIsSetDB->bUserPwd == OSIX_TRUE)
        {
            MEMCPY (pWssStaWebAuthDB->au1UserPwd, pWebAuthDB->au1UserPwd,
                    WSS_STA_WEBUSER_LEN_MAX);
        }

        if (pWssStaWebAuthIsSetDB->bWlanProfileId == OSIX_TRUE)
        {
            /* Updation of wlan mapping is disabled */
            if (pWssStaWebAuthDB->u1RowStatus == ACTIVE)
            {
                /* WlanId cannot be changed once it is updated */
                if (pWebAuthDB->u4WlanId != pWssStaWebAuthDB->u4WlanId)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WssStaWebAuthTreeAdd: Cannot update"
                                "wlan profile id for user \r\n");
                    return OSIX_FAILURE;
                }
            }
            else
            {
                /* this code will be hit in case of snmp set only */
                pWssStaWebAuthDB->u4WlanId = pWebAuthDB->u4WlanId;
            }

        }
        if (pWssStaWebAuthIsSetDB->bMaxUserLifetime == OSIX_TRUE)
        {
            pWssStaWebAuthDB->u4MaxUserLifetime =
                (pWebAuthDB->u4MaxUserLifetime);
        }
        if (pWssStaWebAuthIsSetDB->bUserEmailId == OSIX_TRUE)
        {
            MEMCPY (pWssStaWebAuthDB->au1UserEmailId,
                    pWebAuthDB->au1UserEmailId,
                    STRLEN (pWebAuthDB->au1UserEmailId));
        }

        if (pWssStaWebAuthIsSetDB->bRowStatus == OSIX_TRUE)
        {
            pWssStaWebAuthDB->u1RowStatus = pWebAuthDB->u1RowStatus;
        }

    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaGetClientPerRadio                                *
 *                                                                           *
 *  Description     : This function walks the RBTree and return the output   *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssStaGetClientPerRadio (UINT4 u4BssIfIndex, UINT1 *pu1ClientCount)
{
    RBTreeWalk (gWssStaStateDB, (tRBWalkFn) StaDBClientWalkFn,
                &u4BssIfIndex, pu1ClientCount);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : StaDBClientWalkFn                                          *
 *                                                                           *
 * Description  : This function is used to walk through station DB and       *
 *                and returns the Client Count                               *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
StaDBClientWalkFn (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                   void *pArg, void *pOut)
{

    tWssIfAuthStateDB  *pStaStateDB = NULL;
    UINT4               u4BssIfIndex = 0;
    UINT1              *pu1ClientCount = 0;
    INT1                u1Index = 0;

    UNUSED_PARAM (u4Level);

    pu1ClientCount = (UINT1 *) &pOut;
    u4BssIfIndex = *(UINT4 *) pArg;

    if ((visit == postorder) || (visit == leaf))
    {
        if (pRBElem != NULL)
        {
            pStaStateDB = (tWssIfAuthStateDB *) pRBElem;
            u1Index = (INT1) pStaStateDB->u1Index;
            if (pStaStateDB->aWssStaBSS[u1Index].u4BssIfIndex == u4BssIfIndex)
            {
                pu1ClientCount++;
            }
        }
    }
    return RB_WALK_CONT;
}

/*****************************************************************************
 *                                                                           *
 * Function     : StaDBClientUserDeletionWalkFn                                          *
 *                                                                           *
 * Description  : This function is used to walk through station DB and       *
 *                and deauthenticate the stations that uses given username   *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
StaDBClientUserDeleteWalkFn (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                             void *pArg, void *pOut)
{
    tWssIfAuthStateDB  *pStaStateDB = NULL;
    UINT1              *pu1Name = NULL;
    UINT1              *pu1Count = 0;

    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (visit);

    pu1Name = (UINT1 *) pArg;
    pu1Count = (UINT1 *) &pOut;

    if ((visit == postorder) || (visit == leaf))
    {
        if (pRBElem != NULL)
        {
            pStaStateDB = (tWssIfAuthStateDB *) pRBElem;

            if (MEMCMP (pStaStateDB->au1LoggedUserName,
                        pu1Name, WSS_STA_WEBUSER_LEN_MAX) == 0)
            {
                pStaStateDB->u1StaAuthFinished = OSIX_FALSE;
                pStaStateDB->u1StaAuthRedirected = OSIX_FALSE;
                MEMSET (pStaStateDB->au1LoggedUserName, 0,
                        WSS_STA_WEBUSER_LEN_MAX);
#ifdef KERNEL_CAPWAP_WANTED
                /* Update the Kernel module */
                if (CapwapUpdateKernelUsrDeletionStaTable
                    (pStaStateDB->stationMacAddress) != OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WssStaProcessReAssocFrames:"
                                " Station not found in the DB \r\n");
                    return OSIX_FAILURE;
                }
#endif
                if (CapwapSendWebAuthStatus (pStaStateDB->stationMacAddress,
                                             pStaStateDB->u1StaAuthFinished,
                                             pStaStateDB->aWssStaBSS[0].
                                             u2SessId) != OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssIfProcessWssAuthDBMsg:"
                                "Sending WEB-AUTH Status to WTP failed\r\n");
                    return OSIX_FAILURE;
                }

                /* Remove entry added with web auth timer if username is deleted */
                RBTreeWalk (gWssStaAuthSessionDB,
                            (tRBWalkFn) WebAuthSessionDBWalkFn, 0, 0);
                WssStaDelEntryFromAuthUsrSesDb (pu1Name);

                *pu1Count = (UINT1) (*pu1Count + 1);
            }
        }
    }

    return *pu1Count;

}

/*****************************************************************************
 *  Function Name   : WssStaGetClientPerSSID                                 *
 *                                                                           *
 *  Description     : This function walks the RBTree and return the output   *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssStaGetClientPerSSID (UINT4 u4BssIfIndex, tMacAddr * pu1staMacAddr)
{
#ifdef KERNEL_CAPWAP_WANTED
    RBTreeWalk (gWssStaStateDB, (tRBWalkFn) StaDBClientMacWalkFn,
                &u4BssIfIndex, pu1staMacAddr);
#else
    UNUSED_PARAM (u4BssIfIndex);
    UNUSED_PARAM (pu1staMacAddr);
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaDeAuthFn                                         *
 *                                                                           *
 *  Description     : This function walks the RBTree and return the output   *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssStaDeAuthFn (UINT4 u4BssIfIndex)
{
    RBTreeWalk (gWssStaStateDB, (tRBWalkFn) StaDBDeAuthWalkFn,
                &u4BssIfIndex, 0);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : StaDBDeAuthWalkFn                                          *
 *                                                                           *
 * Description  : This function is used to walk through station DB and       *
 *                and returns the information required to DeAuth the station *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
StaDBDeAuthWalkFn (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                   void *pArg, void *pOut)
{
    tWssIfAuthStateDB  *pStaStateDB = NULL;
    UINT1               u1Count = 0;
    UINT4               u4BssIfIndex = 0;

    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (pOut);

    u4BssIfIndex = *((UINT4 *) pArg);

    if ((visit == postorder) || (visit == leaf))
    {
        if (pRBElem != NULL)
        {
            pStaStateDB = (tWssIfAuthStateDB *) pRBElem;
            if (u4BssIfIndex == pStaStateDB->aWssStaBSS[0].u4BssIfIndex)
            {
                u1Count = gClient.u1Count;
                MEMCPY (gClient.aStaMacAddr[u1Count],
                        pStaStateDB->stationMacAddress, sizeof (tMacAddr));
                gClient.u1Count = gClient.u1Count + 1;
            }
        }
    }
    return RB_WALK_CONT;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WebAuthSessionDBWalkFn                                     *
 *                                                                           *
 * Description  : This function is used to walk through WEBAuth Session DB   *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WebAuthSessionDBWalkFn (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                        void *pArg, void *pOut)
{

    tWssStaAuthSessionDB *pWssStaAuthSessionDB = NULL;
    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (pOut);
    UNUSED_PARAM (pArg);

    if ((visit == postorder) || (visit == leaf))
    {
        if (pRBElem != NULL)
        {
            pWssStaAuthSessionDB = (tWssStaAuthSessionDB *) pRBElem;
            WSSSTA_WEBAUTH_TRC6 (WSSSTAWEBAUTH_MASK,
                                 "\n AuthSessionDBWalkFn Wlan : %d Username : %s, BssId : %d,"
                                 " Last update : %d, LifeTime : %d Current Time : %d\n",
                                 pWssStaAuthSessionDB->u2WlanProfileId,
                                 pWssStaAuthSessionDB->au1LoggedUserName,
                                 pWssStaAuthSessionDB->u4BssIfIndex,
                                 pWssStaAuthSessionDB->u4LastUpdatedTime,
                                 pWssStaAuthSessionDB->u4UserLifeTime,
                                 OsixGetSysUpTime ());
            MEMCPY (gaWssStaAuthSessionDB[gu4StaAuthSessionIndex].
                    stationMacAddress, pWssStaAuthSessionDB->stationMacAddress,
                    sizeof (tMacAddr));
            gaWssStaAuthSessionDB[gu4StaAuthSessionIndex].u2WlanProfileId =
                pWssStaAuthSessionDB->u2WlanProfileId;
            gu4StaAuthSessionIndex++;
        }
    }
    return RB_WALK_CONT;
}

/*****************************************************************************
 * Function     : WssStaDelEntryFromAuthSessionDb
 * Description  :  This function is called whenever a WebAuth Timer expires.
 * Input        : None
 * Output       : None
 * Returns      : None
 *****************************************************************************/
INT4
WssStaDelEntryFromAuthSessionDb (VOID)
{

    tWssStaAuthSessionDB WssStaAuthSessionDB;
    tWssStaAuthSessionDB *pWssStaAuthSessionDB = NULL;
    UINT4               u4CurrTime = 0;

    WSSSTA_WEBAUTH_TRC1 (WSSSTAWEBAUTH_MASK,
                         "\nWssStaDelEntryFromAuthSessionDb () Number of stations : %d \n",
                         gu4StaAuthSessionIndex);
    while (gu4StaAuthSessionIndex > 0)
    {
        MEMSET (&WssStaAuthSessionDB, 0, sizeof (tWssStaAuthSessionDB));
        MEMCPY (WssStaAuthSessionDB.stationMacAddress,
                gaWssStaAuthSessionDB[gu4StaAuthSessionIndex].stationMacAddress,
                sizeof (tMacAddr));
        WssStaAuthSessionDB.u2WlanProfileId =
            gaWssStaAuthSessionDB[gu4StaAuthSessionIndex].u2WlanProfileId;
        /* Delete the entry from Web Auth Timer DB */
        pWssStaAuthSessionDB =
            (tWssStaAuthSessionDB *) RBTreeGet (gWssStaAuthSessionDB,
                                                (tRBElem *) &
                                                WssStaAuthSessionDB);

        if (pWssStaAuthSessionDB != NULL)
        {
            u4CurrTime = OsixGetSysUpTime ();
            if ((u4CurrTime - pWssStaAuthSessionDB->u4LastUpdatedTime) >=
                (pWssStaAuthSessionDB->u4UserLifeTime * WSSSTA_TIME_IN_MINS))
            {

                RBTreeRem (gWssStaAuthSessionDB,
                           (tRBElem *) pWssStaAuthSessionDB);
                MemReleaseMemBlock (WSSIFAUTH_STATION_SESSION_POOLID,
                                    (UINT1 *) pWssStaAuthSessionDB);
                WSSSTA_WEBAUTH_TRC6 (WSSSTAWEBAUTH_MASK,
                                     "\r\n WssStaDelEntryFromAuthSessionDb: "
                                     "delete the entry of timeout station %02x:%02x:%02x:%02x:%02x:%02x "
                                     "from Web Auth session DB. ",
                                     WssStaAuthSessionDB.stationMacAddress[0],
                                     WssStaAuthSessionDB.stationMacAddress[1],
                                     WssStaAuthSessionDB.stationMacAddress[2],
                                     WssStaAuthSessionDB.stationMacAddress[3],
                                     WssStaAuthSessionDB.stationMacAddress[4],
                                     WssStaAuthSessionDB.stationMacAddress[5]);
                WSSSTA_WEBAUTH_TRC3 (WSSSTAWEBAUTH_MASK,
                                     "Last update : %d, Curr Time : %d, TimeOut : %d\r\n",
                                     pWssStaAuthSessionDB->u4LastUpdatedTime,
                                     u4CurrTime,
                                     pWssStaAuthSessionDB->u4UserLifeTime);
            }
        }
        gu4StaAuthSessionIndex--;
    }
    gu4StaAuthSessionIndex = 0;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : WssStaDelEntryFromAuthUsrSesDb
 * Description  :  This function is called whenever a WebAuth username is
 *                 deleted 
 * Input        : None
 * Output       : None
 * Returns      : None
 *****************************************************************************/
INT4
WssStaDelEntryFromAuthUsrSesDb (UINT1 *pu1UserName)
{

    tWssStaAuthSessionDB WssStaAuthSessionDB;
    tWssStaAuthSessionDB *pWssStaAuthSessionDB = NULL;

    while (gu4StaAuthSessionIndex > 0)
    {
        MEMSET (&WssStaAuthSessionDB, 0, sizeof (tWssStaAuthSessionDB));
        MEMCPY (WssStaAuthSessionDB.stationMacAddress,
                gaWssStaAuthSessionDB[gu4StaAuthSessionIndex].stationMacAddress,
                sizeof (tMacAddr));
        WssStaAuthSessionDB.u2WlanProfileId =
            gaWssStaAuthSessionDB[gu4StaAuthSessionIndex].u2WlanProfileId;
        /* Delete the entry from Web Auth Timer DB */
        pWssStaAuthSessionDB =
            (tWssStaAuthSessionDB *) RBTreeGet (gWssStaAuthSessionDB,
                                                (tRBElem *) &
                                                WssStaAuthSessionDB);

        if (pWssStaAuthSessionDB != NULL)
        {
            if (MEMCMP (pWssStaAuthSessionDB->au1LoggedUserName, pu1UserName,
                        WSS_STA_WEBUSER_LEN_MAX) == 0)
            {
                RBTreeRem (gWssStaAuthSessionDB,
                           (tRBElem *) pWssStaAuthSessionDB);
                MemReleaseMemBlock (WSSIFAUTH_STATION_SESSION_POOLID,
                                    (UINT1 *) pWssStaAuthSessionDB);
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "WssStaDelEntryFromAuthUsrSesDb:  "
                            "deleted the entry of timeout station "
                            "from Web Auth Timer DB  \r\n");
            }
        }
        gu4StaAuthSessionIndex--;
    }
    gu4StaAuthSessionIndex = 0;
    return OSIX_SUCCESS;
}

#ifdef KERNEL_CAPWAP_WANTED
/*****************************************************************************
 *                                                                           *
 * Function     : StaDBClientMacWalkFn                                          *
 *                                                                           *
 * Description  : This function is used to walk through station DB and       *
 *                and returns the Client Mac Address                         *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
StaDBClientMacWalkFn (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                      void *pArg, void *pOut)
{

    tWssIfAuthStateDB  *pStaStateDB = NULL;
    UINT4               u4BssIfIndex = 0;
    tMacAddr           *pu1staMacAddr = NULL;
    tMacAddr            staMacAddr;
    INT1                u1Index = 0;

    UNUSED_PARAM (u4Level);

    MEMSET (&staMacAddr, 0, sizeof (tMacAddr));
    pu1staMacAddr = (tMacAddr *) & pOut;
    u4BssIfIndex = *(UINT4 *) pArg;

    if ((visit == postorder) || (visit == leaf))
    {
        if (pRBElem != NULL)
        {
            pStaStateDB = (tWssIfAuthStateDB *) pRBElem;
            u1Index = (INT1) pStaStateDB->u1Index;
            if (pStaStateDB->aWssStaBSS[u1Index].u4BssIfIndex == u4BssIfIndex)
            {
                MEMCPY (pu1staMacAddr, pStaStateDB->stationMacAddress,
                        sizeof (tMacAddr));
                MEMCPY (staMacAddr, pStaStateDB->stationMacAddress,
                        sizeof (tMacAddr));
                CapwapKernelStaTableUpdate (staMacAddr, pStaStateDB);
            }
        }
    }
    return RB_WALK_CONT;
}
#endif

#endif
