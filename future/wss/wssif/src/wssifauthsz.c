/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                     *
 *   $Id: wssifauthsz.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $                                                                          *
 *  Description: This file contains the Memory pool related api  for WTP     *
 *                                                                           *
 *****************************************************************************/

#define _WSSIFAUTHSZ_C
#include "wssifinc.h"
extern INT4  IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams); 
extern INT4  IssSzRegisterModulePoolId( CHR1 *pu1ModName, tMemPoolId *pModPoolId); 
INT4  WssifauthSizingMemCreateMemPools()
{
    INT4 i4RetVal;
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < WSSIFAUTH_MAX_SIZING_ID; i4SizingId++) {
        i4RetVal = (INT4)MemCreateMemPool( 
                          FsWSSIFAUTHSizingParams[i4SizingId].u4StructSize,
                          FsWSSIFAUTHSizingParams[i4SizingId].u4PreAllocatedUnits,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(WSSIFAUTHMemPoolIds[ i4SizingId]));
        if( i4RetVal == (INT4) MEM_FAILURE) {
            WssifauthSizingMemDeleteMemPools();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4   WssifauthSzRegisterModuleSizingParams( CHR1 *pu1ModName)
{
      /* Copy the Module Name */ 
       IssSzRegisterModuleSizingParams( pu1ModName, FsWSSIFAUTHSizingParams); 
      IssSzRegisterModulePoolId( pu1ModName, WSSIFAUTHMemPoolIds); 
      return OSIX_SUCCESS; 
}


VOID  WssifauthSizingMemDeleteMemPools()
{
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < WSSIFAUTH_MAX_SIZING_ID; i4SizingId++) {
        if(WSSIFAUTHMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool( WSSIFAUTHMemPoolIds[ i4SizingId] );
            WSSIFAUTHMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
