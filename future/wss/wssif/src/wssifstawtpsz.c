/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                     *
 *   $Id: wssifstawtpsz.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $                                                                          *
 *  Description: This file contains the Memory pool related api  for WTP     *
 *                                                                           *
 *****************************************************************************/

#define _WSSIFSTAWTPSZ_C
#include "wssifstawtpinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
WssifstawtpSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < WSSIFSTAWTP_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsWSSIFSTAWTPSizingParams[i4SizingId].
                              u4StructSize,
                              FsWSSIFSTAWTPSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(WSSIFSTAWTPMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            WssifstawtpSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
WssifstawtpSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsWSSIFSTAWTPSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, WSSIFSTAWTPMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
WssifstawtpSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < WSSIFSTAWTP_MAX_SIZING_ID; i4SizingId++)
    {
        if (WSSIFSTAWTPMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (WSSIFSTAWTPMemPoolIds[i4SizingId]);
            WSSIFSTAWTPMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
