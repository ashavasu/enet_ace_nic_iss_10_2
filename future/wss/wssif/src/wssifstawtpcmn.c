/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                     *
 *   $Id: wssifstawtpcmn.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $                                                                          *
 *  Description: This file contains the WSSSTA interface API for WTP         *
 *                                                                           *
 *****************************************************************************/
#ifndef __WSSSTA_WTP_CMN_C__
#define __WSSSTA_WTP_CMN_C__

#include "wssifstawtpinc.h"

/* Process WssIf Msg */
UINT4
WssStaWtpProcessWssIfMsg (eWssMsgTypes StaMsgType, unWssMsgStructs *pWssStaInfo)
{
    UINT4 u4RetVal = 0;
    switch(StaMsgType)
    {
        case WSS_WTP_STA_INIT:
            /* Called from WLC HDLR */
            u4RetVal = WssStaInit();
            break;
        case WSS_WTP_STA_PKT_RCVD:
            /* Called from WSS MAC module */
            u4RetVal = WssStaSetStaActiveFlag(pWssStaInfo->StaActiveInfo);
            break;
        case WSS_WTP_STA_CONFIG_REQUEST_AP:
            /* Called by AP HDLR when Station Config Request is received */            
            u4RetVal = WssStaUpdateDB(&(pWssStaInfo->StaInfoAp));
            break;
        case WSS_STA_DEAUTH_MSG:
            u4RetVal = WssStaDeauthMsg(pWssStaInfo);
            break;
        case WSS_WTP_STA_IDLE_TIMEOUT:
            /* Called by AP HDLR When ths StationIDle timeout happens */
            u4RetVal = WssStaIdleTimeoutExpiry();
            break;
        case WSS_STA_GET_DB:
            u4RetVal = WssStaGetStationDB(&(pWssStaInfo->WssStaStateDB));
	    break;
        case WSS_STA_VALIDATE_DB:
            u4RetVal = (UINT4)WssStaValidateConfigReq();
            break;
        case WSS_CONSTRUCT_TPC_REQUEST:
            u4RetVal = WssStaConstructTpcRequest 
                (&(pWssStaInfo->WssSta11hTpcMsg));
            break;
        case WSS_CONSTRUCT_MEAS_REQUEST:
            u4RetVal = WssStaConstructMeasRequest (pWssStaInfo->
                       WssStaStateDB.u1RadioId);
            break;
        default:
            break;
    }
    return u4RetVal;
}
#endif
