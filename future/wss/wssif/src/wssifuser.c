/******************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                       *
 *  $Id: wssifuser.c,v 1.3 2017/11/24 10:37:10 siva Exp $                     *
 *                                                                            *
 * Description: This file contains the Station DB related API for             *
 *              WSSSTA MODULE                                                 *
 ******************************************************************************/

#include "wssifinc.h"
#include "capwap.h"
#include "arp.h"
#include "stdiplow.h"
#ifdef WLC_WANTED
#include "wssstawlcmacr.h"
#include "wssstawlcsz.h"
#endif

#ifdef WTP_WANTED
#include "wssifstawtpdef.h"
#include "wssifstawtpprot.h"
#endif

#ifdef KERNEL_CAPWAP_WANTED
#include "fcusprot.h"
#endif
#if defined (LNXIP4_WANTED)
extern UINT1       *CfaGddGetLnxIntfnameForPort (UINT2 u2Index);
#endif

/*****************************************************************************
 *                                                                           *
 * Function     : WssUserAllocateTCFilterHandle                              *
 *                                                                           *
 * Description  : This function is used to Allocate TC Filter Handle value   *
 *                which is used to add/delete the filter installation        *
 *                                                                           *
 * Input        : Station Mac Address                                        *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_FAILURE_SUCCESS/OSIX_FAILURE                          *
 *                                                                           *
 *****************************************************************************/
UINT4
WssifAllocateTCFilterHandle (UINT4 *pu4Handle)
{
    UINT4               u4Handle;
    BOOL1               bResult = OSIX_FALSE;

    for (u4Handle = 1; u4Handle < WSS_MAX_USER_TC_FILTER_ID; u4Handle++)
    {
        OSIX_BITLIST_IS_BIT_SET (gTCFilterHandleList, u4Handle,
                                 sizeof (tTCFilterHandleList), bResult);

        if (bResult == OSIX_FALSE)
        {
            OSIX_BITLIST_SET_BIT (gTCFilterHandleList, u4Handle,
                                  sizeof (tTCFilterHandleList));
            *pu4Handle = u4Handle;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssUserFreeTCFilterHandle                                  *
 *                                                                           *
 * Description  : This function is used to delete the allocated TC Filter    *
 *                Handle value.                                              *
 *                                                                           *
 * Input        : Station Mac Address                                        *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_FAILURE_SUCCESS/OSIX_FAILURE                          *
 *                                                                           *
 *****************************************************************************/
VOID
WssifFreeTCFilterHandle (UINT4 *pu4Handle)
{
    OSIX_BITLIST_RESET_BIT (gTCFilterHandleList, *pu4Handle,
                            sizeof (tTCFilterHandleList));
}

/*****************************************************************************
 *  Function Name   : WSSUserRoleConfigBandwidth                             *
 *                                                                           *
 *  Description     : This function is invoked to configure the bandwidth    *
 *                    Value to given interface in both AC and AP             *
 *                                                                           *
 *                                                                           *
 *  Input(s)        : StationMacAddr - Station MAC Address                   *
 *                    u4BandWidth    - Alloted Bandwidth                     *
 *                    pu1InterfaceName - Interface to which the bandwidth    *
 *                                       should be limited                   *
 *                    u1DeleteFlag    - Delete Flag                          *
 *                                                                           *
 *  Returns         : OSIX_SUCCESS or OSIX_FAILURE                           *
 *                                                                           *
 *****************************************************************************/
UINT1
WSSUserRoleConfigBandwidth (tMacAddr StationMacAddr, UINT4 u4BandWidth,
                            UINT4 u4DLBandWidth, UINT4 u4ULBandWidth,
                            UINT1 *pu1InterfaceName, UINT1 u1DeleteFlag)
{
#ifdef KERNEL_CAPWAP_WANTED
    CHR1                ac1Command[MAX_WSSIFWSSUSER_BUFFER_SIZE];
    UINT1               u1ByteMask = MAX_WSSIFWSSUSER_ONE_MASK;
#ifdef WTP_WANTED
    UINT1               au1InterfaceName[24] = { 0 };
#ifdef DAK_WANTED
    UINT1              *pu1DevName = NULL;
    UINT2               u2IfIndex = 0;
#endif
#endif
    tWssStaTCFilterHandle WssStaTCFilterHandle;
    tWssStaTCFilterHandle *pWssStaTCFilterHandle = NULL;
#ifdef WTP_WANTED
    UINT1               u1LocalRouting = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    UINT4               u4IfIndex = 0;
#endif
    MEMSET (&WssStaTCFilterHandle, 0, sizeof (tWssStaTCFilterHandle));
    MEMSET (ac1Command, 0, MAX_WSSIFWSSUSER_BUFFER_SIZE);

    if (u4DLBandWidth == 0)
    {
        u4DLBandWidth = u4BandWidth;
    }
    if (u4ULBandWidth == 0)
    {
        u4ULBandWidth = u4BandWidth;
    }
#ifdef WTP_WANTED
    WssifStaGetWtpInterfaceName (au1InterfaceName);
#endif
    if (u1DeleteFlag != OSIX_TRUE)
    {
#ifdef WLC_WANTED
        MEMCPY (WssStaTCFilterHandle.StationMacAddr, StationMacAddr,
                MAC_ADDR_LEN);
        pWssStaTCFilterHandle =
            RBTreeGet (gWssStaTCFilterHandleDB, &WssStaTCFilterHandle);
        if (pWssStaTCFilterHandle != NULL)
        {
            SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                      "%s %s %s%d %s 0x%02x%02x%02x%02x 0x%02x%02x%02x%02x %s 0x%02x%02x%02x%02x 0x%02x%02x%02x%02x %s %d%s %d%s",
                      "filter delete dev", pu1InterfaceName,
                      "protocol ip parent 1:0 prio 1 handle 800::",
                      pWssStaTCFilterHandle->u4TCFilterHandle, "u32 match u32",
                      StationMacAddr[0], StationMacAddr[1], StationMacAddr[2],
                      StationMacAddr[3], u1ByteMask, u1ByteMask, u1ByteMask,
                      u1ByteMask, "at 44 match u32", StationMacAddr[4],
                      StationMacAddr[5], 0, 0, u1ByteMask, u1ByteMask, 0, 0,
                      "at 48 flowid 1:40 police rate", u4DLBandWidth,
                      "kbit buffer", u4DLBandWidth, "kbit drop");
            WssIfSendCmdToTc ((UINT1 *) ac1Command);
            WssifFreeTCFilterHandle (&
                                     (pWssStaTCFilterHandle->u4TCFilterHandle));
            RBTreeRem (gWssStaTCFilterHandleDB,
                       (tRBElem *) pWssStaTCFilterHandle);
            MemReleaseMemBlock (WSSSTA_TCFILTER_HANDLE_POOLID,
                                (UINT1 *) pWssStaTCFilterHandle);
        }
#endif
#ifdef WTP_WANTED
        MEMCPY (WssStaTCFilterHandle.StationMacAddr, StationMacAddr,
                MAC_ADDR_LEN);
        pWssStaTCFilterHandle =
            RBTreeGet (gWssStaTCFilterHandleDB, &WssStaTCFilterHandle);
        if (pWssStaTCFilterHandle != NULL)
        {
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = 0;

            /* Get Local routing status from CAPWAP module */
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) == OSIX_SUCCESS)
            {
                u1LocalRouting = pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting;
            }
            if (u1LocalRouting == 2)
            {
                /*This Filter is for Upload.
                 * Where the station MAC address will be in Source MAC*/
#ifdef DAK_WANTED
                for (u2IfIndex = SYS_DEF_TGT_MAX_ENET_INTERFACES; u2IfIndex <=
                     SYS_DEF_TGT_MAX_ENET_INTERFACES + 1; u2IfIndex++)
                {
                    pu1DevName = CfaGddGetLnxIntfnameForPort (u2IfIndex);
                    MEMSET (au1InterfaceName, 0, sizeof (au1InterfaceName));
                    MEMCPY (au1InterfaceName, pu1DevName, STRLEN (pu1DevName));
                    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                              "%s %s %s%d %s 0x%02x%02x%02x%02x 0x%02x%02x%02x%02x %s 0x%02x%02x%02x%02x 0x%02x%02x%02x%02x %s %d%s %d%s",
                              "filter delete dev", au1InterfaceName,
                              "protocol ip parent 1:0 prio 1 handle 800::",
                              pWssStaTCFilterHandle->u4TCFilterHandle,
                              "u32 match u32", 0, 0, StationMacAddr[0],
                              StationMacAddr[1], 0, 0, u1ByteMask, u1ByteMask,
                              "at 48 match u32", StationMacAddr[2],
                              StationMacAddr[3], StationMacAddr[4],
                              StationMacAddr[5], u1ByteMask, u1ByteMask,
                              u1ByteMask, u1ByteMask,
                              "at 52 flowid 1:40 police rate", u4ULBandWidth,
                              "kbit buffer", u4ULBandWidth, "kbit drop");
                    WssIfSendCmdToTc ((UINT1 *) ac1Command);
                    MEMSET (au1InterfaceName, 0, sizeof (au1InterfaceName));
                }
#else
                SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                          "%s %s %s%d %s 0x%02x%02x%02x%02x 0x%02x%02x%02x%02x %s 0x%02x%02x%02x%02x 0x%02x%02x%02x%02x %s %d%s %d%s",
                          "filter delete dev", au1InterfaceName,
                          "protocol ip parent 1:0 prio 1 handle 800::",
                          pWssStaTCFilterHandle->u4TCFilterHandle,
                          "u32 match u32", 0, 0, StationMacAddr[0],
                          StationMacAddr[1], 0, 0, u1ByteMask, u1ByteMask,
                          "at 48 match u32", StationMacAddr[2],
                          StationMacAddr[3], StationMacAddr[4],
                          StationMacAddr[5], u1ByteMask, u1ByteMask, u1ByteMask,
                          u1ByteMask, "at 52 flowid 1:40 police rate",
                          u4ULBandWidth, "kbit buffer", u4ULBandWidth,
                          "kbit drop");
                WssIfSendCmdToTc ((UINT1 *) ac1Command);
#endif
            }
            else
            {
                /*This Filter is for Download.
                 * Where the station MAC address will be in Dest MAC*/
#ifdef KERNEL_CAPWAP_WANTED
                SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                          "%s %s %s%d %s 0x%02x%02x%02x%02x 0x%02x%02x%02x%02x %s 0x%02x%02x 0x%02x%02x %s %s %d%s %d%s",
                          "filter delete dev", pu1InterfaceName,
                          "protocol ip parent 1:0 prio 1 handle 800::",
                          pWssStaTCFilterHandle->u4TCFilterHandle,
                          "u32 match u32", StationMacAddr[2], StationMacAddr[3],
                          StationMacAddr[4], StationMacAddr[5], u1ByteMask,
                          u1ByteMask, u1ByteMask, u1ByteMask,
                          "at -12 match u16", StationMacAddr[0],
                          StationMacAddr[1], u1ByteMask, u1ByteMask,
                          "at -14 flowid", "1:40 police rate", u4DLBandWidth,
                          "kbit buffer", u4DLBandWidth, "kbit drop");
#endif
                WssIfSendCmdToTc ((UINT1 *) ac1Command);
                MEMSET (ac1Command, 0, MAX_WSSIFWSSUSER_BUFFER_SIZE);
#ifdef DAK_WANTED
                for (u2IfIndex = SYS_DEF_TGT_MAX_ENET_INTERFACES; u2IfIndex <=
                     SYS_DEF_TGT_MAX_ENET_INTERFACES + 1; u2IfIndex++)
                {
                    pu1DevName = CfaGddGetLnxIntfnameForPort (u2IfIndex);
                    MEMSET (au1InterfaceName, 0, sizeof (au1InterfaceName));
                    MEMCPY (au1InterfaceName, pu1DevName, STRLEN (pu1DevName));
                    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                              "%s %s %s%d %s", "filter delete dev",
                              au1InterfaceName,
                              "protocol ip parent 1:0 handle 800::",
                              pWssStaTCFilterHandle->u4TCFilterHandle,
                              "prio 1 u32");
                    WssIfSendCmdToTc ((UINT1 *) ac1Command);
                    MEMSET (au1InterfaceName, 0, sizeof (au1InterfaceName));
                }
#else
                SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                          "%s %s %s%d %s", "filter delete dev",
                          au1InterfaceName,
                          "protocol ip parent 1:0 handle 800::",
                          pWssStaTCFilterHandle->u4TCFilterHandle,
                          "prio 1 u32");
                WssIfSendCmdToTc ((UINT1 *) ac1Command);
#endif
            }

            WssifFreeTCFilterHandle (&
                                     (pWssStaTCFilterHandle->u4TCFilterHandle));
            RBTreeRem (gWssStaTCFilterHandleDB,
                       (tRBElem *) pWssStaTCFilterHandle);
            MemReleaseMemBlock (WSSSTA_TCFILTER_HANDLE_POOLID,
                                (UINT1 *) pWssStaTCFilterHandle);
            pWssStaTCFilterHandle = NULL;

        }
        if (pWssStaTCFilterHandle == NULL)
#endif
        {
            pWssStaTCFilterHandle = (tWssStaTCFilterHandle *)
                MemAllocMemBlk (WSSSTA_TCFILTER_HANDLE_POOLID);

            if (pWssStaTCFilterHandle == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WSSUserRoleConfigBandwidth: Memory Allocation"
                           " Failed For pWssStaTCFilterHandle");
#ifdef WTP_WANTED
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
#endif
                return OSIX_FAILURE;
            }

            MEMSET (pWssStaTCFilterHandle, 0, sizeof (tWssStaTCFilterHandle));
            MEMCPY (pWssStaTCFilterHandle->StationMacAddr, StationMacAddr,
                    MAC_ADDR_LEN);

            if (WssifAllocateTCFilterHandle
                (&(pWssStaTCFilterHandle->u4TCFilterHandle)) == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WSSUserRoleConfigBandwidth: "
                           "Failed to get TCFilterHandle \r\n");
                MemReleaseMemBlock (WSSSTA_TCFILTER_HANDLE_POOLID,
                                    (UINT1 *) pWssStaTCFilterHandle);
#ifdef WTP_WANTED
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
#endif
                return OSIX_FAILURE;
            }

            if (RBTreeAdd (gWssStaTCFilterHandleDB, pWssStaTCFilterHandle) !=
                RB_SUCCESS)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WSSUserRoleConfigBandwidth: "
                           "Failed to add an entry to theTCFilterHandle DB \r\n");
                WssifFreeTCFilterHandle (&
                                         (pWssStaTCFilterHandle->
                                          u4TCFilterHandle));
                MemReleaseMemBlock (WSSSTA_TCFILTER_HANDLE_POOLID,
                                    (UINT1 *) pWssStaTCFilterHandle);
#ifdef WTP_WANTED
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
#endif
                return OSIX_FAILURE;
            }
#ifdef WTP_WANTED
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = 0;

            /* Get Local routing status from CAPWAP module */
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) == OSIX_SUCCESS)
            {
                u1LocalRouting = pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting;
            }
            WSSIF_TRC3 (WSSIF_FAILURE_TRC,
                        "\n\nADD TC FILTER :: Bandwidth : %d Local Routing : %d Filter Handle : %d ",
                        u4BandWidth, u1LocalRouting,
                        pWssStaTCFilterHandle->u4TCFilterHandle);
            WSSIF_TRC6 (WSSIF_FAILURE_TRC,
                        "Station MAC : %02x:%02x:%02x:%02x:%02x:%02x",
                        StationMacAddr[0], StationMacAddr[1], StationMacAddr[2],
                        StationMacAddr[3], StationMacAddr[4],
                        StationMacAddr[5]);
#endif
#ifdef WTP_WANTED
            if (u1LocalRouting == 1)
            {

                /*This Filter is for Download.
                 * Where the station MAC address will be in Dest MAC*/
                SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                          "%s %s %s%d %s 0x%02x%02x%02x%02x 0x%02x%02x%02x%02x %s 0x%02x%02x 0x%02x%02x %s %s %d%s %d%s",
                          "filter add dev", pu1InterfaceName,
                          "protocol ip parent 1:0 prio 1 handle ::",
                          pWssStaTCFilterHandle->u4TCFilterHandle,
                          "u32 match u32", StationMacAddr[2], StationMacAddr[3],
                          StationMacAddr[4], StationMacAddr[5], u1ByteMask,
                          u1ByteMask, u1ByteMask, u1ByteMask,
                          "at -12 match u16", StationMacAddr[0],
                          StationMacAddr[1], u1ByteMask, u1ByteMask,
                          "at -14 flowid", "1:40 police rate", u4DLBandWidth,
                          "kbit buffer", u4DLBandWidth, "kbit drop");
                WssIfSendCmdToTc ((UINT1 *) ac1Command);
            }
            else if (u1LocalRouting == 2)
            {
                /*This Filter is for Upload.
                 * Where the station MAC address will be in Source MAC*/
#ifdef DAK_WANTED
                for (u2IfIndex = SYS_DEF_TGT_MAX_ENET_INTERFACES; u2IfIndex <=
                     SYS_DEF_TGT_MAX_ENET_INTERFACES + 1; u2IfIndex++)
                {
                    pu1DevName = CfaGddGetLnxIntfnameForPort (u2IfIndex);
                    MEMSET (au1InterfaceName, 0, sizeof (au1InterfaceName));
                    MEMCPY (au1InterfaceName, pu1DevName, STRLEN (pu1DevName));
                    printf
                        ("\n\n INSTALLING RULE FOR STATION in interface %s\n\n",
                         au1InterfaceName);
                    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                              "%s %s %s%d %s 0x%02x%02x%02x%02x 0x%02x%02x%02x%02x %s 0x%02x%02x%02x%02x 0x%02x%02x%02x%02x %s %d%s %d%s",
                              "filter add dev", au1InterfaceName,
                              "protocol ip parent 1:0 prio 1 handle ::",
                              pWssStaTCFilterHandle->u4TCFilterHandle,
                              "u32 match u32", 0, 0, StationMacAddr[0],
                              StationMacAddr[1], 0, 0, u1ByteMask, u1ByteMask,
                              "at 48 match u32", StationMacAddr[2],
                              StationMacAddr[3], StationMacAddr[4],
                              StationMacAddr[5], u1ByteMask, u1ByteMask,
                              u1ByteMask, u1ByteMask,
                              "at 52 flowid 1:40 police rate", u4ULBandWidth,
                              "kbit buffer", u4ULBandWidth, "kbit drop");
                    WssIfSendCmdToTc ((UINT1 *) ac1Command);
                    MEMSET (au1InterfaceName, 0, sizeof (au1InterfaceName));
                }
#else
                SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                          "%s %s %s%d %s 0x%02x%02x%02x%02x 0x%02x%02x%02x%02x %s 0x%02x%02x%02x%02x 0x%02x%02x%02x%02x %s %d%s %d%s",
                          "filter add dev", au1InterfaceName,
                          "protocol ip parent 1:0 prio 1 handle ::",
                          pWssStaTCFilterHandle->u4TCFilterHandle,
                          "u32 match u32", 0, 0, StationMacAddr[0],
                          StationMacAddr[1], 0, 0, u1ByteMask, u1ByteMask,
                          "at 48 match u32", StationMacAddr[2],
                          StationMacAddr[3], StationMacAddr[4],
                          StationMacAddr[5], u1ByteMask, u1ByteMask, u1ByteMask,
                          u1ByteMask, "at 52 flowid 1:40 police rate",
                          u4ULBandWidth, "kbit buffer", u4ULBandWidth,
                          "kbit drop");
                WssIfSendCmdToTc ((UINT1 *) ac1Command);
#endif
            }
            else                /*Local Bridging case */
            {
                /*This Filter is for Download.
                 * Where the station MAC address will be in Dest MAC*/
                SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                          "%s %s %s%d %s 0x%02x%02x%02x%02x 0x%02x%02x%02x%02x %s 0x%02x%02x 0x%02x%02x %s %s %d%s %d%s",
                          "filter add dev", pu1InterfaceName,
                          "protocol ip parent 1:0 prio 1 handle ::",
                          pWssStaTCFilterHandle->u4TCFilterHandle,
                          "u32 match u32", StationMacAddr[2], StationMacAddr[3],
                          StationMacAddr[4], StationMacAddr[5], u1ByteMask,
                          u1ByteMask, u1ByteMask, u1ByteMask,
                          "at -12 match u16", StationMacAddr[0],
                          StationMacAddr[1], u1ByteMask, u1ByteMask,
                          "at -14 flowid", "1:40 police rate", u4DLBandWidth,
                          "kbit buffer", u4DLBandWidth, "kbit drop");
                WssIfSendCmdToTc ((UINT1 *) ac1Command);

                /*This Filter is for Upload.
                 * Where the station MAC address will be in Source MAC*/
#ifdef DAK_WANTED
                for (u2IfIndex = SYS_DEF_TGT_MAX_ENET_INTERFACES; u2IfIndex <=
                     SYS_DEF_TGT_MAX_ENET_INTERFACES + 1; u2IfIndex++)
                {
                    pu1DevName = CfaGddGetLnxIntfnameForPort (u2IfIndex);
                    MEMSET (au1InterfaceName, 0, sizeof (au1InterfaceName));
                    MEMCPY (au1InterfaceName, pu1DevName, STRLEN (pu1DevName));
                    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                              "%s %s %s%d %s 0x%02x%02x%02x%02x 0x%02x%02x%02x%02x %s 0x%02x%02x 0x%02x%02x %s %s %d%s %d%s",
                              "filter add dev", au1InterfaceName,
                              "protocol ip parent 1:0 prio 1 handle ::",
                              pWssStaTCFilterHandle->u4TCFilterHandle,
                              "u32 match u32", StationMacAddr[0],
                              StationMacAddr[1], StationMacAddr[2],
                              StationMacAddr[3], u1ByteMask, u1ByteMask,
                              u1ByteMask, u1ByteMask, "at -8 match u16",
                              StationMacAddr[4], StationMacAddr[5], u1ByteMask,
                              u1ByteMask, "at -4 flowid", "1:40 police rate",
                              u4ULBandWidth, "kbit buffer", u4ULBandWidth,
                              "kbit drop");

                    WssIfSendCmdToTc ((UINT1 *) ac1Command);
                    MEMSET (au1InterfaceName, 0, sizeof (au1InterfaceName));
                }
#else
                SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                          "%s %s %s%d %s 0x%02x%02x%02x%02x 0x%02x%02x%02x%02x %s 0x%02x%02x 0x%02x%02x %s %s %d%s %d%s",
                          "filter add dev", au1InterfaceName,
                          "protocol ip parent 1:0 prio 1 handle ::",
                          pWssStaTCFilterHandle->u4TCFilterHandle,
                          "u32 match u32", StationMacAddr[0], StationMacAddr[1],
                          StationMacAddr[2], StationMacAddr[3], u1ByteMask,
                          u1ByteMask, u1ByteMask, u1ByteMask, "at -8 match u16",
                          StationMacAddr[4], StationMacAddr[5], u1ByteMask,
                          u1ByteMask, "at -4 flowid", "1:40 police rate",
                          u4ULBandWidth, "kbit buffer", u4ULBandWidth,
                          "kbit drop");

                WssIfSendCmdToTc ((UINT1 *) ac1Command);
#endif

            }
#endif
#ifdef WLC_WANTED
            /*This Filter is for Download.
             * Where the station MAC address will be in Destination MAC*/
            SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                      "%s %s %s%d %s 0x%02x%02x%02x%02x 0x%02x%02x%02x%02x %s 0x%02x%02x%02x%02x 0x%02x%02x%02x%02x %s %d%s %d%s",
                      "filter add dev", pu1InterfaceName,
                      "protocol ip parent 1:0 prio 1 handle ::",
                      pWssStaTCFilterHandle->u4TCFilterHandle, "u32 match u32",
                      StationMacAddr[0], StationMacAddr[1], StationMacAddr[2],
                      StationMacAddr[3], u1ByteMask, u1ByteMask, u1ByteMask,
                      u1ByteMask, "at 44 match u32", StationMacAddr[4],
                      StationMacAddr[5], 0, 0, u1ByteMask, u1ByteMask, 0, 0,
                      "at 48 flowid 1:40 police rate", u4DLBandWidth,
                      "kbit buffer", u4DLBandWidth, "kbit drop");
            WssIfSendCmdToTc ((UINT1 *) ac1Command);
#endif
        }

    }
    else if (u1DeleteFlag == OSIX_TRUE)
    {
        MEMCPY (WssStaTCFilterHandle.StationMacAddr, StationMacAddr,
                MAC_ADDR_LEN);
        pWssStaTCFilterHandle =
            RBTreeGet (gWssStaTCFilterHandleDB, &WssStaTCFilterHandle);
        if (pWssStaTCFilterHandle != NULL)
        {
#ifdef WTP_WANTED
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = 0;

            /* Get Local routing status from CAPWAP module */
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) == OSIX_SUCCESS)
            {
                u1LocalRouting = pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting;
            }
            if (u1LocalRouting == 2)
            {
#ifdef DAK_WANTED
                for (u2IfIndex = SYS_DEF_TGT_MAX_ENET_INTERFACES; u2IfIndex <=
                     SYS_DEF_TGT_MAX_ENET_INTERFACES + 1; u2IfIndex++)
                {
                    pu1DevName = CfaGddGetLnxIntfnameForPort (u2IfIndex);
                    MEMSET (au1InterfaceName, 0, sizeof (au1InterfaceName));
                    MEMCPY (au1InterfaceName, pu1DevName, STRLEN (pu1DevName));
                    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                              "%s %s %s%d %s 0x%02x%02x 0x%02x%02x %s 0x%02x%02x%02x%02x 0x%02x%02x%02x%02x %s %d%s %d%s",
                              "filter delete dev", au1InterfaceName,
                              "protocol ip parent 1:0 prio 1 handle 800::",
                              pWssStaTCFilterHandle->u4TCFilterHandle,
                              "u32 match u32", StationMacAddr[0],
                              StationMacAddr[1], u1ByteMask, u1ByteMask,
                              "at 48 match u32", StationMacAddr[2],
                              StationMacAddr[3], StationMacAddr[4],
                              StationMacAddr[5], u1ByteMask, u1ByteMask,
                              u1ByteMask, u1ByteMask,
                              "at 52 flowid 1:40 police rate", u4ULBandWidth,
                              "kbit buffer", u4ULBandWidth, "kbit drop");
                    WssIfSendCmdToTc ((UINT1 *) ac1Command);
                    MEMSET (au1InterfaceName, 0, sizeof (au1InterfaceName));
                }
#else
                SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                          "%s %s %s%d %s 0x%02x%02x 0x%02x%02x %s 0x%02x%02x%02x%02x 0x%02x%02x%02x%02x %s %d%s %d%s",
                          "filter delete dev", au1InterfaceName,
                          "protocol ip parent 1:0 prio 1 handle 800::",
                          pWssStaTCFilterHandle->u4TCFilterHandle,
                          "u32 match u32", StationMacAddr[0], StationMacAddr[1],
                          u1ByteMask, u1ByteMask, "at 48 match u32",
                          StationMacAddr[2], StationMacAddr[3],
                          StationMacAddr[4], StationMacAddr[5], u1ByteMask,
                          u1ByteMask, u1ByteMask, u1ByteMask,
                          "at 52 flowid 1:40 police rate", u4ULBandWidth,
                          "kbit buffer", u4ULBandWidth, "kbit drop");
                WssIfSendCmdToTc ((UINT1 *) ac1Command);
#endif
            }
            else
            {
                WssifStaGetWtpInterfaceName (au1InterfaceName);
                /*This Filter is for Download.
                 * Where the station MAC address will be in Dest MAC*/
                SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                          "%s %s %s%d %s 0x%02x%02x%02x%02x 0x%02x%02x%02x%02x %s 0x%02x%02x 0x%02x%02x %s %s %d%s %d%s",
                          "filter delete dev", pu1InterfaceName,
                          "protocol ip parent 1:0 prio 1 handle 800::",
                          pWssStaTCFilterHandle->u4TCFilterHandle,
                          "u32 match u32", StationMacAddr[2], StationMacAddr[3],
                          StationMacAddr[4], StationMacAddr[5], u1ByteMask,
                          u1ByteMask, u1ByteMask, u1ByteMask,
                          "at -12 match u16", StationMacAddr[0],
                          StationMacAddr[1], u1ByteMask, u1ByteMask,
                          "at -14 flowid", "1:40 police rate", u4DLBandWidth,
                          "kbit buffer", u4DLBandWidth, "kbit drop");
                WssIfSendCmdToTc ((UINT1 *) ac1Command);
                MEMSET (ac1Command, 0, MAX_WSSIFWSSUSER_BUFFER_SIZE);
#ifdef DAK_WANTED
                for (u2IfIndex = SYS_DEF_TGT_MAX_ENET_INTERFACES; u2IfIndex <=
                     SYS_DEF_TGT_MAX_ENET_INTERFACES + 1; u2IfIndex++)
                {
                    pu1DevName = CfaGddGetLnxIntfnameForPort (u2IfIndex);
                    MEMSET (au1InterfaceName, 0, sizeof (au1InterfaceName));
                    MEMCPY (au1InterfaceName, pu1DevName, STRLEN (pu1DevName));
                    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                              "%s %s %s%d %s", "filter delete dev",
                              au1InterfaceName,
                              "protocol ip parent 1:0 handle 800::",
                              pWssStaTCFilterHandle->u4TCFilterHandle,
                              "prio 1 u32");
                    WssIfSendCmdToTc ((UINT1 *) ac1Command);
                    MEMSET (au1InterfaceName, 0, sizeof (au1InterfaceName));
                }
#else
                SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                          "%s %s %s%d %s", "filter delete dev",
                          au1InterfaceName,
                          "protocol ip parent 1:0 handle 800::",
                          pWssStaTCFilterHandle->u4TCFilterHandle,
                          "prio 1 u32");
                WssIfSendCmdToTc ((UINT1 *) ac1Command);
#endif

                ArpGetIntfForAddr (0, pWssStaTCFilterHandle->u4StationIp,
                                   &u4IfIndex);

                nmhSetIpNetToMediaType ((INT4) u4IfIndex,
                                        pWssStaTCFilterHandle->u4StationIp, 2);
            }

#endif
#ifdef WLC_WANTED
            SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s %s %s%d %s",
                      "filter delete dev", pu1InterfaceName,
                      "parent 1:0 protocol ip prio 1 handle 800::",
                      pWssStaTCFilterHandle->u4TCFilterHandle, "u32");
            WssIfSendCmdToTc ((UINT1 *) ac1Command);
#endif
            WssifFreeTCFilterHandle (&
                                     (pWssStaTCFilterHandle->u4TCFilterHandle));
            RBTreeRem (gWssStaTCFilterHandleDB,
                       (tRBElem *) pWssStaTCFilterHandle);
            MemReleaseMemBlock (WSSSTA_TCFILTER_HANDLE_POOLID,
                                (UINT1 *) pWssStaTCFilterHandle);

        }
    }
#ifdef WTP_WANTED
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
#endif
    UNUSED_PARAM (pu1InterfaceName);
    return OSIX_SUCCESS;
#endif
    UNUSED_PARAM (StationMacAddr);
    UNUSED_PARAM (u4BandWidth);
    UNUSED_PARAM (u4DLBandWidth);
    UNUSED_PARAM (u4ULBandWidth);
    UNUSED_PARAM (pu1InterfaceName);
    UNUSED_PARAM (u1DeleteFlag);
    return OSIX_SUCCESS;
}

#ifdef WTP_WANTED
/*****************************************************************************
 *  Function Name   : WSSUserRoleConfigBandwidthForIP                        *
 *                                                                           *
 *  Description     : This function is invoked to configure the upload       *
 *                    filters in the WTP                                     *
 *                                                                           *
 *                                                                           *
 *  Input(s)        : StationMacAddr - Station MAC Address                   *
 *                   u4IpAddr - Station IP Address                         *
 *                    u4BandWidth    - Alloted Bandwidth                     *
 *                    pu1InterfaceName - Interface to which the bandwidth    *
 *                                       should be limited                   *
 *                    u1DeleteFlag    - Delete Flag                          *
 *                                                                           *
 *  Returns         : OSIX_SUCCESS or OSIX_FAILURE                           *
 *                                                                           *
 *****************************************************************************/
UINT1
WSSUserRoleConfigBandwidthForIP (tMacAddr StationMacAddr, UINT4 u4IpAddr,
                                 UINT4 u4BandWidth, UINT4 u4DLBandWidth,
                                 UINT4 u4ULBandWidth, UINT1 *pu1InterfaceName,
                                 UINT1 u1DeleteFlag)
{
#ifdef KERNEL_CAPWAP_WANTED
    CHR1                ac1Command[MAX_WSSIFWSSUSER_BUFFER_SIZE];
    UINT1               u1ByteMask = MAX_WSSIFWSSUSER_ONE_MASK;
    tWssStaTCFilterHandle WssStaTCFilterHandle;
    tWssStaTCFilterHandle *pWssStaTCFilterHandle = NULL;
    UINT1               u1LocalRouting = 0;
    UINT1               au1InterfaceName[24] = { 0 };
    CHR1               *pu1IpString = NULL;
    tUtlInAddr          IpAddr;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    MEMSET (&WssStaTCFilterHandle, 0, sizeof (tWssStaTCFilterHandle));
    MEMSET (ac1Command, 0, MAX_WSSIFWSSUSER_BUFFER_SIZE);

    if (u4DLBandWidth == 0)
    {
        u4DLBandWidth = u4BandWidth;
    }
    if (u4ULBandWidth == 0)
    {
        u4ULBandWidth = u4BandWidth;
    }
    WssifStaGetWtpInterfaceName (au1InterfaceName);
    if (u1DeleteFlag != OSIX_TRUE)
    {
        MEMCPY (WssStaTCFilterHandle.StationMacAddr, StationMacAddr,
                MAC_ADDR_LEN);
        pWssStaTCFilterHandle =
            RBTreeGet (gWssStaTCFilterHandleDB, &WssStaTCFilterHandle);
        if (pWssStaTCFilterHandle != NULL)
        {
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = 0;

            /* Get Local routing status from CAPWAP module */
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) == OSIX_SUCCESS)
            {
                u1LocalRouting = pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting;
            }
            if (u1LocalRouting == 1)
            {
                MEMSET (ac1Command, 0, MAX_WSSIFWSSUSER_BUFFER_SIZE);
                SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                          "%s %s %s%d %s", "filter delete dev",
                          au1InterfaceName,
                          "protocol ip parent 1:0 handle 800::",
                          pWssStaTCFilterHandle->u4TCFilterHandle,
                          "prio 1 u32");
                WssIfSendCmdToTc ((UINT1 *) ac1Command);
            }
#ifdef WTP_WANTED
            if (u1LocalRouting == 1)
            {
                IpAddr.u4Addr = OSIX_NTOHL (u4IpAddr);
                pu1IpString = (CHR1 *) CLI_INET_NTOA (IpAddr);
                MEMSET (ac1Command, 0, MAX_WSSIFWSSUSER_BUFFER_SIZE);
                SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                          "%s %s %s%d %s %s %s %d%s %d%s", "filter add dev",
                          au1InterfaceName,
                          "protocol ip parent 1:0 prio 1 handle ::",
                          pWssStaTCFilterHandle->u4TCFilterHandle,
                          "u32 match ip src", pu1IpString,
                          "flowid 1:40 police rate", u4ULBandWidth,
                          "kbit buffer", u4ULBandWidth, "kbit drop");
                WssIfSendCmdToTc ((UINT1 *) ac1Command);
                pWssStaTCFilterHandle->u4StationIp = u4IpAddr;
            }
#endif
        }
    }
    else if (u1DeleteFlag == OSIX_TRUE)
    {
        MEMCPY (WssStaTCFilterHandle.StationMacAddr, StationMacAddr,
                MAC_ADDR_LEN);
        pWssStaTCFilterHandle =
            RBTreeGet (gWssStaTCFilterHandleDB, &WssStaTCFilterHandle);
        if (pWssStaTCFilterHandle != NULL)
        {
#ifdef WTP_WANTED
            if (u1LocalRouting == 1)
            {
                MEMSET (ac1Command, 0, MAX_WSSIFWSSUSER_BUFFER_SIZE);
                IpAddr.u4Addr = OSIX_NTOHL (u4IpAddr);
                pu1IpString = (CHR1 *) CLI_INET_NTOA (IpAddr);
                SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                          "%s %s %s%d %s %s %s %d%s %d%s", "filter delete dev ",
                          au1InterfaceName,
                          " protocol ip parent 1:0 prio 1 handle 800::",
                          pWssStaTCFilterHandle->u4TCFilterHandle,
                          "u32 match ip src ", pu1IpString,
                          "flowid 1:40 police rate", u4ULBandWidth,
                          "kbit buffer", u4ULBandWidth, "kbit drop");
                WssIfSendCmdToTc ((UINT1 *) ac1Command);
            }
#endif
        }
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UNUSED_PARAM (pu1InterfaceName);
    UNUSED_PARAM (u1ByteMask);
#else
    UNUSED_PARAM (StationMacAddr);
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (u4BandWidth);
    UNUSED_PARAM (u4DLBandWidth);
    UNUSED_PARAM (u4ULBandWidth);
    UNUSED_PARAM (pu1InterfaceName);
    UNUSED_PARAM (u1DeleteFlag);
#endif
    return OSIX_SUCCESS;
}

#endif

#ifdef KERNEL_CAPWAP_WANTED
/*****************************************************************************
 *                                                                           *
 * Function     : WssIfWmmQueueCreation                                      *
 *                                                                           *
 * Description  : This function is invoken to get the available physical     * 
 *                interface to create the queues                             *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 ****************************************************************************/
VOID
WssIfWmmQueueCreation (UINT1 *pu1IntfName)
{
    UINT1               au1IntfName[200];

    /********Deleting the queue **********/
    SPRINTF ((CHR1 *) au1IntfName, "qdisc del dev %s root", pu1IntfName);
    WssIfSendCmdToTc (au1IntfName);

    /****************Qdisc details for default queue********/
    SPRINTF ((CHR1 *) au1IntfName,
             "qdisc add dev %s root handle 1: htb default 40", pu1IntfName);
    WssIfSendCmdToTc (au1IntfName);

    /***********Creating class for parent and 1:10, 1:20, 1:30, 1:40 *********/
    SPRINTF ((CHR1 *) au1IntfName,
             "class add dev %s parent 1: classid 1:1 htb rate 300mbit burst 15k",
             pu1IntfName);
    WssIfSendCmdToTc (au1IntfName);
    SPRINTF ((CHR1 *) au1IntfName,
             "class add dev %s parent 1:1 classid 1:10 htb rate 210mbit burst 15k",
             pu1IntfName);
    WssIfSendCmdToTc (au1IntfName);
    SPRINTF ((CHR1 *) au1IntfName,
             "class add dev %s parent 1:1 classid 1:20 htb rate 45mbit burst 15k",
             pu1IntfName);
    WssIfSendCmdToTc (au1IntfName);
    SPRINTF ((CHR1 *) au1IntfName,
             "class add dev %s parent 1:1 classid 1:30 htb rate 15mbit burst 15k",
             pu1IntfName);
    WssIfSendCmdToTc (au1IntfName);
    SPRINTF ((CHR1 *) au1IntfName,
             "class add dev %s parent 1:1 classid 1:40 htb rate 15mbit burst 15k",
             pu1IntfName);
    WssIfSendCmdToTc (au1IntfName);

    /***********Creating qdisc for parent and 1:10, 1:20, 1:30, 1:40 *********/
    SPRINTF ((CHR1 *) au1IntfName,
             "qdisc add dev %s parent 1:10 handle 10: sfq perturb 10",
             pu1IntfName);
    WssIfSendCmdToTc (au1IntfName);
    SPRINTF ((CHR1 *) au1IntfName,
             "qdisc add dev %s parent 1:20 handle 20: sfq perturb 10",
             pu1IntfName);
    WssIfSendCmdToTc (au1IntfName);
    SPRINTF ((CHR1 *) au1IntfName,
             "qdisc add dev %s parent 1:30 handle 30: sfq perturb 10",
             pu1IntfName);
    WssIfSendCmdToTc (au1IntfName);
    SPRINTF ((CHR1 *) au1IntfName,
             "qdisc add dev %s parent 1:40 handle 40: sfq perturb 10",
             pu1IntfName);
    WssIfSendCmdToTc (au1IntfName);
    SPRINTF ((CHR1 *) au1IntfName,
             "filter add dev %s protocol ip parent 1:0 prio 1 u32 match ip tos 0xb8 0xfc flowid 1:10",
             pu1IntfName);
    WssIfSendCmdToTc (au1IntfName);
    SPRINTF ((CHR1 *) au1IntfName,
             "filter add dev %s protocol ip parent 1:0 prio 1 u32 match ip tos 0x88 0xfc flowid 1:20",
             pu1IntfName);
    WssIfSendCmdToTc (au1IntfName);
    OsixTskDelay (1);
    SPRINTF ((CHR1 *) au1IntfName,
             "filter add dev %s protocol ip parent 1:0 prio 1 u32 match ip tos 0x48 0xfc flowid 1:30",
             pu1IntfName);
    WssIfSendCmdToTc (au1IntfName);
    SPRINTF ((CHR1 *) au1IntfName,
             "filter add dev %s protocol ip parent 1:0 prio 1 u32 match ip tos 0x28 0xfc flowid 1:40",
             pu1IntfName);
    WssIfSendCmdToTc (au1IntfName);
}
#endif
