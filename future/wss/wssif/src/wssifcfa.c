/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                 
 *  $Id: wssifcfa.c,v 1.3 2017/11/24 10:37:10 siva Exp $                                                                   
 *  Description: This file contains the interface functions for CFA module
 *                                                                       
 ******************************************************************************/

#ifndef __WSSIF_CFA_C__
#define __WSSIF_CFA_C__

#include "wssifinc.h"
#include "vcm.h"
#include "fscfalw.h"
#include "stdvllow.h"
#include "ifmiblow.h"
#include "cfacli.h"

#ifdef LNXIP4_WANTED
#include "lnxip.h"
#endif

/* Row Status Definitions - same as the MIB variable */
#define   RS_ACTIVE               1
#define   RS_NOTINSERVICE         2
#define   RS_NOTREADY             3
#define   RS_CREATEANDGO          4
#define   RS_CREATEANDWAIT        5
#define   RS_DESTROY              6
#define   RS_INVALID              0    /* proprietary = NO_SUCH_INSTANCE */

extern INT4         VlanSelectContext (UINT4 u4ContextId);
extern INT4         VlanReleaseContext (VOID);
extern INT4         VlanGetContextInfoFromIfIndex (UINT4 u4IfIndex,
                                                   UINT4 *pu4ContextId,
                                                   UINT2 *pu2LocalPort);
extern INT4         CfaSetIfOperStatus (UINT4 u4IfIndex, UINT1 u1OperStatus);
extern INT4 CfaHandlePktFromL2
PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex, UINT4 u4PktSize,
        UINT2 u2Protocol, UINT1 *au1DestAddr, UINT1 u1EncapType));

/*****************************************************************************
 * Function Name      : WssIfProcessCfaMsg                                   *
 *                                                                           *
 * Description        : Wrapper Func through which other modules calls       *
 *                      CFA module                                           *
 *                                                                           *
 * Input(s)           : eMsgType - Op Code                                   *
 *                      pCfaMsgStruct - pointer to input structure           *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/

UINT1
WssIfProcessCfaMsg (UINT4 eMsgType, tCfaMsgStruct * pCfaMsgStruct)
{

    UINT4               u4PktSize = 0;
#ifdef WLC_WANTED
    UINT4               u4Len = 0;
    UINT4               u4DstIpAddr;
    UINT4               u4SrcIpAddr;
    UINT2               u2EtherType = 0;
    UINT1               u1EncapType = 0;
    UINT1               au1DestHwAddr[6] =
        { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
    UINT1               au1DataBuf[WSSIF_BUF_SIZE];
    tCRU_BUF_CHAIN_DESC *pu1VlanFrame = NULL;
    UINT4               u4BssIfIndex = 0;
    UINT4               u4SrcBssIfIndex = 0;
#endif
#ifdef WTP_WANTED
    UINT4               u4IfIndex = 0;

    UNUSED_PARAM (u4IfIndex);
#endif

    if (eMsgType == CFA_WSS_TX_DOT11_PKT)
    {
#ifdef APHDLR_WANTED
        if (pCfaMsgStruct->unCfaMsg.CfaDot11PktBuf.pTxPktBuf == NULL)
        {
            WSSIF_TRC (WSSIF_FAILURE_TRC,
                       "WssIfProcessCfaMsg: Null input received\n");
            return OSIX_FAILURE;
        }
        u4IfIndex = pCfaMsgStruct->unCfaMsg.CfaDot11PktBuf.u4IfIndex;
        u4PktSize = CRU_BUF_Get_ChainValidByteCount
            (pCfaMsgStruct->unCfaMsg.CfaDot11PktBuf.pTxPktBuf);

#ifdef LNXWIRELESS_WANTED
        if (CfaHandlePacketFromWss
            (pCfaMsgStruct->unCfaMsg.CfaDot11PktBuf.pTxPktBuf,
             u4IfIndex) == CFA_SUCCESS)
        {
            return OSIX_SUCCESS;
        }
#elif WASP_WANTED
        if (CfaIwfWssProcessTxFrame
            (pCfaMsgStruct->unCfaMsg.CfaDot11PktBuf.pTxPktBuf,
             u4IfIndex, u4PktSize) == CFA_SUCCESS)
        {
            return OSIX_SUCCESS;
        }
#endif
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfProcessCfaMsg: Posting message to CFA Queue failed\n");
        return OSIX_FAILURE;
#endif
    }
    else if (eMsgType == CFA_WSS_TX_DOT3_PKT)
    {
        if (pCfaMsgStruct->unCfaMsg.CfaDot11PktBuf.pTxPktBuf == NULL)
        {
            WSSIF_TRC (WSSIF_FAILURE_TRC,
                       "WssIfProcessCfaMsg: Null input received\n");
            return OSIX_FAILURE;
        }

        u4PktSize = CRU_BUF_Get_ChainValidByteCount
            (pCfaMsgStruct->unCfaMsg.CfaDot11PktBuf.pTxPktBuf);

#ifdef WLC_WANTED
        MEMSET (au1DataBuf, 0, WSSIF_BUF_SIZE);
        if (u4PktSize < WSSIF_BUF_SIZE)
        {
            CRU_BUF_Copy_FromBufChain (pCfaMsgStruct->unCfaMsg.CfaDot11PktBuf.
                                       pTxPktBuf, au1DataBuf, 0, u4PktSize);
            MEMCPY (&u2EtherType, au1DataBuf + WSSIF_ARP_OFFSET,
                    WSSIF_TWO_BYTE);
        }
        if (OSIX_NTOHS (u2EtherType) == WSSIF_ARP_ETHERTYPE)
        {
            MEMCPY (&u4DstIpAddr, au1DataBuf + 38, sizeof (u4DstIpAddr));
            MEMCPY (&u4SrcIpAddr, au1DataBuf + 28, sizeof (u4SrcIpAddr));

            if (WssIfGetStationIpAddr (&u4BssIfIndex, OSIX_NTOHL (u4DstIpAddr))
                == OSIX_SUCCESS)
            {
                if (WssIfGetIsolatedStationIpAddr (&u4BssIfIndex))
                {
                    /* Check src ip is isolated station */
                    if (WssIfGetStationIpAddr (&u4SrcBssIfIndex,
                                               OSIX_NTOHL (u4SrcIpAddr))
                        == OSIX_SUCCESS)
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "Arp Request Packet Droppig due to isolation enabled for destination\n");
                        CRU_BUF_Release_MsgBufChain
                            (pCfaMsgStruct->unCfaMsg.CfaDot11PktBuf.pTxPktBuf,
                             0);
                        return OSIX_SUCCESS;
                    }
                }

                if (CfaHandlePktFromL2
                    (pCfaMsgStruct->unCfaMsg.CfaDot11PktBuf.pTxPktBuf,
                     u4BssIfIndex, u4PktSize, CFA_ENET_IPV4, au1DestHwAddr,
                     u1EncapType) != CFA_SUCCESS)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC,
                               "CfaHandlePktFromL2 returned failure\n");
                    return OSIX_FAILURE;
                }
                return OSIX_SUCCESS;
            }
        }
        u4Len = (u4PktSize + VLAN_TAG_PID_LEN);
        pu1VlanFrame = CRU_BUF_Allocate_MsgBufChain (u4Len, 0);

        if (pu1VlanFrame == NULL)
        {
            WSSIF_TRC (WSSIF_FAILURE_TRC,
                       "WssIfProcessCfaMsg: Memory Allocation Failed\n");
            return OSIX_FAILURE;
        }
        if ((CRU_BUF_Copy_BufChains (pu1VlanFrame,
                                     pCfaMsgStruct->unCfaMsg.CfaDot11PktBuf.
                                     pTxPktBuf, 0, 0,
                                     u4PktSize)) != CRU_SUCCESS)
        {
            CRU_BUF_Release_MsgBufChain (pu1VlanFrame, 0);
            return OSIX_FAILURE;
        }

        VlanTagFrame (pu1VlanFrame,
                      pCfaMsgStruct->unCfaMsg.CfaDot11PktBuf.u2VlanId, 1);

        if (CfaPostPktFromWss (pu1VlanFrame,
                               pCfaMsgStruct->unCfaMsg.CfaDot11PktBuf.u4IfIndex,
                               (u4PktSize + VLAN_TAG_PID_LEN),
                               PACKET_HDLR_PROTO_DOT11,
                               CFA_ENCAP_ENETV2) != CFA_SUCCESS)
        {
            WSSIF_TRC (WSSIF_FAILURE_TRC,
                       "\r\nPosting packet from WSS to CFA failed \n");
            CRU_BUF_Release_MsgBufChain (pu1VlanFrame, 0);
            return OSIX_FAILURE;
        }

        CRU_BUF_Release_MsgBufChain (pCfaMsgStruct->unCfaMsg.CfaDot11PktBuf.
                                     pTxPktBuf, 0);
#else
        if (CfaPostPktFromWss (pCfaMsgStruct->unCfaMsg.CfaDot11PktBuf.pTxPktBuf,
                               pCfaMsgStruct->unCfaMsg.CfaDot11PktBuf.u4IfIndex,
                               u4PktSize,
                               PACKET_HDLR_PROTO_DOT11,
                               CFA_ENCAP_ENETV2) != CFA_SUCCESS)
        {
            WSSIF_TRC (WSSIF_FAILURE_TRC,
                       "\r\nPosting packet from WSS to CFA failed \n");
            return OSIX_FAILURE;
        }
#endif
    }
    else if (CfaProcessWssIfMsg ((UINT1) eMsgType, pCfaMsgStruct) !=
             OSIX_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfProcessCfaMsg: CfaProcessWssIfMsg returns failure\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/* Utility Functions */

INT4
WssCreateVlan (UINT4 u4IfIndex, UINT2 u2VlanId)
{
    UINT4               u4VlanId = u2VlanId;
    UINT4               u4ErrCode = 0;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    UINT4               u4PortId = 0;

    if (VlanSelectContext (0) != CFA_SUCCESS)
    {
        printf ("VlanSelectContext FAILED\r\n");
        return CFA_FAILURE;
    }

    if (nmhTestv2Dot1qVlanStaticRowStatus (&u4ErrCode, u4VlanId,
                                           VLAN_CREATE_AND_WAIT) ==
        SNMP_FAILURE)
    {
        printf ("CfaWssCreateVlan - Test VLAN_CREATE_AND_WAIT Failed\r\n");
        return CFA_FAILURE;
    }

    /* CREATE VLAN */
    if (nmhSetDot1qVlanStaticRowStatus (u4VlanId, VLAN_CREATE_AND_WAIT)
        == SNMP_FAILURE)
    {
        printf ("CfaWssCreateVlan - VLAN_CREATE Failed\r\n");
        return CFA_FAILURE;
    }

    if (nmhSetDot1qVlanStaticRowStatus (u4VlanId, VLAN_ACTIVE) == SNMP_FAILURE)
    {
        printf ("CfaWssCreateVlan - VLAN_ACTIVE Failed\r\n");
        return CFA_FAILURE;
    }

    if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPort) !=
        VLAN_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssCreateVlan :VlanGetContextInfoFromIfIndex returns failure\n");
        return CFA_FAILURE;
    }

    if (VlanUpdateDynamicVlanInfo (u4ContextId, u2VlanId, u2LocalPort, VLAN_ADD)
        != OSIX_SUCCESS)
    {
    }

    if (VlanUpdateDynamicVlanInfo (u4ContextId, 1, u2LocalPort, VLAN_DELETE)
        != OSIX_SUCCESS)
    {
        printf ("VlanUpdateDynamicVlanInfo - DELETE FAILED\r\n");
        return CFA_FAILURE;
    }

    if (VlanSelectContext (0) != CFA_SUCCESS)
    {
        printf ("VlanSelectContext FAILED\r\n");
        return CFA_FAILURE;
    }

    /* Set the PVID of the IfIndex */
    u4PortId = (UINT4) u2LocalPort;
    if (nmhTestv2Dot1qPvid (&u4ErrCode, (INT4) u4PortId, u4VlanId) ==
        SNMP_FAILURE)
    {
        printf ("nmhTestv2Dot1qPvid FAILED\r\n");
        return CFA_FAILURE;
    }

    if (nmhSetDot1qPvid ((INT4) u4PortId, u4VlanId) == SNMP_FAILURE)
    {
        printf ("nmhSetDot1qPvid FAILED\r\n");
        return CFA_FAILURE;
    }

    VlanPortOperInd (u4IfIndex, CFA_IF_UP);
    return CFA_SUCCESS;
}

INT4
WssDeleteVlan (UINT2 u2VlanId)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4VlanId = (UINT4) u2VlanId;

    if (VlanSelectContext (0) != CFA_SUCCESS)
    {
        return CFA_FAILURE;
    }

    if (nmhTestv2Dot1qVlanStaticRowStatus (&u4ErrCode, u4VlanId, VLAN_DESTROY)
        == SNMP_FAILURE)
    {
        VlanReleaseContext ();
        return CFA_FAILURE;
    }

    if (nmhSetDot1qVlanStaticRowStatus (u4VlanId, VLAN_DESTROY) == SNMP_FAILURE)
    {
        VlanReleaseContext ();
        return CFA_FAILURE;
    }

    VlanReleaseContext ();
    return CFA_SUCCESS;
}

INT4
WssCreateVlanInterface (UINT2 u2VlanId)
{
    tSNMP_OCTET_STRING_TYPE Alias;
    UINT4               u4IfType = CFA_L3IPVLAN;
    UINT4               u4IfIndex = 0;
    UINT1               au1VlanPrefix[5] = "vlan";
    UINT1               au1IfName[10] = { 0 };
    UINT4               u4L2CxtId = VCM_DEFAULT_CONTEXT;
    UINT4               u4ErrCode = 0;
    UINT4               u4IpAddr = 0x0B000001;
    UINT4               u4IpSubnetMask = 0xFF000000;

    SPRINTF ((CHR1 *) au1IfName, "%s%u", au1VlanPrefix, u2VlanId);

    Alias.i4_Length = (INT4) STRLEN (au1IfName);
    Alias.pu1_OctetList = au1IfName;

    /* u2VlanId = (UINT2) atol ((const CHR1 *) &au1IfName[4]); */

    /* Get Free Interface Index */
    if (CfaGetFreeInterfaceIndex (&u4IfIndex, u4IfType) != OSIX_SUCCESS)
    {
        printf ("CfaGetFreeInterfaceIndex FAILED\r\n");
        return CFA_FAILURE;
    }
    printf ("CfaWssCreateVlanInterface:: IfIndex = %d\r\n", u4IfIndex);

    if (nmhSetIfMainRowStatus ((INT4) u4IfIndex, RS_CREATEANDWAIT) ==
        SNMP_FAILURE)
    {
        printf
            ("CfaWssCreateVlanInterface:: nmhSetIfMainRowStatus 1 FAILED\r\n");
        return CFA_FAILURE;
    }

    /* Set the l2 context Id */
    if (VcmTestL2CxtId (&u4ErrCode, u4IfIndex, u4L2CxtId) == VCM_FAILURE)
    {
        VcmCfaDeleteIPIfaceMapping (u4IfIndex, CFA_TRUE);
        nmhSetIfMainRowStatus ((INT4) u4IfIndex, RS_DESTROY);
        printf ("CfaWssCreateVlanInterface:: VcmTestL2CxtId FAILED\r\n");
        return CFA_FAILURE;
    }

    if (VcmSetL2CxtId (u4IfIndex, u4L2CxtId) == VCM_FAILURE)
    {
        VcmCfaDeleteIPIfaceMapping (u4IfIndex, CFA_TRUE);
        nmhSetIfMainRowStatus ((INT4) u4IfIndex, RS_DESTROY);
        printf ("CfaWssCreateVlanInterface:: VcmSetL2CxtId FAILED\r\n");
        return CFA_FAILURE;
    }
    nmhSetIfAlias ((INT4) u4IfIndex, &Alias);

    /* Set the Type */
    if (nmhTestv2IfMainType (&u4ErrCode, (INT4) u4IfIndex, CFA_L3IPVLAN) ==
        SNMP_FAILURE)
    {
        VcmCfaDeleteIPIfaceMapping (u4IfIndex, CFA_TRUE);
        nmhSetIfMainRowStatus ((INT4) u4IfIndex, RS_DESTROY);
        printf ("CfaWssCreateVlanInterface:: nmhTestv2IfMainType FAILED\r\n");
        return CFA_FAILURE;
    }

    if (nmhSetIfMainType ((INT4) u4IfIndex, CFA_L3IPVLAN) == SNMP_FAILURE)
    {
        printf ("CfaWssCreateVlanInterface:: nmhSetIfMainType FAILED\r\n");
        return CFA_FAILURE;
    }

    /* All the mandatory parameters are set.
     * So make the row active */
    if (nmhSetIfMainRowStatus ((INT4) u4IfIndex, RS_ACTIVE) == SNMP_FAILURE)
    {
        printf ("CfaWssCreateVlanInterface:: nmhSetIfMainRowStatus FAILED\r\n");
        return CFA_FAILURE;
    }

    /* Set the IP Addr and Subnet Mask for the VLAN */
    if (nmhTestv2IfIpAddr (&u4ErrCode, (INT4) u4IfIndex, u4IpAddr) ==
        SNMP_FAILURE)
    {
        printf ("CfaWssCreateVlanInterface:: nmhTestv2IfIpAddr FAILED\r\n");
        return CFA_FAILURE;
    }

    /* Test Ip addr and Sub net mask */
    if (CfaTestIfIpAddrAndIfIpSubnetMask ((INT4) u4IfIndex,
                                          u4IpAddr, u4IpSubnetMask)
        == CLI_FAILURE)
    {
        printf
            ("CfaWssCreateVlanInterface:: CfaTestIfIpAddrAndIfIpSubnetMask FAILED\r\n");
        return CFA_FAILURE;
    }

    if (nmhSetIfIpAddr ((INT4) u4IfIndex, u4IpAddr) == SNMP_FAILURE)
    {
        printf ("CfaWssCreateVlanInterface:: nmhSetIfIpAddr FAILED\r\n");
        return CFA_FAILURE;
    }
    if (nmhSetIfIpSubnetMask ((INT4) u4IfIndex, u4IpSubnetMask) == SNMP_FAILURE)
    {
        printf ("CfaWssCreateVlanInterface:: nmhSetIfIpSubnetMask FAILED\r\n");
        return CFA_FAILURE;
    }

    if (CfaIfmHandleInterfaceStatusChange (u4IfIndex,
                                           CFA_IF_UP,
                                           CFA_IF_UP, CFA_FALSE) != CFA_SUCCESS)
    {
        printf ("CfaWssCreateVlanInterface::"
                "CfaIfmHandleInterfaceStatusChange FAILED\r\n");
        return CFA_FAILURE;
    }

    if (CfaSetIfOperStatus (u4IfIndex, CFA_IF_UP) != CFA_SUCCESS)
    {
        printf ("CfaWssCreateVlanInterface:: CfaSetIfOperStatus FAILED\r\n");
        return CFA_FAILURE;
    }

#ifdef LNXIP4_WANTED
    if (LinuxIpUpdateInterfaceStatus (au1IfName,
                                      CFA_L3IPVLAN, CFA_IF_UP) != OSIX_SUCCESS)
    {
        printf ("CfaWssCreateVlanInterface:: LinuxIpUpdIfaceStatus FAILED\r\n");
        return CFA_FAILURE;
    }
#endif

    printf ("CfaWssCreateVlanInterface:: SUCCESS\r\n");
    return CFA_SUCCESS;
}

#endif
