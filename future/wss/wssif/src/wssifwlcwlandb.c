
/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 *  $Id: wssifwlcwlandb.c,v 1.4 2017/11/24 10:37:10 siva Exp $
 *                                                                            *
 *  Description: This file contains the WSSWLAN util module APIs in WLC       *
 *                                                                            *
 ******************************************************************************/
#ifndef __WSSIFWLCWLANDB_C__
#define __WSSIFWLCWLANDB_C__

#include "wssifinc.h"

extern INT4         WssCfgSetLegacyRateStatus (UINT4 *);
extern INT4         WsscfgGetApGroupEnable (UINT4 *);
#include "wsswlanwlcproto.h"

/*****************************************************************************
 *  Function Name   : WlanWssIfIndexDbInit                                   *
 *  Description     : This function used to initialize the WssWlan session   *
 *                    profile RB Tree                                        *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 * **************************************************************************/
UINT1
WssWlanInit (VOID)
{
    WSSIF_FN_ENTRY ();
    if (WssifwlcwlanSizingMemCreateMemPools () != OSIX_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "\nwlanWssInit: MemPoolCreation Failed\r\n");
        return OSIX_FAILURE;

    }
    MEMSET (&gWssWlanGlobals, 0, sizeof (gWssWlanGlobals));
    /* Create RB tree for tWssWlanIfIndexDB */
    if (WssWlanIfIndexDBCreate () != OSIX_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "\nwlanWssInit: RB Tree Creation Failed for\
                tWssWlanIfIndexDB\r\n");
        return OSIX_FAILURE;
    }
    /* Create RB tree for tWssWlanBssInterfaceDB */
    if (WssWlanBssInterfaceDBCreate () != OSIX_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "wlanWssInit: RB Tree Creation Failed for \
                tWssWlanBssInterfaceDB\r\n");
        return OSIX_FAILURE;
    }

    /* Create RB tree for tWssWlanBssIfIndexDB */
    if (WssWlanBssIfIndexDBCreate () != OSIX_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "wlanWssInit: RB Tree Creation Failed for \
                tWssWlanBssIfIndexDB\r\n");
        return OSIX_FAILURE;
    }
    /* Create RB tree for tWssWlanBssIdMappingDB */
    if (WssWlanBssIdMappingDBCreate () != OSIX_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "wlanWssInit: RB Tree Creation Failed for \
                tWssWlanBssIdMappingDB\r\n");
        return OSIX_FAILURE;
    }
    if (WssWlanSSIDMappingDBCreate () != OSIX_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "wlanWssInit: RB Tree Creation Failed for\
                tWssWlanSSIDMappingDB\r\n");
        return OSIX_FAILURE;

    }

    /* Initialize Array */
    MEMSET (&gaWssWlanInternalProfileDB, 0, sizeof (tWssWlanInternalProfileDB));
    /* Initialize Array */
    MEMSET (&gaWssWlanProfileDB, 0, sizeof (tWssWlanProfileDB));

    MEMSET (gWssWlanGlobals.WssWlanGlbMib.au1ManagmentSSID,
            0, WSSWLAN_SSID_NAME_LEN);
    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : WssWlanIfIndexDBRBCmp                                */
/*                                                                           */
/* Description        : This function compares the two WLAN  Interface Index */
/*                       RB Tree's based on the Wlan interface index         */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 , 1 or -1                                          */
/*****************************************************************************/
INT4
WssWlanIfIndexDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tWssWlanIfIndexDB  *pNode1 = e1;
    tWssWlanIfIndexDB  *pNode2 = e2;

    WSSIF_FN_ENTRY ();

    if (pNode1->u4WlanIfIndex > pNode2->u4WlanIfIndex)
    {
        return 1;
    }
    else if (pNode1->u4WlanIfIndex < pNode2->u4WlanIfIndex)
    {
        return -1;
    }
    WSSIF_FN_EXIT ();
    return 0;
}

/*****************************************************************************/
/* Function Name      : WssWlanBssIfIndexDBRBCmp                             */
/*                                                                           */
/* Description        : This function compares the two WLAN BSSInterfaceIndex*/
/*                       RB Tree's based on the BSS interface index          */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 , 1 or -1                                          */
/*****************************************************************************/
INT4
WssWlanBssIfIndexDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tWssWlanBssIfIndexDB *pNode1 = e1;
    tWssWlanBssIfIndexDB *pNode2 = e2;

    WSSIF_FN_ENTRY ();
    if (pNode1->u4BssIfIndex > pNode2->u4BssIfIndex)
    {
        return 1;
    }
    else if (pNode1->u4BssIfIndex < pNode2->u4BssIfIndex)
    {
        return -1;
    }
    WSSIF_FN_EXIT ();
    return 0;
}

/*****************************************************************************/
/* Function Name      : WssWlanBssInterfaceDBRBCmp                           */
/*                                                                           */
/* Description        : This function compares the two WLAN  Interface Index */
/*                       RB Tree's based on the Wlan interface index         */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 , 1 or -1                                          */
/*****************************************************************************/
INT4
WssWlanBssInterfaceDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tWssWlanBssInterfaceDB *pNode1 = e1;
    tWssWlanBssInterfaceDB *pNode2 = e2;

    WSSIF_FN_ENTRY ();

    if (pNode1->u4RadioIfIndex > pNode2->u4RadioIfIndex)
    {
        return 1;
    }
    else if (pNode1->u4RadioIfIndex < pNode2->u4RadioIfIndex)
    {
        return -1;
    }
    if (pNode1->u2WlanProfileId > pNode2->u2WlanProfileId)
    {
        return 1;
    }
    else if (pNode1->u2WlanProfileId < pNode2->u2WlanProfileId)
    {
        return -1;
    }
    WSSIF_FN_EXIT ();
    return 0;
}

/*****************************************************************************/
/* Function Name      : WssWlanBssIdMappingDBRBCmp                           */
/*                                                                           */
/* Description        : This function compares the two WLAN BSSID Mapping DB */
/*                       RB Tree's based on the BSS Mac Address              */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 , 1 or -1                                          */
/*****************************************************************************/
INT4
WssWlanBssIdMappingDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tWssWlanBssIdMappingDB *pNode1 = e1;
    tWssWlanBssIdMappingDB *pNode2 = e2;

    INT4                i4RetVal = 0;

    WSSIF_FN_ENTRY ();
    i4RetVal = MEMCMP (pNode1->BssId, pNode2->BssId, WSS_WLAN_MAC_SIZE);

    if (i4RetVal > 0)
    {
        return 1;
    }
    else if (i4RetVal < 0)
    {
        return -1;
    }
    WSSIF_FN_EXIT ();
    return 0;
}

/*****************************************************************************/
/* Function Name      : WssWlanSsidMappingDBRBCmp                            */
/*                                                                           */
/* Description        : This function compares the two WLAN BSS ID MappingDB */
/*                       RB Tree's based on the BSS Mac Address              */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 0 , 1 or -1                                          */
/*****************************************************************************/
INT4
WssWlanSSIDMappingDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tWssWlanSSIDMappingDB *pNode1 = e1;
    tWssWlanSSIDMappingDB *pNode2 = e2;

    INT4                i4RetVal = 0;

    WSSIF_FN_ENTRY ();
    i4RetVal = MEMCMP (pNode1->au1SSID, pNode2->au1SSID, WSSWLAN_SSID_NAME_LEN);

    if (i4RetVal > 0)
    {
        return 1;
    }
    else if (i4RetVal < 0)
    {
        return -1;
    }
    WSSIF_FN_EXIT ();
    return 0;
}

/****************************************************************************
 *  Function    :  WssWlanBssIfIndexDBCreate
 *  Input       :  None
 *  Description :  This function creates the WssWlanBssIfIndexDB
 *                 i.e. an RBTree.
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/

PUBLIC UINT1
WssWlanBssIfIndexDBCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    WSSIF_FN_ENTRY ();
    u4RBNodeOffset = FSAP_OFFSETOF (tWssWlanBssIfIndexDB, BssIfIndexDBNode);

    if ((gWssWlanGlobals.WssWlanGlbMib.WssWlanBssIfIndexDB =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               WssWlanBssIfIndexDBRBCmp)) == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "WssWlanBssIfIndexDBCreate failed\r\n");
        return OSIX_FAILURE;
    }
    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 *  Function    :  WssWlanIfIndexDBCreate
 *  Input       :  None
 *  Description :  This function creates the WssWlanIfIndexDB
 *                 i.e. an RBTree.
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/

PUBLIC UINT1
WssWlanIfIndexDBCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    WSSIF_FN_ENTRY ();
    u4RBNodeOffset = FSAP_OFFSETOF (tWssWlanIfIndexDB, IfIndexDBNode);

    if ((gWssWlanGlobals.WssWlanGlbMib.WssWlanIfIndexDB =
         RBTreeCreateEmbedded (u4RBNodeOffset, WssWlanIfIndexDBRBCmp)) == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "WssWlanIfIndexDBCreate failed\r\n");
        return OSIX_FAILURE;
    }
    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 *  Function    :  WssWlanBssInterfaceDBCreate
 *  Input       :  None
 *  Description :  This function creates the WssWlanBssInterfaceDB
 *                 i.e. an RBTree.
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/

PUBLIC UINT1
WssWlanBssInterfaceDBCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    WSSIF_FN_ENTRY ();
    u4RBNodeOffset = FSAP_OFFSETOF (tWssWlanBssInterfaceDB, BssInterfaceDBNode);

    if ((gWssWlanGlobals.WssWlanGlbMib.WssWlanBssInterfaceDB =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               WssWlanBssInterfaceDBRBCmp)) == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "WssWlanBssInterfaceDBCreate failed\r\n");
        return OSIX_FAILURE;
    }
    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 *  Function    :  WssWlanBssIdMappingDBCreate
 *  Input       :  None
 *  Description :  This function creates the WssWlanBssIdMappingDB
 *                 i.e. an RBTree.
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/

PUBLIC UINT1
WssWlanBssIdMappingDBCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    WSSIF_FN_ENTRY ();
    u4RBNodeOffset = FSAP_OFFSETOF (tWssWlanBssIdMappingDB, BssIdMappingDBNode);

    if ((gWssWlanGlobals.WssWlanGlbMib.WssWlanBssIdMappingDB =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               WssWlanBssIdMappingDBRBCmp)) == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "WssWlanBssIdMappingDBCreate failed\r\n");
        return OSIX_FAILURE;
    }
    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 *  Function    :  WssWlanSSIDMappingDBCreate
 *  Input       :  None
 *  Description :  This function creates the WssWlanSSIdMappingDB
 *                 i.e. an RBTree.
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/

PUBLIC UINT1
WssWlanSSIDMappingDBCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    WSSIF_FN_ENTRY ();
    u4RBNodeOffset = FSAP_OFFSETOF (tWssWlanSSIDMappingDB, SsidMappingDBNode);

    if ((gWssWlanGlobals.WssWlanGlbMib.WssWlanSSIDMappingDB =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               WssWlanSSIDMappingDBRBCmp)) == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "WssWlanSSIDMappingDBCreate failed\r\n");
        return OSIX_FAILURE;
    }
    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 *  Function    :  WssIfProcessWssWlanDBMsg
 *  Input       :  Pointer to tWssWlanDB , OpCode
 *  Description :  Invoke the correponding DB Table to set/get DB information
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
UINT1
WssIfProcessWssWlanDBMsg (UINT1 u1OpCode, tWssWlanDB * pWssWlanDB)
{
    tWssWlanIfIndexDB   WssWlanIfIndexDB;
    tWssWlanIfIndexDB  *pWssWlanIfIndexDB = NULL;
    tWssWlanBssIfIndexDB WssWlanBssIfIndexDB;
    tWssWlanBssIfIndexDB *pWssWlanBssIfIndexDB = NULL;
    tWssWlanBssInterfaceDB WssWlanBssInterfaceDB;
    tWssWlanBssInterfaceDB *pWssWlanBssInterfaceDB = NULL;
    tWssWlanSSIDMappingDB WssWlanSSIDMappingDB;
    tWssWlanSSIDMappingDB *pWssWlanSSIDMappingDB = NULL;
    tWssWlanBssIdMappingDB WssWlanBssIdMappingDB;
    tWssWlanBssIdMappingDB *pWssWlanBssIdMappingDB = NULL;
    tMacAddr            NullMac = { 0, 0, 0, 0, 0, 0 };
    tRadioIfDB          RadioIfDB;
    tRadioIfDB         *pRadioIfDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2WlanInternalId = 0;
    UINT2               u2WlanProfileId = 0;
    UINT1              *pu1WssWlanBssId = NULL;
    UINT1              
        au1RedirectFileName[WSS_WLAN_DEFAULT_REDIRECT_FILE_LENGTH] =
        "wlan_web_auth_login.html";
    INT4                i4RedirectFileNameLength = 0;
    UINT4               u4ApGroupEnabled = 0;

    MEMSET (&WssWlanIfIndexDB, 0, sizeof (tWssWlanIfIndexDB));
    MEMSET (&WssWlanBssIfIndexDB, 0, sizeof (tWssWlanBssIfIndexDB));
    MEMSET (&WssWlanBssInterfaceDB, 0, sizeof (tWssWlanBssInterfaceDB));
    MEMSET (&WssWlanSSIDMappingDB, 0, sizeof (tWssWlanSSIDMappingDB));
    MEMSET (&WssWlanBssIdMappingDB, 0, sizeof (tWssWlanBssIdMappingDB));
    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    i4RedirectFileNameLength = STRLEN (au1RedirectFileName);
    WSSIF_FN_ENTRY ();
    WSSIF_LOCK;

    switch (u1OpCode)
    {
        case WSS_WLAN_CREATE_PROFILE_ENTRY:

            if (pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "\nWssIfProcessWssWlanDBMsg:\
                        Invalid Profile Id, create failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;

            }
            u2WlanProfileId =
                (UINT2) (pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId - 1);

            if (gaWssWlanProfileDB[u2WlanProfileId].u2WlanInternalId != 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "\nWssIfProcessWssWlanDBMsg:\
                        Profile Entry exist, create failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            else
            {
                gaWssWlanProfileDB[u2WlanProfileId].u2WlanInternalId =
                    pWssWlanDB->WssWlanAttributeDB.u2WlanInternalId;
                u2WlanInternalId =
                    (UINT2) (pWssWlanDB->WssWlanAttributeDB.u2WlanInternalId -
                             1);
                gaWssWlanInternalProfileDB[u2WlanInternalId].u2WlanProfileId =
                    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId;
                gaWssWlanInternalProfileDB[u2WlanInternalId].u1RowStatus =
                    WSSWLAN_RS_NOT_READY;
            }
            break;

        case WSS_WLAN_DESTROY_PROFILE_ENTRY:
            if (pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "\nWssIfProcessWssWlanDBMsg:\
                        Invalid Profile Id, delete failed\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;

            }

            u2WlanProfileId =
                (UINT2) (pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId - 1);

            if (gaWssWlanProfileDB[u2WlanProfileId].u2WlanInternalId == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "\nWssIfProcessWssWlanDBMsg:\
                        Entry for key=0 not found. Deletion failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;

            }

            u2WlanInternalId = (UINT2) (gaWssWlanProfileDB[u2WlanProfileId].
                                        u2WlanInternalId - 1);

            gaWssWlanInternalProfileDB[u2WlanInternalId].pWssWlanIfIndexDB
                = NULL;
            gaWssWlanInternalProfileDB[u2WlanInternalId].
                u1WlanRadioMappingCount = 0;
            /* 0, 1 and 2 are valid values for Mac Type */
            gaWssWlanInternalProfileDB[u2WlanInternalId].u1MacType =
                WSSWLAN_MAC_EMPTY;
            gaWssWlanInternalProfileDB[u2WlanInternalId].u1TunnelMode =
                WSSWLAN_TUNNEL_MODE_BRIDGE;
            gaWssWlanInternalProfileDB[u2WlanInternalId].u1RowStatus = 0;
            gaWssWlanInternalProfileDB[u2WlanInternalId].u2WlanProfileId = 0;

            gaWssWlanProfileDB[u2WlanProfileId].u2WlanInternalId = 0;

            break;

        case WSS_WLAN_GET_PROFILE_ENTRY:
            if (pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId != 0)
            {
                u2WlanProfileId = (UINT2) (pWssWlanDB->WssWlanAttributeDB.
                                           u2WlanProfileId - 1);
                u2WlanInternalId = (UINT2) (gaWssWlanProfileDB[u2WlanProfileId].
                                            u2WlanInternalId - 1);

            }

            else if (pWssWlanDB->WssWlanAttributeDB.u2WlanInternalId != 0)
            {
                u2WlanInternalId =
                    (UINT2) (pWssWlanDB->WssWlanAttributeDB.u2WlanInternalId -
                             1);
            }
            else
            {

                WSSIF_TRC (WSSIF_FAILURE_TRC, "\r\nWssIfProcessWssWlanDBMsg:\
                        Invalid Profile Id and Internal Id, No proper Key\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;

            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u2WlanInternalId =
                    gaWssWlanProfileDB[u2WlanProfileId].u2WlanInternalId;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanMacType != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1MacType =
                    gaWssWlanInternalProfileDB[u2WlanInternalId].u1MacType;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanTunnelMode != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1TunnelMode =
                    gaWssWlanInternalProfileDB[u2WlanInternalId].u1TunnelMode;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bRowStatus != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1RowStatus =
                    gaWssWlanInternalProfileDB[u2WlanInternalId].u1RowStatus;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanRadioMappingCount
                != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1WlanRadioMappingCount =
                    gaWssWlanInternalProfileDB[u2WlanInternalId].
                    u1WlanRadioMappingCount;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex != OSIX_FALSE)
            {
                if (gaWssWlanInternalProfileDB[u2WlanInternalId].
                    pWssWlanIfIndexDB != NULL)
                {
                    pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex =
                        gaWssWlanInternalProfileDB[u2WlanInternalId].
                        pWssWlanIfIndexDB->u4WlanIfIndex;
                }
            }
            break;

        case WSS_WLAN_SET_PROFILE_ENTRY:
            if (pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId != 0)
            {
                u2WlanProfileId =
                    (UINT2) (pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId -
                             1);
                u2WlanInternalId =
                    (UINT2) (gaWssWlanProfileDB[u2WlanProfileId].
                             u2WlanInternalId - 1);

            }
            else if (pWssWlanDB->WssWlanAttributeDB.u2WlanInternalId != 0)
            {
                u2WlanInternalId =
                    (UINT2) (pWssWlanDB->WssWlanAttributeDB.u2WlanInternalId -
                             1);
            }
            else
            {

                WSSIF_TRC (WSSIF_FAILURE_TRC, "\r\nWssIfProcessWssWlanDBMsg:\
                        Invalid Profile Id and Internal Id, No proper Key\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;

            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanMacType != OSIX_FALSE)
            {
                if ((gaWssWlanInternalProfileDB[u2WlanInternalId].u1RowStatus
                     == WSSWLAN_RS_ACTIVE) &&
                    (pWssWlanDB->WssWlanAttributeDB.u1RowStatus
                     == WSSWLAN_RS_ACTIVE))
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC, "\nWssIfProcessWssWlanDBMsg:\
                            WlanProfile RowStatus id Active,\
                            cannot set MacType\n");
                    WSSIF_UNLOCK;
                    return OSIX_FAILURE;

                }
                gaWssWlanInternalProfileDB[u2WlanInternalId].u1MacType =
                    pWssWlanDB->WssWlanAttributeDB.u1MacType;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanTunnelMode != OSIX_FALSE)
            {
                if ((gaWssWlanInternalProfileDB[u2WlanInternalId].u1RowStatus
                     == WSSWLAN_RS_ACTIVE) &&
                    (pWssWlanDB->WssWlanAttributeDB.u1RowStatus
                     == WSSWLAN_RS_ACTIVE))
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC, "\nWssIfProcessWssWlanDBMsg:\
                            WlanProfile RowStatus id Active,\
                            cannot set TunnelMode\n");
                    WSSIF_UNLOCK;
                    return OSIX_FAILURE;

                }

                gaWssWlanInternalProfileDB[u2WlanInternalId].u1TunnelMode =
                    pWssWlanDB->WssWlanAttributeDB.u1TunnelMode;
            }
            if ((pWssWlanDB->WssWlanIsPresentDB.bRowStatus != OSIX_FALSE) &&
                (pWssWlanDB->WssWlanAttributeDB.u1RowStatus ==
                 WSSWLAN_RS_ACTIVE))
            {
                if (gaWssWlanInternalProfileDB[u2WlanInternalId].u1MacType ==
                    WSSWLAN_MAC_EMPTY)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC, "\nWssIfProcessWssWlanDBMsg:\
                            TunnelMode or MacType is empty,\
                            cannot set RowStatus\n");
                    WSSIF_UNLOCK;
                    return OSIX_FAILURE;
                }
                gaWssWlanInternalProfileDB[u2WlanInternalId].u1RowStatus =
                    pWssWlanDB->WssWlanAttributeDB.u1RowStatus;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanRadioMappingCount
                != OSIX_FALSE)
            {
                gaWssWlanInternalProfileDB[u2WlanInternalId].
                    u1WlanRadioMappingCount =
                    pWssWlanDB->WssWlanAttributeDB.u1WlanRadioMappingCount;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex != OSIX_FALSE)
            {
                if (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex == 0)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC,
                               "\nWssIfProcessWssWlanDBMsg:Index missing for\
                            WLAN Profile\n");
                    WSSIF_UNLOCK;
                    return OSIX_FAILURE;
                }
                else
                {
                    MEMSET (&WssWlanIfIndexDB, 0, sizeof (tWssWlanIfIndexDB));
                    WssWlanIfIndexDB.u4WlanIfIndex =
                        pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
                    pWssWlanIfIndexDB =
                        RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.
                                   WssWlanIfIndexDB,
                                   (tRBElem *) & WssWlanIfIndexDB);

                    if (pWssWlanIfIndexDB == NULL)
                    {

                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "\nWssIfProcessWssWlanDBMsg:RBTreeGet from \
                                WssWlanIfIndexDB returned NULL\r\n");
                        WSSIF_UNLOCK;
                        return OSIX_FAILURE;
                    }
                    gaWssWlanInternalProfileDB[u2WlanInternalId].
                        pWssWlanIfIndexDB = pWssWlanIfIndexDB;
                }
            }

            break;

        case WSS_WLAN_CREATE_IFINDEX_ENTRY:
            if (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "\nWssIfProcessWssWlanDBMsg:IfIndex is empty ,\
                        creation failed \r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            if ((pWssWlanIfIndexDB =
                 (tWssWlanIfIndexDB *)
                 MemAllocMemBlk (WSSWLAN_IF_INDEX_DB_POOLID)) == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "\nWssIfProcessWssWlanDBMsg:Memory allocation\
                        failed , return failure \r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            MEMSET (pWssWlanIfIndexDB, 0, sizeof (tWssWlanIfIndexDB));
            pWssWlanIfIndexDB->u4WlanIfIndex =
                pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
            MEMCPY (pWssWlanIfIndexDB->au1FsDot11RedirectFileName,
                    au1RedirectFileName, i4RedirectFileNameLength);
            pWssWlanIfIndexDB->u2WlanSnoopTableLength
                = DEFAULT_MULTICAST_TABLE_LENGTH;
            pWssWlanIfIndexDB->u4WlanSnoopTimer = DEFAULT_MULTICAST_SNOOP_TIMER;
            pWssWlanIfIndexDB->u4WlanSnoopTimeout
                = DEFAULT_MULTICAST_SNOOP_TIMEOUT;
            pWssWlanIfIndexDB->u4LoginAuthMode = LOCAL_LOGIN;
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId != OSIX_FALSE)
            {
                u2WlanProfileId =
                    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId;
                if (u2WlanProfileId == 0)
                {
                    MemReleaseMemBlock (WSSWLAN_IF_INDEX_DB_POOLID,
                                        (UINT1 *) pWssWlanIfIndexDB);
                    WSSIF_TRC (WSSIF_FAILURE_TRC,
                               "\nWssIfProcessWssWlanDBMsg:Profile Entry not \
                            found. Create index failed \r\n");
                    WSSIF_UNLOCK;
                    return OSIX_FAILURE;

                }
                else
                {
                    pWssWlanIfIndexDB->u2WlanInternalId =
                        gaWssWlanProfileDB[u2WlanProfileId -
                                           1].u2WlanInternalId;
                }
            }

            if (RBTreeAdd (gWssWlanGlobals.WssWlanGlbMib.WssWlanIfIndexDB,
                           (tRBElem *) pWssWlanIfIndexDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (WSSWLAN_IF_INDEX_DB_POOLID,
                                    (UINT1 *) pWssWlanIfIndexDB);
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "\nWssIfProcessWssWlanDBMsg:RBTreeAdd failed ,\
                        return failure \r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            /* Allocate the default bandwidth threshold for WLAN */
            pWssWlanIfIndexDB->u4BandwidthThresh =
                WSS_WLAN_DEF_BANDWIDTH_THRESH;
            pWssWlanIfIndexDB->u1AuthMethod = WSS_WLAN_AUTH_ALGO_OPEN;
            pWssWlanIfIndexDB->u1MitigationRequirement =
                WSS_WLAN_POWER_CONSTRAINT;
            pWssWlanIfIndexDB->i2DtimPeriod = WSS_WLAN_DEFAULT_DTIM;
#ifdef BAND_SELECT_WANTED
            pWssWlanIfIndexDB->u1BandSelectStatus = WSS_WLAN_DEF_BANDSELECT;
            pWssWlanIfIndexDB->u1AgeOutSuppression =
                WSS_WLAN_DEF_AGEOUT_SUPPRESION;
            pWssWlanIfIndexDB->u1AssocRejectCount =
                WSS_WLAN_DEF_ASSOC_REJ_COUNT;
            pWssWlanIfIndexDB->u1AssocResetTime =
                WSS_WLAN_DEF_ASSOC_COUNT_FLUSH;
#endif
            break;

        case WSS_WLAN_DESTROY_IFINDEX_ENTRY:
            if (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "\nWssIfProcessWssWlanDBMsg:IfIndex is empty ,\
                        deletion failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            WssWlanIfIndexDB.u4WlanIfIndex =
                pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
            pWssWlanIfIndexDB =
                RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.WssWlanIfIndexDB,
                           (tRBElem *) & WssWlanIfIndexDB);

            if (pWssWlanIfIndexDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "\nWssIfProcessWssWlanDBMsg:RBTreeGet failed ,\
                        return failure \r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;

            }

            RBTreeRem (gWssWlanGlobals.WssWlanGlbMib.WssWlanIfIndexDB,
                       &WssWlanIfIndexDB);
            MemReleaseMemBlock (WSSWLAN_IF_INDEX_DB_POOLID,
                                (UINT1 *) pWssWlanIfIndexDB);
            break;
        case WSS_WLAN_GET_IFINDEX_ENTRY:
            MEMSET (&WssWlanIfIndexDB, 0, sizeof (tWssWlanIfIndexDB));

            if (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "\nWssIfProcessWssWlanDBMsg:\
                        Invalid index received\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            WssWlanIfIndexDB.u4WlanIfIndex =
                pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
            pWssWlanIfIndexDB =
                RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.WssWlanIfIndexDB,
                           (tRBElem *) & WssWlanIfIndexDB);

            if (pWssWlanIfIndexDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:No\
                        entry found for WlanIfIndex\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u2WlanInternalId =
                    pWssWlanIfIndexDB->u2WlanInternalId;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanMulticastMode != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u2WlanMulticastMode =
                    pWssWlanIfIndexDB->u2WlanMulticastMode;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanSnoopTableLength !=
                OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u2WlanSnoopTableLength =
                    pWssWlanIfIndexDB->u2WlanSnoopTableLength;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanSnoopTimer != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4WlanSnoopTimer =
                    pWssWlanIfIndexDB->u4WlanSnoopTimer;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanSnoopTimeout != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4WlanSnoopTimeout =
                    pWssWlanIfIndexDB->u4WlanSnoopTimeout;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId =
                    gaWssWlanInternalProfileDB[pWssWlanIfIndexDB->
                                               u2WlanInternalId -
                                               1].u2WlanProfileId;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanIfType != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1WlanIfType =
                    pWssWlanIfIndexDB->u1WlanIfType;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bAdminStatus != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1AdminStatus =
                    pWssWlanIfIndexDB->u1AdminStatus;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bVlanId != OSIX_FALSE)
            {

                pWssWlanDB->WssWlanAttributeDB.u2VlanId =
                    pWssWlanIfIndexDB->u2VlanId;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bKeyStatus != OSIX_FALSE)
            {

                pWssWlanDB->WssWlanAttributeDB.u1KeyStatus =
                    pWssWlanIfIndexDB->u1KeyStatus;

            }
            if (pWssWlanDB->WssWlanIsPresentDB.bKeyType != OSIX_FALSE)
            {

                pWssWlanDB->WssWlanAttributeDB.u1KeyType =
                    pWssWlanIfIndexDB->u1KeyType;

            }
            if (pWssWlanDB->WssWlanIsPresentDB.bKeyIndex != OSIX_FALSE)
            {

                pWssWlanDB->WssWlanAttributeDB.u1KeyIndex =
                    pWssWlanIfIndexDB->u1KeyIndex;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWepKey != OSIX_FALSE)
            {
                MEMCPY (pWssWlanDB->WssWlanAttributeDB.au1WepKey,
                        pWssWlanIfIndexDB->au1WepKey,
                        STRLEN (pWssWlanIfIndexDB->au1WepKey));
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bDtimPeriod != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.i2DtimPeriod =
                    pWssWlanIfIndexDB->i2DtimPeriod;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bGroupTsc != OSIX_FALSE)
            {
                MEMCPY ((pWssWlanDB->WssWlanAttributeDB.au1GroupTsc),
                        (pWssWlanIfIndexDB->au1GroupTsc),
                        WSS_WLAN_GROUP_TSC_LEN);
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bDesiredSsid != OSIX_FALSE)
            {
                MEMCPY ((pWssWlanDB->WssWlanAttributeDB.au1DesiredSsid),
                        (pWssWlanIfIndexDB->au1DesiredSsid),
                        WSSWLAN_SSID_NAME_LEN);
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bAuthenticationIndex
                != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1AuthenticationIndex =
                    pWssWlanIfIndexDB->u1AuthenticationIndex;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bAuthAlgorithm != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1AuthAlgorithm =
                    pWssWlanIfIndexDB->u1AuthAlgorithm;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWebAuthStatus != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1WebAuthStatus =
                    pWssWlanIfIndexDB->u1WebAuthStatus;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bAuthMethod != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1AuthMethod =
                    pWssWlanIfIndexDB->u1AuthMethod;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bQosProfileId != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1QosProfileId =
                    pWssWlanIfIndexDB->u1QosProfileId;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bCapabilityId != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1CapabilityId =
                    pWssWlanIfIndexDB->u1CapabilityId;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bMaxClientCount != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4MaxClientCount =
                    pWssWlanIfIndexDB->u4MaxClientCount;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bEncryptDecryptCapab
                != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1EncryptDecryptCapab =
                    pWssWlanIfIndexDB->u1EncryptDecryptCapab;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bPowerConstraint != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1PowerConstraint =
                    pWssWlanIfIndexDB->u1PowerConstraint;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bSupressSsid != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1SupressSsid =
                    pWssWlanIfIndexDB->u1SupressSsid;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bExternalWebAuthMethod !=
                OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1ExternalWebAuthMethod =
                    pWssWlanIfIndexDB->u1ExternalWebAuthMethod;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bExternalWebAuthUrl !=
                OSIX_FALSE)
            {
                MEMCPY (pWssWlanDB->WssWlanAttributeDB.au1ExternalWebAuthUrl,
                        pWssWlanIfIndexDB->au1ExternalWebAuthUrl,
                        WSS_WLAN_WEBAUTH_URL_LENGTH);
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bSsidIsolation != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1SsidIsolation =
                    pWssWlanIfIndexDB->u1SsidIsolation;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bCfPollable != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1CfPollable =
                    pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.u1CfPollable;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bCfPollRequest != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1CfPollRequest =
                    pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1CfPollRequest;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bPrivacyOptionImplemented
                != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1PrivacyOptionImplemented =
                    pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1PrivacyOptionImplemented;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bShortPreambleOptionImplemented
                != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.
                    u1ShortPreambleOptionImplemented =
                    pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1ShortPreambleOptionImplemented;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bPBCCOptionImplemented
                != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1PBCCOptionImplemented =
                    pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1PBCCOptionImplemented;

            }
            if (pWssWlanDB->WssWlanIsPresentDB.bChannelAgilityPresent
                != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1ChannelAgilityPresent =
                    pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1ChannelAgilityPresent;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bSpectrumManagementRequired
                != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1SpectrumManagementRequired =
                    pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1SpectrumManagementRequired;

            }
            if (pWssWlanDB->WssWlanIsPresentDB.bQosOptionImplemented
                != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1QosOptionImplemented =
                    pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1QosOptionImplemented;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bShortSlotTimeOptionImplemented
                != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1ShortSlotTimeOptionImplemented =
                    pWssWlanDB->WssWlanAttributeDB.
                    u1ShortSlotTimeOptionImplemented;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bAPSDOptionImplemented
                != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1APSDOptionImplemented =
                    pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1APSDOptionImplemented;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bDSSSOFDMOptionEnabled
                != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1DSSSOFDMOptionEnabled =
                    pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1DSSSOFDMOptionEnabled;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bDelayedBlockAckOptionImplemented
                != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.
                    u1DelayedBlockAckOptionImplemented =
                    pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1DelayedBlockAckOptionImplemented;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.
                bImmediateBlockAckOptionImplemented != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.
                    u1ImmediateBlockAckOptionImplemented =
                    pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1ImmediateBlockAckOptionImplemented;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQAckOptionImplemented
                != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1QAckOptionImplemented =
                    pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1QAckOptionImplemented;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bQueueRequestOptionImplemented
                != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1QueueRequestOptionImplemented
                    =
                    pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1QueueRequestOptionImplemented;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bTXOPRequestOptionImplemented
                != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1TXOPRequestOptionImplemented
                    =
                    pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1TXOPRequestOptionImplemented;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bRSNAPreauthenticationImplemented
                != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.
                    u1RSNAPreauthenticationImplemented =
                    pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1RSNAPreauthenticationImplemented;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bRSNAOptionImplemented
                != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1RSNAOptionImplemented =
                    pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1RSNAOptionImplemented;
            }
#ifdef WPS_WANTED
            if (pWssWlanDB->WssWlanIsPresentDB.bWPSEnabled != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1WPSEnabled =
                    pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.u1WPSEnabled;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWPSAdditionalIE != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1WPSAdditionalIE =
                    pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1WPSAdditionalIE;
            }
#endif

            if (pWssWlanDB->WssWlanIsPresentDB.bPassengerTrustMode
                != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1PassengerTrustMode =
                    pWssWlanIfIndexDB->u1PassengerTrustMode;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bQosTraffic != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1QosTraffic =
                    pWssWlanIfIndexDB->u1QosTraffic;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bMitigationRequirement
                != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1MitigationRequirement =
                    pWssWlanIfIndexDB->u1MitigationRequirement;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosRateLimit != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1QosRateLimit =
                    pWssWlanIfIndexDB->u1QosRateLimit;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamCIR != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamCIR =
                    pWssWlanIfIndexDB->u4QosUpStreamCIR;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamCBS != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamCBS =
                    pWssWlanIfIndexDB->u4QosUpStreamCBS;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamEIR != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamEIR =
                    pWssWlanIfIndexDB->u4QosUpStreamEIR;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamEBS != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamEBS =
                    pWssWlanIfIndexDB->u4QosUpStreamEBS;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamCIR != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamCIR =
                    pWssWlanIfIndexDB->u4QosDownStreamCIR;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamCBS != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamCBS =
                    pWssWlanIfIndexDB->u4QosDownStreamCBS;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamEIR != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamEIR =
                    pWssWlanIfIndexDB->u4QosDownStreamEIR;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamEBS != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamEBS =
                    pWssWlanIfIndexDB->u4QosDownStreamEBS;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bCapability != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u2Capability =
                    pWssWlanIfIndexDB->u2Capability;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWssWlanQosCapab != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.WssWlanQosCapab.u1QosInfo =
                    pWssWlanIfIndexDB->WssWlanQosCapab.u1QosInfo;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWssWlanEdcaParam != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.WssWlanEdcaParam.u1QosInfo =
                    pWssWlanIfIndexDB->WssWlanEdcaParam.u1QosInfo;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bBandwidthThresh != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4BandwidthThresh =
                    pWssWlanIfIndexDB->u4BandwidthThresh;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bFsDot11RedirectFileName !=
                OSIX_FALSE)
            {
                i4RedirectFileNameLength =
                    STRLEN (pWssWlanIfIndexDB->au1FsDot11RedirectFileName);
                MEMCPY (pWssWlanDB->WssWlanAttributeDB.
                        au1FsDot11RedirectFileName,
                        pWssWlanIfIndexDB->au1FsDot11RedirectFileName,
                        i4RedirectFileNameLength);
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bFsLoginAuthMode != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4LoginAuthMode =
                    pWssWlanIfIndexDB->u4LoginAuthMode;
            }

#ifdef BAND_SELECT_WANTED
            if (pWssWlanDB->WssWlanIsPresentDB.bBandSelectStatus != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1BandSelectStatus =
                    pWssWlanIfIndexDB->u1BandSelectStatus;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bAgeOutSuppression != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1AgeOutSuppression =
                    pWssWlanIfIndexDB->u1AgeOutSuppression;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bAssocRejectCount != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1AssocRejectCount =
                    pWssWlanIfIndexDB->u1AssocRejectCount;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bAssocResetTime != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1AssocResetTime =
                    pWssWlanIfIndexDB->u1AssocResetTime;
            }

#endif
            break;
        case WSS_WLAN_SET_IFINDEX_ENTRY:
            MEMSET (&WssWlanIfIndexDB, 0, sizeof (tWssWlanIfIndexDB));

            if (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "\n Invalid index received.\
                        for WSS_WLAN_SET_IFINDEX_ENTRY\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            WssWlanIfIndexDB.u4WlanIfIndex =
                pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
            pWssWlanIfIndexDB =
                RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.WssWlanIfIndexDB,
                           (tRBElem *) & WssWlanIfIndexDB);

            if (pWssWlanIfIndexDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        RBTreeGet from WssWlanIfIndexDB returned NULL\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanIfType != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1WlanIfType =
                    (UINT1) pWssWlanDB->WssWlanAttributeDB.u1WlanIfType;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bAdminStatus != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1AdminStatus =
                    pWssWlanDB->WssWlanAttributeDB.u1AdminStatus;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bAuthMethod != OSIX_FALSE)
            {
                /* stores open or shared */
                pWssWlanIfIndexDB->u1AuthMethod =
                    pWssWlanDB->WssWlanAttributeDB.u1AuthMethod;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWebAuthStatus != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1WebAuthStatus =
                    pWssWlanDB->WssWlanAttributeDB.u1WebAuthStatus;
                /* By default WebAuth method will be internal */
                if (pWssWlanDB->WssWlanAttributeDB.u1WebAuthStatus ==
                    WSSWLAN_ENABLE)
                {
                    pWssWlanIfIndexDB->u1ExternalWebAuthMethod =
                        WEBAUTH_INTERNAL;
                }
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bVlanId != OSIX_FALSE)
            {

                pWssWlanIfIndexDB->u2VlanId =
                    pWssWlanDB->WssWlanAttributeDB.u2VlanId;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanMulticastMode != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u2WlanMulticastMode =
                    pWssWlanDB->WssWlanAttributeDB.u2WlanMulticastMode;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanSnoopTableLength !=
                OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u2WlanSnoopTableLength =
                    pWssWlanDB->WssWlanAttributeDB.u2WlanSnoopTableLength;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanSnoopTimer != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4WlanSnoopTimer =
                    pWssWlanDB->WssWlanAttributeDB.u4WlanSnoopTimer;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanSnoopTimeout != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4WlanSnoopTimeout =
                    pWssWlanDB->WssWlanAttributeDB.u4WlanSnoopTimeout;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bDtimPeriod != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->i2DtimPeriod =
                    pWssWlanDB->WssWlanAttributeDB.i2DtimPeriod;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bGroupTsc != OSIX_FALSE)
            {
                MEMSET (pWssWlanIfIndexDB->au1GroupTsc,
                        0, WSS_WLAN_GROUP_TSC_LEN);
                MEMCPY ((pWssWlanIfIndexDB->au1GroupTsc),
                        (pWssWlanDB->WssWlanAttributeDB.au1GroupTsc),
                        WSS_WLAN_GROUP_TSC_LEN);
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bDesiredSsid != OSIX_FALSE)
            {
                MEMCPY ((pWssWlanIfIndexDB->au1DesiredSsid),
                        (pWssWlanDB->WssWlanAttributeDB.au1DesiredSsid),
                        STRLEN ((pWssWlanDB->WssWlanAttributeDB.
                                 au1DesiredSsid)));
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bQosProfileId != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1QosProfileId =
                    pWssWlanDB->WssWlanAttributeDB.u1QosProfileId;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bCapabilityId != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1CapabilityId =
                    pWssWlanDB->WssWlanAttributeDB.u1CapabilityId;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bEncryptDecryptCapab
                != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1EncryptDecryptCapab =
                    pWssWlanDB->WssWlanAttributeDB.u1EncryptDecryptCapab;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bPowerConstraint != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1PowerConstraint =
                    pWssWlanDB->WssWlanAttributeDB.u1PowerConstraint;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bSupressSsid != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1SupressSsid =
                    pWssWlanDB->WssWlanAttributeDB.u1SupressSsid;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bSsidIsolation != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1SsidIsolation =
                    pWssWlanDB->WssWlanAttributeDB.u1SsidIsolation;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bExternalWebAuthMethod !=
                OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1ExternalWebAuthMethod =
                    pWssWlanDB->WssWlanAttributeDB.u1ExternalWebAuthMethod;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bExternalWebAuthUrl !=
                OSIX_FALSE)
            {
                MEMCPY (pWssWlanIfIndexDB->au1ExternalWebAuthUrl,
                        pWssWlanDB->WssWlanAttributeDB.au1ExternalWebAuthUrl,
                        WSS_WLAN_WEBAUTH_URL_LENGTH);
            }

            if (pWssWlanIfIndexDB->u2Capability == 0)
            {
                pWssWlanIfIndexDB->u2Capability = 1;

            }
            if (pWssWlanDB->WssWlanIsPresentDB.bCfPollable != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.u1CfPollable =
                    pWssWlanDB->WssWlanAttributeDB.u1CfPollable;
                if (pWssWlanDB->WssWlanAttributeDB.u1CfPollable == 1)
                {
                    pWssWlanIfIndexDB->u2Capability =
                        pWssWlanIfIndexDB->u2Capability | (1 << SHIFT_VALUE_2);
                }
                else
                {
                    pWssWlanIfIndexDB->u2Capability =
                        (UINT2) (pWssWlanIfIndexDB->
                                 u2Capability & (~(1 << SHIFT_VALUE_2)));

                }
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bCfPollRequest != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.u1CfPollRequest
                    = pWssWlanDB->WssWlanAttributeDB.u1CfPollRequest;
                if (pWssWlanDB->WssWlanAttributeDB.u1CfPollRequest == 1)
                {
                    pWssWlanIfIndexDB->u2Capability =
                        pWssWlanIfIndexDB->u2Capability | (1 << SHIFT_VALUE_3);
                }
                else
                {
                    pWssWlanIfIndexDB->u2Capability =
                        (UINT2) (pWssWlanIfIndexDB->u2Capability &
                                 (~(1 << SHIFT_VALUE_3)));

                }
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bPrivacyOptionImplemented
                != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1PrivacyOptionImplemented =
                    pWssWlanDB->WssWlanAttributeDB.u1PrivacyOptionImplemented;
                if (pWssWlanDB->WssWlanAttributeDB.u1PrivacyOptionImplemented
                    == 1)
                {
                    pWssWlanIfIndexDB->u2Capability =
                        pWssWlanIfIndexDB->u2Capability | (1 << SHIFT_VALUE_4);
                }
                else
                {
                    pWssWlanIfIndexDB->u2Capability =
                        (UINT2) (pWssWlanIfIndexDB->u2Capability &
                                 (~(1 << SHIFT_VALUE_4)));

                }
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bShortPreambleOptionImplemented
                != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1ShortPreambleOptionImplemented =
                    pWssWlanDB->WssWlanAttributeDB.
                    u1ShortPreambleOptionImplemented;
                if (pWssWlanDB->WssWlanAttributeDB.
                    u1ShortPreambleOptionImplemented == 1)
                {
                    pWssWlanIfIndexDB->u2Capability =
                        pWssWlanIfIndexDB->u2Capability | (1 << SHIFT_VALUE_5);
                }
                else
                {
                    pWssWlanIfIndexDB->u2Capability =
                        (UINT2) (pWssWlanIfIndexDB->u2Capability &
                                 (~(1 << SHIFT_VALUE_5)));
                }
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bPBCCOptionImplemented
                != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1PBCCOptionImplemented =
                    pWssWlanDB->WssWlanAttributeDB.u1PBCCOptionImplemented;
                if (pWssWlanDB->WssWlanAttributeDB.u1PBCCOptionImplemented == 1)
                {
                    pWssWlanIfIndexDB->u2Capability =
                        pWssWlanIfIndexDB->u2Capability | (1 << SHIFT_VALUE_6);
                }
                else
                {
                    pWssWlanIfIndexDB->u2Capability =
                        (UINT2) (pWssWlanIfIndexDB->u2Capability &
                                 (~(1 << SHIFT_VALUE_6)));
                }
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bChannelAgilityPresent
                != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1ChannelAgilityPresent =
                    pWssWlanDB->WssWlanAttributeDB.u1ChannelAgilityPresent;
                if (pWssWlanDB->WssWlanAttributeDB.u1ChannelAgilityPresent == 1)
                {
                    pWssWlanIfIndexDB->u2Capability =
                        pWssWlanIfIndexDB->u2Capability | (1 << SHIFT_VALUE_7);
                }
                else
                {
                    pWssWlanIfIndexDB->u2Capability =
                        (UINT2) (pWssWlanIfIndexDB->u2Capability &
                                 (~(1 << SHIFT_VALUE_7)));
                }
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bSpectrumManagementRequired
                != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1SpectrumManagementRequired =
                    pWssWlanDB->WssWlanAttributeDB.u1SpectrumManagementRequired;
                if (pWssWlanDB->WssWlanAttributeDB.u1SpectrumManagementRequired
                    == 1)
                {
                    pWssWlanIfIndexDB->u2Capability =
                        pWssWlanIfIndexDB->u2Capability | (1 << SHIFT_VALUE_8);
                }
                else
                {

                    pWssWlanIfIndexDB->u2Capability =
                        (UINT2) (pWssWlanIfIndexDB->u2Capability &
                                 (~(1 << SHIFT_VALUE_8)));
                }
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bQosOptionImplemented
                != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1QosOptionImplemented =
                    pWssWlanDB->WssWlanAttributeDB.u1QosOptionImplemented;
                if (pWssWlanDB->WssWlanAttributeDB.u1QosOptionImplemented == 1)
                {
                    pWssWlanIfIndexDB->u2Capability =
                        pWssWlanIfIndexDB->u2Capability | (1 << SHIFT_VALUE_9);
                }
                else
                {
                    pWssWlanIfIndexDB->u2Capability =
                        (UINT2) (pWssWlanIfIndexDB->u2Capability &
                                 (~(1 << SHIFT_VALUE_9)));

                }
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bShortSlotTimeOptionImplemented
                != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1ShortSlotTimeOptionImplemented =
                    pWssWlanDB->WssWlanAttributeDB.
                    u1ShortSlotTimeOptionImplemented;

                if (pWssWlanDB->WssWlanAttributeDB.
                    u1ShortSlotTimeOptionImplemented == 1)
                {
                    pWssWlanIfIndexDB->u2Capability =
                        pWssWlanIfIndexDB->u2Capability | (1 << SHIFT_VALUE_10);
                }
                else
                {
                    pWssWlanIfIndexDB->u2Capability =
                        (UINT2) (pWssWlanIfIndexDB->u2Capability &
                                 (~(1 << SHIFT_VALUE_10)));
                }
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bAPSDOptionImplemented
                != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1APSDOptionImplemented =
                    pWssWlanDB->WssWlanAttributeDB.u1APSDOptionImplemented;
                if (pWssWlanDB->WssWlanAttributeDB.u1APSDOptionImplemented == 1)
                {
                    pWssWlanIfIndexDB->u2Capability =
                        pWssWlanIfIndexDB->u2Capability | (1 << SHIFT_VALUE_11);
                }
                else
                {
                    pWssWlanIfIndexDB->u2Capability =
                        (UINT2) (pWssWlanIfIndexDB->u2Capability &
                                 (~(1 << SHIFT_VALUE_11)));
                }
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bDSSSOFDMOptionEnabled
                != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1DSSSOFDMOptionEnabled =
                    pWssWlanDB->WssWlanAttributeDB.u1DSSSOFDMOptionEnabled;
                if (pWssWlanDB->WssWlanAttributeDB.u1DSSSOFDMOptionEnabled == 1)
                {
                    pWssWlanIfIndexDB->u2Capability =
                        pWssWlanIfIndexDB->u2Capability | (1 << SHIFT_VALUE_13);
                }
                else
                {
                    pWssWlanIfIndexDB->u2Capability =
                        (UINT2) (pWssWlanIfIndexDB->u2Capability &
                                 (~(1 << SHIFT_VALUE_13)));
                }
            }
            if (pWssWlanDB->WssWlanIsPresentDB.
                bDelayedBlockAckOptionImplemented != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1DelayedBlockAckOptionImplemented =
                    pWssWlanDB->WssWlanAttributeDB.
                    u1DelayedBlockAckOptionImplemented;
                if (pWssWlanDB->WssWlanAttributeDB.
                    u1DelayedBlockAckOptionImplemented == 1)
                {
                    pWssWlanIfIndexDB->u2Capability =
                        pWssWlanIfIndexDB->u2Capability | (1 << SHIFT_VALUE_14);
                }
                else
                {
                    pWssWlanIfIndexDB->u2Capability =
                        (UINT2) (pWssWlanIfIndexDB->u2Capability &
                                 (~(1 << SHIFT_VALUE_14)));
                }
            }
            if (pWssWlanDB->WssWlanIsPresentDB.
                bImmediateBlockAckOptionImplemented != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1ImmediateBlockAckOptionImplemented =
                    pWssWlanDB->WssWlanAttributeDB.
                    u1ImmediateBlockAckOptionImplemented;
                if (pWssWlanDB->WssWlanAttributeDB.
                    u1ImmediateBlockAckOptionImplemented == 1)
                {
                    pWssWlanIfIndexDB->u2Capability =
                        pWssWlanIfIndexDB->u2Capability | (1 << SHIFT_VALUE_15);
                }
                else
                {
                    pWssWlanIfIndexDB->u2Capability =
                        (UINT2) (pWssWlanIfIndexDB->u2Capability &
                                 (~(1 << SHIFT_VALUE_15)));
                }
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bAPSDOptionImplemented
                != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1APSDOptionImplemented =
                    pWssWlanDB->WssWlanAttributeDB.u1APSDOptionImplemented;
                if (pWssWlanDB->WssWlanAttributeDB.u1APSDOptionImplemented == 1)
                {
                    pWssWlanIfIndexDB->WssWlanQosCapab.u1QosInfo =
                        pWssWlanIfIndexDB->WssWlanQosCapab.u1QosInfo |
                        (1 << SHIFT_VALUE_7);
                    pWssWlanIfIndexDB->WssWlanEdcaParam.u1QosInfo =
                        pWssWlanIfIndexDB->WssWlanEdcaParam.u1QosInfo |
                        (1 << SHIFT_VALUE_7);
                }
                else
                {
                    pWssWlanIfIndexDB->WssWlanQosCapab.u1QosInfo =
                        (UINT1) (pWssWlanIfIndexDB->WssWlanQosCapab.u1QosInfo &
                                 (~(1 << SHIFT_VALUE_7)));
                    pWssWlanIfIndexDB->WssWlanEdcaParam.u1QosInfo =
                        (UINT1) (pWssWlanIfIndexDB->WssWlanEdcaParam.u1QosInfo &
                                 (~(1 << SHIFT_VALUE_7)));
                }
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQAckOptionImplemented
                != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1QAckOptionImplemented =
                    pWssWlanDB->WssWlanAttributeDB.u1QAckOptionImplemented;
                if (pWssWlanDB->WssWlanAttributeDB.u1QAckOptionImplemented == 1)
                {
                    pWssWlanIfIndexDB->WssWlanQosCapab.u1QosInfo =
                        pWssWlanIfIndexDB->WssWlanQosCapab.u1QosInfo |
                        (1 << SHIFT_VALUE_3);
                    pWssWlanIfIndexDB->WssWlanEdcaParam.u1QosInfo =
                        pWssWlanIfIndexDB->WssWlanEdcaParam.u1QosInfo |
                        (1 << SHIFT_VALUE_3);
                }
                else
                {
                    pWssWlanIfIndexDB->WssWlanQosCapab.u1QosInfo =
                        (UINT1) (pWssWlanIfIndexDB->WssWlanQosCapab.u1QosInfo &
                                 (~(1 << SHIFT_VALUE_3)));
                    pWssWlanIfIndexDB->WssWlanEdcaParam.u1QosInfo =
                        (UINT1) (pWssWlanIfIndexDB->WssWlanEdcaParam.u1QosInfo &
                                 (~(1 << SHIFT_VALUE_3)));
                }
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bQueueRequestOptionImplemented
                != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1QueueRequestOptionImplemented =
                    pWssWlanDB->WssWlanAttributeDB.
                    u1QueueRequestOptionImplemented;
                if (pWssWlanDB->WssWlanAttributeDB.
                    u1QueueRequestOptionImplemented == 1)
                {
                    pWssWlanIfIndexDB->WssWlanQosCapab.u1QosInfo =
                        pWssWlanIfIndexDB->WssWlanQosCapab.u1QosInfo |
                        (1 << SHIFT_VALUE_2);
                    pWssWlanIfIndexDB->WssWlanEdcaParam.u1QosInfo =
                        pWssWlanIfIndexDB->WssWlanEdcaParam.u1QosInfo |
                        (1 << SHIFT_VALUE_2);
                }
                else
                {
                    pWssWlanIfIndexDB->WssWlanQosCapab.u1QosInfo =
                        (UINT1) (pWssWlanIfIndexDB->WssWlanQosCapab.u1QosInfo &
                                 (~(1 << SHIFT_VALUE_2)));
                    pWssWlanIfIndexDB->WssWlanEdcaParam.u1QosInfo =
                        (UINT1) (pWssWlanIfIndexDB->WssWlanEdcaParam.u1QosInfo &
                                 (~(1 << SHIFT_VALUE_2)));
                }
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bTXOPRequestOptionImplemented
                != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1TXOPRequestOptionImplemented =
                    pWssWlanDB->WssWlanAttributeDB.
                    u1TXOPRequestOptionImplemented;
                if (pWssWlanDB->WssWlanAttributeDB.
                    u1TXOPRequestOptionImplemented == 1)
                {
                    pWssWlanIfIndexDB->WssWlanQosCapab.u1QosInfo =
                        pWssWlanIfIndexDB->WssWlanQosCapab.u1QosInfo |
                        (1 << SHIFT_VALUE_1);
                    pWssWlanIfIndexDB->WssWlanEdcaParam.u1QosInfo =
                        pWssWlanIfIndexDB->WssWlanEdcaParam.u1QosInfo |
                        (1 << SHIFT_VALUE_1);
                }
                else
                {
                    pWssWlanIfIndexDB->WssWlanQosCapab.u1QosInfo =
                        (UINT1) (pWssWlanIfIndexDB->WssWlanQosCapab.u1QosInfo &
                                 (~(1 << SHIFT_VALUE_1)));
                    pWssWlanIfIndexDB->WssWlanEdcaParam.u1QosInfo =
                        (UINT1) (pWssWlanIfIndexDB->WssWlanEdcaParam.u1QosInfo &
                                 (~(1 << SHIFT_VALUE_1)));
                }
            }
            if (pWssWlanDB->WssWlanIsPresentDB.
                bRSNAPreauthenticationImplemented != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1RSNAPreauthenticationImplemented =
                    pWssWlanDB->WssWlanAttributeDB.
                    u1RSNAPreauthenticationImplemented;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bRSNAOptionImplemented
                != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1RSNAOptionImplemented =
                    pWssWlanDB->WssWlanAttributeDB.u1RSNAOptionImplemented;
            }
#ifdef WPS_WANTED
            if (pWssWlanDB->WssWlanIsPresentDB.bWPSEnabled != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1WPSEnabled = pWssWlanDB->WssWlanAttributeDB.u1WPSEnabled;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWPSAdditionalIE != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->WssWlanCapabilityProfileDB.
                    u1WPSAdditionalIE =
                    pWssWlanDB->WssWlanAttributeDB.u1WPSAdditionalIE;
            }
#endif
            if (pWssWlanDB->WssWlanIsPresentDB.bPassengerTrustMode
                != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1PassengerTrustMode =
                    pWssWlanDB->WssWlanAttributeDB.u1PassengerTrustMode;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bQosTraffic != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1QosTraffic =
                    pWssWlanDB->WssWlanAttributeDB.u1QosTraffic;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bMaxClientCount != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4MaxClientCount =
                    pWssWlanDB->WssWlanAttributeDB.u4MaxClientCount;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bMitigationRequirement
                != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1MitigationRequirement =
                    pWssWlanDB->WssWlanAttributeDB.u1MitigationRequirement;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosRateLimit != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1QosRateLimit =
                    pWssWlanDB->WssWlanAttributeDB.u1QosRateLimit;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamCIR != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4QosUpStreamCIR =
                    pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamCIR;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamCBS != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4QosUpStreamCBS =
                    pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamCBS;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamEIR != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4QosUpStreamEIR =
                    pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamEIR;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamEBS != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4QosUpStreamEBS =
                    pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamEBS;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamCIR != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4QosDownStreamCIR =
                    pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamCIR;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamCBS != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4QosDownStreamCBS =
                    pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamCBS;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamEIR != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4QosDownStreamEIR =
                    pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamEIR;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamEBS != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4QosDownStreamEBS =
                    pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamEBS;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bAuthenticationIndex
                != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1AuthenticationIndex =
                    pWssWlanDB->WssWlanAttributeDB.u1AuthenticationIndex;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bBandwidthThresh != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4BandwidthThresh =
                    pWssWlanDB->WssWlanAttributeDB.u4BandwidthThresh;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bFsDot11RedirectFileName
                != OSIX_FALSE)
            {
                MEMSET (pWssWlanIfIndexDB->au1FsDot11RedirectFileName, 0,
                        WSS_WLAN_DEFAULT_REDIRECT_FILE_LENGTH);
                i4RedirectFileNameLength =
                    STRLEN (pWssWlanDB->WssWlanAttributeDB.
                            au1FsDot11RedirectFileName);
                MEMCPY (pWssWlanIfIndexDB->au1FsDot11RedirectFileName,
                        pWssWlanDB->WssWlanAttributeDB.
                        au1FsDot11RedirectFileName, i4RedirectFileNameLength);
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bFsLoginAuthMode != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u4LoginAuthMode =
                    pWssWlanDB->WssWlanAttributeDB.u4LoginAuthMode;
            }
            if ((pWssWlanDB->WssWlanIsPresentDB.bKeyStatus != OSIX_FALSE) ||
                (pWssWlanDB->WssWlanIsPresentDB.bKeyType != OSIX_FALSE) ||
                (pWssWlanDB->WssWlanIsPresentDB.bKeyIndex != OSIX_FALSE) ||
                (pWssWlanDB->WssWlanIsPresentDB.bWepKey != OSIX_FALSE) ||
                (pWssWlanDB->WssWlanIsPresentDB.bAuthAlgorithm != OSIX_FALSE))
            {

                if ((pWssWlanIfIndexDB->u1AuthMethod ==
                     WSS_WLAN_AUTH_ALGO_SHARED) ||
                    (pWssWlanIfIndexDB->u1AuthMethod ==
                     WSS_WLAN_AUTH_ALGO_OPEN))
                {

                    if (pWssWlanDB->WssWlanIsPresentDB.bKeyStatus != OSIX_FALSE)
                    {
                        /* Currently not updated by any cli command */
                        pWssWlanIfIndexDB->u1KeyStatus =
                            pWssWlanDB->WssWlanAttributeDB.u1KeyStatus;
                    }
                    if (pWssWlanDB->WssWlanIsPresentDB.bKeyType != OSIX_FALSE)
                    {

                        pWssWlanIfIndexDB->u1KeyType =
                            pWssWlanDB->WssWlanAttributeDB.u1KeyType;
                    }
                    if (pWssWlanDB->WssWlanIsPresentDB.bKeyIndex != OSIX_FALSE)
                    {

                        pWssWlanIfIndexDB->u1KeyIndex =
                            pWssWlanDB->WssWlanAttributeDB.u1KeyIndex;
                    }
                    if (pWssWlanDB->WssWlanIsPresentDB.bWepKey != OSIX_FALSE)
                    {
                        MEMSET (pWssWlanIfIndexDB->au1WepKey, 0,
                                sizeof (pWssWlanIfIndexDB->au1WepKey));
                        if (pWssWlanDB->WssWlanAttributeDB.au1WepKey[0] != '\0')
                        {
                            MEMCPY (pWssWlanIfIndexDB->au1WepKey,
                                    pWssWlanDB->WssWlanAttributeDB.au1WepKey,
                                    STRLEN (pWssWlanDB->WssWlanAttributeDB.
                                            au1WepKey));
                        }

                    }

                    if (pWssWlanDB->WssWlanIsPresentDB.bAuthAlgorithm
                        != OSIX_FALSE)
                    {
                        /* stores enable or disable status */
                        pWssWlanIfIndexDB->u1AuthAlgorithm =
                            pWssWlanDB->WssWlanAttributeDB.u1AuthAlgorithm;
                    }
                }
                else
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                            Currently Open authentication in DB .\
                            Configuration not allowed\r\n");
                    WSSIF_UNLOCK;
                    return OSIX_FAILURE;

                }
            }
#ifdef BAND_SELECT_WANTED
            if (pWssWlanDB->WssWlanIsPresentDB.bBandSelectStatus != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1BandSelectStatus =
                    pWssWlanDB->WssWlanAttributeDB.u1BandSelectStatus;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bAgeOutSuppression != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1AgeOutSuppression =
                    pWssWlanDB->WssWlanAttributeDB.u1AgeOutSuppression;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bAssocRejectCount != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1AssocRejectCount =
                    pWssWlanDB->WssWlanAttributeDB.u1AssocRejectCount;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bAssocResetTime != OSIX_FALSE)
            {
                pWssWlanIfIndexDB->u1AssocResetTime =
                    pWssWlanDB->WssWlanAttributeDB.u1AssocResetTime;
            }
#endif

            break;
        case WSS_WLAN_CREATE_BSS_ENTRY:
            RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                UtlShMemAllocAntennaSelectionBuf ();

            if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "\r%% Memory allocation for Antenna"
                           " selection failed.\r\n");
                return OSIX_FAILURE;
            }
            if (pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "\nWssIfProcessWssWlanDBMsg:IfIndex is empty ,\
                        binding creation failed \r\n");
                WSSIF_UNLOCK;
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                return OSIX_FAILURE;
            }
            /* Check RadioIfDB for existence of RadioIfDB */
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;

            /* Release the lock before calling Radio IF DB as that function 
             * will take the same lock  */
            WSSIF_UNLOCK;
            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB)
                != OSIX_SUCCESS)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:RadioIfIndex is not \
                        available\r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                return OSIX_SUCCESS;
            }
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSSIF_LOCK;

            if (pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:WlanProfileId is empty\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if ((pWssWlanBssInterfaceDB =
                 (tWssWlanBssInterfaceDB *)
                 MemAllocMemBlk (WSSWLAN_BSS_INTERFACE_DB_POOLID)) == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:Memory allocation failed \
                        for Bss Interface creation\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            MEMSET (pWssWlanBssInterfaceDB, 0, sizeof (tWssWlanBssInterfaceDB));
            pWssWlanBssInterfaceDB->u4RadioIfIndex =
                pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
            pWssWlanBssInterfaceDB->u2WlanProfileId =
                pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId;

            if (RBTreeAdd
                (gWssWlanGlobals.WssWlanGlbMib.WssWlanBssInterfaceDB,
                 (tRBElem *) pWssWlanBssInterfaceDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (WSSWLAN_BSS_INTERFACE_DB_POOLID,
                                    (UINT1 *) pWssWlanBssInterfaceDB);
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:RBTreeAdd failed for \
                        BssInterface\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            pWssWlanBssInterfaceDB->u1RowStatus = WSSWLAN_RS_NOT_READY;
            break;
        case WSS_WLAN_DESTROY_BSS_ENTRY:
            if (pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:IfIndex is empty ,\
                        binding deletion failed \r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            if (pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:WlanProfileId is empty\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            WssWlanBssInterfaceDB.u4RadioIfIndex =
                pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
            WssWlanBssInterfaceDB.u2WlanProfileId =
                pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId;
            pWssWlanBssInterfaceDB =
                RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.WssWlanBssInterfaceDB,
                           (tRBElem *) & WssWlanBssInterfaceDB);

            if (pWssWlanBssInterfaceDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:RBTreeGet failed for\
                        BssInterfaceDB\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;

            }
            pWssWlanBssInterfaceDB->pWssWlanBssIfIndexDB = NULL;
            RBTreeRem (gWssWlanGlobals.WssWlanGlbMib.WssWlanBssInterfaceDB,
                       &WssWlanBssInterfaceDB);
            MemReleaseMemBlock (WSSWLAN_BSS_INTERFACE_DB_POOLID,
                                (UINT1 *) pWssWlanBssInterfaceDB);

            break;
        case WSS_WLAN_GET_BSS_ENTRY:

            MEMSET (&WssWlanBssInterfaceDB, 0, sizeof (tWssWlanBssInterfaceDB));

            if ((pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex != 0) &&
                (pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId != 0))
            {
                WssWlanBssInterfaceDB.u2WlanProfileId =
                    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId;
                WssWlanBssInterfaceDB.u4RadioIfIndex =
                    pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;

                pWssWlanBssInterfaceDB =
                    RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.
                               WssWlanBssInterfaceDB,
                               (tRBElem *) & WssWlanBssInterfaceDB);
                if (pWssWlanBssInterfaceDB == NULL)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC, "\nWssIfProcessWssWlanDBMsg:\
                            RBTreeGet from BssIfIndexDB failed\r\n");
                    WSSIF_UNLOCK;
                    return OSIX_FAILURE;
                }

            }
            else
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "\nWssIfProcessWssWlanDBMsg:Key \
                        value u2WlanProfileId or RadioIfIndex is empty\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanId != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1WlanId =
                    pWssWlanBssInterfaceDB->u1WlanId;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bRowStatus != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1RowStatus =
                    pWssWlanBssInterfaceDB->u1RowStatus;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex != OSIX_FALSE)
            {
                if (pWssWlanBssInterfaceDB->u1RowStatus == WSSWLAN_RS_ACTIVE)
                {
                    if (pWssWlanBssInterfaceDB->pWssWlanBssIfIndexDB == NULL)
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "\nWssIfProcessWssWlanDBMsg:"
                                   "pWssWlanBssIfIndexDb in BssInterface"
                                   "is NULL\r\n");
                        WSSIF_UNLOCK;
                        return OSIX_FAILURE;

                    }
                    else
                    {
                        pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex =
                            pWssWlanBssInterfaceDB->pWssWlanBssIfIndexDB->
                            u4BssIfIndex;
                    }
                }
            }
            break;

        case WSS_WLAN_SET_BSS_ENTRY:
            MEMSET (&WssWlanBssInterfaceDB, 0, sizeof (tWssWlanBssInterfaceDB));

            if ((pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex != 0) &&
                (pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId != 0))
            {
                WssWlanBssInterfaceDB.u2WlanProfileId =
                    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId;
                WssWlanBssInterfaceDB.u4RadioIfIndex =
                    pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;

                pWssWlanBssInterfaceDB =
                    RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.
                               WssWlanBssInterfaceDB,
                               (tRBElem *) & WssWlanBssInterfaceDB);
                if (pWssWlanBssInterfaceDB == NULL)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC, "\nWssIfProcessWssWlanDBMsg:\
                            RBTreeGet from BssInterfaceDB failed\r\n");
                    WSSIF_UNLOCK;
                    return OSIX_FAILURE;
                }

            }
            else
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "\nWssIfProcessWssWlanDBMsg:\
                        Key value WlanProfileId or RadioIfIndex is empty\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanId != OSIX_FALSE)
            {
                pWssWlanBssInterfaceDB->u1WlanId =
                    pWssWlanDB->WssWlanAttributeDB.u1WlanId;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bRowStatus != OSIX_FALSE)
            {
                pWssWlanBssInterfaceDB->u1RowStatus =
                    pWssWlanDB->WssWlanAttributeDB.u1RowStatus;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex != OSIX_FALSE)
            {
                WssWlanBssIfIndexDB.u4BssIfIndex =
                    pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex;

                pWssWlanBssIfIndexDB = RBTreeGet
                    (gWssWlanGlobals.WssWlanGlbMib.WssWlanBssIfIndexDB,
                     (tRBElem *) & WssWlanBssIfIndexDB);
                if (pWssWlanBssIfIndexDB == NULL)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC, "\nWssIfProcessWssWlanDBMsg:\
                            RBTreeGet from BssInterface->pBssIfIndexDB\
                            failed\r\n");
                    WSSIF_UNLOCK;
                    return OSIX_FAILURE;
                }
                pWssWlanBssInterfaceDB->pWssWlanBssIfIndexDB =
                    pWssWlanBssIfIndexDB;

            }
            break;

        case WSS_WLAN_CREATE_BSS_IFINDEX_ENTRY:
            if (pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:BSS IfIndex is empty,\
                        creation failed \r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            if ((pWssWlanBssIfIndexDB =
                 (tWssWlanBssIfIndexDB *)
                 MemAllocMemBlk (WSSWLAN_BSS_IFINDEX_DB_POOLID)) == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        Memory allocation failed for BssIfIndex\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            MEMSET (pWssWlanBssIfIndexDB, 0, sizeof (tWssWlanBssIfIndexDB));
            pWssWlanBssIfIndexDB->u4BssIfIndex =
                pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex;

            if (RBTreeAdd
                (gWssWlanGlobals.WssWlanGlbMib.WssWlanBssIfIndexDB,
                 (tRBElem *) pWssWlanBssIfIndexDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (WSSWLAN_BSS_IFINDEX_DB_POOLID,
                                    (UINT1 *) pWssWlanBssIfIndexDB);
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        RBTreeAdd failed for BssIfIndex\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            break;

        case WSS_WLAN_DESTROY_BSS_IFINDEX_ENTRY:
            if (pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        BSS IfIndex is empty, deletion failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            WssWlanBssIfIndexDB.u4BssIfIndex =
                pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex;
            pWssWlanBssIfIndexDB =
                RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.WssWlanBssIfIndexDB,
                           (tRBElem *) & WssWlanBssIfIndexDB);

            if (pWssWlanBssIfIndexDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        RBTreeGet failed for BssIfIndex\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;

            }

            if (pWssWlanDB->WssWlanIsPresentDB.bRadioIfIndex != OSIX_FALSE)
            {
                RadioIfDB.u4VirtualRadioIfIndex =
                    pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
                pRadioIfDB = RBTreeGet (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                                        (tRBElem *) & RadioIfDB);
                if (pRadioIfDB == NULL)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                            RBTreeGet of pRadioIfDB failed,\
                            No such RadioIfIndex\r\n");
                    WSSIF_UNLOCK;
                    return OSIX_FAILURE;
                }

                if (pWssWlanDB->WssWlanIsPresentDB.bWlanId != OSIX_FALSE)
                {
                    /* Set the BSSIfIndex to 0 for the deleted WLAN in the Radio
                     * DB */
                    pRadioIfDB->au4BssIfIndex[pWssWlanDB->WssWlanAttributeDB.
                                              u1WlanId - 1] = 0;
                }
            }

            pWssWlanBssIfIndexDB->pRadioIfDB = NULL;
            RBTreeRem (gWssWlanGlobals.WssWlanGlbMib.WssWlanBssIfIndexDB,
                       &WssWlanBssIfIndexDB);
            MemReleaseMemBlock (WSSWLAN_BSS_IFINDEX_DB_POOLID,
                                (UINT1 *) pWssWlanBssIfIndexDB);

            break;
        case WSS_WLAN_GET_BSS_IFINDEX_ENTRY:
            MEMSET (&WssWlanBssIfIndexDB, 0, sizeof (tWssWlanBssIfIndexDB));

            if (pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex != 0)
            {
                WssWlanBssIfIndexDB.u4BssIfIndex =
                    pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex;

                pWssWlanBssIfIndexDB =
                    RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.
                               WssWlanBssIfIndexDB,
                               (tRBElem *) & WssWlanBssIfIndexDB);
                if (pWssWlanBssIfIndexDB == NULL)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC, "\nWssIfProcessWssWlanDBMsg:\
                            RBTreeGet from BssIfIndexDB failed\r\n");
                    WSSIF_UNLOCK;
                    return OSIX_FAILURE;
                }

            }
            else
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "\nWssIfProcessWssWlanDBMsg:\
                        Key value BssIfIndex is empty\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bIfType != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1IfType =
                    (UINT1) (pWssWlanBssIfIndexDB->u1IfType);
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanBindStatus != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1WlanBindStatus =
                    pWssWlanBssIfIndexDB->u1WlanBindStatus;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u2WlanInternalId =
                    pWssWlanBssIfIndexDB->u2WlanInternalId;
                if (pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex != OSIX_FALSE)
                {
                    if (gaWssWlanInternalProfileDB
                        [pWssWlanBssIfIndexDB->u2WlanInternalId -
                         1].pWssWlanIfIndexDB != NULL)
                    {
                        pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex =
                            gaWssWlanInternalProfileDB[pWssWlanBssIfIndexDB->
                                                       u2WlanInternalId -
                                                       1].pWssWlanIfIndexDB->
                            u4WlanIfIndex;
                    }
                }
                if (pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId != OSIX_FALSE)
                {
                    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId =
                        gaWssWlanInternalProfileDB[pWssWlanBssIfIndexDB->
                                                   u2WlanInternalId -
                                                   1].u2WlanProfileId;
                }

            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanId != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1WlanId =
                    pWssWlanBssIfIndexDB->u1WlanId;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bAdminStatus != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u1AdminStatus =
                    pWssWlanBssIfIndexDB->u1AdminStatus;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bBssId != OSIX_FALSE)
            {
                MEMCPY ((pWssWlanDB->WssWlanAttributeDB.BssId),
                        (pWssWlanBssIfIndexDB->BssId), MAC_ADDR_LEN);
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bRadioIfIndex != OSIX_FALSE)
            {
                if (pWssWlanBssIfIndexDB->pRadioIfDB == NULL)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC, "\nWssIfProcessWssWlanDBMsg:\
                            pRadioIfDB in BssIfIndexDB is NULL \r\n");
                    WSSIF_UNLOCK;
                    return OSIX_FAILURE;

                }
                pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex =
                    pWssWlanBssIfIndexDB->pRadioIfDB->u4VirtualRadioIfIndex;

                if (pWssWlanDB->WssWlanIsPresentDB.bWtpInternalId != OSIX_FALSE)
                {
                    pWssWlanDB->WssWlanAttributeDB.u2WtpInternalId =
                        pWssWlanBssIfIndexDB->pRadioIfDB->u2WtpInternalId;
                }

            }
            if (pWssWlanDB->WssWlanIsPresentDB.bVlanId != OSIX_FALSE)
            {
                WsscfgGetApGroupEnable (&u4ApGroupEnabled);
                if (u4ApGroupEnabled == APGROUP_ENABLE)
                {
                    pWssWlanDB->WssWlanAttributeDB.u2VlanId =
                        pWssWlanBssIfIndexDB->u2VlanId;
                }
                else
                {
                    if (gaWssWlanInternalProfileDB
                        [pWssWlanBssIfIndexDB->u2WlanInternalId -
                         1].pWssWlanIfIndexDB == NULL)
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "\nWssIfProcessWssWlanDBMsg:\
                            Wlan Index missing\r\n");
                        WSSIF_UNLOCK;
                        return OSIX_FAILURE;
                    }
                    pWssWlanDB->WssWlanAttributeDB.u2VlanId =
                        gaWssWlanInternalProfileDB[pWssWlanBssIfIndexDB->
                                                   u2WlanInternalId - 1].
                        pWssWlanIfIndexDB->u2VlanId;

                }
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanDiffServId == OSIX_TRUE)
            {
                pWssWlanDB->WssWlanAttributeDB.WssWlanDiffServ.u4IfIndex =
                    pWssWlanBssIfIndexDB->WssWlanDiffServ.u4IfIndex;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanInterfaceIp != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.WlanInterfaceIp.u4IpAddr =
                    pWssWlanBssIfIndexDB->WlanInterfaceIp.u4IpAddr;
                pWssWlanDB->WssWlanAttributeDB.WlanInterfaceIp.u4SubMask =
                    pWssWlanBssIfIndexDB->WlanInterfaceIp.u4SubMask;
                pWssWlanDB->WssWlanAttributeDB.WlanInterfaceIp.u1AdminStatus =
                    pWssWlanBssIfIndexDB->WlanInterfaceIp.u1AdminStatus;
                pWssWlanDB->WssWlanAttributeDB.WlanInterfaceIp.u1RowStatus =
                    pWssWlanBssIfIndexDB->WlanInterfaceIp.u1RowStatus;
            }

            break;
        case WSS_WLAN_SET_BSS_IFINDEX_ENTRY:
            MEMSET (&WssWlanBssIfIndexDB, 0, sizeof (tWssWlanBssIfIndexDB));

            if (pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex != 0)
            {
                WssWlanBssIfIndexDB.u4BssIfIndex =
                    pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex;

                pWssWlanBssIfIndexDB =
                    RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.
                               WssWlanBssIfIndexDB,
                               (tRBElem *) & WssWlanBssIfIndexDB);
                if (pWssWlanBssIfIndexDB == NULL)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC, "\nssIfProcessWssWlanDBMsg:\
                            RBTreeGet from BssIfIndexDB failed\r\n");
                    WSSIF_UNLOCK;
                    return OSIX_FAILURE;
                }

            }
            else
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "\nssIfProcessWssWlanDBMsg:\
                        Key value BssIfIndex is empty\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bIfType != OSIX_FALSE)
            {
                pWssWlanBssIfIndexDB->u1IfType =
                    pWssWlanDB->WssWlanAttributeDB.u1IfType;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanBindStatus != OSIX_FALSE)
            {
                pWssWlanBssIfIndexDB->u1WlanBindStatus =
                    pWssWlanDB->WssWlanAttributeDB.u1WlanBindStatus;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId != OSIX_FALSE)
            {
                pWssWlanBssIfIndexDB->u2WlanInternalId =
                    pWssWlanDB->WssWlanAttributeDB.u2WlanInternalId;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanId != OSIX_FALSE)
            {
                pWssWlanBssIfIndexDB->u1WlanId =
                    pWssWlanDB->WssWlanAttributeDB.u1WlanId;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bAdminStatus != OSIX_FALSE)
            {
                pWssWlanBssIfIndexDB->u1AdminStatus =
                    pWssWlanDB->WssWlanAttributeDB.u1AdminStatus;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bBssId != OSIX_FALSE)
            {
                MEMCPY ((pWssWlanBssIfIndexDB->BssId),
                        (pWssWlanDB->WssWlanAttributeDB.BssId), MAC_ADDR_LEN);
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bRadioIfIndex != OSIX_FALSE)
            {
                RadioIfDB.u4VirtualRadioIfIndex =
                    pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
                pRadioIfDB =
                    RBTreeGet (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                               (tRBElem *) & RadioIfDB);
                if (pRadioIfDB == NULL)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                            Updation of pRadioIfDB failed,\
                            No such RadioIfIndex\r\n");
                    return OSIX_FAILURE;
                }
                pWssWlanBssIfIndexDB->pRadioIfDB = pRadioIfDB;

                if (pWssWlanDB->WssWlanIsPresentDB.bWlanId != OSIX_FALSE)
                {
                    pWssWlanBssIfIndexDB->u1WlanId =
                        pWssWlanDB->WssWlanAttributeDB.u1WlanId;
                    pRadioIfDB->au4BssIfIndex[pWssWlanDB->WssWlanAttributeDB.
                                              u1WlanId - 1] =
                        pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex;
                }
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bWlanInterfaceIp != OSIX_FALSE)
            {
                pWssWlanBssIfIndexDB->WlanInterfaceIp.u4IpAddr =
                    pWssWlanDB->WssWlanAttributeDB.WlanInterfaceIp.u4IpAddr;
                pWssWlanBssIfIndexDB->WlanInterfaceIp.u4SubMask =
                    pWssWlanDB->WssWlanAttributeDB.WlanInterfaceIp.u4SubMask;
                pWssWlanBssIfIndexDB->WlanInterfaceIp.u1AdminStatus =
                    pWssWlanDB->WssWlanAttributeDB.WlanInterfaceIp.
                    u1AdminStatus;
                pWssWlanBssIfIndexDB->WlanInterfaceIp.u1RowStatus =
                    pWssWlanDB->WssWlanAttributeDB.WlanInterfaceIp.u1RowStatus;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bVlanId != OSIX_FALSE)
            {
                pWssWlanBssIfIndexDB->u2VlanId =
                    pWssWlanDB->WssWlanAttributeDB.u2VlanId;
            }
            break;
        case WSS_WLAN_CREATE_SSID_MAPPING_ENTRY:
            if ((MEMCMP
                 ((pWssWlanDB->WssWlanAttributeDB.au1DesiredSsid), NullMac,
                  sizeof (tMacAddr))) == OSIX_FALSE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:SSID is empty\
                        creation of SsidMappingDb failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if (pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        Wlan Index value invalid\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if ((pWssWlanSSIDMappingDB =
                 (tWssWlanSSIDMappingDB *)
                 MemAllocMemBlk (WSSWLAN_SSID_MAPPING_DB_POOLID)) == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:Memory allocation failed\
                        for SSIDMappingDB\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            MEMSET (pWssWlanSSIDMappingDB, 0, sizeof (tWssWlanSSIDMappingDB));
            MEMCPY (pWssWlanSSIDMappingDB->au1SSID,
                    pWssWlanDB->WssWlanAttributeDB.au1DesiredSsid,
                    STRLEN (pWssWlanDB->WssWlanAttributeDB.au1DesiredSsid));

            pWssWlanSSIDMappingDB->u4WlanIfIndex =
                pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
            if (RBTreeAdd
                (gWssWlanGlobals.WssWlanGlbMib.WssWlanSSIDMappingDB,
                 (tRBElem *) pWssWlanSSIDMappingDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (WSSWLAN_SSID_MAPPING_DB_POOLID,
                                    (UINT1 *) pWssWlanSSIDMappingDB);
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        SSID Mapping DB RBTreeAdd failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            break;
        case WSS_WLAN_DESTROY_SSID_MAPPING_ENTRY:
            if ((MEMCMP
                 ((pWssWlanDB->WssWlanAttributeDB.au1DesiredSsid), NullMac,
                  sizeof (tMacAddr))) == OSIX_FALSE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        SSID is empty . SsidMappingDB failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            MEMCPY (WssWlanSSIDMappingDB.au1SSID,
                    pWssWlanDB->WssWlanAttributeDB.au1DesiredSsid,
                    STRLEN (pWssWlanDB->WssWlanAttributeDB.au1DesiredSsid));
            pWssWlanSSIDMappingDB =
                RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.WssWlanSSIDMappingDB,
                           (tRBElem *) & WssWlanSSIDMappingDB);

            if (pWssWlanSSIDMappingDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:WssWlanSSIDMappingDB\
                        RBTreeGet failed \r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;

            }
            RBTreeRem (gWssWlanGlobals.WssWlanGlbMib.WssWlanSSIDMappingDB,
                       &WssWlanSSIDMappingDB);
            MEMSET (pWssWlanSSIDMappingDB, 0, sizeof (tWssWlanSSIDMappingDB));
            MemReleaseMemBlock (WSSWLAN_SSID_MAPPING_DB_POOLID,
                                (UINT1 *) pWssWlanSSIDMappingDB);

            break;

        case WSS_WLAN_GET_SSID_MAPPING_ENTRY:
            MEMSET (&WssWlanSSIDMappingDB, 0, sizeof (tWssWlanSSIDMappingDB));
            if ((MEMCMP
                 ((pWssWlanDB->WssWlanAttributeDB.au1DesiredSsid), NullMac,
                  sizeof (tMacAddr))) != OSIX_FALSE)
            {
                MEMCPY (WssWlanSSIDMappingDB.au1SSID,
                        pWssWlanDB->WssWlanAttributeDB.au1DesiredSsid,
                        STRLEN (pWssWlanDB->WssWlanAttributeDB.au1DesiredSsid));
                pWssWlanSSIDMappingDB =
                    RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.
                               WssWlanSSIDMappingDB,
                               (tRBElem *) & WssWlanSSIDMappingDB);

                if (pWssWlanSSIDMappingDB == NULL)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                            RBTreeGet from WlanSSIDMappingDB failed\r\n");
                    WSSIF_UNLOCK;
                    return OSIX_FAILURE;
                }

            }
            else
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        Key value SSID is empty\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex =
                    pWssWlanSSIDMappingDB->u4WlanIfIndex;
            }
            break;

        case WSS_WLAN_CREATE_BSSID_MAPPING_ENTRY:
            if ((MEMCMP
                 ((pWssWlanDB->WssWlanAttributeDB.BssId), NullMac,
                  sizeof (tMacAddr))) == OSIX_FALSE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:BSS Mac Addr is empty\
                        ,BssIdMapping creation failure \r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            if (pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessWssWlanDBMsg:BssIfIndex empty,\
                        BssIdMapping Creation failed \r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if ((pWssWlanBssIdMappingDB =
                 (tWssWlanBssIdMappingDB *)
                 MemAllocMemBlk (WSSWLAN_BSSID_MAPPING_DB_POOLID)) == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        Memory allocation failed for BssIdMappingDB\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            MEMSET (pWssWlanBssIdMappingDB, 0, sizeof (tWssWlanBssIdMappingDB));
            MEMCPY (pWssWlanBssIdMappingDB->BssId,
                    pWssWlanDB->WssWlanAttributeDB.BssId, MAC_ADDR_LEN);

            if (RBTreeAdd
                (gWssWlanGlobals.WssWlanGlbMib.WssWlanBssIdMappingDB,
                 (tRBElem *) pWssWlanBssIdMappingDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (WSSWLAN_BSSID_MAPPING_DB_POOLID,
                                    (UINT1 *) pWssWlanBssIdMappingDB);
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        SSID Mapping DB RBTreeAdd failed \r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            pWssWlanBssIdMappingDB->u4BssIfIndex =
                pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex;

            break;
        case WSS_WLAN_DESTROY_BSSID_MAPPING_ENTRY:
            if ((MEMCMP
                 ((pWssWlanDB->WssWlanAttributeDB.BssId), NullMac,
                  sizeof (tMacAddr))) == OSIX_FALSE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        BSS Mac Address is empty\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            MEMCPY (WssWlanBssIdMappingDB.BssId,
                    pWssWlanDB->WssWlanAttributeDB.BssId, MAC_ADDR_LEN);
            pWssWlanBssIdMappingDB =
                RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.WssWlanBssIdMappingDB,
                           (tRBElem *) & WssWlanBssIdMappingDB);

            if (pWssWlanBssIdMappingDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        WssWlanBssIdMappingDB RBTreeGet failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;

            }
            RBTreeRem (gWssWlanGlobals.WssWlanGlbMib.WssWlanBssIdMappingDB,
                       &WssWlanBssIdMappingDB);
            MemReleaseMemBlock (WSSWLAN_BSSID_MAPPING_DB_POOLID,
                                (UINT1 *) pWssWlanBssIdMappingDB);

            break;
        case WSS_WLAN_SET_BSSID_MAPPING_ENTRY:
            MEMSET (&WssWlanBssIdMappingDB, 0, sizeof (tWssWlanBssIdMappingDB));
            if ((MEMCMP
                 ((pWssWlanDB->WssWlanAttributeDB.BssId), NullMac,
                  sizeof (tMacAddr))) != OSIX_FALSE)
            {
                MEMCPY (WssWlanBssIdMappingDB.BssId,
                        pWssWlanDB->WssWlanAttributeDB.BssId, MAC_ADDR_LEN);
                pWssWlanBssIdMappingDB =
                    RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.
                               WssWlanBssIdMappingDB,
                               (tRBElem *) & WssWlanBssIdMappingDB);

                if (pWssWlanBssIdMappingDB == NULL)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                            RBTreeGet from WlanBssIdMappingDB failed, Unable to SET\r\n");
                    WSSIF_UNLOCK;
                    return OSIX_FAILURE;
                }

            }
            else
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        Key value BssMacAddr is empty\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if (pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex != OSIX_FALSE)
            {
                pWssWlanBssIdMappingDB->u4BssIfIndex =
                    pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bRuleId != OSIX_FALSE)
            {
                pWssWlanBssIdMappingDB->u2RuleId =
                    pWssWlanDB->WssWlanAttributeDB.u2RuleId;
            }
            break;
        case WSS_WLAN_GET_BSSID_MAPPING_ENTRY:
            MEMSET (&WssWlanBssIdMappingDB, 0, sizeof (tWssWlanBssIdMappingDB));
            pu1WssWlanBssId = pWssWlanDB->WssWlanAttributeDB.BssId;
            if (pu1WssWlanBssId != NULL)
            {
                MEMCPY (WssWlanBssIdMappingDB.BssId,
                        pWssWlanDB->WssWlanAttributeDB.BssId, MAC_ADDR_LEN);
                pWssWlanBssIdMappingDB =
                    RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.
                               WssWlanBssIdMappingDB,
                               (tRBElem *) & WssWlanBssIdMappingDB);

                if (pWssWlanBssIdMappingDB == NULL)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                            RBTreeGet from WlanBssIdMappingDB failed\r\n");
                    WSSIF_UNLOCK;
                    return OSIX_FAILURE;
                }

            }
            else
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessWssWlanDBMsg:\
                        Key value BssMacAddr is empty\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bRuleId != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u2RuleId =
                    pWssWlanBssIdMappingDB->u2RuleId;
            }
            if (pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex =
                    pWssWlanBssIdMappingDB->u4BssIfIndex;
                WssWlanBssIfIndexDB.u4BssIfIndex =
                    pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex;

                pWssWlanBssIfIndexDB =
                    RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.
                               WssWlanBssIfIndexDB,
                               (tRBElem *) & WssWlanBssIfIndexDB);
                if (pWssWlanBssIfIndexDB == NULL)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC, "\nWssIfProcessWssWlanDBMsg:\
                            RBTreeGet from BssIfIndexDB failed\r\n");
                    WSSIF_UNLOCK;
                    return OSIX_FAILURE;
                }
                if (pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex != OSIX_FALSE)
                {
                    WssWlanIfIndexDB.u4WlanIfIndex =
                        gaWssWlanInternalProfileDB[pWssWlanBssIfIndexDB->
                                                   u2WlanInternalId -
                                                   1].pWssWlanIfIndexDB->
                        u4WlanIfIndex;

                    pWssWlanIfIndexDB =
                        RBTreeGet (gWssWlanGlobals.WssWlanGlbMib.
                                   WssWlanIfIndexDB,
                                   (tRBElem *) & WssWlanIfIndexDB);

                    if (pWssWlanIfIndexDB == NULL)
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "WssIfProcessWssWlanDBMsg:\
                                No entry found for BSSID,\
                                pWlanIfIndex not exists\r\n");
                        WSSIF_UNLOCK;
                        return OSIX_FAILURE;
                    }

                    pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex =
                        WssWlanIfIndexDB.u4WlanIfIndex;

                    if (pWssWlanDB->WssWlanIsPresentDB.bWebAuthStatus
                        != OSIX_FALSE)
                    {
                        pWssWlanDB->WssWlanAttributeDB.u1WebAuthStatus =
                            pWssWlanIfIndexDB->u1WebAuthStatus;
                    }
                    if (pWssWlanDB->WssWlanIsPresentDB.bAuthMethod
                        != OSIX_FALSE)
                    {
                        pWssWlanDB->WssWlanAttributeDB.u1AuthMethod =
                            pWssWlanIfIndexDB->u1AuthMethod;
                    }

                    if (pWssWlanDB->WssWlanIsPresentDB.bWepKey != OSIX_FALSE)
                    {
                        MEMCPY (pWssWlanDB->WssWlanAttributeDB.au1WepKey,
                                pWssWlanIfIndexDB->au1WepKey,
                                STRLEN (pWssWlanIfIndexDB->au1WepKey));
                    }

                    if (pWssWlanDB->WssWlanIsPresentDB.bQosRateLimit
                        != OSIX_FALSE)
                    {
                        pWssWlanDB->WssWlanAttributeDB.u1QosRateLimit =
                            pWssWlanIfIndexDB->u1QosRateLimit;
                    }

                    if (pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamCIR
                        != OSIX_FALSE)
                    {
                        pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamCIR =
                            pWssWlanIfIndexDB->u4QosUpStreamCIR;
                    }

                    if (pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamCBS
                        != OSIX_FALSE)
                    {
                        pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamCBS =
                            pWssWlanIfIndexDB->u4QosUpStreamCBS;
                    }

                    if (pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamEIR
                        != OSIX_FALSE)
                    {
                        pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamEIR =
                            pWssWlanIfIndexDB->u4QosUpStreamEIR;
                    }

                    if (pWssWlanDB->WssWlanIsPresentDB.bQosUpStreamEBS
                        != OSIX_FALSE)
                    {
                        pWssWlanDB->WssWlanAttributeDB.u4QosUpStreamEBS =
                            pWssWlanIfIndexDB->u4QosUpStreamEBS;
                    }

                    if (pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamCIR
                        != OSIX_FALSE)
                    {
                        pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamCIR =
                            pWssWlanIfIndexDB->u4QosDownStreamCIR;
                    }

                    if (pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamCBS
                        != OSIX_FALSE)
                    {
                        pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamCBS =
                            pWssWlanIfIndexDB->u4QosDownStreamCBS;
                    }

                    if (pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamEIR
                        != OSIX_FALSE)
                    {
                        pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamEIR =
                            pWssWlanIfIndexDB->u4QosDownStreamEIR;
                    }

                    if (pWssWlanDB->WssWlanIsPresentDB.bQosDownStreamEBS
                        != OSIX_FALSE)
                    {
                        pWssWlanDB->WssWlanAttributeDB.u4QosDownStreamEBS =
                            pWssWlanIfIndexDB->u4QosDownStreamEBS;
                    }

                }
                pWssWlanDB->WssWlanAttributeDB.u1WlanId =
                    pWssWlanBssIfIndexDB->u1WlanId;
                pWssWlanDB->WssWlanAttributeDB.u2WtpInternalId =
                    pWssWlanBssIfIndexDB->pRadioIfDB->u2WtpInternalId;
                pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex =
                    pWssWlanBssIfIndexDB->pRadioIfDB->u4VirtualRadioIfIndex;
                if (pWssWlanBssIfIndexDB->u2WlanInternalId <= 0)
                {
                    WSSIF_UNLOCK;
                    return OSIX_FAILURE;
                }
                else
                {
                    pWssWlanDB->WssWlanAttributeDB.u2Capability =
                        gaWssWlanInternalProfileDB[pWssWlanBssIfIndexDB->
                                                   u2WlanInternalId -
                                                   1].pWssWlanIfIndexDB->
                        u2Capability;
                }
            }
            break;

        case WSS_WLAN_GET_INTERNAL_PROFILE_ENTRY:
            if (pWssWlanDB->WssWlanAttributeDB.u2WlanInternalId == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "\nWssIfProcessWssWlanDBMsg:\
                        Invalid Internal Id, create failed\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;

            }

            u2WlanInternalId =
                (UINT2) (pWssWlanDB->WssWlanAttributeDB.u2WlanInternalId - 1);

            if (pWssWlanDB->WssWlanIsPresentDB.bWlanProfileId != OSIX_FALSE)
            {
                pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId =
                    (gaWssWlanInternalProfileDB[u2WlanInternalId].
                     u2WlanProfileId);
            }
            else
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "\nWssIfProcessWssWlanDBMsg:\
                        Key value u2WlanProfileId is empty\r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if (pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "\nWssIfProcessWssWlanDBMsg:\
                        Entry not found for WLAN profile \r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            break;
        case WSS_WLAN_SET_MANAGMENT_SSID:

            MEMCPY (gWssWlanGlobals.WssWlanGlbMib.au1ManagmentSSID,
                    pWssWlanDB->WssWlanAttributeDB.au1ManagmentSSID,
                    WSSWLAN_SSID_NAME_LEN);

            break;
        case WSS_WLAN_GET_MANAGMENT_SSID:
            if (gWssWlanGlobals.WssWlanGlbMib.au1ManagmentSSID[0] == '\0')
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "\nWssIfProcessWssWlanDBMsg:\
                        NULL ManagmentSSID value in DB \r\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;

            }
            MEMCPY (pWssWlanDB->WssWlanAttributeDB.au1ManagmentSSID,
                    gWssWlanGlobals.WssWlanGlbMib.au1ManagmentSSID,
                    WSSWLAN_SSID_NAME_LEN);
            break;

        default:
            break;
    }
    WSSIF_UNLOCK;
    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

UINT4
WssIfWlanUpdateDiffServ (INT4 i4IfIndex, UINT4 u4CapwapDot11WlanProfileId)
{
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;
    INT4                i4RetVal;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
    if ((WssWlanConstructUpdateWlanMsg
         (i4IfIndex, (UINT2) u4CapwapDot11WlanProfileId,
          &(pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
            WssWlanUpdateReq))) == OSIX_FAILURE)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
    MEMCPY (&(pWlcHdlrMsgStruct->WssWlanConfigReq.unWlanConfReq.
              WssWlanUpdateReq),
            &(pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
              unWlanConfReq.WssWlanUpdateReq),
            sizeof (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq));

    pWlcHdlrMsgStruct->WssWlanConfigReq.u1WlanOption = WSSWLAN_UPDATE_REQ;
    pWlcHdlrMsgStruct->WssWlanConfigReq.u2SessId =
        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    WssConstructDiffServVendorMessage (i4IfIndex, u4CapwapDot11WlanProfileId,
                                       pWlcHdlrMsgStruct);
    i4RetVal =
        WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_WLAN_CONF_REQ, pWlcHdlrMsgStruct);
    if (i4RetVal == OSIX_FAILURE)

    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    return OSIX_SUCCESS;
}

UINT4
WssIfWlanRadioIfIpCheck (INT4 i4IfIndex, UINT4 u4CapwapDot11WlanProfileId)
{
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    INT4                i4RetVal;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    pWssWlanMsgStruct =
        (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
    if (pWssWlanMsgStruct == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    if ((WssWlanConstructUpdateWlanMsg
         (i4IfIndex, (UINT2) u4CapwapDot11WlanProfileId,
          &(pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.unWlanConfReq.
            WssWlanUpdateReq))) == OSIX_FAILURE)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
    MEMCPY (&(pWlcHdlrMsgStruct->WssWlanConfigReq.unWlanConfReq.
              WssWlanUpdateReq),
            &(pWssWlanMsgStruct->unWssWlanMsg.WssWlanConfigReq.
              unWlanConfReq.WssWlanUpdateReq),
            sizeof (pWssWlanMsgStruct->unWssWlanMsg.
                    WssWlanConfigReq.unWlanConfReq.WssWlanUpdateReq));

    WssConstructWlanInterfaceVendorMessage (i4IfIndex,
                                            u4CapwapDot11WlanProfileId,
                                            pWlcHdlrMsgStruct);
    pWlcHdlrMsgStruct->WssWlanConfigReq.u2SessId =
        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
    pWlcHdlrMsgStruct->WssWlanConfigReq.u1WlanOption = WSSWLAN_UPDATE_REQ;
    i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_WLAN_CONF_REQ,
                                       pWlcHdlrMsgStruct);

    if (i4RetVal == OSIX_FAILURE)

    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
        return OSIX_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
    return OSIX_SUCCESS;
}

#ifdef BAND_SELECT_WANTED
/****************************************************************************
 *  Function    :  WssIfGetBandSelectStatus
 *  Input       :  Pointer to tWssWlanDB , OpCode
 *  Description :  Invoke the correponding DB Table to set/get DB information
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
UINT1
WssIfGetBandSelectStatus (UINT1 *pBandSelect)
{
    *pBandSelect = gu1BandSelectStatus;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *  Function    :  WssIfSetBandSelectStatus
 *  Input       :  Pointer to tWssWlanDB , OpCode
 *  Description :  Invoke the correponding DB Table to set/get DB information
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
UINT1
WssIfSetBandSelectStatus (UINT1 u1BandSelect)
{
    if (gu1BandSelectStatus != u1BandSelect)
    {
        gu1BandSelectStatus = u1BandSelect;
        WssWlanSendBandSelectStatusAll (gu1BandSelectStatus);
    }
    return OSIX_SUCCESS;
}

#endif
/****************************************************************************
 *  Function    :  WssIfGetLegacyRateStatus
 *  Input       :  Pointer to tWssWlanDB , OpCode
 *  Description :  Invoke the correponding DB Table to set/get DB information
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
UINT1
WssIfGetLegacyRateStatus (UINT1 *pLegacyRate)
{
    *pLegacyRate = gu1LegacyStatus;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *  Function    :  WssIfSetLegacyRateStatus
 *  Input       :  Pointer to tWssWlanDB , OpCode
 *  Description :  Invoke the correponding DB Table to set/get DB information
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
INT4
WssIfSetLegacyRateStatus (UINT1 u1LegacyRate)
{
    INT4                i4RetStatus = 0;
    UINT4              *pu4ProfileId = NULL;
    gu1LegacyStatus = u1LegacyRate;
    i4RetStatus = WssCfgSetLegacyRateStatus (pu4ProfileId);
    return i4RetStatus;
}

#endif
