/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                     *
 *  $Id: wssifstawtp.c,v 1.6 2017/11/24 10:37:10 siva Exp $                  *
 *  Description: This file contains the station update related api for WTP   *
 *                                                                           *
 *****************************************************************************/
#ifndef __WSSSTA_WTP_C__
#define __WSSSTA_WTP_C__

#include "wssifstawtpinc.h"
#include "wssifwtpwlandb.h"
#include "wssifradiodb.h"
#include "wssifwtpwlanproto.h"
#ifdef NPAPI_WANTED
#include "nputil.h"
#endif
#include "wssiftdfs.h"
#ifdef RFMGMT_WANTED
#include "wssmacproto.h"
#endif
#include "wssifextn.h"
#include "wssifradioproto.h"
#define WSSSTA_ACTIVE_FLAG_SET 1

#ifdef KERNEL_CAPWAP_WANTED
#include "fcusprot.h"
#include "radionp.h"
#endif
static tRBTree      gWssStaStateDB;
#ifdef KERNEL_CAPWAP_WANTED
static tRBTree      gWssStaVlanDB;
#endif
#ifdef BAND_SELECT_WANTED
tRBTree             gWssStaAPBandSteerDB;
#endif
tWssStaWtpDBWalk    gaWssStaWtpDBWalk[MAX_STA_SUPP_PER_AP];
static UINT1        gu1DBWalkCnt = 0;
UINT1               gu1WtpClientWalkIndex = 0;
#ifdef KERNEL_CAPWAP_WANTED
extern UINT1        WssWlanGetWlanIntfName (UINT1 u1RadioId, UINT1 u1WlanId,
                                            UINT1 *pu1WlanIntfName,
                                            UINT1 *pu1WlanId);
#else
extern VOID         WssWlanGetWlanIntfName (UINT1 u1RadioId, UINT1 u1WlanId,
                                            UINT1 *pu1WlanIntfName,
                                            UINT1 *pu1WlanId);
#endif

/*****************************************************************************
 *  Function Name   : WssStaInit                                             *
 *                                                                           *
 *  Description     : This function initilizes the WSSSTA module.            *
 *                    The datastructures are initialized.                    *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT4
WssStaInit (VOID)
{
    if (WssifstawtpSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssifstawtpSizingMemCreateMemPools: Mem Pool Creation "
                    "Failed \r\n");
        return OSIX_FAILURE;
    }

    gWssStaStateDB = RBTreeCreateEmbedded ((FSAP_OFFSETOF (tWssStaStateDB,
                                                           nextWssStaStateDB)),
                                           WssStaApCompareStaDBRBTree);
    if (gWssStaStateDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaInit: RB Tree Creation Failed for Station DB \r\n");
        return OSIX_FAILURE;
    }

    gWssStaTCFilterHandleDB =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssStaTCFilterHandle,
                                nextWssStaTCFilterHandle)),
                              WssStaCompareTCFilterHandleRBTree);

    if (gWssStaTCFilterHandleDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssstaInit: gWssStaTCFilterHandleDB RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
#ifdef BAND_SELECT_WANTED
    gWssStaAPBandSteerDB =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssStaBandSteerDB, StaBandSteerDBNode)),
                              WssStaCompareWtpBandSteerDBRBTree);
    if (gWssStaAPBandSteerDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssstaInit: gWssStaAPBandSteerDB RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
#endif

#ifdef KERNEL_CAPWAP_WANTED
    gWssStaVlanDB = RBTreeCreateEmbedded ((FSAP_OFFSETOF (tWssStaVlanDB,
                                                          nextWssStaVlanDB)),
                                          WssStaApCompareVlanDBRBTree);
    if (gWssStaVlanDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaInit: RB Tree Creation Failed for Sta VLAN DB \r\n");
        return OSIX_FAILURE;
    }
#endif

    return OSIX_SUCCESS;
}

VOID
WssStaApRecvMemClear (VOID)
{

    WssifstawtpSizingMemDeleteMemPools ();
    return;
}

/*****************************************************************************
 *  Function Name   : StaApDBWalkFn                                          *
 *                                                                           *
 *  Description     : RBTree Walk function for Station DB at WTP             *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
StaApDBWalkFn (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
               void *pArg, void *pOut)
{
    tWssStaStateDB     *pWssStaDB = NULL;
    tWssMacMsgStruct   *pwssDeauthMsg = NULL;
    pwssDeauthMsg =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pwssDeauthMsg == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "StaApDBWalkFn:-"
                    "UtlShMemAllocMacMsgStructBuf returned failure\n");
        return OSIX_FAILURE;
    }

    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (pArg);
    UNUSED_PARAM (pOut);

    MEMSET (&(pwssDeauthMsg->unMacMsg.DeauthMacFrame), 0,
            sizeof (tDot11DeauthMacFrame));

    if (visit == postorder || visit == leaf)
    {
        if (pRBElem != NULL)
        {
            pWssStaDB = (tWssStaStateDB *) pRBElem;
            if (pWssStaDB->staActiveFlag == FALSE)
            {
                MEMCPY (pwssDeauthMsg->unMacMsg.DeauthMacFrame.MacMgmtFrmHdr.
                        u1DA, pWssStaDB->stationMacAddress, sizeof (tMacAddr));
                if (gu1DBWalkCnt < MAX_STA_SUPP_PER_AP)
                {
                    MEMCPY (gaWssStaWtpDBWalk[gu1DBWalkCnt].staMacAddr,
                            pWssStaDB->stationMacAddress, sizeof (tMacAddr));
                    gu1DBWalkCnt++;
                }
            }
            else
            {
                pWssStaDB->staActiveFlag = FALSE;
            }
        }
    }
    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pwssDeauthMsg);
    return RB_WALK_CONT;
}

/*****************************************************************************
 *  Function Name   : WssStaIdleTimeoutExpiry                                *
 *                                                                           *
 *  Description     : Expiry Function for station idle timeout               *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT4
WssStaIdleTimeoutExpiry (VOID)
{

    tWssStaStateDB     *pWssStaDB = NULL;
    UINT1               u1Index = 0;
    tWssStaStateDB      WssStaDB;
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
#endif
#ifdef RFMGMT_WANTED
    tRfMgmtMsgStruct    RfMgmtMsgStruct;

    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
#endif
    unApHdlrMsgStruct   ApHdlrMsgStruct;

    gu1DBWalkCnt = 0;
#ifdef KERNEL_CAPWAP_WANTED
    RBTreeWalk (gWssStaStateDB, (tRBWalkFn) StaDBWalkFnForGetStation, NULL, 0);
#else
    /* This function calculates the number of stations connected */
    RBTreeWalk (gWssStaStateDB, (tRBWalkFn) StaApDBWalkFn, NULL, 0);
#endif

    /* If the number of stations are non-zero, then we need to check 
     * whether station is active or not */
    if (gu1DBWalkCnt != 0)
    {
        /* Scan all the stations and take the action */
        for (u1Index = 0; u1Index < gu1DBWalkCnt; u1Index++)
        {
            MEMSET (&WssStaDB, 0, sizeof (tWssStaStateDB));
            MEMCPY (WssStaDB.stationMacAddress,
                    gaWssStaWtpDBWalk[u1Index].staMacAddr, sizeof (tMacAddr));

            pWssStaDB = ((tWssStaStateDB *) RBTreeGet (gWssStaStateDB,
                                                       (tRBElem *) & WssStaDB));
            if (pWssStaDB == NULL)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WssStaIdleTimeoutExpiry: Failed to "
                            "retrieve data from  Station DB \r\n");
                continue;
            }
            else
            {
#ifdef KERNEL_CAPWAP_WANTED
                if (FcusGetStationIdleSatus (WssStaDB.stationMacAddress) ==
                    OSIX_SUCCESS)
                {
                    /* There is an activity on the station, hence continuing 
                     * the scan for next station, without taking any action on 
                     * the present station */
                    continue;
                }
#endif
                ApHdlrMsgStruct.StationWtpEventReq.u2SessId =
                    pWssStaDB->u2SessId;
                ApHdlrMsgStruct.StationWtpEventReq.deletestation.isPresent =
                    TRUE;
                ApHdlrMsgStruct.StationWtpEventReq.deletestation.u2MessageType =
                    DELETE_STATION;
                ApHdlrMsgStruct.StationWtpEventReq.deletestation.u2MessageLength
                    = WSSSTA_DELSTA_LEN;
                ApHdlrMsgStruct.StationWtpEventReq.deletestation.u1RadioId =
                    pWssStaDB->u1RadioId;
                ApHdlrMsgStruct.StationWtpEventReq.deletestation.u1MacAddrLen =
                    sizeof (tMacAddr);
                MEMCPY (ApHdlrMsgStruct.StationWtpEventReq.
                        deletestation.StaMacAddr,
                        pWssStaDB->stationMacAddress, sizeof (tMacAddr));

                if (WssIfProcessApHdlrMsg (WSS_APHDLR_STAION_WTP_EVENT_REQ,
                                           &ApHdlrMsgStruct) == OSIX_SUCCESS)
                {
#ifdef NPAPI_WANTED
                    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.
                        RadInfo.u4RadioIfIndex =
                        pWssStaDB->u1RadioId + SYS_DEF_MAX_ENET_INTERFACES;
                    SPRINTF ((CHR1 *) FsHwNp.RadioIfNpModInfo.unOpCode.
                             sConfigDot11StaAuthDeAuth.RadInfo.au1RadioIfName,
                             "%s%d", RADIO_INTF_NAME,
                             ApHdlrMsgStruct.StationWtpEventReq.deletestation.
                             u1RadioId - 1);
                    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.
                        RadInfo.au2WlanIfIndex[0] = pWssStaDB->u1WlanId;

                    WssWlanGetWlanIntfName (pWssStaDB->u1RadioId,
                                            pWssStaDB->u1WlanId,
                                            FsHwNp.RadioIfNpModInfo.unOpCode.
                                            sConfigDot11StaAuthDeAuth.RadInfo.
                                            au1WlanIfname[0],
                                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                                            sConfigDot11StaAuthDeAuth.RadInfo.
                                            u1WlanId);

                    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.
                        u1AuthStat = 2;
                    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigDot11StaAuthDeAuth.au1StaAuthMac,
                            pWssStaDB->stationMacAddress, sizeof (tMacAddr));
                    FsHwNp.u4Opcode = FS_RADIO_HW_STA_AUTH_DEAUTH;    /* 88 */
                    if (RadioNpWrHwProgram (&FsHwNp) == FNP_FAILURE)
                    {
                    }
#endif
#ifdef RFMGMT_WANTED
                    RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtStaProcessReq.u1RadioId
                        = pWssStaDB->u1RadioId;
                    MEMCPY (RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtStaProcessReq.
                            ClientMac, pWssStaDB->stationMacAddress,
                            sizeof (tMacAddr));

                    if (WssIfProcessRfMgmtMsg (RFMGMT_DELETE_CLIENT_STATS,
                                               &RfMgmtMsgStruct) !=
                        OSIX_SUCCESS)
                    {
                        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                    "WssStaProcessReAssocFrames: "
                                    "RF module processing failed\r\n");
                    }
#endif

                    RBTreeRem (gWssStaStateDB, (tRBElem *) pWssStaDB);
                    MemReleaseMemBlock (WSSSTA_AP_STADB_POOLID,
                                        (UINT1 *) pWssStaDB);
                }
                else
                {
                    return OSIX_FAILURE;
                }
            }
        }
    }
    else
    {
        /* No stations connected on WLC. Hence require 
           no need to take any action when Idle time expires */
    }
    return OSIX_SUCCESS;

}

/*****************************************************************************
 *  Function Name   : WssStaUpdateDB                                         *
 *                                                                           *
 *  Description     : This function used to Update the WTP session DB.       *
 *                    When the WLC sends a Station config request to the AP, *
 *                    the APHDLR invokes this API to update the station DB   *
 *                    with the Information from CAPWAP                       *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/

UINT4
WssStaUpdateDB (tWssStaConfigReqInfo * pStaConfigReqinfo)
{
    UINT4               u4Retval = OSIX_SUCCESS;
    tWssStaStateDB     *pWssStaDB = NULL;
    tWssStaStateDB      wssStaStateDB;
    UINT1               u1AddFlag = 0;
    UINT1               u1Count = 0;
    UINT1               u1InterfaceName[6] = "eth0";
    UINT1               u1DeleteFlag = OSIX_FALSE;
    tWssWlanMsgStruct  *pWssWlanMsgStruct = NULL;
    UINT1               u1WlanId = 0;
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               u1TempWlanId = 0;
    UINT1               au1WlanName[5];
#ifdef KERNEL_CAPWAP_WANTED
    tWssStaVlanDB       WssStaVlanDB;
#endif
#endif
#ifdef RFMGMT_WANTED
    tRfMgmtMsgStruct    RfMgmtMsgStruct;

    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
#endif
#ifdef NPAPI_WANTED
#ifdef KERNEL_CAPWAP_WANTED
    MEMSET (&WssStaVlanDB, 0, sizeof (tWssStaVlanDB));
#endif
#endif

    MEMSET (&wssStaStateDB, 0, sizeof (tWssStaStateDB));

    /* The AddStation element is present */
    if (pStaConfigReqinfo->AddSta.isPresent)
    {
#ifdef NPAPI_WANTED
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.RadInfo.
            u4RadioIfIndex = pStaConfigReqinfo->AddSta.u1RadioId +
            SYS_DEF_MAX_ENET_INTERFACES;
        SPRINTF ((CHR1 *) FsHwNp.RadioIfNpModInfo.unOpCode.
                 sConfigDot11StaAuthDeAuth.RadInfo.au1RadioIfName, "%s%d",
                 RADIO_INTF_NAME, pStaConfigReqinfo->AddSta.u1RadioId - 1);

        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.RadInfo.
            au2WlanIfIndex[0] = pStaConfigReqinfo->StaMsg.u1WlanId;

        WssWlanGetWlanIntfName (pStaConfigReqinfo->AddSta.u1RadioId,
                                pStaConfigReqinfo->StaMsg.u1WlanId,
                                FsHwNp.RadioIfNpModInfo.unOpCode.
                                sConfigDot11StaAuthDeAuth.RadInfo.
                                au1WlanIfname[0],
                                &FsHwNp.RadioIfNpModInfo.unOpCode.
                                sConfigDot11StaAuthDeAuth.RadInfo.u1WlanId);

        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.u1AuthStat =
            WLAN_CLIENT_ADD;
        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.
                au1StaAuthMac,
                pStaConfigReqinfo->AddSta.StaMacAddress, sizeof (tMacAddr));
        FsHwNp.u4Opcode = FS_RADIO_HW_STA_AUTH_DEAUTH;    /* 88 */
#ifdef KERNEL_CAPWAP_WANTED
        MEMCPY (u1InterfaceName, FsHwNp.RadioIfNpModInfo.unOpCode.
                sConfigDot11StaAuthDeAuth.RadInfo.au1WlanIfname[0],
                STRLEN (FsHwNp.RadioIfNpModInfo.unOpCode.
                        sConfigDot11StaAuthDeAuth.RadInfo.au1WlanIfname[0]));
        /* Invoke Station module to create vlan DB */
        WssStaVlanDB.u2VlanId = pStaConfigReqinfo->AddSta.u2VlanId;

        if (WssStaUpdateVlanDB (&WssStaVlanDB, WSSSTA_VLAN_GET) == OSIX_SUCCESS)
        {
            if (WssStaVlanDB.AddStaFlag == OSIX_FALSE)
            {
                WssStaVlanDB.AddStaFlag = OSIX_TRUE;
                WssStaUpdateVlanDB (&WssStaVlanDB, WSSSTA_VLAN_SET);
            }
        }
        else
        {
            WssStaUpdateVlanDB (&WssStaVlanDB, WSSSTA_VLAN_CREATE);
            WssStaVlanDB.AddStaFlag = OSIX_TRUE;
            WssStaUpdateVlanDB (&WssStaVlanDB, WSSSTA_VLAN_SET);
        }
        SPRINTF ((CHR1 *) FsHwNp.RadioIfNpModInfo.unOpCode.
                 sConfigDot11StaAuthDeAuth.au1IntfName, "%s%d",
                 RADIO_INTF_NAME, pStaConfigReqinfo->AddSta.u1RadioId - 1);
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.
            u2VlanId = pStaConfigReqinfo->AddSta.u2VlanId;
#endif
#endif

        MEMCPY (wssStaStateDB.stationMacAddress,
                (pStaConfigReqinfo->AddSta.StaMacAddress), sizeof (tMacAddr));
        pWssStaDB = ((tWssStaStateDB *) RBTreeGet (gWssStaStateDB,
                                                   (tRBElem *) &
                                                   wssStaStateDB));

        if (pWssStaDB == NULL)
        {
            pWssStaDB = (tWssStaStateDB *)
                MemAllocMemBlk (WSSSTA_AP_STADB_POOLID);
            if (pWssStaDB == NULL)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaUpdateDB: "
                            "Memory Allocation Failed For AP Station DB \r\n");
                return OSIX_FAILURE;
            }
            MEMSET (pWssStaDB, 0, sizeof (tWssStaStateDB));
            u1AddFlag = 1;
        }

        MEMCPY (pWssStaDB->stationMacAddress,
                pStaConfigReqinfo->AddSta.StaMacAddress, sizeof (tMacAddr));
        pWssStaDB->u1RadioId = pStaConfigReqinfo->AddSta.u1RadioId;
        pWssStaDB->u2VlanId = pStaConfigReqinfo->AddSta.u2VlanId;

        /* The Station active flag is set when it is initially added */
        pWssStaDB->staActiveFlag = WSSSTA_ACTIVE_FLAG_SET;
        pWssStaDB->u2SessId = pStaConfigReqinfo->u2SessId;

        if (u1AddFlag)
        {
            /* Add a new entry to the Station DB */
            if (RBTreeAdd (gWssStaStateDB, pWssStaDB) != RB_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WssStaUpdateDB: Failed to add "
                            "entry to the RB Tree \r\n");
                MemReleaseMemBlock (WSSSTA_AP_STADB_POOLID,
                                    (UINT1 *) pWssStaDB);
                return OSIX_FAILURE;
            }
        }
    }

    /* Updating WMM status of Station into Station DB */
    if (pStaConfigReqinfo->VendSpec.WMMStaVendor.isOptional == OSIX_TRUE)
    {
        /* Get the RB Tree node from the tree */

        MEMCPY (wssStaStateDB.stationMacAddress,
                (pStaConfigReqinfo->VendSpec.WMMStaVendor.StaMacAddr),
                sizeof (tMacAddr));

        pWssStaDB =
            ((tWssStaStateDB *)
             RBTreeGet (gWssStaStateDB, (tRBElem *) & wssStaStateDB));

        if (pWssStaDB == NULL)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaUpdateDB - Sta: "
                        "The entry for the Station MAC is not "
                        "available in the Station DB");
            return OSIX_FAILURE;
        }
        pWssStaDB->u1WmmFlag =
            pStaConfigReqinfo->VendSpec.WMMStaVendor.u1WmmStaFlag;
        pWssStaDB->u2TcpPort =
            pStaConfigReqinfo->VendSpec.WMMStaVendor.u2TcpPort;
        pWssStaDB->u4BandwidthThresh =
            pStaConfigReqinfo->VendSpec.WMMStaVendor.u4BandwidthThresh;
#ifdef KERNEL_CAPWAP_WANTED
        if (WssStaUpdateKernelDB (pWssStaDB->u1RadioId,
                                  pWssStaDB->u1WlanId,
                                  pStaConfigReqinfo->VendSpec.WMMStaVendor.
                                  StaMacAddr) == OSIX_FAILURE)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaUpdateDB - WebAuthStatus: "
                        "Kernel DB updation Failed");
        }
#endif
        /* Update and TC filters only if the bandwidth thresh is non zero. zero
         * indicates that the TC filters need not be added for rate limiting */
        if (pWssStaDB->u4BandwidthThresh != 0)
        {
#ifdef NPAPI_WANTED
            WssWlanGetWlanIntfName (pWssStaDB->u1RadioId, pWssStaDB->u1WlanId,
                                    u1InterfaceName, &u1WlanId);
            u1DeleteFlag = OSIX_FALSE;
            WSSUserRoleConfigBandwidth (pWssStaDB->stationMacAddress,
                                        pWssStaDB->u4BandwidthThresh,
                                        pWssStaDB->u4BandwidthThresh,
                                        pWssStaDB->u4BandwidthThresh,
                                        u1InterfaceName, u1DeleteFlag);
#endif
            WSSSTA_TRC1 (WSSSTA_FAILURE_TRC,
                         "\nADD STATION :: WSSUserRoleConfigBandwidth  :: Bandwidth : %d ",
                         pWssStaDB->u4BandwidthThresh);
            WSSSTA_TRC6 (WSSSTA_FAILURE_TRC,
                         "Station MAC : %02x:%02x:%02x:%02x:%02x:%02x",
                         pWssStaDB->stationMacAddress[0],
                         pWssStaDB->stationMacAddress[1],
                         pWssStaDB->stationMacAddress[2],
                         pWssStaDB->stationMacAddress[3],
                         pWssStaDB->stationMacAddress[4],
                         pWssStaDB->stationMacAddress[5]);

        }
    }

    if (pStaConfigReqinfo->VendSpec.isOptional == OSIX_TRUE)
    {
        /* Get the RB Tree node from the tree */

        MEMCPY (wssStaStateDB.stationMacAddress,
                (pStaConfigReqinfo->VendSpec.StaMacAddr), sizeof (tMacAddr));

        pWssStaDB =
            ((tWssStaStateDB *)
             RBTreeGet (gWssStaStateDB, (tRBElem *) & wssStaStateDB));

        if (pWssStaDB == NULL)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaUpdateDB - Sta: "
                        "The entry for the Station MAC is not "
                        "available in the Station DB");
            return OSIX_FAILURE;
        }
#ifdef NPAPI_WANTED
        WssWlanGetWlanIntfName (pWssStaDB->u1RadioId, pWssStaDB->u1WlanId,
                                u1InterfaceName, &u1WlanId);
        pWssStaDB->u4BandWidth = pStaConfigReqinfo->VendSpec.u4BandWidth;
        pWssStaDB->u4DLBandWidth = pStaConfigReqinfo->VendSpec.u4DLBandWidth;
        pWssStaDB->u4ULBandWidth = pStaConfigReqinfo->VendSpec.u4ULBandWidth;
        u1DeleteFlag = OSIX_FALSE;

        WSSUserRoleConfigBandwidth (pWssStaDB->stationMacAddress,
                                    pWssStaDB->u4BandWidth,
                                    pWssStaDB->u4DLBandWidth,
                                    pWssStaDB->u4ULBandWidth, u1InterfaceName,
                                    u1DeleteFlag);
#endif
    }
    /* Check the Vendor specific payload */
    /************Not done for Phase 1***********/
    /* Check if there is an Delete Station request */
    if (pStaConfigReqinfo->DelSta.isPresent)
    {

        /* Since the wlan id is not recieved in delete station message,
         * obtaining the wlan id from the station database*/
        MEMCPY (wssStaStateDB.stationMacAddress,
                (pStaConfigReqinfo->DelSta.StaMacAddr), sizeof (tMacAddr));
        pWssStaDB = ((tWssStaStateDB *) RBTreeGet (gWssStaStateDB,
                                                   (tRBElem *) &
                                                   wssStaStateDB));

#ifdef NPAPI_WANTED
        if (pWssStaDB == NULL)
        {
            return OSIX_FAILURE;
        }
        if (pWssStaDB != NULL)
        {
            u1TempWlanId = pWssStaDB->u1WlanId - 1;
        }
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.RadInfo.
            u4RadioIfIndex = pStaConfigReqinfo->DelSta.u1RadioId +
            SYS_DEF_MAX_ENET_INTERFACES;
        SPRINTF ((CHR1 *) FsHwNp.RadioIfNpModInfo.unOpCode.
                 sConfigDot11StaAuthDeAuth.RadInfo.au1RadioIfName, "%s%d",
                 RADIO_INTF_NAME, pStaConfigReqinfo->DelSta.u1RadioId - 1);

        WssWlanGetWlanIntfName (pWssStaDB->u1RadioId,
                                pWssStaDB->u1WlanId,
                                FsHwNp.RadioIfNpModInfo.unOpCode.
                                sConfigDot11StaAuthDeAuth.RadInfo.
                                au1WlanIfname[0],
                                &FsHwNp.RadioIfNpModInfo.unOpCode.
                                sConfigDot11StaAuthDeAuth.RadInfo.u1WlanId);

        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.u1AuthStat =
            WLAN_CLIENT_DELETE;
        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.
                sConfigDot11StaAuthDeAuth.au1StaAuthMac,
                pStaConfigReqinfo->DelSta.StaMacAddr, sizeof (tMacAddr));
        FsHwNp.u4Opcode = FS_RADIO_HW_STA_AUTH_DEAUTH;    /* 88 */
        if (RadioNpWrHwProgram (&FsHwNp) == FNP_FAILURE)
        {
        }
        MEMCPY (u1InterfaceName, FsHwNp.RadioIfNpModInfo.unOpCode.
                sConfigDot11StaAuthDeAuth.RadInfo.au1WlanIfname[0],
                STRLEN (FsHwNp.RadioIfNpModInfo.unOpCode.
                        sConfigDot11StaAuthDeAuth.RadInfo.au1WlanIfname[0]));
#endif
#ifdef RFMGMT_WANTED
        RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtStaProcessReq.u1RadioId
            = pStaConfigReqinfo->DelSta.u1RadioId;
        MEMCPY (RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtStaProcessReq.ClientMac,
                wssStaStateDB.stationMacAddress, sizeof (tMacAddr));

        if (WssIfProcessRfMgmtMsg (RFMGMT_DELETE_CLIENT_STATS,
                                   &RfMgmtMsgStruct) != OSIX_SUCCESS)
        {
            WSSSTA_TRC (WSSSTA_INFO_TRC,
                        "WssStaProcessReAssocFrames: "
                        "RF module processing failed\r\n");
        }
#endif

        if (pWssStaDB != NULL)
        {
            if ((pWssStaDB->u4BandWidth != 0) && (pWssStaDB->u4DLBandWidth != 0)
                && (pWssStaDB->u4ULBandWidth != 0))
#ifdef NPAPI_WANTED
            {
                u1DeleteFlag = OSIX_TRUE;
                WssWlanGetWlanIntfName (pWssStaDB->u1RadioId,
                                        pWssStaDB->u1WlanId, u1InterfaceName,
                                        &u1WlanId);
                WSSUserRoleConfigBandwidth (pWssStaDB->stationMacAddress,
                                            pWssStaDB->u4BandWidth,
                                            pWssStaDB->u4DLBandWidth,
                                            pWssStaDB->u4ULBandWidth,
                                            u1InterfaceName, u1DeleteFlag);

            }
            /* Delete TC fitlers added with bandwidth thresh config when station
             * is deleted. */
            if (pWssStaDB->u4BandwidthThresh != 0)
            {
                u1DeleteFlag = OSIX_TRUE;
                WssWlanGetWlanIntfName (pWssStaDB->u1RadioId,
                                        pWssStaDB->u1WlanId, u1InterfaceName,
                                        &u1WlanId);
                WSSUserRoleConfigBandwidth (pWssStaDB->stationMacAddress,
                                            pWssStaDB->u4BandwidthThresh,
                                            pWssStaDB->u4BandwidthThresh,
                                            pWssStaDB->u4BandwidthThresh,
                                            u1InterfaceName, u1DeleteFlag);
                WSSSTA_TRC1 (WSSSTA_FAILURE_TRC,
                             "\n\nDELETE STATION :: WSSUserRoleConfigBandwidth  :: Bandwidth : %d ",
                             pWssStaDB->u4BandwidthThresh);
                WSSSTA_TRC6 (WSSSTA_FAILURE_TRC,
                             "Station MAC : %02x:%02x:%02x:%02x:%02x:%02x",
                             pWssStaDB->stationMacAddress[0],
                             pWssStaDB->stationMacAddress[1],
                             pWssStaDB->stationMacAddress[2],
                             pWssStaDB->stationMacAddress[3],
                             pWssStaDB->stationMacAddress[4],
                             pWssStaDB->stationMacAddress[5]);

            }
#endif
            RBTreeRem (gWssStaStateDB, pWssStaDB);
            MemReleaseMemBlock (WSSSTA_AP_STADB_POOLID, (UINT1 *) pWssStaDB);
#ifdef WTP_WANTED
#ifdef KERNEL_CAPWAP_WANTED
            if (WssStaRemoveKernelDB (pStaConfigReqinfo->DelSta.StaMacAddr) ==
                OSIX_FAILURE)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "Failed to remove entry in Kernel Module\r\n");
            }
#endif
#endif
        }
        else
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaUpdateDB: STA Entry "
                        "NOT present in DB\r\n");
        }
    }

    /* Station element is present */
    if (pStaConfigReqinfo->StaMsg.isPresent)
    {
        /* Get the RB Tree node from the tree */
        MEMCPY (wssStaStateDB.stationMacAddress,
                (pStaConfigReqinfo->StaMsg.stationMacAddress),
                sizeof (tMacAddr));
        pWssStaDB =
            ((tWssStaStateDB *)
             RBTreeGet (gWssStaStateDB, (tRBElem *) & wssStaStateDB));
        if (pWssStaDB == NULL)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaUpdateDB - StaMsg: "
                        "The entry for the Station MAC is not "
                        "available in the Station DB");
            return OSIX_FAILURE;
        }

        pWssStaDB->u2AssociationID = pStaConfigReqinfo->StaMsg.u2AssocId;
        pWssStaDB->u1WlanId = pStaConfigReqinfo->StaMsg.u1WlanId;
        pWssStaDB->StaParams.u2CapabilityInfo =
            pStaConfigReqinfo->StaMsg.u2Capab;
        pWssStaDB->u2SessId = pStaConfigReqinfo->u2SessId;
        pWssStaDB->u1RadioId = pStaConfigReqinfo->StaMsg.u1RadioId;
        pWssWlanMsgStruct =
            (tWssWlanMsgStruct *) (VOID *) UtlShMemAllocWssWlanBuf ();
        if (pWssWlanMsgStruct == NULL)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                        "WssStaUpdateDB:- "
                        "UtlShMemAllocWssWlanBuf returned failure\n");
            return OSIX_FAILURE;
        }

        /* Construct WLAN Struct and send for VLAN ID Mapping */
        MEMSET (pWssWlanMsgStruct, 0, sizeof (tWssWlanMsgStruct));
        pWssWlanMsgStruct->unWssWlanMsg.WssStaMsgStruct.
            unAuthMsg.WssStaConfigReqInfo.StaMsg.u1RadioId =
            pWssStaDB->u1RadioId;
        pWssWlanMsgStruct->unWssWlanMsg.WssStaMsgStruct.
            unAuthMsg.WssStaConfigReqInfo.StaMsg.u1WlanId = pWssStaDB->u1WlanId;
        pWssWlanMsgStruct->unWssWlanMsg.WssStaMsgStruct.
            unAuthMsg.WssStaConfigReqInfo.AddSta.u2VlanId = pWssStaDB->u2VlanId;

        if (WssIfProcessWssWlanMsg (WSS_WLAN_SET_VLAN_MAPPING,
                                    pWssWlanMsgStruct) != OSIX_SUCCESS)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                        "WSS_WLAN_SET_VLAN_MAPPING FAILED\r\n");
        }
        UtlShMemFreeWssWlanBuf ((UINT1 *) pWssWlanMsgStruct);
#ifdef KERNEL_CAPWAP_WANTED
#ifdef WTP_WANTED
        if (WssStaUpdateKernelDB
            (pStaConfigReqinfo->StaMsg.u1RadioId,
             pStaConfigReqinfo->StaMsg.u1WlanId,
             pWssStaDB->stationMacAddress) == OSIX_FAILURE)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "Failed to update kernel DB\r\n");
        }
#endif
#endif

#ifdef NPAPI_WANTED
/*FIX_VLAN_MSSID*/
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.RadInfo.
            u4RadioIfIndex = pStaConfigReqinfo->StaMsg.u1RadioId +
            SYS_DEF_MAX_ENET_INTERFACES;
        SPRINTF ((CHR1 *) FsHwNp.RadioIfNpModInfo.unOpCode.
                 sConfigDot11StaAuthDeAuth.RadInfo.au1RadioIfName, "%s%d",
                 RADIO_INTF_NAME, pStaConfigReqinfo->StaMsg.u1RadioId - 1);
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.RadInfo.
            au2WlanIfIndex[0] = pStaConfigReqinfo->StaMsg.u1WlanId;

        WssWlanGetWlanIntfName (pStaConfigReqinfo->StaMsg.u1RadioId,
                                pStaConfigReqinfo->StaMsg.u1WlanId,
                                FsHwNp.RadioIfNpModInfo.unOpCode.
                                sConfigDot11StaAuthDeAuth.RadInfo.
                                au1WlanIfname[0],
                                &FsHwNp.RadioIfNpModInfo.unOpCode.
                                sConfigDot11StaAuthDeAuth.RadInfo.u1WlanId);

        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.u1AuthStat =
            WLAN_CLIENT_ADD;
        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.
                au1StaAuthMac,
                pStaConfigReqinfo->StaMsg.stationMacAddress, sizeof (tMacAddr));
        FsHwNp.u4Opcode = FS_RADIO_HW_STA_AUTH_DEAUTH;    /* 88 */
        MEMCPY (u1InterfaceName, FsHwNp.RadioIfNpModInfo.unOpCode.
                sConfigDot11StaAuthDeAuth.RadInfo.au1WlanIfname[0],
                STRLEN (FsHwNp.RadioIfNpModInfo.unOpCode.
                        sConfigDot11StaAuthDeAuth.RadInfo.au1WlanIfname[0]));
        /* Invoke Station module to create vlan DB */
#ifdef KERNEL_CAPWAP_WANTED
        WssStaVlanDB.u2VlanId = pWssStaDB->u2VlanId;

        if (WssStaUpdateVlanDB (&WssStaVlanDB, WSSSTA_VLAN_GET) == OSIX_SUCCESS)
        {
            if (WssStaVlanDB.AddStaFlag == OSIX_FALSE)
            {
                WssStaVlanDB.AddStaFlag = OSIX_TRUE;
                WssStaUpdateVlanDB (&WssStaVlanDB, WSSSTA_VLAN_SET);
            }
            SPRINTF ((CHR1 *) FsHwNp.RadioIfNpModInfo.unOpCode.
                     sConfigDot11StaAuthDeAuth.au1IntfName, "%s%d",
                     RADIO_INTF_NAME, pStaConfigReqinfo->StaMsg.u1RadioId - 1);
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.
                u2VlanId = pWssStaDB->u2VlanId;
            if (RadioNpWrHwProgram (&FsHwNp) == FNP_FAILURE)
            {
            }
        }
#endif
#endif
    }

    /* 802.11n params is present */
    if (pStaConfigReqinfo->VendSpec.Dot11nStaVendor.isOptional)
    {
        /* Get the RB Tree node from the tree */

        MEMCPY (wssStaStateDB.stationMacAddress,
                (pStaConfigReqinfo->VendSpec.Dot11nStaVendor.stationMacAddr),
                sizeof (tMacAddr));
        pWssStaDB =
            ((tWssStaStateDB *)
             RBTreeGet (gWssStaStateDB, (tRBElem *) & wssStaStateDB));

        if (pWssStaDB == NULL)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaUpdateDB - Dot11nStaVendor: "
                        "The entry for the Station MAC is not "
                        "available in the Station DB");
            return OSIX_FAILURE;
        }
        pWssStaDB->u1HtFlag = pStaConfigReqinfo->VendSpec.
            Dot11nStaVendor.u1HtFlag;
        MEMCPY (pWssStaDB->au1MCSRateSet, pStaConfigReqinfo->VendSpec.
                Dot11nStaVendor.au1MCSRateSet,
                sizeof (pWssStaDB->au1MCSRateSet));
#ifdef NPAPI_WANTED
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.RadInfo.
            u4RadioIfIndex = pWssStaDB->u1RadioId + SYS_DEF_MAX_ENET_INTERFACES;
        SPRINTF ((CHR1 *) FsHwNp.RadioIfNpModInfo.unOpCode.
                 sConfigDot11StaAuthDeAuth.RadInfo.au1RadioIfName, "%s%d",
                 RADIO_INTF_NAME, pWssStaDB->u1RadioId - 1);
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.RadInfo.
            au2WlanIfIndex[0] = pStaConfigReqinfo->StaMsg.u1WlanId;
        u1TempWlanId = pStaConfigReqinfo->StaMsg.u1WlanId - 1;
        sprintf ((char *) au1WlanName, "%s%d", RADIO_ATH_INTF_NAME,
                 u1TempWlanId);
        STRCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.
                RadInfo.au1WlanIfname[0], au1WlanName);

        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.u1AuthStat =
            1;
        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.
                au1StaAuthMac,
                pStaConfigReqinfo->AddSta.StaMacAddress, sizeof (tMacAddr));
        FsHwNp.u4Opcode = FS_RADIO_HW_STA_DOT11N_MCS_RATESET;    /* 103 */
        if (RadioNpWrHwProgram (&FsHwNp) == FNP_FAILURE)
        {
        }
#endif

    }
    /* Station session Key is present */
    if (pStaConfigReqinfo->StaSessKey.isPresent)
    {
        /* Get the RB Tree node from the tree */
        MEMCPY (wssStaStateDB.stationMacAddress,
                (pStaConfigReqinfo->StaSessKey.stationMacAddress),
                sizeof (tMacAddr));
        pWssStaDB = ((tWssStaStateDB *) RBTreeGet (gWssStaStateDB,
                                                   (tRBElem *) &
                                                   wssStaStateDB));
        if (pWssStaDB == NULL)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaUpdateDB - StaSessKey: "
                        "The entry for the Station MAC is not available "
                        "in the Station DB");
            return OSIX_FAILURE;
        }

        /* Update the values */
        pWssStaDB->StaParams.u2ACFlag = pStaConfigReqinfo->StaSessKey.u2Flags;
        MEMCPY (pWssStaDB->StaParams.au1PairwiseTSC,
                pStaConfigReqinfo->StaSessKey.au1PairwiseTSC, 6);
        MEMCPY (pWssStaDB->StaParams.au1PairwiseRSC,
                pStaConfigReqinfo->StaSessKey.au1PairwiseRSC, 6);
        /* Copy the key as well */
        if ((pStaConfigReqinfo->StaSessKey.u2MessageLength ==
             (20 + CAPWAP_WEP_40_KEY_LEN))
            || (pStaConfigReqinfo->StaSessKey.u2MessageLength ==
                (20 + CAPWAP_WEP_104_KEY_LEN)))
        {
            MEMCPY (pWssStaDB->StaParams.au1Key,
                    pStaConfigReqinfo->StaSessKey.au1Key,
                    ((pStaConfigReqinfo->StaSessKey.u2MessageLength) - 20));
        }
        if (pWssStaDB->StaParams.u2ACFlag == 0)
        {
            if ((pStaConfigReqinfo->StaSessKey.u2MessageLength ==
                 (CAPWAP_RSN_IE_LEN + CAPWAP_RSN_IE_LEN +
                  CAPWAP_RSNA_CCMP_KEY_LEN))
                || (pStaConfigReqinfo->StaSessKey.u2MessageLength ==
                    (CAPWAP_RSN_IE_LEN + CAPWAP_RSN_IE_LEN +
                     CAPWAP_RSNA_CCMP_KEY_LEN + PMF_TWO_BYTES)))
            {
                MEMCPY (pWssStaDB->StaParams.au1Key,
                        pStaConfigReqinfo->StaSessKey.au1Key,
                        CAPWAP_RSNA_CCMP_KEY_LEN);
            }
            else if ((pStaConfigReqinfo->StaSessKey.u2MessageLength ==
                      (CAPWAP_RSN_IE_LEN + CAPWAP_RSN_IE_LEN +
                       CAPWAP_RSNA_TKIP_KEY_LEN))
                     || (pStaConfigReqinfo->StaSessKey.u2MessageLength ==
                         (CAPWAP_RSN_IE_LEN + CAPWAP_RSN_IE_LEN +
                          CAPWAP_RSNA_TKIP_KEY_LEN + PMF_TWO_BYTES)))
            {
                MEMCPY (pWssStaDB->StaParams.au1Key,
                        pStaConfigReqinfo->StaSessKey.au1Key,
                        CAPWAP_RSNA_TKIP_KEY_LEN);
            }

        }
        else
        {
            if ((pStaConfigReqinfo->StaSessKey.u2MessageLength ==
                 (CAPWAP_RSN_IE_LEN + CAPWAP_RSN_IE_LEN + PMF_TWO_BYTES +
                  CAPWAP_RSNA_CCMP_KEY_LEN)))
            {
                MEMCPY (pWssStaDB->StaParams.au1Key,
                        pStaConfigReqinfo->StaSessKey.au1Key,
                        CAPWAP_RSNA_CCMP_KEY_LEN);
            }
            else if ((pStaConfigReqinfo->StaSessKey.u2MessageLength ==
                      (CAPWAP_RSN_IE_LEN + CAPWAP_RSN_IE_LEN + PMF_TWO_BYTES +
                       CAPWAP_RSNA_TKIP_KEY_LEN)))
            {
                MEMCPY (pWssStaDB->StaParams.au1Key,
                        pStaConfigReqinfo->StaSessKey.au1Key,
                        CAPWAP_RSNA_TKIP_KEY_LEN);
            }
        }
        /* Copy RSNA Parameters */

        if (pStaConfigReqinfo->StaSessKey.RsnaIEElements.u1ElemId ==
            WSSMAC_RSN_ELEMENT_ID ||
            pStaConfigReqinfo->StaSessKey.RsnaIEElements.u1ElemId ==
            WSSMAC_WPA_ELEMENT_ID)
        {
            pWssStaDB->RsnaIEElements.u1ElemId =
                pStaConfigReqinfo->StaSessKey.RsnaIEElements.u1ElemId;
            pWssStaDB->RsnaIEElements.u1Len =
                pStaConfigReqinfo->StaSessKey.RsnaIEElements.u1Len;
            pWssStaDB->RsnaIEElements.u2Ver =
                pStaConfigReqinfo->StaSessKey.RsnaIEElements.u2Ver;
            pWssStaDB->RsnaIEElements.u2PairwiseCipherSuiteCount =
                pStaConfigReqinfo->StaSessKey.RsnaIEElements.
                u2PairwiseCipherSuiteCount;

            MEMCPY (pWssStaDB->RsnaIEElements.au1GroupCipherSuite,
                    pStaConfigReqinfo->StaSessKey.RsnaIEElements.
                    au1GroupCipherSuite, RSNA_CIPHER_SUITE_LEN);

            for (u1Count = 0;
                 u1Count < pWssStaDB->RsnaIEElements.u2PairwiseCipherSuiteCount;
                 u1Count++)
            {
                MEMCPY (pWssStaDB->RsnaIEElements.aRsnaPwCipherDB[u1Count].
                        au1PairwiseCipher,
                        pStaConfigReqinfo->StaSessKey.RsnaIEElements.
                        aRsnaPwCipherDB[u1Count].au1PairwiseCipher,
                        RSNA_CIPHER_SUITE_LEN);
            }
            pWssStaDB->RsnaIEElements.u2AKMSuiteCount =
                pStaConfigReqinfo->StaSessKey.RsnaIEElements.u2AKMSuiteCount;
            for (u1Count = 0;
                 u1Count < pWssStaDB->RsnaIEElements.u2AKMSuiteCount; u1Count++)
            {
                MEMCPY (pWssStaDB->RsnaIEElements.aRsnaAkmDB[u1Count].
                        au1AKMSuite,
                        pStaConfigReqinfo->StaSessKey.RsnaIEElements.
                        aRsnaAkmDB[u1Count].au1AKMSuite, RSNA_AKM_SELECTOR_LEN);
            }
            if (pStaConfigReqinfo->StaSessKey.RsnaIEElements.u1ElemId ==
                WSSMAC_RSN_ELEMENT_ID)
            {
                pWssStaDB->RsnaIEElements.u2PmkidCount =
                    pStaConfigReqinfo->StaSessKey.RsnaIEElements.u2PmkidCount;
                pWssStaDB->RsnaIEElements.u2RsnCapab =
                    pStaConfigReqinfo->StaSessKey.RsnaIEElements.u2RsnCapab;
                MEMCPY (pWssStaDB->RsnaIEElements.aPmkidList,
                        pStaConfigReqinfo->StaSessKey.RsnaIEElements.aPmkidList,
                        RSNA_AKM_SELECTOR_LEN);
            }
            /* TBD for Update Qos and Update Qos for next release */
            if ((WssStaSetRsnaPairwiseParams (pWssStaDB)) != OSIX_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaUpdateDB -"
                            "WssWlanSetRsnaPairwiseParams returned failure");
                return OSIX_FAILURE;
            }
        }
    }

    /* When WEB Authentication is completed for the station, the status will be
     * indicated from WLC */
    if (pStaConfigReqinfo->WebAuthStatus.isPresent)
    {
        /* Get the RB Tree node from the tree */
        MEMCPY (wssStaStateDB.stationMacAddress,
                (pStaConfigReqinfo->WebAuthStatus.stationMacAddress),
                sizeof (tMacAddr));
        pWssStaDB = ((tWssStaStateDB *) RBTreeGet (gWssStaStateDB,
                                                   (tRBElem *) &
                                                   wssStaStateDB));
        if (pWssStaDB == NULL)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaUpdateDB - WebAuthStatus: "
                        "The entry for the Station MAC is not available "
                        "in the Station DB");
            return OSIX_FAILURE;
        }
        pWssStaDB->u1WebAuthCompStatus =
            pStaConfigReqinfo->WebAuthStatus.u1WebAuthCompStatus;

#ifdef KERNEL_CAPWAP_WANTED
        if (WssStaUpdateKernelDB (pWssStaDB->u1RadioId,
                                  pWssStaDB->u1WlanId,
                                  pStaConfigReqinfo->WebAuthStatus.
                                  stationMacAddress) == OSIX_FAILURE)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaUpdateDB - WebAuthStatus: "
                        "Kernel DB updation Failed");
        }
#endif

    }
    /* TBD for Update Qos and Update Qos for next release */

    UNUSED_PARAM (u1WlanId);
    UNUSED_PARAM (u1DeleteFlag);
    UNUSED_PARAM (u1InterfaceName);
    return u4Retval;
}

/*****************************************************************************
 *  Function Name   : WssStaGetStationDB                                     *
 *                                                                           *
 *  Description     : This function is used to get the Data from DB          *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT4
WssStaGetStationDB (tWssStaStateDB * pWssStaStateDB)
{

    tWssStaStateDB     *pWssStaDB = NULL;
    tWssStaStateDB      WssStaDB;

    MEMSET (&WssStaDB, 0, sizeof (tWssStaStateDB));
    MEMCPY (WssStaDB.stationMacAddress, pWssStaStateDB->stationMacAddress,
            sizeof (tMacAddr));
    pWssStaDB = ((tWssStaStateDB *) RBTreeGet (gWssStaStateDB,
                                               (tRBElem *) & WssStaDB));
    if (pWssStaDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaGetStationDB: "
                    "The entry for the Station MAC is not available in "
                    "the Station DB");
        return OSIX_FAILURE;
    }
    pWssStaStateDB->u1RadioId = pWssStaDB->u1RadioId;
    pWssStaStateDB->u1WlanId = pWssStaDB->u1WlanId;
    pWssStaStateDB->u2AssociationID = pWssStaDB->u2AssociationID;
    pWssStaStateDB->u2SessId = pWssStaDB->u2SessId;
    pWssStaStateDB->staActiveFlag = pWssStaDB->staActiveFlag;
    pWssStaStateDB->u1WmmFlag = pWssStaDB->u1WmmFlag;
    pWssStaStateDB->u2TcpPort = pWssStaDB->u2TcpPort;
    pWssStaStateDB->StaParams.u2CapabilityInfo =
        pWssStaDB->StaParams.u2CapabilityInfo;
    MEMCPY (pWssStaStateDB->StaParams.au1VlanName,
            pWssStaDB->StaParams.au1VlanName,
            sizeof (WssStaDB.StaParams.au1VlanName));
    MEMCPY (pWssStaStateDB->StaParams.au1SuppRates,
            pWssStaDB->StaParams.au1SuppRates,
            sizeof (WssStaDB.StaParams.au1SuppRates));
    pWssStaStateDB->u1WebAuthCompStatus = pWssStaDB->u1WebAuthCompStatus;
    /* TBD For rest of elements */

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaSetStaActiveFlag                                 *
 *                                                                           *
 *  Description     : This function is invoked when the WSS MAC module       *
 *                    intimates the WSSTA module whenever a data packet is   *
 *                    received for a station. The StationActiveflag is set   *
 *                    by this API to denote that the station is active.      *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/

UINT4
WssStaSetStaActiveFlag (tWssStaActiveStatus StaStatusInfo)
{
    UINT4               u4Retval = OSIX_SUCCESS;
    tWssStaStateDB     *pWssStaDB = NULL;
    tWssStaStateDB      wssStaStateDB;
    MEMSET (&wssStaStateDB, 0, sizeof (tWssStaStateDB));

    /* Get the RB Tree node from the tree */
    MEMCPY (wssStaStateDB.stationMacAddress, StaStatusInfo.StaMacAddr,
            sizeof (tMacAddr));
    pWssStaDB = ((tWssStaStateDB *) RBTreeGet (gWssStaStateDB,
                                               (tRBElem *) & wssStaStateDB));
    if (pWssStaDB == NULL)
    {
        return OSIX_FAILURE;
    }
    /* Set the Station Active Flag in the DB */
    pWssStaDB->staActiveFlag = WSSSTA_ACTIVE_FLAG_SET;
    return u4Retval;
}

/*****************************************************************************
 *  Function Name   : WssStaApCompareStaDBRBTree                             *
 *                                                                           *
 *  Description     : This function is used to add a new entry to RB tree    *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssStaApCompareStaDBRBTree (tRBElem * e1, tRBElem * e2)
{

    tWssStaStateDB     *pNode1 = e1;
    tWssStaStateDB     *pNode2 = e2;
    INT4                i4CmpVal = 0;

    i4CmpVal = MEMCMP (&pNode1->stationMacAddress,
                       &pNode2->stationMacAddress, sizeof (tMacAddr));

    if (i4CmpVal > 0)
    {
        return WSSSTA_RB_GREATER;
    }
    else if (i4CmpVal < 0)
    {
        return WSSSTA_RB_LESS;
    }
    return WSSSTA_RB_EQUAL;
}

/*****************************************************************************
 *  Function Name   : WssStaCompareTCFilterHandleRBTree                      *
 *                                                                           *
 *  Description     : This function is used to add TC Filter Handle entry to *
 *                    RB tree                                                *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssStaCompareTCFilterHandleRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssStaTCFilterHandle *pNode1 = (tWssStaTCFilterHandle *) e1;
    tWssStaTCFilterHandle *pNode2 = (tWssStaTCFilterHandle *) e2;
    INT4                i4CmpVal = 0;

    i4CmpVal = MEMCMP (&pNode1->StationMacAddr,
                       &pNode2->StationMacAddr, sizeof (tMacAddress));

    if (i4CmpVal > 0)
    {
        return WSSSTA_RB_GREATER;
    }
    else if (i4CmpVal < 0)
    {
        return WSSSTA_RB_LESS;
    }
    return WSSSTA_RB_EQUAL;
}

#ifdef BAND_SELECT_WANTED
/*****************************************************************************
 *  Function Name   : WssStaCompareWtpBandSteerDBRBTree                      *
 *                                                                           *
 *  Description     : This function is used to add a new entry to RB tree    *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssStaCompareWtpBandSteerDBRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssStaBandSteerDB *pNode1 = (tWssStaBandSteerDB *) e1;
    tWssStaBandSteerDB *pNode2 = (tWssStaBandSteerDB *) e2;
    INT4                i4CmpVal = 0;

    i4CmpVal = MEMCMP (pNode1->stationMacAddress,
                       pNode2->stationMacAddress, sizeof (tMacAddress));
    if (i4CmpVal > 0)
    {
        return WSSSTA_RB_GREATER;
    }
    else if (i4CmpVal < 0)
    {
        return WSSSTA_RB_LESS;
    }
    i4CmpVal = MEMCMP (pNode1->au1Ssid, pNode2->au1Ssid, sizeof (tMacAddress));
    if (i4CmpVal > 0)
    {
        return WSSSTA_RB_GREATER;
    }
    else if (i4CmpVal < 0)
    {
        return WSSSTA_RB_LESS;
    }
    return WSSSTA_RB_EQUAL;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssStaUpdateAPBandSteerProcessDB                           *
 *                                                                           *
 * Description  : This function process BandSteer RBTree operations          *
 *                                                                           *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
UINT4
WssStaUpdateAPBandSteerProcessDB (eProcessDB action,
                                  tWssStaBandSteerDB * pWssStaBandSteerDB)
{
    tWssStaBandSteerDB *pBandSteerDB = NULL;
    switch (action)
    {
        case WSSSTA_CREATE_BAND_STEER_DB:
            /*Memory Allocation for the node to be added in the tree */
            if ((pBandSteerDB =
                 (tWssStaBandSteerDB *) MemAllocMemBlk
                 (WSSSTA_AP_BANDSTEERDB_POOLID)) == NULL)
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "WssStaUpdateBandSteerProcessDB Entry is not present in the BandSteer Process DB \r\n");
                return OSIX_FAILURE;

            }
            MEMSET (pBandSteerDB, 0, sizeof (tWssStaBandSteerDB));
            MEMCPY (pBandSteerDB->stationMacAddress,
                    pWssStaBandSteerDB->stationMacAddress, MAC_ADDR_LEN);
            MEMCPY (pBandSteerDB->BssIdMacAddr,
                    pWssStaBandSteerDB->BssIdMacAddr, MAC_ADDR_LEN);
            MEMCPY (pBandSteerDB->au1Ssid,
                    pWssStaBandSteerDB->au1Ssid, pWssStaBandSteerDB->u1SsidLen);
            pBandSteerDB->u1StaStatus = pWssStaBandSteerDB->u1StaStatus;
            pBandSteerDB->u1SsidLen = pWssStaBandSteerDB->u1SsidLen;
            pBandSteerDB->u1WlanId = pWssStaBandSteerDB->u1WlanId;
            pBandSteerDB->u1RadioId = pWssStaBandSteerDB->u1RadioId;

            /* Add the node to the RBTree */
            if (RBTreeAdd
                (gWssStaAPBandSteerDB, (tRBElem *) pBandSteerDB) != RB_SUCCESS)
            {
                MemReleaseMemBlock (WSSSTA_AP_BANDSTEERDB_POOLID,
                                    (UINT1 *) pBandSteerDB);
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "RBTreeAdd failed in the BandSteer Process DB \r\n");
                return OSIX_FAILURE;
            }
            break;
        case WSSSTA_GET_BAND_STEER_DB:

            pBandSteerDB =
                RBTreeGet (gWssStaAPBandSteerDB,
                           (tRBElem *) pWssStaBandSteerDB);
            if (pBandSteerDB == NULL)
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "RBTreeGet failed in the BandSteer Process DB \r\n");
                return OSIX_FAILURE;

            }
            MEMCPY (pWssStaBandSteerDB->stationMacAddress,
                    pBandSteerDB->stationMacAddress, MAC_ADDR_LEN);
            MEMCPY (pWssStaBandSteerDB->au1Ssid, pBandSteerDB->au1Ssid,
                    pWssStaBandSteerDB->u1SsidLen);
            pWssStaBandSteerDB->u1StaStatus = pBandSteerDB->u1StaStatus;

            break;
        case WSSSTA_SET_BAND_STEER_DB:
            pBandSteerDB =
                RBTreeGet (gWssStaAPBandSteerDB,
                           (tRBElem *) pWssStaBandSteerDB);
            if (pBandSteerDB == NULL)
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "RBTreeGet failed in the BandSteer Process DB \r\n");
                return OSIX_FAILURE;

            }
            MEMCPY (pBandSteerDB->stationMacAddress,
                    pWssStaBandSteerDB->stationMacAddress, MAC_ADDR_LEN);
            MEMCPY (pBandSteerDB->au1Ssid, pWssStaBandSteerDB->au1Ssid,
                    pWssStaBandSteerDB->u1SsidLen);
            pBandSteerDB->u1StaStatus = pWssStaBandSteerDB->u1StaStatus;

            break;
        case WSSSTA_DESTROY_BAND_STEER_DB:
            pBandSteerDB =
                RBTreeGet (gWssStaAPBandSteerDB,
                           (tRBElem *) pWssStaBandSteerDB);
            if (pBandSteerDB == NULL)
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "RBTreeGet failed in the BandSteer Process DB \r\n");
                return OSIX_FAILURE;
            }
            RBTreeRem (gWssStaAPBandSteerDB, (tRBElem *) pBandSteerDB);
            MemReleaseMemBlock (WSSSTA_AP_BANDSTEERDB_POOLID,
                                (UINT1 *) pBandSteerDB);
            pBandSteerDB = NULL;

            break;
        case WSSSTA_GET_FIRST_BAND_STEER_DB:

            pBandSteerDB = RBTreeGetFirst (gWssStaAPBandSteerDB);

            if (pBandSteerDB == NULL)
            {
                /* If not present, return error */
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "RBTreeGetFirst failed in the BandSteer Process DB \r\n");
                return OSIX_FAILURE;
            }
            /* Copy the values from the DB strcuture to output structure */
            MEMCPY (pWssStaBandSteerDB->stationMacAddress,
                    pBandSteerDB->stationMacAddress, MAC_ADDR_LEN);
            MEMCPY (pWssStaBandSteerDB->au1Ssid, pBandSteerDB->au1Ssid,
                    pWssStaBandSteerDB->u1SsidLen);
            pWssStaBandSteerDB->u1StaStatus = pBandSteerDB->u1StaStatus;

            break;
        case WSSSTA_GET_NEXT_BAND_STEER_DB:
            /* Pass the received input and check entry is already present */
            if (MEMCMP (pWssStaBandSteerDB->stationMacAddress, "\0\0\0\0\0\0",
                        MAC_ADDR_LEN) == 0)
            {
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "WssStaUpdateAPBandSteerProcessDB: "
                            "RBTreeGetNext failed in the BandSteer Process DB \r\n");
                return OSIX_FAILURE;
            }
            pBandSteerDB =
                RBTreeGetNext (gWssStaAPBandSteerDB,
                               (tRBElem *) pWssStaBandSteerDB, NULL);
            if (pBandSteerDB == NULL)
            {
                /* If not present, return error */
                WSSSTA_TRC (WSSSTA_MGMT_TRC,
                            "RBTreeGetNext entry not found in the BandSteer Process DB \r\n");
                return OSIX_FAILURE;
            }
            /* Copy the values from the DB strcuture to output structure */
            MEMCPY (pWssStaBandSteerDB->stationMacAddress,
                    pBandSteerDB->stationMacAddress, MAC_ADDR_LEN);
            MEMCPY (pWssStaBandSteerDB->au1Ssid, pBandSteerDB->au1Ssid,
                    pBandSteerDB->u1SsidLen);
            pWssStaBandSteerDB->u1StaStatus = pBandSteerDB->u1StaStatus;
            break;

        default:
            WSSSTA_TRC (WSSSTA_MGMT_TRC,
                        "RBTree Invalid operation in  BandSteer Process DB \r\n");
            return OSIX_FAILURE;
            break;
    }
    return OSIX_SUCCESS;
}
#endif

#ifdef KERNEL_CAPWAP_WANTED
/*****************************************************************************
 *  Function Name   : WssStaApCompareVlanDBRBTree                            *
 *                                                                           *
 *  Description     : This function is used to add a new entry to RB tree    *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssStaApCompareVlanDBRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssStaVlanDB      *pNode1 = e1;
    tWssStaVlanDB      *pNode2 = e2;

    if (pNode1->u2VlanId > pNode2->u2VlanId)
    {
        return WSSSTA_RB_GREATER;
    }
    else if (pNode1->u2VlanId < pNode2->u2VlanId)
    {
        return WSSSTA_RB_LESS;
    }
    return WSSSTA_RB_EQUAL;
}
#endif

/*****************************************************************************
 *  Function Name   : StaApDBWalkFnForDeauth                                 *
 *                                                                           *
 *  Description     : RBTree Walk function for Station DB at WTP             *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
StaDBWalkFnForDeauth (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                      void *pArg, void *pOut)
{
    tWssStaStateDB     *pWssStaDB = NULL;
    tWssStaDeauthMsg   *pStaDeauthMsg = NULL;

    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (pArg);
    UNUSED_PARAM (pOut);

    pStaDeauthMsg = (tWssStaDeauthMsg *) pArg;

    if (visit == postorder || visit == leaf)
    {
        if (pRBElem != NULL)
        {
            pWssStaDB = (tWssStaStateDB *) pRBElem;

            if ((pWssStaDB->u1RadioId == pStaDeauthMsg->u1RadioId) &&
                (pWssStaDB->u1WlanId == pStaDeauthMsg->u1WlanId))
            {
                if (gu1DBWalkCnt < MAX_STA_SUPP_PER_AP)
                {
                    MEMCPY (gaWssStaWtpDBWalk[gu1DBWalkCnt].staMacAddr,
                            pWssStaDB->stationMacAddress, sizeof (tMacAddr));
                    gu1DBWalkCnt++;
                }
            }
        }
    }
    return RB_WALK_CONT;
}

/*****************************************************************************
 *  Function Name   : WssStaDeauthMsg                                        *
 *                                                                           *
 *  Description     : This function is called by WLAN module when a          *
 *                    WLAN is deleted. For the received BSSIfIndex, the      *  
 *                    STA DB is walked to get the STA MAC addresses          *  
 *                    associated with that BSSIFIndex. Deauth msg is sent to *
 *                    WSS MAC to deauthenticate the associated STAs.         *
 *                                                                           * 
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT4
WssStaDeauthMsg (unWssMsgStructs * pWssStaInfo)
{
    tWssStaDeauthMsg    InStaDeauthMsg;

    tWssStaStateDB     *pWssStaDB = NULL;
    UINT1               u1Index = 0;
    tWssStaStateDB      WssStaDB;
    unApHdlrMsgStruct   ApHdlrMsgStruct;
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
#endif
#ifdef RFMGMT_WANTED
    tRfMgmtMsgStruct    RfMgmtMsgStruct;

    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
#endif

    MEMCPY (&InStaDeauthMsg, &(pWssStaInfo->WssStaDeauthMsg),
            sizeof (tWssStaDeauthMsg));
    gu1DBWalkCnt = 0;
    RBTreeWalk (gWssStaStateDB, (tRBWalkFn) StaDBWalkFnForDeauth,
                &InStaDeauthMsg, 0);

    if (gu1DBWalkCnt)
    {
        for (u1Index = 0; u1Index < gu1DBWalkCnt; u1Index++)
        {
            MEMSET (&WssStaDB, 0, sizeof (tWssStaStateDB));
            MEMCPY (WssStaDB.stationMacAddress,
                    gaWssStaWtpDBWalk[u1Index].staMacAddr, sizeof (tMacAddr));
            pWssStaDB = ((tWssStaStateDB *) RBTreeGet (gWssStaStateDB,
                                                       (tRBElem *) & WssStaDB));
            if (pWssStaDB == NULL)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WssStaDeauthMsg: RBTree Get Failed to "
                            "retrieve data from Station DB \r\n");
                return OSIX_FAILURE;
            }
            else
            {
                MEMCPY (ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.
                        DeauthMacFrame.MacMgmtFrmHdr.u1SA,
                        InStaDeauthMsg.BssId, sizeof (tMacAddr));
                MEMCPY (ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.
                        DeauthMacFrame.MacMgmtFrmHdr.u1BssId,
                        InStaDeauthMsg.BssId, sizeof (tMacAddr));
                MEMCPY (ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.
                        DeauthMacFrame.MacMgmtFrmHdr.u1DA,
                        gaWssStaWtpDBWalk[u1Index].staMacAddr,
                        sizeof (tMacAddr));
                ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.DeauthMacFrame.
                    ReasonCode.u2MacFrameReasonCode = WSSSTA_DEAUTH_MS_LEAVING;
                ApHdlrMsgStruct.WssMacMsgStruct.msgType = 0x06;

                if (WssIfProcessApHdlrMsg (WSS_APHDLR_MAC_DEAUTH_MSG,
                                           &ApHdlrMsgStruct) != OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WssStaDeauthMsg: WSS_APHDLR_MAC_DEAUTH_MSG "
                                "Failed \r\n");
                    return OSIX_FAILURE;
                }

#ifdef KERNEL_CAPWAP_WANTED
                if (WssStaRemoveKernelDB
                    (ApHdlrMsgStruct.WssMacMsgStruct.unMacMsg.DeauthMacFrame.
                     MacMgmtFrmHdr.u1DA) == OSIX_FAILURE)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WssStaDeauthMsg: WSS_APHDLR_MAC_DEAUTH_MSG "
                                "Failed \r\n");
                }

#endif
                /* Do the NP update deletion */
#ifdef NPAPI_WANTED
                FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.
                    RadInfo.u4RadioIfIndex =
                    pWssStaInfo->WssStaDeauthMsg.u1RadioId +
                    SYS_DEF_MAX_ENET_INTERFACES;
                SPRINTF ((CHR1 *) FsHwNp.RadioIfNpModInfo.unOpCode.
                         sConfigDot11StaAuthDeAuth.RadInfo.au1RadioIfName,
                         "%s%d", RADIO_INTF_NAME,
                         pWssStaInfo->WssStaDeauthMsg.u1RadioId - 1);
                WssWlanGetWlanIntfName (pWssStaInfo->WssStaDeauthMsg.u1RadioId,
                                        pWssStaInfo->WssStaDeauthMsg.u1WlanId,
                                        FsHwNp.RadioIfNpModInfo.unOpCode.
                                        sConfigDot11StaAuthDeAuth.RadInfo.
                                        au1WlanIfname[0],
                                        &FsHwNp.RadioIfNpModInfo.unOpCode.
                                        sConfigDot11StaAuthDeAuth.RadInfo.
                                        u1WlanId);
                FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.
                    u1AuthStat = WLAN_CLIENT_DELETE;
                MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.
                        sConfigDot11StaAuthDeAuth.au1StaAuthMac,
                        WssStaDB.stationMacAddress, sizeof (tMacAddr));
                FsHwNp.u4Opcode = FS_RADIO_HW_STA_AUTH_DEAUTH;    /* 88 */
                if (RadioNpWrHwProgram (&FsHwNp) == FNP_FAILURE)
                {
                    return OSIX_FAILURE;
                }
#endif
#ifdef RFMGMT_WANTED
                RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtStaProcessReq.u1RadioId
                    = InStaDeauthMsg.u1RadioId;
                MEMCPY (RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtStaProcessReq.
                        ClientMac, gaWssStaWtpDBWalk[u1Index].staMacAddr,
                        sizeof (tMacAddr));

                if (WssIfProcessRfMgmtMsg (RFMGMT_DELETE_CLIENT_STATS,
                                           &RfMgmtMsgStruct) != OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WssStaDeauthMsg: "
                                "RF module processing failed\r\n");
                }
#endif
                /* Remove the tree entry */
                RBTreeRem (gWssStaStateDB, pWssStaDB);
                MemReleaseMemBlock (WSSSTA_AP_STADB_POOLID,
                                    (UINT1 *) pWssStaDB);
            }
        }

    }
    return OSIX_SUCCESS;
}

INT4
WssStaValidateConfigReq ()
{
    return OSIX_SUCCESS;
}

INT4
StaDBWalkFnForGetStation (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                          void *pArg, void *pOut)
{
    tWssStaStateDB     *pWssStaDB = NULL;

    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (pArg);
    UNUSED_PARAM (pOut);

    if (visit == postorder || visit == leaf)
    {
        if (pRBElem != NULL)
        {
            pWssStaDB = (tWssStaStateDB *) pRBElem;

            if (gu1DBWalkCnt < MAX_STA_SUPP_PER_AP)
            {
                MEMCPY (gaWssStaWtpDBWalk[gu1DBWalkCnt].staMacAddr,
                        pWssStaDB->stationMacAddress, sizeof (tMacAddr));
                gu1DBWalkCnt++;
            }
        }
    }
    return RB_WALK_CONT;
}

INT4
WssStaWtpShowClient (VOID)
{
    gu1WtpClientWalkIndex = 0;
    gu1DBWalkCnt = 0;
    RBTreeWalk (gWssStaStateDB, (tRBWalkFn) StaDBWalkFnForGetStation, 0, 0);
    gu1WtpClientWalkIndex = gu1DBWalkCnt;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaSetRsnaPairwiseParams *
 *                                                                           *
 *  Description     : This function is used to get the Data from DB          *
 *  Input(s)        : pWssStaDB - pointer to station db                      *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT4
WssStaSetRsnaPairwiseParams (tWssStaStateDB * pWssStaDB)
{
#ifdef NPAPI_WANTED
    tFsHwNp             FsHwNp;
    UINT1               au1WlanName[6];
    UINT1               u1Count = 0;

    UINT1               au1RsnaCipherSuiteTKIP[] = { 0x00, 0x0F, 0xAC, 2 };
    UINT1               au1RsnaCipherSuiteCCMP[] = { 0x00, 0x0F, 0xAC, 4 };
    UINT1               au1RsnaAuthKeyMgmt8021X[] = { 0x00, 0x0F, 0xAC, 1 };
    UINT1               au1RsnaAuthKeyMgmtPskOver8021X[] =
        { 0x00, 0x0F, 0xAC, 2 };

    UINT1               au1WpaCipherSuiteTKIP[] = { 0x00, 0x50, 0xF2, 2 };
    UINT1               au1WpaCipherSuiteCCMP[] = { 0x00, 0x50, 0xF2, 4 };
    UINT1               au1WpaAuthKeyMgmt8021X[] = { 0x00, 0x50, 0xF2, 1 };
    UINT1               au1WpaAuthKeyMgmtPskOver8021X[] =
        { 0x00, 0x50, 0xF2, 2 };
    MEMSET (au1WlanName, 0, 6);
    MEMSET (&FsHwNp, 0, sizeof (tFsHwNp));

    NP_UTIL_FILL_PARAMS (FsHwNp,
                         /*Generic NP structure */
                         NP_RADIO_MODULE,
                         /* Module ID */
                         FS_RADIO_HW_SET_RSNA_PAIRWISE_PARAMS,
                         /* Function/OpCode */
                         0,
                         /* IfIndex value if applicable */
                         0,
                         /* No. of Port Params */
                         0);
    /* No. of PortList Parms */

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaPairwiseParams.RadInfo.
        u4RadioIfIndex = pWssStaDB->u1RadioId + SYS_DEF_MAX_ENET_INTERFACES;

    SPRINTF ((CHR1 *) FsHwNp.RadioIfNpModInfo.unOpCode.
             sConfigSetRsnaPairwiseParams.RadInfo.au1RadioIfName, "%s%d",
             RADIO_INTF_NAME, (pWssStaDB->u1RadioId) - 1);

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigDot11StaAuthDeAuth.
        RadInfo.au2WlanIfIndex[0] = pWssStaDB->u1WlanId;

    WssWlanGetWlanIntfName (pWssStaDB->u1RadioId,
                            pWssStaDB->u1WlanId,
                            FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigDot11StaAuthDeAuth.RadInfo.au1WlanIfname[0],
                            &FsHwNp.RadioIfNpModInfo.unOpCode.
                            sConfigDot11StaAuthDeAuth.RadInfo.u1WlanId);

    /* Key Index will be 1 for PTK */
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaPairwiseParams.
        u1KeyIndex = 0;
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaPairwiseParams.u1ElemId =
        pWssStaDB->RsnaIEElements.u1ElemId;
    /* Set rsna as enable */
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaPairwiseParams.
        u1RsnaStatus = RSNA_ENABLED;

    /* Setting the group cipher */
    if (MEMCMP (pWssStaDB->RsnaIEElements.
                au1GroupCipherSuite,
                au1RsnaCipherSuiteTKIP, RSNA_CIPHER_SUITE_LEN) == 0 ||
        MEMCMP (pWssStaDB->RsnaIEElements.au1GroupCipherSuite,
                au1WpaCipherSuiteTKIP, RSNA_CIPHER_SUITE_LEN) == 0)

    {
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaPairwiseParams.
            u1GroupCipher = 1;
        /*IEEE80211_CIPHER_TKIP */
    }
    else if (MEMCMP (pWssStaDB->RsnaIEElements.
                     au1GroupCipherSuite,
                     au1RsnaCipherSuiteCCMP, RSNA_CIPHER_SUITE_LEN) == 0 ||
             MEMCMP (pWssStaDB->RsnaIEElements.au1GroupCipherSuite,
                     au1WpaCipherSuiteCCMP, RSNA_CIPHER_SUITE_LEN) == 0)
    {
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaPairwiseParams.
            u1GroupCipher = 3;
        /*IEEE80211_CIPHER_AES_CCM */
    }

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaPairwiseParams.
        u2PairwiseCipherSuiteCount =
        pWssStaDB->RsnaIEElements.u2PairwiseCipherSuiteCount;

    if (MEMCMP
        (pWssStaDB->RsnaIEElements.aRsnaPwCipherDB[u1Count].
         au1PairwiseCipher, au1RsnaCipherSuiteTKIP, RSNA_CIPHER_SUITE_LEN) == 0
        || MEMCMP (pWssStaDB->RsnaIEElements.aRsnaPwCipherDB[u1Count].
                   au1PairwiseCipher, au1WpaCipherSuiteTKIP,
                   RSNA_CIPHER_SUITE_LEN) == 0)

    {
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaPairwiseParams.au1PairwiseCipher[u1Count] = 1;    /*IEEE80211_CIPHER_TKIP */

        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaPairwiseParams.
            u2KeyLength = RSNA_TKIP_KEY_LEN;

        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaPairwiseParams.
                au1PtkKey, pWssStaDB->StaParams.au1Key, RSNA_TKIP_KEY_LEN);

    }
    else if (MEMCMP
             (pWssStaDB->RsnaIEElements.aRsnaPwCipherDB[u1Count].
              au1PairwiseCipher, au1RsnaCipherSuiteCCMP,
              RSNA_CIPHER_SUITE_LEN) == 0 ||
             MEMCMP (pWssStaDB->RsnaIEElements.aRsnaPwCipherDB[u1Count].
                     au1PairwiseCipher, au1WpaCipherSuiteCCMP,
                     RSNA_CIPHER_SUITE_LEN) == 0)
    {
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaPairwiseParams.au1PairwiseCipher[u1Count] = 3;    /*IEEE80211_CIPHER_AES_CCM */
        FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaPairwiseParams.
            u2KeyLength = RSNA_CCMP_KEY_LEN;
        MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaPairwiseParams.
                au1PtkKey, pWssStaDB->StaParams.au1Key, RSNA_CCMP_KEY_LEN);

    }

    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaPairwiseParams.
        u2AKMSuiteCount = pWssStaDB->RsnaIEElements.u2AKMSuiteCount;

    for (u1Count = 0;
         u1Count <
         FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaPairwiseParams.
         u2AKMSuiteCount; u1Count++)
    {
        if (MEMCMP (pWssStaDB->RsnaIEElements.aRsnaAkmDB[u1Count].au1AKMSuite,
                    au1RsnaAuthKeyMgmt8021X, RSNA_AKM_SELECTOR_LEN) == 0 ||
            MEMCMP (pWssStaDB->RsnaIEElements.aRsnaAkmDB[u1Count].au1AKMSuite,
                    au1WpaAuthKeyMgmt8021X, RSNA_AKM_SELECTOR_LEN) == 0)

        {
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaPairwiseParams.au1AKMSuite[u1Count] = 1;    /*WPA_ASE_8021X_UNSPEC */
        }
        else if (MEMCMP
                 (pWssStaDB->RsnaIEElements.aRsnaAkmDB[u1Count].au1AKMSuite,
                  au1RsnaAuthKeyMgmtPskOver8021X, RSNA_AKM_SELECTOR_LEN) == 0 ||
                 MEMCMP (pWssStaDB->RsnaIEElements.aRsnaAkmDB[u1Count].
                         au1AKMSuite, au1WpaAuthKeyMgmtPskOver8021X,
                         RSNA_AKM_SELECTOR_LEN) == 0)
        {
            FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaPairwiseParams.au1AKMSuite[u1Count] = 2;    /*WPA_ASE_8021X_PSK */
        }
    }

    /* Set the privacy field as  enable */
    FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaPairwiseParams.u1Privacy = 1;

    /* Copy the station mac address */
    MEMCPY (FsHwNp.RadioIfNpModInfo.unOpCode.sConfigSetRsnaPairwiseParams.
            stationMacAddress, pWssStaDB->stationMacAddress, sizeof (tMacAddr));

    if (NpUtilHwProgram (&FsHwNp) != FNP_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "NpUtilHwProgram : Values to get from Hw failed\r\n");
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pWssStaDB);
#endif
    return OSIX_SUCCESS;

}

/*****************************************************************************
 *  Function Name   : WssStaGetBssid                                         *
 *                                                                           *
 *  Description     : This function is used to get the BSSID from DB         *
 *                                                                           *
 *  Input(s)        : pStaMacAddr - StationMacAddr                           *
 *                                                                           *
 *  Output(s)       : PBssid - Pointer to BSSID                              *
 *                  :                                                        *
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE                              *
 *****************************************************************************/

UINT1
WssStaGetBssid (tMacAddr * pStaMacAddr, tMacAddr * pBssId)
{

    tWssStaStateDB      WssStaDB;
    tWssStaStateDB     *pWssStaDB = NULL;
    tWssWlanDB         *pwssWlanDBMsg = NULL;

    pwssWlanDBMsg = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();

    if (pwssWlanDBMsg == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaGetBssid:"
                    "Memory Allocation Failed \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pwssWlanDBMsg, 0, sizeof (tWssWlanDB));

    MEMCPY (WssStaDB.stationMacAddress, pStaMacAddr, sizeof (tMacAddr));

    pWssStaDB = ((tWssStaStateDB *) RBTreeGet (gWssStaStateDB,
                                               (tRBElem *) & WssStaDB));

    if (pWssStaDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaGetBssid:"
                    "No Entry available for the station MAC \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
        return OSIX_FAILURE;
    }

    pwssWlanDBMsg->WssWlanAttributeDB.u1RadioId = pWssStaDB->u1RadioId;
    pwssWlanDBMsg->WssWlanAttributeDB.u1WlanId = pWssStaDB->u1WlanId;
    pwssWlanDBMsg->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    pwssWlanDBMsg->WssWlanIsPresentDB.bBssId = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY,
                                  pwssWlanDBMsg) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaGetBssid"
                    " WssIfProcessWssWlanDBMsg returned FAILURE \r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
        return OSIX_FAILURE;
    }

    MEMCPY (pBssId, pwssWlanDBMsg->WssWlanAttributeDB.BssId, sizeof (tMacAddr));

    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
    return OSIX_SUCCESS;
}

#ifdef KERNEL_CAPWAP_WANTED
/*****************************************************************************
 *  Function Name   : WssStaUpdateVlanDB                                     *
 *                                                                           *
 *  Description     : This function is used to get the Data from DB          *
 *  Input(s)        : pWssStaDB - pointer to station db                      *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT1
WssStaUpdateVlanDB (tWssStaVlanDB * pWssStaVlanDB, UINT1 u1UpdateStatus)
{
    tWssStaVlanDB      *pStaVlanDB = NULL;

    pStaVlanDB = ((tWssStaVlanDB *) RBTreeGet (gWssStaVlanDB,
                                               (tRBElem *) pWssStaVlanDB));

    if (pStaVlanDB == NULL)
    {
        if (u1UpdateStatus == WSSSTA_VLAN_CREATE)
        {
            pStaVlanDB = (tWssStaVlanDB *)
                MemAllocMemBlk (WSSSTA_AP_VLANDB_POOLID);
            if (pStaVlanDB == NULL)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaUpdateVlanDB: "
                            "Memory Allocation Failed For AP VLAN DB \r\n");
                return OSIX_FAILURE;
            }

            pStaVlanDB->u2VlanId = pWssStaVlanDB->u2VlanId;
            if (RBTreeAdd (gWssStaVlanDB, pStaVlanDB) != RB_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WssStaUpdateVlanDB: Failed to add "
                            "entry to the RB Tree \r\n");
                MemReleaseMemBlock (WSSSTA_AP_VLANDB_POOLID,
                                    (UINT1 *) pStaVlanDB);
                return OSIX_FAILURE;
            }
        }
        else
        {
            WSSSTA_TRC (WSSSTA_INFO_TRC,
                        "WssStaUpdateVlanDB: No entry found in DB "
                        "for the given VLAN \r\n");
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (u1UpdateStatus == WSSSTA_VLAN_DELETE)
        {
            RBTreeRem (gWssStaVlanDB, (tRBElem *) pStaVlanDB);
            MemReleaseMemBlock (WSSSTA_AP_VLANDB_POOLID, (UINT1 *) pStaVlanDB);
        }

        if (u1UpdateStatus == WSSSTA_VLAN_SET)
        {
            pStaVlanDB->IpCreateFlag = pWssStaVlanDB->IpCreateFlag;
        }
        if (u1UpdateStatus == WSSSTA_VLAN_GET)
        {
            MEMCPY (pWssStaVlanDB, pStaVlanDB, sizeof (tWssStaVlanDB));
        }
    }
    return OSIX_SUCCESS;
}

UINT1
WssStaCreateandMapVlanToWlanIntf (UINT2 u2VlanId)
{
    tWssStaVlanDB       WssStaVlanDB;

    MEMSET (&WssStaVlanDB, 0, sizeof (tWssStaVlanDB));

    /* Invoke Station module to create vlan DB */
    WssStaVlanDB.u2VlanId = u2VlanId;

    if (WssStaUpdateVlanDB (&WssStaVlanDB, WSSSTA_VLAN_GET) == OSIX_SUCCESS)
    {
        if (WssStaVlanDB.AddStaFlag == OSIX_TRUE)
        {
#ifdef KERNEL_CAPWAP_WANTED
#ifdef  NPAPI_WANTED
            RadioNpWrHwVlantoWlanMapping (1, u2VlanId,
                                          WssStaVlanDB.au1IntfName,
                                          WssStaVlanDB.u1WlanId);
#endif
#endif
        }
    }
    else
    {
        if (WssStaUpdateVlanDB (&WssStaVlanDB,
                                WSSSTA_VLAN_CREATE) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }
    WssStaVlanDB.IpCreateFlag = OSIX_TRUE;
    WssStaUpdateVlanDB (&WssStaVlanDB, WSSSTA_VLAN_SET);

    return OSIX_SUCCESS;
}

UINT1
WssStaModifyandMapVlanToWlanIntf (UINT2 u2VlanId)
{
    tWssStaVlanDB       WssStaVlanDB;

    MEMSET (&WssStaVlanDB, 0, sizeof (tWssStaVlanDB));

    /* Invoke Station module to create vlan DB */
    WssStaVlanDB.u2VlanId = u2VlanId;

    if (WssStaUpdateVlanDB (&WssStaVlanDB, WSSSTA_VLAN_GET) == OSIX_SUCCESS)
    {
        if (WssStaVlanDB.AddStaFlag == OSIX_TRUE)
        {
#ifdef NPAPI_WANTED
#ifdef KERNEL_CAPWAP_WANTED
            RadioNpWrHwVlantoWlanMapping (1, u2VlanId,
                                          WssStaVlanDB.au1IntfName,
                                          WssStaVlanDB.u1WlanId);
#endif
#endif
        }
    }
    else
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

UINT1
WssStaDeleteVlanToWlanIntf (UINT2 u2VlanId)
{
    tWssStaVlanDB       WssStaVlanDB;

    MEMSET (&WssStaVlanDB, 0, sizeof (tWssStaVlanDB));

    /* Invoke Station module to create vlan DB */
    WssStaVlanDB.u2VlanId = u2VlanId;

    if (WssStaUpdateVlanDB (&WssStaVlanDB, WSSSTA_VLAN_GET) == OSIX_SUCCESS)
    {
        if (WssStaVlanDB.AddStaFlag == OSIX_TRUE)
        {
#ifdef NPAPI_WANTED
#ifdef KERNEL_CAPWAP_WANTED

            RadioNpWrHwVlantoWlanMapping (1, u2VlanId,
                                          WssStaVlanDB.au1IntfName,
                                          WssStaVlanDB.u1WlanId);
#endif
#endif
        }
    }
    if (WssStaUpdateVlanDB (&WssStaVlanDB, WSSSTA_VLAN_DELETE) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/* WMM changes to get client mac and update the kernel DB*/

/*****************************************************************************
 *  Function Name   : WssStaGetClientPerSSID                                 *
 *                                                                           *
 *  Description     : This function walks the RBTree and return the output   *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssStaGetClientPerSSID (UINT4 u4BssIfIndex)
{
    tMacAddr           *pu1staMacAddr = NULL;
    RBTreeWalk (gWssStaStateDB, (tRBWalkFn) StaDBClientMacWalkFn,
                &u4BssIfIndex, pu1staMacAddr);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : StaDBClientMacWalkFn                                          *
 *                                                                           *
 * Description  : This function is used to walk through station DB and       *
 *                and returns the Client Mac Address                         *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
StaDBClientMacWalkFn (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                      void *pArg, void *pOut)
{

    tWssStaStateDB     *pStaStateDB = NULL;
    UINT4               u4BssIfIndex = 0;
    tMacAddr           *pu1staMacAddr = NULL;
    tMacAddr            staMacAddr;
    UINT1               u1WlanId = 0;
    UINT1               u1RadioId = 0;

    UNUSED_PARAM (u4Level);
    MEMSET (&staMacAddr, 0, sizeof (tMacAddr));
    pu1staMacAddr = (tMacAddr *) & pOut;
    u4BssIfIndex = *(UINT4 *) pArg;

    if ((visit == postorder) || (visit == leaf))
    {
        if (pRBElem != NULL)
        {
            pStaStateDB = (tWssStaStateDB *) pRBElem;
            MEMCPY (pu1staMacAddr, pStaStateDB->stationMacAddress,
                    sizeof (tMacAddr));
            MEMCPY (staMacAddr, pStaStateDB->stationMacAddress,
                    sizeof (tMacAddr));
            u1RadioId = pStaStateDB->u1RadioId;
            u1WlanId = pStaStateDB->u1WlanId;
            WssWtpUpdateKernelDB (u1RadioId, u1WlanId, u4BssIfIndex,
                                  staMacAddr);
        }
    }
    return RB_WALK_CONT;
}
#endif
/*****************************************************************************
 *  Function Name   : WssStaConstructChanlSwitchAnnounce                     *
 *                                                                           *
 *  Description     : This function used to check whether the station is     *
 *                    connected to WLAN that is spectrum management enabled. *
 *                    If yes, Channel Switch announcement frame will be sent * 
 *                    to the station.                              *
 *  Input(s)        : RadioId                                                *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 ******************************************************************************/ UINT1
WssStaConstructChanlSwitchAnnounce (UINT1 u1RadioId,
                                    tRadioIfGetDB * pRadioIfGetDB)
{
    tWssWlanDB         *pwssWlanDBMsg = NULL;
    tWssStaStateDB      WssStaDB;
    tWssStaStateDB     *pWssStaDB = NULL;
    tWssMacMsgStruct   *pWssChanlSwitch = NULL;
    UINT1               u1Index = 0;
    UINT1               u1ChanWidth = 0;
    UINT2               u2SpectrumMgmt = 0;

    pwssWlanDBMsg = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();

    if (pwssWlanDBMsg == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaConstructChanlSwitchAnnounce:"
                    "Memory Allocation Failed \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pwssWlanDBMsg, 0, sizeof (tWssWlanDB));
    MEMSET (&WssStaDB, 0, sizeof (tWssStaStateDB));

    gu1DBWalkCnt = 0;
    /* This function calculates the number of stations connected */
    RBTreeWalk (gWssStaStateDB, (tRBWalkFn) StaApDBWalkFn, NULL, 0);

    if (gu1DBWalkCnt != 0)
    {
        /* Scan all the stations and take the action */
        for (u1Index = 0; u1Index < gu1DBWalkCnt; u1Index++)
        {
            MEMSET (pwssWlanDBMsg, 0, sizeof (tWssWlanDB));
            MEMSET (&WssStaDB, 0, sizeof (tWssStaStateDB));

            MEMCPY (WssStaDB.stationMacAddress,
                    gaWssStaWtpDBWalk[u1Index].staMacAddr, sizeof (tMacAddr));

            pWssStaDB = ((tWssStaStateDB *) RBTreeGet (gWssStaStateDB,
                                                       (tRBElem *) & WssStaDB));
            if (pWssStaDB == NULL)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            " WssStaConstructChanlSwitchAnnounce: Failed to "
                            "retrieve data from  Station DB \r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
                continue;
            }
            if (u1RadioId != pWssStaDB->u1RadioId)
            {
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
                continue;
            }
            pwssWlanDBMsg->WssWlanAttributeDB.u1RadioId = u1RadioId;
            pwssWlanDBMsg->WssWlanAttributeDB.u1WlanId = pWssStaDB->u1WlanId;
            pwssWlanDBMsg->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
            pwssWlanDBMsg->WssWlanIsPresentDB.bBssId = OSIX_TRUE;

            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY,
                                          pwssWlanDBMsg) != OSIX_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WssStaConstructChanlSwitchAnnounce"
                            "WssIfProcessWssWlanDBMsg returned FAILURE \r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
                return OSIX_FAILURE;
            }

            pwssWlanDBMsg->WssWlanIsPresentDB.bCapability = OSIX_TRUE;

            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                          pwssWlanDBMsg) != OSIX_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WssStaConstructChanlSwitchAnnounce"
                            "WssIfProcessWssWlanDBMsg returned FAILURE \r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
                return OSIX_FAILURE;
            }
            u2SpectrumMgmt =
                (UINT2) (((pwssWlanDBMsg->WssWlanAttributeDB.u2Capability)
                          & 0x0100) >> WSSSTA_SHIFT_8);

            /*u2SpectrumMgmt = ((pwssWlanDBMsg->WssWlanAttributeDB.u2Capability)
               & 0x0080) >> 7; */
            if (u2SpectrumMgmt == 1)
            {
                pWssChanlSwitch = (tWssMacMsgStruct *) (VOID *)
                    UtlShMemAllocMacMsgStructBuf ();
                if (pWssChanlSwitch == NULL)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WssStaConstructChanlSwitchAnnounce :"
                                "UtlShMemAllocMacMsgStructBuf returned failure\n");
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
                    return OSIX_FAILURE;
                }
                MEMSET (pWssChanlSwitch, 0, sizeof (tWssMacMsgStruct));

                pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                    ChanlSwitAnnounce.u1MacFrameChanlSwit_Mode = 0;
                /*Does not impose any restriction 
                 * for a station wrt transmission until a channel switch*/
                pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                    ChanlSwitAnnounce.u1MacFrameChanlSwit_NewChanlNum =
                    pRadioIfGetDB->RadioIfGetAllDB.u1CurrentChannel;
                pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                    ChanlSwitAnnounce.u1MacFrameChanlSwit_Cnt = 0;
                /*Channel switch occurs at any time after 
                 * the frame containing the CS element is transmitted*/
                u1ChanWidth = WssIfComputeBandWidth (pRadioIfGetDB);
                /* Action frame construction for 11ac */
                if (pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_AC)
                {
                    pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                        ChanwidthSwitch.u1ChanWidth = pRadioIfGetDB->
                        RadioIfGetAllDB.Dot11AcOperParams.u1VhtChannelWidth;
                    pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                        ChanwidthSwitch.u1CenterFcy0 = pRadioIfGetDB->
                        RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy0;
                    pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                        ChanwidthSwitch.u1CenterFcy1 = pRadioIfGetDB->
                        RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy1;
                    pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                        ChanwidthSwitch.isOptional = OSIX_TRUE;
                    pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                        ChanwidthSwitch.u1ElemLen =
                        WSSMAC_VHT_WIDE_BANDWIDTH_LEN;
                    pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                        ChanwidthSwitch.u1ElemId = WSSMAC_VHT_WIDE_BANDWIDTH_ID;

                    pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                        VhtTxPower.u1TransPower = pRadioIfGetDB->
                        RadioIfGetAllDB.VhtTransmitPower.u1TransPower;
                    pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                        VhtTxPower.u1TxPower20 = pRadioIfGetDB->
                        RadioIfGetAllDB.VhtTransmitPower.u1TxPower20;
                    pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                        VhtTxPower.u1ElemLen = WSSMAC_VHT_POWER_ELEM_LEN;
                    switch (u1ChanWidth)
                    {
                        case RADIO_VALUE_1:
                            pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                                VhtTxPower.u1TxPower40 = pRadioIfGetDB->
                                RadioIfGetAllDB.VhtTransmitPower.u1TxPower40;
                            pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                                VhtTxPower.u1ElemLen += RADIO_VALUE_1;
                            break;
                        case RADIO_VALUE_2:
                            pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                                VhtTxPower.u1TxPower80 = pRadioIfGetDB->
                                RadioIfGetAllDB.VhtTransmitPower.u1TxPower80;
                            pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                                VhtTxPower.u1ElemLen += RADIO_VALUE_2;
                            break;
                        case RADIO_VALUE_3:
                            pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                                VhtTxPower.u1TxPower160 = pRadioIfGetDB->
                                RadioIfGetAllDB.VhtTransmitPower.u1TxPower160;
                            pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                                VhtTxPower.u1ElemLen += RADIO_VALUE_3;
                            break;
                        default:
                            break;
                    }
                    pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                        VhtTxPower.isOptional = OSIX_TRUE;
                    pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                        VhtTxPower.u1ElemId = WSSMAC_VHT_TRANSMIT_POWER_ID;
                }
                MEMCPY (pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                        MacMgmtFrmHdr.u1SA,
                        pwssWlanDBMsg->WssWlanAttributeDB.BssId,
                        sizeof (tMacAddr));
                MEMCPY (pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                        MacMgmtFrmHdr.u1BssId,
                        pwssWlanDBMsg->WssWlanAttributeDB.BssId,
                        sizeof (tMacAddr));

                MEMCPY (pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                        MacMgmtFrmHdr.u1DA,
                        gaWssStaWtpDBWalk[u1Index].staMacAddr,
                        sizeof (tMacAddr));
                if (pRadioIfGetDB->RadioIfGetAllDB.u1ExChanSwitch == OSIX_TRUE)
                {
                    pWssChanlSwitch->unMacMsg.ActionChanlSwitchFrame.
                        ChanlSwitAnnounce.u1OperClass =
                        WssIfObtainOperClass (pRadioIfGetDB->RadioIfGetAllDB.
                                              u1CurrentChannel, u1ChanWidth);
                    if (WssMacProcessWssIfMsg (WSS_DOT11_MGMT_EX_CHANL_SWITCH,
                                               pWssChanlSwitch) != OSIX_SUCCESS)
                    {
                        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                    "WssStaConstructChanlSwitchAnnounce :"
                                    "UtlShMemAllocMacMsgStructBuf returned"
                                    " failure\n");
                        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
                        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssChanlSwitch);
                        return OSIX_FAILURE;

                    }
                }
                else
                {
                    if (WssMacProcessWssIfMsg (WSS_DOT11_MGMT_CHANL_SWITCH,
                                               pWssChanlSwitch) != OSIX_SUCCESS)
                    {
                        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                    "WssStaConstructChanlSwitchAnnounce :"
                                    "UtlShMemAllocMacMsgStructBuf returned"
                                    " failure\n");
                        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
                        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssChanlSwitch);
                        return OSIX_FAILURE;

                    }
                }
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssChanlSwitch);
            }
        }
    }
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
    return OSIX_SUCCESS;

}

/*****************************************************************************
 *  Function Name   : WssStaConstructMeasRequest                             *
 *                                                                           *
 *  Description     : This function used to check whether the station is     *
 *                    connected to WLAN that is spectrum management enabled. *
 *                    If yes, Meas request frame will be sent to the station. *
 *  Input(s)        : RadioId                                                *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT4
WssStaConstructMeasRequest (UINT1 u1RadioId)
{
    tWssWlanDB         *pwssWlanDBMsg = NULL;
    tWssStaStateDB      WssStaDB;
    tWssStaStateDB     *pWssStaDB = NULL;
    tWssMacMsgStruct   *pWssMeasRequest = NULL;
    UINT2               u2SpectrumMgmt = 0;
    UINT1               u1Index = 0;
    pwssWlanDBMsg = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();

    if (pwssWlanDBMsg == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaConstructMeasRequest:"
                    "Memory Allocation Failed \r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pwssWlanDBMsg, 0, sizeof (tWssWlanDB));
    MEMSET (&WssStaDB, 0, sizeof (tWssStaStateDB));
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    gu1DBWalkCnt = 0;
    /* This function calculates the number of stations connected */
    RBTreeWalk (gWssStaStateDB, (tRBWalkFn) StaApDBWalkFn, NULL, 0);
    if (gu1DBWalkCnt != 0)
    {
        /* Scan all the stations and take the action */
        for (u1Index = 0; u1Index < gu1DBWalkCnt; u1Index++)
        {
            MEMSET (pwssWlanDBMsg, 0, sizeof (tWssWlanDB));
            MEMSET (&WssStaDB, 0, sizeof (tWssStaStateDB));

            MEMCPY (WssStaDB.stationMacAddress,
                    gaWssStaWtpDBWalk[u1Index].staMacAddr, sizeof (tMacAddr));

            pWssStaDB = ((tWssStaStateDB *) RBTreeGet (gWssStaStateDB,
                                                       (tRBElem *) & WssStaDB));
            if (pWssStaDB == NULL)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WssStaConstructMeasRequest: Failed to "
                            "retrieve data from  Station DB \r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
                continue;
            }
            if (u1RadioId != pWssStaDB->u1RadioId)
            {
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
                continue;
            }
            pwssWlanDBMsg->WssWlanAttributeDB.u1RadioId = u1RadioId;
            pwssWlanDBMsg->WssWlanAttributeDB.u1WlanId = pWssStaDB->u1WlanId;
            pwssWlanDBMsg->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
            pwssWlanDBMsg->WssWlanIsPresentDB.bBssId = OSIX_TRUE;

            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY,
                                          pwssWlanDBMsg) != OSIX_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaConstructMeasRequest"
                            " WssIfProcessWssWlanDBMsg returned FAILURE \r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
                return OSIX_FAILURE;
            }

            pwssWlanDBMsg->WssWlanIsPresentDB.bCapability = OSIX_TRUE;

            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                          pwssWlanDBMsg) != OSIX_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaConstructMeasRequest"
                            " WssIfProcessWssWlanDBMsg returned FAILURE \r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
                return OSIX_FAILURE;
            }
            RadioIfGetDB.RadioIfGetAllDB.u1RadioId =
                pwssWlanDBMsg->WssWlanAttributeDB.u1RadioId;

            RadioIfGetDB.RadioIfIsGetAllDB.bFirstChannelNumber = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bNoOfChannels = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;

            if (WssIfProcessRadioIfDBMsg (WSS_GET_PHY_RADIO_IF_DB,
                                          &RadioIfGetDB) != OSIX_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaConstructMeasRequest"
                            " WssIfProcessRadioIfDBMsg returned FAILURE \r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
                return OSIX_FAILURE;
            }
            u2SpectrumMgmt = (UINT2) (((pwssWlanDBMsg->WssWlanAttributeDB.
                                        u2Capability) & 0x0100) >>
                                      WSSSTA_SHIFT_8);

            if (u2SpectrumMgmt == 1)
            {
                pWssStaDB->u1MeasReqDialogToken++;
                pWssMeasRequest = (tWssMacMsgStruct *) (VOID *)
                    UtlShMemAllocMacMsgStructBuf ();
                if (pWssMeasRequest == NULL)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WssStaConstructMeasRequest:"
                                "UtlShMemAllocMacMsgStructBuf returned failure\n");
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
                    return OSIX_FAILURE;
                }
                MEMSET (pWssMeasRequest, 0, sizeof (tWssMacMsgStruct));
                pWssMeasRequest->unMacMsg.ActionMeasReqFrame.
                    MeasRequest.u1DialogToken = pWssStaDB->u1MeasReqDialogToken;
                pWssMeasRequest->unMacMsg.ActionMeasReqFrame.
                    MeasRequest.u1MacFrameMESReq_MesToken =
                    WSSSTA_MEAS_REQ_TOKEN;
                pWssMeasRequest->unMacMsg.ActionMeasReqFrame.MeasRequest.
                    u1MacFrameMESReq_MesType = WSSSTA_MEAS_REQ_TYPE;
                pWssMeasRequest->unMacMsg.ActionMeasReqFrame.MeasRequest.
                    u1MacFrameMESReq_MesReqMode = WSSSTA_MEAS_REQ_MODE;
                pWssMeasRequest->unMacMsg.ActionMeasReqFrame.MeasRequest.
                    WssMESReqMesRequest.u1MESReqType_ChannelNum =
                    RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel +
                    (4 * u1Index);
                pWssMeasRequest->unMacMsg.ActionMeasReqFrame.MeasRequest.
                    WssMESReqMesRequest.u2MESReqType_Duration =
                    WSSSTA_MEAS_REQ_DURATION;
                pWssMeasRequest->unMacMsg.ActionMeasReqFrame.MeasRequest.
                    WssMESReqMesRequest.u8MESReqType_StartTime.u4LoWord = 0;
                pWssMeasRequest->unMacMsg.ActionMeasReqFrame.MeasRequest.
                    WssMESReqMesRequest.u8MESReqType_StartTime.u4HiWord = 0;
                MEMCPY (pWssMeasRequest->unMacMsg.ActionMeasReqFrame.
                        MacMgmtFrmHdr.u1SA,
                        pwssWlanDBMsg->WssWlanAttributeDB.BssId,
                        sizeof (tMacAddr));
                MEMCPY (pWssMeasRequest->unMacMsg.ActionMeasReqFrame.
                        MacMgmtFrmHdr.u1BssId,
                        pwssWlanDBMsg->WssWlanAttributeDB.BssId,
                        sizeof (tMacAddr));

                MEMCPY (pWssMeasRequest->unMacMsg.ActionMeasReqFrame.
                        MacMgmtFrmHdr.u1DA,
                        gaWssStaWtpDBWalk[u1Index].staMacAddr,
                        sizeof (tMacAddr));
                if (WssMacProcessWssIfMsg (WSS_DOT11_MGMT_MEAS_REQ,
                                           pWssMeasRequest) != OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WssStaConstructMeasRequest :"
                                "UtlShMemAllocMacMsgStructBuf returned failure\n");
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMeasRequest);
                    return OSIX_FAILURE;

                }
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMeasRequest);
            }
        }
    }
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
    return OSIX_SUCCESS;
}

#ifdef RFMGMT_WANTED
/*****************************************************************************
 *  Function Name   : WssStaConstructTpcRequest                              *
 *                                                                           *
 *  Description     : This function used to check whether the station is     *
 *                    connected to WLAN that is spectrum management enabled. *
 *                    If yes, TPC request frame will be sent to the station. *
 *  Input(s)        : RadioId                                                *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT4
WssStaConstructTpcRequest (tWssSta11hTpcMsg * pWssSta11hTpcMsg)
{
    tWssWlanDB         *pwssWlanDBMsg = NULL;
    tWssStaStateDB      WssStaDB;
    tWssStaStateDB     *pWssStaDB = NULL;
    tWssMacMsgStruct   *pWssTpcRequest = NULL;
    UINT2               u2SpectrumMgmt = 0;
    UINT1               u1Index = 0;
    UINT1               u1LoopCount = 0;
    UINT1               u1ReqSentPerCycle = MAX_STA_SUPP_PER_AP /
        WSSSTA_TPC_REQ_SPLIT;
    UINT1               u1StationReqCompletedCount = 0;

    /*u1StationReqCompletedCount - No of stations for whcih TPC have been
     * already sent*/
    /*u1TpcRequestSlot - can be 1, 2, 3 or 4. Refers to the slot number for
     * which the TPC request time period has been spilt*/
    /*u1ReqSentPerCycle - No of stations for which the TPC request is sent per
     * instance*/
    u1StationReqCompletedCount = (UINT1) ((pWssSta11hTpcMsg->u1TpcRequestSlot *
                                           u1ReqSentPerCycle) -
                                          u1ReqSentPerCycle);

    pwssWlanDBMsg = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();

    if (pwssWlanDBMsg == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaConstructTpcRequest:"
                    "Memory Allocation Failed \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pwssWlanDBMsg, 0, sizeof (tWssWlanDB));
    MEMSET (&WssStaDB, 0, sizeof (tWssStaStateDB));

    gu1DBWalkCnt = 0;
    /* This function calculates the number of stations connected */
    RBTreeWalk (gWssStaStateDB, (tRBWalkFn) StaApDBWalkFn, NULL, 0);
    if (gu1DBWalkCnt != 0)
    {
        if (gu1DBWalkCnt <= u1StationReqCompletedCount)
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
            return OSIX_SUCCESS;
        }
        if ((u1StationReqCompletedCount + u1ReqSentPerCycle) < gu1DBWalkCnt)
        {
            u1LoopCount = (UINT1) (u1StationReqCompletedCount +
                                   u1ReqSentPerCycle);
        }
        else
        {
            u1LoopCount = gu1DBWalkCnt;
        }
        /* Scan all the stations and take the action */
        for (u1Index = u1StationReqCompletedCount;
             u1Index < u1LoopCount; u1Index++)
        {
            MEMSET (pwssWlanDBMsg, 0, sizeof (tWssWlanDB));
            MEMSET (&WssStaDB, 0, sizeof (tWssStaStateDB));

            MEMCPY (WssStaDB.stationMacAddress,
                    gaWssStaWtpDBWalk[u1Index].staMacAddr, sizeof (tMacAddr));

            pWssStaDB = ((tWssStaStateDB *) RBTreeGet (gWssStaStateDB,
                                                       (tRBElem *) & WssStaDB));
            if (pWssStaDB == NULL)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            " WssStaConstructTpcRequest: Failed to "
                            "retrieve data from  Station DB \r\n");
                continue;
            }
            if (pWssSta11hTpcMsg->u1RadioId != pWssStaDB->u1RadioId)
            {
                continue;
            }
            pwssWlanDBMsg->WssWlanAttributeDB.u1RadioId =
                pWssSta11hTpcMsg->u1RadioId;
            pwssWlanDBMsg->WssWlanAttributeDB.u1WlanId = pWssStaDB->u1WlanId;
            pwssWlanDBMsg->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
            pwssWlanDBMsg->WssWlanIsPresentDB.bBssId = OSIX_TRUE;

            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY,
                                          pwssWlanDBMsg) != OSIX_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaConstructTpcRequest"
                            " WssIfProcessWssWlanDBMsg returned FAILURE \r\n");
                continue;
            }

            pwssWlanDBMsg->WssWlanIsPresentDB.bCapability = OSIX_TRUE;

            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                          pwssWlanDBMsg) != OSIX_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaConstructTpcRequest"
                            " WssIfProcessWssWlanDBMsg returned FAILURE \r\n");
                continue;
            }
            u2SpectrumMgmt =
                (UINT2) (((pwssWlanDBMsg->WssWlanAttributeDB.
                           u2Capability) & 0x0100) >> WSSSTA_SHIFT_8);

            if (u2SpectrumMgmt == 1)
            {
                pWssStaDB->u1DialogToken++;
                pWssTpcRequest = (tWssMacMsgStruct *) (VOID *)
                    UtlShMemAllocMacMsgStructBuf ();
                if (pWssTpcRequest == NULL)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WssStaConstructTpcRequest :"
                                "UtlShMemAllocMacMsgStructBuf returned failure\n");
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
                    return OSIX_FAILURE;
                }
                MEMSET (pWssTpcRequest, 0, sizeof (tWssMacMsgStruct));
                pWssTpcRequest->unMacMsg.ActionReqFrame.TPCRequest.
                    u1DialogToken = pWssStaDB->u1DialogToken;
                MEMCPY (pWssTpcRequest->unMacMsg.ActionReqFrame.MacMgmtFrmHdr.
                        u1SA, pwssWlanDBMsg->WssWlanAttributeDB.BssId,
                        sizeof (tMacAddr));
                MEMCPY (pWssTpcRequest->unMacMsg.ActionReqFrame.MacMgmtFrmHdr.
                        u1BssId, pwssWlanDBMsg->WssWlanAttributeDB.BssId,
                        sizeof (tMacAddr));

                MEMCPY (pWssTpcRequest->unMacMsg.ActionReqFrame.MacMgmtFrmHdr.
                        u1DA, gaWssStaWtpDBWalk[u1Index].staMacAddr,
                        sizeof (tMacAddr));
                if (WssMacProcessWssIfMsg
                    (WSS_DOT11_MGMT_TPC_REQ, pWssTpcRequest) != OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                                "WssStaConstructTpcRequest :"
                                "UtlShMemAllocMacMsgStructBuf returned failure\n");
                    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssTpcRequest);
                    continue;
                }
                UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssTpcRequest);
            }
        }
    }
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDBMsg);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaCheckIsLinkMarginUpdated                         *
 *                                                                           *
 *  Description     : Used to check whether the Link margin is same as in    *
 *                    previous TPC response or not.                          *
 *  Input(s)        : pStaMacAddr - StationMacAddr                           *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE                              *
 *****************************************************************************/
UINT1
WssStaCheckIsLinkMarginUpdated (tMacAddr * pStaAddr, INT1 i1LinkMargin)
{
    tWssStaStateDB      WssStaDB;
    tWssStaStateDB     *pWssStaDB = NULL;

    MEMSET (&WssStaDB, 0, sizeof (tWssStaStateDB));

    MEMCPY (WssStaDB.stationMacAddress, pStaAddr, sizeof (tMacAddr));

    pWssStaDB = ((tWssStaStateDB *) RBTreeGet (gWssStaStateDB,
                                               (tRBElem *) & WssStaDB));
    if (pWssStaDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    " WssStaCheckIsLinkMarginUpdated: Failed to "
                    "retrieve data from  Station DB \r\n");
        return OSIX_FAILURE;
    }
    /*If the new link margin is not the same as it is stored in DB,
       update the DB and return failure */
    if (pWssStaDB->i1LinkMargin != i1LinkMargin)
    {
        pWssStaDB->i1LinkMargin = i1LinkMargin;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
#endif
/*****************************************************************************
 *  Function Name   : WssIfStaUpdateIpAddress                                *
 *                                                                           *
 *  Description     : Used to add the upload filter in WTP                   *
 *                                                                           *
 *  Input(s)        : u4IpAddr - Station IP                                  *
 *                    pMacAddr - Station MAC                                 *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE                              *
 *****************************************************************************/
VOID
WssIfStaUpdateIpAddress (UINT4 u4IpAddr, UINT1 *pMacAddr)
{
    tWssStaStateDB     *pWssStaDB = NULL;
    tWssStaStateDB      wssStaStateDB;
    UINT1               au1InterfaceName[24] = { 0 };
    UINT1               u1DeleteFlag = 0;

    MEMSET (&wssStaStateDB, 0, sizeof (tWssStaStateDB));

    MEMCPY (wssStaStateDB.stationMacAddress, pMacAddr, sizeof (tMacAddr));

    WssifStaGetWtpInterfaceName (au1InterfaceName);
    pWssStaDB = ((tWssStaStateDB *) RBTreeGet (gWssStaStateDB,
                                               (tRBElem *) & wssStaStateDB));

    if (pWssStaDB != NULL)
    {

        pWssStaDB->u4IpAddr = u4IpAddr;
        WSSUserRoleConfigBandwidthForIP (pWssStaDB->stationMacAddress, u4IpAddr,
                                         pWssStaDB->u4BandWidth,
                                         pWssStaDB->u4DLBandWidth,
                                         pWssStaDB->u4ULBandWidth,
                                         au1InterfaceName, u1DeleteFlag);
    }
}

/*****************************************************************************
 *  Function Name   : WssIfStaGetIpAddr                                      *
 *                                                                           *
 *  Description     : Used to get the Ip address by capwapassemble  from DB  *  
 *                                                                           *
 *  Input(s)        : pu4IpAddr - Station IP address                         *
 *                    pMacAddr - Station MAC                       *
 *                                         *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE                              *
 *****************************************************************************/
UINT1
WssIfStaGetIpAddr (UINT4 *pu4Ipaddr, UINT1 *pMacAddr)
{

    tWssStaStateDB     *pWssStaDB = NULL;
    tWssStaStateDB      wssStaStateDB;

    MEMSET (&wssStaStateDB, 0, sizeof (tWssStaStateDB));

    MEMCPY (wssStaStateDB.stationMacAddress, pMacAddr, sizeof (tMacAddr));

    pWssStaDB = ((tWssStaStateDB *) RBTreeGet (gWssStaStateDB,
                                               (tRBElem *) & wssStaStateDB));

    if (pWssStaDB != NULL)
    {

        *pu4Ipaddr = pWssStaDB->u4IpAddr;

        return OSIX_SUCCESS;

    }
    else
    {

        return OSIX_FAILURE;
    }

}

/*****************************************************************************
 *  Function Name   : WssIfStaUpdateIpAddressDelete                          *
 *                                                                           *
 *  Description     : Used to delete the upload filter in WTP                *
 *                                                                           *
 *  Input(s)        : u4IpAddr - Station IP                                  *
 *                    pMacAddr - Station MAC                                 *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE                              *
 *****************************************************************************/
VOID
WssIfStaUpdateIpAddressDelete (UINT4 u4IpAddr, UINT1 *pMacAddr)
{
    tWssStaStateDB     *pWssStaDB = NULL;
    tWssStaStateDB      wssStaStateDB;
    UINT1               au1InterfaceName[24] = { 0 };
    UINT1               u1DeleteFlag = 1;

    MEMSET (&wssStaStateDB, 0, sizeof (tWssStaStateDB));

    MEMCPY (wssStaStateDB.stationMacAddress, pMacAddr, sizeof (tMacAddr));
    WssifStaGetWtpInterfaceName (au1InterfaceName);
    pWssStaDB = ((tWssStaStateDB *) RBTreeGet (gWssStaStateDB,
                                               (tRBElem *) & wssStaStateDB));

    if (pWssStaDB != NULL)
    {
        WSSUserRoleConfigBandwidthForIP (pWssStaDB->stationMacAddress, u4IpAddr,
                                         pWssStaDB->u4BandWidth,
                                         pWssStaDB->u4DLBandWidth,
                                         pWssStaDB->u4ULBandWidth,
                                         au1InterfaceName, u1DeleteFlag);
    }

}

/*****************************************************************************
 *  Function Name   : WssifStaGetWtpInterfaceName                            *
 *                                                                           *
 *  Description     : Used to get the interface name in the WTP              *
 *                                                                           *
 *  Input(s)        : pu1InterfaceName - InterfaceName                       *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : None                                                   *
 *****************************************************************************/
VOID
WssifStaGetWtpInterfaceName (UINT1 *pu1InterfaceName)
{
#ifdef KERNEL_CAPWAP_WANTED
    MEMCPY (pu1InterfaceName, gau1InterfaceName, 24);
#else
    UNUSED_PARAM (pu1InterfaceName);
#endif
}

#ifdef BAND_SELECT_WANTED

/*****************************************************************************
 *  Function Name   : WssStaGetRadioInfo                                     *
 *                                                                           *
 *  Description     : Used to get the radio Id and wlan Id from STA MAC      *
 *                                                                           *
 *  Input(s)        : pStaMacAddr - StationMacAddr                           *
 *                                                                           *
 *  Output(s)       : pu1RadioId -  Radio ID                                 *
 *                    pu1WlanId  -  Wlan  Id                                 *
 *                                                                           *
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE                              *
 *****************************************************************************/

INT4
WssStaGetRadioInfo (UINT1 *pu1StaMac, UINT1 *pu1RadioId, UINT1 *u1WlanId)
{
    tWssStaStateDB      WssStaDB;
    tWssStaStateDB     *pWssStaDB = NULL;

    MEMSET (&WssStaDB, 0, sizeof (tWssStaStateDB));

    MEMCPY (WssStaDB.stationMacAddress, pu1StaMac, sizeof (tMacAddr));

    pWssStaDB = ((tWssStaStateDB *) RBTreeGet (gWssStaStateDB,
                                               (tRBElem *) & WssStaDB));

    if (pWssStaDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    " WssStaCheckIsLinkMarginUpdated: Failed to "
                    "retrieve data from  Station DB \r\n");
        return OSIX_FAILURE;
    }

    *pu1RadioId = pWssStaDB->u1RadioId;
    *u1WlanId = pWssStaDB->u1WlanId;

    return OSIX_SUCCESS;

}

#endif

#endif
