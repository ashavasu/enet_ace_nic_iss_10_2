/*****************************************************************************

 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 *  $Id: wssifcapwap.c,v 1.7.2.1 2018/03/08 14:01:03 siva Exp $
 * Description: This file contains the CAPWAP Packet RX related functions    *
 *****************************************************************************/
#ifndef __WSSIFCAPWAP_C__
#define __WSSIFCAPWAP_C__

#include "wssifinc.h"
#include "wsscfginc.h"
#include "wssifcapdb.h"
#include "wssifcapproto.h"
#include "iss.h"
#include "sizereg.h"
#include "capwapconst.h"
#include "wtpnvram.h"
#ifdef LNXIP4_WANTED
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#endif
#include <sys/ioctl.h>

#ifdef KERNEL_CAPWAP_WANTED
#include "fcusprot.h"
#ifdef WTP_WANTED
#include "fcusglob.h"
#endif
#endif

#define MAX_WTP_PROFILE                4097
#define CAPWAP_MODEL_CHECK_ENABLE        1
#define MAX_MTU_SIZE 1500

#if defined (LNXIP4_WANTED)
extern UINT1       *CfaGddGetLnxIntfnameForPort (UINT2 u2Index);
#endif

extern UINT4        WssStaGetStationDB (tWssStaStateDB * pWssStaStateDB);
extern INT4         WlchdlrFreeTransmittedPacket (tRemoteSessionManager *
                                                  pSessEntry);
extern INT4         AphdlrFreeTransmittedPacket (tRemoteSessionManager *
                                                 pSessEntry);

tRBTree             gCapProfileMacMapTable;
tRBTree             gCapProfileNameMapTable;
static tRBTree      gCpSessPortMapTable;
static tRBTree      gCapBindingTable;
static tRBTree      gCpSessProfileIdMapTable;
static tRBTree      gCapDtlsMapTable;
static tRBTree      gCapProfildIdMapTable;
static tRBTree      gCapWlcWhiteListTable;
static tRBTree      gCapWlcBlackListTable;
static tRBTree      gCapStaWhiteListTable;
static tRBTree      gCapStaBlackListTable;
static tRBTree      gCapWtpModelMapTable;
static tRBTree      gCapWtpProfileTable;
static tRBTree      gCapDscpMapTable;
static tRemoteSessionManager gaRemoteSessionManager[MAX_WTP_PROFILE_SUPPORTED];
static tWssIfCapwapDB gWssCapDB;
static tWtpIntProfile gaWtpIntProfile[MAX_WTP_PROFILE_SUPPORTED];
static UINT2        gu2WtpModelEntry;
static UINT2        gu2WtpProfileId;
#ifdef WLC_WANTED
static tRBTree      gCapDhcpGlobalTable;
static tRBTree      gCapWtpDhcpTable;
static tRBTree      gCapNATConfigTable;
static tRBTree      gCapWtpLRConfigTable;
static tRBTree      gCapWtpIpRouteConfigTable;
static tRBTree      gCapWtpDhcpConfigTable;
static tRBTree      gCapWtpFwlFilterTable;
static tRBTree      gCapWtpFwlRuleTable;
static tRBTree      gCapWtpFwlAclTable;
static tRBTree      gCapWtpL3SubIfTable;
#endif /* WLC_WANTED */

extern INT4         WlcHdlrTmrStop (tRemoteSessionManager *, UINT1);

INT4
WssIfGetIfIpInfo (UINT1 *pu1IfName, UINT4 *pNodeIpAddr)
{
    INT4                i4RetVal = OSIX_SUCCESS;
#ifdef LNXIP4_WANTED
    struct ifreq        ifreq;
    struct sockaddr_in *paddr = NULL;
    MEMSET (&ifreq, 0, sizeof (ifreq));
    INT4                i4SockFd = -1;
    WSSIF_FN_ENTRY ();
    i4SockFd = socket (AF_INET, SOCK_DGRAM, 0);
    if (i4SockFd > 0)
    {
        STRNCPY (ifreq.ifr_name, pu1IfName, IFNAMSIZ);
        ifreq.ifr_name[IFNAMSIZ - 1] = '\0';

        if (ioctl (i4SockFd, SIOCGIFADDR, &ifreq) < 0)
        {
            i4RetVal = OSIX_FAILURE;
        }
        else
        {
            paddr = (struct sockaddr_in *) (VOID *) &ifreq.ifr_addr;
            *pNodeIpAddr = OSIX_NTOHL (paddr->sin_addr.s_addr);
        }
        close (i4SockFd);
    }
    else if (i4SockFd < 0)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "Failed to Create the socket \r\n");
        i4RetVal = OSIX_FAILURE;
/* COVERITY FIX START */
        /*close (i4SockFd); */
/* COVERITY FIX END */
    }
/* COVERITY FIX START */
    else
    {
        close (i4SockFd);
    }
/* COVERITY FIX END */
    WSSIF_FN_EXIT ();
#else
    UNUSED_PARAM (pu1IfName);
    UNUSED_PARAM (pNodeIpAddr);
#endif
    return (i4RetVal);

}

INT4
WssIfCapwapDBInit (VOID)
{

#ifdef WTP_WANTED
    UINT1               au1IfName[5] = { 0, 0, 0, 0, 0 };
#endif
    UINT2               u2Index = 0;

#ifdef WTP_WANTED
#ifdef DAK_WANTED
    tMacAddr            DefaultSwitchMac;
    struct ifreq        ifr;
    MEMSET (DefaultSwitchMac, 0, sizeof (tMacAddr));

    int                 sock = socket (AF_INET, SOCK_DGRAM, IPPROTO_IP);
    STRCPY (ifr.ifr_name, "br-lan");
    if (ioctl (sock, SIOCGIFHWADDR, &ifr) == 0)
    {
        MEMCPY (DefaultSwitchMac, ifr.ifr_hwaddr.sa_data, sizeof (tMacAddr));
        IssSetSwitchBaseMacAddressToNvRam (DefaultSwitchMac);
    }
#endif
#endif

    for (u2Index = 0; u2Index < MAX_WTP_PROFILE_SUPPORTED; u2Index++)
    {
        gWssCapDB.CapwapGetDB[u2Index].u1WtpDiscType = 4;
        gWssCapDB.CapwapGetDB[u2Index].u2WtpEchoInterval = 30;
        gWssCapDB.CapwapGetDB[u2Index].u2WtpStatisticsTimer = 120;
        gWssCapDB.CapwapGetDB[u2Index].u2WtpMaxDiscoveryInterval = 20;
        gWssCapDB.CapwapGetDB[u2Index].u1WtpEcnSupport = 1;
        gWssCapDB.CapwapGetDB[u2Index].u1WtpFallbackEnable = 1;
        gWssCapDB.CapwapGetDB[u2Index].u4WtpIdleTimeout = 300;
        gWssCapDB.CapwapGetDB[u2Index].au1DiscPadding[0] = 0xFF;
        gWssCapDB.CapwapGetDB[u2Index].au1DiscPadding[1] = 0xFF;
        gWssCapDB.CapwapGetDB[u2Index].u1WtpCtrlDTLSStatus = 2;
        gWssCapDB.CapwapGetDB[u2Index].u1WtpDataDTLSStatus = 2;
        gWssCapDB.CapwapGetDB[u2Index].u1FragReassembleStatus = 1;
        gWssCapDB.CapwapGetDB[u2Index].u4PathMTU = MAX_MTU_SIZE;
        gWssCapDB.CapwapGetDB[u2Index].u4WtpCtrlDTLSCount = 0;
    }

    gWssCapDB.u1DiscMode = 1;
    gWssCapDB.u1CtrlDTLSStatus = 1;
    gWssCapDB.u1DataDTLSStatus = 1;
    gWssCapDB.u1ModelCheck = CAPWAP_MODEL_CHECK_ENABLE;

#ifdef WLC_WANTED
    gWssCapDB.LocalIpAddr.u4_addr[0] = IssGetIpAddrFromNvRam ();
#endif
#ifdef WTP_WANTED
    /* COVERITY FIX START */
    strncpy ((CHR1 *) au1IfName, CAPWAP_WTP_IF_INTERFACE_NAME,
             sizeof (CAPWAP_WTP_IF_INTERFACE_NAME));
    /* COVERITY FIX END */
    if (WssIfGetIfIpInfo ((UINT1 *) au1IfName,
                          &gWssCapDB.LocalIpAddr.u4_addr[0]) == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Failed to Get the Local Ip Address \r\n");
        /* update the Default */
        gWssCapDB.LocalIpAddr.u4_addr[0] = IssGetIpAddrFromNvRam ();
    }
    /* gWssCapDB.LocalIpAddr.u4_addr[0] = IssGetIpAddrFromNvRam(); */
/* Updates the CAPWAP's database with values obtained from NVRAM */
    gWssCapDB.u1AcRefOption = (UINT1) WssGetAcRefTypeFromWtpNvRam ();
    gWssCapDB.CapwapGetDB[0].u1WtpMacType =
        (UINT1) WssGetWtpMacTypeFromWtpNvRam ();
    gWssCapDB.CapwapGetDB[0].u1WtpTunnelMode =
        (UINT1) WssGetWtpTunlModeFromWtpNvRam ();
    /* Above function is not working
       gWssCapDB.CapwapGetDB[0].u1WtpTunnelMode = 0x08; */
    gWssCapDB.u4CtrludpPort = WssGetCapwapUdpSPortFromWtpNvRam ();
    gWssCapDB.CapwapGetDB[0].u4NativeVlan = WssGetNativeVlanFromWtpNvRam ();
    MEMCPY (gWssCapDB.CapwapGetDB[0].au1WtpModelNumber,
            WssGetWtpModelNumFromWtpNvRam (), 50);
    MEMCPY (gWssCapDB.CapwapGetDB[0].au1WtpSerialNumber,
            WssGetWtpSerialNumFromWtpNvRam (), 50);
    MEMCPY (gWssCapDB.CapwapGetDB[0].au1WtpBoardId,
            WssGetWtpBoardIdFromWtpNvRam (), 50);
    MEMCPY (gWssCapDB.CapwapGetDB[0].au1WtpBoardRivision,
            WssGetWtpBoardRevFromWtpNvRam (), 50);
    MEMCPY (gWssCapDB.CapwapGetDB[0].au1WtpImageIdentifier, IssGetImageName (),
            50);
    MEMCPY (gWssCapDB.CapwapGetDB[0].WtpMacAddress, IssGetSwitchMacFromNvRam (),
            MAC_ADDR_LEN);
    MEMCPY (gWssCapDB.CapwapGetDB[0].au1WtpHWversion, IssGetHardwareVersion (),
            50);
    MEMCPY (gWssCapDB.CapwapGetDB[0].au1WtpSWversion, IssGetSoftwareVersion (),
            50);
    strncpy ((CHR1 *) gWssCapDB.CapwapGetDB[0].au1WtpBootversion, "BOOT VER1",
             sizeof ("BOOT VER1"));
    gWssCapDB.CapwapGetDB[0].u4VendorSMI = 0xff;
    gWssCapDB.CapwapGetDB[0].u1WtpDiscType = WssGetDiscoveryTypeFromWtpNvRam ();
    gWssCapDB.CapwapGetDB[0].u2WtpEchoInterval = 30;
    gWssCapDB.CapwapGetDB[0].u2WtpMaxDiscoveryInterval = 50;
    gWssCapDB.CapwapGetDB[0].u2WtpMaxDiscoveryInterval = 10;
    gWssCapDB.CapwapGetDB[0].u1WtpEcnSupport = 1;
    gWssCapDB.CapwapGetDB[0].u1WtpFallbackEnable = 1;
    gWssCapDB.CapwapGetDB[0].u4WtpIdleTimeout = 300;
    gWssCapDB.CapwapGetDB[0].au1DiscPadding[0] = 0xFF;
    gWssCapDB.CapwapGetDB[0].au1DiscPadding[1] = 0xFF;
    gWssCapDB.CapwapGetDB[0].u2WtpStatisticsTimer = 120;
    MEMCPY (gWssCapDB.CapwapGetDB[0].au1WtpLocation, IssGetSysLocation (), 512);
    MEMCPY (gWssCapDB.CapwapGetDB[0].au1WtpName, IssGetSwitchName (), 512);
#else
    MEMCPY (gWssCapDB.au1WlcSWversion, IssGetSoftwareVersion (), 50);
    MEMCPY (gWssCapDB.au1WlcHWversion, IssGetHardwareVersion (), 50);
    MEMCPY (gWssCapDB.au1WlcName, IssGetSwitchName (), 512);
    gWssCapDB.CapwapGetDB[0].u1WtpTunnelMode = 8;    /* Native Tunnel Mode */
    gWssCapDB.CapwapGetDB[0].u1WtpMacType = 1;    /* Split MAC Type */
    gWssCapDB.u4CtrludpPort = 5246;
    gWssCapDB.u2MaxWtps = MAX_WTP_SUPPORTED;
#endif

    return OSIX_SUCCESS;

}

/*****************************************************************************
 *  Function Name   : WssIfRemoteSessionInit                                *
 *                                                                           *
 *  Description     : This function used to initialize the Wtp Remote        *
 *                    session module                                         *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfRemoteSessionInit ()
{
    UINT4               u4Index = 0;

    for (u4Index = 0; u4Index < MAX_RSM_SUPPORTED; u4Index++)
    {
        /* Initialize the remote session Manager with default values */
        gaRemoteSessionManager[u4Index].u1Used = OSIX_FALSE;
        gaRemoteSessionManager[u4Index].sessionId = 0;
        gaRemoteSessionManager[u4Index].eCurrentState = CAPWAP_IDLE;
        gaRemoteSessionManager[u4Index].PMTU = CAPWAP_MTU_SIZE;
        gaRemoteSessionManager[u4Index].lastProcesseddSeqNum =
            CAPWAP_MAX_SEQ_NUM;
        gaRemoteSessionManager[u4Index].lastTransmitedSeqNum =
            CAPWAP_MAX_SEQ_NUM;
        gaRemoteSessionManager[u4Index].CapwapTxList = NULL;
        gaRemoteSessionManager[u4Index].isRequestMsg = OSIX_FALSE;
        gaRemoteSessionManager[u4Index].isResponsePending = OSIX_FALSE;
        gaRemoteSessionManager[u4Index].lastTransmittedPkt = NULL;
        gaRemoteSessionManager[u4Index].lastTransmittedPktLen = 0;
        gaRemoteSessionManager[u4Index].u4FileWriteOffset = 0;
        gaRemoteSessionManager[u4Index].u1ImageDataTimerRunning = OSIX_FALSE;
        gaRemoteSessionManager[u4Index].ePrevState = CAPWAP_START;
        gaRemoteSessionManager[u4Index].u4ImgDataFileWriteOffset = 0;
        gaRemoteSessionManager[u4Index].u4FileReadOffset = 0;
        gaRemoteSessionManager[u4Index].u1EndofFileReached = OSIX_FALSE;
        gaRemoteSessionManager[u4Index].u1WriteFileOpened = OSIX_FALSE;
        gaRemoteSessionManager[u4Index].u1ReadFileOpened = OSIX_FALSE;
        gaRemoteSessionManager[u4Index].writeFd = NULL;
        gaRemoteSessionManager[u4Index].readFd = NULL;
        gaRemoteSessionManager[u4Index].i4FileSize = 0;
        gaRemoteSessionManager[u4Index].readCrashFd = NULL;
        gaRemoteSessionManager[u4Index].writeCrashFd = NULL;
        gaRemoteSessionManager[u4Index].u1CrashEndofFileReached = OSIX_FALSE;
        gaRemoteSessionManager[u4Index].u1CrashWriteEndofFileReached =
            OSIX_FALSE;
        gaRemoteSessionManager[u4Index].u1CrashWriteFileOpened = OSIX_FALSE;
        gaRemoteSessionManager[u4Index].i4CrashFileSize = 0;
        gaRemoteSessionManager[u4Index].u1CrashReadFileOpened = OSIX_FALSE;
        gaRemoteSessionManager[u4Index].u4CrashFileReadOffset = 0;
        gaRemoteSessionManager[u4Index].u1CrashFileNameSet = OSIX_FALSE;
        gaRemoteSessionManager[u4Index].u1DataTransferTimerRunning = OSIX_FALSE;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfGetRSMFromJoinSessionId (tWssIfCapDB * pCpDB)
{
    UINT2               u2Len = 16;
    UINT2               u2Index = 0;

    WSSIF_FN_ENTRY ();
    for (u2Index = 0; u2Index < MAX_RSM_SUPPORTED; u2Index++)
    {
        if (gaRemoteSessionManager[u2Index].u1Used == OSIX_TRUE)
        {
            if (MEMCMP (pCpDB->JoinSessionID,
                        gaRemoteSessionManager[u2Index].JoinSessionID,
                        u2Len) == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Found the session Entry in RSM \r\n");
                pCpDB->pSessEntry = &gaRemoteSessionManager[u2Index];
                return OSIX_SUCCESS;
            }
        }
    }
    WSSIF_FN_EXIT ();
    return OSIX_FAILURE;
}

/*****************************************************************************
 *  Function Name   : WssIfDeleteRSMFromJoinSessionId                        *
 *                                                                           *
 *  Description     : This function deletes the entry in the                 *
 *                    Remote session Module.                                 *
 *                                                                           *
 *  Input(s)        : u4SessionId - WTP unique session id.The incoming Ip    *
 *                    address or the unique id given by DTLS                 *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : NULL on failure                                        *
 *                    tRemoteSessionManager Pointer on success               *
 *****************************************************************************/
INT4
WssIfDeleteRSMFromJoinSessionId (tWssIfCapDB * pCpDB)
{
    UINT2               u2Len = 16;
    UINT2               u2Index = 0;

    WSSIF_FN_ENTRY ();
    for (u2Index = 0; u2Index < MAX_RSM_SUPPORTED; u2Index++)
    {
        if (gaRemoteSessionManager[u2Index].u1Used == OSIX_TRUE)
        {
            if (MEMCMP (pCpDB->JoinSessionID,
                        gaRemoteSessionManager[u2Index].JoinSessionID, u2Len)
                == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Found the session Entry in RSM \r\n");
                gaRemoteSessionManager[u2Index].sessionId = 0;
                gaRemoteSessionManager[u2Index].eCurrentState = CAPWAP_IDLE;
                gaRemoteSessionManager[u2Index].PMTU = CAPWAP_MTU_SIZE;
                gaRemoteSessionManager[u2Index].lastProcesseddSeqNum =
                    CAPWAP_MAX_SEQ_NUM;
                gaRemoteSessionManager[u2Index].lastTransmitedSeqNum =
                    CAPWAP_MAX_SEQ_NUM;
                gaRemoteSessionManager[u2Index].CapwapTxList = NULL;
                gaRemoteSessionManager[u2Index].isRequestMsg = OSIX_FALSE;
                gaRemoteSessionManager[u2Index].isResponsePending = OSIX_FALSE;
                gaRemoteSessionManager[u2Index].lastTransmittedPkt = NULL;
                gaRemoteSessionManager[u2Index].lastTransmittedPktLen = 0;
                gaRemoteSessionManager[u2Index].u4FileWriteOffset = 0;
                gaRemoteSessionManager[u2Index].u1ImageDataTimerRunning =
                    OSIX_FALSE;
                gaRemoteSessionManager[u2Index].ePrevState = CAPWAP_START;
                gaRemoteSessionManager[u2Index].u4ImgDataFileWriteOffset = 0;
                gaRemoteSessionManager[u2Index].u4FileReadOffset = 0;
                gaRemoteSessionManager[u2Index].u1EndofFileReached = OSIX_FALSE;
                gaRemoteSessionManager[u2Index].u1WriteFileOpened = OSIX_FALSE;
                gaRemoteSessionManager[u2Index].u1ReadFileOpened = OSIX_FALSE;
                gaRemoteSessionManager[u2Index].writeFd = NULL;
                gaRemoteSessionManager[u2Index].readFd = NULL;
                gaRemoteSessionManager[u2Index].i4FileSize = 0;
                MEMSET (&gaRemoteSessionManager[u2Index].JoinSessionID, 0,
                        MAC_LENGTH);;
                return OSIX_SUCCESS;
            }
        }
    }
    WSSIF_FN_EXIT ();
    return OSIX_FAILURE;
}

/*****************************************************************************
 *  Function Name   : WssIfRemoteSessionCreate                              *
 *                                                                           *
 *  Description     : This function activates the entry in the               *
 *                    Remote session Module.                                 *
 *                                                                           *
 *  Input(s)        : u4SessionId - WTP unique session id.The incoming Ip    *
 *                    address or the unique id given by DTLS                 *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : NULL on failure                                        *
 *                    tRemoteSessionManager Pointer on success               *
 *****************************************************************************/
tRemoteSessionManager *
WssIfRemoteSessionCreate (UINT4 u4DestIpAddr, UINT4 u4DestPort)
{
    UINT2               u2Index = 0;
    UINT2               u2FreeIndex = 0;
    UINT2               u2FreeEntryFlag = OSIX_FALSE;

    u2Index = 0;
    while (u2Index < MAX_RSM_SUPPORTED)
    {
        if ((gaRemoteSessionManager[u2Index].u1Used == OSIX_FALSE) &&
            (u2FreeEntryFlag == OSIX_FALSE))
        {
            /*Found the free index. note it */
            u2FreeIndex = u2Index;
            u2FreeEntryFlag = OSIX_TRUE;
            break;
        }
        u2Index++;
    }

    if (u2FreeEntryFlag == OSIX_TRUE)
    {
        /* Create the entry in the Wtp Profile RB Tree */
        if (WssIfSessionPortEntryCreate (u4DestIpAddr, u4DestPort, u2FreeIndex)
            == OSIX_FAILURE)
        {
            WSSIF_TRC (WSSIF_FAILURE_TRC,
                       "Failed to create the entry in Wtp Profile Session Entry \r\n");
            return NULL;
        }
        gaRemoteSessionManager[u2FreeIndex].remoteIpAddr.u4_addr[0] =
            u4DestIpAddr;
        gaRemoteSessionManager[u2FreeIndex].u4RemoteCtrlPort = u4DestPort;
        gaRemoteSessionManager[u2FreeIndex].u2CapwapRSMIndex = u2FreeIndex;

        gaRemoteSessionManager[u2FreeIndex].CapwapTxList =
            UtlDynSllInit (10, NULL);
        gaRemoteSessionManager[u2FreeIndex].u1Used = OSIX_TRUE;
        gaRemoteSessionManager[u2FreeIndex].lastTransmittedPkt = NULL;
        gaRemoteSessionManager[u2FreeIndex].lastTransmittedPktLen = 0;
        gaRemoteSessionManager[u2FreeIndex].u4ImgDataFileWriteOffset = 0;
        gaRemoteSessionManager[u2FreeIndex].u4FileReadOffset = 0;
        gaRemoteSessionManager[u2FreeIndex].u1EndofFileReached = OSIX_FALSE;
        gaRemoteSessionManager[u2FreeIndex].u1ImageDataTimerRunning =
            OSIX_FALSE;
        gaRemoteSessionManager[u2FreeIndex].ePrevState = CAPWAP_START;
        gaRemoteSessionManager[u2FreeIndex].u1WriteFileOpened = OSIX_FALSE;
        gaRemoteSessionManager[u2FreeIndex].u1ReadFileOpened = OSIX_FALSE;
        gaRemoteSessionManager[u2FreeIndex].writeFd = NULL;
        gaRemoteSessionManager[u2FreeIndex].readFd = NULL;
        gaRemoteSessionManager[u2FreeIndex].i4FileSize = 0;
        gaRemoteSessionManager[u2FreeIndex].readCrashFd = NULL;
        gaRemoteSessionManager[u2FreeIndex].writeCrashFd = NULL;
        gaRemoteSessionManager[u2FreeIndex].u1CrashEndofFileReached =
            OSIX_FALSE;
        gaRemoteSessionManager[u2FreeIndex].u1CrashWriteEndofFileReached =
            OSIX_FALSE;
        gaRemoteSessionManager[u2FreeIndex].u1CrashWriteFileOpened = OSIX_FALSE;
        gaRemoteSessionManager[u2FreeIndex].i4CrashFileSize = 0;
        gaRemoteSessionManager[u2FreeIndex].u1CrashReadFileOpened = OSIX_FALSE;
        gaRemoteSessionManager[u2FreeIndex].u4CrashFileReadOffset = 0;
        gaRemoteSessionManager[u2FreeIndex].u1CrashFileNameSet = OSIX_FALSE;
        gaRemoteSessionManager[u2FreeIndex].u1DataTransferTimerRunning =
            OSIX_FALSE;
        return (&gaRemoteSessionManager[u2FreeIndex]);
    }
    else
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Remote session Module is Full can not create New entries \r\n");
        return NULL;
    }
    return NULL;
}

/*****************************************************************************
 *  Function Name   : WssIfRemoteSessionDelete                              *
 *                                                                           *
 *  Description     : This function de-activeate the entry in the            *
 *                    Remote session Module.                                 *
 *                                                                           *
 *  Input(s)        : u4WtpSessionId - Wtp unique Session Id                 *
 *                    u2IntProfileId - Internal Profile Id                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfRemoteSessionDelete (UINT4 u4IpAddr, UINT4 u4DestPort)
{
    tRemoteSessionManager *pSess = NULL;
#ifdef WLC_WANTED
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
#endif

    WSSIF_FN_ENTRY ();
    pSess = WssIfRemoteSessionGet (u4IpAddr, u4DestPort);

    if (pSess != NULL)
    {

#ifdef WLC_WANTED
        WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
            MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

        /*Stop the wlchdlr retransmit timer when capwap session is deleted */
        if (WlcHdlrTmrStop (pSess, 0) == OSIX_FAILURE)
        {
            WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfRemoteSessionDelete"
                       ":: Retransmit Timer Stop Failure\r\n");
        }

        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = pSess->u2IntProfileId;
        pWssIfCapwapDB->CapwapIsGetAllDB.bPathMTU = OSIX_TRUE;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                     pWssIfCapwapDB) != OSIX_SUCCESS)
        {
            WSSIF_TRC (WSSIF_FAILURE_TRC,
                       " Failed to retrieve the MTU from the CAPWAP DB\r\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

#endif

#ifdef WLC_WANTED
        WlchdlrFreeTransmittedPacket (pSess);
#else
        AphdlrFreeTransmittedPacket (pSess);
#endif
        pSess->u1Used = OSIX_FALSE;
        pSess->sessionId = 0;
        pSess->eCurrentState = CAPWAP_IDLE;

#ifdef WLC_WANTED
        pSess->PMTU = pWssIfCapwapDB->CapwapGetDB.u4PathMTU;
#else
        pSess->PMTU = CAPWAP_MTU_SIZE;
#endif

        pSess->lastProcesseddSeqNum = CAPWAP_MAX_SEQ_NUM;
        pSess->lastTransmitedSeqNum = CAPWAP_MAX_SEQ_NUM;
        /* remove memory allocated for the table */
        UtlDynSllShut (pSess->CapwapTxList, NULL);
        pSess->CapwapTxList = NULL;
        pSess->lastTransmittedPkt = NULL;
        pSess->lastTransmittedPktLen = 0;
        pSess->u4FileReadOffset = 0;
        pSess->u4ImgDataFileWriteOffset = 0;
        pSess->u1EndofFileReached = 0;
        pSess->u1ImageDataTimerRunning = OSIX_FALSE;
        pSess->ePrevState = CAPWAP_START;
        pSess->u1WriteFileOpened = OSIX_FALSE;
        pSess->u1ReadFileOpened = OSIX_FALSE;
        pSess->writeFd = NULL;
        pSess->readFd = NULL;
        pSess->readCrashFd = NULL;
        pSess->writeCrashFd = NULL;
        pSess->u1CrashEndofFileReached = OSIX_FALSE;
        pSess->u1CrashWriteEndofFileReached = OSIX_FALSE;
        pSess->u1CrashWriteFileOpened = OSIX_FALSE;
        pSess->i4CrashFileSize = 0;
        pSess->u1CrashReadFileOpened = OSIX_FALSE;
        pSess->u4CrashFileReadOffset = 0;
        pSess->u1CrashFileNameSet = OSIX_FALSE;
        pSess->u1DataTransferTimerRunning = OSIX_FALSE;
#ifdef WLC_WANTED
        WssIfSessionPortEntryDelete (u4IpAddr, (UINT2) u4DestPort);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
#endif
    }
    else
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   " Entry not present in the remote session module \r\n");
        return OSIX_FAILURE;
    }
    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfRemoteSessionGet                                  *
 *                                                                           *
 *  Description     : This function gets the entry in the                    *
 *                    Remote session Module.                                 *
 *                                                                           *
 *  Input(s)        : u4WtpSessionId - Wtp unique Session Id                 *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : NULL on failure                                        *
 *                    pWtpSessProfileMap on success                          *
 *****************************************************************************/
tRemoteSessionManager *
WssIfRemoteSessionGet (UINT4 u4IpAddr, UINT4 u4DestPort)
{
    WSSIF_FN_ENTRY ();

#ifdef WLC_WANTED
    tCapwapSessDestIpPortMapTable *pMapEntry = NULL;

    pMapEntry = WssIfSessionPortEntryGet (u4IpAddr, u4DestPort);
    if (pMapEntry == NULL)
    {
        WSSIF_TRC (WSSIF_MGMT_TRC,
                   "Entry not present in the Wtp session Profile Map Table \r\n");
        return NULL;
    }
    return (&gaRemoteSessionManager[pMapEntry->u2CapwapRSMIndex]);
#else
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (u4DestPort);
    return (&gaRemoteSessionManager[0]);
#endif
    WSSIF_FN_EXIT ();
}

/*****************************************************************************
 *  Function Name   : WssIfWtpProfileSessInit                                *
 *  Description     : This function used to initialize the Wtp session       *
 *                    profile RB Tree                                        *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfPortSessDBCreate ()
{
    gCpSessPortMapTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tCapwapSessDestIpPortMapTable,
                                nextSessionPortMap)),
                              WssIfCompareSessionPortRBTree);
    if (gCpSessPortMapTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfPortSessDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfPortSessDBDelete ()
{
    if (gCpSessPortMapTable != NULL)
    {
        RBTreeDelete (gCpSessPortMapTable);
        gCpSessPortMapTable = NULL;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfWtpProfileIdSessDBCreate (void)
{
    gCpSessProfileIdMapTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tCapwapSessProfileIdMapTable,
                                nextSessionProfileIdMap)),
                              WssIfCompareSessionProfileIdRBTree);
    if (gCpSessProfileIdMapTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpProfileIdSessDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfWtpProfileIdSessDBDelete ()
{
    if (gCpSessProfileIdMapTable != NULL)
    {
        RBTreeDelete (gCpSessProfileIdMapTable);
        gCpSessProfileIdMapTable = NULL;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfCapwapDtlsDBCreate (void)
{
    gCapDtlsMapTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tCapwapDtlsSessEntry, nextCapDtlsSessIdMap)),
                              WssIfCompareCapwapDtlsRBTree);
    if (gCapDtlsMapTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpProfileIdSessDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfCapwapDtlsDBDelete ()
{
    if (gCapDtlsMapTable != NULL)
    {
        RBTreeDelete (gCapDtlsMapTable);
        gCapDtlsMapTable = NULL;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfWtpProfileDBCreate (void)
{
    gCapWtpProfileTable = RBTreeCreateEmbedded ((FSAP_OFFSETOF (tWssIfProfile,
                                                                nextProfile)),
                                                WssIfCompareWtpProfileRBTree);
    if (gCapWtpProfileTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfProfileDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfWtpProfildIdDBCreate (void)
{
    gCapProfildIdMapTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssIfProfileIdMap, nextProfileIdMap)),
                              WssIfCompareWtpProfildIdRBTree);
    if (gCapProfildIdMapTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfProfildIdDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfWtpProfildIdDBDelete ()
{
    if (gCapProfildIdMapTable != NULL)
    {
        RBTreeDelete (gCapProfildIdMapTable);
        gCapProfildIdMapTable = NULL;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpModelTableDBCreate                                *
 *  Description     : This function used to initialize the Wtp Model Table     *
 *                     RB Tree                                               *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/

INT4
WssIfWtpBindingTableDBCreate (void)
{
    gCapBindingTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tCapwapBindingTable, nextBindingTableEntry)),
                              WssIfCompareWtpBindingTableRBTree);
    if (gCapBindingTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfModelTableDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpModelTableDBGet                                *
 *  Description     : This function used to get the entry from wtp model
            table RB tree                             *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/

INT4
WssIfWtpBindingTableDBGet (UINT1 *pu1ModelNumber, UINT1 u1RadioId,
                           UINT2 u2WlanProfileId, INT4 *pi4RowStatus)
{
    tCapwapBindingTable *pCapwapBindingTable = NULL;
    tCapwapBindingTable CapwapBindingTable;
    MEMSET (&CapwapBindingTable, 0, sizeof (tCapwapBindingTable));
    MEMCPY (CapwapBindingTable.au1ModelNumber, pu1ModelNumber,
            MAX_WTP_MODELNUMBER_LEN);
    CapwapBindingTable.u1RadioId = u1RadioId;
    CapwapBindingTable.u2WlanProfileId = u2WlanProfileId;
    pCapwapBindingTable =
        ((tCapwapBindingTable *)
         RBTreeGet (gCapBindingTable, (tRBElem *) & CapwapBindingTable));
    if (pCapwapBindingTable == NULL)
    {
        WSSIF_TRC (WSSIF_MGMT_TRC,
                   "WssIfWtpModelTableDBGet:Entry is not present in Table \r\n");
        return OSIX_FAILURE;
    }
    *pi4RowStatus = (INT4) pCapwapBindingTable->u1RowStatus;
    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : CapwapGetFirstFsWlanBindingTable                               *
 *  Description     : This function used to get the entry from wtp model
                        table RB tree                                        *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/

tCapwapBindingTable *
CapwapGetFirstFsWlanBindingTable ()
{
    tCapwapBindingTable *pCapwapBindingTable = NULL;
    pCapwapBindingTable =
        (tCapwapBindingTable *) RBTreeGetFirst (gCapBindingTable);
    return pCapwapBindingTable;
}

/*****************************************************************************
 *  Function Name   : CapwapGetNextFsWlanBindingTable                       *
 *  Description     : This function used to get the entry from wtp model
                        table RB tree                                        *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/

tCapwapBindingTable *
CapwapGetNextFsWlanBindingTable (tCapwapBindingTable * pCapwapBindingTable)
{
    tCapwapBindingTable *pCapwapBindingTableNext = NULL;
    pCapwapBindingTableNext =
        (tCapwapBindingTable *) RBTreeGetNext (gCapBindingTable,
                                               (tRBElem *)
                                               pCapwapBindingTable, NULL);
    return pCapwapBindingTableNext;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpModelTableDBAdd                                *
 *  Description     : This function used to add a new entry to the wtp
              model table RB tree                         *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/

INT4
WssIfWtpBindingTableDBAdd (UINT1 *pu1ModelNumber, UINT1 u1RadioId,
                           UINT2 u2WlanProfileId, UINT1 u1RowStatus)
{
    tCapwapBindingTable *pCapwapBindingTable = NULL;
    pCapwapBindingTable =
        (tCapwapBindingTable *) MemAllocMemBlk (CAPWAP_MODEL_TABLE_DB_POOLID);
    if (pCapwapBindingTable == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pCapwapBindingTable, 0, sizeof (tCapwapBindingTable));
    MEMCPY (pCapwapBindingTable->au1ModelNumber, pu1ModelNumber,
            MAX_WTP_MODELNUMBER_LEN);
    pCapwapBindingTable->u1RadioId = u1RadioId;
    pCapwapBindingTable->u2WlanProfileId = u2WlanProfileId;
    pCapwapBindingTable->u1RowStatus = u1RowStatus;
    if (RBTreeAdd (gCapBindingTable, pCapwapBindingTable) != RB_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpModelTableDBSet: Failed to add entry to "
                   "the Table\r\n");
        MemReleaseMemBlock (CAPWAP_MODEL_TABLE_DB_POOLID,
                            (UINT1 *) pCapwapBindingTable);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpProfileSessInit                                *
 *  Description     : This function used to update an entry in wtp model
              table RB tree                         *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/

INT4
WssIfWtpBindingTableDBSet (UINT1 *pu1ModelNumber, UINT1 u1RadioId,
                           UINT2 u2WlanProfileId, UINT1 u1RowStatus)
{
    tCapwapBindingTable CapwapBindingTable;
    tCapwapBindingTable *pCapwapBindingTable = NULL;
    MEMSET (&CapwapBindingTable, 0, sizeof (tCapwapBindingTable));
    MEMCPY (CapwapBindingTable.au1ModelNumber, pu1ModelNumber,
            MAX_WTP_MODELNUMBER_LEN);
    CapwapBindingTable.u1RadioId = u1RadioId;
    CapwapBindingTable.u2WlanProfileId = u2WlanProfileId;
    pCapwapBindingTable =
        ((tCapwapBindingTable *)
         RBTreeGet (gCapBindingTable, (tRBElem *) & CapwapBindingTable));
    if (pCapwapBindingTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpModelTableDBSet: Failed to add entry to "
                   "the Table\r\n");
        return OSIX_FAILURE;
    }
    pCapwapBindingTable->u1RowStatus = u1RowStatus;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpModelTableDBDelete                             *
 *  Description     : This function used to delete an entry from wtp model
              table RB tree                         *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/

INT4
WssIfWtpBindingTableDBDelete (UINT1 *pu1ModelNumber, UINT1 u1RadioId,
                              UINT2 u2WlanProfileId)
{
    tCapwapBindingTable CapwapBindingTable;
    tCapwapBindingTable *pCapwapBindingTable = NULL;
    MEMSET (&CapwapBindingTable, 0, sizeof (tCapwapBindingTable));
    MEMCPY (CapwapBindingTable.au1ModelNumber, pu1ModelNumber,
            MAX_WTP_MODELNUMBER_LEN);
    CapwapBindingTable.u1RadioId = u1RadioId;
    CapwapBindingTable.u2WlanProfileId = u2WlanProfileId;
    pCapwapBindingTable =
        ((tCapwapBindingTable *)
         RBTreeGet (gCapBindingTable, (tRBElem *) & CapwapBindingTable));

    if (pCapwapBindingTable != NULL)
    {
        RBTreeRem (gCapBindingTable, (tRBElem *) pCapwapBindingTable);
        MemReleaseMemBlock (CAPWAP_MODEL_TABLE_DB_POOLID,
                            (UINT1 *) pCapwapBindingTable);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

INT4
WssIfWtpProfileMacMapDBCreate (void)
{
    gCapProfileMacMapTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssIfProfileMacMap, nextProfileMacMap)),
                              WssIfCompareWtpProfileMacMapRBTree);
    if (gCapProfileMacMapTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfCompareWtpProfileMacMapRBTree: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfWtpProfileMacMapDBDelete ()
{
    if (gCapProfileMacMapTable != NULL)
    {
        RBTreeDelete (gCapProfileMacMapTable);
        gCapProfileMacMapTable = NULL;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfWtpProfileNameMapDBCreate (void)
{
    gCapProfileNameMapTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssIfProfileNameMap, nextProfileNameMap)),
                              WssIfCompareWtpProfileNameMapRBTree);
    if (gCapProfileNameMapTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfCompareWtpProfileNameMapRBTree: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfWtpProfileNameMapDBDelete ()
{
    if (gCapProfileNameMapTable != NULL)
    {
        RBTreeDelete (gCapProfileNameMapTable);
        gCapProfileNameMapTable = NULL;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfWtpModelDBCreate (void)
{
    gCapWtpModelMapTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssIfWtpModelMap, nextWtpModelEntry)),
                              WssIfCompareWtpModelRBTree);
    if (gCapWtpModelMapTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpModelDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfWtpModelDBDelete ()
{
    if (gCapWtpModelMapTable != NULL)
    {
        RBTreeDelete (gCapWtpModelMapTable);
        gCapWtpModelMapTable = NULL;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfWlcWhiteListDBCreate (void)
{
    gCapWlcWhiteListTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssIfWlcWhiteListDB, nextWlcMacEntry)),
                              WssIfCompareWlcWhiteListRBTree);
    if (gCapWlcWhiteListTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWlcWhiteListDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfWlcWhiteListDBDelete ()
{
    if (gCapWlcWhiteListTable != NULL)
    {
        RBTreeDelete (gCapWlcWhiteListTable);
        gCapWlcWhiteListTable = NULL;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfStaWhiteListDBCreate (void)
{
    gCapStaWhiteListTable = RBTreeCreateEmbedded ((FSAP_OFFSETOF
                                                   (tWssIfStaWhiteListDB,
                                                    nextStaMacEntry)),
                                                  WssIfCompareStaWhiteListRBTree);
    if (gCapStaWhiteListTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfStaWhiteListDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfStaBlackListDBCreate (void)
{
    gCapStaBlackListTable = RBTreeCreateEmbedded ((FSAP_OFFSETOF
                                                   (tWssIfStaBlackListDB,
                                                    nextStaMacEntry)),
                                                  WssIfCompareStaBlackListRBTree);
    if (gCapStaBlackListTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfStaBlackListDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfStaWhiteListDBDelete ()
{
    if (gCapStaWhiteListTable != NULL)
    {
        RBTreeDelete (gCapStaWhiteListTable);
        gCapStaWhiteListTable = NULL;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfStaBlackListDBDelete ()
{
    if (gCapStaBlackListTable != NULL)
    {
        RBTreeDelete (gCapStaBlackListTable);
        gCapStaBlackListTable = NULL;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfWlcBlackListDBCreate (void)
{
    gCapWlcBlackListTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssIfWlcBlackListDB, nextWlcMacEntry)),
                              WssIfCompareWlcBlackListRBTree);
    if (gCapWlcBlackListTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWlcBlackListDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfWlcBlackListDBDelete ()
{
    if (gCapWlcBlackListTable != NULL)
    {
        RBTreeDelete (gCapWlcBlackListTable);
        gCapWlcBlackListTable = NULL;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfCapDscpMapDBCreate                                *
 *  Description     : This function used to create priority to dscp map      *
 *                    RB Tree                                                *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfCapDscpMapDBCreate ()
{
    gCapDscpMapTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tCapwapDscpMapTable,
                                nextDscpValueMap)), WssIfCompareDscpMapRBTree);
    if (gCapDscpMapTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfCapDscpMapDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfCapDscpMapDBDelete                                *
 *  Description     : This function used to delete priority to dscp map      *
 *                    RB Tree                                                *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfCapDscpMapDBDelete ()
{
    if (gCapDscpMapTable != NULL)
    {
        RBTreeDelete (gCapDscpMapTable);
        gCapDscpMapTable = NULL;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfSessionPortEntryCreate                         *
 *  Description     : This function creates the entry in the                 *
 *                    Wtp Profile Session Table.                             *
 *  Input(s)        : u4WtpSessionId - WTP unique session id                 *
 *                    u2IntProfileId - Internal Profile Id                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfSessionPortEntryCreate (UINT4 u4DestIp, UINT4 u4DestPort,
                             UINT2 u2CapwapInternalId)
{
    tCapwapSessDestIpPortMapTable *pWtpSessProfileMap = NULL;

    WSSIF_FN_ENTRY ();
    /* Add entry with the incoming IP address to the RB tree */
    pWtpSessProfileMap =
        (tCapwapSessDestIpPortMapTable *)
        MemAllocMemBlk (CAPWAP_WTPSESSPORT_POOLID);

    if (pWtpSessProfileMap == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "capwapProcessCpCtrlRxMsg:Memory allocation Failure \r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pWtpSessProfileMap, 0, sizeof (tCapwapSessDestIpPortMapTable));

    pWtpSessProfileMap->DestIp.u4_addr[0] = u4DestIp;
    pWtpSessProfileMap->u4DestPort = u4DestPort;
    pWtpSessProfileMap->u2CapwapRSMIndex = u2CapwapInternalId;

    if (RBTreeAdd (gCpSessPortMapTable, pWtpSessProfileMap) != RB_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfAddSessProfileMapEntry: Failed to add entry to the RB Tree \r\n");
        MemReleaseMemBlock (CAPWAP_WTPSESSPORT_POOLID,
                            (UINT1 *) pWtpSessProfileMap);
        return OSIX_FAILURE;
    }
    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

INT4
WssIfSessionProfileIdEntryCreate (UINT2 u2CapwapRSMId, UINT2 u2CapwapInternalId)
{
    tCapwapSessProfileIdMapTable *pSessProfileIdMap = NULL;
    tCapwapSessProfileIdMapTable SessProfileIdEntry;

    MEMSET (&SessProfileIdEntry, 0, sizeof (tCapwapSessProfileIdMapTable));
    SessProfileIdEntry.u2WtpProfileInternalId = u2CapwapInternalId;

    pSessProfileIdMap =
        ((tCapwapSessProfileIdMapTable *)
         RBTreeGet (gCpSessProfileIdMapTable,
                    (tRBElem *) & SessProfileIdEntry));
    if (pSessProfileIdMap != NULL)
    {
        pSessProfileIdMap->u2CapwapRSMIndex = u2CapwapRSMId;
        pSessProfileIdMap->u2WtpProfileInternalId = u2CapwapInternalId;
        return OSIX_SUCCESS;
    }

    /* Add entry with the incoming IP address to the RB tree */
    pSessProfileIdMap =
        (tCapwapSessProfileIdMapTable *)
        MemAllocMemBlk (CAPWAP_WTPSESSPROFILE_POOLID);

    if (pSessProfileIdMap == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfSessionProfileIdEntryCreate:Memory allocation Failure \r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pSessProfileIdMap, 0, sizeof (tCapwapSessProfileIdMapTable));

    pSessProfileIdMap->u2CapwapRSMIndex = u2CapwapRSMId;
    pSessProfileIdMap->u2WtpProfileInternalId = u2CapwapInternalId;

    if (RBTreeAdd (gCpSessProfileIdMapTable, pSessProfileIdMap) != RB_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfSessionProfileIdEntryCreate: Failed to add entry to the RB Tree \r\n");
        MemReleaseMemBlock (CAPWAP_WTPSESSPROFILE_POOLID,
                            (UINT1 *) pSessProfileIdMap);
        return OSIX_FAILURE;
    }

#ifdef WLC_WANTED
    tRemoteSessionManager *pSessEntry = NULL;
    pSessEntry = WssIfGetRsmFromProfileId (pSessProfileIdMap);
    pSessEntry->PMTU = gWssCapDB.CapwapGetDB[u2CapwapInternalId].u4PathMTU;
#endif
    return OSIX_SUCCESS;
}

INT4
WssIfCapwapDtlsEntryCreate (UINT4 u4IpAddr, UINT4 u4DtlsId, tMacAddr MacAddr)
{
    tCapwapDtlsSessEntry *pCapDtlsMap = NULL;
    tCapwapDtlsSessEntry *pGetCapDtlsMap = NULL;
    tCapwapDtlsSessEntry GetCapDtlsMap;

    MEMSET (&GetCapDtlsMap, 0, sizeof (tCapwapDtlsSessEntry));
    GetCapDtlsMap.u4IpAddr = u4IpAddr;

    pGetCapDtlsMap = (tCapwapDtlsSessEntry *) RBTreeGet
        (gCapDtlsMapTable, (tRBElem *) & GetCapDtlsMap);

    if (pGetCapDtlsMap != NULL)
    {
        pGetCapDtlsMap->u4DtlsId = u4DtlsId;
        MEMCPY (pGetCapDtlsMap->MacAddress, MacAddr, 6);
        return OSIX_SUCCESS;
    }

    /* Add entry with the incoming IP address to the RB tree */
    pCapDtlsMap =
        (tCapwapDtlsSessEntry *) MemAllocMemBlk (CAPWAP_DTLSSESS_POOLID);

    if (pCapDtlsMap == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfCapwapDtlsEntryCreate:Memory allocation Failure \r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pCapDtlsMap, 0, sizeof (tCapwapDtlsSessEntry));

    pCapDtlsMap->u4IpAddr = u4IpAddr;
    pCapDtlsMap->u4DtlsId = u4DtlsId;
    MEMCPY (pCapDtlsMap->MacAddress, MacAddr, 6);
    if (RBTreeAdd (gCapDtlsMapTable, pCapDtlsMap) != RB_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfCapwapDtlsEntryCreate: Failed to add entry to the DTLS Temp RB Tree \r\n");
        MemReleaseMemBlock (CAPWAP_DTLSSESS_POOLID, (UINT1 *) pCapDtlsMap);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfWtpProfildIdEntryCreate (UINT4 u4WtpProfildId, UINT2 u2WtpIntProfildId)
{
    tWssIfProfileIdMap *pProfildIdMap = NULL;

    /* Add entry with the incoming IP address to the RB tree */
    pProfildIdMap =
        (tWssIfProfileIdMap *) MemAllocMemBlk (CAPWAP_DTLSSESS_POOLID);

    if (pProfildIdMap == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfProfildIdEntryCreate:Memory allocation Failure \r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pProfildIdMap, 0, sizeof (tWssIfProfileIdMap));

    pProfildIdMap->u4WtpProfildId = u4WtpProfildId;
    pProfildIdMap->u2WtpProfileInternalId = u2WtpIntProfildId;
    if (RBTreeAdd (gCapProfildIdMapTable, pProfildIdMap) != RB_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfProfildIdEntryCreate: Failed to add entry to the Profild ID  RB Tree \r\n");
        MemReleaseMemBlock (CAPWAP_DTLSSESS_POOLID, (UINT1 *) pProfildIdMap);

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
WssIfWtpModelEntryCreate (UINT1 *pWtpModelName, UINT2 u2ModelIndex)
{
    tWssIfWtpModelMap  *pWtpModelMap = NULL;

    /* Add entry with the incoming IP address to the RB tree */
    pWtpModelMap =
        (tWssIfWtpModelMap *) MemAllocMemBlk (CAPWAP_WTPMODEL_POOLID);

    if (pWtpModelMap == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpModelEntryCreate:Memory allocation Failure \r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pWtpModelMap, 0, sizeof (tWssIfWtpModelMap));

    /* u2Len = STRLEN (pWtpModelName); */

    pWtpModelMap->u2ModelIndex = (UINT1) u2ModelIndex;
    MEMCPY (pWtpModelMap->au1ModelNumber, pWtpModelName,
            MAX_WTP_MODELNUMBER_LEN);
    if (RBTreeAdd (gCapWtpModelMapTable, pWtpModelMap) != RB_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfProfildIdEntryCreate: Failed to add entry to the Profild ID  RB Tree \r\n");
        MemReleaseMemBlock (CAPWAP_WTPMODEL_POOLID, (UINT1 *) pWtpModelMap);

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

#ifdef WLC_WANTED
/*****************************************************************************
 *  Function Name   : WssIfCapwapDscpMapEntryCreate                          *
 *  Description     : This function creates the entry in the                 *
 *                    Dscp Map Entry Table.                                  *
 *  Input(s)        : u4IfIndex - Interface index                            *
 *                    u1InPriority - priority value                          *
 *                    u1OutDscp - Dscp value                                 *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfCapwapDscpMapEntryCreate (tCapwapDscpMapTable * pCapDscpMap)
{
    tCapwapDscpMapTable *pCapDscpMapEntry = NULL;
    WSSIF_FN_ENTRY ();
    /* Add entry with the incoming Index to the RB tree */
    pCapDscpMapEntry =
        (tCapwapDscpMapTable *) MemAllocMemBlk (CAPWAP_DSCP_MAP_DB_POOLID);

    if (pCapDscpMapEntry == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfCapwapDscpMapEntryCreate:Memory allocation Failure \r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pCapDscpMapEntry, 0, sizeof (tCapwapDscpMapTable));

    pCapDscpMapEntry->u4IfIndex = pCapDscpMap->u4IfIndex;
    pCapDscpMapEntry->u2WlanProfileId = pCapDscpMap->u2WlanProfileId;
    pCapDscpMapEntry->u1InPriority = pCapDscpMap->u1InPriority;
    pCapDscpMapEntry->u1OutDscp = pCapDscpMap->u1OutDscp;
    pCapDscpMapEntry->i4RowStatus = pCapDscpMap->i4RowStatus;

    if (RBTreeAdd (gCapDscpMapTable, pCapDscpMapEntry) != RB_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfCapwapDscpMapEntryCreate: Failed to add entry to the RB Tree \r\n");
        MemReleaseMemBlock (CAPWAP_DSCP_MAP_DB_POOLID,
                            (UINT1 *) pCapDscpMapEntry);
        return OSIX_FAILURE;
    }
    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}
#endif

#ifdef WTP_WANTED
/*****************************************************************************
 *  Function Name   : WssIfCapwapDscpMapEntryCreate                          *
 *  Description     : This function creates the entry in the                 *
 *                    Dscp Map Entry Table.                                  *
 *  Input(s)        : u4IfIndex - Interface index                            *
 *                    u1InPriority - priority value                          *
 *                    u1OutDscp - Dscp value                                 *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfCapwapDscpMapEntryCreate (UINT4 u4IfIndex, UINT1 u1InPriority,
                               UINT1 u1OutDscp)
{
#if defined (LNXIP4_WANTED)
    tCapwapDscpMapTable *pCapDscpMap = NULL;
    tCapwapDscpMapTable *pTmpCapDscpMap = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    WSSIF_FN_ENTRY ();
    /* Add entry with the incoming Index to the RB tree */

    pTmpCapDscpMap = WssIfCapwapDscpMapEntryGet (u4IfIndex, u1InPriority);

    if (pTmpCapDscpMap == NULL)
    {
        pCapDscpMap =
            (tCapwapDscpMapTable *) MemAllocMemBlk (CAPWAP_DSCP_MAP_DB_POOLID);

        if (pCapDscpMap == NULL)
        {
            WSSIF_TRC (WSSIF_FAILURE_TRC,
                       "WssIfCapwapDscpMapEntryCreate:Memory allocation Failure \r\n");
            return OSIX_FAILURE;
        }
        MEMSET (pCapDscpMap, 0, sizeof (tCapwapDscpMapTable));

        SNPRINTF ((CHR1 *) au1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s",
                  CfaGddGetLnxIntfnameForPort ((UINT2) u4IfIndex));

        MEMCPY (pCapDscpMap->au1InterfaceName, au1IfName,
                CFA_MAX_PORT_NAME_LENGTH);
        pCapDscpMap->u1InPriority = u1InPriority;
        pCapDscpMap->u1OutDscp = u1OutDscp;

        if (RBTreeAdd (gCapDscpMapTable, pCapDscpMap) != RB_SUCCESS)
        {
            WSSIF_TRC (WSSIF_FAILURE_TRC,
                       "WssIfCapwapDscpMapEntryCreate: Failed to add entry to the RB Tree \r\n");
            MemReleaseMemBlock (CAPWAP_DSCP_MAP_DB_POOLID,
                                (UINT1 *) pCapDscpMap);
            return OSIX_FAILURE;
        }
#ifdef KERNEL_CAPWAP_WANTED
#ifdef WLC_WANTED
        if (WssQosUpdateKernelDB (pCapDscpMap) == OSIX_FAILURE)
        {
            WSSIF_TRC (WSSIF_FAILURE_TRC,
                       "WssIfCapwapDscpMapEntryCreate: Kernel updation failed\r\n");
        }
#endif
#endif
    }
    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1InPriority);
    UNUSED_PARAM (u1OutDscp);
    return OSIX_SUCCESS;
#endif
}
#endif

/*****************************************************************************
 *  Function Name   : WssIfSessionPortEntryDelete                        *
 *  Description     : This function deletes the entry in the                 *
 *                    Wtp Profile Session Table.                             *
 *  Input(s)        : u4WtpSessionId - Wtp unique Session Id                 *
 *                    u2IntProfileId - Internal Profile Id                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfSessionPortEntryDelete (UINT4 u4IpAddr, UINT2 u4DestPort)
{
    tCapwapSessDestIpPortMapTable *pWtpSessProfileMap = NULL;

    WSSIF_FN_ENTRY ();

    /* Get the RBTree Node for the particular indices */
    pWtpSessProfileMap = WssIfSessionPortEntryGet (u4IpAddr, u4DestPort);
    if (pWtpSessProfileMap == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfSessionPortEntryGet:The given entry is not found \r\n");
        return OSIX_FAILURE;
    }

    /* Remove the node if it is available */
    RBTreeRem (gCpSessPortMapTable, (tRBElem *) pWtpSessProfileMap);
    MemReleaseMemBlock (CAPWAP_WTPSESSPORT_POOLID,
                        (UINT1 *) pWtpSessProfileMap);

    WSSIF_FN_EXIT ();

    return OSIX_SUCCESS;
}

INT4
WssIfSessionProfileIdEntryDelete (UINT2 u2CapwapInternalId)
{
    tCapwapSessProfileIdMapTable *pSessProfilIdeMap = NULL;

    WSSIF_FN_ENTRY ();

    /* Get the RBTree Node for the particular indices */
    pSessProfilIdeMap = WssIfSessionProfileIdEntryGet (u2CapwapInternalId);
    if (pSessProfilIdeMap == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfSessionPortEntryGet:The given entry is not found \r\n");
        return OSIX_FAILURE;
    }
    /* Remove the node if it is available */
    RBTreeRem (gCpSessProfileIdMapTable, (tRBElem *) pSessProfilIdeMap);
    MemReleaseMemBlock (CAPWAP_WTPSESSPROFILE_POOLID,
                        (UINT1 *) pSessProfilIdeMap);

    WSSIF_FN_EXIT ();

    return OSIX_SUCCESS;
}

INT4
WssIfCapwapDtlsEntryDelete (UINT4 u4IpAddr, UINT4 u4DtlsId)
{
    tCapwapDtlsSessEntry *pCapwapDtlsSessEntry = NULL;
    UNUSED_PARAM (u4DtlsId);

    WSSIF_FN_ENTRY ();

    /* Get the RBTree Node for the particular indices */
    pCapwapDtlsSessEntry = WssIfCapwapDtlsEntryGet (u4IpAddr);
    if (pCapwapDtlsSessEntry == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfCapwapDtlsEntryDelete:The given entry is not found \r\n");
        return OSIX_FAILURE;
    }
    /* Remove the node if it is available */
    RBTreeRem (gCapDtlsMapTable, (tRBElem *) pCapwapDtlsSessEntry);
    MemReleaseMemBlock (CAPWAP_DTLSSESS_POOLID, (UINT1 *) pCapwapDtlsSessEntry);

    WSSIF_FN_EXIT ();

    return OSIX_SUCCESS;
}

INT4
WssIfWtpProfildIdEntryDelete (UINT4 u4WtpProfildId)
{
    tWssIfProfileIdMap *pWtpProfildMap = NULL;

    WSSIF_FN_ENTRY ();

    /* Get the RBTree Node for the particular indices */
    pWtpProfildMap = WssIfWtpProfileIdEntryGet (u4WtpProfildId);
    if (pWtpProfildMap == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfCapwapProfileIdEntryDelete:The given entry is not found \r\n");
        return OSIX_FAILURE;
    }
    /* Remove the node if it is available */
    RBTreeRem (gCapProfildIdMapTable, (tRBElem *) pWtpProfildMap);
    MemReleaseMemBlock (CAPWAP_DTLSSESS_POOLID, (UINT1 *) pWtpProfildMap);

    WSSIF_FN_EXIT ();

    return OSIX_SUCCESS;
}

INT4
WssIfWtpModelEntryDelete (UINT1 *pWtpModelName)
{
    tWssIfWtpModelMap  *pWtpModelMap = NULL;

    WSSIF_FN_ENTRY ();

    /* Get the RBTree Node for the particular indices */
    pWtpModelMap = WssIfWtpModelEntryGet (pWtpModelName);
    if (pWtpModelMap == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpModelEntryDelete:The given entry is not found \r\n");
        return OSIX_FAILURE;
    }
    /* Remove the node if it is available */
    RBTreeRem (gCapWtpModelMapTable, (tRBElem *) pWtpModelMap);
    MemReleaseMemBlock (CAPWAP_WTPMODEL_POOLID, (UINT1 *) pWtpModelMap);

    WSSIF_FN_EXIT ();

    return OSIX_SUCCESS;
}

#ifdef WLC_WANTED
/*****************************************************************************
 *  Function Name   : WssIfCapwapDscpMapEntryDelete                          *
 *  Description     : This function deletes the entry in the                 *
 *                    Dscp Map Table.                                        *
 *  Input(s)        : u4IfIndex - Interface index                            *
 *                    u1InPriority - Priority value                          *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfCapwapDscpMapEntryDelete (tCapwapDscpMapTable * pCapDscpMapEntry)
{
    tCapwapDscpMapTable *pCapDscpMap = NULL;

    WSSIF_FN_ENTRY ();

    /* Get the RBTree Node for the particular indices */
    pCapDscpMap =
        (tCapwapDscpMapTable *) RBTreeGet (gCapDscpMapTable, pCapDscpMapEntry);
    if (pCapDscpMap == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfCapwapDscpMapEntryDelete:The given entry is not found \r\n");
        return OSIX_FAILURE;
    }

    /* Remove the node if it is available */
    RBTreeRem (gCapDscpMapTable, (tRBElem *) pCapDscpMap);

    WSSIF_FN_EXIT ();

    return OSIX_SUCCESS;
}
#endif

#ifdef WTP_WANTED
/*****************************************************************************
 *  Function Name   : WssIfCapwapDscpMapEntryDelete                          *
 *  Description     : This function deletes the entry in the                 *
 *                    Dscp Map Table.                                        *
 *  Input(s)        : u4IfIndex - Interface index                            *
 *                    u1InPriority - Priority value                          *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfCapwapDscpMapEntryDelete (UINT4 u4IfIndex, UINT1 u1InPriority)
{
    tCapwapDscpMapTable *pCapDscpMap = NULL;

    WSSIF_FN_ENTRY ();

    /* Get the RBTree Node for the particular indices */
    pCapDscpMap = WssIfCapwapDscpMapEntryGet (u4IfIndex, u1InPriority);
    if (pCapDscpMap == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfCapwapDscpMapEntryDelete:The given entry is not found \r\n");
        return OSIX_FAILURE;
    }

    /* Remove the node if it is available */
    RBTreeRem (gCapDscpMapTable, (tRBElem *) pCapDscpMap);

#ifdef KERNEL_CAPWAP_WANTED
#ifdef WLC_WANTED
    if (WssQosRemoveKernelDB (pCapDscpMap) == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfCapwapDscpMapEntryDelete: Kernel Remove failed\r\n");
    }
#endif
#endif
    WSSIF_FN_EXIT ();

    return OSIX_SUCCESS;
}
#endif

/*****************************************************************************
 *  Function Name   : WssIfSessionPortEntryGet                            *
 *  Description     : This function gets the entry in the                    *
 *                    Wtp Profile Session Table.                             *
 *  Input(s)        : u4WtpSessionId - Wtp unique Session Id                 *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : NULL on failure                                        *
 *                    pWtpSessProfileMap on success                          *
 *****************************************************************************/

tCapwapSessDestIpPortMapTable *
WssIfSessionPortEntryGet (UINT4 u4IpAddr, UINT4 u4DestPort)
{
    tCapwapSessDestIpPortMapTable *pWtpSessProfileEntry = NULL;
    tCapwapSessDestIpPortMapTable wtpSessProfileEntry;

    WSSIF_FN_ENTRY ();
    MEMSET (&wtpSessProfileEntry, 0, sizeof (tCapwapSessDestIpPortMapTable));

    wtpSessProfileEntry.DestIp.u4_addr[0] = u4IpAddr;
    wtpSessProfileEntry.u4DestPort = u4DestPort;

    pWtpSessProfileEntry =
        ((tCapwapSessDestIpPortMapTable *)
         RBTreeGet (gCpSessPortMapTable, (tRBElem *) & wtpSessProfileEntry));

    if (pWtpSessProfileEntry == NULL)
    {
        /* Possible in case of Plain Text Join Request message */
        WSSIF_TRC (WSSIF_MGMT_TRC,
                   "WssIfSessionPortEntryGet:Entry is not present in Profile Map Table \r\n");
        return NULL;
    }
    WSSIF_FN_EXIT ();
    return pWtpSessProfileEntry;

}

tCapwapSessProfileIdMapTable *
WssIfSessionProfileIdEntryGet (UINT2 u2CapwapInternalId)
{
    tCapwapSessProfileIdMapTable *pSessProfileIdEntry = NULL;
    tCapwapSessProfileIdMapTable SessProfileIdEntry;

    WSSIF_FN_ENTRY ();
    MEMSET (&SessProfileIdEntry, 0, sizeof (tCapwapSessProfileIdMapTable));
    SessProfileIdEntry.u2WtpProfileInternalId = u2CapwapInternalId;

    pSessProfileIdEntry =
        ((tCapwapSessProfileIdMapTable *)
         RBTreeGet (gCpSessProfileIdMapTable,
                    (tRBElem *) & SessProfileIdEntry));

    if (pSessProfileIdEntry == NULL)
    {
        /* Possible in case of Plain Text Join Request message */
        WSSIF_TRC (WSSIF_MGMT_TRC,
                   "WssIfSessionProfileIdEntryGet:Entry is not present in Profile Map Table \r\n");
        return NULL;
    }
    WSSIF_FN_EXIT ();
    return pSessProfileIdEntry;

}

#ifdef WLC_WANTED
/*****************************************************************************
 *  Function Name   : WssIfCapwapDscpMapEntryGet                             *
 *  Description     : This function gets the entry in the                    *
 *                    Capwap Dscp Map Table.                                 *
 *  Input(s)        : u4IfIndex  - Interface index                           *
 *                    u1InPriority - Priority value                          *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : NULL on failure                                        *
 *                    pCapDscpMap on success                                 *
 *****************************************************************************/

INT4
WssIfCapwapDscpMapEntryGet (tCapwapDscpMapTable * pCapDscpMapEntry)
{
    tCapwapDscpMapTable *pCapDscpMap = NULL;

    WSSIF_FN_ENTRY ();

    pCapDscpMap = ((tCapwapDscpMapTable *)
                   RBTreeGet (gCapDscpMapTable, (tRBElem *) pCapDscpMapEntry));

    if (pCapDscpMap == NULL)
    {
        /* Possible in case of Plain Text Join Request message */
        WSSIF_TRC (WSSIF_MGMT_TRC,
                   "WssIfCapwapDscpMapEntryGet:Entry is not present in Dscp Map Table \r\n");
        return OSIX_FAILURE;
    }
    pCapDscpMapEntry->u1InPriority = pCapDscpMap->u1InPriority;
    pCapDscpMapEntry->u1OutDscp = pCapDscpMap->u1OutDscp;
    pCapDscpMapEntry->i4RowStatus = pCapDscpMap->i4RowStatus;
    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfCapwapDscpMapEntrySet                             *
 *  Description     : This function gets the entry in the                    *
 *                    Capwap Dscp Map Table.                                 *
 *  Input(s)        : u4IfIndex  - Interface index                           *
 *                    u1InPriority - Priority value                          *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : NULL on failure                                        *
 *                    pCapDscpMap on success                                 *
 *****************************************************************************/

INT4
WssIfCapwapDscpMapEntrySet (tCapwapDscpMapTable * pCapDscpMapEntry)
{
    tCapwapDscpMapTable *pCapDscpMap = NULL;

    WSSIF_FN_ENTRY ();

    pCapDscpMap =
        ((tCapwapDscpMapTable *)
         RBTreeGet (gCapDscpMapTable, (tRBElem *) pCapDscpMapEntry));

    if (pCapDscpMap == NULL)
    {
        /* Possible in case of Plain Text Join Request message */
        WSSIF_TRC (WSSIF_MGMT_TRC,
                   "WssIfCapwapDscpMapEntrySet:Entry is not present in Dscp Map Table \r\n");
        return OSIX_FAILURE;
    }
    pCapDscpMap->u1InPriority = pCapDscpMapEntry->u1InPriority;
    pCapDscpMap->u1OutDscp = pCapDscpMapEntry->u1OutDscp;
    pCapDscpMap->i4RowStatus = pCapDscpMapEntry->i4RowStatus;
    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfDscpConfigTableGetFirstEntry                      *
 *  Description     : This function used to get first entry of the NAT Config*
                      Entry table RB tree                                    *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : pu4BaseProfileId, pi4EntryIndex                        *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfDscpConfigTableGetFirstEntry (INT4 *pi4IfIndex,
                                   UINT4 *pu4CapwapDot11WlanProfileId)
{
    tCapwapDscpMapTable *pCapDscpMap = NULL;

    pCapDscpMap = (tCapwapDscpMapTable *) RBTreeGetFirst (gCapDscpMapTable);

    if (pCapDscpMap == NULL)
    {
        return OSIX_FAILURE;
    }
    *pi4IfIndex = (INT4) pCapDscpMap->u4IfIndex;
    *pu4CapwapDot11WlanProfileId = (UINT4) pCapDscpMap->u2WlanProfileId;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfDscpConfigTableGetNextEntry                       *
 **  Description     : This function used to get next entry of the current    *
                       entry whose Entry Index is given,  from NAT Config     *
 *                     table RB tree                                          *
 **  Input(s)        : pi4NATConfigEntryIndex,pu4WtpProfileId                 *
 **  Output(s)       : pu4NextWtpProfileId. pi4NextNATConfigEntryIndex        *
 **  Returns         : OSIX_SUCCESS on success
                       OSIX_FAILURE on failure                                *
 *******************************************************************************/
INT4
WssIfDscpConfigTableGetNextEntry (INT4 *pi4IfIndex,
                                  UINT4 *pu4CapwapDot11WlanProfileId,
                                  INT4 *pi4NextIfIndex,
                                  UINT4 *pu4NextCapwapDot11WlanProfileId)
{
    tCapwapDscpMapTable CapDscpMap;
    tCapwapDscpMapTable CapNextDscpMap;
    tCapwapDscpMapTable *pCapDscpMap = &CapDscpMap;
    tCapwapDscpMapTable *pCapNextDscpMap = NULL;

    MEMSET (&CapDscpMap, 0, sizeof (tCapwapDscpMapTable));
    MEMSET (&CapNextDscpMap, 0, sizeof (tCapwapDscpMapTable));

    pCapDscpMap->u4IfIndex = (UINT4) *pi4IfIndex;
    pCapDscpMap->u2WlanProfileId = (UINT2) *pu4CapwapDot11WlanProfileId;

    pCapNextDscpMap = (tCapwapDscpMapTable *) RBTreeGetNext
        (gCapDscpMapTable, (tRBElem *) pCapDscpMap, NULL);
    if (pCapNextDscpMap == NULL)
    {
        return OSIX_FAILURE;
    }

    *pi4NextIfIndex = (INT4) pCapNextDscpMap->u4IfIndex;
    *pu4NextCapwapDot11WlanProfileId = (UINT4) pCapNextDscpMap->u2WlanProfileId;
    return OSIX_SUCCESS;
}

#endif

#ifdef WTP_WANTED
/*****************************************************************************
 *  Function Name   : WssIfCapwapDscpMapEntryGet                             *
 *  Description     : This function gets the entry in the                    *
 *                    Capwap Dscp Map Table.                                 *
 *  Input(s)        : u4IfIndex  - Interface index                           *
 *                    u1InPriority - Priority value                          *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : NULL on failure                                        *
 *                    pCapDscpMap on success                                 *
 *****************************************************************************/

tCapwapDscpMapTable *
WssIfCapwapDscpMapEntryGet (UINT4 u4IfIndex, UINT1 u1InPriority)
{
#if defined (LNXIP4_WANTED)
    tCapwapDscpMapTable *pCapDscpMapEntry = NULL;
    tCapwapDscpMapTable CapDscpMapEntry;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    WSSIF_FN_ENTRY ();
    MEMSET (&CapDscpMapEntry, 0, sizeof (tCapwapDscpMapTable));
    MEMSET (&au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    SNPRINTF ((CHR1 *) au1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s",
              CfaGddGetLnxIntfnameForPort ((UINT2) u4IfIndex));

    MEMCPY (CapDscpMapEntry.au1InterfaceName, au1IfName,
            CFA_MAX_PORT_NAME_LENGTH);
    CapDscpMapEntry.u1InPriority = u1InPriority;

    pCapDscpMapEntry =
        ((tCapwapDscpMapTable *)
         RBTreeGet (gCapDscpMapTable, (tRBElem *) & CapDscpMapEntry));

    if (pCapDscpMapEntry == NULL)
    {
        /* Possible in case of Plain Text Join Request message */
        WSSIF_TRC (WSSIF_MGMT_TRC,
                   "WssIfCapwapDscpMapEntryGet:Entry is not present in Dscp Map Table \r\n");
        return NULL;
    }
    WSSIF_FN_EXIT ();
    return pCapDscpMapEntry;
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1InPriority);
    return NULL;
#endif
}
#endif

tRemoteSessionManager *
WssIfGetRsmFromProfileId (tCapwapSessProfileIdMapTable * pSessProfileIdEntry)
{
    if (pSessProfileIdEntry == NULL)
    {
        WSSIF_TRC (WSSIF_MGMT_TRC, "Session Profile Map entry is NULL \r\n");
    }
    else
    {
        return (&gaRemoteSessionManager[pSessProfileIdEntry->u2CapwapRSMIndex]);
    }
    return NULL;
}

tRemoteSessionManager *
WssIfGetRsmFromCapwapId (UINT2 u2CapwapRSMIndex)
{
    WSSIF_FN_ENTRY ();
    return (&gaRemoteSessionManager[u2CapwapRSMIndex]);
    WSSIF_FN_EXIT ();
}

tCapwapDtlsSessEntry *
WssIfCapwapDtlsEntryGet (UINT4 u4IpAddr)
{
    tCapwapDtlsSessEntry *pCapwapDtlsSessEntry = NULL;
    tCapwapDtlsSessEntry CapwapDtlsSessEntry;

    WSSIF_FN_ENTRY ();
    MEMSET (&CapwapDtlsSessEntry, 0, sizeof (tCapwapDtlsSessEntry));
    CapwapDtlsSessEntry.u4IpAddr = u4IpAddr;

    pCapwapDtlsSessEntry =
        ((tCapwapDtlsSessEntry *)
         RBTreeGet (gCapDtlsMapTable, (tRBElem *) & CapwapDtlsSessEntry));

    if (pCapwapDtlsSessEntry == NULL)
    {
        /* Possible in case of Plain Text Join Request message */
        WSSIF_TRC (WSSIF_MGMT_TRC,
                   "WssIfCapwapDtlsEntryGet:Entry is not present in Profile Map Table \r\n");
        return NULL;
    }
    WSSIF_FN_EXIT ();
    return pCapwapDtlsSessEntry;

}

tWssIfProfileIdMap *
WssIfWtpProfileIdEntryGet (UINT4 u4WtpProfildId)
{

    tWssIfProfileIdMap *pWssIfProfileId = NULL;
    tWssIfProfileIdMap  WssIfProfileId;

    WSSIF_FN_ENTRY ();
    MEMSET (&WssIfProfileId, 0, sizeof (tWssIfProfileIdMap));

    WssIfProfileId.u4WtpProfildId = u4WtpProfildId;

    pWssIfProfileId = ((tWssIfProfileIdMap *) RBTreeGet (gCapProfildIdMapTable,
                                                         (tRBElem *) &
                                                         WssIfProfileId));

    if (pWssIfProfileId == NULL)
    {
        WSSIF_TRC (WSSIF_MGMT_TRC,
                   "WssIfWtpProfileIdEntryGet:Entry is not present in Profile Map Table \r\n");
        return NULL;
    }
    WSSIF_FN_EXIT ();
    return pWssIfProfileId;

}

tWssIfWtpModelMap  *
WssIfWtpModelEntryGet (UINT1 *pWtpModelName)
{

    tWssIfWtpModelMap  *pWtpModelMap = NULL;
    tWssIfWtpModelMap   WtpModelMap;

    WSSIF_FN_ENTRY ();
    MEMSET (&WtpModelMap, 0, sizeof (tWssIfWtpModelMap));

    MEMCPY (WtpModelMap.au1ModelNumber, pWtpModelName, MAX_WTP_MODELNUMBER_LEN);

    pWtpModelMap = ((tWssIfWtpModelMap *) RBTreeGet (gCapWtpModelMapTable,
                                                     (tRBElem *) &
                                                     WtpModelMap));

    if (pWtpModelMap == NULL)
    {
        WSSIF_TRC (WSSIF_MGMT_TRC,
                   "WssIfWtpModelEntryGet:Entry is not present in WTP Model Table \r\n");
        return NULL;
    }
    WSSIF_FN_EXIT ();
    return pWtpModelMap;

}

/*****************************************************************************/
/* Function Name      : WssIfCompareSessionPortRBTree                         */
/*                                                                           */
/* Description        : This function compares the two WTP session RB Tree's */
/*                      based on the based on the IP addr or the session     */
/*                      index                                                */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    :  */
/*****************************************************************************/
INT4
WssIfCompareSessionPortRBTree (tRBElem * e1, tRBElem * e2)
{
    tCapwapSessDestIpPortMapTable *pNode1 = e1;
    tCapwapSessDestIpPortMapTable *pNode2 = e2;
    INT4                i4CmpVal = 0;

    i4CmpVal = MEMCMP (&pNode1->DestIp, &pNode2->DestIp, sizeof (tIpAddr));

    if (i4CmpVal > 0)
    {
        return WSSIF_RB_GREATER;
    }
    else if (i4CmpVal < 0)
    {
        return WSSIF_RB_LESS;
    }

    if (pNode1->u4DestPort > pNode2->u4DestPort)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u4DestPort < pNode2->u4DestPort)
    {
        return WSSIF_RB_LESS;
    }

    return WSSIF_RB_EQUAL;
}

INT4
WssIfCompareSessionProfileIdRBTree (tRBElem * e1, tRBElem * e2)
{

    tCapwapSessProfileIdMapTable *pNode1 = e1;
    tCapwapSessProfileIdMapTable *pNode2 = e2;

    if (pNode1->u2WtpProfileInternalId > pNode2->u2WtpProfileInternalId)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u2WtpProfileInternalId < pNode2->u2WtpProfileInternalId)
    {
        return WSSIF_RB_LESS;
    }

    return WSSIF_RB_EQUAL;
}

INT4
WssIfCompareCapwapDtlsRBTree (tRBElem * e1, tRBElem * e2)
{
    tCapwapDtlsSessEntry *pNode1 = e1;
    tCapwapDtlsSessEntry *pNode2 = e2;

    if (pNode1->u4IpAddr > pNode2->u4IpAddr)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u4IpAddr < pNode2->u4IpAddr)
    {
        return WSSIF_RB_LESS;
    }
    return WSSIF_RB_EQUAL;
}

INT4
WssIfCompareWtpProfileRBTree (tRBElem * e1, tRBElem * e2)
{

    tWssIfProfile      *pNode1 = e1;
    tWssIfProfile      *pNode2 = e2;

    if (pNode1->u4WtpProfileId > pNode2->u4WtpProfileId)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u4WtpProfileId < pNode2->u4WtpProfileId)
    {
        return WSSIF_RB_LESS;
    }
    return WSSIF_RB_EQUAL;

}

INT4
WssIfCompareWtpProfildIdRBTree (tRBElem * e1, tRBElem * e2)
{

    tWssIfProfileIdMap *pNode1 = e1;
    tWssIfProfileIdMap *pNode2 = e2;

    if (pNode1->u4WtpProfildId > pNode2->u4WtpProfildId)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u4WtpProfildId < pNode2->u4WtpProfildId)
    {
        return WSSIF_RB_LESS;
    }
    return WSSIF_RB_EQUAL;

}

INT4
WssIfCompareWtpBindingTableRBTree (tRBElem * e1, tRBElem * e2)
{
    tCapwapBindingTable *pNode1 = e1;
    tCapwapBindingTable *pNode2 = e2;
    INT4                i4RetVal = 0;

    i4RetVal =
        MEMCMP (pNode1->au1ModelNumber, pNode2->au1ModelNumber,
                MAX_WTP_MODELNUMBER_LEN);

    if (i4RetVal > 0)
    {
        return WSSIF_RB_GREATER;
    }
    else if (i4RetVal < 0)
    {
        return WSSIF_RB_LESS;
    }
    if (pNode1->u1RadioId > pNode2->u1RadioId)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u1RadioId < pNode2->u1RadioId)
    {
        return WSSIF_RB_LESS;
    }
    if (pNode1->u2WlanProfileId > pNode2->u2WlanProfileId)
    {
        return WSSIF_RB_GREATER;
    }
    if (pNode1->u2WlanProfileId < pNode2->u2WlanProfileId)
    {
        return WSSIF_RB_LESS;
    }
    return WSSIF_RB_EQUAL;
}

INT4
WssIfCompareWtpProfileMacMapRBTree (tRBElem * e1, tRBElem * e2)
{

    tWssIfProfileMacMap *pNode1 = e1;
    tWssIfProfileMacMap *pNode2 = e2;
    INT4                i4RetVal = 0;

    i4RetVal = MEMCMP (pNode1->MacAddr, pNode2->MacAddr, MAC_ADDR_LEN);

    if (i4RetVal > 0)
    {
        return 1;
    }
    else if (i4RetVal < 0)
    {
        return -1;
    }
    return 0;
}

INT4
WssIfCompareWtpProfileNameMapRBTree (tRBElem * e1, tRBElem * e2)
{

    tWssIfProfileNameMap *pNode1 = e1;
    tWssIfProfileNameMap *pNode2 = e2;
    INT4                i4RetVal = 0;

    i4RetVal = MEMCMP (pNode1->au1WtpProfileName, pNode2->au1WtpProfileName,
                       WSSIF_CAPWAP_PROFILE_NAME_LEN);

    if (i4RetVal > 0)
    {
        return 1;
    }
    else if (i4RetVal < 0)
    {
        return -1;
    }
    return 0;
}

INT4
WssIfCompareWtpModelRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssIfWtpModelMap  *pNode1 = e1;
    tWssIfWtpModelMap  *pNode2 = e2;
    INT4                i4RetVal = 0;

    i4RetVal = MEMCMP (pNode1->au1ModelNumber, pNode2->au1ModelNumber,
                       MAX_WTP_MODELNUMBER_LEN);

    if (i4RetVal > 0)
    {
        return 1;
    }
    else if (i4RetVal < 0)
    {
        return -1;
    }
    return 0;

}

INT4
WssIfCompareWlcWhiteListRBTree (tRBElem * e1, tRBElem * e2)
{

    tWssIfWlcWhiteListDB *pNode1 = e1;
    tWssIfWlcWhiteListDB *pNode2 = e2;
    INT4                i4RetVal = 0;

    i4RetVal = MEMCMP (pNode1->MacAddr, pNode2->MacAddr, MAC_ADDR_LEN);

    if (i4RetVal > 0)
    {
        return 1;
    }
    else if (i4RetVal < 0)
    {
        return -1;
    }
    return 0;
}

INT4
WssIfCompareWlcBlackListRBTree (tRBElem * e1, tRBElem * e2)
{

    tWssIfWlcBlackListDB *pNode1 = e1;
    tWssIfWlcBlackListDB *pNode2 = e2;
    INT4                i4RetVal = 0;

    i4RetVal = MEMCMP (pNode1->MacAddr, pNode2->MacAddr, MAC_ADDR_LEN);

    if (i4RetVal > 0)
    {
        return 1;
    }
    else if (i4RetVal < 0)
    {
        return -1;
    }
    return 0;
}

INT4
WssIfCompareStaWhiteListRBTree (tRBElem * e1, tRBElem * e2)
{

    tWssIfStaWhiteListDB *pNode1 = e1;
    tWssIfStaWhiteListDB *pNode2 = e2;
    INT4                i4RetVal = 0;

    i4RetVal = MEMCMP (pNode1->MacAddr, pNode2->MacAddr, MAC_ADDR_LEN);

    if (i4RetVal > 0)
    {
        return 1;
    }
    else if (i4RetVal < 0)
    {
        return -1;
    }
    return 0;
}

INT4
WssIfCompareStaBlackListRBTree (tRBElem * e1, tRBElem * e2)
{

    tWssIfStaBlackListDB *pNode1 = e1;
    tWssIfStaBlackListDB *pNode2 = e2;
    INT4                i4RetVal = 0;

    i4RetVal = MEMCMP (pNode1->MacAddr, pNode2->MacAddr, MAC_ADDR_LEN);

    if (i4RetVal > 0)
    {
        return 1;
    }
    else if (i4RetVal < 0)
    {
        return -1;
    }
    return 0;
}

#ifdef WLC_WANTED
/*****************************************************************************/
/* Function Name      : WssIfCompareDscpMapRBTree                            */
/*                                                                           */
/* Description        : This function compares the two Dscp map RB Tree's    */
/*                      based on the Interface name  and  the Priority value */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    :  */
/*****************************************************************************/
INT4
WssIfCompareDscpMapRBTree (tRBElem * e1, tRBElem * e2)
{
    tCapwapDscpMapTable *pNode1 = e1;
    tCapwapDscpMapTable *pNode2 = e2;

    if (pNode1->u4IfIndex > pNode2->u4IfIndex)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u4IfIndex < pNode2->u4IfIndex)
    {
        return WSSIF_RB_LESS;
    }
    if (pNode1->u2WlanProfileId > pNode2->u2WlanProfileId)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u2WlanProfileId < pNode2->u2WlanProfileId)
    {
        return WSSIF_RB_LESS;
    }

    return WSSIF_RB_EQUAL;
}
#endif

#ifdef WTP_WANTED
/*****************************************************************************/
/* Function Name      : WssIfCompareDscpMapRBTree                            */
/*                                                                           */
/* Description        : This function compares the two Dscp map RB Tree's    */
/*                      based on the Interface name  and  the Priority value */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    :  */
/*****************************************************************************/
INT4
WssIfCompareDscpMapRBTree (tRBElem * e1, tRBElem * e2)
{
    tCapwapDscpMapTable *pNode1 = e1;
    tCapwapDscpMapTable *pNode2 = e2;
    INT4                i4CmpVal = 0;

    i4CmpVal =
        MEMCMP (pNode1->au1InterfaceName, pNode2->au1InterfaceName,
                CFA_MAX_PORT_NAME_LENGTH);

    if (i4CmpVal > 0)
    {
        return WSSIF_RB_GREATER;
    }
    else if (i4CmpVal < 0)
    {
        return WSSIF_RB_LESS;
    }
    if (pNode1->u1InPriority > pNode2->u1InPriority)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u1InPriority < pNode2->u1InPriority)
    {
        return WSSIF_RB_LESS;
    }

    return WSSIF_RB_EQUAL;
}
#endif

INT4
WssIfSetProfildIndex (UINT2 u2WtpProfileId)
{
    tWssIfProfile      *pProfile = NULL;
    UINT2               u2WtpInternalId = 0;

    /* Add entry with the incoming IP address to the RB tree */
    pProfile = (tWssIfProfile *) MemAllocMemBlk (CAPWAP_WTPPROFILE_POOLID);

    if (pProfile == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfSetProfildIndex: Memory allocation Failure \r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pProfile, 0, sizeof (tWssIfProfile));

    CapwapGetFreeInternalId (&u2WtpProfileId, &u2WtpInternalId);

    pProfile->u4WtpProfileId = u2WtpProfileId;
    pProfile->u2WtpInternalId = u2WtpInternalId;

    if (RBTreeAdd (gCapWtpProfileTable, pProfile) != RB_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfSetProfildIndex: Failed to add entry to "
                   "the Profile Table\r\n");
        MemReleaseMemBlock (CAPWAP_WTPPROFILE_POOLID, (UINT1 *) pProfile);

        return OSIX_FAILURE;
    }

    gaWtpIntProfile[u2WtpInternalId].u2ProfileId = u2WtpProfileId;
    gu2WtpProfileId++;
    gaWtpIntProfile[u2WtpInternalId].u1Used = OSIX_TRUE;

    return OSIX_SUCCESS;
}

INT4
WssIfGetProfileIndex (UINT2 u2WtpProfileId, UINT2 *pInternalId)
{
    tWssIfProfile      *pWssIfProfile = NULL;
    tWssIfProfile       wtpProfile;

    MEMSET (&wtpProfile, 0, sizeof (tWssIfProfile));

    wtpProfile.u4WtpProfileId = (UINT4) u2WtpProfileId;

    pWssIfProfile = RBTreeGet (gCapWtpProfileTable, (tRBElem *) & wtpProfile);
    if (pWssIfProfile == NULL)
    {
        WSSIF_TRC (WSSIF_MGMT_TRC,
                   "\r\nWssIfGetProfileIndex: Entry not present for Profile\r\n");
        return OSIX_FAILURE;
    }

    *pInternalId = pWssIfProfile->u2WtpInternalId;
    return OSIX_SUCCESS;
}

INT4
WssIfDeleteProfileIndex (UINT2 u2WtpProfileId)
{
    tWssIfProfile      *pWssIfProfile = NULL;
    tWssIfProfile       wtpProfile;

    MEMSET (&wtpProfile, 0, sizeof (tWssIfProfile));

    wtpProfile.u4WtpProfileId = u2WtpProfileId;

    pWssIfProfile = RBTreeGet (gCapWtpProfileTable, (tRBElem *) & wtpProfile);
    if (pWssIfProfile == NULL)
    {
        WSSIF_TRC (WSSIF_MGMT_TRC,
                   "\r\nWssIfGetProfileIndex: Entry not present for Profile\r\n");
        return OSIX_FAILURE;
    }

    RBTreeRem (gCapWtpProfileTable, (tRBElem *) pWssIfProfile);
    MemReleaseMemBlock (CAPWAP_WTPPROFILE_POOLID, (UINT1 *) pWssIfProfile);
    return OSIX_SUCCESS;
}

INT4
CapwapGetFreeInternalId (UINT2 *pProfildId, UINT2 *pWtpInternalId)
{
    UINT2               u2Index = 0;

    UNUSED_PARAM (pProfildId);

    while (u2Index < MAX_WTP_PROFILE_SUPPORTED)
    {
        if (gaWtpIntProfile[u2Index].u1Used == OSIX_FALSE)
        {
            *pWtpInternalId = u2Index;
            break;
        }
        u2Index++;
    }
    return OSIX_SUCCESS;
}

INT4
CapwapGetInternalProfildId (UINT2 *pProfildId, UINT2 *pWtpInternalId)
{
    tWssIfProfile      *pWssIfProfile = NULL;
    tWssIfProfile       wtpProfile;

    MEMSET (&wtpProfile, 0, sizeof (tWssIfProfile));

    wtpProfile.u4WtpProfileId = *pProfildId;

    pWssIfProfile = RBTreeGet (gCapWtpProfileTable, (tRBElem *) & wtpProfile);
    if (pWssIfProfile == NULL)
    {
        WSSIF_TRC (WSSIF_MGMT_TRC,
                   "WssIfGetProfileIndex: Entry not present for Profile\r\n");
        return OSIX_FAILURE;
    }

    *pWtpInternalId = pWssIfProfile->u2WtpInternalId;
    return OSIX_SUCCESS;
}

INT4
CapwapGetProfileIdFromInternalProfId (UINT2 wtpInternalId, UINT2 *pWtpProfileId)
{
    *pWtpProfileId = gaWtpIntProfile[wtpInternalId].u2ProfileId;
    return OSIX_SUCCESS;
}

INT4
WssIfProcessCapwapDBMsg (UINT1 u1MsgType, tWssIfCapDB * pWssIfCapwapDB)
{
    tRemoteSessionManager *pSessEntry = NULL;
    tCapwapSessProfileIdMapTable *pSessProfileIdEntry = NULL;
    tCapwapSessDestIpPortMapTable *pSessDestIpEntry = NULL;
    UINT4               u4Index = 0;
    UINT4               u4TmpAssocMapping = 0;
    UINT4               u4ShiftVal = 0;
    UINT4               u4Val = 0;
    UINT4               u4FreeAid = 0;
    UINT2               u2WtpProfileId = 0;
    UINT2               u2Len = 0;
    UINT2               u2ProfileId = 0;
    UINT2               u2ModelIndex = 0;
    tWssIfWtpModelMap   WtpModelMap;
    tCapwapDtlsSessEntry *pCapwapDtlsSessEntry = NULL;
    UINT1               u1RadioId = 0;
    UINT1               u1NoOfRadio = 0;
    UINT2               u2Index = 0;
    UINT1               u1Index = 0;
    UINT1               u1Index1 = 0;
    tRadioIfDB          RadioIfDB;
    tRadioIfDB         *pGetRadioIfDB = NULL;
    tWssIfProfileMacMap *pWssIfProfileMacMap = NULL;
    tWssIfProfileMacMap wtpProfileMacEntry;
    tWssIfProfileNameMap *pWssIfProfileNameMap = NULL;
    tWssIfProfileNameMap wtpProfileNameEntry;
    tWssIfProfileNameMap *pWssIfProfileGroupMap = NULL;
    tWssIfProfileNameMap wtpGroupNameEntry;
    tWssIfWtpModelMap  *pWtpModelMap = NULL;
    tWssIfWlcBlackListDB WssIfWlcBlackListDB;
    tWssIfWlcBlackListDB *pWssIfWlcBlackListDB = NULL;
    tWssIfWlcWhiteListDB WssIfWlcWhiteListDB;
    tWssIfWlcWhiteListDB *pWssIfWlcWhiteListDB = NULL;
    tWssIfStaWhiteListDB WssIfStaWhiteListDB;
    tWssIfStaWhiteListDB *pWssIfStaWhiteListDB = NULL;
    tWssIfStaBlackListDB WssIfStaBlackListDB;
    tWssIfStaBlackListDB *pWssIfStaBlackListDB = NULL;
    UINT1              *pu1WtpModelNumber = NULL;
    pu1WtpModelNumber = pWssIfCapwapDB->CapwapGetDB.au1WtpModelNumber;

    WSSIF_FN_ENTRY ();

    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));
    MEMSET (&wtpProfileNameEntry, 0, sizeof (tWssIfProfileNameMap));
    MEMSET (&wtpProfileMacEntry, 0, sizeof (tWssIfProfileMacMap));
    MEMSET (&WssIfWlcBlackListDB, 0, sizeof (tWssIfWlcBlackListDB));
    MEMSET (&WssIfWlcWhiteListDB, 0, sizeof (tWssIfWlcWhiteListDB));
    MEMSET (&WssIfStaWhiteListDB, 0, sizeof (tWssIfStaWhiteListDB));
    MEMSET (&WssIfStaBlackListDB, 0, sizeof (tWssIfStaBlackListDB));
    switch (u1MsgType)
    {
        case WSS_CAPWAP_INIT_DB:
            if (WssIfPortSessDBCreate () == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfPortSessDBCreate Returned Failure \r\n");
                return OSIX_FAILURE;
            }
            if (WssIfWtpProfileIdSessDBCreate () == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfWtpProfileIdSessDBCreate Returned Failure \r\n");
                return OSIX_FAILURE;
            }
            if (WssIfRemoteSessionInit () == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfRemoteSessionInit Returned Failure \r\n");
                return OSIX_FAILURE;
            }
            if (WssifcapSizingMemCreateMemPools () == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfSizingMemCreateMemPools Returned Failure \r\n");
                return OSIX_FAILURE;

            }
            if (WssIfCapwapDBInit () == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfCapwapDBInit Returned Failure \r\n");
                return OSIX_FAILURE;
            }
            if (WssIfCapwapDtlsDBCreate () == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfCapwapDtlsDBCreate Returned Failure \r\n");
                return OSIX_FAILURE;
            }
            if (WssIfWtpProfileDBCreate () == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfWtpProfileIdDBCreate Returned Failure \r\n");
                return OSIX_FAILURE;
            }
            if (WssIfWtpProfildIdDBCreate () == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfWtpProfildIdDBCreate Returned Failure \r\n");
                return OSIX_FAILURE;
            }
            if (WssIfWtpProfileNameMapDBCreate () == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            if (WssIfWtpProfileMacMapDBCreate () == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            if (WssIfWtpModelDBCreate () == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            if (WssIfWlcBlackListDBCreate () != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            if (WssIfWlcWhiteListDBCreate () != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            if (WssIfStaWhiteListDBCreate () != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            if (WssIfStaBlackListDBCreate () != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            if (WssIfCapDscpMapDBCreate () != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            if (WssIfWtpBindingTableDBCreate () != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
#ifdef WLC_WANTED
            if (WssIfDhcpGlobalDBCreate () != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            if (WssIfWtpDhcpDBCreate () != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            if (WssIfNATConfigEntryDBCreate () != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            if (WssIfWtpLRConfigDBCreate () != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            if (WssIfWtpIpRouteConfigDBCreate () != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            if (WssIfWtpDhcpConfigDBCreate () != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            if (WssIfWtpFwlFilterDBCreate () == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            if (WssIfWtpFwlRuleDBCreate () == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            if (WssIfWtpFwlAclDBCreate () == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            if (WssIfWtpL3SubIfDBCreate () == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
#endif
            break;
        case WSS_CAPWAP_SHUTDOWN_DB:
            /* do the delete version of functions in WSS_CAPWAP_INIT_DB, but in the
             * reverse order */
            if (WssIfStaBlackListDBDelete () != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            if (WssIfStaWhiteListDBDelete () != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            if (WssIfWlcWhiteListDBDelete () != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            if (WssIfWlcBlackListDBDelete () != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            if (WssIfWtpModelDBDelete () == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            if (WssIfWtpProfileMacMapDBDelete () == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            if (WssIfWtpProfileNameMapDBDelete () == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            if (WssIfWtpProfildIdDBDelete () == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfWtpProfildIdDBDelete Returned Failure \r\n");
                return OSIX_FAILURE;
            }
            if (WssIfCapwapDtlsDBDelete () == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfCapwapDtlsDBDelete Returned Failure \r\n");
                return OSIX_FAILURE;
            }

            /* nothing to do for reverse of WssIfCapwapDBInit */

            WssifcapSizingMemDeleteMemPools ();

            /* nothing to do for reverse of WssIfRemoteSessionInit */

            if (WssIfWtpProfileIdSessDBDelete () == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfWtpProfileIdSessDBDelete Returned Failure \r\n");
                return OSIX_FAILURE;
            }
            if (WssIfPortSessDBDelete () == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfPortSessDBDelete Returned Failure \r\n");
                return OSIX_FAILURE;
            }
            if (WssIfCapDscpMapDBDelete () == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfCapDscpMapDBDelete Returned Failure \r\n");
                return OSIX_FAILURE;
            }
#ifdef WLC_WANTED
            if (WssIfWtpFwlFilterDBDelete () == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfWtpFwlFilterDBDelete Returned Failure \r\n");
                return OSIX_FAILURE;
            }
            if (WssIfWtpFwlRuleDBDelete () == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfWtpFwlRuleDBDelete Returned Failure \r\n");
                return OSIX_FAILURE;
            }
            if (WssIfWtpFwlAclDBDelete () == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfWtpFwlAclDBDelete Returned Failure \r\n");
                return OSIX_FAILURE;
            }
            if (WssIfWtpL3SubIfDBDelete () == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfWtpL3SubIfDBDelete Returned Failure \r\n");
                return OSIX_FAILURE;
            }
#endif
            break;
        case WSS_CAPWAP_GET_DB:
        {
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bCtrlUdpPort == OSIX_TRUE)
            {
                pWssIfCapwapDB->u4CtrludpPort = gWssCapDB.u4CtrludpPort;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bAcName == OSIX_TRUE)
            {
                u2Len = (UINT2) (STRLEN (gWssCapDB.au1WlcName));
                MEMCPY (pWssIfCapwapDB->au1WlcName,
                        gWssCapDB.au1WlcName, u2Len);
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bDiscMode == OSIX_TRUE)
            {
                pWssIfCapwapDB->u1DiscMode = gWssCapDB.u1DiscMode;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bCtrlDTLSStatus == OSIX_TRUE)
            {
                pWssIfCapwapDB->u1CtrlDTLSStatus = gWssCapDB.u1CtrlDTLSStatus;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bDataDTLSStatus == OSIX_TRUE)
            {
                pWssIfCapwapDB->u1DataDTLSStatus = gWssCapDB.u1DataDTLSStatus;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bActiveWtps == OSIX_TRUE)
            {
                pWssIfCapwapDB->u2ActiveWtps = gWssCapDB.u2ActiveWtps;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bLocalIpAddr == OSIX_TRUE)
            {
                pWssIfCapwapDB->LocalIpAddr.u4_addr[0] =
                    gWssCapDB.LocalIpAddr.u4_addr[0];
            }

            if (pWssIfCapwapDB->CapwapIsGetAllDB.bMaxWtps == OSIX_TRUE)
            {
                pWssIfCapwapDB->u2MaxWtps = gWssCapDB.u2MaxWtps;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bNativeVlan == OSIX_TRUE)
            {
                pWssIfCapwapDB->CapwapGetDB.u4NativeVlan =
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u4NativeVlan;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bWlcDescriptor == OSIX_TRUE)
            {
                pWssIfCapwapDB->u2ActiveStation = gWssCapDB.u2ActiveStation;
                pWssIfCapwapDB->u2MaxStationLimit = gWssCapDB.u2MaxStationLimit;
                pWssIfCapwapDB->u2ActiveWtps = gWssCapDB.u2ActiveWtps;
                pWssIfCapwapDB->u2MaxWtps = gWssCapDB.u2MaxWtps;
                pWssIfCapwapDB->u1Security = gWssCapDB.u1Security;
                pWssIfCapwapDB->u1RMAC = gWssCapDB.u1RMAC;
                pWssIfCapwapDB->u1DataDTLSStatus = gWssCapDB.u1DataDTLSStatus;
                pWssIfCapwapDB->u4VendorSMI = gWssCapDB.u4VendorSMI;
                u2Len = (UINT2) (STRLEN (gWssCapDB.au1WlcHWversion));
                MEMCPY (pWssIfCapwapDB->au1WlcHWversion,
                        gWssCapDB.au1WlcHWversion, u2Len);
                u2Len = (UINT2) (STRLEN (gWssCapDB.au1WlcSWversion));
                MEMCPY (pWssIfCapwapDB->au1WlcSWversion,
                        gWssCapDB.au1WlcSWversion, u2Len);
            }
            /* Get the Internal Profile Id */
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId == OSIX_TRUE)
            {
                u2ProfileId = pWssIfCapwapDB->CapwapGetDB.u2WtpProfileId;
                if (WssIfGetProfileIndex (u2ProfileId, &u2WtpProfileId) ==
                    OSIX_SUCCESS)
                {
                    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                        u2WtpProfileId;
                }
                pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            }
            else if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId ==
                     OSIX_TRUE)
            {
                u2WtpProfileId = pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bImageName == OSIX_TRUE)
            {
                MEMCPY (pWssIfCapwapDB->CapwapGetDB.au1WtpImageIdentifier,
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].
                        au1WtpImageIdentifier,
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].au1WtpImageIdLen);
                pWssIfCapwapDB->CapwapGetDB.au1WtpImageIdLen =
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].au1WtpImageIdLen;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId == OSIX_TRUE)
            {
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWlcIpAddress == OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u4WlcIpAddress =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u4WlcIpAddress;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWlcCtrlPort == OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u4WlcCtrlPort =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u4WlcCtrlPort;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDataPort == OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u4WtpDataPort =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u4WtpDataPort;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDescriptor ==
                    OSIX_TRUE)
                {
                    pWssIfCapwapDB->u4VendorSMI =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u4VendorSMI;
#ifdef WLC_WANTED
                    pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpNoofRadio;
#else
                    pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio =
                        SYS_DEF_MAX_RADIO_INTERFACES;
#endif

                    u2Len =
                        (UINT2) (STRLEN
                                 (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                  au1WtpHWversion));
                    MEMCPY (pWssIfCapwapDB->CapwapGetDB.au1WtpHWversion,
                            gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            au1WtpHWversion, u2Len);

                    u2Len =
                        (UINT2) (STRLEN
                                 (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                  au1WtpSWversion));
                    MEMCPY (pWssIfCapwapDB->CapwapGetDB.au1WtpSWversion,
                            gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            au1WtpSWversion, u2Len);
                    u2Len =
                        (UINT2) STRLEN (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                        au1WtpBootversion);
                    MEMCPY (pWssIfCapwapDB->CapwapGetDB.au1WtpBootversion,
                            gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            au1WtpBootversion, u2Len);

                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName == OSIX_TRUE)
                {
                    MEMSET (pWssIfCapwapDB->CapwapGetDB.au1ProfileName, 0,
                            sizeof (pWssIfCapwapDB->CapwapGetDB.
                                    au1ProfileName));
                    u2Len =
                        (UINT2) STRLEN (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                        au1ProfileName);
                    MEMCPY (pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                            gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            au1ProfileName, u2Len);
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress ==
                    OSIX_TRUE)
                {
                    /* COVERITY FIX START */
                    MEMCPY (pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
                            gWssCapDB.CapwapGetDB[u2WtpProfileId].WtpMacAddress,
                            MAC_ADDR_LEN);
                    /* COVERITY FIX END */
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpModelNumber ==
                    OSIX_TRUE)
                {
                    u2Len =
                        (UINT2) STRLEN (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                        au1WtpModelNumber);
                    MEMCPY (pWssIfCapwapDB->CapwapGetDB.au1WtpModelNumber,
                            gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            au1WtpModelNumber, u2Len);
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpSerialNumber ==
                    OSIX_TRUE)
                {
                    u2Len =
                        (UINT2) STRLEN (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                        au1WtpSerialNumber);
                    MEMCPY (pWssIfCapwapDB->CapwapGetDB.au1WtpSerialNumber,
                            gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            au1WtpSerialNumber, u2Len);
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpUpTime == OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u4WtpUpTime =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u4WtpUpTime;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpBoardId == OSIX_TRUE)
                {
                    u2Len =
                        (UINT2) STRLEN (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                        au1WtpBoardId);
                    MEMCPY (pWssIfCapwapDB->CapwapGetDB.au1WtpBoardId,
                            gWssCapDB.CapwapGetDB[u2WtpProfileId].au1WtpBoardId,
                            u2Len);
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpBoardRivision ==
                    OSIX_TRUE)
                {
                    u2Len =
                        (UINT2) STRLEN (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                        au1WtpBoardRivision);
                    MEMCPY (pWssIfCapwapDB->CapwapGetDB.au1WtpBoardRivision,
                            gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            au1WtpBoardRivision, u2Len);
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bVendorSMI == OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u4VendorSMI =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u4VendorSMI;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpCtrlDTLSStatus ==
                    OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u1WtpCtrlDTLSStatus =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].
                        u1WtpCtrlDTLSStatus;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpCtrlDTLSCount ==
                    OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u4WtpCtrlDTLSCount =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].
                        u4WtpCtrlDTLSCount;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDataDTLSStatus ==
                    OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u1WtpDataDTLSStatus =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].
                        u1WtpDataDTLSStatus;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bImageTransfer ==
                    OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u1ImageTransferFlag =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].
                        u1ImageTransferFlag;
                }

                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDiscType == OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u1WtpDiscType =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpDiscType;
                    /* if (gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpDiscType == AC_REFERRAL_DISC_TYPE) */
                    {
                        pWssIfCapwapDB->u1AcRefOption = gWssCapDB.u1AcRefOption;
                    }
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWlcStaticIp == OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u4WlcStaticIp =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u4WlcStaticIp;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMaxDiscoveryInterval ==
                    OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u2WtpMaxDiscoveryInterval =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].
                        u2WtpMaxDiscoveryInterval;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bTunnelMode == OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u1WtpTunnelMode =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpTunnelMode;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bMacType == OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u1WtpMacType =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpMacType;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bMacType == OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u1WtpMacType =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpMacType;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bDiscPadding == OSIX_TRUE)
                {
                    u2Len =
                        (UINT2) STRLEN (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                        au1DiscPadding);
                    MEMCPY (pWssIfCapwapDB->CapwapGetDB.au1DiscPadding,
                            gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            au1DiscPadding, u2Len);
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStatisticsTimer ==
                    OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u2WtpStatisticsTimer =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].
                        u2WtpStatisticsTimer;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bAcNamewithPriority ==
                    OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].u1Priority =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].AcNameWithPri[0].
                        u1Priority;
                    u2Len =
                        (UINT2) STRLEN (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                        AcNameWithPri[0].wlcName);
                    MEMCPY (pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].
                            wlcName,
                            gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            AcNameWithPri[0].wlcName, u2Len);

                    pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[1].u1Priority =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].AcNameWithPri[1].
                        u1Priority;
                    u2Len =
                        (UINT2) STRLEN (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                        AcNameWithPri[1].wlcName);
                    MEMCPY (pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[1].
                            wlcName,
                            gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            AcNameWithPri[1].wlcName, u2Len);

                    pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[2].u1Priority =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].AcNameWithPri[2].
                        u1Priority;
                    u2Len =
                        (UINT2) STRLEN (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                        AcNameWithPri[2].wlcName);
                    MEMCPY (pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[2].
                            wlcName,
                            gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            AcNameWithPri[2].wlcName, u2Len);

                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bTransportProtocol ==
                    OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u1ProtocolType =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u1ProtocolType;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocation == OSIX_TRUE)
                {
                    u2Len =
                        (UINT2) STRLEN (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                        au1WtpLocation);
                    MEMCPY (pWssIfCapwapDB->CapwapGetDB.au1WtpLocation,
                            gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            au1WtpLocation, u2Len);
                }

                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting ==
                    OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpLocalRouting;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bFragReassembleStatus ==
                    OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u1FragReassembleStatus =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].
                        u1FragReassembleStatus;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bPathMTU == OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u4PathMTU =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u4PathMTU;
                }

                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpName == OSIX_TRUE)
                {
                    u2Len =
                        (UINT2) STRLEN (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                        au1WtpName);
                    MEMCPY (pWssIfCapwapDB->CapwapGetDB.au1WtpName,
                            gWssCapDB.CapwapGetDB[u2WtpProfileId].au1WtpName,
                            u2Len);
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpEchoInterval ==
                    OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u2WtpEchoInterval =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u2WtpEchoInterval;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpFallbackEnable ==
                    OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u1WtpFallbackEnable =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].
                        u1WtpFallbackEnable;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpIdleTimeout ==
                    OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u4WtpIdleTimeout =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u4WtpIdleTimeout;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpEcnSupport ==
                    OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u1WtpEcnSupport =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpEcnSupport;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio == OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpNoofRadio;
                }
                for (u1Index = 0;
                     u1Index < pWssIfCapwapDB->CapwapGetDB.u1WtpNoofRadio;
                     u1Index++)
                {
                    if (pWssIfCapwapDB->CapwapIsGetAllDB.bRadioAdminStatus ==
                        OSIX_TRUE)
                    {
                        pWssIfCapwapDB->CapwapGetDB.
                            au1WtpRadioAdminStatus[u1Index] =
                            gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            au1WtpRadioAdminStatus[u1Index];
                    }
                    if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpRadioType ==
                        OSIX_TRUE)
                    {
                        pWssIfCapwapDB->CapwapGetDB.au4WtpRadioType[u1Index] =
                            gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            au4WtpRadioType[u1Index];
                    }
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bRowStatus == OSIX_TRUE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u1RowStatus =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u1RowStatus;
                }
#ifdef KERNEL_CAPWAP_WANTED
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bIntfName == OSIX_TRUE)
                {
                    MEMCPY (pWssIfCapwapDB->CapwapGetDB.au1IntfName,
                            gWssCapDB.CapwapGetDB[u2WtpProfileId].au1IntfName,
                            INTERFACE_LEN);
                }
#endif
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpRebootStats ==
                    OSIX_TRUE)
                {
                    /*MEMCPY (pWssIfCapwapDB->CapwapGetDB.WtpRebootStats,
                       gWssCapDB.CapwapGetDB[u2WtpProfileId].WtpRebootStats, sizeof(tApRebootStats));  */
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bFreeAssocId == OSIX_TRUE)
                {
                    /*u4AssocIdMapping is mapping corresponding to use
                     * and free Assoc Id. Each bit of the corresponds to
                     * one Association ID. Whenever a association ID is
                     * being used value 1 is set in the corresponding bit*/
                    /* This followoing is used to get the Free Association ID
                     * and update that the AID as used ID in u4AssocIdMapping */

                    for (u4Index = 0;
                         u4Index < (WSSIF_CAPWAP_VALUE_32 * MAX_AID_INDEX);
                         u4Index++)
                    {
                        if ((u4Index % WSSIF_CAPWAP_VALUE_32) == 0)
                        {
                            u4TmpAssocMapping =
                                gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                u4AssocIdMapping[u4Index /
                                                 WSSIF_CAPWAP_VALUE_32];
                            u4ShiftVal = 0;
                        }
                        u4Val = (u4TmpAssocMapping >>
                                 (u4Index %
                                  WSSIF_CAPWAP_VALUE_32)) & 0x00000001;
                        u4FreeAid++;
                        if (u4Val == 0)
                        {
                            if ((u4FreeAid <= MAX_NUM_OF_STA_PER_AP) &&
                                (u4FreeAid <= MAX_AID_VALUE))
                            {
                                u4ShiftVal =
                                    (u4ShiftVal | 1) << (u4Index %
                                                         WSSIF_CAPWAP_VALUE_32);

                                gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                    u4AssocIdMapping[u4Index /
                                                     WSSIF_CAPWAP_VALUE_32] =
                                    gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                    u4AssocIdMapping[u4Index /
                                                     WSSIF_CAPWAP_VALUE_32] |
                                    u4ShiftVal;

                                pWssIfCapwapDB->CapwapGetDB.u4FreeAssocId
                                    = u4FreeAid;
                                break;
                            }
                        }
                    }
                    if (pWssIfCapwapDB->CapwapGetDB.u4FreeAssocId == 0)
                    {
                        /*If Association ID is depleted 0xFFFFFFFF is given as
                         * ID and association will be rejected*/
                        pWssIfCapwapDB->CapwapGetDB.u4FreeAssocId =
                            WSSIF_CAPWAP_AID_DEPLETED;
                    }
                }

            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB == OSIX_TRUE)
            {
                u1RadioId = pWssIfCapwapDB->CapwapGetDB.u1RadioId;
                if ((u1RadioId > 31) || (u1RadioId <= 0))
                {
                    WSSIF_TRC1 (WSSIF_FAILURE_TRC,
                                "Received the unknown Radio ID  Radio Id = %d \r\n",
                                u1RadioId);
                    return OSIX_FAILURE;
                }
                u2WtpProfileId = pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
                WSSIF_TRC2 (WSSIF_MGMT_TRC, "REceived internal id = %d %d \r\n",
                            u2WtpProfileId,
                            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId);
                WSSIF_TRC2 (WSSIF_MGMT_TRC, "Received Radio id = %d %d \r\n",
                            u1RadioId, pWssIfCapwapDB->CapwapGetDB.u1RadioId);
                if (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                    pRadioIFDB[u1RadioId - 1] != NULL)
                {
                    pWssIfCapwapDB->CapwapGetDB.u4IfIndex =
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].
                        pRadioIFDB[u1RadioId - 1]->u4VirtualRadioIfIndex;
                }
            }
            break;
        }
        case WSS_CAPWAP_SET_DB:
        {
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bAcName == OSIX_TRUE)
            {
                u2Len = (UINT2) STRLEN (pWssIfCapwapDB->au1WlcName);

                MEMSET (gWssCapDB.au1WlcName, 0, 512);
                MEMCPY (gWssCapDB.au1WlcName, pWssIfCapwapDB->au1WlcName,
                        u2Len);
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bDiscMode == OSIX_TRUE)
            {
                gWssCapDB.u1DiscMode = pWssIfCapwapDB->u1DiscMode;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bModelCheck == OSIX_TRUE)
            {
                gWssCapDB.u1ModelCheck =
                    pWssIfCapwapDB->CapwapGetDB.u1ModelCheck;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bCtrlUdpPort == OSIX_TRUE)
            {
                gWssCapDB.u4CtrludpPort = pWssIfCapwapDB->u4CtrludpPort;
#ifdef WTP_WANTED
                /* Write in wtpnvram */
                WssSetCapwapUdpSPortToWtpNvRam (gWssCapDB.u4CtrludpPort);

#endif
#ifdef WLC_WANTED
#ifdef CAP_WANTED1
                /* Write in wlcnvram */
                WssSetCapwapUdpSPortToWlcNvRam (gWssCapDB.u4CtrludpPort);
#endif

#endif
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bCtrlDTLSStatus == OSIX_TRUE)
            {
                gWssCapDB.u1CtrlDTLSStatus = pWssIfCapwapDB->u1CtrlDTLSStatus;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bDataDTLSStatus == OSIX_TRUE)
            {
                gWssCapDB.u1DataDTLSStatus = pWssIfCapwapDB->u1DataDTLSStatus;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bActiveWtps == OSIX_TRUE)
            {
                gWssCapDB.u2ActiveWtps++;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bNativeVlan == OSIX_TRUE)
            {
                u2WtpProfileId = pWssIfCapwapDB->CapwapGetDB.u2ProfileId;
                gWssCapDB.CapwapGetDB[u2WtpProfileId].u4NativeVlan =
                    pWssIfCapwapDB->CapwapGetDB.u4NativeVlan;
            }

            u2WtpProfileId = pWssIfCapwapDB->CapwapGetDB.u2ProfileId;
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bImageName == OSIX_TRUE)
            {
                u2ProfileId = pWssIfCapwapDB->CapwapGetDB.u2ProfileId;
                if (WssIfGetProfileIndex (u2ProfileId, &u2WtpProfileId) ==
                    OSIX_SUCCESS)
                {
                    pWssIfCapwapDB->CapwapGetDB.au1WtpImageIdLen =
                        STRLEN (pWssIfCapwapDB->CapwapGetDB.
                                au1WtpImageIdentifier);
                    MEMCPY (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            au1WtpImageIdentifier,
                            pWssIfCapwapDB->CapwapGetDB.au1WtpImageIdentifier,
                            pWssIfCapwapDB->CapwapGetDB.au1WtpImageIdLen);
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].au1WtpImageIdLen =
                        pWssIfCapwapDB->CapwapGetDB.au1WtpImageIdLen;
                }
            }

            /* Get the Internal Profile Id */
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId == OSIX_TRUE)
            {
                u2ProfileId = pWssIfCapwapDB->CapwapGetDB.u2ProfileId;
                if (WssIfGetProfileIndex (u2ProfileId, &u2WtpProfileId) ==
                    OSIX_SUCCESS)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u2ProfileId =
                        pWssIfCapwapDB->CapwapGetDB.u2ProfileId;
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u2WtpInternalId =
                        u2WtpProfileId;
                    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                }
            }
            else if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId ==
                     OSIX_TRUE)
            {
                u2WtpProfileId = pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bRadioIfDB == OSIX_TRUE)
            {
                u1RadioId = pWssIfCapwapDB->CapwapGetDB.u1RadioId;
                if (u1RadioId > 31)
                {
                    WSSIF_TRC1 (WSSIF_FAILURE_TRC,
                                "Received the unknow radio id %d \r\n",
                                u1RadioId);
                    return OSIX_FAILURE;
                }
                RadioIfDB.u4VirtualRadioIfIndex =
                    pWssIfCapwapDB->CapwapGetDB.u4IfIndex;
                pGetRadioIfDB =
                    RBTreeGet (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                               (tRBElem *) (&RadioIfDB));
                if (pGetRadioIfDB == NULL)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC,
                               "WssIfProcessCapwapDBMsg : RBTreeGet from RadioIfDB Failed, set failed \r\n");
                    return OSIX_FAILURE;
                }
                u2WtpProfileId = pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
                gWssCapDB.CapwapGetDB[u2WtpProfileId].pRadioIFDB[u1RadioId -
                                                                 1] =
                    pGetRadioIfDB;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId == OSIX_TRUE)
            {
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWlcIpAddress == OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u4WlcIpAddress =
                        pWssIfCapwapDB->CapwapGetDB.u4WlcIpAddress;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWlcCtrlPort == OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u4WlcCtrlPort =
                        pWssIfCapwapDB->CapwapGetDB.u4WlcCtrlPort;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDataPort == OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u4WtpDataPort =
                        pWssIfCapwapDB->CapwapGetDB.u4WtpDataPort;
                }
#ifdef KERNEL_CAPWAP_WANTED
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bIntfName == OSIX_TRUE)
                {
                    MEMCPY (gWssCapDB.CapwapGetDB[u2WtpProfileId].au1IntfName,
                            pWssIfCapwapDB->CapwapGetDB.au1IntfName,
                            INTERFACE_LEN);
                }
#endif

                if (pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName == OSIX_TRUE)
                {
                    u2Len =
                        (UINT2) STRLEN (pWssIfCapwapDB->CapwapGetDB.
                                        au1ProfileName);
                    MEMCPY (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            au1ProfileName,
                            pWssIfCapwapDB->CapwapGetDB.au1ProfileName, u2Len);
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress ==
                    OSIX_TRUE)
                {
                    MEMCPY (gWssCapDB.CapwapGetDB[u2WtpProfileId].WtpMacAddress,
                            pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
                            MAC_ADDR_LEN);
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpModelNumber ==
                    OSIX_TRUE)
                {
                    u2Len =
                        (UINT2) STRLEN (pWssIfCapwapDB->CapwapGetDB.
                                        au1WtpModelNumber);
                    MEMCPY (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            au1WtpModelNumber,
                            pWssIfCapwapDB->CapwapGetDB.au1WtpModelNumber,
                            u2Len);
                    /* Copy the contents from Model Table to WTP Profile Table */
                    MEMSET (&WtpModelMap, 0, sizeof (tWssIfWtpModelMap));
                    MEMCPY (WtpModelMap.au1ModelNumber, pu1WtpModelNumber,
                            MAX_WTP_MODELNUMBER_LEN);
                    pWtpModelMap =
                        ((tWssIfWtpModelMap *)
                         RBTreeGet (gCapWtpModelMapTable,
                                    (tRBElem *) & WtpModelMap));
                    if (pWtpModelMap == NULL)
                    {
                        WSSIF_TRC (WSSIF_MGMT_TRC,
                                   "The Given Model Entry is not present in WTP Model Table \r\n");
                        return OSIX_FAILURE;
                    }
                    u2ModelIndex = pWtpModelMap->u2ModelIndex;
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpNoofRadio =
                        gWssCapDB.WtpModelEntry[u2ModelIndex].u1NumOfRadio;
                    for (u1Index1 = 0;
                         u1Index1 <
                         gWssCapDB.WtpModelEntry[u2ModelIndex].u1NumOfRadio;
                         u1Index1++)
                    {
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            au1WtpRadioAdminStatus[u1Index1] =
                            gWssCapDB.WtpModelEntry[u2ModelIndex].
                            au1WtpRadioAdminStatus[u1Index1];
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            au4WtpRadioType[u1Index1] =
                            gWssCapDB.WtpModelEntry[u2ModelIndex].
                            au4WtpRadioType[u1Index1];
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            au1WtpMaxSSIDSupported[u1Index1] =
                            gWssCapDB.WtpModelEntry[u2ModelIndex].
                            au1WtpMaxSSIDSupported[u1Index1];
                    }
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpMacType =
                        gWssCapDB.WtpModelEntry[u2ModelIndex].u1MacType;
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpTunnelMode =
                        gWssCapDB.WtpModelEntry[u2ModelIndex].u1TunnelMode;
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].
                        u2MaxStationsSupported =
                        gWssCapDB.WtpModelEntry[u2ModelIndex].
                        u2MaxStationsSupported;

                    gWssCapDB.CapwapGetDB[u2WtpProfileId].au1WtpImageIdLen =
                        gWssCapDB.WtpModelEntry[u2ModelIndex].u1WtpImageLen;
                    MEMCPY (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            au1WtpImageIdentifier,
                            gWssCapDB.WtpModelEntry[u2ModelIndex].
                            au1WtpImageName,
                            gWssCapDB.WtpModelEntry[u2ModelIndex].
                            u1WtpImageLen);
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpName == OSIX_TRUE)
                {
                    u2Len =
                        (UINT2) STRLEN (pWssIfCapwapDB->CapwapGetDB.au1WtpName);
                    MEMCPY (gWssCapDB.CapwapGetDB[u2WtpProfileId].au1WtpName,
                            pWssIfCapwapDB->CapwapGetDB.au1WtpName, u2Len);
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocation == OSIX_TRUE)
                {
                    u2Len =
                        (UINT2) STRLEN (pWssIfCapwapDB->CapwapGetDB.
                                        au1WtpLocation);
                    MEMCPY (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            au1WtpLocation,
                            pWssIfCapwapDB->CapwapGetDB.au1WtpLocation, u2Len);
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting ==
                    OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpLocalRouting =
                        pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bFragReassembleStatus ==
                    OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].
                        u1FragReassembleStatus =
                        pWssIfCapwapDB->CapwapGetDB.u1FragReassembleStatus;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bPathMTU == OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].
                        u4PathMTU = pWssIfCapwapDB->CapwapGetDB.u4PathMTU;
                }

                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStaticIpEnable ==
                    OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpStaticIpEnable =
                        pWssIfCapwapDB->CapwapGetDB.u1WtpStaticIpEnable;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStaticIpType ==
                    OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpStaticIpType =
                        pWssIfCapwapDB->CapwapGetDB.u1WtpStaticIpType;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStaticIpAddress ==
                    OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].WtpStaticIpAddress.
                        u4_addr[0] =
                        pWssIfCapwapDB->CapwapGetDB.WtpStaticIpAddress.
                        u4_addr[0];
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpNetmask == OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].WtpNetmask.
                        u4_addr[0] =
                        pWssIfCapwapDB->CapwapGetDB.WtpNetmask.u4_addr[0];
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpUpTime == OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u4WtpUpTime =
                        pWssIfCapwapDB->CapwapGetDB.u4WtpUpTime;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpGateway == OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].WtpGateway.
                        u4_addr[0] =
                        pWssIfCapwapDB->CapwapGetDB.WtpGateway.u4_addr[0];
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpFallbackEnable ==
                    OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpFallbackEnable =
                        pWssIfCapwapDB->CapwapGetDB.u1WtpFallbackEnable;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpEchoInterval ==
                    OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u2WtpEchoInterval =
                        pWssIfCapwapDB->CapwapGetDB.u2WtpEchoInterval;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpIdleTimeout ==
                    OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u4WtpIdleTimeout =
                        pWssIfCapwapDB->CapwapGetDB.u4WtpIdleTimeout;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bImageTransfer ==
                    OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u1ImageTransferFlag =
                        pWssIfCapwapDB->CapwapGetDB.u1ImageTransferFlag;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bAcNamewithPriority ==
                    OSIX_TRUE)
                {
                    if (pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].
                        u1Priority <= 3)
                    {
                        UINT1               u1Loop = 0;
                        UINT1               awlcname[512];
                        memset (&awlcname[0], 0, 512);
                        for (u1Loop = 0; u1Loop < 3; u1Loop++)
                        {

                            if (pWssIfCapwapDB->CapwapGetDB.
                                AcNameWithPri[u1Loop].u1Priority != OSIX_FALSE)
                            {
                                if (MEMCMP
                                    (&pWssIfCapwapDB->CapwapGetDB.
                                     AcNameWithPri[u1Loop].wlcName[0],
                                     &awlcname[0], 512) != 0)
                                {
                                    u1Index =
                                        (UINT1) (pWssIfCapwapDB->CapwapGetDB.
                                                 AcNameWithPri[u1Loop].
                                                 u1Priority - 1);
                                    gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                        AcNameWithPri[u1Index].u1Priority =
                                        pWssIfCapwapDB->CapwapGetDB.
                                        AcNameWithPri[u1Loop].u1Priority;
                                    u2Len =
                                        (UINT2) (STRLEN
                                                 (pWssIfCapwapDB->CapwapGetDB.
                                                  AcNameWithPri[u1Loop].
                                                  wlcName) + 1);
                                    MEMCPY (gWssCapDB.
                                            CapwapGetDB[u2WtpProfileId].
                                            AcNameWithPri[u1Index].wlcName,
                                            pWssIfCapwapDB->CapwapGetDB.
                                            AcNameWithPri[u1Loop].wlcName,
                                            u2Len);
                                }
                                else
                                {
                                    u1Index =
                                        (UINT1) (pWssIfCapwapDB->CapwapGetDB.
                                                 AcNameWithPri[u1Loop].
                                                 u1Priority - 1);
                                    gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                        AcNameWithPri[u1Index].u1Priority = 0;
                                    MEMSET (gWssCapDB.
                                            CapwapGetDB[u2WtpProfileId].
                                            AcNameWithPri[u1Index].wlcName, 0,
                                            512);
                                }
                            }
                        }
                    }
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMaxDiscoveryInterval ==
                    OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].
                        u2WtpMaxDiscoveryInterval =
                        pWssIfCapwapDB->CapwapGetDB.u2WtpMaxDiscoveryInterval;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpCtrlDTLSStatus ==
                    OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpCtrlDTLSStatus =
                        pWssIfCapwapDB->CapwapGetDB.u1WtpCtrlDTLSStatus;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDataDTLSStatus ==
                    OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpDataDTLSStatus =
                        pWssIfCapwapDB->CapwapGetDB.u1WtpDataDTLSStatus;
                }

                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpCtrlDTLSCount ==
                    OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u4WtpCtrlDTLSCount =
                        pWssIfCapwapDB->CapwapGetDB.u4WtpCtrlDTLSCount;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpReportInterval ==
                    OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u2WtpReportInterval =
                        pWssIfCapwapDB->CapwapGetDB.u2WtpReportInterval;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStatisticsTimer ==
                    OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u2WtpStatisticsTimer =
                        pWssIfCapwapDB->CapwapGetDB.u2WtpStatisticsTimer;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpEcnSupport ==
                    OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpEcnSupport =
                        pWssIfCapwapDB->CapwapGetDB.u1WtpEcnSupport;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDiscType == OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpDiscType =
                        pWssIfCapwapDB->CapwapGetDB.u1WtpDiscType;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWlcStaticIp == OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u4WlcStaticIp =
                        pWssIfCapwapDB->CapwapGetDB.u4WlcStaticIp;
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bRowStatus == OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].u1RowStatus =
                        pWssIfCapwapDB->CapwapGetDB.u1RowStatus;
                    if (gWssCapDB.CapwapGetDB[u2WtpProfileId].u1RowStatus ==
                        ACTIVE)
                    {
                        /* update the Wtp Board Data */
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u4VendorSMI =
                            0xFF;
                        /* COVERITY FIX START */
                        strncpy ((CHR1 *) gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                 au1WtpSerialNumber, "WTP-200000",
                                 sizeof ("WTP-200000"));
                        strncpy ((CHR1 *) gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                 au1WtpBoardId, "BID-300A",
                                 sizeof ("BID-300A"));
                        strncpy ((CHR1 *) gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                 au1WtpBoardRivision, "BOARD-REV:1.0",
                                 sizeof ("BOARD-REV:1.0"));
                        MEMCPY (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                au1WtpHWversion, IssGetHardwareVersion (), 50);
                        MEMCPY (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                au1WtpSWversion, IssGetSoftwareVersion (), 50);
                        strncpy ((CHR1 *) gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                 au1WtpBootversion, "BOOT VER1",
                                 sizeof ("BOOT VER1"));
                        /* COVERITY FIX END */
                    }
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpRebootStats ==
                    OSIX_TRUE)
                {
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].WtpRebootStats.
                        u2WtpRebootCount =
                        (UINT2) (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                 WtpRebootStats.u2WtpRebootCount +
                                 pWssIfCapwapDB->CapwapGetDB.WtpRebootStats.
                                 u2WtpRebootCount);
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].WtpRebootStats.
                        u2AcIntiatedCount =
                        (UINT2) (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                 WtpRebootStats.u2AcIntiatedCount +
                                 pWssIfCapwapDB->CapwapGetDB.WtpRebootStats.
                                 u2AcIntiatedCount);
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].WtpRebootStats.
                        u2WtpLinkFailureCount =
                        (UINT2) (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                 WtpRebootStats.u2WtpLinkFailureCount +
                                 pWssIfCapwapDB->CapwapGetDB.WtpRebootStats.
                                 u2WtpLinkFailureCount);
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].WtpRebootStats.
                        u2WtpSwFailureCount =
                        (UINT2) (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                 WtpRebootStats.u2WtpSwFailureCount +
                                 pWssIfCapwapDB->CapwapGetDB.WtpRebootStats.
                                 u2WtpSwFailureCount);
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].WtpRebootStats.
                        u2WtpHwFailureCount =
                        (UINT2) (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                 WtpRebootStats.u2WtpHwFailureCount +
                                 pWssIfCapwapDB->CapwapGetDB.WtpRebootStats.
                                 u2WtpHwFailureCount);
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].WtpRebootStats.
                        u2WtpOtherFailCount =
                        (UINT2) (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                 WtpRebootStats.u2WtpOtherFailCount +
                                 pWssIfCapwapDB->CapwapGetDB.WtpRebootStats.
                                 u2WtpOtherFailCount);
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].WtpRebootStats.
                        u2WtpUnknownFailCount =
                        (UINT2) (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                 WtpRebootStats.u2WtpUnknownFailCount +
                                 pWssIfCapwapDB->CapwapGetDB.WtpRebootStats.
                                 u2WtpUnknownFailCount);
                    gWssCapDB.CapwapGetDB[u2WtpProfileId].WtpRebootStats.
                        u1WtpLastFailureType =
                        (UINT1) (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                 WtpRebootStats.u1WtpLastFailureType +
                                 pWssIfCapwapDB->CapwapGetDB.WtpRebootStats.
                                 u1WtpLastFailureType);

                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bFreeAssocId == OSIX_TRUE)
                {
                    for (u4Index = 0; u4Index < pWssIfCapwapDB->CapwapGetDB.
                         u4FreeAssocId; u4Index++)
                    {
                        u1Index = u4Index / WSSIF_CAPWAP_VALUE_32;
                        if (u4Index == (pWssIfCapwapDB->CapwapGetDB.
                                        u4FreeAssocId - 1))
                        {
                            gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                u4AssocIdMapping[u1Index] = gWssCapDB.
                                CapwapGetDB[u2WtpProfileId].
                                u4AssocIdMapping[u1Index] & ~(0x00000001 <<
                                                              (u4Index %
                                                               WSSIF_CAPWAP_VALUE_32));
                            break;

                        }
                    }
                }

            }
            break;
        }
        case WSS_CAPWAP_VALIDATE_DB:
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId == OSIX_TRUE)
            {
                u2WtpProfileId = pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocation == OSIX_TRUE)
                {
                    u2Len =
                        (UINT2) STRLEN (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                        au1WtpLocation);
                    if (MEMCMP
                        (gWssCapDB.CapwapGetDB[u2WtpProfileId].au1WtpLocation,
                         pWssIfCapwapDB->CapwapGetDB.au1WtpLocation,
                         u2Len) != 0)
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "Failed to Validate the Location Data \r\n");
                        WSSIF_TRC2 (WSSIF_FAILURE_TRC,
                                    "Received Location = %s Configured Location = %s \r\n",
                                    pWssIfCapwapDB->CapwapGetDB.au1WtpLocation,
                                    gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                    au1WtpLocation);
                        return OSIX_FAILURE;
                    }
                }
                if (gWssCapDB.u1ModelCheck == CAPWAP_MODEL_CHECK_ENABLE)
                {
                    if (pWssIfCapwapDB->CapwapIsGetAllDB.bTunnelMode ==
                        OSIX_TRUE)
                    {
                        WSSIF_TRC2 (WSSIF_MGMT_TRC,
                                    "Received Tunnel mode = %d configured tunnel mode = %d \r\n",
                                    pWssIfCapwapDB->CapwapGetDB.u1WtpTunnelMode,
                                    gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                    u1WtpTunnelMode);
                        if (pWssIfCapwapDB->CapwapGetDB.u1WtpTunnelMode !=
                            gWssCapDB.CapwapGetDB[u2WtpProfileId].
                            u1WtpTunnelMode)
                        {
                            WSSIF_TRC (WSSIF_FAILURE_TRC,
                                       "The Received Tunnel mode is not Supported for this model \r\n");
                            return OSIX_FAILURE;
                        }
                    }
                    if (pWssIfCapwapDB->CapwapIsGetAllDB.bMacType == OSIX_TRUE)
                    {
                        WSSIF_TRC2 (WSSIF_MGMT_TRC,
                                    "Received Mac Type = %d cnfigured Mac Type = %d \r\n",
                                    pWssIfCapwapDB->CapwapGetDB.u1WtpMacType,
                                    gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                    u1WtpMacType);
                        if (pWssIfCapwapDB->CapwapGetDB.u1WtpMacType !=
                            gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpMacType)
                        {
                            WSSIF_TRC (WSSIF_FAILURE_TRC,
                                       "The Received Mac Type is not supported for this model \r\n");
                            return OSIX_FAILURE;
                        }
                    }
                }

                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpEcnSupport ==
                    OSIX_TRUE)
                {
                    if (pWssIfCapwapDB->CapwapGetDB.u1WtpEcnSupport !=
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpEcnSupport)
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "The Received ECN mode is not supported \r\n");
                        return OSIX_FAILURE;
                    }
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bAcNamewithPriority ==
                    OSIX_TRUE)
                {
                    if (pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].
                        u1Priority ==
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].AcNameWithPri[0].
                        u1Priority)
                    {
                        u2Len =
                            (UINT2) (STRLEN
                                     (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                      AcNameWithPri[0].wlcName));
                        if (MEMCMP
                            (pWssIfCapwapDB->CapwapGetDB.AcNameWithPri[0].
                             wlcName,
                             gWssCapDB.CapwapGetDB[u2WtpProfileId].
                             AcNameWithPri[0].wlcName, u2Len) == 0)
                        {
                            WSSIF_TRC (WSSIF_FAILURE_TRC,
                                       "Key value not present. Creation failed \r\n");
                            return OSIX_FAILURE;
                        }
                    }
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpDiscType == OSIX_TRUE)
                {
                    if (pWssIfCapwapDB->CapwapGetDB.u1WtpDiscType !=
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u1WtpDiscType)
                    {
                        WSSIF_TRC (WSSIF_MGMT_TRC,
                                   "The Received Discovery Type is not same as the DB \r\n");
                        return OSIX_FAILURE;
                    }
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bAcName == OSIX_TRUE)
                {
                    u2Len = (UINT2) STRLEN (pWssIfCapwapDB->au1WlcName);
                    if (MEMCMP (pWssIfCapwapDB->au1WlcName,
                                gWssCapDB.au1WlcName, u2Len) != 0)
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "The Received AC Name is not same as DB \r\n");
                        return OSIX_FAILURE;
                    }
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress ==
                    OSIX_TRUE)
                {
                    if (MEMCMP (pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
                                gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                WtpMacAddress, MAC_ADDR_LEN) != 0)
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "The Received MAC is not same as DB \r\n");
                        return OSIX_FAILURE;
                    }
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpModelNumber ==
                    OSIX_TRUE)
                {
                    u2Len =
                        (((UINT2)
                          STRLEN (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                  au1WtpModelNumber)) >
                         ((UINT2)
                          STRLEN (pWssIfCapwapDB->CapwapGetDB.
                                  au1WtpModelNumber))) ? ((UINT2)
                                                          STRLEN (gWssCapDB.
                                                                  CapwapGetDB
                                                                  [u2WtpProfileId].
                                                                  au1WtpModelNumber))
                        : ((UINT2)
                           STRLEN (pWssIfCapwapDB->CapwapGetDB.
                                   au1WtpModelNumber));

                    if (MEMCMP
                        (pWssIfCapwapDB->CapwapGetDB.au1WtpModelNumber,
                         gWssCapDB.CapwapGetDB[u2WtpProfileId].
                         au1WtpModelNumber, u2Len) != 0)
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "The Received Model is not same as DB \r\n");
                        return OSIX_FAILURE;
                    }
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpSerialNumber ==
                    OSIX_TRUE)
                {
                    u2Len =
                        (((UINT2)
                          STRLEN (gWssCapDB.CapwapGetDB[u2WtpProfileId].
                                  au1WtpSerialNumber)) >
                         ((UINT2)
                          STRLEN (pWssIfCapwapDB->CapwapGetDB.
                                  au1WtpSerialNumber))) ? ((UINT2)
                                                           STRLEN (gWssCapDB.
                                                                   CapwapGetDB
                                                                   [u2WtpProfileId].
                                                                   au1WtpSerialNumber))
                        : ((UINT2)
                           STRLEN (pWssIfCapwapDB->CapwapGetDB.
                                   au1WtpSerialNumber));
                    if (MEMCMP
                        (pWssIfCapwapDB->CapwapGetDB.au1WtpSerialNumber,
                         gWssCapDB.CapwapGetDB[u2WtpProfileId].
                         au1WtpSerialNumber, u2Len) != 0)
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "The Received Serial Number is not same as DB \r\n");
                        return OSIX_FAILURE;
                    }
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpBoardId == OSIX_TRUE)
                {
                    u2Len =
                        (UINT2) STRLEN (pWssIfCapwapDB->CapwapGetDB.
                                        au1WtpBoardId);
                    if (MEMCMP
                        (pWssIfCapwapDB->CapwapGetDB.au1WtpBoardId,
                         gWssCapDB.CapwapGetDB[u2WtpProfileId].au1WtpBoardId,
                         u2Len) != 0)
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "The Received Board Id is not same as DB \r\n");
                        return OSIX_FAILURE;
                    }
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpBoardRivision ==
                    OSIX_TRUE)
                {
                    u2Len =
                        (UINT2) STRLEN (pWssIfCapwapDB->CapwapGetDB.
                                        au1WtpBoardRivision);
                    if (MEMCMP
                        (pWssIfCapwapDB->CapwapGetDB.au1WtpBoardRivision,
                         gWssCapDB.CapwapGetDB[u2WtpProfileId].
                         au1WtpBoardRivision, u2Len) != 0)
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "The Received Board Rivision not same as DB \r\n");
                        return OSIX_FAILURE;
                    }
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpIdleTimeout ==
                    OSIX_TRUE)
                {
                    if (pWssIfCapwapDB->CapwapGetDB.u4WtpIdleTimeout !=
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u4WtpIdleTimeout)
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "The Received Idle Timeout is not same as DB \r\n");
                        return OSIX_FAILURE;
                    }
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpFallbackEnable ==
                    OSIX_TRUE)
                {
                    if (pWssIfCapwapDB->CapwapGetDB.u1WtpFallbackEnable !=
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].
                        u1WtpFallbackEnable)
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "The Received Wtp Fallback is not same as DB \r\n");
                        return OSIX_FAILURE;
                    }
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpName == OSIX_TRUE)
                {
                    u2Len =
                        (UINT2) STRLEN (pWssIfCapwapDB->CapwapGetDB.au1WtpName);
                    if (STRCMP
                        (pWssIfCapwapDB->CapwapGetDB.au1WtpName,
                         gWssCapDB.CapwapGetDB[u2WtpProfileId].au1WtpName) != 0)
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "The Received WTP name not same as DB \r\n");
                        return OSIX_FAILURE;
                    }
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpStaticIpEnable ==
                    OSIX_TRUE)
                {
                    if (pWssIfCapwapDB->CapwapGetDB.u1WtpStaticIpEnable !=
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].
                        u1WtpStaticIpEnable)
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "The Received Wtp static Ip enable is not same as DB \r\n");
                        return OSIX_FAILURE;
                    }
                    if (pWssIfCapwapDB->CapwapGetDB.WtpStaticIpAddress.
                        u4_addr[0] !=
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].
                        WtpStaticIpAddress.u4_addr[0])
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "The Received Wtp static Ip address is not same as DB \r\n");
                        return OSIX_FAILURE;

                    }
                    if (pWssIfCapwapDB->CapwapGetDB.WtpNetmask.u4_addr[0] !=
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].WtpNetmask.
                        u4_addr[0])
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "The Received Wtp static Net Mask is not same as DB \r\n");
                        return OSIX_FAILURE;
                    }
                    if (pWssIfCapwapDB->CapwapGetDB.WtpGateway.u4_addr[0] !=
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].WtpGateway.
                        u4_addr[0])
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "The Received Wtp static gate way is not same as DB \r\n");
                        return OSIX_FAILURE;
                    }
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMaxDiscoveryInterval ==
                    OSIX_TRUE)
                {
                    if (pWssIfCapwapDB->CapwapGetDB.u2WtpMaxDiscoveryInterval !=
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].
                        u2WtpMaxDiscoveryInterval)
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "The Received Discovery Interval Time is not same as DB \r\n");
                        return OSIX_FAILURE;
                    }
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpEchoInterval ==
                    OSIX_TRUE)
                {
                    if (pWssIfCapwapDB->CapwapGetDB.u2WtpEchoInterval !=
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].u2WtpEchoInterval)
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "The Received Echo interval is not same as DB \r\n");
                        return OSIX_FAILURE;
                    }
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpReportInterval ==
                    OSIX_TRUE)
                {
                    if (pWssIfCapwapDB->CapwapGetDB.u2WtpReportInterval !=
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].
                        u2WtpReportInterval)
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "The Received Echo interval is not same as DB \r\n");
                        return OSIX_FAILURE;
                    }
                }
            }
            break;
        case WSS_CAPWAP_CREATE_PROFILE_ENTRY:
        {
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId != OSIX_TRUE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Profile Id value not present. Creation failed \r\n");
                return OSIX_FAILURE;
            }
            else
            {
                u2ProfileId = pWssIfCapwapDB->CapwapGetDB.u2ProfileId;
            }
            if (WssIfSetProfildIndex (u2ProfileId) == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Failed to Get Free WTP Profile Index \r\n ");
                return OSIX_FAILURE;
            }
            if (WssIfGetProfileIndex (u2ProfileId, &u2WtpProfileId) ==
                OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Failed to Get Free WTP Profile Index \r\n ");
                return OSIX_FAILURE;
            }
            gWssCapDB.CapwapGetDB[u2WtpProfileId].u1FragReassembleStatus =
                OSIX_TRUE;
            break;
        }
        case WSS_CAPWAP_DESTROY_PROFILE_ENTRY:
        {
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId != OSIX_TRUE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Profile Id value not present. Creation failed \r\n");
                return OSIX_FAILURE;
            }
            else
            {
                u2ProfileId = pWssIfCapwapDB->CapwapGetDB.u2ProfileId;
            }

#ifdef KERNEL_CAPWAP_WANTED
#ifdef WLC_WANTED
/*calling kernel remove function */
            if (pWssIfCapwapDB->pSessEntry != NULL)
            {
                CapwapRemoveKernelIpAndMacTable
                    (pWssIfCapwapDB->pSessEntry->remoteIpAddr.u4_addr[0]);
            }
#endif
#endif
            if (WssIfGetProfileIndex (u2ProfileId, &u2WtpProfileId) ==
                OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Failed to Get Free WTP Profile Index \r\n ");
                return OSIX_FAILURE;
            }
            if (WssIfDeleteProfileIndex (u2ProfileId) == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Failed to Get Free WTP Profile Index \r\n ");
                return OSIX_FAILURE;
            }
            MEMSET (&gWssCapDB.CapwapGetDB[u2WtpProfileId], 0,
                    sizeof (tCapwapGetAllDB));
            MEMSET (&gaWtpIntProfile[u2WtpProfileId], 0,
                    sizeof (tWtpIntProfile));
            gu2WtpProfileId--;
            break;
        }
        case WSS_CAPWAP_GET_RSM:
        {
            pSessEntry =
                WssIfRemoteSessionGet (pWssIfCapwapDB->u4DestIp,
                                       pWssIfCapwapDB->u4DestPort);
            pWssIfCapwapDB->pSessEntry = pSessEntry;
            break;

        }
        case WSS_CAPWAP_GET_RSM_FROM_CAPWAPID:
        {
            pSessEntry =
                WssIfGetRsmFromCapwapId (pWssIfCapwapDB->u2CapwapRSMId);
            pWssIfCapwapDB->pSessEntry = pSessEntry;
            break;
        }
        case WSS_CAPWAP_GET_RSM_FROM_SESS_IDX:
        {
            pSessEntry = &gaRemoteSessionManager[pWssIfCapwapDB->u2CapwapRSMId];
            pWssIfCapwapDB->pSessEntry = pSessEntry;
            break;
        }
        case WSS_CAPWAP_GET_RSM_FROM_INDEX:
        {
            pSessProfileIdEntry =
                WssIfSessionProfileIdEntryGet (pWssIfCapwapDB->CapwapGetDB.
                                               u2WtpInternalId);

            if (pSessProfileIdEntry == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Received the null pointer from Session Profile Tabler \r\n");
                pWssIfCapwapDB->pSessEntry = NULL;
            }
            else
            {
                pSessEntry = WssIfGetRsmFromProfileId (pSessProfileIdEntry);
                pWssIfCapwapDB->pSessEntry = pSessEntry;
            }
            break;
        }
        case WSS_CAPWAP_DELETE_RSM:
        {
            if (WssIfRemoteSessionDelete (pWssIfCapwapDB->u4DestIp,
                                          pWssIfCapwapDB->u4DestPort) ==
                OSIX_SUCCESS)
            {
                if (gWssCapDB.u2ActiveWtps > 0)
                {
                    gWssCapDB.u2ActiveWtps--;
                }
            }
            break;
        }
        case WSS_CAPWAP_CREATE_RSM:
        {
            pSessEntry =
                WssIfRemoteSessionCreate (pWssIfCapwapDB->u4DestIp,
                                          pWssIfCapwapDB->u4DestPort);
            pWssIfCapwapDB->pSessEntry = pSessEntry;
            break;
        }
        case WSS_CAPWAP_CREATE_SESSION_PORT_ENTRY:
        {
            /* Create the entry in the Wtp Profile RB Tree */
            if (WssIfSessionPortEntryCreate
                (pWssIfCapwapDB->u4DestIp, pWssIfCapwapDB->u4DestPort,
                 pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId) == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Failed to create the entry in Wtp Profile Session Entry \r\n");
                return OSIX_FAILURE;
            }
            break;
        }
        case WSS_CAPWAP_DELETE_SESSION_PORT_ENTRY:
        {
            /* Delete the entry in the Wtp Profile RB Tree */
            if (WssIfSessionPortEntryDelete
                (pWssIfCapwapDB->u4DestIp,
                 (UINT2) (pWssIfCapwapDB->u4DestPort)) == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Failed to delete the entry in Wtp Profile Session Entry \r\n");
                return OSIX_FAILURE;
            }
            break;
        }
        case WSS_CAPWAP_GET_SESSION_PORT_ENTRY:
        {
            /* Get the entry in the Wtp Profile RB Tree */
            pSessDestIpEntry =
                WssIfSessionPortEntryGet (pWssIfCapwapDB->u4DestIp,
                                          pWssIfCapwapDB->u4DestPort);
            pWssIfCapwapDB->pSessDestIpEntry = pSessDestIpEntry;
            break;
        }
        case WSS_CAPWAP_CREATE_SESSPROFID_ENTRY:
        {
            WssIfSessionProfileIdEntryCreate (pWssIfCapwapDB->u2CapwapRSMId,
                                              pWssIfCapwapDB->CapwapGetDB.
                                              u2WtpInternalId);
            break;
        }
        case WSS_CAPWAP_DELETE_SESSPROFID_ENTRY:
        {
            WssIfSessionProfileIdEntryDelete (pWssIfCapwapDB->CapwapGetDB.
                                              u2WtpInternalId);
            break;
        }
        case WSS_CAPWAP_GET_SESSPROFID_ENTRY:
        {
            pSessProfileIdEntry =
                WssIfSessionProfileIdEntryGet (pWssIfCapwapDB->CapwapGetDB.
                                               u2WtpInternalId);
            if (pSessProfileIdEntry == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Not found the entry in Session Profile Entry Table \r\n");
                pWssIfCapwapDB->pSessProfileIdEntry = NULL;
            }
            else
            {
                pWssIfCapwapDB->pSessProfileIdEntry = pSessProfileIdEntry;
            }
            break;
        }
        case WSS_CAPWAP_CREATE_DSCP_MAP_ENTRY:
        {
#ifdef WTP_WANTED
            /* Create the entry in the Dscp Map RB Tree */
            if (WssIfCapwapDscpMapEntryCreate
                (pWssIfCapwapDB->u4IfIndex, pWssIfCapwapDB->u1InPriority,
                 pWssIfCapwapDB->u1OutDscp) == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Failed to create the entry in Dscp Map Entry \r\n");
                return OSIX_FAILURE;
            }
#endif
            break;
        }
        case WSS_CAPWAP_DELETE_DSCP_MAP_ENTRY:
        {
#ifdef WTP_WANTED
            /* Delete the entry in the Dscp Map RB Tree */
            if (WssIfCapwapDscpMapEntryDelete
                (pWssIfCapwapDB->u4IfIndex,
                 pWssIfCapwapDB->u1InPriority) == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Failed to delete the entry in Dscp Map Entry \r\n");
                return OSIX_FAILURE;
            }
#endif
            break;
        }
        case WSS_CAPWAP_DELETE_RSM_FROM_SESSIONID:
        {
            if (WssIfDeleteRSMFromJoinSessionId (pWssIfCapwapDB) ==
                OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Failed to Delete RSM based on Join Session Id \r\n");
                return OSIX_FAILURE;
            }
            break;
        }
        case WSS_CAPWAP_GET_RSM_FROM_SESSIONID:
        {
            if (WssIfGetRSMFromJoinSessionId (pWssIfCapwapDB) == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Failed to Get RSM based on Join Session Id \r\n");
                return OSIX_FAILURE;
            }
            break;
        }
        case WSS_CAPWAP_GET_CTRLUDP_PORT:
        {
            if (gWssCapDB.u4CtrludpPort == 0)
            {
                return OSIX_FAILURE;
            }

            pWssIfCapwapDB->u4CtrludpPort = gWssCapDB.u4CtrludpPort;
            break;
        }
        case WSS_CAPWAP_CREATE_TEMPDTLS_ENTRY:
            if (WssIfCapwapDtlsEntryCreate (pWssIfCapwapDB->u4DestIp,
                                            pWssIfCapwapDB->u4DtlsId,
                                            pWssIfCapwapDB->BaseMacAddress) ==
                OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Failed to create the enntry in DTLS session table \r\n");
                return OSIX_FAILURE;
            }
            break;
        case WSS_CAPWAP_DELETE_TEMPDTLS_ENTRY:
            if (WssIfCapwapDtlsEntryDelete
                (pWssIfCapwapDB->u4DestIp,
                 pWssIfCapwapDB->u4DtlsId) == OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Failed to Delete the entry from DTLS Temporary Table \r\n");
                return OSIX_FAILURE;
            }
            break;
        case WSS_CAPWAP_GET_TEMPDTLS_ENTRY:
            pCapwapDtlsSessEntry =
                WssIfCapwapDtlsEntryGet (pWssIfCapwapDB->u4DestIp);
            if (pCapwapDtlsSessEntry == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "DTLS Not found the entry in Session Profile Entry Table \r\n");
                pWssIfCapwapDB->pDtlsEntry = NULL;
            }
            else
            {
                pWssIfCapwapDB->pDtlsEntry = pCapwapDtlsSessEntry;
            }

            break;

        case WSS_CAPWAP_SHOW_WTPSTATE_TABLE:
        {
#ifdef WLC_WANTED
            u2ProfileId = pWssIfCapwapDB->CapwapGetDB.u2WtpProfileId;
            if (WssIfGetProfileIndex (u2ProfileId, &u2WtpProfileId) !=
                OSIX_SUCCESS)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfGetProfileIndex Failed  \r\n");
                return OSIX_FAILURE;
            }
            pSessProfileIdEntry =
                WssIfSessionProfileIdEntryGet (u2WtpProfileId);
            if (pSessProfileIdEntry == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "SHOW_WTPSTATE: "
                           "Received the null pointer from Session Profile Tabler \r\n");
            }
            pSessEntry = WssIfGetRsmFromProfileId (pSessProfileIdEntry);
            if (pSessEntry == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Received the null pointer RSM profile DB \r\n");
                return OSIX_FAILURE;
            }
#else
            pSessEntry =
                WssIfRemoteSessionGet (pWssIfCapwapDB->u4DestIp,
                                       pWssIfCapwapDB->u4DestPort);
            pWssIfCapwapDB->pSessEntry = pSessEntry;
#endif
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bIpAddressType == OSIX_TRUE)
            {
                pWssIfCapwapDB->CapwapGetDB.u1WtpStaticIpType = 1;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bIpAddress == OSIX_TRUE)
            {
                pWssIfCapwapDB->CapwapGetDB.WtpStaticIpAddress.u4_addr[0] =
                    pSessEntry->remoteIpAddr.u4_addr[0];
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bCapwapState == OSIX_TRUE)
            {
                pWssIfCapwapDB->CapwapGetDB.u4CapwapState =
                    pSessEntry->eCurrentState;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress == OSIX_TRUE)
            {
                MEMCPY (pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
                        gWssCapDB.CapwapGetDB[u2WtpProfileId].WtpMacAddress,
                        MAC_ADDR_LEN);
            }
            break;
        }
        case WSS_CAPWAP_CREATE_PROFILE_MAPPING_ENTRY:

            /* Add entry with the incoming IP address to the RB tree */
            pWssIfProfileNameMap = (tWssIfProfileNameMap *)
                MemAllocMemBlk (CAPWAP_WTPPROFILENAME_POOLID);

            if (pWssIfProfileNameMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessCapwapDBMsg:Memory allocation Failure \r\n");
                return OSIX_FAILURE;
            }
            MEMSET (pWssIfProfileNameMap, 0, sizeof (tWssIfProfileNameMap));
            pWssIfProfileNameMap->u2WtpProfileId =
                pWssIfCapwapDB->CapwapGetDB.u2ProfileId;
            MEMCPY (pWssIfProfileNameMap->au1WtpProfileName,
                    pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                    STRLEN (pWssIfCapwapDB->CapwapGetDB.au1ProfileName));

            if (RBTreeAdd (gCapProfileNameMapTable, pWssIfProfileNameMap) !=
                RB_SUCCESS)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessCapwapDBMsg: Failed to add entry to the RB Tree \r\n");
                MemReleaseMemBlock (CAPWAP_WTPPROFILENAME_POOLID,
                                    (UINT1 *) pWssIfProfileNameMap);
                return OSIX_FAILURE;
            }
            break;
        case WSS_CAPWAP_DELETE_PROFILE_MAPPING_ENTRY:
            MEMCPY (wtpProfileNameEntry.au1WtpProfileName,
                    pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                    STRLEN (pWssIfCapwapDB->CapwapGetDB.au1ProfileName));
            pWssIfProfileNameMap =
                RBTreeGet (gCapProfileNameMapTable,
                           (tRBElem *) & wtpProfileNameEntry);
            if (pWssIfProfileNameMap == NULL)
            {
                WSSIF_TRC (WSSIF_MGMT_TRC,
                           "\r\n WssIfProcessCapwapDBMsg: Entry not present for Profile name DB\r\n");
                return OSIX_FAILURE;
            }

            RBTreeRem (gCapProfileNameMapTable,
                       (tRBElem *) pWssIfProfileNameMap);
            MemReleaseMemBlock (CAPWAP_WTPPROFILENAME_POOLID,
                                (UINT1 *) pWssIfProfileNameMap);
            break;

        case WSS_CAPWAP_GET_PROFILE_MAPPING_ENTRY:
        {
            MEMCPY (wtpProfileNameEntry.au1WtpProfileName,
                    pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                    STRLEN (pWssIfCapwapDB->CapwapGetDB.au1ProfileName));

            pWssIfProfileNameMap =
                RBTreeGet (gCapProfileNameMapTable,
                           (tRBElem *) & wtpProfileNameEntry);
            if (pWssIfProfileNameMap == NULL)
            {
                WSSIF_TRC (WSSIF_MGMT_TRC,
                           "\r\n WssIfProcessCapwapDBMsg: Entry not present for Profile name DB\r\n");
                return OSIX_FAILURE;
            }
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                pWssIfProfileNameMap->u2WtpProfileId;
            break;
        }
        case WSS_CAPWAP_GET_PROFILEID_MAPPING_ENTRY:
        {
            pWssIfProfileGroupMap = RBTreeGetFirst (gCapProfileNameMapTable);
            if (pWssIfProfileGroupMap != NULL)
            {
                do
                {
                    if (pWssIfProfileGroupMap->u2WtpProfileId ==
                        pWssIfCapwapDB->CapwapGetDB.u2ProfileId)
                    {
                        MEMCPY (pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                                pWssIfProfileGroupMap->au1WtpProfileName,
                                STRLEN (pWssIfProfileGroupMap->
                                        au1WtpProfileName));
                        break;
                    }

                    MEMCPY (wtpGroupNameEntry.au1WtpProfileName,
                            pWssIfProfileGroupMap->au1WtpProfileName,
                            STRLEN (pWssIfProfileGroupMap->au1WtpProfileName));
                    wtpGroupNameEntry.u2WtpProfileId = pWssIfProfileGroupMap->
                        u2WtpProfileId;
                    pWssIfProfileGroupMap =
                        RBTreeGetNext (gCapProfileNameMapTable,
                                       (tRBElem *) & wtpGroupNameEntry, NULL);

                }
                while (pWssIfProfileGroupMap != NULL);
            }

            break;
        }

        case WSS_CAPWAP_CREATE_MACADDR_MAPPING_ENTRY:
            /* Add entry with the incoming IP address to the RB tree */
            pWssIfProfileMacMap = (tWssIfProfileMacMap *)
                MemAllocMemBlk (CAPWAP_WTPPROFILEMAC_POOLID);

            if (pWssIfProfileMacMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessCapwapDBMsg:Memory allocation Failure \r\n");
                return OSIX_FAILURE;
            }
            MEMSET (pWssIfProfileMacMap, 0, sizeof (tWssIfProfileMacMap));
            pWssIfProfileMacMap->u2WtpProfileId =
                pWssIfCapwapDB->CapwapGetDB.u2ProfileId;
            MEMCPY (pWssIfProfileMacMap->MacAddr,
                    pWssIfCapwapDB->CapwapGetDB.WtpMacAddress, MAC_ADDR_LEN);

            if (RBTreeAdd (gCapProfileMacMapTable, pWssIfProfileMacMap) !=
                RB_SUCCESS)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessCapwapDBMsg: Failed to add entry to the RB Tree \r\n");
                MemReleaseMemBlock (CAPWAP_WTPPROFILEMAC_POOLID,
                                    (UINT1 *) pWssIfProfileMacMap);
                return OSIX_FAILURE;
            }
            break;
        case WSS_CAPWAP_DELETE_MACADDR_MAPPING_ENTRY:
            MEMCPY (wtpProfileMacEntry.MacAddr,
                    pWssIfCapwapDB->CapwapGetDB.WtpMacAddress, MAC_ADDR_LEN);
            pWssIfProfileMacMap =
                RBTreeGet (gCapProfileMacMapTable,
                           (tRBElem *) & wtpProfileMacEntry);
            if (pWssIfProfileMacMap == NULL)
            {
                WSSIF_TRC (WSSIF_MGMT_TRC,
                           "\r\n WssIfProcessCapwapDBMsg: Entry not present for mapping DB\r\n");
                return OSIX_FAILURE;
            }

            RBTreeRem (gCapProfileMacMapTable, (tRBElem *) pWssIfProfileMacMap);
            MemReleaseMemBlock (CAPWAP_WTPPROFILEMAC_POOLID,
                                (UINT1 *) pWssIfProfileMacMap);
            break;

        case WSS_CAPWAP_GET_MACADDR_MAPPING_ENTRY:
        {
            MEMCPY (wtpProfileMacEntry.MacAddr,
                    pWssIfCapwapDB->CapwapGetDB.WtpMacAddress, MAC_ADDR_LEN);

            pWssIfProfileMacMap =
                RBTreeGet (gCapProfileMacMapTable,
                           (tRBElem *) & wtpProfileMacEntry);
            if (pWssIfProfileMacMap == NULL)
            {
                WSSIF_TRC (WSSIF_MGMT_TRC,
                           "\r\n WssIfProcessCapwapDBMsg: Entry not present for mapping DB\r\n");
                return OSIX_FAILURE;
            }
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                pWssIfProfileMacMap->u2WtpProfileId;
            break;
        }
        case WSS_CAPWAP_SET_WTP_MODEL_ENTRY:
        {
            MEMSET (&WtpModelMap, 0, sizeof (tWssIfWtpModelMap));
            u2Len =
                (UINT2) (STRLEN
                         (pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.
                          au1ModelNumber));
            MEMCPY (WtpModelMap.au1ModelNumber,
                    pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.au1ModelNumber,
                    u2Len);
            pWtpModelMap =
                ((tWssIfWtpModelMap *)
                 RBTreeGet (gCapWtpModelMapTable, (tRBElem *) & WtpModelMap));
            if (pWtpModelMap == NULL)
            {
                WSSIF_TRC (WSSIF_MGMT_TRC,
                           "The Given Model Entry is not present in WTP Model Table \r\n");
                return OSIX_FAILURE;
            }
            u2ModelIndex = pWtpModelMap->u2ModelIndex;
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio == OSIX_TRUE)
            {
                gWssCapDB.WtpModelEntry[u2ModelIndex].u1NumOfRadio =
                    pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.u1NumOfRadio;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bRadioCount == OSIX_TRUE)
            {
                u1NoOfRadio =
                    pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.u1NumOfRadio;
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bRadioAdminStatus ==
                    OSIX_TRUE)
                {
                    gWssCapDB.WtpModelEntry[u2ModelIndex].
                        au1WtpRadioAdminStatus[u1NoOfRadio - 1] =
                        pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.
                        au1WtpRadioAdminStatus[u1NoOfRadio - 1];
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpRadioType == OSIX_TRUE)
                {
                    gWssCapDB.WtpModelEntry[u2ModelIndex].
                        au4WtpRadioType[u1NoOfRadio - 1] =
                        pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.
                        au4WtpRadioType[u1NoOfRadio - 1];
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bMaxSSIDSupported ==
                    OSIX_TRUE)
                {
                    gWssCapDB.WtpModelEntry[u2ModelIndex].
                        au1WtpMaxSSIDSupported[u1NoOfRadio - 1] =
                        pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.
                        au1WtpMaxSSIDSupported[u1NoOfRadio - 1];
                }
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bMacType == OSIX_TRUE)
            {
                gWssCapDB.WtpModelEntry[u2ModelIndex].u1MacType =
                    pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.u1MacType;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bTunnelMode == OSIX_TRUE)
            {
                gWssCapDB.WtpModelEntry[u2ModelIndex].u1TunnelMode =
                    pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.u1TunnelMode;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bMaxStationsSupported ==
                OSIX_TRUE)
            {
                gWssCapDB.WtpModelEntry[u2ModelIndex].u2MaxStationsSupported =
                    pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.
                    u2MaxStationsSupported;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpQosPofile == OSIX_TRUE)
            {
                u2Len =
                    (UINT2) (STRLEN
                             (pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.
                              au1WtpQosProfildName));
                MEMCPY (gWssCapDB.WtpModelEntry[u2ModelIndex].
                        au1WtpQosProfildName,
                        pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.
                        au1WtpQosProfildName, u2Len);
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bImageName == OSIX_TRUE)
            {
                u2Len =
                    (UINT2) (STRLEN
                             (pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.
                              au1WtpImageName));
                MEMCPY (gWssCapDB.WtpModelEntry[u2ModelIndex].au1WtpImageName,
                        pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.
                        au1WtpImageName, u2Len);
                gWssCapDB.WtpModelEntry[u2ModelIndex].u1WtpImageLen =
                    (UINT1) u2Len;
            }

            if (pWssIfCapwapDB->CapwapIsGetAllDB.bRowStatus == OSIX_TRUE)
            {
                gWssCapDB.WtpModelEntry[u2ModelIndex].u1RowStatus =
                    pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.u1RowStatus;
                MEMCPY (gWssCapDB.WtpModelEntry[u2ModelIndex].
                        au1WtpHwVersion, IssGetHardwareVersion (), 50);
                MEMCPY (gWssCapDB.WtpModelEntry[u2ModelIndex].
                        au1WtpSwVersion, IssGetSoftwareVersion (), 50);
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bLocalRouting == OSIX_TRUE)
            {
                gWssCapDB.WtpModelEntry[u2ModelIndex].u1LocalRouting =
                    pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.u1LocalRouting;
            }
            break;
        }
        case WSS_CAPWAP_GET_WTP_MODEL_ENTRY:
        {
            MEMSET (&WtpModelMap, 0, sizeof (tWssIfWtpModelMap));
            u2Len =
                (UINT2) (STRLEN
                         (pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.
                          au1ModelNumber));
            MEMCPY (WtpModelMap.au1ModelNumber,
                    pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.au1ModelNumber,
                    u2Len);
            pWtpModelMap =
                ((tWssIfWtpModelMap *)
                 RBTreeGet (gCapWtpModelMapTable, (tRBElem *) & WtpModelMap));
            if (pWtpModelMap == NULL)
            {
                WSSIF_TRC (WSSIF_MGMT_TRC,
                           "The Given Model Entry is not present in WTP Model Table \r\n");
                return OSIX_FAILURE;
            }
            u2ModelIndex = pWtpModelMap->u2ModelIndex;
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bNoOfRadio == OSIX_TRUE)
            {
                pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.u1NumOfRadio =
                    gWssCapDB.WtpModelEntry[u2ModelIndex].u1NumOfRadio;

                u1NoOfRadio =
                    gWssCapDB.WtpModelEntry[u2ModelIndex].u1NumOfRadio;
                for (u1Index = 0; u1Index < u1NoOfRadio; u1Index++)
                {
                    if (pWssIfCapwapDB->CapwapIsGetAllDB.bRadioAdminStatus ==
                        OSIX_TRUE)
                    {
                        pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.
                            au1WtpRadioAdminStatus[u1Index] =
                            gWssCapDB.WtpModelEntry[u2ModelIndex].
                            au1WtpRadioAdminStatus[u1Index];
                    }
                    if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpRadioType ==
                        OSIX_TRUE)
                    {
                        pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.
                            au4WtpRadioType[u1Index] =
                            gWssCapDB.WtpModelEntry[u2ModelIndex].
                            au4WtpRadioType[u1Index];
                    }
                    if (pWssIfCapwapDB->CapwapIsGetAllDB.bMaxSSIDSupported ==
                        OSIX_TRUE)
                    {
                        pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.
                            au1WtpMaxSSIDSupported[u1Index] =
                            gWssCapDB.WtpModelEntry[u2ModelIndex].
                            au1WtpMaxSSIDSupported[u1Index];
                    }
                }
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bMacType == OSIX_TRUE)
            {
                pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.u1MacType =
                    gWssCapDB.WtpModelEntry[u2ModelIndex].u1MacType;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bTunnelMode == OSIX_TRUE)
            {
                pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.u1TunnelMode =
                    gWssCapDB.WtpModelEntry[u2ModelIndex].u1TunnelMode;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bMaxStationsSupported ==
                OSIX_TRUE)
            {
                pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.
                    u2MaxStationsSupported =
                    gWssCapDB.WtpModelEntry[u2ModelIndex].
                    u2MaxStationsSupported;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpMacAddress == OSIX_TRUE)
            {
                MEMCPY (pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
                        gWssCapDB.CapwapGetDB[u2ModelIndex].WtpMacAddress,
                        MAC_ADDR_LEN);
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bImageName == OSIX_TRUE)
            {
                MEMCPY (pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.
                        au1WtpImageName,
                        gWssCapDB.WtpModelEntry[u2ModelIndex].au1WtpImageName,
                        gWssCapDB.WtpModelEntry[u2ModelIndex].u1WtpImageLen);

                pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.u1WtpImageLen =
                    gWssCapDB.WtpModelEntry[u2ModelIndex].u1WtpImageLen;
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bSwVersion == OSIX_TRUE)
            {
                MEMCPY (pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.
                        au1WtpSwVersion,
                        gWssCapDB.WtpModelEntry[u2ModelIndex].au1WtpSwVersion,
                        STRLEN (gWssCapDB.WtpModelEntry[u2ModelIndex].
                                au1WtpSwVersion));
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bHwVersion == OSIX_TRUE)
            {
                MEMCPY (pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.
                        au1WtpHwVersion,
                        gWssCapDB.WtpModelEntry[u2ModelIndex].au1WtpHwVersion,
                        STRLEN (gWssCapDB.WtpModelEntry[u2ModelIndex].
                                au1WtpHwVersion));
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bLocalRouting == OSIX_TRUE)
            {
                pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.u1LocalRouting =
                    gWssCapDB.WtpModelEntry[u2ModelIndex].u1LocalRouting;
            }
            break;
        }
        case WSS_CAPWAP_DESTROY_WTP_MODEL_ENTRY:
        {
            MEMSET (&WtpModelMap, 0, sizeof (tWssIfWtpModelMap));
            u2Len =
                (UINT2) (STRLEN
                         (pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.
                          au1ModelNumber));
            MEMCPY (WtpModelMap.au1ModelNumber,
                    pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.au1ModelNumber,
                    u2Len);
            pWtpModelMap =
                ((tWssIfWtpModelMap *)
                 RBTreeGet (gCapWtpModelMapTable, (tRBElem *) & WtpModelMap));
            if (pWtpModelMap == NULL)
            {
                WSSIF_TRC (WSSIF_MGMT_TRC,
                           "The Given Model Entry is not present in WTP Model Table \r\n");
                return OSIX_FAILURE;
            }
            /* Check All the WTP profiles, if Any of the WTP profiles using this model table delete should not be allowed */
            u2Len =
                (UINT2) (STRLEN
                         (pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.
                          au1ModelNumber));
            for (u2Index = 0; u2Index < MAX_WTP_PROFILE_SUPPORTED; u2Index++)
            {
                if ((MEMCMP
                     (&gWssCapDB.CapwapGetDB[u2Index].WtpModelEntry.
                      au1ModelNumber,
                      &pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.au1ModelNumber,
                      u2Len)) == 0)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC,
                               "This Model Table is in use by the WTP Profile, Cannot the deleted \r\n");
                    return OSIX_FAILURE;
                }
            }
            u2ModelIndex = pWtpModelMap->u2ModelIndex;
            MEMSET (&gWssCapDB.WtpModelEntry[u2ModelIndex], 0,
                    sizeof (tWtpModelEntry));
            if (WssIfWtpModelEntryDelete
                (pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.au1ModelNumber) ==
                OSIX_FAILURE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Failed to Delete the WTP Model Entry \r\n");
                return OSIX_FAILURE;
            }

            gu2WtpModelEntry--;
            break;
        }

        case WSS_CAPWAP_CREATE_WTP_MODEL_ENTRY:
        {
            u2Len =
                (UINT2) (STRLEN
                         (pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.
                          au1ModelNumber));

            /* Add entry with the incoming IP address to the RB tree */
            pWtpModelMap =
                (tWssIfWtpModelMap *) MemAllocMemBlk (CAPWAP_WTPMODEL_POOLID);
            if (pWtpModelMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfWtpModelEntryCreate:Memory allocation Failure \r\n");
                return OSIX_FAILURE;
            }
            pWtpModelMap->u2ModelIndex = gu2WtpModelEntry;
            MEMSET (pWtpModelMap->au1ModelNumber, 0, MAX_WTP_MODELNUMBER_LEN);
            MEMCPY (pWtpModelMap->au1ModelNumber,
                    pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.au1ModelNumber,
                    u2Len);
            if (RBTreeAdd (gCapWtpModelMapTable, pWtpModelMap) != RB_SUCCESS)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProfildIdEntryCreate: Failed to add entry to the Profild ID  RB Tree \r\n");
                MemReleaseMemBlock (CAPWAP_WTPMODEL_POOLID,
                                    (UINT1 *) pWtpModelMap);
                return OSIX_FAILURE;
            }
            MEMCPY (gWssCapDB.WtpModelEntry[gu2WtpModelEntry].au1ModelNumber,
                    pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.au1ModelNumber,
                    u2Len);
            gWssCapDB.WtpModelEntry[pWtpModelMap->u2ModelIndex].u1MacType =
                CAPWAP_WTP_DEF_MAC_TYPE;
            gWssCapDB.WtpModelEntry[pWtpModelMap->u2ModelIndex].u1TunnelMode =
                CAPWAP_WTP_DEF_TUNN_MODE;
            gWssCapDB.WtpModelEntry[pWtpModelMap->u2ModelIndex].u1LocalRouting =
                CAPWAP_DISABLE;
            gu2WtpModelEntry++;
            break;
        }
        case WSS_CAPWAP_GET_INDEX_FROM_MAC:
        {
            for (u2Index = 0; u2Index < MAX_WTP_PROFILE_SUPPORTED; u2Index++)
            {
                if (MEMCMP (&gWssCapDB.CapwapGetDB[u2Index].WtpMacAddress,
                            &pWssIfCapwapDB->CapwapGetDB.WtpMacAddress,
                            MAC_ADDR_LEN) == 0)
                {
                    if (gWssCapDB.CapwapGetDB[u2Index].u1RowStatus != ACTIVE)
                    {
                        WSSIF_TRC (WSSIF_FAILURE_TRC,
                                   "GetIndexfromMac: Row inactive, return failure\r\n");
                        return OSIX_FAILURE;
                    }
                    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                        gWssCapDB.CapwapGetDB[u2Index].u2WtpInternalId;
                    return OSIX_SUCCESS;
                }
            }
            return OSIX_FAILURE;
            break;
        }
        case WSS_CAPWAP_GET_INTERNAL_ID:
        {
            /* Get the Internal Profile Id */
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId == OSIX_TRUE)
            {
                u2ProfileId = pWssIfCapwapDB->CapwapGetDB.u2ProfileId;
                if (WssIfGetProfileIndex (u2ProfileId, &u2WtpProfileId)
                    != OSIX_SUCCESS)
                {
                    WSSIF_TRC2 (WSSIF_FAILURE_TRC, "User configured"
                                "profild id = %d and internal"
                                "profile id = %d \r\n", u2ProfileId,
                                u2WtpProfileId);
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bRowStatus == OSIX_TRUE)
                {
                    if (pWssIfCapwapDB->CapwapGetDB.u1RowStatus == DESTROY)
                    {
                        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                            u2WtpProfileId;
                        break;
                    }
                }
                if (gWssCapDB.CapwapGetDB[u2WtpProfileId].u1RowStatus == 0)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC, "Entry not present\r\n");
                    return OSIX_FAILURE;
                }
                if (gWssCapDB.CapwapGetDB[u2WtpProfileId].u1RowStatus != ACTIVE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                        u2WtpProfileId;
                    WSSIF_TRC (WSSIF_FAILURE_TRC,
                               "Entry present but Row Inactive \r\n");
                    return NOT_IN_SERVICE;
                }
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpProfileId;
            }
            else
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Inconsistent input received\r\n");
            }
            break;
        }

        case WSS_CAPWAP_GET_CAPW_ATP_ID:
        {
            /* Get the Internal Profile Id */
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId == OSIX_TRUE)
            {
                u2ProfileId = pWssIfCapwapDB->CapwapGetDB.u2ProfileId;
                if (WssIfGetProfileIndex (u2ProfileId, &u2WtpProfileId)
                    == OSIX_SUCCESS)
                {
                    printf ("User configured profild id = %d and internal "
                            "profile id = %d \r\n", u2ProfileId,
                            u2WtpProfileId);
                }
                if (pWssIfCapwapDB->CapwapIsGetAllDB.bRowStatus == OSIX_TRUE)
                {
                    if (pWssIfCapwapDB->CapwapGetDB.u1RowStatus == DESTROY)
                    {
                        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                            u2WtpProfileId;
                        break;
                    }
                }
                if (gWssCapDB.CapwapGetDB[u2WtpProfileId].u1RowStatus == 0)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC, "Entry not present\r\n");
                    return OSIX_FAILURE;
                }
                if (gWssCapDB.CapwapGetDB[u2WtpProfileId].u1RowStatus != ACTIVE)
                {
                    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                        u2WtpProfileId;
                    WSSIF_TRC (WSSIF_FAILURE_TRC,
                               "Entry present but Row Inactive \r\n");
                    return NOT_IN_SERVICE;
                }
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpProfileId;
            }
            else
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "Inconsistent input received\r\n");
            }
            break;
        }

        case WSS_CAPWAP_GET_SESS_STATS:
        {
            /* Get the Internal Profile Id */
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId == OSIX_TRUE)
            {
                u2ProfileId = pWssIfCapwapDB->CapwapGetDB.u2ProfileId;
                if (WssIfGetProfileIndex (u2ProfileId, &u2WtpProfileId) ==
                    OSIX_SUCCESS)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC,
                               "Failed to Get Internal Profild Id from User configurated Profile Id \r\n");
                    return OSIX_FAILURE;
                }
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bDiscStats == OSIX_TRUE)
            {
                MEMCPY (&pWssIfCapwapDB->CapwapGetDB.CapwaStats.CapDisc,
                        &gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapDisc, sizeof (tWssIfSessStats));
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bJoinStats == OSIX_TRUE)
            {
                MEMCPY (&pWssIfCapwapDB->CapwapGetDB.CapwaStats.CapJoin,
                        &gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapJoin, sizeof (tWssIfSessStats));

            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bConfigStats == OSIX_TRUE)
            {
                MEMCPY (&pWssIfCapwapDB->CapwapGetDB.CapwaStats.CapConfig,
                        &gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapConfig, sizeof (tWssIfSessStats));
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bRunStats == OSIX_TRUE)
            {
                MEMCPY (&pWssIfCapwapDB->CapwapGetDB.CapwaStats.CapEcho,
                        &gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapEcho, sizeof (tWssIfSessStats));
                MEMCPY (&pWssIfCapwapDB->CapwapGetDB.CapwaStats.CapKeeAlive,
                        &gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapKeeAlive, sizeof (tWssIfSessStats));
                MEMCPY (&pWssIfCapwapDB->CapwapGetDB.CapwaStats.CapConfUpdate,
                        &gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapConfUpdate, sizeof (tWssIfSessStats));
                MEMCPY (&pWssIfCapwapDB->CapwapGetDB.CapwaStats.CapStaUpdate,
                        &gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapStaUpdate, sizeof (tWssIfSessStats));
                MEMCPY (&pWssIfCapwapDB->CapwapGetDB.CapwaStats.CapData,
                        &gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapData, sizeof (tWssIfSessStats));
                MEMCPY (&pWssIfCapwapDB->CapwapGetDB.CapwaStats.CapWlanUpdate,
                        &gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapWlanUpdate, sizeof (tWssIfSessStats));
                MEMCPY (&pWssIfCapwapDB->CapwapGetDB.CapwaStats.CapPriDisc,
                        &gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapPriDisc, sizeof (tWssIfSessStats));
                MEMCPY (&pWssIfCapwapDB->CapwapGetDB.CapwaStats.CapClearConf,
                        &gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapClearConf, sizeof (tWssIfSessStats));
                MEMCPY (&pWssIfCapwapDB->CapwapGetDB.CapwaStats.CapReset,
                        &gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapReset, sizeof (tWssIfSessStats));
            }

            break;
        }
        case WSS_CAPWAP_CLEAR_SESS_STATS:
        {
            /* Get the Internal Profile Id */
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId == OSIX_TRUE)
            {
                u2ProfileId = pWssIfCapwapDB->CapwapGetDB.u2ProfileId;
                if (WssIfGetProfileIndex (u2ProfileId, &u2WtpProfileId) ==
                    OSIX_SUCCESS)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC,
                               "Failed to Get Internal Profild Id from User configurated Profile Id \r\n");
                    return OSIX_FAILURE;
                }
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bDiscStats == OSIX_TRUE)
            {
                MEMSET (&gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapDisc, 0, sizeof (tWssIfSessStats));
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bJoinStats == OSIX_TRUE)
            {
                MEMSET (&gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapJoin, 0, sizeof (tWssIfSessStats));
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bConfigStats == OSIX_TRUE)
            {
                MEMSET (&gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapConfig, 0, sizeof (tWssIfSessStats));
            }
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bRunStats == OSIX_TRUE)
            {
                MEMSET (&gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapEcho, 0, sizeof (tWssIfSessStats));
                MEMSET (&gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapKeeAlive, 0, sizeof (tWssIfSessStats));
                MEMSET (&gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapConfUpdate, 0, sizeof (tWssIfSessStats));
                MEMSET (&gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapData, 0, sizeof (tWssIfSessStats));
                MEMSET (&gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapWlanUpdate, 0, sizeof (tWssIfSessStats));
                MEMSET (&gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapPriDisc, 0, sizeof (tWssIfSessStats));
                MEMSET (&gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapClearConf, 0, sizeof (tWssIfSessStats));
                MEMSET (&gWssCapDB.CapwapGetDB[u2WtpProfileId].CapwaStats.
                        CapReset, 0, sizeof (tWssIfSessStats));
            }
            break;
        }

        case WSS_CAPWAP_CREATE_BASEACNAMELIST_ENTRY:
        {
            break;
        }

        case WSS_CAPWAP_DESTROY_BASEACNAMELIST_ENTRY:
        {
            break;
        }

        case WSS_CAPWAP_CREATE_WHITELIST_ENTRY:
        {
            pWssIfWlcWhiteListDB =
                (tWssIfWlcWhiteListDB *)
                MemAllocMemBlk (CAPWAP_WLCWHITELIST_POOLID);

            if (pWssIfWlcWhiteListDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessCapwapDBMsg:WhiteList Memory allocation Failure \r\n");
                return OSIX_FAILURE;
            }
            MEMSET (pWssIfWlcWhiteListDB, 0, sizeof (tWssIfWlcWhiteListDB));
            MEMCPY (pWssIfWlcWhiteListDB->MacAddr,
                    pWssIfCapwapDB->BaseMacAddress, sizeof (tMacAddr));

            if (RBTreeAdd (gCapWlcWhiteListTable, pWssIfWlcWhiteListDB) !=
                RB_SUCCESS)
            {

                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessCapwapDBMsg: Failed to add entry to the\
                            WhiteListRB Tree \r\n");
                MemReleaseMemBlock (CAPWAP_WLCWHITELIST_POOLID,
                                    (UINT1 *) pWssIfWlcWhiteListDB);
                return OSIX_FAILURE;

            }
        }
            break;
        case WSS_CAPWAP_DELETE_WHITELIST_ENTRY:
        {
            MEMCPY (WssIfWlcWhiteListDB.MacAddr, pWssIfCapwapDB->BaseMacAddress,
                    sizeof (tMacAddr));
            pWssIfWlcWhiteListDB =
                ((tWssIfWlcWhiteListDB *) RBTreeGet (gCapWlcWhiteListTable,
                                                     (tRBElem *) &
                                                     WssIfWlcWhiteListDB));
            if (pWssIfWlcWhiteListDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessCapwapDBMsg: Failed to delete entry from\
                            WhiteListRB Tree \r\n");
                return OSIX_FAILURE;

            }
            RBTreeRem (gCapWlcWhiteListTable, (tRBElem *) pWssIfWlcWhiteListDB);
            MemReleaseMemBlock (CAPWAP_WLCWHITELIST_POOLID,
                                (UINT1 *) pWssIfWlcWhiteListDB);
        }
            break;
        case WSS_CAPWAP_GET_WHITELIST_ENTRY:
        {
            MEMCPY (WssIfWlcWhiteListDB.MacAddr, pWssIfCapwapDB->BaseMacAddress,
                    sizeof (tMacAddr));
            pWssIfWlcWhiteListDB =
                ((tWssIfWlcWhiteListDB *) RBTreeGet (gCapWlcWhiteListTable,
                                                     (tRBElem *) &
                                                     WssIfWlcWhiteListDB));
            if (pWssIfWlcWhiteListDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessCapwapDBMsg: Failed to get entry from\
                            WhiteListRB Tree \r\n");
                return OSIX_FAILURE;

            }
        }
            break;

        case WSS_CAPWAP_CREATE_BLACKLIST_ENTRY:
        {
            pWssIfWlcBlackListDB =
                (tWssIfWlcBlackListDB *)
                MemAllocMemBlk (CAPWAP_WLCBLACKLIST_POOLID);

            if (pWssIfWlcBlackListDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessCapwapDBMsg:BlackList Memory allocation Failure \r\n");
                return OSIX_FAILURE;
            }
            MEMSET (pWssIfWlcBlackListDB, 0, sizeof (tWssIfWlcBlackListDB));
            MEMCPY (pWssIfWlcBlackListDB->MacAddr,
                    pWssIfCapwapDB->BaseMacAddress, sizeof (tMacAddr));

            if (RBTreeAdd (gCapWlcBlackListTable, pWssIfWlcBlackListDB) !=
                RB_SUCCESS)
            {

                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessCapwapDBMsg: Failed to add entry to the\
                            BlackListRB Tree \r\n");
                MemReleaseMemBlock (CAPWAP_WLCBLACKLIST_POOLID,
                                    (UINT1 *) pWssIfWlcBlackListDB);
                return OSIX_FAILURE;

            }
        }
            break;
        case WSS_CAPWAP_DELETE_BLACKLIST_ENTRY:
        {
            MEMCPY (WssIfWlcBlackListDB.MacAddr, pWssIfCapwapDB->BaseMacAddress,
                    sizeof (tMacAddr));
            pWssIfWlcBlackListDB =
                ((tWssIfWlcBlackListDB *) RBTreeGet (gCapWlcBlackListTable,
                                                     (tRBElem *) &
                                                     WssIfWlcBlackListDB));
            if (pWssIfWlcBlackListDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessCapwapDBMsg: Failed to delete entry from\
                            BlackListRB Tree \r\n");
                return OSIX_FAILURE;

            }
            RBTreeRem (gCapWlcBlackListTable, (tRBElem *) pWssIfWlcBlackListDB);
            MemReleaseMemBlock (CAPWAP_WLCBLACKLIST_POOLID,
                                (UINT1 *) pWssIfWlcBlackListDB);
        }
            break;
        case WSS_CAPWAP_GET_BLACKLIST_ENTRY:
        {
            MEMCPY (WssIfWlcBlackListDB.MacAddr, pWssIfCapwapDB->BaseMacAddress,
                    sizeof (tMacAddr));
            pWssIfWlcBlackListDB =
                ((tWssIfWlcBlackListDB *) RBTreeGet (gCapWlcBlackListTable,
                                                     (tRBElem *) &
                                                     WssIfWlcBlackListDB));
            if (pWssIfWlcBlackListDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessCapwapDBMsg: Failed to get entry from\
                            BlackListRB Tree \r\n");
                return OSIX_FAILURE;

            }
        }
            break;

        case WSS_CAPWAP_CREATE_STA_WHITELIST_ENTRY:
        {
            pWssIfStaWhiteListDB =
                (tWssIfStaWhiteListDB *)
                MemAllocMemBlk (CAPWAP_STAWHITELIST_POOLID);

            if (pWssIfStaWhiteListDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessCapwapDBMsg:"
                           "Sta WhiteList Memory allocation Failure \r\n");
                return OSIX_FAILURE;
            }
            MEMSET (pWssIfStaWhiteListDB, 0, sizeof (tWssIfStaWhiteListDB));
            MEMCPY (pWssIfStaWhiteListDB->MacAddr,
                    pWssIfCapwapDB->BaseMacAddress, sizeof (tMacAddr));
            pWssIfStaWhiteListDB->i4RowStatus =
                pWssIfCapwapDB->i4stationRowStatus;

            if (RBTreeAdd (gCapStaWhiteListTable, pWssIfStaWhiteListDB)
                != RB_SUCCESS)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessCapwapDBMsg: Failed to add entry to \
                            Sta WhiteListRB Tree \r\n");
                MemReleaseMemBlock (CAPWAP_STAWHITELIST_POOLID,
                                    (UINT1 *) pWssIfStaWhiteListDB);
                return OSIX_FAILURE;
            }
        }
            break;

        case WSS_CAPWAP_DELETE_STA_WHITELIST_ENTRY:
        {
            MEMCPY (WssIfStaWhiteListDB.MacAddr,
                    pWssIfCapwapDB->BaseMacAddress, sizeof (tMacAddr));
            pWssIfStaWhiteListDB =
                ((tWssIfStaWhiteListDB *) RBTreeGet (gCapStaWhiteListTable,
                                                     (tRBElem *) &
                                                     WssIfStaWhiteListDB));
            if (pWssIfStaWhiteListDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessCapwapDBMsg: Failed to delete entry \
                            from Sta WhiteListRB Tree \r\n");
                return OSIX_FAILURE;
            }
            RBTreeRem (gCapStaWhiteListTable, (tRBElem *) pWssIfStaWhiteListDB);
            MemReleaseMemBlock (CAPWAP_STAWHITELIST_POOLID,
                                (UINT1 *) pWssIfStaWhiteListDB);
        }
            break;

        case WSS_CAPWAP_GET_STA_WHITELIST_ENTRY:
        {
            MEMCPY (WssIfStaWhiteListDB.MacAddr,
                    pWssIfCapwapDB->BaseMacAddress, sizeof (tMacAddr));
            pWssIfStaWhiteListDB =
                ((tWssIfStaWhiteListDB *) RBTreeGet (gCapStaWhiteListTable,
                                                     (tRBElem *) &
                                                     WssIfStaWhiteListDB));
            if (pWssIfStaWhiteListDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessCapwapDBMsg: Failed to get entry from\
                            Sta WhiteListRB Tree \r\n");
                return OSIX_FAILURE;
            }
            pWssIfCapwapDB->i4stationRowStatus =
                pWssIfStaWhiteListDB->i4RowStatus;

        }
            break;
        case WSS_CAPWAP_SET_STA_WHITELIST_ENTRY:
        {
            MEMCPY (WssIfStaWhiteListDB.MacAddr,
                    pWssIfCapwapDB->BaseMacAddress, sizeof (tMacAddr));
            pWssIfStaWhiteListDB =
                ((tWssIfStaWhiteListDB *) RBTreeGet (gCapStaWhiteListTable,
                                                     (tRBElem *) &
                                                     WssIfStaWhiteListDB));
            if (pWssIfStaWhiteListDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessCapwapDBMsg: Failed to get entry from\
                            Sta WhiteListRB Tree \r\n");
                return OSIX_FAILURE;
            }
            pWssIfStaWhiteListDB->i4RowStatus =
                pWssIfCapwapDB->i4stationRowStatus;

        }
            break;
        case WSS_CAPWAP_GETFIRST_STA_WHITELIST_ENTRY:
        {
            pWssIfStaWhiteListDB = RBTreeGetFirst (gCapStaWhiteListTable);
            if (pWssIfStaWhiteListDB == NULL)
            {
                return OSIX_FAILURE;
            }
            MEMCPY (pWssIfCapwapDB->BaseMacAddress,
                    pWssIfStaWhiteListDB->MacAddr, sizeof (tMacAddr));

        }
            break;
        case WSS_CAPWAP_GETNEXT_STA_WHITELIST_ENTRY:
        {
            MEMCPY (WssIfStaWhiteListDB.MacAddr,
                    pWssIfCapwapDB->BaseMacAddress, sizeof (tMacAddr));
            pWssIfStaWhiteListDB = RBTreeGetNext (gCapStaWhiteListTable,
                                                  &WssIfStaWhiteListDB, NULL);

            if (pWssIfStaWhiteListDB == NULL)
            {
                return OSIX_FAILURE;
            }
            MEMCPY (pWssIfCapwapDB->BaseMacAddress,
                    pWssIfStaWhiteListDB->MacAddr, sizeof (tMacAddr));

        }
            break;
/***********************************************************************************************************/
        case WSS_CAPWAP_CREATE_STA_BLACKLIST_ENTRY:
        {
            /*if ((STRCMP ((pWssIfCapwapDB->BaseMacAddress), "\0\0\0\0\0\0")) ==
               OSIX_FALSE)
               {
               WSSIF_TRC (WSSIF_FAILURE_TRC,
               "WssIfProcessCapwapDBMsg:Null input\r\n");
               return OSIX_FAILURE;
               } */
            pWssIfStaBlackListDB =
                (tWssIfStaBlackListDB *)
                MemAllocMemBlk (CAPWAP_STABLACKLIST_POOLID);

            if (pWssIfStaBlackListDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC, "WssIfProcessCapwapDBMsg:"
                           "Sta BlackList Memory allocation Failure \r\n");
                return OSIX_FAILURE;
            }
            MEMSET (pWssIfStaBlackListDB, 0, sizeof (tWssIfStaBlackListDB));
            MEMCPY (pWssIfStaBlackListDB->MacAddr,
                    pWssIfCapwapDB->BaseMacAddress, sizeof (tMacAddr));
            pWssIfStaBlackListDB->i4RowStatus =
                pWssIfCapwapDB->i4stationRowStatus;

            if (RBTreeAdd (gCapStaBlackListTable, pWssIfStaBlackListDB)
                != RB_SUCCESS)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessCapwapDBMsg: Failed to add entry to \
                                                    Sta WhiteListRB Tree \r\n");
                MemReleaseMemBlock (CAPWAP_STABLACKLIST_POOLID,
                                    (UINT1 *) pWssIfStaBlackListDB);
                return OSIX_FAILURE;
            }
        }
            break;

        case WSS_CAPWAP_DELETE_STA_BLACKLIST_ENTRY:
        {
            MEMCPY (WssIfStaBlackListDB.MacAddr,
                    pWssIfCapwapDB->BaseMacAddress, sizeof (tMacAddr));
            pWssIfStaBlackListDB =
                ((tWssIfStaBlackListDB *) RBTreeGet (gCapStaBlackListTable,
                                                     (tRBElem *) &
                                                     WssIfStaBlackListDB));
            if (pWssIfStaBlackListDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessCapwapDBMsg: Failed to delete entry \
                            from Sta BlackListRB Tree \r\n");
                return OSIX_FAILURE;
            }
            RBTreeRem (gCapStaBlackListTable, (tRBElem *) pWssIfStaBlackListDB);
            MemReleaseMemBlock (CAPWAP_STABLACKLIST_POOLID,
                                (UINT1 *) pWssIfStaBlackListDB);
        }
            break;
        case WSS_CAPWAP_GET_STA_BLACKLIST_ENTRY:
        {
            MEMCPY (WssIfStaBlackListDB.MacAddr,
                    pWssIfCapwapDB->BaseMacAddress, sizeof (tMacAddr));
            pWssIfStaBlackListDB =
                ((tWssIfStaBlackListDB *) RBTreeGet (gCapStaBlackListTable,
                                                     (tRBElem *) &
                                                     WssIfStaBlackListDB));
            if (pWssIfStaBlackListDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessCapwapDBMsg: Failed to get entry from\
                            Sta BlackListRB Tree \r\n");
                return OSIX_FAILURE;
            }
            pWssIfCapwapDB->i4stationRowStatus =
                pWssIfStaBlackListDB->i4RowStatus;

        }
            break;
        case WSS_CAPWAP_SET_STA_BLACKLIST_ENTRY:
        {
            MEMCPY (WssIfStaBlackListDB.MacAddr,
                    pWssIfCapwapDB->BaseMacAddress, sizeof (tMacAddr));
            pWssIfStaBlackListDB =
                ((tWssIfStaBlackListDB *) RBTreeGet (gCapStaBlackListTable,
                                                     (tRBElem *) &
                                                     WssIfStaBlackListDB));
            if (pWssIfStaBlackListDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "WssIfProcessCapwapDBMsg: Failed to get entry from\
                            Sta BlackListRB Tree \r\n");
                return OSIX_FAILURE;
            }
            pWssIfStaBlackListDB->i4RowStatus =
                pWssIfCapwapDB->i4stationRowStatus;

        }
            break;
        case WSS_CAPWAP_GETFIRST_STA_BLACKLIST_ENTRY:
        {
            pWssIfStaBlackListDB = RBTreeGetFirst (gCapStaBlackListTable);
            if (pWssIfStaBlackListDB == NULL)
            {
                return OSIX_FAILURE;
            }
            MEMCPY (pWssIfCapwapDB->BaseMacAddress,
                    pWssIfStaBlackListDB->MacAddr, sizeof (tMacAddr));

        }
            break;
        case WSS_CAPWAP_GETNEXT_STA_BLACKLIST_ENTRY:
        {
            MEMCPY (WssIfStaBlackListDB.MacAddr,
                    pWssIfCapwapDB->BaseMacAddress, sizeof (tMacAddr));
            pWssIfStaBlackListDB = RBTreeGetNext (gCapStaBlackListTable,
                                                  &WssIfStaBlackListDB, NULL);

            if (pWssIfStaBlackListDB == NULL)
            {
                return OSIX_FAILURE;
            }
            MEMCPY (pWssIfCapwapDB->BaseMacAddress,
                    pWssIfStaBlackListDB->MacAddr, sizeof (tMacAddr));

        }
            break;
/************************************************************************************************************/
        case WSS_CAPWAP_GET_MODEL_COUNT:
        {
            if (pWssIfCapwapDB->CapwapIsGetAllDB.bModelCount == OSIX_TRUE)
            {
                pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.u1ModelCount =
                    (UINT1) gu2WtpModelEntry;

            }
        }
            break;

        default:
        {
            WSSIF_TRC (WSSIF_FAILURE_TRC,
                       "Received the unknown CAPWAP DB Message Type \r\n");
            return OSIX_FAILURE;
        }
    }
    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

#ifdef KERNEL_CAPWAP_WANTED
#ifdef WTP_WANTED

/*****************************************************************************
 *  Function Name   : WssWtpUpdateKernelDB                                   *
 *  Description     : This function calls the WssStaUpdateKernelDB to update *
                      the kernel DB                                          *
 *  Input(s)        : u1RadioId - Radio Id                                   *
 *                    u1WlanId - Wlan Id                                     *
 *                    stationMac - Station Mac Address                       *
                      u4BssIfIndex                                           *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssWtpUpdateKernelDB (UINT1 u1RadioId, UINT1 u1WlanId, UINT4 u4BssIfIndex,
                      tMacAddr stationMac)
{
    tWssWlanDB          WssWlanDB;

    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    WssWlanDB.WssWlanAttributeDB.u1RadioId = u1RadioId;
    WssWlanDB.WssWlanAttributeDB.u1WlanId = u1WlanId;
    WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY, &WssWlanDB);
    if (WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex == u4BssIfIndex)
    {
        WssStaUpdateKernelDB (u1RadioId, u1WlanId, stationMac);
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaUpdateKernelDB                                   *
 *  Description     : This function calls the npapi to update the kernel DB  *
 *  Input(s)        : u1RadioId - Radio Id                                   *
 *                    u1WlanId - Wlan Id                                     *
 *                    stationMac - Station Mac Address                       *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssStaUpdateKernelDB (UINT1 u1RadioId, UINT1 u1WlanId, tMacAddr stationMac)
{
    UINT1               u1Module = 0;
    UINT2               u2Field = 0;
    tWtpKernelDB        KernelDBWtp;
    tWssWlanDB          wssWlanDB;
    tRadioIfGetDB       RadioIfGetDB;
    tWssStaStateDB      WssStaStateDB;
    UINT4               u4ApGroupEnabled = 0;

    MEMSET (&KernelDBWtp, 0, sizeof (tWtpKernelDB));
    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&WssStaStateDB, 0, sizeof (tWssStaStateDB));

    wssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    wssWlanDB.WssWlanAttributeDB.u1RadioId = u1RadioId;
    wssWlanDB.WssWlanAttributeDB.u1WlanId = u1WlanId;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY, &wssWlanDB) ==
        OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Failed to retrieve data from WssWlanDB\r\n");
        return OSIX_FAILURE;
    }

    wssWlanDB.WssWlanIsPresentDB.bBssId = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bVlanId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY, &wssWlanDB) ==
        OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Failed to retrieve data from WssWlanDB\r\n");
        return OSIX_FAILURE;
    }

    wssWlanDB.WssWlanIsPresentDB.bWebAuthStatus = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bExternalWebAuthMethod = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &wssWlanDB) ==
        OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Failed to retrieve data from WssWlanDB\r\n");
        return OSIX_FAILURE;
    }

    /*Get the Wireless interface name through which the station is 
     * asssociated*/
    wssWlanDB.WssWlanIsPresentDB.bWlanIntfName = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY, &wssWlanDB)
        == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Failed to retrieve data from WssWlanDB\r\n");
        return OSIX_FAILURE;
    }

    MEMCPY (WssStaStateDB.stationMacAddress, stationMac, sizeof (tMacAddr));
    if (WssStaGetStationDB (&WssStaStateDB) == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "Kernel Notification Failed \r\n");
        /*    return OSIX_FAILURE; */
    }

    /* Get the MAC Type of Wlan */
    wssWlanDB.WssWlanIsPresentDB.bMacType = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bCapability = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bQosProfileId = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bPassengerTrustMode = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &wssWlanDB) ==
        OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Failed to retrieve data from WssWlanDB\r\n");
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u1QosIndex = 1;
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        u1RadioId + SYS_DEF_MAX_ENET_INTERFACES;
    RadioIfGetDB.RadioIfIsGetAllDB.bTaggingPolicy = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_QOS_CONFIG_DB, &RadioIfGetDB)
        == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "Failed to retrieve data from WssWlanDB\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&KernelDBWtp, 0, sizeof (tWtpKernelDB));
    MEMCPY (KernelDBWtp.rbData.staData.staMac, stationMac, sizeof (tMacAddr));
    MEMCPY (KernelDBWtp.rbData.staData.bssid,
            wssWlanDB.WssWlanAttributeDB.BssId, sizeof (tMacAddr));
    KernelDBWtp.rbData.staData.u2VlanId = wssWlanDB.WssWlanAttributeDB.u2VlanId;
    KernelDBWtp.rbData.staData.u1ExternalWebAuth
        = wssWlanDB.WssWlanAttributeDB.u1ExternalWebAuthMethod;
    MEMCPY (KernelDBWtp.rbData.staData.wlanIntf,
            wssWlanDB.WssWlanAttributeDB.au1WlanIntfName, INTERFACE_LEN);
    KernelDBWtp.rbData.staData.u1MacType =
        wssWlanDB.WssWlanAttributeDB.u1MacType;
    KernelDBWtp.rbData.staData.u1QosProfilePriority =
        wssWlanDB.WssWlanAttributeDB.u1QosProfileId;
    /*Updating kernel DB that connected station is 
       Whether WMM enabled or disabled */
    KernelDBWtp.rbData.staData.u1WMMEnable = WssStaStateDB.u1WmmFlag;
    KernelDBWtp.rbData.staData.u2TcpPort = WssStaStateDB.u2TcpPort;
    KernelDBWtp.rbData.staData.u1TaggingPolicy =
        RadioIfGetDB.RadioIfGetAllDB.u1TaggingPolicy;
    KernelDBWtp.rbData.staData.u2TcpPort = WssStaStateDB.u2TcpPort;
    KernelDBWtp.rbData.staData.u1TrustMode =
        wssWlanDB.WssWlanAttributeDB.u1PassengerTrustMode;
    KernelDBWtp.rbData.staData.u1WebAuthStatus =
        wssWlanDB.WssWlanAttributeDB.u1WebAuthStatus;
    KernelDBWtp.rbData.staData.u1WebAuthEnable =
        WssStaStateDB.u1WebAuthCompStatus;

    u1Module = TX_MODULE;
    u2Field = AP_STATION_TABLE;
    if (capwapNpUpdateKernel (u1Module, u2Field, &KernelDBWtp) == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "Kernel Notification Failed \r\n");
        return OSIX_FAILURE;
    }

    u1Module = RX_MODULE;
    u2Field = AP_STATION_TABLE;
    if (capwapNpUpdateKernel (u1Module, u2Field, &KernelDBWtp) == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "Kernel Notification Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaRemoveKernelDB                                   *
 *  Description     : This function calls the npapi to update the kernel DB  *
 *  Input(s)        : stationMac - Station Mac Address                       *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssStaRemoveKernelDB (tMacAddr stationMac)
{
    UINT1               u1Module = 0;
    UINT2               u2Field = 0;
    INT4                i4RetVal = 0;
    tWtpKernelDB        KernelDBWtp;

    MEMSET (&KernelDBWtp, 0, sizeof (tWtpKernelDB));
    MEMCPY (KernelDBWtp.rbData.staData.staMac, stationMac, sizeof (tMacAddr));

    u1Module = TX_MODULE;
    u2Field = AP_STATION_TABLE;
    i4RetVal = capwapNpKernelRemove (u1Module, u2Field, &KernelDBWtp);
    if (i4RetVal == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "Kernel removal Failed \r\n");
        return OSIX_FAILURE;
    }

    u1Module = RX_MODULE;
    u2Field = AP_STATION_TABLE;
    i4RetVal = capwapNpKernelRemove (u1Module, u2Field, &KernelDBWtp);
    if (i4RetVal == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "Kernel removal Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaRemoveAllKernelDB                                *
 *  Description     : This function calls the npapi to update the kernel DB  *
 *  Input(s)        : stationMac - Station Mac Address                       *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
VOID
WssStaRemoveAllKernelDB ()
{
    UINT1               u1Module = 0;
    UINT2               u2Field = 0;
    INT4                i4RetVal = 0;
    tWtpKernelDB        KernelDBWtp;

    MEMSET (&KernelDBWtp, 0, sizeof (tWtpKernelDB));

    u1Module = TX_MODULE;
    u2Field = AP_STATION_DESTROY;
    i4RetVal = capwapNpKernelRemove (u1Module, u2Field, &KernelDBWtp);
    if (i4RetVal == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "Kernel removal Failed \r\n");
        return;
    }

    u1Module = RX_MODULE;
    u2Field = AP_STATION_DESTROY;
    i4RetVal = capwapNpKernelRemove (u1Module, u2Field, &KernelDBWtp);
    if (i4RetVal == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "Kernel removal Failed \r\n");
        return;
    }
    return;
}
#endif
#endif

/*****************************************************************************
 *  Function Name   : WssifStartBinding                                   *
 *  Description     : Initiates the radio binding for auto profile creation  *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/

INT4
WssifStartBinding (tCapwapBindingTable * pCapwapModelTable,
                   tWssIfCapDB * pWssIfCapDB)
{
#ifdef WLC_WANTED
    tWsscfgCapwapDot11WlanBindEntry WsscfgSetCapwapDot11WlanBindEntry;
    tWsscfgIsSetCapwapDot11WlanBindEntry WsscfgIsSetCapwapDot11WlanBindEntry;
    UINT4               u4ErrorCode = 0;

    MEMSET (&WsscfgSetCapwapDot11WlanBindEntry, 0,
            sizeof (tWsscfgCapwapDot11WlanBindEntry));
    MEMSET (&WsscfgIsSetCapwapDot11WlanBindEntry, 0,
            sizeof (tWsscfgIsSetCapwapDot11WlanBindEntry));

    WsscfgSetCapwapDot11WlanBindEntry.MibObject.u4CapwapDot11WlanProfileId =
        pCapwapModelTable->u2WlanProfileId;
    WsscfgSetCapwapDot11WlanBindEntry.MibObject.i4CapwapDot11WlanBindRowStatus =
        CREATE_AND_WAIT;
    WsscfgSetCapwapDot11WlanBindEntry.MibObject.i4IfIndex =
        pWssIfCapDB->CapwapGetDB.u4IfIndex;
    WsscfgIsSetCapwapDot11WlanBindEntry.bCapwapDot11WlanBindRowStatus =
        OSIX_TRUE;
    WsscfgIsSetCapwapDot11WlanBindEntry.bIfIndex = OSIX_TRUE;
    WsscfgIsSetCapwapDot11WlanBindEntry.bCapwapDot11WlanProfileId = OSIX_TRUE;

    if (WsscfgTestAllCapwapDot11WlanBindTable
        (&u4ErrorCode, &WsscfgSetCapwapDot11WlanBindEntry,
         &WsscfgIsSetCapwapDot11WlanBindEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (WsscfgSetAllCapwapDot11WlanBindTable
        (&WsscfgSetCapwapDot11WlanBindEntry,
         &WsscfgIsSetCapwapDot11WlanBindEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    WsscfgSetCapwapDot11WlanBindEntry.MibObject.i4CapwapDot11WlanBindRowStatus =
        pCapwapModelTable->u1RowStatus;
    if (WsscfgTestAllCapwapDot11WlanBindTable
        (&u4ErrorCode, &WsscfgSetCapwapDot11WlanBindEntry,
         &WsscfgIsSetCapwapDot11WlanBindEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)

    {
        return OSIX_FAILURE;
    }
    if (WsscfgSetAllCapwapDot11WlanBindTable
        (&WsscfgSetCapwapDot11WlanBindEntry,
         &WsscfgIsSetCapwapDot11WlanBindEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pCapwapModelTable);
    UNUSED_PARAM (pWssIfCapDB);
#endif
    return OSIX_SUCCESS;
}

#ifdef KERNEL_CAPWAP_WANTED
#ifdef WLC_WANTED
/*****************************************************************************
 *  Function Name   : WssQosUpdateKernelDB                                   *
 *  Description     : This function calls the npapi to update the kernel DB  *
 *  Input(s)        : Dscp Map Table - pCapDscpMap                           *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssQosUpdateKernelDB (tCapwapDscpMapTable * pCapDscpMap)
{
#ifdef DEBUG_WANTED
    UINT1               u1Module = 0;
    UINT2               u2Field = 0;
    tHandleRbWlc        KernelDBWlc;

    MEMSET (&KernelDBWlc, 0, sizeof (tHandleRbWlc));

    MEMCPY (KernelDBWlc.rbData.unTable.qosData.au1InterfaceName, pCapDscpMap->
            au1InterfaceName, CFA_MAX_PORT_NAME_LENGTH);
    KernelDBWlc.rbData.unTable.qosData.u1dscpin = pCapDscpMap->u1InPriority;
    KernelDBWlc.rbData.unTable.qosData.u1dscpout = pCapDscpMap->u1OutDscp;

    u1Module = DSCP_MODULE;
    u2Field = AP_QOS_TABLE;
    if (capwapNpUpdateKernelDBWlc (u1Module, u2Field, &KernelDBWlc) ==
        OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "Kernel Notification Failed \r\n");
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pCapDscpMap);
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssQosRemoveKernelDB                                   *
 *  Description     : This function calls the npapi to update the kernel DB  *
 *  Input(s)        : stationMac - Station Mac Address                       *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssQosRemoveKernelDB (tCapwapDscpMapTable * pCapDscpMap)
{
#ifdef DEBUG_WANTED
    UINT1               u1Module = 0;
    UINT2               u2Field = 0;
    INT4                i4RetVal = 0;
    tHandleRbWlc        KernelDBWlc;

    MEMSET (&KernelDBWlc, 0, sizeof (tHandleRbWlc));

    MEMCPY (KernelDBWlc.rbData.unTable.qosData.au1InterfaceName, pCapDscpMap->
            au1InterfaceName, CFA_MAX_PORT_NAME_LENGTH);
    KernelDBWlc.rbData.unTable.qosData.u1dscpin = pCapDscpMap->u1InPriority;
    KernelDBWlc.rbData.unTable.qosData.u1dscpout = pCapDscpMap->u1OutDscp;

    u1Module = DSCP_MODULE;
    u2Field = AP_QOS_TABLE;
    i4RetVal = capwapNpRemoveKernelDBWlc (u1Module, u2Field, &KernelDBWlc);
    if (i4RetVal == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "Kernel removal Failed \r\n");
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pCapDscpMap);
#endif
    return OSIX_SUCCESS;
}
#endif
#endif
#endif

INT4
CapwapValidateWlanTable (UINT4 u4CapwapDot11WlanProfileId)
{
#ifdef WLC_WANTED
    tWsscfgCapwapDot11WlanEntry *pWsscfgCapwapDot11WlanEntry = NULL;

    pWsscfgCapwapDot11WlanEntry = (tWsscfgCapwapDot11WlanEntry *)
        MemAllocMemBlk (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID);
    if (pWsscfgCapwapDot11WlanEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWsscfgCapwapDot11WlanEntry->MibObject.
        u4CapwapDot11WlanProfileId = u4CapwapDot11WlanProfileId;

    if (WsscfgGetAllCapwapDot11WlanTable (pWsscfgCapwapDot11WlanEntry)
        != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgCapwapDot11WlanEntry);
        return OSIX_FAILURE;
    }
    if (pWsscfgCapwapDot11WlanEntry->MibObject.i4CapwapDot11WlanRowStatus !=
        ACTIVE)
    {
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgCapwapDot11WlanEntry);
        return OSIX_FAILURE;
    }
    MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                        (UINT1 *) pWsscfgCapwapDot11WlanEntry);
#else
    UNUSED_PARAM (u4CapwapDot11WlanProfileId);
#endif
    return OSIX_SUCCESS;
}

#ifdef WLC_WANTED
/*****************************************************************************
 *  Function Name   : WssIfDhcpGlobalDBCreate                                *
 *  Description     : This function used to initialize the DHCP Global Table *
 *                    profile RB Tree                                        *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfDhcpGlobalDBCreate ()
{
    gCapDhcpGlobalTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssIfDhcpPoolDB,
                                nextDhcpPoolId)), WssIfCompareDhcpPoolRBTree);
    if (gCapDhcpGlobalTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfDhcpGlobalDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpLRConfigDBCreate                               *
 *  Description     : This function used to initialize the WTP Local Routing
 *                    Configuration Table RB Tree                            *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpLRConfigDBCreate ()
{
    gCapWtpLRConfigTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssIfWtpLRConfigDB,
                                nextWtpLRConfigEntry)),
                              WssIfCompareWtpLRConfigRBTree);
    if (gCapWtpLRConfigTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpLRConfigDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpDhcpConfigDBCreate                             *
 *  Description     : This function used to initialize the WTP Dhcp
 *                    Configuration Table RB Tree                            *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpDhcpConfigDBCreate ()
{
    gCapWtpDhcpConfigTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssIfWtpDhcpConfigDB,
                                nextWtpDhcpConfigEntry)),
                              WssIfCompareWtpDhcpConfigRBTree);
    if (gCapWtpDhcpConfigTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpDhcpConfigDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpIpRouteConfigDBCreate                          *
 *  Description     : This function used to initialize the WTP Local Routing
 *                    Ip Route Configuration Table RB Tree                   *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpIpRouteConfigDBCreate ()
{
    gCapWtpIpRouteConfigTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssIfWtpIpRouteConfigDB,
                                nextWtpIpRouteConfigEntry)),
                              WssIfCompareWtpIpRouteConfigRBTree);
    if (gCapWtpIpRouteConfigTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpIpRouteConfigDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfNATConfigEntryDBCreate                            *
 *  Description     : This function used to initialize the NAT Config Entry
 *                    Table profile RB Tree                                  *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfNATConfigEntryDBCreate ()
{
    gCapNATConfigTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssIfNATConfigEntryDB,
                                nextNATConfigEntry)),
                              WssIfCompareNATConfigEntryRBTree);
    if (gCapNATConfigTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfNATConfigDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpFwlFilterDBCreate                              *
 *  Description     : This function used to initialize the Firewall Filter DB*
 *                    Table                                                  *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 ******************************************************************************/
INT4
WssIfWtpFwlFilterDBCreate ()
{
    gCapWtpFwlFilterTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssIfWtpFwlFilterDB,
                                nextWtpFwlFilterEntry)),
                              WssIfCompareWtpFwlFilterRBTree);
    if (gCapWtpFwlFilterTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpFwlFilterDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpFwlRuleDBCreate                                *
 *  Description     : This function used to initialize the Firewall Rule DB  *
 *                    Table                                                  *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 ******************************************************************************/
INT4
WssIfWtpFwlRuleDBCreate ()
{
    gCapWtpFwlRuleTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssIfWtpFwlRuleDB,
                                nextWtpFwlRuleEntry)),
                              WssIfCompareWtpFwlRuleRBTree);
    if (gCapWtpFwlRuleTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpFwlRuleDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpFwlAclDBCreate                                 *
 *  Description     : This function used to initialize the Firewall Acl DB   *
 *                    Table                                                  *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 ******************************************************************************/
INT4
WssIfWtpFwlAclDBCreate ()
{
    gCapWtpFwlAclTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssIfWtpFwlAclDB,
                                nextWtpFwlAclEntry)),
                              WssIfCompareWtpFwlAclRBTree);
    if (gCapWtpFwlAclTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpFwlAclDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpL3SubIfDBCreate                                *
 *  Description     : This function used to initialize the L3 SUB IF DB      *
 *                    Table                                                  *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 ******************************************************************************/
INT4
WssIfWtpL3SubIfDBCreate (VOID)
{
    gCapWtpL3SubIfTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssIfWtpL3SubIfDB,
                                nextWtpL3SubIfEntry)),
                              WssIfWtpCompareL3SubIfRBTree);
    if (gCapWtpL3SubIfTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpL3SubIfDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfDhcpGlobalDBDelete                                *
 *  Description     : This function used to delete the DHCP Global Table     *
 *                    profile RB Tree                                        *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfDhcpGlobalDBDelete ()
{
    if (gCapDhcpGlobalTable != NULL)
    {
        RBTreeDelete (gCapDhcpGlobalTable);
        gCapDhcpGlobalTable = NULL;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpFwlFilterDBDelete                              *
 *  Description     : This function used to delete the firewall filter Table *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpFwlFilterDBDelete ()
{
    if (gCapWtpFwlFilterTable != NULL)
    {
        RBTreeDelete (gCapWtpFwlFilterTable);
        gCapWtpFwlFilterTable = NULL;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpFwlRuleDBDelete                                *
 *  Description     : This function used to delete the firewall Rule Table   *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpFwlRuleDBDelete ()
{
    if (gCapWtpFwlRuleTable != NULL)
    {
        RBTreeDelete (gCapWtpFwlRuleTable);
        gCapWtpFwlRuleTable = NULL;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpFwlAclDBDelete                                 *
 *  Description     : This function used to delete the firewall Acl Table    *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpFwlAclDBDelete ()
{
    if (gCapWtpFwlAclTable != NULL)
    {
        RBTreeDelete (gCapWtpFwlAclTable);
        gCapWtpFwlAclTable = NULL;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpLRConfigDBDelete                               *
 *  Description     : This function used to delete the WTP Local Routing
 *                    Config Table RB Tree                                   *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpLRConfigDBDelete ()
{
    if (gCapWtpLRConfigTable != NULL)
    {
        RBTreeDelete (gCapWtpLRConfigTable);
        gCapWtpLRConfigTable = NULL;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpDhcpConfigDBDelete                             *
 *  Description     : This function used to delete the WTP Dhcp
 *                    Config Table RB Tree                                   *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpDhcpConfigDBDelete ()
{
    if (gCapWtpDhcpConfigTable != NULL)
    {
        RBTreeDelete (gCapWtpDhcpConfigTable);
        gCapWtpDhcpConfigTable = NULL;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpIpRouteConfigDBDelete                          *
 *  Description     : This function used to delete the WTP Local Routing
 *                    Ip Route Config Table RB Tree                          *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpIpRouteConfigDBDelete ()
{
    if (gCapWtpIpRouteConfigTable != NULL)
    {
        RBTreeDelete (gCapWtpIpRouteConfigTable);
        gCapWtpIpRouteConfigTable = NULL;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfNATConfigEntryDBDelete                            *
 *  Description     : This function used to delete the NAT Config Entry Table*
 *                    profile RB Tree                                        *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfNATConfigEntryDBDelete ()
{
    if (gCapNATConfigTable != NULL)
    {
        RBTreeDelete (gCapNATConfigTable);
        gCapNATConfigTable = NULL;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpL3SubIfDBDelete                                *
 *  Description     : This function used to delete the L3SUB IF Table        *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpL3SubIfDBDelete (VOID)
{
    if (gCapWtpL3SubIfTable != NULL)
    {
        RBTreeDelete (gCapWtpL3SubIfTable);
        gCapWtpL3SubIfTable = NULL;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : WssIfCompareDhcpPoolRBTree                           */
/*                                                                           */
/* Description        : This function compares the two DHCP Pools            */
/*                      based on the Poold Id                                */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : WSSIF_RB_GREATER, WSSIF_RB_LESS, WSSIF_RB_EQUAL      */
/*****************************************************************************/
INT4
WssIfCompareDhcpPoolRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssIfDhcpPoolDB   *pNode1 = e1;
    tWssIfDhcpPoolDB   *pNode2 = e2;

    if (pNode1->u4DhcpPoolId > pNode2->u4DhcpPoolId)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u4DhcpPoolId < pNode2->u4DhcpPoolId)
    {
        return WSSIF_RB_LESS;
    }

    return WSSIF_RB_EQUAL;
}

/*****************************************************************************/
/* Function Name      : WssIfCompareWtpLRConfigRBTree                        */
/*                                                                           */
/* Description        : This function compares the two Wtp Local Routing     */
/*                      Config Entries  based on the Wtp Profile Ids         */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : WSSIF_RB_GREATER, WSSIF_RB_LESS, WSSIF_RB_EQUAL      */
/*****************************************************************************/
INT4
WssIfCompareWtpLRConfigRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssIfWtpLRConfigDB *pNode1 = e1;
    tWssIfWtpLRConfigDB *pNode2 = e2;

    if (pNode1->u4WtpProfileId > pNode2->u4WtpProfileId)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u4WtpProfileId < pNode2->u4WtpProfileId)
    {
        return WSSIF_RB_LESS;
    }

    return WSSIF_RB_EQUAL;
}

/*****************************************************************************/
/* Function Name      : WssIfCompareWtpDhcpConfigRBTree                      */
/*                                                                           */
/* Description        : This function compares the two Wtp Dhcp              */
/*                      Config Entries  based on the Wtp Profile Ids         */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : WSSIF_RB_GREATER, WSSIF_RB_LESS, WSSIF_RB_EQUAL      */
/*****************************************************************************/
INT4
WssIfCompareWtpDhcpConfigRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssIfWtpDhcpConfigDB *pNode1 = e1;
    tWssIfWtpDhcpConfigDB *pNode2 = e2;

    if (pNode1->u4WtpProfileId > pNode2->u4WtpProfileId)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u4WtpProfileId < pNode2->u4WtpProfileId)
    {
        return WSSIF_RB_LESS;
    }

    return WSSIF_RB_EQUAL;
}

/*****************************************************************************/
/* Function Name      : WssIfCompareWtpIpRouteConfigRBTree                   */
/*                                                                           */
/* Description        : This function compares the two Wtp Local Routing     */
/*                      Ip Route Config Entries  based on the Wtp Profile Ids*/
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : WSSIF_RB_GREATER, WSSIF_RB_LESS, WSSIF_RB_EQUAL      */
/*****************************************************************************/
INT4
WssIfCompareWtpIpRouteConfigRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssIfWtpIpRouteConfigDB *pNode1 = e1;
    tWssIfWtpIpRouteConfigDB *pNode2 = e2;

    if (pNode1->u4WtpProfileId > pNode2->u4WtpProfileId)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u4WtpProfileId < pNode2->u4WtpProfileId)
    {
        return WSSIF_RB_LESS;
    }
    else if (pNode1->u4Subnet > pNode2->u4Subnet)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u4Subnet < pNode2->u4Subnet)
    {
        return WSSIF_RB_LESS;
    }
    else if (pNode1->u4Mask > pNode2->u4Mask)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u4Mask < pNode2->u4Mask)
    {
        return WSSIF_RB_LESS;
    }
    else if (pNode1->u4Gateway > pNode2->u4Gateway)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u4Gateway < pNode2->u4Gateway)
    {
        return WSSIF_RB_LESS;
    }
    return WSSIF_RB_EQUAL;
}

/*****************************************************************************/
/* Function Name      : WssIfCompareNATConfigEntryRBTree                     */
/*                                                                           */
/* Description        : This function compares the two NAT Config Entries    */
/*                      based on the Entry index                             */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : WSSIF_RB_GREATER, WSSIF_RB_LESS, WSSIF_RB_EQUAL      */
/*****************************************************************************/
INT4
WssIfCompareNATConfigEntryRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssIfNATConfigEntryDB *pNode1 = e1;
    tWssIfNATConfigEntryDB *pNode2 = e2;

    if (pNode1->i4NATConfigEntryIndex > pNode2->i4NATConfigEntryIndex)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->i4NATConfigEntryIndex < pNode2->i4NATConfigEntryIndex)
    {
        return WSSIF_RB_LESS;
    }
    else if (pNode1->u4WtpProfileId > pNode2->u4WtpProfileId)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u4WtpProfileId < pNode2->u4WtpProfileId)
    {
        return WSSIF_RB_LESS;
    }
    return WSSIF_RB_EQUAL;
}

/*****************************************************************************/
/* Function Name      : WssIfCompareWtpFwlFilterRBTree                       */
/*                                                                           */
/* Description        : This function compares the two Firewall Filters      */
/*                      based on the Entry index                             */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : WSSIF_RB_GREATER, WSSIF_RB_LESS, WSSIF_RB_EQUAL      */
/*****************************************************************************/
INT4
WssIfCompareWtpFwlFilterRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssIfWtpFwlFilterDB *pNode1 = e1;
    tWssIfWtpFwlFilterDB *pNode2 = e2;
    INT4                i4RetVal = 0;

    i4RetVal = MEMCMP (pNode1->au1FilterName, pNode2->au1FilterName,
                       AP_FWL_MAX_FILTER_NAME_LEN);

    if (pNode1->u4WtpProfileId > pNode2->u4WtpProfileId)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u4WtpProfileId < pNode2->u4WtpProfileId)
    {
        return WSSIF_RB_LESS;
    }
    else if (i4RetVal > 0)
    {
        return WSSIF_RB_GREATER;
    }
    else if (i4RetVal < 0)
    {
        return WSSIF_RB_LESS;
    }
    return WSSIF_RB_EQUAL;
}

/*****************************************************************************/
/* Function Name      : WssIfCompareWtpFwlRuleRBTree                         */
/*                                                                           */
/* Description        : This function compares the two Firewall Rules        */
/*                      based on the Entry index                             */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : WSSIF_RB_GREATER, WSSIF_RB_LESS, WSSIF_RB_EQUAL      */
/*****************************************************************************/
INT4
WssIfCompareWtpFwlRuleRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssIfWtpFwlRuleDB *pNode1 = e1;
    tWssIfWtpFwlRuleDB *pNode2 = e2;
    INT4                i4RetVal = 0;

    i4RetVal = MEMCMP (pNode1->au1RuleName, pNode2->au1RuleName,
                       AP_FWL_MAX_RULE_NAME_LEN);

    if (pNode1->u4WtpProfileId > pNode2->u4WtpProfileId)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u4WtpProfileId < pNode2->u4WtpProfileId)
    {
        return WSSIF_RB_LESS;
    }
    else if (i4RetVal > 0)
    {
        return WSSIF_RB_GREATER;
    }
    else if (i4RetVal < 0)
    {
        return WSSIF_RB_LESS;
    }
    return WSSIF_RB_EQUAL;
}

/*****************************************************************************/
/* Function Name      : WssIfCompareWtpFwlAclRBTree                          */
/*                                                                           */
/* Description        : This function compares the two Firewall Access       */
/*                      control lists based on the Entry index               */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : WSSIF_RB_GREATER, WSSIF_RB_LESS, WSSIF_RB_EQUAL      */
/*****************************************************************************/
INT4
WssIfCompareWtpFwlAclRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssIfWtpFwlAclDB  *pNode1 = e1;
    tWssIfWtpFwlAclDB  *pNode2 = e2;
    INT4                i4RetVal = 0;

    i4RetVal = MEMCMP (pNode1->au1AclName, pNode2->au1AclName,
                       AP_FWL_MAX_RULE_NAME_LEN);

    if (pNode1->u4WtpProfileId > pNode2->u4WtpProfileId)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u4WtpProfileId < pNode2->u4WtpProfileId)
    {
        return WSSIF_RB_LESS;
    }
    else if (i4RetVal > 0)
    {
        return WSSIF_RB_GREATER;
    }
    else if (i4RetVal < 0)
    {
        return WSSIF_RB_LESS;
    }
    else if (pNode1->i4AclIfIndex > pNode2->i4AclIfIndex)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->i4AclIfIndex < pNode2->i4AclIfIndex)
    {
        return WSSIF_RB_LESS;
    }
    else if (pNode1->u1AclDirection > pNode2->u1AclDirection)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u1AclDirection < pNode2->u1AclDirection)
    {
        return WSSIF_RB_LESS;
    }
    return WSSIF_RB_EQUAL;
}

/*****************************************************************************/
/* Function Name      : WssIfWtpCompareL3SubIfRBTree                            */
/*                                                                           */
/* Description        : This function compares the two L3 SUb Interfaces     */
/*                      based on the Entry index                             */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : WSSIF_RB_GREATER, WSSIF_RB_LESS, WSSIF_RB_EQUAL      */
/*****************************************************************************/
INT4
WssIfWtpCompareL3SubIfRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssIfWtpL3SubIfDB *pNode1 = e1;
    tWssIfWtpL3SubIfDB *pNode2 = e2;

    if (pNode1->u4WtpProfileId > pNode2->u4WtpProfileId)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u4WtpProfileId < pNode2->u4WtpProfileId)
    {
        return WSSIF_RB_LESS;
    }
    else if (pNode1->u4IfIndex > pNode2->u4IfIndex)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u4IfIndex < pNode2->u4IfIndex)
    {
        return WSSIF_RB_LESS;
    }
    return WSSIF_RB_EQUAL;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpDhcpDBCreate                                   *
 *  Description     : This function used to initialize the DHCP Global Table *
 *                    profile RB Tree                                        *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpDhcpDBCreate ()
{
    gCapWtpDhcpTable =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssIfWtpDhcpPoolDB,
                                nextWtpDhcpPoolId)),
                              WssIfCompareWtpDhcpPoolRBTree);
    if (gCapWtpDhcpTable == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpDhcpDBCreate: RB Tree Creation Failed \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpDhcpDBDelete                                   *
 *  Description     : This function used to delete the DHCP Global Table     *
 *                    profile RB Tree                                        *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpDhcpDBDelete ()
{
    if (gCapWtpDhcpTable != NULL)
    {
        RBTreeDelete (gCapWtpDhcpTable);
        gCapWtpDhcpTable = NULL;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : WssIfCompareWtpDhcpPoolRBTree                        */
/*                                                                           */
/* Description        : This function compares the two DHCP Pools            */
/*                      based on the Poold Id                                */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    :                                                      */
/*****************************************************************************/
INT4
WssIfCompareWtpDhcpPoolRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssIfWtpDhcpPoolDB *pNode1 = e1;
    tWssIfWtpDhcpPoolDB *pNode2 = e2;

    if (pNode1->u4WtpProfileId > pNode2->u4WtpProfileId)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u4WtpProfileId < pNode2->u4WtpProfileId)
    {
        return WSSIF_RB_LESS;
    }
    if (pNode1->u4DhcpPoolId > pNode2->u4DhcpPoolId)
    {
        return WSSIF_RB_GREATER;
    }
    else if (pNode1->u4DhcpPoolId < pNode2->u4DhcpPoolId)
    {
        return WSSIF_RB_LESS;
    }

    return WSSIF_RB_EQUAL;
}

/*****************************************************************************
 *  Function Name   : WssIfDhcpGlobalAddEntry                                *
 *  Description     : This function used to add a new entry to the wtp
              model table RB tree                         *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/

INT4
WssIfDhcpGlobalAddEntry (tWssIfDhcpPoolDB * pDhcpPoolDB)
{
    tWssIfDhcpPoolDB   *pWssIfDhcpPoolDB = NULL;

    pWssIfDhcpPoolDB =
        (tWssIfDhcpPoolDB *) MemAllocMemBlk (CAPWAP_DHCP_GLOBAL_DB_POOLID);

    if (pWssIfDhcpPoolDB == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pWssIfDhcpPoolDB, 0, sizeof (tWssIfDhcpPoolDB));

    pWssIfDhcpPoolDB->u4DhcpPoolId = pDhcpPoolDB->u4DhcpPoolId;
    pWssIfDhcpPoolDB->u4Subnet = pDhcpPoolDB->u4Subnet;
    pWssIfDhcpPoolDB->u4Mask = pDhcpPoolDB->u4Mask;
    pWssIfDhcpPoolDB->u4StartIp = pDhcpPoolDB->u4StartIp;
    pWssIfDhcpPoolDB->u4EndIp = pDhcpPoolDB->u4EndIp;
    pWssIfDhcpPoolDB->u4LeaseExprTime = pDhcpPoolDB->u4LeaseExprTime;
    pWssIfDhcpPoolDB->u4DefaultIp = pDhcpPoolDB->u4DefaultIp;
    pWssIfDhcpPoolDB->u4DnsServerIp = pDhcpPoolDB->u4DnsServerIp;
    pWssIfDhcpPoolDB->i4RowStatus = pDhcpPoolDB->i4RowStatus;

    if (RBTreeAdd (gCapDhcpGlobalTable, pWssIfDhcpPoolDB) != RB_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfDhcpGlobalAddEntry: Failed to add entry to "
                   "the Table\r\n");
        MemReleaseMemBlock (CAPWAP_DHCP_GLOBAL_DB_POOLID,
                            (UINT1 *) pWssIfDhcpPoolDB);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpLRConfigAddEntry                               *
 *  Description     : This function used to add a new entry to the wtp
              Local Routing Config table RB tree                     *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/

INT4
WssIfWtpLRConfigAddEntry (tWssIfWtpLRConfigDB * pConfigEntry)
{
    tWssIfWtpLRConfigDB *pWssIfWtpLRConfigEntry = NULL;

    if ((pWssIfWtpLRConfigEntry =
         (tWssIfWtpLRConfigDB *)
         MemAllocMemBlk (CAPWAP_WTP_LR_CONFIG_DB_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pWssIfWtpLRConfigEntry, 0, sizeof (tWssIfWtpLRConfigDB));

    pWssIfWtpLRConfigEntry->u4WtpProfileId = pConfigEntry->u4WtpProfileId;
    pWssIfWtpLRConfigEntry->i4WtpDhcpServerStatus =
        pConfigEntry->i4WtpDhcpServerStatus;
    pWssIfWtpLRConfigEntry->i4WtpDhcpRelayStatus =
        pConfigEntry->i4WtpDhcpRelayStatus;
    pWssIfWtpLRConfigEntry->i4WtpFirewallStatus = WssIfWtpFirewallStatusGet ();
    pWssIfWtpLRConfigEntry->i4RowStatus = pConfigEntry->i4RowStatus;

    if (RBTreeAdd (gCapWtpLRConfigTable, pWssIfWtpLRConfigEntry) != RB_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpLRConfigAddEntry: Failed to add entry to "
                   "the Table\r\n");
        MemReleaseMemBlock (CAPWAP_WTP_LR_CONFIG_DB_POOLID, (UINT1 *)
                            pWssIfWtpLRConfigEntry);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpDhcpConfigAddEntry                             *
 *  Description     : This function used to add a new entry to the wtp
                      Dhcp Config table RB tree                              *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpDhcpConfigAddEntry (tWssIfWtpDhcpConfigDB * pConfigEntry)
{
    tWssIfWtpDhcpConfigDB *pWssIfWtpDhcpConfigEntry = NULL;

    pWssIfWtpDhcpConfigEntry =
        (tWssIfWtpDhcpConfigDB *)
        MemAllocMemBlk (CAPWAP_WTP_DHCP_CONFIG_DB_POOLID);
    if (pWssIfWtpDhcpConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pWssIfWtpDhcpConfigEntry, 0, sizeof (tWssIfWtpDhcpConfigDB));

    pWssIfWtpDhcpConfigEntry->u4WtpProfileId = pConfigEntry->u4WtpProfileId;
    pWssIfWtpDhcpConfigEntry->i4WtpDhcpServerStatus =
        pConfigEntry->i4WtpDhcpServerStatus;
    pWssIfWtpDhcpConfigEntry->i4WtpDhcpRelayStatus =
        pConfigEntry->i4WtpDhcpRelayStatus;

    if (RBTreeAdd (gCapWtpDhcpConfigTable, pWssIfWtpDhcpConfigEntry) !=
        RB_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpDhcpConfigAddEntry: Failed to add entry to "
                   "the Table\r\n");
        MemReleaseMemBlock (CAPWAP_WTP_DHCP_CONFIG_DB_POOLID, (UINT1 *)
                            pWssIfWtpDhcpConfigEntry);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpIpRouteConfigAddEntry                          *
 *  Description     : This function used to add a new entry to the wtp
              Ip Route Local Routing Config table RB tree            *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/

INT4
WssIfWtpIpRouteConfigAddEntry (tWssIfWtpIpRouteConfigDB * pConfigEntry)
{
    tWssIfWtpIpRouteConfigDB *pWssIfWtpIpRouteConfigEntry = NULL;

    pWssIfWtpIpRouteConfigEntry =
        (tWssIfWtpIpRouteConfigDB *)
        MemAllocMemBlk (CAPWAP_WTP_IP_ROUTE_CONFIG_DB_POOLID);

    if (pWssIfWtpIpRouteConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pWssIfWtpIpRouteConfigEntry, 0, sizeof (tWssIfWtpIpRouteConfigDB));

    pWssIfWtpIpRouteConfigEntry->u4WtpProfileId = pConfigEntry->u4WtpProfileId;
    pWssIfWtpIpRouteConfigEntry->u4Subnet = pConfigEntry->u4Subnet;
    pWssIfWtpIpRouteConfigEntry->u4Mask = pConfigEntry->u4Mask;
    pWssIfWtpIpRouteConfigEntry->u4Gateway = pConfigEntry->u4Gateway;
    pWssIfWtpIpRouteConfigEntry->i4RowStatus = pConfigEntry->i4RowStatus;

    if (RBTreeAdd (gCapWtpIpRouteConfigTable,
                   pWssIfWtpIpRouteConfigEntry) != RB_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpIpRouteConfigAddEntry: Failed to add entry to "
                   "the Table\r\n");
        MemReleaseMemBlock (CAPWAP_WTP_IP_ROUTE_CONFIG_DB_POOLID, (UINT1 *)
                            pWssIfWtpIpRouteConfigEntry);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfNATConfigAddEntry                                 *
 *  Description     : This function used to add a new entry to the NAT Config
              table RB tree                                      *
 *                                                                           *
 *  Input(s)        : pointer to node of type tWssIfNATConfigEntryDB         *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfNATConfigAddEntry (tWssIfNATConfigEntryDB * pConfigEntry)
{
    tWssIfNATConfigEntryDB *pWssIfNATConfigEntry = NULL;

    pWssIfNATConfigEntry =
        (tWssIfNATConfigEntryDB *)
        MemAllocMemBlk (CAPWAP_NAT_CONFIG_ENTRY_DB_POOLID);
    if (pWssIfNATConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pWssIfNATConfigEntry, 0, sizeof (tWssIfNATConfigEntryDB));

    pWssIfNATConfigEntry->i4NATConfigEntryIndex =
        pConfigEntry->i4NATConfigEntryIndex;
    pWssIfNATConfigEntry->u4WtpProfileId = pConfigEntry->u4WtpProfileId;
    pWssIfNATConfigEntry->i1WanType = WssIfWtpNATStatusGet ();
    pWssIfNATConfigEntry->i4RowStatus = pConfigEntry->i4RowStatus;

    if (RBTreeAdd (gCapNATConfigTable, pWssIfNATConfigEntry) != RB_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfNATConfigAddEntry: Failed to add entry to "
                   "the Table\r\n");
        MemReleaseMemBlock (CAPWAP_NAT_CONFIG_ENTRY_DB_POOLID,
                            (UINT1 *) pWssIfNATConfigEntry);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpFwlFilterAddEntry                              *
 *  Description     : This function used to add a new entry to the Firewall
                      filter RB tree                                         *
 *                                                                           *
 *  Input(s)        : pointer to node of type tWssIfWtpFwlFilterDB           *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpFwlFilterAddEntry (tWssIfWtpFwlFilterDB * pConfigEntry)
{
    tWssIfWtpFwlFilterDB *pWssIfWtpFwlFilter = NULL;

    pWssIfWtpFwlFilter =
        (tWssIfWtpFwlFilterDB *)
        MemAllocMemBlk (CAPWAP_WTP_FWL_FILTER_DB_POOLID);
    if (pWssIfWtpFwlFilter == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pWssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    pWssIfWtpFwlFilter->u4WtpProfileId = pConfigEntry->u4WtpProfileId;
    MEMCPY (pWssIfWtpFwlFilter->au1FilterName, pConfigEntry->au1FilterName,
            AP_FWL_MAX_FILTER_NAME_LEN);

    pWssIfWtpFwlFilter->u2AddrType = AP_FWL_IP_VERSION_4;
    pWssIfWtpFwlFilter->SrcStartAddr.v4Addr = AP_FWL_DEFAULT_ADDRESS;
    pWssIfWtpFwlFilter->SrcStartAddr.u4AddrType = AP_FWL_IP_VERSION_4;
    pWssIfWtpFwlFilter->SrcEndAddr.v4Addr = AP_FWL_DEFAULT_ADDRESS;
    pWssIfWtpFwlFilter->SrcEndAddr.u4AddrType = AP_FWL_IP_VERSION_4;
    pWssIfWtpFwlFilter->DestStartAddr.v4Addr = AP_FWL_DEFAULT_ADDRESS;
    pWssIfWtpFwlFilter->DestStartAddr.u4AddrType = AP_FWL_IP_VERSION_4;
    pWssIfWtpFwlFilter->DestEndAddr.v4Addr = AP_FWL_DEFAULT_ADDRESS;
    pWssIfWtpFwlFilter->DestEndAddr.u4AddrType = AP_FWL_IP_VERSION_4;
    pWssIfWtpFwlFilter->u1Proto = AP_FWL_DEFAULT_PROTO;
    pWssIfWtpFwlFilter->u2SrcMaxPort = AP_FWL_DEFAULT_PORT;
    pWssIfWtpFwlFilter->u2SrcMinPort = AP_FWL_DEFAULT_PORT;
    pWssIfWtpFwlFilter->u2DestMaxPort = AP_FWL_DEFAULT_PORT;
    pWssIfWtpFwlFilter->u2DestMinPort = AP_FWL_DEFAULT_PORT;
    pWssIfWtpFwlFilter->u1Tos = AP_FWL_TOS_ANY;
    pWssIfWtpFwlFilter->u1FilterRefCount = AP_FWL_DEFAULT_COUNT;
    pWssIfWtpFwlFilter->u4FilterHitCount = AP_FWL_DEFAULT_COUNT;
    pWssIfWtpFwlFilter->u1FilterAccounting = AP_FWL_DISABLE;
    pWssIfWtpFwlFilter->i4RowStatus = pConfigEntry->i4RowStatus;

    if (RBTreeAdd (gCapWtpFwlFilterTable, pWssIfWtpFwlFilter) != RB_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpFwlFilter: Failed to add entry to "
                   "the Table\r\n");
        MemReleaseMemBlock (CAPWAP_WTP_FWL_FILTER_DB_POOLID,
                            (UINT1 *) pWssIfWtpFwlFilter);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/* Function     : ApFwlAddDefaultRules                                      */
/*                                                                          */
/* Description  : Initialise the default firewall filters/access-lists      */
/*                rules and then enable FireWall in the particular Ap       */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None.                                                     */
/*                                                                          */
/****************************************************************************/
VOID
ApFwlAddDefaultRules (UINT4 u4CapwapBaseWtpProfileId)
{
    INT1                ai1FilterName[AP_FWL_MAX_FILTER_NAME_LEN] =
        { AP_FWL_ZERO };
    MEMSET (ai1FilterName, AP_FWL_ZERO, sizeof (ai1FilterName));
    STRNCPY (ai1FilterName, "Def_FTP_Filter", (sizeof (ai1FilterName) -
                                               AP_FWL_ONE));
    if ((ApFwlInitDefaultRule (u4CapwapBaseWtpProfileId, ai1FilterName, NULL,
                               AP_FWL_FTP_CTRL_PORT, 0,
                               AP_FWL_TCP,
                               AP_FWL_IP_VERSION_4)) != AP_FWL_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpFwlFilter: Failed to add default filter for "
                   "FTP CTRL PORT\r\n");
    }

    MEMSET (ai1FilterName, AP_FWL_ZERO, sizeof (ai1FilterName));
    STRNCPY (ai1FilterName, "Def_TELNET_Filter", (sizeof (ai1FilterName) -
                                                  AP_FWL_ONE));
    if ((ApFwlInitDefaultRule (u4CapwapBaseWtpProfileId, ai1FilterName, NULL,
                               AP_FWL_TELNET_PORT, 0,
                               AP_FWL_TCP,
                               AP_FWL_IP_VERSION_4)) != AP_FWL_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpFwlFilter: Failed to add default filter for "
                   "TELNET PORT\r\n");
    }

    MEMSET (ai1FilterName, AP_FWL_ZERO, sizeof (ai1FilterName));
    STRNCPY (ai1FilterName, "Def_SMTP_Filter", (sizeof (ai1FilterName) -
                                                AP_FWL_ONE));
    if ((ApFwlInitDefaultRule (u4CapwapBaseWtpProfileId, ai1FilterName, NULL,
                               AP_FWL_SMTP_PORT, 0,
                               AP_FWL_TCP,
                               AP_FWL_IP_VERSION_4)) != AP_FWL_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpFwlFilter: Failed to add default filter for "
                   "SMTP PORT\r\n");
    }

    MEMSET (ai1FilterName, AP_FWL_ZERO, sizeof (ai1FilterName));
    STRNCPY (ai1FilterName, "Def_DNS_TCP_Filter", (sizeof (ai1FilterName) -
                                                   AP_FWL_ONE));
    if ((ApFwlInitDefaultRule (u4CapwapBaseWtpProfileId, ai1FilterName, NULL,
                               AP_FWL_DNS_PORT, 0,
                               AP_FWL_TCP,
                               AP_FWL_IP_VERSION_4)) != AP_FWL_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpFwlFilter: Failed to add default filter for "
                   "DNS PORT\r\n");
    }
    MEMSET (ai1FilterName, AP_FWL_ZERO, sizeof (ai1FilterName));
    STRNCPY (ai1FilterName, "Def_DNS_UDP_Filter", (sizeof (ai1FilterName) -
                                                   AP_FWL_ONE));
    if ((ApFwlInitDefaultRule (u4CapwapBaseWtpProfileId, ai1FilterName, NULL,
                               AP_FWL_DNS_PORT, 0,
                               AP_FWL_UDP,
                               AP_FWL_IP_VERSION_4)) != AP_FWL_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpFwlFilter: Failed to add default filter for "
                   "DNS PORT\r\n");
    }

    MEMSET (ai1FilterName, AP_FWL_ZERO, sizeof (ai1FilterName));
    STRNCPY (ai1FilterName, "Def_HTTP_Filter", (sizeof (ai1FilterName) -
                                                AP_FWL_ONE));
    if ((ApFwlInitDefaultRule (u4CapwapBaseWtpProfileId, ai1FilterName, NULL,
                               AP_FWL_HTTP_PORT, 0,
                               AP_FWL_TCP,
                               AP_FWL_IP_VERSION_4)) != AP_FWL_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpFwlFilter: Failed to add default filter for "
                   "HTTP PORT\r\n");
    }

    MEMSET (ai1FilterName, AP_FWL_ZERO, sizeof (ai1FilterName));
    STRNCPY (ai1FilterName, "Def_HTTPS_Filter", (sizeof (ai1FilterName) -
                                                 AP_FWL_ONE));
    if ((ApFwlInitDefaultRule (u4CapwapBaseWtpProfileId, ai1FilterName, NULL,
                               AP_FWL_HTTPS_PORT, 0,
                               AP_FWL_TCP,
                               AP_FWL_IP_VERSION_4)) != AP_FWL_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpFwlFilter: Failed to add default filter for "
                   "HTTPS PORT\r\n");
    }

    MEMSET (ai1FilterName, AP_FWL_ZERO, sizeof (ai1FilterName));
    STRNCPY (ai1FilterName, "Def_POP3_Filter", (sizeof (ai1FilterName) -
                                                AP_FWL_ONE));
    if ((ApFwlInitDefaultRule (u4CapwapBaseWtpProfileId, ai1FilterName, NULL,
                               AP_FWL_POP3_PORT, 0,
                               AP_FWL_TCP,
                               AP_FWL_IP_VERSION_4)) != AP_FWL_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpFwlFilter: Failed to add default filter for "
                   "POP3 PORT\r\n");
    }

    /* there is no NAT ALG support */
    MEMSET (ai1FilterName, AP_FWL_ZERO, sizeof (ai1FilterName));
    STRNCPY (ai1FilterName, "Def_IMAP_Filter", (sizeof (ai1FilterName) -
                                                AP_FWL_ONE));
    if ((ApFwlInitDefaultRule (u4CapwapBaseWtpProfileId, ai1FilterName, NULL,
                               AP_FWL_IMAP_PORT, 0,
                               AP_FWL_TCP,
                               AP_FWL_IP_VERSION_4)) != AP_FWL_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpFwlFilter: Failed to add default filter for "
                   "IMAP PORT\r\n");
    }

    MEMSET (ai1FilterName, AP_FWL_ZERO, sizeof (ai1FilterName));
    STRNCPY (ai1FilterName, "Def_SNTP_UDP_Filter",
             (sizeof (ai1FilterName) - AP_FWL_ONE));
    if ((ApFwlInitDefaultRule (u4CapwapBaseWtpProfileId, ai1FilterName, NULL,
                               AP_FWL_SNTP_PORT, 0,
                               AP_FWL_UDP,
                               AP_FWL_IP_VERSION_4)) != AP_FWL_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpFwlFilter: Failed to add default filter for "
                   "SNTP PORT\r\n");
    }
}

/****************************************************************************/
/*                                                                          */
/* Function     : ApFwlInitDefaultRule                                      */
/*                                                                          */
/* Description  : Initialise the default firewall filters/acls/rules        */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None.                                                     */
/*                                                                          */
/****************************************************************************/
INT1
ApFwlInitDefaultRule (UINT4 u4CapwapBaseWtpProfileId, INT1 *pFilterName,
                      INT1 *pAclName, UINT4 u4Port, UINT4 u4Priority,
                      UINT1 u1Proto, UINT1 u1AddrType)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;
    STRNCPY (WssIfWtpFwlFilter.au1FilterName, pFilterName,
             STRLEN (pFilterName - 1));
    if (u1AddrType == AP_FWL_IP_VERSION_4)
    {
        WssIfWtpFwlFilter.SrcStartAddr.v4Addr = AP_FWL_ZERO;
        WssIfWtpFwlFilter.SrcEndAddr.v4Addr = AP_FWL_ZERO;
        SPRINTF ((CHR1 *) WssIfWtpFwlFilter.au1SrcAddr, "0.0.0.0/0");
        WssIfWtpFwlFilter.SrcStartAddr.u4AddrType = AP_FWL_IP_VERSION_4;
        WssIfWtpFwlFilter.SrcEndAddr.u4AddrType = AP_FWL_IP_VERSION_4;

        WssIfWtpFwlFilter.DestStartAddr.v4Addr = AP_FWL_ZERO;
        WssIfWtpFwlFilter.DestStartAddr.u4AddrType = AP_FWL_IP_VERSION_4;
        WssIfWtpFwlFilter.DestEndAddr.v4Addr = AP_FWL_ZERO;
        WssIfWtpFwlFilter.DestEndAddr.u4AddrType = AP_FWL_IP_VERSION_4;
        STRCPY (WssIfWtpFwlFilter.au1DestAddr, "0.0.0.0/0");
        WssIfWtpFwlFilter.u2AddrType = AP_FWL_IP_VERSION_4;
    }
    WssIfWtpFwlFilter.u1Proto = u1Proto;
    /* Source port can be ANY (source port > 1) */
    WssIfWtpFwlFilter.u2SrcMinPort = AP_FWL_MIN_PORT_VALUE;
    WssIfWtpFwlFilter.u2SrcMaxPort = AP_FWL_MAX_PORT_VALUE;

    STRCPY (WssIfWtpFwlFilter.au1SrcPort, ">1");

    if (u4Port != AP_FWL_DEFAULT_PORT)
    {
        /* Destination port is well-known port = u4Port */
        WssIfWtpFwlFilter.u2DestMinPort = (UINT2) (u4Port - AP_FWL_ONE);
        WssIfWtpFwlFilter.u2DestMaxPort = (UINT2) (u4Port + AP_FWL_ONE);
        SPRINTF ((CHR1 *) WssIfWtpFwlFilter.au1DestPort, "=%d", u4Port);
    }
    else
    {
        /* For destination port >1 */
        WssIfWtpFwlFilter.u2DestMinPort = AP_FWL_MIN_PORT_VALUE;
        WssIfWtpFwlFilter.u2DestMaxPort = AP_FWL_MAX_PORT_VALUE;
        STRCPY (WssIfWtpFwlFilter.au1DestPort, ">1");
    }
    /* DHCP Reply <IN> direction */

    if (u4Port == AP_FWL_DHCP_CLIENT_PORT)
    {
        STRCPY (WssIfWtpFwlFilter.au1SrcPort, "=67");
        STRCPY (WssIfWtpFwlFilter.au1DestPort, "=68");
        WssIfWtpFwlFilter.u2SrcMinPort = AP_FWL_DHCP_SERVER_PORT - AP_FWL_ONE;
        WssIfWtpFwlFilter.u2SrcMaxPort = AP_FWL_DHCP_SERVER_PORT + AP_FWL_ONE;
    }

    /* DHCP Discover <OUT> direction */

    if (u4Port == AP_FWL_DHCP_SERVER_PORT)
    {
        STRCPY (WssIfWtpFwlFilter.au1SrcPort, "=68");
        STRCPY (WssIfWtpFwlFilter.au1DestPort, "=67");
        WssIfWtpFwlFilter.u2SrcMinPort = AP_FWL_DHCP_CLIENT_PORT - AP_FWL_ONE;
        WssIfWtpFwlFilter.u2SrcMaxPort = AP_FWL_DHCP_CLIENT_PORT + AP_FWL_ONE;
    }

    WssIfWtpFwlFilter.u1Tos = AP_FWL_TOS_ANY;
    WssIfWtpFwlFilter.u4FilterHitCount = AP_FWL_DEFAULT_COUNT;
    WssIfWtpFwlFilter.u1FilterAccounting = AP_FWL_DISABLE;

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpFwlFilter.i4RowStatus = ACTIVE;

    /* ADD the Filter entry to the Database */
    UNUSED_PARAM (pAclName);
    UNUSED_PARAM (u4Priority);
    return AP_FWL_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : ApFwlConcatAddrAndMask                           */
/*                                                                          */
/*    Description        : Gets the address and mask and returns it in      */
/*                         required notation (a.b.c.d / x).                 */
/*                                                                          */
/*    Input(s)           : u4Addr       --  IP Address                      */
/*                         u4Mask       --  IP Address Mask                 */
/*                                                                          */
/*    Output(s)          : pu1Addr      -- Pointer string in required       */
/*                                         notation.                        */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
ApFwlConcatAddrAndMask (UINT4 u4Addr, UINT4 u4Mask, UINT1 *pu1Addr)
{
    UINT1               u1StringIndex = AP_FWL_ZERO;
    UINT1               u1Index = AP_FWL_ZERO;
    UINT4               u4ByteAddr = AP_FWL_ZERO;
    UINT4               u4No_Shift = AP_FWL_ZERO;
    UINT4               u4Mask1 = AP_FWL_ZERO;
    UINT1               u1DivFactor = AP_FWL_ZERO;
    INT1                i1TmpChar = AP_FWL_ZERO;

    u4No_Shift = AP_FWL_SHIFT_VALUE;
    u4Mask1 = AP_FWL_MASK_VALUE;
    u4ByteAddr = AP_FWL_ZERO;
    u1StringIndex = AP_FWL_ZERO;
    u1DivFactor = AP_FWL_ZERO;

    /* This loop is executed 4 times since the sizeof IP address is 4 bytes. */
    for (u1Index = AP_FWL_ZERO; u1Index < sizeof (UINT4); u1Index++)
    {
        /* Get the length of the each 8 bit of the IP address. The IP address
         * length of each 8 bit(in decimal form) varies from 0 to 3. So to
         * calculate the length the u1DivFactor is used. If the length is 3 it
         * is 100, else if length is  2 then iu1DivFactor is 10 else u1DivFactor
         * is 1. To find the length, each 8 bit value is dived by 100 or 10.
         */
        u4ByteAddr = ((u4Addr & u4Mask1) >> u4No_Shift);
        if ((u4ByteAddr / AP_FWL_DIV_FACTOR_100) != AP_FWL_ZERO)
        {
            u1DivFactor = AP_FWL_DIV_FACTOR_100;
        }
        else if ((u4ByteAddr / AP_FWL_DIV_FACTOR_10) != AP_FWL_ZERO)
        {
            u1DivFactor = AP_FWL_DIV_FACTOR_10;
        }
        else
        {
            u1DivFactor = AP_FWL_ONE;
        }
        /* Extract the address value and store it in dotted notation. This
         * is done for the 24 bits of the IP Address.
         */
        while (u1DivFactor != AP_FWL_ZERO)
        {
            i1TmpChar =
                (INT1) ((u4ByteAddr / u1DivFactor) + AP_FWL_ASCII_VALUE);
            *(pu1Addr + u1StringIndex) = (UINT1) i1TmpChar;
            u1StringIndex++;
            u4ByteAddr = u4ByteAddr % u1DivFactor;
            u1DivFactor /= ((UINT1) AP_FWL_DIV_FACTOR_10);
        }                        /* end of while */

        /* extract the last 8 bit of the IP address */
        *(pu1Addr + u1StringIndex) = '.';
        u1StringIndex++;
        u4No_Shift = u4No_Shift - AP_FWL_REMAINING_SHIFT_VALUE;
        u4Mask1 = u4Mask1 >> AP_FWL_REMAINING_SHIFT_VALUE;
    }                            /* end of for */

    u1StringIndex--;
    *(pu1Addr + u1StringIndex) = '/';
    u1StringIndex++;
    u4ByteAddr = u4Mask;
    u1Index = AP_FWL_ZERO;

    /* get the mask value . It is got by counting the number of 1's in the
     * Mask variable.
     */
    while (u4ByteAddr != AP_FWL_ZERO)
    {
        if ((u4ByteAddr & AP_FWL_MASK_1_BIT) != AP_FWL_ZERO)
        {
            u1Index++;
        }
        u4ByteAddr = (u4ByteAddr >> AP_FWL_ONE);
    }                            /* end of while */

    /* concat the mask value with the address  and the required notaion
     * is got.
     */
    *(pu1Addr + u1StringIndex) =
        (UINT1) ((u1Index / AP_FWL_DIV_FACTOR_10) + AP_FWL_ASCII_VALUE);
    u1StringIndex++;
    *(pu1Addr + u1StringIndex) =
        (UINT1) ((u1Index % AP_FWL_DIV_FACTOR_10) + AP_FWL_ASCII_VALUE);
    u1StringIndex++;
    *(pu1Addr + u1StringIndex) = '\0';

    /* FWL_DBG1(FWL_DBG_CONFIG, "\n Address and MAsk value = %s \n", pu1Addr); */
}                                /* End of function -- FwlConcatAddrAndmask */

/*****************************************************************************
 *  Function Name   :  *
 *  Description     : This function used to add default entry to the Firewall
                      filter RB tree                                         *
 *                                                                           *
 *  Input(s)        : pointer to node of type tWssIfWtpFwlFilterDB           *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpFwlFilterAddDefEntry (tWssIfWtpFwlFilterDB * pConfigEntry)
{
    tWssIfWtpFwlFilterDB *pWssIfWtpFwlFilter = NULL;

    pWssIfWtpFwlFilter =
        (tWssIfWtpFwlFilterDB *)
        MemAllocMemBlk (CAPWAP_WTP_FWL_FILTER_DB_POOLID);
    if (pWssIfWtpFwlFilter == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pWssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    pWssIfWtpFwlFilter->u4WtpProfileId = pConfigEntry->u4WtpProfileId;
    MEMCPY (pWssIfWtpFwlFilter->au1FilterName, pConfigEntry->au1FilterName,
            AP_FWL_MAX_FILTER_NAME_LEN);
    MEMCPY (pWssIfWtpFwlFilter->au1SrcAddr, pConfigEntry->au1SrcAddr,
            STRLEN (pConfigEntry->au1SrcAddr));
    MEMCPY (pWssIfWtpFwlFilter->au1DestAddr, pConfigEntry->au1DestAddr,
            STRLEN (pConfigEntry->au1DestAddr));
    MEMCPY (pWssIfWtpFwlFilter->au1SrcPort, pConfigEntry->au1SrcPort,
            STRLEN (pConfigEntry->au1SrcPort));
    MEMCPY (pWssIfWtpFwlFilter->au1DestPort, pConfigEntry->au1DestPort,
            STRLEN (pConfigEntry->au1DestPort));
    pWssIfWtpFwlFilter->u2AddrType = pConfigEntry->u2AddrType;
    pWssIfWtpFwlFilter->SrcStartAddr.v4Addr = pConfigEntry->SrcStartAddr.v4Addr;
    pWssIfWtpFwlFilter->SrcStartAddr.u4AddrType =
        pConfigEntry->SrcStartAddr.u4AddrType;
    pWssIfWtpFwlFilter->SrcEndAddr.v4Addr = pConfigEntry->SrcEndAddr.v4Addr;
    pWssIfWtpFwlFilter->SrcEndAddr.u4AddrType =
        pConfigEntry->SrcEndAddr.u4AddrType;
    pWssIfWtpFwlFilter->DestStartAddr.v4Addr =
        pConfigEntry->DestStartAddr.v4Addr;
    pWssIfWtpFwlFilter->DestStartAddr.u4AddrType =
        pConfigEntry->DestStartAddr.u4AddrType;
    pWssIfWtpFwlFilter->DestEndAddr.v4Addr = pConfigEntry->DestEndAddr.v4Addr;
    pWssIfWtpFwlFilter->DestEndAddr.u4AddrType =
        pConfigEntry->DestEndAddr.u4AddrType;
    pWssIfWtpFwlFilter->u1Proto = pConfigEntry->u1Proto;
    pWssIfWtpFwlFilter->u2SrcMaxPort = pConfigEntry->u2SrcMaxPort;
    pWssIfWtpFwlFilter->u2SrcMinPort = pConfigEntry->u2SrcMinPort;
    pWssIfWtpFwlFilter->u2DestMaxPort = pConfigEntry->u2DestMaxPort;
    pWssIfWtpFwlFilter->u2DestMinPort = pConfigEntry->u2DestMinPort;
    pWssIfWtpFwlFilter->u1Tos = pConfigEntry->u1Tos;
    pWssIfWtpFwlFilter->u1FilterRefCount = pConfigEntry->u1FilterRefCount;
    pWssIfWtpFwlFilter->u4FilterHitCount = pConfigEntry->u4FilterHitCount;
    pWssIfWtpFwlFilter->u1FilterAccounting = pConfigEntry->u1FilterAccounting;
    pWssIfWtpFwlFilter->i4RowStatus = pConfigEntry->i4RowStatus;

    if (RBTreeAdd (gCapWtpFwlFilterTable, pWssIfWtpFwlFilter) != RB_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpFwlFilter: Failed to add entry to "
                   "the Table\r\n");
        MemReleaseMemBlock (CAPWAP_WTP_FWL_FILTER_DB_POOLID,
                            (UINT1 *) pWssIfWtpFwlFilter);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpFwlRuleAddEntry                                *
 *  Description     : This function used to add a new entry to the firewall
 *                      rule table                                           *
 *                                                                           *
 *  Input(s)        : pointer to node of type tWssIfWtpFwlRuleDB             *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpFwlRuleAddEntry (tWssIfWtpFwlRuleDB * pConfigEntry)
{
    tWssIfWtpFwlRuleDB *pWssIfWtpFwlRule = NULL;
    UINT1               u1Index = AP_FWL_ZERO;

    pWssIfWtpFwlRule =
        (tWssIfWtpFwlRuleDB *) MemAllocMemBlk (CAPWAP_WTP_FWL_RULE_DB_POOLID);
    if (pWssIfWtpFwlRule == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pWssIfWtpFwlRule, AP_FWL_ZERO, sizeof (tWssIfWtpFwlRuleDB));

    pWssIfWtpFwlRule->u4WtpProfileId = pConfigEntry->u4WtpProfileId;
    MEMCPY (pWssIfWtpFwlRule->au1RuleName, pConfigEntry->au1RuleName,
            STRLEN (pConfigEntry->au1RuleName));
    STRCPY (pWssIfWtpFwlRule->au1FilterSet, AP_FWL_NULL_STRING);
    for (u1Index = AP_FWL_ZERO; u1Index < AP_FWL_MAX_FILTERS_IN_RULE; u1Index++)
    {
        pWssIfWtpFwlRule->apFilterInfo[u1Index] = NULL;
    }
    pWssIfWtpFwlRule->u2MinSrcPort = AP_FWL_ZERO;
    pWssIfWtpFwlRule->u2MaxSrcPort = AP_FWL_ZERO;
    pWssIfWtpFwlRule->u2MinDestPort = AP_FWL_ZERO;
    pWssIfWtpFwlRule->u2MaxDestPort = AP_FWL_ZERO;
    pWssIfWtpFwlRule->u1FilterConditionFlag = AP_FWL_ZERO;
    pWssIfWtpFwlRule->u1RuleRefCount = AP_FWL_DEFAULT_COUNT;
    pWssIfWtpFwlRule->i4RowStatus = NOT_READY;

    if (RBTreeAdd (gCapWtpFwlRuleTable, pWssIfWtpFwlRule) != RB_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpFwlRuleAddEntry: Failed to add entry to "
                   "the Table\r\n");
        MemReleaseMemBlock (CAPWAP_WTP_FWL_RULE_DB_POOLID,
                            (UINT1 *) pWssIfWtpFwlRule);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpFwlAclAddEntry                                 *
 *  Description     : This function used to add a new entry to the firewall
 *                      Acl table                                            *
 *                                                                           *
 *  Input(s)        : pointer to node of type tWssIfWtpFwlAclDB              *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpFwlAclAddEntry (tWssIfWtpFwlAclDB * pConfigEntry)
{
    tWssIfWtpFwlAclDB  *pWssIfWtpFwlAcl = NULL;

    pWssIfWtpFwlAcl =
        (tWssIfWtpFwlAclDB *) MemAllocMemBlk (CAPWAP_WTP_FWL_ACL_DB_POOLID);
    if (pWssIfWtpFwlAcl == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pWssIfWtpFwlAcl, 0, sizeof (tWssIfWtpFwlAclDB));

    pWssIfWtpFwlAcl->u4WtpProfileId = pConfigEntry->u4WtpProfileId;
    MEMCPY (pWssIfWtpFwlAcl->au1AclName, pConfigEntry->au1AclName,
            AP_FWL_MAX_ACL_NAME_LEN);
    pWssIfWtpFwlAcl->i4AclIfIndex = pConfigEntry->i4AclIfIndex;
    pWssIfWtpFwlAcl->u1AclDirection = pConfigEntry->u1AclDirection;
    pWssIfWtpFwlAcl->u4StartTime = pConfigEntry->u4StartTime;
    pWssIfWtpFwlAcl->u4EndTime = pConfigEntry->u4EndTime;
    pWssIfWtpFwlAcl->u4Scheduled = pConfigEntry->u4Scheduled;
    pWssIfWtpFwlAcl->u2SeqNum = pConfigEntry->u2SeqNum;
    pWssIfWtpFwlAcl->u1Action = pConfigEntry->u1Action;
    pWssIfWtpFwlAcl->u1LogTrigger = pConfigEntry->u1LogTrigger;
    pWssIfWtpFwlAcl->u1FragAction = pConfigEntry->u1FragAction;
    pWssIfWtpFwlAcl->u1AclType = pConfigEntry->u1AclType;
    pWssIfWtpFwlAcl->i4RowStatus = pConfigEntry->i4RowStatus;

    if (RBTreeAdd (gCapWtpFwlAclTable, pWssIfWtpFwlAcl) != RB_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpFwlAclAddEntry: Failed to add entry to "
                   "the Table\r\n");
        MemReleaseMemBlock (CAPWAP_WTP_FWL_ACL_DB_POOLID,
                            (UINT1 *) pWssIfWtpFwlAcl);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpDhcpPoolAddEntry                               *
 *  Description     : This function used to add a new entry to the wtp
              model table RB tree                         *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpDhcpPoolAddEntry (tWssIfWtpDhcpPoolDB * pWtpDhcpPoolDB)
{
    tWssIfWtpDhcpPoolDB *pWssIfWtpDhcpPoolDB = NULL;

    pWssIfWtpDhcpPoolDB =
        (tWssIfWtpDhcpPoolDB *) MemAllocMemBlk (CAPWAP_WTP_DHCP_DB_POOLID);
    if (pWssIfWtpDhcpPoolDB == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pWssIfWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));

    pWssIfWtpDhcpPoolDB->u4WtpProfileId = pWtpDhcpPoolDB->u4WtpProfileId;
    pWssIfWtpDhcpPoolDB->u4DhcpPoolId = pWtpDhcpPoolDB->u4DhcpPoolId;
    pWssIfWtpDhcpPoolDB->u4Subnet = pWtpDhcpPoolDB->u4Subnet;
    pWssIfWtpDhcpPoolDB->u4Mask = pWtpDhcpPoolDB->u4Mask;
    pWssIfWtpDhcpPoolDB->u4StartIp = pWtpDhcpPoolDB->u4StartIp;
    pWssIfWtpDhcpPoolDB->u4EndIp = pWtpDhcpPoolDB->u4EndIp;
    pWssIfWtpDhcpPoolDB->u4LeaseExprTime = 3600;
    pWssIfWtpDhcpPoolDB->u4DefaultIp = pWtpDhcpPoolDB->u4DefaultIp;
    pWssIfWtpDhcpPoolDB->u4DnsServerIp = pWtpDhcpPoolDB->u4DnsServerIp;
    pWssIfWtpDhcpPoolDB->i4RowStatus = pWtpDhcpPoolDB->i4RowStatus;

    if (RBTreeAdd (gCapWtpDhcpTable, pWssIfWtpDhcpPoolDB) != RB_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpDhcpGlobalAddEntry: Failed to add entry to "
                   "the Table\r\n");
        MemReleaseMemBlock (CAPWAP_WTP_DHCP_DB_POOLID,
                            (UINT1 *) pWssIfWtpDhcpPoolDB);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpL3SubIfAddEntry                                *
 *  Description     : This function used to add a new entry to the L3 SUB 
 *                    interface table                                        *
 *                                                                           *
 *  Input(s)        : pointer to node of type tWssIfWtpL3SubIfDB             *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpL3SubIfAddEntry (tWssIfWtpL3SubIfDB * pConfigEntry)
{
    tWssIfWtpL3SubIfDB *pWssIfWtpL3SubIfEntry = NULL;

    pWssIfWtpL3SubIfEntry =
        (tWssIfWtpL3SubIfDB *) MemAllocMemBlk (CAPWAP_L3_SUB_IF_DB_POOLID);
    if (pWssIfWtpL3SubIfEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pWssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));

    pWssIfWtpL3SubIfEntry->u4WtpProfileId = pConfigEntry->u4WtpProfileId;
    pWssIfWtpL3SubIfEntry->u4IfIndex = pConfigEntry->u4IfIndex;
    pWssIfWtpL3SubIfEntry->u4PhyPort = pConfigEntry->u4PhyPort;
    pWssIfWtpL3SubIfEntry->u2VlanId = pConfigEntry->u2VlanId;
    pWssIfWtpL3SubIfEntry->i4IfType = pConfigEntry->i4IfType;
    pWssIfWtpL3SubIfEntry->u4IpAddr = 0;
    pWssIfWtpL3SubIfEntry->u4BroadcastAddr = 0xffffffff;
    pWssIfWtpL3SubIfEntry->u4SubnetMask = 0;
    pWssIfWtpL3SubIfEntry->i4IfNwType = AP_NETWORK_TYPE_LAN;
    pWssIfWtpL3SubIfEntry->u1IfAdminStatus = AP_IF_DOWN;
    pWssIfWtpL3SubIfEntry->i4RowStatus = NOT_READY;

    if (RBTreeAdd (gCapWtpL3SubIfTable, pWssIfWtpL3SubIfEntry) != RB_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpL3SubIfAddEntry: Failed to add entry to "
                   "the Table\r\n");
        MemReleaseMemBlock (CAPWAP_L3_SUB_IF_DB_POOLID,
                            (UINT1 *) pWssIfWtpL3SubIfEntry);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfDhcpGlobalDeleteEntry                             *
 *  Description     : This function used to delete a new entry to the wtp    *
              model table RB tree                         *
 *                                                                           *
 *  Input(s)        : pointer to node of type tWssIfDhcpPoolDB               *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/

INT4
WssIfDhcpGlobalDeleteEntry (tWssIfDhcpPoolDB * pDhcpPoolDB)
{
    tWssIfDhcpPoolDB   *pWssIfDhcpPoolDB = NULL;

    pWssIfDhcpPoolDB =
        (tWssIfDhcpPoolDB *) RBTreeGet (gCapDhcpGlobalTable, pDhcpPoolDB);

    if (pWssIfDhcpPoolDB == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfDhcpGlobalDeleteEntry: Failed to delete entry to "
                   "the Table\r\n");
        return OSIX_FAILURE;
    }
    RBTreeRem (gCapDhcpGlobalTable, pWssIfDhcpPoolDB);
    MemReleaseMemBlock (CAPWAP_DHCP_GLOBAL_DB_POOLID,
                        (UINT1 *) pWssIfDhcpPoolDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfNATConfigDeleteEntry                              *
 *  Description     : This function used to delete an entry from the NAT
 *                Config Entry table RB tree                          *
 *                                                                           *
 *  Input(s)        : pointer to node of type tWssIfNATConfigEntryDB         *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfNATConfigDeleteEntry (tWssIfNATConfigEntryDB * pConfigEntry)
{
    tWssIfNATConfigEntryDB *pWssIfNATConfigEntry = NULL;

    pWssIfNATConfigEntry =
        (tWssIfNATConfigEntryDB *) RBTreeGet (gCapNATConfigTable, pConfigEntry);

    if (pWssIfNATConfigEntry == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfNATConfigDeleteEntry: Failed to delete entry to "
                   "the Table\r\n");
        return OSIX_FAILURE;
    }
    RBTreeRem (gCapNATConfigTable, pWssIfNATConfigEntry);
    MemReleaseMemBlock (CAPWAP_NAT_CONFIG_ENTRY_DB_POOLID,
                        (UINT1 *) pWssIfNATConfigEntry);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpLRConfigDeleteEntry                            *
 *  Description     : This function used to delete an entry from the Wtp
 *                    Local routing Config Entry table RB tree                 *
 *                                                                           *
 *  Input(s)        : pointer to node of type tWssIfWtpLRConfigDB            *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpLRConfigDeleteEntry (tWssIfWtpLRConfigDB * pConfigEntry)
{
    tWssIfWtpLRConfigDB *pWssIfWtpLRConfigEntry = NULL;

    pWssIfWtpLRConfigEntry = (tWssIfWtpLRConfigDB *) RBTreeGet
        (gCapWtpLRConfigTable, pConfigEntry);

    if (pWssIfWtpLRConfigEntry == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpLRConfigDeleteEntry: Failed to delete entry to "
                   "the Table\r\n");
        return OSIX_FAILURE;
    }
    RBTreeRem (gCapWtpLRConfigTable, pWssIfWtpLRConfigEntry);
    MemReleaseMemBlock (CAPWAP_WTP_LR_CONFIG_DB_POOLID,
                        (UINT1 *) pWssIfWtpLRConfigEntry);
    pWssIfWtpLRConfigEntry = NULL;
    return OSIX_SUCCESS;
}

INT4
WssIfWtpDhcpConfigDeleteEntry (tWssIfWtpDhcpConfigDB * pConfigEntry)
{
    tWssIfWtpDhcpConfigDB *pWssIfWtpDhcpConfigEntry = NULL;

    pWssIfWtpDhcpConfigEntry = (tWssIfWtpDhcpConfigDB *) RBTreeGet
        (gCapWtpDhcpConfigTable, pConfigEntry);

    if (pWssIfWtpDhcpConfigEntry == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpDhcpConfigDeleteEntry: Failed to delete entry to "
                   "the Table\r\n");
        return OSIX_FAILURE;
    }
    RBTreeRem (gCapWtpDhcpConfigTable, pWssIfWtpDhcpConfigEntry);
    MemReleaseMemBlock (CAPWAP_WTP_DHCP_CONFIG_DB_POOLID,
                        (UINT1 *) pWssIfWtpDhcpConfigEntry);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpIpRouteConfigDeleteEntry                       *
 *  Description     : This function used to delete an entry from the Wtp
 *                    Local routing Ip Route Config Entry table RB tree      *
 *                                                                           *
 *  Input(s)        : pointer to node of type tWssIfWtpIpRouteConfigDB       *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpIpRouteConfigDeleteEntry (tWssIfWtpIpRouteConfigDB * pConfigEntry)
{
    tWssIfWtpIpRouteConfigDB *pWssIfWtpIpRouteConfigEntry = NULL;

    pWssIfWtpIpRouteConfigEntry = (tWssIfWtpIpRouteConfigDB *) RBTreeGet
        (gCapWtpIpRouteConfigTable, pConfigEntry);

    if (pWssIfWtpIpRouteConfigEntry == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpIpRouteConfigDeleteEntry: Failed to delete entry to "
                   "the Table\r\n");
        return OSIX_FAILURE;
    }
    RBTreeRem (gCapWtpIpRouteConfigTable, pWssIfWtpIpRouteConfigEntry);
    MemReleaseMemBlock (CAPWAP_WTP_IP_ROUTE_CONFIG_DB_POOLID,
                        (UINT1 *) pWssIfWtpIpRouteConfigEntry);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpDhcpPoolDeleteEntry                            *
 *  Description     : This function used to delete a new entry to the wtp    *
              model table RB tree                         *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpDhcpPoolDeleteEntry (tWssIfWtpDhcpPoolDB * pWtpDhcpPoolDB)
{
    tWssIfWtpDhcpPoolDB *pWssIfWtpDhcpPoolDB = NULL;

    pWssIfWtpDhcpPoolDB =
        (tWssIfWtpDhcpPoolDB *) RBTreeGet (gCapWtpDhcpTable, pWtpDhcpPoolDB);

    if (pWssIfWtpDhcpPoolDB == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpDhcpGlobalDeleteEntry: Failed to delete entry to "
                   "the Table\r\n");
        return OSIX_FAILURE;
    }
    RBTreeRem (gCapWtpDhcpTable, pWssIfWtpDhcpPoolDB);
    MemReleaseMemBlock (CAPWAP_WTP_DHCP_DB_POOLID,
                        (UINT1 *) pWssIfWtpDhcpPoolDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfDhcpGlobalGetFirstEntry                           *
 *  Description     : This function used to get first entry of the global    *
              DHCP table RB tree                         *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfDhcpGlobalGetFirstEntry ()
{
    tWssIfDhcpPoolDB   *pWssIfDhcpPoolDB = NULL;
    pWssIfDhcpPoolDB =
        (tWssIfDhcpPoolDB *) RBTreeGetFirst (gCapDhcpGlobalTable);

    if (pWssIfDhcpPoolDB == NULL)
    {
        return 0;
    }
    return pWssIfDhcpPoolDB->u4DhcpPoolId;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpFwlAclDeleteEntry                              *
 *  Description     : This function used to delete a entry from the wtp      *
                      firewall Acl table                                     *
 *                                                                           *
 *  Input(s)        : pointer to node of type tWssIfWtpFwlAclDB              *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/

INT4
WssIfWtpFwlAclDeleteEntry (tWssIfWtpFwlAclDB * pWtpFwlAcl)
{
    tWssIfWtpFwlAclDB  *pWssIfWtpFwlAcl = NULL;

    pWssIfWtpFwlAcl = (tWssIfWtpFwlAclDB *)
        RBTreeGet (gCapWtpFwlAclTable, pWtpFwlAcl);
    if (pWssIfWtpFwlAcl == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpFwlAclDeleteEntry: Failed to delete entry to "
                   "the Table\r\n");
        return OSIX_FAILURE;
    }
    RBTreeRem (gCapWtpFwlAclTable, pWssIfWtpFwlAcl);
    MemReleaseMemBlock (CAPWAP_WTP_FWL_ACL_DB_POOLID,
                        (UINT1 *) pWssIfWtpFwlAcl);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpDhcpPoolGetFirstEntry                          *
 *  Description     : This function used to get first entry of the Wtp       *
              DHCP pool table RB tree                         *
 *                                                                           *
 *  Input(s)        : pu4WtpProfileId, pi4PoolIndex                          *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpDhcpPoolGetFirstEntry (UINT4 *pu4WtpProfileId, INT4 *pi4PoolIndex)
{
    tWssIfWtpDhcpPoolDB *pWssIfWtpDhcpPoolDB = NULL;
    pWssIfWtpDhcpPoolDB =
        (tWssIfWtpDhcpPoolDB *) RBTreeGetFirst (gCapWtpDhcpTable);
    if (pWssIfWtpDhcpPoolDB == NULL)
    {
        return OSIX_FAILURE;
    }
    *pu4WtpProfileId = pWssIfWtpDhcpPoolDB->u4WtpProfileId;
    *pi4PoolIndex = (INT4) pWssIfWtpDhcpPoolDB->u4DhcpPoolId;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfNATConfigGetFirstEntry                            *
 *  Description     : This function used to get first entry of the NAT Config*
              Entry table RB tree                         *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : pu4BaseProfileId, pi4EntryIndex                        *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfNATConfigGetFirstEntry (UINT4 *pu4BaseProfileId, INT4 *pi4EntryIndex)
{
    tWssIfNATConfigEntryDB *pWssIfNATConfigEntry = NULL;

    pWssIfNATConfigEntry = (tWssIfNATConfigEntryDB *)
        RBTreeGetFirst (gCapNATConfigTable);

    if (pWssIfNATConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    *pu4BaseProfileId = pWssIfNATConfigEntry->u4WtpProfileId;
    *pi4EntryIndex = pWssIfNATConfigEntry->i4NATConfigEntryIndex;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpLRConfigGetFirstEntry                          *
 *  Description     : This function used to get first entry of the Wtp Local
 *                    Routing  Config Entry table RB tree                    *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : pu4BaseProfileId                                       *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpLRConfigGetFirstEntry (UINT4 *pu4BaseProfileId)
{
    tWssIfWtpLRConfigDB *pWssIfWtpLRConfigEntry = NULL;

    pWssIfWtpLRConfigEntry = (tWssIfWtpLRConfigDB *)
        RBTreeGetFirst (gCapWtpLRConfigTable);

    if (pWssIfWtpLRConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    *pu4BaseProfileId = pWssIfWtpLRConfigEntry->u4WtpProfileId;
    return OSIX_SUCCESS;
}

INT4
WssIfWtpDhcpConfigGetFirstEntry (UINT4 *pu4BaseProfileId)
{
    tWssIfWtpDhcpConfigDB *pWssIfWtpDhcpConfigEntry = NULL;

    pWssIfWtpDhcpConfigEntry = (tWssIfWtpDhcpConfigDB *)
        RBTreeGetFirst (gCapWtpDhcpConfigTable);

    if (pWssIfWtpDhcpConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    *pu4BaseProfileId = pWssIfWtpDhcpConfigEntry->u4WtpProfileId;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpIpRouteConfigGetFirstEntry                     *
 *  Description     : This function used to get first entry of the Wtp Local
 *                    Routing Ip Route Config Entry table RB tree            *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : pu4BaseProfileId                                       *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpIpRouteConfigGetFirstEntry (UINT4 *pu4BaseProfileId,
                                    UINT4 *pu4FsWtpIpRouteConfigSubnet,
                                    UINT4 *pu4FsWtpIpRouteConfigNetMask,
                                    UINT4 *pu4FsWtpIpRouteConfigGateway)
{
    tWssIfWtpIpRouteConfigDB *pWssIfWtpIpRouteConfigEntry = NULL;

    pWssIfWtpIpRouteConfigEntry = (tWssIfWtpIpRouteConfigDB *)
        RBTreeGetFirst (gCapWtpIpRouteConfigTable);

    if (pWssIfWtpIpRouteConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    *pu4BaseProfileId = pWssIfWtpIpRouteConfigEntry->u4WtpProfileId;
    *pu4FsWtpIpRouteConfigSubnet = pWssIfWtpIpRouteConfigEntry->u4Subnet;
    *pu4FsWtpIpRouteConfigNetMask = pWssIfWtpIpRouteConfigEntry->u4Mask;
    *pu4FsWtpIpRouteConfigGateway = pWssIfWtpIpRouteConfigEntry->u4Gateway;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfDhcpGlobalGetNextEntry                            *
 **  Description     : This function used to get next entry of the current    *
               entry whose Pool Id is given,  from global DHCP        *
 *                     table RB tree                                          *
 **  Input(s)        : None                                                   *
 **  Output(s)       : None                                                   *
 **  Returns         : OSIX_FAILURE on failure                                *
 *                     OSIX_SUCCESS on success                                *
 *******************************************************************************/

INT4
WssIfDhcpGlobalGetNextEntry (INT4 *pi4FsWlanDhcpSrvSubnetPoolIndex)
{
    tWssIfDhcpPoolDB   *pWssIfDhcpPoolDB = NULL;
    tWssIfDhcpPoolDB   *pWssIfDhcpNextPoolDB = NULL;

    pWssIfDhcpNextPoolDB = (tWssIfDhcpPoolDB *) RBTreeGetNext
        (gCapDhcpGlobalTable, (tRBElem *) pWssIfDhcpPoolDB, NULL);
    if (pWssIfDhcpNextPoolDB == NULL)
    {
        return 0;
    }
    return (pWssIfDhcpNextPoolDB->u4DhcpPoolId);
    UNUSED_PARAM (pi4FsWlanDhcpSrvSubnetPoolIndex);
}

/*****************************************************************************
 **  Function Name   : WssIfWtpDhcpPoolGetNextEntry                           *
 **  Description     : This function used to get next entry of the current    *
               entry whose wtp Profile Id is given,  from WTP DHCP    *
 *                     pool RB tree                                           *
 **  Input(s)        : u4WtpProfileId, pu4WtpProfileId,
             i4PoolIndex, pi4PoolIndex                            *
 **  Output(s)       : None                                                   *
 **  Returns         : OSIX_FAILURE on failure                                *
 *                     OSIX_SUCCESS on success                                *
 *******************************************************************************/
INT4
WssIfWtpDhcpPoolGetNextEntry (UINT4 u4WtpProfileId, UINT4 *pu4WtpProfileId,
                              UINT4 u4PoolIndex, UINT4 *pu4PoolIndex)
{
    tWssIfWtpDhcpPoolDB WssIfWtpDhcpPoolDB;
    tWssIfWtpDhcpPoolDB *pWssIfWtpDhcpPoolDB = NULL;

    MEMSET (&WssIfWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));

    WssIfWtpDhcpPoolDB.u4WtpProfileId = u4WtpProfileId;
    WssIfWtpDhcpPoolDB.u4DhcpPoolId = u4PoolIndex;
    pWssIfWtpDhcpPoolDB = (tWssIfWtpDhcpPoolDB *) RBTreeGetNext
        (gCapWtpDhcpTable, (tRBElem *) & WssIfWtpDhcpPoolDB, NULL);
    if (pWssIfWtpDhcpPoolDB == NULL)
    {
        return OSIX_FAILURE;
    }
    *pu4WtpProfileId = pWssIfWtpDhcpPoolDB->u4WtpProfileId;
    *pu4PoolIndex = (INT4) pWssIfWtpDhcpPoolDB->u4DhcpPoolId;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfNATConfigGetNextEntry                             *
 **  Description     : This function used to get next entry of the current    *
               entry whose Entry Index is given,  from NAT Config     *
 *                     table RB tree                                          *
 **  Input(s)        : pi4NATConfigEntryIndex,pu4WtpProfileId                 *
 **  Output(s)       : pu4NextWtpProfileId. pi4NextNATConfigEntryIndex        *
 **  Returns         : OSIX_SUCCESS on success
                       OSIX_FAILURE on failure                                *
 *******************************************************************************/
INT4
WssIfNATConfigGetNextEntry (INT4 *pi4NATConfigEntryIndex,
                            UINT4 *pu4WtpProfileId, UINT4 *pu4NextWtpProfileId,
                            INT4 *pi4NextNATConfigEntryIndex)
{
    tWssIfNATConfigEntryDB WssIfNATConfigEntry;
    tWssIfNATConfigEntryDB WssIfNextNATConfigEntry;
    tWssIfNATConfigEntryDB *pWssIfNATConfigEntry = &WssIfNATConfigEntry;
    tWssIfNATConfigEntryDB *pWssIfNextNATConfigEntry = NULL;

    MEMSET (&WssIfNATConfigEntry, 0, sizeof (tWssIfNATConfigEntryDB));
    MEMSET (&WssIfNextNATConfigEntry, 0, sizeof (tWssIfNATConfigEntryDB));

    pWssIfNATConfigEntry->i4NATConfigEntryIndex = *pi4NATConfigEntryIndex;
    pWssIfNATConfigEntry->u4WtpProfileId = *pu4WtpProfileId;
    pWssIfNextNATConfigEntry = (tWssIfNATConfigEntryDB *) RBTreeGetNext
        (gCapNATConfigTable, (tRBElem *) pWssIfNATConfigEntry, NULL);
    if (pWssIfNextNATConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    *pu4NextWtpProfileId = pWssIfNextNATConfigEntry->u4WtpProfileId;
    *pi4NextNATConfigEntryIndex =
        pWssIfNextNATConfigEntry->i4NATConfigEntryIndex;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfWtpLRConfigGetNextEntry                           *
 **  Description     : This function used to get next entry of the current    *
               entry whose Entry Index is given,  from Wtp Local 
                       Routing Config table RB tree                           *
 **  Input(s)        : pu4WtpProfileId - Current Wtp Profile Id               *
 **  Output(s)       : pu4NextWtpProfileId - Next Wtp Profile Id              *
 **  Returns         : OSIX_SUCCESS on Success
                       OSIX_FAILURE on Failure                                *
 *****************************************************************************/
INT4
WssIfWtpLRConfigGetNextEntry (UINT4 *pu4WtpProfileId,
                              UINT4 *pu4NextWtpProfileId)
{
    tWssIfWtpLRConfigDB WssIfWtpLRConfigEntry;
    tWssIfWtpLRConfigDB *pWssIfWtpLRConfigEntry = &WssIfWtpLRConfigEntry;
    tWssIfWtpLRConfigDB *pWssIfNextWtpLRConfigEntry = NULL;

    MEMSET (&WssIfWtpLRConfigEntry, 0, sizeof (tWssIfWtpLRConfigDB));

    pWssIfWtpLRConfigEntry->u4WtpProfileId = *pu4WtpProfileId;
    pWssIfNextWtpLRConfigEntry = (tWssIfWtpLRConfigDB *) RBTreeGetNext
        (gCapWtpLRConfigTable, (tRBElem *) pWssIfWtpLRConfigEntry, NULL);
    if (pWssIfNextWtpLRConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    *pu4NextWtpProfileId = pWssIfNextWtpLRConfigEntry->u4WtpProfileId;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfWtpDhcpConfigGetNextEntry                         *
 **  Description     : This function used to get next entry of the current    *
               entry whose Entry Index is given,  from Wtp Local 
                       Routing Config table RB tree                           *
 **  Input(s)        : pu4WtpProfileId - Current Wtp Profile Id               *
 **  Output(s)       : pu4NextWtpProfileId - Next Wtp Profile Id              *
 **  Returns         : OSIX_SUCCESS on Success
                       OSIX_FAILURE on Failure                                *
 *****************************************************************************/

INT4
WssIfWtpDhcpConfigGetNextEntry (UINT4 *pu4WtpProfileId,
                                UINT4 *pu4NextWtpProfileId)
{
    tWssIfWtpDhcpConfigDB WssIfWtpDhcpConfigEntry;
    tWssIfWtpDhcpConfigDB *pWssIfNextWtpDhcpConfigEntry = NULL;

    MEMSET (&WssIfWtpDhcpConfigEntry, 0, sizeof (tWssIfWtpDhcpConfigDB));

    WssIfWtpDhcpConfigEntry.u4WtpProfileId = *pu4WtpProfileId;

    pWssIfNextWtpDhcpConfigEntry = (tWssIfWtpDhcpConfigDB *) RBTreeGetNext
        (gCapWtpDhcpConfigTable, (tRBElem *) & WssIfWtpDhcpConfigEntry, NULL);
    if (pWssIfNextWtpDhcpConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    *pu4NextWtpProfileId = pWssIfNextWtpDhcpConfigEntry->u4WtpProfileId;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfWtpIpRouteConfigGetNextEntry                      *
 **  Description     : This function used to get next entry of the current    *
               entry whose Entry Index is given,  from Wtp Local 
                       Routing Ip Route Config table RB tree                  *
 **  Input(s)        : pu4WtpProfileId - Current Wtp Profile Id               *
 **  Output(s)       : pu4NextWtpProfileId - Next Wtp Profile Id              *
 **  Returns         : OSIX_SUCCESS on Success
                       OSIX_FAILURE on Failure                                *
 *****************************************************************************/
INT4
WssIfWtpIpRouteConfigGetNextEntry (UINT4 *pu4WtpProfileId,
                                   UINT4 *pu4NextWtpProfileId,
                                   UINT4 *pu4FsWtpIpRouteConfigSubnet,
                                   UINT4 *pu4NextFsWtpIpRouteConfigSubnet,
                                   UINT4 *pu4FsWtpIpRouteConfigNetMask,
                                   UINT4 *pu4NextFsWtpIpRouteConfigNetMask,
                                   UINT4 *pu4FsWtpIpRouteConfigGateway,
                                   UINT4 *pu4NextFsWtpIpRouteConfigGateway)
{
    tWssIfWtpIpRouteConfigDB WssIfWtpIpRouteConfigEntry;
    tWssIfWtpIpRouteConfigDB *pWssIfWtpIpRouteConfigEntry =
        &WssIfWtpIpRouteConfigEntry;
    tWssIfWtpIpRouteConfigDB *pWssIfNextWtpIpRouteConfigEntry = NULL;

    MEMSET (&WssIfWtpIpRouteConfigEntry, 0, sizeof (tWssIfWtpIpRouteConfigDB));

    pWssIfWtpIpRouteConfigEntry->u4WtpProfileId = *pu4WtpProfileId;
    pWssIfWtpIpRouteConfigEntry->u4Subnet = *pu4FsWtpIpRouteConfigSubnet;
    pWssIfWtpIpRouteConfigEntry->u4Mask = *pu4FsWtpIpRouteConfigNetMask;
    pWssIfWtpIpRouteConfigEntry->u4Gateway = *pu4FsWtpIpRouteConfigGateway;
    pWssIfNextWtpIpRouteConfigEntry = (tWssIfWtpIpRouteConfigDB *) RBTreeGetNext
        (gCapWtpIpRouteConfigTable, (tRBElem *)
         pWssIfWtpIpRouteConfigEntry, NULL);
    if (pWssIfNextWtpIpRouteConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    *pu4NextWtpProfileId = pWssIfNextWtpIpRouteConfigEntry->u4WtpProfileId;
    *pu4NextFsWtpIpRouteConfigSubnet =
        pWssIfNextWtpIpRouteConfigEntry->u4Subnet;
    *pu4NextFsWtpIpRouteConfigNetMask = pWssIfNextWtpIpRouteConfigEntry->u4Mask;
    *pu4NextFsWtpIpRouteConfigGateway =
        pWssIfNextWtpIpRouteConfigEntry->u4Gateway;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * *  Function Name   : WssIfDhcpGlobalGetEntry                                *
 * *  Description     : This function used to get a entry from global DHCP     *
 *                      table RB tree                                          *
 * *  Input(s)        : None                                                   *
 * *  Output(s)       : None                                                   *
 * *  Returns         : OSIX_FAILURE on failure                                *
 *                 OSIX_SUCCESS on success                                *
 ******************************************************************************/

INT4
WssIfDhcpGlobalGetEntry (tWssIfDhcpPoolDB * pDhcpPoolDB)
{
    tWssIfDhcpPoolDB   *pWssIfDhcpPoolDB = NULL;

    pWssIfDhcpPoolDB =
        (tWssIfDhcpPoolDB *) RBTreeGet (gCapDhcpGlobalTable, pDhcpPoolDB);

    if (pWssIfDhcpPoolDB == NULL)
    {
        return OSIX_FAILURE;
    }

    pDhcpPoolDB->u4DhcpPoolId = pWssIfDhcpPoolDB->u4DhcpPoolId;
    pDhcpPoolDB->u4Subnet = pWssIfDhcpPoolDB->u4Subnet;
    pDhcpPoolDB->u4Mask = pWssIfDhcpPoolDB->u4Mask;
    pDhcpPoolDB->u4StartIp = pWssIfDhcpPoolDB->u4StartIp;
    pDhcpPoolDB->u4EndIp = pWssIfDhcpPoolDB->u4EndIp;
    pDhcpPoolDB->u4LeaseExprTime = pWssIfDhcpPoolDB->u4LeaseExprTime;
    pDhcpPoolDB->u4DefaultIp = pWssIfDhcpPoolDB->u4DefaultIp;
    pDhcpPoolDB->u4DnsServerIp = pWssIfDhcpPoolDB->u4DnsServerIp;
    pDhcpPoolDB->i4RowStatus = pWssIfDhcpPoolDB->i4RowStatus;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * *  Function Name   : WssIfWtpLRConfigGetEntry                               *
 * *  Description     : This function used to get a entry from Wtp Local
 *                      Routing Config table RB tree                           *
 * *  Input(s)        : pointer to struct type tWssIfWtpLRConfigDB             *
 * *  Output(s)       : None                                                   *
 * *  Returns         : OSIX_FAILURE on failure                                *
 *                 OSIX_SUCCESS on success                                *
 ******************************************************************************/

INT4
WssIfWtpLRConfigGetEntry (tWssIfWtpLRConfigDB * pWtpLRConfig)
{
    tWssIfWtpLRConfigDB *pWssIfWtpLRConfigEntry = NULL;

    pWssIfWtpLRConfigEntry = (tWssIfWtpLRConfigDB *) RBTreeGet
        (gCapWtpLRConfigTable, pWtpLRConfig);

    if (pWssIfWtpLRConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    pWtpLRConfig->u4WtpProfileId = pWssIfWtpLRConfigEntry->u4WtpProfileId;
    pWtpLRConfig->i4WtpDhcpServerStatus =
        pWssIfWtpLRConfigEntry->i4WtpDhcpServerStatus;
    pWtpLRConfig->i4WtpDhcpRelayStatus =
        pWssIfWtpLRConfigEntry->i4WtpDhcpRelayStatus;
    pWtpLRConfig->i4WtpFirewallStatus =
        pWssIfWtpLRConfigEntry->i4WtpFirewallStatus;
    pWtpLRConfig->i4RowStatus = pWssIfWtpLRConfigEntry->i4RowStatus;

    return OSIX_SUCCESS;
}

INT4
WssIfWtpDhcpConfigGetEntry (tWssIfWtpDhcpConfigDB * pWtpDhcpConfig)
{
    tWssIfWtpDhcpConfigDB *pWssIfWtpDhcpConfigEntry = NULL;

    pWssIfWtpDhcpConfigEntry = (tWssIfWtpDhcpConfigDB *) RBTreeGet
        (gCapWtpDhcpConfigTable, pWtpDhcpConfig);

    if (pWssIfWtpDhcpConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    pWtpDhcpConfig->u4WtpProfileId = pWssIfWtpDhcpConfigEntry->u4WtpProfileId;
    pWtpDhcpConfig->i4WtpDhcpServerStatus =
        pWssIfWtpDhcpConfigEntry->i4WtpDhcpServerStatus;
    pWtpDhcpConfig->i4WtpDhcpRelayStatus =
        pWssIfWtpDhcpConfigEntry->i4WtpDhcpRelayStatus;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * *  Function Name   : WssIfWtpIpRouteConfigGetEntry                          *
 * *  Description     : This function used to get a entry from Wtp Local
 *                      Routing Ip Route Config table RB tree                  *
 * *  Input(s)        : pointer to struct type tWssIfWtpIpRouteConfigDB        *
 * *  Output(s)       : None                                                   *
 * *  Returns         : OSIX_FAILURE on failure                                *
 *                 OSIX_SUCCESS on success                                *
 ******************************************************************************/

INT4
WssIfWtpIpRouteConfigGetEntry (tWssIfWtpIpRouteConfigDB * pWtpIpRouteConfig)
{
    tWssIfWtpIpRouteConfigDB *pWssIfWtpIpRouteConfigEntry = NULL;

    pWssIfWtpIpRouteConfigEntry = (tWssIfWtpIpRouteConfigDB *) RBTreeGet
        (gCapWtpIpRouteConfigTable, pWtpIpRouteConfig);

    if (pWssIfWtpIpRouteConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    pWtpIpRouteConfig->u4WtpProfileId =
        pWssIfWtpIpRouteConfigEntry->u4WtpProfileId;
    pWtpIpRouteConfig->u4Subnet = pWssIfWtpIpRouteConfigEntry->u4Subnet;
    pWtpIpRouteConfig->u4Mask = pWssIfWtpIpRouteConfigEntry->u4Mask;
    pWtpIpRouteConfig->u4Gateway = pWssIfWtpIpRouteConfigEntry->u4Gateway;
    pWtpIpRouteConfig->i4RowStatus = pWssIfWtpIpRouteConfigEntry->i4RowStatus;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpFwlFilterDeleteEntry                           *
 *  Description     : This function used to delete a  entry from the wtp     *
                      firewall filter table                                     *
 *                                                                           *
 *  Input(s)        : pointer to node of type tWssIfWtpFwlFilterDB           *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/

INT4
WssIfWtpFwlFilterDeleteEntry (tWssIfWtpFwlFilterDB * pWtpFwlFilter)
{
    tWssIfWtpFwlFilterDB *pWssIfWtpFwlFilter = NULL;

    pWssIfWtpFwlFilter = (tWssIfWtpFwlFilterDB *)
        RBTreeGet (gCapWtpFwlFilterTable, pWtpFwlFilter);
    if (pWssIfWtpFwlFilter == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpFwlFilterDeleteEntry: Failed to delete entry to "
                   "the Table\r\n");
        return OSIX_FAILURE;
    }
    RBTreeRem (gCapWtpFwlFilterTable, pWssIfWtpFwlFilter);
    MemReleaseMemBlock (CAPWAP_WTP_FWL_FILTER_DB_POOLID,
                        (UINT1 *) pWssIfWtpFwlFilter);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * *  Function Name   : WssIfNATConfigGetEntry                                *
 * *  Description     : This function used to get a entry from the NAT Config
 *             table                                                  *
 * *  Input(s)        : pointer to a struct node of type 
 *             "tWssIfNATConfigEntryDB"*
 * *  Output(s)       : None                                                   *
 * *  Returns         : OSIX_FAILURE on failure                                *
 *                 OSIX_SUCCESS on success                                *
 ******************************************************************************/
INT4
WssIfNATConfigGetEntry (tWssIfNATConfigEntryDB * pConfigEntry)
{
    tWssIfNATConfigEntryDB *pWssIfNATConfigEntry = NULL;

    pWssIfNATConfigEntry = (tWssIfNATConfigEntryDB *)
        RBTreeGet (gCapNATConfigTable, pConfigEntry);

    if (pWssIfNATConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    pConfigEntry->i4NATConfigEntryIndex =
        pWssIfNATConfigEntry->i4NATConfigEntryIndex;
    pConfigEntry->u4WtpProfileId = pWssIfNATConfigEntry->u4WtpProfileId;
    pConfigEntry->i1WanType = pWssIfNATConfigEntry->i1WanType;
    pConfigEntry->i4RowStatus = pWssIfNATConfigEntry->i4RowStatus;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * *  Function Name   : WssIfWtpDhcpPoolGetEntry                              *
 * *  Description     : This function used to get a entry from WTP DHCP Pool  *
 *                      table RB tree                                         *
 * *  Input(s)        : pointer to a node of struct tWssIfWtpDhcpPoolDB       *
 * *  Output(s)       : None                                                  *
 * *  Returns         : OSIX_FAILURE on failure                               *
 *                 OSIX_SUCCESS on success                               *
 ******************************************************************************/
INT4
WssIfWtpDhcpPoolGetEntry (tWssIfWtpDhcpPoolDB * pWtpDhcpPoolDB)
{
    tWssIfWtpDhcpPoolDB *pWssIfWtpDhcpPoolDB = NULL;

    pWssIfWtpDhcpPoolDB =
        (tWssIfWtpDhcpPoolDB *) RBTreeGet (gCapWtpDhcpTable, pWtpDhcpPoolDB);

    if (pWssIfWtpDhcpPoolDB == NULL)
    {
        return OSIX_FAILURE;
    }

    pWtpDhcpPoolDB->u4Subnet = pWssIfWtpDhcpPoolDB->u4Subnet;
    pWtpDhcpPoolDB->u4Mask = pWssIfWtpDhcpPoolDB->u4Mask;
    pWtpDhcpPoolDB->u4StartIp = pWssIfWtpDhcpPoolDB->u4StartIp;
    pWtpDhcpPoolDB->u4EndIp = pWssIfWtpDhcpPoolDB->u4EndIp;
    pWtpDhcpPoolDB->u4LeaseExprTime = pWssIfWtpDhcpPoolDB->u4LeaseExprTime;
    pWtpDhcpPoolDB->u4DefaultIp = pWssIfWtpDhcpPoolDB->u4DefaultIp;
    pWtpDhcpPoolDB->u4DnsServerIp = pWssIfWtpDhcpPoolDB->u4DnsServerIp;
    pWtpDhcpPoolDB->i4RowStatus = pWssIfWtpDhcpPoolDB->i4RowStatus;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfDhcpGlobalSetEntry                                *
 **  Description     : This function used to set an  entry in the DHCP global *
 *                     table RB tree                                          *
 **  Input(s)        : pointer to a node of type tWssIfDhcpPoolDB             *
 **  Output(s)       : None                                                   *
 **  Returns         : OSIX_FAILURE on failure                                *
 *                     OSIX_SUCCESS on success                                *
 ******************************************************************************/

INT4
WssIfDhcpGlobalSetEntry (tWssIfDhcpPoolDB * pDhcpPoolDB)
{
    tWssIfDhcpPoolDB   *pWssIfDhcpPoolDB = NULL;

    pWssIfDhcpPoolDB =
        (tWssIfDhcpPoolDB *) RBTreeGet (gCapDhcpGlobalTable, pDhcpPoolDB);

    if (pWssIfDhcpPoolDB == NULL)
    {
        return OSIX_FAILURE;
    }
    pWssIfDhcpPoolDB->u4Subnet = pDhcpPoolDB->u4Subnet;
    pWssIfDhcpPoolDB->u4Mask = pDhcpPoolDB->u4Mask;
    pWssIfDhcpPoolDB->u4StartIp = pDhcpPoolDB->u4StartIp;
    pWssIfDhcpPoolDB->u4EndIp = pDhcpPoolDB->u4EndIp;
    pWssIfDhcpPoolDB->u4LeaseExprTime = pDhcpPoolDB->u4LeaseExprTime;
    pWssIfDhcpPoolDB->u4DefaultIp = pDhcpPoolDB->u4DefaultIp;
    pWssIfDhcpPoolDB->u4DnsServerIp = pDhcpPoolDB->u4DnsServerIp;
    pWssIfDhcpPoolDB->i4RowStatus = pDhcpPoolDB->i4RowStatus;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfWtpLRConfigSetEntry                               *
 **  Description     : This function used to set an  entry in the WTP Local
                       Routing Config table RB tree                           *
 **  Input(s)        : pointer to a node of type tWssIfWtpLRConfigDB          *
 **  Output(s)       : None                                                   *
 **  Returns         : OSIX_FAILURE on failure                                *
 *                     OSIX_SUCCESS on success                                *
 ******************************************************************************/

INT4
WssIfWtpLRConfigSetEntry (tWssIfWtpLRConfigDB * pWtpLRConfig)
{
    tWssIfWtpLRConfigDB *pWssIfWtpLRConfigEntry = NULL;

    pWssIfWtpLRConfigEntry = (tWssIfWtpLRConfigDB *) RBTreeGet
        (gCapWtpLRConfigTable, pWtpLRConfig);

    if (pWssIfWtpLRConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWssIfWtpLRConfigEntry->u4WtpProfileId = pWtpLRConfig->u4WtpProfileId;
    pWssIfWtpLRConfigEntry->i4WtpDhcpServerStatus =
        pWtpLRConfig->i4WtpDhcpServerStatus;
    pWssIfWtpLRConfigEntry->i4WtpDhcpRelayStatus =
        pWtpLRConfig->i4WtpDhcpRelayStatus;
    pWssIfWtpLRConfigEntry->i4WtpFirewallStatus =
        pWtpLRConfig->i4WtpFirewallStatus;
    pWssIfWtpLRConfigEntry->i4RowStatus = pWtpLRConfig->i4RowStatus;

    return OSIX_SUCCESS;
}

INT4
WssIfWtpDhcpConfigSetEntry (tWssIfWtpDhcpConfigDB * pWtpDhcpConfig)
{
    tWssIfWtpDhcpConfigDB *pWssIfWtpDhcpConfigEntry = NULL;

    pWssIfWtpDhcpConfigEntry = (tWssIfWtpDhcpConfigDB *) RBTreeGet
        (gCapWtpDhcpConfigTable, pWtpDhcpConfig);

    if (pWssIfWtpDhcpConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWssIfWtpDhcpConfigEntry->u4WtpProfileId = pWtpDhcpConfig->u4WtpProfileId;
    pWssIfWtpDhcpConfigEntry->i4WtpDhcpServerStatus =
        pWtpDhcpConfig->i4WtpDhcpServerStatus;
    pWssIfWtpDhcpConfigEntry->i4WtpDhcpRelayStatus =
        pWtpDhcpConfig->i4WtpDhcpRelayStatus;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfWtpIpRouteConfigSetEntry                          *
 **  Description     : This function used to set an  entry in the WTP Local
                       Routing Ip Route Config table RB tree                  *
 **  Input(s)        : pointer to a node of type tWssIfWtpIpRouteConfigDB   *
 **  Output(s)       : None                                                   *
 **  Returns         : OSIX_FAILURE on failure                                *
 *                     OSIX_SUCCESS on success                                *
 ******************************************************************************/

INT4
WssIfWtpIpRouteConfigSetEntry (tWssIfWtpIpRouteConfigDB * pWtpIpRouteConfig)
{
    tWssIfWtpIpRouteConfigDB *pWssIfWtpIpRouteConfigEntry = NULL;

    pWssIfWtpIpRouteConfigEntry = (tWssIfWtpIpRouteConfigDB *) RBTreeGet
        (gCapWtpIpRouteConfigTable, pWtpIpRouteConfig);

    if (pWssIfWtpIpRouteConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWssIfWtpIpRouteConfigEntry->u4WtpProfileId =
        pWtpIpRouteConfig->u4WtpProfileId;
    pWssIfWtpIpRouteConfigEntry->u4Subnet = pWtpIpRouteConfig->u4Subnet;
    pWssIfWtpIpRouteConfigEntry->u4Mask = pWtpIpRouteConfig->u4Mask;
    pWssIfWtpIpRouteConfigEntry->u4Gateway = pWtpIpRouteConfig->u4Gateway;
    pWssIfWtpIpRouteConfigEntry->i4RowStatus = pWtpIpRouteConfig->i4RowStatus;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfNATConfigSetEntry                                *
 **  Description     : This function used to set the contents of the  entry
               in the NAT Config table RB tree                       *
 **  Input(s)        : pointer to node of type tWssIfNATConfigEntryDB.       *
 **  Output(s)       : None                                                  *
 **  Returns         : OSIX_FAILURE on failure                               *
 *                     OSIX_SUCCESS on success                               *
 ******************************************************************************/
INT4
WssIfNATConfigSetEntry (tWssIfNATConfigEntryDB * pConfigEntry)
{
    tWssIfNATConfigEntryDB *pWssIfNATConfigEntry = NULL;

    pWssIfNATConfigEntry = (tWssIfNATConfigEntryDB *)
        RBTreeGet (gCapNATConfigTable, pConfigEntry);

    if (pWssIfNATConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWssIfNATConfigEntry->i4NATConfigEntryIndex =
        pConfigEntry->i4NATConfigEntryIndex;
    pWssIfNATConfigEntry->u4WtpProfileId = pConfigEntry->u4WtpProfileId;
    pWssIfNATConfigEntry->i1WanType = pConfigEntry->i1WanType;
    pWssIfNATConfigEntry->i4RowStatus = pConfigEntry->i4RowStatus;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpFwlRuleDeleteEntry                             *
 *  Description     : This function used to delete a  entry from the wtp     *
                      firewall rule table                                     *
 *                                                                           *
 *  Input(s)        : pointer to node of type tWssIfWtpFwlRuleDB             *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/

INT4
WssIfWtpFwlRuleDeleteEntry (tWssIfWtpFwlRuleDB * pWtpFwlRule)
{
    tWssIfWtpFwlRuleDB *pWssIfWtpFwlRule = NULL;

    pWssIfWtpFwlRule = (tWssIfWtpFwlRuleDB *)
        RBTreeGet (gCapWtpFwlRuleTable, pWtpFwlRule);
    if (pWssIfWtpFwlRule == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpFwlRuleDeleteEntry: Failed to delete entry to "
                   "the Table\r\n");
        return OSIX_FAILURE;
    }
    RBTreeRem (gCapWtpFwlRuleTable, pWssIfWtpFwlRule);
    MemReleaseMemBlock (CAPWAP_WTP_FWL_RULE_DB_POOLID,
                        (UINT1 *) pWssIfWtpFwlRule);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpL3SubIfDeleteEntry                             *
 *  Description     : This function used to delete a  entry from the wtp     *
                      L3 SUB Iface  table                                     *
 *                                                                           *
 *  Input(s)        : pointer to node of type tWssIfWtpL3SubIfDB             *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpL3SubIfDeleteEntry (tWssIfWtpL3SubIfDB * pWtpL3SubIfEntry)
{
    tWssIfWtpL3SubIfDB *pWssIfWtpL3SubIfEntry = NULL;

    pWssIfWtpL3SubIfEntry = (tWssIfWtpL3SubIfDB *)
        RBTreeGet (gCapWtpL3SubIfTable, pWtpL3SubIfEntry);
    if (pWssIfWtpL3SubIfEntry == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfWtpL3SubIfDeleteEntry: Failed to delete entry to "
                   "the Table\r\n");
        return OSIX_FAILURE;

    }
    RBTreeRem (gCapWtpL3SubIfTable, pWssIfWtpL3SubIfEntry);
    MemReleaseMemBlock (CAPWAP_L3_SUB_IF_DB_POOLID,
                        (UINT1 *) pWssIfWtpL3SubIfEntry);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfWtpDhcpPoolSetEntry                               *
 **  Description     : This function used to set a entry in the wtp Dhcp pool *
 *                     table RB tree                                          *
 **  Input(s)        : pointer to the node of type tWssIfWtpDhcpPoolDB        *
 **  Output(s)       : None                                                   *
 **  Returns         : OSIX_FAILURE on failure                                *
 *                     OSIX_SUCCESS on success                                *
 ******************************************************************************/
INT4
WssIfWtpDhcpPoolSetEntry (tWssIfWtpDhcpPoolDB * pWtpDhcpPoolDB)
{
    tWssIfWtpDhcpPoolDB *pWssIfWtpDhcpPoolDB = NULL;

    pWssIfWtpDhcpPoolDB =
        (tWssIfWtpDhcpPoolDB *) RBTreeGet (gCapWtpDhcpTable, pWtpDhcpPoolDB);

    if (pWssIfWtpDhcpPoolDB == NULL)
    {
        return OSIX_FAILURE;
    }
    pWssIfWtpDhcpPoolDB->u4Subnet = pWtpDhcpPoolDB->u4Subnet;
    pWssIfWtpDhcpPoolDB->u4Mask = pWtpDhcpPoolDB->u4Mask;
    pWssIfWtpDhcpPoolDB->u4StartIp = pWtpDhcpPoolDB->u4StartIp;
    pWssIfWtpDhcpPoolDB->u4EndIp = pWtpDhcpPoolDB->u4EndIp;
    pWssIfWtpDhcpPoolDB->u4LeaseExprTime = pWtpDhcpPoolDB->u4LeaseExprTime;
    pWssIfWtpDhcpPoolDB->u4DefaultIp = pWtpDhcpPoolDB->u4DefaultIp;
    pWssIfWtpDhcpPoolDB->u4DnsServerIp = pWtpDhcpPoolDB->u4DnsServerIp;
    pWssIfWtpDhcpPoolDB->i4RowStatus = pWtpDhcpPoolDB->i4RowStatus;
    pWssIfWtpDhcpPoolDB->u4WtpProfileId = pWtpDhcpPoolDB->u4WtpProfileId;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfDhcpServerStatusGet                               *
 **  Description     : This function used to get the Dhcp server status       *
 **  Input(s)        : None                                                   *
 **  Output(s)       : None                                                   *
 **  Returns         : gDhcpServerStatus                                      *
 ******************************************************************************/
INT4
WssIfDhcpServerStatusGet ()
{
    return gDhcpServerStatus;
}

/*****************************************************************************
 **  Function Name   : WssIfDhcpServerStatusSet                               *
 **  Description     : This function used to set the Dhcp server status       *
 **  Input(s)        : i4DhcpServerStatus - status to be set                  *
 **  Output(s)       : None                                                   *
 **  Returns         : OSIX_SUCCESS                                           *
 ******************************************************************************/
INT4
WssIfDhcpServerStatusSet (INT4 i4DhcpServerStatus)
{
    gDhcpServerStatus = i4DhcpServerStatus;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfWtpNATStatusGet                                      *
 **  Description     : This function used to get the Wtp NAT status           *
 **  Input(s)        : None                                                   *
 **  Output(s)       : None                                                   *
 **  Returns         : gWtpNATStatus                                          *
 ******************************************************************************/
INT4
WssIfWtpNATStatusGet ()
{
    return gWtpNATStatus;
}

/*****************************************************************************
 **  Function Name   : WssIfWtpNATStatusSet                                   *
 **  Description     : This function used to set the WTP NAT status           *
 **  Input(s)        : i4WtpNATStatus - status to be set                      *
 **  Output(s)       : None                                                   *
 **  Returns         : OSIX_SUCCESS                                           *
 ******************************************************************************/
INT4
WssIfWtpNATStatusSet (INT4 i4WtpNATStatus)
{
    gWtpNATStatus = i4WtpNATStatus;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfDhcpRelayStatusGet                                *
 **  Description     : This function used to get the Dhcp relay status        *
 **  Input(s)        : None                                                   *
 **  Output(s)       : None                                                   *
 **  Returns         : gDhcpRelayStatus                                       *
 ******************************************************************************/
INT4
WssIfDhcpRelayStatusGet ()
{
    return gDhcpRelayStatus;
}

/*****************************************************************************
 **  Function Name   : WssIfDhcpRelayStatusSet                                *
 **  Description     : This function used to set the Dhcp server status       *
 **  Input(s)        : i4DhcpRelayStatus - status to be set                   *
 **  Output(s)       : None                                                   *
 **  Returns         : OSIX_SUCCESS                                           *
 ******************************************************************************/
INT4
WssIfDhcpRelayStatusSet (INT4 i4DhcpRelayStatus)
{
    gDhcpRelayStatus = i4DhcpRelayStatus;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfDhcpNextSrvIpAddrGet                              *
 **  Description     : This function used to get the next Dhcp server address *
 **  Input(s)        : None                                                   *
 **  Output(s)       : None                                                   *
 **  Returns         : gNextDhcpSrvAddress                                    *
 ******************************************************************************/
INT4
WssIfDhcpNextSrvIpAddrGet ()
{
    return gNextDhcpSrvAddress;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpFwlFilterGetEntry                              *
 *  Description     : This function used to get a entry from the Firewall
                      filter RB tree                                         *
 *                                                                           *
 *  Input(s)        : pointer to node of type tWssIfWtpFwlFilterDB           *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpFwlFilterGetEntry (tWssIfWtpFwlFilterDB * pConfigEntry)
{
    tWssIfWtpFwlFilterDB *pWssIfWtpFwlFilter = NULL;

    pWssIfWtpFwlFilter = (tWssIfWtpFwlFilterDB *) RBTreeGet
        (gCapWtpFwlFilterTable, pConfigEntry);

    if (pWssIfWtpFwlFilter == NULL)
    {
        return OSIX_FAILURE;
    }

    pConfigEntry->u4WtpProfileId = pWssIfWtpFwlFilter->u4WtpProfileId;
    MEMCPY (pConfigEntry->au1FilterName, pWssIfWtpFwlFilter->au1FilterName,
            STRLEN (pWssIfWtpFwlFilter->au1FilterName));
    MEMCPY (pConfigEntry->au1SrcPort, pWssIfWtpFwlFilter->au1SrcPort,
            STRLEN (pWssIfWtpFwlFilter->au1SrcPort));
    MEMCPY (pConfigEntry->au1DestPort, pWssIfWtpFwlFilter->au1DestPort,
            STRLEN (pWssIfWtpFwlFilter->au1DestPort));
    MEMCPY (pConfigEntry->au1SrcAddr, pWssIfWtpFwlFilter->au1SrcAddr,
            STRLEN (pWssIfWtpFwlFilter->au1SrcAddr));
    MEMCPY (pConfigEntry->au1DestAddr, pWssIfWtpFwlFilter->au1DestAddr,
            STRLEN (pWssIfWtpFwlFilter->au1DestAddr));
    pConfigEntry->u2AddrType = pWssIfWtpFwlFilter->u2AddrType;
    pConfigEntry->SrcStartAddr.v4Addr = pWssIfWtpFwlFilter->SrcStartAddr.v4Addr;
    pConfigEntry->SrcStartAddr.u4AddrType =
        pWssIfWtpFwlFilter->SrcStartAddr.u4AddrType;
    pConfigEntry->SrcEndAddr.v4Addr = pWssIfWtpFwlFilter->SrcEndAddr.v4Addr;
    pConfigEntry->SrcEndAddr.u4AddrType =
        pWssIfWtpFwlFilter->SrcEndAddr.u4AddrType;
    pConfigEntry->DestStartAddr.v4Addr =
        pWssIfWtpFwlFilter->DestStartAddr.v4Addr;
    pConfigEntry->DestStartAddr.u4AddrType =
        pWssIfWtpFwlFilter->DestStartAddr.u4AddrType;
    pConfigEntry->DestEndAddr.v4Addr = pWssIfWtpFwlFilter->DestEndAddr.v4Addr;
    pConfigEntry->DestEndAddr.u4AddrType =
        pWssIfWtpFwlFilter->DestEndAddr.u4AddrType;
    pConfigEntry->u1Proto = pWssIfWtpFwlFilter->u1Proto;
    pConfigEntry->u2SrcMaxPort = pWssIfWtpFwlFilter->u2SrcMaxPort;
    pConfigEntry->u2SrcMinPort = pWssIfWtpFwlFilter->u2SrcMinPort;
    pConfigEntry->u2DestMaxPort = pWssIfWtpFwlFilter->u2DestMaxPort;
    pConfigEntry->u2DestMinPort = pWssIfWtpFwlFilter->u2DestMinPort;
    pConfigEntry->u1TcpAck = pWssIfWtpFwlFilter->u1TcpAck;
    pConfigEntry->u1TcpRst = pWssIfWtpFwlFilter->u1TcpRst;
    pConfigEntry->u1Tos = pWssIfWtpFwlFilter->u1Tos;
    pConfigEntry->u1FilterRefCount = pWssIfWtpFwlFilter->u1FilterRefCount;
    pConfigEntry->u4FilterHitCount = pWssIfWtpFwlFilter->u4FilterHitCount;
    pConfigEntry->u1FilterAccounting = pWssIfWtpFwlFilter->u1FilterAccounting;
    pConfigEntry->i4RowStatus = pWssIfWtpFwlFilter->i4RowStatus;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpFwlRuleGetEntry                                *
 *  Description     : This function used to get a  entry from the firewall
 *                      rule table                                           *
 *                                                                           *
 *  Input(s)        : pointer to node of type tWssIfWtpFwlRuleDB             *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpFwlRuleGetEntry (tWssIfWtpFwlRuleDB * pConfigEntry)
{
    tWssIfWtpFwlRuleDB *pWssIfWtpFwlRule = NULL;
    UINT1               u1Index = AP_FWL_ZERO;

    pWssIfWtpFwlRule = (tWssIfWtpFwlRuleDB *) RBTreeGet
        (gCapWtpFwlRuleTable, pConfigEntry);

    if (pWssIfWtpFwlRule == NULL)
    {
        return OSIX_FAILURE;
    }

    pConfigEntry->u4WtpProfileId = pWssIfWtpFwlRule->u4WtpProfileId;
    MEMCPY (pConfigEntry->au1RuleName, pWssIfWtpFwlRule->au1RuleName,
            AP_FWL_MAX_RULE_NAME_LEN);
    MEMCPY (pConfigEntry->au1FilterSet, pWssIfWtpFwlRule->au1FilterSet,
            AP_FWL_MAX_FILTER_SET_LEN);
    for (u1Index = AP_FWL_ZERO; u1Index < AP_FWL_MAX_FILTERS_IN_RULE; u1Index++)
    {
        pConfigEntry->apFilterInfo[u1Index] =
            pWssIfWtpFwlRule->apFilterInfo[u1Index];
    }
    pConfigEntry->u2MinSrcPort = pWssIfWtpFwlRule->u2MinSrcPort;
    pConfigEntry->u2MaxSrcPort = pWssIfWtpFwlRule->u2MaxSrcPort;
    pConfigEntry->u2MinDestPort = pWssIfWtpFwlRule->u2MinDestPort;
    pConfigEntry->u2MaxDestPort = pWssIfWtpFwlRule->u2MaxDestPort;
    pConfigEntry->u1FilterConditionFlag =
        pWssIfWtpFwlRule->u1FilterConditionFlag;
    pConfigEntry->u1RuleRefCount = pWssIfWtpFwlRule->u1RuleRefCount;
    pConfigEntry->i4RowStatus = pWssIfWtpFwlRule->i4RowStatus;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpFwlAclGetEntry                                 *
 *  Description     : This function used to get an entry from the firewall
 *                      Acl table                                            *
 *                                                                           *
 *  Input(s)        : pointer to node of type tWssIfWtpFwlAclDB              *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpFwlAclGetEntry (tWssIfWtpFwlAclDB * pConfigEntry)
{
    tWssIfWtpFwlAclDB  *pWssIfWtpFwlAcl = NULL;

    pWssIfWtpFwlAcl = (tWssIfWtpFwlAclDB *) RBTreeGet
        (gCapWtpFwlAclTable, pConfigEntry);

    if (pWssIfWtpFwlAcl == NULL)
    {
        return OSIX_FAILURE;
    }

    pConfigEntry->u4WtpProfileId = pWssIfWtpFwlAcl->u4WtpProfileId;
    MEMCPY (pConfigEntry->au1AclName, pWssIfWtpFwlAcl->au1AclName,
            AP_FWL_MAX_ACL_NAME_LEN);
    pConfigEntry->i4AclIfIndex = pWssIfWtpFwlAcl->i4AclIfIndex;
    pConfigEntry->u1AclDirection = pWssIfWtpFwlAcl->u1AclDirection;
    pConfigEntry->u4StartTime = pWssIfWtpFwlAcl->u4StartTime;
    pConfigEntry->u4EndTime = pWssIfWtpFwlAcl->u4EndTime;
    pConfigEntry->u4Scheduled = pWssIfWtpFwlAcl->u4Scheduled;
    pConfigEntry->u2SeqNum = pWssIfWtpFwlAcl->u2SeqNum;
    pConfigEntry->u1Action = pWssIfWtpFwlAcl->u1Action;
    pConfigEntry->u1LogTrigger = pWssIfWtpFwlAcl->u1LogTrigger;
    pConfigEntry->u1FragAction = pWssIfWtpFwlAcl->u1FragAction;
    pConfigEntry->u1AclType = pWssIfWtpFwlAcl->u1AclType;
    pConfigEntry->i4RowStatus = pWssIfWtpFwlAcl->i4RowStatus;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpL3SubIfGetEntry                                *
 *  Description     : This function used to get a  entry from the L3 SUB IF
 *                      table                                                *
 *                                                                           *
 *  Input(s)        : pointer to node of type tWssIfWtpL3SubIfDB             *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpL3SubIfGetEntry (tWssIfWtpL3SubIfDB * pConfigEntry)
{
    tWssIfWtpL3SubIfDB *pWssIfWtpL3SubIfEntry = NULL;

    pWssIfWtpL3SubIfEntry = (tWssIfWtpL3SubIfDB *) RBTreeGet
        (gCapWtpL3SubIfTable, pConfigEntry);

    if (pWssIfWtpL3SubIfEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    pConfigEntry->u4WtpProfileId = pWssIfWtpL3SubIfEntry->u4WtpProfileId;
    pConfigEntry->u4IfIndex = pWssIfWtpL3SubIfEntry->u4IfIndex;
    pConfigEntry->u4PhyPort = pWssIfWtpL3SubIfEntry->u4PhyPort;
    pConfigEntry->u2VlanId = pWssIfWtpL3SubIfEntry->u2VlanId;
    pConfigEntry->i4IfType = pWssIfWtpL3SubIfEntry->i4IfType;
    pConfigEntry->u4IpAddr = pWssIfWtpL3SubIfEntry->u4IpAddr;
    pConfigEntry->u4SubnetMask = pWssIfWtpL3SubIfEntry->u4SubnetMask;
    pConfigEntry->u4BroadcastAddr = pWssIfWtpL3SubIfEntry->u4BroadcastAddr;
    pConfigEntry->i4IfNwType = pWssIfWtpL3SubIfEntry->i4IfNwType;
    pConfigEntry->u1IfAdminStatus = pWssIfWtpL3SubIfEntry->u1IfAdminStatus;
    pConfigEntry->i4RowStatus = pWssIfWtpL3SubIfEntry->i4RowStatus;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfDhcpNextSrvIpAddrSet                              *
 **  Description     : This function used to set the next Dhcp server address *
 **  Input(s)        : u4NextDhcpSrvAddr - address to be set                  *
 **  Output(s)       : None                                                   *
 **  Returns         : OSIX_SUCCESS                                           *
 ******************************************************************************/
INT4
WssIfDhcpNextSrvIpAddrSet (UINT4 u4NextDhcpSrvAddr)
{
    gNextDhcpSrvAddress = u4NextDhcpSrvAddr;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfWtpFirewallStatusGet                              *
 **  Description     : This function used to get the WTP Firewall status      *
 **  Input(s)        : None                                                   *
 **  Output(s)       : None                                                   *
 **  Returns         : gWtpFirewallStatus                                     *
 ******************************************************************************/
INT4
WssIfWtpFirewallStatusGet ()
{
    return gWtpFirewallStatus;
}

INT4
/*****************************************************************************
 **  Function Name   : WssIfWtpFirewallStatusSet                              *
 **  Description     : This function used to set the WTP firewall status      *
 **  Input(s)        : i4WtpFirewallStatus - firewall status to be set        *
 **  Output(s)       : None                                                   *
 **  Returns         : OSIX_SUCCESS                                           *
 ******************************************************************************/
WssIfWtpFirewallStatusSet (INT4 i4WtpFirewallStatus)
{
    gWtpFirewallStatus = i4WtpFirewallStatus;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfWlanDNSServerIpAddrGet                            *
 **  Description     : This function used to get the DNS Server Address       *
 **  Input(s)        : None                                                   *
 **  Output(s)       : None                                                   *
 **  Returns         : gWtpFirewallStatus                                     *
 ******************************************************************************/
INT4
WssIfWlanDNSServerIpAddrGet ()
{
    return gWlanDNSServerIpAddr;
}

INT4
/*****************************************************************************
 **  Function Name   : WssIfWlanDNSServerIpAddrSet                           *
 **  Description     : This function used to set the DNS Server Address      *
 **  Input(s)        : i4WlanDNSServerIpAddr - DNS Server Address to be set  *
 **  Output(s)       : None                                                  *
 **  Returns         : OSIX_SUCCESS                                          *
 ******************************************************************************/
WssIfWlanDNSServerIpAddrSet (UINT4 i4WlanDNSServerIpAddr)
{
    gWlanDNSServerIpAddr = i4WlanDNSServerIpAddr;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfWlanDefaultRouterIpAddrGet                        *
 **  Description     : This function used to get the Default Router Address   *
 **  Input(s)        : None                                                   *
 **  Output(s)       : None                                                   *
 **  Returns         : gWlanDefaultRouterIpAddr                               *
 ******************************************************************************/
INT4
WssIfWlanDefaultRouterIpAddrGet ()
{
    return gWlanDefaultRouterIpAddr;
}

INT4
/*****************************************************************************
 **  Function Name   : WssIfWlanDefaultRouterIpAddrSet                       *
 **  Description     : This function used to set the Default Router Address  *
 **  Input(s)        : i4WlanDefaultRouterIpAddr - Default Router Address to *
                       be set.                                               *
 **  Output(s)       : None                                                  *
 **  Returns         : OSIX_SUCCESS                                          *
 ******************************************************************************/
WssIfWlanDefaultRouterIpAddrSet (UINT4 i4WlanDefaultRouterIpAddr)
{
    gWlanDefaultRouterIpAddr = i4WlanDefaultRouterIpAddr;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpFwlFilterSetEntry                              *
 *  Description     : This function used to set a entry in the Firewall
                      filter RB tree                                         *
 *                                                                           *
 *  Input(s)        : pointer to node of type tWssIfWtpFwlFilterDB           *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpFwlFilterSetEntry (tWssIfWtpFwlFilterDB * pConfigEntry)
{
    tWssIfWtpFwlFilterDB *pWssIfWtpFwlFilter = NULL;

    pWssIfWtpFwlFilter = (tWssIfWtpFwlFilterDB *) RBTreeGet
        (gCapWtpFwlFilterTable, pConfigEntry);

    if (pWssIfWtpFwlFilter == NULL)
    {
        return OSIX_FAILURE;
    }

    pWssIfWtpFwlFilter->u4WtpProfileId = pConfigEntry->u4WtpProfileId;
    MEMCPY (pWssIfWtpFwlFilter->au1FilterName, pConfigEntry->au1FilterName,
            AP_FWL_MAX_FILTER_NAME_LEN);
    MEMCPY (pWssIfWtpFwlFilter->au1SrcAddr, pConfigEntry->au1SrcAddr,
            STRLEN (pConfigEntry->au1SrcAddr));
    MEMCPY (pWssIfWtpFwlFilter->au1DestAddr, pConfigEntry->au1DestAddr,
            STRLEN (pConfigEntry->au1DestAddr));
    MEMCPY (pWssIfWtpFwlFilter->au1SrcPort, pConfigEntry->au1SrcPort,
            STRLEN (pConfigEntry->au1SrcPort));
    MEMCPY (pWssIfWtpFwlFilter->au1DestPort, pConfigEntry->au1DestPort,
            STRLEN (pConfigEntry->au1DestPort));
    pWssIfWtpFwlFilter->u2AddrType = pConfigEntry->u2AddrType;
    pWssIfWtpFwlFilter->SrcStartAddr.v4Addr = pConfigEntry->SrcStartAddr.v4Addr;
    pWssIfWtpFwlFilter->SrcStartAddr.u4AddrType =
        pConfigEntry->SrcStartAddr.u4AddrType;
    pWssIfWtpFwlFilter->SrcEndAddr.v4Addr = pConfigEntry->SrcEndAddr.v4Addr;
    pWssIfWtpFwlFilter->SrcEndAddr.u4AddrType =
        pConfigEntry->SrcEndAddr.u4AddrType;
    pWssIfWtpFwlFilter->DestStartAddr.v4Addr =
        pConfigEntry->DestStartAddr.v4Addr;
    pWssIfWtpFwlFilter->DestStartAddr.u4AddrType =
        pConfigEntry->DestStartAddr.u4AddrType;
    pWssIfWtpFwlFilter->DestEndAddr.v4Addr = pConfigEntry->DestEndAddr.v4Addr;
    pWssIfWtpFwlFilter->DestEndAddr.u4AddrType =
        pConfigEntry->DestEndAddr.u4AddrType;
    pWssIfWtpFwlFilter->u1Proto = pConfigEntry->u1Proto;
    pWssIfWtpFwlFilter->u2SrcMaxPort = pConfigEntry->u2SrcMaxPort;
    pWssIfWtpFwlFilter->u2SrcMinPort = pConfigEntry->u2SrcMinPort;
    pWssIfWtpFwlFilter->u2DestMaxPort = pConfigEntry->u2DestMaxPort;
    pWssIfWtpFwlFilter->u2DestMinPort = pConfigEntry->u2DestMinPort;
    pWssIfWtpFwlFilter->u1TcpAck = pConfigEntry->u1TcpAck;
    pWssIfWtpFwlFilter->u1TcpRst = pConfigEntry->u1TcpRst;
    pWssIfWtpFwlFilter->u1Tos = pConfigEntry->u1Tos;
    pWssIfWtpFwlFilter->u1FilterRefCount = pConfigEntry->u1FilterRefCount;
    pWssIfWtpFwlFilter->u4FilterHitCount = pConfigEntry->u4FilterHitCount;
    pWssIfWtpFwlFilter->u1FilterAccounting = pConfigEntry->u1FilterAccounting;
    pWssIfWtpFwlFilter->i4RowStatus = pConfigEntry->i4RowStatus;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpFwlRuleSetEntry                                *
 *  Description     : This function used to set an entry in the firewall
 *                      rule table                                           *
 *                                                                           *
 *  Input(s)        : pointer to node of type tWssIfWtpFwlRuleDB             *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpFwlRuleSetEntry (tWssIfWtpFwlRuleDB * pConfigEntry)
{
    tWssIfWtpFwlRuleDB *pWssIfWtpFwlRule = NULL;
    UINT1               u1Index = AP_FWL_ZERO;
    pWssIfWtpFwlRule = (tWssIfWtpFwlRuleDB *) RBTreeGet
        (gCapWtpFwlRuleTable, pConfigEntry);

    if (pWssIfWtpFwlRule == NULL)
    {
        return OSIX_FAILURE;
    }

    pWssIfWtpFwlRule->u4WtpProfileId = pConfigEntry->u4WtpProfileId;
    MEMCPY (pWssIfWtpFwlRule->au1RuleName, pConfigEntry->au1RuleName,
            STRLEN (pConfigEntry->au1RuleName));
    MEMCPY (pWssIfWtpFwlRule->au1FilterSet, pConfigEntry->au1FilterSet,
            STRLEN (pConfigEntry->au1FilterSet));
    for (u1Index = AP_FWL_ZERO; u1Index < AP_FWL_MAX_FILTERS_IN_RULE; u1Index++)
    {
        pWssIfWtpFwlRule->apFilterInfo[u1Index] =
            pConfigEntry->apFilterInfo[u1Index];
    }
    pWssIfWtpFwlRule->u2MinSrcPort = pConfigEntry->u2MinSrcPort;
    pWssIfWtpFwlRule->u2MaxSrcPort = pConfigEntry->u2MaxSrcPort;
    pWssIfWtpFwlRule->u2MinDestPort = pConfigEntry->u2MinDestPort;
    pWssIfWtpFwlRule->u2MaxDestPort = pConfigEntry->u2MaxDestPort;
    pWssIfWtpFwlRule->u1FilterConditionFlag =
        pConfigEntry->u1FilterConditionFlag;
    pWssIfWtpFwlRule->u1RuleRefCount = pConfigEntry->u1RuleRefCount;
    pWssIfWtpFwlRule->i4RowStatus = pConfigEntry->i4RowStatus;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpFwlAclSetEntry                                 *
 *  Description     : This function used to set an entry to the firewall
 *                      Acl table                                            *
 *                                                                           *
 *  Input(s)        : pointer to node of type tWssIfWtpFwlAclDB              *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpFwlAclSetEntry (tWssIfWtpFwlAclDB * pConfigEntry)
{
    tWssIfWtpFwlAclDB  *pWssIfWtpFwlAcl = NULL;

    pWssIfWtpFwlAcl = (tWssIfWtpFwlAclDB *) RBTreeGet
        (gCapWtpFwlAclTable, pConfigEntry);

    if (pWssIfWtpFwlAcl == NULL)
    {
        return OSIX_FAILURE;
    }

    pWssIfWtpFwlAcl->u4WtpProfileId = pConfigEntry->u4WtpProfileId;
    MEMCPY (pWssIfWtpFwlAcl->au1AclName, pConfigEntry->au1AclName,
            STRLEN (pConfigEntry->au1AclName));
    pWssIfWtpFwlAcl->i4AclIfIndex = pConfigEntry->i4AclIfIndex;
    pWssIfWtpFwlAcl->u1AclDirection = pConfigEntry->u1AclDirection;
    pWssIfWtpFwlAcl->u4StartTime = pConfigEntry->u4StartTime;
    pWssIfWtpFwlAcl->u4EndTime = pConfigEntry->u4EndTime;
    pWssIfWtpFwlAcl->u4Scheduled = pConfigEntry->u4Scheduled;
    pWssIfWtpFwlAcl->u2SeqNum = pConfigEntry->u2SeqNum;
    pWssIfWtpFwlAcl->u1Action = pConfigEntry->u1Action;
    pWssIfWtpFwlAcl->u1LogTrigger = pConfigEntry->u1LogTrigger;
    pWssIfWtpFwlAcl->u1FragAction = pConfigEntry->u1FragAction;
    pWssIfWtpFwlAcl->u1AclType = pConfigEntry->u1AclType;
    pWssIfWtpFwlAcl->i4RowStatus = pConfigEntry->i4RowStatus;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpL3SubIfSetEntry                                *
 *  Description     : This function used to set an entry in the L3 SUB IF
 *                       table                                               *
 *                                                                           *
 *  Input(s)        : pointer to node of type tWssIfWtpL3SubIfDB             *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpL3SubIfSetEntry (tWssIfWtpL3SubIfDB * pConfigEntry)
{
    tWssIfWtpL3SubIfDB *pWssIfWtpL3SubIfEntry = NULL;
    pWssIfWtpL3SubIfEntry = (tWssIfWtpL3SubIfDB *) RBTreeGet
        (gCapWtpL3SubIfTable, pConfigEntry);

    if (pWssIfWtpL3SubIfEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    pWssIfWtpL3SubIfEntry->u4WtpProfileId = pConfigEntry->u4WtpProfileId;
    pWssIfWtpL3SubIfEntry->u4IfIndex = pConfigEntry->u4IfIndex;
    pWssIfWtpL3SubIfEntry->u4PhyPort = pConfigEntry->u4PhyPort;
    pWssIfWtpL3SubIfEntry->u2VlanId = pConfigEntry->u2VlanId;
    pWssIfWtpL3SubIfEntry->i4IfType = pConfigEntry->i4IfType;
    pWssIfWtpL3SubIfEntry->u4IpAddr = pConfigEntry->u4IpAddr;
    pWssIfWtpL3SubIfEntry->u4SubnetMask = pConfigEntry->u4SubnetMask;
    pWssIfWtpL3SubIfEntry->u4BroadcastAddr = pConfigEntry->u4BroadcastAddr;
    pWssIfWtpL3SubIfEntry->i4IfNwType = pConfigEntry->i4IfNwType;
    pWssIfWtpL3SubIfEntry->u1IfAdminStatus = pConfigEntry->u1IfAdminStatus;
    pWssIfWtpL3SubIfEntry->i4RowStatus = pConfigEntry->i4RowStatus;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfStartDhcpPoolUpdateReq                            *
 **  Description     : This function used to start the DHCP pool update
            request                                               *
 **  Input(s)        : u4WtpProfileId                                         *
 **  Output(s)       : None                                                   *
 **  Returns         : OSIX_FAILURE on failure
               OSIX_SUCCESS on success                                *
 ******************************************************************************/

INT4
WssifStartDhcpPoolUpdateReq (UINT4 u4WtpProfileId, UINT1 u1ServerStatus,
                             INT4 i4DhcpPoolId, UINT1 u1EntryStatus)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssIfWtpDhcpPoolDB *pWtpDhcpPoolDB = NULL;
    tWssIfWtpDhcpPoolDB *pWssIfWtpDhcpPoolDB = NULL;
    UINT2               u2PoolCount = 0;
    INT4                i4Retval = 0;
    tWssIfWtpDhcpPoolDB *pWssifWtpDhcpPoolDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    pWtpDhcpPoolDB =
        (tWssIfWtpDhcpPoolDB *) MemAllocMemBlk (CAPWAP_WTP_DHCP_DB_POOLID);
    if (pWtpDhcpPoolDB == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    MEMSET (pWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));

    pWssifWtpDhcpPoolDB =
        (tWssIfWtpDhcpPoolDB *) MemAllocMemBlk (CAPWAP_WTP_DHCP_DB_POOLID);
    if (pWssifWtpDhcpPoolDB == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_WTP_DHCP_DB_POOLID,
                            (UINT1 *) pWtpDhcpPoolDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWssifWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));

    pWtpDhcpPoolDB->u4WtpProfileId = u4WtpProfileId;
    pWtpDhcpPoolDB->u4DhcpPoolId = 0;

    pWssIfCapwapDB->CapwapGetDB.u2WtpProfileId = (UINT2) u4WtpProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_WTP_DHCP_DB_POOLID,
                            (UINT1 *) pWtpDhcpPoolDB);
        MemReleaseMemBlock (CAPWAP_WTP_DHCP_DB_POOLID,
                            (UINT1 *) pWssifWtpDhcpPoolDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting == LOCAL_ROUTING_DISABLE)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_WTP_DHCP_DB_POOLID,
                            (UINT1 *) pWtpDhcpPoolDB);
        MemReleaseMemBlock (CAPWAP_WTP_DHCP_DB_POOLID,
                            (UINT1 *) pWssifWtpDhcpPoolDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4WtpProfileId;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                 pWssIfCapwapDB) != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_WTP_DHCP_DB_POOLID,
                            (UINT1 *) pWtpDhcpPoolDB);
        MemReleaseMemBlock (CAPWAP_WTP_DHCP_DB_POOLID,
                            (UINT1 *) pWssifWtpDhcpPoolDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (i4DhcpPoolId == 0)
    {
        pWssIfWtpDhcpPoolDB = (tWssIfWtpDhcpPoolDB *) RBTreeGetNext
            (gCapWtpDhcpTable, (tRBElem *) pWtpDhcpPoolDB, NULL);

        if (pWssIfWtpDhcpPoolDB != NULL)
        {
            do
            {
                pWtpDhcpPoolDB->u4WtpProfileId =
                    pWssIfWtpDhcpPoolDB->u4WtpProfileId;
                pWtpDhcpPoolDB->u4DhcpPoolId =
                    pWssIfWtpDhcpPoolDB->u4DhcpPoolId;
                if (pWssIfWtpDhcpPoolDB->u4WtpProfileId != u4WtpProfileId)
                {
                    continue;
                }
                if (pWssIfWtpDhcpPoolDB->i4RowStatus == ACTIVE)
                {
                    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                        unVendorSpec.DhcpPoolConfigUpdateReq.
                        u4DhcpPoolId[u2PoolCount] =
                        pWssIfWtpDhcpPoolDB->u4DhcpPoolId;
                    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                        unVendorSpec.DhcpPoolConfigUpdateReq.
                        u4Subnet[u2PoolCount] = pWssIfWtpDhcpPoolDB->u4Subnet;
                    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                        unVendorSpec.DhcpPoolConfigUpdateReq.
                        u4Mask[u2PoolCount] = pWssIfWtpDhcpPoolDB->u4Mask;
                    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                        unVendorSpec.DhcpPoolConfigUpdateReq.
                        u4StartIp[u2PoolCount] = pWssIfWtpDhcpPoolDB->u4StartIp;
                    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                        unVendorSpec.DhcpPoolConfigUpdateReq.
                        u4EndIp[u2PoolCount] = pWssIfWtpDhcpPoolDB->u4EndIp;
                    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                        unVendorSpec.DhcpPoolConfigUpdateReq.
                        u4LeaseExprTime[u2PoolCount] =
                        pWssIfWtpDhcpPoolDB->u4LeaseExprTime;
                    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                        unVendorSpec.DhcpPoolConfigUpdateReq.
                        u4DnsServerIp[u2PoolCount] =
                        pWssIfWtpDhcpPoolDB->u4DnsServerIp;
                    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                        unVendorSpec.DhcpPoolConfigUpdateReq.
                        u4DefaultIp[u2PoolCount] =
                        pWssIfWtpDhcpPoolDB->u4DefaultIp;
                    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                        unVendorSpec.DhcpPoolConfigUpdateReq.
                        u2PoolStatus[u2PoolCount] = (UINT2) u1EntryStatus;
                    u2PoolCount = u2PoolCount + 1;
                    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                        isOptional = OSIX_TRUE;
                }
            }
            while ((pWssIfWtpDhcpPoolDB = (tWssIfWtpDhcpPoolDB *) RBTreeGetNext
                    (gCapWtpDhcpTable, (tRBElem *) pWtpDhcpPoolDB,
                     NULL)) != NULL);
        }
        else
        {
            u2PoolCount = 0;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional =
                OSIX_TRUE;
        }
    }
    else
    {
        pWssifWtpDhcpPoolDB->u4WtpProfileId = u4WtpProfileId;
        pWssifWtpDhcpPoolDB->u4DhcpPoolId = (UINT4) i4DhcpPoolId;

        if (WssIfWtpDhcpPoolGetEntry (pWssifWtpDhcpPoolDB) == OSIX_FAILURE)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            MemReleaseMemBlock (CAPWAP_WTP_DHCP_DB_POOLID,
                                (UINT1 *) pWtpDhcpPoolDB);
            MemReleaseMemBlock (CAPWAP_WTP_DHCP_DB_POOLID,
                                (UINT1 *) pWssifWtpDhcpPoolDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        if (pWssifWtpDhcpPoolDB->i4RowStatus == ACTIVE)
        {
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                DhcpPoolConfigUpdateReq.u4DhcpPoolId[u2PoolCount] =
                pWssifWtpDhcpPoolDB->u4DhcpPoolId;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                DhcpPoolConfigUpdateReq.u4Subnet[u2PoolCount] =
                pWssifWtpDhcpPoolDB->u4Subnet;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                DhcpPoolConfigUpdateReq.u4Mask[u2PoolCount] =
                pWssifWtpDhcpPoolDB->u4Mask;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                DhcpPoolConfigUpdateReq.u4StartIp[u2PoolCount] =
                pWssifWtpDhcpPoolDB->u4StartIp;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                DhcpPoolConfigUpdateReq.u4EndIp[u2PoolCount] =
                pWssifWtpDhcpPoolDB->u4EndIp;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                DhcpPoolConfigUpdateReq.u4LeaseExprTime[u2PoolCount] =
                pWssifWtpDhcpPoolDB->u4LeaseExprTime;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                DhcpPoolConfigUpdateReq.u4DnsServerIp[u2PoolCount] =
                pWssifWtpDhcpPoolDB->u4DnsServerIp;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                DhcpPoolConfigUpdateReq.u4DefaultIp[u2PoolCount] =
                pWssifWtpDhcpPoolDB->u4DefaultIp;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                DhcpPoolConfigUpdateReq.u2PoolStatus[u2PoolCount] =
                (UINT2) u1EntryStatus;
            u2PoolCount = u2PoolCount + 1;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional =
                OSIX_TRUE;
        }
    }
    if (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional ==
        OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleType =
            CAPWAP_VEND_SPECIFIC_PAYLOAD_MSG_TYPE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            DhcpPoolConfigUpdateReq.u2MsgEleLen =
            ((WTP_DHCP_VEND_FIXED_LEN * (u2PoolCount)) + 7);
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            DhcpPoolConfigUpdateReq.u2NoofPools = u2PoolCount;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            DhcpPoolConfigUpdateReq.u2MsgEleType = DHCP_POOL;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            DhcpPoolConfigUpdateReq.u4VendorId = VENDOR_ID;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            DhcpPoolConfigUpdateReq.u1ServerStatus = u1ServerStatus;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.elementId =
            VENDOR_DHCP_POOL;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            DhcpPoolConfigUpdateReq.b1IsOptional = OSIX_TRUE;

        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleLen =
            (UINT2) (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                     unVendorSpec.DhcpPoolConfigUpdateReq.u2MsgEleLen + 4);

        i4Retval = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                           pWlcHdlrMsgStruct);
        if (i4Retval == OSIX_FAILURE)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            MemReleaseMemBlock (CAPWAP_WTP_DHCP_DB_POOLID,
                                (UINT1 *) pWtpDhcpPoolDB);
            MemReleaseMemBlock (CAPWAP_WTP_DHCP_DB_POOLID,
                                (UINT1 *) pWssifWtpDhcpPoolDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_WTP_DHCP_DB_POOLID,
                            (UINT1 *) pWtpDhcpPoolDB);
        MemReleaseMemBlock (CAPWAP_WTP_DHCP_DB_POOLID,
                            (UINT1 *) pWssifWtpDhcpPoolDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return i4Retval;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    MemReleaseMemBlock (CAPWAP_WTP_DHCP_DB_POOLID, (UINT1 *) pWtpDhcpPoolDB);
    MemReleaseMemBlock (CAPWAP_WTP_DHCP_DB_POOLID,
                        (UINT1 *) pWssifWtpDhcpPoolDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfStartDhcpRelayUpdateReq                           *
 **  Description     : This function used to start the DHCP relay update
            request                                               *
 **  Input(s)        : u4NextIpAddress                                        *
 **  Output(s)       : None                                                   *
 **  Returns         : OSIX_FAILURE on failure
               OSIX_SUCCESS on success                                *
 ******************************************************************************/

INT4
WssifStartDhcpRelayUpdateReq (UINT4 u4ProfileId, UINT1 u1RelayStatus,
                              UINT4 u4NextIpAddress)
{
    INT4                i4RetVal = 0;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWssIfWtpDhcpConfigDB *pWssIfWtpDhcpConfigDB = NULL;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4NextProfileId = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    pWssIfWtpDhcpConfigDB = (tWssIfWtpDhcpConfigDB *)
        MemAllocMemBlk (CAPWAP_WTP_DHCP_CONFIG_DB_POOLID);
    if (pWssIfWtpDhcpConfigDB == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    MEMSET (pWssIfWtpDhcpConfigDB, 0, sizeof (tWssIfWtpDhcpConfigDB));

    if (u4ProfileId != 0)
    {
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpProfileId = (UINT2) u4ProfileId;
        pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4ProfileId;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                     pWssIfCapwapDB) != OSIX_SUCCESS)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            MemReleaseMemBlock (CAPWAP_WTP_DHCP_CONFIG_DB_POOLID,
                                (UINT1 *) pWssIfWtpDhcpConfigDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                     pWssIfCapwapDB) != OSIX_SUCCESS)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            MemReleaseMemBlock (CAPWAP_WTP_DHCP_CONFIG_DB_POOLID,
                                (UINT1 *) pWssIfWtpDhcpConfigDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        if (pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting ==
            LOCAL_ROUTING_DISABLE)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            MemReleaseMemBlock (CAPWAP_WTP_DHCP_CONFIG_DB_POOLID,
                                (UINT1 *) pWssIfWtpDhcpConfigDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }

        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            DhcpRelayConfigUpdateReq.u2MsgEleType = DHCP_RELAY;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            DhcpRelayConfigUpdateReq.u2MsgEleLen = 9;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            DhcpRelayConfigUpdateReq.u4VendorId = VENDOR_ID;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            DhcpRelayConfigUpdateReq.u1RelayStatus = u1RelayStatus;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            DhcpRelayConfigUpdateReq.u4NextIpAddress = u4NextIpAddress;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional =
            OSIX_TRUE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleType =
            CAPWAP_VEND_SPECIFIC_PAYLOAD_MSG_TYPE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.elementId =
            VENDOR_DHCP_RELAY;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            DhcpPoolConfigUpdateReq.b1IsOptional = 1;

        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleLen =
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            DhcpRelayConfigUpdateReq.u2MsgEleLen + 4;

        i4RetVal = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                           pWlcHdlrMsgStruct);
    }
    else
    {
        i4RetVal = WssIfWtpDhcpConfigGetFirstEntry (&u4NextProfileId);
        if (i4RetVal == OSIX_FAILURE)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            MemReleaseMemBlock (CAPWAP_WTP_DHCP_CONFIG_DB_POOLID,
                                (UINT1 *) pWssIfWtpDhcpConfigDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }
        do
        {
            u4WtpProfileId = u4NextProfileId;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpProfileId = (UINT2) u4WtpProfileId;
            pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4WtpProfileId;

            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                MemReleaseMemBlock (CAPWAP_WTP_DHCP_CONFIG_DB_POOLID,
                                    (UINT1 *) pWssIfWtpDhcpConfigDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB,
                                         pWssIfCapwapDB) != OSIX_SUCCESS)
            {
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                MemReleaseMemBlock (CAPWAP_WTP_DHCP_CONFIG_DB_POOLID,
                                    (UINT1 *) pWssIfWtpDhcpConfigDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
            if (pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting ==
                LOCAL_ROUTING_DISABLE)
            {
                i4RetVal = WssIfWtpDhcpConfigGetNextEntry (&u4WtpProfileId,
                                                           &u4NextProfileId);
                continue;
            }

            pWssIfWtpDhcpConfigDB->u4WtpProfileId = u4WtpProfileId;
            if (WssIfWtpDhcpConfigGetEntry (pWssIfWtpDhcpConfigDB) !=
                OSIX_SUCCESS)
            {
            }
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                DhcpRelayConfigUpdateReq.u2MsgEleType = DHCP_RELAY;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                DhcpRelayConfigUpdateReq.u2MsgEleLen = 9;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                DhcpRelayConfigUpdateReq.u4VendorId = VENDOR_ID;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                DhcpRelayConfigUpdateReq.u1RelayStatus =
                (UINT1) pWssIfWtpDhcpConfigDB->i4WtpDhcpRelayStatus;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                DhcpRelayConfigUpdateReq.u4NextIpAddress = u4NextIpAddress;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;

            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional =
                OSIX_TRUE;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleType =
                CAPWAP_VEND_SPECIFIC_PAYLOAD_MSG_TYPE;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.elementId =
                VENDOR_DHCP_RELAY;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                DhcpPoolConfigUpdateReq.b1IsOptional = 1;

            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleLen =
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                DhcpRelayConfigUpdateReq.u2MsgEleLen + 4;
            if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                        pWlcHdlrMsgStruct) != OSIX_SUCCESS)
            {
            }

            i4RetVal = WssIfWtpDhcpConfigGetNextEntry (&u4WtpProfileId,
                                                       &u4NextProfileId);

        }
        while (i4RetVal == OSIX_SUCCESS);

    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    MemReleaseMemBlock (CAPWAP_WTP_DHCP_CONFIG_DB_POOLID,
                        (UINT1 *) pWssIfWtpDhcpConfigDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssifFireWallUpdateReq                                *
 **  Description     : This function used to configure the firewall config in
            WTP                                                   *
 **  Input(s)        : u4WtpProfileIds                                        *
 **  Output(s)       : None                                                   *
 **  Returns         : OSIX_FAILURE on failure
               OSIX_SUCCESS on success                                *
 ******************************************************************************/
INT4
WssifFireWallUpdateReq (UINT4 u4WtpProfileId, UINT4 u4RowStatus)
{
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssIfWtpLRConfigDB *pWssIfWtpLRConfigEntry = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    INT4                i4Retval = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    if ((pWssIfWtpLRConfigEntry =
         (tWssIfWtpLRConfigDB *)
         MemAllocMemBlk (CAPWAP_WTP_LR_CONFIG_DB_POOLID)) == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    MEMSET (pWssIfWtpLRConfigEntry, 0, sizeof (tWssIfWtpLRConfigDB));

    pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4WtpProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_WTP_LR_CONFIG_DB_POOLID,
                            (UINT1 *) pWssIfWtpLRConfigEntry);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting == LOCAL_ROUTING_DISABLE)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_WTP_LR_CONFIG_DB_POOLID,
                            (UINT1 *) pWssIfWtpLRConfigEntry);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    pWssIfWtpLRConfigEntry->u4WtpProfileId = u4WtpProfileId;
    if (WssIfWtpLRConfigGetEntry (pWssIfWtpLRConfigEntry) != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_WTP_LR_CONFIG_DB_POOLID,
                            (UINT1 *) pWssIfWtpLRConfigEntry);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    if (pWssIfWtpLRConfigEntry->i4RowStatus != ACTIVE)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_WTP_LR_CONFIG_DB_POOLID,
                            (UINT1 *) pWssIfWtpLRConfigEntry);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4WtpProfileId;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                 pWssIfCapwapDB) != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_WTP_LR_CONFIG_DB_POOLID,
                            (UINT1 *) pWssIfWtpLRConfigEntry);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    if (u4RowStatus == ACTIVE)
    {
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            FireWallUpdateReq.u1FirewallStatus =
            (UINT1) (pWssIfWtpLRConfigEntry->i4WtpFirewallStatus);
    }
    if (u4RowStatus == DESTROY)
    {
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            FireWallUpdateReq.u1FirewallStatus = 0;
    }
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        FireWallUpdateReq.u2MsgEleType = FIREWALL_STATUS;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        FireWallUpdateReq.u2MsgEleLen = 5;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        FireWallUpdateReq.u4VendorId = VENDOR_ID;

    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional = OSIX_TRUE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleType =
        CAPWAP_VEND_SPECIFIC_PAYLOAD_MSG_TYPE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.elementId =
        VENDOR_FIREWALL_STATUS;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        FireWallUpdateReq.b1IsOptional = OSIX_TRUE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;

    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleLen =
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        FireWallUpdateReq.u2MsgEleLen + 4;

    i4Retval = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                       pWlcHdlrMsgStruct);
    if (i4Retval == OSIX_FAILURE)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_WTP_LR_CONFIG_DB_POOLID,
                            (UINT1 *) pWssIfWtpLRConfigEntry);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    MemReleaseMemBlock (CAPWAP_WTP_LR_CONFIG_DB_POOLID,
                        (UINT1 *) pWssIfWtpLRConfigEntry);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssifNatUpdateReq                                      *
 **  Description     : This function shall enable/disable NAT configuration in*
               WTP                                                    *
 **  Input(s)        : WTP Profile ID, Interface index                        *
 **  Output(s)       : None                                                   *
 **  Returns         : OSIX_FAILURE on failure
               OSIX_SUCCESS on success                                *
 ******************************************************************************/
INT4
WssifNatUpdateReq (UINT4 u4WtpProfileId, INT4 i4NatEntryIndex,
                   UINT4 u4RowStatus)
{
    INT4                i4Retval = 0;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssIfNATConfigEntryDB *pWssifNATConfigEntryDB = NULL;
    tWssIfNATConfigEntryDB *pWssifNATConfigEntry = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    pWssifNATConfigEntryDB =
        (tWssIfNATConfigEntryDB *)
        MemAllocMemBlk (CAPWAP_NAT_CONFIG_ENTRY_DB_POOLID);
    if (pWssifNATConfigEntryDB == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    MEMSET (pWssifNATConfigEntryDB, 0, sizeof (tWssIfNATConfigEntryDB));

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4WtpProfileId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_NAT_CONFIG_ENTRY_DB_POOLID,
                            (UINT1 *) pWssifNATConfigEntryDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting == LOCAL_ROUTING_DISABLE)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_NAT_CONFIG_ENTRY_DB_POOLID,
                            (UINT1 *) pWssifNATConfigEntryDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    if (i4NatEntryIndex == 0)
    {
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4WtpProfileId;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                     pWssIfCapwapDB) != OSIX_SUCCESS)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            MemReleaseMemBlock (CAPWAP_NAT_CONFIG_ENTRY_DB_POOLID,
                                (UINT1 *) pWssifNATConfigEntryDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        pWssifNATConfigEntryDB->u4WtpProfileId = u4WtpProfileId;

        pWssifNATConfigEntry = RBTreeGetNext (gCapNATConfigTable,
                                              pWssifNATConfigEntryDB, NULL);
        if (pWssifNATConfigEntry == NULL)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            MemReleaseMemBlock (CAPWAP_NAT_CONFIG_ENTRY_DB_POOLID,
                                (UINT1 *) pWssifNATConfigEntryDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }
        if (pWssifNATConfigEntry->i4RowStatus == ACTIVE)
        {
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                unVendorSpec.NatUpdateReq.u2MsgEleLen = 10;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                unVendorSpec.NatUpdateReq.u4NATConfigIndex =
                (UINT4) pWssifNATConfigEntry->i4NATConfigEntryIndex;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                unVendorSpec.NatUpdateReq.i2WanType =
                (INT2) pWssifNATConfigEntry->i1WanType;

            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional =
                OSIX_TRUE;
        }
    }
    else
    {
        pWssifNATConfigEntryDB->i4NATConfigEntryIndex = i4NatEntryIndex;
        pWssifNATConfigEntryDB->u4WtpProfileId = u4WtpProfileId;

        if (WssIfNATConfigGetEntry (pWssifNATConfigEntryDB) == OSIX_FAILURE)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            MemReleaseMemBlock (CAPWAP_NAT_CONFIG_ENTRY_DB_POOLID,
                                (UINT1 *) pWssifNATConfigEntryDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
            unVendorSpec.NatUpdateReq.u2MsgEleLen = 10;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
            unVendorSpec.NatUpdateReq.u4NATConfigIndex =
            pWssifNATConfigEntryDB->i4NATConfigEntryIndex;

        if (u4RowStatus == DESTROY)
        {
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                unVendorSpec.NatUpdateReq.i2WanType = 1;
        }
        else
        {
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                unVendorSpec.NatUpdateReq.i2WanType =
                (INT2) pWssifNATConfigEntryDB->i1WanType;
        }

        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional =
            OSIX_TRUE;
    }

    if (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional ==
        OSIX_TRUE)
    {
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
            unVendorSpec.NatUpdateReq.u2MsgEleType = NAT_STATUS;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
            unVendorSpec.NatUpdateReq.u4VendorId = VENDOR_ID;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleType =
            CAPWAP_VEND_SPECIFIC_PAYLOAD_MSG_TYPE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.elementId =
            VENDOR_NAT_STATUS;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
            unVendorSpec.NatUpdateReq.b1IsOptional = OSIX_TRUE;

        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleLen =
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            NatUpdateReq.u2MsgEleLen + 4;

        i4Retval = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                           pWlcHdlrMsgStruct);
        if (i4Retval == OSIX_FAILURE)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            MemReleaseMemBlock (CAPWAP_NAT_CONFIG_ENTRY_DB_POOLID,
                                (UINT1 *) pWssifNATConfigEntryDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    MemReleaseMemBlock (CAPWAP_NAT_CONFIG_ENTRY_DB_POOLID,
                        (UINT1 *) pWssifNATConfigEntryDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssifRouteTable                                        *
 **  Description     : This function shall configure/ckear the routes created *
               in WTP                                                 *
 **  Input(s)        :                                                        *
 **  Output(s)       : None                                                   *
 **  Returns         : OSIX_FAILURE on failure
               OSIX_SUCCESS on success                                *
 ******************************************************************************/
INT4
WssifRouteTable (UINT4 u4WtpProfileId, UINT4 u4Subnet,
                 UINT4 u4Mask, UINT4 u4Gateway, UINT4 u4RowStatus)
{
    INT4                i4Retval = 0;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssIfWtpIpRouteConfigDB *pWssIfWtpIpRouteConfigEntry = NULL;
    tWssIfWtpIpRouteConfigDB *pWssIfNextWtpIpRouteConfigEntry = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2Index = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    pWssIfWtpIpRouteConfigEntry =
        (tWssIfWtpIpRouteConfigDB *)
        MemAllocMemBlk (CAPWAP_WTP_IP_ROUTE_CONFIG_DB_POOLID);

    if (pWssIfWtpIpRouteConfigEntry == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    MEMSET (pWssIfWtpIpRouteConfigEntry, 0, sizeof (tWssIfWtpIpRouteConfigDB));

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4WtpProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_WTP_IP_ROUTE_CONFIG_DB_POOLID,
                            (UINT1 *) pWssIfWtpIpRouteConfigEntry);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting == LOCAL_ROUTING_DISABLE)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_WTP_IP_ROUTE_CONFIG_DB_POOLID,
                            (UINT1 *) pWssIfWtpIpRouteConfigEntry);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4WtpProfileId;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                 pWssIfCapwapDB) != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_WTP_IP_ROUTE_CONFIG_DB_POOLID,
                            (UINT1 *) pWssIfWtpIpRouteConfigEntry);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        RouteTableUpdateReq.u2MsgEleType = ROUTE_TABLE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        RouteTableUpdateReq.u4VendorId = VENDOR_ID;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional = OSIX_TRUE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleType =
        CAPWAP_VEND_SPECIFIC_PAYLOAD_MSG_TYPE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.elementId =
        VENDOR_ROUTE_TABLE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        RouteTableUpdateReq.b1IsOptional = OSIX_TRUE;

    if ((u4Subnet == 0) && (u4Mask == 0) && (u4Gateway == 0))
    {
        pWssIfNextWtpIpRouteConfigEntry = (tWssIfWtpIpRouteConfigDB *)
            RBTreeGetFirst (gCapWtpIpRouteConfigTable);

        if (pWssIfNextWtpIpRouteConfigEntry == NULL)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            MemReleaseMemBlock (CAPWAP_WTP_IP_ROUTE_CONFIG_DB_POOLID,
                                (UINT1 *) pWssIfWtpIpRouteConfigEntry);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }

        do
        {
            pWssIfWtpIpRouteConfigEntry->u4Subnet =
                pWssIfNextWtpIpRouteConfigEntry->u4Subnet;
            pWssIfWtpIpRouteConfigEntry->u4Mask =
                pWssIfNextWtpIpRouteConfigEntry->u4Mask;
            pWssIfWtpIpRouteConfigEntry->u4Gateway =
                pWssIfNextWtpIpRouteConfigEntry->u4Gateway;
            pWssIfWtpIpRouteConfigEntry->i4RowStatus =
                pWssIfNextWtpIpRouteConfigEntry->i4RowStatus;
            pWssIfWtpIpRouteConfigEntry->u4WtpProfileId =
                pWssIfNextWtpIpRouteConfigEntry->u4WtpProfileId;

            if (pWssIfNextWtpIpRouteConfigEntry->i4RowStatus == ACTIVE)
            {
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                    RouteTableUpdateReq.u4Subnet[u2Index] =
                    pWssIfWtpIpRouteConfigEntry->u4Subnet;
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                    RouteTableUpdateReq.u4NetMask[u2Index] =
                    pWssIfWtpIpRouteConfigEntry->u4Mask;
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                    RouteTableUpdateReq.u4Gateway[u2Index] =
                    pWssIfWtpIpRouteConfigEntry->u4Gateway;
                u2Index = u2Index + 1;
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                    RouteTableUpdateReq.u2NoofRoutes = u2Index;
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                    RouteTableUpdateReq.u1EntryStatus = ACTIVE;
            }
        }
        while ((pWssIfNextWtpIpRouteConfigEntry =
                (tWssIfWtpIpRouteConfigDB *) RBTreeGetNext
                (gCapWtpIpRouteConfigTable,
                 (tRBElem *) pWssIfWtpIpRouteConfigEntry, NULL)) != NULL);
    }
    else
    {
        if (u4RowStatus == DESTROY)
        {

            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                RouteTableUpdateReq.u1EntryStatus = DESTROY;
        }
        else
        {
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                RouteTableUpdateReq.u1EntryStatus = ACTIVE;
        }
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            RouteTableUpdateReq.u4Subnet[u2Index] = u4Subnet;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            RouteTableUpdateReq.u4NetMask[u2Index] = u4Mask;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            RouteTableUpdateReq.u4Gateway[u2Index] = u4Gateway;
        u2Index = u2Index + 1;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            RouteTableUpdateReq.u2NoofRoutes = u2Index;

    }

    if (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        RouteTableUpdateReq.u1EntryStatus == ACTIVE ||
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        RouteTableUpdateReq.u1EntryStatus == DESTROY)
    {
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            RouteTableUpdateReq.u2MsgEleLen = (11 + (u2Index * 12));

        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleLen =
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            RouteTableUpdateReq.u2MsgEleLen + 4;

        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;

        i4Retval = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                           pWlcHdlrMsgStruct);
        if (i4Retval == OSIX_FAILURE)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            MemReleaseMemBlock (CAPWAP_WTP_IP_ROUTE_CONFIG_DB_POOLID,
                                (UINT1 *) pWssIfWtpIpRouteConfigEntry);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    MemReleaseMemBlock (CAPWAP_WTP_IP_ROUTE_CONFIG_DB_POOLID,
                        (UINT1 *) pWssIfWtpIpRouteConfigEntry);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;

}

/*****************************************************************************
 **  Function Name   : WssIfRadioUpdateDiffServ                               *
 **  Description     : This function shall configure/ckear the DSCP params for*
               WTP                                                    *
 **  Input(s)        :                                                        *
 **  Output(s)       : None                                                   *
 **  Returns         : OSIX_FAILURE on failure
               OSIX_SUCCESS on success                                *
 ******************************************************************************/
UINT4
WssIfRadioUpdateDiffServ (INT4 i4IfIndex)
{
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tCapwapDscpMapTable CapDscpMapEntry;
    INT4                i4RetVal;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    MEMSET (&CapDscpMapEntry, 0, sizeof (tCapwapDscpMapTable));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting == LOCAL_ROUTING_DISABLE)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    /* Invoke Diff Serv DB */
    CapDscpMapEntry.u4IfIndex = (UINT4) i4IfIndex;
    CapDscpMapEntry.u2WlanProfileId = 0;

    if (WssIfCapwapDscpMapEntryGet (&CapDscpMapEntry) != OSIX_SUCCESS)
    {
    }
    if (CapDscpMapEntry.i4RowStatus == ACTIVE)
    {
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional =
            OSIX_TRUE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleType =
            VENDOR_SPECIFIC_PAYLOAD;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.elementId =
            VENDOR_CAPW_DIFF_SERV_TYPE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            VendorSpecDiffServ.u4VendorId = VENDOR_ID;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            VendorSpecDiffServ.isOptional = OSIX_TRUE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            VendorSpecDiffServ.u2MsgEleType = VENDOR_CAPW_DIFF_SERV_MSG;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            VendorSpecDiffServ.u2MsgEleLen = 8;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            VendorSpecDiffServ.u1InPriority = CapDscpMapEntry.u1InPriority;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            VendorSpecDiffServ.u1OutDscp = CapDscpMapEntry.u1OutDscp;

        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            VendorSpecDiffServ.u1RadioId = 0;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            VendorSpecDiffServ.u1EntryStatus = ACTIVE;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleLen =
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            VendorSpecDiffServ.u2MsgEleLen + 4;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;
        i4RetVal =
            WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                    pWlcHdlrMsgStruct);
        if (i4RetVal == OSIX_FAILURE)

        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    return OSIX_SUCCESS;

}

/*****************************************************************************
 **  Function Name   : WssifFirewallFilterTable                               *
 **  Description     : This function shall configure/ckear the routes created *
               in WTP                                                 *
 **  Input(s)        :                                                        *
 **  Output(s)       : None                                                   *
 **  Returns         : OSIX_FAILURE on failure
               OSIX_SUCCESS on success                                *
 ******************************************************************************/
INT4
WssifFirewallFilterTable (UINT4 u4WtpProfileId, UINT1 *pu1FilterName,
                          UINT1 *pu1SrcAddr, UINT1 *pu1DestAddr, UINT1 u1Proto,
                          UINT1 *pu1SrcPort, UINT1 *pu1DestPort,
                          UINT2 u2AddrType, UINT4 u4RowStatus)
{
    INT4                i4Retval = 0;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssIfWtpFwlFilterDB *pWssIfWtpFwlFilterDB = NULL;
    tWssIfWtpFwlFilterDB *pWssIfNextWtpFwlFilterDB = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    pWssIfWtpFwlFilterDB =
        (tWssIfWtpFwlFilterDB *)
        MemAllocMemBlk (CAPWAP_WTP_FWL_FILTER_DB_POOLID);
    if (pWssIfWtpFwlFilterDB == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    MEMSET (pWssIfWtpFwlFilterDB, 0, sizeof (tWssIfWtpFwlFilterDB));

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4WtpProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_WTP_FWL_FILTER_DB_POOLID,
                            (UINT1 *) pWssIfWtpFwlFilterDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting == LOCAL_ROUTING_DISABLE)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_WTP_FWL_FILTER_DB_POOLID,
                            (UINT1 *) pWssIfWtpFwlFilterDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4WtpProfileId;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                 pWssIfCapwapDB) != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_WTP_FWL_FILTER_DB_POOLID,
                            (UINT1 *) pWssIfWtpFwlFilterDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        FirewallFilterUpdateReq.u2MsgEleType = FIREWALL_FILTER_TABLE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        FirewallFilterUpdateReq.u2MsgEleLen = 134;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        FirewallFilterUpdateReq.u4VendorId = VENDOR_ID;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional = OSIX_TRUE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleType =
        CAPWAP_VEND_SPECIFIC_PAYLOAD_MSG_TYPE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.elementId =
        VENDOR_FIREWALL_FILTER_TABLE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        FirewallFilterUpdateReq.b1IsOptional = OSIX_TRUE;

    if ((pu1FilterName == NULL) && (pu1SrcAddr == NULL) && (pu1DestAddr == NULL)
        && (pu1SrcPort == NULL) && (pu1DestPort == NULL) && (u1Proto == 0)
        && (u2AddrType == 0))
    {
        pWssIfNextWtpFwlFilterDB = (tWssIfWtpFwlFilterDB *)
            RBTreeGetFirst (gCapWtpFwlFilterTable);

        if (pWssIfNextWtpFwlFilterDB == NULL)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            MemReleaseMemBlock (CAPWAP_WTP_FWL_FILTER_DB_POOLID,
                                (UINT1 *) pWssIfWtpFwlFilterDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }

        do
        {
            pWssIfWtpFwlFilterDB->u4WtpProfileId =
                pWssIfNextWtpFwlFilterDB->u4WtpProfileId;
            MEMCPY (pWssIfWtpFwlFilterDB->au1FilterName,
                    pWssIfNextWtpFwlFilterDB->au1FilterName,
                    STRLEN (pWssIfNextWtpFwlFilterDB->au1FilterName));
            MEMCPY (pWssIfWtpFwlFilterDB->au1SrcPort,
                    pWssIfNextWtpFwlFilterDB->au1SrcPort,
                    STRLEN (pWssIfNextWtpFwlFilterDB->au1SrcPort));
            MEMCPY (pWssIfWtpFwlFilterDB->au1DestPort,
                    pWssIfNextWtpFwlFilterDB->au1DestPort,
                    STRLEN (pWssIfNextWtpFwlFilterDB->au1DestPort));
            MEMCPY (pWssIfWtpFwlFilterDB->au1SrcAddr,
                    pWssIfNextWtpFwlFilterDB->au1SrcAddr,
                    STRLEN (pWssIfNextWtpFwlFilterDB->au1SrcAddr));
            MEMCPY (pWssIfWtpFwlFilterDB->au1DestAddr,
                    pWssIfNextWtpFwlFilterDB->au1DestAddr,
                    STRLEN (pWssIfNextWtpFwlFilterDB->au1DestAddr));
            pWssIfWtpFwlFilterDB->u1Proto = pWssIfNextWtpFwlFilterDB->u1Proto;
            pWssIfWtpFwlFilterDB->u2AddrType =
                pWssIfNextWtpFwlFilterDB->u2AddrType;
            if (pWssIfNextWtpFwlFilterDB->u4WtpProfileId != u4WtpProfileId)
            {
                continue;
            }
            if (pWssIfNextWtpFwlFilterDB->i4RowStatus == ACTIVE)
            {
                MEMCPY (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                        unVendorSpec.FirewallFilterUpdateReq.au1FilterName,
                        pWssIfWtpFwlFilterDB->au1FilterName,
                        STRLEN (pWssIfWtpFwlFilterDB->au1FilterName));
                MEMCPY (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                        unVendorSpec.FirewallFilterUpdateReq.au1SrcPort,
                        pWssIfWtpFwlFilterDB->au1SrcPort,
                        STRLEN (pWssIfWtpFwlFilterDB->au1SrcPort));
                MEMCPY (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                        unVendorSpec.FirewallFilterUpdateReq.au1DestPort,
                        pWssIfWtpFwlFilterDB->au1DestPort,
                        STRLEN (pWssIfWtpFwlFilterDB->au1DestPort));
                MEMCPY (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                        unVendorSpec.FirewallFilterUpdateReq.au1SrcAddr,
                        pWssIfWtpFwlFilterDB->au1SrcAddr,
                        STRLEN (pWssIfWtpFwlFilterDB->au1SrcAddr));
                MEMCPY (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                        unVendorSpec.FirewallFilterUpdateReq.au1DestAddr,
                        pWssIfWtpFwlFilterDB->au1DestAddr,
                        STRLEN (pWssIfWtpFwlFilterDB->au1DestAddr));
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                    FirewallFilterUpdateReq.u1Proto =
                    pWssIfWtpFwlFilterDB->u1Proto;
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                    FirewallFilterUpdateReq.u2AddrType =
                    pWssIfWtpFwlFilterDB->u2AddrType;
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                    FirewallFilterUpdateReq.u1EntryStatus = ACTIVE;
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleLen =
                    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                    unVendorSpec.FirewallFilterUpdateReq.u2MsgEleLen + 4;

                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                    FirewallFilterUpdateReq.u2NoOfFilters = 1;

                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
                    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;

                i4Retval =
                    WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                            pWlcHdlrMsgStruct);
                if (i4Retval == OSIX_FAILURE)
                {
                    continue;
                }

            }
        }
        while ((pWssIfNextWtpFwlFilterDB =
                (tWssIfWtpFwlFilterDB *) RBTreeGetNext (gCapWtpFwlFilterTable,
                                                        (tRBElem *)
                                                        pWssIfWtpFwlFilterDB,
                                                        NULL)) != NULL);
    }
    else
    {
        if (u4RowStatus == DESTROY)
        {

            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                FirewallFilterUpdateReq.u1EntryStatus = DESTROY;
        }
        else
        {
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                FirewallFilterUpdateReq.u1EntryStatus = ACTIVE;
        }
        if (pu1FilterName != NULL)
        {
            MEMCPY (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                    unVendorSpec.FirewallFilterUpdateReq.au1FilterName,
                    pu1FilterName, STRLEN (pu1FilterName - 1));
        }
        if (pu1SrcPort != NULL)
        {
            MEMCPY (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                    unVendorSpec.FirewallFilterUpdateReq.au1SrcPort, pu1SrcPort,
                    STRLEN (pu1SrcPort - 1));
        }
        if (pu1DestPort != NULL)
        {
            MEMCPY (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                    unVendorSpec.FirewallFilterUpdateReq.au1DestPort,
                    pu1DestPort, STRLEN (pu1DestPort - 1));
        }
        if (pu1SrcAddr != NULL)
        {
            MEMCPY (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                    unVendorSpec.FirewallFilterUpdateReq.au1SrcAddr, pu1SrcAddr,
                    STRLEN (pu1SrcAddr - 1));
        }
        if (pu1DestAddr != NULL)
        {
            MEMCPY (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                    unVendorSpec.FirewallFilterUpdateReq.au1DestAddr,
                    pu1DestAddr, STRLEN (pu1DestAddr - 1));
        }
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            FirewallFilterUpdateReq.u1Proto = u1Proto;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            FirewallFilterUpdateReq.u2AddrType = u2AddrType;
        if (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            FirewallFilterUpdateReq.u1EntryStatus == ACTIVE)
        {
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleLen =
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                FirewallFilterUpdateReq.u2MsgEleLen + 4;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                FirewallFilterUpdateReq.u2NoOfFilters = 1;
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
                pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;

            i4Retval =
                WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                        pWlcHdlrMsgStruct);
            if (i4Retval == OSIX_FAILURE)
            {
                UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
                MemReleaseMemBlock (CAPWAP_WTP_FWL_FILTER_DB_POOLID,
                                    (UINT1 *) pWssIfWtpFwlFilterDB);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }
        }
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    MemReleaseMemBlock (CAPWAP_WTP_FWL_FILTER_DB_POOLID,
                        (UINT1 *) pWssIfWtpFwlFilterDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssifFirewallAclTable                                  *
 **  Description     : This function shall configure the ACL in WTP           *
 **  Input(s)        :                                                        *
 **  Output(s)       : None                                                   *
 **  Returns         : OSIX_FAILURE on failure                                  
                       OSIX_SUCCESS on success                                *
 ******************************************************************************/
INT4
WssifFirewallAclTable (UINT4 u4WtpProfileId, UINT1 *pu1AclName,
                       INT4 i4AclIfIndex, UINT1 u1AclDirection, UINT1 u1Action,
                       UINT2 u2SeqNum, INT4 i4RowStatus)
{
    INT4                i4Retval = 0;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssIfWtpFwlAclDB  *pWssIfWtpFwlAclDB = NULL;
    tWssIfWtpFwlRuleDB *pWssIfWtpFwlRuleDB = NULL;
    tWssIfWtpFwlAclDB  *pWssIfNextWtpFwlAclDB = NULL;
    tWssIfWtpFwlRuleDB *pWssIfNextWtpFwlRuleDB = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    pWssIfWtpFwlAclDB =
        (tWssIfWtpFwlAclDB *) MemAllocMemBlk (CAPWAP_WTP_FWL_ACL_DB_POOLID);
    if (pWssIfWtpFwlAclDB == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    MEMSET (pWssIfWtpFwlAclDB, 0, sizeof (tWssIfWtpFwlAclDB));

    pWssIfWtpFwlRuleDB =
        (tWssIfWtpFwlRuleDB *) MemAllocMemBlk (CAPWAP_WTP_FWL_RULE_DB_POOLID);
    if (pWssIfWtpFwlRuleDB == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_WTP_FWL_ACL_DB_POOLID,
                            (UINT1 *) pWssIfWtpFwlAclDB);
        return OSIX_FAILURE;
    }
    MEMSET (pWssIfWtpFwlRuleDB, AP_FWL_ZERO, sizeof (tWssIfWtpFwlRuleDB));

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4WtpProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_WTP_FWL_ACL_DB_POOLID,
                            (UINT1 *) pWssIfWtpFwlAclDB);
        MemReleaseMemBlock (CAPWAP_WTP_FWL_RULE_DB_POOLID,
                            (UINT1 *) pWssIfWtpFwlRuleDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting == LOCAL_ROUTING_DISABLE)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_WTP_FWL_ACL_DB_POOLID,
                            (UINT1 *) pWssIfWtpFwlAclDB);
        MemReleaseMemBlock (CAPWAP_WTP_FWL_RULE_DB_POOLID,
                            (UINT1 *) pWssIfWtpFwlRuleDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4WtpProfileId;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                 pWssIfCapwapDB) != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_WTP_FWL_ACL_DB_POOLID,
                            (UINT1 *) pWssIfWtpFwlAclDB);
        MemReleaseMemBlock (CAPWAP_WTP_FWL_RULE_DB_POOLID,
                            (UINT1 *) pWssIfWtpFwlRuleDB);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        FirewallAclUpdateReq.u2MsgEleType = FIREWALL_ACL_TABLE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        FirewallAclUpdateReq.u2MsgEleLen = 91;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        FirewallAclUpdateReq.u4VendorId = VENDOR_ID;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional = OSIX_TRUE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleType =
        CAPWAP_VEND_SPECIFIC_PAYLOAD_MSG_TYPE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.elementId =
        VENDOR_FIREWALL_ACL_TABLE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        FirewallAclUpdateReq.b1IsOptional = OSIX_TRUE;

    if ((pu1AclName == 0) && (i4AclIfIndex == 0) && (u1AclDirection == 0) &&
        (u1Action == 0) && (u2SeqNum == 0) && (i4RowStatus == 0))
    {
        pWssIfNextWtpFwlAclDB = (tWssIfWtpFwlAclDB *)
            RBTreeGetFirst (gCapWtpFwlAclTable);

        pWssIfNextWtpFwlRuleDB = (tWssIfWtpFwlRuleDB *)
            RBTreeGetFirst (gCapWtpFwlRuleTable);

        if (pWssIfNextWtpFwlAclDB == NULL)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            MemReleaseMemBlock (CAPWAP_WTP_FWL_ACL_DB_POOLID,
                                (UINT1 *) pWssIfWtpFwlAclDB);
            MemReleaseMemBlock (CAPWAP_WTP_FWL_RULE_DB_POOLID,
                                (UINT1 *) pWssIfWtpFwlRuleDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }
        if (pWssIfNextWtpFwlRuleDB == NULL)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            MemReleaseMemBlock (CAPWAP_WTP_FWL_ACL_DB_POOLID,
                                (UINT1 *) pWssIfWtpFwlAclDB);
            MemReleaseMemBlock (CAPWAP_WTP_FWL_RULE_DB_POOLID,
                                (UINT1 *) pWssIfWtpFwlRuleDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }
        do
        {
            pWssIfWtpFwlAclDB->u4WtpProfileId =
                pWssIfNextWtpFwlAclDB->u4WtpProfileId;
            pWssIfWtpFwlRuleDB->u4WtpProfileId =
                pWssIfNextWtpFwlRuleDB->u4WtpProfileId;
            MEMCPY (pWssIfWtpFwlAclDB->au1AclName,
                    pWssIfNextWtpFwlAclDB->au1AclName,
                    STRLEN (pWssIfNextWtpFwlAclDB->au1AclName));
            MEMCPY (pWssIfWtpFwlRuleDB->au1RuleName,
                    pWssIfNextWtpFwlRuleDB->au1RuleName,
                    STRLEN (pWssIfNextWtpFwlRuleDB->au1RuleName));
            MEMCPY (pWssIfWtpFwlRuleDB->au1FilterSet,
                    pWssIfNextWtpFwlRuleDB->au1FilterSet,
                    STRLEN (pWssIfNextWtpFwlRuleDB->au1FilterSet));
            pWssIfWtpFwlAclDB->i4AclIfIndex =
                pWssIfNextWtpFwlAclDB->i4AclIfIndex;
            pWssIfWtpFwlAclDB->u1AclDirection =
                pWssIfNextWtpFwlAclDB->u1AclDirection;
            pWssIfWtpFwlAclDB->u1Action = pWssIfNextWtpFwlAclDB->u1Action;
            pWssIfWtpFwlAclDB->u2SeqNum = pWssIfNextWtpFwlAclDB->u2SeqNum;
            if ((pWssIfNextWtpFwlAclDB->u4WtpProfileId != u4WtpProfileId) ||
                (pWssIfNextWtpFwlRuleDB->u4WtpProfileId != u4WtpProfileId))
            {
                continue;
            }
            if ((pWssIfNextWtpFwlAclDB->i4RowStatus == ACTIVE) &&
                (pWssIfNextWtpFwlRuleDB->i4RowStatus == ACTIVE))
            {
                MEMCPY (&pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                        unVendorSpec.FirewallAclUpdateReq.au1AclName,
                        pWssIfWtpFwlAclDB->au1AclName,
                        STRLEN (pWssIfWtpFwlAclDB->au1AclName));
                MEMCPY (&pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                        unVendorSpec.FirewallAclUpdateReq.au1FilterSet,
                        pWssIfWtpFwlRuleDB->au1FilterSet,
                        STRLEN (pWssIfWtpFwlRuleDB->au1FilterSet));
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                    FirewallAclUpdateReq.i4AclIfIndex =
                    pWssIfWtpFwlAclDB->i4AclIfIndex;
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                    FirewallAclUpdateReq.u1AclDirection =
                    pWssIfWtpFwlAclDB->u1AclDirection;
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                    FirewallAclUpdateReq.u1Action = pWssIfWtpFwlAclDB->u1Action;
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                    FirewallAclUpdateReq.u2SeqNum = pWssIfWtpFwlAclDB->u2SeqNum;
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                    FirewallAclUpdateReq.u1EntryStatus = ACTIVE;
                if (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                    unVendorSpec.FirewallAclUpdateReq.u1EntryStatus == ACTIVE)
                {
                    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                        u2MsgEleLen =
                        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                        unVendorSpec.FirewallAclUpdateReq.u2MsgEleLen + 4;

                    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                        unVendorSpec.FirewallAclUpdateReq.u2NoOfAcl = 1;
                    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
                        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;

                    i4Retval =
                        WssIfProcessWlcHdlrMsg
                        (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ, pWlcHdlrMsgStruct);
                    if (i4Retval == OSIX_FAILURE)
                    {
                        continue;
                    }
                }
            }
        }
        while (((pWssIfNextWtpFwlAclDB = (tWssIfWtpFwlAclDB *) RBTreeGetNext
                 (gCapWtpFwlAclTable, (tRBElem *) pWssIfWtpFwlAclDB,
                  NULL)) != NULL)
               &&
               ((pWssIfNextWtpFwlRuleDB =
                 (tWssIfWtpFwlRuleDB *) RBTreeGetNext (gCapWtpFwlRuleTable,
                                                       (tRBElem *)
                                                       pWssIfWtpFwlRuleDB,
                                                       NULL)) != NULL));
    }
    else
    {
        if (i4RowStatus == DESTROY)
        {

            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                FirewallAclUpdateReq.u1EntryStatus = DESTROY;
        }
        else
        {
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                FirewallAclUpdateReq.u1EntryStatus = ACTIVE;
        }
        if (pu1AclName != NULL)
        {
            MEMCPY (&pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                    unVendorSpec.FirewallAclUpdateReq.au1AclName, pu1AclName,
                    STRLEN (pu1AclName - 1));
            pWssIfWtpFwlRuleDB->u4WtpProfileId = u4WtpProfileId;
            MEMCPY (pWssIfWtpFwlRuleDB->au1RuleName,
                    pu1AclName, STRLEN (pu1AclName - 1));
        }
        if (OSIX_SUCCESS == WssIfWtpFwlRuleGetEntry (pWssIfWtpFwlRuleDB))
        {
            MEMCPY (&pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.
                    unVendorSpec.FirewallAclUpdateReq.au1FilterSet,
                    pWssIfWtpFwlRuleDB->au1FilterSet,
                    STRLEN (pWssIfWtpFwlRuleDB->au1FilterSet));
        }
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            FirewallAclUpdateReq.i4AclIfIndex = i4AclIfIndex;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            FirewallAclUpdateReq.u1AclDirection = u1AclDirection;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            FirewallAclUpdateReq.u1Action = u1Action;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            FirewallAclUpdateReq.u2SeqNum = u2SeqNum;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleLen =
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            FirewallAclUpdateReq.u2MsgEleLen + 4;

        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            FirewallAclUpdateReq.u2NoOfAcl = 1;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;

        i4Retval = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                           pWlcHdlrMsgStruct);
        if (i4Retval == OSIX_FAILURE)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            MemReleaseMemBlock (CAPWAP_WTP_FWL_ACL_DB_POOLID,
                                (UINT1 *) pWssIfWtpFwlAclDB);
            MemReleaseMemBlock (CAPWAP_WTP_FWL_RULE_DB_POOLID,
                                (UINT1 *) pWssIfWtpFwlRuleDB);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    MemReleaseMemBlock (CAPWAP_WTP_FWL_ACL_DB_POOLID,
                        (UINT1 *) pWssIfWtpFwlAclDB);
    MemReleaseMemBlock (CAPWAP_WTP_FWL_RULE_DB_POOLID,
                        (UINT1 *) pWssIfWtpFwlRuleDB);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;

}

/*****************************************************************************
 **  Function Name   : WssIfL3SubIfUpdateReq                                  *
 **  Description     : This function shall configure/clear the L3 SUb Ifaces  *
               in WTP                                                 *
 **  Input(s)        :                                                        *
 **  Output(s)       : None                                                   *
 **  Returns         : OSIX_FAILURE on failure
               OSIX_SUCCESS on success                                *
 ******************************************************************************/
INT4
WssIfL3SubIfUpdateReq (tWssIfWtpL3SubIfDB * ptWssIfWtpL3SubIfUpdEntry)
{
    INT4                i4Retval = 0;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tWssIfWtpL3SubIfDB *pWssIfWtpL3SubIfEntry = NULL;
    tWssIfWtpL3SubIfDB *pWssIfNextWtpL3SubIfEntry = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2Index = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    pWssIfWtpL3SubIfEntry =
        (tWssIfWtpL3SubIfDB *) MemAllocMemBlk (CAPWAP_L3_SUB_IF_DB_POOLID);
    if (pWssIfWtpL3SubIfEntry == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    MEMSET (pWssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpLocalRouting = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2ProfileId =
        (UINT2) ptWssIfWtpL3SubIfUpdEntry->u4WtpProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_L3_SUB_IF_DB_POOLID,
                            (UINT1 *) pWssIfWtpL3SubIfEntry);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    if (pWssIfCapwapDB->CapwapGetDB.u1WtpLocalRouting == LOCAL_ROUTING_DISABLE)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_L3_SUB_IF_DB_POOLID,
                            (UINT1 *) pWssIfWtpL3SubIfEntry);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_SUCCESS;
    }

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2ProfileId =
        (UINT2) ptWssIfWtpL3SubIfUpdEntry->u4WtpProfileId;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                 pWssIfCapwapDB) != OSIX_SUCCESS)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_L3_SUB_IF_DB_POOLID,
                            (UINT1 *) pWssIfWtpL3SubIfEntry);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        L3SubIfUpdateReq.u2MsgEleType = L3SUBIF_TABLE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        L3SubIfUpdateReq.u4VendorId = VENDOR_ID;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.isOptional = OSIX_TRUE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleType =
        CAPWAP_VEND_SPECIFIC_PAYLOAD_MSG_TYPE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.elementId =
        VENDOR_L3SUBIF_TABLE;
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        L3SubIfUpdateReq.b1IsOptional = OSIX_TRUE;

    if (ptWssIfWtpL3SubIfUpdEntry->u4IfIndex == 0)
    {
        pWssIfNextWtpL3SubIfEntry = (tWssIfWtpL3SubIfDB *)
            RBTreeGetFirst (gCapWtpL3SubIfTable);

        if (pWssIfNextWtpL3SubIfEntry == NULL)
        {
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            MemReleaseMemBlock (CAPWAP_L3_SUB_IF_DB_POOLID,
                                (UINT1 *) pWssIfWtpL3SubIfEntry);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_SUCCESS;
        }

        do
        {
            pWssIfWtpL3SubIfEntry->u4WtpProfileId =
                pWssIfNextWtpL3SubIfEntry->u4WtpProfileId;
            pWssIfWtpL3SubIfEntry->u4IfIndex =
                pWssIfNextWtpL3SubIfEntry->u4IfIndex;
            pWssIfWtpL3SubIfEntry->u4PhyPort =
                pWssIfNextWtpL3SubIfEntry->u4PhyPort;
            pWssIfWtpL3SubIfEntry->u2VlanId =
                pWssIfNextWtpL3SubIfEntry->u2VlanId;
            pWssIfWtpL3SubIfEntry->i4IfType =
                pWssIfNextWtpL3SubIfEntry->i4IfType;
            pWssIfWtpL3SubIfEntry->u4IpAddr =
                pWssIfNextWtpL3SubIfEntry->u4IpAddr;
            pWssIfWtpL3SubIfEntry->u4SubnetMask =
                pWssIfNextWtpL3SubIfEntry->u4SubnetMask;
            pWssIfWtpL3SubIfEntry->u4BroadcastAddr =
                pWssIfNextWtpL3SubIfEntry->u4BroadcastAddr;
            pWssIfWtpL3SubIfEntry->i4IfNwType =
                pWssIfNextWtpL3SubIfEntry->i4IfNwType;
            pWssIfWtpL3SubIfEntry->u1IfAdminStatus =
                pWssIfNextWtpL3SubIfEntry->u1IfAdminStatus;
            pWssIfWtpL3SubIfEntry->i4RowStatus =
                pWssIfNextWtpL3SubIfEntry->i4RowStatus;

            if (pWssIfNextWtpL3SubIfEntry->i4RowStatus == ACTIVE)
            {
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                    L3SubIfUpdateReq.u4PhyPort[u2Index] =
                    pWssIfWtpL3SubIfEntry->u4PhyPort;
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                    L3SubIfUpdateReq.u1IfNwType[u2Index] =
                    (UINT1) pWssIfWtpL3SubIfEntry->i4IfNwType;
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                    L3SubIfUpdateReq.u2VlanId[u2Index] =
                    pWssIfWtpL3SubIfEntry->u2VlanId;
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                    L3SubIfUpdateReq.u4IpAddr[u2Index] =
                    pWssIfWtpL3SubIfEntry->u4IpAddr;
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                    L3SubIfUpdateReq.u4SubnetMask[u2Index] =
                    pWssIfWtpL3SubIfEntry->u4SubnetMask;
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                    L3SubIfUpdateReq.u1IfAdminStatus[u2Index] =
                    pWssIfWtpL3SubIfEntry->u1IfAdminStatus;
                u2Index = u2Index + 1;
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                    L3SubIfUpdateReq.u2NoOfIfaces = u2Index;
                pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                    L3SubIfUpdateReq.u1EntryStatus = ACTIVE;
            }
        }
        while ((pWssIfNextWtpL3SubIfEntry =
                (tWssIfWtpL3SubIfDB *) RBTreeGetNext
                (gCapWtpL3SubIfTable,
                 (tRBElem *) pWssIfWtpL3SubIfEntry, NULL)) != NULL);
    }
    else
    {
        if (ptWssIfWtpL3SubIfUpdEntry->i4RowStatus == DESTROY)
        {

            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                L3SubIfUpdateReq.u1EntryStatus = DESTROY;
        }
        else
        {
            pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
                L3SubIfUpdateReq.u1EntryStatus = ACTIVE;
        }
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            L3SubIfUpdateReq.u4PhyPort[u2Index] =
            ptWssIfWtpL3SubIfUpdEntry->u4PhyPort;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            L3SubIfUpdateReq.u1IfNwType[u2Index] =
            (UINT1) ptWssIfWtpL3SubIfUpdEntry->i4IfNwType;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            L3SubIfUpdateReq.u2VlanId[u2Index] =
            ptWssIfWtpL3SubIfUpdEntry->u2VlanId;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            L3SubIfUpdateReq.u4IpAddr[u2Index] =
            ptWssIfWtpL3SubIfUpdEntry->u4IpAddr;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            L3SubIfUpdateReq.u4SubnetMask[u2Index] =
            ptWssIfWtpL3SubIfUpdEntry->u4SubnetMask;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            L3SubIfUpdateReq.u1IfAdminStatus[u2Index] =
            ptWssIfWtpL3SubIfUpdEntry->u1IfAdminStatus;
        u2Index = u2Index + 1;
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
            L3SubIfUpdateReq.u2NoOfIfaces = u2Index;
    }

    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        L3SubIfUpdateReq.u2MsgEleLen = AP_L3_UPDATEREQ_MSGLEN (u2Index);

    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.u2MsgEleLen =
        pWlcHdlrMsgStruct->CapwapConfigUpdateReq.vendSpec.unVendorSpec.
        L3SubIfUpdateReq.u2MsgEleLen + 4;

    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;

    i4Retval = WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                       pWlcHdlrMsgStruct);
    if (i4Retval == OSIX_FAILURE)
    {
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        MemReleaseMemBlock (CAPWAP_L3_SUB_IF_DB_POOLID,
                            (UINT1 *) pWssIfWtpL3SubIfEntry);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
    MemReleaseMemBlock (CAPWAP_L3_SUB_IF_DB_POOLID,
                        (UINT1 *) pWssIfWtpL3SubIfEntry);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;

}

/*****************************************************************************
 *  Function Name   : WssIfWtpFwlFilterGetFirstEntry                         *
 *  Description     : This function used to get first entry of the firewall
 *                    filter table                                           *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : pu4BaseProfileId, pu4FilterName                        *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpFwlFilterGetFirstEntry (UINT4 *pu4BaseProfileId, UINT1 *pu1FilterName)
{
    tWssIfWtpFwlFilterDB *pWssIfWtpFwlFilter = NULL;
    pWssIfWtpFwlFilter = (tWssIfWtpFwlFilterDB *)
        RBTreeGetFirst (gCapWtpFwlFilterTable);

    if (pWssIfWtpFwlFilter == NULL)
    {
        return OSIX_FAILURE;
    }
    *pu4BaseProfileId = pWssIfWtpFwlFilter->u4WtpProfileId;
    MEMCPY (pu1FilterName, pWssIfWtpFwlFilter->au1FilterName,
            AP_FWL_MAX_FILTER_NAME_LEN);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfWtpFwlFilterGetNextEntry                             *
 **  Description     : This function used to get next entry of the current       *
               entry whose wtp Profile Id is given,  from WTP FWL FILTER RB tree *                                           *
 **  Input(s)        : u4WtpProfileId, *pu1FilterName                            *
 **  Output(s)       : *pu4NextWtpProfileId, *pu1NextFilterName                  *
 **  Returns         : OSIX_FAILURE on failure                                   *
 *                     OSIX_SUCCESS on success                                   *
 *******************************************************************************/
INT4
WssIfWtpFwlFilterGetNextEntry (UINT4 u4WtpProfileId, UINT4 *pu4NextWtpProfileId,
                               UINT1 *pu1FilterName, UINT1 *pu1NextFilterName)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;
    tWssIfWtpFwlFilterDB *pWssIfWtpFwlFilter = NULL;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    WssIfWtpFwlFilter.u4WtpProfileId = u4WtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pu1FilterName,
            AP_FWL_MAX_FILTER_NAME_LEN);
    pWssIfWtpFwlFilter = (tWssIfWtpFwlFilterDB *) RBTreeGetNext
        (gCapWtpFwlFilterTable, (tRBElem *) & WssIfWtpFwlFilter, NULL);
    if (pWssIfWtpFwlFilter == NULL)
    {
        return OSIX_FAILURE;
    }
    *pu4NextWtpProfileId = pWssIfWtpFwlFilter->u4WtpProfileId;
    MEMCPY (pu1NextFilterName, pWssIfWtpFwlFilter->au1FilterName,
            AP_FWL_MAX_FILTER_NAME_LEN);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpFwlRuleGetFirstEntry                           *
 *  Description     : This function used to get first entry of the firewall
 *                    Rule table                                             *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : pu4BaseProfileId, pu4RuleName                          *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpFwlRuleGetFirstEntry (UINT4 *pu4BaseProfileId, UINT1 *pu1FilterName)
{
    tWssIfWtpFwlRuleDB *pWssIfWtpFwlRule = NULL;
    pWssIfWtpFwlRule = (tWssIfWtpFwlRuleDB *)
        RBTreeGetFirst (gCapWtpFwlRuleTable);

    if (pWssIfWtpFwlRule == NULL)
    {
        return OSIX_FAILURE;
    }
    *pu4BaseProfileId = pWssIfWtpFwlRule->u4WtpProfileId;
    MEMCPY (pu1FilterName, pWssIfWtpFwlRule->au1RuleName,
            AP_FWL_MAX_FILTER_NAME_LEN);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfWtpFwlRuleGetNextEntry                               *
 **  Description     : This function used to get next entry of the current       *
               entry whose wtp Profile Id is given,  from WTP FWL Rule RB tree *                                           *
 **  Input(s)        : u4WtpProfileId, *pu1RuleName                            *
 **  Output(s)       : *pu4NextWtpProfileId, *pu1NextRuleName                  *
 **  Returns         : OSIX_FAILURE on failure                                   *
 *                     OSIX_SUCCESS on success                                   *
 *******************************************************************************/
INT4
WssIfWtpFwlRuleGetNextEntry (UINT4 u4WtpProfileId, UINT4 *pu4NextWtpProfileId,
                             UINT1 *pu1RuleName, UINT1 *pu1NextRuleName)
{
    tWssIfWtpFwlRuleDB  WssIfWtpFwlRule;
    tWssIfWtpFwlRuleDB *pWssIfWtpFwlRule = NULL;

    MEMSET (&WssIfWtpFwlRule, 0, sizeof (tWssIfWtpFwlRuleDB));

    WssIfWtpFwlRule.u4WtpProfileId = u4WtpProfileId;
    MEMCPY (WssIfWtpFwlRule.au1RuleName, pu1RuleName, AP_FWL_MAX_RULE_NAME_LEN);
    pWssIfWtpFwlRule = (tWssIfWtpFwlRuleDB *) RBTreeGetNext
        (gCapWtpFwlRuleTable, (tRBElem *) & WssIfWtpFwlRule, NULL);
    if (pWssIfWtpFwlRule == NULL)
    {
        return OSIX_FAILURE;
    }
    *pu4NextWtpProfileId = pWssIfWtpFwlRule->u4WtpProfileId;
    MEMCPY (pu1NextRuleName, pWssIfWtpFwlRule->au1RuleName,
            AP_FWL_MAX_RULE_NAME_LEN);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpFwlAclGetFirstEntry                            *
 *  Description     : This function used to get first entry of the firewall
 *                    Acl table                                              *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : pu4BaseProfileId, pu4AclName                           *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpFwlAclGetFirstEntry (UINT4 *pu4BaseProfileId, UINT1 *pu1AclName,
                             INT4 *pi4AclIfIndex, UINT1 *pu1AclDirection)
{
    tWssIfWtpFwlAclDB  *pWssIfWtpFwlAcl = NULL;
    pWssIfWtpFwlAcl = (tWssIfWtpFwlAclDB *) RBTreeGetFirst (gCapWtpFwlAclTable);

    if (pWssIfWtpFwlAcl == NULL)
    {
        return OSIX_FAILURE;
    }
    *pu4BaseProfileId = pWssIfWtpFwlAcl->u4WtpProfileId;
    MEMCPY (pu1AclName, pWssIfWtpFwlAcl->au1AclName, AP_FWL_MAX_ACL_NAME_LEN);
    *pi4AclIfIndex = pWssIfWtpFwlAcl->i4AclIfIndex;
    *pu1AclDirection = pWssIfWtpFwlAcl->u1AclDirection;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfWtpFwlAclGetNextEntry                             *
 **  Description     : This function used to get next entry of the current       *
               entry whose wtp Profile Id is given,  from WTP FWL ACL RB tree *                                           *
 **  Input(s)        : u4WtpProfileId, *pu1AclName                            *
 **  Output(s)       : *pu4NextWtpProfileId, *pu1NextAclName                  *
 **  Returns         : OSIX_FAILURE on failure                                   *
 *                     OSIX_SUCCESS on success                                   *
 *******************************************************************************/
INT4
WssIfWtpFwlAclGetNextEntry (UINT4 u4WtpProfileId, UINT4 *pu4NextWtpProfileId,
                            UINT1 *pu1AclName, UINT1 *pu1NextAclName,
                            INT4 i4AclIfIndex, INT4 *pi4AclIfIndex,
                            UINT1 u1AclDirection, UINT1 *pu1AclDirection)
{
    tWssIfWtpFwlAclDB   WssIfWtpFwlAcl;
    tWssIfWtpFwlAclDB  *pWssIfWtpFwlAcl = NULL;

    MEMSET (&WssIfWtpFwlAcl, 0, sizeof (tWssIfWtpFwlAclDB));

    WssIfWtpFwlAcl.u4WtpProfileId = u4WtpProfileId;
    MEMCPY (WssIfWtpFwlAcl.au1AclName, pu1AclName, STRLEN (pu1AclName - 1));
    WssIfWtpFwlAcl.i4AclIfIndex = i4AclIfIndex;
    WssIfWtpFwlAcl.u1AclDirection = u1AclDirection;
    pWssIfWtpFwlAcl = (tWssIfWtpFwlAclDB *) RBTreeGetNext
        (gCapWtpFwlAclTable, (tRBElem *) & WssIfWtpFwlAcl, NULL);
    if (pWssIfWtpFwlAcl == NULL)
    {
        return OSIX_FAILURE;
    }
    *pu4NextWtpProfileId = pWssIfWtpFwlAcl->u4WtpProfileId;
    MEMCPY (pu1NextAclName, pWssIfWtpFwlAcl->au1AclName,
            AP_FWL_MAX_ACL_NAME_LEN);
    *pi4AclIfIndex = pWssIfWtpFwlAcl->i4AclIfIndex;
    *pu1AclDirection = pWssIfWtpFwlAcl->u1AclDirection;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpL3SubIfGetFirstEntry                           *
 *  Description     : This function used to get first entry of the L3 SUB
 *                    If table                                               *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : pu4BaseProfileId, pu4IfIndex                           *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpL3SubIfGetFirstEntry (UINT4 *pu4BaseProfileId, UINT4 *pu4IfIndex)
{

    tWssIfWtpL3SubIfDB *pWssIfWtpL3SubIfEntry = NULL;

    pWssIfWtpL3SubIfEntry = (tWssIfWtpL3SubIfDB *)
        RBTreeGetFirst (gCapWtpL3SubIfTable);

    if (pWssIfWtpL3SubIfEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    *pu4BaseProfileId = pWssIfWtpL3SubIfEntry->u4WtpProfileId;
    *pu4IfIndex = pWssIfWtpL3SubIfEntry->u4IfIndex;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 **  Function Name   : WssIfWtpL3SubIfGetNextEntry                               *
 **  Description     : This function used to get next entry of the current       *
               entry whose wtp Profile Id is given,  from WTP L3 SUB If RB tree *                                           *
 **  Input(s)        : u4WtpProfileId, u4IfIndex                            *
 **  Output(s)       : *pu4NextWtpProfileId, *pu4NextIfIndex                  *
 **  Returns         : OSIX_FAILURE on failure                                   *
 *                     OSIX_SUCCESS on success                                   *
 *******************************************************************************/
INT4
WssIfWtpL3SubIfGetNextEntry (UINT4 u4WtpProfileId, UINT4 *pu4NextWtpProfileId,
                             UINT4 u4IfIndex, UINT4 *pu4NexIfIndex)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;
    tWssIfWtpL3SubIfDB *pWssIfWtpL3SubIfEntry = NULL;

    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));

    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4WtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = u4IfIndex;
    pWssIfWtpL3SubIfEntry = (tWssIfWtpL3SubIfDB *) RBTreeGetNext
        (gCapWtpL3SubIfTable, (tRBElem *) & WssIfWtpL3SubIfEntry, NULL);

    if (pWssIfWtpL3SubIfEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    *pu4NextWtpProfileId = pWssIfWtpL3SubIfEntry->u4WtpProfileId;
    *pu4NexIfIndex = pWssIfWtpL3SubIfEntry->u4IfIndex;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfWtpL3SubIfDeleteEntryOnProfile                    *
 *  Description     : This function used to delete L3 sub If entry created
 *                    with the profile ID                                    *
 *                                                                           *
 *  Input(s)        : None                                                   *
 *                                                                           *
 *  Output(s)       : u4WtpProfileId                                 *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfWtpL3SubIfDeleteEntryOnProfile (UINT4 u4WtpProfileId)
{
    tWssIfWtpL3SubIfDB *pWssIfNextWtpL3SubIfEntry = NULL;
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;

    pWssIfNextWtpL3SubIfEntry = (tWssIfWtpL3SubIfDB *)
        RBTreeGetFirst (gCapWtpL3SubIfTable);

    if (pWssIfNextWtpL3SubIfEntry == NULL)
    {
        return OSIX_SUCCESS;
    }

    do
    {
        MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));
        WssIfWtpL3SubIfEntry.u4WtpProfileId =
            pWssIfNextWtpL3SubIfEntry->u4WtpProfileId;
        WssIfWtpL3SubIfEntry.u4IfIndex = pWssIfNextWtpL3SubIfEntry->u4IfIndex;
        WssIfWtpL3SubIfEntry.u4PhyPort = pWssIfNextWtpL3SubIfEntry->u4PhyPort;
        WssIfWtpL3SubIfEntry.u2VlanId = pWssIfNextWtpL3SubIfEntry->u2VlanId;
        WssIfWtpL3SubIfEntry.i4IfType = pWssIfNextWtpL3SubIfEntry->i4IfType;
        WssIfWtpL3SubIfEntry.u4IpAddr = pWssIfNextWtpL3SubIfEntry->u4IpAddr;
        WssIfWtpL3SubIfEntry.u4SubnetMask =
            pWssIfNextWtpL3SubIfEntry->u4SubnetMask;
        WssIfWtpL3SubIfEntry.u4BroadcastAddr =
            pWssIfNextWtpL3SubIfEntry->u4BroadcastAddr;
        WssIfWtpL3SubIfEntry.i4IfNwType = pWssIfNextWtpL3SubIfEntry->i4IfNwType;
        WssIfWtpL3SubIfEntry.u1IfAdminStatus =
            pWssIfNextWtpL3SubIfEntry->u1IfAdminStatus;
        WssIfWtpL3SubIfEntry.i4RowStatus =
            pWssIfNextWtpL3SubIfEntry->i4RowStatus;

        if (pWssIfNextWtpL3SubIfEntry->u4WtpProfileId == u4WtpProfileId)
        {
            if (WssIfWtpL3SubIfDeleteEntry (&WssIfWtpL3SubIfEntry) !=
                OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
        }
    }
    while ((pWssIfNextWtpL3SubIfEntry =
            (tWssIfWtpL3SubIfDB *) RBTreeGetNext
            (gCapWtpL3SubIfTable,
             (tRBElem *) & WssIfWtpL3SubIfEntry, NULL)) != NULL);
    return OSIX_SUCCESS;
}

#endif

#ifdef WTP_WANTED
#ifdef KERNEL_CAPWAP_WANTED
/*****************************************************************************
 *  Function Name   : WssQosUpdateKernelDB                                   *
 *  Description     : This function calls the npapi to update the kernel DB  *
 *  Input(s)        : Dscp Map Table - pCapDscpMap                           *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssQosUpdateKernelDB (tCapwapDscpMapTable * pCapDscpMap)
{
    UINT1               u1Module = 0;
    UINT2               u2Field = 0;
    tWtpKernelDB        KernelDBWtp;

    MEMSET (&KernelDBWtp, 0, sizeof (tWtpKernelDB));

    MEMCPY (KernelDBWtp.rbData.qosData.au1InterfaceName, pCapDscpMap->
            au1InterfaceName, CFA_MAX_PORT_NAME_LENGTH);
    KernelDBWtp.rbData.qosData.u1dscpin = pCapDscpMap->u1InPriority;
    KernelDBWtp.rbData.qosData.u1dscpout = pCapDscpMap->u1OutDscp;

    u1Module = DSCP_MODULE;
    u2Field = AP_QOS_TABLE;
    if (capwapNpUpdateKernel (u1Module, u2Field, &KernelDBWtp) == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "Kernel Notification Failed \r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssQosRemoveKernelDB                                   *
 *  Description     : This function calls the npapi to update the kernel DB  *
 *  Input(s)        : stationMac - Station Mac Address                       *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                                                                           *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssQosRemoveKernelDB (tCapwapDscpMapTable * pCapDscpMap)
{
    UINT1               u1Module = 0;
    UINT2               u2Field = 0;
    INT4                i4RetVal = 0;
    tWtpKernelDB        KernelDBWtp;

    MEMSET (&KernelDBWtp, 0, sizeof (tWtpKernelDB));

    MEMCPY (KernelDBWtp.rbData.qosData.au1InterfaceName, pCapDscpMap->
            au1InterfaceName, CFA_MAX_PORT_NAME_LENGTH);
    KernelDBWtp.rbData.qosData.u1dscpin = pCapDscpMap->u1InPriority;
    KernelDBWtp.rbData.qosData.u1dscpout = pCapDscpMap->u1OutDscp;

    u1Module = DSCP_MODULE;
    u2Field = AP_QOS_TABLE;
    i4RetVal = capwapNpKernelRemove (u1Module, u2Field, &KernelDBWtp);
    if (i4RetVal == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "Kernel removal Failed \r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}
#endif
#endif
