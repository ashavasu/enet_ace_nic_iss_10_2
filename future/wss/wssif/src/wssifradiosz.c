/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                     *
 *   $Id: wssifradiosz.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $                                                                          *
 *  Description: This file contains the Memory pool related api  for WTP     *
 *                                                                           *
 *****************************************************************************/

#define _WSSIFRADIOSZ_C
#include "wssifinc.h"
extern INT4  IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams); 
extern INT4  IssSzRegisterModulePoolId( CHR1 *pu1ModName, tMemPoolId *pModPoolId); 
INT4  WssifradioSizingMemCreateMemPools()
{
    INT4 i4RetVal;
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < WSSIFRADIO_MAX_SIZING_ID; i4SizingId++) {
        i4RetVal = (INT4)MemCreateMemPool( 
                          FsWSSIFRADIOSizingParams[i4SizingId].u4StructSize,
                          FsWSSIFRADIOSizingParams[i4SizingId].u4PreAllocatedUnits,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(WSSIFRADIOMemPoolIds[ i4SizingId]));
        if( i4RetVal == (INT4) MEM_FAILURE) {
            WssifradioSizingMemDeleteMemPools();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4   WssifradioSzRegisterModuleSizingParams( CHR1 *pu1ModName)
{
      /* Copy the Module Name */ 
       IssSzRegisterModuleSizingParams( pu1ModName, FsWSSIFRADIOSizingParams); 
      IssSzRegisterModulePoolId( pu1ModName, WSSIFRADIOMemPoolIds); 
      return OSIX_SUCCESS; 
}


VOID  WssifradioSizingMemDeleteMemPools()
{
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < WSSIFRADIO_MAX_SIZING_ID; i4SizingId++) {
        if(WSSIFRADIOMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool( WSSIFRADIOMemPoolIds[ i4SizingId] );
            WSSIFRADIOMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
