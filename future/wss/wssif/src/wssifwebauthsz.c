/******************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                       *
 *  $Id: wssifwebauthsz.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $                                                                            *
 * Description: This file contains the MEMPOOL related API for WSSSTA MODULE  *
 ******************************************************************************/

#define _WSSIFWEBAUTHSZ_C
#include "wssifinc.h"
extern INT4  IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams); 
extern INT4  IssSzRegisterModulePoolId( CHR1 *pu1ModName, tMemPoolId *pModPoolId); 
INT4  WssifwebauthSizingMemCreateMemPools()
{
    INT4 i4RetVal;
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < WSSIFWEBAUTH_MAX_SIZING_ID; i4SizingId++) {
        i4RetVal = (INT4)MemCreateMemPool( 
                          FsWSSIFWEBAUTHSizingParams[i4SizingId].u4StructSize,
                          FsWSSIFWEBAUTHSizingParams[i4SizingId].u4PreAllocatedUnits,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(WSSIFWEBAUTHMemPoolIds[ i4SizingId]));
        if( i4RetVal == (INT4) MEM_FAILURE) {
            WssifwebauthSizingMemDeleteMemPools();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4   WssifwebauthSzRegisterModuleSizingParams( CHR1 *pu1ModName)
{
      /* Copy the Module Name */ 
       IssSzRegisterModuleSizingParams( pu1ModName, FsWSSIFWEBAUTHSizingParams); 
      IssSzRegisterModulePoolId( pu1ModName, WSSIFWEBAUTHMemPoolIds); 
      return OSIX_SUCCESS; 
}


VOID  WssifwebauthSizingMemDeleteMemPools()
{
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < WSSIFWEBAUTH_MAX_SIZING_ID; i4SizingId++) {
        if(WSSIFWEBAUTHMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool( WSSIFWEBAUTHMemPoolIds[ i4SizingId] );
            WSSIFWEBAUTHMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
