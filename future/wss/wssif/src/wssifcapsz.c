/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                     *
 *   $Id: wssifcapsz.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $                                                                          *
 *  Description: This file contains the Memory pool related api  for WTP     *
 *                                                                           *
 *****************************************************************************/

#define _WSSIFCAPSZ_C
#include "wssifinc.h"
extern INT4  IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams); 
extern INT4  IssSzRegisterModulePoolId( CHR1 *pu1ModName, tMemPoolId *pModPoolId); 
INT4  WssifcapSizingMemCreateMemPools()
{
    INT4 i4RetVal;
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < WSSIFCAP_MAX_SIZING_ID; i4SizingId++) {
        i4RetVal = (INT4)MemCreateMemPool( 
                          FsWSSIFCAPSizingParams[i4SizingId].u4StructSize,
                          FsWSSIFCAPSizingParams[i4SizingId].u4PreAllocatedUnits,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(WSSIFCAPMemPoolIds[ i4SizingId]));
        if( i4RetVal == (INT4) MEM_FAILURE) {
            WssifcapSizingMemDeleteMemPools();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4   WssifcapSzRegisterModuleSizingParams( CHR1 *pu1ModName)
{
      /* Copy the Module Name */ 
       IssSzRegisterModuleSizingParams( pu1ModName, FsWSSIFCAPSizingParams); 
      IssSzRegisterModulePoolId( pu1ModName, WSSIFCAPMemPoolIds); 
      return OSIX_SUCCESS; 
}


VOID  WssifcapSizingMemDeleteMemPools()
{
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < WSSIFCAP_MAX_SIZING_ID; i4SizingId++) {
        if(WSSIFCAPMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool( WSSIFCAPMemPoolIds[ i4SizingId] );
            WSSIFCAPMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
