/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wssifwlchdlr.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains the WLCHDLR Interface routines.
 *
 *****************************************************************************/
#ifndef __WSSIFWLCHDLR_C__
#define __WSSIFWLCHDLR_C__

#include "wssifinc.h"
#include "wlchdlrproto.h"
#include "iss.h"
#include "dns.h"

extern INT4         VlanGetContextInfoFromIfIndex (UINT4 u4IfIndex,
                                                   UINT4 *pu4ContextId,
                                                   UINT2 *pu2LocalPort);
static tWssIfWlcHdlrDB WssIfWlcDB;

/************************************************************************/
/*  Function Name   : wlchdlrDBInit                                     */
/*  Description     : This function initializes WLCHdlr DB              */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
wlchdlrDBInit (VOID)
{
    WssIfWlcDB.u4LocalIpAddr = 0xFFFFFFFF;
    WssIfWlcDB.u4VendorSMI = 0xFF;
    WssIfWlcDB.u1NumOfIPv4List = 0;
    WssIfWlcDB.ACIpv4list.ipAddr[0] = 0xFFFFFFFF;
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : WssIfProcessStaticNameinWlcHdlrMsg                */
/*  Description     : This routine processes the Static Name            */
/*  Input(s)        : tmpWssIfCapDB - Pointer to WssIfCapDB             */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
WssIfProcessStaticNameinWlcHdlrMsg (tWssIfCapDB * tmpWssIfCapDB)
{
    UINT1               u1NofIpAdrs = 0;
    UINT1               u1Loop = 0;
    tIPvXAddr           pResolvedIP;
    UINT4               u4Err = 0;

    MEMSET (&pResolvedIP, 0, sizeof (tIPvXAddr));

    for (u1Loop = 0; u1Loop < 3; u1Loop++)
    {
        if (tmpWssIfCapDB->CapwapGetDB.AcNameWithPri[u1Loop].u1Priority
            != OSIX_FALSE)
        {
            DNSResolveIPFromName ((UINT1 *) &tmpWssIfCapDB->CapwapGetDB.
                                  AcNameWithPri[u1Loop].wlcName, &pResolvedIP,
                                  &u4Err);
            if (u4Err != OSIX_FAILURE)
            {
                MEMCPY (&WssIfWlcDB.ACIpv4list.ipAddr[u1NofIpAdrs],
                        pResolvedIP.au1Addr, IPVX_IPV4_ADDR_LEN);
                u1NofIpAdrs++;
            }
        }
    }
    WssIfWlcDB.u1NumOfIPv4List = u1NofIpAdrs;
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : WssIfProcessWlcHdlrDBMsg                          */
/*  Description     : The function is the WlcHdlr DB Interface for      */
/*                    other WSS modules.                                */
/*  Input(s)        : u1MsgType - Message Type                          */
/*                    pWssIfWlcHdlrDB - Pointer to WlcHdlr DB           */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
WssIfProcessWlcHdlrDBMsg (UINT1 u1MsgType, tWssIfWlcHdlrDB * pWssIfWlcHdlrDB)
{
    UINT1               u1Index = 0;

    switch (u1MsgType)
    {
        case WSS_WLCHDLR_GET_LOCAL_IPV4:
            pWssIfWlcHdlrDB->u4LocalIpAddr = WssIfWlcDB.u4LocalIpAddr;
            break;
        case WSS_WLCHDLR_GET_AC_IPV4_LIST:
            pWssIfWlcHdlrDB->u1NumOfIPv4List = WssIfWlcDB.u1NumOfIPv4List;
            for (u1Index = 0; u1Index < WssIfWlcDB.u1NumOfIPv4List; u1Index++)
            {
                pWssIfWlcHdlrDB->ACIpv4list.ipAddr[u1Index] =
                    WssIfWlcDB.ACIpv4list.ipAddr[u1Index];
            }
            break;
        default:
            return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : WssIfProcessWlcHdlrMsg                            */
/*  Description     : The function is the WlcHdlr Interface for other   */
/*                    WSS modules.                                      */
/*  Input(s)        : MsgType - Message Type                            */
/*                    pWssIfMsg - Message Data                          */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
WssIfProcessWlcHdlrMsg (UINT1 MsgType, unWlcHdlrMsgStruct * pWssIfMsg)
{
    INT4                i4RetVal = 0;

    i4RetVal = WlcHdlrProcessWssIfMsg (MsgType, pWssIfMsg);
    return i4RetVal;
}

/************************************************************************/
/*  Function Name   : WssGetSessIDFromIndex                            */
/*  Description     : The function is the WlcHdlr Interface for getting*/
/*                    session Id from Index                            */
/*  Input(s)        : IfIndex , pointer to session Id                  */
/*  Output(s)       : Session Id                                       */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
WssGetSessIDFromIndex (UINT4 u4IfIndex, UINT2 *pu2SessId)
{
    tWssWlanDB          WssWlanDB;
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    WssWlanDB.WssWlanAttributeDB.u4BssIfIndex = u4IfIndex;
    WssWlanDB.WssWlanIsPresentDB.bWtpInternalId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                  &WssWlanDB) != OSIX_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfProcessWssWlanDBMsg: Bss IfIndex DB \
               Get failed, return failure index \r\n");
        return OSIX_FAILURE;
    }
    *pu2SessId = WssWlanDB.WssWlanAttributeDB.u2WtpInternalId;
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : WssIfBssidVlanMemberAction                        */
/*  Description     : The function is the interface to VLAN module for  */
/*                    adding/removing BSSID port from a VLAN            */
/*  Input(s)        : u4IfIndex - BSSID Port Index                      */
/*                    u2VlanId  - VLAN ID                               */
/*                    u1Action  - VLAN Member port action               */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
WssIfBssidVlanMemberAction (UINT4 u4IfIndex, UINT2 u2VlanId, UINT1 u1Action)
{
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    WSSIF_TRC (WSSIF_MGMT_TRC, "CALLING VLAN For BSSID MEMBER\
            ADDITION /DELTEION ##################\n");

    if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPort) !=
        VLAN_SUCCESS)
    {
        WSSIF_TRC (WSSIF_MGMT_TRC, "VlanGetContextInfoFromIfIndex : \
           Failed\n ");
        return OSIX_FAILURE;
    }
    WSSIF_TRC2 (WSSIF_MGMT_TRC,
       "\r\n ContextId = %d, LocalPort = %d", u4ContextId, u2LocalPort);

    return VlanUpdateDynamicVlanInfo (0, u2VlanId, u2LocalPort, u1Action);
}
#endif
