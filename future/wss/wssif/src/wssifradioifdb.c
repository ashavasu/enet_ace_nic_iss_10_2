/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: wssifradioifdb.c,v 1.3 2017/11/24 10:37:10 siva Exp $
 *
 * Description: This files contains the Radio module DB related functions
 ***********************************************************************/
#ifndef __WSSIFRADIOIFDB_C__
#define __WSSIFRADIOIFDB_C__

#include "wssifinc.h"
#include "radioifextn.h"

/*****************************************************************************
 *  Function     : RadioIfInit                                               *
 *                                                                           * 
 *  Description  : To Initialisae Radio DB                                   *
 *                                                                           *
 *  Input        : None                                                      *
 *                                                                           *
 *  Output       : None                                                      *
 *                                                                           *
 *  Returns      : OSIX_SUCCESS, if initialization succeeds                  *
 *                 OSIX_FAILURE, otherwise                                   *
 *                                                                           *
 *****************************************************************************/

#define CONFIG_VHTELEMENTS(vhtcap, bit, shift)  vhtcap |= (bit << shift);
#define BIT(x) (1ULL<<(x))
UINT1
RadioIfInit (VOID)
{
    WSSIF_FN_ENTRY ();

    /* Create the RB Tree */
    if (RadioIfDBCreate () == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "RadioIfInitt:Creation of RadioIf Profile DB RB Tree Failed\n");
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    if (WssifradioSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "RadioIfInit:Creation of MemPool Failed\n");
        lrInitComplete (OSIX_FAILURE);
        return OSIX_FAILURE;
    }

    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 *  Function    :  RadioIfDBRBCmp
 *  Input       :  RBTREE Two Nodes 
 *  Description :  Compares index of two nodes 
 *  Output      :  None
 *  Returns     :  0 or 1
 *****************************************************************************/

INT4
RadioIfDBRBCmp (tRBElem * e1, tRBElem * e2)
{
    tRadioIfDB         *pNode1 = e1;
    tRadioIfDB         *pNode2 = e2;

    WSSIF_FN_ENTRY ();

    if (pNode1->u4VirtualRadioIfIndex > pNode2->u4VirtualRadioIfIndex)
    {
        return 1;
    }
    else if (pNode1->u4VirtualRadioIfIndex < pNode2->u4VirtualRadioIfIndex)
    {
        return -1;
    }

    WSSIF_FN_EXIT ();
    return 0;
}

/****************************************************************************
 *  Function    :  RadioIfDBCreate
 *  Input       :  None
 *  Description :  This function creates the RadioIfDB
 *                 i.e. a RBTree.
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/

PUBLIC UINT1
RadioIfDBCreate ()
{
    UINT4               u4RBNodeOffset = 0;

    WSSIF_FN_ENTRY ();

    u4RBNodeOffset = FSAP_OFFSETOF (tRadioIfDB, RadioIfIndexDBNode);

    if ((gRadioIfGlobals.RadioIfGlbMib.RadioIfDB =
         RBTreeCreateEmbedded (u4RBNodeOffset, RadioIfDBRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }

    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 *  Function    : WssIfGetDot11Frequency
 *  Input    :  RadioIfIndex
 *  Description :  This function gets the TX power from database
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
WssIfGetDot11Frequency (UINT4 u4RadioIndex, UINT1 *pu1Dot11Frequency)
{
    UINT1               u1retVal;
    tRadioIfGetDB       pRadioIfGetDB;
    pRadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIndex;
    pRadioIfGetDB.RadioIfIsGetAllDB.bCurrentFrequency = OSIX_TRUE;
    u1retVal = WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &pRadioIfGetDB);
    if (u1retVal != OSIX_FAILURE)
    {
        *pu1Dot11Frequency = pRadioIfGetDB.RadioIfGetAllDB.u1CurrentFrequency;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *  Function    : WssIfGetDot11FragThreshold
 *  Input    :  RadioIfIndex
 *  Description :  This function gets the TX power from database
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
WssIfGetDot11FragThreshold (UINT4 u4RadioIndex, UINT2 *pu2FragThreshold)
{
    UINT1               u1retVal;
    tRadioIfGetDB       pRadioIfGetDB;
    pRadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIndex;
    pRadioIfGetDB.RadioIfIsGetAllDB.bFragmentationThreshold = OSIX_TRUE;
    u1retVal = WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &pRadioIfGetDB);
    if (u1retVal != OSIX_FAILURE)
    {
        *pu2FragThreshold =
            (INT4) pRadioIfGetDB.RadioIfGetAllDB.u2FragmentationThreshold;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *  Function    : WssIfGetDot11RTSThreshold
 *  Input    :  RadioIfIndex
 *  Description :  This function gets the TX power from database
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
WssIfGetDot11RTSThreshold (UINT4 u4RadioIndex, UINT2 *pu2RTSThreshold)
{
    UINT1               u1retVal;
    tRadioIfGetDB       pRadioIfGetDB;
    pRadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIndex;
    pRadioIfGetDB.RadioIfIsGetAllDB.bRTSThreshold = OSIX_TRUE;

    u1retVal = WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &pRadioIfGetDB);
    if (u1retVal != OSIX_FAILURE)
    {
        *pu2RTSThreshold = pRadioIfGetDB.RadioIfGetAllDB.u2RTSThreshold;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *  Function    : WssIfGetDot11Country
 *  Input    :  RadioIfIndex
 *  Description :  This function gets the TX power from database
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
WssIfGetDot11Country (UINT4 u4RadioIndex, UINT1 *pu1CountryName)
{
    UINT1               u1retVal;
    tRadioIfGetDB       pRadioIfGetDB;
    pRadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIndex;
    pRadioIfGetDB.RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;
    u1retVal = WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &pRadioIfGetDB);
    if (u1retVal != OSIX_FAILURE)
    {
        MEMCPY (pu1CountryName, pRadioIfGetDB.RadioIfGetAllDB.au1CountryString,
                sizeof (pRadioIfGetDB.RadioIfGetAllDB.au1CountryString));
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *  Function    : WssIfGetAntennaDiversity
 *  *  Input    :  RadioIndex
 *  Description :  This function gets the TX power from database
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
WssIfGetAntennaDiversity (UINT4 u4RadioIndex, UINT4 *pu4AntennaDiversity)
{
    UINT1               u1retVal;
    tRadioIfGetDB       pRadioIfGetDB;
    pRadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIndex;
    pRadioIfGetDB.RadioIfIsGetAllDB.bAntennaMode = OSIX_TRUE;
    u1retVal = WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &pRadioIfGetDB);
    if (u1retVal != OSIX_FAILURE)
    {
        *pu4AntennaDiversity = pRadioIfGetDB.RadioIfGetAllDB.u1AntennaMode;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *  Function    :  wsswlangetBeaconInterval
 *  Input       :  radioindex
 *  Description :  This function gets the Beacon interval from database
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
WssIfGetBeaconInterval (UINT4 u4RadioIndex, BOOL1 bFlag, UINT4 *pu4BeaconItrval)
{
    UINT1               u1retVal;
    tRadioIfGetDB       pRadioIfGetDB;
    pRadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIndex;
    pRadioIfGetDB.RadioIfIsGetAllDB.bBeaconPeriod = bFlag;
    u1retVal = WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &pRadioIfGetDB);
    if (u1retVal != OSIX_FAILURE)
    {
        *pu4BeaconItrval = (UINT4) pRadioIfGetDB.RadioIfGetAllDB.u2BeaconPeriod;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *  Function    :  wsswlangetMaxpower
 *  Input       :  radioindex
 *  Description :  This function gets the MAX power from database
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
WssIfGetMaxpower (UINT4 u4RadioIndex, BOOL1 bFlag, UINT4 *pu4PwrLevel)
{
    UINT1               u1retVal;
    tRadioIfGetDB       pRadioIfGetDB;
    pRadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIndex;
    pRadioIfGetDB.RadioIfIsGetAllDB.bMaxTxPowerLevel = bFlag;
    u1retVal = WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &pRadioIfGetDB);
    if (u1retVal != OSIX_FAILURE)
    {
        *pu4PwrLevel = (UINT4) pRadioIfGetDB.RadioIfGetAllDB.u2MaxTxPowerLevel;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *  Function    :  wsswlanGetTxpower
 *  Input       :  radioindex
 *  Description :  This function gets the TX power from database
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
WssIfGetTxpower (UINT4 u4RadioIndex, BOOL1 bFlag, INT4 *pi4PwrLevel)
{
    UINT1               u1retVal;
    tRadioIfGetDB       pRadioIfGetDB;
    pRadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIndex;
    pRadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPowerLevel = bFlag;
    u1retVal = WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &pRadioIfGetDB);
    if (u1retVal != OSIX_FAILURE)
    {
        *pi4PwrLevel =
            (INT4) pRadioIfGetDB.RadioIfGetAllDB.u2CurrentTxPowerLevel;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *  Function    :  wsswlanGetAifs
 *  Input       :  radioindex
 *                 EDCATbleIndex
 *  Description :  This function gets AIFS for each table index from database
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
WssIfGetAifs (UINT4 u4RadioIndex, BOOL1 bFlag, INT4 *pi4ValDot11EDCATbleAifs,
              INT4 i4EDCATbleIndex)
{
    UINT1               u1retval;
    tRadioIfGetDB       pRadioIfGetDB;
    pRadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIndex;
    pRadioIfGetDB.RadioIfIsGetAllDB.bAifsn = bFlag;
    u1retval =
        WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_QOS_CONFIG_DB, &pRadioIfGetDB);
    if (u1retval != OSIX_FAILURE)
    {
        *pi4ValDot11EDCATbleAifs =
            (INT4) pRadioIfGetDB.RadioIfGetAllDB.au1Aifsn[i4EDCATbleIndex];
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *  Function    :  wsswlangetcwmin
 *  Input       :  radioindex
 *                 EDCATbleIndex
 *  Description :  This function gets cwmin for each table index from database
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
WssIfGetCwmin (UINT4 u4RadioIndex, BOOL1 bFlag, INT4 *i4ValEDCATbleCWmin,
               INT4 i4EDCATbleIndex)
{
    UINT1               u1retval;
    tRadioIfGetDB       pRadioIfGetDB;
    pRadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIndex;
    pRadioIfGetDB.RadioIfIsGetAllDB.bCwMin = bFlag;
    u1retval =
        WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_QOS_CONFIG_DB, &pRadioIfGetDB);
    if (u1retval != OSIX_FAILURE)
    {
        *i4ValEDCATbleCWmin =
            (INT4) pRadioIfGetDB.RadioIfGetAllDB.au2CwMin[i4EDCATbleIndex];
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *  Function    :  wsswlangetcwmax
 *  Input       :  radioindex
 *                 EDCATbleIndex
 *  Description :  This function gets cwmax for each table index from database
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
WssIfGetCwmax (UINT4 u4RadioIndex, BOOL1 bFlag, INT4 *i4ValEDCATbleCWmax,
               INT4 i4EDCATbleIndex)
{
    UINT1               u1retval;
    tRadioIfGetDB       pRadioIfGetDB;
    pRadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIndex;
    pRadioIfGetDB.RadioIfIsGetAllDB.bCwMax = bFlag;
    u1retval =
        WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_QOS_CONFIG_DB, &pRadioIfGetDB);
    if (u1retval != OSIX_FAILURE)
    {
        *i4ValEDCATbleCWmax =
            (INT4) pRadioIfGetDB.RadioIfGetAllDB.au2CwMax[i4EDCATbleIndex];
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *  Function    :  wsswlangetChannel
 *  Input       :  radioindex
 *  Description :  This function gets the channel from database
 *  Output      :  None
 *  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
WssIfGetChannel (UINT4 u4RadioIndex, BOOL1 bFlag, UINT2 *u2channel)
{
    UINT1               u1retVal;
    tRadioIfGetDB       pRadioIfGetDB;
    pRadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIndex;
    pRadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = bFlag;
    u1retVal = WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &pRadioIfGetDB);
    if (u1retVal != OSIX_FAILURE)
    {
        *u2channel = (UINT2) pRadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/*****************************************************************************
 * Function Name      : WssIfProcessRadioIfDBMsg                             *
 *                                                                           *
 * Description        : Function to create, update and delete the Radio param*
 *                                                                           *
 * Input(s)           : Opcode - To select the particular task               *
 *                      pRadioIfGetDB - pointer the input structure          *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *****************************************************************************/
UINT1
WssIfProcessRadioIfDBMsg (UINT1 u1OpCode, tRadioIfGetDB * pRadioIfGetDB)
{
    tRadioIfDB         *pGetRadioIfDB = NULL;
    tRadioIfDB          RadioIfDB;
    tRadioIfDB         *pAddRadioIfDB = NULL;
    tRadioIfDB         *pRemRadioIfDB = NULL;
    UINT1               u1QosIndex = 0;
    UINT1               u1SetQosIndex = 0;
    UINT1               u1WlanId = 0;
    UINT1               u1Index = 0;
    UINT1               u1index = 0;
    UINT1               au1CountryString[RADIO_COUNTRY_LENGTH];
    UINT1               u1ACBit = 0;

    WSSIF_FN_ENTRY ();

    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));
    MEMSET (au1CountryString, 0, RADIO_COUNTRY_LENGTH);

    if (pRadioIfGetDB == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfProcessRadioIfDBMsg: Input validation failed\n");
        return OSIX_FAILURE;
    }

    WSSIF_LOCK;
    switch (u1OpCode)
    {
        case WSS_SET_RADIO_QOS_CONFIG_DB_WLC:

            RadioIfDB.u4VirtualRadioIfIndex
                = pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

            /* Get the corresponding node from RB Tree (RadioIfDb) */
            pGetRadioIfDB =
                RBTreeGet (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                           (tRBElem *) (&RadioIfDB));

            if (pGetRadioIfDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIfDBMsg: Input Validation failed for \
                        QOS params\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bQosIndex == OSIX_FALSE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIfDBMsg: QoS Index missing, return failure\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            u1SetQosIndex =
                (UINT1) (pRadioIfGetDB->RadioIfGetAllDB.u1QosIndex - 1);
            u1QosIndex = pRadioIfGetDB->RadioIfGetAllDB.u1QosIndex;

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bQueueDepth == OSIX_TRUE)
            {
                pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1SetQosIndex].
                    u1QueueDepth =
                    pRadioIfGetDB->RadioIfGetAllDB.au1QueueDepth[u1QosIndex];
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCwMin == OSIX_TRUE)
            {
                pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1SetQosIndex].u2CwMin =
                    pRadioIfGetDB->RadioIfGetAllDB.au2CwMin[u1QosIndex];
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCwMax == OSIX_TRUE)
            {
                pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1SetQosIndex].u2CwMax =
                    pRadioIfGetDB->RadioIfGetAllDB.au2CwMax[u1QosIndex];
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAifsn == OSIX_TRUE)
            {
                pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1SetQosIndex].u1Aifsn =
                    pRadioIfGetDB->RadioIfGetAllDB.au1Aifsn[u1QosIndex];
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxOpLimit == OSIX_TRUE)
            {
                pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1SetQosIndex].
                    u2TxOpLimit =
                    pRadioIfGetDB->RadioIfGetAllDB.au2TxOpLimit[u1QosIndex];
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bQosPrio == OSIX_TRUE)
            {
                pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1SetQosIndex].
                    u1QosPrio =
                    pRadioIfGetDB->RadioIfGetAllDB.au1QosPrio[u1QosIndex];
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bDscp == OSIX_TRUE)
            {
                pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1SetQosIndex].u1Dscp =
                    pRadioIfGetDB->RadioIfGetAllDB.au1Dscp[u1QosIndex];
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTaggingPolicy == OSIX_TRUE)
            {
                pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1SetQosIndex].
                    u1TaggingPolicy =
                    pRadioIfGetDB->RadioIfGetAllDB.u1TaggingPolicy;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAdmissionControl == OSIX_TRUE)
            {
                pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1SetQosIndex].
                    u1AdmissionControl =
                    pRadioIfGetDB->
                    RadioIfGetAllDB.au1AdmissionControl[u1QosIndex];
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bWtpInternalId == OSIX_TRUE)
            {
                pGetRadioIfDB->u2WtpInternalId =
                    pRadioIfGetDB->RadioIfGetAllDB.u2WtpInternalId;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioId == OSIX_TRUE)
            {
                pGetRadioIfDB->u1RadioId =
                    pRadioIfGetDB->RadioIfGetAllDB.u1RadioId;
            }
            break;
        case WSS_SET_RADIO_QOS_CONFIG_DB_WTP:

            RadioIfDB.u4VirtualRadioIfIndex =
                pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

            /* Get the corresponding node from RB Tree (RadioIfDb) */
            pGetRadioIfDB =
                RBTreeGet (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                           (tRBElem *) (&RadioIfDB));

            if (pGetRadioIfDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIfDBMsg: Input Validation Failed for \
                        Qos Config\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bQosIndex == OSIX_FALSE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIfDBMsg: QoS Index missing, return failure\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            u1SetQosIndex = (pRadioIfGetDB->RadioIfGetAllDB.u1QosIndex);
            u1QosIndex = pRadioIfGetDB->RadioIfGetAllDB.u1QosIndex;
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bQueueDepth == OSIX_TRUE)
            {
                pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1SetQosIndex].
                    u1QueueDepth =
                    pRadioIfGetDB->RadioIfGetAllDB.au1QueueDepth[u1QosIndex];
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCwMin == OSIX_TRUE)
            {
                pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1SetQosIndex].u2CwMin =
                    pRadioIfGetDB->RadioIfGetAllDB.au2CwMin[u1QosIndex];
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCwMax == OSIX_TRUE)
            {
                pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1SetQosIndex].u2CwMax =
                    pRadioIfGetDB->RadioIfGetAllDB.au2CwMax[u1QosIndex];
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAifsn == OSIX_TRUE)
            {
                pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1SetQosIndex].u1Aifsn =
                    pRadioIfGetDB->RadioIfGetAllDB.au1Aifsn[u1QosIndex];
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxOpLimit == OSIX_TRUE)
            {
                pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1SetQosIndex].
                    u2TxOpLimit =
                    pRadioIfGetDB->RadioIfGetAllDB.au2TxOpLimit[u1QosIndex];
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bQosPrio == OSIX_TRUE)
            {
                pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1SetQosIndex].
                    u1QosPrio =
                    pRadioIfGetDB->RadioIfGetAllDB.au1QosPrio[u1QosIndex];
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bDscp == OSIX_TRUE)
            {
                pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1SetQosIndex].u1Dscp =
                    pRadioIfGetDB->RadioIfGetAllDB.au1Dscp[u1QosIndex];
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTaggingPolicy == OSIX_TRUE)
            {
                pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1SetQosIndex].
                    u1TaggingPolicy =
                    pRadioIfGetDB->RadioIfGetAllDB.u1TaggingPolicy;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAdmissionControl == OSIX_TRUE)
            {
                pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1SetQosIndex].
                    u1AdmissionControl =
                    pRadioIfGetDB->
                    RadioIfGetAllDB.au1AdmissionControl[u1QosIndex];
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bWtpInternalId == OSIX_TRUE)
            {
                pGetRadioIfDB->u2WtpInternalId =
                    pRadioIfGetDB->RadioIfGetAllDB.u2WtpInternalId;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioId == OSIX_TRUE)
            {
                pGetRadioIfDB->u1RadioId =
                    pRadioIfGetDB->RadioIfGetAllDB.u1RadioId;
            }
            break;

        case WSS_SET_PHY_RADIO_IF_DB:
            pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex =
                pRadioIfGetDB->RadioIfGetAllDB.u1RadioId +
                (UINT4) SYS_DEF_MAX_ENET_INTERFACES;

            if (pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex >
                SYS_DEF_MAX_PHYSICAL_INTERFACES)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIfDBMsg: Invalid index, set failed\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            /* Fallthrough */
        case WSS_SET_RADIO_IF_DB:

            RadioIfDB.u4VirtualRadioIfIndex =
                pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

            /* Get the corresponding node from RB Tree (RadioIfDb) */
            pGetRadioIfDB =
                RBTreeGet (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                           (tRBElem *) (&RadioIfDB));

            if (pGetRadioIfDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIfDBMsg: Entry not exist, set failed\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            /* Copying the values , which are needed */
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bDTIMPeriod == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u2DtimPeriod =
                    pRadioIfGetDB->RadioIfGetAllDB.u1DTIMPeriod;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bWtpInternalId == OSIX_TRUE)
            {
                pGetRadioIfDB->u2WtpInternalId =
                    pRadioIfGetDB->RadioIfGetAllDB.u2WtpInternalId;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxPowerLevel
                == OSIX_TRUE)
            {
                pGetRadioIfDB->u2CurrentTxPowerLevel =
                    pRadioIfGetDB->RadioIfGetAllDB.u2CurrentTxPowerLevel;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxPower == OSIX_TRUE)
            {
                pGetRadioIfDB->i2CurrentTxPower =
                    pRadioIfGetDB->RadioIfGetAllDB.i2CurrentTxPower;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioId == OSIX_TRUE)
            {
                pGetRadioIfDB->u1RadioId =
                    pRadioIfGetDB->RadioIfGetAllDB.u1RadioId;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bBindingType == OSIX_TRUE)
            {
                pGetRadioIfDB->u1BindingType =
                    pRadioIfGetDB->RadioIfGetAllDB.u1BindingType;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAdminStatus == OSIX_TRUE)
            {
                pGetRadioIfDB->u1AdminStatus =
                    pRadioIfGetDB->RadioIfGetAllDB.u1AdminStatus;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bOperStatus == OSIX_TRUE)
            {
                pGetRadioIfDB->u1OperStatus =
                    pRadioIfGetDB->RadioIfGetAllDB.u1OperStatus;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bIfType == OSIX_TRUE)
            {
                pGetRadioIfDB->u1IfType =
                    pRadioIfGetDB->RadioIfGetAllDB.u1IfType;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bBssIdCount == OSIX_TRUE)
            {
                pGetRadioIfDB->u1BssIdCount =
                    pRadioIfGetDB->RadioIfGetAllDB.u1BssIdCount;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioAntennaType == OSIX_TRUE)
            {
                pGetRadioIfDB->u1RadioAntennaType =
                    pRadioIfGetDB->RadioIfGetAllDB.u1RadioAntennaType;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bFailureStatus == OSIX_TRUE)
            {
                pGetRadioIfDB->u1FailureStatus =
                    pRadioIfGetDB->RadioIfGetAllDB.u1FailureStatus;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMacAddr == OSIX_TRUE)
            {
                MEMCPY ((pGetRadioIfDB->MacAddr),
                        (pRadioIfGetDB->RadioIfGetAllDB.MacAddr),
                        sizeof (pRadioIfGetDB->RadioIfGetAllDB.MacAddr));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bFirstChannelNumber
                == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfChannelInfo.u2FirstChannelNumber =
                    pRadioIfGetDB->RadioIfGetAllDB.u2FirstChannelNumber;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bNoOfChannels == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfChannelInfo.u2NoOfChannels =
                    pRadioIfGetDB->RadioIfGetAllDB.u2NoOfChannels;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxTxPowerLevel == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfChannelInfo.u2MaxTxPowerLevel =
                    pRadioIfGetDB->RadioIfGetAllDB.u2MaxTxPowerLevel;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSupportedBand == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfChannelInfo.u1SupportedBand =
                    pRadioIfGetDB->RadioIfGetAllDB.u1SupportedBand;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bOFDMChannelWidth == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfChannelInfo.u4OFDMChannelWidth =
                    pRadioIfGetDB->RadioIfGetAllDB.u4OFDMChannelWidth;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRssiThreshold == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfChannelInfo.i4RssiThreshold =
                    pRadioIfGetDB->RadioIfGetAllDB.i4RssiThreshold;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bEDThreshold == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfChannelInfo.i4EDThreshold =
                    pRadioIfGetDB->RadioIfGetAllDB.i4EDThreshold;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentCCAMode == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfChannelInfo.u1CurrentCCAMode =
                    pRadioIfGetDB->RadioIfGetAllDB.u1CurrentCCAMode;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentChannel == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfChannelInfo.u1CurrentChannel =
                    pRadioIfGetDB->RadioIfGetAllDB.u1CurrentChannel;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentFrequency == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfChannelInfo.u1CurrentFrequency
                    = pRadioIfGetDB->RadioIfGetAllDB.u1CurrentFrequency;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bNoOfSupportedPowerLevels
                == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfChannelInfo.u1NoOfSupportedPowerLevels
                    = pRadioIfGetDB->RadioIfGetAllDB.u1NoOfSupportedPowerLevels;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxPowerLevel == OSIX_TRUE)
            {
                MEMCPY ((pGetRadioIfDB->RadioIfChannelInfo.au2TxPowerLevel),
                        (pRadioIfGetDB->RadioIfGetAllDB.au2TxPowerLevel),
                        sizeof (pRadioIfGetDB->
                                RadioIfGetAllDB.au2TxPowerLevel));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bDiversitySupport == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1DiversitySupport =
                    pRadioIfGetDB->RadioIfGetAllDB.u1DiversitySupport;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAntennaMode == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1AntennaMode =
                    pRadioIfGetDB->RadioIfGetAllDB.u1AntennaMode;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxAntenna == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1CurrentTxAntenna =
                    pRadioIfGetDB->RadioIfGetAllDB.u1CurrentTxAntenna;
            }

            for (u1index = 0; u1index <
                 pRadioIfGetDB->RadioIfGetAllDB.u1CurrentTxAntenna; u1index++)
            {
                if (pRadioIfGetDB->
                    RadioIfIsGetAllDB.bAntennaSelection[u1index] == OSIX_TRUE)
                {
                    pGetRadioIfDB->
                        RadioIfConfigParam.au1AntennaSelection[u1index] =
                        pRadioIfGetDB->
                        RadioIfGetAllDB.pu1AntennaSelection[u1index];
                }
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRTSThreshold == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u2RTSThreshold =
                    pRadioIfGetDB->RadioIfGetAllDB.u2RTSThreshold;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bT1Threshold == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfChannelInfo.u4T1Threshold =
                    pRadioIfGetDB->RadioIfGetAllDB.u4T1Threshold;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bShortRetry == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1ShortRetry =
                    pRadioIfGetDB->RadioIfGetAllDB.u1ShortRetry;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bLongRetry == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1LongRetry =
                    pRadioIfGetDB->RadioIfGetAllDB.u1LongRetry;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bFragmentationThreshold
                == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u2FragmentationThreshold
                    = pRadioIfGetDB->RadioIfGetAllDB.u2FragmentationThreshold;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxMsduLifetime == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u4TxMsduLifetime
                    = pRadioIfGetDB->RadioIfGetAllDB.u4TxMsduLifetime;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxMsduLifetime == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u4RxMsduLifetime
                    = pRadioIfGetDB->RadioIfGetAllDB.u4RxMsduLifetime;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bBeaconPeriod == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u2BeaconPeriod
                    = pRadioIfGetDB->RadioIfGetAllDB.u2BeaconPeriod;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bDot11RadioType == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfChannelInfo.i4EDThreshold =
                    RADIOIF_DEF_ENERGY_THRESHOLD;
                pGetRadioIfDB->RadioIfChannelInfo.u4T1Threshold =
                    RADIOIF_DEF_ENERGY_THRESHOLD;
                pGetRadioIfDB->RadioIfConfigParam.u4Dot11RadioType =
                    pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType;
#ifdef WLC_WANTED
                if ((pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType ==
                     RADIO_TYPE_BGN)
                    || (pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType ==
                        RADIO_TYPE_AN)
                    || (pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType ==
                        RADIO_TYPE_AC))

                {
                    WssIfSetDefault11nParams (pGetRadioIfDB);
                }
                if (pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_AC)
                {
                    WssIfSetDefault11AcParams (pGetRadioIfDB);
                }
#endif
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bShortPreamble == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1ShortPreamble =
                    pRadioIfGetDB->RadioIfGetAllDB.u1ShortPreamble;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bNoOfBssId == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1NoOfBssId =
                    pRadioIfGetDB->RadioIfGetAllDB.u1NoOfBssId;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bProbeInclude == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1ProbeInclude =
                    pRadioIfGetDB->RadioIfGetAllDB.u1ProbeInclude;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bBeaconInclude == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1BeaconInclude =
                    pRadioIfGetDB->RadioIfGetAllDB.u1BeaconInclude;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bBPFlag == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1BPFlag =
                    pRadioIfGetDB->RadioIfGetAllDB.u1BPFlag;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHtFlag == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1HtFlag =
                    pRadioIfGetDB->RadioIfGetAllDB.u1HtFlag;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxSuppMCS == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1MaxSuppMCS =
                    pRadioIfGetDB->RadioIfGetAllDB.u1MaxSuppMCS;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxManMCS == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1MaxManMCS =
                    pRadioIfGetDB->RadioIfGetAllDB.u1MaxManMCS;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxAntenna == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1TxAntenna =
                    pRadioIfGetDB->RadioIfGetAllDB.u1TxAntenna;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxAntenna == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1RxAntenna =
                    pRadioIfGetDB->RadioIfGetAllDB.u1RxAntenna;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxMpduInAmpdu == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1MaxMpduInAmpdu =
                    pRadioIfGetDB->RadioIfGetAllDB.u1MaxMpduInAmpdu;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxMsdu == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1MaxMsdu =
                    pRadioIfGetDB->RadioIfGetAllDB.u1MaxMsdu;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHTMaxAmpduLengthExponent
                == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1HTMaxAmpduLengthExponent =
                    pRadioIfGetDB->RadioIfGetAllDB.u1HTMaxAmpduLengthExponent;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVHTMaxAmpduLengthExponent
                == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1VHTMaxAmpduLengthExponent =
                    pRadioIfGetDB->RadioIfGetAllDB.u1VHTMaxAmpduLengthExponent;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHTOption == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1HTOption =
                    pRadioIfGetDB->RadioIfGetAllDB.u1HTOption;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCountryString == OSIX_TRUE)
            {
                MEMCPY ((pGetRadioIfDB->RadioIfConfigParam.au1CountryString),
                        (pRadioIfGetDB->RadioIfGetAllDB.au1CountryString),
                        sizeof (pRadioIfGetDB->
                                RadioIfGetAllDB.au1CountryString));
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMultiDomainInfo == OSIX_TRUE)
            {
                MEMCPY ((pGetRadioIfDB->RadioIfConfigParam.MultiDomainInfo),
                        (pRadioIfGetDB->RadioIfGetAllDB.MultiDomainInfo),
                        sizeof (pRadioIfGetDB->
                                RadioIfGetAllDB.MultiDomainInfo));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bOperationalRate == OSIX_TRUE)
            {
                MEMCPY ((pGetRadioIfDB->RadioIfConfigParam.au1OperationalRate),
                        (pRadioIfGetDB->RadioIfGetAllDB.au1OperationalRate),
                        sizeof (pRadioIfGetDB->
                                RadioIfGetAllDB.au1OperationalRate));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSupportedRate == OSIX_TRUE)
            {
                MEMCPY ((pGetRadioIfDB->RadioIfConfigParam.au1SupportedRate),
                        (pRadioIfGetDB->RadioIfGetAllDB.au1SupportedRate),
                        sizeof (pRadioIfGetDB->
                                RadioIfGetAllDB.au1SupportedRate));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMCSRateSet == OSIX_TRUE)
            {
                MEMCPY ((pGetRadioIfDB->RadioIfConfigParam.au1MCSRateSet),
                        (pRadioIfGetDB->RadioIfGetAllDB.au1MCSRateSet),
                        sizeof (pRadioIfGetDB->RadioIfGetAllDB.au1MCSRateSet));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHTCapInfo == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u2HTCapInfo =
                    pRadioIfGetDB->RadioIfGetAllDB.u2HTCapInfo;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAMPDUParam == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1AMPDUParam =
                    pRadioIfGetDB->RadioIfGetAllDB.u1AMPDUParam;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHTInfoSecChanAbove ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1HTInfoSecChanAbove =
                    pRadioIfGetDB->RadioIfGetAllDB.u1HTInfoSecChanAbove;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHTInfoSecChanBelow ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1HTInfoSecChanBelow =
                    pRadioIfGetDB->RadioIfGetAllDB.u1HTInfoSecChanBelow;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxSupportMcsSet == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1TxSupportMcsSet =
                    pRadioIfGetDB->RadioIfGetAllDB.u1TxSupportMcsSet;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMCSSet == OSIX_TRUE)
            {
                MEMCPY ((pGetRadioIfDB->RadioIfConfigParam.au1SuppMCSSet),
                        (pRadioIfGetDB->RadioIfGetAllDB.au1SuppMCSSet),
                        sizeof (pGetRadioIfDB->
                                RadioIfConfigParam.au1SuppMCSSet));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bManMCSSet == OSIX_TRUE)
            {
                MEMCPY ((pGetRadioIfDB->RadioIfConfigParam.au1ManMCSSet),
                        (pRadioIfGetDB->RadioIfGetAllDB.au1ManMCSSet),
                        sizeof (pGetRadioIfDB->
                                RadioIfConfigParam.au1ManMCSSet));
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioStats == OSIX_TRUE)
            {
                MEMCPY (&(pGetRadioIfDB->radioStats),
                        &(pRadioIfGetDB->RadioIfGetAllDB.radioStats),
                        sizeof (pGetRadioIfDB->radioStats));
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bReportInterval == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u2ReportInterval =
                    pRadioIfGetDB->RadioIfGetAllDB.u2ReportInterval;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxClientCount == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u4MaxClientCount =
                    pRadioIfGetDB->RadioIfGetAllDB.u4MaxClientCount;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioCounters == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u4RadioInOctets =
                    pRadioIfGetDB->RadioIfGetAllDB.u4RadioInOctets;
                pGetRadioIfDB->RadioIfConfigParam.u4RadioOutOctets =
                    pRadioIfGetDB->RadioIfGetAllDB.u4RadioOutOctets;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRadioType == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u4SuppRadioType =
                    pRadioIfGetDB->RadioIfGetAllDB.u4SuppRadioType;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAllowChannelWidth ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1AllowChannelWidth =
                    pRadioIfGetDB->RadioIfGetAllDB.u1AllowChannelWidth;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCapability == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u2Capability =
                    pRadioIfGetDB->RadioIfGetAllDB.u2Capability;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadarFound == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1RadarFound =
                    pRadioIfGetDB->RadioIfGetAllDB.u1RadarFound;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bChanlSwitStatus == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1ChanlSwitStatus =
                    pRadioIfGetDB->RadioIfGetAllDB.u1ChanlSwitStatus;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.b11hDfsStatus == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u111hDfsStatus =
                    pRadioIfGetDB->RadioIfGetAllDB.u111hDfsStatus;
            }
            break;

        case WSS_GET_PHY_RADIO_IF_DB:
            pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex =
                pRadioIfGetDB->RadioIfGetAllDB.u1RadioId +
                (UINT4) SYS_DEF_MAX_ENET_INTERFACES;

            if (pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex >
                SYS_DEF_MAX_PHYSICAL_INTERFACES)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIfDBMsg : Invalid index, get failed\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            /* Fallthrough */

        case WSS_GET_RADIO_IF_DB:

            RadioIfDB.u4VirtualRadioIfIndex =
                pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

            /* Get the corresponding node from RB Tree (RadioIfDb) */
            pGetRadioIfDB =
                RBTreeGet (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                           (tRBElem *) (&RadioIfDB));

            if (pGetRadioIfDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIfDBMsg : Entry not found, get failed \n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            /* Copying the values , which are needed */

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioIfIndex == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex =
                    pGetRadioIfDB->u4VirtualRadioIfIndex;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bWtpInternalId == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u2WtpInternalId =
                    pGetRadioIfDB->u2WtpInternalId;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxPowerLevel
                == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u2CurrentTxPowerLevel =
                    pGetRadioIfDB->u2CurrentTxPowerLevel;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxPower == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.i2CurrentTxPower =
                    pGetRadioIfDB->i2CurrentTxPower;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioId == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1RadioId =
                    pGetRadioIfDB->u1RadioId;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bBindingType == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1BindingType =
                    pGetRadioIfDB->u1BindingType;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAdminStatus == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1AdminStatus =
                    pGetRadioIfDB->u1AdminStatus;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bOperStatus == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1OperStatus =
                    pGetRadioIfDB->u1OperStatus;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bIfType == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1IfType =
                    pGetRadioIfDB->u1IfType;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bBssIdCount == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1BssIdCount =
                    pGetRadioIfDB->u1BssIdCount;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioAntennaType == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1RadioAntennaType =
                    pGetRadioIfDB->u1RadioAntennaType;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bFailureStatus == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1FailureStatus =
                    pGetRadioIfDB->u1FailureStatus;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bDTIMPeriod == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1DTIMPeriod =
                    (UINT1) (pGetRadioIfDB->RadioIfConfigParam.u2DtimPeriod);
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMacAddr == OSIX_TRUE)
            {
                MEMCPY ((pRadioIfGetDB->RadioIfGetAllDB.MacAddr),
                        (pGetRadioIfDB->MacAddr),
                        sizeof (pGetRadioIfDB->MacAddr));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bFirstChannelNumber
                == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u2FirstChannelNumber =
                    pGetRadioIfDB->RadioIfChannelInfo.u2FirstChannelNumber;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bNoOfChannels == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u2NoOfChannels =
                    pGetRadioIfDB->RadioIfChannelInfo.u2NoOfChannels;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxTxPowerLevel == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u2MaxTxPowerLevel =
                    pGetRadioIfDB->RadioIfChannelInfo.u2MaxTxPowerLevel;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSupportedBand == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1SupportedBand =
                    pGetRadioIfDB->RadioIfChannelInfo.u1SupportedBand;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bOFDMChannelWidth == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u4OFDMChannelWidth =
                    pGetRadioIfDB->RadioIfChannelInfo.u4OFDMChannelWidth;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRssiThreshold == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.i4RssiThreshold =
                    pGetRadioIfDB->RadioIfChannelInfo.i4RssiThreshold;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bEDThreshold == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.i4EDThreshold =
                    pGetRadioIfDB->RadioIfChannelInfo.i4EDThreshold;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentCCAMode == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1CurrentCCAMode =
                    pGetRadioIfDB->RadioIfChannelInfo.u1CurrentCCAMode;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentChannel == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1CurrentChannel =
                    pGetRadioIfDB->RadioIfChannelInfo.u1CurrentChannel;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentFrequency == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1CurrentFrequency =
                    pGetRadioIfDB->RadioIfChannelInfo.u1CurrentFrequency;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bNoOfSupportedPowerLevels
                == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1NoOfSupportedPowerLevels =
                    pGetRadioIfDB->
                    RadioIfChannelInfo.u1NoOfSupportedPowerLevels;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxPowerLevel == OSIX_TRUE)
            {
                MEMCPY ((pRadioIfGetDB->RadioIfGetAllDB.au2TxPowerLevel),
                        (pGetRadioIfDB->RadioIfChannelInfo.au2TxPowerLevel),
                        sizeof (pGetRadioIfDB->
                                RadioIfChannelInfo.au2TxPowerLevel));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bDiversitySupport == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1DiversitySupport =
                    pGetRadioIfDB->RadioIfConfigParam.u1DiversitySupport;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAntennaMode == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1AntennaMode =
                    pGetRadioIfDB->RadioIfConfigParam.u1AntennaMode;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCurrentTxAntenna == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1CurrentTxAntenna =
                    pGetRadioIfDB->RadioIfConfigParam.u1CurrentTxAntenna;
            }

            for (u1Index = 0; u1Index <
                 pGetRadioIfDB->RadioIfConfigParam.u1CurrentTxAntenna;
                 u1Index++)
            {
                if (pRadioIfGetDB->
                    RadioIfIsGetAllDB.bAntennaSelection[u1Index] == OSIX_TRUE)
                {
                    if ((pGetRadioIfDB->
                         RadioIfConfigParam.au1AntennaSelection[u1Index] == 1)
                        || (pGetRadioIfDB->
                            RadioIfConfigParam.au1AntennaSelection[u1Index] ==
                            2))
                    {
                        pRadioIfGetDB->
                            RadioIfGetAllDB.pu1AntennaSelection[u1Index] =
                            pGetRadioIfDB->
                            RadioIfConfigParam.au1AntennaSelection[u1Index];
                    }
                }
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRTSThreshold == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u2RTSThreshold =
                    pGetRadioIfDB->RadioIfConfigParam.u2RTSThreshold;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bT1Threshold == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u4T1Threshold =
                    pGetRadioIfDB->RadioIfChannelInfo.u4T1Threshold;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bShortRetry == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1ShortRetry =
                    pGetRadioIfDB->RadioIfConfigParam.u1ShortRetry;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bLongRetry == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1LongRetry =
                    pGetRadioIfDB->RadioIfConfigParam.u1LongRetry;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bFragmentationThreshold
                == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u2FragmentationThreshold =
                    pGetRadioIfDB->RadioIfConfigParam.u2FragmentationThreshold;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxMsduLifetime == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u4TxMsduLifetime =
                    pGetRadioIfDB->RadioIfConfigParam.u4TxMsduLifetime;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxMsduLifetime == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u4RxMsduLifetime
                    = pGetRadioIfDB->RadioIfConfigParam.u4RxMsduLifetime;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bBeaconPeriod == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u2BeaconPeriod
                    = pGetRadioIfDB->RadioIfConfigParam.u2BeaconPeriod;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bDot11RadioType == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType =
                    pGetRadioIfDB->RadioIfConfigParam.u4Dot11RadioType;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bShortPreamble == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1ShortPreamble
                    = pGetRadioIfDB->RadioIfConfigParam.u1ShortPreamble;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bNoOfBssId == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1NoOfBssId =
                    pGetRadioIfDB->RadioIfConfigParam.u1NoOfBssId;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bProbeInclude == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1ProbeInclude =
                    pGetRadioIfDB->RadioIfConfigParam.u1ProbeInclude;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bBeaconInclude == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1BeaconInclude =
                    pGetRadioIfDB->RadioIfConfigParam.u1BeaconInclude;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHtFlag == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1HtFlag =
                    pGetRadioIfDB->RadioIfConfigParam.u1HtFlag;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxSuppMCS == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1MaxSuppMCS =
                    pGetRadioIfDB->RadioIfConfigParam.u1MaxSuppMCS;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxManMCS == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1MaxManMCS =
                    pGetRadioIfDB->RadioIfConfigParam.u1MaxManMCS;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxAntenna == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1TxAntenna =
                    pGetRadioIfDB->RadioIfConfigParam.u1TxAntenna;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxAntenna == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1RxAntenna =
                    pGetRadioIfDB->RadioIfConfigParam.u1RxAntenna;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bBPFlag == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1BPFlag =
                    pGetRadioIfDB->RadioIfConfigParam.u1BPFlag;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxSupportMcsSet == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1TxSupportMcsSet =
                    pGetRadioIfDB->RadioIfConfigParam.u1TxSupportMcsSet;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bManMCSSet == OSIX_TRUE)
            {
                MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.au1ManMCSSet,
                        pGetRadioIfDB->RadioIfConfigParam.au1ManMCSSet,
                        RADIOIF_11N_MCS_RATE_LEN_PKT);
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMCSSet == OSIX_TRUE)
            {
                MEMCPY ((pRadioIfGetDB->RadioIfGetAllDB.au1SuppMCSSet),
                        (pGetRadioIfDB->RadioIfConfigParam.au1SuppMCSSet),
                        sizeof (pGetRadioIfDB->
                                RadioIfConfigParam.au1SuppMCSSet));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAMPDUSupport == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1AMPDUSupport =
                    pGetRadioIfDB->RadioIfConfigParam.u1AMPDUSupport;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxMpduInAmpdu == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1MaxMpduInAmpdu =
                    pGetRadioIfDB->RadioIfConfigParam.u1MaxMpduInAmpdu;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxMsdu == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1MaxMsdu =
                    pGetRadioIfDB->RadioIfConfigParam.u1MaxMsdu;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHTMaxAmpduLengthExponent
                == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1HTMaxAmpduLengthExponent =
                    pGetRadioIfDB->
                    RadioIfConfigParam.u1HTMaxAmpduLengthExponent;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVHTMaxAmpduLengthExponent
                == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1VHTMaxAmpduLengthExponent =
                    pGetRadioIfDB->
                    RadioIfConfigParam.u1VHTMaxAmpduLengthExponent;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHTOption == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1HTOption =
                    pGetRadioIfDB->RadioIfConfigParam.u1HTOption;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCountryString == OSIX_TRUE)
            {
                MEMCPY ((pRadioIfGetDB->RadioIfGetAllDB.au1CountryString),
                        (pGetRadioIfDB->RadioIfConfigParam.au1CountryString),
                        sizeof (pGetRadioIfDB->
                                RadioIfConfigParam.au1CountryString));
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMultiDomainInfo == OSIX_TRUE)
            {
                MEMCPY ((pRadioIfGetDB->RadioIfGetAllDB.MultiDomainInfo),
                        (pGetRadioIfDB->RadioIfConfigParam.MultiDomainInfo),
                        sizeof (pGetRadioIfDB->
                                RadioIfConfigParam.MultiDomainInfo));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bOperationalRate == OSIX_TRUE)
            {
                MEMCPY ((pRadioIfGetDB->RadioIfGetAllDB.au1OperationalRate),
                        (pGetRadioIfDB->RadioIfConfigParam.au1OperationalRate),
                        sizeof (pGetRadioIfDB->
                                RadioIfConfigParam.au1OperationalRate));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSupportedRate == OSIX_TRUE)
            {
                MEMCPY ((pRadioIfGetDB->RadioIfGetAllDB.au1SupportedRate),
                        (pGetRadioIfDB->RadioIfConfigParam.au1SupportedRate),
                        sizeof (pGetRadioIfDB->
                                RadioIfConfigParam.au1SupportedRate));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bIeee80211Stats == OSIX_TRUE)
            {
                MEMCPY (&(pRadioIfGetDB->RadioIfGetAllDB.ieee80211Stats),
                        &pGetRadioIfDB->ieee80211Stats,
                        sizeof (pGetRadioIfDB->ieee80211Stats));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioStats == OSIX_TRUE)
            {
                MEMCPY (&(pRadioIfGetDB->RadioIfGetAllDB.radioStats),
                        &(pGetRadioIfDB->radioStats),
                        sizeof (pGetRadioIfDB->radioStats));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMCSRateSet == OSIX_TRUE)
            {
                MEMCPY ((pRadioIfGetDB->RadioIfGetAllDB.au1MCSRateSet),
                        (pGetRadioIfDB->RadioIfConfigParam.au1MCSRateSet),
                        sizeof (pGetRadioIfDB->
                                RadioIfConfigParam.au1MCSRateSet));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bReportInterval == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u2ReportInterval =
                    pGetRadioIfDB->RadioIfConfigParam.u2ReportInterval;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxClientCount == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u4MaxClientCount =
                    pGetRadioIfDB->RadioIfConfigParam.u4MaxClientCount;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioCounters == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u4RadioInOctets =
                    pGetRadioIfDB->RadioIfConfigParam.u4RadioInOctets;
                pRadioIfGetDB->RadioIfGetAllDB.u4RadioOutOctets =
                    pGetRadioIfDB->RadioIfConfigParam.u4RadioOutOctets;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRadioType == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u4SuppRadioType =
                    pGetRadioIfDB->RadioIfConfigParam.u4SuppRadioType;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAllowChannelWidth ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1AllowChannelWidth =
                    pGetRadioIfDB->RadioIfConfigParam.u1AllowChannelWidth;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCapability == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u2Capability =
                    pGetRadioIfDB->RadioIfConfigParam.u2Capability;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadarFound == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1RadarFound =
                    pGetRadioIfDB->RadioIfConfigParam.u1RadarFound;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bChanlSwitStatus == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1ChanlSwitStatus =
                    pGetRadioIfDB->RadioIfConfigParam.u1ChanlSwitStatus;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.b11hDfsStatus == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u111hDfsStatus =
                    pGetRadioIfDB->RadioIfConfigParam.u111hDfsStatus;
            }
            break;

        case WSS_DESTROY_WLAN_BSS_IFINDEX_DB:
            RadioIfDB.u4VirtualRadioIfIndex =
                pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

            /* Get the corresponding node from RB Tree (RadioIfDb) */
            pGetRadioIfDB =
                RBTreeGet (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                           (tRBElem *) (&RadioIfDB));

            if (pGetRadioIfDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIFDBMsg: BSS Index entry not found\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

            if (pGetRadioIfDB->au4BssIfIndex[u1WlanId - 1] == 0)
            {
                WSSIF_UNLOCK;
                return OSIX_SUCCESS;
            }
            pGetRadioIfDB->au4BssIfIndex[u1WlanId - 1] = 0;

            break;

        case WSS_GET_WLAN_BSS_IFINDEX_DB:

            RadioIfDB.u4VirtualRadioIfIndex =
                pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

            /* Get the corresponding node from RB Tree (RadioIfDb) */
            pGetRadioIfDB =
                RBTreeGet (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                           (tRBElem *) (&RadioIfDB));

            if (pGetRadioIfDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIFDBMsg: BSS Index entry not found\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

            if (pGetRadioIfDB->au4BssIfIndex[u1WlanId - 1] == 0)
            {
                WSSIF_TRC1 (WSSIF_MGMT_TRC,
                            "RadioIFDBMsg: Can't access WLAN DB for WlanId-1 %d\r\n",
                            u1WlanId - 1);
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            pRadioIfGetDB->RadioIfGetAllDB.u4BssIfIndex =
                pGetRadioIfDB->au4BssIfIndex[u1WlanId - 1];
            break;

        case WSS_GET_NEXT_WLAN_BSS_IFINDEX_DB:

            RadioIfDB.u4VirtualRadioIfIndex =
                pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

            /* Get the corresponding node from RB Tree (RadioIfDb) */
            pGetRadioIfDB =
                RBTreeGet (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                           (tRBElem *) (&RadioIfDB));

            if (pGetRadioIfDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIFDBMsg: GetNext BSS Index entry failed\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            u1WlanId = pRadioIfGetDB->RadioIfGetAllDB.u1WlanId;

            if (pGetRadioIfDB->au4BssIfIndex[u1WlanId - 1] == 0)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIFDBMsg: BSS free index available\n");
                WSSIF_UNLOCK;
            }
            pRadioIfGetDB->RadioIfGetAllDB.u4BssIfIndex =
                pGetRadioIfDB->au4BssIfIndex[u1WlanId - 1];
            break;

        case WSS_GET_RADIO_QOS_CONFIG_DB:

            RadioIfDB.u4VirtualRadioIfIndex =
                pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

            /* Get the corresponding node from RB Tree (RadioIfDb) */
            pGetRadioIfDB =
                RBTreeGet (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                           (tRBElem *) (&RadioIfDB));

            if (pGetRadioIfDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIfDBMsg: Qos Config entry not found\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bQosIndex == OSIX_FALSE)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIfDBMsg: QoS Config Index missing\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            u1QosIndex =
                (UINT1) (pRadioIfGetDB->RadioIfGetAllDB.u1QosIndex - 1);
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bQueueDepth != OSIX_FALSE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.au1QueueDepth[u1QosIndex] =
                    pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1QosIndex].
                    u1QueueDepth;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCwMin != OSIX_FALSE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.au2CwMin[u1QosIndex] =
                    pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1QosIndex].u2CwMin;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCwMax != OSIX_FALSE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.au2CwMax[u1QosIndex] =
                    pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1QosIndex].u2CwMax;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAifsn != OSIX_FALSE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.au1Aifsn[u1QosIndex] =
                    pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1QosIndex].u1Aifsn;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxOpLimit != OSIX_FALSE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.au2TxOpLimit[u1QosIndex] =
                    pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1QosIndex].u2TxOpLimit;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bQosPrio != OSIX_FALSE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.au1QosPrio[u1QosIndex] =
                    pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1QosIndex].u1QosPrio;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bDscp != OSIX_FALSE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.au1Dscp[u1QosIndex] =
                    pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1QosIndex].u1Dscp;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTaggingPolicy != OSIX_FALSE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1TaggingPolicy =
                    pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1QosIndex].
                    u1TaggingPolicy;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAdmissionControl == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.au1AdmissionControl[u1QosIndex]
                    =
                    pGetRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[u1QosIndex].
                    u1AdmissionControl;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bWtpInternalId == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u2WtpInternalId =
                    pGetRadioIfDB->u2WtpInternalId;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRadioId == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1RadioId =
                    pGetRadioIfDB->u1RadioId;
            }
            /* pGetRadioIfDB = NULL; */
            break;

        case WSS_ADD_RADIO_IF_DB_PHY:
            /*Memory Allocation for the node to be added in the tree */
            if ((pAddRadioIfDB =
                 (tRadioIfDB *) MemAllocMemBlk (RADIOIF_INDEX_DB_POOLID))
                == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIfDBMsg: Memory alloc failed for Phy Radio\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            /*Initialisation */
            MEMSET (pAddRadioIfDB, 0, sizeof (tRadioIfDB));

            /* Copy the info received from CFA (to add the node in Tree) */
            pAddRadioIfDB->u4VirtualRadioIfIndex =
                pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
            pAddRadioIfDB->u1IfType = pRadioIfGetDB->RadioIfGetAllDB.u1IfType;
            pAddRadioIfDB->u1RadioId = pRadioIfGetDB->RadioIfGetAllDB.u1RadioId;
            pAddRadioIfDB->u1AdminStatus =
                pRadioIfGetDB->RadioIfGetAllDB.u1AdminStatus;

            /*To Add the node in the RBTree (RadioIfDb) */
            if (RBTreeAdd (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                           (tRBElem *) pAddRadioIfDB) != OSIX_SUCCESS)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIfDBMsg: Add Entry for physical radio failed\n");
                WSSIF_UNLOCK;
                MemReleaseMemBlock (RADIOIF_INDEX_DB_POOLID,
                                    (UINT1 *) pAddRadioIfDB);
                return OSIX_FAILURE;
            }

            break;

        case WSS_ADD_RADIO_IF_DB:

            /*Memory Allocation for the node to be added in the tree */
            if ((pAddRadioIfDB = (tRadioIfDB *) MemAllocMemBlk
                 (RADIOIF_INDEX_DB_POOLID)) == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIfDBMsg: Memory alloc failed for radio index\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            /*Initialisation */
            MEMSET (pAddRadioIfDB, 0, sizeof (tRadioIfDB));

            /* Copy the info received from CFA (to add the node in Tree) */
            pAddRadioIfDB->u4VirtualRadioIfIndex =
                pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
            pAddRadioIfDB->u1IfType = pRadioIfGetDB->RadioIfGetAllDB.u1IfType;
            pAddRadioIfDB->u1RadioId = pRadioIfGetDB->RadioIfGetAllDB.u1RadioId;
            pAddRadioIfDB->u2WtpInternalId =
                pRadioIfGetDB->RadioIfGetAllDB.u2WtpInternalId;
            pAddRadioIfDB->u1AdminStatus =
                pRadioIfGetDB->RadioIfGetAllDB.u1AdminStatus;
            pAddRadioIfDB->u1OperStatus =
                pRadioIfGetDB->RadioIfGetAllDB.u1OperStatus;
            pAddRadioIfDB->RadioIfConfigParam.u4Dot11RadioType =
                pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType;

            if (pAddRadioIfDB->RadioIfConfigParam.u4Dot11RadioType ==
                RADIO_TYPE_B
                || pAddRadioIfDB->RadioIfConfigParam.u4Dot11RadioType ==
                RADIO_TYPE_BG
                || pAddRadioIfDB->RadioIfConfigParam.u4Dot11RadioType ==
                RADIO_TYPE_BGN)

            {
                MEMCPY ((pAddRadioIfDB->RadioIfConfigParam.au1OperationalRate),
                        (gau1OperationalRate_RadioB),
                        sizeof (gau1OperationalRate_RadioB));
            }
            else if ((pAddRadioIfDB->RadioIfConfigParam.u4Dot11RadioType ==
                      RADIO_TYPE_A)
                     || (pAddRadioIfDB->RadioIfConfigParam.u4Dot11RadioType ==
                         RADIO_TYPE_G
                         || pAddRadioIfDB->
                         RadioIfConfigParam.u4Dot11RadioType == RADIO_TYPE_AN)
                     || (pAddRadioIfDB->RadioIfConfigParam.u4Dot11RadioType ==
                         RADIO_TYPE_AC))
            {
                MEMCPY ((pAddRadioIfDB->RadioIfConfigParam.au1OperationalRate),
                        (gau1OperationalRate_RadioA),
                        sizeof (gau1OperationalRate_RadioA));
            }

            /* Updating with default values as per RFC 5416 */

            pAddRadioIfDB->RadioIfConfigParam.u2RTSThreshold =
                RADIOIF_DEF_RTS_THRESHOLD;
            pAddRadioIfDB->RadioIfConfigParam.u1ShortRetry =
                RADIOIF_DEF_SHORT_RETRY;
            pAddRadioIfDB->RadioIfConfigParam.u1LongRetry =
                RADIOIF_DEF_LONG_RETRY;
            pAddRadioIfDB->RadioIfConfigParam.u2FragmentationThreshold =
                RADIOIF_DEF_FRAG_THRESHOLD;
            pAddRadioIfDB->RadioIfConfigParam.u4TxMsduLifetime =
                RADIOIF_DEF_TX_LIFETIME;
            pAddRadioIfDB->RadioIfConfigParam.u4RxMsduLifetime =
                RADIOIF_DEF_RX_LIFETIME;
            pAddRadioIfDB->RadioIfConfigParam.u1CurrentTxAntenna =
                RADIOIF_DEF_ANT_COUNT;
            pAddRadioIfDB->RadioIfConfigParam.au1AntennaSelection[0] =
                RADIO_ANT_INTERNAL;
            pAddRadioIfDB->RadioIfConfigParam.u1DiversitySupport =
                RADIO_ANT_DIVERSITY_DISABLED;
            pAddRadioIfDB->RadioIfConfigParam.u1AntennaMode =
                RADIOIF_DEF_ANT_MODE;
            pAddRadioIfDB->RadioIfConfigParam.u2BeaconPeriod =
                WSSIF_RADIOIF_DEF_BEACON_PERIOD;
            pAddRadioIfDB->RadioIfConfigParam.u2DtimPeriod =
                WSSIF_RADIOIF_DEF_DTIM_PERIOD;
            pAddRadioIfDB->RadioIfConfigParam.u1MultiDomainCapabilityEnabled =
                WSSIF_MULTIDOMAIN_CAPABILITY_ENABLED;
            if ((pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType ==
                 RADIO_TYPE_BGN)
                || (pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_AN)
                || (pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType ==
                    RADIO_TYPE_AC))

            {
                WssIfSetDefault11nParams (pAddRadioIfDB);
            }
            if (pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType ==
                RADIO_TYPE_AC)
            {
                WssIfSetDefault11AcParams (pAddRadioIfDB);
            }

            if (pAddRadioIfDB->RadioIfConfigParam.u4Dot11RadioType ==
                RADIO_TYPE_B
                || pAddRadioIfDB->RadioIfConfigParam.u4Dot11RadioType ==
                RADIO_TYPE_BG
                || pAddRadioIfDB->RadioIfConfigParam.u4Dot11RadioType ==
                RADIO_TYPE_BGN)

            {
                pAddRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_BE].
                    u2CwMin = WSSIF_RADIOIF_DEF_CWMIN_BE_B;
                pAddRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_BE].
                    u2CwMax = WSSIF_RADIOIF_DEF_CWMAX_BE_B;
                pAddRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_VI].
                    u2CwMin = WSSIF_RADIOIF_DEF_CWMIN_VI_B;
                pAddRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_VI].
                    u2CwMax = WSSIF_RADIOIF_DEF_CWMAX_VI_B;
                pAddRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_VI].
                    u2TxOpLimit = WSSIF_RADIOIF_DEF_TXOP_VI_B;
                pAddRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_VO].
                    u2CwMin = WSSIF_RADIOIF_DEF_CWMIN_VO_B;
                pAddRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_VO].
                    u2CwMax = WSSIF_RADIOIF_DEF_CWMAX_VO_B;
                pAddRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_BG].
                    u2CwMin = WSSIF_RADIOIF_DEF_CWMIN_BG_B;
                /* Default Threshold Energy */
                pAddRadioIfDB->RadioIfChannelInfo.i4EDThreshold
                    = RADIOIF_DEF_ENERGY_THRESHOLD;

            }
            else if ((pAddRadioIfDB->RadioIfConfigParam.u4Dot11RadioType ==
                      RADIO_TYPE_A)
                     || (pAddRadioIfDB->RadioIfConfigParam.u4Dot11RadioType ==
                         RADIO_TYPE_G
                         || pAddRadioIfDB->
                         RadioIfConfigParam.u4Dot11RadioType == RADIO_TYPE_AN)
                     || (pAddRadioIfDB->RadioIfConfigParam.u4Dot11RadioType ==
                         RADIO_TYPE_AC))
            {

                pAddRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_BE].
                    u2CwMin = WSSIF_RADIOIF_DEF_CWMIN_BE;
                pAddRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_BE].
                    u2CwMax = WSSIF_RADIOIF_DEF_CWMAX_BE;
                pAddRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_VI].
                    u2CwMin = WSSIF_RADIOIF_DEF_CWMIN_VI;
                pAddRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_VI].
                    u2CwMax = WSSIF_RADIOIF_DEF_CWMAX_VI;
                pAddRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_VI].
                    u2TxOpLimit = WSSIF_RADIOIF_DEF_TXOP_VI;
                pAddRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_VO].
                    u2CwMin = WSSIF_RADIOIF_DEF_CWMIN_VO;
                pAddRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_VO].
                    u2CwMax = WSSIF_RADIOIF_DEF_CWMAX_VO;
                pAddRadioIfDB->
                    RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_BG].
                    u2CwMin = WSSIF_RADIOIF_DEF_CWMIN_BG;
                pAddRadioIfDB->RadioIfChannelInfo.u4T1Threshold
                    = RADIOIF_DEF_ENERGY_THRESHOLD;

            }
            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_BE].
                u2TxOpLimit = WSSIF_RADIOIF_DEF_TXOP_BE;
            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_BE].
                u1Aifsn = WSSIF_RADIOIF_DEF_AIFSN_BE;
            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_BE].
                u1QosPrio = WSSIF_RADIOIF_DEF_QOS_PRIO_BE;
            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_BE].
                u1Dscp = WSSIF_RADIOIF_DEF_QOS_DSCP_BE;
            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_BE].
                u1TaggingPolicy = WSSIF_RADIOIF_DEF_TAG_BE;
            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_BE].
                u1AdmissionControl = WSSIF_RADIOIF_DEF_ADMISSION_BE;

            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_VI].
                u1Aifsn = WSSIF_RADIOIF_DEF_AIFSN_VI;
            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_VI].
                u1QosPrio = WSSIF_RADIOIF_DEF_QOS_PRIO_VI;
            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_VI].
                u1Dscp = WSSIF_RADIOIF_DEF_QOS_DSCP_VI;
            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_VI].
                u1TaggingPolicy = WSSIF_RADIOIF_DEF_TAG_VI;
            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_VI].
                u1AdmissionControl = WSSIF_RADIOIF_DEF_ADMISSION_VI;

            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_VO].
                u2TxOpLimit = WSSIF_RADIOIF_DEF_TXOP_VO;
            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_VO].
                u1Aifsn = WSSIF_RADIOIF_DEF_AIFSN_VO;
            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_VO].
                u1QosPrio = WSSIF_RADIOIF_DEF_QOS_PRIO_VO;
            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_VO].
                u1Dscp = WSSIF_RADIOIF_DEF_QOS_DSCP_VO;
            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_VO].
                u1TaggingPolicy = WSSIF_RADIOIF_DEF_TAG_VO;
            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_VO].
                u1AdmissionControl = WSSIF_RADIOIF_DEF_ADMISSION_VO;

            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_BG].
                u2CwMax = WSSIF_RADIOIF_DEF_CWMAX_BG;
            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_BG].
                u2TxOpLimit = WSSIF_RADIOIF_DEF_TXOP_BG;
            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_BG].
                u1Aifsn = WSSIF_RADIOIF_DEF_AIFSN_BG;
            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_BG].
                u1QosPrio = WSSIF_RADIOIF_DEF_QOS_PRIO_BG;
            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_BG].
                u1Dscp = WSSIF_RADIOIF_DEF_QOS_DSCP_BG;
            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_BG].
                u1TaggingPolicy = WSSIF_RADIOIF_DEF_TAG_BG;
            pAddRadioIfDB->RadioIfConfigParam.RadioIfQosConfig[RADIOIF_EDCA_BG].
                u1AdmissionControl = WSSIF_RADIOIF_DEF_ADMISSION_BG;
            SPRINTF ((CHR1 *) au1CountryString, "%s",
                     WSSIF_RADIOIF_DEF_COUNTRY);
            MEMCPY (pAddRadioIfDB->RadioIfConfigParam.au1CountryString,
                    au1CountryString, RADIO_COUNTRY_LENGTH);

            /*To Add the node in the RBTree (RadioIfDb) */
            if (RBTreeAdd (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                           (tRBElem *) pAddRadioIfDB) != OSIX_SUCCESS)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIfDBMsg: Virtual Radio DB entry failed\n");
                MemReleaseMemBlock (RADIOIF_INDEX_DB_POOLID,
                                    (UINT1 *) pAddRadioIfDB);
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }
            break;

        case WSS_DEL_RADIO_IF_DB:

            RadioIfDB.u4VirtualRadioIfIndex =
                pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

            /* Get the corresponding node from RB Tree (RadioIfDb) */
            pRemRadioIfDB =
                RBTreeGet (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                           (tRBElem *) (&RadioIfDB));

            if (pRemRadioIfDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIfDBMsg : Entry not found, delete failed\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            /*Remove the corresponding node from the tree */
            RBTreeRem (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB, pRemRadioIfDB);

            MemReleaseMemBlock (RADIOIF_INDEX_DB_POOLID,
                                (UINT1 *) pRemRadioIfDB);
            break;

        case WSS_GET_RADIO_IF_DB_11AC:
            RadioIfDB.u4VirtualRadioIfIndex =
                pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

            /* Get the corresponding node from RB Tree (RadioIfDb) */
            pGetRadioIfDB =
                RBTreeGet (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                           (tRBElem *) (&RadioIfDB));

            if (pGetRadioIfDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIfDBMsg : Entry not found, get failed \n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            /* Copying the values , which are needed */

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxMPDULen == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1MaxMPDULen =
                    pGetRadioIfDB->Dot11AcCapaParams.u1MaxMPDULen;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMaxMPDULen == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppMaxMPDULen =
                    pGetRadioIfDB->Dot11AcCapaParams.u1SuppMaxMPDULen;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppChanWidth == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppChanWidth =
                    pGetRadioIfDB->Dot11AcCapaParams.u1SuppChanWidth;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxLpdc == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1RxLpdc =
                    pGetRadioIfDB->Dot11AcCapaParams.u1RxLpdc;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRxLpdc == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppRxLpdc =
                    pGetRadioIfDB->Dot11AcCapaParams.u1SuppRxLpdc;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi80 == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1ShortGi80 =
                    pGetRadioIfDB->Dot11AcCapaParams.u1ShortGi80;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppShortGi80 == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppShortGi80 =
                    pGetRadioIfDB->Dot11AcCapaParams.u1SuppShortGi80;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi160 == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1ShortGi160 =
                    pGetRadioIfDB->Dot11AcCapaParams.u1ShortGi160;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtTxStbc == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                    u1SuppTxStbc =
                    pGetRadioIfDB->Dot11AcCapaParams.u1SuppTxStbc;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxStbc == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1TxStbc =
                    pGetRadioIfDB->Dot11AcCapaParams.u1TxStbc;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRxStbcStatus == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                    u1SuppRxStbcStatus = pGetRadioIfDB->Dot11AcCapaParams.
                    u1SuppRxStbcStatus;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtRxStbc == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                    u1SuppRxStbc =
                    pGetRadioIfDB->Dot11AcCapaParams.u1SuppRxStbc;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxStbc == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1RxStbc =
                    pGetRadioIfDB->Dot11AcCapaParams.u1RxStbc;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxStbcStatus == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                    u1RxStbcStatus = pGetRadioIfDB->Dot11AcCapaParams.
                    u1RxStbcStatus;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuBeamFormer == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuBeamFormer
                    = pGetRadioIfDB->Dot11AcCapaParams.u1SuBeamFormer;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuBeamFormee == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuBeamFormee
                    = pGetRadioIfDB->Dot11AcCapaParams.u1SuBeamFormee;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppBeamFormAnt == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppBeamFormAnt =
                    pGetRadioIfDB->Dot11AcCapaParams.u1SuppBeamFormAnt;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSoundDimension == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SoundDimension =
                    pGetRadioIfDB->Dot11AcCapaParams.u1SoundDimension;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMuBeamFormer == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1MuBeamFormer
                    = pGetRadioIfDB->Dot11AcCapaParams.u1MuBeamFormer;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMuBeamFormee == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1MuBeamFormee
                    = pGetRadioIfDB->Dot11AcCapaParams.u1MuBeamFormee;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtTxOpPs == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1VhtTxOpPs =
                    pGetRadioIfDB->Dot11AcCapaParams.u1VhtTxOpPs;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHtcVhtCapable == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1HtcVhtCapable =
                    pGetRadioIfDB->Dot11AcCapaParams.u1HtcVhtCapable;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtMaxAMPDU == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1VhtMaxAMPDU =
                    pGetRadioIfDB->Dot11AcCapaParams.u1VhtMaxAMPDU;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtMaxAMPDU == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppVhtMaxAMPDU =
                    pGetRadioIfDB->Dot11AcCapaParams.u1SuppVhtMaxAMPDU;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtLinkAdaption == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1VhtLinkAdaption =
                    pGetRadioIfDB->Dot11AcCapaParams.u1VhtLinkAdaption;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtRxAntenna == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1VhtRxAntenna =
                    pGetRadioIfDB->Dot11AcCapaParams.u1VhtRxAntenna;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtTxAntenna == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1VhtTxAntenna =
                    pGetRadioIfDB->Dot11AcCapaParams.u1VhtTxAntenna;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtCapInfo == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u4VhtCapInfo =
                    pGetRadioIfDB->Dot11AcCapaParams.u4VhtCapInfo;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtCapInfo == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                    u4SuppVhtCapInfo =
                    pGetRadioIfDB->Dot11AcCapaParams.u4SuppVhtCapInfo;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtCapaMcs == OSIX_TRUE)
            {
                MEMCPY ((pRadioIfGetDB->RadioIfGetAllDB.
                         Dot11AcCapaParams.au1VhtCapaMcs),
                        (pGetRadioIfDB->Dot11AcCapaParams.au1VhtCapaMcs),
                        sizeof (pGetRadioIfDB->Dot11AcCapaParams.
                                au1VhtCapaMcs));
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtCapaMcs == OSIX_TRUE)
            {
                MEMCPY ((pRadioIfGetDB->RadioIfGetAllDB.
                         Dot11AcCapaParams.au1SuppVhtCapaMcs),
                        (pGetRadioIfDB->Dot11AcCapaParams.au1SuppVhtCapaMcs),
                        sizeof (pGetRadioIfDB->Dot11AcCapaParams.
                                au1SuppVhtCapaMcs));
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRxDataRate == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                    u2SuppRxDataRate =
                    pGetRadioIfDB->Dot11AcCapaParams.u2SuppRxDataRate;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTxDataRate == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                    u2SuppTxDataRate =
                    pGetRadioIfDB->Dot11AcCapaParams.u2SuppTxDataRate;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxDataRate == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                    u2RxDataRate =
                    pGetRadioIfDB->Dot11AcCapaParams.u2RxDataRate;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxDataRate == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                    u2TxDataRate =
                    pGetRadioIfDB->Dot11AcCapaParams.u2TxDataRate;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtSTS == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppVhtSTS =
                    pGetRadioIfDB->Dot11AcCapaParams.u1SuppVhtSTS;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtSTS == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1VhtSTS =
                    pGetRadioIfDB->Dot11AcCapaParams.u1VhtSTS;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSpecMgmt == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                    u1SpecMgmt = pGetRadioIfDB->Dot11AcCapaParams.u1SpecMgmt;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtChannelWidth == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcOperParams.u1VhtChannelWidth =
                    pGetRadioIfDB->Dot11AcOperParams.u1VhtChannelWidth;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtChannelWidth ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcOperParams.u1SuppVhtChannelWidth =
                    pGetRadioIfDB->Dot11AcOperParams.u1SuppVhtChannelWidth;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCenterFcy0 == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy0 =
                    pGetRadioIfDB->Dot11AcOperParams.u1CenterFcy0;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppCenterFcy0 == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcOperParams.u1SuppCenterFcy0 =
                    pGetRadioIfDB->Dot11AcOperParams.u1SuppCenterFcy0;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCenterFcy1 == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy1 =
                    pGetRadioIfDB->Dot11AcOperParams.u1CenterFcy1;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtOperInfo == OSIX_TRUE)
            {
                MEMCPY ((pRadioIfGetDB->RadioIfGetAllDB.
                         Dot11AcOperParams.au1VhtOperInfo),
                        (pGetRadioIfDB->Dot11AcOperParams.au1VhtOperInfo),
                        sizeof (pGetRadioIfDB->Dot11AcOperParams.
                                au1VhtOperInfo));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtOperMcsSet == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.
                    u2VhtOperMcsSet =
                    pGetRadioIfDB->Dot11AcOperParams.u2VhtOperMcsSet;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtOperModeNotify
                == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.
                    u1VhtOperModeNotify =
                    pGetRadioIfDB->Dot11AcOperParams.u1VhtOperModeNotify;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtOperModeElem == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.
                    u1VhtOperModeElem =
                    pGetRadioIfDB->Dot11AcOperParams.u1VhtOperModeElem;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtOption == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.
                    u1VhtOption = pGetRadioIfDB->Dot11AcOperParams.u1VhtOption;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHTOption == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1HTOption =
                    pGetRadioIfDB->RadioIfConfigParam.u1HTOption;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTransPower == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.
                    u1TransPower = pGetRadioIfDB->VhtTransmitPower.u1TransPower;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxTxPowerCount == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.
                    u1MaxTxPowerCount =
                    pGetRadioIfDB->VhtTransmitPower.u1MaxTxPowerCount;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxTxPowerUnit == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.
                    u1MaxTxPowerUnit =
                    pGetRadioIfDB->VhtTransmitPower.u1MaxTxPowerUnit;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxPower20 == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.
                    u1TxPower20 = pGetRadioIfDB->VhtTransmitPower.u1TxPower20;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxPower40 == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.
                    u1TxPower40 = pGetRadioIfDB->VhtTransmitPower.u1TxPower40;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxPower80 == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.
                    u1TxPower80 = pGetRadioIfDB->VhtTransmitPower.u1TxPower80;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxPower160 == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.
                    u1TxPower160 = pGetRadioIfDB->VhtTransmitPower.u1TxPower160;
            }

            break;

        case WSS_SET_RADIO_IF_DB_11AC:
            RadioIfDB.u4VirtualRadioIfIndex =
                pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

            /* Get the corresponding node from RB Tree (RadioIfDb) */
            pGetRadioIfDB =
                RBTreeGet (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                           (tRBElem *) (&RadioIfDB));

            if (pGetRadioIfDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIfDBMsg: Entry not exist, set failed\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            /* Copying the values , which are needed */
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxMPDULen == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1MaxMPDULen =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1MaxMPDULen;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMaxMPDULen == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1SuppMaxMPDULen =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppMaxMPDULen;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppChanWidth == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1SuppChanWidth =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppChanWidth;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxLpdc == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1RxLpdc =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1RxLpdc;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRxLpdc == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1SuppRxLpdc =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppRxLpdc;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi80 == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1ShortGi80 =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1ShortGi80;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppShortGi80 == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1SuppShortGi80 =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppShortGi80;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi160 == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1ShortGi160 =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1ShortGi160;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtTxStbc == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1SuppTxStbc =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppTxStbc;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxStbc == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1TxStbc =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1TxStbc;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtRxStbc == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1SuppRxStbc =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppRxStbc;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRxStbcStatus == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1SuppRxStbcStatus =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                    u1SuppRxStbcStatus;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxStbc == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1RxStbc =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1RxStbc;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxStbcStatus == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1RxStbcStatus =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                    u1RxStbcStatus;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuBeamFormer == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1SuBeamFormer =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuBeamFormer;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuBeamFormee == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1SuBeamFormee =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuBeamFormee;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppBeamFormAnt == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1SuppBeamFormAnt =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppBeamFormAnt;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSoundDimension == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1SoundDimension =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SoundDimension;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMuBeamFormer == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1MuBeamFormer =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1MuBeamFormer;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMuBeamFormee == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1MuBeamFormee =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1MuBeamFormee;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtTxOpPs == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1VhtTxOpPs =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1VhtTxOpPs;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHtcVhtCapable == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1HtcVhtCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1HtcVhtCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtMaxAMPDU == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1VhtMaxAMPDU =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1VhtMaxAMPDU;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtMaxAMPDU == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1SuppVhtMaxAMPDU =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppVhtMaxAMPDU;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtLinkAdaption == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1VhtLinkAdaption =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1VhtLinkAdaption;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtRxAntenna == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1VhtRxAntenna =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1VhtRxAntenna;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtTxAntenna == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1VhtTxAntenna =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1VhtTxAntenna;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtCapInfo == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u4VhtCapInfo =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u4VhtCapInfo;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtCapInfo == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u4SuppVhtCapInfo =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u4SuppVhtCapInfo;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtCapaMcs == OSIX_TRUE)
            {
                MEMCPY ((pGetRadioIfDB->Dot11AcCapaParams.au1SuppVhtCapaMcs),
                        (pRadioIfGetDB->RadioIfGetAllDB.
                         Dot11AcCapaParams.au1SuppVhtCapaMcs),
                        sizeof (pRadioIfGetDB->RadioIfGetAllDB.
                                Dot11AcCapaParams.au1SuppVhtCapaMcs));
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtCapaMcs == OSIX_TRUE)
            {
                MEMCPY ((pGetRadioIfDB->Dot11AcCapaParams.au1VhtCapaMcs),
                        (pRadioIfGetDB->RadioIfGetAllDB.
                         Dot11AcCapaParams.au1VhtCapaMcs),
                        sizeof (pRadioIfGetDB->RadioIfGetAllDB.
                                Dot11AcCapaParams.au1VhtCapaMcs));
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRxDataRate == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u2SuppRxDataRate =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u2SuppRxDataRate;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTxDataRate == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u2SuppTxDataRate =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u2SuppTxDataRate;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxDataRate == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u2RxDataRate =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u2RxDataRate;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxDataRate == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u2TxDataRate =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u2TxDataRate;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtSTS == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1SuppVhtSTS =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcCapaParams.u1SuppVhtSTS;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtSTS == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1VhtSTS =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1VhtSTS;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSpecMgmt == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcCapaParams.u1SpecMgmt =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SpecMgmt;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtChannelWidth == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcOperParams.u1VhtChannelWidth =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcOperParams.u1VhtChannelWidth;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppVhtChannelWidth ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcOperParams.u1SuppVhtChannelWidth =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcOperParams.u1SuppVhtChannelWidth;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCenterFcy0 == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcOperParams.u1CenterFcy0 =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcOperParams.u1CenterFcy0;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppCenterFcy0 == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcOperParams.u1SuppCenterFcy0 =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcOperParams.u1SuppCenterFcy0;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCenterFcy1 == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcOperParams.u1CenterFcy1 =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcOperParams.u1CenterFcy1;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtOperInfo == OSIX_TRUE)
            {
                MEMCPY ((pGetRadioIfDB->Dot11AcOperParams.au1VhtOperInfo),
                        (pRadioIfGetDB->RadioIfGetAllDB.
                         Dot11AcOperParams.au1VhtOperInfo),
                        sizeof (pRadioIfGetDB->RadioIfGetAllDB.
                                Dot11AcOperParams.au1VhtOperInfo));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtOperMcsSet == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcOperParams.u2VhtOperMcsSet =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcOperParams.u2VhtOperMcsSet;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtOperModeNotify
                == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcOperParams.u1VhtOperModeNotify =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcOperParams.u1VhtOperModeNotify;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtOperModeElem == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcOperParams.u1VhtOperModeElem =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcOperParams.u1VhtOperModeElem;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bVhtOption == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11AcOperParams.u1VhtOption =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11AcOperParams.u1VhtOption;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHTOption == OSIX_TRUE)
            {
                pGetRadioIfDB->RadioIfConfigParam.u1HTOption =
                    pRadioIfGetDB->RadioIfGetAllDB.u1HTOption;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTransPower == OSIX_TRUE)
            {
                pGetRadioIfDB->VhtTransmitPower.u1TransPower =
                    pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.
                    u1TransPower;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxTxPowerCount == OSIX_TRUE)
            {
                pGetRadioIfDB->VhtTransmitPower.u1MaxTxPowerCount =
                    pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.
                    u1MaxTxPowerCount;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxTxPowerUnit == OSIX_TRUE)
            {
                pGetRadioIfDB->VhtTransmitPower.u1MaxTxPowerUnit =
                    pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.
                    u1MaxTxPowerUnit;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxPower20 == OSIX_TRUE)
            {
                pGetRadioIfDB->VhtTransmitPower.u1TxPower20 =
                    pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.u1TxPower20;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxPower40 == OSIX_TRUE)
            {
                pGetRadioIfDB->VhtTransmitPower.u1TxPower40 =
                    pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.u1TxPower40;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxPower80 == OSIX_TRUE)
            {
                pGetRadioIfDB->VhtTransmitPower.u1TxPower80 =
                    pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.u1TxPower80;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxPower160 == OSIX_TRUE)
            {
                pGetRadioIfDB->VhtTransmitPower.u1TxPower160 =
                    pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.
                    u1TxPower160;
            }
            break;

        case WSS_ASSEMBLE_11AC_CAPABILITY_INFO:
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                 u1SuppMaxMPDULen & 0x3);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u4VhtCapInfo =
                (pRadioIfGetDB->RadioIfGetAllDB.
                 Dot11AcCapaParams.u4VhtCapInfo & (~(0x3))) | u1ACBit;

            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                 u1SuppRxLpdc & 0x1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u4VhtCapInfo =
                (pRadioIfGetDB->RadioIfGetAllDB.
                 Dot11AcCapaParams.u4VhtCapInfo & (~(0x10))) | u1ACBit << 4;

            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                 u1SuppShortGi80 & 0x1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u4VhtCapInfo =
                (pRadioIfGetDB->RadioIfGetAllDB.
                 Dot11AcCapaParams.u4VhtCapInfo & (~(0x20))) | u1ACBit << 5;

            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                 u1SuppTxStbc & 0x1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u4VhtCapInfo =
                (pRadioIfGetDB->RadioIfGetAllDB.
                 Dot11AcCapaParams.u4VhtCapInfo & (~(0x100))) | u1ACBit << 7;

            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                 u1SuppRxStbc & 0x7);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u4VhtCapInfo =
                (pRadioIfGetDB->RadioIfGetAllDB.
                 Dot11AcCapaParams.u4VhtCapInfo & (~(0x700))) | u1ACBit << 8;

            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                 u1SuppVhtMaxAMPDU & 0x7);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u4VhtCapInfo =
                (pRadioIfGetDB->RadioIfGetAllDB.
                 Dot11AcCapaParams.u4VhtCapInfo & (~(0x3800000))) | u1ACBit <<
                23;
            break;

        case WSS_ASSEMBLE_11AC_OPER_INFO:

            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.au1VhtOperInfo[0] =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.
                u1SuppVhtChannelWidth;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.au1VhtOperInfo[1] =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.
                u1SuppCenterFcy0;
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.au1VhtOperInfo[2] =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy1;

            break;

        case WSS_PARSE_11AC_CAPABILITY_INFO:
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppMaxMPDULen =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                 u4VhtCapInfo & 3);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppChanWidth =
                (((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                   u4VhtCapInfo) >> 2) & 3);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppRxLpdc =
                (((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                   u4VhtCapInfo) >> 4) & 1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppShortGi80 =
                (((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                   u4VhtCapInfo) >> 5) & 1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1ShortGi160 =
                (((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                   u4VhtCapInfo) >> 6) & 1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppTxStbc =
                (((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                   u4VhtCapInfo) >> 7) & 1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppRxStbc =
                (((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                   u4VhtCapInfo) >> 8) & 7);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuBeamFormer =
                (((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                   u4VhtCapInfo) >> 11) & 1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuBeamFormee =
                (((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                   u4VhtCapInfo) >> 12) & 1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppBeamFormAnt =
                (((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                   u4VhtCapInfo) >> 13) & 7);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SoundDimension =
                (((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                   u4VhtCapInfo) >> 16) & 7);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1MuBeamFormer =
                (((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                   u4VhtCapInfo) >> 19) & 1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1MuBeamFormee =
                (((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                   u4VhtCapInfo) >> 20) & 1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1VhtTxOpPs =
                (((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                   u4VhtCapInfo) >> 21) & 1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1HtcVhtCapable =
                (((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                   u4VhtCapInfo) >> 22) & 1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1SuppVhtMaxAMPDU =
                (((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                   u4VhtCapInfo) >> 23) & 7);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1VhtLinkAdaption =
                (((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                   u4VhtCapInfo) >> 26) & 3);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1VhtRxAntenna =
                (((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                   u4VhtCapInfo) >> 28) & 1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u1VhtTxAntenna =
                (((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                   u4VhtCapInfo) >> 29) & 1);
            break;

        case WSS_PARSE_11AC_OPER_INFO:
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.
                u1SuppVhtChannelWidth =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.
                au1VhtOperInfo[0];
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u1SuppCenterFcy0 =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.
                au1VhtOperInfo[1];
            pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy1 =
                pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.
                au1VhtOperInfo[2];
            break;

        case WSS_GET_RADIO_IF_DB_11N:
            RadioIfDB.u4VirtualRadioIfIndex =
                pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

            /* Get the corresponding node from RB Tree (RadioIfDb) */
            pGetRadioIfDB =
                RBTreeGet (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                           (tRBElem *) (&RadioIfDB));

            if (pGetRadioIfDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIfDBMsg : Entry not found, get failed \n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            /* Copying the values , which are needed */
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHtCapInfo == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u2SuppHtCapInfo =
                    pGetRadioIfDB->Dot11NCapaParams.u2SuppHtCapInfo;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHtCapInfo == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u2HtCapInfo =
                    pGetRadioIfDB->Dot11NCapaParams.u2HtCapInfo;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bLdpcCoding == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1LdpcCoding =
                    pGetRadioIfDB->Dot11NCapaParams.u1LdpcCoding;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppLdpcCoding == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppLdpcCoding =
                    pGetRadioIfDB->Dot11NCapaParams.u1SuppLdpcCoding;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bChannelWidth == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1ChannelWidth =
                    pGetRadioIfDB->Dot11NCapaParams.u1ChannelWidth;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppChannelWidth == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppChannelWidth =
                    pGetRadioIfDB->Dot11NCapaParams.u1SuppChannelWidth;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSmPowerSave == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SmPowerSave =
                    pGetRadioIfDB->Dot11NCapaParams.u1SmPowerSave;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppSmPowerSave == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppSmPowerSave =
                    pGetRadioIfDB->Dot11NCapaParams.u1SuppSmPowerSave;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHtGreenField == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1HtGreenField =
                    pGetRadioIfDB->Dot11NCapaParams.u1HtGreenField;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHtGreenField == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppHtGreenField =
                    pGetRadioIfDB->Dot11NCapaParams.u1SuppHtGreenField;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi20 == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1ShortGi20 =
                    pGetRadioIfDB->Dot11NCapaParams.u1ShortGi20;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppShortGi20 == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppShortGi20 =
                    pGetRadioIfDB->Dot11NCapaParams.u1SuppShortGi20;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi40 == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1ShortGi40 =
                    pGetRadioIfDB->Dot11NCapaParams.u1ShortGi40;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppShortGi40 == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppShortGi40 =
                    pGetRadioIfDB->Dot11NCapaParams.u1SuppShortGi40;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxStbc == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1TxStbc =
                    pGetRadioIfDB->Dot11NCapaParams.u1TxStbc;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTxStbc == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppTxStbc =
                    pGetRadioIfDB->Dot11NCapaParams.u1SuppTxStbc;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxStbc == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1RxStbc =
                    pGetRadioIfDB->Dot11NCapaParams.u1RxStbc;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRxStbc == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppRxStbc =
                    pGetRadioIfDB->Dot11NCapaParams.u1SuppRxStbc;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bDelayedBlockack == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1DelayedBlockack =
                    pGetRadioIfDB->Dot11NCapaParams.u1DelayedBlockack;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppDelayedBlockack ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppDelayedBlockack =
                    pGetRadioIfDB->Dot11NCapaParams.u1SuppDelayedBlockack;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxAmsduLen == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1MaxAmsduLen =
                    pGetRadioIfDB->Dot11NCapaParams.u1MaxAmsduLen;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMaxAmsduLen == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppMaxAmsduLen =
                    pGetRadioIfDB->Dot11NCapaParams.u1SuppMaxAmsduLen;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bDsssCck40 == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1DsssCck40 =
                    pGetRadioIfDB->Dot11NCapaParams.u1DsssCck40;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppDsssCck40 == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppDsssCck40 =
                    pGetRadioIfDB->Dot11NCapaParams.u1SuppDsssCck40;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bFortyInto == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1FortyInto =
                    pGetRadioIfDB->Dot11NCapaParams.u1FortyInto;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppFortyInto == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppFortyInto =
                    pGetRadioIfDB->Dot11NCapaParams.u1SuppFortyInto;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHtCapaMcs == OSIX_TRUE)
            {
                MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.
                        Dot11NsuppMcsParams.au1HtCapaMcs,
                        pGetRadioIfDB->Dot11NsuppMcsParams.au1HtCapaMcs,
                        sizeof (pRadioIfGetDB->RadioIfGetAllDB.
                                Dot11NsuppMcsParams.au1HtCapaMcs));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHtCapaMcs == OSIX_TRUE)
            {
                MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.
                        Dot11NsuppMcsParams.au1SuppHtCapaMcs,
                        pGetRadioIfDB->Dot11NsuppMcsParams.au1SuppHtCapaMcs,
                        sizeof (pRadioIfGetDB->RadioIfGetAllDB.
                                Dot11NsuppMcsParams.au1SuppHtCapaMcs));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxMcsBitmask == OSIX_TRUE)
            {
                MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.
                        Dot11NsuppMcsParams.au1RxMcsBitmask,
                        pGetRadioIfDB->Dot11NsuppMcsParams.au1RxMcsBitmask,
                        sizeof (pRadioIfGetDB->RadioIfGetAllDB.
                                Dot11NsuppMcsParams.au1RxMcsBitmask));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRxMcsBitmask == OSIX_TRUE)
            {
                MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.
                        Dot11NsuppMcsParams.au1SuppRxMcsBitmask,
                        pGetRadioIfDB->Dot11NsuppMcsParams.au1SuppRxMcsBitmask,
                        sizeof (pRadioIfGetDB->RadioIfGetAllDB.
                                Dot11NsuppMcsParams.au1SuppRxMcsBitmask));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHighSuppDataRate == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NsuppMcsParams.u2SuppHighSuppDataRate =
                    pGetRadioIfDB->Dot11NsuppMcsParams.u2SuppHighSuppDataRate;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHighDataRate == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NsuppMcsParams.u2HighSuppDataRate =
                    pGetRadioIfDB->Dot11NsuppMcsParams.u2HighSuppDataRate;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxMcsSetDefined == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NsuppMcsParams.u1TxMcsSetDefined =
                    pGetRadioIfDB->Dot11NsuppMcsParams.u1TxMcsSetDefined;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTxMcsSetDefined ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NsuppMcsParams.u1SuppTxMcsSetDefined =
                    pGetRadioIfDB->Dot11NsuppMcsParams.u1SuppTxMcsSetDefined;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxRxMcsSetNotEqual ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NsuppMcsParams.u1TxRxMcsSetNotEqual =
                    pGetRadioIfDB->Dot11NsuppMcsParams.u1TxRxMcsSetNotEqual;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTxRxMcsSetNotEqual ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NsuppMcsParams.u1SuppTxRxMcsSetNotEqual =
                    pGetRadioIfDB->Dot11NsuppMcsParams.u1SuppTxRxMcsSetNotEqual;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxMaxNoSpatStrm == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NsuppMcsParams.u1TxMaxNoSpatStrm =
                    pGetRadioIfDB->Dot11NsuppMcsParams.u1TxMaxNoSpatStrm;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTxMaxNoSpatStrm ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NsuppMcsParams.u1SuppTxMaxNoSpatStrm =
                    pGetRadioIfDB->Dot11NsuppMcsParams.u1SuppTxMaxNoSpatStrm;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxUnequalModSupp == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NsuppMcsParams.u1TxUnequalModSupp =
                    pGetRadioIfDB->Dot11NsuppMcsParams.u1TxUnequalModSupp;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTxUnequalModSupp ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NsuppMcsParams.u1SuppTxUnequalModSupp =
                    pGetRadioIfDB->Dot11NsuppMcsParams.u1SuppTxUnequalModSupp;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppAmpduParam == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NampduParams.u1SuppAmpduParam =
                    pGetRadioIfDB->Dot11NampduParams.u1SuppAmpduParam;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAmpduParam == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NampduParams.u1AmpduParam =
                    pGetRadioIfDB->Dot11NampduParams.u1AmpduParam;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxAmpduLen == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NampduParams.u1MaxAmpduLen =
                    pGetRadioIfDB->Dot11NampduParams.u1MaxAmpduLen;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMaxAmpduLen == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NampduParams.u1SuppMaxAmpduLen =
                    pGetRadioIfDB->Dot11NampduParams.u1SuppMaxAmpduLen;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMinAmpduStart == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NampduParams.u1MinAmpduStart =
                    pGetRadioIfDB->Dot11NampduParams.u1MinAmpduStart;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMinAmpduStart ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NampduParams.u1SuppMinAmpduStart =
                    pGetRadioIfDB->Dot11NampduParams.u1SuppMinAmpduStart;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHtExtCap == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u2HtExtCap =
                    pGetRadioIfDB->Dot11NhtExtCapParams.u2HtExtCap;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHtExtCap == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u2SuppHtExtCap =
                    pGetRadioIfDB->Dot11NhtExtCapParams.u2SuppHtExtCap;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bPco == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u1Pco =
                    pGetRadioIfDB->Dot11NhtExtCapParams.u1Pco;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppPco == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u1SuppPco =
                    pGetRadioIfDB->Dot11NhtExtCapParams.u1SuppPco;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bPcoTransTime == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u1PcoTransTime =
                    pGetRadioIfDB->Dot11NhtExtCapParams.u1PcoTransTime;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppPcoTransTime == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u1SuppPcoTransTime =
                    pGetRadioIfDB->Dot11NhtExtCapParams.u1SuppPcoTransTime;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMcsFeedback == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u1McsFeedback =
                    pGetRadioIfDB->Dot11NhtExtCapParams.u1McsFeedback;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMcsFeedback == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u1SuppMcsFeedback =
                    pGetRadioIfDB->Dot11NhtExtCapParams.u1SuppMcsFeedback;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHtcSupp == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u1HtcSupp =
                    pGetRadioIfDB->Dot11NhtExtCapParams.u1HtcSupp;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHtcSupp == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u1SuppHtcSupp =
                    pGetRadioIfDB->Dot11NhtExtCapParams.u1SuppHtcSupp;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRdResponder == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u1SuppRdResponder =
                    pGetRadioIfDB->Dot11NhtExtCapParams.u1SuppRdResponder;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRdResponder == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u1RdResponder =
                    pGetRadioIfDB->Dot11NhtExtCapParams.u1RdResponder;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTxBeamCapParam ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u4SuppTxBeamCapParam =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.u4SuppTxBeamCapParam;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxBeamCapParam == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u4TxBeamCapParam =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.u4TxBeamCapParam;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppImpTranBeamRecCapable ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppImpTranBeamRecCapable =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppImpTranBeamRecCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bImpTranBeamRecCapable ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1ImpTranBeamRecCapable =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1ImpTranBeamRecCapable;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRecStagSoundCapable ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppRecStagSoundCapable =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppRecStagSoundCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRecStagSoundCapable ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1RecStagSoundCapable =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1RecStagSoundCapable;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTransStagSoundCapable ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppTransStagSoundCapable =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppTransStagSoundCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTransStagSoundCapable ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1TransStagSoundCapable =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1TransStagSoundCapable;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRecNdpCapable ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppRecNdpCapable =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppRecNdpCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRecNdpCapable == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1RecNdpCapable =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1RecNdpCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTransNdpCapable ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppTransNdpCapable =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppTransNdpCapable;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTransNdpCapable == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1TransNdpCapable =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1TransNdpCapable;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppImpTranBeamCapable ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppImpTranBeamCapable =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppImpTranBeamCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bImpTranBeamCapable ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1ImpTranBeamCapable =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1ImpTranBeamCapable;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppCalibration == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppCalibration =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppCalibration;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCalibration == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1Calibration =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1Calibration;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppExplCsiTranBeamCapable ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppExplCsiTranBeamCapable =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppExplCsiTranBeamCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bExplCsiTranBeamCapable ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1ExplCsiTranBeamCapable =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1ExplCsiTranBeamCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bExplNoncompSteCapable ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1ExplNoncompSteCapable =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1ExplNoncompSteCapable;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppExplNoncompSteCapable ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppExplNoncompSteCapable =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppExplNoncompSteCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bExplCompSteCapable ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1ExplCompSteCapable =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1ExplCompSteCapable;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppExplCompSteCapable ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppExplCompSteCapable =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppExplCompSteCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bExplTranBeamCsiFeedback ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1ExplTranBeamCsiFeedback =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1ExplTranBeamCsiFeedback;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppExplTranBeamCsiFeedback ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppExplTranBeamCsiFeedback =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppExplTranBeamCsiFeedback;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bExplNoncompBeamFbCapable ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1ExplNoncompBeamFbCapable =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1ExplNoncompBeamFbCapable;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.
                bSuppExplNoncompBeamFbCapable == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppExplNoncompBeamFbCapable =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppExplNoncompBeamFbCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bExplCompBeamFbCapable ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1ExplCompBeamFbCapable =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1ExplCompBeamFbCapable;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppExplCompBeamFbCapable ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppExplCompBeamFbCapable =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppExplCompBeamFbCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMinGrouping == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1MinGrouping =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1MinGrouping;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMinGrouping == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppMinGrouping =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppMinGrouping;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCsiNoofBeamAntSupported ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1CsiNoofBeamAntSupported =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1CsiNoofBeamAntSupported;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppCsiNoofBeamAntSupported ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppCsiNoofBeamAntSupported =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppCsiNoofBeamAntSupported;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bNoncompSteNoBeamformer ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1NoncompSteNoBeamformerAntSupp =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1NoncompSteNoBeamformerAntSupp;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppNoncompSteNoBeamformer ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppNoncompSteNoBeamformer =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppNoncompSteNoBeamformer;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCompSteNoBeamformer ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1CompSteNoBeamformerAntSupp =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1CompSteNoBeamformerAntSupp;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppCompSteNoBeamformer ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppCompSteNoBeamformerAntSupp =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppCompSteNoBeamformerAntSupp;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCSIMaxNoRowsBeamformer ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1CSIMaxNoRowsBeamformerSupp =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1CSIMaxNoRowsBeamformerSupp;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppCSIMaxNoRowsBeamformer ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppCSIMaxNoRowsBeamformerSupp =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppCSIMaxNoRowsBeamformerSupp;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bChannelEstCapability ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1ChannelEstCapability =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1ChannelEstCapability;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppChannelEstCapability ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppChannelEstCapability =
                    pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppChannelEstCapability;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bPrimaryChannel == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1PrimaryChannel =
                    pGetRadioIfDB->Dot11NhtOperation.u1PrimaryChannel;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSecondaryChannel == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SecondaryChannel =
                    pGetRadioIfDB->Dot11NhtOperation.u1SecondaryChannel;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppSecondaryChannel ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppSecondaryChannel =
                    pGetRadioIfDB->Dot11NhtOperation.u1SuppSecondaryChannel;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bStaChanWidth == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1StaChanWidth =
                    pGetRadioIfDB->Dot11NhtOperation.u1StaChanWidth;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppStaChanWidth == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppStaChanWidth =
                    pGetRadioIfDB->Dot11NhtOperation.u1SuppStaChanWidth;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRifsMode == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1RifsMode =
                    pGetRadioIfDB->Dot11NhtOperation.u1RifsMode;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRifsMode == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppRifsMode =
                    pGetRadioIfDB->Dot11NhtOperation.u1SuppRifsMode;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHtProtection == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1HtProtection =
                    pGetRadioIfDB->Dot11NhtOperation.u1HtProtection;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHtProtection == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppHtProtection =
                    pGetRadioIfDB->Dot11NhtOperation.u1SuppHtProtection;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bNongfHtStasPresent ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1NongfHtStasPresent =
                    pGetRadioIfDB->Dot11NhtOperation.u1NongfHtStasPresent;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppNongfHtStasPresent ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppNongfHtStasPresent =
                    pGetRadioIfDB->Dot11NhtOperation.u1SuppNongfHtStasPresent;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bObssNonHtStasPresent ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1ObssNonHtStasPresent =
                    pGetRadioIfDB->Dot11NhtOperation.u1ObssNonHtStasPresent;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppObssNonHtStasPresent ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppObssNonHtStasPresent =
                    pGetRadioIfDB->Dot11NhtOperation.u1SuppObssNonHtStasPresent;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bDualBeacon == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1DualBeacon =
                    pGetRadioIfDB->Dot11NhtOperation.u1DualBeacon;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppDualBeacon == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppDualBeacon =
                    pGetRadioIfDB->Dot11NhtOperation.u1SuppDualBeacon;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bDualCtsProtection ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1DualCtsProtection =
                    pGetRadioIfDB->Dot11NhtOperation.u1DualCtsProtection;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppDualCtsProtection ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppDualCtsProtection =
                    pGetRadioIfDB->Dot11NhtOperation.u1SuppDualCtsProtection;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bStbcBeacon == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1StbcBeacon =
                    pGetRadioIfDB->Dot11NhtOperation.u1StbcBeacon;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppStbcBeacon == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppStbcBeacon =
                    pGetRadioIfDB->Dot11NhtOperation.u1SuppStbcBeacon;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bLsigTxopProtFulSup ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1LsigTxopProtFulSup =
                    pGetRadioIfDB->Dot11NCapaParams.u1LsigTxopProtFulSup;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppLsigTxopProtFulSup ==
                OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppLsigTxopProtFulSup =
                    pGetRadioIfDB->Dot11NCapaParams.u1SuppLsigTxopProtFulSup;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bPcoActive == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1PcoActive =
                    pGetRadioIfDB->Dot11NhtOperation.u1PcoActive;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppPcoActive == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppPcoActive =
                    pGetRadioIfDB->Dot11NhtOperation.u1SuppPcoActive;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bPcoPhase == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1PcoPhase =
                    pGetRadioIfDB->Dot11NhtOperation.u1PcoPhase;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppPcoPhase == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppPcoPhase =
                    pGetRadioIfDB->Dot11NhtOperation.u1SuppPcoPhase;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHTOpeInfo == OSIX_TRUE)
            {
                MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                        au1HTOpeInfo,
                        pGetRadioIfDB->Dot11NhtOperation.au1HTOpeInfo,
                        sizeof (pRadioIfGetDB->RadioIfGetAllDB.
                                Dot11NhtOperation.au1HTOpeInfo));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHTOpeInfo == OSIX_TRUE)
            {
                MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                        au1SuppHTOpeInfo,
                        pGetRadioIfDB->Dot11NhtOperation.au1SuppHTOpeInfo,
                        sizeof (pRadioIfGetDB->RadioIfGetAllDB.
                                Dot11NhtOperation.au1SuppHTOpeInfo));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppBasicMCSSet == OSIX_TRUE)
            {
                MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.
                        Dot11NhtOperation.au1SuppBasicMCSSet,
                        pGetRadioIfDB->Dot11NhtOperation.au1SuppBasicMCSSet,
                        sizeof (pRadioIfGetDB->RadioIfGetAllDB.
                                Dot11NhtOperation.au1SuppBasicMCSSet));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAMPDUStatus == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1AMPDUStatus =
                    pGetRadioIfDB->Dot11NCapaParams.u1AMPDUStatus;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAMPDUSubFrame == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1AMPDUSubFrame =
                    pGetRadioIfDB->Dot11NCapaParams.u1AMPDUSubFrame;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAMPDULimit == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u2AMPDULimit =
                    pGetRadioIfDB->Dot11NCapaParams.u2AMPDULimit;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAMSDUStatus == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u1AMSDUStatus =
                    pGetRadioIfDB->Dot11NCapaParams.u1AMSDUStatus;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAMSDULimit == OSIX_TRUE)
            {
                pRadioIfGetDB->RadioIfGetAllDB.u2AMSDULimit =
                    pGetRadioIfDB->Dot11NCapaParams.u2AMSDULimit;
            }

            break;

        case WSS_SET_RADIO_IF_DB_11N:
            RadioIfDB.u4VirtualRadioIfIndex =
                pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;

            /* Get the corresponding node from RB Tree (RadioIfDb) */
            pGetRadioIfDB =
                RBTreeGet (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                           (tRBElem *) (&RadioIfDB));

            if (pGetRadioIfDB == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "RadioIfDBMsg: Entry not exist, set failed\n");
                WSSIF_UNLOCK;
                return OSIX_FAILURE;
            }

            /* Copying the values , which are needed */
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHtCapInfo == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u2SuppHtCapInfo =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u2SuppHtCapInfo;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHtCapInfo == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u2HtCapInfo =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppSuppHtCapInfo ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u2SuppHtCapInfo =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u2SuppHtCapInfo;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bLdpcCoding == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1LdpcCoding =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1LdpcCoding;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppLdpcCoding == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1SuppLdpcCoding =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppLdpcCoding;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bChannelWidth == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1ChannelWidth =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1ChannelWidth;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppChannelWidth == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1SuppChannelWidth =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppChannelWidth;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSmPowerSave == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1SmPowerSave =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SmPowerSave;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppSmPowerSave == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1SuppSmPowerSave =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppSmPowerSave;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHtGreenField == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1HtGreenField =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1HtGreenField;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHtGreenField == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1SuppHtGreenField =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppHtGreenField;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi20 == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1ShortGi20 =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1ShortGi20;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppShortGi20 == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1SuppShortGi20 =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppShortGi20;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bShortGi40 == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1ShortGi40 =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1ShortGi40;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppShortGi40 == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1SuppShortGi40 =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppShortGi40;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxStbc == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1TxStbc =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1TxStbc;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTxStbc == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1SuppTxStbc =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppTxStbc;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxStbc == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1RxStbc =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1RxStbc;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRxStbc == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1SuppRxStbc =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppRxStbc;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bDelayedBlockack == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1DelayedBlockack =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1DelayedBlockack;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppDelayedBlockack ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1SuppDelayedBlockack =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppDelayedBlockack;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxAmsduLen == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1MaxAmsduLen =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1MaxAmsduLen;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMaxAmsduLen == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1SuppMaxAmsduLen =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppMaxAmsduLen;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bDsssCck40 == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1DsssCck40 =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1DsssCck40;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppDsssCck40 == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1SuppDsssCck40 =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppDsssCck40;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bFortyInto == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1FortyInto =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1FortyInto;
            }
            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppFortyInto == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1SuppFortyInto =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppFortyInto;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHtCapaMcs == OSIX_TRUE)
            {
                MEMCPY (pGetRadioIfDB->Dot11NsuppMcsParams.au1HtCapaMcs,
                        pRadioIfGetDB->RadioIfGetAllDB.
                        Dot11NsuppMcsParams.au1HtCapaMcs,
                        sizeof (pGetRadioIfDB->Dot11NsuppMcsParams.
                                au1HtCapaMcs));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHtCapaMcs == OSIX_TRUE)
            {
                MEMCPY (pGetRadioIfDB->Dot11NsuppMcsParams.au1SuppHtCapaMcs,
                        pRadioIfGetDB->RadioIfGetAllDB.
                        Dot11NsuppMcsParams.au1SuppHtCapaMcs,
                        sizeof (pGetRadioIfDB->Dot11NsuppMcsParams.
                                au1SuppHtCapaMcs));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRxMcsBitmask == OSIX_TRUE)
            {
                MEMCPY (pGetRadioIfDB->Dot11NsuppMcsParams.au1RxMcsBitmask,
                        pRadioIfGetDB->RadioIfGetAllDB.
                        Dot11NsuppMcsParams.au1RxMcsBitmask,
                        sizeof (pGetRadioIfDB->Dot11NsuppMcsParams.
                                au1RxMcsBitmask));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRxMcsBitmask == OSIX_TRUE)
            {
                MEMCPY (pGetRadioIfDB->Dot11NsuppMcsParams.au1SuppRxMcsBitmask,
                        pRadioIfGetDB->RadioIfGetAllDB.
                        Dot11NsuppMcsParams.au1SuppRxMcsBitmask,
                        sizeof (pGetRadioIfDB->Dot11NsuppMcsParams.
                                au1SuppRxMcsBitmask));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHighSuppDataRate == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NsuppMcsParams.u2HighSuppDataRate =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NsuppMcsParams.u2HighSuppDataRate;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHighSuppDataRate ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NsuppMcsParams.u2SuppHighSuppDataRate =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NsuppMcsParams.u2SuppHighSuppDataRate;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxMcsSetDefined == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NsuppMcsParams.u1TxMcsSetDefined =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NsuppMcsParams.u1TxMcsSetDefined;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTxMcsSetDefined ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NsuppMcsParams.u1SuppTxMcsSetDefined =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NsuppMcsParams.u1SuppTxMcsSetDefined;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxRxMcsSetNotEqual ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NsuppMcsParams.u1TxRxMcsSetNotEqual =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NsuppMcsParams.u1TxRxMcsSetNotEqual;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTxRxMcsSetNotEqual ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NsuppMcsParams.u1SuppTxRxMcsSetNotEqual =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NsuppMcsParams.u1SuppTxRxMcsSetNotEqual;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxMaxNoSpatStrm == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NsuppMcsParams.u1TxMaxNoSpatStrm =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NsuppMcsParams.u1TxMaxNoSpatStrm;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTxMaxNoSpatStrm ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NsuppMcsParams.u1SuppTxMaxNoSpatStrm =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NsuppMcsParams.u1SuppTxMaxNoSpatStrm;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxUnequalModSupp == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NsuppMcsParams.u1TxUnequalModSupp =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NsuppMcsParams.u1TxUnequalModSupp;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTxUnequalModSupp ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NsuppMcsParams.u1SuppTxUnequalModSupp =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NsuppMcsParams.u1SuppTxUnequalModSupp;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppAmpduParam == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NampduParams.u1SuppAmpduParam =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NampduParams.u1SuppAmpduParam;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAmpduParam == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NampduParams.u1AmpduParam =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NampduParams.u1AmpduParam;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMaxAmpduLen == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NampduParams.u1MaxAmpduLen =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NampduParams.u1MaxAmpduLen;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMaxAmpduLen == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NampduParams.u1SuppMaxAmpduLen =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NampduParams.u1SuppMaxAmpduLen;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMinAmpduStart == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NampduParams.u1MinAmpduStart =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NampduParams.u1MinAmpduStart;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMinAmpduStart ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NampduParams.u1SuppMinAmpduStart =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NampduParams.u1SuppMinAmpduStart;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHtExtCap == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtExtCapParams.u2HtExtCap =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u2HtExtCap;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHtExtCap == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtExtCapParams.u2SuppHtExtCap =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u2SuppHtExtCap;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bPco == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtExtCapParams.u1Pco =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u1Pco;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppPco == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtExtCapParams.u1SuppPco =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u1SuppPco;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bPcoTransTime == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtExtCapParams.u1PcoTransTime =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u1PcoTransTime;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppPcoTransTime == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtExtCapParams.u1SuppPcoTransTime =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u1SuppPcoTransTime;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMcsFeedback == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtExtCapParams.u1McsFeedback =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u1McsFeedback;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMcsFeedback == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtExtCapParams.u1SuppMcsFeedback =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u1SuppMcsFeedback;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHtcSupp == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtExtCapParams.u1HtcSupp =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u1HtcSupp;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHtcSupp == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtExtCapParams.u1SuppHtcSupp =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u1SuppHtcSupp;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRdResponder == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtExtCapParams.u1RdResponder =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u1RdResponder;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRdResponder == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtExtCapParams.u1SuppRdResponder =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u1SuppRdResponder;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTxBeamCapParam ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.u4SuppTxBeamCapParam =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u4SuppTxBeamCapParam;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTxBeamCapParam == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.u4TxBeamCapParam =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u4TxBeamCapParam;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bImpTranBeamRecCapable ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1ImpTranBeamRecCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1ImpTranBeamRecCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppImpTranBeamRecCapable ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppImpTranBeamRecCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1SuppImpTranBeamRecCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRecStagSoundCapable ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1RecStagSoundCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1RecStagSoundCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRecStagSoundCapable ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppRecStagSoundCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1SuppRecStagSoundCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTransStagSoundCapable ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1TransStagSoundCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1TransStagSoundCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTransStagSoundCapable ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppTransStagSoundCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1SuppTransStagSoundCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRecNdpCapable == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1RecNdpCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1RecNdpCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRecNdpCapable ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppRecNdpCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppRecNdpCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bTransNdpCapable == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1TransNdpCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1TransNdpCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppTransNdpCapable ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppTransNdpCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppTransNdpCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bImpTranBeamCapable ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1ImpTranBeamCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1ImpTranBeamCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppImpTranBeamCapable ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppImpTranBeamCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1SuppImpTranBeamCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCalibration == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1Calibration =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1Calibration;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppCalibration == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppCalibration =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppCalibration;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bExplCsiTranBeamCapable ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1ExplCsiTranBeamCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplCsiTranBeamCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppExplCsiTranBeamCapable ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppExplCsiTranBeamCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1SuppExplCsiTranBeamCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bExplNoncompSteCapable ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1ExplNoncompSteCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1ExplNoncompSteCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppExplNoncompSteCapable ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppExplNoncompSteCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1SuppExplNoncompSteCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bExplCompSteCapable ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1ExplCompSteCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1ExplCompSteCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppExplCompSteCapable ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppExplCompSteCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1SuppExplCompSteCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bExplTranBeamCsiFeedback ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1ExplTranBeamCsiFeedback =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplTranBeamCsiFeedback;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppExplTranBeamCsiFeedback ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppExplTranBeamCsiFeedback =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1SuppExplTranBeamCsiFeedback;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bExplNoncompBeamFbCapable ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1ExplNoncompBeamFbCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1ExplNoncompBeamFbCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.
                bSuppExplNoncompBeamFbCapable == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppExplNoncompBeamFbCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1SuppExplNoncompBeamFbCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bExplCompBeamFbCapable ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1ExplCompBeamFbCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1ExplCompBeamFbCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppExplCompBeamFbCapable ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppExplCompBeamFbCapable =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1SuppExplCompBeamFbCapable;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bMinGrouping == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1MinGrouping =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1MinGrouping;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppMinGrouping == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppMinGrouping =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1SuppMinGrouping;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCsiNoofBeamAntSupported ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1CsiNoofBeamAntSupported =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1CsiNoofBeamAntSupported;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppCsiNoofBeamAntSupported ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppCsiNoofBeamAntSupported =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1SuppCsiNoofBeamAntSupported;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bNoncompSteNoBeamformer ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1NoncompSteNoBeamformerAntSupp =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1NoncompSteNoBeamformerAntSupp;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppNoncompSteNoBeamformer ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppNoncompSteNoBeamformer =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1SuppNoncompSteNoBeamformer;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCompSteNoBeamformer ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1CompSteNoBeamformerAntSupp =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1CompSteNoBeamformerAntSupp;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppCompSteNoBeamformer ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppCompSteNoBeamformerAntSupp =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1SuppCompSteNoBeamformerAntSupp;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bCSIMaxNoRowsBeamformer ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1CSIMaxNoRowsBeamformerSupp =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1CSIMaxNoRowsBeamformerSupp;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppCSIMaxNoRowsBeamformer ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppCSIMaxNoRowsBeamformerSupp =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1SuppCSIMaxNoRowsBeamformerSupp;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bChannelEstCapability ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.u1ChannelEstCapability =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtTxBeamCapParams.u1ChannelEstCapability;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppChannelEstCapability ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtTxBeamCapParams.
                    u1SuppChannelEstCapability =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                    u1SuppChannelEstCapability;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bPrimaryChannel == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1PrimaryChannel =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1PrimaryChannel;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppPrimaryChannel ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1SuppPrimaryChannel =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppPrimaryChannel;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSecondaryChannel == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1SecondaryChannel =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SecondaryChannel;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppSecondaryChannel ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1SuppSecondaryChannel =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppSecondaryChannel;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bStaChanWidth == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1StaChanWidth =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1StaChanWidth;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppStaChanWidth == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1SuppStaChanWidth =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppStaChanWidth;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bRifsMode == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1RifsMode =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.u1RifsMode;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppRifsMode == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1SuppRifsMode =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppRifsMode;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHtProtection == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1HtProtection =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1HtProtection;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHtProtection == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1SuppHtProtection =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppHtProtection;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bNongfHtStasPresent ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1NongfHtStasPresent =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1NongfHtStasPresent;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppNongfHtStasPresent ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1SuppNongfHtStasPresent =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppNongfHtStasPresent;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bObssNonHtStasPresent ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1ObssNonHtStasPresent =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1ObssNonHtStasPresent;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppObssNonHtStasPresent ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1SuppObssNonHtStasPresent =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppObssNonHtStasPresent;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bDualBeacon == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1DualBeacon =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1DualBeacon;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppDualBeacon == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1SuppDualBeacon =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppDualBeacon;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bDualCtsProtection ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1DualCtsProtection =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1DualCtsProtection;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppDualCtsProtection ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1SuppDualCtsProtection =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppDualCtsProtection;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bStbcBeacon == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1StbcBeacon =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1StbcBeacon;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppStbcBeacon == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1SuppStbcBeacon =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppStbcBeacon;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bLsigTxopProtFulSup ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1LsigTxopProtFulSup =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1LsigTxopProtFulSup;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppLsigTxopProtFulSup ==
                OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1SuppLsigTxopProtFulSup =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u1SuppLsigTxopProtFulSup;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bPcoActive == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1PcoActive =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1PcoActive;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppPcoActive == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1SuppPcoActive =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppPcoActive;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bPcoPhase == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1PcoPhase =
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.u1PcoPhase;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppPcoPhase == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NhtOperation.u1SuppPcoPhase =
                    pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtOperation.u1SuppPcoPhase;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bHTOpeInfo == OSIX_TRUE)
            {
                MEMCPY (pGetRadioIfDB->Dot11NhtOperation.au1HTOpeInfo,
                        pRadioIfGetDB->RadioIfGetAllDB.
                        Dot11NhtOperation.au1HTOpeInfo,
                        sizeof (pGetRadioIfDB->Dot11NhtOperation.au1HTOpeInfo));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppHTOpeInfo == OSIX_TRUE)
            {
                MEMCPY (pGetRadioIfDB->Dot11NhtOperation.au1SuppHTOpeInfo,
                        pRadioIfGetDB->RadioIfGetAllDB.
                        Dot11NhtOperation.au1SuppHTOpeInfo,
                        sizeof (pGetRadioIfDB->Dot11NhtOperation.
                                au1SuppHTOpeInfo));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bBasicMCSSet == OSIX_TRUE)
            {
                MEMCPY (pGetRadioIfDB->Dot11NhtOperation.au1BasicMCSSet,
                        pRadioIfGetDB->RadioIfGetAllDB.
                        Dot11NhtOperation.au1BasicMCSSet,
                        sizeof (pGetRadioIfDB->Dot11NhtOperation.
                                au1BasicMCSSet));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bSuppBasicMCSSet == OSIX_TRUE)
            {
                MEMCPY (pGetRadioIfDB->Dot11NhtOperation.au1SuppBasicMCSSet,
                        pRadioIfGetDB->RadioIfGetAllDB.
                        Dot11NhtOperation.au1SuppBasicMCSSet,
                        sizeof (pGetRadioIfDB->Dot11NhtOperation.
                                au1SuppBasicMCSSet));
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAMPDUStatus == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1AMPDUStatus =
                    pRadioIfGetDB->RadioIfGetAllDB.u1AMPDUStatus;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAMPDUSubFrame == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1AMPDUSubFrame =
                    pRadioIfGetDB->RadioIfGetAllDB.u1AMPDUSubFrame;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAMPDULimit == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u2AMPDULimit =
                    pRadioIfGetDB->RadioIfGetAllDB.u2AMPDULimit;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAMSDUStatus == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u1AMSDUStatus =
                    pRadioIfGetDB->RadioIfGetAllDB.u1AMSDUStatus;
            }

            if (pRadioIfGetDB->RadioIfIsGetAllDB.bAMSDULimit == OSIX_TRUE)
            {
                pGetRadioIfDB->Dot11NCapaParams.u2AMSDULimit =
                    pRadioIfGetDB->RadioIfGetAllDB.u2AMSDULimit;
            }

            break;

        case WSS_ASSEMBLE_11N_CAPABILITY_INFO:
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                 u1SuppLdpcCoding & RADIO_VALUE_1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo =
                (UINT2) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                          u2HtCapInfo & (~(0x01))) | u1ACBit);
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                 u1SuppChannelWidth & RADIO_VALUE_1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo =
                (UINT2) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                          u2HtCapInfo & (~(0x2))) | (u1ACBit <<
                                                     RADIO_SHIFT_BIT1));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                 u1SuppSmPowerSave & RADIO_MASK_BIT3);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo =
                (UINT2) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                          u2HtCapInfo & (~(0xc))) | (u1ACBit <<
                                                     RADIO_SHIFT_BIT2));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                 u1SuppHtGreenField & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo =
                (UINT2) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                          u2HtCapInfo & (~(0x10))) | (u1ACBit <<
                                                      RADIO_SHIFT_BIT4));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                 u1SuppShortGi20 & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo =
                (UINT2) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                          u2HtCapInfo & (~(0x20))) | (u1ACBit <<
                                                      RADIO_SHIFT_BIT5));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                 u1SuppShortGi40 & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo =
                (UINT2) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                          u2HtCapInfo & (~(0x40))) | (u1ACBit <<
                                                      RADIO_SHIFT_BIT6));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                 u1SuppTxStbc & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo =
                (UINT2) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                          u2HtCapInfo & (~(0x80))) | (u1ACBit <<
                                                      RADIO_SHIFT_BIT7));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                 u1SuppRxStbc & RADIO_MASK_BIT3);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo =
                (UINT2) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                          u2HtCapInfo & (~(0x300))) | (u1ACBit <<
                                                       RADIO_SHIFT_BIT8));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                 u1SuppDelayedBlockack & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo =
                (UINT2) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                          u2HtCapInfo & (~(0x400))) | (u1ACBit <<
                                                       RADIO_SHIFT_BIT10));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                 u1SuppMaxAmsduLen & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo =
                (UINT2) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                          u2HtCapInfo & (~(0x800))) | (u1ACBit <<
                                                       RADIO_SHIFT_BIT11));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                 u1SuppDsssCck40 & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo =
                (UINT2) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                          u2HtCapInfo & (~(0x1000))) | (u1ACBit <<
                                                        RADIO_SHIFT_BIT12));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                 u1SuppFortyInto & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo =
                (UINT2) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                          u2HtCapInfo & (~(0x4000))) | (u1ACBit <<
                                                        RADIO_SHIFT_BIT14));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                 u1SuppLsigTxopProtFulSup & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u2HtCapInfo =
                (UINT2) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                          u2HtCapInfo & (~(0x8000))) | (u1ACBit <<
                                                        RADIO_SHIFT_BIT15));
            break;

        case WSS_ASSEMBLE_11N_AMPDU_INFO:
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.
                 u1SuppMaxAmpduLen & RADIO_MASK_BIT3);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.u1AmpduParam =
                (UINT1) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.
                          u1AmpduParam & (~(0x3))) | u1ACBit);
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.
                 u1SuppMinAmpduStart & RADIO_MASK_BIT7);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.u1AmpduParam =
                (UINT1) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.
                          u1AmpduParam & (~(0x1C))) | (u1ACBit <<
                                                       RADIO_SHIFT_BIT2));
            break;

        case WSS_ASSEMBLE_11N_MCS_INFO:
            MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                    au1HtCapaMcs,
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                    au1SuppRxMcsBitmask,
                    sizeof (pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                            au1SuppRxMcsBitmask));
            MEMCPY (&pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                    au1HtCapaMcs[10],
                    &pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                    u2SuppHighSuppDataRate,
                    sizeof (pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                            u2SuppHighSuppDataRate));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                 u1SuppTxMcsSetDefined & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1HtCapaMcs[12] |= u1ACBit;

            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                 u1SuppTxRxMcsSetNotEqual & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1HtCapaMcs[12] =
                (UINT1) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                          au1HtCapaMcs[12] & (~(0x2))) | (u1ACBit <<
                                                          RADIO_SHIFT_BIT1));

            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                 u1SuppTxMaxNoSpatStrm & RADIO_MASK_BIT3);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1HtCapaMcs[12] =
                (UINT1) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                          au1HtCapaMcs[12] & (~(0xC))) | (u1ACBit <<
                                                          RADIO_SHIFT_BIT2));

            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                 u1SuppTxUnequalModSupp & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                au1HtCapaMcs[12] =
                (UINT1) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                          au1HtCapaMcs[12] & (~(0xC))) | (u1ACBit <<
                                                          RADIO_SHIFT_BIT4));

            break;
        case WSS_ASSEMBLE_11N_EXTCAP_INFO:
            u1ACBit =
                ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                  u1Pco & RADIO_MASK_BIT1));
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u2HtExtCap =
                (UINT2) (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                         u2HtExtCap | u1ACBit);
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                 u1SuppPcoTransTime & RADIO_MASK_BIT3);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u2HtExtCap =
                (UINT2) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                          u2HtExtCap & (~(0x6))) | (u1ACBit <<
                                                    RADIO_SHIFT_BIT1));

            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                 u1SuppMcsFeedback & RADIO_MASK_BIT3);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u2HtExtCap =
                (UINT2) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                          u2HtExtCap & (~(0x18))) | (u1ACBit <<
                                                     RADIO_SHIFT_BIT3));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                 u1SuppHtcSupp & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u2HtExtCap =
                (UINT2) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                          u2HtExtCap & (~(0x20))) | (u1ACBit <<
                                                     RADIO_SHIFT_BIT5));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                 u1SuppRdResponder & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u2HtExtCap =
                (UINT2) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.
                          u2HtExtCap & (~(0x40))) | (u1ACBit <<
                                                     RADIO_SHIFT_BIT6));
            break;

        case WSS_ASSEMBLE_11N_BEAMFORMING_INFO:
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                 u1SuppImpTranBeamRecCapable & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u4TxBeamCapParam |= u1ACBit;
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                 u1SuppRecStagSoundCapable & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u4TxBeamCapParam =
                (UINT4) ((pRadioIfGetDB->RadioIfGetAllDB.
                          Dot11NhtTxBeamCapParams.
                          u4TxBeamCapParam & (~(0x2))) | (u1ACBit <<
                                                          RADIO_SHIFT_BIT1));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                 u1SuppTransStagSoundCapable & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u4TxBeamCapParam =
                (UINT4) ((pRadioIfGetDB->RadioIfGetAllDB.
                          Dot11NhtTxBeamCapParams.
                          u4TxBeamCapParam & (~(0x4))) | (u1ACBit <<
                                                          RADIO_SHIFT_BIT2));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                 u1SuppRecNdpCapable & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u4TxBeamCapParam =
                (UINT4) ((pRadioIfGetDB->RadioIfGetAllDB.
                          Dot11NhtTxBeamCapParams.
                          u4TxBeamCapParam & (~(0x8))) | (u1ACBit <<
                                                          RADIO_SHIFT_BIT3));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                 u1SuppTransNdpCapable & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u4TxBeamCapParam =
                (UINT4) ((pRadioIfGetDB->RadioIfGetAllDB.
                          Dot11NhtTxBeamCapParams.
                          u4TxBeamCapParam & (~(0x10))) | (u1ACBit <<
                                                           RADIO_SHIFT_BIT4));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                 u1SuppImpTranBeamCapable & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u4TxBeamCapParam =
                (UINT4) ((pRadioIfGetDB->RadioIfGetAllDB.
                          Dot11NhtTxBeamCapParams.
                          u4TxBeamCapParam & (~(0x20))) | (u1ACBit <<
                                                           RADIO_SHIFT_BIT5));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                 u1SuppCalibration & RADIO_MASK_BIT3);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u4TxBeamCapParam =
                (UINT4) ((pRadioIfGetDB->RadioIfGetAllDB.
                          Dot11NhtTxBeamCapParams.
                          u4TxBeamCapParam & (~(0xc0))) | (u1ACBit <<
                                                           RADIO_SHIFT_BIT6));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                 u1SuppExplCsiTranBeamCapable & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u4TxBeamCapParam =
                (UINT4) ((pRadioIfGetDB->RadioIfGetAllDB.
                          Dot11NhtTxBeamCapParams.
                          u4TxBeamCapParam & (~(0x100))) | (u1ACBit <<
                                                            RADIO_SHIFT_BIT8));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                 u1SuppExplNoncompSteCapable & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u4TxBeamCapParam =
                (UINT4) ((pRadioIfGetDB->RadioIfGetAllDB.
                          Dot11NhtTxBeamCapParams.
                          u4TxBeamCapParam & (~(0x200))) | (u1ACBit <<
                                                            RADIO_SHIFT_BIT9));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                 u1SuppExplCompSteCapable & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u4TxBeamCapParam =
                (UINT4) ((pRadioIfGetDB->RadioIfGetAllDB.
                          Dot11NhtTxBeamCapParams.
                          u4TxBeamCapParam & (~(0x400))) | (u1ACBit <<
                                                            RADIO_SHIFT_BIT10));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                 u1SuppExplTranBeamCsiFeedback & RADIO_MASK_BIT3);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u4TxBeamCapParam =
                (UINT4) ((pRadioIfGetDB->RadioIfGetAllDB.
                          Dot11NhtTxBeamCapParams.
                          u4TxBeamCapParam & (~(0x1800))) | (u1ACBit <<
                                                             RADIO_SHIFT_BIT11));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                 u1SuppExplNoncompBeamFbCapable & RADIO_MASK_BIT3);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u4TxBeamCapParam =
                (UINT4) ((pRadioIfGetDB->RadioIfGetAllDB.
                          Dot11NhtTxBeamCapParams.
                          u4TxBeamCapParam & (~(0x6000))) | (u1ACBit <<
                                                             RADIO_SHIFT_BIT13));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                 u1SuppExplCompBeamFbCapable & RADIO_MASK_BIT3);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u4TxBeamCapParam =
                (UINT4) ((pRadioIfGetDB->RadioIfGetAllDB.
                          Dot11NhtTxBeamCapParams.
                          u4TxBeamCapParam & (~(0x18000))) | (u1ACBit <<
                                                              RADIO_SHIFT_BIT15));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                 u1SuppMinGrouping & RADIO_MASK_BIT3);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u4TxBeamCapParam =
                (UINT4) ((pRadioIfGetDB->RadioIfGetAllDB.
                          Dot11NhtTxBeamCapParams.
                          u4TxBeamCapParam & (~(0x60000))) | (u1ACBit <<
                                                              RADIO_SHIFT_BIT17));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                 u1SuppCsiNoofBeamAntSupported & RADIO_MASK_BIT3);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u4TxBeamCapParam =
                (UINT4) ((pRadioIfGetDB->RadioIfGetAllDB.
                          Dot11NhtTxBeamCapParams.
                          u4TxBeamCapParam & (~(0x180000))) | (u1ACBit <<
                                                               RADIO_SHIFT_BIT19));

            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                 u1SuppNoncompSteNoBeamformer & RADIO_MASK_BIT3);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u4TxBeamCapParam =
                (UINT4) ((pRadioIfGetDB->RadioIfGetAllDB.
                          Dot11NhtTxBeamCapParams.
                          u4TxBeamCapParam & (~(0x600000))) | (u1ACBit <<
                                                               RADIO_SHIFT_BIT21));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                 u1SuppCompSteNoBeamformerAntSupp & RADIO_MASK_BIT3);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u4TxBeamCapParam =
                (UINT4) ((pRadioIfGetDB->RadioIfGetAllDB.
                          Dot11NhtTxBeamCapParams.
                          u4TxBeamCapParam & (~(0x1800000))) | (u1ACBit <<
                                                                RADIO_SHIFT_BIT23));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                 u1SuppCSIMaxNoRowsBeamformerSupp & RADIO_MASK_BIT3);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u4TxBeamCapParam =
                (UINT4) ((pRadioIfGetDB->RadioIfGetAllDB.
                          Dot11NhtTxBeamCapParams.
                          u4TxBeamCapParam & (~(0x6000000))) | (u1ACBit <<
                                                                RADIO_SHIFT_BIT25));
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                 u1SuppChannelEstCapability & RADIO_MASK_BIT3);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u4TxBeamCapParam =
                (UINT4) ((pRadioIfGetDB->RadioIfGetAllDB.
                          Dot11NhtTxBeamCapParams.
                          u4TxBeamCapParam & (~(0x18000000))) | (u1ACBit <<
                                                                 RADIO_SHIFT_BIT27));

            break;
        case WSS_ASSEMBLE_11N_OPER_INFO:
            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                 u1SecondaryChannel & RADIO_MASK_BIT3);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.au1HTOpeInfo[0] =
                (UINT1) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                          au1HTOpeInfo[0] & (~(0x3))) | u1ACBit);

            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                 u1StaChanWidth & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.au1HTOpeInfo[0] =
                (UINT1) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                          au1HTOpeInfo[0] & (~(0x4))) | (u1ACBit <<
                                                         RADIO_SHIFT_BIT2));

            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                 u1RifsMode & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.au1HTOpeInfo[0] =
                (UINT1) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                          au1HTOpeInfo[0] & (~(0x8))) | (u1ACBit <<
                                                         RADIO_SHIFT_BIT3));

            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                 u1HtProtection & RADIO_MASK_BIT3);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.au1HTOpeInfo[1] =
                (UINT1) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                          au1HTOpeInfo[1] & (~(0x3))) | (u1ACBit));

            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                 u1NongfHtStasPresent & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.au1HTOpeInfo[1] =
                (UINT1) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                          au1HTOpeInfo[1] & (~(0x4))) | (u1ACBit <<
                                                         RADIO_SHIFT_BIT2));

            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                 u1ObssNonHtStasPresent & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.au1HTOpeInfo[1] =
                (UINT1) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                          au1HTOpeInfo[1] & (~(0x10))) | (u1ACBit <<
                                                          RADIO_SHIFT_BIT4));

            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                 u1DualBeacon & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.au1HTOpeInfo[3] =
                (UINT1) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                          au1HTOpeInfo[3] & (~(0x40))) | (u1ACBit <<
                                                          RADIO_SHIFT_BIT6));

            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                 u1DualCtsProtection & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.au1HTOpeInfo[3] =
                (UINT1) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                          au1HTOpeInfo[3] & (~(0x80))) | (u1ACBit <<
                                                          RADIO_SHIFT_BIT7));

            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                 u1StbcBeacon & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.au1HTOpeInfo[4] =
                (UINT1) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                          au1HTOpeInfo[4]) | (u1ACBit));

            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                 u1LsigTxopProtFulSup & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.au1HTOpeInfo[4] =
                (UINT1) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                          au1HTOpeInfo[4] & (~(0x2))) | (u1ACBit <<
                                                         RADIO_SHIFT_BIT1));

            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                 u1PcoActive & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.au1HTOpeInfo[4] =
                (UINT1) ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                          au1HTOpeInfo[4] & (~(0x4))) | (u1ACBit <<
                                                         RADIO_SHIFT_BIT2));

            u1ACBit =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                 u1PcoPhase & RADIO_MASK_BIT1);
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.au1HTOpeInfo[4] =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                 au1HTOpeInfo[4] & (~(0x8))) | (u1ACBit << RADIO_SHIFT_BIT3);

            break;

        case WSS_PARSE_11N_CAPABILITY_INFO:
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppLdpcCoding
                = (pRadioIfGetDB->RadioIfGetAllDB.
                   Dot11NCapaParams.u2SuppHtCapInfo & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppChannelWidth
                = ((pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u2SuppHtCapInfo >> RADIO_SHIFT_BIT1)
                   & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppSmPowerSave
                = ((pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u2SuppHtCapInfo >> RADIO_SHIFT_BIT2)
                   & RADIO_MASK_BIT3);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppHtGreenField
                = ((pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u2SuppHtCapInfo >> RADIO_SHIFT_BIT4)
                   & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppShortGi20
                = ((pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u2SuppHtCapInfo >> RADIO_SHIFT_BIT5)
                   & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppShortGi40
                = ((pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u2SuppHtCapInfo >> RADIO_SHIFT_BIT6)
                   & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppTxStbc
                = ((pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u2SuppHtCapInfo >> RADIO_SHIFT_BIT7)
                   & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppRxStbc
                = ((pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u2SuppHtCapInfo >> RADIO_SHIFT_BIT8)
                   & RADIO_MASK_BIT3);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                u1SuppDelayedBlockack =
                ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                  u2SuppHtCapInfo >> RADIO_SHIFT_BIT10) & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppMaxAmsduLen
                = ((pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u2SuppHtCapInfo >> RADIO_SHIFT_BIT11)
                   & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppDsssCck40
                = ((pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u2SuppHtCapInfo >> RADIO_SHIFT_BIT12)
                   & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppFortyInto
                = ((pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NCapaParams.u2SuppHtCapInfo >> RADIO_SHIFT_BIT14)
                   & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                u1SuppLsigTxopProtFulSup =
                ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                  u2SuppHtCapInfo >> RADIO_SHIFT_BIT15) & RADIO_MASK_BIT1);

            break;
        case WSS_PARSE_11N_AMPDU_INFO:
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.u1SuppMaxAmpduLen
                = ((pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NampduParams.u1SuppAmpduParam) & RADIO_MASK_BIT3);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NampduParams.u1SuppMinAmpduStart
                = ((pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NampduParams.u1SuppAmpduParam >> RADIO_SHIFT_BIT2)
                   & RADIO_MASK_BIT7);
            break;
        case WSS_PARSE_11N_MCS_INFO:
            MEMCPY (pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                    au1SuppRxMcsBitmask,
                    pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                    au1SuppHtCapaMcs,
                    sizeof (pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                            au1RxMcsBitmask));

            MEMCPY (&pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                    u2SuppHighSuppDataRate,
                    &pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                    au1SuppHtCapaMcs[11],
                    sizeof (pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                            u2HighSuppDataRate));

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                u1SuppTxMcsSetDefined =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                 au1SuppHtCapaMcs[12] & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                u1SuppTxRxMcsSetNotEqual =
                ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                  au1SuppHtCapaMcs[12] >> RADIO_SHIFT_BIT1) & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                u1SuppTxMaxNoSpatStrm =
                ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                  au1SuppHtCapaMcs[12] >> RADIO_SHIFT_BIT2) & RADIO_MASK_BIT3);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                u1SuppTxUnequalModSupp =
                ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
                  au1SuppHtCapaMcs[12] >> RADIO_SHIFT_BIT4) & RADIO_MASK_BIT1);

            break;
        case WSS_PARSE_11N_EXTCAP_INFO:
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u1Pco
                = ((pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u2SuppHtExtCap) & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u1PcoTransTime
                = ((pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u2SuppHtExtCap >> RADIO_SHIFT_BIT1)
                   & RADIO_MASK_BIT3);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u1McsFeedback
                = ((pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u2SuppHtExtCap >> RADIO_SHIFT_BIT8)
                   & RADIO_MASK_BIT3);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u1HtcSupp
                = ((pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u2SuppHtExtCap >> RADIO_SHIFT_BIT10)
                   & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtExtCapParams.u1RdResponder
                = ((pRadioIfGetDB->RadioIfGetAllDB.
                    Dot11NhtExtCapParams.u2SuppHtExtCap >> RADIO_SHIFT_BIT10)
                   & RADIO_MASK_BIT1);
            break;

        case WSS_PARSE_11N_BEAMFORMING_INFO:
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ImpTranBeamRecCapable = ((pRadioIfGetDB->RadioIfGetAllDB.
                                            Dot11NhtTxBeamCapParams.
                                            u4SuppTxBeamCapParam) &
                                           RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1RecStagSoundCapable = ((pRadioIfGetDB->RadioIfGetAllDB.
                                          Dot11NhtTxBeamCapParams.
                                          u4SuppTxBeamCapParam >>
                                          RADIO_SHIFT_BIT1) & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1TransStagSoundCapable = ((pRadioIfGetDB->RadioIfGetAllDB.
                                            Dot11NhtTxBeamCapParams.
                                            u4SuppTxBeamCapParam >>
                                            RADIO_SHIFT_BIT2) &
                                           RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1RecNdpCapable = ((pRadioIfGetDB->RadioIfGetAllDB.
                                    Dot11NhtTxBeamCapParams.
                                    u4SuppTxBeamCapParam >> RADIO_SHIFT_BIT3) &
                                   RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1TransNdpCapable = ((pRadioIfGetDB->RadioIfGetAllDB.
                                      Dot11NhtTxBeamCapParams.
                                      u4SuppTxBeamCapParam >> RADIO_SHIFT_BIT4)
                                     & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ImpTranBeamCapable = ((pRadioIfGetDB->RadioIfGetAllDB.
                                         Dot11NhtTxBeamCapParams.
                                         u4SuppTxBeamCapParam >>
                                         RADIO_SHIFT_BIT5) & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1Calibration = ((pRadioIfGetDB->RadioIfGetAllDB.
                                  Dot11NhtTxBeamCapParams.
                                  u4SuppTxBeamCapParam >> RADIO_SHIFT_BIT6) &
                                 RADIO_MASK_BIT3);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplCsiTranBeamCapable = ((pRadioIfGetDB->RadioIfGetAllDB.
                                             Dot11NhtTxBeamCapParams.
                                             u4SuppTxBeamCapParam >>
                                             RADIO_SHIFT_BIT8) &
                                            RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplNoncompSteCapable = ((pRadioIfGetDB->RadioIfGetAllDB.
                                            Dot11NhtTxBeamCapParams.
                                            u4SuppTxBeamCapParam >>
                                            RADIO_SHIFT_BIT9) &
                                           RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplCompSteCapable = ((pRadioIfGetDB->RadioIfGetAllDB.
                                         Dot11NhtTxBeamCapParams.
                                         u4SuppTxBeamCapParam >>
                                         RADIO_SHIFT_BIT10) & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplTranBeamCsiFeedback = ((pRadioIfGetDB->RadioIfGetAllDB.
                                              Dot11NhtTxBeamCapParams.
                                              u4SuppTxBeamCapParam >>
                                              RADIO_SHIFT_BIT11) &
                                             RADIO_MASK_BIT3);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplNoncompBeamFbCapable = ((pRadioIfGetDB->RadioIfGetAllDB.
                                               Dot11NhtTxBeamCapParams.
                                               u4SuppTxBeamCapParam >>
                                               RADIO_SHIFT_BIT13) &
                                              RADIO_MASK_BIT3);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ExplCompBeamFbCapable = ((pRadioIfGetDB->RadioIfGetAllDB.
                                            Dot11NhtTxBeamCapParams.
                                            u4SuppTxBeamCapParam >>
                                            RADIO_SHIFT_BIT15) &
                                           RADIO_MASK_BIT3);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1MinGrouping = ((pRadioIfGetDB->RadioIfGetAllDB.
                                  Dot11NhtTxBeamCapParams.
                                  u4SuppTxBeamCapParam >> RADIO_SHIFT_BIT17) &
                                 RADIO_MASK_BIT3);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1CsiNoofBeamAntSupported = ((pRadioIfGetDB->RadioIfGetAllDB.
                                              Dot11NhtTxBeamCapParams.
                                              u4SuppTxBeamCapParam >>
                                              RADIO_SHIFT_BIT19) &
                                             RADIO_MASK_BIT3);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1NoncompSteNoBeamformerAntSupp =
                ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                  u4SuppTxBeamCapParam >> RADIO_SHIFT_BIT21) & RADIO_MASK_BIT3);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1CompSteNoBeamformerAntSupp = ((pRadioIfGetDB->RadioIfGetAllDB.
                                                 Dot11NhtTxBeamCapParams.
                                                 u4SuppTxBeamCapParam >>
                                                 RADIO_SHIFT_BIT23) &
                                                RADIO_MASK_BIT3);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1CSIMaxNoRowsBeamformerSupp = ((pRadioIfGetDB->RadioIfGetAllDB.
                                                 Dot11NhtTxBeamCapParams.
                                                 u4SuppTxBeamCapParam >>
                                                 RADIO_SHIFT_BIT25) &
                                                RADIO_MASK_BIT3);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
                u1ChannelEstCapability = ((pRadioIfGetDB->RadioIfGetAllDB.
                                           Dot11NhtTxBeamCapParams.
                                           u4SuppTxBeamCapParam >>
                                           RADIO_SHIFT_BIT27) &
                                          RADIO_MASK_BIT3);
            break;
        case WSS_PARSE_11N_OPER_INFO:
            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                u1PrimaryChannel = pRadioIfGetDB->RadioIfGetAllDB.
                Dot11NhtOperation.u1SuppPrimaryChannel;

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                u1SecondaryChannel =
                (pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                 au1SuppHTOpeInfo[0] & RADIO_MASK_BIT3);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.u1StaChanWidth =
                ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                  au1SuppHTOpeInfo[0] >> RADIO_SHIFT_BIT2) & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.u1RifsMode =
                ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                  au1SuppHTOpeInfo[0] >> RADIO_SHIFT_BIT3) & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.u1HtProtection =
                ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                  au1SuppHTOpeInfo[1]) & RADIO_MASK_BIT3);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                u1NongfHtStasPresent =
                ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                  au1SuppHTOpeInfo[1] >> RADIO_SHIFT_BIT2) & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                u1ObssNonHtStasPresent =
                ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                  au1SuppHTOpeInfo[1] >> RADIO_SHIFT_BIT4) & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.u1DualBeacon =
                ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                  au1SuppHTOpeInfo[3] >> RADIO_SHIFT_BIT6) & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                u1DualCtsProtection =
                ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                  au1SuppHTOpeInfo[3] >> RADIO_SHIFT_BIT7) & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.u1StbcBeacon =
                ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                  au1SuppHTOpeInfo[4]) & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                u1LsigTxopProtFulSup =
                ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                  au1SuppHTOpeInfo[4] >> RADIO_SHIFT_BIT1) & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.u1PcoActive =
                ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                  au1SuppHTOpeInfo[4] >> RADIO_SHIFT_BIT2) & RADIO_MASK_BIT1);

            pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                u1LsigTxopProtFulSup =
                ((pRadioIfGetDB->RadioIfGetAllDB.Dot11NhtOperation.
                  au1SuppHTOpeInfo[4] >> RADIO_SHIFT_BIT3) & RADIO_MASK_BIT1);

            break;

        default:
            WSSIF_TRC1 (WSSIF_FAILURE_TRC,
                        "RadioIfDBMsg : Invalid OpCode %d\n", u1OpCode);
            WSSIF_UNLOCK;
            return OSIX_FAILURE;
    }

    WSSIF_UNLOCK;

    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : WssIfSetDefault11AcParams                            *
 *                                                                           *
 * Description        : Function to set the default values for 11ac params   *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 *                                                                           *
 *****************************************************************************/
VOID
WssIfSetDefault11AcParams (tRadioIfDB * pRadioIfDB)
{
    UINT1               au1SuppVhtCapaMcs[DOT11AC_VHT_CAP_MCS_LEN] =
        RADIO_11AC_MCS_ENABLE;

    pRadioIfDB->Dot11AcCapaParams.u1SuppVhtSTS = RADIO_VALUE_3;
    pRadioIfDB->Dot11AcCapaParams.u1VhtSTS = RADIO_VALUE_3;
    pRadioIfDB->Dot11AcCapaParams.u1SpecMgmt = OSIX_TRUE;
    pRadioIfDB->Dot11AcCapaParams.u1SuppMaxMPDULen = RADIO_11AC_MPDU_DEFAULT;
    pRadioIfDB->Dot11AcCapaParams.u1SuppRxLpdc = RADIO_11AC_RX_LDPC_DEFAULT;
    pRadioIfDB->Dot11AcCapaParams.u1SuppShortGi80 = RADIO_11AC_SHORTGI80_ENABLE;
    pRadioIfDB->Dot11AcCapaParams.u1ShortGi80 = RADIO_11AC_SHORTGI80_ENABLE;
    pRadioIfDB->Dot11AcCapaParams.u1SuppTxStbc = RADIO_11AC_STBC_ENABLE;
    pRadioIfDB->Dot11AcCapaParams.u1SuppRxStbcStatus = RADIO_11AC_STBC_ENABLE;
    pRadioIfDB->Dot11AcCapaParams.u1SuppRxStbc = RADIO_11AC_MAX_RX_STBC;
    pRadioIfDB->Dot11AcCapaParams.u1SuppVhtMaxAMPDU = RADIO_11AC_AMPDU_DEFAULT;
    MEMCPY ((pRadioIfDB->Dot11AcCapaParams.au1SuppVhtCapaMcs),
            au1SuppVhtCapaMcs, DOT11AC_VHT_CAP_MCS_LEN);
    MEMCPY ((pRadioIfDB->Dot11AcCapaParams.au1VhtCapaMcs),
            au1SuppVhtCapaMcs, DOT11AC_VHT_CAP_MCS_LEN);
    /* For the current AC card, Oper elements can't be fetched from hardware.
     * So separate elements has no effect.*/
    pRadioIfDB->Dot11AcOperParams.u1VhtOperModeNotify = OSIX_TRUE;
    pRadioIfDB->Dot11AcOperParams.u1VhtOption = OSIX_TRUE;
    pRadioIfDB->Dot11AcOperParams.u1SuppVhtChannelWidth =
        RADIO_11AC_CHANWIDTH_DEFAULT;
    pRadioIfDB->Dot11AcOperParams.u1SuppCenterFcy0 = RADIO_11AC_SUPP_CENTER_FCY;
    pRadioIfDB->Dot11AcOperParams.u1CenterFcy0 = RADIO_11AC_CENTER_FCY_DEFAULT;
    pRadioIfDB->Dot11AcOperParams.u2VhtOperMcsSet = RADIO_11AC_OPERMCS_DEFAULT;
}

/*****************************************************************************
 * Function Name      : WssIfComputeBandWidth                                *
 *                                                                           *
 * Description        : Function to compute bandwidth from HT and VHT        *
 *                      channel bandwidths                                   *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : Computed UINT1 channel bandwidth                     *
 *                                                                           *
 *****************************************************************************/
UINT1
WssIfComputeBandWidth (tRadioIfGetDB * pRadioIfGetDB)
{
    UINT1               u1HtChannelWidth = 0;
    UINT1               u1VhtChannelWidth = 0;
    UINT1               u1Width = 0;

    u1HtChannelWidth = pRadioIfGetDB->RadioIfGetAllDB.u1ChannelWidth;
    u1VhtChannelWidth = pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.
        u1VhtChannelWidth;

    if ((u1HtChannelWidth == RADIO_DOT11N_CHANNELWIDTH_20)
        && (u1VhtChannelWidth == OSIX_FALSE))
    {
        u1Width = RADIO_VALUE_0;
    }
    else if ((u1HtChannelWidth == RADIO_DOT11N_CHANNELWIDTH_40)
             && (u1VhtChannelWidth == OSIX_FALSE))
    {
        u1Width = RADIO_VALUE_1;
    }
    else if (u1VhtChannelWidth == RADIO_11AC_CHANWIDTH80)
    {
        u1Width = RADIO_VALUE_2;
    }
    else if ((u1VhtChannelWidth == RADIO_11AC_CHANWIDTH160)
             || (u1VhtChannelWidth == RADIO_11AC_CHANWIDTH80P80))
    {
        u1Width = RADIO_VALUE_3;
    }

    return u1Width;
}

/*****************************************************************************
 * Function Name      : WssIfAssembleOperModeNotify                          *
 *                                                                           *
 * Description        : Function to assemble Operation mode notification     *
 *                      field                                                *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 *                                                                           *
 *****************************************************************************/
VOID
WssIfAssembleOperModeNotify (tRadioIfGetDB * pRadioIfGetDB)
{
    UINT1               u1Bit = 0;
    UINT1               u1Width = 0;

    u1Width = WssIfComputeBandWidth (pRadioIfGetDB);

    u1Bit = u1Width & RADIO_VALUE_3;
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u1VhtOperModeElem =
        (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u1VhtOperModeElem
         & (~(RADIO_VALUE_3))) | u1Bit;

    u1Bit = ((pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
              u1VhtSTS - 1) & RADIO_VALUE_7);
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u1VhtOperModeElem =
        (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.u1VhtOperModeElem
         & (~(RADIO_11AC_RX_STS))) | u1Bit << RADIO_VALUE_4;
    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtOperModeElem = OSIX_TRUE;
/*
 * Need to Assemble Rx NSS type during Beamforming implementation
 * */
}

/*****************************************************************************
 * Function Name      : WssIfAssembleVhtPowerEnvelope                        *
 *                                                                           *
 * Description        : Function to assemble VHT power envelope              *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 *                                                                           *
 *****************************************************************************/
VOID
WssIfAssembleVhtPowerEnvelope (tRadioIfGetDB * pRadioIfGetDB)
{
    UINT1               u1Bit = 0;
    UINT1               u1HtChannelWidth = 0;
    UINT1               u1VhtChannelWidth = 0;
    UINT1               u1PowerCount = 0;

    u1HtChannelWidth = pRadioIfGetDB->RadioIfGetAllDB.u1ChannelWidth;
    u1VhtChannelWidth = pRadioIfGetDB->RadioIfGetAllDB.Dot11AcOperParams.
        u1VhtChannelWidth;

    if ((u1HtChannelWidth == RADIO_DOT11N_CHANNELWIDTH_20)
        && (u1VhtChannelWidth == RADIO_VALUE_0))
    {
        u1PowerCount = RADIO_VALUE_0;
        pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.
            u1TxPower20 = (UINT1) pRadioIfGetDB->RadioIfGetAllDB.i1LocalTxPower;
        pRadioIfGetDB->RadioIfIsGetAllDB.bTxPower20 = OSIX_TRUE;
    }
    else if ((u1HtChannelWidth == RADIO_DOT11N_CHANNELWIDTH_40)
             && (u1VhtChannelWidth == RADIO_VALUE_0))
    {
        u1PowerCount = RADIO_VALUE_1;
        pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.
            u1TxPower20 = (UINT1) pRadioIfGetDB->RadioIfGetAllDB.i1LocalTxPower;
        pRadioIfGetDB->RadioIfIsGetAllDB.bTxPower20 = OSIX_TRUE;
        pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.
            u1TxPower40 = (UINT1) pRadioIfGetDB->RadioIfGetAllDB.i1LocalTxPower;
        pRadioIfGetDB->RadioIfIsGetAllDB.bTxPower40 = OSIX_TRUE;
    }
    else if (((u1HtChannelWidth == RADIO_DOT11N_CHANNELWIDTH_20) ||
              (u1HtChannelWidth == RADIO_DOT11N_CHANNELWIDTH_40))
             && (u1VhtChannelWidth == RADIO_VALUE_1))
    {
        u1PowerCount = RADIO_VALUE_2;
        pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.
            u1TxPower20 = (UINT1) pRadioIfGetDB->RadioIfGetAllDB.i1LocalTxPower;
        pRadioIfGetDB->RadioIfIsGetAllDB.bTxPower20 = OSIX_TRUE;
        pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.
            u1TxPower40 = (UINT1) pRadioIfGetDB->RadioIfGetAllDB.i1LocalTxPower;
        pRadioIfGetDB->RadioIfIsGetAllDB.bTxPower40 = OSIX_TRUE;
        pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.
            u1TxPower80 = (UINT1) pRadioIfGetDB->RadioIfGetAllDB.i1LocalTxPower;
        pRadioIfGetDB->RadioIfIsGetAllDB.bTxPower80 = OSIX_TRUE;
    }

    pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.
        u1MaxTxPowerCount = u1PowerCount;
    pRadioIfGetDB->RadioIfIsGetAllDB.bMaxTxPowerCount = OSIX_TRUE;
    u1Bit = u1PowerCount & RADIO_VALUE_3;
    pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.u1TransPower =
        (pRadioIfGetDB->RadioIfGetAllDB.VhtTransmitPower.u1TransPower
         & (~(RADIO_VALUE_3))) | u1Bit;

    pRadioIfGetDB->RadioIfIsGetAllDB.bTransPower = OSIX_TRUE;
/*
 * Need to Assemble Rx NSS type during Beamforming implementation
 * */
}

/*****************************************************************************
 * Function Name      : WssIfVhtHighestSuppDataRate                          *
 *                                                                           *
 * Description        : Function to compute the highest supported            *
 *                      long GI data rate in VHT                             *    
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 *                                                                           *
 *****************************************************************************/

VOID
WssIfVhtHighestSuppDataRate (tRadioIfGetDB * pRadioIfGetDB)
{
    UINT1               u1ChanWidth = 0;
    FLT4                f4BwFactor = 0;
    FLT4                f4SgiFactor = 0;
    UINT1               u1RxSTS = 0;
    UINT1               u1TxSTS = 0;
    FLT4                f4RxMcsFactor = 0;
    FLT4                f4TxMcsFactor = 0;
    UINT2               u2RxDataRate = 0;
    UINT2               u2TxDataRate = 0;
    UINT1               u1Loop = 0;
    UINT2               u2RxMcs = 0;
    UINT2               u2TxMcs = 0;
    UINT1               u1Mcs = 0;
    UINT1               u1McsIndex = 0;

/*Channel Bandwidth and Short guard interval factor*/
    u1ChanWidth = WssIfComputeBandWidth (pRadioIfGetDB);
    switch (u1ChanWidth)
    {
        case RADIO_VALUE_0:
            f4BwFactor = RADIO_BANDWIDTH0_FACTOR;
            if (pRadioIfGetDB->RadioIfGetAllDB.u1ShortGi20 == OSIX_TRUE)
            {
                f4SgiFactor = RADIO_SGI_FACTOR;
            }
            else
            {
                f4SgiFactor = RADIO_LGI_FACTOR;
            }
            break;
        case RADIO_VALUE_1:
            f4BwFactor = RADIO_BANDWIDTH1_FACTOR;
            if (pRadioIfGetDB->RadioIfGetAllDB.u1ShortGi40 == OSIX_TRUE)
            {
                f4SgiFactor = RADIO_SGI_FACTOR;
            }
            else
            {
                f4SgiFactor = RADIO_LGI_FACTOR;
            }
            break;
        case RADIO_VALUE_2:
            f4BwFactor = RADIO_BANDWIDTH2_FACTOR;
            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
                u1ShortGi80 == OSIX_TRUE)
            {
                f4SgiFactor = RADIO_SGI_FACTOR;
            }
            else
            {
                f4SgiFactor = RADIO_LGI_FACTOR;
            }
            break;
        case RADIO_VALUE_3:
            f4BwFactor = RADIO_BANDWIDTH3_FACTOR;
            break;
        default:
            break;
    }
/*Rx Mcs Factor STS computation*/
    MEMCPY (&u2RxMcs,
            (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
             au1VhtCapaMcs), DOT11AC_MCS_MAP);

    for (u1Loop = RADIO_VALUE_0; u1Loop < RADIO_11AC_VHT_CAPA_MCS_LEN; u1Loop++)
    {
        u1Mcs = (UINT2) (u2RxMcs & RADIO_11AC_VHT_MCS_SHIFT);
        if (u1Mcs == RADIO_VHT_MCS_DISABLE)
        {
            u1RxSTS = u1Loop;
            break;
        }
        else
        {
            u1McsIndex = u1Mcs;
        }
        u2RxMcs = (u2RxMcs >> RADIO_11AC_VHT_MCS_SHIFT_BITS);
    }
    switch (u1McsIndex)
    {
        case RADIO_VALUE_0:
            f4RxMcsFactor = RADIO_MCS7_FACTOR;
            break;
        case RADIO_VALUE_1:
            f4RxMcsFactor = RADIO_MCS8_FACTOR;
            break;
        default:
            f4RxMcsFactor = RADIO_MCS9_FACTOR;
            break;

    }
/*Tx Mcs Factor computation*/
    u1Mcs = RADIO_VALUE_0;
    u1McsIndex = RADIO_VALUE_0;
    MEMCPY (&u2TxMcs,
            &(pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
              au1VhtCapaMcs[DOT11AC_MCS_MAP + DOT11AC_MCS_MAP]),
            DOT11AC_MCS_MAP);
    for (u1Loop = RADIO_VALUE_0; u1Loop < RADIO_11AC_VHT_CAPA_MCS_LEN; u1Loop++)
    {
        u1Mcs = (UINT2) (u2TxMcs & RADIO_11AC_VHT_MCS_SHIFT);
        if (u1Mcs == RADIO_VHT_MCS_DISABLE)
        {
            u1TxSTS = u1Loop;
            break;
        }
        else
        {
            u1McsIndex = u1Mcs;
        }
        u2TxMcs = (u2TxMcs >> RADIO_11AC_VHT_MCS_SHIFT_BITS);
    }
    switch (u1McsIndex)
    {
        case RADIO_VALUE_0:
            f4TxMcsFactor = RADIO_MCS7_FACTOR;
            break;
        case RADIO_VALUE_1:
            f4TxMcsFactor = RADIO_MCS8_FACTOR;
            break;
        default:
            f4TxMcsFactor = RADIO_MCS9_FACTOR;
            break;
    }
/*Rx Highest Supported Long GI Data Rate*/
    u2RxDataRate = ceil ((u1RxSTS * f4BwFactor * f4RxMcsFactor) / f4SgiFactor);
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u2RxDataRate
        = u2RxDataRate;
    u2TxDataRate = ceil ((u1TxSTS * f4BwFactor * f4TxMcsFactor) / f4SgiFactor);
    pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.u2TxDataRate
        = u2TxDataRate;
    u2RxDataRate = OSIX_HTONS ((UINT2) u2RxDataRate << RADIO_VALUE_3);
    MEMCPY (&(pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
              au1VhtCapaMcs[RADIO_VALUE_2]), &u2RxDataRate, RADIO_VALUE_2);
    u2TxDataRate = OSIX_HTONS ((UINT2) u2TxDataRate << RADIO_VALUE_3);
    MEMCPY (&(pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
              au1VhtCapaMcs[RADIO_VALUE_6]), &u2TxDataRate, RADIO_VALUE_2);
    pRadioIfGetDB->RadioIfIsGetAllDB.bRxDataRate = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bTxDataRate = OSIX_TRUE;
    pRadioIfGetDB->RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;
}

/*****************************************************************************
 * Function Name      : WssIfVhtSts                                     *
 *                                                                           *
 * Description        : Function to compute Space time streams               *
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - pointer to the input structure       *
 *             RxSts - Reception streams
 *             TxSts - Transmission streams
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 *                                                                           *
 *****************************************************************************/
VOID
WssIfVhtSts (tRadioIfGetDB * pRadioIfGetDB, UINT1 *u1Sts)
{
    UINT2               u2RxMcs = 0;
    UINT2               u2TxMcs = 0;
    UINT2               u2Mcs = 0;
    UINT1               u1Loop = 0;
    UINT1               u1Mcs = 0;

    MEMCPY (&u2RxMcs,
            (pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
             au1VhtCapaMcs), DOT11AC_MCS_MAP);
    MEMCPY (&u2TxMcs,
            &(pRadioIfGetDB->RadioIfGetAllDB.Dot11AcCapaParams.
              au1VhtCapaMcs[DOT11AC_MCS_MAP + DOT11AC_MCS_MAP]),
            DOT11AC_MCS_MAP);
    u2Mcs = (u2RxMcs & u2TxMcs);

    for (u1Loop = RADIO_VALUE_0; u1Loop < RADIO_11AC_VHT_CAPA_MCS_LEN; u1Loop++)
    {
        u1Mcs = (UINT2) (u2Mcs & RADIO_11AC_VHT_MCS_SHIFT);
        if (u1Mcs != RADIO_VHT_MCS_DISABLE)
        {
            *u1Sts = RADIO_11AC_VHT_CAPA_MCS_LEN - u1Loop - RADIO_VALUE_1;
            break;
        }
        u2Mcs = (u2Mcs >> RADIO_11AC_VHT_MCS_SHIFT_BITS);
    }
}

/*****************************************************************************
 * Function Name      : WssIfObtainOperClass                                 *
 *                                                                           *
 * Description        : Function to obtain the operating class from channel  *
 *                      width and channel allocated                          *
 *                                                                           *
 * Input(s)           : u1Channel - Allocated Channel                        *
 *                      u1ChanWidth - Configured Channel Width               *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : Computed UINT1 Operating class number                *
 *                                                                           *
 *****************************************************************************/

UINT1
WssIfObtainOperClass (UINT1 u1Channel, UINT1 u1ChanWidth)
{
    UNUSED_PARAM (u1Channel);
    UNUSED_PARAM (u1ChanWidth);
    return OSIX_TRUE;
}

/*****************************************************************************
 * Function Name      : WssIfSetDefault11nParams                             *
 *                                                                           *
 * Description        : Function to set the default values for 11ac params   *
 *                                                                           *
 * Input(s)           : Opcode - To select the particular task               *
 *                      pRadioIfGetDB - pointer the input structure          *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *****************************************************************************/
VOID
WssIfSetDefault11nParams (tRadioIfDB * pRadioIfDB)
{
    /* HT Capa */
    pRadioIfDB->Dot11NCapaParams.u1SuppLdpcCoding = RADIO_DOT11N_LDPC_ENABLE;
    pRadioIfDB->Dot11NCapaParams.u1SuppShortGi20 = RADIO_DOT11N_SGI_ENABLE;
    pRadioIfDB->Dot11NCapaParams.u1SuppSmPowerSave =
        RADIO_DOT11N_SM_POWER_SAVE_DISABLE;
    pRadioIfDB->Dot11NCapaParams.u1SmPowerSave =
        RADIO_DOT11N_SM_POWER_SAVE_DISABLE;

    pRadioIfDB->Dot11NCapaParams.u1SuppHtGreenField = 1;
    pRadioIfDB->Dot11NCapaParams.u1SuppDelayedBlockack = 1;
    pRadioIfDB->Dot11NCapaParams.u1SuppFortyInto = 1;
    pRadioIfDB->Dot11NCapaParams.u1SuppMaxAmsduLen = 1;
    pRadioIfDB->Dot11NCapaParams.u1SuppLsigTxopProtFulSup = 1;
    pRadioIfDB->Dot11NCapaParams.u1SuppTxStbc = RADIO_DOT11N_TX_STBC_ENABLE;
    pRadioIfDB->Dot11NCapaParams.u1TxStbc = RADIO_DOT11N_TX_STBC_ENABLE;
    pRadioIfDB->Dot11NCapaParams.u1SuppRxStbc = 0;
    pRadioIfDB->Dot11NCapaParams.u1SuppDsssCck40 =
        RADIO_DOT11N_DSSS_CCK_MODE_40MHZ;
    pRadioIfDB->Dot11NCapaParams.u1ShortGi20 = RADIO_DOT11N_SGI_ENABLE;
    pRadioIfDB->Dot11NCapaParams.u1SuppShortGi20 = RADIO_DOT11N_SGI_ENABLE;
    /* AMPDU */
    pRadioIfDB->Dot11NampduParams.u1SuppMaxAmpduLen =
        RADIO_DOT11N_AMPDU_MAX_LEN;
    pRadioIfDB->Dot11NampduParams.u1SuppMinAmpduStart =
        RADIO_DOT11N_AMPDU_TIME_SPACING;
    pRadioIfDB->Dot11NampduParams.u1MaxAmpduLen = RADIO_DOT11N_AMPDU_MAX_LEN;
    pRadioIfDB->Dot11NampduParams.u1MinAmpduStart =
        RADIO_DOT11N_AMPDU_TIME_SPACING;

    /* MCS */
    pRadioIfDB->Dot11NsuppMcsParams.
        au1SuppRxMcsBitmask[RADIO_VALUE_0] = RADIO_DOT11N_MCSRATE_ENABLE;
    pRadioIfDB->Dot11NsuppMcsParams.
        au1SuppRxMcsBitmask[RADIO_VALUE_1] = RADIO_DOT11N_MCSRATE_ENABLE;
    pRadioIfDB->Dot11NsuppMcsParams.
        au1SuppRxMcsBitmask[RADIO_VALUE_2] = RADIO_DOT11N_MCSRATE_ENABLE;
    pRadioIfDB->Dot11NsuppMcsParams.au1RxMcsBitmask[RADIO_VALUE_0] =
        RADIO_DOT11N_MCSRATE_ENABLE;
    pRadioIfDB->Dot11NsuppMcsParams.au1RxMcsBitmask[RADIO_VALUE_1] =
        RADIO_DOT11N_MCSRATE_ENABLE;
    pRadioIfDB->Dot11NsuppMcsParams.au1RxMcsBitmask[RADIO_VALUE_2] =
        RADIO_DOT11N_MCSRATE_ENABLE;

    pRadioIfDB->Dot11NsuppMcsParams.u1SuppTxMcsSetDefined = 1;
    pRadioIfDB->Dot11NsuppMcsParams.u1SuppTxRxMcsSetNotEqual = 1;

    /* HT CAP */
    pRadioIfDB->Dot11NhtExtCapParams.u1SuppPco = 0x1;
    pRadioIfDB->Dot11NhtExtCapParams.u1SuppPcoTransTime = 0x3;
    pRadioIfDB->Dot11NhtExtCapParams.u1SuppMcsFeedback = 0x3;
    pRadioIfDB->Dot11NhtExtCapParams.u1SuppHtcSupp = 0x1;
    pRadioIfDB->Dot11NhtExtCapParams.u1SuppRdResponder = 0x1;

    /* BEAM FROMING */
    pRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppImpTranBeamRecCapable = 0x1;
    pRadioIfDB->Dot11NhtTxBeamCapParams.u4SuppTxBeamCapParam = 0x1;
    pRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppRecStagSoundCapable = 0x1;
    pRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppTransStagSoundCapable = 0x1;
    pRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppRecNdpCapable = 0x1;
    pRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppTransNdpCapable = 0x1;
    pRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppImpTranBeamCapable = 0x1;
    pRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppCalibration = 0x3;
    pRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppExplCsiTranBeamCapable = 0x1;
    pRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppExplNoncompSteCapable = 0x1;
    pRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppExplCompSteCapable = 0x1;
    pRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppExplTranBeamCsiFeedback = 0x3;
    pRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppExplNoncompBeamFbCapable = 0x3;
    pRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppExplCompBeamFbCapable = 0x3;
    pRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppMinGrouping = 0x3;
    pRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppCsiNoofBeamAntSupported = 0x3;
    pRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppNoncompSteNoBeamformer = 0x3;
    pRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppCompSteNoBeamformerAntSupp = 0x3;
    pRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppCSIMaxNoRowsBeamformerSupp = 0x3;
    pRadioIfDB->Dot11NhtTxBeamCapParams.u1SuppChannelEstCapability = 0x3;

    /* HT Information */
    pRadioIfDB->Dot11NhtOperation.u1SuppPrimaryChannel = 200;
    pRadioIfDB->Dot11NhtOperation.u1SuppSecondaryChannel = 3;
    pRadioIfDB->Dot11NhtOperation.u1SuppStaChanWidth = 1;
    pRadioIfDB->Dot11NhtOperation.u1SuppRifsMode = 1;
    pRadioIfDB->Dot11NhtOperation.u1SuppHtProtection = 1;
    pRadioIfDB->Dot11NhtOperation.u1NongfHtStasPresent = 0;
    pRadioIfDB->Dot11NhtOperation.u1SuppObssNonHtStasPresent = 1;
    pRadioIfDB->Dot11NhtOperation.u1SuppDualBeacon = 1;
    pRadioIfDB->Dot11NhtOperation.u1SuppDualCtsProtection = 1;
    pRadioIfDB->Dot11NhtOperation.u1SuppStbcBeacon = 1;
    pRadioIfDB->Dot11NhtOperation.u1SuppPcoActive = 1;
    pRadioIfDB->Dot11NhtOperation.u1SuppPcoPhase = 1;
    pRadioIfDB->Dot11NhtOperation.au1HTOpeInfo[RADIO_VALUE_1] =
        RADIO_DEFAULT_NON_GREEN_FIELD;

    pRadioIfDB->Dot11NCapaParams.u1AMPDUStatus = RADIO_DOT11N_ENABLE;
    pRadioIfDB->Dot11NCapaParams.u1AMPDUSubFrame =
        RADIO_DOT11N_DEF_AMPDU_SUBFRAME;
    pRadioIfDB->Dot11NCapaParams.u2AMPDULimit = RADIO_DOT11N_DEF_AMPDU_LIMIT;
    pRadioIfDB->Dot11NCapaParams.u1AMSDUStatus = RADIO_DOT11N_ENABLE;
    pRadioIfDB->Dot11NCapaParams.u2AMSDULimit = RADIO_DOT11N_DEF_AMSDU_LIMIT;
}

/*****************************************************************************
 * Function Name      : WssIfDot11nSplStreamAndUnequal                       *
 *                                                                           *
 * Description        : Function to compute the highest supported            *
 *                       data rate for 80211.n                               *    
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 *                                                                           *
 *****************************************************************************/

VOID
WssIfDot11nSplStreamAndUnequal (tRadioIfGetDB * pRadioIfGetDB)
{
    UINT1               u1SplStream = 0;
    UINT1               u1Loop;

    if ((pRadioIfGetDB->RadioIfGetAllDB.
         Dot11NsuppMcsParams.u1SuppTxMcsSetDefined
         == RADIO_DOT11N_TXMCSSETDEF_DISABLE) &&
        (pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.
         u1SuppTxRxMcsSetNotEqual == RADIO_DOT11N_TCRXMCSSETNOTEQUAL_DISABLE))
    {
        pRadioIfGetDB->RadioIfGetAllDB.
            Dot11NsuppMcsParams.u1SuppTxMaxNoSpatStrm =
            RADIO_DOT11N_TXMAXNOSPATSTRM_DISABLE;
        pRadioIfGetDB->RadioIfGetAllDB.
            Dot11NsuppMcsParams.u1SuppTxUnequalModSupp =
            RADIO_DOT11N_TXUNEQUALMODSUPP_DISABLE;
    }
    if ((pRadioIfGetDB->RadioIfGetAllDB.
         Dot11NsuppMcsParams.u1SuppTxMcsSetDefined ==
         RADIO_DOT11N_TXMCSSETDEF_ENABLE) &&
        (pRadioIfGetDB->RadioIfGetAllDB.
         Dot11NsuppMcsParams.u1SuppTxRxMcsSetNotEqual ==
         RADIO_DOT11N_TCRXMCSSETNOTEQUAL_DISABLE))
    {
        pRadioIfGetDB->RadioIfGetAllDB.
            Dot11NsuppMcsParams.u1SuppTxMaxNoSpatStrm =
            RADIO_DOT11N_TXMAXNOSPATSTRM_DISABLE;
        pRadioIfGetDB->RadioIfGetAllDB.
            Dot11NsuppMcsParams.u1SuppTxUnequalModSupp =
            RADIO_DOT11N_TXUNEQUALMODSUPP_DISABLE;
    }
    if ((pRadioIfGetDB->RadioIfGetAllDB.
         Dot11NsuppMcsParams.u1SuppTxMcsSetDefined ==
         RADIO_DOT11N_TXMCSSETDEF_ENABLE) &&
        (pRadioIfGetDB->RadioIfGetAllDB.
         Dot11NsuppMcsParams.u1SuppTxRxMcsSetNotEqual ==
         RADIO_DOT11N_TCRXMCSSETNOTEQUAL_ENABLE))
    {
        for (u1Loop = RADIO_VALUE_0; u1Loop < RADIO_DOT11N_MCS_STREAM_MAX;
             u1Loop++)
        {
            if ((pRadioIfGetDB->RadioIfGetAllDB.
                 Dot11NsuppMcsParams.au1SuppRxMcsBitmask[u1Loop] &
                 RADIO_DOT11N_MCSRATE_ENABLE) & RADIO_DOT11N_MCSRATE_ENABLE)
            {
                u1SplStream = u1Loop;
            }
        }
        pRadioIfGetDB->RadioIfGetAllDB.
            Dot11NsuppMcsParams.u1SuppTxMaxNoSpatStrm = u1SplStream;
        pRadioIfGetDB->RadioIfGetAllDB.
            Dot11NsuppMcsParams.u1SuppTxUnequalModSupp = 0;
        pRadioIfGetDB->RadioIfIsGetAllDB.bTxMaxNoSpatStrm = OSIX_TRUE;
        pRadioIfGetDB->RadioIfIsGetAllDB.bTxUnequalModSupp = OSIX_TRUE;
    }

}

/*****************************************************************************
 * Function Name      : WssIfDot11nHighestSuppDataRate                       *
 *                                                                           *
 * Description        : Function to compute the highest supported            *
 *                       data rate for 80211.n                               *    
 *                                                                           *
 * Input(s)           : pRadioIfGetDB - pointer to the input structure       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 *                                                                           *
 *****************************************************************************/

VOID
WssIfDot11nHighestSuppDataRate (tRadioIfGetDB * pRadioIfGetDB)
{
    UINT1               u1ChanWidth = 0;
    FLT4                f4BwFactor = 0;
    FLT4                f4SgiFactor = 0;
    UINT1               u1SplStream = 0;
    FLT4                f4McsFactor = 0;
    UINT2               u2DataRate = 0;
    UINT1               u1Loop = 0;
    UINT2               u1McsIndex = 0;
    UINT1               u1McsLoop = 0;

/*Channel Bandwidth and Short guard interval factor*/
    u1ChanWidth =
        pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.u1SuppChannelWidth;
    switch (u1ChanWidth)
    {
        case RADIO_VALUE_0:
            f4BwFactor = RADIO_BANDWIDTH0_FACTOR;
            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                u1SuppShortGi20 == OSIX_TRUE)
            {
                f4SgiFactor = (FLT4) RADIO_SGI_FACTOR;
            }
            else
            {
                f4SgiFactor = RADIO_LGI_FACTOR;
            }
            break;
        case RADIO_VALUE_1:
            f4BwFactor = RADIO_BANDWIDTH1_FACTOR;
            if (pRadioIfGetDB->RadioIfGetAllDB.Dot11NCapaParams.
                u1SuppShortGi40 == OSIX_TRUE)
            {
                f4SgiFactor = (FLT4) RADIO_SGI_FACTOR;
            }
            else
            {
                f4SgiFactor = RADIO_LGI_FACTOR;
            }
            break;
        default:
            break;
    }
/*Mcs Factor STS computation*/
    for (u1Loop = RADIO_VALUE_0; u1Loop < RADIO_DOT11N_MCS_STREAM_MAX; u1Loop++)
    {
        if ((pRadioIfGetDB->RadioIfGetAllDB.
             Dot11NsuppMcsParams.au1SuppRxMcsBitmask[u1Loop] &
             RADIO_DOT11N_MCSRATE_ENABLE) & RADIO_DOT11N_MCSRATE_ENABLE)
        {
            u1SplStream = (UINT1) (u1Loop + 1);
            for (u1McsLoop = RADIO_VALUE_0;
                 u1McsLoop < RADIO_DOT11N_MCS_BIT_MAX; u1McsLoop++)
            {
                if ((pRadioIfGetDB->RadioIfGetAllDB.
                     Dot11NsuppMcsParams.au1SuppRxMcsBitmask[u1Loop]
                     >> u1McsLoop) & RADIO_DOT11N_SHIFT_BIT)
                {
                    u1McsIndex = u1McsLoop;
                }
            }
        }
    }
    switch (u1McsIndex)
    {
        case RADIO_VALUE_0:
            f4McsFactor = RADIO_MCS0_FACTOR;
            break;
        case RADIO_VALUE_1:
            f4McsFactor = RADIO_MCS1_FACTOR;
            break;
        case RADIO_VALUE_2:
            f4McsFactor = RADIO_MCS2_FACTOR;
            break;
        case RADIO_VALUE_3:
            f4McsFactor = RADIO_MCS3_FACTOR;
            break;
        case RADIO_VALUE_4:
            f4McsFactor = RADIO_MCS4_FACTOR;
            break;
        case RADIO_VALUE_5:
            f4McsFactor = RADIO_MCS5_FACTOR;
            break;
        case RADIO_VALUE_6:
            f4McsFactor = RADIO_MCS6_FACTOR;
            break;
        case RADIO_VALUE_7:
            f4McsFactor = RADIO_MCS7_FACTOR;
            break;
        default:
            break;

    }
/*Highest Supported Data Rate*/
    u2DataRate =
        (UINT2) ((u1SplStream * f4BwFactor * f4McsFactor) / f4SgiFactor);
    pRadioIfGetDB->RadioIfGetAllDB.Dot11NsuppMcsParams.u2SuppHighSuppDataRate =
        u2DataRate;
    pRadioIfGetDB->RadioIfIsGetAllDB.bHighSuppDataRate = OSIX_TRUE;
}

#endif
