/*****************************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved                     *
 *  $Id: wssifmac.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *  Description: This file contains the WSS MAC module APIs.                 *
 *                                                                           *
 *****************************************************************************/

#ifndef _WSSIF_MAC_C_
#define _WSSIF_MAC_C_

#include "wssifinc.h"
#include "wssmacproto.h"

/****************************************************************************
 * *                                                                        *
 * * Function     : WssIfProcessWssMacMsg                                   *
 * *                                                                        *
 * * Description  : Calls WSS MAC module API to process fn calls from other *
 * *                WSS modules.                                            *
 * *                                                                        *
 * * Input        : eMsgType - Msg opcode                                   *
 * *                pWssMacMsgStruct - Pointer to WSS MAC msg struct        * 
 * *                                                                        *
 * * Output       : None                                                    *
 * *                                                                        *
 * * Returns      : OSIX_SUCCESS on success                                 *
 * *                OSIX_FAILURE otherwise                                  *
 * *                                                                        *
 * **************************************************************************/

UINT1
WssIfProcessWssMacMsg (UINT4 eMsgType, tWssMacMsgStruct * pWssMacMsgStruct)
{
    if (WssMacProcessWssIfMsg (eMsgType, pWssMacMsgStruct) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

UINT1
WssIfConstructProbeRspStruct (tWssMacMsgStruct * pWssMacReqStruct,
                              tWssMacMsgStruct * pWssMacRspStruct)
{
    if (WssMacConstructProbeRsp (pWssMacReqStruct, pWssMacRspStruct)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
WssMacGetDot11HdrSize (UINT1 *pu1Dot11PktBuf)
{
    INT4                i4HdrSize = 0;
    UINT1               u1Addr4Flag = DOT11_FLAG_NO;
    UINT1               u1QosFlag = DOT11_FLAG_NO;
    tDot11RegFrameHdr   pDot11Hdr;

    if (pu1Dot11PktBuf == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssMacGetDot11HdrSize: pu1Dot11PktBuf is NULL \r\n");
        return i4HdrSize;
    }
    MEMSET (&pDot11Hdr, 0, sizeof (tDot11RegFrameHdr));
    MEMCPY (&pDot11Hdr, pu1Dot11PktBuf, DOT11_REG_HDR_SIZE);

    /* Set the HdrSize to Regular Dot11 frame header size */
    i4HdrSize = DOT11_REG_HDR_SIZE;

    /* Check if the Frame Control's Direction is AP->AP */
    if (((pDot11Hdr.au1FrameCtrl[DOT11_ELEM_BYTE_1]) & DOT11_FC_BYTE1_DIR_ALL)
        == DOT11_FC_BYTE1_DIR_AP_AP)
    {
        u1Addr4Flag = DOT11_FLAG_YES;
    }

    /* Check if the Frame is a QoS Data Frame */
    if (((pDot11Hdr.au1FrameCtrl[DOT11_ELEM_BYTE_0]) &
         (DOT11_FC_BYTE0_TYPE_ALL | DOT11_FC_BYTE0_SUBTYPE_QOS)) ==
        (DOT11_FC_BYTE0_TYPE_DATA | DOT11_FC_BYTE0_SUBTYPE_QOS))
    {
        u1QosFlag = DOT11_FLAG_YES;
    }

    if (u1Addr4Flag == DOT11_FLAG_YES)
    {
        if (u1QosFlag == DOT11_FLAG_YES)
        {
            /* Frame is Reg.Frame + Addr4 + QoSCtrl */
            i4HdrSize = DOT11_FULL_QOS_HDR_SIZE;
        }
        else
        {
            /* Frame is Reg.Frame + Addr4 */
            i4HdrSize = DOT11_FULL_FRAM_HDR_SIZE;
        }
    }
    else
    {
        if (u1QosFlag == DOT11_FLAG_YES)
        {
            /* Frame is Reg.Frame + QoSCtrl */
            i4HdrSize = DOT11_REG_QOS_HDR_SIZE;
        }
    }

    return (i4HdrSize);
}

UINT1              *
WssMacPullPtr (UINT1 **pBuf, INT4 u4NewLen)
{
    (*pBuf) += u4NewLen;

    return *pBuf;
}

UINT1
WssMacDot11ToDot3 (UINT1 **pu1Dot11PktBuf, UINT2 *u2Dot11PktLen)
{
    INT4                i4HdrSize = 0;
    tDot11FullQosFrameHdr Dot11FrmHdr;
    tDot2LlcFrameHdr    LlcHdr, *pLlcHdr = NULL;
    tDot3EthFrameHdr   *pEthHdr = NULL;
    INT4                i4NewHdrSize = 0;
    INT2                i2BufLen = 0;
    UINT2               u2EthType = 0;
    VOID               *pVoidPtr = NULL;
#if WSSMAC_PKT_DUMP
    UINT1               i = 0;
    UINT1              *pTempPtr = NULL
#endif
        i4HdrSize = WssMacGetDot11HdrSize ((*pu1Dot11PktBuf));
    MEMSET (&Dot11FrmHdr, 0, sizeof (tDot11FullQosFrameHdr));
    MEMSET (&LlcHdr, 0, sizeof (tDot2LlcFrameHdr));

    if (i4HdrSize == 0)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssMacDot11ToDot3: Hdr Size of pu1Dot11PktBuf is ZERO \r\n");
        return OSIX_FAILURE;
    }

    MEMCPY (&Dot11FrmHdr, (*pu1Dot11PktBuf), i4HdrSize);

    pVoidPtr = (*pu1Dot11PktBuf) + i4HdrSize;
    MEMCPY ((UINT1 *) &LlcHdr, pVoidPtr, DOT2_LLC_DSAP_SSAP_SIZE);
    MEMCPY (&(LlcHdr.unLlcType.SnapHdr.u1Control),
            (pVoidPtr + DOT2_LLC_DSAP_SSAP_SIZE), sizeof (UINT1));
    MEMCPY (&(LlcHdr.unLlcType.SnapHdr.u1OrgCode),
            (pVoidPtr + DOT2_LLC_DSAP_SSAP_SIZE + sizeof (UINT1)),
            DOT2_ORG_CODE_LEN);
    MEMCPY (&(LlcHdr.unLlcType.SnapHdr.u2EthType),
            (pVoidPtr + DOT2_LLC_DSAP_SSAP_SIZE + sizeof (UINT1)
             + DOT2_ORG_CODE_LEN), sizeof (UINT2));
    pLlcHdr = &LlcHdr;

    if ((CHECK_FOR_SNAP (pLlcHdr)) || (CHECK_FOR_SNAP_BRTUNL (pLlcHdr)))
    {
        u2EthType = LlcHdr.unLlcType.SnapHdr.u2EthType;

        i4NewHdrSize = i4HdrSize + (INT4) DOT2_LLC_HDR_SIZE -
            (INT4) DOT3_ETH_HDR_SIZE;
        i4HdrSize += (INT4) DOT2_LLC_HDR_SIZE;
        if (WssMacPullPtr (&(*pu1Dot11PktBuf), i4NewHdrSize) == NULL)
        {
            WSSIF_TRC (WSSIF_FAILURE_TRC,
                       "WssMacDot11ToDot3: SNAP-WssMacPullPtr returned NULL \r\n");
            return OSIX_FAILURE;
        }
    }
    else
    {
        i4NewHdrSize = i4HdrSize - DOT3_ETH_HDR_SIZE;
        if (WssMacPullPtr (&(*pu1Dot11PktBuf), i4NewHdrSize) == NULL)
        {
            WSSIF_TRC (WSSIF_FAILURE_TRC,
                       "WssMacDot11ToDot3: WssMacPullPtr returned NULL \r\n");
            return OSIX_FAILURE;
        }
    }

    pVoidPtr = NULL;
    pVoidPtr = *pu1Dot11PktBuf;
    pEthHdr = (tDot3EthFrameHdr *) pVoidPtr;

    switch (Dot11FrmHdr.au1FrameCtrl[DOT11_ELEM_BYTE_1] &
            DOT11_FC_BYTE1_DIR_ALL)
    {
        case DOT11_FC_BYTE1_DIR_ST_ST:
            MEMCPY (pEthHdr->au1FrameDAddr,
                    Dot11FrmHdr.au1FrameAddr1, DOT3_HDR_ADDR_LEN);
            MEMCPY (pEthHdr->au1FrameSAddr,
                    Dot11FrmHdr.au1FrameAddr2, DOT3_HDR_ADDR_LEN);
            break;

        case DOT11_FC_BYTE1_DIR_ST_AP:
            MEMCPY (pEthHdr->au1FrameDAddr,
                    Dot11FrmHdr.au1FrameAddr3, DOT3_HDR_ADDR_LEN);
            MEMCPY (pEthHdr->au1FrameSAddr,
                    Dot11FrmHdr.au1FrameAddr2, DOT3_HDR_ADDR_LEN);
            break;

        case DOT11_FC_BYTE1_DIR_AP_ST:
            MEMCPY (pEthHdr->au1FrameDAddr,
                    Dot11FrmHdr.au1FrameAddr1, DOT3_HDR_ADDR_LEN);
            MEMCPY (pEthHdr->au1FrameSAddr,
                    Dot11FrmHdr.au1FrameAddr3, DOT3_HDR_ADDR_LEN);
            break;

            /*case DOT11_FC_BYTE1_DIR_AP_AP:
               MEMCPY(pEthHdr->au1FrameDAddr, 
               Dot11FrmHdr.au1FrameAddr3, DOT3_HDR_ADDR_LEN);
               MEMCPY(pEthHdr->au1FrameSAddr, 
               Dot11FrmHdr.au1FrameAddr4, DOT3_HDR_ADDR_LEN);
               break; */

        default:                /*DOT11_FC_BYTE1_DIR_AP_AP */
            MEMCPY (pEthHdr->au1FrameDAddr,
                    Dot11FrmHdr.au1FrameAddr3, DOT3_HDR_ADDR_LEN);
            MEMCPY (pEthHdr->au1FrameSAddr,
                    Dot11FrmHdr.au1FrameAddr4, DOT3_HDR_ADDR_LEN);
    }

    *u2Dot11PktLen =
        (UINT2) (((*u2Dot11PktLen) - i4HdrSize) + DOT3_ETH_HDR_SIZE);

    if (u2EthType == 0)
    {
        i2BufLen = (INT2) ((*u2Dot11PktLen) - DOT3_ETH_HDR_SIZE);
        pEthHdr->u2FrameEthType = OSIX_HTONS (i2BufLen);
    }
    else
    {
        pEthHdr->u2FrameEthType = u2EthType;
    }

#if WSSMAC_PKT_DUMP
    pTempPtr = (*pu1Dot11PktBuf);
    printf ("\n----------------Converted pkt dump------------------\n");
    for (i = 0; i < 128; pTempPtr++, i++)
    {
        printf (" %x", *pTempPtr);
        if ((i != 0) && (i % 8) == 0)
        {
            printf ("\n");
        }
    }
    printf ("\n----------------End of pkt dump---------------------\n");
#endif

    return OSIX_SUCCESS;
}

UINT1
WssMacDot3ToDot11 (UINT1 **pu1PktBuf, UINT1 *pu1BssId, UINT2 *u4PktLen)
{
    tDot3EthFrameHdr    EthHdr;
    INT4                i4HdrSize = 0;
    UINT1               u1Qos = 0;
    UINT1               au1TempPktBuf[DOT11_MAX_PKT_SIZE];
    tDot11RegQosFrameHdr Dot11QosHdr;
    tDot11RegFrameHdr   Dot11Hdr;
    tDot2LlcFrameHdr    LlcHdr;
#if WSSMAC_PKT_DUMP
    UINT1               i = 0;
#endif

    if (pu1PktBuf == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssMacDot3ToDot11: pu1Dot11PktBuf is NULL \r\n");
        return OSIX_FAILURE;
    }
    else if ((*pu1PktBuf) == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssMacDot3ToDot11: *pu1Dot11PktBuf is NULL \r\n");
        return OSIX_FAILURE;
    }

    if (pu1BssId == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssMacDot3ToDot11: pu1BssId is NULL \r\n");
        return OSIX_FAILURE;
    }

    if (u4PktLen == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssMacDot3ToDot11: u4PktLen is NULL \r\n");
        return OSIX_FAILURE;
    }
    MEMSET (&EthHdr, 0, sizeof (tDot3EthFrameHdr));
    MEMSET (au1TempPktBuf, 0, DOT11_MAX_PKT_SIZE);
    MEMSET (&Dot11QosHdr, 0, sizeof (tDot11RegQosFrameHdr));
    MEMSET (&Dot11Hdr, 0, sizeof (tDot11RegFrameHdr));
    MEMSET (&LlcHdr, 0, sizeof (tDot2LlcFrameHdr));

    MEMCPY (&EthHdr, *pu1PktBuf, DOT3_ETH_HDR_SIZE);

    /* Check for VLAN Type */
    if (EthHdr.u2FrameEthType == DOT1X_ETHTYPE_VLAN)
    {
        u1Qos = 1;
    }

    Dot11QosHdr.au1FrameCtrl[DOT11_ELEM_BYTE_0] =
        Dot11Hdr.au1FrameCtrl[DOT11_ELEM_BYTE_0] = DOT11_FC_BYTE0_VERSION |
        DOT11_FC_BYTE0_TYPE_DATA;

    if (u1Qos)
    {
        Dot11QosHdr.au1FrameCtrl[DOT11_ELEM_BYTE_1] = DOT11_FC_BYTE1_DIR_AP_ST;

        MEMCPY (Dot11QosHdr.au1FrameAddr1, EthHdr.au1FrameDAddr,
                DOT11_HDR_MAC_ADDR_LEN);
        MEMCPY (Dot11QosHdr.au1FrameAddr2, pu1BssId, DOT11_HDR_MAC_ADDR_LEN);
        MEMCPY (Dot11QosHdr.au1FrameAddr3, EthHdr.au1FrameSAddr,
                DOT11_HDR_MAC_ADDR_LEN);

        MEMCPY (au1TempPktBuf, (UINT1 *) &Dot11QosHdr, DOT11_REG_QOS_HDR_SIZE);
        i4HdrSize = DOT11_REG_QOS_HDR_SIZE;
    }
    else
    {
        Dot11Hdr.au1FrameCtrl[DOT11_ELEM_BYTE_1] = DOT11_FC_BYTE1_DIR_AP_ST;

        MEMCPY (Dot11Hdr.au1FrameAddr1,
                EthHdr.au1FrameDAddr, DOT11_HDR_MAC_ADDR_LEN);
        MEMCPY (Dot11Hdr.au1FrameAddr2, pu1BssId, DOT11_HDR_MAC_ADDR_LEN);
        MEMCPY (Dot11Hdr.au1FrameAddr3,
                EthHdr.au1FrameSAddr, DOT11_HDR_MAC_ADDR_LEN);

        MEMCPY (au1TempPktBuf, (UINT1 *) &Dot11Hdr, DOT11_REG_HDR_SIZE);
        i4HdrSize = DOT11_REG_HDR_SIZE;
    }

    if (OSIX_HTONS (EthHdr.u2FrameEthType) >= DOT3_MAX_LEN)
    {
        LlcHdr.u1LlcDsap = LlcHdr.u1LlcSsap = DOT2_SNAP_LSAP;
        LlcHdr.unLlcType.SnapHdr.u1Control = DOT2_UI;

        if ((EthHdr.u2FrameEthType == DOT2_ETHTYPE_AARP) ||
            (EthHdr.u2FrameEthType == DOT2_ETHTYPE_IPX))
        {
            LlcHdr.unLlcType.SnapHdr.u1OrgCode[0] = DOT2_SNAP_ORGCODE;
            LlcHdr.unLlcType.SnapHdr.u1OrgCode[1] = DOT2_SNAP_ORGCODE;
            LlcHdr.unLlcType.SnapHdr.u1OrgCode[2] = DOT2_SNAP_BRTUNL_ORGCODE;
        }
        else
        {
            LlcHdr.unLlcType.SnapHdr.u1OrgCode[0] = DOT2_SNAP_ORGCODE;
            LlcHdr.unLlcType.SnapHdr.u1OrgCode[1] = DOT2_SNAP_ORGCODE;
            LlcHdr.unLlcType.SnapHdr.u1OrgCode[2] = DOT2_SNAP_ORGCODE;
        }

        LlcHdr.unLlcType.SnapHdr.u2EthType = EthHdr.u2FrameEthType;

        MEMCPY ((au1TempPktBuf + i4HdrSize),
                (UINT1 *) &LlcHdr, DOT2_LLC_DSAP_SSAP_SIZE);
        MEMCPY ((au1TempPktBuf + i4HdrSize + DOT2_LLC_DSAP_SSAP_SIZE),
                (UINT1 *) (&(LlcHdr.unLlcType.SnapHdr.u1Control)),
                sizeof (UINT1));
        MEMCPY ((au1TempPktBuf + i4HdrSize + DOT2_LLC_DSAP_SSAP_SIZE +
                 sizeof (UINT1)),
                (UINT1 *) (&(LlcHdr.unLlcType.SnapHdr.u1OrgCode)),
                DOT2_ORG_CODE_LEN);
        MEMCPY ((au1TempPktBuf + i4HdrSize + DOT2_LLC_DSAP_SSAP_SIZE +
                 sizeof (UINT1) + DOT2_ORG_CODE_LEN),
                &(LlcHdr.unLlcType.SnapHdr.u2EthType), sizeof (UINT2));
        i4HdrSize += DOT2_LLC_HDR_SIZE;

        MEMCPY ((au1TempPktBuf + i4HdrSize),
                ((*pu1PktBuf) + DOT3_ETH_HDR_SIZE),
                ((*u4PktLen) - DOT3_ETH_HDR_SIZE));
    }
    else
    {
        MEMCPY ((au1TempPktBuf + i4HdrSize),
                ((*pu1PktBuf) + DOT3_ETH_HDR_SIZE),
                ((*u4PktLen) - DOT3_ETH_HDR_SIZE));
    }

    *u4PktLen = (UINT2) ((*u4PktLen) + i4HdrSize - DOT3_ETH_HDR_SIZE);
    MEMCPY ((*pu1PktBuf), au1TempPktBuf, *u4PktLen);

#if WSSMAC_PKT_DUMP
    UINT1              *pTempPtr = NULL;
    pTempPtr = *pu1PktBuf;

    printf ("Converted Pkt Len = %u\r\n", *u4PktLen);
    printf ("\n----------------Converted pkt dump------------------\n");
    for (i = 0; i < 128; pTempPtr++, i++)
    {
        printf (" %x", *pTempPtr);
        if ((i != 0) && (i % 8) == 0)
        {
            printf ("\n");
        }
    }
    printf ("\n----------------End of pkt dump----------------------\n");
#endif
    return OSIX_SUCCESS;
}

#endif
