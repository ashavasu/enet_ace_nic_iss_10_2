/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wssifrfm.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains the RFMGMT module APIs 
 *                            
 ********************************************************************/

#ifndef _WSSIF_RFM_C_
#define _WSSIF_RFM_C_

#include "wssifinc.h"
#include "rfminc.h"

/****************************************************************************
 * *                                                                        *
 * * Function     : WssIfProcessRfMgmtMsg                                   *
 * *                                                                        *
 * * Description  : Calls RF MGMT module API to process fn calls from other *
 * *                WSS modules.                                            *
 * *                                                                        *
 * * Input        : eMsgType - Msg opcode                                   *
 * *                pRfMgmtMsgStruct - Pointer to RF MGMT msg struct        * 
 * *                                                                        *
 * * Output       : None                                                    *
 * *                                                                        *
 * * Returns      : RFMGMT_SUCCESS on success                               *
 * *                RFMGMT_FAILURE otherwise                                *
 * *                                                                        *
 * **************************************************************************/

UINT1
WssIfProcessRfMgmtMsg(UINT4 eMsgType, tRfMgmtMsgStruct *pRfMgmtMsgStruct)
{
    RFMGMT_LOCK;
    if (RfMgmtProcessWssIfMsg ((UINT1)eMsgType, pRfMgmtMsgStruct) != RFMGMT_SUCCESS)
    {
        RFMGMT_UNLOCK;
        return OSIX_FAILURE;
    }

    RFMGMT_UNLOCK;
    return OSIX_SUCCESS;          
}

#endif
