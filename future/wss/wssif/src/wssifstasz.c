/******************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                       *
 *  $Id: wssifstasz.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $                                                                            *
 * Description: This file contains the MEMPOOL related API for WSSSTA MODULE  *
 ******************************************************************************/

#if 0                            /* This file is no longer needed.Because content of this file is present in wssifauthsz.c and wssifwebauthsz.c */
#ifndef __WSSSTASZ__C
#define __WSSSTASZ__C
#include "wssifinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         WssIfAuthSzRegisterModuleSizingParams (CHR1 * pu1ModName);

/*****************************************************************************
 *                                                                           *
 * Function     : WssIfAuthRecvMemClear                                      *
 *                                                                           *
 * Description  : This Function is used to clear the Mem pool                *
 *                                                                           *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
VOID
WssIfAuthRecvMemClear (VOID)
{

    WssIfAuthSizingMemDeleteMemPools ();
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssIfAuthSizingMemCreateMemPools                           *
 *                                                                           *
 * Description  : This Function is used to create a Mem pool                 *
 *                                                                           *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssIfAuthSizingMemCreateMemPools (VOID)
{

    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < WSSIFAUTH_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (gaFsWSSIFAUTHSizingParams[i4SizingId].
                                     u4StructSize,
                                     gaFsWSSIFAUTHSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(gaWSSIFAUTHMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            WssIfAuthSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssIfAuthSzRegisterModuleSizingParams                      *
 *                                                                           *
 * Description  : This Function is used to register sizing params            *
 *                                                                           *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
WssIfAuthSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{

    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, gaFsWSSIFAUTHSizingParams);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssIfAuthSizingMemDeleteMemPools                           *
 *                                                                           *
 * Description  : This Function is used to delete mem pool                   *
 *                                                                           *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
VOID
WssIfAuthSizingMemDeleteMemPools (VOID)
{

    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < WSSIFAUTH_MAX_SIZING_ID; i4SizingId++)
    {
        if (gaWSSIFAUTHMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (gaWSSIFAUTHMemPoolIds[i4SizingId]);
            gaWSSIFAUTHMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}

/*****************************************************************************
 * Function     : WssIfWebAuthSizingMemCreateMemPools
 * Description  : This Function is used to create a Mem pool
 * Input        : None
 * Output       : None
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
WssIfWebAuthSizingMemCreateMemPools (VOID)
{

    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < WSSIFWEBAUTH_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (gaFsWSSIFWEBAUTHSizingParams[i4SizingId].
                                     u4StructSize,
                                     gaFsWSSIFWEBAUTHSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(gaWSSIFWEBAUTHMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            WssIfWebAuthSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : WssIfWebAuthSzRegisterModuleSizingParams
 * Description  : This Function is used to register sizing params
 * Input        : None
 * Output       : None
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
WssIfWebAuthSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{

    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, gaFsWSSIFWEBAUTHSizingParams);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function     : WssIfWebAuthSizingMemDeleteMemPools
 * Description  : This Function is used to delete mem pool
 *****************************************************************************/
VOID
WssIfWebAuthSizingMemDeleteMemPools (VOID)
{

    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < WSSIFWEBAUTH_MAX_SIZING_ID; i4SizingId++)
    {
        if (gaWSSIFWEBAUTHMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (gaWSSIFAUTHMemPoolIds[i4SizingId]);
            gaWSSIFWEBAUTHMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
#endif
#endif
