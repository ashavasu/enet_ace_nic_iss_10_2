/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wssifpmdb.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains functions to update db related to WSSPM 
 * module.
 ****************************************************************************/
#ifndef __WSSIFPMDB_C__
#define __WSSIFPMDB_C__

#include "wssifinc.h"
#include "wssifcapdb.h"
#include "wssifpmdb.h"
#include "wsspm.h"
#include "wsspmprot.h"

#define MAX_WTP_PROFILE_SUPPORTED   NUM_OF_AP_SUPPORTED
#define MAX_WTP_PROFILE             4097
#define MAX_RADIOS            32

/***************************************************************************
 * FUNCTION NAME    : WssIfProcessPMDBMsg
 *
 * DESCRIPTION      : This function updates the WSSPm related db
 *                    based on the received opcode value.
 *
 * INPUT            : u1MsgType
 *                    pWssIfPMDB
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
INT4
WssIfProcessPMDBMsg (UINT2 u1MsgType, tWssIfPMDB * pWssIfPMDB)
{

    tWssPMInfo          WssPMInfo;
    tWssPmWTPIDStatsProcessRBDB *pWtpIdStatsMap = NULL;
    tWssPmWTPIDRadIDStatsProcessRBDB *pWtpIdRadioIdStatsMap = NULL;
    tWssPmWLANBSSIDStatsProcessRBDB *pBssIdStatsMap = NULL;
    tWssPmCapwATPStatsProcessRBDB *pWlanCapwATPStatsMap = NULL;
    tWssPmWlanClientStatsProcessRBDB *pWlanClientStatsMap = NULL;
    tWssPmWlanRadioStatsProcessRBDB *pWlanRadioStatsMap = NULL;
    tWssPmWlanSSIDStatsProcessRBDB *pWlanSSIDStatsMap = NULL;
    MEMSET (&WssPMInfo, 0, sizeof (tWssPMInfo));
    WSSIF_FN_ENTRY ();

    /* process the opcode received */
    switch (u1MsgType)
    {
        case WSS_PM_WTPPRF_ADD:
            if (pWssIfPMDB->tWtpPmAttState.bWssPmWtpAdd == TRUE)
            {
                WssPMInfo.u2ProfileId = pWssIfPMDB->u2ProfileId;
                WssPMInfo.stats_timer = pWssIfPMDB->stats_timer;
                pmWTPInfoNotify (WTPPRF_ADD, &WssPMInfo);
            }
            break;

        case WSS_PM_WTPPRF_DEL:
            if (pWssIfPMDB->tWtpPmAttState.bWssPmWtpDel == TRUE)
            {
                WssPMInfo.u2ProfileId = pWssIfPMDB->u2ProfileId;
                pmWTPInfoNotify (WTPPRF_DEL, &WssPMInfo);
            }
            break;

        case WSS_PM_BSSID_ADD:
            if (pWssIfPMDB->tWtpPmAttState.bWssPmBSSIDAdd == TRUE)
            {
                WssPMInfo.u2ProfileBSSID = pWssIfPMDB->u2ProfileBSSID;
                pmWTPInfoNotify (BSSID_ADD, &WssPMInfo);
            }
            break;

        case WSS_PM_BSSID_DEL:
            if (pWssIfPMDB->tWtpPmAttState.bWssPmBSSIDDel == TRUE)
            {
                WssPMInfo.u2ProfileBSSID = pWssIfPMDB->u2ProfileBSSID;
                pmWTPInfoNotify (BSSID_DEL, &WssPMInfo);
            }
            break;

        case WSS_PM_RADIO_ADD:

            if (pWssIfPMDB->tWtpPmAttState.bWssPmRadioAdd == TRUE)
            {
                WssPMInfo.u4RadioIfIndex = pWssIfPMDB->u4RadioIfIndex;
                pmWTPInfoNotify (RADIO_ADD, &WssPMInfo);
            }
            break;

        case WSS_PM_RADIO_DEL:

            if (pWssIfPMDB->tWtpPmAttState.bWssPmRadioDel == TRUE)
            {
                WssPMInfo.u4RadioIfIndex = pWssIfPMDB->u4RadioIfIndex;
                pmWTPInfoNotify (RADIO_DEL, &WssPMInfo);
            }
            break;

        case WSS_PM_SSID_ADD:

            if (pWssIfPMDB->tWtpPmAttState.bWssPmSSIDAdd == TRUE)
            {
                WssPMInfo.u4FsDot11WlanProfileId =
                    pWssIfPMDB->u4FsDot11WlanProfileId;
                pmWTPInfoNotify (SSID_ADD, &WssPMInfo);
            }
            break;

        case WSS_PM_SSID_DEL:

            if (pWssIfPMDB->tWtpPmAttState.bWssPmSSIDDel == TRUE)
            {
                WssPMInfo.u4FsDot11WlanProfileId =
                    pWssIfPMDB->u4FsDot11WlanProfileId;
                pmWTPInfoNotify (SSID_DEL, &WssPMInfo);
            }
            break;

        case WSS_PM_CLIENT_ADD:

            if (pWssIfPMDB->tWtpPmAttState.bWssPmClientAdd == TRUE)
            {
                MEMCPY (&WssPMInfo.FsWlanClientStatsMACAddress,
                        &pWssIfPMDB->FsWlanClientStatsMACAddress,
                        sizeof (tMacAddr));
                pmWTPInfoNotify (CLIENT_ADD, &WssPMInfo);
            }
            break;

        case WSS_PM_CLIENT_DEL:

            if (pWssIfPMDB->tWtpPmAttState.bWssPmClientDel == TRUE)
            {
                MEMCPY (&WssPMInfo.FsWlanClientStatsMACAddress,
                        &pWssIfPMDB->FsWlanClientStatsMACAddress,
                        sizeof (tMacAddr));
                pmWTPInfoNotify (CLIENT_DEL, &WssPMInfo);
            }
            break;

        case WSS_PM_CAPW_ATP_ADD:

            if (pWssIfPMDB->tWtpPmAttState.bWssPmCapwATPAdd == TRUE)
            {
                WssPMInfo.u4CapwapBaseWtpProfileId =
                    pWssIfPMDB->u4CapwapBaseWtpProfileId;
                pmWTPInfoNotify (CAPWATP_ADD, &WssPMInfo);
            }
            break;

        case WSS_PM_CAPW_ATP_DEL:

            if (pWssIfPMDB->tWtpPmAttState.bWssPmCapwATPDel == TRUE)
            {
                WssPMInfo.u4CapwapBaseWtpProfileId =
                    pWssIfPMDB->u4CapwapBaseWtpProfileId;
                pmWTPInfoNotify (CAPWATP_DEL, &WssPMInfo);
            }
            break;

        case WSS_PM_WTPPRF_STAT_TIMER_RESET:
            if ((pWssIfPMDB->tWtpPmAttState.bWssPmWtpAdd == TRUE)
                && (pWssIfPMDB->tWtpPmAttState.bWssPmStatTmrReset))
            {
                WssPMInfo.u2ProfileId = pWssIfPMDB->u2ProfileId;
                WssPMInfo.stats_timer = pWssIfPMDB->stats_timer;
                pmWTPInfoNotify (STAT_TIMER_RESET, &WssPMInfo);
            }
            break;

        case WSS_PM_WTP_EVT_REBOOT_STATS_SET:
            if (pWssIfPMDB->tWtpPmAttState.bWssPmRbtStatsSet == TRUE)
            {
                WssPMInfo.u2ProfileId = pWssIfPMDB->u2ProfileId;
                MEMCPY (&WssPMInfo.wtpEventInfo.rebootStats,
                        &pWssIfPMDB->tWtpRebootStatistics,
                        sizeof (tWtpRebootStats));
                pmWTPEvtReqNotify (REBOOT_STATS_SET, &WssPMInfo);
            }
            break;

        case WSS_PM_WTP_EVT_RADIO_STATS_SET:
            if (pWssIfPMDB->tWtpPmAttState.bWssPmRadStatsSet == TRUE)
            {
                WssPMInfo.u2ProfileId = pWssIfPMDB->u2ProfileId;
                WssPMInfo.u4RadioIfIndex = pWssIfPMDB->u4RadioIfIndex;
                MEMCPY (&WssPMInfo.wtpEventInfo.radioStats,
                        &pWssIfPMDB->tRadioStatistics, sizeof (tWtpRadioStats));
                pmWTPEvtReqNotify (RADIO_STATS_SET, &WssPMInfo);
            }
            break;

        case WSS_PM_WTP_EVT_WTP_802_11_STATS_SET:
            if (pWssIfPMDB->tWtpPmAttState.bWssPmIEEE80211StatsSet == TRUE)
            {
                WssPMInfo.u2ProfileId = pWssIfPMDB->u2ProfileId;
                WssPMInfo.u4RadioIfIndex = pWssIfPMDB->u4RadioIfIndex;
                MEMCPY (&WssPMInfo.wtpEventInfo.dot11Stats,
                        &pWssIfPMDB->tdot11Statistics,
                        sizeof (tDot11Statistics));
                pmWTPEvtReqNotify (IEEE_802_11_STATS_SET, &WssPMInfo);
            }
            break;

        case WSS_PM_WTP_EVT_WTP_CAPWAP_SESS_STATS_SET:
            if (pWssIfPMDB->tWtpPmAttState.bWssPmWtpEvtWtpCWStatsSet == TRUE)
            {
                WssPMInfo.u2ProfileId = pWssIfPMDB->u2ProfileId;
                MEMCPY (&WssPMInfo.wtpEventInfo.capwapSessStats,
                        &pWssIfPMDB->tWtpCWSessStats,
                        sizeof (tWtpCapwapSessStats));
                pmWTPEvtReqNotify (CAPWAP_SESS_STATS_SET, &WssPMInfo);
            }
            break;
        case WSS_PM_WTP_EVT_VSP_BSSID_STATS_SET:
            if (pWssIfPMDB->tWtpPmAttState.bWssPmVSPBSSIDStatsSet == TRUE)
            {
                MEMCPY (&WssPMInfo.wtpEventInfo.bssIdMachdlrStats,
                        &pWssIfPMDB->tWtpVSPBSSIDStats,
                        sizeof (tBSSIDMacHndlrStats));
                WssPMInfo.u2ProfileBSSID = pWssIfPMDB->u2ProfileBSSID;
                pmWTPEvtReqNotify (VSP_BSSID_STATS_SET, &WssPMInfo);
            }
            break;
        case WSS_PM_WTP_EVT_VSP_SSID_STATS_SET:
            if (pWssIfPMDB->tWtpPmAttState.bWssPmVSPSSIDStatsSet == TRUE)
            {
                MEMCPY (&WssPMInfo.wtpEventInfo.bssIdMachdlrStats,
                        &pWssIfPMDB->tWtpVSPBSSIDStats,
                        sizeof (tBSSIDMacHndlrStats));
                WssPMInfo.u4FsDot11WlanProfileId = 
                            pWssIfPMDB->u4FsDot11WlanProfileId;
                pmWTPEvtReqNotify (VSP_SSID_STATS_SET, &WssPMInfo);
            }
            break;
        case WSS_PM_WTP_EVT_VSP_RADIO_STATS_SET:
            if (pWssIfPMDB->tWtpPmAttState.bWssPmVSPRadioStatsSet == TRUE)
            {
                MEMCPY (&WssPMInfo.wtpEventInfo.radioClientStats,
                        &pWssIfPMDB->radioClientStats, sizeof (tWtpRCStats));
                WssPMInfo.u4RadioIfIndex = pWssIfPMDB->u4RadioIfIndex;
                pmWTPEvtReqNotify (VSP_RADIO_STATS_SET, &WssPMInfo);
            }
            break;
        case WSS_PM_WTP_EVT_VSP_CLIENT_STATS_SET:
            if (pWssIfPMDB->tWtpPmAttState.bWssPmVSPClientStatsSet == TRUE)
            {
                WssPMInfo.wtpEventInfo.clientStats.u4ClientStatsBytesSentCount = 
                    pWssIfPMDB->clientStats.u4ClientStatsBytesSentCount;
                WssPMInfo.wtpEventInfo.clientStats.u4ClientStatsBytesRecvdCount = 
                    pWssIfPMDB->clientStats.u4ClientStatsBytesRecvdCount;
                WssPMInfo.wtpEventInfo.clientStats.u4StaIpAddr=
                    pWssIfPMDB->clientStats.u4StaIpAddr;


                MEMCPY (WssPMInfo.FsWlanClientStatsMACAddress,
                        pWssIfPMDB->FsWlanClientStatsMACAddress,
                        sizeof (tMacAddr));
                pmWTPEvtReqNotify (CLIENT_STATS_SET, &WssPMInfo);
            }
            break;
        case WSS_PM_WTP_EVT_VSP_AP_STATS_SET:
            if (pWssIfPMDB->tWtpPmAttState.bWssPmVSPApStatsSet == TRUE)
            {
                WssPMInfo.u2ProfileId = pWssIfPMDB->u2ProfileId;
                MEMCPY (&WssPMInfo.wtpEventInfo.ApElement,
                        &pWssIfPMDB->ApElement, sizeof (tApParams));
                pmWTPEvtReqNotify (VSP_AP_STATS_SET, &WssPMInfo);
            }
            break;
        case WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_SET:
            {
                WssPMInfo.u2ProfileId = pWssIfPMDB->u2ProfileId;
                MEMCPY (&WssPMInfo.wtpEventInfo.wtpCapwapStats,
                        &pWssIfPMDB->wtpVSPCPWPWtpStats, sizeof (tWtpCPWPWtpStats));
                pmWTPEvtReqNotify (CAPWAP_SESS_STATS_SET, &WssPMInfo);
            }
            break;
        case WSS_PM_WTP_EVT_VSP_CAPWAP_STATS_GET:
            {
                pWtpIdStatsMap = WssIfWtpIDStatsEntryGet (pWssIfPMDB->u2ProfileId);
                if (pWtpIdStatsMap == NULL)
                {
                    WSSIF_TRC (WSSIF_FAILURE_TRC,
                               "pWtpIdStatsMap from DB(WTP profile ID) is NULL \r\n");
                    return OSIX_FAILURE;
                }
                MEMCPY (&pWssIfPMDB->wtpVSPCPWPWtpStats,
                        &pWtpIdStatsMap->CapwapSessStats,
                        sizeof (tWtpCPWPWtpStats));
            }
            break;
        case WSS_PM_WLC_CAPWAP_STATS_SET:
            WssPMInfo.u2ProfileId = pWssIfPMDB->u2ProfileId;

			WssPMInfo.wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                                 bWssPmWLCCapwapDiscReqRxd =
				pWssIfPMDB->tWtpPmAttState.wssPmWLCStatsSetDB.
                                 bWssPmWLCCapwapDiscReqRxd;

			WssPMInfo.wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                                bWssPmWLCCapwapDiscRespSent =
				pWssIfPMDB->tWtpPmAttState.wssPmWLCStatsSetDB.
                                bWssPmWLCCapwapDiscRespSent;

			WssPMInfo.wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                                bWssPmWLCCapwapDiscUnsuccReqProcessed =
				pWssIfPMDB->tWtpPmAttState.wssPmWLCStatsSetDB.
                                bWssPmWLCCapwapDiscUnsuccReqProcessed;

			WssPMInfo.wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                                bWssPmWLCCapwapDiscReasonLastUnsuccAtt =
				pWssIfPMDB->tWtpPmAttState.wssPmWLCStatsSetDB.
                                bWssPmWLCCapwapDiscReasonLastUnsuccAtt;

			WssPMInfo.wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                                bWssPmWLCCapwapDiscLastSuccAttTime =
				pWssIfPMDB->tWtpPmAttState.wssPmWLCStatsSetDB.
                                bWssPmWLCCapwapDiscLastSuccAttTime;

			WssPMInfo.wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                                bWssPmWLCCapwapDiscLastUnsuccAttTime =
				pWssIfPMDB->tWtpPmAttState.wssPmWLCStatsSetDB.
                                bWssPmWLCCapwapDiscLastUnsuccAttTime;

			WssPMInfo.wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                                bWssPmWLCCapwapJoinReqRxd =
				pWssIfPMDB->tWtpPmAttState.wssPmWLCStatsSetDB.
                                bWssPmWLCCapwapJoinReqRxd;

			WssPMInfo.wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                                bWssPmWLCCapwapJoinRespSent =
				pWssIfPMDB->tWtpPmAttState.wssPmWLCStatsSetDB.
                                bWssPmWLCCapwapJoinRespSent;

			WssPMInfo.wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                                bWssPmWLCCapwapJoinUnsuccReqProcessed =
				pWssIfPMDB->tWtpPmAttState.wssPmWLCStatsSetDB.
                                bWssPmWLCCapwapJoinUnsuccReqProcessed;

			WssPMInfo.wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                                bWssPmWLCCapwapJoinReasonForLastUnsuccAtt =
				pWssIfPMDB->tWtpPmAttState.wssPmWLCStatsSetDB.
                                bWssPmWLCCapwapJoinReasonForLastUnsuccAtt;

			WssPMInfo.wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                                bWssPmWLCCapwapJoinLastSuccAttTime =
				pWssIfPMDB->tWtpPmAttState.wssPmWLCStatsSetDB.
                                bWssPmWLCCapwapJoinLastSuccAttTime;

			WssPMInfo.wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                                bWssPmWLCCapwapJoinLastUnsuccAttTime =
				pWssIfPMDB->tWtpPmAttState.wssPmWLCStatsSetDB.
                                bWssPmWLCCapwapJoinLastUnsuccAttTime;

			WssPMInfo.wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                                bWssPmWLCCapwapCfgRespRcvd =
				pWssIfPMDB->tWtpPmAttState.wssPmWLCStatsSetDB.
                                bWssPmWLCCapwapCfgRespRxd;

			WssPMInfo.wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                                bWssPmWLCCapwapCfgReqSent =
				pWssIfPMDB->tWtpPmAttState.wssPmWLCStatsSetDB.
                                bWssPmWLCCapwapCfgReqSent;

			WssPMInfo.wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                                bWssPmWLCCapwapCfgUnsuccRespProcessed =
				pWssIfPMDB->tWtpPmAttState.wssPmWLCStatsSetDB.
                                bWssPmWLCCapwapCfgUnsuccRespProcessed;

			WssPMInfo.wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                                bWssPmWLCCapwapCfgReasonForLastUnsuccAtt =
				pWssIfPMDB->tWtpPmAttState.wssPmWLCStatsSetDB.
                                bWssPmWLCCapwapCfgReasonForLastUnsuccAtt;

			WssPMInfo.wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                                bWssPmWLCCapwapCfgLastSuccAttTime =
				pWssIfPMDB->tWtpPmAttState.wssPmWLCStatsSetDB.
                                bWssPmWLCCapwapCfgLastSuccAttTime;

			WssPMInfo.wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                                bWssPmWLCCapwapCfgLastUnsuccAttTime =
				pWssIfPMDB->tWtpPmAttState.wssPmWLCStatsSetDB.
                                bWssPmWLCCapwapCfgLastUnsuccAttTime;

			WssPMInfo.wssPmIntAttr.WssPmWLCCapwapStatsSetFlag.
                                bWssPmWLCCapwapKeepAlivePkts =
				pWssIfPMDB->tWtpPmAttState.wssPmWLCStatsSetDB.
                                bWssPmWLCCapwapKeepAlivePkts;


            pmWTPEvtReqNotify (CAPWAP_WLC_SESS_STATS_SET, &WssPMInfo);
            break;

        case WSS_PM_WLC_CAPWAP_RUN_STATS_SET:
            WssPMInfo.u2ProfileId = pWssIfPMDB->u2ProfileId;
            MEMCPY (&(WssPMInfo.wssPmIntAttr.WssPmWLCCapwapStatsSetFlag),
                    &(pWssIfPMDB->WssPmWLCCapwapStatsSetFlag),
                    sizeof (tWssPmWLCCapwapStatsSetDB));
            pmWTPEvtReqNotify (CAPWAP_WLC_SESS_RUN_STATS_SET, &WssPMInfo);
            break;


        case WSS_PM_CLI_CPWP_DISC_STATS_GET:
            pWtpIdStatsMap = WssIfWtpIDStatsEntryGet (pWssIfPMDB->u2ProfileId);
            if (pWtpIdStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWtpIdStatsMap from DB(WTP profile ID) is NULL \r\n");
                return OSIX_FAILURE;
            }
            MEMCPY (&pWssIfPMDB->tWtpCWSessStats.wtpCapwapSessDiscStats,
                    &pWtpIdStatsMap->pWtpCapwapSessStats.wtpCapwapSessDiscStats,
                    sizeof (tWtpCapwapSessDiscStats));
            break;

        case WSS_PM_CLI_CAPWAP_DISC_STATS_CLEAR:
            pWtpIdStatsMap = WssIfWtpIDStatsEntryGet (pWssIfPMDB->u2ProfileId);
            if (pWtpIdStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWtpIdStatsMap from DB(WTP profile ID) is NULL \r\n");
                return OSIX_FAILURE;
            }
            MEMSET (&pWtpIdStatsMap->pWtpCapwapSessStats.wtpCapwapSessDiscStats,
                    0, sizeof (tWtpCapwapSessDiscStats));
            break;
        case WSS_PM_CLI_CPWP_JOIN_STATS_GET:
            pWtpIdStatsMap = WssIfWtpIDStatsEntryGet (pWssIfPMDB->u2ProfileId);
            if (pWtpIdStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWtpIdStatsMap from DB(WTP profile ID) is NULL \r\n");
                return OSIX_FAILURE;
            }
            MEMCPY (&pWssIfPMDB->tWtpCWSessStats.wtpCapwapSessJoinStats,
                    &pWtpIdStatsMap->pWtpCapwapSessStats.wtpCapwapSessJoinStats,
                    sizeof (tWtpCapwapSessJoinStats));
            break;
        case WSS_PM_CLI_CPWP_JOIN_STATS_CLEAR:
            pWtpIdStatsMap = WssIfWtpIDStatsEntryGet (pWssIfPMDB->u2ProfileId);
            if (pWtpIdStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWtpIdStatsMap from DB(WTP profile ID) is NULL \r\n");
                return OSIX_FAILURE;
            }
            MEMSET (&pWtpIdStatsMap->pWtpCapwapSessStats.wtpCapwapSessJoinStats,
                    0, sizeof (tWtpCapwapSessJoinStats));
            break;
        case WSS_PM_CLI_CAPWAP_CONFIG_STATS_GET:
            pWtpIdStatsMap = WssIfWtpIDStatsEntryGet (pWssIfPMDB->u2ProfileId);
            if (pWtpIdStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWtpIdStatsMap from DB(WTP profile ID) is NULL \r\n");
                return OSIX_FAILURE;
            }
            MEMCPY (&pWssIfPMDB->tWtpCWSessStats.wtpCapwapSessConfigStats,
                    &pWtpIdStatsMap->pWtpCapwapSessStats.
                    wtpCapwapSessConfigStats,
                    sizeof (tWtpCapwapSessConfigStats));
            break;
        case WSS_PM_CLI_CAPWAP_CONFIG_STATS_CLEAR:
            pWtpIdStatsMap = WssIfWtpIDStatsEntryGet (pWssIfPMDB->u2ProfileId);
            if (pWtpIdStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWtpIdStatsMap from DB(WTP profile ID) is NULL \r\n");
                return OSIX_FAILURE;
            }
            MEMSET (&pWtpIdStatsMap->pWtpCapwapSessStats.
                    wtpCapwapSessConfigStats, 0,
                    sizeof (tWtpCapwapSessConfigStats));
            break;
        case WSS_PM_CLI_CAPWAP_RUN_STATS_GET:
            pWtpIdStatsMap = WssIfWtpIDStatsEntryGet (pWssIfPMDB->u2ProfileId);
            if (pWtpIdStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWtpIdStatsMap from DB(WTP profile ID) is NULL \r\n");
                return OSIX_FAILURE;
            }
            pWssIfPMDB->tWtpCWSessStats.wtpKeepAlivePktsSent =
                pWtpIdStatsMap->pWtpCapwapSessStats.wtpKeepAlivePktsSent;
            break;
        case WSS_PM_CLI_CAPWAP_RUN_STATS_CLEAR:
            pWtpIdStatsMap = WssIfWtpIDStatsEntryGet (pWssIfPMDB->u2ProfileId);
            if (pWtpIdStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWtpIdStatsMap from DB(WTP profile ID) is NULL \r\n");
                return OSIX_FAILURE;
            }
            pWtpIdStatsMap->pWtpCapwapSessStats.wtpKeepAlivePktsSent = 0;
            break;
        case WSS_PM_CLI_ALL_WTP_CAPWAP_STATS_GET:
            pWtpIdStatsMap = WssIfWtpIDStatsEntryGet (pWssIfPMDB->u2ProfileId);
            if (pWtpIdStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWtpIdStatsMap from DB(WTP profile ID) is NULL \r\n");
                return OSIX_FAILURE;
            }
            MEMCPY (&pWssIfPMDB->tWtpCWSessStats,
                    &pWtpIdStatsMap->pWtpCapwapSessStats,
                    sizeof (tWtpCapwapSessStats));
            break;

        case WSS_PM_CLI_ALL_WTP_CAPWAP_STATS_CLEAR:
            pWtpIdStatsMap = WssIfWtpIDStatsEntryGet (pWssIfPMDB->u2ProfileId);
            if (pWtpIdStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWtpIdStatsMap from DB(WTP profile ID) is NULL \r\n");
                return OSIX_FAILURE;
            }
            MEMSET (&pWtpIdStatsMap->pWtpCapwapSessStats,
                    0, sizeof (tWtpCapwapSessStats));
            break;

        case WSS_PM_CLI_CAPWAP_AP_RADIO_STATS_GET:
            pWtpIdRadioIdStatsMap = WssIfWtpIDRadIDStatsEntryGet ((UINT2)
                                                                  (pWssIfPMDB->
                                                                   u4RadioIfIndex));
            if (pWtpIdRadioIdStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWtpIdRadioIdStatsMap from DB(WTP profile ID) is NULL \r\n");
                return OSIX_FAILURE;
            }
            MEMCPY (&pWssIfPMDB->tRadioStatistics,
                    &pWtpIdRadioIdStatsMap->pWtpRadioStats,
                    sizeof (tWtpRadioStats));
            break;

        case WSS_PM_CLI_CAPWAP_AP_RADIO_STATS_CLEAR:
            pWtpIdRadioIdStatsMap = WssIfWtpIDRadIDStatsEntryGet ((UINT2)
                                                                  (pWssIfPMDB->
                                                                   u4RadioIfIndex));
            if (pWtpIdRadioIdStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWtpIdRadioIdStatsMap from DB(WTP profile ID) is NULL \r\n");
                return OSIX_FAILURE;
            }

            MEMSET (&pWtpIdRadioIdStatsMap->pWtpRadioStats,
                    0, sizeof (tWtpRadioStats));
            break;

        case WSS_PM_CLI_CAPWAP_SHOW_AP_IEEE_80211_STATS_GET:
            pWtpIdRadioIdStatsMap = WssIfWtpIDRadIDStatsEntryGet ((UINT2)
                                                                  (pWssIfPMDB->
                                                                   u4RadioIfIndex));
            if (pWtpIdRadioIdStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWtpIdRadioIdStatsMap from DB(WTP profile ID) is NULL \r\n");
                return OSIX_FAILURE;
            }
            MEMCPY (&pWssIfPMDB->tdot11Statistics,
                    &pWtpIdRadioIdStatsMap->pWtpdot11Stats,
                    sizeof (tDot11Statistics));
            break;
        case WSS_PM_CLI_CAPWAP_SHOW_AP_IEEE_80211_STATS_CLEAR:
            pWtpIdRadioIdStatsMap = WssIfWtpIDRadIDStatsEntryGet ((UINT2)
                                                                  (pWssIfPMDB->
                                                                   u4RadioIfIndex));
            if (pWtpIdRadioIdStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWtpIdRadioIdStatsMap from DB(WTP profile ID) is NULL \r\n");
                return OSIX_FAILURE;
            }
            MEMSET (&pWtpIdRadioIdStatsMap->pWtpdot11Stats,
                    0, sizeof (tDot11Statistics));
            break;
        case WSS_PM_CLI_WTP_REBOOT_STATS_GET:
            pWtpIdStatsMap = WssIfWtpIDStatsEntryGet (pWssIfPMDB->u2ProfileId);
            if (pWtpIdStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWtpIdStatsMap from DB(WTP profile ID) is NULL \r\n");
                return OSIX_FAILURE;
            }
            MEMCPY (&pWssIfPMDB->tWtpRebootStatistics,
                    &pWtpIdStatsMap->pWtpRebootStat, sizeof (tWtpRebootStats));
            break;
        case WSS_PM_CLI_WTP_REBOOT_STATS_CLEAR:
            pWtpIdStatsMap = WssIfWtpIDStatsEntryGet (pWssIfPMDB->u2ProfileId);
            if (pWtpIdStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWtpIdStatsMap from DB(WTP profile ID) is NULL \r\n");
                return OSIX_FAILURE;
            }
            MEMSET (&pWtpIdStatsMap->pWtpRebootStat,
                    0, sizeof (tWtpRebootStats));
            break;
        case WSS_PM_CLI_WTP_ID_STATS_CLEAR:
            pWtpIdStatsMap = WssIfWtpIDStatsEntryGet (pWssIfPMDB->u2ProfileId);
            if (pWtpIdStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWtpIdStatsMap from DB(WTP profile ID) is NULL \r\n");
                return OSIX_FAILURE;
            }
            MEMSET (&pWtpIdStatsMap->pWtpCapwapSessStats,
                    0, sizeof (tWtpCapwapSessStats));
            MEMSET (&pWtpIdStatsMap->pWtpRebootStat,
                    0, sizeof (tWtpRebootStats));
            MEMSET (&pWtpIdStatsMap->CapwapSessStats,
                    0, sizeof (tWtpCPWPWtpStats));

            pWlanCapwATPStatsMap =
                WssIfCapwATPStatsEntryGet (pWssIfPMDB->u2ProfileId);
            if (pWlanCapwATPStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWlanCapwATPStatsMap from DB(WTP profile ID) is NULL \r\n");
                return OSIX_FAILURE;
            }
            pWlanCapwATPStatsMap->pApElement.u4SentBytes = 0;
            pWlanCapwATPStatsMap->pApElement.u4RcvdBytes = 0;
            pWlanCapwATPStatsMap->pApElement.u4SentTrafficRate = 0;
            pWlanCapwATPStatsMap->pApElement.u4RcvdTrafficRate = 0;
            break;
        case WSS_PM_CLI_ALL_WTP_STATS_GET:
            break;
        case WSS_PM_CLI_BSS_ID_STATS_GET:
            pBssIdStatsMap = WssIfWLANBSSIDStatsEntryGet ((UINT2)
                                                          (pWssIfPMDB->
                                                           u2ProfileBSSID));
            if (pBssIdStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pBssIdStatsMap from DB(BssIfIndex) is NULL \r\n");
                return OSIX_FAILURE;
            }
            MEMCPY (&pWssIfPMDB->wtpBSSIDStnStats,
                    &pBssIdStatsMap->pBSSIDStnStats, sizeof (tBSSIDStnStats));
            break;
        case WSS_PM_WTP_EVT_VSP_BSSID_STATS_GET:
            pBssIdStatsMap =
                WssIfWLANBSSIDStatsEntryGet ((UINT2)pWssIfPMDB->u2ProfileBSSID);
            if (pBssIdStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pBssIdStatsMap from DB(BssIfIndex) is NULL \r\n");
                return OSIX_FAILURE;
            }
            MEMCPY (&pWssIfPMDB->tWtpVSPBSSIDStats,
                    &pBssIdStatsMap->pBSSIDMacHndlrStats,
                    sizeof (tBSSIDMacHndlrStats));
            break;
        case WSS_PM_WTP_EVT_VSP_SSID_STATS_GET:
            pWlanSSIDStatsMap =
                WssIfWlanSSIDStatsEntryGet (pWssIfPMDB->u4FsDot11WlanProfileId);
            if (pWlanSSIDStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWlanSSIDStatsMap from DB is NULL \r\n");
                return OSIX_FAILURE;
            }
            MEMCPY (&pWssIfPMDB->tWtpVSPBSSIDStats,
                    &pWlanSSIDStatsMap->pSSIDMacHndlrStats,
                    sizeof (tBSSIDMacHndlrStats));
            break;
        case WSS_PM_WTP_EVT_VSP_RADIO_STATS_GET:
            pWlanRadioStatsMap =
                WssIfWlanRadioStatsEntryGet ((INT4) pWssIfPMDB->u4RadioIfIndex);
            if (pWlanRadioStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWlanRadioStatsMap from DB(Radio Index) is NULL \r\n");
                return OSIX_FAILURE;
            }

            MEMCPY (&pWssIfPMDB->radioClientStats,
                    &pWlanRadioStatsMap->pradioClientStats,
                    sizeof (tWtpRCStats));
            break;

        case WSS_PM_WTP_EVT_VSP_CLIENT_STATS_GET:
            pWlanClientStatsMap =
                WssIfWlanClientStatsEntryGet (pWssIfPMDB->
                                              FsWlanClientStatsMACAddress);
            if (pWlanClientStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWlanClientStatsMap from DB(Client) is NULL \r\n");
                return OSIX_FAILURE;
            }

            MEMCPY (&pWssIfPMDB->clientStats,
                    &pWlanClientStatsMap->pclientStats, sizeof (tWtpStaStats));
            break;
        case WSS_PM_WTP_EVT_VSP_AP_STATS_GET:
            pWlanCapwATPStatsMap =
                WssIfCapwATPStatsEntryGet (pWssIfPMDB->u2ProfileId);
            if (pWlanCapwATPStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWlanCapwATPStatsMap from DB(Client) is NULL \r\n");
                return OSIX_FAILURE;
            }
            MEMCPY (&pWssIfPMDB->ApElement,
                    &pWlanCapwATPStatsMap->pApElement, sizeof (tApParams));
            break;
        case WSS_PM_CLI_RADIO_STATS_CLEAR:
            pWlanRadioStatsMap =
                WssIfWlanRadioStatsEntryGet ((INT4)
                                             (pWssIfPMDB->u4RadioIfIndex));
            if (pWlanRadioStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWlanRadioStatsMap from DB(WTP profile ID) is NULL \r\n");
                return OSIX_FAILURE;
            }
            MEMSET (&pWlanRadioStatsMap->pradioClientStats,
                    0, sizeof (tWtpRCStats));
            break;
        case WSS_PM_CLI_CLIENT_STATS_CLEAR:
            pWlanClientStatsMap =
                WssIfWlanClientStatsEntryGet (pWssIfPMDB->
                                              FsWlanClientStatsMACAddress);
            if (pWlanClientStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pWlanClientStatsMap from DB(WTP profile ID) is NULL \r\n");
                return OSIX_FAILURE;
            }
            MEMSET (&pWlanClientStatsMap->pclientStats,
                    0, sizeof (tWtpStaStats));
            break;
        case WSS_PM_CLI_BSS_ID_STATS_CLEAR:
            pBssIdStatsMap = WssIfWLANBSSIDStatsEntryGet ((UINT2)
                                                          (pWssIfPMDB->
                                                           u2ProfileBSSID));
            if (pBssIdStatsMap == NULL)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "pBssIdStatsMap from DB(BssIfIndex) is NULL \r\n");
                return OSIX_FAILURE;
            }
            MEMSET (&pBssIdStatsMap->pBSSIDStnStats,
                    0, sizeof (tBSSIDStnStats));
            break;
        case WSS_PM_CLI_PM_CLEAR_STATS_SET:
            break;
        default:
            break;
    }
    return OSIX_SUCCESS;
}

#endif
