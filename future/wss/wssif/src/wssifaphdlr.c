/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                      *
 *                                                                           *
 * $Id: wssifaphdlr.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $                                                                     *
 * Description: This file contains the ApHdlr Interface functions            *
 *****************************************************************************/
#ifndef __WSSIFAPHDLR_C__

#include "wssifinc.h"
#include "iss.h"
#include "wtpnvram.h"
#include "aphdlrprot.h"
#include "radioifconst.h"
static tWssIfApHdlrDB WssIfApDB;
extern UINT1        gau1OperationalRate_RadioB[RADIOIF_OPER_RATE];
extern UINT1        gau1OperationalRate_RadioA[RADIOIF_OPER_RATE];
/************************************************************************/
/*  Function Name   : WssIfProcessApHdlrDBMsg                           */
/*  Description     : The function is the ApHdlr DB interface for other */
/*                    WSS modules.                                      */
/*  Input(s)        : u1MsgType - Message Type                          */
/*                    pWssIfApHdlrDB - Message Data                     */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
WssIfProcessApHdlrDBMsg (UINT1 u1MsgType, tWssIfApHdlrDB * pWssIfApHdlrDB)
{
    WSSIF_FN_ENTRY ();

    switch (u1MsgType)
    {
        case WSS_APHDLR_GET_WTPREBOOT_STATS:
            pWssIfApHdlrDB->u2RebootCount = REBOOT_COUNT_NOT_AVAILABLE;
            pWssIfApHdlrDB->u2LinkFailCount = WssIfApDB.u2LinkFailCount;
            pWssIfApHdlrDB->u2AcInitiatedCount = WssIfApDB.u2AcInitiatedCount;
            pWssIfApHdlrDB->u2SwFailCount = WssIfApDB.u2SwFailCount;
            pWssIfApHdlrDB->u2HwFailCount = WssIfApDB.u2HwFailCount;
            pWssIfApHdlrDB->u2OtherFailCount = WssIfApDB.u2OtherFailCount;
            pWssIfApHdlrDB->u2UnknownFailCount = WssIfApDB.u2UnknownFailCount;
            pWssIfApHdlrDB->u1LastFailureType = WssIfApDB.u1LastFailureType;
            break;
        case WSS_APHDLR_SET_WTPREBOOT_STATS:
            WssIfApDB.u2RebootCount = pWssIfApHdlrDB->u2RebootCount;
            WssIfApDB.u2LinkFailCount = pWssIfApHdlrDB->u2LinkFailCount;
            WssIfApDB.u2AcInitiatedCount = pWssIfApHdlrDB->u2AcInitiatedCount;
            WssIfApDB.u2SwFailCount = pWssIfApHdlrDB->u2SwFailCount;
            WssIfApDB.u2HwFailCount = pWssIfApHdlrDB->u2HwFailCount;
            WssIfApDB.u2OtherFailCount = pWssIfApHdlrDB->u2OtherFailCount;
            WssIfApDB.u2UnknownFailCount = pWssIfApHdlrDB->u2UnknownFailCount;
            WssIfApDB.u1LastFailureType = pWssIfApHdlrDB->u1LastFailureType;
            break;

        case WSS_APHDLR_GET_LOCAL_IPV4:
            pWssIfApHdlrDB->u4LocalIpAddr = WssIfApDB.u4LocalIpAddr;
            break;
        case WSS_APHDLR_SET_AP_STATS:
            WssIfApDB.ApStats.u4SentBytes = pWssIfApHdlrDB->ApStats.u4SentBytes;
            WssIfApDB.ApStats.u4RcvdBytes = pWssIfApHdlrDB->ApStats.u4RcvdBytes;
            break;
        case WSS_APHDLR_GET_AP_STATS:
            pWssIfApHdlrDB->ApStats.u4SentBytes = WssIfApDB.ApStats.u4SentBytes;
            pWssIfApHdlrDB->ApStats.u4RcvdBytes = WssIfApDB.ApStats.u4RcvdBytes;
            break;
        default:
            return OSIX_FAILURE;
    }
    WSSIF_FN_EXIT ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : WssIfProcessApHdlrMsg                             */
/*  Description     : The function is the ApHdlr interface for other    */
/*                    WSS modules.                                      */
/*  Input(s)        : MsgType - Message Type                            */
/*                    pWssIfMsg - Message Data                          */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
WssIfProcessApHdlrMsg (UINT1 MsgType, unApHdlrMsgStruct * pWssIfMsg)
{
    if (ApHdlrProcessWssIfMsg (MsgType, pWssIfMsg) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

#ifdef BCNMGR_WANTED
INT4
WssifUpdateBeacon (UINT4 u4RadioIfIndex, UINT4 u4WlanIfIndex)
{

    tBcnMgrMsgStruct    BcnMgrMsgStruct;
    tWssWlanDB          WssWlanDB;
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               au1TempSuppRate[RADIOIF_OPER_RATE];
    UINT1               u1Index2 = 0;
    UINT1               u1Index3 = 0;
    UINT1               u1Count = 0;
    INT1                u1MCSBitMask;

    MEMSET (au1TempSuppRate, 0, sizeof (au1TempSuppRate));
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&BcnMgrMsgStruct, 0, sizeof (tBcnMgrMsgStruct));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex = u4WlanIfIndex;
    WssWlanDB.WssWlanIsPresentDB.bBssId = OSIX_TRUE;
    WssWlanDB.WssWlanIsPresentDB.bDesiredSsid = OSIX_TRUE;
    WssWlanDB.WssWlanIsPresentDB.bKeyStatus = OSIX_TRUE;
    WssWlanDB.WssWlanIsPresentDB.bRSNAIEElements = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg
        (WSS_WLAN_GET_IFINDEX_ENTRY, &WssWlanDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    MEMCPY (BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1Bssid,
            WssWlanDB.WssWlanAttributeDB.BssId, MAC_ADDR_LEN);

    MEMCPY (BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.au1Ssid,
            WssWlanDB.WssWlanAttributeDB.au1DesiredSsid,
            STRLEN (WssWlanDB.WssWlanAttributeDB.au1DesiredSsid));

    BcnMgrMsgStruct.u2TimerStart = OSIX_TRUE;

    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bBeaconPeriod = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bSupportedRate = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bOperationalRate = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bFirstChannelNumber = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bNoOfChannels = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bMCSRateSet = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bChannelWidth = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bShortGi20 = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bShortGi40 = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg
        (WSS_GET_RADIO_IF_DB, &RadioIfGetDB) != OSIX_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC, "WssWlanProcessConfigRequest:\
                        RadioDBMsg failed to return MacAddr\r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    if (RadioIfGetDB.RadioIfGetAllDB.u2BeaconPeriod != 0)
    {
        BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u2BeaconPeriod =
            RadioIfGetDB.RadioIfGetAllDB.u2BeaconPeriod;
    }
    else
    {
        BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u2BeaconPeriod = 100;

    }

    if (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == 2)
    {
        /* Radio Type a */
        BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u2Capability = 0x0104;
    }
    else if (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == 1)
    {
        /* Radio Type b */
        BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u2Capability = 0x2100;
    }
    else if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == 4) ||
             (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == 13))
    {
        /* Radio Type g or ng */
        BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u2Capability = 0x2104;
    }
    /*Ext support rate */
    if (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_B)
    {
        MEMCPY (BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.au1SuppRate,
                (gau1OperationalRate_RadioB),
                sizeof (gau1OperationalRate_RadioB));
    }
    else if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_A)
             || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_G)
             || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_AN)
             || (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
                 RADIO_TYPE_NG))
    {
        MEMCPY (BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.au1SuppRate,
                (gau1OperationalRate_RadioA),
                sizeof (gau1OperationalRate_RadioA));
    }
    else if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
              RADIO_TYPE_BG) ||
             (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_BGN))
    {
        for (u1Count = VALUE_0; u1Count < RADIOIF_OPER_RATE; u1Count++)
        {
            if (u1Count < VALUE_4)
            {
                BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.au1SuppRate[u1Count] =
                    gau1OperationalRate_RadioB[u1Count];
            }
            else
            {
                BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.au1SuppRate[u1Count] =
                    gau1OperationalRate_RadioA[u1Count - VALUE_4];
            }
        }

        MEMSET (BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.au1ExtSuppRates,
                VALUE_0, WSSMAC_MAX_EXT_SUPP_RATES);

        for (u1Count = VALUE_4; u1Count < RADIOIF_OPER_RATE; u1Count++)
        {
            BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.au1ExtSuppRates[u1Count -
                                                                  VALUE_4] =
                gau1OperationalRate_RadioA[u1Count];
        }
        BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1ExtSupport =
            WSSMAC_EXT_SUPPORT_ENABLE;
    }

    if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
         RADIO_TYPE_BGN) ||
        (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
         RADIO_TYPE_AN) ||
        (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_NG))
    {
        if (RadioIfGetDB.RadioIfGetAllDB.u1ChannelWidth ==
            DOT11N_CHANNEL_WIDTH40)
        {
            BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTCapabilities.u2HTCapInfo =
                BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTCapabilities.
                u2HTCapInfo | (VALUE_1 << SHIFT_VALUE_1);
        }
        else if (RadioIfGetDB.RadioIfGetAllDB.u1ChannelWidth ==
                 DOT11N_CHANNEL_WIDTH20)
        {
            BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTCapabilities.u2HTCapInfo =
                BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTCapabilities.
                u2HTCapInfo & (~(VALUE_1 << SHIFT_VALUE_1));
        }
        if (RadioIfGetDB.RadioIfGetAllDB.u1ShortGi20 ==
            DOT11N_GI_SHORT20_ENABLE)
        {
            BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTCapabilities.u2HTCapInfo =
                BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTCapabilities.
                u2HTCapInfo | (VALUE_1 << SHIFT_VALUE_5);

        }
        else
        {
            BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTCapabilities.u2HTCapInfo =
                BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTCapabilities.
                u2HTCapInfo & (~(VALUE_1 << SHIFT_VALUE_5));
        }
        if (RadioIfGetDB.RadioIfGetAllDB.u1ShortGi40 ==
            DOT11N_GI_SHORT40_ENABLE)
        {
            BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTCapabilities.u2HTCapInfo =
                BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTCapabilities.
                u2HTCapInfo | (VALUE_1 << SHIFT_VALUE_6);
        }
        else
        {
            BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTCapabilities.u2HTCapInfo =
                BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTCapabilities.
                u2HTCapInfo & (~(VALUE_1 << SHIFT_VALUE_6));
        }

        MEMSET (BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTCapabilities.
                au1SuppMCSSet, VALUE_0, WSSMAC_SUPP_MCS_SET);
        for (u1MCSBitMask = VALUE_0; u1MCSBitMask < RADIOIF_11N_MCS_RATE_LEN;
             u1MCSBitMask++)
        {
            for (u1Index2 = VALUE_0; u1Index2 < BIT_8; u1Index2++)
            {
                if ((RadioIfGetDB.RadioIfGetAllDB.au1MCSRateSet[u1MCSBitMask]
                     & (VALUE_1 << u1Index2)) != VALUE_0)
                {
                    BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTCapabilities.
                        au1SuppMCSSet[u1MCSBitMask] |= (VALUE_1 << u1Index2);
                }
            }
        }
        MEMSET (&BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTCapabilities.
                u1AMPDUParam, VALUE_0,
                sizeof (BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTCapabilities.
                        u1AMPDUParam));
        MEMSET (&BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTCapabilities.
                u2HTExtCap, VALUE_0,
                sizeof (BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTCapabilities.
                        u2HTExtCap));
        MEMSET (&BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTCapabilities.
                u4TranBeamformCap, VALUE_0,
                sizeof (BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTCapabilities.
                        u4TranBeamformCap));
        MEMSET (&BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTCapabilities.u1ASELCap,
                VALUE_0,
                sizeof (BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTCapabilities.
                        u1ASELCap));

        /* HT Operation for 802.11n */
        BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTOperation.u1PrimaryCh =
            RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;
        MEMSET (BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTOperation.au1HTOpeInfo,
                VALUE_0, WSSMAC_HTOPE_INFO);
        MEMSET (BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.HTOperation.
                au1BasicMCSSet, VALUE_0, WSSMAC_BASIC_MCS_SET);
        BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1Dot11Support =
            DOT11N_SUPPORT_ENABLE;
    }

    BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1DsCurChannel =
        RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;

    BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1DtimPeriod =
        RadioIfGetDB.RadioIfGetAllDB.u1DTIMPeriod;

    MEMCPY (BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.au1CountryStr,
            RadioIfGetDB.RadioIfGetAllDB.au1CountryString,
            sizeof (BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.au1CountryStr));
    BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1FirstChanlNum =
        RadioIfGetDB.RadioIfGetAllDB.u2FirstChannelNumber;
    BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1MaxTxPowerLevel =
        RadioIfGetDB.RadioIfGetAllDB.u2MaxTxPowerLevel;

    BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1MaxTxPowerLevel = 19;

    BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1NumOfChanls =
        RadioIfGetDB.RadioIfGetAllDB.u2NoOfChannels;

    if (WssWlanDB.WssWlanAttributeDB.u1KeyStatus == 0)
    {
        if (WssWlanDB.WssWlanAttributeDB.RsnaIEElements.u1ElemId != 0)
        {
            BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u2Capability = 0x3101;

            BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u1KeyStatus = 1;
            BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u2RSN_Version =
                WssWlanDB.WssWlanAttributeDB.RsnaIEElements.u2Ver;

            MEMCPY (&
                    (BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.
                     u4RSN_GrpCipherSuit),
                    WssWlanDB.WssWlanAttributeDB.RsnaIEElements.
                    au1GroupCipherSuite, RSNA_CIPHER_SUITE_LEN);
            BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.
                u2RSN_PairCiphSuitCnt =
                WssWlanDB.WssWlanAttributeDB.RsnaIEElements.
                u2PairwiseCipherSuiteCount;

            for (u1Count = 0;
                 u1Count <
                 WssWlanDB.WssWlanAttributeDB.RsnaIEElements.
                 u2PairwiseCipherSuiteCount; u1Count++)
            {
                MEMCPY (&
                        (BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.
                         aRSN_PairCiphSuitList[u1Count]),
                        WssWlanDB.WssWlanAttributeDB.RsnaIEElements.
                        aRsnaPwCipherDB[u1Count].au1PairwiseCipher,
                        RSNA_CIPHER_SUITE_LEN);
            }

            BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u2RSN_AKMSuitCnt =
                WssWlanDB.WssWlanAttributeDB.RsnaIEElements.u2AKMSuiteCount;

            for (u1Count = 0;
                 u1Count <
                 WssWlanDB.WssWlanAttributeDB.RsnaIEElements.
                 u2AKMSuiteCount; u1Count++)
            {
                MEMCPY (&
                        (BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.
                         au4RSN_AKMSuitList[u1Count]),
                        WssWlanDB.WssWlanAttributeDB.RsnaIEElements.
                        aRsnaAkmDB[u1Count].au1AKMSuite, RSNA_AKM_SELECTOR_LEN);
            }

            BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u2RSN_Capability =
                WssWlanDB.WssWlanAttributeDB.RsnaIEElements.u2RsnCapab;
            BcnMgrMsgStruct.unBcn.BcnMgrBcnParams.u2RSN_PMKIdCnt =
                WssWlanDB.WssWlanAttributeDB.RsnaIEElements.u2PmkidCount;
        }
    }

    if (WssIfProcessBcnMgrMsg (BCNMGR_RECV_CONF_UPDATE,
                               &BcnMgrMsgStruct) != OSIX_SUCCESS)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssWlanProcessConfigRequest: "
                   "Beacon Manager failed to make updations\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                         RadioIfGetAllDB.pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.
                                     RadioIfGetAllDB.pu1AntennaSelection);

    return OSIX_SUCCESS;

}

INT4
WssifUpdateBeaconParamForAllBssId (tRadioIfGetDB * pRadioIfGetDB)
{
    tWssWlanDB          WssWlanDB;
    UINT4               u4Index = 0;
    UINT1               u1RadioIndex = 0;
    UINT1               u1MaxEnetInterface = 0;

    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));

    pRadioIfGetDB->RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  (pRadioIfGetDB)) != OSIX_SUCCESS)
    {
        UtlShMemFreeAntennaSelectionBuf (pRadioIfGetDB->RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    if (pRadioIfGetDB->RadioIfGetAllDB.u1BssIdCount == 0)
    {
        pRadioIfGetDB->RadioIfGetAllDB.u1WlanId = 1;
    }
    else
    {
        for (u4Index = 1; u4Index <= RADIO_MAX_BSSID_SUPPORTED; u4Index++)
        {
            WssWlanDB.WssWlanAttributeDB.u1WlanId = (UINT1) u4Index;
            u1RadioIndex =
                (UINT1) pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
            u1MaxEnetInterface = (UINT1) SYS_DEF_MAX_ENET_INTERFACES;
            WssWlanDB.WssWlanAttributeDB.u1RadioId =
                u1RadioIndex - u1MaxEnetInterface;

            WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg
                (WSS_WLAN_GET_INTERFACE_ENTRY, &WssWlanDB) != OSIX_SUCCESS)
            {
                continue;
            }

            if (WssifUpdateBeacon
                (pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex,
                 WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex) != OSIX_SUCCESS)
            {
                WSSIF_TRC (WSSIF_FAILURE_TRC,
                           "\n\nBEACON UPDATION FAILED \n\n");
            }

        }                        /* for */

    }                            /* else */

    return OSIX_SUCCESS;

}

#endif /* beacon */

#endif
