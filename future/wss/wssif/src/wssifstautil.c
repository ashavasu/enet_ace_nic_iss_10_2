/******************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                       *
 *  $Id: wssifstautil.c,v 1.3 2017/11/24 10:37:10 siva Exp $
 *  *
 * Description: This file contains the Station DB related API for             *
 *              WSSSTA MODULE                                                 *
 ******************************************************************************/

#ifndef __WSSIFAUTH_UTIL__C
#define __WSSIFAUTH_UTIL__C
#include "wssifinc.h"
#include "wlchdlr.h"
#include "iss.h"
#include "pnac.h"
#include "pnachdrs.h"
#include  "wssstawlcprot.h"
#include "userrole.h"
#include "wssstawlcinc.h"
#ifdef WSSUSER_WANTED
#include "userinc.h"
#endif
#include "fswebnm.h"
#ifdef KERNEL_CAPWAP_WANTED
#include "fcusprot.h"
#endif
#include "wssifpmdb.h"
#include "wsspm.h"
#include "wsspmprot.h"
#include "wssstawlcmacr.h"
#include "wsscfgprot.h"

tRBTree             gWssStaStateDB;
extern tRBTree      gWssStaWepProcessDB;
static tRBTree      gWssStaStationLdDB;
static tRBTree      gWssStaAssoCntDB;
tWssStaDBWalkMac    gaWssStaDBWalkMac[MAX_STA_SUPP_PER_WLC];
static UINT4        gu4WalkIndex = 0;
tWssClientSummary   gaWssClientSummary[MAX_STA_SUPP_PER_WLC];
UINT4               gu4ClientWalkIndex = 0;
tRBTree             gWssBssIdStatsDB;
tRBTree             gWssStaAuthSessionDB;
extern tRBTree      gWssStaWebAuthDB;

/*****************************************************************************
 *                                                                           *
 * Function     : WssIfAuthInit                                              *
 *                                                                           *
 * Description  : Memory creation and initialization required for            *
 *                WssSta module                                              *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
UINT4
WssIfAuthInit (VOID)
{

    if (WssifauthSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssIfAuthSizingMemCreateMemPools: "
                    "Mem Pool Creation Failed \r\n");
        return OSIX_FAILURE;
    }

    gWssStaStateDB = RBTreeCreateEmbedded ((FSAP_OFFSETOF (tWssIfAuthStateDB,
                                                           nextWssIfAuthStateDB)),
                                           WssStaCompareStaDBRBTree);
    if (gWssStaStateDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaInit: RB Tree Creation Failed for Station DB \r\n");
        return OSIX_FAILURE;
    }

    gWssStaStationLdDB =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssIfAuthLoadBalanceDB, nextStaLoadBalanceDB)),
                              WssStaCompareStaLOADRBTree);
    if (gWssStaStationLdDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaInit: RB Tree Creation Failed "
                    "for Station Load DB \r\n");
        return OSIX_FAILURE;
    }

    gWssStaAssoCntDB =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF
                               (tWssIfAuthWtpAssocCountInfo,
                                nextWtpAssocCountInfo)),
                              WssStaCompareAssoCntRBTree);
    if (gWssStaAssoCntDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaInit: RB Tree Creation Failed "
                    "for Association Count DB \r\n");
    }

    gWssBssIdStatsDB =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tWssIfBssIdStatsDB,
                                              nextWssIfBssIdStatDB)),
                              WssStaCompareBSSIDRBTree);
    if (gWssBssIdStatsDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaInit: RB Tree Creation Failed for Bss ID Station "
                    "Stats DB \r\n");
    }

    /* WEB AUTH timer DB to be used for maintaining station conect time
     * information for lifetime support */
    gWssStaAuthSessionDB =
        RBTreeCreateEmbedded ((FSAP_OFFSETOF (tWssStaAuthSessionDB,
                                              nextWssStaAuthSessionDB)),
                              WssStaCompareAuthSessionRBTree);
    if (gWssStaAuthSessionDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssStaInit: RB Tree Creation Failed for WEB auth timer DB "
                    "Stats DB \r\n");
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : WssIfAuthRecvMemClear                                      *
 *                                                                           *
 * Description  : This Function is used to clear the Mem pool                *
 *                                                                           *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
VOID
WssIfAuthRecvMemClear (VOID)
{

    WssifauthSizingMemDeleteMemPools ();
    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : StaDBWalkFn                                                *
 *                                                                           *
 * Description  : This function is used to walk through station DB           *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
StaDBWalkFn (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
             void *pArg, void *pOut)
{

    tWssIfAuthStateDB  *pStaStateDB = NULL;
    UINT4               u4BssIfIndex = 0;
    UINT1               u1Index = 0;
    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (pOut);

    u4BssIfIndex = *(UINT4 *) pArg;

    if ((visit == postorder) || (visit == leaf))
    {
        if (pRBElem != NULL)
        {
            pStaStateDB = (tWssIfAuthStateDB *) pRBElem;
            u1Index = pStaStateDB->u1Index;
            if (pStaStateDB->aWssStaBSS[u1Index].u4BssIfIndex == u4BssIfIndex)
            {
                if (gu4WalkIndex < MAX_STA_SUPP_PER_WLC)
                {
                    MEMCPY (gaWssStaDBWalkMac[gu4WalkIndex].staMacAddr,
                            pStaStateDB->stationMacAddress, sizeof (tMacAddr));
                    gu4WalkIndex++;
                }
            }
        }
    }
    return RB_WALK_CONT;
}

/*****************************************************************************
 *  Function Name   : WssIfStaDelStation                                     *
 *                                                                           *
 *  Description     : This function used to delete all the stations          *
 *                    belong to a BSSIfIndex.                                *
 *  Input(s)        : WssStaDelProfile                                       *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT4
WssIfStaDelStation (tWssStaDelProfile WssStaDelProfile)
{
    tWssIfAuthStateDB  *pStaStateDB = NULL;
    UINT4               u4BssIfIndex = WssStaDelProfile.u4BssIfIndex;
    tRadioIfGetDB       radioIfgetDB;
    UINT1               u1RadioId;
    unWlcHdlrMsgStruct *pwlcHdlrMsg = NULL;
    INT1                IndexToDelDB = 0;
    UINT4               Index = 0;
    UINT2               u2SessId = 0;
    tWssifauthDBMsgStruct *pWssifauthDBMsgStruct = NULL;
    UINT1               u1IndexToGetStaDB = 0;
    tWssWlanDB          wssWlanDB;
    tMacAddr            BssId;
    tMacAddr            WlcMacAddr;
    UINT4               u4RadioIfIndex = 0;

    UNUSED_PARAM (pStaStateDB);
    UNUSED_PARAM (u2SessId);
    pwlcHdlrMsg = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pwlcHdlrMsg == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssIfStaDelStation:- "
                    "UtlShMemAllocWlcBuf returned failure\n");
        return OSIX_FAILURE;
    }

    gu4WalkIndex = 0;
    RBTreeWalk (gWssStaStateDB, (tRBWalkFn) StaDBWalkFn, &u4BssIfIndex, 0);

    MEMSET (&radioIfgetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&BssId, 0, sizeof (tMacAddr));

    if (gu4WalkIndex)
    {
        wssWlanDB.WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
        wssWlanDB.WssWlanIsPresentDB.bBssId = OSIX_TRUE;
        wssWlanDB.WssWlanAttributeDB.u4BssIfIndex = u4BssIfIndex;

        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                      &wssWlanDB))
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssIfStaDelStation:: "
                        "Failed to retrieve data from WssWlanDB\r\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
            return OSIX_FAILURE;
        }
        u4RadioIfIndex = wssWlanDB.WssWlanAttributeDB.u4RadioIfIndex;
        MEMCPY (BssId, wssWlanDB.WssWlanAttributeDB.BssId, sizeof (tMacAddr));
        radioIfgetDB.RadioIfGetAllDB.pu1AntennaSelection =
            UtlShMemAllocAntennaSelectionBuf ();

        if (radioIfgetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                        " selection failed.\r\n");
            UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
            return OSIX_FAILURE;
        }

        radioIfgetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
        radioIfgetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIfIndex;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &radioIfgetDB))
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssIfStaDelStation:: "
                        "Failed to retrieve data from RadioIfDB\r\n");
            UtlShMemFreeAntennaSelectionBuf (radioIfgetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
            return OSIX_FAILURE;
        }
        u1RadioId = radioIfgetDB.RadioIfGetAllDB.u1RadioId;
        UtlShMemFreeAntennaSelectionBuf (radioIfgetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);

        MEMCPY (WlcMacAddr, IssGetSwitchMacFromNvRam (), sizeof (tMacAddr));

        for (Index = 0; Index < gu4WalkIndex; Index++)
        {
            pWssifauthDBMsgStruct =
                (tWssifauthDBMsgStruct *) (VOID *)
                UtlShMemAllocWssIfAuthDbBuf ();
            if (pWssifauthDBMsgStruct == NULL)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WssIfStaDelStation:- "
                            "UtlShMemAllocWssIfAuthDbBuf returned failure\n");
                UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
                return OSIX_FAILURE;
            }
            MEMSET (pWssifauthDBMsgStruct, 0, sizeof (tWssifauthDBMsgStruct));
            MEMCPY (pWssifauthDBMsgStruct->WssIfAuthStateDB.stationMacAddress,
                    gaWssStaDBWalkMac[Index].staMacAddr, sizeof (tMacAddr));
            pWssifauthDBMsgStruct->WssIfAuthStateDB.
                aWssStaBSS[u1IndexToGetStaDB].u4BssIfIndex = u4BssIfIndex;
            if (WssIfProcessWssAuthDBMsg
                (WSS_AUTH_GET_STATION_DB,
                 pWssifauthDBMsgStruct) == OSIX_SUCCESS)
            {
                u2SessId =
                    pWssifauthDBMsgStruct->WssIfAuthStateDB.
                    aWssStaBSS[u1IndexToGetStaDB].u2SessId;
                MEMSET (pwlcHdlrMsg, 0, sizeof (tStationConfReq));
                pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.isPresent =
                    TRUE;
                pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.u2MessageType =
                    DELETE_STATION;
                pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.
                    u2MessageLength = WSSSTA_DELSTA_LEN;
                pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.u1RadioId =
                    u1RadioId;
                pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.u1MacAddrLen =
                    sizeof (tMacAddr);
                pwlcHdlrMsg->StationConfReq.wssStaConfig.u2SessId = u2SessId;
                MEMCPY (pwlcHdlrMsg->StationConfReq.wssStaConfig.DelSta.
                        StaMacAddr, gaWssStaDBWalkMac[Index].staMacAddr,
                        sizeof (tMacAddr));
            }
            if (WssStaDelProfile.u1MsgType == WSSSTA_CLEAR_CONFIG)
            {
                /* For clear config , do only the DB deletion */
                MEMSET (pWssifauthDBMsgStruct, 0,
                        sizeof (tWssifauthDBMsgStruct));
                MEMCPY (pWssifauthDBMsgStruct->WssIfAuthStateDB.
                        stationMacAddress, gaWssStaDBWalkMac[Index].staMacAddr,
                        sizeof (tMacAddr));
                pWssifauthDBMsgStruct->WssIfAuthStateDB.
                    aWssStaBSS[IndexToDelDB].u4BssIfIndex = u4BssIfIndex;
                if (WssIfProcessWssAuthDBMsg
                    (WSS_AUTH_DELETE_DB, pWssifauthDBMsgStruct) != OSIX_SUCCESS)
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssIfStaDelStation:: "
                                "WSS_WLCHDLR_MAC_DEAUTH_MSG returned failure\r\n");
                    UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                pWssifauthDBMsgStruct);
                    return OSIX_FAILURE;

                }
            }
            else
            {
                if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_STATION_CONF_REQ,
                                            pwlcHdlrMsg) == OSIX_SUCCESS)
                {
                    MEMSET (pWssifauthDBMsgStruct, 0,
                            sizeof (tWssifauthDBMsgStruct));
                    MEMCPY (pWssifauthDBMsgStruct->WssIfAuthStateDB.
                            stationMacAddress,
                            gaWssStaDBWalkMac[Index].staMacAddr,
                            sizeof (tMacAddr));
                    pWssifauthDBMsgStruct->WssIfAuthStateDB.
                        aWssStaBSS[IndexToDelDB].u4BssIfIndex = u4BssIfIndex;
                    if (WssIfProcessWssAuthDBMsg (WSS_AUTH_DELETE_DB,
                                                  pWssifauthDBMsgStruct) ==
                        OSIX_SUCCESS)
                    {
                    }
                    else
                    {
                        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssIfStaDelStation:: "
                                    "WSS_AUTH_DELETE_DB returned failure\r\n");
                        UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
                        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                    pWssifauthDBMsgStruct);
                        return OSIX_FAILURE;
                    }
                }
                else
                {
                    WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssIfStaDelStation:: "
                                "WSS_WLCHDLR_STATION_CONF_REQ returned failure\r\n");
                    UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
                    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *)
                                                pWssifauthDBMsgStruct);
                    return OSIX_FAILURE;
                }
            }
#ifdef KERNEL_CAPWAP_WANTED
            if (CapwapRemoveKernelStaTable (gaWssStaDBWalkMac[Index].staMacAddr)
                == OSIX_FAILURE)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "Failed to update Kernel DB \r\n");
                UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
                return OSIX_FAILURE;
            }
#endif
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssifauthDBMsgStruct);
        }
        UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
        return OSIX_SUCCESS;
    }
    else
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssIfStaDelStation::gu4WalkIndex is "
                    "NULL\r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pwlcHdlrMsg);
        return OSIX_SUCCESS;
    }
}

/*****************************************************************************
 *  Function Name   : WssStaDeAuthStation                                    *
 *                                                                           *
 *  Description     : This function is used to delete a station when         *
 *                  : DELETE_STATION msg comes to WLC                        *
 *  Input(s)        : tWssStaDelStation                                      *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT4
WssStaDeAuthStation (tWssStaDeleteStation WssStaDelStation)
{

    tWssIfAuthStateDB   WssStaDB;
    tWssPMInfo          WssPmInfo;
    tWssIfAuthStateDB  *pStaStateDB = NULL;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;

#ifdef RSNA_WANTED
    tWssWlanDB         *pwssWlanDB = NULL;
    BOOL1               bSessDelete = OSIX_FALSE;
#endif

#ifdef WSSUSER_WANTED
    tWssUserNotifyParams WssUserNotifyParams;

    MEMSET (&WssUserNotifyParams, 0, sizeof (tWssUserNotifyParams));
#endif

    MEMSET (&WssStaDB, 0, sizeof (tWssIfAuthStateDB));
    MEMCPY (WssStaDB.stationMacAddress,
            WssStaDelStation.StaMacAddr, sizeof (tMacAddr));

#ifdef KERNEL_CAPWAP_WANTED
    if (CapwapRemoveKernelStaTable (WssStaDB.stationMacAddress) == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "Failed to update Kernel DB \r\n");
        return OSIX_FAILURE;
    }
#endif

    MEMSET (&WssPmInfo, 0, sizeof (tWssPMInfo));
    MEMCPY (&WssPmInfo.FsWlanClientStatsMACAddress,
            WssStaDelStation.StaMacAddr, MAC_ADDR_LEN);
    pStaStateDB = ((tWssIfAuthStateDB *) RBTreeGet (gWssStaStateDB,
                                                    (tRBElem *) & WssStaDB));
    if (pStaStateDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_MGMT_TRC, "Entry is not present in Station DB \r\n");
        return OSIX_FAILURE;
    }

    pWssStaWepProcessDB = WssStaProcessEntryGet (WssStaDelStation.StaMacAddr);
    if (pWssStaWepProcessDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_MGMT_TRC, "Entry is not present in Station DB \r\n");
        return OSIX_FAILURE;
    }

#ifdef  RSNA_WANTED
    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();

    MEMCPY (pwssWlanDB->WssWlanAttributeDB.BssId,
            pWssStaWepProcessDB->BssIdMacAddr, sizeof (tMacAddr));

    pwssWlanDB->WssWlanIsPresentDB.bBssId = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY, pwssWlanDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssIfGetProfileIfIndex:: "
                    "Failed to retrieve data from WssWlanDB\r\n");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_FAILURE;
    }
    RsnaHandleExistingStationAssocRequest (WssStaDelStation.StaMacAddr,
                                           pwssWlanDB->WssWlanAttributeDB.
                                           u4BssIfIndex, &bSessDelete);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
#endif

#ifdef WSSUSER_WANTED
#if defined (WLC_WANTED) && defined (LNXIP4_WANTED)
    WssUserSetBandWidth (pStaStateDB->stationMacAddress,
                         pWssStaWepProcessDB->u4BandWidth,
                         pWssStaWepProcessDB->u4DLBandWidth,
                         pWssStaWepProcessDB->u4ULBandWidth, OSIX_TRUE);
#endif
    MEMCPY (WssUserNotifyParams.staMac, pStaStateDB->stationMacAddress,
            MAC_ADDR_LEN);
    WssUserNotifyParams.eWssUserNotifyType = WSSUSER_SESSION_DELETE;
    WssUserNotifyParams.u4TerminateCause = IDLE_TIMEOUT;
    if (WssUserProcessNotification (&WssUserNotifyParams) == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "User-Session entry deletion returns failure\n");
    }
#endif

    WssFreeAssocId (pWssStaWepProcessDB);
    pWssStaWepProcessDB->u2AssociationID = 0;
    RBTreeRem (gWssStaStateDB, (tRBElem *) pStaStateDB);
    MemReleaseMemBlock (WSSIFAUTH_STADB_POOLID, (UINT1 *) pStaStateDB);
    if (pWssStaWepProcessDB != NULL)
    {
        RBTreeRem (gWssStaWepProcessDB, (tRBElem *) pWssStaWepProcessDB);
        MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                            (UINT1 *) pWssStaWepProcessDB);
    }

    if (pmWTPInfoNotify (CLIENT_DEL, &WssPmInfo) != OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "Delete Client entry : Update PM Stats DB Failed \r\n");
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : WssStaDeleteStation                                  *
 *                                                                           *
 * Description        : This functions Removes the Station entries for the   *
 *                      particular Bssid                                     *
 *                                                                           *
 * Input(s)           : u4BssIfIndex  - Bssid in which the associated        *
 *                      station entries to be removed.                       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if entries are removed                 *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
WssStaDeleteStation (UINT4 u4BssIfIndex)
{
    tWssIfAuthStateDB  *pStaDB = NULL;
    tWssIfAuthStateDB  *pNextStaDB = NULL;

#ifdef WSSUSER_WANTED
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    tWssUserNotifyParams WssUserNotifyParams;

    MEMSET (&WssUserNotifyParams, 0, sizeof (tWssUserNotifyParams));
#endif

    pStaDB = ((tWssIfAuthStateDB *) RBTreeGetFirst (gWssStaStateDB));
    if (pStaDB == NULL)
    {
        return OSIX_SUCCESS;
    }
    do
    {
        if (u4BssIfIndex == pStaDB->aWssStaBSS[0].u4BssIfIndex)
        {
            pNextStaDB = ((tWssIfAuthStateDB *) RBTreeGetNext (gWssStaStateDB,
                                                               (tRBElem *)
                                                               pStaDB, NULL));
#ifdef WSSUSER_WANTED
            pWssStaWepProcessDB =
                WssStaProcessEntryGet (pStaDB->stationMacAddress);

            if (pWssStaWepProcessDB != NULL)
            {
#if defined (WLC_WANTED) && defined (LNXIP4_WANTED)
                WssUserSetBandWidth (pStaDB->stationMacAddress,
                                     pWssStaWepProcessDB->u4BandWidth,
                                     pWssStaWepProcessDB->u4DLBandWidth,
                                     pWssStaWepProcessDB->u4ULBandWidth,
                                     OSIX_TRUE);
#endif
            }

            MEMCPY (WssUserNotifyParams.staMac, pStaDB->stationMacAddress,
                    MAC_ADDR_LEN);
            WssUserNotifyParams.eWssUserNotifyType = WSSUSER_SESSION_DELETE;
            WssUserNotifyParams.u4TerminateCause = LOST_SERVICE;

            if (WssUserProcessNotification (&WssUserNotifyParams)
                == OSIX_FAILURE)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "RadioIfDeleteStation: User-Session entry deletion returns failure\n");
            }
#endif
            RBTreeRem (gWssStaStateDB, (tRBElem *) pStaDB);
            MemReleaseMemBlock (WSSIFAUTH_STADB_POOLID, (UINT1 *) pStaDB);
            WssIfWlanClientStatsEntryDelete (pStaDB->stationMacAddress);
#ifdef WSSUSER_WANTED
            if (pWssStaWepProcessDB != NULL)
            {
                RBTreeRem (gWssStaWepProcessDB,
                           (tRBElem *) pWssStaWepProcessDB);
                MemReleaseMemBlock (WSSSTA_AUTHDB_POOLID,
                                    (UINT1 *) pWssStaWepProcessDB);
            }
#endif
        }
        else
        {

            pNextStaDB = ((tWssIfAuthStateDB *) RBTreeGetNext (gWssStaStateDB,
                                                               (tRBElem *)
                                                               pStaDB, NULL));
        }
        pStaDB = pNextStaDB;
    }
    while (pStaDB != NULL);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaUpdateLdDB                                       *
 *                                                                           *
 *  Description     : This function is used to update the station Load DB    *
 *                  :                                                        *
 *  Input(s)        : tWssIfAuthLoadBalanceDB *                              *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT4
WssStaUpdateLdDB (tWssIfAuthLoadBalanceDB * pLoadDB)
{

    tWssIfAuthLoadBalanceDB *pLoadBalanceDB = NULL;
    tWssIfAuthLoadBalanceDB StaLoadBalanceDB;

    MEMSET (&StaLoadBalanceDB, 0, sizeof (tWssIfAuthLoadBalanceDB));

    MEMCPY (StaLoadBalanceDB.stationMacAddress, pLoadDB->stationMacAddress,
            sizeof (tMacAddr));
    StaLoadBalanceDB.u4BssIfIndex = pLoadDB->u4BssIfIndex;

    pLoadBalanceDB = ((tWssIfAuthLoadBalanceDB *) RBTreeGet (gWssStaStationLdDB,
                                                             (tRBElem *) &
                                                             StaLoadBalanceDB));
    if (pLoadBalanceDB == NULL)
    {
        pLoadBalanceDB = (tWssIfAuthLoadBalanceDB *)
            MemAllocMemBlk (WSSIFAUTH_LOADDB_POOLID);
        if (pLoadBalanceDB == NULL)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaUpdateLdDB: "
                        "Memory Allocation Failed For "
                        "Association Load DB \r\n");
        }
        else
        {
            MEMCPY (pLoadBalanceDB->stationMacAddress,
                    pLoadDB->stationMacAddress, sizeof (tMacAddr));
            pLoadBalanceDB->u4BssIfIndex = pLoadDB->u4BssIfIndex;
            if (RBTreeAdd (gWssStaStationLdDB, pLoadBalanceDB) != RB_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WssStaUpdateLdDB: Failed to add entry to "
                            "Station Load DB  RB Tree \r\n");
                return OSIX_FAILURE;
            }

        }
    }
    else
    {
        pLoadDB->u4AssocRejectionCount = pLoadBalanceDB->u4AssocRejectionCount;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaSetLdDB                                          *
 *                                                                           *
 *  Description     : This function is used to set station Load DB           *
 *                  :                                                        *
 *  Input(s)        : tWssIfAuthLoadBalanceDB *                              *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT4
WssStaSetLdDB (tWssIfAuthLoadBalanceDB * pLoadDB)
{

    tWssIfAuthLoadBalanceDB *pLoadBalanceDB = NULL;
    tWssIfAuthLoadBalanceDB StaLoadBalanceDB;

    MEMSET (&StaLoadBalanceDB, 0, sizeof (tWssIfAuthLoadBalanceDB));

    MEMCPY (StaLoadBalanceDB.stationMacAddress, pLoadDB->stationMacAddress,
            sizeof (tMacAddr));
    StaLoadBalanceDB.u4BssIfIndex = pLoadDB->u4BssIfIndex;

    pLoadBalanceDB = ((tWssIfAuthLoadBalanceDB *) RBTreeGet (gWssStaStationLdDB,
                                                             (tRBElem *) &
                                                             StaLoadBalanceDB));
    if (pLoadBalanceDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaSetAssocCntDB: "
                    "Failed to retrieve data from Balance Load  DB \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        pLoadBalanceDB->u4AssocRejectionCount = pLoadDB->u4AssocRejectionCount;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaAssocCntDB                                       *
 *                                                                           *
 *  Description     : This function is used to update Assoc cnt DB           *
 *                  :                                                        *
 *  Input(s)        : tWssIfAuthWtpAssocCountInfo *                          *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT4
WssStaAssocCntDB (tWssIfAuthWtpAssocCountInfo * pWssStaWtpAssocCount)
{

    tWssIfAuthWtpAssocCountInfo assoCntDB;
    tWssIfAuthWtpAssocCountInfo *pAssoCntInfo = NULL;

    MEMSET (&assoCntDB, 0, sizeof (tWssIfAuthWtpAssocCountInfo));
    assoCntDB.u2WtpId = pWssStaWtpAssocCount->u2WtpId;
    pAssoCntInfo = ((tWssIfAuthWtpAssocCountInfo *) RBTreeGet (gWssStaAssoCntDB,
                                                               (tRBElem *) &
                                                               assoCntDB));
    if (pAssoCntInfo == NULL)
    {
        pAssoCntInfo = (tWssIfAuthWtpAssocCountInfo *)
            MemAllocMemBlk (WSSIFAUTH_ASSOCOUNT_POOLID);
        if (pAssoCntInfo == NULL)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaAssocCntDB: "
                        "Memory Allocation Failed For "
                        "Association Count DB \r\n");
            return OSIX_FAILURE;
        }
        else
        {
            pAssoCntInfo->u2WtpId = pWssStaWtpAssocCount->u2WtpId;
            pAssoCntInfo->u4AssoCount = 0;
            pWssStaWtpAssocCount->u4AssoCount = 0;
            if (RBTreeAdd (gWssStaAssoCntDB, pAssoCntInfo) != RB_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "WssStaAssocCntDB: Failed to add entry to "
                            "Assoc cnt RB Tree \r\n");
                return OSIX_FAILURE;
            }
        }
    }
    else
    {
        pWssStaWtpAssocCount->u4AssoCount = pAssoCntInfo->u4AssoCount;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaSetAssocCount                                    *
 *                                                                           *
 *  Description     : This function is used to set Assoc cnt DB              *
 *                  :                                                        *
 *  Input(s)        : tWssIfAuthWtpAssocCountInfo *                          *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT4
WssStaSetAssocCount (tWssIfAuthWtpAssocCountInfo * pWssStaWtpAssocCount)
{

    tWssIfAuthWtpAssocCountInfo *pAssoCntInfo = NULL;
    tWssIfAuthWtpAssocCountInfo assoCntDB;

    MEMSET (&assoCntDB, 0, sizeof (tWssIfAuthWtpAssocCountInfo));
    assoCntDB.u2WtpId = pWssStaWtpAssocCount->u2WtpId;

    pAssoCntInfo = ((tWssIfAuthWtpAssocCountInfo *) RBTreeGet (gWssStaAssoCntDB,
                                                               (tRBElem *) &
                                                               assoCntDB));
    if (pAssoCntInfo == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssStaSetAssocCntDB: "
                    "Failed to retrieve data from Assoc Count DB \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        pAssoCntInfo->u4AssoCount = pWssStaWtpAssocCount->u4AssoCount;
        return OSIX_SUCCESS;
    }
}

/*****************************************************************************
 *  Function Name   : WssStaCompareStaDBRBTree                               *
 *                                                                           *
 *  Description     : This function is used to insert a new entry to RB tree *
 *                  :                                                        *
 *  Input(s)        : tRBElem *                                              *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssStaCompareStaDBRBTree (tRBElem * e1, tRBElem * e2)
{

    tWssIfAuthStateDB  *pNode1 = e1;
    tWssIfAuthStateDB  *pNode2 = e2;
    INT4                i4CmpVal = 0;

    i4CmpVal = MEMCMP (&pNode1->stationMacAddress,
                       &pNode2->stationMacAddress, sizeof (tMacAddr));
    if (i4CmpVal > 0)
    {
        return WSSSTA_RB_GREATER;
    }
    else if (i4CmpVal < 0)
    {
        return WSSSTA_RB_LESS;
    }
    return WSSSTA_RB_EQUAL;
}

/*****************************************************************************
 *  Function Name   : WssStaCompareStaLOADRBTree                             *
 *                                                                           *
 *  Description     : This function is used to insert a new entry to RB tree *
 *                  :                                                        *
 *  Input(s)        : tRBElem *                                              *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssStaCompareStaLOADRBTree (tRBElem * e1, tRBElem * e2)
{

    tWssIfAuthLoadBalanceDB *pNode1 = e1;
    tWssIfAuthLoadBalanceDB *pNode2 = e2;
    INT4                i4CmpVal = 0;

    if (pNode1->u4BssIfIndex > pNode2->u4BssIfIndex)
    {
        return WSSSTA_RB_GREATER;
    }
    else if (pNode1->u4BssIfIndex < pNode2->u4BssIfIndex)
    {
        return WSSSTA_RB_LESS;
    }

    i4CmpVal = MEMCMP (&pNode1->stationMacAddress,
                       &pNode2->stationMacAddress, sizeof (tMacAddr));
    if (i4CmpVal > 0)
    {
        return WSSSTA_RB_GREATER;
    }
    else if (i4CmpVal < 0)
    {
        return WSSSTA_RB_LESS;
    }
    return WSSSTA_RB_EQUAL;
}

/*****************************************************************************
 *  Function Name   : WssStaCompareAssoCntRBTree                             *
 *                                                                           *
 *  Description     : This function is used to insert a new entry to RB tree *
 *                  :                                                        *
 *  Input(s)        : tRBElem *                                              *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssStaCompareAssoCntRBTree (tRBElem * e1, tRBElem * e2)
{

    tWssIfAuthWtpAssocCountInfo *pNode1 = e1;
    tWssIfAuthWtpAssocCountInfo *pNode2 = e2;

    if (pNode1->u2WtpId > pNode2->u2WtpId)
    {
        return WSSSTA_RB_GREATER;
    }
    else if (pNode1->u2WtpId < pNode2->u2WtpId)
    {
        return WSSSTA_RB_LESS;
    }
    return WSSSTA_RB_EQUAL;
}

INT4
ClientDBWalkFn (tRBElem * pRBElem, eRBVisit visit, UINT4 u4Level,
                void *pArg, void *pOut)
{

    tWssIfAuthStateDB  *pStaStateDB;

    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (pOut);
    UNUSED_PARAM (pArg);

    if (visit == postorder || visit == leaf)
    {
        if (pRBElem != NULL)
        {
            pStaStateDB = (tWssIfAuthStateDB *) pRBElem;
            if (gu4ClientWalkIndex < MAX_STA_SUPP_PER_WLC)
            {
                MEMCPY (gaWssClientSummary[gu4ClientWalkIndex].staMacAddr,
                        pStaStateDB->stationMacAddress, sizeof (tMacAddr));
                gaWssClientSummary[gu4ClientWalkIndex].u4BssIfIndex =
                    pStaStateDB->aWssStaBSS[0].u4BssIfIndex;
                gaWssClientSummary[gu4ClientWalkIndex].stationState =
                    pStaStateDB->aWssStaBSS[0].stationState;
                gu4ClientWalkIndex++;

            }
        }
    }
    return RB_WALK_CONT;
}

INT4
WssStaShowClient (VOID)
{
    UINT4               u4BssIfIndex = 0;
    UINT4               u4Index = 0;

    for (u4Index = 0; u4Index < gu4ClientWalkIndex; u4Index++)
    {
        MEMSET (&gaWssClientSummary[u4Index], 0, sizeof (tWssClientSummary));
    }
    gu4ClientWalkIndex = 0;
    RBTreeWalk (gWssStaStateDB, (tRBWalkFn) ClientDBWalkFn, &u4BssIfIndex, 0);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssStaCompareBSSIDRBTree                               *
 *                                                                           *
 *  Description     : This function is used to insert a new entry to RB tree *
 *                  :                                                        *
 *  Input(s)        : tRBElem *                                              *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssStaCompareBSSIDRBTree (tRBElem * e1, tRBElem * e2)
{

    tWssIfBssIdStatsDB *pNode1 = e1;
    tWssIfBssIdStatsDB *pNode2 = e2;

    if (pNode1->u4BssIfIndex > pNode2->u4BssIfIndex)
    {
        return WSSSTA_RB_GREATER;
    }
    else if (pNode1->u4BssIfIndex < pNode2->u4BssIfIndex)
    {
        return WSSSTA_RB_LESS;
    }
    return WSSSTA_RB_EQUAL;
}

/*****************************************************************************
 *  Function Name   : WssStaCompareAuthSessionRBTree                         *
 *                                                                           *
 *  Description     : This function is used to insert a new entry to RB tree *
 *                  :                                                        *
 *  Input(s)        : tRBElem *                                              *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/

INT4
WssStaCompareAuthSessionRBTree (tRBElem * e1, tRBElem * e2)
{
    tWssStaAuthSessionDB *pNode1 = e1;
    tWssStaAuthSessionDB *pNode2 = e2;
    INT4                i4CmpVal = 0;

    i4CmpVal = MEMCMP (&pNode1->stationMacAddress,
                       &pNode2->stationMacAddress, sizeof (tMacAddr));
    if (i4CmpVal > 0)
    {
        return WSSSTA_RB_GREATER;
    }
    else if (i4CmpVal < 0)
    {
        return WSSSTA_RB_LESS;
    }
    /* Check for WLAN ID */
    if (pNode1->u2WlanProfileId > pNode2->u2WlanProfileId)
    {
        return WSSSTA_RB_GREATER;
    }
    else if (pNode1->u2WlanProfileId < pNode2->u2WlanProfileId)
    {
        return WSSSTA_RB_LESS;
    }
    return WSSSTA_RB_EQUAL;
}

/*****************************************************************************
 *  Function Name   : WssIfUpdateBSSIDStnStatsDB                             *
 *                                                                           *
 *  Description     : This function used to add an entry in the BSS ID       *
 *                    Station statistics DB                                  *  
 *  Input(s)        : tWssIfBssIdStatsDB                                     *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/

INT4
WssIfUpdateBSSIDStnStatsDB (UINT4 u4BssIfIndex)
{
    tWssIfBssIdStatsDB *pBssIdStnStatsEntry = NULL;
    tWssIfBssIdStatsDB  tempBssIdStnStatsEntry;

    MEMSET (&tempBssIdStnStatsEntry, 0, sizeof (tempBssIdStnStatsEntry));

    tempBssIdStnStatsEntry.u4BssIfIndex = u4BssIfIndex;

    pBssIdStnStatsEntry = ((tWssIfBssIdStatsDB *) RBTreeGet (gWssBssIdStatsDB,
                                                             (tRBElem *) &
                                                             tempBssIdStnStatsEntry));

    if (pBssIdStnStatsEntry == NULL)
    {
        pBssIdStnStatsEntry =
            (tWssIfBssIdStatsDB *)
            MemAllocMemBlk (WSSIFAUTH_BSSID_STATS_POOLID);

        if (pBssIdStnStatsEntry == NULL)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                        "Memory allocation failed for pBssIdStnStatsEntry \r\n");
            return OSIX_FAILURE;
        }

        MEMSET (&pBssIdStnStatsEntry->bssIdStnStats, 0, sizeof (tBSSIDStnStat));
        pBssIdStnStatsEntry->u4BssIfIndex = u4BssIfIndex;

        if (RBTreeAdd (gWssBssIdStatsDB, pBssIdStnStatsEntry) != RB_SUCCESS)
        {
            WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                        "Failed to add entry to the RB Tree gWssBssIdStatsDB \r\n");
            MemReleaseMemBlock (WSSIFAUTH_BSSID_STATS_POOLID,
                                (UINT1 *) pBssIdStnStatsEntry);
            return OSIX_FAILURE;
        }
    }
    else
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    " Entry already present in the Station Auth Stats DB \r\n");
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfGetBssIdStnStatsEntry                             *
 *                                                                           *
 *  Description     : This function used to update all the stations stats    *
 *                    in BSS ID Station statistics DB                        *
 *                                                                           *
 *  Input(s)        : tWssIfBssIdStatsDB                                     *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/

tWssIfBssIdStatsDB *
WssIfGetBssIdStnStatsEntry (UINT4 u4BssIfIndex)
{
    tWssIfBssIdStatsDB *pBssIdStnStatsEntry = NULL;
    tWssIfBssIdStatsDB  tempBssIdStnStatsEntry;

    WSSSTA_FN_ENTRY ();

    MEMSET (&tempBssIdStnStatsEntry, 0, sizeof (tempBssIdStnStatsEntry));

    tempBssIdStnStatsEntry.u4BssIfIndex = u4BssIfIndex;

    pBssIdStnStatsEntry = ((tWssIfBssIdStatsDB *) RBTreeGet (gWssBssIdStatsDB,
                                                             (tRBElem *) &
                                                             tempBssIdStnStatsEntry));

    if (pBssIdStnStatsEntry == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "Entry not found in DB \r\n");
        return NULL;
    }

    WSSSTA_FN_EXIT ();
    return pBssIdStnStatsEntry;
}

/*****************************************************************************
 *  Function Name   : WssIfDelBssIdStnStatsEntry                             *
 *                                                                           *
 *  Description     : This function used to delete entry from BSS ID         *
 *                    Station statistics DB                                  *
 *  Input(s)        : tWssIfBssIdStatsDB                                     *
 *                                                                           *
 *  Output(s)       : None                                                   *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
INT4
WssIfDelBssIdStnStatsEntry (UINT4 u4BssIfIndex)
{
    tWssIfBssIdStatsDB *pBssIdStnStatsEntry = NULL;
    tWssIfBssIdStatsDB  tempBssIdStnStatsEntry;

    MEMSET (&tempBssIdStnStatsEntry, 0, sizeof (tempBssIdStnStatsEntry));

    tempBssIdStnStatsEntry.u4BssIfIndex = u4BssIfIndex;

    pBssIdStnStatsEntry = ((tWssIfBssIdStatsDB *) RBTreeGet (gWssBssIdStatsDB,
                                                             (tRBElem *) &
                                                             tempBssIdStnStatsEntry));
    if (pBssIdStnStatsEntry == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "Entry not found in DB \r\n");
        return OSIX_FAILURE;
    }
    else
    {
        RBTreeRem (gWssBssIdStatsDB, (tRBElem *) pBssIdStnStatsEntry);
        MemReleaseMemBlock (WSSIFAUTH_BSSID_STATS_POOLID,
                            (UINT1 *) pBssIdStnStatsEntry);
    }
    return OSIX_FAILURE;
}

/*****************************************************************************
 *  Function Name   : WssIfGetProfileIfIndex                                 *
 *                                                                           *
 *  Description     : This function used to get the profile interface index  *
 *                    from BSSIfIndex.                                       *
 *                                                                           *
 *  Input(s)        : u4BssIfIndex    - Binding If index                     *
 *                                                                           *
 *  Output(s)       : pu2WlanProfileId                                       *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT1
WssIfGetProfileIfIndex (UINT4 u4BssIfIndex, UINT4 *pu4WlanProfileIndex)
{
    tWssWlanDB          wssWlanDB;

    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));

    wssWlanDB.WssWlanAttributeDB.u4BssIfIndex = u4BssIfIndex;
    wssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, &wssWlanDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssIfGetProfileIfIndex:: "
                    "Failed to retrieve data from WssWlanDB\r\n");
        return OSIX_FAILURE;
    }
    *pu4WlanProfileIndex = wssWlanDB.WssWlanAttributeDB.u4WlanIfIndex;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfGetProfileIfIndex                                 *
 *                                                                           *
 *  Description     : This function used to get the profile interface index  *
 *                    from BSSIfIndex.                                       *
 *                                                                           *
 *  Input(s)        : u4BssIfIndex    - Binding If index                     *
 *                                                                           *
 *  Output(s)       : pu2WlanProfileId                                       *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT1
WssIfGetCapwapDB (tMacAddr StaMacAddr, tWssIfCapDB * pWssIfCapwapDB)
{
    tWssWlanDB          wssWlanDB;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;

    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));

    pWssStaWepProcessDB = WssStaProcessEntryGet (StaMacAddr);

    if (pWssStaWepProcessDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssIfGetApMacAddr:: "
                    "Entry  is not present in Process DB \r\n");
        return OSIX_FAILURE;

    }

    wssWlanDB.WssWlanAttributeDB.u4BssIfIndex =
        pWssStaWepProcessDB->u4BssIfIndex;
    wssWlanDB.WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bWtpInternalId = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, &wssWlanDB) !=
        OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssIfGetApMacAddr:: "
                    "Failed to retrieve data from WssWlanDB\r\n");
        return OSIX_FAILURE;
    }

    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
        wssWlanDB.WssWlanAttributeDB.u2WtpInternalId;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) !=
        OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssIfGetApMacAddr:: "
                    "Failed to retrieve data from WssIfCapwapDB\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssIfGetAuthenticationAlgoDetails                      *
 *                                                                           *
 *  Description     : This function used to get the authentication algorithm *
 *                    used                                                   *
 *                                                                           *
 *  Input(s)        : i4IfIndex - Profile If index                           *
 *                                                                           *
 *  Output(s)       : pu1Dot11AuthAlgo                                       *
 *                  :                                                        *
 *  Returns         : OSIX_FAILURE on failure                                *
 *                    OSIX_SUCCESS on success                                *
 *****************************************************************************/
UINT1
WssIfGetAuthenticationAlgoDetails (INT4 i4ProfileIndex, UINT1 *pu1Dot11AuthAlgo,
                                   UINT1 *pu1WebAuthStatus)
{
    tWssWlanDB          wssWlanDB;
    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));

    /* Fetch existing Auth Algorithm from DB using the WlanProfileId */
    wssWlanDB.WssWlanAttributeDB.u4WlanIfIndex = (UINT4) i4ProfileIndex;
    wssWlanDB.WssWlanIsPresentDB.bAuthMethod = OSIX_TRUE;
    wssWlanDB.WssWlanIsPresentDB.bWebAuthStatus = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &wssWlanDB))
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssIfGetAuthenticationAlgoDetails:: "
                    "Failed to retrieve data from WssWlanDB\r\n");
        return OSIX_FAILURE;
    }

    *pu1Dot11AuthAlgo = wssWlanDB.WssWlanAttributeDB.u1AuthMethod;
    *pu1WebAuthStatus = wssWlanDB.WssWlanAttributeDB.u1WebAuthStatus;
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WssGetIsRsnaAuthentication                             *
 *                                                                           *
 *  Description     : This function used to get the authentication type      *
 *                    from BSSIfIndex.                                       *
 *                                                                           *
 *  Input(s)        : u4BssIfIndex    - Binding If index                     *
 *                                                                           *
 *  Returns         : OSIX_FAILURE if RSNA is disabled                       *
 *                    OSIX_SUCCESS if RSNA is enabled                        *
 *****************************************************************************/
UINT1
WssGetIsRsnaAuthentication (UINT2 u2BssIfIndex)
{
#ifdef  RSNA_WANTED
    UINT4               wlanProfileIndex = 0;
    INT4                i4Dot11RSNAEnabled = RSNA_DISABLED;
    INT4                i4FsRSNAEnabled = WPA_DISABLED;

    if (WssIfGetProfileIfIndex ((UINT4) u2BssIfIndex, &wlanProfileIndex) !=
        OSIX_SUCCESS)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WssGetIsRsnaAuthentication::: "
                    " Failed to retrieve the WLAN profile index from WssWlanDB\r\n");
        return OSIX_FAILURE;
    }

    RsnaGetDot11RSNAEnabled ((INT4) wlanProfileIndex, &i4Dot11RSNAEnabled);
#ifdef WPA_WANTED
    WpaGetFsRSNAEnabled ((INT4) wlanProfileIndex, &i4FsRSNAEnabled);
#endif
    if (i4Dot11RSNAEnabled == RSNA_ENABLED || i4FsRSNAEnabled == WPA_ENABLED)
    {
        return OSIX_SUCCESS;

    }
#endif
    UNUSED_PARAM (u2BssIfIndex);
    return OSIX_FAILURE;
}

/*****************************************************************************
 *  Function Name   : WssIfDeauthenticationStations                          *
 *                                                                           *
 *  Description     : This function is used to deauthenticate all the        *
 *                    Stations in station  DB                                *
 *                                                                           *
 *  Input(s)        : VOID                                                   *
 *                                                                           *
 *  Returns         : OSIX_SUCCESS or OSIX_FAILURE                                                       *
 *                                                                           *
 *****************************************************************************/
UINT1
WssIfDeauthenticationStations (UINT4 u4BssIfIndex, UINT1 *pu1Bssid)
{
    tWssIfAuthStateDB  *pStaDB = NULL;
    tWssIfAuthStateDB  *pNextStaDB = NULL;

    pStaDB = ((tWssIfAuthStateDB *) RBTreeGetFirst (gWssStaStateDB));
    if (pStaDB == NULL)
    {
        return OSIX_FAILURE;
    }
    do
    {
        if (u4BssIfIndex == pStaDB->aWssStaBSS[0].u4BssIfIndex)
        {
            WssRsnaSendDeAuthMsg (pu1Bssid, pStaDB->stationMacAddress);
            WssStaDeleteRsnaSessionKey (pStaDB->stationMacAddress,
                                        pStaDB->aWssStaBSS[0].u4BssIfIndex);
        }

        pNextStaDB = ((tWssIfAuthStateDB *) RBTreeGetNext (gWssStaStateDB,
                                                           (tRBElem *) pStaDB,
                                                           NULL));
        pStaDB = pNextStaDB;
    }
    while (pStaDB != NULL);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : WSSRadiusAuthentication                                *
 *                                                                           *
 *  Description     : This function is used to transmit the radius packets   *
 *                    It acts as an interface between radius and PNAC        *
 *                                                                           *
 *  Input(s)        : p_RadiusInputAuth, p_RadiusServerAuth                  *
 *                                                                           *
 *  Returns         : The Identifier of the packet if packet is transmitted  *
 *                    successfully                                           *
 *                                                                           *
 *****************************************************************************/

INT4
WSSRadiusAuthentication (tRADIUS_INPUT_AUTH * p_RadiusInputAuth,
                         tRADIUS_SERVER * p_RadiusServerAuth,
                         VOID (*CallBackfPtr) (VOID *),
                         UINT4 ifIndex, UINT1 u1ReqId)
{
    tWssWlanDB         *pwssWlanDB = NULL;
    UINT1               au1CallingStationId[WSSWLAN_STATION_ID_LEN];
    UINT1               au1CalledStationId[WSSWLAN_STATION_ID_LEN];
    INT4                i4CallingStationIdLen = 0;
    INT4                i4CalledStationIdLen = 0;
    UINT1               au1Dot11DesiredSSID[WSSWLAN_SSID_NAME_LEN + 1];
    UINT1               au1UserName[LEN_USER_NAME];
#ifdef WSSUSER_WANTED
    tRadInterface      *pInterface = NULL;
    BOOL1               bRestrictedUser = OSIX_FALSE;
#endif

    MEMSET (au1Dot11DesiredSSID, 0, WSSWLAN_SSID_NAME_LEN + 1);
    MEMSET (au1UserName, 0, LEN_USER_NAME);

    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanDB == NULL)
    {
        return NOT_OK;
    }
    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (au1CallingStationId, 0, WSSWLAN_STATION_ID_LEN);
    MEMSET (au1CalledStationId, 0, WSSWLAN_STATION_ID_LEN);

    pWssStaWepProcessDB = WssStaProcessEntryGet (p_RadiusInputAuth->au1StaMac);

    if (pWssStaWepProcessDB == NULL)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return NOT_OK;
    }

    if (p_RadiusInputAuth->u1_ProtocolType == PRO_EAP)
    {
        MEMCPY (au1UserName, p_RadiusInputAuth->p_UserInfoEAP->a_u1UserName,
                STRLEN (p_RadiusInputAuth->p_UserInfoEAP->a_u1UserName));
    }
    else if (p_RadiusInputAuth->u1_ProtocolType == PRO_PAP)
    {
        MEMCPY (au1UserName, p_RadiusInputAuth->p_UserInfoPAP->a_u1UserName,
                STRLEN (p_RadiusInputAuth->p_UserInfoPAP->a_u1UserName));
    }

#ifdef WSSUSER_WANTED

    /* Check whether we can allow the user */
    if ((p_RadiusInputAuth->u1_ProtocolType == PRO_PAP) ||
        (p_RadiusInputAuth->p_UserInfoEAP->
         a_u1Response[PNAC_EAP_RESPONSE_OFFSET] == PNAC_EAP_RESPONSE))
    {
        WssUserProcessRestrictedUser (p_RadiusInputAuth->au1StaMac, au1UserName,
                                      &bRestrictedUser);
        if (bRestrictedUser == OSIX_TRUE)
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);

            if (p_RadiusInputAuth->u1_ProtocolType == PRO_PAP)
            {
                pInterface =
                    (tRadInterface *) (VOID *) UtlShMemAllocRadInterface ();
                if (pInterface == NULL)
                {
                    return NOT_OK;
                }
                MEMSET (pInterface, 0, sizeof (tRadInterface));

                pInterface->TaskId = p_RadiusInputAuth->TaskId;
                pInterface->Access = 2;
                WebnmHandleRadiusResponse (pInterface);
            }
            return OK;
        }
    }
#endif

    MEMCPY (pwssWlanDB->WssWlanAttributeDB.BssId,
            pWssStaWepProcessDB->BssIdMacAddr, sizeof (tMacAddr));
    pwssWlanDB->WssWlanIsPresentDB.bBssId = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    pwssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSSID_MAPPING_ENTRY, pwssWlanDB)
        == OSIX_FAILURE)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return SNMP_FAILURE;
    }

    pwssWlanDB->WssWlanIsPresentDB.bDesiredSsid = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, pwssWlanDB)
        == OSIX_FAILURE)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return SNMP_FAILURE;
    }
    MEMCPY (au1Dot11DesiredSSID, pwssWlanDB->WssWlanAttributeDB.au1DesiredSsid,
            WSSWLAN_SSID_NAME_LEN);

    SPRINTF ((CHR1 *) au1CalledStationId, "%02x-%02x-%02x-%02x-%02x-%02x:",
             pWssStaWepProcessDB->BssIdMacAddr[0],
             pWssStaWepProcessDB->BssIdMacAddr[1],
             pWssStaWepProcessDB->BssIdMacAddr[2],
             pWssStaWepProcessDB->BssIdMacAddr[3],
             pWssStaWepProcessDB->BssIdMacAddr[4],
             pWssStaWepProcessDB->BssIdMacAddr[5]);

    MEMCPY (&au1CalledStationId[18], au1Dot11DesiredSSID,
            WSSWLAN_SSID_NAME_LEN);

    i4CalledStationIdLen = (INT4) STRLEN (au1CalledStationId);
    au1CalledStationId[i4CalledStationIdLen] = '\0';
    SPRINTF ((CHR1 *) au1CallingStationId, "%02x-%02x-%02x-%02x-%02x-%02x",
             p_RadiusInputAuth->au1StaMac[0],
             p_RadiusInputAuth->au1StaMac[1],
             p_RadiusInputAuth->au1StaMac[2],
             p_RadiusInputAuth->au1StaMac[3],
             p_RadiusInputAuth->au1StaMac[4], p_RadiusInputAuth->au1StaMac[5]);
    i4CallingStationIdLen = (INT4) STRLEN (au1CallingStationId);
    au1CallingStationId[i4CallingStationIdLen] = '\0';

    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    MEMCPY (p_RadiusInputAuth->au1CalledStationId, au1CalledStationId,
            i4CalledStationIdLen);
    MEMCPY (p_RadiusInputAuth->au1CallingStationId, au1CallingStationId,
            i4CallingStationIdLen);
    p_RadiusInputAuth->bCalledStationFlag = OSIX_TRUE;
    p_RadiusInputAuth->bCallingStationFlag = OSIX_TRUE;

    /* Fill the parameters */
    return (radiusAuthentication (p_RadiusInputAuth, p_RadiusServerAuth,
                                  CallBackfPtr, ifIndex, u1ReqId));
}

/*****************************************************************************
 *  Function Name   : WSSHandleRadResponse                                   *
 *                                                                           *
 *  Description     : This function is used to handle the radius response    *
 *                                                                           *
 *                                                                           *
 *  Input(s)        : VOID                                                   *
 *                                                                           *
 *  Returns         : OSIX_SUCCESS or OSIX_FAILURE                           *
 *                                                                           *
 *****************************************************************************/

VOID
WSSHandleRadResponse (VOID *pRadRespRcvd)
{
    tRadInterface      *pRadResp = NULL;
    tWlanSTATrapInfo    WTPTrapInfo;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    UINT4               u4ProfileIndex = 0;
#ifdef WSSUSER_WANTED
    tWssUserNotifyParams WssUserNotifyParams;
    tWssPMInfo          WssPmInfo;
    tUserRoleEntry      UserRoleEntry;
    tUserGroupEntry    *pUserGroupEntry = NULL;
    tUserRoleEntry     *pUserRoleEntry = NULL;
    tUserGroupEntry     UserGroupEntry;
    UINT4               u4WlanId = 0;

    MEMSET (&UserGroupEntry, 0, sizeof (tUserGroupEntry));
    MEMSET (&UserRoleEntry, 0, sizeof (tUserRoleEntry));
    MEMSET (&WssUserNotifyParams, 0, sizeof (tWssUserNotifyParams));
    MEMSET (&WssPmInfo, 0, sizeof (tWssPMInfo));
#endif
    MEMSET (&WTPTrapInfo, 0, sizeof (tWlanSTATrapInfo));
    pRadResp = (tRadInterface *) pRadRespRcvd;
    pWssStaWepProcessDB = WssStaProcessEntryGet (pRadResp->au1StaMac);

    if (pWssStaWepProcessDB == NULL)
    {
        PnacFreeMemForInterface ((tRadInterface *) pRadRespRcvd);
        return;
    }

    /* Retrivie the profile Index from the local database */
    if (WssIfGetProfileIfIndex (pWssStaWepProcessDB->u4BssIfIndex,
                                &u4ProfileIndex) == OSIX_FAILURE)
    {
        PnacFreeMemForInterface ((tRadInterface *) pRadRespRcvd);
        return;
    }

#ifdef WSSUSER_WANTED
    if ((WssUserRoleModuleStatus () == OSIX_SUCCESS)
        && (pRadResp->bAccessAcceptRcvd == OSIX_TRUE))
    {
        WssUserNotifyParams.u4Bandwidth = pRadResp->u4Bandwidth;
        WssUserNotifyParams.u4DLBandwidth = pRadResp->u4DLBandwidth;
        WssUserNotifyParams.u4ULBandwidth = pRadResp->u4ULBandwidth;
        WssUserNotifyParams.u4Volume = pRadResp->u4Volume;
        WssUserNotifyParams.u4Time = pRadResp->u4Timeout;

        WssUserNotifyParams.u4UserNameLen = STRLEN (pRadResp->au1Username);
        MEMCPY (WssUserNotifyParams.staMac, pRadResp->au1StaMac, MAC_ADDR_LEN);
        MEMCPY (WssUserNotifyParams.au1UserName, pRadResp->au1Username,
                WssUserNotifyParams.u4UserNameLen);
        WssUserNotifyParams.i4NasPort = pRadResp->i4NasPort;
        WssUserNotifyParams.u1EAPType = pRadResp->u1EAPType;
        WssUserNotifyParams.eWssUserNotifyType = WSSUSER_SESSION_ADD;

        /* Check if the received attributes are empty */
        if ((WssUserNotifyParams.u4Bandwidth == 0) &&
            (WssUserNotifyParams.u4DLBandwidth == 0) &&
            (WssUserNotifyParams.u4ULBandwidth == 0) &&
            (WssUserNotifyParams.u4Volume == 0) &&
            (WssUserNotifyParams.u4Time == 0))
        {
            /* Received attributes are zero. Check if the static configuration been available */
            /* Retrive the Station Mac from the local database */

            /* Retrivie the Wlan Id from the local database */
            if (WssGetCapwapWlanId (u4ProfileIndex, &u4WlanId) == OSIX_FAILURE)
            {
                PnacFreeMemForInterface ((tRadInterface *) pRadRespRcvd);
                return;
            }

            UserRoleEntry.u4WlanIndex = u4ProfileIndex;

            MEMCPY (UserRoleEntry.au1UserName, WssUserNotifyParams.au1UserName,
                    WssUserNotifyParams.u4UserNameLen);

            UserRoleEntry.u4UserNameLen = WssUserNotifyParams.u4UserNameLen;

            pUserRoleEntry = (tUserRoleEntry *) RBTreeGet (gWssUserGlobals.
                                                           WssUserRoleEntryTable,
                                                           (tRBElem *) &
                                                           UserRoleEntry);

            if (pUserRoleEntry == NULL)
            {
                WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC, "WssUserAddSessionEntry:\
            User Role entry is NULL\n");

        /******************
         * This is the sceanario where, User Role Module is enabled but
         * no User Group information present both in Radius Server and
         * in Static Configuration.  In this case, we have to
         * restrict this User
         ***************/

                pRadResp->Access = ACCESS_REJECT;
                gWssUserGlobals.u4BlockedCount++;
            }
            else
            {
                UserGroupEntry.u4GroupId = pUserRoleEntry->u4GroupId;

                /* Retrive the UserRole entry from the local database */
                pUserGroupEntry =
                    (tUserGroupEntry *) RBTreeGet (gWssUserGlobals.
                                                   WssUserGroupEntryTable,
                                                   (tRBElem *) &
                                                   UserGroupEntry);

                if (pUserGroupEntry == NULL)
                {
                    WSSUSER_TRC (WSSUSER_ALL_FAILURE_TRC,
                                 "WssUserAddSessionEntry:\
                User Group entry is NULL\n");

                    /* This is a negative scenario */
                    pRadResp->Access = ACCESS_REJECT;
                    gWssUserGlobals.u4BlockedCount++;
                }
                else
                {
                    WssUserNotifyParams.u4Bandwidth =
                        pUserGroupEntry->u4Bandwidth;
                    WssUserNotifyParams.u4DLBandwidth =
                        pUserGroupEntry->u4DLBandwidth;
                    WssUserNotifyParams.u4ULBandwidth =
                        pUserGroupEntry->u4ULBandwidth;
                    WssUserNotifyParams.u4Volume = pUserGroupEntry->u4Volume;
                    WssUserNotifyParams.u4Time = pUserGroupEntry->u4Time;

                    if (WssUserProcessNotification (&WssUserNotifyParams)
                        == OSIX_FAILURE)
                    {
                        WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WSSHandleRadResponse: "
                                    "WssUserProcessNotification returned failure\r\n");
                        PnacFreeMemForInterface ((tRadInterface *)
                                                 pRadRespRcvd);
                        return;
                    }
                }
            }
        }
        else
        {
            if (WssUserProcessNotification (&WssUserNotifyParams)
                == OSIX_FAILURE)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC, "WSSHandleRadResponse: "
                            "WssUserProcessNotification returned failure\r\n");
                PnacFreeMemForInterface ((tRadInterface *) pRadRespRcvd);
                return;
            }
        }
        if (pRadResp->Access == ACCESS_ACCEPT)
        {
            MEMCPY (&WssPmInfo.FsWlanClientStatsMACAddress,
                    pRadResp->au1StaMac, MAC_ADDR_LEN);

            if (pmWTPInfoNotify (CLIENT_DEL, &WssPmInfo) != OSIX_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "Delete Client entry : Update PM Stats DB Failed \r\n");
            }

            if (pmWTPInfoNotify (CLIENT_ADD, &WssPmInfo) != OSIX_SUCCESS)
            {
                WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                            "Create Client entry : Update PM Stats DB Failed \r\n");
            }
        }
    }
#endif
    /*DOT1X and WEB AUTH RADIUS TRAPS */
    if (pRadResp->Access == ACCESS_REJECT)
    {
        WTPTrapInfo.u4ifIndex = u4ProfileIndex;

        MEMCPY (&WTPTrapInfo.au1CliMac, pRadResp->au1StaMac, MAC_ADDR_LEN);

        if (pRadResp->u1EAPType == PRO_EAP)
        {
            WTPTrapInfo.u4AuthFailure = WSS_WLAN_DOT1X_AUTH_FAIL_TRAP;
        }
        else if (pRadResp->u1EAPType == PRO_PAP)
        {
            WTPTrapInfo.u4AuthFailure = WSS_WLAN_WEB_AUTH_FAIL_TRAP;
        }

        WlanSnmpifSendTrap (WSS_WLAN_AUTH_FAILURE, &WTPTrapInfo);
    }
    if (pRadResp->u1EAPType == PRO_EAP)
    {
        PnacHandleRadResponse (pRadRespRcvd);
    }
#ifdef RADIUS_WANTED
    else if (pRadResp->u1EAPType == PRO_PAP)
    {
        WebnmHandleRadiusResponse (pRadRespRcvd);
    }
#endif
}

#ifdef WSSUSER_WANTED
#if defined (WLC_WANTED) && defined (LNXIP4_WANTED)
/*****************************************************************************
 *  Function Name   : WssifAddRemoveTcFilter                                 *
 *                                                                           *
 *  Description     : This function is used to handle the TC Filter Addition *
 *                    and deletion                                           *
 *                                                                           *
 *                                                                           *
 *  Input(s)        : u4CapwapBaseWtpProfileId - WTP Profile Id              *
 *                    u1DeleteFlag    - Flag to Delete the TC Filter         *
 *                                                                           * 
 *  Returns         : OSIX_SUCCESS or OSIX_FAILURE                           *
 *                                                                           *
 *****************************************************************************/
UINT1
WssifAddRemoveTcFilter (UINT4 u4CapwapBaseWtpProfileId, UINT1 u1DeleteFlag)
{
    tWssWlanDB         *pWssWlanDB = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2WtpInternalId = 0;
    UINT4               u4Index = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE);
    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4CapwapBaseWtpProfileId;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID, pWssIfCapwapDB) ==
        OSIX_SUCCESS)
    {
        u2WtpInternalId = pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;

        pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
        if (pWssWlanDB == NULL)
        {
            WSSIF_TRC (WSSIF_FAILURE_TRC,
                       "CapwapShowApTraffic:- "
                       "UtlShMemAllocWlanDbBuf returned failure\n");
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        WssStaShowClient ();
        for (u4Index = 0; u4Index < gu4ClientWalkIndex; u4Index++)
        {
            MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
            pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex =
                gaWssClientSummary[u4Index].u4BssIfIndex;
            pWssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                          pWssWlanDB) == OSIX_SUCCESS)
            {
                MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
                RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                    pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
                RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
                if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                              &RadioIfGetDB) == OSIX_SUCCESS)
                {
                    if (u2WtpInternalId ==
                        RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId)
                    {
                        pWssStaWepProcessDB =
                            WssStaProcessEntryGet (gaWssClientSummary[u4Index].
                                                   staMacAddr);

                        if (pWssStaWepProcessDB != NULL)
                        {
                            WssUserSetBandWidth (gaWssClientSummary[u4Index].
                                                 staMacAddr,
                                                 pWssStaWepProcessDB->
                                                 u4BandWidth,
                                                 pWssStaWepProcessDB->
                                                 u4DLBandWidth,
                                                 pWssStaWepProcessDB->
                                                 u4ULBandWidth, u1DeleteFlag);
                        }
                    }
                }
            }

        }
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return SNMP_SUCCESS;
}
#endif
#endif

/*****************************************************************************
 *  Function Name   : WssIfGetStationIpAddr                                  *
 *                                                                           *
 *  Description     : This function is used to get the station ip address    *
 *                                                                           *
 *  Input(s)        : u4BssIfIndex - Binding Interface index                 *
 *                    u4IpAddr - Input Station Ip Address                    *
 *                                                                           * 
 *  Returns         : OSIX_SUCCESS or OSIX_FAILURE                           *
 *                                                                           *
 *****************************************************************************/
UINT1
WssIfGetStationIpAddr (UINT4 *pu4BssIfIndex, UINT4 u4IpAddr)
{
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    UINT4               u4Index = 0;

    WssStaShowClient ();
    for (u4Index = 0; u4Index < gu4ClientWalkIndex; u4Index++)
    {
        pWssStaWepProcessDB =
            WssStaProcessEntryGet (gaWssClientSummary[u4Index].staMacAddr);

        if (pWssStaWepProcessDB != NULL)
        {
            if (pWssStaWepProcessDB->u4StationIpAddress == u4IpAddr)
            {
                *pu4BssIfIndex = pWssStaWepProcessDB->u4BssIfIndex;
                return OSIX_SUCCESS;
            }
        }
    }
    return OSIX_FAILURE;
}

/*****************************************************************************
 *  Function Name   : WssIfGetIsolatedStationIpAddr                          *
 *                                                                           *
 *  Description     : This function is used to get the station is isolated   *
 *                                                     station ip address    *
 *                                                                           *
 *  Input(s)        : u4BssIfIndex - Binding Interface index                 *
 *                                                                           * 
 *  Returns         : OSIX_SUCCESS or OSIX_FAILURE                           *
 *                                                                           *
 *****************************************************************************/
UINT1
WssIfGetIsolatedStationIpAddr (UINT4 *pu4BssIfIndex)
{

    tWssWlanDB         *pWssWlanDB = NULL;

    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "CapwapShowApTraffic:- "
                   "UtlShMemAllocWlanDbBuf returned failure\n");
        return OSIX_FAILURE;
    }

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
    pWssWlanDB->WssWlanAttributeDB.u4BssIfIndex = *pu4BssIfIndex;
    pWssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;

    pWssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                                  pWssWlanDB) != OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }

    pWssWlanDB->WssWlanIsPresentDB.bSsidIsolation = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                  pWssWlanDB) == OSIX_SUCCESS)
    {
        if (pWssWlanDB->WssWlanAttributeDB.u1SsidIsolation)
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
            return OSIX_SUCCESS;
        }
    }
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    return OSIX_FAILURE;
}

/*****************************************************************************
 *  Function Name   : WssIfGetBSSIDofConnectedStation                         *
 *                                                                           *
 *  Description     : This function is used to get BSSID of the station      *
 *                                                     connected             *
 *                                                                           *
 *  Input(s)        : Station Mac Address                                    *
 *                                                                           *
 *  Returns         : OSIX_SUCCESS or OSIX_FAILURE                           *
 *                                                                           *
 *****************************************************************************/
UINT1
WssIfGetBSSIDofConnectedStation (tMacAddr StaMac, UINT1 *pu1BssId)
{

    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;

    pWssStaWepProcessDB = WssStaProcessEntryGet (StaMac);
    if (pWssStaWepProcessDB == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMCPY (pu1BssId, pWssStaWepProcessDB->BssIdMacAddr, sizeof (tMacAddr));
    return OSIX_SUCCESS;
}

#endif
