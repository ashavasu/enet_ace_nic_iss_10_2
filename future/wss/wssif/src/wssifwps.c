/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wssifwps.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains the WPS module APIs 
 *                            
 ********************************************************************/
#ifdef WPS_WANTED
#ifndef _WSSIF_WPS_C_
#define _WSSIF_WPS_C_

#include "wssifinc.h"
#include "wps.h"

/****************************************************************************
 * *                                                                        *
 * * Function     : WssIfProcessWpsMsg                                   *
 * *                                                                        *
 * * Description  : Calls WPS module API to process fn calls from other *
 * *                WSS modules.                                            *
 * *                                                                        *
 * * Input        : eMsgType - Msg opcode                                   *
 * *                pWpsMsgStruct - Pointer to WPS msg struct        * 
 * *                                                                        *
 * * Output       : None                                                    *
 * *                                                                        *
 * * Returns      : WPS_SUCCESS on success                               *
 * *                WPS_FAILURE otherwise                                *
 * *                                                                        *
 * **************************************************************************/

UINT1
WssIfProcessWpsMsg(UINT4 eMsgType, tWssWpsNotifyParams *WssWpsNotifyParams)
{
    UNUSED_PARAM(eMsgType);
    if (WpsProcessWssNotification(WssWpsNotifyParams) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;          
}

#endif
#endif
