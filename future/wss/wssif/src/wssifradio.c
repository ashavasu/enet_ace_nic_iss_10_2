/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: wssifradio.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This files contains the interface functions for Radio module
 ***********************************************************************/
#ifndef _WSSIF_RADIO_C
#define _WSSIF_RADIO_C

#include "wssifinc.h"
#include "radioifproto.h"

/*****************************************************************************
 * Function Name      : WssIfProcessRadioIfMsg                               *
 *                                                                           *
 * Description        : Wrapper Func through which other modules calls       *
 *                      Radio module                                         *
 *                                                                           *
 * Input(s)           : eMsgType - Op Code                                   *
 *                      pCfaMsgStruct - pointer to input structure           *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS, if initialization succeeds             *
 *                      OSIX_FAILURE, otherwise                              *
 *                                                                           *
 *****************************************************************************/
UINT1
WssIfProcessRadioIfMsg(UINT4 eMsgType, tRadioIfMsgStruct *pRadioIfMsgStruct)
{
    if (RadioIfProcessWssIfMsg((UINT1)eMsgType, pRadioIfMsgStruct) != OSIX_SUCCESS)
    {
        WSSIF_TRC(WSSIF_FAILURE_TRC, 
                "WssIfProcessRadioIfMsg: RadioIfProcessWssIfMsg returns "
                "failure\r\n");
        return OSIX_FAILURE;
    }
   
    return OSIX_SUCCESS;
}

#endif

