/* $Id: wssifmain.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ */

#ifndef _WSSIF_MAIN_C
#define _WSSIF_MAIN_C

#include "wssifinc.h"

/****************************************************************************
 * *                                                                           *
 * * Function     : WssIfInit              
 * *                                                                           *
 * * Description  : This function initiliazes the WSSIF module and creates a
		            semaphore for WSSIF DB
 * *                                                                           *
 * * Input        : None
 * *                                                                           *
 * * Output       : None
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                 OSIX_FAILURE, otherwise
 * *                
 * *                                                                           *
 * *****************************************************************************/

UINT1 WssIfInit (VOID)
{
    MEMSET (&gWssIfGlobals, 0, sizeof (gWssIfGlobals));
    MEMSET (&gTCFilterHandleList, 0, sizeof (gTCFilterHandleList));
    MEMCPY (gWssIfGlobals.au1TaskSemName, WSSIF_LOCK_SEM_NAME, OSIX_NAME_LEN);

#ifdef WLC_WANTED
    /* Register Handles for Authentication & Response Callback function,
     * which are used to add attributes to Radius Request packet before invoking 
     * radiusAuthentication() and decode attribytes from Radius Accept, 
     * PNAC Rad Response is invoked.*/

    RadApiRegisterHandleRadAuthResCB (WSSRadiusAuthentication, WSSHandleRadResponse);
#endif

    if (OsixCreateSem (WSSIF_LOCK_SEM_NAME, WSSIF_SEM_CREATE_INIT_CNT, 0,
                       &gWssIfGlobals.wssifTaskSemId) == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfInit: Semaphore Creation failure\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}


/****************************************************************************
 * *                                                                           *
 * * Function     : WssIfDeInit              
 * *                                                                           *
 * * Description  : This function will delete the resource allocated       
 * *                                                                           *
 * * Input        : None
 * *                                                                           *
 * * Output       : None
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                 OSIX_FAILURE, otherwise
 * *                
 * *                                                                           *
 * *****************************************************************************/

UINT1 WssIfDeInit (VOID)
{
    if (gWssIfGlobals.wssifTaskSemId)
    {
        OsixSemDel (gWssIfGlobals.wssifTaskSemId);
    }

    return OSIX_SUCCESS;
}


/****************************************************************************
 * *                                                                           *
 * * Function     : WssIfLock       
 * *                                                                           *
 * * Description  : This function will take the sem lock which will be invoked
 * *                before accessing the DB
 * *                                                                           *
 * * Input        : None
 * *                                                                           *
 * * Output       : None
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                 OSIX_FAILURE, otherwise
 * *                
 * *                                                                           *
 * *****************************************************************************/

UINT1 WssIfLock (VOID)
{
    if (OsixSemTake(gWssIfGlobals.wssifTaskSemId) == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfLock: TakeSem failure\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}


/****************************************************************************
 * *                                                                           *
 * * Function     : WssIfUnLock       
 * *                                                                           *
 * * Description  : This function will take the release the lock which will be 
 * *                invoked after the DB access is completed
 * *                                                                           *
 * * Input        : None
 * *                                                                           *
 * * Output       : None
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS, if initialization succeeds                   *
 * *                 OSIX_FAILURE, otherwise
 * *                
 * *                                                                           *
 * *****************************************************************************/

UINT1 WssIfUnLock (VOID)
{
    if (OsixSemGive(gWssIfGlobals.wssifTaskSemId) == OSIX_FAILURE)
    {
        WSSIF_TRC (WSSIF_FAILURE_TRC,
                   "WssIfLock: GiveSem failure\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

#endif

