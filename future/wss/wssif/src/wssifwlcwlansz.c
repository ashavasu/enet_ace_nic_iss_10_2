/******************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved                       *
 *  $Id: wssifwlcwlansz.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $                                                                            *
 * Description: This file contains the MEMPOOL related API for WSSSTA MODULE  *
 ******************************************************************************/

#define _WSSIFWLCWLANSZ_C
#include "wssifinc.h"
extern INT4  IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams); 
extern INT4  IssSzRegisterModulePoolId( CHR1 *pu1ModName, tMemPoolId *pModPoolId); 
INT4  WssifwlcwlanSizingMemCreateMemPools()
{
    INT4 i4RetVal;
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < WSSIFWLCWLAN_MAX_SIZING_ID; i4SizingId++) {
        i4RetVal = (INT4)MemCreateMemPool( 
                          FsWSSIFWLCWLANSizingParams[i4SizingId].u4StructSize,
                          FsWSSIFWLCWLANSizingParams[i4SizingId].u4PreAllocatedUnits,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(WSSIFWLCWLANMemPoolIds[ i4SizingId]));
        if( i4RetVal == (INT4) MEM_FAILURE) {
            WssifwlcwlanSizingMemDeleteMemPools();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4   WssifwlcwlanSzRegisterModuleSizingParams( CHR1 *pu1ModName)
{
      /* Copy the Module Name */ 
       IssSzRegisterModuleSizingParams( pu1ModName, FsWSSIFWLCWLANSizingParams); 
      IssSzRegisterModulePoolId( pu1ModName, WSSIFWLCWLANMemPoolIds); 
      return OSIX_SUCCESS; 
}


VOID  WssifwlcwlanSizingMemDeleteMemPools()
{
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < WSSIFWLCWLAN_MAX_SIZING_ID; i4SizingId++) {
        if(WSSIFWLCWLANMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool( WSSIFWLCWLANMemPoolIds[ i4SizingId] );
            WSSIFWLCWLANMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
