/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: testlw.c,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "testlw.h"
# include  "rfmgmt.h"
# include  "rfminc.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetSendpackets
 Input       :  The Indices

                The Object 
                retValSendpackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetSendpackets (INT4 *pi4RetValSendpackets)
{
    UNUSED_PARAM (pi4RetValSendpackets);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetSendpackets
 Input       :  The Indices

                The Object 
                setValSendpackets
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetSendpackets (INT4 i4SetValSendpackets)
{
    UNUSED_PARAM (i4SetValSendpackets);
    if ((i4SetValSendpackets == 1) || (i4SetValSendpackets == 11))
    {
        FsRfMgmtHwGetNeighborInfoTest (i4SetValSendpackets);
    }
    else if ((i4SetValSendpackets == 2)
             || ((i4SetValSendpackets > 3) && (i4SetValSendpackets <= 7)))
    {
        FsRfMgmtHwGetClientInfoTest (i4SetValSendpackets);
    }
    else if ((i4SetValSendpackets == 3) || (i4SetValSendpackets > 12))
    {
        FsRfMgmtHwGetNeighborInfoAP (i4SetValSendpackets);
    }
    else
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Sendpackets
 Input       :  The Indices

                The Object 
                testValSendpackets
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Sendpackets (UINT4 *pu4ErrorCode, INT4 i4TestValSendpackets)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValSendpackets);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Sendpackets
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Sendpackets (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : FsRfMgmtHwGetNeighborInfoTest                              */
/*                                                                           */
/* Description  : This function will scan for the neighbor AP message and    */
/*                post the request to RFMGMT module                          */
/*                                                                           */
/* Input        : pRfMgmtNpWrHwGetNeighInfo                                  */
/*                                                                           */
/* Output       : pRfMgmtNpWrHwGetNeighInfo                                  */
/*                                                                           */
/*                                                                           */
/* Returns      : FNP_SUCCESS, is processing succeeds                        */
/*                FNP_FAILURE, otherwise                                     */
/*****************************************************************************/
INT4
FsRfMgmtHwGetNeighborInfoTest (INT4 i4SetValSendpackets)
{
    tRfMgmtMsgStruct    MsgStruct;
    UINT4               u4RadioIfIndex = SYS_DEF_MAX_ENET_INTERFACES + 1;
    tMacAddr            MacAddr;
    UINT2               u2ScannedChannel = 1;
    INT2                i2Rssi = -60;
    tMacAddr            Mac = { 0x00, 0x02, 0x02, 0x03, 0x04, 0x02 };

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    MEMSET (&MsgStruct, 0, sizeof (tRfMgmtMsgStruct));

    MEMCPY (MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.NeighborMacAddr,
            Mac, sizeof (tMacAddr));

    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4RadioIfIndex = u4RadioIfIndex;
    if (i4SetValSendpackets == 1)
    {
        MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel =
            u2ScannedChannel;
    }
    else
    {
        MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 6;
    }
    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.i2Rssi = i2Rssi;
    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4MsgType = RFMGMT_NEIGH_SCAN_MSG;

    /* Post the message to RFMGMT module */
    if (RfMgmtWtpEnquePkts (&MsgStruct) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : FsRfMgmtHwGetNeighborInfoAP                                  */
/*                                                                           */
/* Description  : This function will scan for the neighbor AP message and    */
/*                post the request to RFMGMT module                          */
/*                                                                           */
/* Input        : pRfMgmtNpWrHwGetNeighInfo                                  */
/*                                                                           */
/* Output       : pRfMgmtNpWrHwGetNeighInfo                                  */
/*                                                                           */
/*                                                                           */
/* Returns      : FNP_SUCCESS, is processing succeeds                        */
/*                FNP_FAILURE, otherwise                                     */
/*****************************************************************************/
INT4
FsRfMgmtHwGetNeighborInfoAP (INT4 i4SetValSendpackets)
{
    tRfMgmtMsgStruct    MsgStruct;
    UINT4               u4RadioIfIndex = SYS_DEF_MAX_ENET_INTERFACES + 1;
    UINT2               u2ScannedChannel[3] = { 6, 11, 1 };
    INT2                i2Rssi = -60;
    UINT1               i = 0;
    tMacAddr            Mac[3] = {
        {0x00, 0x03, 0x02, 0x03, 0x04, 0x02},
        {0x00, 0x04, 0x02, 0x03, 0x04, 0x02},
        {0x00, 0x05, 0x02, 0x03, 0x04, 0x02}
    };
    tMacAddr            MacAddr = { 0x00, 0x02, 0x02, 0x03, 0x04, 0x02 };

    MEMSET (&MsgStruct, 0, sizeof (tRfMgmtMsgStruct));

    if (i4SetValSendpackets == 13)
    {
        i2Rssi = -160;
    }
    else
    {
        i2Rssi = -60;
    }
    for (i = 0; i < 3; i++)
    {
        if (i4SetValSendpackets == 51)
        {
            for (i = 0; i < 3; i++)
            {
                if (i == 0)
                {
                    MacAddr[1] = 0x3;
                    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 6;
                }
                else if (i == 1)
                {
                    MacAddr[1] = 0x4;
                    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 11;
                }
                else
                {
                    MacAddr[1] = 0x5;
                    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 1;
                }
                MEMCPY (MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.NeighborMacAddr,
                        MacAddr, sizeof (tMacAddr));
                MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4RadioIfIndex =
                    u4RadioIfIndex;

                MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.i2Rssi = i2Rssi - i;
                MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4MsgType =
                    RFMGMT_NEIGH_SCAN_MSG;

                /* Post the message to RFMGMT module */
                if (RfMgmtWtpEnquePkts (&MsgStruct) != OSIX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
            return SNMP_SUCCESS;

        }
        if (i4SetValSendpackets == 52)
        {
            for (i = 0; i < 3; i++)
            {
                if (i == 0)
                {
                    MacAddr[1] = 0x2;
                    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 1;
                }
                else if (i == 1)
                {
                    MacAddr[1] = 0x4;
                    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 11;
                }
                else
                {
                    MacAddr[1] = 0x5;
                    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 1;
                }
                MEMCPY (MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.NeighborMacAddr,
                        MacAddr, sizeof (tMacAddr));
                MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4RadioIfIndex =
                    u4RadioIfIndex;

                MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.i2Rssi = i2Rssi - i;
                MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4MsgType =
                    RFMGMT_NEIGH_SCAN_MSG;

                /* Post the message to RFMGMT module */
                if (RfMgmtWtpEnquePkts (&MsgStruct) != OSIX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
            return SNMP_SUCCESS;

        }
        if (i4SetValSendpackets == 53)
        {
            for (i = 0; i < 3; i++)
            {
                if (i == 0)
                {
                    MacAddr[1] = 0x2;
                    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 1;
                }
                else if (i == 1)
                {
                    MacAddr[1] = 0x3;
                    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 6;
                }
                else
                {
                    MacAddr[1] = 0x5;
                    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 1;
                }
                MEMCPY (MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.NeighborMacAddr,
                        MacAddr, sizeof (tMacAddr));
                MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4RadioIfIndex =
                    u4RadioIfIndex;

                MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.i2Rssi = i2Rssi - i;
                MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4MsgType =
                    RFMGMT_NEIGH_SCAN_MSG;

                /* Post the message to RFMGMT module */
                if (RfMgmtWtpEnquePkts (&MsgStruct) != OSIX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
            return SNMP_SUCCESS;
        }
        if (i4SetValSendpackets == 54)
        {
            for (i = 0; i < 3; i++)
            {
                if (i == 0)
                {
                    MacAddr[1] = 0x2;
                    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 1;
                }
                else if (i == 1)
                {
                    MacAddr[1] = 0x3;
                    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 6;
                }
                else
                {
                    MacAddr[1] = 0x4;
                    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 11;
                }
                MEMCPY (MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.NeighborMacAddr,
                        MacAddr, sizeof (tMacAddr));
                MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4RadioIfIndex =
                    u4RadioIfIndex;

                MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.i2Rssi = i2Rssi - i;
                MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4MsgType =
                    RFMGMT_NEIGH_SCAN_MSG;

                /* Post the message to RFMGMT module */
                if (RfMgmtWtpEnquePkts (&MsgStruct) != OSIX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
            return SNMP_SUCCESS;
        }

        if (i4SetValSendpackets == 21)
        {
            MacAddr[1] = 0x05;
            MEMCPY (MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.NeighborMacAddr,
                    MacAddr, sizeof (tMacAddr));
            MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4RadioIfIndex =
                u4RadioIfIndex;
            MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 1;

            MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.i2Rssi = i2Rssi - i;
            MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4MsgType =
                RFMGMT_NEIGH_SCAN_MSG;

            /* Post the message to RFMGMT module */
            if (RfMgmtWtpEnquePkts (&MsgStruct) != OSIX_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        }
        else if (i4SetValSendpackets == 26)
        {
            MacAddr[1] = 0x06;
            MEMCPY (MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.NeighborMacAddr,
                    MacAddr, sizeof (tMacAddr));
            MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4RadioIfIndex =
                u4RadioIfIndex;
            MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 6;

            MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.i2Rssi = i2Rssi - i;
            MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4MsgType =
                RFMGMT_NEIGH_SCAN_MSG;

            /* Post the message to RFMGMT module */
            if (RfMgmtWtpEnquePkts (&MsgStruct) != OSIX_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        }
        else if (i4SetValSendpackets == 31)
        {
            MacAddr[1] = 0x07;
            MEMCPY (MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.NeighborMacAddr,
                    MacAddr, sizeof (tMacAddr));
            MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4RadioIfIndex =
                u4RadioIfIndex;
            MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 11;

            MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.i2Rssi = i2Rssi - i;
            MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4MsgType =
                RFMGMT_NEIGH_SCAN_MSG;

            /* Post the message to RFMGMT module */
            if (RfMgmtWtpEnquePkts (&MsgStruct) != OSIX_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        }
        if (i4SetValSendpackets == 20)
        {
            for (i = 0; i < 5; i++)
            {
                MacAddr[1] = i + 3;
                MEMCPY (MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.NeighborMacAddr,
                        MacAddr, sizeof (tMacAddr));
                MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4RadioIfIndex =
                    u4RadioIfIndex;

                if ((i == 0) || (i == 3))
                    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 6;
                else if ((i == 1) || (i == 4))
                    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 11;
                else
                    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 1;

                MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.i2Rssi = i2Rssi - i;
                MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4MsgType =
                    RFMGMT_NEIGH_SCAN_MSG;

                /* Post the message to RFMGMT module */
                if (RfMgmtWtpEnquePkts (&MsgStruct) != OSIX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
            return SNMP_SUCCESS;
        }
        if (i4SetValSendpackets == 50)
        {
            for (i = 0; i < 3; i++)
            {
                if (i == 0)
                {
                    MacAddr[1] = 0x4;
                    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 11;
                }
                else if (i == 1)
                {
                    MacAddr[1] = 0x2;
                    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 1;
                }
                else
                {
                    MacAddr[1] = 0x6;
                    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 6;
                }
                MEMCPY (MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.NeighborMacAddr,
                        MacAddr, sizeof (tMacAddr));
                MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4RadioIfIndex =
                    u4RadioIfIndex;

                MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.i2Rssi = i2Rssi - i;
                MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4MsgType =
                    RFMGMT_NEIGH_SCAN_MSG;

                /* Post the message to RFMGMT module */
                if (RfMgmtWtpEnquePkts (&MsgStruct) != OSIX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
            return SNMP_SUCCESS;
        }
        else if (i4SetValSendpackets != 14)
        {
            MEMCPY (MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.NeighborMacAddr,
                    Mac[i], sizeof (tMacAddr));
            MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4RadioIfIndex =
                u4RadioIfIndex;
            MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel =
                u2ScannedChannel[i];
            MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.i2Rssi = i2Rssi - i;
            MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4MsgType =
                RFMGMT_NEIGH_SCAN_MSG;

        }
        else
        {
            MEMCPY (MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.NeighborMacAddr,
                    MacAddr, sizeof (tMacAddr));
            MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4RadioIfIndex =
                u4RadioIfIndex;
            MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel = 1;

            MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.i2Rssi = i2Rssi - i;
            MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4MsgType =
                RFMGMT_NEIGH_SCAN_MSG;

            /* Post the message to RFMGMT module */
            if (RfMgmtWtpEnquePkts (&MsgStruct) != OSIX_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        }

        /* Post the message to RFMGMT module */
        if (RfMgmtWtpEnquePkts (&MsgStruct) != OSIX_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

INT4
FsRfMgmtHwGetClientInfoTest (INT4 i4SetValSendpackets)
{
    tRfMgmtMsgStruct    MsgStruct;
    UINT4               u4RadioIfIndex = SYS_DEF_MAX_ENET_INTERFACES + 1;
    tMacAddr            MacAddr;
    UINT2               u2SNR = -25;
    /*tMacAddr         Mac = {0x0, 0x0, 0x0, 0x0, 0x0, 0x0}; */
    tMacAddr            Mac;
    tMacAddr            Mac1 = { 0x02, 0x03, 0x04, 0xAB, 0x43, 0x44 };
    tMacAddr            Mac2 = { 0x04, 0x03, 0x04, 0xAB, 0x43, 0x44 };
    tMacAddr            Mac3 = { 0x05, 0x03, 0x04, 0xAB, 0x43, 0x44 };

    MEMSET (&Mac, 0, sizeof (tMacAddr));

    /* Mac = {0x02, 0x03, 0x04, 0xAB, 0x43, 0x44}; */
    if (i4SetValSendpackets == 2)
    {
        /*MEMCPY(Mac, "0x02, 0x03, 0x04, 0xAB, 0x43, 0x44",sizeof (tMacAddr)); */
        MEMCPY (Mac, Mac1, sizeof (tMacAddr));
    }
    if (i4SetValSendpackets == 4)
    {
        /*MEMCPY(Mac, "0x04, 0x03, 0x04, 0xAB, 0x43, 0x44",sizeof (tMacAddr)); */
        MEMCPY (Mac, Mac2, sizeof (tMacAddr));
    }
    if (i4SetValSendpackets == 5)
    {
        /*MEMCPY(Mac,"0x05, 0x03, 0x04, 0xAB, 0x43, 0x44",sizeof (tMacAddr)); */
        MEMCPY (Mac, Mac3, sizeof (tMacAddr));
        u2SNR = -100;
    }
    if (i4SetValSendpackets == 6)
    {
        MEMCPY (Mac, Mac1, sizeof (tMacAddr));
        u2SNR = -100;
    }
    if (i4SetValSendpackets == 7)
    {
        MEMCPY (Mac, Mac1, sizeof (tMacAddr));
        u2SNR = -30;
    }

    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    MEMSET (&MsgStruct, 0, sizeof (tRfMgmtMsgStruct));

    printf ("\nLine - %d, Function - %s\n", __LINE__, __FUNCTION__);

    /* Invoke Hardware call to scan for the neighbor message */
    /* WirelessHwClientScan (pRfMgmtNpWrHwGetClientInfo); */

    printf ("\nLine - %d, Function - %s\n", __LINE__, __FUNCTION__);
    MEMCPY (MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.ClientMacAddress,
            Mac, sizeof (tMacAddr));
    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4RadioIfIndex = u4RadioIfIndex;

    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.i2ClientSNR = u2SNR;

    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4MsgType = RFMGMT_CLIENT_SCAN_MSG;

    /* Post the message to RFMGMT module */
    if (RfMgmtWtpEnquePkts (&MsgStruct) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceTestAPStatsTable
 Input       :  The Indices
                IfIndex
                TestScannedChannel
                TestNeighborMacAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceTestAPStatsTable (INT4 i4IfIndex,
                                          UINT4 u4TestScannedChannel,
                                          tMacAddr TestNeighborMacAddress)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestScannedChannel);
    UNUSED_PARAM (TestNeighborMacAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexTestAPStatsTable
 Input       :  The Indices
                IfIndex
                TestScannedChannel
                TestNeighborMacAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexTestAPStatsTable (INT4 *pi4IfIndex,
                                  UINT4 *pu4TestScannedChannel,
                                  tMacAddr * pTestNeighborMacAddress)
{
    UNUSED_PARAM (pi4IfIndex);
    UNUSED_PARAM (pu4TestScannedChannel);
    UNUSED_PARAM (pTestNeighborMacAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexTestAPStatsTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                TestScannedChannel
                nextTestScannedChannel
                TestNeighborMacAddress
                nextTestNeighborMacAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexTestAPStatsTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                 UINT4 u4TestScannedChannel,
                                 UINT4 *pu4NextTestScannedChannel,
                                 tMacAddr TestNeighborMacAddress,
                                 tMacAddr * pNextTestNeighborMacAddress)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4NextIfIndex);
    UNUSED_PARAM (u4TestScannedChannel);
    UNUSED_PARAM (pu4NextTestScannedChannel);
    UNUSED_PARAM (TestNeighborMacAddress);
    UNUSED_PARAM (pNextTestNeighborMacAddress);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetTestRSSIValue
 Input       :  The Indices
                IfIndex
                TestScannedChannel
                TestNeighborMacAddress

                The Object 
                retValTestRSSIValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTestRSSIValue (INT4 i4IfIndex, UINT4 u4TestScannedChannel,
                     tMacAddr TestNeighborMacAddress,
                     INT4 *pi4RetValTestRSSIValue)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestScannedChannel);
    UNUSED_PARAM (TestNeighborMacAddress);
    UNUSED_PARAM (pi4RetValTestRSSIValue);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetTestRSSIValue
 Input       :  The Indices
                IfIndex
                TestScannedChannel
                TestNeighborMacAddress

                The Object 
                setValTestRSSIValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTestRSSIValue (INT4 i4IfIndex, UINT4 u4TestScannedChannel,
                     tMacAddr TestNeighborMacAddress,
                     INT4 i4SetValTestRSSIValue)
{
    tRfMgmtMsgStruct    MsgStruct;

    MEMSET (&MsgStruct, 0, sizeof (tRfMgmtMsgStruct));

    MEMCPY (MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.NeighborMacAddr,
            TestNeighborMacAddress, sizeof (tMacAddr));
    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4RadioIfIndex = i4IfIndex;
    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u2ScannedChannel =
        u4TestScannedChannel;

    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.i2Rssi = i4SetValTestRSSIValue;
    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4MsgType = RFMGMT_NEIGH_SCAN_MSG;

    /* Post the message to RFMGMT module */
    if (RfMgmtWtpEnquePkts (&MsgStruct) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2TestRSSIValue
 Input       :  The Indices
                IfIndex
                TestScannedChannel
                TestNeighborMacAddress

                The Object 
                testValTestRSSIValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TestRSSIValue (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                        UINT4 u4TestScannedChannel,
                        tMacAddr TestNeighborMacAddress,
                        INT4 i4TestValTestRSSIValue)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestScannedChannel);
    UNUSED_PARAM (TestNeighborMacAddress);
    UNUSED_PARAM (i4TestValTestRSSIValue);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2TestAPStatsTable
 Input       :  The Indices
                IfIndex
                TestScannedChannel
                TestNeighborMacAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2TestAPStatsTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : TestTpcClientTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceTestTpcClientTable
 Input       :  The Indices
                IfIndex
                TestClientMacAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceTestTpcClientTable (INT4 i4IfIndex,
                                            tMacAddr TestClientMacAddress)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (TestClientMacAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexTestTpcClientTable
 Input       :  The Indices
                IfIndex
                TestClientMacAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexTestTpcClientTable (INT4 *pi4IfIndex,
                                    tMacAddr * pTestClientMacAddress)
{
    UNUSED_PARAM (pi4IfIndex);
    UNUSED_PARAM (pTestClientMacAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexTestTpcClientTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                TestClientMacAddress
                nextTestClientMacAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexTestTpcClientTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                   tMacAddr TestClientMacAddress,
                                   tMacAddr * pNextTestClientMacAddress)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4NextIfIndex);
    UNUSED_PARAM (TestClientMacAddress);
    UNUSED_PARAM (pNextTestClientMacAddress);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetTestClientSNR
 Input       :  The Indices
                IfIndex
                TestClientMacAddress

                The Object
                retValTestClientSNR
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTestClientSNR (INT4 i4IfIndex, tMacAddr TestClientMacAddress,
                     INT4 *pi4RetValTestClientSNR)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (TestClientMacAddress);
    UNUSED_PARAM (pi4RetValTestClientSNR);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetTestClientSNR
 Input       :  The Indices
                IfIndex
                TestClientMacAddress

                The Object
                setValTestClientSNR
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetTestClientSNR (INT4 i4IfIndex, tMacAddr TestClientMacAddress,
                     INT4 i4SetValTestClientSNR)
{
    tRfMgmtMsgStruct    MsgStruct;

    MEMSET (&MsgStruct, 0, sizeof (tRfMgmtMsgStruct));

    MEMCPY (MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.ClientMacAddress,
            TestClientMacAddress, sizeof (tMacAddr));
    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4RadioIfIndex = i4IfIndex;
    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.i2ClientSNR = i4SetValTestClientSNR;

    MsgStruct.unRfMgmtMsg.RfMgmtQueueReq.u4MsgType = RFMGMT_CLIENT_SCAN_MSG;

    /* Post the message to RFMGMT module */
    if (RfMgmtWtpEnquePkts (&MsgStruct) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2TestClientSNR
 Input       :  The Indices
                IfIndex
                TestClientMacAddress

                The Object
                testValTestClientSNR
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TestClientSNR (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                        tMacAddr TestClientMacAddress,
                        INT4 i4TestValTestClientSNR)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (TestClientMacAddress);
    UNUSED_PARAM (i4TestValTestClientSNR);
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2TestTpcClientTable
 Input       :  The Indices
                IfIndex
                TestClientMacAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2TestTpcClientTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
