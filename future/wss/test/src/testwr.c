/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: testwr.c,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "testlw.h"
# include  "testwr.h"
# include  "testdb.h"

VOID
RegisterTEST ()
{
    SNMPRegisterMib (&testOID, &testEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&testOID, (const UINT1 *) "test");
}

VOID
UnRegisterTEST ()
{
    SNMPUnRegisterMib (&testOID, &testEntry);
    SNMPDelSysorEntry (&testOID, (const UINT1 *) "test");
}

INT4
SendpacketsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetSendpackets (&(pMultiData->i4_SLongValue)));
}

INT4
SendpacketsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetSendpackets (pMultiData->i4_SLongValue));
}

INT4
SendpacketsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Sendpackets (pu4Error, pMultiData->i4_SLongValue));
}

INT4
SendpacketsDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Sendpackets (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexTestAPStatsTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexTestAPStatsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexTestAPStatsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
TestScannedChannelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceTestAPStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[1].u4_ULongValue;

    return SNMP_SUCCESS;

}

INT4
TestNeighborMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceTestAPStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[2].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[2].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
TestRSSIValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceTestAPStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetTestRSSIValue (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiIndex->pIndex[1].u4_ULongValue,
                                 (*(tMacAddr *) pMultiIndex->pIndex[2].
                                  pOctetStrValue->pu1_OctetList),
                                 &(pMultiData->i4_SLongValue)));

}

INT4
TestRSSIValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetTestRSSIValue (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiIndex->pIndex[1].u4_ULongValue,
                                 (*(tMacAddr *) pMultiIndex->pIndex[2].
                                  pOctetStrValue->pu1_OctetList),
                                 pMultiData->i4_SLongValue));

}

INT4
TestRSSIValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2TestRSSIValue (pu4Error,
                                    pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    (*(tMacAddr *) pMultiIndex->pIndex[2].
                                     pOctetStrValue->pu1_OctetList),
                                    pMultiData->i4_SLongValue));

}

INT4
TestAPStatsTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2TestAPStatsTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexTestTpcClientTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexTestTpcClientTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexTestTpcClientTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
TestClientMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceTestTpcClientTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[1].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[1].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
TestClientSNRGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceTestTpcClientTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetTestClientSNR (pMultiIndex->pIndex[0].i4_SLongValue,
                                 (*(tMacAddr *) pMultiIndex->pIndex[1].
                                  pOctetStrValue->pu1_OctetList),
                                 &(pMultiData->i4_SLongValue)));

}

INT4
TestClientSNRSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetTestClientSNR (pMultiIndex->pIndex[0].i4_SLongValue,
                                 (*(tMacAddr *) pMultiIndex->pIndex[1].
                                  pOctetStrValue->pu1_OctetList),
                                 pMultiData->i4_SLongValue));

}

INT4
TestClientSNRTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2TestClientSNR (pu4Error,
                                    pMultiIndex->pIndex[0].i4_SLongValue,
                                    (*(tMacAddr *) pMultiIndex->pIndex[1].
                                     pOctetStrValue->pu1_OctetList),
                                    pMultiData->i4_SLongValue));

}

INT4
TestTpcClientTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2TestTpcClientTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
