/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: testdb.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _TESTDB_H
#define _TESTDB_H

UINT1 TestAPStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 TestTpcClientTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};

UINT4 test [] ={1,3,6,1,4,1,29601,2,200};
tSNMP_OID_TYPE testOID = {9, test};


UINT4 Sendpackets [ ] ={1,3,6,1,4,1,29601,2,200,1,1};
UINT4 TestScannedChannel [ ] ={1,3,6,1,4,1,29601,2,200,1,2,1,1};
UINT4 TestNeighborMacAddress [ ] ={1,3,6,1,4,1,29601,2,200,1,2,1,2};
UINT4 TestRSSIValue [ ] ={1,3,6,1,4,1,29601,2,200,1,2,1,3};
UINT4 TestClientMacAddress [ ] ={1,3,6,1,4,1,29601,2,200,1,3,1,1};
UINT4 TestClientSNR [ ] ={1,3,6,1,4,1,29601,2,200,1,3,1,2};




tMbDbEntry testMibEntry[]= {

{{11,Sendpackets}, NULL, SendpacketsGet, SendpacketsSet, SendpacketsTest, SendpacketsDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{13,TestScannedChannel}, GetNextIndexTestAPStatsTable, TestScannedChannelGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, TestAPStatsTableINDEX, 3, 0, 0, NULL},

{{13,TestNeighborMacAddress}, GetNextIndexTestAPStatsTable, TestNeighborMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, TestAPStatsTableINDEX, 3, 0, 0, NULL},

{{13,TestRSSIValue}, GetNextIndexTestAPStatsTable, TestRSSIValueGet, TestRSSIValueSet, TestRSSIValueTest, TestAPStatsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, TestAPStatsTableINDEX, 3, 0, 0, NULL},
{{13,TestClientMacAddress}, GetNextIndexTestTpcClientTable, TestClientMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, TestTpcClientTableINDEX, 2, 0, 0, NULL},

{{13,TestClientSNR}, GetNextIndexTestTpcClientTable, TestClientSNRGet, TestClientSNRSet, TestClientSNRTest, TestTpcClientTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, TestTpcClientTableINDEX, 2, 0, 0, NULL},
};
tMibData testEntry = { 6, testMibEntry };

#endif /* _TESTDB_H */

