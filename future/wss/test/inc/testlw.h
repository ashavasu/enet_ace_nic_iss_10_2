/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: testlw.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetSendpackets ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetSendpackets ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Sendpackets ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Sendpackets ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT4 FsRfMgmtHwGetNeighborInfoTest ARG_LIST ((INT4));
INT4 FsRfMgmtHwGetNeighborInfoAP ARG_LIST ((INT4));

INT4 FsRfMgmtHwGetClientInfoTest ARG_LIST ((INT4));
/* Proto Validate Index Instance for TestAPStatsTable. */
INT1
nmhValidateIndexInstanceTestAPStatsTable ARG_LIST((INT4  , UINT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for TestAPStatsTable  */

INT1
nmhGetFirstIndexTestAPStatsTable ARG_LIST((INT4 * , UINT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexTestAPStatsTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetTestRSSIValue ARG_LIST((INT4  , UINT4  , tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetTestRSSIValue ARG_LIST((INT4  , UINT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2TestRSSIValue ARG_LIST((UINT4 *  ,INT4  , UINT4  , tMacAddr  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2TestAPStatsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
/* Proto Validate Index Instance for TestTpcClientTable. */
INT1
nmhValidateIndexInstanceTestTpcClientTable ARG_LIST((INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for TestTpcClientTable  */

INT1
nmhGetFirstIndexTestTpcClientTable ARG_LIST((INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexTestTpcClientTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetTestClientSNR ARG_LIST((INT4  , tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetTestClientSNR ARG_LIST((INT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2TestClientSNR ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));

/* Low Level DEP Routines for.  */

/* Low Level DEP Routines for.  */

INT1
nmhDepv2TestTpcClientTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
