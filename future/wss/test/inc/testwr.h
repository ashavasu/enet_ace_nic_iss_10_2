/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: testwr.h,v 1.1.1.1 2016/09/20 10:42:38 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/
#ifndef _TESTWR_H
#define _TESTWR_H

VOID RegisterTEST(VOID);

VOID UnRegisterTEST(VOID);
INT4 SendpacketsGet(tSnmpIndex *, tRetVal *);
INT4 SendpacketsSet(tSnmpIndex *, tRetVal *);
INT4 SendpacketsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 SendpacketsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexTestAPStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 TestScannedChannelGet(tSnmpIndex *, tRetVal *);
INT4 TestNeighborMacAddressGet(tSnmpIndex *, tRetVal *);
INT4 TestRSSIValueGet(tSnmpIndex *, tRetVal *);
INT4 TestRSSIValueSet(tSnmpIndex *, tRetVal *);
INT4 TestRSSIValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 TestAPStatsTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexTestTpcClientTable(tSnmpIndex *, tSnmpIndex *);
INT4 TestClientMacAddressGet(tSnmpIndex *, tRetVal *);
INT4 TestClientSNRGet(tSnmpIndex *, tRetVal *);
INT4 TestClientSNRSet(tSnmpIndex *, tRetVal *);
INT4 TestClientSNRTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 TestTpcClientTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _TESTWR_H */
