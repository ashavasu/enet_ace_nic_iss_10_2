/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wsscfgtask.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 * Description:This file contains procedures related to
 *             WSSCFG - Task Initialization
 *******************************************************************/

#include "wsscfginc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : WsscfgTaskSpawnWsscfgTask()                                */
/*                                                                           */
/* Description  : This procedure is provided to Spawn Wsscfg Task            */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS, if WSSCFG Task is successfully spawned       */
/*                OSIX_FAILURE, otherwise                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID 
WsscfgTaskSpawnWsscfgTask(INT1 * pi1Arg)
{

    UNUSED_PARAM(pi1Arg);
    WSSCFG_TRC_FUNC((WSSCFG_FN_ENTRY, "FUNC:WsscfgTaskSpawnWsscfgTask\n"));

    /* task initializations */
    if (WsscfgMainTaskInit() == OSIX_FAILURE) 
    {
        WSSCFG_TRC((WSSCFG_TASK_TRC, "!!!!! WSSCFG TASK INIT FAILURE !!!!!\n"));
        WsscfgMainDeInit();
        lrInitComplete(OSIX_FAILURE);
        return;
    }

    lrInitComplete(OSIX_SUCCESS);
    WsscfgMainTask();
    WSSCFG_TRC_FUNC((WSSCFG_FN_EXIT,
                     "EXIT:WsscfgTaskSpawnWsscfgTask\n"));
    return;
}

/*------------------------------------------------------------------------*/
/*                        End of the file  wsscfgtask.c                     */
/*------------------------------------------------------------------------*/
