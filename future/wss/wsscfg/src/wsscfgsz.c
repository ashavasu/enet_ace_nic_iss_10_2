/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: wsscfgsz.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: This file contains the WSSCFG sizing  routines.
 *
 *****************************************************************************/

#define _WSSCFGSZ_C
#include "wsscfginc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
WsscfgSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < WSSCFG_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsWSSCFGSizingParams[i4SizingId].u4StructSize,
                              FsWSSCFGSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(WSSCFGMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            WsscfgSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
WsscfgSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsWSSCFGSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, WSSCFGMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
WsscfgSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < WSSCFG_MAX_SIZING_ID; i4SizingId++)
    {
        if (WSSCFGMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (WSSCFGMemPoolIds[i4SizingId]);
            WSSCFGMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
