/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: fs11ndb.c,v 1.3 2017/11/24 10:37:08 siva Exp $
 *
 * Description: This file contains the configuration routines for WLAN Radio
 *              module.
 *
 *****************************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "wsscfginc.h"
# include  "wsscfgcli.h"

#ifndef __FS11NDB_C__
#define __FS11NDB_C__

/****************************************************************************                                  
 Function    :  wsscfgGetDot11LDPCCodingOptionImplemented                                                         
 Input       :  The Indices                                                                                    
                IfIndex                                                                                        
                                                                                                               
                The Object                                                                                     
                retValDot11LDPCCodingOptionImplemented                                                         
 Output      :  The Get Low Lev Routine Take the Indices &                                                     
                store the Value requested in the Return val.                                                   
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgGetDot11LDPCCodingOptionImplemented (INT4 i4IfIndex,
                                           INT4
                                           *pi4RetValDot11LDPCCodingOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppLdpcCoding = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11LDPCCodingOptionImplemented =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppLdpcCoding;
    return OSIX_SUCCESS;
}

/****************************************************************************                                  
 Function    :  wsscfgGetDot11LDPCCodingOptionActivated                                                           
 Input       :  The Indices                                                                                    
                IfIndex                                                                                        
                                                                                                               
                The Object                                                                                     
                retValDot11LDPCCodingOptionActivated                                                           
 Output      :  The Get Low Lev Routine Take the Indices &                                                     
                store the Value requested in the Return val.                                                   
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                   
****************************************************************************/
INT1
wsscfgGetDot11LDPCCodingOptionActivated (INT4 i4IfIndex,
                                         INT4
                                         *pi4RetValDot11LDPCCodingOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bLdpcCoding = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11LDPCCodingOptionActivated =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1LdpcCoding;
    return OSIX_SUCCESS;
}

/****************************************************************************                                  
 Function    :  wsscfgGetDot11MIMOPowerSave                                                                       
 Input       :  The Indices                                                                                    
                IfIndex                                                                                        
                                                                                                               
                The Object                                                                                     
                retValDot11MIMOPowerSave                                                                       
 Output      :  The Get Low Lev Routine Take the Indices &                                                     
                store the Value requested in the Return val.                                                   
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                   
****************************************************************************/
INT1
wsscfgGetDot11MIMOPowerSave (INT4 i4IfIndex, INT4 *pi4RetValDot11MIMOPowerSave)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppSmPowerSave = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11MIMOPowerSave
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppSmPowerSave;
    return OSIX_SUCCESS;
}

/****************************************************************************                                  
 Function    :  wsscfgGetFsDot11nConfigMIMOPowerSave                                                              
 Input       :  The Indices                                                                                    
                IfIndex                                                                                        
                                                                                                               
                The Object                                                                                     
                retValFsDot11nConfigMIMOPowerSave                                                              
 Output      :  The Get Low Lev Routine Take the Indices &                                                     
                store the Value requested in the Return val.                                                   
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                   
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigMIMOPowerSave (INT4 i4IfIndex,
                                      INT4
                                      *pi4RetValFsDot11nConfigMIMOPowerSave)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSmPowerSave = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigMIMOPowerSave
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SmPowerSave;

    return OSIX_SUCCESS;
}

/****************************************************************************                                  
 Function    :  wsscfgGetDot11HTGreenfieldOptionImplemented                                                       
 Input       :  The Indices                                                                                    
                IfIndex                                                                                        
                                                                                                               
                The Object                                                                                     
                retValDot11HTGreenfieldOptionImplemented                                                       
 Output      :  The Get Low Lev Routine Take the Indices &                                                     
                store the Value requested in the Return val.                                                   
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                   
****************************************************************************/
INT1
wsscfgGetDot11HTGreenfieldOptionImplemented (INT4 i4IfIndex,
                                             INT4
                                             *pi4RetValDot11HTGreenfieldOptionImplemented)
{

    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppHtGreenField = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11HTGreenfieldOptionImplemented
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppHtGreenField;
    return OSIX_SUCCESS;
}

/****************************************************************************                                  
 Function    :  wsscfgGetDot11HTGreenfieldOptionActivated                                                         
 Input       :  The Indices                                                                                    
                IfIndex                                                                                        
                                                                                                               
                The Object                                                                                     
                retValDot11HTGreenfieldOptionActivated                                                         
 Output      :  The Get Low Lev Routine Take the Indices &                                                     
                store the Value requested in the Return val.                                                   
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                   
****************************************************************************/
INT1
wsscfgGetDot11HTGreenfieldOptionActivated (INT4 i4IfIndex,
                                           INT4
                                           *pi4RetValDot11HTGreenfieldOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bHtGreenField = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;

    }
    *pi4RetValDot11HTGreenfieldOptionActivated =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1HtGreenField;
    return OSIX_SUCCESS;
}

/****************************************************************************                                  
 Function    :  wsscfgGetDot11ShortGIOptionInTwentyImplemented                                                    
 Input       :  The Indices                                                                                    
                IfIndex                                                                                        
                                                                                                               
                The Object                                                                                     
                retValDot11ShortGIOptionInTwentyImplemented                                                    
 Output      :  The Get Low Lev Routine Take the Indices &                                                     
                store the Value requested in the Return val.                                                   
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                   
****************************************************************************/
INT1
wsscfgGetDot11ShortGIOptionInTwentyImplemented (INT4 i4IfIndex,
                                                INT4
                                                *pi4RetValDot11ShortGIOptionInTwentyImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppShortGi20 = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11ShortGIOptionInTwentyImplemented
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppShortGi20;
    return OSIX_SUCCESS;
}

/****************************************************************************                                  
 Function    :  WSS_GET_RADIO_IF_DBwsscfgGetDot11ShortGIOptionInTwentyActivated                                   
 Input       :  The Indices                                                                                    
                IfIndex                                                                                        
                                                                                                               
                The Object                                                                                     
                retValDot11ShortGIOptionInTwentyActivated                                                      
 Output      :  The Get Low Lev Routine Take the Indices &                                                     
                store the Value requested in the Return val.                                                   
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                   
****************************************************************************/
INT1
wsscfgGetDot11ShortGIOptionInTwentyActivated (INT4 i4IfIndex,
                                              INT4
                                              *pi4RetValDot11ShortGIOptionInTwentyActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bShortGi20 = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N,
                                  &RadioIfGetDB) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11ShortGIOptionInTwentyActivated =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1ShortGi20;
    return OSIX_SUCCESS;
}

/****************************************************************************                                  
 Function    :  wsscfgGetDot11ShortGIOptionInFortyImplemented                                                     
 Input       :  The Indices                                                                                    
                IfIndex                                                                                        
                                                                                                               
                The Object                                                                                     
                retValDot11ShortGIOptionInFortyImplemented                                                     
 Output      :  The Get Low Lev Routine Take the Indices &                                                     
                store the Value requested in the Return val.                                                   
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                   
****************************************************************************/
INT1
wsscfgGetDot11ShortGIOptionInFortyImplemented (INT4 i4IfIndex,
                                               INT4
                                               *pi4RetValDot11ShortGIOptionInFortyImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppShortGi40 = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11ShortGIOptionInFortyImplemented
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppShortGi40;
    return OSIX_SUCCESS;
}

/****************************************************************************                                  
 Function    :  wsscfgGetDot11ShortGIOptionInFortyActivated                                                       
 Input       :  The Indices                                                                                    
                IfIndex                                                                                        
                                                                                                               
                The Object                                                                                     
                retValDot11ShortGIOptionInFortyActivated                                                       
 Output      :  The Get Low Lev Routine Take the Indices &                                                     
                store the Value requested in the Return val.                                                   
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                   
****************************************************************************/
INT1
wsscfgGetDot11ShortGIOptionInFortyActivated (INT4 i4IfIndex,
                                             INT4
                                             *pi4RetValDot11ShortGIOptionInFortyActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bShortGi40 = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11ShortGIOptionInFortyActivated =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1ShortGi40;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11TxSTBCOptionImplemented                                                              
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11TxSTBCOptionImplemented                                                              
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                   
****************************************************************************/
INT1
wsscfgGetDot11TxSTBCOptionImplemented (INT4 i4IfIndex,
                                       INT4
                                       *pi4RetValDot11TxSTBCOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppTxStbc = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11TxSTBCOptionImplemented =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppTxStbc;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11TxSTBCOptionActivated                                                                
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11TxSTBCOptionActivated                                                                
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11TxSTBCOptionActivated (INT4 i4IfIndex,
                                     INT4 *pi4RetValDot11TxSTBCOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bTxStbc = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11TxSTBCOptionActivated =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1TxStbc;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11RxSTBCOptionImplemented                                                              
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11RxSTBCOptionImplemented                                                              
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11RxSTBCOptionImplemented (INT4 i4IfIndex,
                                       INT4
                                       *pi4RetValDot11RxSTBCOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppRxStbc = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11RxSTBCOptionImplemented =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppRxStbc;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11RxSTBCOptionActivated                                                                
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11RxSTBCOptionActivated                                                                
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11RxSTBCOptionActivated (INT4 i4IfIndex,
                                     INT4 *pi4RetValDot11RxSTBCOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRxStbc = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11RxSTBCOptionActivated =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1RxStbc;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11NDelayedBlockAckOptionImplemented                                                    
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11NDelayedBlockAckOptionImplemented                                                    
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11NDelayedBlockAckOptionImplemented (INT4 i4IfIndex,
                                                 INT4
                                                 *pi4RetValDot11NDelayedBlockAckOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppDelayedBlockack = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11NDelayedBlockAckOptionImplemented
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
        u1SuppDelayedBlockack;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigDelayedBlockAckOptionActivated                                              
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigDelayedBlockAckOptionActivated                                              
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigDelayedBlockAckOptionActivated (INT4 i4IfIndex,
                                                       INT4
                                                       *pi4RetValFsDot11nConfigDelayedBlockAckOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bDelayedBlockack = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    *pi4RetValFsDot11nConfigDelayedBlockAckOptionActivated
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1DelayedBlockack;

    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11MaxAMSDULength                                                                       
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11MaxAMSDULength                                                                       
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11MaxAMSDULength (INT4 i4IfIndex,
                              INT4 *pi4RetValDot11MaxAMSDULength)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppMaxAmsduLen = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11MaxAMSDULength
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppMaxAmsduLen;

    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigMaxAMSDULengthConfig                                                        
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigMaxAMSDULengthConfig                                                        
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigMaxAMSDULengthConfig (INT4 i4IfIndex,
                                             INT4
                                             *pi4RetValFsDot11nConfigMaxAMSDULengthConfig)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bMaxAmsduLen = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigMaxAMSDULengthConfig
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1MaxAmsduLen;

    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigtHTDSSCCKModein40MHz                                                        
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigtHTDSSCCKModein40MHz                                                        
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigtHTDSSCCKModein40MHz (INT4 i4IfIndex,
                                             INT4
                                             *pi4RetValFsDot11nConfigtHTDSSCCKModein40MHz)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppDsssCck40 = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigtHTDSSCCKModein40MHz
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppDsssCck40;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigtHTDSSCCKModein40MHzConfig                                                  
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigtHTDSSCCKModein40MHzConfig                                                  
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigtHTDSSCCKModein40MHzConfig (INT4 i4IfIndex,
                                                   INT4
                                                   *pi4RetValFsDot11nConfigtHTDSSCCKModein40MHzConfig)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bDsssCck40 = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigtHTDSSCCKModein40MHzConfig
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1DsssCck40;

    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11FortyMHzIntolerant                                                                   
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11FortyMHzIntolerant                                                                   
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11FortyMHzIntolerant (INT4 i4IfIndex,
                                  INT4 *pi4RetValDot11FortyMHzIntolerant)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bFortyInto = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11FortyMHzIntolerant
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1FortyInto;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11LsigTxopProtectionOptionImplemented                                                  
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11LsigTxopProtectionOptionImplemented                                                  
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11LsigTxopProtectionOptionImplemented (INT4 i4IfIndex,
                                                   INT4
                                                   *pi4RetValDot11LsigTxopProtectionOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppLsigTxopProtFulSup = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11LsigTxopProtectionOptionImplemented
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
        u1SuppLsigTxopProtFulSup;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11LSIGTXOPFullProtectionActivated                                                      
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11LSIGTXOPFullProtectionActivated                                                      
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11LSIGTXOPFullProtectionActivated (INT4 i4IfIndex,
                                               INT4
                                               *pi4RetValDot11LSIGTXOPFullProtectionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bLsigTxopProtFulSup = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11LSIGTXOPFullProtectionActivated
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.
        u1LsigTxopProtFulSup;

    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11MaxRxAMPDUFactor                                                                     
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11MaxRxAMPDUFactor                                                                     
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11MaxRxAMPDUFactor (INT4 i4IfIndex,
                                UINT4 *pu4RetValDot11MaxRxAMPDUFactor)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppMaxAmpduLen = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4RetValDot11MaxRxAMPDUFactor
        =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NampduParams.
        u1SuppMaxAmpduLen;

    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigMaxRxAMPDUFactorConfig                                                      
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigMaxRxAMPDUFactorConfig                                                      
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigMaxRxAMPDUFactorConfig (INT4 i4IfIndex,
                                               UINT4
                                               *pu4RetValFsDot11nConfigMaxRxAMPDUFactorConfig)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bMaxAmpduLen = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4RetValFsDot11nConfigMaxRxAMPDUFactorConfig
        = (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NampduParams.u1MaxAmpduLen;

    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11MinimumMPDUStartSpacing                                                              
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11MinimumMPDUStartSpacing                                                              
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11MinimumMPDUStartSpacing (INT4 i4IfIndex,
                                       UINT4
                                       *pu4RetValDot11MinimumMPDUStartSpacing)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppMinAmpduStart = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4RetValDot11MinimumMPDUStartSpacing
        =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NampduParams.
        u1SuppMinAmpduStart;

    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigMinimumMPDUStartSpacingConfig                                               
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigMinimumMPDUStartSpacingConfig                                               
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigMinimumMPDUStartSpacingConfig (INT4 i4IfIndex,
                                                      UINT4
                                                      *pu4RetValFsDot11nConfigMinimumMPDUStartSpacingConfig)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bMinAmpduStart = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4RetValFsDot11nConfigMinimumMPDUStartSpacingConfig
        =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NampduParams.u1MinAmpduStart;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11PCOOptionImplemented                                                                 
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11PCOOptionImplemented                                                                 
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11PCOOptionImplemented (INT4 i4IfIndex,
                                    INT4 *pi4RetValDot11PCOOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppPco = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11PCOOptionImplemented
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u1SuppPco;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigPCOOptionActivated                                                          
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigPCOOptionActivated                                                          
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigPCOOptionActivated (INT4 i4IfIndex,
                                           INT4
                                           *pi4RetValFsDot11nConfigPCOOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bPco = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigPCOOptionActivated
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u1Pco;

    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11TransitionTime                                                                       
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11TransitionTime                                                                       
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11TransitionTime (INT4 i4IfIndex,
                              UINT4 *pu4RetValDot11TransitionTime)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppPcoTransTime = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4RetValDot11TransitionTime
        =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.
        u1SuppPcoTransTime;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigTransitionTimeConfig                                                        
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigTransitionTimeConfig                                                        
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigTransitionTimeConfig (INT4 i4IfIndex,
                                             UINT4
                                             *pu4RetValFsDot11nConfigTransitionTimeConfig)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bPcoTransTime = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4RetValFsDot11nConfigTransitionTimeConfig
        =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.
        u1PcoTransTime;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11MCSFeedbackOptionImplemented                                                         
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11MCSFeedbackOptionImplemented                                                         
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11MCSFeedbackOptionImplemented (INT4 i4IfIndex,
                                            INT4
                                            *pi4RetValDot11MCSFeedbackOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppMcsFeedback = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11MCSFeedbackOptionImplemented
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.
        u1SuppMcsFeedback;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigMCSFeedbackOptionActivated                                                  
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigMCSFeedbackOptionActivated                                                  
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigMCSFeedbackOptionActivated (INT4 i4IfIndex,
                                                   INT4
                                                   *pi4RetValFsDot11nConfigMCSFeedbackOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bMcsFeedback = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigMCSFeedbackOptionActivated
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u1McsFeedback;
    return OSIX_SUCCESS;

}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11HTControlFieldSupported                                                              
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11HTControlFieldSupported                                                              
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11HTControlFieldSupported (INT4 i4IfIndex,
                                       INT4
                                       *pi4RetValDot11HTControlFieldSupported)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppHtcSupp = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11HTControlFieldSupported
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u1SuppHtcSupp;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigHTControlFieldSupported                                                     
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigHTControlFieldSupported                                                     
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigHTControlFieldSupported (INT4 i4IfIndex,
                                                INT4
                                                *pi4RetValFsDot11nConfigHTControlFieldSupported)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bHtcSupp = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigHTControlFieldSupported
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u1HtcSupp;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11RDResponderOptionImplemented                                                         
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11RDResponderOptionImplemented                                                         
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11RDResponderOptionImplemented (INT4 i4IfIndex,
                                            INT4
                                            *pi4RetValDot11RDResponderOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppRdResponder = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11RDResponderOptionImplemented
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.
        u1SuppRdResponder;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigRDResponderOptionActivated                                                  
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigRDResponderOptionActivated                                                  
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigRDResponderOptionActivated (INT4 i4IfIndex,
                                                   INT4
                                                   *pi4RetValFsDot11nConfigRDResponderOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRdResponder = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigRDResponderOptionActivated
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u1RdResponder;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgGetFsDot11nConfigImplicitTransmitBeamformingRecvOptionImplemented
 Input       :  The Indices
                IfIndex

                The Object
                retValFsDot11nConfigImplicitTransmitBeamformingRecvOptionImplemented
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigImplicitTransmitBeamformingRecvOptionImplemented (INT4
                                                                         i4IfIndex,
                                                                         INT4
                                                                         *pi4RetValFsDot11nConfigImplicitTransmitBeamformingRecvOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppImpTranBeamRecCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigImplicitTransmitBeamformingRecvOptionImplemented
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1SuppImpTranBeamRecCapable;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgGetFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                retValFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated                                                                                       Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated (INT4
                                                                       i4IfIndex,
                                                                       INT4
                                                                       *pi4RetValFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bImpTranBeamRecCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1ImpTranBeamRecCapable;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11ReceiveStaggerSoundingOptionImplemented                                              
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11ReceiveStaggerSoundingOptionImplemented                                              
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11ReceiveStaggerSoundingOptionImplemented (INT4 i4IfIndex,
                                                       INT4
                                                       *pi4RetValDot11ReceiveStaggerSoundingOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppRecStagSoundCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11ReceiveStaggerSoundingOptionImplemented
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1SuppRecStagSoundCapable;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigReceiveStaggerSoundingOptionActivated                                       
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigReceiveStaggerSoundingOptionActivated                                       
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigReceiveStaggerSoundingOptionActivated (INT4 i4IfIndex,
                                                              INT4
                                                              *pi4RetValFsDot11nConfigReceiveStaggerSoundingOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRecStagSoundCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigReceiveStaggerSoundingOptionActivated
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1RecStagSoundCapable;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgGetDot11TransmitStaggerSoundingOptionImplemented
 Input       :  The Indices
                IfIndex

                The Object
                retValDot11TransmitStaggerSoundingOptionImplemented
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgGetDot11TransmitStaggerSoundingOptionImplemented (INT4 i4IfIndex,
                                                        INT4
                                                        *pi4RetValDot11TransmitStaggerSoundingOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppTransStagSoundCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11TransmitStaggerSoundingOptionImplemented
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1SuppTransStagSoundCapable;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigTransmitStaggerSoundingOptionActivated                                      
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigTransmitStaggerSoundingOptionActivated                                      
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigTransmitStaggerSoundingOptionActivated (INT4 i4IfIndex,
                                                               INT4
                                                               *pi4RetValFsDot11nConfigTransmitStaggerSoundingOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bTransStagSoundCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigTransmitStaggerSoundingOptionActivated
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1TransStagSoundCapable;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11ReceiveNDPOptionImplemented                                                          
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11ReceiveNDPOptionImplemented                                                          
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11ReceiveNDPOptionImplemented (INT4 i4IfIndex,
                                           INT4
                                           *pi4RetValDot11ReceiveNDPOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppRecNdpCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11ReceiveNDPOptionImplemented
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1SuppRecNdpCapable;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigReceiveNDPOptionActivated                                                   
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigReceiveNDPOptionActivated                                                   
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigReceiveNDPOptionActivated (INT4 i4IfIndex,
                                                  INT4
                                                  *pi4RetValFsDot11nConfigReceiveNDPOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRecNdpCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigReceiveNDPOptionActivated
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1RecNdpCapable;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11TransmitNDPOptionImplemented                                                         
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11TransmitNDPOptionImplemented                                                         
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11TransmitNDPOptionImplemented (INT4 i4IfIndex,
                                            INT4
                                            *pi4RetValDot11TransmitNDPOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppTransNdpCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11TransmitNDPOptionImplemented
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1SuppTransNdpCapable;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigTransmitNDPOptionActivated                                                  
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigTransmitNDPOptionActivated                                                  
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigTransmitNDPOptionActivated (INT4 i4IfIndex,
                                                   INT4
                                                   *pi4RetValFsDot11nConfigTransmitNDPOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bTransNdpCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigTransmitNDPOptionActivated
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1TransNdpCapable;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11ImplicitTransmitBeamformingOptionImplemented                                         
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11ImplicitTransmitBeamformingOptionImplemented                                         
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11ImplicitTransmitBeamformingOptionImplemented (INT4 i4IfIndex,
                                                            INT4
                                                            *pi4RetValDot11ImplicitTransmitBeamformingOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppImpTranBeamCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11ImplicitTransmitBeamformingOptionImplemented
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1SuppImpTranBeamCapable;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigImplicitTransmitBeamformingOptionActivated                                  
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigImplicitTransmitBeamformingOptionActivated                                  
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigImplicitTransmitBeamformingOptionActivated (INT4
                                                                   i4IfIndex,
                                                                   INT4
                                                                   *pi4RetValFsDot11nConfigImplicitTransmitBeamformingOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bImpTranBeamCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigImplicitTransmitBeamformingOptionActivated
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1ImpTranBeamCapable;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11CalibrationOptionImplemented                                                         
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11CalibrationOptionImplemented                                                         
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11CalibrationOptionImplemented (INT4 i4IfIndex,
                                            INT4
                                            *pi4RetValDot11CalibrationOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppCalibration = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11CalibrationOptionImplemented
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1SuppCalibration;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigCalibrationOptionActivated                                                  
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigCalibrationOptionActivated                                                  
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigCalibrationOptionActivated (INT4 i4IfIndex,
                                                   INT4
                                                   *pi4RetValFsDot11nConfigCalibrationOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bCalibration = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigCalibrationOptionActivated
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1Calibration;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11ExplicitCSITransmitBeamformingOptionImplemented                                      
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11ExplicitCSITransmitBeamformingOptionImplemented                                      
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11ExplicitCSITransmitBeamformingOptionImplemented (INT4 i4IfIndex,
                                                               INT4
                                                               *pi4RetValDot11ExplicitCSITransmitBeamformingOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppExplCsiTranBeamCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11ExplicitCSITransmitBeamformingOptionImplemented
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1SuppExplCsiTranBeamCapable;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated                               
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated                               
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated (INT4
                                                                      i4IfIndex,
                                                                      INT4
                                                                      *pi4RetValFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bExplCsiTranBeamCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1ExplCsiTranBeamCapable;
    return OSIX_SUCCESS;

}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11ExplicitNonCompressedBeamformingMatrixOptionImplemented                              
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11ExplicitNonCompressedBeamformingMatrixOptionImplemented                              
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11ExplicitNonCompressedBeamformingMatrixOptionImplemented (INT4
                                                                       i4IfIndex,
                                                                       INT4
                                                                       *pi4RetValDot11ExplicitNonCompressedBeamformingMatrixOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppExplNoncompSteCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11ExplicitNonCompressedBeamformingMatrixOptionImplemented
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1SuppExplNoncompSteCapable;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated                          
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated                          
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated (INT4
                                                                           i4IfIndex,
                                                                           INT4
                                                                           *pi4RetValFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bExplNoncompSteCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1ExplNoncompSteCapable;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgGetFsDot11nConfigExplicitCompressedBeamformingMatrixOptImplemented
 Input       :  The Indices
                IfIndex

                The Object
                retValFsDot11nConfigExplicitCompressedBeamformingMatrixOptImplemented
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigExplicitCompressedBeamformingMatrixOptImplemented (INT4
                                                                          i4IfIndex,
                                                                          INT4
                                                                          *pi4RetValFsDot11nConfigExplicitCompressedBeamformingMatrixOptImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppExplCompSteCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigExplicitCompressedBeamformingMatrixOptImplemented
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1SuppExplCompSteCapable;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated                          
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated                          
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated (INT4
                                                                           i4IfIndex,
                                                                           INT4
                                                                           *pi4RetValFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bExplCompSteCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1ExplCompSteCapable;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11ExplicitTransmitBeamformingCSIFeedbackOptionImplemented                              
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11ExplicitTransmitBeamformingCSIFeedbackOptionImplemented                              
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11ExplicitTransmitBeamformingCSIFeedbackOptionImplemented (INT4
                                                                       i4IfIndex,
                                                                       INT4
                                                                       *pi4RetValDot11ExplicitTransmitBeamformingCSIFeedbackOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppExplTranBeamCsiFeedback = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11ExplicitTransmitBeamformingCSIFeedbackOptionImplemented
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1SuppExplTranBeamCsiFeedback;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated                          
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated                          
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated (INT4
                                                                           i4IfIndex,
                                                                           INT4
                                                                           *pi4RetValFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bExplTranBeamCsiFeedback = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1ExplTranBeamCsiFeedback;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11ExplicitNonCompressedBeamformingFeedbackOptionImplemented                            
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11ExplicitNonCompressedBeamformingFeedbackOptionImplemented                            
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11ExplicitNonCompressedBeamformingFeedbackOptionImplemented (INT4
                                                                         i4IfIndex,
                                                                         INT4
                                                                         *pi4RetValDot11ExplicitNonCompressedBeamformingFeedbackOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppExplNoncompBeamFbCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11ExplicitNonCompressedBeamformingFeedbackOptionImplemented
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1SuppExplNoncompBeamFbCapable;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated                              
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated                              
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated (INT4
                                                                       i4IfIndex,
                                                                       INT4
                                                                       *pi4RetValFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bExplNoncompBeamFbCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1ExplNoncompBeamFbCapable;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11ExplicitCompressedBeamformingFeedbackOptionImplemented                               
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11ExplicitCompressedBeamformingFeedbackOptionImplemented                               
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11ExplicitCompressedBeamformingFeedbackOptionImplemented (INT4
                                                                      i4IfIndex,
                                                                      INT4
                                                                      *pi4RetValDot11ExplicitCompressedBeamformingFeedbackOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppExplCompBeamFbCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11ExplicitCompressedBeamformingFeedbackOptionImplemented
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1SuppExplCompBeamFbCapable;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated                           
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated                           
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated (INT4
                                                                          i4IfIndex,
                                                                          INT4
                                                                          *pi4RetValFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bExplCompBeamFbCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1ExplCompBeamFbCapable;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigMinimalGroupingImplemented                                                  
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigMinimalGroupingImplemented                                                  
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigMinimalGroupingImplemented (INT4 i4IfIndex,
                                                   UINT4
                                                   *pu4RetValFsDot11nConfigMinimalGroupingImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppMinGrouping = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4RetValFsDot11nConfigMinimalGroupingImplemented
        =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1SuppMinGrouping;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigMinimalGroupingActivated                                                    
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigMinimalGroupingActivated                                                    
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigMinimalGroupingActivated (INT4 i4IfIndex,
                                                 UINT4
                                                 *pu4RetValFsDot11nConfigMinimalGroupingActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bMinGrouping = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4RetValFsDot11nConfigMinimalGroupingActivated
        =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1MinGrouping;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11NumberBeamFormingCSISupportAntenna                                                   
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11NumberBeamFormingCSISupportAntenna                                                   
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgGetDot11NumberBeamFormingCSISupportAntenna (INT4 i4IfIndex,
                                                  UINT4
                                                  *pu4RetValDot11NumberBeamFormingCSISupportAntenna)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppCsiNoofBeamAntSupported = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4RetValDot11NumberBeamFormingCSISupportAntenna
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1SuppCsiNoofBeamAntSupported;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigNumberBeamFormingCSISupportAntenna                                          
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigNumberBeamFormingCSISupportAntenna                                          
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigNumberBeamFormingCSISupportAntenna (INT4 i4IfIndex,
                                                           UINT4
                                                           *pu4RetValFsDot11nConfigNumberBeamFormingCSISupportAntenna)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bCsiNoofBeamAntSupported = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4RetValFsDot11nConfigNumberBeamFormingCSISupportAntenna
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1CsiNoofBeamAntSupported;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11NumberNonCompressedBeamformingMatrixSupportAntenna                                   
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11NumberNonCompressedBeamformingMatrixSupportAntenna                                   
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11NumberNonCompressedBeamformingMatrixSupportAntenna (INT4
                                                                  i4IfIndex,
                                                                  UINT4
                                                                  *pu4RetValDot11NumberNonCompressedBeamformingMatrixSupportAntenna)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppNoncompSteNoBeamformer = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4RetValDot11NumberNonCompressedBeamformingMatrixSupportAntenna
        =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1SuppNoncompSteNoBeamformer;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna                           
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna                          
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna (INT4
                                                                           i4IfIndex,
                                                                           UINT4
                                                                           *pu4RetValFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bNoncompSteNoBeamformer = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4RetValFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna
        =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1NoncompSteNoBeamformerAntSupp;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11NumberCompressedBeamformingMatrixSupportAntenna                                      
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11NumberCompressedBeamformingMatrixSupportAntenna                                      
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11NumberCompressedBeamformingMatrixSupportAntenna (INT4 i4IfIndex,
                                                               UINT4
                                                               *pu4RetValDot11NumberCompressedBeamformingMatrixSupportAntenna)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppCompSteNoBeamformer = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4RetValDot11NumberCompressedBeamformingMatrixSupportAntenna
        =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1SuppCompSteNoBeamformerAntSupp;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna                             
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna                             
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna (INT4
                                                                        i4IfIndex,
                                                                        UINT4
                                                                        *pu4RetValFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bCompSteNoBeamformer = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4RetValFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna
        =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1CompSteNoBeamformerAntSupp;
    return OSIX_SUCCESS;

}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaImplemented                            
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaImplemented                            
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaImplemented (INT4
                                                                         i4IfIndex,
                                                                         UINT4
                                                                         *pu4RetValFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppCSIMaxNoRowsBeamformer = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4RetValFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaImplemented
        =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1SuppCSIMaxNoRowsBeamformerSupp;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated                              
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated                              
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated (INT4
                                                                       i4IfIndex,
                                                                       UINT4
                                                                       *pu4RetValFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bCSIMaxNoRowsBeamformer = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4RetValFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated
        =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1CSIMaxNoRowsBeamformerSupp;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigChannelEstimationCapabilityImplemented                                      
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigChannelEstimationCapabilityImplemented                                      
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigChannelEstimationCapabilityImplemented (INT4 i4IfIndex,
                                                               UINT4
                                                               *pu4RetValFsDot11nConfigChannelEstimationCapabilityImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppChannelEstCapability = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4RetValFsDot11nConfigChannelEstimationCapabilityImplemented =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1SuppChannelEstCapability;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigChannelEstimationCapabilityActivated                                        
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigChannelEstimationCapabilityActivated                                        
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigChannelEstimationCapabilityActivated (INT4 i4IfIndex,
                                                             UINT4
                                                             *pu4RetValFsDot11nConfigChannelEstimationCapabilityActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bChannelEstCapability = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4RetValFsDot11nConfigChannelEstimationCapabilityActivated
        =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1ChannelEstCapability;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11CurrentPrimaryChannel                                                                
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11CurrentPrimaryChannel                                                                
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11CurrentPrimaryChannel (INT4 i4IfIndex,
                                     UINT4 *pu4RetValDot11CurrentPrimaryChannel)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppPrimaryChannel = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4RetValDot11CurrentPrimaryChannel
        =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
        u1SuppPrimaryChannel;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigCurrentPrimaryChannel                                                       
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigCurrentPrimaryChannel                                                       
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigCurrentPrimaryChannel (INT4 i4IfIndex,
                                              UINT4
                                              *pu4RetValFsDot11nConfigCurrentPrimaryChannel)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bPrimaryChannel = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4RetValFsDot11nConfigCurrentPrimaryChannel
        =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1PrimaryChannel;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11CurrentSecondaryChannel                                                              
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11CurrentSecondaryChannel                                                              
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11CurrentSecondaryChannel (INT4 i4IfIndex,
                                       UINT4
                                       *pu4RetValDot11CurrentSecondaryChannel)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppSecondaryChannel = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4RetValDot11CurrentSecondaryChannel
        =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
        u1SuppSecondaryChannel;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigCurrentSecondaryChannel                                                     
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigCurrentSecondaryChannel                                                     
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigCurrentSecondaryChannel (INT4 i4IfIndex,
                                                UINT4
                                                *pu4RetValFsDot11nConfigCurrentSecondaryChannel)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSecondaryChannel = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4RetValFsDot11nConfigCurrentSecondaryChannel
        =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
        u1SecondaryChannel;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigSTAChannelWidth                                                             
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigSTAChannelWidth                                                             
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigSTAChannelWidth (INT4 i4IfIndex,
                                        INT4
                                        *pi4RetValFsDot11nConfigSTAChannelWidth)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppStaChanWidth = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigSTAChannelWidth
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
        u1SuppStaChanWidth;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigSTAChannelWidthConfig                                                       
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigSTAChannelWidthConfig                                                       
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigSTAChannelWidthConfig (INT4 i4IfIndex,
                                              INT4
                                              *pi4RetValFsDot11nConfigSTAChannelWidthConfig)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bStaChanWidth = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigSTAChannelWidthConfig
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1StaChanWidth;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgGetDot11RIFSMode
 Input       :  The Indices
                IfIndex

                The Object
                retValDot11RIFSMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgGetDot11RIFSMode (INT4 i4IfIndex, INT4 *pi4RetValDot11RIFSMode)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRifsMode = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11RIFSMode
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1RifsMode;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11HTProtection                                                                         
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11HTProtection                                                                         
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11HTProtection (INT4 i4IfIndex, INT4 *pi4RetValDot11HTProtection)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bHtProtection = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11HTProtection
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1HtProtection;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigNonGreenfieldHTSTAsPresent                                                  
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigNonGreenfieldHTSTAsPresent                                                  
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigNonGreenfieldHTSTAsPresent (INT4 i4IfIndex,
                                                   INT4
                                                   *pi4RetValFsDot11nConfigNonGreenfieldHTSTAsPresent)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppNongfHtStasPresent = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigNonGreenfieldHTSTAsPresent
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
        u1SuppNongfHtStasPresent;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigNonGreenfieldHTSTAsPresentConfig                                            
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigNonGreenfieldHTSTAsPresentConfig                                            
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigNonGreenfieldHTSTAsPresentConfig (INT4 i4IfIndex,
                                                         INT4
                                                         *pi4RetValFsDot11nConfigNonGreenfieldHTSTAsPresentConfig)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bNongfHtStasPresent = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigNonGreenfieldHTSTAsPresentConfig
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
        u1NongfHtStasPresent;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigOBSSNonHTSTAsPresent                                                        
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigOBSSNonHTSTAsPresent                                                        
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigOBSSNonHTSTAsPresent (INT4 i4IfIndex,
                                             INT4
                                             *pi4RetValFsDot11nConfigOBSSNonHTSTAsPresent)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppObssNonHtStasPresent = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigOBSSNonHTSTAsPresent
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
        u1SuppObssNonHtStasPresent;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigOBSSNonHTSTAsPresentConfig                                                  
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigOBSSNonHTSTAsPresentConfig                                                  
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigOBSSNonHTSTAsPresentConfig (INT4 i4IfIndex,
                                                   INT4
                                                   *pi4RetValFsDot11nConfigOBSSNonHTSTAsPresentConfig)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bObssNonHtStasPresent = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigOBSSNonHTSTAsPresentConfig
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
        u1ObssNonHtStasPresent;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigDualBeacon                                                                  
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigDualBeacon                                                                  
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigDualBeacon (INT4 i4IfIndex,
                                   INT4 *pi4RetValFsDot11nConfigDualBeacon)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppDualBeacon = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigDualBeacon
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppDualBeacon;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigDualBeaconConfig                                                            
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigDualBeaconConfig                                                            
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigDualBeaconConfig (INT4 i4IfIndex,
                                         INT4
                                         *pi4RetValFsDot11nConfigDualBeaconConfig)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bDualBeacon = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigDualBeaconConfig
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1DualBeacon;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11DualCTSProtection                                                                    
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11DualCTSProtection                                                                    
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11DualCTSProtection (INT4 i4IfIndex,
                                 INT4 *pi4RetValDot11DualCTSProtection)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bDualCtsProtection = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11DualCTSProtection
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
        u1DualCtsProtection;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigSTBCBeacon                                                                  
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigSTBCBeacon                                                                  
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigSTBCBeacon (INT4 i4IfIndex,
                                   INT4 *pi4RetValFsDot11nConfigSTBCBeacon)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppStbcBeacon = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigSTBCBeacon
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppStbcBeacon;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigSTBCBeaconConfig                                                            
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigSTBCBeaconConfig                                                            
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigSTBCBeaconConfig (INT4 i4IfIndex,
                                         INT4
                                         *pi4RetValFsDot11nConfigSTBCBeaconConfig)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bStbcBeacon = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigSTBCBeaconConfig
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1StbcBeacon;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetDot11PCOActivated                                                                         
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValDot11PCOActivated                                                                         
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetDot11PCOActivated (INT4 i4IfIndex, INT4 *pi4RetValDot11PCOActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bPcoActive = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11PCOActivated
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1PcoActive;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigPCOPhase                                                                    
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigPCOPhase                                                                    
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigPCOPhase (INT4 i4IfIndex,
                                 INT4 *pi4RetValFsDot11nConfigPCOPhase)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppPcoPhase = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigPCOPhase
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppPcoPhase;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigPCOPhaseConfig                                                              
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigPCOPhaseConfig                                                              
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigPCOPhaseConfig (INT4 i4IfIndex,
                                       INT4
                                       *pi4RetValFsDot11nConfigPCOPhaseConfig)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bPcoPhase = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigPCOPhaseConfig
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1PcoPhase;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgGetDot11TxMCSSetDefined
 Input       :  The Indices
                IfIndex

                The Object
                retValDot11TxMCSSetDefined
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgGetDot11TxMCSSetDefined (INT4 i4IfIndex,
                               INT4 *pi4RetValDot11TxMCSSetDefined)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bTxMcsSetDefined = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11TxMCSSetDefined =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
        u1TxMcsSetDefined;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgGetDot11TxRxMCSSetNotEqual
 Input       :  The Indices
                IfIndex

                The Object
                retValDot11TxRxMCSSetNotEqual
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgGetDot11TxRxMCSSetNotEqual (INT4 i4IfIndex,
                                  INT4 *pi4RetValDot11TxRxMCSSetNotEqual)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppTxRxMcsSetNotEqual = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValDot11TxRxMCSSetNotEqual =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
        u1SuppTxRxMcsSetNotEqual;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgGetFsDot11nConfigTxRxMCSSetNotEqual
 Input       :  The Indices
                IfIndex

                The Object
                retValFsDot11nConfigTxRxMCSSetNotEqual
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigTxRxMCSSetNotEqual (INT4 i4IfIndex,
                                           INT4
                                           *pi4RetValFsDot11nConfigTxRxMCSSetNotEqual)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bTxRxMcsSetNotEqual = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigTxRxMCSSetNotEqual =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
        u1TxRxMcsSetNotEqual;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigRxSTBCOptionImplemented                                                             
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigRxSTBCOptionImplemented                                                              
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigRxSTBCOptionImplemented (INT4 i4IfIndex,
                                                INT4
                                                *pi4RetValFsDot11nConfigRxSTBCOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppRxStbc = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigRxSTBCOptionImplemented =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppRxStbc;
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgGetFsDot11nConfigRxSTBCOptionActivated                                                               
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                retValFsDot11nConfigRxSTBCOptionActivated                                                                
 Output      :  The Get Low Lev Routine Take the Indices &                                                      
                store the Value requested in the Return val.                                                    
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigRxSTBCOptionActivated (INT4 i4IfIndex,
                                              INT4
                                              *pi4RetValFsDot11nConfigRxSTBCOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRxStbc = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4RetValFsDot11nConfigRxSTBCOptionActivated =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1RxStbc;
    return OSIX_SUCCESS;
}

/******************************SET ROUTINES**************************************/

/****************************************************************************                                   
 Function    :  wsscfgSetDot11LDPCCodingOptionActivated                                                            
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                setValDot11LDPCCodingOptionActivated                                                            
 Output      :  The Set Low Lev Routine Take the Indices &                                                      
                Sets the Value accordingly.                                                                     
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgSetDot11LDPCCodingOptionActivated (INT4 i4IfIndex,
                                         INT4
                                         i4SetValDot11LDPCCodingOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bLdpcCoding = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1LdpcCoding =
        (UINT1) i4SetValDot11LDPCCodingOptionActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgSetFsDot11nConfigMIMOPowerSave                                                               
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                setValFsDot11nConfigMIMOPowerSave                                                               
 Output      :  The Set Low Lev Routine Take the Indices &                                                      
                Sets the Value accordingly.                                                                     
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigMIMOPowerSave (INT4 i4IfIndex,
                                      INT4 i4SetValFsDot11nConfigMIMOPowerSave)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bSmPowerSave = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SmPowerSave =
        i4SetValFsDot11nConfigMIMOPowerSave;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************                                   
 Function    :  wsscfgSetDot11HTGreenfieldOptionActivated                                                          
 Input       :  The Indices                                                                                     
                IfIndex                                                                                         
                                                                                                                
                The Object                                                                                      
                setValDot11HTGreenfieldOptionActivated                                                          
 Output      :  The Set Low Lev Routine Take the Indices &                                                      
                Sets the Value accordingly.                                                                     
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                                    
****************************************************************************/
INT1
wsscfgSetDot11HTGreenfieldOptionActivated (INT4 i4IfIndex,
                                           INT4
                                           i4SetValDot11HTGreenfieldOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bHtGreenField = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1HtGreenField =
        i4SetValDot11HTGreenfieldOptionActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*************************************************************************
 Function    :  wsscfgSetDot11ShortGIOptionInTwentyActivated 
 Input       :  The Indices
                IfIndex

                The Object
                setValDot11ShortGIOptionInTwentyActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetDot11ShortGIOptionInTwentyActivated (INT4 i4IfIndex,
                                              INT4
                                              i4SetValDot11ShortGIOptionInTwentyActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bShortGi20 = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1ShortGi20 =
        i4SetValDot11ShortGIOptionInTwentyActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetDot11ShortGIOptionInFortyActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValDot11ShortGIOptionInFortyActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetDot11ShortGIOptionInFortyActivated (INT4 i4IfIndex,
                                             INT4
                                             i4SetValDot11ShortGIOptionInFortyActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bShortGi40 = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1ShortGi40 =
        (UINT1) i4SetValDot11ShortGIOptionInFortyActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetDot11TxSTBCOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValDot11TxSTBCOptionActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetDot11TxSTBCOptionActivated (INT4 i4IfIndex,
                                     INT4 i4SetValDot11TxSTBCOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bTxStbc = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1TxStbc =
        (UINT1) i4SetValDot11TxSTBCOptionActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetDot11RxSTBCOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValDot11RxSTBCOptionActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetDot11RxSTBCOptionActivated (INT4 i4IfIndex,
                                     INT4 i4SetValDot11RxSTBCOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bRxStbc = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1RxStbc =
        (UINT1) i4SetValDot11RxSTBCOptionActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigDelayedBlockAckOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigDelayedBlockAckOptionActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigDelayedBlockAckOptionActivated (INT4 i4IfIndex,
                                                       INT4
                                                       i4SetValFsDot11nConfigDelayedBlockAckOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bDelayedBlockack = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1DelayedBlockack =
        i4SetValFsDot11nConfigDelayedBlockAckOptionActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigMaxAMSDULengthConfig
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigMaxAMSDULengthConfig
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigMaxAMSDULengthConfig (INT4 i4IfIndex,
                                             INT4
                                             i4SetValFsDot11nConfigMaxAMSDULengthConfig)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bMaxAmsduLen = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1MaxAmsduLen =
        i4SetValFsDot11nConfigMaxAMSDULengthConfig;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigtHTDSSCCKModein40MHzConfig
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigtHTDSSCCKModein40MHzConfig
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigtHTDSSCCKModein40MHzConfig (INT4 i4IfIndex,
                                                   INT4
                                                   i4SetValFsDot11nConfigtHTDSSCCKModein40MHzConfig)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bDsssCck40 = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1DsssCck40 =
        i4SetValFsDot11nConfigtHTDSSCCKModein40MHzConfig;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetDot11FortyMHzIntolerant
 Input       :  The Indices
                IfIndex

                The Object
                setValDot11FortyMHzIntolerant
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetDot11FortyMHzIntolerant (INT4 i4IfIndex,
                                  INT4 i4SetValDot11FortyMHzIntolerant)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bFortyInto = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1FortyInto =
        i4SetValDot11FortyMHzIntolerant;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetDot11LSIGTXOPFullProtectionActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValDot11LSIGTXOPFullProtectionActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetDot11LSIGTXOPFullProtectionActivated (INT4 i4IfIndex,
                                               INT4
                                               i4SetValDot11LSIGTXOPFullProtectionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bLsigTxopProtFulSup = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1LsigTxopProtFulSup =
        i4SetValDot11LSIGTXOPFullProtectionActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigMaxRxAMPDUFactorConfig
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigMaxRxAMPDUFactorConfig
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigMaxRxAMPDUFactorConfig (INT4 i4IfIndex,
                                               UINT4
                                               u4SetValFsDot11nConfigMaxRxAMPDUFactorConfig)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bMaxAmpduLen = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NampduParams.u1MaxAmpduLen =
        u4SetValFsDot11nConfigMaxRxAMPDUFactorConfig;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigMinimumMPDUStartSpacingConfig
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigMinimumMPDUStartSpacingConfig
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigMinimumMPDUStartSpacingConfig (INT4 i4IfIndex,
                                                      UINT4
                                                      u4SetValFsDot11nConfigMinimumMPDUStartSpacingConfig)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bMinAmpduStart = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NampduParams.u1MinAmpduStart =
        (UINT1) u4SetValFsDot11nConfigMinimumMPDUStartSpacingConfig;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigPCOOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigPCOOptionActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigPCOOptionActivated (INT4 i4IfIndex,
                                           INT4
                                           i4SetValFsDot11nConfigPCOOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bPco = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u1Pco =
        i4SetValFsDot11nConfigPCOOptionActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigTransitionTimeConfig
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigTransitionTimeConfig
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigTransitionTimeConfig (INT4 i4IfIndex,
                                             UINT4
                                             u4SetValFsDot11nConfigTransitionTimeConfig)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bPcoTransTime = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u1PcoTransTime =
        u4SetValFsDot11nConfigTransitionTimeConfig;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigMCSFeedbackOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigMCSFeedbackOptionActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigMCSFeedbackOptionActivated (INT4 i4IfIndex,
                                                   INT4
                                                   i4SetValFsDot11nConfigMCSFeedbackOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bMcsFeedback = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u1McsFeedback =
        i4SetValFsDot11nConfigMCSFeedbackOptionActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigHTControlFieldSupported
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigHTControlFieldSupported
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigHTControlFieldSupported (INT4 i4IfIndex,
                                                INT4
                                                i4SetValFsDot11nConfigHTControlFieldSupported)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bHtcSupp = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u1HtcSupp =
        i4SetValFsDot11nConfigHTControlFieldSupported;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigRDResponderOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigRDResponderOptionActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigRDResponderOptionActivated (INT4 i4IfIndex,
                                                   INT4
                                                   i4SetValFsDot11nConfigRDResponderOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bRdResponder = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtExtCapParams.u1RdResponder =
        i4SetValFsDot11nConfigRDResponderOptionActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated (INT4
                                                                       i4IfIndex,
                                                                       INT4
                                                                       i4SetValFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bImpTranBeamRecCapable = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1ImpTranBeamRecCapable =
        i4SetValFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigReceiveStaggerSoundingOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigReceiveStaggerSoundingOptionActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigReceiveStaggerSoundingOptionActivated (INT4 i4IfIndex,
                                                              INT4
                                                              i4SetValFsDot11nConfigReceiveStaggerSoundingOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bRecStagSoundCapable = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.u1RecStagSoundCapable =
        i4SetValFsDot11nConfigReceiveStaggerSoundingOptionActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigTransmitStaggerSoundingOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigTransmitStaggerSoundingOptionActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigTransmitStaggerSoundingOptionActivated (INT4 i4IfIndex,
                                                               INT4
                                                               i4SetValFsDot11nConfigTransmitStaggerSoundingOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bTransStagSoundCapable = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1TransStagSoundCapable =
        i4SetValFsDot11nConfigTransmitStaggerSoundingOptionActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigReceiveNDPOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigReceiveNDPOptionActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigReceiveNDPOptionActivated (INT4 i4IfIndex,
                                                  INT4
                                                  i4SetValFsDot11nConfigReceiveNDPOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bRecNdpCapable = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.u1RecNdpCapable =
        i4SetValFsDot11nConfigReceiveNDPOptionActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigTransmitNDPOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigTransmitNDPOptionActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigTransmitNDPOptionActivated (INT4 i4IfIndex,
                                                   INT4
                                                   i4SetValFsDot11nConfigTransmitNDPOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bTransNdpCapable = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.u1TransNdpCapable =
        i4SetValFsDot11nConfigTransmitNDPOptionActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigImplicitTransmitBeamformingOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigImplicitTransmitBeamformingOptionActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigImplicitTransmitBeamformingOptionActivated (INT4
                                                                   i4IfIndex,
                                                                   INT4
                                                                   i4SetValFsDot11nConfigImplicitTransmitBeamformingOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bImpTranBeamCapable = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.u1ImpTranBeamCapable =
        i4SetValFsDot11nConfigImplicitTransmitBeamformingOptionActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigCalibrationOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigCalibrationOptionActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigCalibrationOptionActivated (INT4 i4IfIndex,
                                                   INT4
                                                   i4SetValFsDot11nConfigCalibrationOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bCalibration = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.u1Calibration =
        i4SetValFsDot11nConfigCalibrationOptionActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated (INT4
                                                                      i4IfIndex,
                                                                      INT4
                                                                      i4SetValFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bExplCsiTranBeamCapable = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1ExplCsiTranBeamCapable =
        i4SetValFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated (INT4
                                                                           i4IfIndex,
                                                                           INT4
                                                                           i4SetValFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bExplNoncompSteCapable = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1ExplNoncompSteCapable =
        i4SetValFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated (INT4
                                                                           i4IfIndex,
                                                                           INT4
                                                                           i4SetValFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bExplCompSteCapable = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.u1ExplCompSteCapable =
        i4SetValFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated (INT4
                                                                           i4IfIndex,
                                                                           INT4
                                                                           i4SetValFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bExplTranBeamCsiFeedback = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1ExplTranBeamCsiFeedback =
        i4SetValFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated (INT4
                                                                       i4IfIndex,
                                                                       INT4
                                                                       i4SetValFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bExplNoncompBeamFbCapable = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1ExplNoncompBeamFbCapable =
        i4SetValFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated (INT4
                                                                          i4IfIndex,
                                                                          INT4
                                                                          i4SetValFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bExplCompBeamFbCapable = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1ExplCompBeamFbCapable =
        i4SetValFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigMinimalGroupingActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigMinimalGroupingActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigMinimalGroupingActivated (INT4 i4IfIndex,
                                                 UINT4
                                                 u4SetValFsDot11nConfigMinimalGroupingActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bMinGrouping = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.u1MinGrouping =
        u4SetValFsDot11nConfigMinimalGroupingActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigNumberBeamFormingCSISupportAntenna
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigNumberBeamFormingCSISupportAntenna
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigNumberBeamFormingCSISupportAntenna (INT4 i4IfIndex,
                                                           UINT4
                                                           u4SetValFsDot11nConfigNumberBeamFormingCSISupportAntenna)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bCsiNoofBeamAntSupported = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1CsiNoofBeamAntSupported =
        u4SetValFsDot11nConfigNumberBeamFormingCSISupportAntenna;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna (INT4
                                                                           i4IfIndex,
                                                                           UINT4
                                                                           u4SetValFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bNoncompSteNoBeamformer = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1NoncompSteNoBeamformerAntSupp =
        u4SetValFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna (INT4
                                                                        i4IfIndex,
                                                                        UINT4
                                                                        u4SetValFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bCompSteNoBeamformer = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1CompSteNoBeamformerAntSupp =
        u4SetValFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated (INT4
                                                                       i4IfIndex,
                                                                       UINT4
                                                                       u4SetValFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bCSIMaxNoRowsBeamformer = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1CSIMaxNoRowsBeamformerSupp =
        u4SetValFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigChannelEstimationCapabilityActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigChannelEstimationCapabilityActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigChannelEstimationCapabilityActivated (INT4 i4IfIndex,
                                                             UINT4
                                                             u4SetValFsDot11nConfigChannelEstimationCapabilityActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bChannelEstCapability = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtTxBeamCapParams.
        u1ChannelEstCapability =
        u4SetValFsDot11nConfigChannelEstimationCapabilityActivated;
    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigCurrentPrimaryChannel
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigCurrentPrimaryChannel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigCurrentPrimaryChannel (INT4 i4IfIndex,
                                              UINT4
                                              u4SetValFsDot11nConfigCurrentPrimaryChannel)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bPrimaryChannel = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1PrimaryChannel =
        u4SetValFsDot11nConfigCurrentPrimaryChannel;

    if (RadioIfSetDot11nOperParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigCurrentSecondaryChannel
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigCurrentSecondaryChannel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigCurrentSecondaryChannel (INT4 i4IfIndex,
                                                UINT4
                                                u4SetValFsDot11nConfigCurrentSecondaryChannel)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bSecondaryChannel = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SecondaryChannel =
        u4SetValFsDot11nConfigCurrentSecondaryChannel;

    if (RadioIfSetDot11nOperParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigSTAChannelWidthConfig
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigSTAChannelWidthConfig
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigSTAChannelWidthConfig (INT4 i4IfIndex,
                                              INT4
                                              i4SetValFsDot11nConfigSTAChannelWidthConfig)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bStaChanWidth = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1StaChanWidth =
        i4SetValFsDot11nConfigSTAChannelWidthConfig;

    if (RadioIfSetDot11nOperParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetDot11RIFSMode
 Input       :  The Indices
                IfIndex

                The Object
                setValDot11RIFSMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetDot11RIFSMode (INT4 i4IfIndex, INT4 i4SetValDot11RIFSMode)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bRifsMode = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1RifsMode =
        i4SetValDot11RIFSMode;

    if (RadioIfSetDot11nOperParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetDot11HTProtection
 Input       :  The Indices
                IfIndex

                The Object
                setValDot11HTProtection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetDot11HTProtection (INT4 i4IfIndex, INT4 i4SetValDot11HTProtection)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bHtProtection = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1HtProtection =
        i4SetValDot11HTProtection;

    if (RadioIfSetDot11nOperParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigNonGreenfieldHTSTAsPresentConfig
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigNonGreenfieldHTSTAsPresentConfig
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigNonGreenfieldHTSTAsPresentConfig (INT4 i4IfIndex,
                                                         INT4
                                                         i4SetValFsDot11nConfigNonGreenfieldHTSTAsPresentConfig)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bNongfHtStasPresent = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1NongfHtStasPresent =
        i4SetValFsDot11nConfigNonGreenfieldHTSTAsPresentConfig;

    if (RadioIfSetDot11nOperParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigOBSSNonHTSTAsPresentConfig
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigOBSSNonHTSTAsPresentConfig
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigOBSSNonHTSTAsPresentConfig (INT4 i4IfIndex,
                                                   INT4
                                                   i4SetValFsDot11nConfigOBSSNonHTSTAsPresentConfig)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bObssNonHtStasPresent = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1ObssNonHtStasPresent =
        i4SetValFsDot11nConfigOBSSNonHTSTAsPresentConfig;

    if (RadioIfSetDot11nOperParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigDualBeaconConfig
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigDualBeaconConfig
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigDualBeaconConfig (INT4 i4IfIndex,
                                         INT4
                                         i4SetValFsDot11nConfigDualBeaconConfig)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bDualBeacon = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1DualBeacon =
        i4SetValFsDot11nConfigDualBeaconConfig;

    if (RadioIfSetDot11nOperParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetDot11DualCTSProtection
 Input       :  The Indices
                IfIndex

                The Object
                setValDot11DualCTSProtection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetDot11DualCTSProtection (INT4 i4IfIndex,
                                 INT4 i4SetValDot11DualCTSProtection)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bDualCtsProtection = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1DualCtsProtection =
        i4SetValDot11DualCTSProtection;

    if (RadioIfSetDot11nOperParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigSTBCBeaconConfig
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigSTBCBeaconConfig
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigSTBCBeaconConfig (INT4 i4IfIndex,
                                         INT4
                                         i4SetValFsDot11nConfigSTBCBeaconConfig)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bStbcBeacon = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1StbcBeacon =
        i4SetValFsDot11nConfigSTBCBeaconConfig;

    if (RadioIfSetDot11nOperParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetDot11PCOActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValDot11PCOActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetDot11PCOActivated (INT4 i4IfIndex, INT4 i4SetValDot11PCOActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bPcoActive = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1PcoActive =
        i4SetValDot11PCOActivated;

    if (RadioIfSetDot11nOperParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigPCOPhaseConfig
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigPCOPhaseConfig
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigPCOPhaseConfig (INT4 i4IfIndex,
                                       INT4
                                       i4SetValFsDot11nConfigPCOPhaseConfig)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bPcoPhase = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1PcoPhase =
        i4SetValFsDot11nConfigPCOPhaseConfig;

    if (RadioIfSetDot11nOperParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetDot11TxMCSSetDefined
 Input       :  The Indices
                IfIndex

                The Object
                setValDot11TxMCSSetDefined
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetDot11TxMCSSetDefined (INT4 i4IfIndex,
                               INT4 i4SetValDot11TxMCSSetDefined)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bTxMcsSetDefined = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.u1TxMcsSetDefined =
        (UINT1) i4SetValDot11TxMCSSetDefined;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigTxRxMCSSetNotEqual
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigTxRxMCSSetNotEqual
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigTxRxMCSSetNotEqual (INT4 i4IfIndex,
                                           INT4
                                           i4SetValFsDot11nConfigTxRxMCSSetNotEqual)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bTxRxMcsSetNotEqual = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.u1TxRxMcsSetNotEqual =
        i4SetValFsDot11nConfigTxRxMCSSetNotEqual;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigRxSTBCOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                SetValFsDot11nConfigRxSTBCOptionActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigRxSTBCOptionActivated (INT4 i4IfIndex,
                                              INT4
                                              i4SetValFsDot11nConfigRxSTBCOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bRxStbc = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1RxStbc =
        (UINT1) i4SetValFsDot11nConfigRxSTBCOptionActivated;

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/**************************TEST ROUTINES************************************/

/****************************************************************************
 Function    :  wsscfgTestv2Dot11LDPCCodingOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValDot11LDPCCodingOptionActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2Dot11LDPCCodingOptionActivated (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                            INT4
                                            i4TestValDot11LDPCCodingOptionActivated)
{
    INT4                i4Dot11LDPCCodingOptionImplemented = 0;

    if ((i4TestValDot11LDPCCodingOptionActivated != 0) &&
        (i4TestValDot11LDPCCodingOptionActivated != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11LDPCCodingOptionImplemented (i4IfIndex,
                                                   &i4Dot11LDPCCodingOptionImplemented)
        == OSIX_SUCCESS)
    {
        if (i4Dot11LDPCCodingOptionImplemented <
            i4TestValDot11LDPCCodingOptionActivated)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigMIMOPowerSave
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigMIMOPowerSave
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigMIMOPowerSave (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                         INT4
                                         i4TestValFsDot11nConfigMIMOPowerSave)
{

    INT4                i4Dot11MIMOPowerSave = 0;

    if ((i4TestValFsDot11nConfigMIMOPowerSave < 0) ||
        (i4TestValFsDot11nConfigMIMOPowerSave > 3))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11MIMOPowerSave (i4IfIndex,
                                     &i4Dot11MIMOPowerSave) == OSIX_SUCCESS)
    {
        if (i4Dot11MIMOPowerSave < i4TestValFsDot11nConfigMIMOPowerSave)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
    CLI_SET_ERR (CLI_11N_POWER_SAVE_ERROR);
    return OSIX_FAILURE;
}

/****************************************************************************
 Function    :  wsscfgTestv2Dot11HTGreenfieldOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValDot11HTGreenfieldOptionActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2Dot11HTGreenfieldOptionActivated (UINT4 *pu4ErrorCode,
                                              INT4 i4IfIndex,
                                              INT4
                                              i4TestValDot11HTGreenfieldOptionActivated)
{
    INT4                i4Dot11HTGreenfieldOptionImplemented = 0;

    if ((i4TestValDot11HTGreenfieldOptionActivated != 0) &&
        (i4TestValDot11HTGreenfieldOptionActivated != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11HTGreenfieldOptionImplemented (i4IfIndex,
                                                     &i4Dot11HTGreenfieldOptionImplemented)
        == OSIX_SUCCESS)
    {
        if ((i4Dot11HTGreenfieldOptionImplemented == 0)
            && (i4TestValDot11HTGreenfieldOptionActivated == 1))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2Dot11ShortGIOptionInTwentyActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValDot11ShortGIOptionInTwentyActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2Dot11ShortGIOptionInTwentyActivated (UINT4 *pu4ErrorCode,
                                                 INT4 i4IfIndex,
                                                 INT4
                                                 i4TestValDot11ShortGIOptionInTwentyActivated)
{
    INT4                i4Dot11ShortGIOptionInTwentyImplemented = 0;

    if ((i4TestValDot11ShortGIOptionInTwentyActivated != 0) &&
        (i4TestValDot11ShortGIOptionInTwentyActivated != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11ShortGIOptionInTwentyImplemented (i4IfIndex,
                                                        &i4Dot11ShortGIOptionInTwentyImplemented)
        == OSIX_SUCCESS)
    {
        if (i4Dot11ShortGIOptionInTwentyImplemented <
            i4TestValDot11ShortGIOptionInTwentyActivated)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2Dot11ShortGIOptionInFortyActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValDot11ShortGIOptionInFortyActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2Dot11ShortGIOptionInFortyActivated (UINT4 *pu4ErrorCode,
                                                INT4 i4IfIndex,
                                                INT4
                                                i4TestValDot11ShortGIOptionInFortyActivated)
{
    INT4                i4Dot11ShortGIOptionInFortyImplemented = 0;

    if ((i4TestValDot11ShortGIOptionInFortyActivated != 0) &&
        (i4TestValDot11ShortGIOptionInFortyActivated != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11ShortGIOptionInFortyImplemented (i4IfIndex,
                                                       &i4Dot11ShortGIOptionInFortyImplemented)
        == OSIX_SUCCESS)
    {
        if (i4Dot11ShortGIOptionInFortyImplemented <
            i4TestValDot11ShortGIOptionInFortyActivated)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2Dot11TxSTBCOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValDot11TxSTBCOptionActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2Dot11TxSTBCOptionActivated (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                        INT4
                                        i4TestValDot11TxSTBCOptionActivated)
{
    INT4                i4Dot11TxSTBCOptionImplemented = 0;

    if ((i4TestValDot11TxSTBCOptionActivated != 0) &&
        (i4TestValDot11TxSTBCOptionActivated != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    if (wsscfgGetDot11TxSTBCOptionImplemented (i4IfIndex,
                                               &i4Dot11TxSTBCOptionImplemented)
        == OSIX_SUCCESS)
    {
        if ((i4Dot11TxSTBCOptionImplemented == 0) &&
            (i4TestValDot11TxSTBCOptionActivated == 1))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2Dot11RxSTBCOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValDot11RxSTBCOptionActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2Dot11RxSTBCOptionActivated (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                        INT4
                                        i4TestValDot11RxSTBCOptionActivated)
{
    INT4                i4Dot11RxSTBCOptionImplemented = 0;

    if ((i4TestValDot11RxSTBCOptionActivated < 0) ||
        (i4TestValDot11RxSTBCOptionActivated > 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    if (wsscfgGetDot11RxSTBCOptionImplemented (i4IfIndex,
                                               &i4Dot11RxSTBCOptionImplemented)
        == OSIX_SUCCESS)
    {
        if ((i4Dot11RxSTBCOptionImplemented < 0) ||
            (i4TestValDot11RxSTBCOptionActivated > 1))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigDelayedBlockAckOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigDelayedBlockAckOptionActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigDelayedBlockAckOptionActivated (UINT4 *pu4ErrorCode,
                                                          INT4 i4IfIndex,
                                                          INT4
                                                          i4TestValFsDot11nConfigDelayedBlockAckOptionActivated)
{
    INT4                i4Dot11NDelayedBlockAckOptionImplemented = 0;

    if ((i4TestValFsDot11nConfigDelayedBlockAckOptionActivated != 0) &&
        (i4TestValFsDot11nConfigDelayedBlockAckOptionActivated != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11NDelayedBlockAckOptionImplemented (i4IfIndex,
                                                         &i4Dot11NDelayedBlockAckOptionImplemented)
        == OSIX_SUCCESS)
    {
        if (i4Dot11NDelayedBlockAckOptionImplemented <
            i4TestValFsDot11nConfigDelayedBlockAckOptionActivated)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigMaxAMSDULengthConfig
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigMaxAMSDULengthConfig
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigMaxAMSDULengthConfig (UINT4 *pu4ErrorCode,
                                                INT4 i4IfIndex,
                                                INT4
                                                i4TestValFsDot11nConfigMaxAMSDULengthConfig)
{
    INT4                i4TestValFsDot11nConfigMaxAMSDULength = 0;

    if ((i4TestValFsDot11nConfigMaxAMSDULengthConfig != 0) &&
        (i4TestValFsDot11nConfigMaxAMSDULengthConfig != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11MaxAMSDULength (i4IfIndex,
                                      &i4TestValFsDot11nConfigMaxAMSDULength) ==
        OSIX_SUCCESS)
    {
        if (i4TestValFsDot11nConfigMaxAMSDULength <
            i4TestValFsDot11nConfigMaxAMSDULengthConfig)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigtHTDSSCCKModein40MHzConfig
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigtHTDSSCCKModein40MHzConfig
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigtHTDSSCCKModein40MHzConfig (UINT4 *pu4ErrorCode,
                                                      INT4 i4IfIndex,
                                                      INT4
                                                      i4TestValFsDot11nConfigtHTDSSCCKModein40MHzConfig)
{
    INT4                i4FsDot11nConfigtHTDSSCCKModein40MHz = 0;

    if ((i4TestValFsDot11nConfigtHTDSSCCKModein40MHzConfig != 0) &&
        (i4TestValFsDot11nConfigtHTDSSCCKModein40MHzConfig != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetFsDot11nConfigtHTDSSCCKModein40MHz (i4IfIndex,
                                                     &i4FsDot11nConfigtHTDSSCCKModein40MHz)
        == OSIX_SUCCESS)
    {
        if (i4FsDot11nConfigtHTDSSCCKModein40MHz <
            i4TestValFsDot11nConfigtHTDSSCCKModein40MHzConfig)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2Dot11FortyMHzIntolerant
 Input       :  The Indices
                IfIndex

                The Object
                testValDot11FortyMHzIntolerant
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2Dot11FortyMHzIntolerant (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                     INT4 i4TestValDot11FortyMHzIntolerant)
{
    INT4                i4TestValDot11FortyMHzIntolerantImplemented = 0;
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if ((i4TestValDot11FortyMHzIntolerant != 0) &&
        (i4TestValDot11FortyMHzIntolerant != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppFortyInto = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    i4TestValDot11FortyMHzIntolerantImplemented
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1SuppFortyInto;

    if (i4TestValDot11FortyMHzIntolerantImplemented <
        i4TestValDot11FortyMHzIntolerant)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2Dot11LSIGTXOPFullProtectionActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValDot11LSIGTXOPFullProtectionActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2Dot11LSIGTXOPFullProtectionActivated (UINT4 *pu4ErrorCode,
                                                  INT4 i4IfIndex,
                                                  INT4
                                                  i4TestValDot11LSIGTXOPFullProtectionActivated)
{
    INT4                i4TestValDot11LSIGTXOPFullProtectionImplemented = 0;
    if ((i4TestValDot11LSIGTXOPFullProtectionActivated != 0) &&
        (i4TestValDot11LSIGTXOPFullProtectionActivated != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    if (wsscfgGetDot11LsigTxopProtectionOptionImplemented (i4IfIndex,
                                                           &i4TestValDot11LSIGTXOPFullProtectionImplemented)
        == OSIX_SUCCESS)
    {
        if (i4TestValDot11LSIGTXOPFullProtectionImplemented <
            i4TestValDot11LSIGTXOPFullProtectionActivated)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigMaxRxAMPDUFactorConfig
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigMaxRxAMPDUFactorConfig
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigMaxRxAMPDUFactorConfig (UINT4 *pu4ErrorCode,
                                                  INT4 i4IfIndex,
                                                  UINT4
                                                  u4TestValFsDot11nConfigMaxRxAMPDUFactorConfig)
{
    UINT4               u4Dot11MaxRxAMPDUFactor = 0;

    if (u4TestValFsDot11nConfigMaxRxAMPDUFactorConfig > 3)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11MaxRxAMPDUFactor (i4IfIndex,
                                        &u4Dot11MaxRxAMPDUFactor) ==
        OSIX_SUCCESS)
    {
        if (u4Dot11MaxRxAMPDUFactor <
            u4TestValFsDot11nConfigMaxRxAMPDUFactorConfig)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigMinimumMPDUStartSpacingConfig
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigMinimumMPDUStartSpacingConfig
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigMinimumMPDUStartSpacingConfig (UINT4 *pu4ErrorCode,
                                                         INT4 i4IfIndex,
                                                         UINT4
                                                         u4TestValFsDot11nConfigMinimumMPDUStartSpacingConfig)
{
    UINT4               u4Dot11MinimumMPDUStartSpacing = 0;

    if (u4TestValFsDot11nConfigMinimumMPDUStartSpacingConfig > 7)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11MinimumMPDUStartSpacing (i4IfIndex,
                                               &u4Dot11MinimumMPDUStartSpacing)
        == OSIX_SUCCESS)
    {
        if (u4Dot11MinimumMPDUStartSpacing <
            u4TestValFsDot11nConfigMinimumMPDUStartSpacingConfig)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigPCOOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigPCOOptionActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigPCOOptionActivated (UINT4 *pu4ErrorCode,
                                              INT4 i4IfIndex,
                                              INT4
                                              i4TestValFsDot11nConfigPCOOptionActivated)
{
    INT4                i4Dot11PCOOptionImplemented = 0;

    if ((i4TestValFsDot11nConfigPCOOptionActivated != 0) &&
        (i4TestValFsDot11nConfigPCOOptionActivated != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11PCOOptionImplemented (i4IfIndex,
                                            &i4Dot11PCOOptionImplemented) ==
        OSIX_SUCCESS)
    {
        if (i4Dot11PCOOptionImplemented <
            i4TestValFsDot11nConfigPCOOptionActivated)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigTransitionTimeConfig
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigTransitionTimeConfig
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigTransitionTimeConfig (UINT4 *pu4ErrorCode,
                                                INT4 i4IfIndex,
                                                UINT4
                                                u4TestValFsDot11nConfigTransitionTimeConfig)
{
    UINT4               u4Dot11TransitionTime = 0;

    if (u4TestValFsDot11nConfigTransitionTimeConfig > 3)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11TransitionTime (i4IfIndex,
                                      &u4Dot11TransitionTime) == OSIX_SUCCESS)
    {
        if (u4Dot11TransitionTime < u4TestValFsDot11nConfigTransitionTimeConfig)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigMCSFeedbackOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigMCSFeedbackOptionActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigMCSFeedbackOptionActivated (UINT4 *pu4ErrorCode,
                                                      INT4 i4IfIndex,
                                                      INT4
                                                      i4TestValFsDot11nConfigMCSFeedbackOptionActivated)
{
    INT4                i4Dot11MCSFeedbackOptionImplemented = 0;

    if ((i4TestValFsDot11nConfigMCSFeedbackOptionActivated < 0) ||
        (i4TestValFsDot11nConfigMCSFeedbackOptionActivated > 3))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11MCSFeedbackOptionImplemented (i4IfIndex,
                                                    &i4Dot11MCSFeedbackOptionImplemented)
        == OSIX_SUCCESS)
    {
        if (i4Dot11MCSFeedbackOptionImplemented <
            i4TestValFsDot11nConfigMCSFeedbackOptionActivated)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigHTControlFieldSupported
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigHTControlFieldSupported
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigHTControlFieldSupported (UINT4 *pu4ErrorCode,
                                                   INT4 i4IfIndex,
                                                   INT4
                                                   i4TestValFsDot11nConfigHTControlFieldSupported)
{
    INT4                i4Dot11HTControlFieldSupported = 0;

    if (i4TestValFsDot11nConfigHTControlFieldSupported > 1 ||
        i4TestValFsDot11nConfigHTControlFieldSupported < 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11HTControlFieldSupported (i4IfIndex,
                                               &i4Dot11HTControlFieldSupported)
        == OSIX_SUCCESS)
    {
        if (i4Dot11HTControlFieldSupported <
            i4TestValFsDot11nConfigHTControlFieldSupported)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigRDResponderOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigRDResponderOptionActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigRDResponderOptionActivated (UINT4 *pu4ErrorCode,
                                                      INT4 i4IfIndex,
                                                      INT4
                                                      i4TestValFsDot11nConfigRDResponderOptionActivated)
{
    INT4                i4Dot11RDResponderOptionImplemented = 0;

    if ((i4TestValFsDot11nConfigRDResponderOptionActivated < 0) ||
        (i4TestValFsDot11nConfigRDResponderOptionActivated > 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11RDResponderOptionImplemented (i4IfIndex,
                                                    &i4Dot11RDResponderOptionImplemented)
        == OSIX_SUCCESS)
    {
        if (i4Dot11RDResponderOptionImplemented <
            i4TestValFsDot11nConfigRDResponderOptionActivated)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated (UINT4
                                                                          *pu4ErrorCode,
                                                                          INT4
                                                                          i4IfIndex,
                                                                          INT4
                                                                          i4TestValFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated)
{
    INT4               
        i4FsDot11nConfigImplicitTransmitBeamformingRecvOptionImplemented = 0;

    if ((i4TestValFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated
         != 0)
        &&
        (i4TestValFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated
         != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetFsDot11nConfigImplicitTransmitBeamformingRecvOptionImplemented
        (i4IfIndex,
         &i4FsDot11nConfigImplicitTransmitBeamformingRecvOptionImplemented) ==
        OSIX_SUCCESS)
    {
        if (i4FsDot11nConfigImplicitTransmitBeamformingRecvOptionImplemented <
            i4TestValFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigReceiveStaggerSoundingOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigReceiveStaggerSoundingOptionActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigReceiveStaggerSoundingOptionActivated (UINT4
                                                                 *pu4ErrorCode,
                                                                 INT4 i4IfIndex,
                                                                 INT4
                                                                 i4TestValFsDot11nConfigReceiveStaggerSoundingOptionActivated)
{
    INT4                i4Dot11ReceiveStaggerSoundingOptionImplemented = 0;

    if ((i4TestValFsDot11nConfigReceiveStaggerSoundingOptionActivated != 0) &&
        (i4TestValFsDot11nConfigReceiveStaggerSoundingOptionActivated != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11ReceiveStaggerSoundingOptionImplemented (i4IfIndex,
                                                               &i4Dot11ReceiveStaggerSoundingOptionImplemented)
        == OSIX_SUCCESS)
    {
        if (i4Dot11ReceiveStaggerSoundingOptionImplemented <
            i4TestValFsDot11nConfigReceiveStaggerSoundingOptionActivated)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigTransmitStaggerSoundingOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigTransmitStaggerSoundingOptionActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigTransmitStaggerSoundingOptionActivated (UINT4
                                                                  *pu4ErrorCode,
                                                                  INT4
                                                                  i4IfIndex,
                                                                  INT4
                                                                  i4TestValFsDot11nConfigTransmitStaggerSoundingOptionActivated)
{
    INT4                i4Dot11TransmitStaggerSoundingOptionImplemented = 0;

    if ((i4TestValFsDot11nConfigTransmitStaggerSoundingOptionActivated != 0) &&
        (i4TestValFsDot11nConfigTransmitStaggerSoundingOptionActivated != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11TransmitStaggerSoundingOptionImplemented (i4IfIndex,
                                                                &i4Dot11TransmitStaggerSoundingOptionImplemented)
        == OSIX_SUCCESS)
    {
        if (i4Dot11TransmitStaggerSoundingOptionImplemented <
            i4TestValFsDot11nConfigTransmitStaggerSoundingOptionActivated)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigReceiveNDPOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigReceiveNDPOptionActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigReceiveNDPOptionActivated (UINT4 *pu4ErrorCode,
                                                     INT4 i4IfIndex,
                                                     INT4
                                                     i4TestValFsDot11nConfigReceiveNDPOptionActivated)
{
    INT4                i4Dot11ReceiveNDPOptionImplemented = 0;

    if ((i4TestValFsDot11nConfigReceiveNDPOptionActivated != 0) &&
        (i4TestValFsDot11nConfigReceiveNDPOptionActivated != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11ReceiveNDPOptionImplemented (i4IfIndex,
                                                   &i4Dot11ReceiveNDPOptionImplemented)
        == OSIX_SUCCESS)
    {
        if ((i4Dot11ReceiveNDPOptionImplemented == 0)
            && (i4TestValFsDot11nConfigReceiveNDPOptionActivated == 1))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigTransmitNDPOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigTransmitNDPOptionActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigTransmitNDPOptionActivated (UINT4 *pu4ErrorCode,
                                                      INT4 i4IfIndex,
                                                      INT4
                                                      i4TestValFsDot11nConfigTransmitNDPOptionActivated)
{
    INT4                i4Dot11TransmitNDPOptionImplemented = 0;

    if ((i4TestValFsDot11nConfigTransmitNDPOptionActivated != 0) &&
        (i4TestValFsDot11nConfigTransmitNDPOptionActivated != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11TransmitNDPOptionImplemented (i4IfIndex,
                                                    &i4Dot11TransmitNDPOptionImplemented)
        == OSIX_SUCCESS)
    {
        if ((i4Dot11TransmitNDPOptionImplemented == 0)
            && (i4TestValFsDot11nConfigTransmitNDPOptionActivated == 1))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigImplicitTransmitBeamformingOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigImplicitTransmitBeamformingOptionActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigImplicitTransmitBeamformingOptionActivated (UINT4
                                                                      *pu4ErrorCode,
                                                                      INT4
                                                                      i4IfIndex,
                                                                      INT4
                                                                      i4TestValFsDot11nConfigImplicitTransmitBeamformingOptionActivated)
{
    INT4                i4Dot11ImplicitTransmitBeamformingOptionImplemented = 0;

    if ((i4TestValFsDot11nConfigImplicitTransmitBeamformingOptionActivated != 0)
        && (i4TestValFsDot11nConfigImplicitTransmitBeamformingOptionActivated !=
            1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11ImplicitTransmitBeamformingOptionImplemented (i4IfIndex,
                                                                    &i4Dot11ImplicitTransmitBeamformingOptionImplemented)
        == OSIX_SUCCESS)
    {
        if ((i4Dot11ImplicitTransmitBeamformingOptionImplemented == 0)
            &&
            (i4TestValFsDot11nConfigImplicitTransmitBeamformingOptionActivated
             == 1))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigCalibrationOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigCalibrationOptionActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigCalibrationOptionActivated (UINT4 *pu4ErrorCode,
                                                      INT4 i4IfIndex,
                                                      INT4
                                                      i4TestValFsDot11nConfigCalibrationOptionActivated)
{
    INT4                i4Dot11CalibrationOptionImplemented = 0;

    if ((i4TestValFsDot11nConfigCalibrationOptionActivated < 0) ||
        (i4TestValFsDot11nConfigCalibrationOptionActivated > 3))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11CalibrationOptionImplemented (i4IfIndex,
                                                    &i4Dot11CalibrationOptionImplemented)
        == OSIX_SUCCESS)
    {
        if (i4Dot11CalibrationOptionImplemented <
            i4TestValFsDot11nConfigCalibrationOptionActivated)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigExplicitCSITransmitBeamformingOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigExplicitCSITransmitBeamformingOptionActivated (UINT4
                                                                         *pu4ErrorCode,
                                                                         INT4
                                                                         i4IfIndex,
                                                                         INT4
                                                                         i4TestValFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated)
{
    INT4                i4Dot11ExplicitCSITransmitBeamformingOptionImplemented =
        0;

    if ((i4TestValFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated !=
         0)
        && (i4TestValFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated
            != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11ExplicitCSITransmitBeamformingOptionImplemented
        (i4IfIndex,
         &i4Dot11ExplicitCSITransmitBeamformingOptionImplemented) ==
        OSIX_SUCCESS)
    {
        if ((i4Dot11ExplicitCSITransmitBeamformingOptionImplemented == 0)
            &&
            (i4TestValFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated
             == 1))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated
    (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
     INT4
     i4TestValFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated)
{
    INT4               
        i4Dot11ExplicitNonCompressedBeamformingMatrixOptionImplemented = 0;

    if ((i4TestValFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated != 0) && (i4TestValFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11ExplicitNonCompressedBeamformingMatrixOptionImplemented
        (i4IfIndex,
         &i4Dot11ExplicitNonCompressedBeamformingMatrixOptionImplemented) ==
        OSIX_SUCCESS)
    {
        if ((i4Dot11ExplicitNonCompressedBeamformingMatrixOptionImplemented ==
             0)
            &&
            (i4TestValFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated
             == 1))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated
    (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
     INT4
     i4TestValFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated)
{
    INT4               
        i4FsDot11nConfigExplicitCompressedBeamformingMatrixOptImplemented = 0;

    if ((i4TestValFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated != 0) && (i4TestValFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetFsDot11nConfigExplicitCompressedBeamformingMatrixOptImplemented
        (i4IfIndex,
         &i4FsDot11nConfigExplicitCompressedBeamformingMatrixOptImplemented) ==
        OSIX_SUCCESS)
    {
        if ((i4FsDot11nConfigExplicitCompressedBeamformingMatrixOptImplemented
             == 0)
            &&
            (i4TestValFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated
             == 1))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated
    (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
     INT4
     i4TestValFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated)
{
    INT4               
        i4Dot11ExplicitTransmitBeamformingCSIFeedbackOptionImplemented = 0;

    if ((i4TestValFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated < 0) || (i4TestValFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated > 3))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11ExplicitTransmitBeamformingCSIFeedbackOptionImplemented
        (i4IfIndex,
         &i4Dot11ExplicitTransmitBeamformingCSIFeedbackOptionImplemented) ==
        OSIX_SUCCESS)
    {
        if (i4Dot11ExplicitTransmitBeamformingCSIFeedbackOptionImplemented <
            i4TestValFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated (UINT4
                                                                          *pu4ErrorCode,
                                                                          INT4
                                                                          i4IfIndex,
                                                                          INT4
                                                                          i4TestValFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated)
{
    INT4               
        i4Dot11ExplicitNonCompressedBeamformingFeedbackOptionImplemented = 0;

    if ((i4TestValFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated <
         0)
        ||
        (i4TestValFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated >
         3))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11ExplicitNonCompressedBeamformingFeedbackOptionImplemented
        (i4IfIndex,
         &i4Dot11ExplicitNonCompressedBeamformingFeedbackOptionImplemented) ==
        OSIX_SUCCESS)
    {
        if (i4Dot11ExplicitNonCompressedBeamformingFeedbackOptionImplemented <
            i4TestValFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated
    (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
     INT4
     i4TestValFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated)
{
    INT4               
        i4Dot11ExplicitCompressedBeamformingFeedbackOptionImplemented = 0;

    if ((i4TestValFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated < 0) || (i4TestValFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated > 3))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11ExplicitCompressedBeamformingFeedbackOptionImplemented
        (i4IfIndex,
         &i4Dot11ExplicitCompressedBeamformingFeedbackOptionImplemented) ==
        OSIX_SUCCESS)
    {
        if (i4Dot11ExplicitCompressedBeamformingFeedbackOptionImplemented <
            i4TestValFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigMinimalGroupingActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigMinimalGroupingActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigMinimalGroupingActivated (UINT4 *pu4ErrorCode,
                                                    INT4 i4IfIndex,
                                                    UINT4
                                                    u4TestValFsDot11nConfigMinimalGroupingActivated)
{
    UINT4               u4FsDot11nConfigMinimalGroupingImplemented = 0;

    if (u4TestValFsDot11nConfigMinimalGroupingActivated > 3)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetFsDot11nConfigMinimalGroupingImplemented (i4IfIndex,
                                                           &u4FsDot11nConfigMinimalGroupingImplemented)
        == OSIX_SUCCESS)
    {
        if (u4FsDot11nConfigMinimalGroupingImplemented <
            u4TestValFsDot11nConfigMinimalGroupingActivated)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigNumberBeamFormingCSISupportAntenna
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigNumberBeamFormingCSISupportAntenna
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigNumberBeamFormingCSISupportAntenna (UINT4
                                                              *pu4ErrorCode,
                                                              INT4 i4IfIndex,
                                                              UINT4
                                                              u4TestValFsDot11nConfigNumberBeamFormingCSISupportAntenna)
{
    UINT4               u4Dot11NumberBeamFormingCSISupportAntenna = 0;

    if (u4TestValFsDot11nConfigNumberBeamFormingCSISupportAntenna > 3)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11NumberBeamFormingCSISupportAntenna (i4IfIndex,
                                                          &u4Dot11NumberBeamFormingCSISupportAntenna)
        == OSIX_SUCCESS)
    {
        if (u4Dot11NumberBeamFormingCSISupportAntenna <
            u4TestValFsDot11nConfigNumberBeamFormingCSISupportAntenna)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna
    (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
     UINT4
     u4TestValFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna)
{
    UINT4              
        u4Dot11NumberNonCompressedBeamformingMatrixSupportAntenna = 0;

    if (u4TestValFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna > 3)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11NumberNonCompressedBeamformingMatrixSupportAntenna
        (i4IfIndex,
         &u4Dot11NumberNonCompressedBeamformingMatrixSupportAntenna) ==
        OSIX_SUCCESS)
    {
        if (u4Dot11NumberNonCompressedBeamformingMatrixSupportAntenna <
            u4TestValFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna (UINT4
                                                                           *pu4ErrorCode,
                                                                           INT4
                                                                           i4IfIndex,
                                                                           UINT4
                                                                           u4TestValFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna)
{
    UINT4               u4Dot11NumberCompressedBeamformingMatrixSupportAntenna =
        0;

    if (u4TestValFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna >
        3)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11NumberCompressedBeamformingMatrixSupportAntenna
        (i4IfIndex,
         &u4Dot11NumberCompressedBeamformingMatrixSupportAntenna) ==
        OSIX_SUCCESS)
    {
        if (u4Dot11NumberCompressedBeamformingMatrixSupportAntenna <
            u4TestValFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated (UINT4
                                                                          *pu4ErrorCode,
                                                                          INT4
                                                                          i4IfIndex,
                                                                          UINT4
                                                                          u4TestValFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated)
{
    UINT4              
        u4FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaImplemented = 0;

    if (u4TestValFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated >
        3)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaImplemented
        (i4IfIndex,
         &u4FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaImplemented) ==
        OSIX_SUCCESS)
    {
        if (u4FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaImplemented <
            u4TestValFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigChannelEstimationCapabilityActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigChannelEstimationCapabilityActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigChannelEstimationCapabilityActivated (UINT4
                                                                *pu4ErrorCode,
                                                                INT4 i4IfIndex,
                                                                UINT4
                                                                u4TestValFsDot11nConfigChannelEstimationCapabilityActivated)
{
    UINT4               u4FsDot11nConfigChannelEstimationCapabilityImplemented =
        0;

    if (u4TestValFsDot11nConfigChannelEstimationCapabilityActivated > 3)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetFsDot11nConfigChannelEstimationCapabilityImplemented
        (i4IfIndex,
         &u4FsDot11nConfigChannelEstimationCapabilityImplemented) ==
        OSIX_SUCCESS)
    {
        if (u4FsDot11nConfigChannelEstimationCapabilityImplemented <
            u4TestValFsDot11nConfigChannelEstimationCapabilityActivated)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigCurrentPrimaryChannel
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigCurrentPrimaryChannel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigCurrentPrimaryChannel (UINT4 *pu4ErrorCode,
                                                 INT4 i4IfIndex,
                                                 UINT4
                                                 u4TestValFsDot11nConfigCurrentPrimaryChannel)
{
#ifdef WLC_WANTED
    UINT4               u4RadioType;
    tSNMP_OCTET_STRING_TYPE TmpChannelList;
    UINT1               u1Loop;
    INT4                iRetVal;
    UINT2               u2Flag = 0;
    UINT1               au1Dot11bChannelList[RFMGMT_MAX_CHANNELA];

    MEMSET (&TmpChannelList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1Dot11bChannelList, 0, RFMGMT_MAX_CHANNELA);

    TmpChannelList.pu1_OctetList = au1Dot11bChannelList;

    if ((u4TestValFsDot11nConfigCurrentPrimaryChannel < 1) ||
        (u4TestValFsDot11nConfigCurrentPrimaryChannel > 200))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    iRetVal = nmhGetFsDot11RadioType (i4IfIndex, &u4RadioType);
    if (iRetVal == SNMP_SUCCESS)
    {
        if (nmhGetFsRrmAllowedChannels (u4RadioType, &TmpChannelList) ==
            SNMP_SUCCESS)
        {
            for (u1Loop = 0; u1Loop < RFMGMT_MAX_CHANNELA; u1Loop++)
            {
                if (TmpChannelList.pu1_OctetList[u1Loop] ==
                    u4TestValFsDot11nConfigCurrentPrimaryChannel)
                {
                    u2Flag = 1;
                    break;
                }
            }
        }
        if (u2Flag == 1)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValFsDot11nConfigCurrentPrimaryChannel);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigCurrentSecondaryChannel
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigCurrentSecondaryChannel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigCurrentSecondaryChannel (UINT4 *pu4ErrorCode,
                                                   INT4 i4IfIndex,
                                                   UINT4
                                                   u4TestValFsDot11nConfigCurrentSecondaryChannel)
{
    UINT4               u4TestCurrentSecondaryChannel = 0;
    if (u4TestValFsDot11nConfigCurrentSecondaryChannel > 3)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11CurrentSecondaryChannel (i4IfIndex,
                                               &u4TestCurrentSecondaryChannel)
        == OSIX_SUCCESS)
    {
        if (u4TestCurrentSecondaryChannel <
            u4TestValFsDot11nConfigCurrentSecondaryChannel)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigSTAChannelWidthConfig
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigSTAChannelWidthConfig
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigSTAChannelWidthConfig (UINT4 *pu4ErrorCode,
                                                 INT4 i4IfIndex,
                                                 INT4
                                                 i4TestValFsDot11nConfigSTAChannelWidthConfig)
{
    INT4                i4FsDot11nConfigSTAChannelWidth = 0;

    if ((i4TestValFsDot11nConfigSTAChannelWidthConfig != 0) &&
        (i4TestValFsDot11nConfigSTAChannelWidthConfig != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetFsDot11nConfigSTAChannelWidth (i4IfIndex,
                                                &i4FsDot11nConfigSTAChannelWidth)
        == OSIX_SUCCESS)
    {
        if (i4FsDot11nConfigSTAChannelWidth <
            i4TestValFsDot11nConfigSTAChannelWidthConfig)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2Dot11RIFSMode
 Input       :  The Indices
                IfIndex

                The Object
                testValDot11RIFSMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2Dot11RIFSMode (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                           INT4 i4TestValDot11RIFSMode)
{
    INT4                i4TestValDot11RIFSModeImplemented = 0;
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if ((i4TestValDot11RIFSMode != 0) && (i4TestValDot11RIFSMode != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppRifsMode = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    i4TestValDot11RIFSModeImplemented
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppRifsMode;

    if (i4TestValDot11RIFSMode > i4TestValDot11RIFSModeImplemented)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2Dot11HTProtection
 Input       :  The Indices
                IfIndex

                The Object
                testValDot11HTProtection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2Dot11HTProtection (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               INT4 i4TestValDot11HTProtection)
{
    INT4                i4TestValDot11HTProtectionImplemented = 0;
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if ((i4TestValDot11HTProtection < 0) || (i4TestValDot11HTProtection > 3))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppDualCtsProtection = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    i4TestValDot11HTProtectionImplemented
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
        u1SuppDualCtsProtection;

    if (i4TestValDot11HTProtectionImplemented < i4TestValDot11HTProtection)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigNonGreenfieldHTSTAsPresentConfig
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigNonGreenfieldHTSTAsPresentConfig
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigNonGreenfieldHTSTAsPresentConfig (UINT4 *pu4ErrorCode,
                                                            INT4 i4IfIndex,
                                                            INT4
                                                            i4TestValFsDot11nConfigNonGreenfieldHTSTAsPresentConfig)
{
    INT4                i4FsDot11nConfigNonGreenfieldHTSTAsPresent = 0;

    if ((i4TestValFsDot11nConfigNonGreenfieldHTSTAsPresentConfig != 0) &&
        (i4TestValFsDot11nConfigNonGreenfieldHTSTAsPresentConfig != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetFsDot11nConfigNonGreenfieldHTSTAsPresent (i4IfIndex,
                                                           &i4FsDot11nConfigNonGreenfieldHTSTAsPresent)
        == OSIX_SUCCESS)
    {
        if ((i4FsDot11nConfigNonGreenfieldHTSTAsPresent)
            < (i4TestValFsDot11nConfigNonGreenfieldHTSTAsPresentConfig))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigOBSSNonHTSTAsPresentConfig
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigOBSSNonHTSTAsPresentConfig
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigOBSSNonHTSTAsPresentConfig (UINT4 *pu4ErrorCode,
                                                      INT4 i4IfIndex,
                                                      INT4
                                                      i4TestValFsDot11nConfigOBSSNonHTSTAsPresentConfig)
{
    INT4                i4FsDot11nConfigOBSSNonHTSTAsPresent = 0;

    if ((i4TestValFsDot11nConfigOBSSNonHTSTAsPresentConfig != 0) &&
        (i4TestValFsDot11nConfigOBSSNonHTSTAsPresentConfig != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetFsDot11nConfigOBSSNonHTSTAsPresent (i4IfIndex,
                                                     &i4FsDot11nConfigOBSSNonHTSTAsPresent)
        == OSIX_SUCCESS)
    {
        if (i4FsDot11nConfigOBSSNonHTSTAsPresent <
            i4TestValFsDot11nConfigOBSSNonHTSTAsPresentConfig)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigDualBeaconConfig
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigDualBeaconConfig
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigDualBeaconConfig (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                            INT4
                                            i4TestValFsDot11nConfigDualBeaconConfig)
{
    INT4                i4FsDot11nConfigDualBeacon = 0;

    if ((i4TestValFsDot11nConfigDualBeaconConfig != 0) &&
        (i4TestValFsDot11nConfigDualBeaconConfig != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetFsDot11nConfigDualBeacon (i4IfIndex,
                                           &i4FsDot11nConfigDualBeacon) ==
        OSIX_SUCCESS)
    {
        if (i4FsDot11nConfigDualBeacon <
            i4TestValFsDot11nConfigDualBeaconConfig)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2Dot11DualCTSProtection
 Input       :  The Indices
                IfIndex

                The Object
                testValDot11DualCTSProtection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2Dot11DualCTSProtection (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                    INT4 i4TestValDot11DualCTSProtection)
{
    INT4                i4TestValDot11DualCTSProtectionImplemented = 0;
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if ((i4TestValDot11DualCTSProtection != 0) &&
        (i4TestValDot11DualCTSProtection != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppDualCtsProtection = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    i4TestValDot11DualCTSProtectionImplemented
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
        u1SuppDualCtsProtection;

    if ((i4TestValDot11DualCTSProtection == 1)
        && (i4TestValDot11DualCTSProtectionImplemented == 0))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigSTBCBeaconConfig
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigSTBCBeaconConfig
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigSTBCBeaconConfig (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                            INT4
                                            i4TestValFsDot11nConfigSTBCBeaconConfig)
{
    INT4                i4FsDot11nConfigSTBCBeacon = 0;

    if ((i4TestValFsDot11nConfigSTBCBeaconConfig != 0) &&
        (i4TestValFsDot11nConfigSTBCBeaconConfig != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetFsDot11nConfigSTBCBeacon (i4IfIndex,
                                           &i4FsDot11nConfigSTBCBeacon) ==
        OSIX_SUCCESS)
    {
        if (i4FsDot11nConfigSTBCBeacon <
            i4TestValFsDot11nConfigSTBCBeaconConfig)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2Dot11PCOActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValDot11PCOActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2Dot11PCOActivated (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               INT4 i4TestValDot11PCOActivated)
{
    INT4                i4TestValDot11PCOImplemented = 0;
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if ((i4TestValDot11PCOActivated != 0) && (i4TestValDot11PCOActivated != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppPcoActive = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    i4TestValDot11PCOImplemented
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SuppPcoActive;

    if ((i4TestValDot11PCOActivated == 1)
        && (i4TestValDot11PCOImplemented == 0))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigPCOPhaseConfig
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigPCOPhaseConfig
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigPCOPhaseConfig (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                          INT4
                                          i4TestValFsDot11nConfigPCOPhaseConfig)
{
    INT4                i4FsDot11nConfigPCOPhase = 0;

    if ((i4TestValFsDot11nConfigPCOPhaseConfig != 0) &&
        (i4TestValFsDot11nConfigPCOPhaseConfig != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetFsDot11nConfigPCOPhase (i4IfIndex,
                                         &i4FsDot11nConfigPCOPhase) ==
        OSIX_SUCCESS)
    {
        if (i4FsDot11nConfigPCOPhase < i4TestValFsDot11nConfigPCOPhaseConfig)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2Dot11TxMCSSetDefined
 Input       :  The Indices
                IfIndex

                The Object
                testValDot11TxMCSSetDefined
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2Dot11TxMCSSetDefined (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                  INT4 i4TestValDot11TxMCSSetDefined)
{

    INT4                i4TestValDot11TxMCSSetDefinedImplemented = 0;
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if ((i4TestValDot11TxMCSSetDefined != 0) &&
        (i4TestValDot11TxMCSSetDefined != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppTxMcsSetDefined = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    i4TestValDot11TxMCSSetDefinedImplemented
        =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
        u1SuppTxMcsSetDefined;

    if (i4TestValDot11TxMCSSetDefined >
        i4TestValDot11TxMCSSetDefinedImplemented)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigTxRxMCSSetNotEqual
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigTxRxMCSSetNotEqual
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigTxRxMCSSetNotEqual (UINT4 *pu4ErrorCode,
                                              INT4 i4IfIndex,
                                              INT4
                                              i4TestValFsDot11nConfigTxRxMCSSetNotEqual)
{
    INT4                i4Dot11TxRxMCSSetNotEqual = 0;

    if ((i4TestValFsDot11nConfigTxRxMCSSetNotEqual != 0) &&
        (i4TestValFsDot11nConfigTxRxMCSSetNotEqual != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (wsscfgGetDot11TxRxMCSSetNotEqual (i4IfIndex,
                                          &i4Dot11TxRxMCSSetNotEqual) ==
        OSIX_SUCCESS)
    {
        if ((i4Dot11TxRxMCSSetNotEqual == 0)
            && (i4TestValFsDot11nConfigTxRxMCSSetNotEqual == 1))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11nConfigRxSTBCOptionActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11nConfigRxSTBCOptionActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11nConfigRxSTBCOptionActivated (UINT4 *pu4ErrorCode,
                                                 INT4 i4IfIndex,
                                                 INT4
                                                 i4TestValFsDot11nConfigRxSTBCOptionActivated)
{
    INT4                i4Dot11RxSTBCOptionImplemented = 0;

    if ((i4TestValFsDot11nConfigRxSTBCOptionActivated < 0) ||
        (i4TestValFsDot11nConfigRxSTBCOptionActivated > 3))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    if (wsscfgGetDot11RxSTBCOptionImplemented (i4IfIndex,
                                               &i4Dot11RxSTBCOptionImplemented)
        == OSIX_SUCCESS)
    {
        if ((i4Dot11RxSTBCOptionImplemented < 0) ||
            (i4TestValFsDot11nConfigRxSTBCOptionActivated > 1))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11N_INCONSISTENT_VALUE);
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

#ifdef WLC_WANTED
/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigAmpduStatus
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigAmpduStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigAmpduStatus (INT4 i4IfIndex, UINT1 u1AmpduStatus)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bAMPDUStatus = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u1AMPDUStatus = u1AmpduStatus;

    if (RadioIfSetFsDot11nExtParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigAmpduSubFrame
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigAmpduSubFrame
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigAmpduSubFrame (INT4 i4IfIndex, UINT1 u1AmpduSubFrame)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bAMPDUSubFrame = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u1AMPDUSubFrame = u1AmpduSubFrame;

    if (RadioIfSetFsDot11nExtParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigAmpduLimit
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigAmpduStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigAmpduLimit (INT4 i4IfIndex, UINT2 u2AmpduLimit)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bAMPDULimit = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u2AMPDULimit = u2AmpduLimit;

    if (RadioIfSetFsDot11nExtParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigAmsduStatus
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigAspduStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigAmsduStatus (INT4 i4IfIndex, UINT1 u1AmsduStatus)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bAMSDUStatus = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u1AMSDUStatus = u1AmsduStatus;

    if (RadioIfSetFsDot11nExtParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgSetFsDot11nConfigAmsduLimit
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigAmsduLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11nConfigAmsduLimit (INT4 i4IfIndex, UINT2 u2AmsduLimit)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bAMSDULimit = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u2AMSDULimit = u2AmsduLimit;

    if (RadioIfSetFsDot11nExtParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
#endif
/****************************************************************************
 Function    :  wsscfgGetFsDot11nConfigAmpduStatus
 Input       :  The Indices
                IfIndex

                The Object
                pu1AmpduStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigAmpduStatus (INT4 i4IfIndex, INT4 *pi4AmpduStatus)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bAMPDUStatus = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4AmpduStatus = (INT4) RadioIfGetDB.RadioIfGetAllDB.u1AMPDUStatus;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgGetFsDot11nConfigAmpduSubFrame
 Input       :  The Indices
                IfIndex

                The Object
                p1AmpduSubFrame
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigAmpduSubFrame (INT4 i4IfIndex, UINT4 *pu4AmpduSubFrame)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bAMPDUSubFrame = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4AmpduSubFrame = (UINT4) RadioIfGetDB.RadioIfGetAllDB.u1AMPDUSubFrame;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgGetFsDot11nConfigAmpduLimit
 Input       :  The Indices
                IfIndex

                The Object
                pu1AmpduLimit 
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigAmpduLimit (INT4 i4IfIndex, UINT4 *pu4AmpduLimit)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bAMPDULimit = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4AmpduLimit = (UINT4) RadioIfGetDB.RadioIfGetAllDB.u2AMPDULimit;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgGetFsDot11nConfigAmsduStatus
 Input       :  The Indices
                IfIndex

                The Object
                pu1AmsduStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigAmsduStatus (INT4 i4IfIndex, INT4 *pi4AmsduStatus)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bAMSDUStatus = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pi4AmsduStatus = (INT4) RadioIfGetDB.RadioIfGetAllDB.u1AMSDUStatus;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgGetFsDot11nConfigAmsduLimit
 Input       :  The Indices
                IfIndex

                The Object
                pu1AmsduLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgGetFsDot11nConfigAmsduLimit (INT4 i4IfIndex, UINT4 *pu4AmsduLimit)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bAMSDULimit = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    *pu4AmsduLimit = (UINT4) RadioIfGetDB.RadioIfGetAllDB.u2AMSDULimit;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestFsDot11nConfigAmpduStatus
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigAmpduStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestFsDot11nConfigAmpduStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                     INT4 i4TestValFsDot11nAMPDUStatus)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bAMSDULimit = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return OSIX_FAILURE;
    }
    if ((i4TestValFsDot11nAMPDUStatus !=
         CLI_WSSCFG_ENABLE) &&
        (i4TestValFsDot11nAMPDUStatus != CLI_WSSCFG_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestFsDot11nConfigAmpduSubFrame
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigAmpduSubFrame
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestFsDot11nConfigAmpduSubFrame (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                       INT4 u4TestValFsDot11nAMPDUSubFrame)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bAMSDULimit = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return OSIX_FAILURE;
    }
    if ((u4TestValFsDot11nAMPDUSubFrame < 2) ||
        (u4TestValFsDot11nAMPDUSubFrame > 64))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestFsDot11nConfigAmpduLimit
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigAmpduStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestFsDot11nConfigAmpduLimit (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                    INT4 u4TestValFsDot11nAMPDULimit)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bAMSDULimit = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return OSIX_FAILURE;
    }
    if ((u4TestValFsDot11nAMPDULimit < 1024) ||
        (u4TestValFsDot11nAMPDULimit > 65535))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestFsDot11nConfigAmsduStatus
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigAspduStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestFsDot11nConfigAmsduStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                     INT4 i4TestValFsDot11nAMSDUStatus)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bAMSDULimit = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return OSIX_FAILURE;
    }
    if ((i4TestValFsDot11nAMSDUStatus != CLI_WSSCFG_ENABLE) &&
        (i4TestValFsDot11nAMSDUStatus != CLI_WSSCFG_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  wsscfgTestFsDot11nConfigAmsduLimit
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11nConfigAmsduLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT1
wsscfgTestFsDot11nConfigAmsduLimit (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                    INT4 u4TestValFsDot11nAMPDULimit)
{

    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bAMSDULimit = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return OSIX_FAILURE;
    }
    if ((u4TestValFsDot11nAMPDULimit < 1024) ||
        (u4TestValFsDot11nAMPDULimit > 4096))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

#endif
