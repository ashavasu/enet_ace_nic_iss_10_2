/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: fswsscli.c,v 1.5 2018/02/15 10:22:53 siva Exp $
 *
 * Description: This file contains the routines for fs11ac,dhcp and
 *          firewall related cli commands.
 *
 *********************************************************************/
#ifndef __FSWSSCLI_C__
#define __FSWSSCLI_C__

#include "wsscfginc.h"
#include "wsscfgcli.h"
#include "capwapcli.h"
#include "wsscfgprot.h"
#include "fswssllw.h"

UINT4               gu4DhcpPoolId;

/****************************************************************************
 * Function    :  cli_process_fswsslr_cmd
 * Description :  This function is exported to CLI module to handle the
                  fs11ac cli commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
cli_process_fswsslr_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[WSSCFG_CLI_MAX_ARGS];
    INT1                i1argno = 0;
    INT1                i1RetStatus = CLI_SUCCESS;
    INT4                i4Inst = 0;
    INT4                i4AclIfIndex = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4NetworkId = 0;
    UINT4               u4NetMask = 0;
    UINT4               u4StartIp = 0;
    UINT4               u4EndIp = 0;
    UINT4               u4Natid = 0;
    UINT4               u4Poolid = 0;
    UINT2               u2Proto = 0;
    UINT1               u1AclDirection = 0;
    UINT2               u2SeqNum = 0;
    UINT1               u1Action = 0;
    UINT1               au1FilterName[AP_FWL_MAX_FILTER_NAME_LEN];
    UINT1               au1AclName[AP_FWL_MAX_ACL_NAME_LEN];
    UINT1               au1SrcAddr[AP_FWL_MAX_ADDR_LEN];
    UINT1               au1DestAddr[AP_FWL_MAX_ADDR_LEN];
    UINT1               au1SrcPort[AP_FWL_MAX_PORT_LEN];
    UINT1               au1DestPort[AP_FWL_MAX_PORT_LEN];
    UINT1               au1IfName[MAX_PROMPT_LEN];
    CliRegisterLock (CliHandle, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    MEMSET (au1FilterName, 0, AP_FWL_MAX_FILTER_NAME_LEN);
    MEMSET (au1AclName, 0, AP_FWL_MAX_ACL_NAME_LEN);
    MEMSET (au1SrcAddr, 0, AP_FWL_MAX_ADDR_LEN);
    MEMSET (au1DestAddr, 0, AP_FWL_MAX_ADDR_LEN);
    MEMSET (au1SrcPort, 0, AP_FWL_MAX_PORT_LEN);
    MEMSET (au1DestPort, 0, AP_FWL_MAX_PORT_LEN);
    MEMSET (au1IfName, 0, MAX_PROMPT_LEN);

    va_start (ap, u4Command);

    /* Walk through the rest of the arguements and store in args array. 
     * Store RFMGMT_CLI_MAX_ARGS arguements at the max. 
     */

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }
    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == WSSCFG_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);
    switch (u4Command)
    {
        case CLI_AP_DHCPSRV_POOL:
            i1RetStatus = FsWssAPDhcpSrvPool (CliHandle, (INT4) *args[0]);
            break;
        case CLI_AP_NO_DHCPSRV_POOL:
            i1RetStatus = FsWssAPNoDhcpSrvPool (CliHandle, (INT4) *args[0],
                                                (UINT1 *) args[1]);
            break;
        case CLI_AP_NAT_POOLID:
            i1RetStatus = FsWssApNatPool (CliHandle, (UINT4) *args[0]);
            break;

        case CLI_AP_NAT_MAP:
            CLI_GET_NATID (&u4Natid);
            i1RetStatus =
                FsWssApNatMapping (CliHandle, (UINT1 *) args[0],
                                   (INT4) *args[1], u4Natid);
            break;

        case CLI_AP_FIREWALL_STATUS:
            i1RetStatus =
                FsWssFirewall (CliHandle, (UINT4) *args[0], (UINT1 *) args[1],
                               (UINT4 *) args[2]);
            break;
        case CLI_AP_DHCP_MODE:
            i1RetStatus = FsWssDhcpMode (CliHandle, (UINT4) *args[0],
                                         (UINT1 *) args[1], (UINT4 *) args[2]);
            break;
        case NO_CLI_AP_NAT_MAP:
            i1RetStatus =
                FsWssNoApNatMapping (CliHandle, (INT4) *args[0],
                                     (UINT1 *) args[1]);
            break;
            break;
        case CLI_AP_FIREWALL:
            i1RetStatus = FsWssAPFirewall (CliHandle);
            break;

        case CLI_AP_DHCPSRV_NETWORK:

            CLI_GET_POOLID (&u4Poolid);
            u4NetworkId = *args[0];

            if (CLI_PTR_TO_U4 (args[1]) == NETWORK_MASK_BITS)
            {
                u4NetMask = ~(0xffffffff >> *args[2]);
            }
            else if (CLI_PTR_TO_U4 (args[1]) == NETWORK_MASK_DECIMAL)
            {
                u4NetMask = *args[2];
            }
            else if (CLI_PTR_TO_U4 (args[1]) == NETWORK_DEFAULT_MASK)
            {
                u4NetMask = DHCP_DEF_SUBNET_MASK;
            }
            if (args[3] != NULL)
            {
                u4EndIp = *args[3];
            }
            else
            {
                u4EndIp =
                    (u4NetworkId & u4NetMask) + (0xffffffff & (~u4NetMask));
                u4EndIp -= 0x00000001;
            }
            u4StartIp = u4NetworkId;

            if (!(u4StartIp & 0x000000ff))
            {
                /* If the start IP begins in the format of x.x.x.0, then the start
                 *                    IP should start from x.x.x.1 */
                u4StartIp = u4StartIp + 1;
            }
            u4NetworkId = (u4NetworkId & u4NetMask);
            if (((u4StartIp & u4NetMask) != u4NetworkId))

            {
                CliPrintf (CliHandle,
                           "\r%% Invalid Network & Subnet Mask combination\r\n");
                i1RetStatus = CLI_FAILURE;
            }
            else if ((u4EndIp & u4NetMask) != u4NetworkId)
            {
                CliPrintf (CliHandle, "\r%% Invalid Subnet. "
                           "Check if End IP addresses belong to this subnet.\r\n ");
                i1RetStatus = CLI_FAILURE;
            }
            else if (u4EndIp <= u4StartIp)
            {
                CliPrintf (CliHandle, "\r%% Invalid Endip. "
                           "Check if End IP addresses is less than start ip.\r\n ");
                i1RetStatus = CLI_FAILURE;
            }

            if (i1RetStatus != CLI_FAILURE)
            {
                i1RetStatus = FsWssDhcpSrvNetwork (CliHandle,
                                                   u4NetworkId, u4NetMask,
                                                   u4StartIp, u4EndIp,
                                                   (UINT1 *) args[4], u4Poolid);
            }
            break;
        case CLI_AP_DHCPSRV_LEASE:
            CLI_GET_POOLID (&u4Poolid);
            i1RetStatus = FsWssDhcpSrvLease (CliHandle,
                                             (INT4 *) args[0],
                                             (INT4 *) args[1],
                                             (INT4 *) args[2],
                                             (UINT1 *) args[3],
                                             (UINT1 *) args[4], u4Poolid);
            break;

        case CLI_AP_DHCP_RELAY_NEXT_IP:
            if (args[0] != NULL)
            {
                i1RetStatus = FsWssDhcpRelayNextip (CliHandle,
                                                    (UINT4) *args[0]);
            }
            break;
        case CLI_AP_DEFAULT_ROUTER_IP:
            if (args[0] != NULL)
            {
                i1RetStatus = FsWssDefaultRouterIp (CliHandle, args[0]);
            }
            break;
        case CLI_AP_DNS_SERVER_IP:
            if (args[0] != NULL)
            {
                i1RetStatus = FsWssDnsServerIp (CliHandle, args[0]);
            }
            break;
        case CLI_AP_WLAN_RADIO_IP_ADDR:
            i1RetStatus = FsWssDhcpwlanRadioip (CliHandle,
                                                args[0], args[1],
                                                (UINT1 *) args[2],
                                                args[3], args[4]);
            break;
        case CLI_AP_NO_WLAN_RADIO_IP_ADDR:
            i1RetStatus = FsWssNoDhcpwlanRadioip (CliHandle,
                                                  args[0], args[1],
                                                  (UINT1 *) args[2],
                                                  args[3], args[4]);
            break;

        case CLI_AP_ROUTE_CONFIG:
            i1RetStatus = FsWssRouteConfig (CliHandle,
                                            args[0], args[1],
                                            args[2], (UINT1 *) args[3]);
            break;
        case CLI_AP_NO_ROUTE_CONFIG:
            i1RetStatus = FsWssNoRouteConfig (CliHandle,
                                              args[0], args[1],
                                              args[2], (UINT1 *) args[3]);
            break;

        case CLI_AP_DHCPSRV_POOL_OPTION:
            CLI_GET_POOLID (&u4Poolid);
            i1RetStatus = FsWssDhcpSrvPoolOption (CliHandle,
                                                  (INT4 *) args[0], args[1],
                                                  (UINT1 *) args[2], u4Poolid);
            break;
        case CLI_ADD_AP_FWL_FILTER:
            /*
             * args[0] = Firewall filter name
             * args[1] = Source IP range - for ex 10.0.0.0/16 | any
             * args[2] = Destination IP range | any
             * args[3] = Protocol filter is: 
             *           tcp  | udp  |
             *           icmp | igmp | ggp  | ip   | egp |
             *           igp  | nvp  | irtp | idpr | rsvp |
             *           mhrp | igrp | ospf
             *
             * args[4] = Source Port range - for ex. >=23
             * args[5] = Destination Port range - for ex. >1023
             * args[6] = established
             * args[7] = reset
             *
             */
            /* Copy the firewall fiter name which is the index of the table */
            if (STRLEN (args[0]) > AP_FWL_MAX_FILTER_NAME_LEN - AP_FWL_ONE)
            {
                CliPrintf (CliHandle, "%%ERROR: Filter name is "
                           "too long !\r\n");

                i1RetStatus = CLI_FAILURE;
                break;
            }
            if (STRNCASECMP (args[0], "fildef", 6) == AP_FWL_ZERO)
            {
                CliPrintf (CliHandle, "%%ERROR: Filter name has "
                           "to be other than fildef !\r\n");
                i1RetStatus = CLI_FAILURE;
                break;
            }
            STRCPY (au1FilterName, args[0]);
            /* If the source and destination address is specified as "any"
             * use 0.0.0.0/0 as range
             */

            /*Source IP address range */
            if (STRNCASECMP (args[1], "any", 3) == AP_FWL_ZERO)
            {
                STRCPY (au1SrcAddr, "0.0.0.0/0");
            }
            else
            {
                if (STRLEN (args[1]) > AP_FWL_MAX_ADDR_LEN)
                {
                    CliPrintf (CliHandle, "%%ERROR: Maximum source address  "
                               "length can be 85 !\r\n");
                    i1RetStatus = CLI_FAILURE;
                    break;
                }
                STRCPY (au1SrcAddr, args[1]);
            }

            /* Dest IP address range */
            if (STRNCASECMP (args[2], "any", 3) == AP_FWL_ZERO)
            {
                STRCPY (au1DestAddr, "0.0.0.0/0");
            }
            else
            {
                if (STRLEN (args[2]) < AP_FWL_MAX_ADDR_LEN)
                {
                    STRCPY (au1DestAddr, args[2]);
                }
                else
                {
                    CliPrintf (CliHandle,
                               "%%ERROR: Maximum destination address  "
                               "length can be 85 !\r\n");
                    i1RetStatus = CLI_FAILURE;
                    break;
                }
            }
            /* Protocol - for ex. tcp, udp, ip, etc */
            if ((args[3] != NULL) && (STRCMP (args[3], "other") != AP_FWL_ZERO))
            {
                if ((u2Proto =
                     (UINT2) Ap_cli_get_fwl_filter_protocol ((UINT1 *) args[3]))
                    == AP_FWL_ZERO)
                {
                    CliPrintf (CliHandle, "%%ERROR: Invalid Protocol !\r\n");
                    i1RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else if ((args[3] != NULL)
                     && (STRCMP (args[3], "other") == FWL_ZERO))
            {
                u2Proto = *(UINT2 *) (VOID *) args[4];
                /* other proto */
            }
            else
            {
                u2Proto = WSSCFG_DEFAULT_PROTO;    /* default proto */
            }
            /* User has specified the src port range */
            if (args[5] != NULL)
            {
                /* If the source port range is specified as "any" set >1
                 * If protocol is other than udp/tcp/any then dont set source port */
                if ((args[3] != NULL) &&
                    (STRCASECMP (args[3], "tcp") != AP_FWL_ZERO) &&
                    (STRCASECMP (args[3], "udp") != AP_FWL_ZERO) &&
                    (STRCASECMP (args[3], "any") != AP_FWL_ZERO) &&
                    (STRCASECMP (args[3], "other") != AP_FWL_ZERO))
                {
                    CliPrintf (CliHandle,
                               "%%ERROR: Source-Port entry for TCP, UDP"
                               " protocols only!\r\n");
                    i1RetStatus = CLI_FAILURE;
                    break;
                }
                if (STRNCASECMP (args[5], "any", 3) == AP_FWL_ZERO)
                {
                    STRCPY (au1SrcPort, ">1");
                }
                else
                {
                    if (STRLEN (args[5]) > FWL_MAX_PORT_LEN)
                    {
                        CliPrintf (CliHandle,
                                   "%%ERROR: Maximum source address range "
                                   "length can be 12 !\r\n");
                        i1RetStatus = CLI_FAILURE;
                        break;

                    }
                    STRCPY (au1SrcPort, args[5]);
                }
            }
            else
            {
                if ((args[3] == NULL) ||
                    (STRCASECMP (args[3], "tcp") == AP_FWL_ZERO) ||
                    (STRCASECMP (args[3], "udp") == AP_FWL_ZERO) ||
                    (STRCASECMP (args[3], "any") == AP_FWL_ZERO) ||
                    (STRCASECMP (args[3], "other") == AP_FWL_ZERO))
                {
                    STRCPY (au1SrcPort, ">1");
                }
            }

            /* User has specified the dest port range */
            if (args[FWL_INDEX_6] != NULL)
            {
                /* If the destination port range is specified as "any" set >1
                 * If protocol is other than udp/tcp/any then dont set dest-port */
                if ((args[3] != NULL) &&
                    (STRCASECMP (args[3], "tcp") != AP_FWL_ZERO) &&
                    (STRCASECMP (args[3], "udp") != AP_FWL_ZERO) &&
                    (STRCASECMP (args[3], "any") != AP_FWL_ZERO) &&
                    (STRCASECMP (args[3], "other") != AP_FWL_ZERO))
                {
                    CliPrintf (CliHandle, "%%ERROR: Destination-Port entry"
                               "for TCP,UDP protocols only!\r\n");
                    i1RetStatus = CLI_FAILURE;
                    break;
                }

                if (STRNCASECMP (args[6], "any", 3) == AP_FWL_ZERO)
                {
                    STRCPY (au1DestPort, ">1");
                }
                else
                {
                    if ((STRLEN (args[6])) < AP_FWL_MAX_PORT_LEN)
                    {
                        STRNCPY (au1DestPort,
                                 args[6],
                                 MEM_MAX_BYTES (STRLEN (args[6]),
                                                AP_FWL_MAX_PORT_LEN));
                    }
                }
            }
            else
            {
                if ((args[3] == NULL) ||
                    (STRCASECMP (args[3], "tcp") == AP_FWL_ZERO) ||
                    (STRCASECMP (args[3], "udp") == AP_FWL_ZERO) ||
                    (STRCASECMP (args[3], "any") == AP_FWL_ZERO) ||
                    (STRCASECMP (args[3], "other") == AP_FWL_ZERO))
                {
                    STRCPY (au1DestPort, ">1");
                }
            }

            i1RetStatus =
                ApFwlCliAddFilter (CliHandle, (UINT1 *) args[7], au1FilterName,
                                   au1SrcAddr, au1DestAddr, u2Proto, au1SrcPort,
                                   au1DestPort);

            break;

        case CLI_DEL_AP_FWL_FILTER:
            if (STRLEN (args[0]) > AP_FWL_MAX_FILTER_NAME_LEN - AP_FWL_ONE)
            {
                CliPrintf (CliHandle, "%%ERROR: Filter name is too long !\r\n");

                i1RetStatus = CLI_FAILURE;
                break;
            }
            i1RetStatus = ApFwlCliDeleteFilter (CliHandle,
                                                (UINT1 *) args[1],
                                                (UINT1 *) args[0]);
            break;

        case CLI_ADD_AP_FWL_ACCESS_LIST:

            if (STRLEN (args[0]) > AP_FWL_MAX_ACL_NAME_LEN - AP_FWL_ONE)
            {
                CliPrintf (CliHandle, "%%ERROR: ACL name is too long !\r\n");
                i1RetStatus = CLI_FAILURE;
                break;
            }
            STRCPY (au1AclName, args[0]);

            if (STRCMP (au1AclName, "acldef") == AP_FWL_ZERO)
            {
                CliPrintf (CliHandle, "%%ERROR: Acl name has "
                           "to be other than fildef !\r\n");
                i1RetStatus = CLI_FAILURE;
                break;
            }
            STRCPY (au1FilterName, args[1]);
            u1AclDirection = (UINT4) CLI_ATOI (args[2]);
            u1Action = (UINT4) CLI_ATOI (args[3]);
            u2SeqNum = *((UINT4 *) (VOID *) args[4]);
            i4AclIfIndex = 0;
            i1RetStatus =
                ApFwlCliAddAccessList (CliHandle, (UINT1 *) args[5], au1AclName,
                                       au1FilterName, i4AclIfIndex,
                                       u1AclDirection, u2SeqNum, u1Action);

            break;

        case CLI_DEL_AP_FWL_ACCESS_LIST:
            if (STRLEN (args[0]) > AP_FWL_MAX_ACL_NAME_LEN - AP_FWL_ONE)
            {
                CliPrintf (CliHandle, "%%ERROR: ACL name is too long !\r\n");
                i1RetStatus = CLI_FAILURE;
                break;
            }
            i1RetStatus =
                ApFwlCliDeleteAccessList (CliHandle, (UINT1 *) args[2], 0,
                                          (UINT1 *) args[0],
                                          (UINT4) CLI_ATOI (args[1]));
            break;
        case CLI_AP_L3_SUBIF_CREATE:
            /* Check If the interface is already created with the given port and vlan */
            if (FsWssApCliGetL3IfIndexFromPort (CLI_PTR_TO_U4 (args[2]),
                                                CLI_PTR_TO_U4 (args[3]),
                                                &u4IfIndex) == CLI_SUCCESS)
            {
                /* Change the mode to iss(config-subif)# mode */
                SPRINTF ((CHR1 *) au1IfName, "%s%d",
                         CLI_AP_L3SUBIF_MODE, u4IfIndex);
                if (CliChangePath ((CHR1 *) au1IfName) != CLI_SUCCESS)
                {
                }
                break;
            }
            else
            {
                if ((FsWssApCliGetFreeIfIndex (AP_L3SUBIF, &u4IfIndex)) ==
                    CLI_SUCCESS)
                {
                    i1RetStatus =
                        FsWssApCliCreateL3SubIf (CliHandle, (UINT1 *) args[0],
                                                 u4IfIndex,
                                                 CLI_PTR_TO_I4 (args[2]),
                                                 CLI_PTR_TO_I4 (args[3]));
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r%% Maximum Number of L3Sub interfaces"
                               " Reached \r\n");
                    i1RetStatus = CLI_FAILURE;
                }
            }
            break;

        case CLI_AP_L3_SUBIF_DELETE:
            i1RetStatus = FsWssApCliDeleteL3SubIf (CliHandle, (UINT1 *) args[0],
                                                   CLI_PTR_TO_U4 (args[1]));
            break;

        case CLI_AP_L3_SUBIF_NW_TYPE:
            u4IfIndex = CLI_GET_IFINDEX ();
            if ((UINT1 *) args[1] != NULL)
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[1], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Profile Id \r\n");
                    return CLI_FAILURE;
                }
            }

            i1RetStatus = FsWssApCliSetL3SubIfNwType (CliHandle, u4IfIndex,
                                                      u4WtpProfileId,
                                                      CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_AP_L3_SUBIF_ADMIN_STATUS:
            u4IfIndex = CLI_GET_IFINDEX ();
            if ((UINT1 *) args[1] != NULL)
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[1], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Profile Id \r\n");
                    return CLI_FAILURE;
                }
            }

            i1RetStatus = FsWssApCliSetL3SubIfAdminStatus (CliHandle, u4IfIndex,
                                                           u4WtpProfileId,
                                                           CLI_PTR_TO_I4 (args
                                                                          [0]));
            break;

        case CLI_AP_L3_SUBIF_IP_ADDRESS:
            u4IfIndex = CLI_GET_IFINDEX ();

            if (args[0] != NULL && args[1] != NULL)
            {
                if ((UINT1 *) args[2] != NULL)
                {
                    if (CapwapGetWtpProfileIdFromProfileName
                        ((UINT1 *) args[2], &u4WtpProfileId) != OSIX_SUCCESS)
                    {
                        CliPrintf (CliHandle, "\r%% Invalid Profile Id \r\n");
                        return CLI_FAILURE;
                    }
                }

                i1RetStatus = FsWssApCliSetL3SubIfIpAddr (CliHandle, u4IfIndex,
                                                          u4WtpProfileId,
                                                          (UINT4) *args[0],
                                                          (UINT4) *args[1]);
            }
            /* Remove the IP address configured on this interface */
            else
            {
                if ((UINT1 *) args[0] != NULL)
                {
                    if (CapwapGetWtpProfileIdFromProfileName
                        ((UINT1 *) args[0], &u4WtpProfileId) != OSIX_SUCCESS)
                    {
                        CliPrintf (CliHandle, "\r%% Invalid Profile Id \r\n");
                        return CLI_FAILURE;
                    }
                }

                i1RetStatus = FsWssApCliSetL3SubIfIpAddr (CliHandle, u4IfIndex,
                                                          u4WtpProfileId, 0, 0);
            }
            break;

        default:
            break;
    }
    if ((i1RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_WSSCFG_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", WsscfgCliErrString[u4ErrCode]);
            CLI_FATAL_ERROR (CliHandle);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i1RetStatus);

    CliUnRegisterLock (CliHandle);
    WSSCFG_UNLOCK;
    UNUSED_PARAM (u4IfIndex);
    return i1RetStatus;

}

/****************************************************************************
 ** Function    :  cli_process_fswsslr_show_cmd
 ** Description :  This function is exported to CLI module to handle the
 *                 fs11ac cli show commands to take the corresponding action.
 ** Input       :  Variable arguments
 ** Output      :  None
 ** Returns     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/
INT4
cli_process_fswsslr_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[WSSCFG_CLI_MAX_ARGS];
    INT1                i1argno = 0;
    INT1                i1RetStatus = CLI_SUCCESS;
    INT4                i4Inst = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4ErrCode = 0;
    CliRegisterLock (CliHandle, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    va_start (ap, u4Command);

    /* Walk through the rest of the arguements and store in args array.
     *      * Store RFMGMT_CLI_MAX_ARGS arguements at the max.
     *           */
    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }

    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == CLI_FSWSSLR_MAX_ERR)
        {
            break;
        }
    }
    va_end (ap);

    switch (u4Command)
    {
        case CLI_AP_SHOW_LOCAL_INFO:
        {
            INT4                i4WlanDhcpServerStatus = 0;
            INT4                i4WlanDhcpRelayStatus = 0;
            INT4                i4WlanFirewallStatus = 0;
            INT4                i4WlanNATStatus = 0;
            UINT4               u4WlanDhcpNextSrvIpAddr = 0;
            UINT4               u4WlanDNSServerIpAddr = 0;
            UINT4               u4WlanDefaultServerIpAddr = 0;
            CHR1               *pu1IpAddr = NULL;

            if (nmhGetFsWlanDhcpServerStatus (&i4WlanDhcpServerStatus) ==
                SNMP_SUCCESS)
            {
                if (i4WlanDhcpServerStatus == 1)
                {
                    CliPrintf (CliHandle,
                               "\r\nDhcp Server         : Enabled\r");
                }
                else if (i4WlanDhcpServerStatus == 0)
                {
                    CliPrintf (CliHandle,
                               "\r\nDhcp Server         : Disabled\r");
                }
            }
            if (nmhGetFsWlanDhcpRelayStatus (&i4WlanDhcpRelayStatus) ==
                SNMP_SUCCESS)
            {
                if (i4WlanDhcpRelayStatus == 1)
                {
                    CliPrintf (CliHandle,
                               "\r\nDhcp Relay          : Enabled\r");
                }
                else if (i4WlanDhcpRelayStatus == 0)
                {
                    CliPrintf (CliHandle,
                               "\r\nDhcp Relay          : Disabled\r");
                }
            }
            if (nmhGetFsWlanFirewallStatus (&i4WlanFirewallStatus) ==
                SNMP_SUCCESS)
            {
                if (i4WlanFirewallStatus == 1)
                {
                    CliPrintf (CliHandle,
                               "\r\nAp Firewall         : Enabled\r");
                }
                else if (i4WlanFirewallStatus == 0)
                {
                    CliPrintf (CliHandle,
                               "\r\nAp Firewall         : Disabled\r");
                }
            }
            if (nmhGetFsWlanNATStatus (&i4WlanNATStatus) == SNMP_SUCCESS)
            {
                if (i4WlanNATStatus == WAN_TYPE_PUBLIC)
                {
                    CliPrintf (CliHandle, "\r\nAp NAT              : LAN\r");
                }
                else if (i4WlanNATStatus == WAN_TYPE_PRIVATE)
                {
                    CliPrintf (CliHandle, "\r\nAp NAT              : WAN\r");
                }
            }
            if (nmhGetFsWlanDhcpNextSrvIpAddr (&u4WlanDhcpNextSrvIpAddr) ==
                SNMP_SUCCESS)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4WlanDhcpNextSrvIpAddr);
                CliPrintf (CliHandle,
                           "\r\nDhcp Next Server    : %s", pu1IpAddr);
            }
            if (nmhGetFsWlanDNSServerIpAddr (&u4WlanDNSServerIpAddr) ==
                SNMP_SUCCESS)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4WlanDNSServerIpAddr);
                CliPrintf (CliHandle,
                           "\r\nDNS Server          : %s", pu1IpAddr);
            }
            if (nmhGetFsWlanDefaultRouterIpAddr (&u4WlanDefaultServerIpAddr) ==
                SNMP_SUCCESS)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                           u4WlanDefaultServerIpAddr);
                CliPrintf (CliHandle,
                           "\r\nDefault Gateway      : %s", pu1IpAddr);
            }
        }
            break;
        case CLI_AP_SHOW_DHCP_POOL:
        {
            i1RetStatus = FsWssShowAPDhcpSrvPool (CliHandle, (INT4 *) args[0],
                                                  (UINT1 *) args[1],
                                                  (UINT1 *) args[2]);
        }
            break;
        case CLI_AP_SHOW_DHCP_CONFIG:
        {
            i1RetStatus = FsWssShowAPDhcpConfig (CliHandle, (UINT1 *) args[0]);
        }
            break;
        case CLI_AP_SHOW_FIREWALL_CONFIG:
        {
            i1RetStatus =
                FsWssShowAPFirewallConfig (CliHandle, (UINT1 *) args[0]);
        }
            break;
        case CLI_AP_SHOW_FIREWALL_FILTERS:
        {
            i1RetStatus =
                FsWssShowAPFirewallFilters (CliHandle, (UINT1 *) args[0]);
        }
            break;
        case CLI_AP_SHOW_FIREWALL_ACL:
        {
            i1RetStatus = FsWssShowAPFirewallAcl (CliHandle, (UINT1 *) args[0]);
        }
            break;

        case CLI_AP_SHOW_NAT_CONFIG:
        {
            i1RetStatus = FsWssShowAPNATConfig (CliHandle, (INT4) *args[0],
                                                (UINT1 *) args[1]);
        }
            break;
        case CLI_AP_SHOW_IP_ROUTE:
        {
            i1RetStatus = FsWssShowAPIpRoute (CliHandle, (UINT1 *) args[0]);
        }
            break;
        case CLI_AP_SHOW_IP_CONFIG:
        {
            i1RetStatus = FsWssShowAPIpConfig (CliHandle, (UINT1 *) args[0],
                                               (UINT4 *) args[1],
                                               (UINT4 *) args[2],
                                               (UINT1 *) args[3]);
        }
            break;
        case CLI_AP_SHOW_L3_SUBIF:
        {
            i1RetStatus = FsWssShowAPL3SubIf (CliHandle, (UINT1 *) args[0]);
        }
            break;
        default:
            break;
    }
    if ((i1RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_WSSCFG_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", WsscfgCliErrString[u4ErrCode]);
            CLI_FATAL_ERROR (CliHandle);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i1RetStatus);

    CliUnRegisterLock (CliHandle);
    WSSCFG_UNLOCK;
    UNUSED_PARAM (u4IfIndex);
    return i1RetStatus;

}

/****************************************************************************
 * Function    :  FsWssAPDhcpSrvPool
 * Description :  This function is used to change the path for dhcp
 * configuration

 * Input       :  CliHandle, dhcp pool id

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/

INT1
FsWssAPDhcpSrvPool (tCliHandle CliHandle, INT4 u4DhcpPool)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    {
        SPRINTF ((CHR1 *) au1Cmd, "%s%d", CLI_AP_DHCP_POOL, u4DhcpPool);
        if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  FsWssAPNoDhcpSrvPool
 * Description :  This function is used to delete a dhcp pool

 * Input       :  CliHandle, dhcp pool id, ap profile name.

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/

INT1
FsWssAPNoDhcpSrvPool (tCliHandle CliHandle, INT4 i4DhcpPool,
                      UINT1 *pu1ProfileName)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    INT4                i4RetValue = 0;
    INT4                i4DhcpPoolId = 0;
    INT4                i4DhcpNextPoolId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4WtpNextProfileId = 0;

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);

    if (pu1ProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Name Conversion - failed");
            return CLI_FAILURE;
        }
        if (nmhGetFsDot11WtpDhcpSrvSubnetPoolRowStatus (u4WtpProfileId,
                                                        i4DhcpPool,
                                                        &i4RetValue) ==
            SNMP_SUCCESS)
        {
            if (nmhSetFsDot11WtpDhcpSrvSubnetPoolRowStatus (u4WtpProfileId,
                                                            i4DhcpPool,
                                                            DESTROY) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "Error in deleting the entry");
                return CLI_FAILURE;
            }
        }
        else
        {
            CliPrintf (CliHandle, "Entry Not Found");
            return CLI_SUCCESS;
        }
    }
    else
    {
        if (nmhGetFirstIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable
            (&u4WtpNextProfileId, &i4DhcpNextPoolId) == SNMP_SUCCESS)
        {
            do
            {
                if (i4DhcpNextPoolId == i4DhcpPool)
                {
                    if (nmhSetFsDot11WtpDhcpSrvSubnetPoolRowStatus
                        (u4WtpNextProfileId, i4DhcpNextPoolId,
                         DESTROY) == SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle, "Error in deleting %s - %d",
                                   u4WtpProfileId, i4DhcpNextPoolId);
                    }
                }
                u4WtpProfileId = u4WtpNextProfileId;
                i4DhcpPoolId = i4DhcpNextPoolId;
            }
            while (nmhGetNextIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable
                   (u4WtpProfileId, &u4WtpNextProfileId, i4DhcpPoolId,
                    &i4DhcpNextPoolId) == SNMP_SUCCESS);
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  FsWssAPFirewall
 * Description :  This function is used to change the path for firewall
 * configuration

 * Input       :  CliHandle.

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/

INT1
FsWssAPFirewall (tCliHandle CliHandle)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    {
        SPRINTF ((CHR1 *) au1Cmd, "%s", CLI_AP_FIREWALL_PROMPT);
        if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  FsWssFirewall
 * Description :  This function is used to configure firewall status.

 * Input       :  CliHandle, status input, ap profile name, all profile
 * indication

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/

INT1
FsWssFirewall (tCliHandle CliHandle, UINT4 u4Status, UINT1 *pu1ProfileName,
               UINT4 *pu4Allprofiles)
{
    UINT4               u4WtpProfileId = 0;
    INT4                i4RetValue = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4WtpNextProfileId = 0;

    if ((pu1ProfileName == NULL) && (pu4Allprofiles == NULL))
    {
        if (nmhTestv2FsWlanFirewallStatus (&u4ErrCode, (INT4) u4Status) ==
            SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_WSS_LR_FIREWALL_ERRROR);
            CliPrintf (CliHandle, "\r\nValidation of Input - failed");
            return CLI_FAILURE;
        }
        if (nmhSetFsWlanFirewallStatus (u4Status) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_WSS_LR_FIREWALL_ERRROR);
            CliPrintf (CliHandle, "\r\nConfiguration of Input - failed");
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }
    if (pu1ProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Name Conversion - failed");
            return CLI_FAILURE;
        }
        if (nmhGetFsWtpFirewallRowStatus (u4WtpProfileId, &i4RetValue) ==
            SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_WSS_LR_FIREWALL_ERRROR);
            CliPrintf (CliHandle, "\r\nEntry does not exist");
            return CLI_FAILURE;
        }
        else
        {
            if (nmhSetFsWtpFirewallRowStatus (u4WtpProfileId, NOT_IN_SERVICE) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_WSS_LR_FIREWALL_ERRROR);
                CliPrintf (CliHandle, "\r\nConfiguration of Input - failed");
                return CLI_FAILURE;
            }
            if (nmhTestv2FsWtpFirewallStatus
                (&u4ErrCode, u4WtpProfileId, u4Status) == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_WSS_LR_FIREWALL_ERRROR);
                CliPrintf (CliHandle, "\r\nValidation of Input - failed");
                return CLI_FAILURE;
            }
            if (nmhSetFsWtpFirewallStatus (u4WtpProfileId, u4Status) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_WSS_LR_FIREWALL_ERRROR);
                CliPrintf (CliHandle, "\r\nConfiguration of Input - failed");
                return CLI_FAILURE;
            }
            if (nmhSetFsWtpFirewallRowStatus (u4WtpProfileId, ACTIVE) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_WSS_LR_FIREWALL_ERRROR);
                CliPrintf (CliHandle, "\r\nConfiguration of Input - failed");
                return CLI_FAILURE;
            }
        }
    }
    else
    {
        if (nmhGetFirstIndexFsWtpFirewallConfigTable (&u4WtpNextProfileId)
            == SNMP_SUCCESS)
        {
            do
            {
                u4WtpProfileId = u4WtpNextProfileId;
                if (nmhGetFsWtpFirewallRowStatus
                    (u4WtpNextProfileId, &i4RetValue) == SNMP_SUCCESS)
                {
                    if (nmhSetFsWtpFirewallRowStatus
                        (u4WtpProfileId, NOT_IN_SERVICE) == SNMP_FAILURE)
                    {
                        continue;
                    }
                    if (nmhTestv2FsWtpFirewallStatus
                        (&u4ErrCode, u4WtpNextProfileId,
                         u4Status) == SNMP_SUCCESS)
                    {
                        if (nmhSetFsWtpFirewallStatus
                            (u4WtpNextProfileId, u4Status) == SNMP_FAILURE)
                        {
                            continue;
                        }
                    }
                    if (nmhSetFsWtpFirewallRowStatus (u4WtpProfileId, ACTIVE) ==
                        SNMP_FAILURE)
                    {
                        continue;
                    }
                }

            }
            while (nmhGetNextIndexFsWtpFirewallConfigTable (u4WtpProfileId,
                                                            &u4WtpNextProfileId)
                   == SNMP_SUCCESS);

        }

    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  FsWssDhcpMode
 * Description :  This function is used to configure dhcp mode.

 * Input       :  CliHandle, mode input, ap profile name, all profile
 * indication

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/

INT1
FsWssDhcpMode (tCliHandle CliHandle, UINT4 u4DhcpMode, UINT1 *pu1ProfileName,
               UINT4 *pu4Allprofiles)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4WtpNextProfileId = 0;
    UINT4               u4DhcpServerStatus = 0;
    UINT4               u4DhcpRelayStatus = 0;

    if (u4DhcpMode == DHCP_SERVER_ENABLE)
    {
        u4DhcpServerStatus = 1;
    }
    else if (u4DhcpMode == DHCP_SERVER_DISABLE)
    {
        u4DhcpServerStatus = 0;
    }
    else if (u4DhcpMode == DHCP_RELAY_ENABLE)
    {
        u4DhcpRelayStatus = 1;
    }
    else
    {
        u4DhcpRelayStatus = 0;
    }

    if ((pu1ProfileName != NULL) && (pu4Allprofiles == NULL))
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nNull Inputs");
            return CLI_FAILURE;
        }

        if ((u4DhcpMode == 1) || (u4DhcpMode == 2))
        {
            if (nmhTestv2FsWtpDhcpServerStatus (&u4ErrorCode,
                                                u4WtpProfileId,
                                                u4DhcpServerStatus) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nValidation of Input - failed");
                return CLI_FAILURE;
            }
            if (nmhSetFsWtpDhcpServerStatus (u4WtpProfileId, u4DhcpServerStatus)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nConfiguration of Input - failed");
                return CLI_FAILURE;
            }
        }
        else if ((u4DhcpMode == 3) || (u4DhcpMode == 4))
        {
            if (nmhTestv2FsWtpDhcpRelayStatus (&u4ErrorCode, u4WtpProfileId,
                                               u4DhcpRelayStatus) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nValidation of Input - failed");
                return CLI_FAILURE;
            }
            if (nmhSetFsWtpDhcpRelayStatus (u4WtpProfileId, u4DhcpRelayStatus)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nConfiguration of Input - failed");
                return CLI_FAILURE;
            }
        }
    }
    if ((pu1ProfileName == NULL) && (pu4Allprofiles != NULL))
    {
        if ((u4DhcpMode == 1) || (u4DhcpMode == 2))
        {
            if (nmhGetFirstIndexFsWtpDhcpConfigTable (&u4WtpNextProfileId)
                == SNMP_SUCCESS)
            {
                do
                {
                    if (nmhTestv2FsWtpDhcpServerStatus (&u4ErrorCode,
                                                        u4WtpNextProfileId,
                                                        u4DhcpServerStatus) ==
                        SNMP_SUCCESS)
                    {
                        if (nmhSetFsWtpDhcpServerStatus (u4WtpNextProfileId,
                                                         u4DhcpServerStatus) ==
                            SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nConfiguration of Input - failed");
                            return CLI_FAILURE;
                        }
                    }
                    u4WtpProfileId = u4WtpNextProfileId;

                }
                while (nmhGetNextIndexFsWtpDhcpConfigTable (u4WtpProfileId,
                                                            &u4WtpNextProfileId)
                       == SNMP_SUCCESS);
            }
        }
        if ((u4DhcpMode == 3) || (u4DhcpMode == 4))
        {
            if (nmhGetFirstIndexFsWtpDhcpConfigTable (&u4WtpNextProfileId)
                == SNMP_SUCCESS)
            {
                do
                {
                    if (nmhTestv2FsWtpDhcpRelayStatus (&u4ErrorCode,
                                                       u4WtpNextProfileId,
                                                       u4DhcpRelayStatus) ==
                        SNMP_SUCCESS)
                    {
                        if (nmhSetFsWtpDhcpRelayStatus
                            (u4WtpNextProfileId,
                             u4DhcpRelayStatus) == SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nValidation of Input - failed");
                            return CLI_FAILURE;
                        }
                    }
                    u4WtpProfileId = u4WtpNextProfileId;
                }
                while (nmhGetNextIndexFsWtpDhcpConfigTable (u4WtpProfileId,
                                                            &u4WtpNextProfileId)
                       == SNMP_SUCCESS);
            }
        }
    }

    if ((pu1ProfileName == NULL) && (pu4Allprofiles == NULL))
    {
        if ((u4DhcpMode == 1) || (u4DhcpMode == 2))
        {
            if (nmhTestv2FsWlanDhcpServerStatus (&u4ErrorCode,
                                                 (INT4) u4DhcpServerStatus) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nValidation of Input - failed");
                return CLI_FAILURE;
            }
            if (nmhSetFsWlanDhcpServerStatus ((INT4) u4DhcpServerStatus)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nConfiguration of Input - failed");
                return CLI_FAILURE;
            }
        }
        else if ((u4DhcpMode == 3) || (u4DhcpMode == 4))
        {
            if (nmhTestv2FsWlanDhcpRelayStatus (&u4ErrorCode,
                                                (INT4) u4DhcpRelayStatus) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nValidation of Input - failed");
                return CLI_FAILURE;
            }
            if (nmhSetFsWlanDhcpRelayStatus ((INT4) u4DhcpRelayStatus) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nConfiguration of Input - failed");
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  FsWssDhcpSrvNetwork
 * Description :  This function is used to configure dhcp configurations

 * Input       :  CliHandle, subnet, netmask, pool start ip, pool end ip,
 * profile name, dhcp pool index.
 * indication

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/

INT1
FsWssDhcpSrvNetwork (tCliHandle CliHandle,
                     UINT4 u4Subnet, UINT4 u4NetMask,
                     UINT4 u4IpAddrStart, UINT4 u4IpAddrEnd,
                     UINT1 *pu1ProfileName, UINT4 u4PoolIndex)
{
    UINT4               u4NextWtpProfileId = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4WtpProfileId = 0;
    INT4                i4RetValue = 0;

    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexCapwapBaseWtpProfileTable
            (&u4NextWtpProfileId) != SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        do
        {
            u4WtpProfileId = u4NextWtpProfileId;
            if (nmhGetFsDot11WtpDhcpSrvSubnetPoolRowStatus
                (u4WtpProfileId, u4PoolIndex, &i4RetValue) == SNMP_SUCCESS)
            {
                if (nmhSetFsDot11WtpDhcpSrvSubnetPoolRowStatus
                    (u4WtpProfileId, u4PoolIndex, DESTROY) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\nDeletion of pre-existing entry - Failed");
                    continue;
                }
            }
            if (nmhSetFsDot11WtpDhcpSrvSubnetPoolRowStatus
                (u4NextWtpProfileId, u4PoolIndex,
                 CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nConfiguration of input - Failed");
                continue;
            }
            if (nmhTestv2FsDot11WtpDhcpSrvSubnetSubnet
                (&u4ErrorCode, u4NextWtpProfileId, u4PoolIndex,
                 u4Subnet) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nValidation of Input - failed");
                continue;
            }
            if (nmhTestv2FsDot11WtpDhcpSrvSubnetMask
                (&u4ErrorCode, u4NextWtpProfileId, u4PoolIndex,
                 u4NetMask) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nValidation of Input - failed");
                continue;
            }
            if (nmhTestv2FsDot11WtpDhcpSrvSubnetStartIpAddress
                (&u4ErrorCode, u4NextWtpProfileId, u4PoolIndex,
                 u4IpAddrStart) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nValidation of Input - failed");
                continue;
            }
            if (nmhTestv2FsDot11WtpDhcpSrvSubnetEndIpAddress
                (&u4ErrorCode, u4NextWtpProfileId, u4PoolIndex,
                 u4IpAddrEnd) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nValidation of Input - failed");
                continue;
            }
            if (nmhSetFsDot11WtpDhcpSrvSubnetSubnet
                (u4NextWtpProfileId, u4PoolIndex, u4Subnet) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nConfiguration of Input - failed");
                continue;
            }
            if (nmhSetFsDot11WtpDhcpSrvSubnetMask
                (u4NextWtpProfileId, u4PoolIndex, u4NetMask) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nConfiguration of Input - failed");
                continue;
            }

            if (nmhSetFsDot11WtpDhcpSrvSubnetStartIpAddress
                (u4NextWtpProfileId, u4PoolIndex,
                 u4IpAddrStart) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nConfiguration of Input - failed");
                continue;
            }
            if (nmhSetFsDot11WtpDhcpSrvSubnetEndIpAddress
                (u4NextWtpProfileId, u4PoolIndex, u4IpAddrEnd) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nConfiguration of Input - failed");
                continue;
            }
            if (nmhSetFsDot11WtpDhcpSrvSubnetPoolRowStatus
                (u4NextWtpProfileId, u4PoolIndex, ACTIVE) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nConfiguration of input - Failed");
                continue;
            }
        }
        while (nmhGetNextIndexCapwapBaseWtpProfileTable
               (u4WtpProfileId, &u4NextWtpProfileId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Name Conversion - failed");
            return CLI_FAILURE;
        }
        if (nmhGetFsDot11WtpDhcpSrvSubnetPoolRowStatus
            (u4WtpProfileId, u4PoolIndex, &i4RetValue) == SNMP_SUCCESS)
        {
            if (nmhSetFsDot11WtpDhcpSrvSubnetPoolRowStatus
                (u4WtpProfileId, u4PoolIndex, DESTROY) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\nDeletion of pre-existing entry - Failed");
                return CLI_FAILURE;
            }
        }
        if (nmhSetFsDot11WtpDhcpSrvSubnetPoolRowStatus
            (u4WtpProfileId, u4PoolIndex, CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nConfiguration of input - Failed");
            return CLI_FAILURE;
        }
        if (nmhTestv2FsDot11WtpDhcpSrvSubnetSubnet
            (&u4ErrorCode, u4WtpProfileId, u4PoolIndex,
             u4Subnet) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nValidation of Input - failed");
            return CLI_FAILURE;
        }
        if (nmhTestv2FsDot11WtpDhcpSrvSubnetMask
            (&u4ErrorCode, u4WtpProfileId, u4PoolIndex,
             u4NetMask) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nValidation of Input - failed");
            return CLI_FAILURE;
        }
        if (nmhTestv2FsDot11WtpDhcpSrvSubnetStartIpAddress
            (&u4ErrorCode, u4WtpProfileId, u4PoolIndex,
             u4IpAddrStart) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nValidation of Input - failed");
            return CLI_FAILURE;
        }
        if (nmhTestv2FsDot11WtpDhcpSrvSubnetEndIpAddress
            (&u4ErrorCode, u4WtpProfileId, u4PoolIndex,
             u4IpAddrEnd) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nValidation of Input - failed");
            return CLI_FAILURE;
        }
        if (nmhSetFsDot11WtpDhcpSrvSubnetSubnet
            (u4WtpProfileId, u4PoolIndex, u4Subnet) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nConfiguration of Input - failed");
            return CLI_FAILURE;
        }
        if (nmhSetFsDot11WtpDhcpSrvSubnetMask
            (u4WtpProfileId, u4PoolIndex, u4NetMask) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nConfiguration of Input - failed");
            return CLI_FAILURE;
        }

        if (nmhSetFsDot11WtpDhcpSrvSubnetStartIpAddress
            (u4WtpProfileId, u4PoolIndex, u4IpAddrStart) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nConfiguration of Input - failed");
            return CLI_FAILURE;
        }
        if (nmhSetFsDot11WtpDhcpSrvSubnetEndIpAddress
            (u4WtpProfileId, u4PoolIndex, u4IpAddrEnd) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nConfiguration of Input - failed");
            return CLI_FAILURE;
        }
        if (nmhSetFsDot11WtpDhcpSrvSubnetPoolRowStatus
            (u4NextWtpProfileId, u4PoolIndex, ACTIVE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nConfiguration of input - Failed");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  FsWssDhcpSrvLease
 * Description :  This function is used to configure dhcp configuration's 
 * lease time 

 * Input       :  CliHandle, days, hours, minutes, Infinite, profile name, all
 * profile indication

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/

INT1
FsWssDhcpSrvLease (tCliHandle CliHandle, INT4 *pi4Days, INT4 *pi4Hours,
                   INT4 *pi4Minutes, UINT1 *pu1Infinite, UINT1 *pu1ProfileName,
                   UINT4 u4PoolIndex)
{
    UINT4               u4NextWtpProfileId = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4WtpProfileId = 0;
    INT4                i4LeaseTime = 0;
    INT4                i4RowStatus = 0;

    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexCapwapBaseWtpProfileTable
            (&u4NextWtpProfileId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (pi4Days != NULL)
        {
            i4LeaseTime = (*pi4Days) * 24 * 60 * 60;
            if (pi4Hours != NULL)
            {
                i4LeaseTime += (*pi4Hours) * 60 * 60;
            }
            if (pi4Minutes != NULL)
            {
                i4LeaseTime += (*pi4Minutes) * 60;
            }
        }
        else if (pu1Infinite != NULL)
        {
            i4LeaseTime = 0x7fffffff;
        }

        do
        {
            u4WtpProfileId = u4NextWtpProfileId;
            if (nmhGetFsDot11WtpDhcpSrvSubnetPoolRowStatus (u4NextWtpProfileId,
                                                            u4PoolIndex,
                                                            &i4RowStatus) ==
                SNMP_FAILURE)
            {
                i4RowStatus = CREATE_AND_WAIT;
            }
            else
            {
                i4RowStatus = NOT_IN_SERVICE;
            }
            if (nmhSetFsDot11WtpDhcpSrvSubnetPoolRowStatus (u4NextWtpProfileId,
                                                            u4PoolIndex,
                                                            i4RowStatus) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nConfiguration of input - Failed");
                continue;
            }
            if (nmhTestv2FsDot11WtpDhcpSrvSubnetLeaseTime
                (&u4ErrorCode, u4WtpProfileId, (INT4) u4PoolIndex,
                 i4LeaseTime) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nValidation of Input - failed");
                continue;
            }
            if (nmhSetFsDot11WtpDhcpSrvSubnetLeaseTime
                (u4WtpProfileId, (INT4) u4PoolIndex,
                 i4LeaseTime) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nConfiguration of Input - failed");
                continue;
            }
            if (nmhSetFsDot11WtpDhcpSrvSubnetPoolRowStatus
                (u4NextWtpProfileId, u4PoolIndex, ACTIVE) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nConfiguration of input - Failed");
                continue;
            }
        }
        while (nmhGetNextIndexCapwapBaseWtpProfileTable
               (u4WtpProfileId, &u4NextWtpProfileId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Name Conversion - failed");
            return CLI_FAILURE;
        }
        if (nmhSetFsDot11WtpDhcpSrvSubnetPoolRowStatus
            (u4WtpProfileId, u4PoolIndex, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nConfiguration of input - Failed");
            return CLI_FAILURE;
        }
        if (pi4Days != NULL)
        {
            i4LeaseTime = (*pi4Days) * 24 * 60 * 60;
            if (pi4Hours != NULL)
            {
                i4LeaseTime += (*pi4Hours) * 60 * 60;
            }
            if (pi4Minutes != NULL)
            {
                i4LeaseTime += (*pi4Minutes) * 60;
            }
        }
        else if (pu1Infinite != NULL)
        {
            i4LeaseTime = 0x7fffffff;
        }
        if (nmhTestv2FsDot11WtpDhcpSrvSubnetLeaseTime
            (&u4ErrorCode, u4WtpProfileId, (INT4) u4PoolIndex,
             i4LeaseTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nValidation of Input - failed");
            return CLI_FAILURE;
        }
        if (nmhSetFsDot11WtpDhcpSrvSubnetLeaseTime
            (u4WtpProfileId, (INT4) u4PoolIndex, i4LeaseTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nConfiguration of Input - failed");
            return CLI_FAILURE;
        }
        if (nmhSetFsDot11WtpDhcpSrvSubnetPoolRowStatus
            (u4WtpProfileId, u4PoolIndex, ACTIVE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nConfiguration of input - Failed");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

INT1
FsWssDhcpRelayNextip (tCliHandle CliHandle, UINT4 u4NextIpAddr)
{
    UINT4               u4ErrorCode = 0;
    if (nmhTestv2FsWlanDhcpNextSrvIpAddr (&u4ErrorCode, u4NextIpAddr) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nValidation of Input - failed");
        return CLI_FAILURE;
    }
    if (nmhSetFsWlanDhcpNextSrvIpAddr (u4NextIpAddr) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nConfiguration - failed");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

INT1
FsWssDefaultRouterIp (tCliHandle CliHandle, UINT4 *pu4DefaultRouterIp)
{
    UINT4               u4ErrorCode = 0;

    if (pu4DefaultRouterIp == NULL)
    {
        CliPrintf (CliHandle, "\r\nNull Inputs");
        return CLI_FAILURE;
    }
    if (nmhTestv2FsWlanDefaultRouterIpAddr (&u4ErrorCode, *pu4DefaultRouterIp)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nValidation of Input - failed");
        return CLI_FAILURE;
    }
    if (nmhSetFsWlanDefaultRouterIpAddr (*pu4DefaultRouterIp) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nConfiguration of Input - failed");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

INT1
FsWssDnsServerIp (tCliHandle CliHandle, UINT4 *pu4DnsServerIp)
{
    UINT4               u4ErrorCode = 0;

    if (pu4DnsServerIp == NULL)
    {
        CliPrintf (CliHandle, "\r\nNull Inputs");
        return CLI_FAILURE;
    }
    if (nmhTestv2FsWlanDNSServerIpAddr (&u4ErrorCode, *pu4DnsServerIp) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nValidation of Input - failed");
        return CLI_FAILURE;
    }
    if (nmhSetFsWlanDNSServerIpAddr (*pu4DnsServerIp) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nConfiguration of Input - failed");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

INT1
FsWssDhcpwlanRadioip (tCliHandle CliHandle, UINT4 *pu4IpAddr, UINT4 *pu4Mask,
                      UINT1 *pu1ProfileName, UINT4 *pu4RadioId,
                      UINT4 *pu4WlanId)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RetValue = 0;
    INT4                i4RadioIfIndex = 0;
    if ((pu4IpAddr == NULL) || (pu4Mask == NULL) || (pu1ProfileName == NULL) ||
        (pu4RadioId == NULL) || (pu4WlanId == NULL))
    {
        CliPrintf (CliHandle, "\r\nNull Inputs");
        return CLI_FAILURE;
    }
    if (CapwapGetWtpProfileIdFromProfileName
        (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nProfile Name Conversion - failed");
        return CLI_FAILURE;
    }
    u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);
    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
        (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_11AC_RADIOINDEX_ERROR);
        CliPrintf (CliHandle, "\r\nRadioId Conversion - failed");
        return CLI_FAILURE;
    }
    if (nmhGetFsWlanRadioIfRowStatus (i4RadioIfIndex, *pu4WlanId, &i4RetValue)
        == SNMP_SUCCESS)
    {
        if (nmhSetFsWlanRadioIfRowStatus
            (i4RadioIfIndex, *pu4WlanId, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nConfiguration of input - failed");
            return CLI_FAILURE;
        }
        if (nmhSetFsWlanRadioIfIpAddr (i4RadioIfIndex, *pu4WlanId, *pu4IpAddr)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nConfiguration of input - failed");
            return CLI_FAILURE;
        }
        if (nmhSetFsWlanRadioIfIpSubnetMask
            (i4RadioIfIndex, *pu4WlanId, *pu4Mask) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nConfiguration of input - failed");
            return CLI_FAILURE;
        }
        if (nmhSetFsWlanRadioIfRowStatus (i4RadioIfIndex, *pu4WlanId, ACTIVE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nConfiguration of input - failed");
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsWlanRadioIfRowStatus (&u4ErrorCode, i4RadioIfIndex,
                                             *pu4WlanId, CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nValidation of input - failed");
            return CLI_FAILURE;
        }
        if (nmhTestv2FsWlanRadioIfIpAddr (&u4ErrorCode,
                                          i4RadioIfIndex, *pu4WlanId,
                                          *pu4IpAddr) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nValidation of input - failed");
            return CLI_FAILURE;
        }
        if (nmhTestv2FsWlanRadioIfIpSubnetMask (&u4ErrorCode, i4RadioIfIndex,
                                                *pu4WlanId,
                                                *pu4Mask) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nValidation of input - failed");
            return CLI_FAILURE;
        }
        if (nmhSetFsWlanRadioIfRowStatus
            (i4RadioIfIndex, *pu4WlanId, CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nConfiguration of input - failed");
            return CLI_FAILURE;
        }
        if (nmhSetFsWlanRadioIfIpAddr (i4RadioIfIndex, *pu4WlanId, *pu4IpAddr)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nConfiguration of input - failed");
            return CLI_FAILURE;
        }
        if (nmhSetFsWlanRadioIfIpSubnetMask
            (i4RadioIfIndex, *pu4WlanId, *pu4Mask) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nConfiguration of input - failed");
            return CLI_FAILURE;
        }
        if (nmhSetFsWlanRadioIfRowStatus (i4RadioIfIndex, *pu4WlanId, ACTIVE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nConfiguration of input - failed");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

INT1
FsWssNoDhcpwlanRadioip (tCliHandle CliHandle, UINT4 *pu4IpAddr, UINT4 *pu4Mask,
                        UINT1 *pu1ProfileName, UINT4 *pu4RadioId,
                        UINT4 *pu4WlanId)
{
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RetValue = 0;
    INT4                i4RadioIfIndex = 0;
    if ((pu4IpAddr == NULL) || (pu4Mask == NULL) || (pu1ProfileName == NULL) ||
        (pu4RadioId == NULL) || (pu4WlanId == NULL))
    {
        CliPrintf (CliHandle, "\r\nNull Inputs");
        return CLI_FAILURE;
    }

    if (CapwapGetWtpProfileIdFromProfileName
        (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nProfile Name Conversion - failed");
        return CLI_FAILURE;
    }
    u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);
    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
        (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_11AC_RADIOINDEX_ERROR);
        CliPrintf (CliHandle, "\r\nRadioId Conversion - failed");
        return CLI_FAILURE;
    }
    if (nmhGetFsWlanRadioIfRowStatus (i4RadioIfIndex, *pu4WlanId, &i4RetValue)
        == SNMP_SUCCESS)
    {
        if (nmhSetFsWlanRadioIfRowStatus (i4RadioIfIndex, *pu4WlanId, DESTROY)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nDeletion - failed");
            return CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, " Entry Not Found");
        return CLI_SUCCESS;
    }
    return CLI_SUCCESS;
}

INT1
FsWssRouteConfig (tCliHandle CliHandle, UINT4 *pu4Subnet, UINT4 *pu4Netmask,
                  UINT4 *pu4Gateway, UINT1 *pu1ProfileName)
{
    UINT4               u4WtpProfileId = 0;
    UINT4               u4WtpNextProfileId = 0;
    INT4                i4RetValue = 0;
    UINT4               u4ErrorCode = 0;
    if ((pu4Subnet == NULL) || (pu4Netmask == NULL) || (pu4Gateway == NULL))
    {
        CliPrintf (CliHandle, "\r\nNull inputs");
        return CLI_FAILURE;
    }
    if (pu1ProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Name Conversion - failed");
            return CLI_FAILURE;
        }
        if (nmhGetFsWtpIpRouteConfigRowStatus
            (u4WtpProfileId, *pu4Subnet, *pu4Netmask, *pu4Gateway,
             &i4RetValue) == SNMP_SUCCESS)
        {
            if (nmhSetFsWtpIpRouteConfigRowStatus
                (u4WtpProfileId, *pu4Subnet, *pu4Netmask, *pu4Gateway,
                 NOT_READY) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nConfiguration of input - failed");
                return CLI_FAILURE;
            }
            if (nmhTestv2FsWtpIpRouteConfigRowStatus
                (&u4ErrorCode, u4WtpProfileId, *pu4Subnet, *pu4Netmask,
                 *pu4Gateway, ACTIVE) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nValidation of input - failed");
                return CLI_FAILURE;
            }

            if (nmhSetFsWtpIpRouteConfigRowStatus
                (u4WtpProfileId, *pu4Subnet, *pu4Netmask, *pu4Gateway,
                 ACTIVE) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nConfiguration of input - failed");
                return CLI_FAILURE;
            }
        }
        else
        {
            if (nmhTestv2FsWtpIpRouteConfigRowStatus
                (&u4ErrorCode, u4WtpProfileId, *pu4Subnet, *pu4Netmask,
                 *pu4Gateway, CREATE_AND_GO) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nValidation of input - failed");
                return CLI_FAILURE;
            }

            if (nmhSetFsWtpIpRouteConfigRowStatus
                (u4WtpProfileId, *pu4Subnet, *pu4Netmask, *pu4Gateway,
                 CREATE_AND_GO) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nConfiguration of input - failed");
                return CLI_FAILURE;
            }
        }
    }
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpNextProfileId)
            == SNMP_SUCCESS)
        {
            do
            {
                if (nmhTestv2FsWtpIpRouteConfigRowStatus (&u4ErrorCode,
                                                          u4WtpNextProfileId,
                                                          *pu4Subnet,
                                                          *pu4Netmask,
                                                          *pu4Gateway,
                                                          CREATE_AND_GO) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\nValidation of input - failed");
                    return CLI_FAILURE;
                }

                if (nmhSetFsWtpIpRouteConfigRowStatus
                    (u4WtpNextProfileId, *pu4Subnet, *pu4Netmask, *pu4Gateway,
                     CREATE_AND_GO) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\nConfiguration of input - failed");
                    return CLI_FAILURE;
                }
                u4WtpProfileId = u4WtpNextProfileId;
            }
            while (nmhGetNextIndexCapwapBaseWtpProfileTable
                   (u4WtpProfileId, &u4WtpNextProfileId) == SNMP_SUCCESS);
        }
    }
    return CLI_SUCCESS;
}

INT1
FsWssNoRouteConfig (tCliHandle CliHandle, UINT4 *pu4Subnet, UINT4 *pu4Netmask,
                    UINT4 *pu4Gateway, UINT1 *pu1ProfileName)
{
    UINT4               u4WtpProfileId = 0;
    UINT4               u4WtpNextProfileId = 0;
    INT4                i4RetValue = 0;
    UINT4               u4TableSubnet = 0;
    UINT4               u4TableNextSubnet = 0;
    UINT4               u4TableMask = 0;
    UINT4               u4TableNextMask = 0;
    UINT4               u4TableGateway = 0;
    UINT1               u1Flag = OSIX_FALSE;

    if ((pu4Subnet == NULL) || (pu4Netmask == NULL) || (pu4Gateway == NULL))
    {
        CliPrintf (CliHandle, "\r\nNull Inputs");
        return CLI_FAILURE;
    }

    if (pu1ProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Name Conversion - failed");
            return CLI_FAILURE;
        }
        if (pu1ProfileName != NULL)
        {
            if (nmhGetFsWtpIpRouteConfigRowStatus
                (u4WtpProfileId, *pu4Subnet, *pu4Netmask, *pu4Gateway,
                 &i4RetValue) == SNMP_SUCCESS)
            {
                if (nmhSetFsWtpIpRouteConfigRowStatus
                    (u4WtpProfileId, *pu4Subnet, *pu4Netmask, *pu4Gateway,
                     DESTROY) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\nDeletion of input - failed");
                    return CLI_FAILURE;
                }
            }
            else
            {
                CliPrintf (CliHandle, "Entry Not Found");
                return CLI_SUCCESS;
            }
        }
    }
    if (pu1ProfileName == NULL)
    {
        u4TableSubnet = *pu4Subnet;
        u4TableMask = *pu4Netmask;
        u4TableGateway = *pu4Gateway;

        while (nmhGetNextIndexCapwapBaseWtpProfileTable
               (u4WtpProfileId, &u4WtpNextProfileId) == SNMP_SUCCESS)
        {
            u4WtpProfileId = u4WtpNextProfileId;

            if (nmhGetFsWtpIpRouteConfigRowStatus
                (u4WtpProfileId, *pu4Subnet, *pu4Netmask, *pu4Gateway,
                 &i4RetValue) == SNMP_SUCCESS)
            {
                if (nmhSetFsWtpIpRouteConfigRowStatus
                    (u4WtpProfileId, *pu4Subnet, *pu4Netmask, *pu4Gateway,
                     DESTROY) == SNMP_FAILURE)
                {
                    continue;
                }
            }
            u1Flag = OSIX_TRUE;
        }
        if (u1Flag == OSIX_FALSE)
        {
            CliPrintf (CliHandle, "Entry Not Found");
        }
    }
    UNUSED_PARAM (u4TableGateway);
    UNUSED_PARAM (u4TableNextMask);
    UNUSED_PARAM (u4TableMask);
    UNUSED_PARAM (u4TableNextSubnet);
    UNUSED_PARAM (u4TableSubnet);
    return CLI_SUCCESS;
}

INT1
FsWssDhcpSrvPoolOption (tCliHandle CliHandle,
                        INT4 *pu4Option,
                        UINT4 *pu4Optionvalue,
                        UINT1 *pu1ProfileName, UINT4 u4PoolId)
{
    UINT4               u4NextWtpProfileId = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4WtpProfileId = 0;
    INT4                i4RowStatus = 0;

    if ((pu4Option == NULL) || (pu4Optionvalue == NULL))
    {
        CliPrintf (CliHandle, "Null Inputs");
        return CLI_FAILURE;
    }

    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexCapwapBaseWtpProfileTable
            (&u4NextWtpProfileId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4WtpProfileId = u4NextWtpProfileId;
            if (nmhGetFsDot11WtpDhcpSrvSubnetPoolRowStatus (u4NextWtpProfileId,
                                                            u4PoolId,
                                                            &i4RowStatus) ==
                SNMP_FAILURE)
            {
                i4RowStatus = CREATE_AND_WAIT;
            }
            else
            {
                i4RowStatus = NOT_IN_SERVICE;
            }
            if (nmhSetFsDot11WtpDhcpSrvSubnetPoolRowStatus (u4NextWtpProfileId,
                                                            u4PoolId,
                                                            i4RowStatus) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nConfiguration of input - Failed");
                continue;
            }
            if (*pu4Option == CLI_AP_DEFAULT_ROUTER_OPTION)
            {
                if (nmhTestv2FsDot11WtpDhcpSrvDefaultRouter
                    (&u4ErrorCode, u4WtpProfileId, u4PoolId,
                     *pu4Optionvalue) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\nConfiguration of input - Failed");
                    continue;
                }
                if (nmhSetFsDot11WtpDhcpSrvDefaultRouter
                    (u4WtpProfileId, u4PoolId, *pu4Optionvalue) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\nConfiguration of input - Failed");
                    continue;
                }
            }
            if (*pu4Option == CLI_AP_DNS_SERVER_OPTION)
            {
                if (nmhTestv2FsDot11WtpDhcpSrvDnsServerIp
                    (&u4ErrorCode, u4WtpProfileId, u4PoolId,
                     *pu4Optionvalue) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\nConfiguration of input - Failed");
                    continue;
                }
                if (nmhSetFsDot11WtpDhcpSrvDnsServerIp
                    (u4WtpProfileId, u4PoolId, *pu4Optionvalue) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\nConfiguration of input - Failed");
                    continue;
                }
            }
            if (nmhSetFsDot11WtpDhcpSrvSubnetPoolRowStatus
                (u4NextWtpProfileId, u4PoolId, ACTIVE) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nConfiguration of input - Failed");
                continue;
            }
        }
        while (nmhGetNextIndexCapwapBaseWtpProfileTable
               (u4WtpProfileId, &u4NextWtpProfileId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Name Conversion - failed");
            return CLI_FAILURE;
        }
        if (nmhSetFsDot11WtpDhcpSrvSubnetPoolRowStatus
            (u4WtpProfileId, u4PoolId, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nConfiguration of input - Failed");
            return CLI_FAILURE;
        }
        if (*pu4Option == CLI_AP_DEFAULT_ROUTER_OPTION)
        {
            if (nmhTestv2FsDot11WtpDhcpSrvDefaultRouter
                (&u4ErrorCode, u4WtpProfileId, u4PoolId,
                 *pu4Optionvalue) == SNMP_SUCCESS)
            {
                if (nmhSetFsDot11WtpDhcpSrvDefaultRouter
                    (u4WtpProfileId, u4PoolId, *pu4Optionvalue) == SNMP_SUCCESS)
                {
                    if (nmhSetFsDot11WtpDhcpSrvSubnetPoolRowStatus
                        (u4WtpProfileId, u4PoolId, ACTIVE) == SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nConfiguration of input - Failed");
                        return CLI_FAILURE;
                    }
                    return CLI_SUCCESS;
                }
                else
                {
                    CliPrintf (CliHandle,
                               "Error in Configuration of Default Router");
                    return CLI_FAILURE;
                }

            }
            else
            {
                CliPrintf (CliHandle, "Validation failed for Default Router");
                return CLI_FAILURE;
            }
        }
        else if (*pu4Option == CLI_AP_DNS_SERVER_OPTION)
        {
            if (nmhTestv2FsDot11WtpDhcpSrvDnsServerIp
                (&u4ErrorCode, u4WtpProfileId, u4PoolId,
                 *pu4Optionvalue) == SNMP_SUCCESS)
            {
                if (nmhSetFsDot11WtpDhcpSrvDnsServerIp
                    (u4WtpProfileId, u4PoolId, *pu4Optionvalue) == SNMP_SUCCESS)
                {
                    if (nmhSetFsDot11WtpDhcpSrvSubnetPoolRowStatus
                        (u4WtpProfileId, u4PoolId, ACTIVE) == SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nConfiguration of input - Failed");
                        return CLI_FAILURE;
                    }
                    return CLI_SUCCESS;
                    return CLI_SUCCESS;
                }
                else
                {
                    CliPrintf (CliHandle,
                               "Error in Configuration of Dns Server");
                    return CLI_FAILURE;
                }

            }
            else
            {
                CliPrintf (CliHandle, "Validation failed for Dns Server");
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

INT1
FsWssApNatPool (tCliHandle CliHandle, UINT4 u4NatPoolid)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);

    {
        SPRINTF ((CHR1 *) au1Cmd, "%s%d", CLI_NAT_POOL, u4NatPoolid);
        if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

INT1
FsWssApNatMapping (tCliHandle CliHandle, UINT1 *pu1ProfileName,
                   INT4 i4FsifMainWanType, UINT4 u4NatIndex)
{
    INT4                i4RowStatus = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4WtpNextProfileId = 0;

    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpNextProfileId)
            == SNMP_SUCCESS)
        {
            do
            {
                u4WtpProfileId = u4WtpNextProfileId;

                if (nmhGetFsWtpNATConfigRowStatus
                    (u4WtpProfileId, u4NatIndex, &i4RowStatus) == SNMP_FAILURE)
                {
                    if (nmhSetFsWtpNATConfigRowStatus
                        (u4WtpProfileId, u4NatIndex,
                         CREATE_AND_WAIT) == SNMP_FAILURE)
                    {
                        continue;
                    }
                    if (nmhTestv2FsWtpIfMainWanType
                        (&u4ErrorCode, u4WtpProfileId, u4NatIndex,
                         i4FsifMainWanType) == SNMP_FAILURE)
                    {
                        continue;
                    }
                    if (nmhSetFsWtpIfMainWanType
                        (u4WtpProfileId, u4NatIndex,
                         i4FsifMainWanType) == SNMP_FAILURE)
                    {
                        continue;
                    }
                    if (nmhSetFsWtpNATConfigRowStatus
                        (u4WtpProfileId, u4NatIndex, ACTIVE) == SNMP_FAILURE)
                    {
                        continue;
                    }
                }
                else
                {
                    if (nmhTestv2FsWtpIfMainWanType
                        (&u4ErrorCode, u4WtpProfileId, u4NatIndex,
                         i4FsifMainWanType) == SNMP_FAILURE)
                    {
                        continue;
                    }
                    if (nmhSetFsWtpIfMainWanType
                        (u4WtpProfileId, u4NatIndex,
                         i4FsifMainWanType) == SNMP_FAILURE)
                    {
                        continue;
                    }
                    if (nmhSetFsWtpNATConfigRowStatus
                        (u4WtpProfileId, u4NatIndex, ACTIVE) == SNMP_FAILURE)
                    {
                        continue;
                    }
                }
            }
            while (nmhGetNextIndexCapwapBaseWtpProfileTable
                   (u4WtpProfileId, &u4WtpNextProfileId) == SNMP_SUCCESS);
        }
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "Profile Name Conversion - failed");
            return CLI_FAILURE;
        }
        if (nmhSetFsWtpNATConfigRowStatus
            (u4WtpProfileId, u4NatIndex, CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Configuration of input - failed");
            return CLI_FAILURE;
        }
        if (nmhTestv2FsWtpIfMainWanType
            (&u4ErrorCode, u4WtpProfileId, u4NatIndex,
             i4FsifMainWanType) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Validation of input - failed");
            return CLI_FAILURE;
        }
        if (nmhSetFsWtpIfMainWanType
            (u4WtpProfileId, u4NatIndex, i4FsifMainWanType) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Configuration of input - failed");
            return CLI_FAILURE;
        }
        if (nmhSetFsWtpNATConfigRowStatus (u4WtpProfileId, u4NatIndex, ACTIVE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nFailed to set row status active");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

INT1
FsWssShowAPDhcpConfig (tCliHandle CliHandle, UINT1 *pu1ProfileName)
{
    INT4                i4RetValFsWtpDhcpServerStatus = 0;
    INT4                i4RetValFsWtpDhcpRelayStatus = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4WtpNextProfileId = 0;
    UINT1               au1WtpName[256];
    tSNMP_OCTET_STRING_TYPE WtpProfileName;

    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1WtpName, 0, 256);

    WtpProfileName.pu1_OctetList = au1WtpName;
    if (pu1ProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_WSS_LR_FIREWALL_ERRROR);
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, "\r\nAP Profile          : %s\n", pu1ProfileName);
        if (nmhGetFsWtpDhcpServerStatus (u4WtpProfileId,
                                         &i4RetValFsWtpDhcpServerStatus) ==
            SNMP_SUCCESS)
        {
            if (i4RetValFsWtpDhcpServerStatus == 1)
                CliPrintf (CliHandle, "\r\nDhcp Server         : Enabled");
            else if (i4RetValFsWtpDhcpServerStatus == 0)
                CliPrintf (CliHandle, "\r\nDhcp Server         : Disabled");
        }
        if (nmhGetFsWtpDhcpRelayStatus (u4WtpProfileId,
                                        &i4RetValFsWtpDhcpRelayStatus) ==
            SNMP_SUCCESS)
        {
            if (i4RetValFsWtpDhcpRelayStatus == 1)
                CliPrintf (CliHandle, "\r\nDhcp Relay          : Enabled");
            else if (i4RetValFsWtpDhcpRelayStatus == 0)
                CliPrintf (CliHandle, "\r\nDhcp Relay          : Disabled");
        }
        CliPrintf (CliHandle, "\r\n");
    }
    else
    {
        if (nmhGetFirstIndexFsWtpDhcpConfigTable (&u4WtpNextProfileId)
            == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r\nAp Profile Name    Dhcp Server     Dhcp Relay");
            CliPrintf (CliHandle,
                       "\r\n---------------    -----------     ----------");
            CliPrintf (CliHandle, "\r\n");
            while (u4WtpProfileId != u4WtpNextProfileId)
            {
                if (nmhGetCapwapBaseWtpProfileName
                    (u4WtpNextProfileId, &WtpProfileName) != SNMP_SUCCESS)
                {
                    return CLI_FAILURE;
                }

                CliPrintf (CliHandle, "\r\n%+15s",
                           WtpProfileName.pu1_OctetList);
                if (nmhGetFsWtpDhcpServerStatus
                    (u4WtpNextProfileId,
                     &i4RetValFsWtpDhcpServerStatus) == SNMP_SUCCESS)
                {
                    if (i4RetValFsWtpDhcpServerStatus == 1)
                    {
                        CliPrintf (CliHandle, "%+15s", "Enabled");
                    }
                    else if (i4RetValFsWtpDhcpServerStatus == 0)
                    {
                        CliPrintf (CliHandle, "%+15s", "Disabled");
                    }
                }
                if (nmhGetFsWtpDhcpRelayStatus (u4WtpNextProfileId,
                                                &i4RetValFsWtpDhcpRelayStatus)
                    == SNMP_SUCCESS)
                {
                    if (i4RetValFsWtpDhcpRelayStatus == 1)
                    {
                        CliPrintf (CliHandle, "%+15s", "Enabled");
                    }
                    else if (i4RetValFsWtpDhcpRelayStatus == 0)
                    {
                        CliPrintf (CliHandle, "%+15s", "Disabled");
                    }
                }
                CliPrintf (CliHandle, "\r\n");
                u4WtpProfileId = u4WtpNextProfileId;
                if (nmhGetNextIndexFsWtpDhcpConfigTable (u4WtpProfileId,
                                                         &u4WtpNextProfileId) ==
                    SNMP_SUCCESS)
                {
                    continue;
                }
            }
        }
    }
    return CLI_SUCCESS;
}

INT1
FsWssShowAPFirewallConfig (tCliHandle CliHandle, UINT1 *pu1ProfileName)
{
    INT4                i4RetValFsWtpFirewallStatus = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4WtpNextProfileId = 0;
    UINT1               au1WtpName[256];
    tSNMP_OCTET_STRING_TYPE WtpProfileName;

    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1WtpName, 0, 256);

    WtpProfileName.pu1_OctetList = au1WtpName;
    if (pu1ProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Name Conversion - failed");
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, "\r\nAP Profile          : %s\n", pu1ProfileName);
        CliPrintf (CliHandle, "---------------------------------------------");
        if (nmhGetFsWtpFirewallStatus (u4WtpProfileId,
                                       &i4RetValFsWtpFirewallStatus) ==
            SNMP_SUCCESS)
        {
            if (i4RetValFsWtpFirewallStatus == 1)
            {
                CliPrintf (CliHandle, "\r\nAp Firewall - Enabled\r\n");
            }
            else if (i4RetValFsWtpFirewallStatus == 0)
            {
                CliPrintf (CliHandle, "\r\nAp Firewall - Disabled\r\n");
            }
        }
    }
    else
    {
        if (nmhGetFirstIndexFsWtpFirewallConfigTable (&u4WtpNextProfileId)
            == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nAp Profile Name       Firewall\r");
            CliPrintf (CliHandle, "\r\n---------------       --------\r");
            CliPrintf (CliHandle, "\r\n");
            while (u4WtpProfileId != u4WtpNextProfileId)
            {
                if (nmhGetCapwapBaseWtpProfileName
                    (u4WtpNextProfileId, &WtpProfileName) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nProfile Name Re-Conversion - Failed");
                    return CLI_FAILURE;
                }

                CliPrintf (CliHandle, "\r\n%+15s",
                           WtpProfileName.pu1_OctetList);
                if (nmhGetFsWtpFirewallStatus
                    (u4WtpNextProfileId,
                     &i4RetValFsWtpFirewallStatus) == SNMP_SUCCESS)
                {
                    if (i4RetValFsWtpFirewallStatus == 1)
                        CliPrintf (CliHandle, "%+15s", "Enabled");
                    else if (i4RetValFsWtpFirewallStatus == 0)
                        CliPrintf (CliHandle, "%+15s", "Disabled");
                    CliPrintf (CliHandle, "\r\n");
                }
                u4WtpProfileId = u4WtpNextProfileId;
                if (nmhGetNextIndexFsWtpFirewallConfigTable (u4WtpProfileId,
                                                             &u4WtpNextProfileId)
                    == SNMP_SUCCESS)
                {
                    continue;
                }
            }
        }
    }
    return CLI_SUCCESS;
}

INT1
FsWssShowAPIpRoute (tCliHandle CliHandle, UINT1 *pu1ProfileName)
{
    UINT4               u4WtpNextProfileId = 0;
    UINT4               u4WtpIpRouteConfigSubnet = 0;
    UINT4               u4GivenProfileId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4WtpNextIpRouteConfigSubnet = 0;
    UINT4               u4WtpIpRouteConfigNetMask = 0;
    UINT4               u4WtpNextIpRouteConfigNetMask = 0;
    UINT4               u4WtpIpRouteConfigGateway = 0;
    UINT4               u4WtpNextIpRouteConfigGateway = 0;
    UINT1               au1WtpName[256];
    tSNMP_OCTET_STRING_TYPE WtpProfileName;
    CHR1               *pu1IpAddr = NULL;

    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1WtpName, 0, 256);

    WtpProfileName.pu1_OctetList = au1WtpName;
    if (pu1ProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Name Conversion - failed");
            return CLI_FAILURE;
        }
        u4GivenProfileId = u4WtpProfileId;
        if (nmhGetFirstIndexFsWtpIpRouteConfigTable (&u4WtpNextProfileId,
                                                     &u4WtpNextIpRouteConfigSubnet,
                                                     &u4WtpNextIpRouteConfigNetMask,
                                                     &u4WtpNextIpRouteConfigGateway)
            == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r\nAP Name           Subnet          Netmask          Gateway");
            CliPrintf (CliHandle,
                       "\r\n-------           ------          -------          -------");
            CliPrintf (CliHandle, "\r\n");

            do
            {
                u4WtpProfileId = u4WtpNextProfileId;
                u4WtpIpRouteConfigSubnet = u4WtpNextIpRouteConfigSubnet;
                u4WtpIpRouteConfigNetMask = u4WtpNextIpRouteConfigNetMask;
                u4WtpIpRouteConfigGateway = u4WtpNextIpRouteConfigGateway;
                if (u4GivenProfileId == u4WtpNextProfileId)
                {
                    if (nmhGetCapwapBaseWtpProfileName
                        (u4WtpNextProfileId, &WtpProfileName) == SNMP_SUCCESS)
                    {

                        CliPrintf (CliHandle, "%+7s",
                                   WtpProfileName.pu1_OctetList);
                        CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                                   u4WtpNextIpRouteConfigSubnet);
                        CliPrintf (CliHandle, "%+17s", pu1IpAddr);
                        CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                                   u4WtpNextIpRouteConfigNetMask);
                        CliPrintf (CliHandle, "%+17s", pu1IpAddr);
                        CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                                   u4WtpNextIpRouteConfigGateway);
                        CliPrintf (CliHandle, "%+17s", pu1IpAddr);
                        CliPrintf (CliHandle, "\r\n");
                    }
                }
            }
            while (nmhGetNextIndexFsWtpIpRouteConfigTable (u4WtpProfileId,
                                                           &u4WtpNextProfileId,
                                                           u4WtpIpRouteConfigSubnet,
                                                           &u4WtpNextIpRouteConfigSubnet,
                                                           u4WtpIpRouteConfigNetMask,
                                                           &u4WtpNextIpRouteConfigNetMask,
                                                           u4WtpIpRouteConfigGateway,
                                                           &u4WtpNextIpRouteConfigGateway)
                   == SNMP_SUCCESS);
        }
    }
    else
    {
        if (nmhGetFirstIndexFsWtpIpRouteConfigTable (&u4WtpNextProfileId,
                                                     &u4WtpNextIpRouteConfigSubnet,
                                                     &u4WtpNextIpRouteConfigNetMask,
                                                     &u4WtpNextIpRouteConfigGateway)
            == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r\nAP Name         Subnet            Netmask           Gateway");
            CliPrintf (CliHandle,
                       "\r\n-------         ------            -------           -------\r\n");

            do
            {
                u4WtpProfileId = u4WtpNextProfileId;
                u4WtpIpRouteConfigSubnet = u4WtpNextIpRouteConfigSubnet;
                u4WtpIpRouteConfigNetMask = u4WtpNextIpRouteConfigNetMask;
                u4WtpIpRouteConfigGateway = u4WtpNextIpRouteConfigGateway;

                if (nmhGetCapwapBaseWtpProfileName
                    (u4WtpNextProfileId, &WtpProfileName) == SNMP_SUCCESS)
                {

                    CliPrintf (CliHandle, "%+7s", WtpProfileName.pu1_OctetList);
                    CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                               u4WtpNextIpRouteConfigSubnet);
                    CliPrintf (CliHandle, "%17s", pu1IpAddr);
                    CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                               u4WtpNextIpRouteConfigNetMask);
                    CliPrintf (CliHandle, "%17s", pu1IpAddr);
                    CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                               u4WtpNextIpRouteConfigGateway);
                    CliPrintf (CliHandle, "%17s", pu1IpAddr);
                    CliPrintf (CliHandle, "\r\n");
                }
            }
            while (nmhGetNextIndexFsWtpIpRouteConfigTable (u4WtpProfileId,
                                                           &u4WtpNextProfileId,
                                                           u4WtpIpRouteConfigSubnet,
                                                           &u4WtpNextIpRouteConfigSubnet,
                                                           u4WtpIpRouteConfigNetMask,
                                                           &u4WtpNextIpRouteConfigNetMask,
                                                           u4WtpIpRouteConfigGateway,
                                                           &u4WtpNextIpRouteConfigGateway)
                   == SNMP_SUCCESS);

        }
    }
    return CLI_SUCCESS;
}

INT1
FsWssShowAPDhcpSrvPool (tCliHandle CliHandle, INT4 *pi4DhcpPoolId,
                        UINT1 *pu1AllPools, UINT1 *pu1ProfileName)
{
    tSNMP_OCTET_STRING_TYPE ProfileName;
    UINT1               au1ProfileName[32];
    UINT4               u4DhcpSrvSubnetSubnet = 0;
    UINT4               u4DhcpSrvSubnetMask = 0;
    UINT4               u4DhcpSrvSubnetStartIpAddress = 0;
    UINT4               u4DhcpSrvSubnetEndIpAddress = 0;
    INT4                i4DhcpSrvSubnetLeaseTime = 0;
    UINT4               u4DhcpSrvDefaultRouter = 0;
    UINT4               u4DhcpSrvDnsServerIp = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4WtpNextProfileId = 0;
    UINT4               u4GivenProfileId = 0;
    INT4                i4NextDhcpPoolId = 0;
    INT4                i4DhcpPoolId = 0;
    CHR1               *pu1IpAddr = NULL;

    MEMSET (au1ProfileName, 0, 32);
    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    ProfileName.pu1_OctetList = au1ProfileName;

    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable
            (&u4WtpNextProfileId, &i4NextDhcpPoolId) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nNo entry found\r\n");
            return CLI_SUCCESS;
        }

        CliPrintf (CliHandle, "\r\n%s%+12s%+14s%+17s%+17s%+17s%+17s%+17s%+17s",
                   "Pool Id", "    AP Name", "Subnet", "Netmask", "Start IP",
                   "End IP", "Gateway", "Dns", "Lease Time");
        CliPrintf (CliHandle, "\r\n%s%+12s%+14s%+17s%+17s%+17s%+17s%+17s%+17s",
                   "-------", "    -------", "------", "-------", "--------",
                   "------", "-------", "---", "----------");
        CliPrintf (CliHandle, "\r\n");
        do
        {
            u4WtpProfileId = u4WtpNextProfileId;
            i4DhcpPoolId = i4NextDhcpPoolId;

            if (pi4DhcpPoolId != NULL)
            {
                if (*pi4DhcpPoolId != i4NextDhcpPoolId)
                {
                    continue;
                }
            }
            CliPrintf (CliHandle, "\r\n%2d", i4NextDhcpPoolId);
            if (nmhGetCapwapBaseWtpProfileName (u4WtpNextProfileId,
                                                &ProfileName) == SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "    %10s", au1ProfileName);
            }
            if (nmhGetFsDot11WtpDhcpSrvSubnetSubnet
                (u4WtpNextProfileId, i4NextDhcpPoolId,
                 &u4DhcpSrvSubnetSubnet) == SNMP_SUCCESS)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4DhcpSrvSubnetSubnet);
                CliPrintf (CliHandle, "%+20s", pu1IpAddr);
            }
            if (nmhGetFsDot11WtpDhcpSrvSubnetMask
                (u4WtpNextProfileId, i4NextDhcpPoolId,
                 &u4DhcpSrvSubnetMask) == SNMP_SUCCESS)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4DhcpSrvSubnetMask);
                CliPrintf (CliHandle, "%+17s", pu1IpAddr);
            }
            if (nmhGetFsDot11WtpDhcpSrvSubnetStartIpAddress
                (u4WtpNextProfileId, i4NextDhcpPoolId,
                 &u4DhcpSrvSubnetStartIpAddress) == SNMP_SUCCESS)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                           u4DhcpSrvSubnetStartIpAddress);
                CliPrintf (CliHandle, "%+17s", pu1IpAddr);
            }
            if (nmhGetFsDot11WtpDhcpSrvSubnetEndIpAddress
                (u4WtpNextProfileId, i4NextDhcpPoolId,
                 &u4DhcpSrvSubnetEndIpAddress) == SNMP_SUCCESS)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                           u4DhcpSrvSubnetEndIpAddress);
                CliPrintf (CliHandle, "%+17s", pu1IpAddr);
            }
            if (nmhGetFsDot11WtpDhcpSrvDefaultRouter
                (u4WtpNextProfileId, i4NextDhcpPoolId,
                 &u4DhcpSrvDefaultRouter) == SNMP_SUCCESS)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4DhcpSrvDefaultRouter);
                CliPrintf (CliHandle, "%+17s", pu1IpAddr);
            }
            if (nmhGetFsDot11WtpDhcpSrvDnsServerIp
                (u4WtpNextProfileId, i4NextDhcpPoolId,
                 &u4DhcpSrvDnsServerIp) == SNMP_SUCCESS)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4DhcpSrvDnsServerIp);
                CliPrintf (CliHandle, "%+17s", pu1IpAddr);
            }
            if (nmhGetFsDot11WtpDhcpSrvSubnetLeaseTime
                (u4WtpNextProfileId, i4NextDhcpPoolId,
                 &i4DhcpSrvSubnetLeaseTime) == SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "%11d", i4DhcpSrvSubnetLeaseTime);
            }
            CliPrintf (CliHandle, "\r\n");
        }
        while (nmhGetNextIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable
               (u4WtpProfileId, &u4WtpNextProfileId,
                i4DhcpPoolId, &i4NextDhcpPoolId) == SNMP_SUCCESS);

        return CLI_SUCCESS;
    }
    if (CapwapGetWtpProfileIdFromProfileName
        (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
    {
        CLI_SET_ERR (CLI_WSS_LR_FIREWALL_ERRROR);
        return CLI_FAILURE;
    }
    u4GivenProfileId = u4WtpProfileId;
    if (pu1AllPools == NULL)
    {
        CliPrintf (CliHandle, "\r\nAP Name             : %s\r", pu1ProfileName);
        if (pi4DhcpPoolId == NULL)
        {
            CliPrintf (CliHandle, "Null Inputs");
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, "\r\nDhcp Pool Id        : %d\r", *pi4DhcpPoolId);
        if (nmhGetFsDot11WtpDhcpSrvSubnetSubnet (u4WtpProfileId, *pi4DhcpPoolId,
                                                 &u4DhcpSrvSubnetSubnet) ==
            SNMP_SUCCESS)
        {
            CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4DhcpSrvSubnetSubnet);
            CliPrintf (CliHandle, "\r\nSubnet              : %s\r", pu1IpAddr);
        }
        if (nmhGetFsDot11WtpDhcpSrvSubnetMask (u4WtpProfileId, *pi4DhcpPoolId,
                                               &u4DhcpSrvSubnetMask) ==
            SNMP_SUCCESS)
        {
            CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4DhcpSrvSubnetMask);
            CliPrintf (CliHandle, "\r\nNetmask             : %s\r", pu1IpAddr);
        }
        if (nmhGetFsDot11WtpDhcpSrvSubnetStartIpAddress
            (u4WtpProfileId, *pi4DhcpPoolId,
             &u4DhcpSrvSubnetStartIpAddress) == SNMP_SUCCESS)
        {
            CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                       u4DhcpSrvSubnetStartIpAddress);
            CliPrintf (CliHandle, "\r\nStart IP            : %s\r", pu1IpAddr);
        }
        if (nmhGetFsDot11WtpDhcpSrvSubnetEndIpAddress
            (u4WtpProfileId, *pi4DhcpPoolId,
             &u4DhcpSrvSubnetEndIpAddress) == SNMP_SUCCESS)
        {
            CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4DhcpSrvSubnetEndIpAddress);
            CliPrintf (CliHandle, "\r\nEnd IP              : %s\r", pu1IpAddr);
        }
        if (nmhGetFsDot11WtpDhcpSrvDefaultRouter
            (u4WtpProfileId, *pi4DhcpPoolId,
             &u4DhcpSrvDefaultRouter) == SNMP_SUCCESS)
        {
            CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4DhcpSrvDefaultRouter);
            CliPrintf (CliHandle, "\r\nGateway             : %s\r", pu1IpAddr);
        }
        if (nmhGetFsDot11WtpDhcpSrvDnsServerIp (u4WtpProfileId, *pi4DhcpPoolId,
                                                &u4DhcpSrvDnsServerIp) ==
            SNMP_SUCCESS)
        {
            CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4DhcpSrvDnsServerIp);
            CliPrintf (CliHandle, "\r\nDns                 : %s\r", pu1IpAddr);
        }
        if (nmhGetFsDot11WtpDhcpSrvSubnetLeaseTime
            (u4WtpProfileId, *pi4DhcpPoolId,
             &i4DhcpSrvSubnetLeaseTime) == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nLease Time          : %d\r",
                       i4DhcpSrvSubnetLeaseTime);
        }
        CliPrintf (CliHandle, "\r\n");
    }

    UNUSED_PARAM (u4GivenProfileId);
    return CLI_SUCCESS;
}

INT1
FsWssShowAPIpConfig (tCliHandle CliHandle, UINT1 *pu1ProfileName,
                     UINT4 *pu4RadioId, UINT4 *pu4Wlanid, UINT1 *pu1Allprofiles)
{
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4RadioNextIfIndex = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4WtpNextProfileId = 0;
    UINT4               u4WlanRadioIfIpAddr = 0;
    UINT4               u4WlanRadioIfIpSubnetMask = 0;
    INT4                i4AdminStatus = 0;
    UINT1               au1WtpName[256];
    tSNMP_OCTET_STRING_TYPE WtpProfileName;
    CHR1               *pu1IpAddr = NULL;

    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1WtpName, 0, 256);

    WtpProfileName.pu1_OctetList = au1WtpName;
    if (pu1Allprofiles == NULL)
    {
        if ((pu1ProfileName == NULL) || (pu4RadioId == NULL) ||
            (pu4Wlanid == NULL))
        {
            CliPrintf (CliHandle, "Null Inputs");
            return CLI_FAILURE;
        }
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_WSS_LR_FIREWALL_ERRROR);
            return CLI_FAILURE;
        }
        u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);
        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
            (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_11AC_RADIOINDEX_ERROR);
            return CLI_FAILURE;
        }
        if (nmhGetFsWlanRadioIfIpAddr (i4RadioIfIndex, *pu4Wlanid,
                                       &u4WlanRadioIfIpAddr) == SNMP_SUCCESS)
        {
            CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4WlanRadioIfIpAddr);
            CliPrintf (CliHandle, "\r\nIpAddress           : %s", pu1IpAddr);
        }
        if (nmhGetFsWlanRadioIfIpSubnetMask (i4RadioIfIndex, *pu4Wlanid,
                                             &u4WlanRadioIfIpSubnetMask) ==
            SNMP_SUCCESS)
        {
            CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4WlanRadioIfIpSubnetMask);
            CliPrintf (CliHandle, "\r\nSubnet Mask         : %s", pu1IpAddr);
        }
        if (nmhGetFsWlanRadioIfAdminStatus (i4RadioIfIndex, *pu4Wlanid,
                                            &i4AdminStatus) == SNMP_SUCCESS)
        {
            if (i4AdminStatus == 1)
                CliPrintf (CliHandle, "\r\nAdmin Status        : up");
            else if (i4AdminStatus == 2)
                CliPrintf (CliHandle, "\r\nAdmin Status - down");
        }
    }
    else
    {
        if (nmhGetFirstIndexFsWlanRadioIfTable
            (&i4RadioNextIfIndex, &u4WtpNextProfileId) == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n%s%+9s%+17s%+17s%+14s", "Wlan Id",
                       "IfIndex", "IpAddr", "Subnet", "Admin Status");
            CliPrintf (CliHandle, "\r\n");
            do
            {

                u4WtpProfileId = u4WtpNextProfileId;
                i4RadioIfIndex = i4RadioNextIfIndex;
                CliPrintf (CliHandle, "\r\n%7d", u4WtpNextProfileId);
                CliPrintf (CliHandle, "%9d", i4RadioNextIfIndex);
                if (nmhGetFsWlanRadioIfIpAddr
                    (i4RadioNextIfIndex, u4WtpNextProfileId,
                     &u4WlanRadioIfIpAddr) == SNMP_SUCCESS)
                {
                    CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4WlanRadioIfIpAddr);
                    CliPrintf (CliHandle, "%+17s", pu1IpAddr);
                }
                if (nmhGetFsWlanRadioIfIpSubnetMask
                    (i4RadioNextIfIndex, u4WtpNextProfileId,
                     &u4WlanRadioIfIpSubnetMask) == SNMP_SUCCESS)
                {
                    CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                               u4WlanRadioIfIpSubnetMask);
                    CliPrintf (CliHandle, "%+17s", pu1IpAddr);
                }
                if (nmhGetFsWlanRadioIfAdminStatus
                    (i4RadioNextIfIndex, u4WtpNextProfileId,
                     &i4AdminStatus) == SNMP_SUCCESS)
                {
                    if (i4AdminStatus == 1)
                        CliPrintf (CliHandle, "%+14s", "up");
                    else if (i4AdminStatus == 2)
                        CliPrintf (CliHandle, "%+14s", "down");
                }
                CliPrintf (CliHandle, "\r\n");
            }
            while (nmhGetNextIndexFsWlanRadioIfTable
                   (i4RadioIfIndex, &i4RadioNextIfIndex, u4WtpProfileId,
                    &u4WtpNextProfileId) == SNMP_SUCCESS);
        }
    }
    return CLI_SUCCESS;
}

INT1
FsWssNoApNatMapping (tCliHandle CliHandle, INT4 i4NatPool,
                     UINT1 *pu1ProfileName)
{
    INT4                i4NatPoolId = 0;
    INT4                i4NatNextPoolId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4WtpNextProfileId = 0;
    UINT4               u4GivenProfileId = 0;
    u4GivenProfileId = u4WtpProfileId;

    if (pu1ProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Name Conversion - failed");
            return CLI_FAILURE;
        }
        if (nmhSetFsWtpNATConfigRowStatus (u4WtpProfileId, i4NatPool, DESTROY)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Error in deleting the entry");
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhGetFirstIndexFsWtpNATTable
            (&u4WtpNextProfileId, &i4NatNextPoolId) == SNMP_SUCCESS)
        {
            do
            {
                if (u4WtpNextProfileId == u4GivenProfileId)
                {
                    if (nmhSetFsWtpNATConfigRowStatus
                        (u4WtpProfileId, i4NatPoolId, DESTROY) == SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle, "Error in deleting the entry");
                        return CLI_FAILURE;
                    }
                }
                u4WtpProfileId = u4WtpNextProfileId;
                i4NatPoolId = i4NatNextPoolId;
            }
            while (nmhGetNextIndexFsWtpNATTable
                   (u4WtpProfileId, &u4WtpNextProfileId, i4NatPoolId,
                    &i4NatNextPoolId) == SNMP_SUCCESS);
        }
    }

    return CLI_SUCCESS;
}

INT4
FsWssShowAPNATConfig (tCliHandle CliHandle, INT4 i4NatPool,
                      UINT1 *pu1ProfileName)
{
    INT4                i4NatPoolId = 0;
    INT4                i4NatNextPoolId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4WtpNextProfileId = 0;
    INT4                i4WtpIfMainWanType = 0;
    INT4                i4WtpNATConfigRowStatus = 0;
    UINT1               au1WtpName[256];
    tSNMP_OCTET_STRING_TYPE WtpProfileName;

    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1WtpName, 0, 256);

    WtpProfileName.pu1_OctetList = au1WtpName;
    if (pu1ProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Name Conversion - failed");
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, "\r\n%s%+15s%+10s", "Pool Id", "Network Type",
                   "Status");
        CliPrintf (CliHandle, "\r\n%s%+15s%+10s", "-------", "------------",
                   "------");
        CliPrintf (CliHandle, "\r\n");
        if (nmhGetFsWtpIfMainWanType
            (u4WtpProfileId, i4NatPool, &i4WtpIfMainWanType) == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n   %d     ", i4NatPool);
            if (i4WtpIfMainWanType == WAN_TYPE_PUBLIC)
            {
                CliPrintf (CliHandle, "      LAN");
            }
            else if (i4WtpIfMainWanType == WAN_TYPE_PRIVATE)
            {
                CliPrintf (CliHandle, "      WAN");
            }
            else
            {
                CliPrintf (CliHandle, "      Other");
            }
            if (nmhGetFsWtpNATConfigRowStatus
                (u4WtpProfileId, i4NatPool,
                 &i4WtpNATConfigRowStatus) == SNMP_SUCCESS)
            {
                if (i4WtpNATConfigRowStatus == 1)
                {
                    CliPrintf (CliHandle, "        Active");
                }
                else
                {
                    CliPrintf (CliHandle, "        Inactive");
                }
                CliPrintf (CliHandle, "\r\n");
            }
        }
    }
    else
    {
        if (nmhGetFirstIndexFsWtpNATTable
            (&u4WtpNextProfileId, &i4NatNextPoolId) == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n%s%+15s%+15s%+10s", "AP Name", "Pool Id",
                       "Network Type", "Status");
            CliPrintf (CliHandle, "\r\n%s%+15s%+15s%+10s", "-------", "-------",
                       "------------", "------");
            CliPrintf (CliHandle, "\r\n");
            do
            {
                u4WtpProfileId = u4WtpNextProfileId;
                i4NatPoolId = i4NatNextPoolId;

                if (nmhGetCapwapBaseWtpProfileName
                    (u4WtpNextProfileId, &WtpProfileName) != SNMP_SUCCESS)
                {
                    continue;
                }

                if (i4NatPool != 0)
                {
                    if (i4NatPool != i4NatNextPoolId)
                    {
                        continue;
                    }
                }
                CliPrintf (CliHandle, "\r\n%s", WtpProfileName.pu1_OctetList);

                CliPrintf (CliHandle, "   %12d     ", i4NatPoolId);
                if (nmhGetFsWtpIfMainWanType (u4WtpNextProfileId,
                                              i4NatNextPoolId,
                                              &i4WtpIfMainWanType) ==
                    SNMP_SUCCESS)
                {
                    if (i4WtpIfMainWanType == WAN_TYPE_PUBLIC)
                    {
                        CliPrintf (CliHandle, "      LAN");
                    }
                    else if (i4WtpIfMainWanType == WAN_TYPE_PRIVATE)

                    {
                        CliPrintf (CliHandle, "      WAN");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "      Other");
                    }
                }
                if (nmhGetFsWtpNATConfigRowStatus (u4WtpNextProfileId,
                                                   i4NatNextPoolId,
                                                   &i4WtpNATConfigRowStatus) ==
                    SNMP_SUCCESS)
                {
                    if (i4WtpNATConfigRowStatus == 1)
                    {
                        CliPrintf (CliHandle, "      Active");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "      Inactive");
                    }
                    CliPrintf (CliHandle, "\r\n");
                }
            }
            while (nmhGetNextIndexFsWtpNATTable
                   (u4WtpProfileId, &u4WtpNextProfileId, i4NatPoolId,
                    &i4NatNextPoolId) == SNMP_SUCCESS);
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  ApFwlCliFormatFilterProtocol
 * Description :  This function is used to print filter protocol.

 * Input       :  CliHandle, protocol number.

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
ApFwlCliFormatFilterProtocol (tCliHandle CliHandle, UINT1 u1Protocol)
{
    switch (u1Protocol)
    {
        case WSSCFG_ICMP:
        {
            CliPrintf (CliHandle, "%-7s", "icmp");
        }
            break;

        case WSSCFG_ICMPV6:
        {
            CliPrintf (CliHandle, "%-7s", "icmpv6");
        }
            break;

        case WSSCFG_IGMP:
        {
            CliPrintf (CliHandle, "%-7s", "igmp");
        }
            break;

        case WSSCFG_GGP:
        {
            CliPrintf (CliHandle, "%-7s", "ggp");
        }
            break;

        case WSSCFG_IP:
        {
            CliPrintf (CliHandle, "%-7s", "ip");
        }
            break;

        case WSSCFG_TCP:
        {
            CliPrintf (CliHandle, "%-7s", "tcp");
        }
            break;

        case WSSCFG_EGP:
        {
            CliPrintf (CliHandle, "%-7s", "egp");
        }
            break;

        case WSSCFG_IGP:
        {
            CliPrintf (CliHandle, "%-7s", "igp");
        }
            break;

        case WSSCFG_NVP:
        {
            CliPrintf (CliHandle, "%-7s", "nvp");
        }
            break;

        case WSSCFG_UDP:
        {
            CliPrintf (CliHandle, "%-7s", "udp");
        }
            break;

        case WSSCFG_IRTP:
        {
            CliPrintf (CliHandle, "%-7s", "irtp");
        }
            break;

        case WSSCFG_IDPR:
        {
            CliPrintf (CliHandle, "%-7s", "idpr");
        }
            break;

        case WSSCFG_RSVP:
        {
            CliPrintf (CliHandle, "%-7s", "rsvp");
        }
            break;

        case WSSCFG_MHRP:
        {
            CliPrintf (CliHandle, "%-7s", "mhrp");
        }
            break;

        case WSSCFG_IGRP:
        {
            CliPrintf (CliHandle, "%-7s", "igrp");
        }
            break;

        case WSSCFG_OSPFIGP:
        {
            CliPrintf (CliHandle, "%-7s", "ospf");
        }
            break;

        default:
            if ((u1Protocol >= 1) && (u1Protocol < WSSCFG_PROTO_ANY))
            {
                CliPrintf (CliHandle, "%-7d", u1Protocol);
            }
            else
            {
                CliPrintf (CliHandle, "%-7s", "any");
            }
    }
    return (CLI_SUCCESS);
}

/****************************************************************************
 * Function    :  FsWssShowAPFirewallFilters
 * Description :  This function is used to show firewall filters

 * Input       :  CliHandle, profile name.

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT1
FsWssShowAPFirewallFilters (tCliHandle CliHandle, UINT1 *pu1ProfileName)
{
    tSNMP_OCTET_STRING_TYPE FwlFilterName;
    tSNMP_OCTET_STRING_TYPE NextFwlFilterName;
    tSNMP_OCTET_STRING_TYPE Temp;
    UINT1               au1FwlFilterName[AP_FWL_MAX_FILTER_NAME_LEN] =
        { AP_FWL_ZERO };
    UINT1               au1NextFwlFilterName[AP_FWL_MAX_FILTER_NAME_LEN] =
        { AP_FWL_ZERO };
    UINT1               au1Temp[AP_FWL_MAX_FILTER_NAME_LEN] = { AP_FWL_ZERO };
    INT4                i4Proto = AP_FWL_ZERO;
    INT4                i4AddrType = AP_FWL_ZERO;
    INT4                i4rc = -1;
    INT4                i4FilterAccounting = AP_FWL_ZERO;
    UINT4               u4FilterHitCount = AP_FWL_ZERO;
    UINT4               u4WtpProfileId = AP_FWL_ZERO;
    UINT4               u4WtpPrevProfileId = AP_FWL_ZERO;
    UINT4               u4UserGivenId = AP_FWL_ZERO;
    UINT1               au1WtpName[256];
    tSNMP_OCTET_STRING_TYPE WtpProfileName;

    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1WtpName, 0, 256);

    WtpProfileName.pu1_OctetList = au1WtpName;

    CliPrintf (CliHandle, "\r\n%50s\r\n", "Ap Firewall Filters");
    CliPrintf (CliHandle, "%50s\r\n", "-------------------");
    CliPrintf (CliHandle,
               "Ap        Filter              Proto  Source              "
               "Destination        Src   Dest  Packet     Hit\r\n");
    CliPrintf (CliHandle,
               "Profile                              Address             "
               "Address            port  port  Accounting Count\r\n");
    CliPrintf (CliHandle,
               "-------   ------              -----  -------             "
               "-------            ----  ----  ---------- -----\r\n");

    FwlFilterName.pu1_OctetList = au1FwlFilterName;
    NextFwlFilterName.pu1_OctetList = au1NextFwlFilterName;
    Temp.pu1_OctetList = au1Temp;
    if (pu1ProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4UserGivenId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Name Conversion - failed");
            return CLI_FAILURE;
        }
    }
    i4rc =
        nmhGetFirstIndexFsWtpFwlFilterTable (&u4WtpProfileId, &FwlFilterName);

    if (i4rc == SNMP_SUCCESS)
    {
        MEMCPY (NextFwlFilterName.pu1_OctetList, FwlFilterName.pu1_OctetList,
                FwlFilterName.i4_Length);
        NextFwlFilterName.i4_Length = FwlFilterName.i4_Length;

        do
        {
            MEMCPY (FwlFilterName.pu1_OctetList,
                    NextFwlFilterName.pu1_OctetList,
                    NextFwlFilterName.i4_Length);
            FwlFilterName.i4_Length = NextFwlFilterName.i4_Length;
            u4WtpPrevProfileId = u4WtpProfileId;
            if (nmhGetCapwapBaseWtpProfileName
                (u4WtpProfileId, &WtpProfileName) == SNMP_SUCCESS)
            {
                if (pu1ProfileName != NULL)
                {
                    if (u4WtpProfileId != u4UserGivenId)
                    {
                        continue;
                    }
                }
                nmhGetFsWtpFwlFilterAddrType (u4WtpProfileId,
                                              &NextFwlFilterName, &i4AddrType);
                if (i4AddrType == AP_FWL_IP_VERSION_4)
                {
                    au1NextFwlFilterName[NextFwlFilterName.i4_Length] =
                        AP_FWL_END_OF_STRING;

                    CliPrintf (CliHandle, "%-10s",
                               WtpProfileName.pu1_OctetList);
                    CliPrintf (CliHandle, "%-20s", au1NextFwlFilterName);

                    nmhGetFsWtpFwlFilterProtocol (u4WtpProfileId,
                                                  &NextFwlFilterName, &i4Proto);
                    ApFwlCliFormatFilterProtocol (CliHandle, (UINT1) i4Proto);

                    nmhGetFsWtpFwlFilterSrcAddress (u4WtpProfileId,
                                                    &NextFwlFilterName, &Temp);
                    CliPrintf (CliHandle, "%-20s", Temp.pu1_OctetList);
                    MEMSET (Temp.pu1_OctetList, 0, AP_FWL_MAX_FILTER_NAME_LEN);
                    nmhGetFsWtpFwlFilterDestAddress (u4WtpProfileId,
                                                     &NextFwlFilterName, &Temp);
                    CliPrintf (CliHandle, "%-19s", Temp.pu1_OctetList);
                    MEMSET (Temp.pu1_OctetList, 0, AP_FWL_MAX_FILTER_NAME_LEN);

                    nmhGetFsWtpFwlFilterSrcPort (u4WtpProfileId,
                                                 &NextFwlFilterName, &Temp);
                    CliPrintf (CliHandle, "%-6s", Temp.pu1_OctetList);
                    MEMSET (Temp.pu1_OctetList, 0, AP_FWL_MAX_FILTER_NAME_LEN);

                    nmhGetFsWtpFwlFilterDestPort (u4WtpProfileId,
                                                  &NextFwlFilterName, &Temp);
                    CliPrintf (CliHandle, "%-6s", Temp.pu1_OctetList);
                    MEMSET (Temp.pu1_OctetList, 0, AP_FWL_MAX_FILTER_NAME_LEN);

                    nmhGetFsWtpFwlFilterAccounting (u4WtpProfileId,
                                                    &NextFwlFilterName,
                                                    &i4FilterAccounting);
                    CliPrintf (CliHandle, "%-11s",
                               ((i4FilterAccounting ==
                                 AP_FWL_ENABLE) ? "ENABLED" : "DISABLED"));

                    nmhGetFsWtpFwlFilterHitsCount (u4WtpProfileId,
                                                   &NextFwlFilterName,
                                                   &u4FilterHitCount);
                    CliPrintf (CliHandle, "%d\r\n", u4FilterHitCount);
                }
            }
        }
        while (nmhGetNextIndexFsWtpFwlFilterTable (u4WtpPrevProfileId,
                                                   &u4WtpProfileId,
                                                   &FwlFilterName,
                                                   &NextFwlFilterName) ==
               SNMP_SUCCESS);

    }

    return (CLI_SUCCESS);
}

#ifdef DEBUG_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlCliUpdateFilter                                 */
/*                                                                           */
/*     DESCRIPTION      : This function creates a firewall filter.           */
/*                                                                           */
/*     INPUT            : pu1FilterName - Filter Name                        */
/*                        pu1SrcAddress - Source Address                     */
/*                        pu1DestAddress - Destination Address               */
/*                        u4ProtoPres    - Flag set it protocol present      */
/*                        u4Protocol     - Protocol                          */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
FwlCliUpdateFilter (tCliHandle CliHandle, tFwlFilterEntry * pFwlFilterEntry)
{
    UINT1              *pu1FilterName = pFwlFilterEntry->au1FilterName;
    UINT4               u4SrcPortPres = pFwlFilterEntry->u4SrcPortPres;
    UINT1              *pu1SrcPortRange = pFwlFilterEntry->au1SrcPortRange;
    UINT4               u4DestPortPres = pFwlFilterEntry->u4DestPortPres;
    UINT1              *pu1DestPortRange = pFwlFilterEntry->au1DestPortRange;
    UINT4               u4EsPres = pFwlFilterEntry->u4EsPres;
    UINT4               u4Established = pFwlFilterEntry->u4Established;
    UINT4               u4RstPres = pFwlFilterEntry->u4RstPres;
    UINT4               u4Reset = pFwlFilterEntry->u4Reset;
    tSNMP_OCTET_STRING_TYPE Name;
    tSNMP_OCTET_STRING_TYPE DestPort;
    tSNMP_OCTET_STRING_TYPE SrcPort;
    UINT1               au1Name[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    UINT1               au1DestPort[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    UINT1               au1SrcPort[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
    INT4                i4RowStatus = FWL_ZERO;
    UINT4               u4Error = FWL_ZERO;

    MEMSET (au1Name, FWL_ZERO, FWL_MAX_FILTER_NAME_LEN);

    Name.i4_Length = (INT4) STRLEN (pu1FilterName);
    Name.pu1_OctetList = au1Name;
    MEMCPY (Name.pu1_OctetList, pu1FilterName, Name.i4_Length);
    Name.pu1_OctetList[Name.i4_Length] = FWL_END_OF_STRING;

    if ((u4DestPortPres == FALSE) &&
        (u4SrcPortPres == FALSE) && (u4EsPres == FALSE) && (u4RstPres == FALSE))
    {
        return (CLI_SUCCESS);
    }

    if (nmhGetFsWtpFwlFilterRowStatus (&Name, &i4RowStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%%No such filter exists.\r\n"
                   "First add the filter first using the "
                   "\"firewall filter add\" command !\r\n");
        return (CLI_FAILURE);
    }

    if (u4SrcPortPres == TRUE)
    {
        SrcPort.i4_Length = (INT4) STRLEN (pu1SrcPortRange);
        SrcPort.pu1_OctetList = au1SrcPort;
        MEMCPY (SrcPort.pu1_OctetList, pu1SrcPortRange, SrcPort.i4_Length);

        if (nmhTestv2FwlFilterSrcPort (&u4Error, &Name, &SrcPort) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "%%Specify Source Port range (1..65535) as: >,<,=,>=,<=.\r\n"
                       "Example: >6000 or <=9000 or =540 etc\r\n");
            return (CLI_FAILURE);
        }
    }

    if (u4DestPortPres == TRUE)
    {
        DestPort.i4_Length = (INT4) STRLEN (pu1DestPortRange);
        DestPort.pu1_OctetList = au1DestPort;
        MEMCPY (DestPort.pu1_OctetList, pu1DestPortRange, DestPort.i4_Length);

        if (nmhTestv2FwlFilterDestPort (&u4Error, &Name, &DestPort) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "%%Specify Destination Port range (1..65535) as: >,<,=,>=,<=.\r\n"
                       "Example: >6000 or <=9000 or =540 etc\r\n");
            return (CLI_FAILURE);
        }
    }

    if (u4EsPres == TRUE)
    {
        if (nmhTestv2FwlFilterAckBit (&u4Error, &Name, (INT4) u4Established) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%%Improper value for Established !\r\n");
            return (CLI_FAILURE);
        }
    }

    if (u4RstPres == TRUE)
    {
        if (nmhTestv2FwlFilterRstBit (&u4Error, &Name, (INT4) u4Reset) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%%Improper value for Reset !\r\n");
            return (CLI_FAILURE);
        }
    }

    if (nmhGetFsWtpFwlFilterRowStatus (&Name, &i4RowStatus) != SNMP_SUCCESS)
    {
        /* NOTE: this section of the code will never be entered */
        return (CLI_FAILURE);
    }
    else
    {
        if (i4RowStatus == FWL_ACTIVE)
        {
            nmhSetFwlFilterRowStatus (&Name, FWL_NOT_IN_SERVICE);
        }
    }

    if (u4SrcPortPres == TRUE)
    {
        nmhSetFwlFilterSrcPort (&Name, &SrcPort);
    }

    if (u4DestPortPres == TRUE)
    {
        nmhSetFwlFilterDestPort (&Name, &DestPort);
    }

    if (u4EsPres == TRUE)
    {
        nmhSetFwlFilterAckBit (&Name, u4Established);
    }

    if (u4RstPres == TRUE)
    {
        nmhSetFwlFilterRstBit (&Name, u4Reset);
    }

    nmhSetFwlFilterRowStatus (&Name, FWL_ACTIVE);
    return (CLI_SUCCESS);
}
#endif
/***********************************************************************/
/*  Function Name : Ap_cli_get_fwl_filter_protocol                        */
/*  Description   : Function to get the Protocol from the input string */
/*  Input(s)      : pu1Prot - Pointer to the String                    */
/*  Output(s)     : None                                               */
/*  Return        : Protocol Number                                    */
/***********************************************************************/
INT4
Ap_cli_get_fwl_filter_protocol (UINT1 *pu1Prot)
{
    if (STRCASECMP (pu1Prot, "icmp") == FWL_ZERO)
    {
        return WSSCFG_ICMP;
    }
    if (STRCASECMP (pu1Prot, "igmp") == FWL_ZERO)
    {
        return WSSCFG_IGMP;
    }
    if (STRCASECMP (pu1Prot, "ggp") == FWL_ZERO)
    {
        return WSSCFG_GGP;
    }
    if (STRCASECMP (pu1Prot, "ip") == FWL_ZERO)
    {
        return WSSCFG_IP;
    }
    if (STRCASECMP (pu1Prot, "tcp") == FWL_ZERO)
    {
        return WSSCFG_TCP;
    }
    if (STRCASECMP (pu1Prot, "egp") == FWL_ZERO)
    {
        return WSSCFG_EGP;
    }
    if (STRCASECMP (pu1Prot, "igp") == FWL_ZERO)
    {
        return WSSCFG_IGP;
    }
    if (STRCASECMP (pu1Prot, "nvp") == FWL_ZERO)
    {
        return WSSCFG_NVP;
    }
    if (STRCASECMP (pu1Prot, "udp") == FWL_ZERO)
    {
        return WSSCFG_UDP;
    }
    if (STRCASECMP (pu1Prot, "irtp") == FWL_ZERO)
    {
        return WSSCFG_IRTP;
    }
    if (STRCASECMP (pu1Prot, "idpr") == FWL_ZERO)
    {
        return WSSCFG_IDPR;
    }
    if (STRCASECMP (pu1Prot, "rsvp") == FWL_ZERO)
    {
        return WSSCFG_RSVP;
    }
    if (STRCASECMP (pu1Prot, "mhrp") == FWL_ZERO)
    {
        return WSSCFG_MHRP;
    }
    if (STRCASECMP (pu1Prot, "icmpv6") == FWL_ZERO)
    {
        return WSSCFG_ICMPV6;
    }
    if (STRCASECMP (pu1Prot, "igrp") == FWL_ZERO)
    {
        return WSSCFG_IGRP;
    }
    if (STRCASECMP (pu1Prot, "ospf") == FWL_ZERO)
    {
        return WSSCFG_OSPF;
    }
    if (STRCASECMP (pu1Prot, "any") == FWL_ZERO)
    {
        return WSSCFG_ANY;
    }
    if (STRCASECMP (pu1Prot, "other") == FWL_ZERO)
    {
        return AP_FWL_ONE;
    }
    return AP_FWL_ZERO;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ApFwlCliAddFilter                                  */
/*                                                                           */
/*     DESCRIPTION      : This function creates a firewall filter.           */
/*                                                                           */
/*     INPUT            : pu1ProfileName - Ap Profile Name                   */
/*                        pu1FilterName - Filter Name                        */
/*                        pu1SrcAddr - Source Address                        */
/*                        pu1DestAddr - Destination Address                  */
/*                        u2Proto     - Protocol                             */
/*                        pu1SrcPort - Source Port                           */
/*                        pu1DestPort - Destination Port                     */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
ApFwlCliAddFilter (tCliHandle CliHandle, UINT1 *pu1ProfileName,
                   UINT1 *pu1FilterName, UINT1 *pu1SrcAddr, UINT1 *pu1DestAddr,
                   UINT2 u2Proto, UINT1 *pu1SrcPort, UINT1 *pu1DestPort)
{
    tSNMP_OCTET_STRING_TYPE Name;
    tSNMP_OCTET_STRING_TYPE SrcAddr;
    tSNMP_OCTET_STRING_TYPE DestAddr;
    tSNMP_OCTET_STRING_TYPE SrcPort;
    tSNMP_OCTET_STRING_TYPE DestPort;
    UINT1               au1Name[AP_FWL_MAX_FILTER_NAME_LEN] = { AP_FWL_ZERO };
    UINT1               au1SrcAddr[AP_FWL_MAX_ADDR_LEN] = { AP_FWL_ZERO };
    UINT1               au1DestAddr[AP_FWL_MAX_ADDR_LEN] = { AP_FWL_ZERO };
    UINT1               au1SrcPort[AP_FWL_MAX_FILTER_NAME_LEN] =
        { AP_FWL_ZERO };
    UINT1               au1DestPort[AP_FWL_MAX_FILTER_NAME_LEN] =
        { AP_FWL_ZERO };
    INT4                i4RowStatus = AP_FWL_ZERO;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4Protocol = u2Proto;
    UINT4               u4Error = AP_FWL_ZERO;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4WtpPrevProfileId = 0;

    MEMSET (au1Name, AP_FWL_ZERO, AP_FWL_MAX_FILTER_NAME_LEN);
    MEMSET (au1SrcAddr, AP_FWL_ZERO, AP_FWL_MAX_ADDR_LEN);
    MEMSET (au1DestAddr, AP_FWL_ZERO, AP_FWL_MAX_ADDR_LEN);
    MEMSET (au1SrcPort, AP_FWL_ZERO, AP_FWL_MAX_PORT_LEN);
    MEMSET (au1DestPort, AP_FWL_ZERO, AP_FWL_MAX_PORT_LEN);

    Name.i4_Length = (INT4) STRLEN (pu1FilterName);
    Name.pu1_OctetList = au1Name;
    MEMCPY (Name.pu1_OctetList, pu1FilterName, Name.i4_Length);

    DestAddr.i4_Length = (INT4) STRLEN (pu1DestAddr);
    DestAddr.pu1_OctetList = au1DestAddr;
    MEMCPY (DestAddr.pu1_OctetList, pu1DestAddr, DestAddr.i4_Length);

    SrcAddr.i4_Length = (INT4) STRLEN (pu1SrcAddr);
    SrcAddr.pu1_OctetList = au1SrcAddr;
    MEMCPY (SrcAddr.pu1_OctetList, pu1SrcAddr, SrcAddr.i4_Length);
    if (pu1ProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Name Conversion - failed");
            return CLI_FAILURE;
        }
        if ((i4RetVal = nmhGetFsWtpFwlFilterRowStatus (u4WtpProfileId, &Name,
                                                       &i4RowStatus)) !=
            SNMP_SUCCESS)
        {
            /* ICSA Fix -S-
             * filter name should not be same as already 
             * existing acl name */
            INT4                i4IfaceIndex = 0;

            if (nmhGetFsWtpFwlAclRowStatus (u4WtpProfileId, i4IfaceIndex, &Name,
                                            AP_FWL_DIRECTION_OUT, &i4RowStatus)
                == SNMP_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "%%A matching ACL name already exists for "
                           "this filter name.\r\nFilter name should be "
                           "unique!\r\n");
                return (CLI_FAILURE);
            }

            if (nmhGetFsWtpFwlAclRowStatus (u4WtpProfileId, i4IfaceIndex, &Name,
                                            AP_FWL_DIRECTION_IN, &i4RowStatus)
                == SNMP_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "%%A matching ACL name already exists for "
                           "this filter name. Filter name should be"
                           "unique!\r\n");
                return (CLI_FAILURE);
            }
            /* ICSA Fix -E- */

            if (nmhSetFsWtpFwlFilterRowStatus (u4WtpProfileId, &Name,
                                               CREATE_AND_WAIT) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "%%Max filters configured. Cannot add "
                           "a new filter !\r\n");
                return (CLI_FAILURE);
            }
        }
        else
        {
            if (i4RowStatus == ACTIVE)
            {
                nmhSetFsWtpFwlFilterRowStatus (u4WtpProfileId, &Name,
                                               NOT_IN_SERVICE);
            }
        }

        /* Set the Address type */
        if (nmhTestv2FsWtpFwlFilterAddrType (&u4Error, u4WtpProfileId,
                                             &Name, AP_FWL_IP_VERSION_4) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%% Invalid AddressType \r\n");
            nmhSetFsWtpFwlFilterRowStatus (u4WtpProfileId, &Name, DESTROY);
            return (CLI_FAILURE);
        }
        else
        {
            nmhSetFsWtpFwlFilterAddrType (u4WtpProfileId, &Name,
                                          AP_FWL_IP_VERSION_4);
        }

        if (nmhTestv2FsWtpFwlFilterSrcAddress (&u4Error, u4WtpProfileId,
                                               &Name, &SrcAddr) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "%%Source IP Address or range is improper \r\n"
                       " Supported formats : 1. [<range_opr>]<addr>/(0..32) \r\n"
                       "                     2. <range_opr><start_addr><range_opr>"
                       "<end_addr>\r\n" " range_opr can be <,>,>=,<="
                       " addr can a.b.c.d or a.b.c or a.b"
                       "(any valid internet format)");
            if (i4RetVal != SNMP_SUCCESS)
            {
                nmhSetFsWtpFwlFilterRowStatus (u4WtpProfileId, &Name, DESTROY);
            }
            else if (i4RowStatus == ACTIVE)
            {
                nmhSetFsWtpFwlFilterRowStatus (u4WtpProfileId, &Name, ACTIVE);
            }
            return (CLI_FAILURE);
        }

        if (nmhTestv2FsWtpFwlFilterDestAddress (&u4Error, u4WtpProfileId, &Name,
                                                &DestAddr) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "%%Destination IP Address or range is improper \r\n"
                       " Supported formats : 1. [<range_opr>]<addr>/(0..32) \r\n"
                       "                     2. <range_opr><start_addr><range_opr>"
                       "<end_addr>\r\n"
                       " range_opr can be <,>,>=,<= \r\n"
                       " addr can a.b.c.d or a.b.c or a.b"
                       "(any valid internet format) \r\n");

            if (i4RetVal != SNMP_SUCCESS)
            {
                nmhSetFsWtpFwlFilterRowStatus (u4WtpProfileId, &Name, DESTROY);
            }
            else if (i4RowStatus == ACTIVE)
            {
                nmhSetFsWtpFwlFilterRowStatus (u4WtpProfileId, &Name, ACTIVE);
            }
            return (CLI_FAILURE);
        }

        if (u2Proto != AP_FWL_ZERO)
        {
            if (nmhTestv2FsWtpFwlFilterProtocol (&u4Error, u4WtpProfileId,
                                                 &Name,
                                                 (INT4) u2Proto) !=
                SNMP_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "%%Protocol can only take one the following"
                           "values\r\n1 - ICMP , 2 - IGMP , "
                           "3 - GGP, 4 - IP , 6 - TCP , "
                           "8 - EGP , 9 - IGP , 11 - NVP , "
                           "17 - UDP , 28 - IRTP , 35 - IDPR ,"
                           "46 - RSVP , 48 - MHRP , "
                           "88 - IGRP , 89 - OSPF !\r\n");
                if (i4RetVal != SNMP_SUCCESS)
                {
                    nmhSetFsWtpFwlFilterRowStatus (u4WtpProfileId, &Name,
                                                   DESTROY);
                }
                else if (i4RowStatus == ACTIVE)
                {
                    nmhSetFsWtpFwlFilterRowStatus (u4WtpProfileId, &Name,
                                                   ACTIVE);
                }

                return (CLI_FAILURE);
            }
        }

        if (pu1SrcPort != NULL)
        {
            SrcPort.i4_Length = (INT4) STRLEN (pu1SrcPort);
            SrcPort.pu1_OctetList = au1SrcPort;
            MEMCPY (SrcPort.pu1_OctetList, pu1SrcPort, SrcPort.i4_Length);

            if (nmhTestv2FsWtpFwlFilterSrcPort (&u4Error, u4WtpProfileId, &Name,
                                                &SrcPort) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "%%Specify Source Port range (1..65535) as: >,<,=,>=,<=.\r\n"
                           "Example: >6000 or <=9000 or =540 etc\r\n");
                return (CLI_FAILURE);
            }
        }

        if (pu1DestPort != NULL)
        {
            DestPort.i4_Length = (INT4) STRLEN (pu1DestPort);
            DestPort.pu1_OctetList = au1DestPort;
            MEMCPY (DestPort.pu1_OctetList, pu1DestPort, DestPort.i4_Length);

            if (nmhTestv2FsWtpFwlFilterDestPort
                (&u4Error, u4WtpProfileId, &Name, &DestPort) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "%%Specify Destination Port range (1..65535) as: >,<,=,>=,<=.\r\n"
                           "Example: >6000 or <=9000 or =540 etc\r\n");
                return (CLI_FAILURE);
            }
        }
        if (nmhSetFsWtpFwlFilterSrcAddress (u4WtpProfileId, &Name, &SrcAddr)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Configuration of Source Addr. - failed\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsWtpFwlFilterDestAddress (u4WtpProfileId, &Name, &DestAddr)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "Configuration of Destination Addr. - failed\r\n");
            return CLI_FAILURE;
        }
        if ((u2Proto != AP_FWL_ZERO))
        {
            if (nmhSetFsWtpFwlFilterProtocol (u4WtpProfileId, &Name, u4Protocol)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "Configuration of protocol - failed\r\n");
                return CLI_FAILURE;
            }
        }
        if (pu1SrcPort != NULL)
        {
            if (nmhSetFsWtpFwlFilterSrcPort (u4WtpProfileId, &Name, &SrcPort)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "Configuration of source port range - failed\r\n");
            }
        }
        if (pu1DestPort != NULL)
        {
            if (nmhSetFsWtpFwlFilterDestPort (u4WtpProfileId, &Name, &DestPort)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "Configuration of destination port range - failed\r\n");
            }
        }
        nmhSetFsWtpFwlFilterRowStatus (u4WtpProfileId, &Name, ACTIVE);
    }
    else
    {
        if (nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId)
            == SNMP_SUCCESS)
        {
            do
            {
                u4WtpPrevProfileId = u4WtpProfileId;
                if ((i4RetVal = nmhGetFsWtpFwlFilterRowStatus (u4WtpProfileId,
                                                               &Name,
                                                               &i4RowStatus)) !=
                    SNMP_SUCCESS)
                {
                    /* ICSA Fix -S-
                     * filter name should not be same as already 
                     * existing acl name */
                    INT4                i4IfaceIndex = 0;
                    if (nmhGetFsWtpFwlAclRowStatus (u4WtpProfileId,
                                                    i4IfaceIndex, &Name,
                                                    AP_FWL_DIRECTION_OUT,
                                                    &i4RowStatus) ==
                        SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "%%A matching ACL name already exists for "
                                   "this filter name.\r\nFilter name should be "
                                   "unique!\r\n");
                        return (CLI_FAILURE);
                    }

                    if (nmhGetFsWtpFwlAclRowStatus (u4WtpProfileId,
                                                    i4IfaceIndex, &Name,
                                                    AP_FWL_DIRECTION_IN,
                                                    &i4RowStatus) ==
                        SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "%%A matching ACL name already exists for "
                                   "this filter name. Filter name should be"
                                   "unique!\r\n");
                        return (CLI_FAILURE);
                    }
                    /* ICSA Fix -E- */

                    if (nmhSetFsWtpFwlFilterRowStatus (u4WtpProfileId, &Name,
                                                       CREATE_AND_WAIT) !=
                        SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "%%Max filters configured."
                                   " Cannot add a new filter !\r\n");
                        return (CLI_FAILURE);
                    }
                }
                else
                {
                    if (i4RowStatus == ACTIVE)
                    {
                        nmhSetFsWtpFwlFilterRowStatus (u4WtpProfileId, &Name,
                                                       NOT_IN_SERVICE);
                    }
                }

                /* Set the Address type */
                if (nmhTestv2FsWtpFwlFilterAddrType (&u4Error, u4WtpProfileId,
                                                     &Name,
                                                     AP_FWL_IP_VERSION_4) !=
                    SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "%% Invalid AddressType \r\n");
                    nmhSetFsWtpFwlFilterRowStatus (u4WtpProfileId, &Name,
                                                   DESTROY);
                    return (CLI_FAILURE);
                }
                else
                {
                    nmhSetFsWtpFwlFilterAddrType (u4WtpProfileId, &Name,
                                                  AP_FWL_IP_VERSION_4);
                }

                if (nmhTestv2FsWtpFwlFilterSrcAddress (&u4Error, u4WtpProfileId,
                                                       &Name, &SrcAddr) !=
                    SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "%%Source IP Address or range is improper \r\n"
                               " Supported formats : 1. [<range_opr>]<addr>/(0..32) \r\n"
                               "                     2. <range_opr><start_addr>"
                               "<range_opr>"
                               "<end_addr>\r\n"
                               " range_opr can be <,>,>=,<="
                               " addr can a.b.c.d or a.b.c or a.b"
                               "(any valid internet format)");
                    if (i4RetVal != SNMP_SUCCESS)
                    {
                        nmhSetFsWtpFwlFilterRowStatus (u4WtpProfileId, &Name,
                                                       DESTROY);
                    }
                    else if (i4RowStatus == ACTIVE)
                    {
                        nmhSetFsWtpFwlFilterRowStatus (u4WtpProfileId, &Name,
                                                       ACTIVE);
                    }
                    continue;
                }

                if (nmhTestv2FsWtpFwlFilterDestAddress (&u4Error,
                                                        u4WtpProfileId, &Name,
                                                        &DestAddr) !=
                    SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "%%Destination IP Address or range is improper \r\n"
                               " Supported formats : 1. [<range_opr>]<addr>/"
                               "(0..32) \r\n"
                               "                     2. <range_opr><start_addr>"
                               "<range_opr>" "<end_addr>\r\n"
                               " range_opr can be <,>,>=,<= \r\n"
                               " addr can a.b.c.d or a.b.c or a.b"
                               "(any valid internet format) \r\n");

                    if (i4RetVal != SNMP_SUCCESS)
                    {
                        nmhSetFsWtpFwlFilterRowStatus (u4WtpProfileId, &Name,
                                                       DESTROY);
                    }
                    else if (i4RowStatus == ACTIVE)
                    {
                        nmhSetFsWtpFwlFilterRowStatus (u4WtpProfileId,
                                                       &Name, ACTIVE);
                    }
                    continue;
                }

                if (u2Proto != AP_FWL_ZERO)
                {
                    if (nmhTestv2FsWtpFwlFilterProtocol
                        (&u4Error, u4WtpProfileId, &Name,
                         (INT4) u2Proto) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "%%Protocol can only take one the following "
                                   "values\r\n1 - ICMP , 2 - IGMP , "
                                   "3 - GGP, 4 - IP , 6 - TCP , "
                                   "8 - EGP , 9 - IGP , 11 - NVP , "
                                   "17 - UDP , 28 - IRTP , 35 - IDPR ,"
                                   "46 - RSVP , 48 - MHRP , "
                                   "88 - IGRP , 89 - OSPF !\r\n");
                        if (i4RetVal != SNMP_SUCCESS)
                        {
                            nmhSetFsWtpFwlFilterRowStatus (u4WtpProfileId,
                                                           &Name, DESTROY);
                        }
                        else if (i4RowStatus == ACTIVE)
                        {
                            nmhSetFsWtpFwlFilterRowStatus (u4WtpProfileId,
                                                           &Name, ACTIVE);
                        }
                        continue;
                    }
                }

                if (pu1SrcPort != NULL)
                {
                    SrcPort.i4_Length = (INT4) STRLEN (pu1SrcPort);
                    SrcPort.pu1_OctetList = au1SrcPort;
                    MEMCPY (SrcPort.pu1_OctetList, pu1SrcPort,
                            SrcPort.i4_Length);

                    if (nmhTestv2FsWtpFwlFilterSrcPort
                        (&u4Error, u4WtpProfileId, &Name,
                         &SrcPort) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "%Specify Source Port range (1..65535) as: >,<,=,>=,"
                                   "<=.\r\n"
                                   "Example: >6000 or <=9000 or =540 etc\r\n");
                        continue;
                    }
                }

                if (pu1DestPort != NULL)
                {
                    DestPort.i4_Length = (INT4) STRLEN (pu1DestPort);
                    DestPort.pu1_OctetList = au1DestPort;
                    MEMCPY (DestPort.pu1_OctetList, pu1DestPort,
                            DestPort.i4_Length);

                    if (nmhTestv2FsWtpFwlFilterDestPort (&u4Error,
                                                         u4WtpProfileId, &Name,
                                                         &DestPort) !=
                        SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "%%Specify Destination Port range (1..65535) as: >,<"
                                   ",=,>=,<=.\r\n"
                                   "Example: >6000 or <=9000 or =540 etc\r\n");
                        continue;
                    }
                }
                if (nmhSetFsWtpFwlFilterSrcAddress (u4WtpProfileId, &Name,
                                                    &SrcAddr) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "Configuration of Source Addr. - failed\r\n");
                    continue;
                }
                if (nmhSetFsWtpFwlFilterDestAddress (u4WtpProfileId, &Name,
                                                     &DestAddr) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "Configuration of Destination Addr. - failed\r\n");
                    continue;
                }
                if ((u2Proto != AP_FWL_ZERO))
                {
                    if (nmhSetFsWtpFwlFilterProtocol (u4WtpProfileId, &Name,
                                                      u4Protocol)
                        == SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "Configuration of protocol - failed\r\n");
                        continue;
                    }
                }
                if (pu1SrcPort != NULL)
                {
                    if (nmhSetFsWtpFwlFilterSrcPort
                        (u4WtpProfileId, &Name, &SrcPort) == SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "Configuration of source port range - failed\r\n");
                    }
                }
                if (pu1DestPort != NULL)
                {
                    if (nmhSetFsWtpFwlFilterDestPort (u4WtpProfileId, &Name,
                                                      &DestPort)
                        == SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "Configuration of destination port range -"
                                   " failed\r\n");
                    }
                }
                nmhSetFsWtpFwlFilterRowStatus (u4WtpProfileId, &Name, ACTIVE);
            }
            while (nmhGetNextIndexCapwapBaseWtpProfileTable
                   (u4WtpPrevProfileId, &u4WtpProfileId) == SNMP_SUCCESS);
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ApFwlCliDeleteFilter                               */
/*                                                                           */
/*     DESCRIPTION      : This function deletes a firewall filter.           */
/*                                                                           */
/*     INPUT            : CliHandle,
 *                        pu1ProfileName - Ap Profile Name                   */
/*                        pu1FilterName - Filter Name                        */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
ApFwlCliDeleteFilter (tCliHandle CliHandle, UINT1 *pu1ProfileName,
                      UINT1 *pu1FilterName)
{
    tSNMP_OCTET_STRING_TYPE FilterName;
    UINT1               au1FilterName[AP_FWL_MAX_FILTER_NAME_LEN]
        = { AP_FWL_ZERO };
    UINT4               u4ErrorCode = AP_FWL_ZERO;
    UINT4               u4WtpProfileId = AP_FWL_ZERO;
    UINT4               u4WtpPrevProfileId = AP_FWL_ZERO;

    if (STRCMP (pu1FilterName, "default") == FWL_ZERO)
    {
        CliPrintf (CliHandle, "%%Cannot delete default Filter !\r\n");
        return (CLI_FAILURE);
    }
    if (pu1ProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Name Conversion - failed");
            return CLI_FAILURE;
        }

        FilterName.i4_Length = (INT4) STRLEN (pu1FilterName);
        FilterName.pu1_OctetList = au1FilterName;
        MEMCPY (FilterName.pu1_OctetList, pu1FilterName, FilterName.i4_Length);

        if (nmhTestv2FsWtpFwlFilterRowStatus (&u4ErrorCode, u4WtpProfileId,
                                              &FilterName,
                                              DESTROY) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetFsWtpFwlFilterRowStatus (u4WtpProfileId, &FilterName, DESTROY)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Deletion of Filter - failed");
            return (CLI_FAILURE);
        }
    }
    else
    {
        if (nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId)
            == SNMP_SUCCESS)
        {
            do
            {
                u4WtpPrevProfileId = u4WtpProfileId;
                FilterName.i4_Length = (INT4) STRLEN (pu1FilterName);
                FilterName.pu1_OctetList = au1FilterName;
                MEMCPY (FilterName.pu1_OctetList, pu1FilterName,
                        FilterName.i4_Length);

                if (nmhTestv2FsWtpFwlFilterRowStatus (&u4ErrorCode,
                                                      u4WtpProfileId,
                                                      &FilterName,
                                                      DESTROY) == SNMP_SUCCESS)
                {
                    if (nmhSetFsWtpFwlFilterRowStatus (u4WtpProfileId,
                                                       &FilterName, DESTROY)
                        == SNMP_FAILURE)
                    {
                        continue;
                    }
                }
            }
            while (nmhGetNextIndexCapwapBaseWtpProfileTable
                   (u4WtpPrevProfileId, &u4WtpProfileId) == SNMP_SUCCESS);
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ApFwlCliAddAccessList                              */
/*                                                                           */
/*     DESCRIPTION      : This function creates a firewall Acl               */
/*                                                                           */
/*     INPUT            : CliHandle,                                         */
/*                        pu1ProfileName - Ap Profile Name                   */
/*                        pu1AclName - Acl Name                              */
/*                        pu1FilterName - Filter Name                        */
/*                        i4AclIfIndex - Acl Index                           */
/*                        u1AclDirection -  Acl Direction                    */
/*                        u2SeqNum - Acl Priority                            */
/*                        u1Action - Acl Action                              */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
ApFwlCliAddAccessList (tCliHandle CliHandle, UINT1 *pu1ProfileName,
                       UINT1 *pu1AclName, UINT1 *pu1FilterName,
                       INT4 i4AclIfIndex, UINT1 u1AclDirection, UINT2 u2SeqNum,
                       UINT1 u1Action)
{
    tSNMP_OCTET_STRING_TYPE AclName;
    tSNMP_OCTET_STRING_TYPE FilterSet;
    UINT1               au1AclName[AP_FWL_MAX_ACL_NAME_LEN] = { AP_FWL_ZERO };
    UINT1               au1FilterSet[AP_FWL_MAX_FILTER_NAME_LEN] =
        { AP_FWL_ZERO };
    INT4                i4RowStatus = AP_FWL_ZERO;
    UINT4               u4Error = AP_FWL_ZERO;
    UINT4               u4NewAclFlag = AP_FWL_ZERO;
    UINT4               u4WtpProfileId = AP_FWL_ZERO;
    UINT4               u4WtpPrevProfileId = AP_FWL_ZERO;
    INT4                i4RetVal = SNMP_FAILURE;

    AclName.i4_Length = (INT4) STRLEN (pu1AclName);
    AclName.pu1_OctetList = au1AclName;
    MEMCPY (AclName.pu1_OctetList, pu1AclName, AclName.i4_Length);

    FilterSet.i4_Length = (INT4) STRLEN (pu1FilterName);
    FilterSet.pu1_OctetList = au1FilterSet;
    MEMCPY (FilterSet.pu1_OctetList, pu1FilterName, FilterSet.i4_Length);

    if ((FilterSet.pu1_OctetList == NULL) || (AclName.pu1_OctetList == NULL))
    {
        CliPrintf (CliHandle, "%%Access List could not "
                   "be created due to lack of " "resources !\r\n");
        return (CLI_FAILURE);
    }

    if (!((u1Action == WSSCFG_DENY) || (u1Action == WSSCFG_PERMIT)))
    {
        CliPrintf (CliHandle, "%%Action can be either permit " "or deny !\r\n");
        return (CLI_FAILURE);
    }

    if (u2SeqNum <= AP_FWL_ZERO)
    {
        CliPrintf (CliHandle, "%%Priority should be greater than zero !\r\n");
        return (CLI_FAILURE);
    }
    if (pu1ProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Name Conversion - failed");
            return CLI_FAILURE;
        }

        /* ICSA Fix -S-
         * ACL name should not be same as already 
         * filter name */

        if ((i4RetVal = nmhGetFsWtpFwlFilterRowStatus (u4WtpProfileId, &AclName,
                                                       &i4RowStatus)) ==
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%%A matching Filter name already exists for "
                       "this ACL name.\r\nACL name should be " "unique!\r\n");
            return (CLI_FAILURE);
        }
        /* ICSA Fix -E- */

        if ((i4RetVal = nmhGetFsWtpFwlRuleRowStatus (u4WtpProfileId, &AclName,
                                                     &i4RowStatus)) !=
            SNMP_SUCCESS)
        {
            if (nmhSetFsWtpFwlRuleRowStatus (u4WtpProfileId, &AclName,
                                             CREATE_AND_WAIT) != SNMP_SUCCESS)
            {
                return (CLI_FAILURE);

            }
            if (nmhTestv2FsWtpFwlRuleFilterSet (&u4Error, u4WtpProfileId,
                                                &AclName,
                                                &FilterSet) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "%%Check the filter set specified or specify "
                           "filter combination like filter1,filter2 !\r\n");
                if (i4RetVal != SNMP_SUCCESS)
                {
                    nmhSetFsWtpFwlRuleRowStatus (u4WtpProfileId, &AclName,
                                                 DESTROY);
                }
                return (CLI_FAILURE);
            }

            if (nmhSetFsWtpFwlRuleFilterSet (u4WtpProfileId, &AclName,
                                             &FilterSet) != SNMP_SUCCESS)
            {
                nmhSetFsWtpFwlRuleRowStatus (u4WtpProfileId, &AclName, DESTROY);
                return (CLI_FAILURE);
            }
        }
        else
        {
            tSNMP_OCTET_STRING_TYPE RuleFilterSet;
            UINT1               au1RuleFilterSet[MAX_OCTETSTRING_SIZE] =
                { AP_FWL_ZERO };

            RuleFilterSet.i4_Length = 256;
            RuleFilterSet.pu1_OctetList = au1RuleFilterSet;

            /* Rule exists. Now get the filter set for the corresponding
             * acl name */
            if (nmhGetFsWtpFwlRuleFilterSet (u4WtpProfileId, &AclName,
                                             &RuleFilterSet) == SNMP_SUCCESS)
            {
                RuleFilterSet.pu1_OctetList[RuleFilterSet.i4_Length]
                    = AP_FWL_ZERO;
                FilterSet.pu1_OctetList[FilterSet.i4_Length] = AP_FWL_ZERO;

                /* Check if the filter set is different from the user specified
                 * one.
                 * If so dont allow the user to modify the filter set */
                if (STRCMP (RuleFilterSet.pu1_OctetList,
                            FilterSet.pu1_OctetList) != FWL_ZERO)
                {
                    CliPrintf (CliHandle,
                               "%%Filter Set cannot be modified\r\n");
                    return (CLI_FAILURE);
                }
            }
        }
        nmhSetFsWtpFwlRuleRowStatus (u4WtpProfileId, &AclName, ACTIVE);
        if (nmhGetFsWtpFwlAclRowStatus (u4WtpProfileId, i4AclIfIndex, &AclName,
                                        u1AclDirection,
                                        &i4RowStatus) != SNMP_SUCCESS)
        {
            if (nmhTestv2FsWtpFwlAclRowStatus
                (&u4Error, u4WtpProfileId, i4AclIfIndex, &AclName,
                 u1AclDirection, CREATE_AND_WAIT) != SNMP_SUCCESS)
            {
                nmhSetFsWtpFwlRuleRowStatus (u4WtpProfileId, &AclName, DESTROY);
                return (CLI_FAILURE);
            }
            if (nmhSetFsWtpFwlAclRowStatus (u4WtpProfileId, i4AclIfIndex,
                                            &AclName, u1AclDirection,
                                            CREATE_AND_WAIT) != SNMP_SUCCESS)
            {
                return (CLI_FAILURE);
            }
            u4NewAclFlag = AP_FWL_ONE;
        }
        else
        {
            if (i4RowStatus == ACTIVE)
            {
                nmhSetFsWtpFwlAclRowStatus (u4WtpProfileId, i4AclIfIndex,
                                            &AclName, u1AclDirection,
                                            NOT_IN_SERVICE);
            }
        }

        nmhSetFsWtpFwlAclAction (u4WtpProfileId, i4AclIfIndex, &AclName,
                                 u1AclDirection, u1Action);

        i4RetVal =
            nmhTestv2FsWtpFwlAclSequenceNumber (&u4Error, u4WtpProfileId,
                                                i4AclIfIndex, &AclName,
                                                u1AclDirection,
                                                (INT4) u2SeqNum);
        if (SNMP_FAILURE == i4RetVal)
        {
            if (AP_FWL_ONE == u4NewAclFlag)
            {
                nmhSetFsWtpFwlAclRowStatus (u4WtpProfileId, i4AclIfIndex,
                                            &AclName, u1AclDirection, DESTROY);
            }
            return (CLI_FAILURE);
        }

        if (nmhSetFsWtpFwlAclSequenceNumber (u4WtpProfileId, i4AclIfIndex,
                                             &AclName, u1AclDirection,
                                             (INT4) u2SeqNum) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%%Cannot assign the given priority "
                       "since an Access list already \r\n"
                       "exists with this priority !\r\n");
            return (CLI_FAILURE);
        }

        nmhSetFsWtpFwlAclRowStatus (u4WtpProfileId, i4AclIfIndex, &AclName,
                                    u1AclDirection, ACTIVE);
    }
    else
    {
        if (nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId)
            == SNMP_SUCCESS)
        {
            do
            {
                u4WtpPrevProfileId = u4WtpProfileId;
                if ((i4RetVal = nmhGetFsWtpFwlFilterRowStatus (u4WtpProfileId,
                                                               &AclName,
                                                               &i4RowStatus)) ==
                    SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "%%A matching Filter name already exists for "
                               "this ACL name.\r\nACL name should be "
                               "unique!\r\n");
                    continue;
                }
                /* ICSA Fix -E- */

                if ((i4RetVal = nmhGetFsWtpFwlRuleRowStatus (u4WtpProfileId,
                                                             &AclName,
                                                             &i4RowStatus)) !=
                    SNMP_SUCCESS)
                {
                    if (nmhSetFsWtpFwlRuleRowStatus (u4WtpProfileId, &AclName,
                                                     CREATE_AND_WAIT) !=
                        SNMP_SUCCESS)
                    {
                        continue;

                    }
                    if (nmhTestv2FsWtpFwlRuleFilterSet (&u4Error,
                                                        u4WtpProfileId,
                                                        &AclName,
                                                        &FilterSet) !=
                        SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "%%Check the filter set specified or specify "
                                   "filter combination like filter1,filter2 !\r\n");
                        if (i4RetVal != SNMP_SUCCESS)
                        {
                            nmhSetFsWtpFwlRuleRowStatus (u4WtpProfileId,
                                                         &AclName, DESTROY);
                        }
                        continue;
                    }

                    if (nmhSetFsWtpFwlRuleFilterSet (u4WtpProfileId, &AclName,
                                                     &FilterSet) !=
                        SNMP_SUCCESS)
                    {
                        nmhSetFsWtpFwlRuleRowStatus (u4WtpProfileId, &AclName,
                                                     DESTROY);
                        continue;
                    }
                }
                else
                {
                    tSNMP_OCTET_STRING_TYPE RuleFilterSet;
                    UINT1               au1RuleFilterSet[MAX_OCTETSTRING_SIZE] =
                        { AP_FWL_ZERO };

                    RuleFilterSet.i4_Length = 256;
                    RuleFilterSet.pu1_OctetList = au1RuleFilterSet;

                    /* Rule exists. Now get the filter set for the corresponding
                     * acl name */
                    if (nmhGetFsWtpFwlRuleFilterSet (u4WtpProfileId, &AclName,
                                                     &RuleFilterSet) ==
                        SNMP_SUCCESS)
                    {
                        RuleFilterSet.pu1_OctetList[RuleFilterSet.i4_Length]
                            = AP_FWL_ZERO;
                        FilterSet.pu1_OctetList[FilterSet.i4_Length]
                            = AP_FWL_ZERO;

                        /* Check if the filter set is different from the
                         * user specified one.
                         * If so dont allow the user to modify the filter set */
                        if (STRCMP (RuleFilterSet.pu1_OctetList,
                                    FilterSet.pu1_OctetList) != FWL_ZERO)
                        {
                            CliPrintf (CliHandle,
                                       "%%Filter Set cannot be modified\r\n");
                            continue;
                        }
                    }
                }
                nmhSetFsWtpFwlRuleRowStatus (u4WtpProfileId, &AclName, ACTIVE);
                if (nmhGetFsWtpFwlAclRowStatus (u4WtpProfileId, i4AclIfIndex,
                                                &AclName,
                                                u1AclDirection,
                                                &i4RowStatus) != SNMP_SUCCESS)
                {
                    if (nmhTestv2FsWtpFwlAclRowStatus
                        (&u4Error, u4WtpProfileId, i4AclIfIndex, &AclName,
                         u1AclDirection, CREATE_AND_WAIT) != SNMP_SUCCESS)
                    {
                        nmhSetFsWtpFwlRuleRowStatus (u4WtpProfileId, &AclName,
                                                     DESTROY);
                        continue;
                    }
                    if (nmhSetFsWtpFwlAclRowStatus
                        (u4WtpProfileId, i4AclIfIndex, &AclName, u1AclDirection,
                         CREATE_AND_WAIT) != SNMP_SUCCESS)
                    {
                        continue;
                    }
                    u4NewAclFlag = AP_FWL_ONE;
                }
                else
                {
                    if (i4RowStatus == ACTIVE)
                    {
                        nmhSetFsWtpFwlAclRowStatus (u4WtpProfileId,
                                                    i4AclIfIndex, &AclName,
                                                    u1AclDirection,
                                                    NOT_IN_SERVICE);
                    }
                }

                nmhSetFsWtpFwlAclAction (u4WtpProfileId, i4AclIfIndex, &AclName,
                                         u1AclDirection, u1Action);

                i4RetVal =
                    nmhTestv2FsWtpFwlAclSequenceNumber (&u4Error,
                                                        u4WtpProfileId,
                                                        i4AclIfIndex, &AclName,
                                                        u1AclDirection,
                                                        (INT4) u2SeqNum);
                if (SNMP_FAILURE == i4RetVal)
                {
                    if (AP_FWL_ONE == u4NewAclFlag)
                    {
                        nmhSetFsWtpFwlAclRowStatus (u4WtpProfileId,
                                                    i4AclIfIndex, &AclName,
                                                    u1AclDirection, DESTROY);
                    }
                    continue;
                }

                if (nmhSetFsWtpFwlAclSequenceNumber (u4WtpProfileId,
                                                     i4AclIfIndex, &AclName,
                                                     u1AclDirection,
                                                     (INT4) u2SeqNum) !=
                    SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "%%Cannot assign the given priority "
                               "since an Access list already \r\n"
                               "exists with this priority !\r\n");
                    continue;
                }

                nmhSetFsWtpFwlAclRowStatus (u4WtpProfileId, i4AclIfIndex,
                                            &AclName, u1AclDirection, ACTIVE);
            }
            while (nmhGetNextIndexCapwapBaseWtpProfileTable
                   (u4WtpPrevProfileId, &u4WtpProfileId) == SNMP_SUCCESS);
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ApFwlCliDeleteAccessList                           */
/*                                                                           */
/*     DESCRIPTION      : This function creates a firewall Acl               */
/*                                                                           */
/*     INPUT            : CliHandle,                                         */
/*                        pu1ProfileName - Ap Profile Name                   */
/*                        pu1AclName - Acl Name                              */
/*                        pu1FilterName - Filter Name                        */
/*                        i4AclIfIndex - Acl Index                           */
/*                        u1AclDirection -  Acl Direction                    */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
ApFwlCliDeleteAccessList (tCliHandle CliHandle, UINT1 *pu1ProfileName,
                          UINT4 i4AclIfIndex, UINT1 *pu1AclName,
                          UINT4 u1AclDirection)
{
    INT4                i4Status = SNMP_FAILURE;
    UINT4               u4ErrorCode = AP_FWL_ZERO;
    tSNMP_OCTET_STRING_TYPE AclName;
    UINT1               au1AclName[MAX_OCTETSTRING_SIZE] = { AP_FWL_ZERO };
    UINT4               u4WtpProfileId = AP_FWL_ZERO;
    UINT4               u4WtpPrevProfileId = AP_FWL_ZERO;

    AclName.i4_Length = (INT4) STRLEN (pu1AclName);
    AclName.pu1_OctetList = au1AclName;
    MEMCPY (AclName.pu1_OctetList, pu1AclName, AclName.i4_Length);
    if (pu1ProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Name Conversion - failed");
            return CLI_FAILURE;
        }
        i4Status = nmhSetFsWtpFwlAclRowStatus (u4WtpProfileId, i4AclIfIndex,
                                               &AclName, u1AclDirection,
                                               DESTROY);
        if (i4Status == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%% ACL does not exist !\r\n");
            return (CLI_FAILURE);
        }

        if (nmhTestv2FsWtpFwlRuleRowStatus (&u4ErrorCode, u4WtpProfileId,
                                            &AclName, DESTROY) == SNMP_SUCCESS)
        {
            nmhSetFsWtpFwlRuleRowStatus (u4WtpProfileId, &AclName, DESTROY);
        }
    }
    else
    {
        if (nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId)
            == SNMP_SUCCESS)
        {
            do
            {
                u4WtpPrevProfileId = u4WtpProfileId;
                i4Status = nmhSetFsWtpFwlAclRowStatus (u4WtpProfileId,
                                                       i4AclIfIndex, &AclName,
                                                       u1AclDirection, DESTROY);
                if (i4Status == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "%% ACL does not exist !\r\n");
                    return (CLI_FAILURE);
                }

                if (nmhTestv2FsWtpFwlRuleRowStatus
                    (&u4ErrorCode, u4WtpProfileId, &AclName,
                     DESTROY) == SNMP_SUCCESS)
                {
                    nmhSetFsWtpFwlRuleRowStatus (u4WtpProfileId, &AclName,
                                                 DESTROY);
                }
            }
            while (nmhGetNextIndexCapwapBaseWtpProfileTable
                   (u4WtpPrevProfileId, &u4WtpProfileId) == SNMP_SUCCESS);
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FsWssShowAPFirewallAcl                             */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Acl list                */
/*                                                                           */
/*     INPUT            : CliHandle,                                         */
/*                        pu1ProfileName - Ap Profile Name                   */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT1
FsWssShowAPFirewallAcl (tCliHandle CliHandle, UINT1 *pu1ProfileName)
{
    tSNMP_OCTET_STRING_TYPE FwlAclName;
    tSNMP_OCTET_STRING_TYPE NextFwlAclName;
    tSNMP_OCTET_STRING_TYPE Temp;
    UINT1               au1FwlAclName[AP_FWL_MAX_ACL_NAME_LEN] =
        { AP_FWL_ZERO };
    UINT1               au1NextFwlAclName[AP_FWL_MAX_ACL_NAME_LEN] =
        { AP_FWL_ZERO };
    UINT1               au1Temp[AP_FWL_MAX_ACL_NAME_LEN] = { AP_FWL_ZERO };
    INT4                i4NextFwlAclIfIndex = AP_FWL_INVALID;
    INT4                i4NextFwlAclDirection = AP_FWL_INVALID;
    INT4                i4FwlAclDirection = AP_FWL_INVALID;
    INT4                i4FwlAclIfIndex = AP_FWL_INVALID;
    INT4                i4rc = -1;
    UINT1               au1IfName[AP_FWL_INTERFACE_NAME_LEN_MAX] =
        { AP_FWL_ZERO };
    INT4                i4SeqNum = AP_FWL_ZERO;
    INT4                i4Action = AP_FWL_ZERO;
    UINT4               u4WtpProfileId = AP_FWL_ZERO;
    UINT4               u4WtpPrevProfileId = AP_FWL_ZERO;
    UINT4               u4UserGivenId = AP_FWL_ZERO;
    UINT1               au1WtpName[256];
    tSNMP_OCTET_STRING_TYPE WtpProfileName;

    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1WtpName, 0, 256);

    WtpProfileName.pu1_OctetList = au1WtpName;

    CliPrintf (CliHandle, "\r\n\t\tAp Firewall Access Lists");
    CliPrintf (CliHandle, "\r\n\t\t------------------------\r\n\r\n");
    CliPrintf (CliHandle,
               "Profile          ACL Name            Iface      Filter Combination\
        Dire-  Action  Prio- \r\n");
    CliPrintf (CliHandle,
               "                                                                  \
        ction          rity\r\n");
    CliPrintf (CliHandle,
               "-------     --------------------     ---------  ---------------------\
        ------ ------  -----\r\n");

    FwlAclName.pu1_OctetList = au1FwlAclName;
    NextFwlAclName.pu1_OctetList = au1NextFwlAclName;
    Temp.pu1_OctetList = au1Temp;
    if (pu1ProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4UserGivenId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Name Conversion - failed");
            return CLI_FAILURE;
        }
    }
    i4rc =
        nmhGetFirstIndexFsWtpFwlAclTable (&u4WtpProfileId, &i4FwlAclIfIndex,
                                          &FwlAclName, &i4FwlAclDirection);
    if (i4rc == SNMP_SUCCESS)
    {
        MEMCPY (NextFwlAclName.pu1_OctetList, FwlAclName.pu1_OctetList,
                FwlAclName.i4_Length);
        NextFwlAclName.i4_Length = FwlAclName.i4_Length;
        au1NextFwlAclName[NextFwlAclName.i4_Length] = AP_FWL_END_OF_STRING;
        i4NextFwlAclIfIndex = i4FwlAclIfIndex;
        i4NextFwlAclDirection = i4FwlAclDirection;
        do
        {
            MEMCPY (FwlAclName.pu1_OctetList, NextFwlAclName.pu1_OctetList,
                    NextFwlAclName.i4_Length);
            FwlAclName.i4_Length = NextFwlAclName.i4_Length;

            i4FwlAclIfIndex = i4NextFwlAclIfIndex;
            i4FwlAclDirection = i4NextFwlAclDirection;
            u4WtpPrevProfileId = u4WtpProfileId;

            if (nmhGetCapwapBaseWtpProfileName
                (u4WtpProfileId, &WtpProfileName) == SNMP_SUCCESS)
            {
                if (pu1ProfileName != NULL)
                {
                    if (u4WtpProfileId != u4UserGivenId)
                    {
                        continue;
                    }
                }

                MEMSET (au1IfName, '\0', sizeof (au1IfName));

                nmhGetFsWtpFwlAclSequenceNumber (u4WtpProfileId,
                                                 i4NextFwlAclIfIndex,
                                                 &NextFwlAclName,
                                                 i4NextFwlAclDirection,
                                                 &i4SeqNum);

                nmhGetFsWtpFwlAclAction (u4WtpProfileId, i4NextFwlAclIfIndex,
                                         &NextFwlAclName, i4NextFwlAclDirection,
                                         &i4Action);

                if (i4NextFwlAclIfIndex == 0)
                {
                    STRNCPY (au1IfName, "Global", STRLEN ("Global"));
                }
                else
                {
                    /*
                       CfaCliGetIfName ((UINT4) i4NextFwlAclIfIndex,
                       (INT1 *) au1IfName); */
                }
                CliPrintf (CliHandle, "%-12s", WtpProfileName.pu1_OctetList);
                CliPrintf (CliHandle, "%-25s", au1NextFwlAclName);
                CliPrintf (CliHandle, "%-11s", au1IfName);

                nmhGetFsWtpFwlRuleFilterSet (u4WtpProfileId, &NextFwlAclName,
                                             &Temp);
                au1Temp[Temp.i4_Length] = '\0';

                CliPrintf (CliHandle, "%-26s", au1Temp);

                if (i4NextFwlAclDirection == AP_FWL_DIRECTION_IN)
                {
                    CliPrintf (CliHandle, "%-7s", "in");
                }
                else
                {
                    CliPrintf (CliHandle, "%-7s", "out");
                }

                if (i4Action == WSSCFG_PERMIT)
                {
                    STRCPY (&au1Temp, "permit");
                }
                else
                {
                    STRCPY (&au1Temp, "deny");
                }
                CliPrintf (CliHandle, "%-8s", au1Temp);
                CliPrintf (CliHandle, "%-6d", i4SeqNum);
                CliPrintf (CliHandle, "\r\n");
            }
        }                        /* get the next elem */
        while
            (nmhGetNextIndexFsWtpFwlAclTable
             (u4WtpPrevProfileId, &u4WtpProfileId, i4FwlAclIfIndex,
              &i4NextFwlAclIfIndex, &FwlAclName, &NextFwlAclName,
              i4FwlAclDirection, &i4NextFwlAclDirection) == SNMP_SUCCESS);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FsWssApCliGetL3IfIndexFromPort                     */
/*                                                                           */
/*     DESCRIPTION      : This function returns IFIndex provided Physical    */
/*              Port and Vlan Id                      */
/*                                                                           */
/*     INPUT            : u4PhyPort - Physical Port                          */
/*              u4VlanId - Vlan Identifier                 */
/*     OUTPUT           : IfIndex - Interface Index of L3SUBIF               */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
FsWssApCliGetL3IfIndexFromPort (UINT4 u4PhyPort, UINT4 u4VlanId,
                                UINT4 *pu4IfIndex)
{
    INT4                i4RetVal = CLI_FAILURE;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4WtpNextProfileId = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4NextIfIndex = 0;
    UINT4               u4IfPhyPort = 0;
    UINT4               u4EncapVlanId = 0;

    if ((nmhGetFirstIndexFsWtpIfMainTable (&u4WtpNextProfileId,
                                           (INT4 *) &u4NextIfIndex)) ==
        SNMP_SUCCESS)
    {
        do
        {
            u4WtpProfileId = u4WtpNextProfileId;
            u4IfIndex = u4NextIfIndex;

            if ((nmhGetFsWtpIfMainEncapDot1qVlanId (u4WtpNextProfileId,
                                                    (INT4) u4NextIfIndex,
                                                    (INT4 *) &u4EncapVlanId)) !=
                SNMP_SUCCESS)
            {
                continue;
            }
            if ((nmhGetFsWtpIfMainPhyPort (u4WtpNextProfileId,
                                           u4NextIfIndex,
                                           (INT4 *) &u4IfPhyPort)) !=
                SNMP_SUCCESS)
            {
                continue;
            }
            if ((u4IfPhyPort == u4PhyPort) && (u4EncapVlanId == u4VlanId))
            {
                *pu4IfIndex = u4NextIfIndex;
                i4RetVal = CLI_SUCCESS;
                break;
            }
        }
        while ((nmhGetNextIndexFsWtpIfMainTable (u4WtpProfileId,
                                                 &u4WtpNextProfileId,
                                                 (INT4) u4IfIndex,
                                                 (INT4 *) &u4NextIfIndex)) ==
               SNMP_SUCCESS);
    }
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FsWssApCliGetFreeIfIndex                           */
/*                                                                           */
/*     DESCRIPTION      : This function returns the free IfIndex for         */
/*              L3 SUb Interface                      */
/*                                                                           */
/*     INPUT            : u1IfType - Interface Type                          */
/*     OUTPUT           : IfIndex - Free Interface Index                     */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
FsWssApCliGetFreeIfIndex (UINT1 u1IfType, UINT4 *pu4FreeIfIndex)
{
    INT4                i4RetVal = CLI_FAILURE;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4WtpNextProfileId = 0;
    UINT4               u4Index = 0;
    UINT1               u1EntryFound = 0;
    INT4                i4RowStatus = 0;

    UNUSED_PARAM (u1IfType);

    for (u4Index = 1; u4Index <= MAX_WTP_L3_SUB_IF_POOL; u4Index++)
    {
        if (nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId)
            == SNMP_SUCCESS)
        {
            do
            {
                if ((nmhGetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                 (INT4) u4Index,
                                                 &i4RowStatus)) == SNMP_SUCCESS)
                {
                    u1EntryFound = TRUE;
                    break;
                }
                u4WtpProfileId = u4WtpNextProfileId;
            }
            while ((nmhGetNextIndexCapwapBaseWtpProfileTable
                    (u4WtpProfileId, &u4WtpNextProfileId)) == SNMP_SUCCESS);

            if (u1EntryFound != TRUE)
            {
                *pu4FreeIfIndex = u4Index;
                i4RetVal = CLI_SUCCESS;
                break;
            }
        }
        u1EntryFound = FALSE;
    }
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FsWssApCliCreateL3SubIf                            */
/*                                                                           */
/*     DESCRIPTION      : This function creates the L3SUB IF and update in   */
/*              the DB to be passed on AP                 */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*              pu1WtpProfileName - WTP PRofile Name              */
/*              u4IfIndex, i4PhyPort, i4VlanId              */
/*     OUTPUT           : NONE                                               */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
FsWssApCliCreateL3SubIf (tCliHandle CliHandle, UINT1 *pu1WtpProfileName,
                         UINT4 u4IfIndex, INT4 i4PhyPort, INT4 i4VlanId)
{
    INT4                i4RetVal = CLI_SUCCESS;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4WtpNextProfileId = 0;
    INT4                i4IfRowStatus = 0;
    UINT4               u4ErrorCode = 0;
    UINT1               au1IfName[MAX_PROMPT_LEN];

    MEMSET (au1IfName, 0, MAX_PROMPT_LEN);

    if (pu1WtpProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1WtpProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Invalid Profile Id \r\n");
            return CLI_FAILURE;
        }

        /* check if the interface rowstatus can be set */
        if (nmhGetFsWtpIfMainRowStatus (u4WtpProfileId,
                                        u4IfIndex,
                                        &i4IfRowStatus) != SNMP_SUCCESS)
        {
            if ((nmhTestv2FsWtpIfMainRowStatus (&u4ErrorCode, u4WtpProfileId,
                                                u4IfIndex,
                                                CREATE_AND_WAIT)) ==
                SNMP_SUCCESS)
            {
                if ((nmhSetFsWtpIfMainRowStatus (u4WtpProfileId,
                                                 (INT4) u4IfIndex,
                                                 CREATE_AND_WAIT)) !=
                    SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r%% Failed to create L3 SUB IF \r\n");
                    return CLI_FAILURE;
                }

                /* Update Physical port number */
                if ((nmhTestv2FsWtpIfMainPhyPort (&u4ErrorCode, u4WtpProfileId,
                                                  u4IfIndex,
                                                  i4PhyPort)) != SNMP_SUCCESS)
                {
                    switch (u4ErrorCode)
                    {
                        case SNMP_ERR_WRONG_VALUE:
                            CliPrintf (CliHandle, "\r%% Invalid Phy Port \r\n");
                            break;
                        default:
                            CliPrintf (CliHandle,
                                       "\r%% Invalid Interface \r\n");
                            break;
                    }
                    i4RetVal = CLI_FAILURE;
                }
                else
                {
                    if ((nmhSetFsWtpIfMainPhyPort
                         (u4WtpProfileId, (INT4) u4IfIndex,
                          i4PhyPort)) != SNMP_SUCCESS)
                    {
                        i4RetVal = CLI_FAILURE;
                    }
                }

                /* Update VLAN Id information */
                if ((nmhTestv2FsWtpIfMainEncapDot1qVlanId
                     (&u4ErrorCode, u4WtpProfileId, (INT4) u4IfIndex,
                      i4VlanId)) != SNMP_SUCCESS)
                {
                    switch (u4ErrorCode)
                    {
                        case SNMP_ERR_NO_CREATION:
                            CliPrintf (CliHandle,
                                       "\r%% L3 SUB IF Entry is not created yet.\r\n");
                            break;
                        case SNMP_ERR_WRONG_VALUE:
                            CliPrintf (CliHandle, "\r%% Invalid Vlan ID \r\n");
                            break;
                        default:
                            CliPrintf (CliHandle,
                                       "\r%% Invalid Interface \r\n");
                            break;
                    }
                    i4RetVal = CLI_FAILURE;
                }
                else
                {

                    if ((nmhSetFsWtpIfMainEncapDot1qVlanId
                         (u4WtpProfileId, (INT4) u4IfIndex,
                          i4VlanId)) != SNMP_SUCCESS)
                    {
                        i4RetVal = CLI_FAILURE;
                    }
                }

                /* Set Interface Type - Only L3 SUB Interface is used now. */
                if (nmhSetFsWtpIfMainType (u4WtpProfileId, (INT4) u4IfIndex,
                                           AP_L3SUBIF) != SNMP_SUCCESS)
                {
                    i4RetVal = CLI_FAILURE;
                }

                /* Make the row status as ACTIVE */
                if (i4RetVal != CLI_FAILURE)
                {
                    if ((nmhSetFsWtpIfMainRowStatus
                         (u4WtpProfileId, (INT4) u4IfIndex,
                          ACTIVE)) != SNMP_FAILURE)
                    {
                        /* Change the mode to iss(config-subif)# mode */
                        SPRINTF ((CHR1 *) au1IfName, "%s%d",
                                 CLI_AP_L3SUBIF_MODE, u4IfIndex);
                        if (CliChangePath ((CHR1 *) au1IfName) != CLI_SUCCESS)
                        {
                        }
                        return CLI_SUCCESS;
                    }
                    else
                    {
                        i4RetVal = CLI_FAILURE;
                    }
                }

                if (i4RetVal == CLI_FAILURE)
                {
                    nmhSetFsWtpIfMainRowStatus (u4WtpProfileId,
                                                (INT4) u4IfIndex, DESTROY);
                    return CLI_FAILURE;
                }
            }
            else
            {
                CliPrintf (CliHandle, "\r%% Failed to create L3 SUB IF \r\n");
                return CLI_FAILURE;
            }
        }
    }
    else
    {
        if (nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpNextProfileId)
            == SNMP_SUCCESS)
        {
            do
            {
                /* check if the interface rowstatus can be set */
                if (nmhGetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                u4IfIndex,
                                                &i4IfRowStatus) != SNMP_SUCCESS)
                {
                    if ((nmhTestv2FsWtpIfMainRowStatus
                         (&u4ErrorCode, u4WtpNextProfileId, u4IfIndex,
                          CREATE_AND_WAIT)) == SNMP_SUCCESS)
                    {
                        if ((nmhSetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                         (INT4) u4IfIndex,
                                                         CREATE_AND_WAIT)) !=
                            SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r%% Failed to create L3 SUB IF \r\n");
                            continue;
                        }

                        /* Update Physical port number */
                        if ((nmhTestv2FsWtpIfMainPhyPort
                             (&u4ErrorCode, u4WtpNextProfileId, u4IfIndex,
                              i4PhyPort)) != SNMP_SUCCESS)
                        {
                            switch (u4ErrorCode)
                            {
                                case SNMP_ERR_WRONG_VALUE:
                                    CliPrintf (CliHandle,
                                               "\r%% Invalid Phy Port \r\n");
                                    break;
                                default:
                                    CliPrintf (CliHandle,
                                               "\r%% Invalid Interface \r\n");
                                    break;
                            }
                            nmhSetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                        (INT4) u4IfIndex,
                                                        DESTROY);
                            continue;
                        }
                        else
                        {
                            if ((nmhSetFsWtpIfMainPhyPort
                                 (u4WtpNextProfileId, (INT4) u4IfIndex,
                                  i4PhyPort)) != SNMP_SUCCESS)
                            {
                                nmhSetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                            (INT4) u4IfIndex,
                                                            DESTROY);
                                continue;
                            }
                        }

                        /* Update VLAN Id information */
                        if ((nmhTestv2FsWtpIfMainEncapDot1qVlanId
                             (&u4ErrorCode, u4WtpNextProfileId,
                              (INT4) u4IfIndex, i4VlanId)) != SNMP_SUCCESS)
                        {
                            switch (u4ErrorCode)
                            {
                                case SNMP_ERR_WRONG_VALUE:
                                    CliPrintf (CliHandle,
                                               "\r%% Invalid Vlan ID \r\n");
                                    break;
                                default:
                                    CliPrintf (CliHandle,
                                               "\r%% Invalid Interface \r\n");
                                    break;
                            }
                            nmhSetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                        (INT4) u4IfIndex,
                                                        DESTROY);
                            continue;
                        }
                        else
                        {

                            if ((nmhSetFsWtpIfMainEncapDot1qVlanId
                                 (u4WtpNextProfileId, (INT4) u4IfIndex,
                                  i4VlanId)) != SNMP_SUCCESS)
                            {
                                nmhSetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                            (INT4) u4IfIndex,
                                                            DESTROY);
                                continue;
                            }
                        }

                        /* Set Interface Type - Only L3 SUB Interface is used now. */
                        if (nmhSetFsWtpIfMainType
                            (u4WtpNextProfileId, (INT4) u4IfIndex,
                             AP_L3SUBIF) != SNMP_SUCCESS)
                        {
                            nmhSetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                        (INT4) u4IfIndex,
                                                        DESTROY);
                            continue;
                        }

                        /* Make the row status as ACTIVE */
                        if (i4RetVal != CLI_FAILURE)
                        {
                            if ((nmhSetFsWtpIfMainRowStatus
                                 (u4WtpNextProfileId, (INT4) u4IfIndex,
                                  ACTIVE)) != SNMP_FAILURE)
                            {
                                /* Change the mode to iss(config-subif)# mode */
                                SPRINTF ((CHR1 *) au1IfName, "%s%d",
                                         CLI_AP_L3SUBIF_MODE, u4IfIndex);
                                if (CliChangePath ((CHR1 *) au1IfName) !=
                                    CLI_SUCCESS)
                                {
                                }
                                continue;
                            }
                            else
                            {
                                nmhSetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                            (INT4) u4IfIndex,
                                                            DESTROY);
                                continue;
                            }
                        }

                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Failed to create L3 SUB IF \r\n");
                        continue;
                    }
                }
                u4WtpProfileId = u4WtpNextProfileId;
            }
            while (nmhGetNextIndexCapwapBaseWtpProfileTable
                   (u4WtpProfileId, &u4WtpNextProfileId) == SNMP_SUCCESS);
        }
    }
    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FsWssApCliDeleteL3SubIf                            */
/*                                                                           */
/*     DESCRIPTION      : This function deletes the L3SUB IF and update in   */
/*              the DB to be passed on AP                 */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*              pu1WtpProfileName - WTP PRofile Name              */
/*              u4IfIndex                                  */
/*     OUTPUT           : NONE                                               */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
FsWssApCliDeleteL3SubIf (tCliHandle CliHandle, UINT1 *pu1ProfileName,
                         UINT4 u4IfIndex)
{
    INT4                i4RetVal = CLI_FAILURE;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4WtpNextProfileId = 0;
    UINT4               u4ErrorCode = 0;

    if (pu1ProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Invalid Profile Id \r\n");
            return CLI_FAILURE;
        }

        /* Delete the Interface Entry */
        if ((nmhTestv2FsWtpIfMainRowStatus (&u4ErrorCode, u4WtpProfileId,
                                            u4IfIndex,
                                            DESTROY)) == SNMP_SUCCESS)
        {
            if ((nmhSetFsWtpIfMainRowStatus (u4WtpProfileId,
                                             (INT4) u4IfIndex,
                                             DESTROY)) == SNMP_SUCCESS)
            {
                i4RetVal = CLI_SUCCESS;
            }
        }
    }
    else
    {
        if (nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpNextProfileId)
            == SNMP_SUCCESS)
        {
            do
            {
                u4WtpProfileId = u4WtpNextProfileId;
                /* Delete the Interface Entry */
                if ((nmhTestv2FsWtpIfMainRowStatus
                     (&u4ErrorCode, u4WtpNextProfileId, u4IfIndex,
                      DESTROY)) == SNMP_SUCCESS)
                {
                    if ((nmhSetFsWtpIfMainRowStatus (u4WtpProfileId,
                                                     (INT4) u4IfIndex,
                                                     DESTROY)) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% Failed to Delete L3 SUB IF :: %d on AP %d\r\n",
                                   u4WtpProfileId, u4IfIndex);
                        continue;
                    }
                }
                i4RetVal = CLI_SUCCESS;
            }
            while (nmhGetNextIndexCapwapBaseWtpProfileTable
                   (u4WtpProfileId, &u4WtpNextProfileId) == SNMP_SUCCESS);
        }
    }
    if (i4RetVal == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Failed to Delete L3 SUB IF \r\n");
    }
    return (i4RetVal);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FsWssApCliSetL3SubIfNwType                         */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Network Type of L3 SUB IF   */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*              pu1WtpProfileName - WTP PRofile Name              */
/*              u4IfIndex, i4IfNwType                        */
/*     OUTPUT           : NONE                                               */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
FsWssApCliSetL3SubIfNwType (tCliHandle CliHandle, UINT4 u4IfIndex,
                            UINT4 u4WtpProfileId, INT4 i4IfNwType)
{
    UINT4               u4WtpNextProfileId = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4IfRowStatus = 0;

    UNUSED_PARAM (CliHandle);

    /* check if the profile is not NULL, else set network type for all the profiles */
    if (u4WtpProfileId != 0)
    {
        /* Check whether the interface exists */
        if ((nmhGetFsWtpIfMainRowStatus (u4WtpProfileId,
                                         (INT4) u4IfIndex,
                                         &i4IfRowStatus)) == SNMP_SUCCESS)
        {
            nmhSetFsWtpIfMainRowStatus (u4WtpProfileId,
                                        (INT4) u4IfIndex, NOT_IN_SERVICE);
            if ((nmhTestv2FsWtpIfMainNetworkType (&u4ErrorCode, u4WtpProfileId,
                                                  u4IfIndex,
                                                  i4IfNwType)) != SNMP_SUCCESS)
            {
                nmhSetFsWtpIfMainRowStatus (u4WtpProfileId,
                                            (INT4) u4IfIndex, ACTIVE);
                return CLI_FAILURE;
            }
            if ((nmhSetFsWtpIfMainNetworkType (u4WtpProfileId,
                                               u4IfIndex,
                                               i4IfNwType)) != SNMP_SUCCESS)
            {
                nmhSetFsWtpIfMainRowStatus (u4WtpProfileId,
                                            (INT4) u4IfIndex, ACTIVE);
                return CLI_FAILURE;
            }
            nmhSetFsWtpIfMainRowStatus (u4WtpProfileId,
                                        (INT4) u4IfIndex, ACTIVE);
        }
    }
    else
    {
        if (nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpNextProfileId)
            == SNMP_SUCCESS)
        {
            do
            {
                u4WtpProfileId = u4WtpNextProfileId;
                /* Delete the Interface Entry */
                if ((nmhGetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                 (INT4) u4IfIndex,
                                                 &i4IfRowStatus)) ==
                    SNMP_SUCCESS)
                {
                    nmhSetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                (INT4) u4IfIndex,
                                                NOT_IN_SERVICE);
                    if ((nmhTestv2FsWtpIfMainNetworkType
                         (&u4ErrorCode, u4WtpProfileId, u4IfIndex,
                          i4IfNwType)) != SNMP_SUCCESS)
                    {
                        nmhSetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                    (INT4) u4IfIndex, ACTIVE);
                        continue;
                    }
                    if ((nmhSetFsWtpIfMainNetworkType (u4WtpNextProfileId,
                                                       u4IfIndex,
                                                       i4IfNwType)) !=
                        SNMP_SUCCESS)
                    {
                        nmhSetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                    (INT4) u4IfIndex, ACTIVE);
                        continue;
                    }
                    nmhSetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                (INT4) u4IfIndex, ACTIVE);
                }
            }
            while (nmhGetNextIndexCapwapBaseWtpProfileTable
                   (u4WtpProfileId, &u4WtpNextProfileId) == SNMP_SUCCESS);
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FsWssApCliSetL3SubIfAdminStatus                    */
/*                                                                           */
/*     DESCRIPTION      : This function sets the AdminStatuse of L3 SUB IF   */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*              pu1WtpProfileName - WTP PRofile Name              */
/*              u4IfIndex, i4IfAdminStatus                   */
/*     OUTPUT           : NONE                                               */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
FsWssApCliSetL3SubIfAdminStatus (tCliHandle CliHandle, UINT4 u4IfIndex,
                                 UINT4 u4WtpProfileId, INT4 i4IfAdminStatus)
{
    UINT4               u4WtpNextProfileId = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4IfRowStatus = 0;

    UNUSED_PARAM (CliHandle);

    /* check if the profile is not NULL, else set admin status for all the profiles */
    if (u4WtpProfileId != 0)
    {
        /* Check whether the interface exists */
        if ((nmhGetFsWtpIfMainRowStatus (u4WtpProfileId,
                                         (INT4) u4IfIndex,
                                         &i4IfRowStatus)) == SNMP_SUCCESS)
        {
            nmhSetFsWtpIfMainRowStatus (u4WtpProfileId,
                                        (INT4) u4IfIndex, NOT_IN_SERVICE);

            if ((nmhTestv2FsWtpIfMainAdminStatus (&u4ErrorCode, u4WtpProfileId,
                                                  u4IfIndex,
                                                  i4IfAdminStatus)) !=
                SNMP_SUCCESS)
            {
                /* Make the rowstatus as active */
                nmhSetFsWtpIfMainRowStatus (u4WtpProfileId,
                                            (INT4) u4IfIndex, ACTIVE);
                return CLI_FAILURE;
            }

            if ((nmhSetFsWtpIfMainAdminStatus (u4WtpProfileId,
                                               u4IfIndex,
                                               i4IfAdminStatus)) !=
                SNMP_SUCCESS)
            {
                /* Make the rowstatus as active */
                nmhSetFsWtpIfMainRowStatus (u4WtpProfileId,
                                            (INT4) u4IfIndex, ACTIVE);
                return CLI_FAILURE;
            }
            /* Make the rowstatus as active */
            nmhSetFsWtpIfMainRowStatus (u4WtpProfileId,
                                        (INT4) u4IfIndex, ACTIVE);
        }
    }
    else
    {
        if (nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpNextProfileId)
            == SNMP_SUCCESS)
        {
            do
            {
                u4WtpProfileId = u4WtpNextProfileId;
                /* Set the rostatus of the interface as not in service */
                if ((nmhGetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                 (INT4) u4IfIndex,
                                                 &i4IfRowStatus)) ==
                    SNMP_SUCCESS)
                {
                    nmhSetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                (INT4) u4IfIndex,
                                                NOT_IN_SERVICE);
                    if ((nmhTestv2FsWtpIfMainAdminStatus
                         (&u4ErrorCode, u4WtpNextProfileId, u4IfIndex,
                          i4IfAdminStatus)) != SNMP_SUCCESS)
                    {
                        /* Make the rowstatus as active */
                        nmhSetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                    (INT4) u4IfIndex, ACTIVE);
                        CliPrintf (CliHandle,
                                   "\r%% Failed to admin status for IF : %d on AP : %d",
                                   u4IfIndex, u4WtpNextProfileId);
                        continue;
                    }
                    if ((nmhSetFsWtpIfMainAdminStatus (u4WtpNextProfileId,
                                                       u4IfIndex,
                                                       i4IfAdminStatus)) !=
                        SNMP_SUCCESS)
                    {
                        /* Make the rowstatus as active */
                        nmhSetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                    (INT4) u4IfIndex, ACTIVE);
                        CliPrintf (CliHandle,
                                   "\r%% Failed to admin status for IF : %d on AP : %d",
                                   u4IfIndex, u4WtpNextProfileId);
                        continue;
                    }
                    /* Make the rowstatus as active */
                    nmhSetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                (INT4) u4IfIndex, ACTIVE);
                }
            }
            while (nmhGetNextIndexCapwapBaseWtpProfileTable
                   (u4WtpProfileId, &u4WtpNextProfileId) == SNMP_SUCCESS);
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FsWssApCliSetL3SubIfIpAddr                         */
/*                                                                           */
/*     DESCRIPTION      : This function sets the IP Address of L3 SUB IF     */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*              pu1WtpProfileName - WTP PRofile Name              */
/*              u4IfIndex, u4IpAddr, u4SubnetMask               */
/*     OUTPUT           : NONE                                               */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
FsWssApCliSetL3SubIfIpAddr (tCliHandle CliHandle, UINT4 u4IfIndex,
                            UINT4 u4WtpProfileId, UINT4 u4IpAddr,
                            UINT4 u4SubnetMask)
{
    UINT4               u4WtpNextProfileId = 0;
    UINT4               u4PrevIpAddr = 0;
    UINT4               u4PrevSubnetMask = 0;
    UINT4               u4BroadCastAddr = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4IfRowStatus = 0;

    UNUSED_PARAM (CliHandle);

    /* check if the profile is not NULL, else set admin status for all the profiles */
    if (u4WtpProfileId != 0)
    {
        /* Check whether the interface exists */
        if ((nmhGetFsWtpIfMainRowStatus (u4WtpProfileId,
                                         (INT4) u4IfIndex,
                                         &i4IfRowStatus)) == SNMP_SUCCESS)
        {
            nmhSetFsWtpIfMainRowStatus (u4WtpProfileId,
                                        (INT4) u4IfIndex, NOT_IN_SERVICE);

            if ((FsWssApCliTestIpAddrAndSubnetMask (u4WtpProfileId, u4IfIndex,
                                                    u4IpAddr,
                                                    u4SubnetMask)) ==
                CLI_SUCCESS)
            {
                /* Get Previous IP addr and mask */
                nmhGetFsWtpIfIpAddr (u4WtpProfileId, u4IfIndex, &u4PrevIpAddr);
                nmhGetFsWtpIfIpSubnetMask (u4WtpProfileId, u4IfIndex,
                                           &u4PrevSubnetMask);
                if ((nmhSetFsWtpIfIpAddr (u4WtpProfileId, u4IfIndex, u4IpAddr))
                    != SNMP_SUCCESS)
                {
                    /* Make the rowstatus as active */
                    nmhSetFsWtpIfMainRowStatus (u4WtpProfileId,
                                                (INT4) u4IfIndex, ACTIVE);
                    return CLI_FAILURE;
                }

                if ((nmhSetFsWtpIfIpSubnetMask (u4WtpProfileId,
                                                u4IfIndex,
                                                u4SubnetMask)) != SNMP_SUCCESS)
                {
                    /* Make the rowstatus as active */
                    nmhSetFsWtpIfMainRowStatus (u4WtpProfileId,
                                                (INT4) u4IfIndex, ACTIVE);
                    nmhSetFsWtpIfIpAddr (u4WtpProfileId, (INT4) u4IfIndex,
                                         u4PrevIpAddr);
                    return CLI_FAILURE;
                }

                /* Set Broadcast Address */
                u4BroadCastAddr = u4IpAddr | (~(u4SubnetMask));
                if ((nmhTestv2FsWtpIfIpBroadcastAddr
                     (&u4ErrorCode, u4WtpProfileId, u4IfIndex,
                      u4BroadCastAddr)) == SNMP_SUCCESS)
                {
                    if ((nmhSetFsWtpIfIpBroadcastAddr (u4WtpProfileId,
                                                       u4IfIndex,
                                                       u4BroadCastAddr)) !=
                        SNMP_SUCCESS)
                    {
                        /* Make the rowstatus as active */
                        nmhSetFsWtpIfMainRowStatus (u4WtpProfileId,
                                                    (INT4) u4IfIndex, ACTIVE);
                        nmhSetFsWtpIfIpAddr (u4WtpProfileId, (INT4) u4IfIndex,
                                             u4PrevIpAddr);
                        nmhSetFsWtpIfIpSubnetMask (u4WtpProfileId,
                                                   (INT4) u4IfIndex,
                                                   u4PrevSubnetMask);
                        return CLI_FAILURE;
                    }
                }
                /* Make the rowstatus as active */
                nmhSetFsWtpIfMainRowStatus (u4WtpProfileId,
                                            (INT4) u4IfIndex, ACTIVE);
            }
            else
            {
                /* Make the rowstatus as active */
                nmhSetFsWtpIfMainRowStatus (u4WtpProfileId,
                                            (INT4) u4IfIndex, ACTIVE);
                return CLI_FAILURE;
            }
        }
    }
    else
    {
        if (nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpNextProfileId)
            == SNMP_SUCCESS)
        {
            do
            {
                u4WtpProfileId = u4WtpNextProfileId;
                /* Check whether the interface exists */
                if ((nmhGetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                 (INT4) u4IfIndex,
                                                 &i4IfRowStatus)) ==
                    SNMP_SUCCESS)
                {
                    nmhSetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                (INT4) u4IfIndex,
                                                NOT_IN_SERVICE);

                    if ((FsWssApCliTestIpAddrAndSubnetMask
                         (u4WtpNextProfileId, u4IfIndex, u4IpAddr,
                          u4SubnetMask)) == CLI_SUCCESS)
                    {
                        /* Get Previous IP addr and mask */
                        nmhGetFsWtpIfIpAddr (u4WtpNextProfileId, u4IfIndex,
                                             &u4PrevIpAddr);
                        nmhGetFsWtpIfIpSubnetMask (u4WtpNextProfileId,
                                                   u4IfIndex,
                                                   &u4PrevSubnetMask);
                        if ((nmhSetFsWtpIfIpAddr
                             (u4WtpNextProfileId, u4IfIndex,
                              u4IpAddr)) != SNMP_SUCCESS)
                        {
                            /* Make the rowstatus as active */
                            nmhSetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                        (INT4) u4IfIndex,
                                                        ACTIVE);
                            CliPrintf (CliHandle,
                                       "\r%% Failed to set IP ADDR for IF : %d on AP : %d",
                                       u4IfIndex, u4WtpNextProfileId);
                            continue;
                        }

                        if ((nmhSetFsWtpIfIpSubnetMask (u4WtpNextProfileId,
                                                        u4IfIndex,
                                                        u4SubnetMask)) !=
                            SNMP_SUCCESS)
                        {
                            /* Make the rowstatus as active */
                            nmhSetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                        (INT4) u4IfIndex,
                                                        ACTIVE);
                            nmhSetFsWtpIfIpAddr (u4WtpNextProfileId,
                                                 (INT4) u4IfIndex,
                                                 u4PrevIpAddr);
                            CliPrintf (CliHandle,
                                       "\r%% Failed to set IP ADDR for IF : %d on AP : %d",
                                       u4IfIndex, u4WtpNextProfileId);
                            continue;
                        }

                        /* Set Broadcast Address */
                        u4BroadCastAddr = u4IpAddr | (~(u4SubnetMask));
                        if ((nmhTestv2FsWtpIfIpBroadcastAddr
                             (&u4ErrorCode, u4WtpNextProfileId, u4IfIndex,
                              u4BroadCastAddr)) == SNMP_SUCCESS)
                        {
                            if ((nmhSetFsWtpIfIpBroadcastAddr
                                 (u4WtpNextProfileId, u4IfIndex,
                                  u4BroadCastAddr)) != SNMP_SUCCESS)
                            {
                                /* Make the rowstatus as active */
                                nmhSetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                            (INT4) u4IfIndex,
                                                            ACTIVE);
                                nmhSetFsWtpIfIpAddr (u4WtpNextProfileId,
                                                     (INT4) u4IfIndex,
                                                     u4PrevIpAddr);
                                nmhSetFsWtpIfIpSubnetMask (u4WtpNextProfileId,
                                                           (INT4) u4IfIndex,
                                                           u4PrevSubnetMask);
                                CliPrintf (CliHandle,
                                           "\r%% Failed to set IP ADDR for IF : %d on AP : %d",
                                           u4IfIndex, u4WtpNextProfileId);
                                continue;
                            }
                        }
                        /* Make the rowstatus as active */
                        nmhSetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                    (INT4) u4IfIndex, ACTIVE);
                    }
                    else
                    {
                        /* Make the rowstatus as active */
                        CliPrintf (CliHandle,
                                   "\r%% Failed to set IP ADDR for IF : %d on AP : %d",
                                   u4IfIndex, u4WtpNextProfileId);
                        nmhSetFsWtpIfMainRowStatus (u4WtpNextProfileId,
                                                    (INT4) u4IfIndex, ACTIVE);
                        continue;
                    }
                }

            }
            while (nmhGetNextIndexCapwapBaseWtpProfileTable
                   (u4WtpProfileId, &u4WtpNextProfileId) == SNMP_SUCCESS);
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FsWssApCliTestIpAddrAndSubnetMask                  */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given IP addr and mask */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*              pu1WtpProfileName - WTP PRofile Name              */
/*              u4IfIndex, u4TestValIfIpAddr                 */
/*              u4TestValIfIpSubnetMask                 */
/*     OUTPUT           : NONE                                               */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
FsWssApCliTestIpAddrAndSubnetMask (UINT4 u4WtpProfileId, UINT4 u4IfIndex,
                                   UINT4 u4TestValIfIpAddr,
                                   UINT4 u4TestValIfIpSubnetMask)
{
    UINT4               u4BroadCastAddr = 0;
    UINT4               u4NetworkIpAddr = 0;
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsWtpIfIpAddr (&u4ErrorCode, u4WtpProfileId, (INT4) u4IfIndex,
                                u4TestValIfIpAddr) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsWtpIfIpSubnetMask
        (&u4ErrorCode, u4WtpProfileId, (INT4) u4IfIndex,
         u4TestValIfIpSubnetMask) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_WSS_INVALID_SUBNET);
        return CLI_FAILURE;
    }

    /*when 'no ipaddress' command for primary ipaddress is executed then u4TestValIfIpAddr and u4TestValIfIpSubnetMask can have a value ZERO,if ipaddress command is executed u4TestValIfIpAddr and u4TestValIfIpSubnetMask always not equal to zero */
    if (u4TestValIfIpAddr == 0 && u4TestValIfIpSubnetMask == 0)
    {
        return CLI_SUCCESS;
    }

    /*when netmask is 31 bit mask we need NOT to check ip address with broadcast address */
    if ((u4TestValIfIpSubnetMask != 0xffffffff)
        && (u4TestValIfIpSubnetMask != IP_ADDR_31BIT_MASK))
    {
        /* ipaddress should not be Broadcast Address */
        u4BroadCastAddr = u4TestValIfIpAddr | (~(u4TestValIfIpSubnetMask));
        if (u4TestValIfIpAddr == u4BroadCastAddr)
        {
            CLI_SET_ERR (CLI_WSS_INVALID_SUBNET);
            return CLI_FAILURE;
        }

        /* ipaddress should not be Network Address */
        u4NetworkIpAddr = u4TestValIfIpAddr & (~(u4TestValIfIpSubnetMask));
        if (u4NetworkIpAddr == 0)
        {
            CLI_SET_ERR (CLI_WSS_INVALID_SUBNET);
            return CLI_FAILURE;
        }
    }

    /* the interface should not have any other interface above it */
    if (FsWssCliValidateIfIpSubnet (u4WtpProfileId, u4IfIndex,
                                    u4TestValIfIpAddr,
                                    u4TestValIfIpSubnetMask) != CLI_SUCCESS)
    {
        CLI_SET_ERR (CLI_WSS_IP_ADDR_EXISTS);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FsWssShowAPL3SubIf                                 */
/*                                                                           */
/*     DESCRIPTION      : This function dispplays the L3 SUB IF created on AP */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*              pu1WtpProfileName - WTP PRofile Name              */
/*     OUTPUT           : NONE                                               */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
FsWssShowAPL3SubIf (tCliHandle CliHandle, UINT1 *pu1ProfileName)
{
    UINT4               u4WtpNextProfileId = 0;
    UINT4               u4GivenProfileId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4WtpIfIndex = 0;
    UINT4               u4WtpNextIfIndex = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4SubnetMask = 0;
    UINT4               u4MaskBits = 0;
    INT4                i4RowStatus = 0;
    INT4                i4IfAdminStatus = 0;
    INT4                i4IfNwType = 0;
    INT4                i4IfPhyPort = 0;
    INT4                i4VlanId = 0;
    UINT1               au1WtpName[256];
    tSNMP_OCTET_STRING_TYPE WtpProfileName;
    CHR1               *pu1IpAddr = NULL;

    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1WtpName, 0, 256);

    WtpProfileName.pu1_OctetList = au1WtpName;
    if (pu1ProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile Name Conversion - failed");
            return CLI_FAILURE;
        }
        u4GivenProfileId = u4WtpProfileId;
        if (nmhGetFirstIndexFsWtpIfMainTable (&u4WtpNextProfileId,
                                              (INT4 *) &u4WtpNextIfIndex) ==
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n----------------------------------\r\n");

            do
            {
                u4WtpProfileId = u4WtpNextProfileId;
                u4WtpIfIndex = u4WtpNextIfIndex;

                if (u4GivenProfileId == u4WtpNextProfileId)
                {
                    if (nmhGetCapwapBaseWtpProfileName
                        (u4WtpNextProfileId, &WtpProfileName) == SNMP_SUCCESS)
                    {

                        CliPrintf (CliHandle, "AP Name : %s\r\n",
                                   WtpProfileName.pu1_OctetList);
                        if ((nmhGetFsWtpIfMainRowStatus
                             (u4WtpNextProfileId, u4WtpNextIfIndex,
                              &i4RowStatus) == SNMP_SUCCESS)
                            && (i4RowStatus == ACTIVE))
                        {

                            nmhGetFsWtpIfMainAdminStatus (u4WtpNextProfileId,
                                                          u4WtpNextIfIndex,
                                                          &i4IfAdminStatus);
                            nmhGetFsWtpIfMainNetworkType (u4WtpNextProfileId,
                                                          u4WtpNextIfIndex,
                                                          &i4IfNwType);
                            nmhGetFsWtpIfMainPhyPort (u4WtpNextProfileId,
                                                      u4WtpNextIfIndex,
                                                      &i4IfPhyPort);
                            nmhGetFsWtpIfMainEncapDot1qVlanId
                                (u4WtpNextProfileId, u4WtpNextIfIndex,
                                 &i4VlanId);
                            nmhGetFsWtpIfIpAddr (u4WtpNextProfileId,
                                                 u4WtpNextIfIndex, &u4IpAddr);
                            nmhGetFsWtpIfIpSubnetMask (u4WtpNextProfileId,
                                                       u4WtpNextIfIndex,
                                                       &u4SubnetMask);

                            CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4IpAddr);
                            u4MaskBits = CliGetMaskBits (u4SubnetMask);

                            if (i4IfAdminStatus == AP_IF_UP)
                            {
                                CliPrintf (CliHandle,
                                           "SUB Interface : %d/%d is up\r\n",
                                           i4IfPhyPort, i4VlanId);
                            }
                            else
                            {
                                CliPrintf (CliHandle,
                                           "SUB Interface : %d/%d is down\r\n",
                                           i4IfPhyPort, i4VlanId);
                            }
                            CliPrintf (CliHandle, "Ip Address is %s/",
                                       pu1IpAddr);
                            CliPrintf (CliHandle, "%d\r\n", u4MaskBits);

                            if (i4IfNwType == NETWORK_TYPE_WAN)
                            {

                                CliPrintf (CliHandle, "Network type is WAN ",
                                           i4IfNwType);
                            }
                            else
                            {
                                CliPrintf (CliHandle, "Network type is LAN ",
                                           i4IfNwType);
                            }
                            CliPrintf (CliHandle, "\r\n");
                        }
                    }
                }
            }
            while (nmhGetNextIndexFsWtpIfMainTable (u4WtpProfileId,
                                                    &u4WtpNextProfileId,
                                                    (INT4) u4WtpIfIndex,
                                                    (INT4 *) &u4WtpNextIfIndex)
                   == SNMP_SUCCESS);
        }
    }
    else
    {
        if (nmhGetFirstIndexFsWtpIfMainTable (&u4WtpNextProfileId,
                                              (INT4 *) &u4WtpNextIfIndex) ==
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n--------------------------------\r\n");

            do
            {
                u4WtpProfileId = u4WtpNextProfileId;
                u4WtpIfIndex = u4WtpNextIfIndex;

                if (nmhGetCapwapBaseWtpProfileName
                    (u4WtpNextProfileId, &WtpProfileName) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "AP Name : %s\r\n",
                               WtpProfileName.pu1_OctetList);

                    if ((nmhGetFsWtpIfMainRowStatus
                         (u4WtpNextProfileId, u4WtpNextIfIndex,
                          &i4RowStatus) == SNMP_SUCCESS)
                        && (i4RowStatus == ACTIVE))
                    {

                        nmhGetFsWtpIfMainAdminStatus (u4WtpNextProfileId,
                                                      u4WtpNextIfIndex,
                                                      &i4IfAdminStatus);
                        nmhGetFsWtpIfMainNetworkType (u4WtpNextProfileId,
                                                      u4WtpNextIfIndex,
                                                      &i4IfNwType);
                        nmhGetFsWtpIfMainPhyPort (u4WtpNextProfileId,
                                                  u4WtpNextIfIndex,
                                                  &i4IfPhyPort);
                        nmhGetFsWtpIfMainEncapDot1qVlanId (u4WtpNextProfileId,
                                                           u4WtpNextIfIndex,
                                                           &i4VlanId);
                        nmhGetFsWtpIfIpAddr (u4WtpNextProfileId,
                                             u4WtpNextIfIndex, &u4IpAddr);
                        nmhGetFsWtpIfIpSubnetMask (u4WtpNextProfileId,
                                                   u4WtpNextIfIndex,
                                                   &u4SubnetMask);

                        CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4IpAddr);
                        u4MaskBits = CliGetMaskBits (u4SubnetMask);

                        if (i4IfAdminStatus == AP_IF_UP)
                        {
                            CliPrintf (CliHandle,
                                       "SUB Interface : %d/%d is up\r\n",
                                       i4IfPhyPort, i4VlanId);
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "SUB Interface : %d/%d is down\r\n",
                                       i4IfPhyPort, i4VlanId);
                        }
                        CliPrintf (CliHandle, "Ip Address is %s/", pu1IpAddr);
                        CliPrintf (CliHandle, "%d\r\n", u4MaskBits);

                        if (i4IfNwType == NETWORK_TYPE_WAN)
                        {

                            CliPrintf (CliHandle, "Network type is WAN ",
                                       i4IfNwType);
                        }
                        else
                        {
                            CliPrintf (CliHandle, "Network type is LAN ",
                                       i4IfNwType);
                        }

                        CliPrintf (CliHandle, "\r\n");
                    }
                }
            }
            while (nmhGetNextIndexFsWtpIfMainTable (u4WtpProfileId,
                                                    &u4WtpNextProfileId,
                                                    (INT4) u4WtpIfIndex,
                                                    (INT4 *) &u4WtpNextIfIndex)
                   == SNMP_SUCCESS);
        }
    }
    return CLI_SUCCESS;
}

#endif
