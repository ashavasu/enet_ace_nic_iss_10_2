/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
*  $Id: wsscfgdsg.c,v 1.4.2.1 2018/03/09 13:25:51 siva Exp $
*
* Description: This file contains the routines for DataStructure access for the module Wsscfg 
*********************************************************************/

#include "wsscfginc.h"
#include "wsscfgcli.h"

#include "capwapcli.h"
#include "capwapclidefg.h"
#include "capwapclitdfsg.h"
#include "capwapclitdfs.h"
#include "capwapcliprotg.h"

extern tRBTree      gWssPmWlanSSIDStatsProcessDB;
extern tRBTree      gWssPmWlanClientStatsProcessDB;
extern tRBTree      gWssPmWlanRadioStatsProcessDB;
extern tRBTree      gWssPmCapwATPStatsProcessDB;
extern tRBTree      gWssPmWLANBSSIDStatsProcessDB;
extern tRBTree      gWssStaWebAuthDB;
/****************************************************************************
 Function    :  WsscfgGetFirstDot11StationConfigTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11StationConfigEntry or NULL
****************************************************************************/
INT1
WsscfgGetFirstDot11StationConfigTable (INT4 *pi4IfIndex)
{
    tWssWlanIfIndexDB  *pWssWlanIfIndexDB = NULL;
    tRadioIfDB         *pRadioIfDB = NULL;

    /* trying logic to look through both database and return in ascending order */
    pWssWlanIfIndexDB =
        (tWssWlanIfIndexDB *) RBTreeGetFirst (gWssWlanGlobals.
                                              WssWlanGlbMib.WssWlanIfIndexDB);
    pRadioIfDB =
        (tRadioIfDB *) RBTreeGetFirst (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB);
    if ((pWssWlanIfIndexDB != NULL) && (pRadioIfDB == NULL))
    {
        /* only wlan-if is present, return it */
        *pi4IfIndex = pWssWlanIfIndexDB->u4WlanIfIndex;
        return SNMP_SUCCESS;
    }
    else if ((pWssWlanIfIndexDB == NULL) && (pRadioIfDB != NULL))
    {
        /* only radio-if is present, return it */
        *pi4IfIndex = pRadioIfDB->u4VirtualRadioIfIndex;
        return SNMP_SUCCESS;
    }
    else if ((pWssWlanIfIndexDB != NULL) && (pRadioIfDB != NULL))
    {
        /* both are present, compare and return the minimum of it */
        *pi4IfIndex =
            (pWssWlanIfIndexDB->u4WlanIfIndex >
             pRadioIfDB->
             u4VirtualRadioIfIndex) ?
            pRadioIfDB->u4VirtualRadioIfIndex : pWssWlanIfIndexDB->
            u4WlanIfIndex;
        return SNMP_SUCCESS;
    }
    /* no entry present, just return error */

    return SNMP_FAILURE;
}

/* WSS Changes Starts */

/****************************************************************************
 Function    :  WsscfgGetFirstDot11StationConfigTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11StationConfigEntry or NULL
****************************************************************************/
INT1
WsscfgGetFirstDot11CountersTable (INT4 *pIfIndex)
{
    tRadioIfDB         *pRadioIfDB = NULL;

    pRadioIfDB = (tRadioIfDB *) RBTreeGetFirst (gRadioIfGlobals.
                                                RadioIfGlbMib.RadioIfDB);
    if (NULL == pRadioIfDB)
    {
        /* no entry present, just return error */
        return SNMP_FAILURE;
    }

    *pIfIndex = pRadioIfDB->u4VirtualRadioIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11StationConfigTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11StationConfigEntry or NULL
****************************************************************************/
tWsscfgDot11RSNAStatsEntry *
WsscfgGetFirstDot11RSNAStatsTable ()
{
    tWsscfgDot11RSNAStatsEntry *pWsscfgDot11RSNAStatsEntry = NULL;

    pWsscfgDot11RSNAStatsEntry =
        (tWsscfgDot11RSNAStatsEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.Dot11RSNAStatsTable);

    return pWsscfgDot11RSNAStatsEntry;
}

/****************************************************************************
 Function    :  
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :   or NULL
****************************************************************************/
tWsscfgDot11QosCountersEntry *
WsscfgGetFirstDot11QosCountersTable ()
{
    tWsscfgDot11QosCountersEntry *pWsscfgDot11QosCountersEntry = NULL;

    pWsscfgDot11QosCountersEntry =
        (tWsscfgDot11QosCountersEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.Dot11QosCountersTable);

    return pWsscfgDot11QosCountersEntry;
}

/****************************************************************************
 Function    :  
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :   or NULL
****************************************************************************/
tWsscfgDot11ResourceInfoEntry *
WsscfgGetFirstDot11ResourceInfoTable ()
{
    tWsscfgDot11ResourceInfoEntry *pWsscfgDot11ResourceInfoEntry = NULL;

    pWsscfgDot11ResourceInfoEntry =
        (tWsscfgDot11ResourceInfoEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.Dot11ResourceInfoTable);

    return pWsscfgDot11ResourceInfoEntry;
}

/****************************************************************************
 Function    :  
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :   or NULL
****************************************************************************/
tWsscfgDot11RegDomainsSupportedEntry
    * WsscfgGetFirstDot11RegDomainsSupportedTable ()
{
    tWsscfgDot11RegDomainsSupportedEntry
        * pWsscfgDot11RegDomainsSupportedEntry = NULL;

    pWsscfgDot11RegDomainsSupportedEntry =
        (tWsscfgDot11RegDomainsSupportedEntry *)
        RBTreeGetFirst (gWsscfgGlobals.
                        WsscfgGlbMib.Dot11RegDomainsSupportedTable);

    return pWsscfgDot11RegDomainsSupportedEntry;
}

/****************************************************************************
 Function    :  
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :   or NULL
****************************************************************************/
tWsscfgDot11SupportedDataRatesTxEntry
    * WsscfgGetFirstDot11SupportedDataRatesTxTable ()
{
    tWsscfgDot11SupportedDataRatesTxEntry
        * pWsscfgDot11SupportedDataRatesTxEntry = NULL;

    pWsscfgDot11SupportedDataRatesTxEntry =
        (tWsscfgDot11SupportedDataRatesTxEntry *)
        RBTreeGetFirst (gWsscfgGlobals.
                        WsscfgGlbMib.Dot11SupportedDataRatesTxTable);

    return pWsscfgDot11SupportedDataRatesTxEntry;
}

/****************************************************************************
 Function    :  
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :   or NULL
****************************************************************************/
tWsscfgDot11SupportedDataRatesRxEntry
    * WsscfgGetFirstDot11SupportedDataRatesRxTable ()
{
    tWsscfgDot11SupportedDataRatesRxEntry
        * pWsscfgDot11SupportedDataRatesRxEntry = NULL;

    pWsscfgDot11SupportedDataRatesRxEntry =
        (tWsscfgDot11SupportedDataRatesRxEntry *)
        RBTreeGetFirst (gWsscfgGlobals.
                        WsscfgGlbMib.Dot11SupportedDataRatesRxTable);

    return pWsscfgDot11SupportedDataRatesRxEntry;
}

/****************************************************************************
 Function    :  
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :   or NULL
****************************************************************************/
tWsscfgDot11PhyHRDSSSEntry *
WsscfgGetFirstDot11PhyHRDSSSTable ()
{
    tWsscfgDot11PhyHRDSSSEntry *pWsscfgDot11PhyHRDSSSEntry = NULL;

    pWsscfgDot11PhyHRDSSSEntry =
        (tWsscfgDot11PhyHRDSSSEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyHRDSSSTable);

    return pWsscfgDot11PhyHRDSSSEntry;
}

/* WSS Changes Ends */

/****************************************************************************
 Function    :  WsscfgGetNextDot11StationConfigTable
 Input       :  pCurrentWsscfgDot11StationConfigEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11StationConfigEntry or NULL
****************************************************************************/
INT1
WsscfgGetNextDot11StationConfigTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tWssWlanIfIndexDB   WssWlanIfIndexDB;
    tRadioIfDB          RadioIfDB;
    tRadioIfDB         *pNextRadioIfDB = NULL;
    tWssWlanIfIndexDB  *pNextWssWlanIfIndexDB = NULL;

    MEMSET (&WssWlanIfIndexDB, 0, sizeof (tWssWlanIfIndexDB));
    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));
    /* assign index */
    WssWlanIfIndexDB.u4WlanIfIndex = i4IfIndex;
    RadioIfDB.u4VirtualRadioIfIndex = i4IfIndex;

    /* trying logic to look through both database and return in ascending order */
    pNextWssWlanIfIndexDB =
        (tWssWlanIfIndexDB *) RBTreeGetNext (gWssWlanGlobals.
                                             WssWlanGlbMib.WssWlanIfIndexDB,
                                             (tRBElem *) &
                                             WssWlanIfIndexDB, NULL);
    pNextRadioIfDB =
        (tRadioIfDB *) RBTreeGetNext (gRadioIfGlobals.
                                      RadioIfGlbMib.RadioIfDB,
                                      (tRBElem *) & RadioIfDB, NULL);
    if ((pNextWssWlanIfIndexDB != NULL) && (pNextRadioIfDB == NULL))
    {
        /* only wlan-if is present, return it */
        *pi4NextIfIndex = pNextWssWlanIfIndexDB->u4WlanIfIndex;
        return SNMP_SUCCESS;
    }
    else if ((pNextWssWlanIfIndexDB == NULL) && (pNextRadioIfDB != NULL))
    {
        /* only radio-if is present, return it */
        *pi4NextIfIndex = pNextRadioIfDB->u4VirtualRadioIfIndex;
        return SNMP_SUCCESS;
    }
    else if ((pNextWssWlanIfIndexDB != NULL) && (pNextRadioIfDB != NULL))
    {
        /* both are present, compare and return the minimum of it */
        *pi4NextIfIndex =
            (pNextWssWlanIfIndexDB->u4WlanIfIndex >
             pNextRadioIfDB->
             u4VirtualRadioIfIndex) ?
            pNextRadioIfDB->u4VirtualRadioIfIndex :
            pNextWssWlanIfIndexDB->u4WlanIfIndex;
        return SNMP_SUCCESS;
    }
    /* no entry present, just return error */

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  WsscfgGetDot11StationConfigTable
 Input       :  pWsscfgDot11StationConfigEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11StationConfigEntry or NULL
****************************************************************************/
tWsscfgDot11StationConfigEntry *
WsscfgGetDot11StationConfigTable (tWsscfgDot11StationConfigEntry *
                                  pWsscfgDot11StationConfigEntry)
{
    tWsscfgDot11StationConfigEntry *pGetWsscfgDot11StationConfigEntry = NULL;

    pGetWsscfgDot11StationConfigEntry =
        (tWsscfgDot11StationConfigEntry *)
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11StationConfigTable,
                   (tRBElem *) pWsscfgDot11StationConfigEntry);

    return pGetWsscfgDot11StationConfigEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11AuthenticationAlgorithmsTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11AuthenticationAlgorithmsEntry or NULL
****************************************************************************/

INT1
WsscfgGetFirstDot11AuthenticationAlgorithmsTable (INT4 *pi4IfIndex)
{
    tWssWlanIfIndexDB  *pWssWlanIfIndexDB = NULL;

    pWssWlanIfIndexDB =
        (tWssWlanIfIndexDB *) RBTreeGetFirst (gWssWlanGlobals.
                                              WssWlanGlbMib.WssWlanIfIndexDB);
    if (NULL == pWssWlanIfIndexDB)
    {
        return SNMP_FAILURE;
    }
    *pi4IfIndex = pWssWlanIfIndexDB->u4WlanIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11AuthenticationAlgorithmsTable
 Input       :  pCurrentWsscfgDot11AuthenticationAlgorithmsEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11AuthenticationAlgorithmsEntry or NULL
****************************************************************************/
INT1
WsscfgGetNextDot11AuthenticationAlgorithmsTable (INT4 i4IfIndex,
                                                 INT4 *pi4NextIfIndex)
{
    tWssWlanIfIndexDB   WssWlanIfIndexDB;
    tWssWlanIfIndexDB  *pNextWssWlanIfIndexDB = NULL;

    MEMSET (&WssWlanIfIndexDB, 0, sizeof (tWssWlanIfIndexDB));
    WssWlanIfIndexDB.u4WlanIfIndex = i4IfIndex;

    pNextWssWlanIfIndexDB =
        (tWssWlanIfIndexDB *) RBTreeGetNext (gWssWlanGlobals.
                                             WssWlanGlbMib.WssWlanIfIndexDB,
                                             (tRBElem *) &
                                             WssWlanIfIndexDB, NULL);
    if (NULL == pNextWssWlanIfIndexDB)
    {
        return SNMP_FAILURE;
    }
    *pi4NextIfIndex = pNextWssWlanIfIndexDB->u4WlanIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetDot11AuthenticationAlgorithmsTable
 Input       :  pWsscfgDot11AuthenticationAlgorithmsEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11AuthenticationAlgorithmsEntry or NULL
****************************************************************************/
tWsscfgDot11AuthenticationAlgorithmsEntry
    *
    WsscfgGetDot11AuthenticationAlgorithmsTable
    (tWsscfgDot11AuthenticationAlgorithmsEntry *
     pWsscfgDot11AuthenticationAlgorithmsEntry)
{
    tWsscfgDot11AuthenticationAlgorithmsEntry
        * pGetWsscfgDot11AuthenticationAlgorithmsEntry = NULL;

    pGetWsscfgDot11AuthenticationAlgorithmsEntry =
        (tWsscfgDot11AuthenticationAlgorithmsEntry *)
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.Dot11AuthenticationAlgorithmsTable,
                   (tRBElem *) pWsscfgDot11AuthenticationAlgorithmsEntry);

    return pGetWsscfgDot11AuthenticationAlgorithmsEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11WEPDefaultKeysTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11WEPDefaultKeysEntry or NULL
****************************************************************************/
tWsscfgDot11WEPDefaultKeysEntry *
WsscfgGetFirstDot11WEPDefaultKeysTable ()
{
    tWsscfgDot11WEPDefaultKeysEntry *pWsscfgDot11WEPDefaultKeysEntry = NULL;

    pWsscfgDot11WEPDefaultKeysEntry =
        (tWsscfgDot11WEPDefaultKeysEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.Dot11WEPDefaultKeysTable);

    return pWsscfgDot11WEPDefaultKeysEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11WEPDefaultKeysTable
 Input       :  pCurrentWsscfgDot11WEPDefaultKeysEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11WEPDefaultKeysEntry or NULL
****************************************************************************/
tWsscfgDot11WEPDefaultKeysEntry *
WsscfgGetNextDot11WEPDefaultKeysTable (tWsscfgDot11WEPDefaultKeysEntry *
                                       pCurrentWsscfgDot11WEPDefaultKeysEntry)
{
    tWsscfgDot11WEPDefaultKeysEntry
        * pNextWsscfgDot11WEPDefaultKeysEntry = NULL;

    pNextWsscfgDot11WEPDefaultKeysEntry =
        (tWsscfgDot11WEPDefaultKeysEntry *)
        RBTreeGetNext
        (gWsscfgGlobals.WsscfgGlbMib.Dot11WEPDefaultKeysTable,
         (tRBElem *) pCurrentWsscfgDot11WEPDefaultKeysEntry, NULL);

    return pNextWsscfgDot11WEPDefaultKeysEntry;
}

/****************************************************************************
 *  Function    :  WsscfgGetNextWlanStatisticsTable
 *  Input       :  pCurrentWsscfgDot11WEPDefaultKeysEntry
 *  Description :  This Routine is used to take
 *                    next index from a table
 *  Returns     :  pNextWsscfgDot11WEPDefaultKeysEntry or NULL
 *****************************************************************************/
INT1
WsscfgGetNextWlanStatisticsTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
#ifdef WLC_WANTED
    tWssPmWlanSSIDStatsProcessRBDB WsscfgWlanSSIDStats;
    tWssPmWlanSSIDStatsProcessRBDB *pNextWssPmWlanSSIDStatsProcessRBDB = NULL;
    UINT4               u4FsDot11WlanProfileId = 0;
    tWssWlanDB          WssWlanDB;
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));

    MEMSET (&WsscfgWlanSSIDStats, 0, sizeof (tWssPmWlanSSIDStatsProcessRBDB));

    WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex = (UINT4) i4IfIndex;
    WssWlanDB.WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &WssWlanDB)
        == OSIX_SUCCESS)
    {
        u4FsDot11WlanProfileId =
            (UINT4) WssWlanDB.WssWlanAttributeDB.u2WlanProfileId;
    }

    /* Assign the index */
    WsscfgWlanSSIDStats.u4FsDot11WlanProfileId = u4FsDot11WlanProfileId;

    pNextWssPmWlanSSIDStatsProcessRBDB =
        (tWssPmWlanSSIDStatsProcessRBDB *)
        RBTreeGetNext
        (gWssPmWlanSSIDStatsProcessDB, (tRBElem *) & WsscfgWlanSSIDStats, NULL);

    if (pNextWssPmWlanSSIDStatsProcessRBDB != NULL)
    {
        WssWlanDB.WssWlanAttributeDB.u2WlanProfileId =
            (UINT2) pNextWssPmWlanSSIDStatsProcessRBDB->u4FsDot11WlanProfileId;
        WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY, &WssWlanDB)
            == OSIX_SUCCESS)
        {
            *pi4NextIfIndex = (INT4) WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4NextIfIndex);
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  WsscfgGetNextWlanSSIDStatsTable
 Input       :  pCurrentWsscfgDot11WEPDefaultKeysEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11WEPDefaultKeysEntry or NULL
****************************************************************************/
INT1
WsscfgGetNextWlanSSIDStatsTable (UINT4 u4FsDot11WlanProfileId,
                                 UINT4 *pu4NextFsDot11WlanProfileId)
{

    tWssPmWlanSSIDStatsProcessRBDB WsscfgWlanSSIDStats;
    tWssPmWlanSSIDStatsProcessRBDB *pNextWssPmWlanSSIDStatsProcessRBDB = NULL;

    MEMSET (&WsscfgWlanSSIDStats, 0, sizeof (tWssPmWlanSSIDStatsProcessRBDB));

    /* Assign the index */
    WsscfgWlanSSIDStats.u4FsDot11WlanProfileId = u4FsDot11WlanProfileId;

    pNextWssPmWlanSSIDStatsProcessRBDB =
        (tWssPmWlanSSIDStatsProcessRBDB *)
        RBTreeGetNext
        (gWssPmWlanSSIDStatsProcessDB, (tRBElem *) & WsscfgWlanSSIDStats, NULL);

    if (pNextWssPmWlanSSIDStatsProcessRBDB != NULL)
    {
        *pu4NextFsDot11WlanProfileId =
            pNextWssPmWlanSSIDStatsProcessRBDB->u4FsDot11WlanProfileId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  WsscfgGetNextWlanClientStatsTable
 Input       :  pCurrentWsscfgDot11WEPDefaultKeysEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11WEPDefaultKeysEntry or NULL
****************************************************************************/
INT1
WsscfgGetNextWlanClientStatsTable (tMacAddr FsWlanClientStatsMACAddress,
                                   tMacAddr * pNextFsWlanClientStatsMACAddress)
{
    tWssPmWlanClientStatsProcessRBDB WsscfgWlanClientStats;
    tWssPmWlanClientStatsProcessRBDB
        * pNextWssPmWlanClientStatsProcessRBDB = NULL;

    MEMSET (&WsscfgWlanClientStats, 0,
            sizeof (tWssPmWlanClientStatsProcessRBDB));

    /* Assign the index */
    MEMCPY (WsscfgWlanClientStats.FsWlanClientStatsMACAddress,
            FsWlanClientStatsMACAddress, sizeof (tMacAddr));

    pNextWssPmWlanClientStatsProcessRBDB =
        (tWssPmWlanClientStatsProcessRBDB *)
        RBTreeGetNext
        (gWssPmWlanClientStatsProcessDB,
         (tRBElem *) & WsscfgWlanClientStats, NULL);

    if (pNextWssPmWlanClientStatsProcessRBDB != NULL)
    {
        MEMCPY (pNextFsWlanClientStatsMACAddress,
                pNextWssPmWlanClientStatsProcessRBDB->
                FsWlanClientStatsMACAddress, sizeof (tMacAddr));

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  WsscfgGetNextWlanRadioStatsTable
 Input       :  pCurrentWsscfgDot11WEPDefaultKeysEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11WEPDefaultKeysEntry or NULL
****************************************************************************/
INT1
WsscfgGetNextWlanRadioStatsTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tWssPmWlanRadioStatsProcessRBDB WsscfgWlanRadioStats;
    tWssPmWlanRadioStatsProcessRBDB
        * pNextWssPmWlanRadioStatsProcessRBDB = NULL;
    MEMSET (&WsscfgWlanRadioStats, 0, sizeof (tWssPmWlanRadioStatsProcessRBDB));

    /*Assign Index */
    WsscfgWlanRadioStats.i4IfIndex = i4IfIndex;

    pNextWssPmWlanRadioStatsProcessRBDB =
        (tWssPmWlanRadioStatsProcessRBDB *)
        RBTreeGetNext
        (gWssPmWlanRadioStatsProcessDB,
         (tRBElem *) & WsscfgWlanRadioStats, NULL);

    if (pNextWssPmWlanRadioStatsProcessRBDB != NULL)
    {
        *pi4NextIfIndex = pNextWssPmWlanRadioStatsProcessRBDB->i4IfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 *  Function    :  WsscfgGetNextApWlanStatisticsTable
 *  Input       :  pCurrentWsscfgFsWlanBSSIDStatsEntry
 *  Description :  This Routine is used to take
 *                    next index from a table
 *  Returns     :  pNextWsscfgFsWlanBSSIDStatsEntry or NULL
 *****************************************************************************/
INT1
WsscfgGetNextApWlanStatisticsTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                    UINT4 u4CapwapDot11WlanProfileId,
                                    UINT4 *pu4NextCapwapDot11WlanProfileId)
{
#ifdef WLC_WANTED
    tWssPmWLANBSSIDStatsProcessRBDB WsscfgWlanBSSIDStats;
    tWssPmWLANBSSIDStatsProcessRBDB *pNextWssPmWLANBSSIDStatsProcessRBDB = NULL;
    tWssWlanDB          WssWlanDB;
    INT4                i4BssIfIndex = 0;

    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&WsscfgWlanBSSIDStats, 0, sizeof (tWssPmWLANBSSIDStatsProcessRBDB));

    if (nmhGetCapwapDot11WlanBindBssIfIndex
        (i4IfIndex, u4CapwapDot11WlanProfileId, &i4BssIfIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    WsscfgWlanBSSIDStats.u4WlanBSSIndex = (UINT4) i4BssIfIndex;

    pNextWssPmWLANBSSIDStatsProcessRBDB =
        (tWssPmWLANBSSIDStatsProcessRBDB *)
        RBTreeGetNext
        (gWssPmWLANBSSIDStatsProcessDB, (tRBElem *) & WsscfgWlanBSSIDStats,
         NULL);

    if (pNextWssPmWLANBSSIDStatsProcessRBDB != NULL)
    {
        WssWlanDB.WssWlanAttributeDB.u4BssIfIndex =
            pNextWssPmWLANBSSIDStatsProcessRBDB->u4WlanBSSIndex;
        WssWlanDB.WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
        WssWlanDB.WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
        WssWlanDB.WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;

        if (WssIfProcessWssWlanDBMsg
            (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, &WssWlanDB) == OSIX_SUCCESS)
        {
            *pi4NextIfIndex =
                (INT4) WssWlanDB.WssWlanAttributeDB.u4RadioIfIndex;
            *pu4NextCapwapDot11WlanProfileId =
                (UINT4) WssWlanDB.WssWlanAttributeDB.u2WlanProfileId;
        }
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4NextIfIndex);
    UNUSED_PARAM (u4CapwapDot11WlanProfileId);
    UNUSED_PARAM (pu4NextCapwapDot11WlanProfileId);
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  WsscfgGetDot11WEPDefaultKeysTable
 Input       :  pWsscfgDot11WEPDefaultKeysEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11WEPDefaultKeysEntry or NULL
****************************************************************************/
tWsscfgDot11WEPDefaultKeysEntry *
WsscfgGetDot11WEPDefaultKeysTable (tWsscfgDot11WEPDefaultKeysEntry *
                                   pWsscfgDot11WEPDefaultKeysEntry)
{
    tWsscfgDot11WEPDefaultKeysEntry *pGetWsscfgDot11WEPDefaultKeysEntry = NULL;

    pGetWsscfgDot11WEPDefaultKeysEntry =
        (tWsscfgDot11WEPDefaultKeysEntry *)
        RBTreeGet
        (gWsscfgGlobals.WsscfgGlbMib.Dot11WEPDefaultKeysTable,
         (tRBElem *) pWsscfgDot11WEPDefaultKeysEntry);

    return pGetWsscfgDot11WEPDefaultKeysEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11WEPKeyMappingsTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11WEPKeyMappingsEntry or NULL
****************************************************************************/
tWsscfgDot11WEPKeyMappingsEntry *
WsscfgGetFirstDot11WEPKeyMappingsTable ()
{
    tWsscfgDot11WEPKeyMappingsEntry *pWsscfgDot11WEPKeyMappingsEntry = NULL;

    pWsscfgDot11WEPKeyMappingsEntry =
        (tWsscfgDot11WEPKeyMappingsEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.Dot11WEPKeyMappingsTable);

    return pWsscfgDot11WEPKeyMappingsEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11WEPKeyMappingsTable
 Input       :  pCurrentWsscfgDot11WEPKeyMappingsEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11WEPKeyMappingsEntry or NULL
****************************************************************************/
tWsscfgDot11WEPKeyMappingsEntry *
WsscfgGetNextDot11WEPKeyMappingsTable (tWsscfgDot11WEPKeyMappingsEntry *
                                       pCurrentWsscfgDot11WEPKeyMappingsEntry)
{
    tWsscfgDot11WEPKeyMappingsEntry
        * pNextWsscfgDot11WEPKeyMappingsEntry = NULL;

    pNextWsscfgDot11WEPKeyMappingsEntry =
        (tWsscfgDot11WEPKeyMappingsEntry *)
        RBTreeGetNext
        (gWsscfgGlobals.WsscfgGlbMib.Dot11WEPKeyMappingsTable,
         (tRBElem *) pCurrentWsscfgDot11WEPKeyMappingsEntry, NULL);

    return pNextWsscfgDot11WEPKeyMappingsEntry;
}

/****************************************************************************
 Function    :  WsscfgGetDot11WEPKeyMappingsTable
 Input       :  pWsscfgDot11WEPKeyMappingsEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11WEPKeyMappingsEntry or NULL
****************************************************************************/
tWsscfgDot11WEPKeyMappingsEntry *
WsscfgGetDot11WEPKeyMappingsTable (tWsscfgDot11WEPKeyMappingsEntry *
                                   pWsscfgDot11WEPKeyMappingsEntry)
{
    tWsscfgDot11WEPKeyMappingsEntry *pGetWsscfgDot11WEPKeyMappingsEntry = NULL;

    pGetWsscfgDot11WEPKeyMappingsEntry =
        (tWsscfgDot11WEPKeyMappingsEntry *)
        RBTreeGet
        (gWsscfgGlobals.WsscfgGlbMib.Dot11WEPKeyMappingsTable,
         (tRBElem *) pWsscfgDot11WEPKeyMappingsEntry);

    return pGetWsscfgDot11WEPKeyMappingsEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11PrivacyTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11PrivacyEntry or NULL
****************************************************************************/
tWsscfgDot11PrivacyEntry *
WsscfgGetFirstDot11PrivacyTable ()
{
    tWsscfgDot11PrivacyEntry *pWsscfgDot11PrivacyEntry = NULL;

    pWsscfgDot11PrivacyEntry =
        (tWsscfgDot11PrivacyEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.Dot11PrivacyTable);

    return pWsscfgDot11PrivacyEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11PrivacyTable
 Input       :  pCurrentWsscfgDot11PrivacyEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11PrivacyEntry or NULL
****************************************************************************/
tWsscfgDot11PrivacyEntry *
WsscfgGetNextDot11PrivacyTable (tWsscfgDot11PrivacyEntry *
                                pCurrentWsscfgDot11PrivacyEntry)
{
    tWsscfgDot11PrivacyEntry *pNextWsscfgDot11PrivacyEntry = NULL;

    pNextWsscfgDot11PrivacyEntry =
        (tWsscfgDot11PrivacyEntry *)
        RBTreeGetNext (gWsscfgGlobals.WsscfgGlbMib.Dot11PrivacyTable,
                       (tRBElem *) pCurrentWsscfgDot11PrivacyEntry, NULL);

    return pNextWsscfgDot11PrivacyEntry;
}

/****************************************************************************
 Function    :  WsscfgGetDot11PrivacyTable
 Input       :  pWsscfgDot11PrivacyEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11PrivacyEntry or NULL
****************************************************************************/
tWsscfgDot11PrivacyEntry *
WsscfgGetDot11PrivacyTable (tWsscfgDot11PrivacyEntry * pWsscfgDot11PrivacyEntry)
{
    tWsscfgDot11PrivacyEntry *pGetWsscfgDot11PrivacyEntry = NULL;

    pGetWsscfgDot11PrivacyEntry =
        (tWsscfgDot11PrivacyEntry *) RBTreeGet (gWsscfgGlobals.
                                                WsscfgGlbMib.Dot11PrivacyTable,
                                                (tRBElem *)
                                                pWsscfgDot11PrivacyEntry);

    return pGetWsscfgDot11PrivacyEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11MultiDomainCapabilityTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11MultiDomainCapabilityEntry or NULL
****************************************************************************/
INT1
WsscfgGetFirstDot11MultiDomainCapabilityTable (INT4 *pi4IfIndex,
                                               UINT4 *pu4Dot11MultiDomainIndex)
{
    tRadioIfDB         *pRadioIfDB = NULL;

    pRadioIfDB =
        (tRadioIfDB *) RBTreeGetFirst (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB);

    if (pRadioIfDB != NULL)
    {
        *pi4IfIndex = (INT4) pRadioIfDB->u4VirtualRadioIfIndex;
        if (pRadioIfDB->RadioIfConfigParam.MultiDomainInfo[0].
            u2FirstChannelNo == 0)
        {
            return OSIX_FAILURE;
        }
        *pu4Dot11MultiDomainIndex = 1;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11MultiDomainCapabilityTable
 Input       :  pCurrentWsscfgDot11MultiDomainCapabilityEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11MultiDomainCapabilityEntry or NULL
****************************************************************************/
INT1
WsscfgGetNextDot11MultiDomainCapabilityTable (INT4 i4IfIndex,
                                              INT4 *pi4NextIfIndex,
                                              UINT4
                                              u4Dot11MultiDomainCapabilityIndex,
                                              UINT4
                                              *pu4NextDot11MultiDomainCapabilityIndex)
{
    tRadioIfDB          RadioIfDB;
    tRadioIfGetDB       RadioIfGetDB;
    tRadioIfDB         *pNextRadioIfDB = NULL;

    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfDB.u4VirtualRadioIfIndex = (UINT4) i4IfIndex;
    pNextRadioIfDB =
        RBTreeGet (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB, &RadioIfDB);
    if (pNextRadioIfDB != NULL)
    {
        if (pNextRadioIfDB->RadioIfConfigParam.
            MultiDomainInfo[u4Dot11MultiDomainCapabilityIndex].
            u2FirstChannelNo != 0)
        {
            *pi4NextIfIndex = i4IfIndex;
            *pu4NextDot11MultiDomainCapabilityIndex =
                u4Dot11MultiDomainCapabilityIndex + 1;
            return SNMP_SUCCESS;
        }
        else
        {
            pNextRadioIfDB =
                (tRadioIfDB *) RBTreeGetNext (gRadioIfGlobals.
                                              RadioIfGlbMib.RadioIfDB,
                                              (tRBElem *) & RadioIfDB, NULL);
            if (pNextRadioIfDB != NULL)
            {
                *pi4NextIfIndex = (INT4) pNextRadioIfDB->u4VirtualRadioIfIndex;
                if (pNextRadioIfDB->RadioIfConfigParam.
                    MultiDomainInfo[0].u2FirstChannelNo == 0)
                {
                    return SNMP_FAILURE;
                }
                *pu4NextDot11MultiDomainCapabilityIndex = 1;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  WsscfgGetDot11MultiDomainCapabilityTable
 Input       :  pWsscfgDot11MultiDomainCapabilityEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11MultiDomainCapabilityEntry or NULL
****************************************************************************/
tWsscfgDot11MultiDomainCapabilityEntry
    *
    WsscfgGetDot11MultiDomainCapabilityTable
    (tWsscfgDot11MultiDomainCapabilityEntry *
     pWsscfgDot11MultiDomainCapabilityEntry)
{
    tWsscfgDot11MultiDomainCapabilityEntry
        * pGetWsscfgDot11MultiDomainCapabilityEntry = NULL;

    pGetWsscfgDot11MultiDomainCapabilityEntry =
        (tWsscfgDot11MultiDomainCapabilityEntry *)
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.Dot11MultiDomainCapabilityTable,
                   (tRBElem *) pWsscfgDot11MultiDomainCapabilityEntry);

    return pGetWsscfgDot11MultiDomainCapabilityEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11SpectrumManagementTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11SpectrumManagementEntry or NULL
****************************************************************************/
INT1
WsscfgGetFirstDot11SpectrumManagementTable (INT4 *pi4IfIndex)
{

    tWssWlanIfIndexDB  *pWssWlanIfIndexDB = NULL;

    /* trying logic to look through both database and return in ascending order */
    pWssWlanIfIndexDB =
        (tWssWlanIfIndexDB *) RBTreeGetFirst (gWssWlanGlobals.
                                              WssWlanGlbMib.WssWlanIfIndexDB);
    if ((pWssWlanIfIndexDB != NULL))
    {
        *pi4IfIndex = pWssWlanIfIndexDB->u4WlanIfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11SpectrumManagementTable
 Input       :  pCurrentWsscfgDot11SpectrumManagementEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11SpectrumManagementEntry or NULL
****************************************************************************/
INT1
WsscfgGetNextDot11SpectrumManagementTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{

    tWssWlanIfIndexDB   WssWlanIfIndexDB;
    tWssWlanIfIndexDB  *pNextWssWlanIfIndexDB = NULL;

    MEMSET (&WssWlanIfIndexDB, 0, sizeof (tWssWlanIfIndexDB));
    WssWlanIfIndexDB.u4WlanIfIndex = i4IfIndex;

    pNextWssWlanIfIndexDB =
        (tWssWlanIfIndexDB *) RBTreeGetNext (gWssWlanGlobals.
                                             WssWlanGlbMib.WssWlanIfIndexDB,
                                             (tRBElem *) &
                                             WssWlanIfIndexDB, NULL);
    if (pNextWssWlanIfIndexDB != NULL)
    {
        *pi4NextIfIndex = pNextWssWlanIfIndexDB->u4WlanIfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  WsscfgGetDot11SpectrumManagementTable
 Input       :  pWsscfgDot11SpectrumManagementEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11SpectrumManagementEntry or NULL
****************************************************************************/
tWsscfgDot11SpectrumManagementEntry
    *
WsscfgGetDot11SpectrumManagementTable (tWsscfgDot11SpectrumManagementEntry *
                                       pWsscfgDot11SpectrumManagementEntry)
{
    tWsscfgDot11SpectrumManagementEntry
        * pGetWsscfgDot11SpectrumManagementEntry = NULL;

    pGetWsscfgDot11SpectrumManagementEntry =
        (tWsscfgDot11SpectrumManagementEntry *)
        RBTreeGet
        (gWsscfgGlobals.WsscfgGlbMib.Dot11SpectrumManagementTable,
         (tRBElem *) pWsscfgDot11SpectrumManagementEntry);

    return pGetWsscfgDot11SpectrumManagementEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11RegulatoryClassesTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11RegulatoryClassesEntry or NULL
****************************************************************************/
tWsscfgDot11RegulatoryClassesEntry
    * WsscfgGetFirstDot11RegulatoryClassesTable ()
{
    tWsscfgDot11RegulatoryClassesEntry
        * pWsscfgDot11RegulatoryClassesEntry = NULL;

    pWsscfgDot11RegulatoryClassesEntry =
        (tWsscfgDot11RegulatoryClassesEntry *)
        RBTreeGetFirst (gWsscfgGlobals.
                        WsscfgGlbMib.Dot11RegulatoryClassesTable);

    return pWsscfgDot11RegulatoryClassesEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11RegulatoryClassesTable
 Input       :  pCurrentWsscfgDot11RegulatoryClassesEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11RegulatoryClassesEntry or NULL
****************************************************************************/
tWsscfgDot11RegulatoryClassesEntry
    *
    WsscfgGetNextDot11RegulatoryClassesTable
    (tWsscfgDot11RegulatoryClassesEntry *
     pCurrentWsscfgDot11RegulatoryClassesEntry)
{
    tWsscfgDot11RegulatoryClassesEntry
        * pNextWsscfgDot11RegulatoryClassesEntry = NULL;

    pNextWsscfgDot11RegulatoryClassesEntry =
        (tWsscfgDot11RegulatoryClassesEntry *)
        RBTreeGetNext (gWsscfgGlobals.
                       WsscfgGlbMib.Dot11RegulatoryClassesTable,
                       (tRBElem *)
                       pCurrentWsscfgDot11RegulatoryClassesEntry, NULL);

    return pNextWsscfgDot11RegulatoryClassesEntry;
}

/****************************************************************************
 Function    :  WsscfgGetDot11RegulatoryClassesTable
 Input       :  pWsscfgDot11RegulatoryClassesEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11RegulatoryClassesEntry or NULL
****************************************************************************/
tWsscfgDot11RegulatoryClassesEntry
    * WsscfgGetDot11RegulatoryClassesTable (tWsscfgDot11RegulatoryClassesEntry *
                                            pWsscfgDot11RegulatoryClassesEntry)
{
    tWsscfgDot11RegulatoryClassesEntry
        * pGetWsscfgDot11RegulatoryClassesEntry = NULL;

    pGetWsscfgDot11RegulatoryClassesEntry =
        (tWsscfgDot11RegulatoryClassesEntry *)
        RBTreeGet
        (gWsscfgGlobals.WsscfgGlbMib.Dot11RegulatoryClassesTable,
         (tRBElem *) pWsscfgDot11RegulatoryClassesEntry);

    return pGetWsscfgDot11RegulatoryClassesEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11OperationTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11OperationEntry or NULL
****************************************************************************/
INT1
WsscfgGetFirstDot11OperationTable (INT4 *pi4IfIndex)
{
    tRadioIfDB         *pRadioIfDB = NULL;

    pRadioIfDB =
        (tRadioIfDB *) RBTreeGetFirst (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB);
    if (NULL == pRadioIfDB)
    {
        /* no entry present, just return error */
        return SNMP_FAILURE;
    }
    *pi4IfIndex = pRadioIfDB->u4VirtualRadioIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11OperationTable
 Input       :  pCurrentWsscfgDot11OperationEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11OperationEntry or NULL
****************************************************************************/
INT1
WsscfgGetNextDot11OperationTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tRadioIfDB          RadioIfDB;
    tRadioIfDB         *pNextRadioIfDB = NULL;

    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));
    /* assign index */
    RadioIfDB.u4VirtualRadioIfIndex = i4IfIndex;

    pNextRadioIfDB =
        (tRadioIfDB *) RBTreeGetNext (gRadioIfGlobals.
                                      RadioIfGlbMib.RadioIfDB,
                                      (tRBElem *) & RadioIfDB, NULL);
    if (NULL == pNextRadioIfDB)
    {
        /* no entry present, just return error */
        return SNMP_FAILURE;
    }
    *pi4NextIfIndex = pNextRadioIfDB->u4VirtualRadioIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetDot11OperationTable
 Input       :  pWsscfgDot11OperationEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11OperationEntry or NULL
****************************************************************************/
tWsscfgDot11OperationEntry *
WsscfgGetDot11OperationTable (tWsscfgDot11OperationEntry *
                              pWsscfgDot11OperationEntry)
{
    tWsscfgDot11OperationEntry *pGetWsscfgDot11OperationEntry = NULL;

    pGetWsscfgDot11OperationEntry =
        (tWsscfgDot11OperationEntry *)
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11OperationTable,
                   (tRBElem *) pWsscfgDot11OperationEntry);

    return pGetWsscfgDot11OperationEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11GroupAddressesTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11GroupAddressesEntry or NULL
****************************************************************************/
tWsscfgDot11GroupAddressesEntry *
WsscfgGetFirstDot11GroupAddressesTable ()
{
    tWsscfgDot11GroupAddressesEntry *pWsscfgDot11GroupAddressesEntry = NULL;

    pWsscfgDot11GroupAddressesEntry =
        (tWsscfgDot11GroupAddressesEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.Dot11GroupAddressesTable);

    return pWsscfgDot11GroupAddressesEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11GroupAddressesTable
 Input       :  pCurrentWsscfgDot11GroupAddressesEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11GroupAddressesEntry or NULL
****************************************************************************/
tWsscfgDot11GroupAddressesEntry *
WsscfgGetNextDot11GroupAddressesTable (tWsscfgDot11GroupAddressesEntry *
                                       pCurrentWsscfgDot11GroupAddressesEntry)
{
    tWsscfgDot11GroupAddressesEntry
        * pNextWsscfgDot11GroupAddressesEntry = NULL;

    pNextWsscfgDot11GroupAddressesEntry =
        (tWsscfgDot11GroupAddressesEntry *)
        RBTreeGetNext
        (gWsscfgGlobals.WsscfgGlbMib.Dot11GroupAddressesTable,
         (tRBElem *) pCurrentWsscfgDot11GroupAddressesEntry, NULL);

    return pNextWsscfgDot11GroupAddressesEntry;
}

/****************************************************************************
 Function    :  WsscfgGetDot11GroupAddressesTable
 Input       :  pWsscfgDot11GroupAddressesEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11GroupAddressesEntry or NULL
****************************************************************************/
tWsscfgDot11GroupAddressesEntry *
WsscfgGetDot11GroupAddressesTable (tWsscfgDot11GroupAddressesEntry *
                                   pWsscfgDot11GroupAddressesEntry)
{
    tWsscfgDot11GroupAddressesEntry *pGetWsscfgDot11GroupAddressesEntry = NULL;

    pGetWsscfgDot11GroupAddressesEntry =
        (tWsscfgDot11GroupAddressesEntry *)
        RBTreeGet
        (gWsscfgGlobals.WsscfgGlbMib.Dot11GroupAddressesTable,
         (tRBElem *) pWsscfgDot11GroupAddressesEntry);

    return pGetWsscfgDot11GroupAddressesEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11EDCATable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11EDCAEntry or NULL
****************************************************************************/
INT4
WsscfgGetFirstDot11EDCATable (INT4 *pi4IfIndex, INT4 *pi4Dot11EDCATableIndex)
{
    tRadioIfDB         *pRadioIfDB = NULL;

    /* look through radio-if database */
    pRadioIfDB =
        (tRadioIfDB *) RBTreeGetFirst (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB);
    if (pRadioIfDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4IfIndex = pRadioIfDB->u4VirtualRadioIfIndex;
    *pi4Dot11EDCATableIndex = RADIOIF_EDCA_BE + 1;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11EDCATable
 Input       :  pCurrentWsscfgDot11EDCAEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11EDCAEntry or NULL
****************************************************************************/
INT4
WsscfgGetNextDot11EDCATable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                             INT4 i4Dot11EDCATableIndex,
                             INT4 *pi4NextDot11EDCATableIndex)
{
    tRadioIfDB          RadioIfDB;
    tRadioIfDB         *pNextRadioIfDB = NULL;

    switch (i4Dot11EDCATableIndex)
    {
        case (RADIOIF_EDCA_BE + 1):
            *pi4NextIfIndex = i4IfIndex;
            *pi4NextDot11EDCATableIndex = RADIOIF_EDCA_VI + 1;
            break;
        case (RADIOIF_EDCA_VI + 1):
            *pi4NextIfIndex = i4IfIndex;
            *pi4NextDot11EDCATableIndex = RADIOIF_EDCA_VO + 1;
            break;
        case (RADIOIF_EDCA_VO + 1):
            *pi4NextIfIndex = i4IfIndex;
            *pi4NextDot11EDCATableIndex = RADIOIF_EDCA_BG + 1;
            break;
        case (RADIOIF_EDCA_BG + 1):
            /* get the next radio-if */
            MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));
            /* assign index */
            RadioIfDB.u4VirtualRadioIfIndex = i4IfIndex;
            /* look through radio-if database */
            pNextRadioIfDB =
                (tRadioIfDB *) RBTreeGetNext (gRadioIfGlobals.
                                              RadioIfGlbMib.RadioIfDB,
                                              (tRBElem *) & RadioIfDB, NULL);
            if (pNextRadioIfDB == NULL)
            {
                return SNMP_FAILURE;
            }

            *pi4NextIfIndex = pNextRadioIfDB->u4VirtualRadioIfIndex;
            *pi4NextDot11EDCATableIndex = RADIOIF_EDCA_BE + 1;
            break;
        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetDot11EDCATable
 Input       :  pWsscfgDot11EDCAEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11EDCAEntry or NULL
****************************************************************************/
tWsscfgDot11EDCAEntry *
WsscfgGetDot11EDCATable (tWsscfgDot11EDCAEntry * pWsscfgDot11EDCAEntry)
{
    tWsscfgDot11EDCAEntry *pGetWsscfgDot11EDCAEntry = NULL;

    pGetWsscfgDot11EDCAEntry =
        (tWsscfgDot11EDCAEntry *) RBTreeGet (gWsscfgGlobals.
                                             WsscfgGlbMib.Dot11EDCATable,
                                             (tRBElem *) pWsscfgDot11EDCAEntry);

    return pGetWsscfgDot11EDCAEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11QAPEDCATable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11QAPEDCAEntry or NULL
****************************************************************************/
tWsscfgDot11QAPEDCAEntry *
WsscfgGetFirstDot11QAPEDCATable ()
{
    tWsscfgDot11QAPEDCAEntry *pWsscfgDot11QAPEDCAEntry = NULL;

    pWsscfgDot11QAPEDCAEntry =
        (tWsscfgDot11QAPEDCAEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.Dot11QAPEDCATable);

    return pWsscfgDot11QAPEDCAEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11QAPEDCATable
 Input       :  pCurrentWsscfgDot11QAPEDCAEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11QAPEDCAEntry or NULL
****************************************************************************/
tWsscfgDot11QAPEDCAEntry *
WsscfgGetNextDot11QAPEDCATable (tWsscfgDot11QAPEDCAEntry *
                                pCurrentWsscfgDot11QAPEDCAEntry)
{
    tWsscfgDot11QAPEDCAEntry *pNextWsscfgDot11QAPEDCAEntry = NULL;

    pNextWsscfgDot11QAPEDCAEntry =
        (tWsscfgDot11QAPEDCAEntry *)
        RBTreeGetNext (gWsscfgGlobals.WsscfgGlbMib.Dot11QAPEDCATable,
                       (tRBElem *) pCurrentWsscfgDot11QAPEDCAEntry, NULL);

    return pNextWsscfgDot11QAPEDCAEntry;
}

/****************************************************************************
 Function    :  WsscfgGetDot11QAPEDCATable
 Input       :  pWsscfgDot11QAPEDCAEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11QAPEDCAEntry or NULL
****************************************************************************/
tWsscfgDot11QAPEDCAEntry *
WsscfgGetDot11QAPEDCATable (tWsscfgDot11QAPEDCAEntry * pWsscfgDot11QAPEDCAEntry)
{
    tWsscfgDot11QAPEDCAEntry *pGetWsscfgDot11QAPEDCAEntry = NULL;

    pGetWsscfgDot11QAPEDCAEntry =
        (tWsscfgDot11QAPEDCAEntry *) RBTreeGet (gWsscfgGlobals.
                                                WsscfgGlbMib.Dot11QAPEDCATable,
                                                (tRBElem *)
                                                pWsscfgDot11QAPEDCAEntry);

    return pGetWsscfgDot11QAPEDCAEntry;
}

/****************************************************************************
 Function    :  WsscfgGetDot11ResourceTypeIDName
 Input       :  The Indices
 Input       :  pDot11ResourceTypeIDName
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetDot11ResourceTypeIDName (UINT1 *pDot11ResourceTypeIDName)
{
    MEMCPY (pDot11ResourceTypeIDName,
            gWsscfgGlobals.WsscfgGlbMib.au1Dot11ResourceTypeIDName,
            gWsscfgGlobals.WsscfgGlbMib.i4Dot11ResourceTypeIDNameLen);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11PhyOperationTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11PhyOperationEntry or NULL
****************************************************************************/
tWsscfgDot11PhyOperationEntry *
WsscfgGetFirstDot11PhyOperationTable ()
{
    tWsscfgDot11PhyOperationEntry *pWsscfgDot11PhyOperationEntry = NULL;

    pWsscfgDot11PhyOperationEntry =
        (tWsscfgDot11PhyOperationEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyOperationTable);

    return pWsscfgDot11PhyOperationEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11PhyOperationTable
 Input       :  pCurrentWsscfgDot11PhyOperationEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11PhyOperationEntry or NULL
****************************************************************************/
tWsscfgDot11PhyOperationEntry *
WsscfgGetNextDot11PhyOperationTable (tWsscfgDot11PhyOperationEntry *
                                     pCurrentWsscfgDot11PhyOperationEntry)
{
    tWsscfgDot11PhyOperationEntry *pNextWsscfgDot11PhyOperationEntry = NULL;

    pNextWsscfgDot11PhyOperationEntry =
        (tWsscfgDot11PhyOperationEntry *)
        RBTreeGetNext
        (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyOperationTable,
         (tRBElem *) pCurrentWsscfgDot11PhyOperationEntry, NULL);

    return pNextWsscfgDot11PhyOperationEntry;
}

/****************************************************************************
 Function    :  WsscfgGetDot11PhyOperationTable
 Input       :  pWsscfgDot11PhyOperationEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11PhyOperationEntry or NULL
****************************************************************************/
tWsscfgDot11PhyOperationEntry *
WsscfgGetDot11PhyOperationTable (tWsscfgDot11PhyOperationEntry *
                                 pWsscfgDot11PhyOperationEntry)
{
    tWsscfgDot11PhyOperationEntry *pGetWsscfgDot11PhyOperationEntry = NULL;

    pGetWsscfgDot11PhyOperationEntry =
        (tWsscfgDot11PhyOperationEntry *)
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyOperationTable,
                   (tRBElem *) pWsscfgDot11PhyOperationEntry);

    return pGetWsscfgDot11PhyOperationEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11PhyAntennaTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11PhyAntennaEntry or NULL
****************************************************************************/
INT4
WsscfgGetFirstDot11PhyAntennaTable (INT4 *pi4IfIndex)
{
    tRadioIfDB         *pRadioIfDB = NULL;

    /* look through radio-if database */
    pRadioIfDB =
        (tRadioIfDB *) RBTreeGetFirst (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB);
    if (pRadioIfDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4IfIndex = pRadioIfDB->u4VirtualRadioIfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11PhyAntennaTable
 Input       :  pCurrentWsscfgDot11PhyAntennaEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11PhyAntennaEntry or NULL
****************************************************************************/
INT4
WsscfgGetNextDot11PhyAntennaTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tRadioIfDB          RadioIfDB;
    tRadioIfDB         *pNextRadioIfDB = NULL;

    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));

    /* assign index */
    RadioIfDB.u4VirtualRadioIfIndex = i4IfIndex;

    /* look through radio-if database */
    pNextRadioIfDB =
        (tRadioIfDB *) RBTreeGetNext (gRadioIfGlobals.
                                      RadioIfGlbMib.RadioIfDB,
                                      (tRBElem *) & RadioIfDB, NULL);

    if (pNextRadioIfDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextIfIndex = pNextRadioIfDB->u4VirtualRadioIfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetDot11PhyAntennaTable
 Input       :  pWsscfgDot11PhyAntennaEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11PhyAntennaEntry or NULL
****************************************************************************/
tWsscfgDot11PhyAntennaEntry *
WsscfgGetDot11PhyAntennaTable (tWsscfgDot11PhyAntennaEntry *
                               pWsscfgDot11PhyAntennaEntry)
{
    tWsscfgDot11PhyAntennaEntry *pGetWsscfgDot11PhyAntennaEntry = NULL;

    pGetWsscfgDot11PhyAntennaEntry =
        (tWsscfgDot11PhyAntennaEntry *)
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyAntennaTable,
                   (tRBElem *) pWsscfgDot11PhyAntennaEntry);

    return pGetWsscfgDot11PhyAntennaEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11PhyTxPowerTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11PhyTxPowerEntry or NULL
****************************************************************************/
INT1
WsscfgGetFirstDot11PhyTxPowerTable (INT4 *pi4IfIndex)
{
    tRadioIfDB         *pRadioIfDB = NULL;

    pRadioIfDB =
        (tRadioIfDB *) RBTreeGetFirst (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB);
    if (NULL == pRadioIfDB)
    {
        /* no entry present, just return error */
        return SNMP_FAILURE;
    }
    *pi4IfIndex = pRadioIfDB->u4VirtualRadioIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11PhyTxPowerTable
 Input       :  pCurrentWsscfgDot11PhyTxPowerEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11PhyTxPowerEntry or NULL
****************************************************************************/
INT1
WsscfgGetNextDot11PhyTxPowerTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tRadioIfDB          RadioIfDB;
    tRadioIfDB         *pNextRadioIfDB = NULL;

    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));
    /* assign index */
    RadioIfDB.u4VirtualRadioIfIndex = i4IfIndex;

    pNextRadioIfDB =
        (tRadioIfDB *) RBTreeGetNext (gRadioIfGlobals.
                                      RadioIfGlbMib.RadioIfDB,
                                      (tRBElem *) & RadioIfDB, NULL);
    if (NULL == pNextRadioIfDB)
    {
        /* no entry present, just return error */
        return SNMP_FAILURE;
    }
    *pi4NextIfIndex = pNextRadioIfDB->u4VirtualRadioIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetDot11PhyTxPowerTable
 Input       :  pWsscfgDot11PhyTxPowerEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11PhyTxPowerEntry or NULL
****************************************************************************/
tWsscfgDot11PhyTxPowerEntry *
WsscfgGetDot11PhyTxPowerTable (tWsscfgDot11PhyTxPowerEntry *
                               pWsscfgDot11PhyTxPowerEntry)
{
    tWsscfgDot11PhyTxPowerEntry *pGetWsscfgDot11PhyTxPowerEntry = NULL;

    pGetWsscfgDot11PhyTxPowerEntry =
        (tWsscfgDot11PhyTxPowerEntry *)
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyTxPowerTable,
                   (tRBElem *) pWsscfgDot11PhyTxPowerEntry);

    return pGetWsscfgDot11PhyTxPowerEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11PhyFHSSTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11PhyFHSSEntry or NULL
****************************************************************************/
tWsscfgDot11PhyFHSSEntry *
WsscfgGetFirstDot11PhyFHSSTable ()
{
    tWsscfgDot11PhyFHSSEntry *pWsscfgDot11PhyFHSSEntry = NULL;

    pWsscfgDot11PhyFHSSEntry =
        (tWsscfgDot11PhyFHSSEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyFHSSTable);

    return pWsscfgDot11PhyFHSSEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11PhyFHSSTable
 Input       :  pCurrentWsscfgDot11PhyFHSSEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11PhyFHSSEntry or NULL
****************************************************************************/
tWsscfgDot11PhyFHSSEntry *
WsscfgGetNextDot11PhyFHSSTable (tWsscfgDot11PhyFHSSEntry *
                                pCurrentWsscfgDot11PhyFHSSEntry)
{
    tWsscfgDot11PhyFHSSEntry *pNextWsscfgDot11PhyFHSSEntry = NULL;

    pNextWsscfgDot11PhyFHSSEntry =
        (tWsscfgDot11PhyFHSSEntry *)
        RBTreeGetNext (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyFHSSTable,
                       (tRBElem *) pCurrentWsscfgDot11PhyFHSSEntry, NULL);

    return pNextWsscfgDot11PhyFHSSEntry;
}

/****************************************************************************
 Function    :  WsscfgGetDot11PhyFHSSTable
 Input       :  pWsscfgDot11PhyFHSSEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11PhyFHSSEntry or NULL
****************************************************************************/
tWsscfgDot11PhyFHSSEntry *
WsscfgGetDot11PhyFHSSTable (tWsscfgDot11PhyFHSSEntry * pWsscfgDot11PhyFHSSEntry)
{
    tWsscfgDot11PhyFHSSEntry *pGetWsscfgDot11PhyFHSSEntry = NULL;

    pGetWsscfgDot11PhyFHSSEntry =
        (tWsscfgDot11PhyFHSSEntry *) RBTreeGet (gWsscfgGlobals.
                                                WsscfgGlbMib.Dot11PhyFHSSTable,
                                                (tRBElem *)
                                                pWsscfgDot11PhyFHSSEntry);

    return pGetWsscfgDot11PhyFHSSEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11PhyDSSSTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11PhyDSSSEntry or NULL
****************************************************************************/
INT4
WsscfgGetFirstDot11PhyDSSSTable (INT4 *pi4IfIndex)
{
    tRadioIfDB         *pRadioIfDB = NULL;

    /* look through radio-if database */
    pRadioIfDB =
        (tRadioIfDB *) RBTreeGetFirst (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB);
    if (pRadioIfDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4IfIndex = pRadioIfDB->u4VirtualRadioIfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11PhyDSSSTable
 Input       :  pCurrentWsscfgDot11PhyDSSSEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11PhyDSSSEntry or NULL
****************************************************************************/
INT4
WsscfgGetNextDot11PhyDSSSTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tRadioIfDB          RadioIfDB;
    tRadioIfDB         *pNextRadioIfDB = NULL;

    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));

    /* assign index */
    RadioIfDB.u4VirtualRadioIfIndex = i4IfIndex;

    /* look through radio-if database */
    pNextRadioIfDB =
        (tRadioIfDB *) RBTreeGetNext (gRadioIfGlobals.
                                      RadioIfGlbMib.RadioIfDB,
                                      (tRBElem *) & RadioIfDB, NULL);

    if (pNextRadioIfDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextIfIndex = pNextRadioIfDB->u4VirtualRadioIfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetDot11PhyDSSSTable
 Input       :  pWsscfgDot11PhyDSSSEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11PhyDSSSEntry or NULL
****************************************************************************/
tWsscfgDot11PhyDSSSEntry *
WsscfgGetDot11PhyDSSSTable (tWsscfgDot11PhyDSSSEntry * pWsscfgDot11PhyDSSSEntry)
{
    tWsscfgDot11PhyDSSSEntry *pGetWsscfgDot11PhyDSSSEntry = NULL;

    pGetWsscfgDot11PhyDSSSEntry =
        (tWsscfgDot11PhyDSSSEntry *) RBTreeGet (gWsscfgGlobals.
                                                WsscfgGlbMib.Dot11PhyDSSSTable,
                                                (tRBElem *)
                                                pWsscfgDot11PhyDSSSEntry);

    return pGetWsscfgDot11PhyDSSSEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11PhyIRTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11PhyIREntry or NULL
****************************************************************************/
tWsscfgDot11PhyIREntry *
WsscfgGetFirstDot11PhyIRTable ()
{
    tWsscfgDot11PhyIREntry *pWsscfgDot11PhyIREntry = NULL;

    pWsscfgDot11PhyIREntry =
        (tWsscfgDot11PhyIREntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyIRTable);

    return pWsscfgDot11PhyIREntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11PhyIRTable
 Input       :  pCurrentWsscfgDot11PhyIREntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11PhyIREntry or NULL
****************************************************************************/
tWsscfgDot11PhyIREntry *
WsscfgGetNextDot11PhyIRTable (tWsscfgDot11PhyIREntry *
                              pCurrentWsscfgDot11PhyIREntry)
{
    tWsscfgDot11PhyIREntry *pNextWsscfgDot11PhyIREntry = NULL;

    pNextWsscfgDot11PhyIREntry =
        (tWsscfgDot11PhyIREntry *)
        RBTreeGetNext (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyIRTable,
                       (tRBElem *) pCurrentWsscfgDot11PhyIREntry, NULL);

    return pNextWsscfgDot11PhyIREntry;
}

/****************************************************************************
 Function    :  WsscfgGetDot11PhyIRTable
 Input       :  pWsscfgDot11PhyIREntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11PhyIREntry or NULL
****************************************************************************/
tWsscfgDot11PhyIREntry *
WsscfgGetDot11PhyIRTable (tWsscfgDot11PhyIREntry * pWsscfgDot11PhyIREntry)
{
    tWsscfgDot11PhyIREntry *pGetWsscfgDot11PhyIREntry = NULL;

    pGetWsscfgDot11PhyIREntry =
        (tWsscfgDot11PhyIREntry *) RBTreeGet (gWsscfgGlobals.
                                              WsscfgGlbMib.Dot11PhyIRTable,
                                              (tRBElem *)
                                              pWsscfgDot11PhyIREntry);

    return pGetWsscfgDot11PhyIREntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11AntennasListTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11AntennasListEntry or NULL
****************************************************************************/
INT4
WsscfgGetFirstDot11AntennasListTable (INT4 *pi4IfIndex,
                                      INT4 *pi4Dot11AntennaListIndex)
{
    tRadioIfDB         *pRadioIfDB = NULL;

    /* look through radio-if database */
    pRadioIfDB =
        (tRadioIfDB *) RBTreeGetFirst (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB);
    if (pRadioIfDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4IfIndex = pRadioIfDB->u4VirtualRadioIfIndex;
    *pi4Dot11AntennaListIndex = 1;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11AntennasListTable
 Input       :  pCurrentWsscfgDot11AntennasListEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11AntennasListEntry or NULL
****************************************************************************/
INT4
WsscfgGetNextDot11AntennasListTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                     INT4 i4Dot11AntennaListIndex,
                                     INT4 *pi4NextDot11AntennaListIndex)
{
    tRadioIfDB          RadioIfDB;
    tRadioIfDB         *pNextRadioIfDB = NULL;
    /* get the next radio-if */
    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));
    /* assign index */
    RadioIfDB.u4VirtualRadioIfIndex = i4IfIndex;
    /* look through radio-if database */

    pNextRadioIfDB =
        RBTreeGet (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB, &RadioIfDB);
    if (pNextRadioIfDB != NULL)
    {
        if (i4Dot11AntennaListIndex ==
            pNextRadioIfDB->RadioIfConfigParam.u1CurrentTxAntenna)
        {
            pNextRadioIfDB =
                (tRadioIfDB *) RBTreeGetNext (gRadioIfGlobals.
                                              RadioIfGlbMib.RadioIfDB,
                                              (tRBElem *) & RadioIfDB, NULL);
            if (pNextRadioIfDB == NULL)
            {
                return SNMP_FAILURE;
            }
        }
        else
        {
            /* Since this radio contains multiple antenna, and this
             * antenna is not a key in RBTree, directly increment and
             *return the antenna index value is done here */
            *pi4NextDot11AntennaListIndex = i4Dot11AntennaListIndex + 1;
            *pi4NextIfIndex = i4IfIndex;
            return SNMP_SUCCESS;
        }
        *pi4NextIfIndex = pNextRadioIfDB->u4VirtualRadioIfIndex;
        *pi4NextDot11AntennaListIndex = 1;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  WsscfgGetDot11AntennasListTable
 Input       :  pWsscfgDot11AntennasListEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11AntennasListEntry or NULL
****************************************************************************/
tWsscfgDot11AntennasListEntry *
WsscfgGetDot11AntennasListTable (tWsscfgDot11AntennasListEntry *
                                 pWsscfgDot11AntennasListEntry)
{
    tWsscfgDot11AntennasListEntry *pGetWsscfgDot11AntennasListEntry = NULL;

    pGetWsscfgDot11AntennasListEntry =
        (tWsscfgDot11AntennasListEntry *)
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11AntennasListTable,
                   (tRBElem *) pWsscfgDot11AntennasListEntry);

    return pGetWsscfgDot11AntennasListEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11PhyOFDMTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11PhyOFDMEntry or NULL
****************************************************************************/
INT4
WsscfgGetFirstDot11PhyOFDMTable (INT4 *pi4IfIndex)
{
    tRadioIfDB         *pRadioIfDB = NULL;

    /* look through radio-if database */
    pRadioIfDB =
        (tRadioIfDB *) RBTreeGetFirst (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB);
    if (pRadioIfDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4IfIndex = pRadioIfDB->u4VirtualRadioIfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11PhyOFDMTable
 Input       :  pCurrentWsscfgDot11PhyOFDMEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11PhyOFDMEntry or NULL
****************************************************************************/
INT4
WsscfgGetNextDot11PhyOFDMTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tRadioIfDB          RadioIfDB;
    tRadioIfDB         *pNextRadioIfDB = NULL;

    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));

    /* assign index */
    RadioIfDB.u4VirtualRadioIfIndex = i4IfIndex;

    /* look through radio-if database */
    pNextRadioIfDB =
        (tRadioIfDB *) RBTreeGetNext (gRadioIfGlobals.
                                      RadioIfGlbMib.RadioIfDB,
                                      (tRBElem *) & RadioIfDB, NULL);

    if (pNextRadioIfDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextIfIndex = pNextRadioIfDB->u4VirtualRadioIfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetDot11PhyOFDMTable
 Input       :  pWsscfgDot11PhyOFDMEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11PhyOFDMEntry or NULL
****************************************************************************/
tWsscfgDot11PhyOFDMEntry *
WsscfgGetDot11PhyOFDMTable (tWsscfgDot11PhyOFDMEntry * pWsscfgDot11PhyOFDMEntry)
{
    tWsscfgDot11PhyOFDMEntry *pGetWsscfgDot11PhyOFDMEntry = NULL;

    pGetWsscfgDot11PhyOFDMEntry =
        (tWsscfgDot11PhyOFDMEntry *) RBTreeGet (gWsscfgGlobals.
                                                WsscfgGlbMib.Dot11PhyOFDMTable,
                                                (tRBElem *)
                                                pWsscfgDot11PhyOFDMEntry);

    return pGetWsscfgDot11PhyOFDMEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11HoppingPatternTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11HoppingPatternEntry or NULL
****************************************************************************/
tWsscfgDot11HoppingPatternEntry *
WsscfgGetFirstDot11HoppingPatternTable ()
{
    tWsscfgDot11HoppingPatternEntry *pWsscfgDot11HoppingPatternEntry = NULL;

    pWsscfgDot11HoppingPatternEntry =
        (tWsscfgDot11HoppingPatternEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.Dot11HoppingPatternTable);

    return pWsscfgDot11HoppingPatternEntry;
}

/****************************************************************************
 Functionn    :  WsscfgGetFirstFsWlanClientStatsTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsWlanClientStatsEntry or NULL
****************************************************************************/
INT1
WsscfgGetFirstClientStatsTable (tMacAddr * pFsWlanClientStatsMACAddress)
{
    tWssPmWlanClientStatsProcessRBDB *pWssPmWlanClientStatsProcessRBDB = NULL;

    pWssPmWlanClientStatsProcessRBDB =
        (tWssPmWlanClientStatsProcessRBDB *)
        RBTreeGetFirst (gWssPmWlanClientStatsProcessDB);
    if (pWssPmWlanClientStatsProcessRBDB != NULL)
    {
        MEMCPY (*pFsWlanClientStatsMACAddress,
                pWssPmWlanClientStatsProcessRBDB->FsWlanClientStatsMACAddress,
                sizeof (tMacAddr));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Functionn    :  WsscfgGetFirstFsWlanClientStatsTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsWlanClientStatsEntry or NULL
****************************************************************************/
INT1
WsscfgGetFirstRadioStatsTable (INT4 *pi4IfIndex)
{
    tWssPmWlanRadioStatsProcessRBDB *pWssPmWlanRadioStatsProcessRBDB = NULL;

    pWssPmWlanRadioStatsProcessRBDB =
        (tWssPmWlanRadioStatsProcessRBDB *)
        RBTreeGetFirst (gWssPmWlanRadioStatsProcessDB);
    if (pWssPmWlanRadioStatsProcessRBDB != NULL)
    {
        *pi4IfIndex = pWssPmWlanRadioStatsProcessRBDB->i4IfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 *  Functionn   : WsscfgGetFirstFsWlanStatisticsTable
 *  Input       :  NONE
 *  Description :  This Routine is used to take
 *                 first index from a table
 *  Returns     :  pWsscfgFsWlanSSIDStatsEntry or NULL
 *****************************************************************************/
INT1
WsscfgGetFirstWlanStatisticsTable (INT4 *pi4IfIndex)
{
#ifdef WLC_WANTED
    tWssPmWlanSSIDStatsProcessRBDB *pWssPmWlanSSIDStatsProcessRBDB = NULL;
    tWssWlanDB          WssWlanDB;
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    pWssPmWlanSSIDStatsProcessRBDB =
        (tWssPmWlanSSIDStatsProcessRBDB *)
        RBTreeGetFirst (gWssPmWlanSSIDStatsProcessDB);

    if (pWssPmWlanSSIDStatsProcessRBDB != NULL)
    {
        WssWlanDB.WssWlanAttributeDB.u2WlanProfileId =
            (UINT2) pWssPmWlanSSIDStatsProcessRBDB->u4FsDot11WlanProfileId;
        WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY, &WssWlanDB)
            == OSIX_SUCCESS)
        {
            *pi4IfIndex = (INT4) WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
#else
    UNUSED_PARAM (pi4IfIndex);
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Functionn    : WsscfgGetFirstFsWlanSSIDStatsTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsWlanSSIDStatsEntry or NULL
****************************************************************************/
INT1
WsscfgGetFirstSSIDStatsTable (UINT4 *pu4FsDot11WlanProfileId)
{
    tWssPmWlanSSIDStatsProcessRBDB *pWssPmWlanSSIDStatsProcessRBDB = NULL;

    pWssPmWlanSSIDStatsProcessRBDB =
        (tWssPmWlanSSIDStatsProcessRBDB *)
        RBTreeGetFirst (gWssPmWlanSSIDStatsProcessDB);

    if (pWssPmWlanSSIDStatsProcessRBDB != NULL)
    {
        *pu4FsDot11WlanProfileId =
            pWssPmWlanSSIDStatsProcessRBDB->u4FsDot11WlanProfileId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Functionn    : WsscfgGetFirstFsApWlanStatisticsTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsWlanBSSIDStatsEntry or NULL
****************************************************************************/
INT1
WsscfgGetFirstApWlanStatisticsTable (INT4 *pi4IfIndex,
                                     UINT4 *pu4CapwapDot11WlanProfileId)
{
#ifdef WLC_WANTED
    tWssPmWLANBSSIDStatsProcessRBDB *pWssPmWlanBSSIDStatsProcessRBDB = NULL;
    tWssWlanDB          WssWlanDB;
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    pWssPmWlanBSSIDStatsProcessRBDB =
        (tWssPmWLANBSSIDStatsProcessRBDB *)
        RBTreeGetFirst (gWssPmWLANBSSIDStatsProcessDB);

    if (pWssPmWlanBSSIDStatsProcessRBDB != NULL)
    {
        WssWlanDB.WssWlanAttributeDB.u4BssIfIndex =
            pWssPmWlanBSSIDStatsProcessRBDB->u4WlanBSSIndex;
        WssWlanDB.WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
        WssWlanDB.WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
        WssWlanDB.WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;

        if (WssIfProcessWssWlanDBMsg
            (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, &WssWlanDB) == OSIX_SUCCESS)
        {
            *pi4IfIndex = (INT4) WssWlanDB.WssWlanAttributeDB.u4RadioIfIndex;
            *pu4CapwapDot11WlanProfileId =
                (UINT4) WssWlanDB.WssWlanAttributeDB.u2WlanProfileId;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
#else
    UNUSED_PARAM (pi4IfIndex);
    UNUSED_PARAM (pu4CapwapDot11WlanProfileId);
    return SNMP_SUCCESS;
#endif
}

/******************************************************************** 
 Function    :  WsscfgSetFsSecurityWebAuthType
 Input       :  i4FsSecurityWebAuthType
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetFsSecurityWebAuthType (INT4 i4FsSecurityWebAuthType)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthType =
        i4FsSecurityWebAuthType;
    nmhSetCmnNew (FsSecurityWebAuthType, 0, WsscfgMainTaskLock,
                  WsscfgMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthType);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFsSecurityWebAuthUrl
 Input       :  The Indices
 Input       :  pFsSecurityWebAuthUrl
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetFsSecurityWebAuthUrl (UINT1 *pFsSecurityWebAuthUrl)
{
    MEMCPY (pFsSecurityWebAuthUrl,
            gWsscfgGlobals.WsscfgGlbMib.au1FsSecurityWebAuthUrl,
            gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthUrlLen);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetFsSecurityWebAuthUrl
 Input       :  pFsSecurityWebAuthUrl
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgSetFsSecurityWebAuthUrl (tSNMP_OCTET_STRING_TYPE * pFsSecurityWebAuthUrl)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    MEMSET (gWsscfgGlobals.WsscfgGlbMib.au1FsSecurityWebAuthUrl, 0,
            sizeof (gWsscfgGlobals.WsscfgGlbMib.au1FsSecurityWebAuthUrl));
    MEMCPY (gWsscfgGlobals.WsscfgGlbMib.au1FsSecurityWebAuthUrl,
            pFsSecurityWebAuthUrl->pu1_OctetList,
            pFsSecurityWebAuthUrl->i4_Length);
    gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthUrlLen =
        pFsSecurityWebAuthUrl->i4_Length;
    nmhSetCmnNew (FsSecurityWebAuthUrl, 0, WsscfgMainTaskLock,
                  WsscfgMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gWsscfgGlobals.WsscfgGlbMib.au1FsSecurityWebAuthUrl);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFsSecurityWebAuthRedirectUrl
 Input       :  The Indices
 Input       :  pFsSecurityWebAuthRedirectUrl
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetFsSecurityWebAuthRedirectUrl (UINT1 *pFsSecurityWebAuthRedirectUrl)
{
    MEMCPY (pFsSecurityWebAuthRedirectUrl,
            gWsscfgGlobals.WsscfgGlbMib.
            au1FsSecurityWebAuthRedirectUrl,
            gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthRedirectUrlLen);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetFsSecurityWebAuthRedirectUrl
 Input       :  pFsSecurityWebAuthRedirectUrl
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgSetFsSecurityWebAuthRedirectUrl (UINT1 *pFsSecurityWebAuthRedirectUrl)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    MEMSET (gWsscfgGlobals.WsscfgGlbMib.
            au1FsSecurityWebAuthRedirectUrl, 0,
            sizeof (gWsscfgGlobals.
                    WsscfgGlbMib.au1FsSecurityWebAuthRedirectUrl));
    MEMCPY (gWsscfgGlobals.WsscfgGlbMib.
            au1FsSecurityWebAuthRedirectUrl,
            pFsSecurityWebAuthRedirectUrl,
            gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthRedirectUrlLength);

    nmhSetCmnNew (FsSecurityWebAuthRedirectUrl, 0, WsscfgMainTaskLock,
                  WsscfgMainTaskUnLock, 0, 0, 0, i4SetOption, "%s",
                  gWsscfgGlobals.WsscfgGlbMib.au1FsSecurityWebAuthRedirectUrl);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFsSecurityWebAddr
 Input       :  The Indices
 Input       :  pi4FsSecurityWebAddr
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetFsSecurityWebAddr (INT4 *pi4FsSecurityWebAddr)
{
    *pi4FsSecurityWebAddr = gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAddr;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetFsSecurityWebAddr
 Input       :  i4FsSecurityWebAddr
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgSetFsSecurityWebAddr (INT4 i4FsSecurityWebAddr)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAddr = i4FsSecurityWebAddr;
    nmhSetCmnNew (FsSecurityWebAddr, 0, WsscfgMainTaskLock,
                  WsscfgMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAddr);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFsSecurityWebAuthWebTitle
 Input       :  The Indices
 Input       :  pFsSecurityWebAuthWebTitle
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetFsSecurityWebAuthWebTitle (UINT1 *pFsSecurityWebAuthWebTitle)
{
    MEMCPY (pFsSecurityWebAuthWebTitle,
            gWsscfgGlobals.WsscfgGlbMib.au1FsSecurityWebAuthWebTitle,
            gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthWebTitleLen);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetFsSecurityWebAuthWebTitle
 Input       :  pFsSecurityWebAuthWebTitle
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgSetFsSecurityWebAuthWebTitle (tSNMP_OCTET_STRING_TYPE *
                                    pFsSecurityWebAuthWebTitle)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    MEMSET (gWsscfgGlobals.WsscfgGlbMib.au1FsSecurityWebAuthWebTitle,
            0,
            sizeof (gWsscfgGlobals.WsscfgGlbMib.au1FsSecurityWebAuthWebTitle));
    MEMCPY (gWsscfgGlobals.WsscfgGlbMib.
            au1FsSecurityWebAuthWebTitle,
            pFsSecurityWebAuthWebTitle->pu1_OctetList,
            MEM_MAX_BYTES ((UINT4) pFsSecurityWebAuthWebTitle->i4_Length,
                           sizeof (gWsscfgGlobals.WsscfgGlbMib.
                                   au1FsSecurityWebAuthWebTitle)));
    gWsscfgGlobals.WsscfgGlbMib.
        au1FsSecurityWebAuthWebTitle[pFsSecurityWebAuthWebTitle->i4_Length] =
        '\0';
    gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthWebTitleLen =
        STRLEN (pFsSecurityWebAuthWebTitle->pu1_OctetList);
    nmhSetCmnNew (FsSecurityWebAuthWebTitle, 0, WsscfgMainTaskLock,
                  WsscfgMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gWsscfgGlobals.WsscfgGlbMib.au1FsSecurityWebAuthWebTitle);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFsSecurityWebAuthWebMessage
 Input       :  The Indices
 Input       :  pFsSecurityWebAuthWebMessage
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetFsSecurityWebAuthWebMessage (UINT1 *pFsSecurityWebAuthWebMessage)
{
    MEMCPY (pFsSecurityWebAuthWebMessage,
            gWsscfgGlobals.WsscfgGlbMib.au1FsSecurityWebAuthWebMessage,
            gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthWebMessageLen);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetFsSecurityWebAuthWebMessage
 Input       :  pFsSecurityWebAuthWebMessage
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgSetFsSecurityWebAuthWebMessage (tSNMP_OCTET_STRING_TYPE *
                                      pFsSecurityWebAuthWebMessage)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    MEMSET (gWsscfgGlobals.WsscfgGlbMib.au1FsSecurityWebAuthWebMessage,
            0,
            sizeof (gWsscfgGlobals.
                    WsscfgGlbMib.au1FsSecurityWebAuthWebMessage));
    MEMCPY (gWsscfgGlobals.WsscfgGlbMib.
            au1FsSecurityWebAuthWebMessage,
            pFsSecurityWebAuthWebMessage->pu1_OctetList,
            MEM_MAX_BYTES ((UINT4) pFsSecurityWebAuthWebMessage->i4_Length,
                           sizeof (gWsscfgGlobals.WsscfgGlbMib.
                                   au1FsSecurityWebAuthWebMessage)));
    gWsscfgGlobals.WsscfgGlbMib.
        au1FsSecurityWebAuthWebMessage[pFsSecurityWebAuthWebMessage->
                                       i4_Length] = '\0';
    gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthWebMessageLen =
        STRLEN (pFsSecurityWebAuthWebMessage->pu1_OctetList);
    nmhSetCmnNew (FsSecurityWebAuthWebMessage, 0, WsscfgMainTaskLock,
                  WsscfgMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gWsscfgGlobals.WsscfgGlbMib.au1FsSecurityWebAuthWebMessage);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFsSecurityWebAuthWebLogoFileName
 Input       :  The Indices
 Input       :  pFsSecurityWebAuthWebLogoFileName
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetFsSecurityWebAuthWebLogoFileName (UINT1
                                           *pFsSecurityWebAuthWebLogoFileName)
{
    MEMCPY (pFsSecurityWebAuthWebLogoFileName,
            gWsscfgGlobals.WsscfgGlbMib.
            au1FsSecurityWebAuthWebLogoFileName,
            gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthWebLogoFileNameLen);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetFsSecurityWebAuthWebLogoFileName
 Input       :  pFsSecurityWebAuthWebLogoFileName
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgSetFsSecurityWebAuthWebLogoFileName (tSNMP_OCTET_STRING_TYPE *
                                           pFsSecurityWebAuthWebLogoFileName)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    MEMSET (gWsscfgGlobals.WsscfgGlbMib.
            au1FsSecurityWebAuthWebLogoFileName, 0,
            sizeof (gWsscfgGlobals.
                    WsscfgGlbMib.au1FsSecurityWebAuthWebLogoFileName));
    MEMCPY (gWsscfgGlobals.WsscfgGlbMib.
            au1FsSecurityWebAuthWebLogoFileName,
            pFsSecurityWebAuthWebLogoFileName->pu1_OctetList,
            MEM_MAX_BYTES ((UINT4) pFsSecurityWebAuthWebLogoFileName->i4_Length,
                           sizeof (gWsscfgGlobals.WsscfgGlbMib.
                                   au1FsSecurityWebAuthWebLogoFileName)));
    gWsscfgGlobals.WsscfgGlbMib.
        au1FsSecurityWebAuthWebLogoFileName[pFsSecurityWebAuthWebLogoFileName->
                                            i4_Length] = '\0';
    gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthWebLogoFileNameLen =
        STRLEN (pFsSecurityWebAuthWebLogoFileName->pu1_OctetList);
    nmhSetCmnNew (FsSecurityWebAuthWebLogoFileName, 0, WsscfgMainTaskLock,
                  WsscfgMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gWsscfgGlobals.WsscfgGlbMib.
                  au1FsSecurityWebAuthWebLogoFileName);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFsSecurityWebAuthWebSuccMessage
 Input       :  The Indices
 Input       :  pFsSecurityWebAuthWebSuccMessage
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetFsSecurityWebAuthWebSuccMessage (UINT1
                                          *pFsSecurityWebAuthWebSuccMessage)
{
    MEMCPY (pFsSecurityWebAuthWebSuccMessage,
            gWsscfgGlobals.WsscfgGlbMib.
            au1FsSecurityWebAuthWebSuccMessage,
            gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthWebSuccMessageLen);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetFsSecurityWebAuthWebSuccMessage
 Input       :  pFsSecurityWebAuthWebSuccMessage
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgSetFsSecurityWebAuthWebSuccMessage (tSNMP_OCTET_STRING_TYPE *
                                          pFsSecurityWebAuthWebSuccMessage)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    MEMSET (gWsscfgGlobals.WsscfgGlbMib.
            au1FsSecurityWebAuthWebSuccMessage, 0,
            sizeof (gWsscfgGlobals.
                    WsscfgGlbMib.au1FsSecurityWebAuthWebSuccMessage));
    gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthWebSuccMessageLen =
        STRLEN (pFsSecurityWebAuthWebSuccMessage->pu1_OctetList);
    MEMCPY (gWsscfgGlobals.WsscfgGlbMib.
            au1FsSecurityWebAuthWebSuccMessage,
            pFsSecurityWebAuthWebSuccMessage->pu1_OctetList,
            MEM_MAX_BYTES ((UINT4) pFsSecurityWebAuthWebSuccMessage->i4_Length,
                           sizeof (gWsscfgGlobals.WsscfgGlbMib.
                                   au1FsSecurityWebAuthWebSuccMessage)));
    gWsscfgGlobals.WsscfgGlbMib.
        au1FsSecurityWebAuthWebSuccMessage[pFsSecurityWebAuthWebSuccMessage->
                                           i4_Length] = '\0';
    nmhSetCmnNew (FsSecurityWebAuthWebSuccMessage, 0, WsscfgMainTaskLock,
                  WsscfgMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gWsscfgGlobals.WsscfgGlbMib.
                  au1FsSecurityWebAuthWebSuccMessage);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFsSecurityWebAuthWebFailMessage
 Input       :  The Indices
 Input       :  pFsSecurityWebAuthWebFailMessage
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetFsSecurityWebAuthWebFailMessage (UINT1
                                          *pFsSecurityWebAuthWebFailMessage)
{
    MEMCPY (pFsSecurityWebAuthWebFailMessage,
            gWsscfgGlobals.WsscfgGlbMib.
            au1FsSecurityWebAuthWebFailMessage,
            gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthWebFailMessageLen);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetFsSecurityWebAuthWebFailMessage
 Input       :  pFsSecurityWebAuthWebFailMessage
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgSetFsSecurityWebAuthWebFailMessage (tSNMP_OCTET_STRING_TYPE *
                                          pFsSecurityWebAuthWebFailMessage)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    MEMSET (gWsscfgGlobals.WsscfgGlbMib.
            au1FsSecurityWebAuthWebFailMessage, 0,
            sizeof (gWsscfgGlobals.
                    WsscfgGlbMib.au1FsSecurityWebAuthWebFailMessage));
    MEMCPY (gWsscfgGlobals.WsscfgGlbMib.
            au1FsSecurityWebAuthWebFailMessage,
            pFsSecurityWebAuthWebFailMessage->pu1_OctetList,
            MEM_MAX_BYTES ((UINT4) pFsSecurityWebAuthWebFailMessage->i4_Length,
                           sizeof (gWsscfgGlobals.WsscfgGlbMib.
                                   au1FsSecurityWebAuthWebFailMessage)));
    gWsscfgGlobals.WsscfgGlbMib.
        au1FsSecurityWebAuthWebFailMessage[pFsSecurityWebAuthWebFailMessage->
                                           i4_Length] = '\0';
    gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthWebFailMessageLen =
        STRLEN (pFsSecurityWebAuthWebFailMessage->pu1_OctetList);
    nmhSetCmnNew (FsSecurityWebAuthWebFailMessage, 0, WsscfgMainTaskLock,
                  WsscfgMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gWsscfgGlobals.WsscfgGlbMib.
                  au1FsSecurityWebAuthWebFailMessage);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFsSecurityWebAuthWebButtonText
 Input       :  The Indices
 Input       :  pFsSecurityWebAuthWebButtonText
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetFsSecurityWebAuthWebButtonText (UINT1 *pFsSecurityWebAuthWebButtonText)
{
    MEMCPY (pFsSecurityWebAuthWebButtonText,
            gWsscfgGlobals.WsscfgGlbMib.
            au1FsSecurityWebAuthWebButtonText,
            gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthWebButtonTextLen);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetFsSecurityWebAuthWebButtonText
 Input       :  pFsSecurityWebAuthWebButtonText
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgSetFsSecurityWebAuthWebButtonText (tSNMP_OCTET_STRING_TYPE *
                                         pFsSecurityWebAuthWebButtonText)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    MEMSET (gWsscfgGlobals.WsscfgGlbMib.
            au1FsSecurityWebAuthWebButtonText, 0,
            sizeof (gWsscfgGlobals.
                    WsscfgGlbMib.au1FsSecurityWebAuthWebButtonText));
    MEMCPY (gWsscfgGlobals.WsscfgGlbMib.
            au1FsSecurityWebAuthWebButtonText,
            pFsSecurityWebAuthWebButtonText->pu1_OctetList,
            MEM_MAX_BYTES ((UINT4) pFsSecurityWebAuthWebButtonText->i4_Length,
                           sizeof (gWsscfgGlobals.WsscfgGlbMib.
                                   au1FsSecurityWebAuthWebButtonText)));
    gWsscfgGlobals.WsscfgGlbMib.
        au1FsSecurityWebAuthWebButtonText[pFsSecurityWebAuthWebButtonText->
                                          i4_Length] = '\0';
    gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthWebButtonTextLen =
        STRLEN (pFsSecurityWebAuthWebButtonText->pu1_OctetList);
    nmhSetCmnNew (FsSecurityWebAuthWebButtonText, 0, WsscfgMainTaskLock,
                  WsscfgMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gWsscfgGlobals.WsscfgGlbMib.
                  au1FsSecurityWebAuthWebButtonText);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFsSecurityWebAuthWebLoadBalInfo
 Input       :  The Indices
 Input       :  pFsSecurityWebAuthWebLoadBalInfo
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetFsSecurityWebAuthWebLoadBalInfo (UINT1
                                          *pFsSecurityWebAuthWebLoadBalInfo)
{
    MEMCPY (pFsSecurityWebAuthWebLoadBalInfo,
            gWsscfgGlobals.WsscfgGlbMib.
            au1FsSecurityWebAuthWebLoadBalInfo,
            gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthWebLoadBalInfoLen);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetFsSecurityWebAuthWebLoadBalInfo
 Input       :  pFsSecurityWebAuthWebLoadBalInfo
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgSetFsSecurityWebAuthWebLoadBalInfo (tSNMP_OCTET_STRING_TYPE *
                                          pFsSecurityWebAuthWebLoadBalInfo)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    MEMSET (gWsscfgGlobals.WsscfgGlbMib.
            au1FsSecurityWebAuthWebLoadBalInfo, 0,
            sizeof (gWsscfgGlobals.
                    WsscfgGlbMib.au1FsSecurityWebAuthWebLoadBalInfo));
    MEMCPY (gWsscfgGlobals.WsscfgGlbMib.
            au1FsSecurityWebAuthWebLoadBalInfo,
            pFsSecurityWebAuthWebLoadBalInfo->pu1_OctetList,
            MEM_MAX_BYTES ((UINT4) pFsSecurityWebAuthWebLoadBalInfo->i4_Length,
                           sizeof (gWsscfgGlobals.WsscfgGlbMib.
                                   au1FsSecurityWebAuthWebLoadBalInfo)));
    gWsscfgGlobals.WsscfgGlbMib.
        au1FsSecurityWebAuthWebLoadBalInfo[pFsSecurityWebAuthWebLoadBalInfo->
                                           i4_Length] = '\0';
    gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthWebLoadBalInfoLen =
        STRLEN (pFsSecurityWebAuthWebLoadBalInfo->pu1_OctetList);
    nmhSetCmnNew (FsSecurityWebAuthWebLoadBalInfo, 0, WsscfgMainTaskLock,
                  WsscfgMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gWsscfgGlobals.WsscfgGlbMib.
                  au1FsSecurityWebAuthWebLoadBalInfo);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFsSecurityWebAuthDisplayLang
 Input       :  The Indices
 Input       :  pi4FsSecurityWebAuthDisplayLang
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetFsSecurityWebAuthDisplayLang (INT4 *pi4FsSecurityWebAuthDisplayLang)
{
    *pi4FsSecurityWebAuthDisplayLang =
        gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthDisplayLang;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetFsSecurityWebAuthDisplayLang
 Input       :  i4FsSecurityWebAuthDisplayLang
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgSetFsSecurityWebAuthDisplayLang (INT4 i4FsSecurityWebAuthDisplayLang)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthDisplayLang =
        i4FsSecurityWebAuthDisplayLang;
    nmhSetCmnNew (FsSecurityWebAuthDisplayLang, 0, WsscfgMainTaskLock,
                  WsscfgMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthDisplayLang);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFsSecurityWebAuthColor
 Input       :  The Indices
 Input       :  pi4FsSecurityWebAuthColor
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetFsSecurityWebAuthColor (INT4 *pi4FsSecurityWebAuthColor)
{
    *pi4FsSecurityWebAuthColor =
        gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthColor;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetFsSecurityWebAuthColor
 Input       :  i4FsSecurityWebAuthColor
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgSetFsSecurityWebAuthColor (INT4 i4FsSecurityWebAuthColor)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthColor =
        i4FsSecurityWebAuthColor;
    nmhSetCmnNew (FsSecurityWebAuthColor, 0, WsscfgMainTaskLock,
                  WsscfgMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthColor);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11HoppingPatternTable
 Input       :  pCurrentWsscfgDot11HoppingPatternEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11HoppingPatternEntry or NULL
****************************************************************************/
tWsscfgDot11HoppingPatternEntry *
WsscfgGetNextDot11HoppingPatternTable (tWsscfgDot11HoppingPatternEntry *
                                       pCurrentWsscfgDot11HoppingPatternEntry)
{
    tWsscfgDot11HoppingPatternEntry
        * pNextWsscfgDot11HoppingPatternEntry = NULL;

    pNextWsscfgDot11HoppingPatternEntry =
        (tWsscfgDot11HoppingPatternEntry *)
        RBTreeGetNext
        (gWsscfgGlobals.WsscfgGlbMib.Dot11HoppingPatternTable,
         (tRBElem *) pCurrentWsscfgDot11HoppingPatternEntry, NULL);

    return pNextWsscfgDot11HoppingPatternEntry;
}

/****************************************************************************
 Function    :  WsscfgGetDot11HoppingPatternTable
 Input       :  pWsscfgDot11HoppingPatternEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11HoppingPatternEntry or NULL
****************************************************************************/
tWsscfgDot11HoppingPatternEntry *
WsscfgGetDot11HoppingPatternTable (tWsscfgDot11HoppingPatternEntry *
                                   pWsscfgDot11HoppingPatternEntry)
{
    tWsscfgDot11HoppingPatternEntry *pGetWsscfgDot11HoppingPatternEntry = NULL;

    pGetWsscfgDot11HoppingPatternEntry =
        (tWsscfgDot11HoppingPatternEntry *)
        RBTreeGet
        (gWsscfgGlobals.WsscfgGlbMib.Dot11HoppingPatternTable,
         (tRBElem *) pWsscfgDot11HoppingPatternEntry);

    return pGetWsscfgDot11HoppingPatternEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11PhyERPTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11PhyERPEntry or NULL
****************************************************************************/
tWsscfgDot11PhyERPEntry *
WsscfgGetFirstDot11PhyERPTable ()
{
    tWsscfgDot11PhyERPEntry *pWsscfgDot11PhyERPEntry = NULL;

    pWsscfgDot11PhyERPEntry =
        (tWsscfgDot11PhyERPEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyERPTable);

    return pWsscfgDot11PhyERPEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11PhyERPTable
 Input       :  pCurrentWsscfgDot11PhyERPEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11PhyERPEntry or NULL
****************************************************************************/
tWsscfgDot11PhyERPEntry *
WsscfgGetNextDot11PhyERPTable (tWsscfgDot11PhyERPEntry *
                               pCurrentWsscfgDot11PhyERPEntry)
{
    tWsscfgDot11PhyERPEntry *pNextWsscfgDot11PhyERPEntry = NULL;

    pNextWsscfgDot11PhyERPEntry =
        (tWsscfgDot11PhyERPEntry *)
        RBTreeGetNext (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyERPTable,
                       (tRBElem *) pCurrentWsscfgDot11PhyERPEntry, NULL);

    return pNextWsscfgDot11PhyERPEntry;
}

/****************************************************************************
 Function    :  WsscfgGetDot11PhyERPTable
 Input       :  pWsscfgDot11PhyERPEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11PhyERPEntry or NULL
****************************************************************************/
tWsscfgDot11PhyERPEntry *
WsscfgGetDot11PhyERPTable (tWsscfgDot11PhyERPEntry * pWsscfgDot11PhyERPEntry)
{
    tWsscfgDot11PhyERPEntry *pGetWsscfgDot11PhyERPEntry = NULL;

    pGetWsscfgDot11PhyERPEntry =
        (tWsscfgDot11PhyERPEntry *) RBTreeGet (gWsscfgGlobals.
                                               WsscfgGlbMib.Dot11PhyERPTable,
                                               (tRBElem *)
                                               pWsscfgDot11PhyERPEntry);

    return pGetWsscfgDot11PhyERPEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11RSNAConfigTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11RSNAConfigEntry or NULL
****************************************************************************/
tWsscfgDot11RSNAConfigEntry *
WsscfgGetFirstDot11RSNAConfigTable ()
{
    tWsscfgDot11RSNAConfigEntry *pWsscfgDot11RSNAConfigEntry = NULL;

    pWsscfgDot11RSNAConfigEntry =
        (tWsscfgDot11RSNAConfigEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.Dot11RSNAConfigTable);

    return pWsscfgDot11RSNAConfigEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11RSNAConfigTable
 Input       :  pCurrentWsscfgDot11RSNAConfigEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11RSNAConfigEntry or NULL
****************************************************************************/
tWsscfgDot11RSNAConfigEntry *
WsscfgGetNextDot11RSNAConfigTable (tWsscfgDot11RSNAConfigEntry *
                                   pCurrentWsscfgDot11RSNAConfigEntry)
{
    tWsscfgDot11RSNAConfigEntry *pNextWsscfgDot11RSNAConfigEntry = NULL;

    pNextWsscfgDot11RSNAConfigEntry =
        (tWsscfgDot11RSNAConfigEntry *)
        RBTreeGetNext
        (gWsscfgGlobals.WsscfgGlbMib.Dot11RSNAConfigTable, (tRBElem *)
         pCurrentWsscfgDot11RSNAConfigEntry, NULL);

    return pNextWsscfgDot11RSNAConfigEntry;
}

/****************************************************************************
 Function    :  WsscfgGetDot11RSNAConfigTable
 Input       :  pWsscfgDot11RSNAConfigEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11RSNAConfigEntry or NULL
****************************************************************************/
tWsscfgDot11RSNAConfigEntry *
WsscfgGetDot11RSNAConfigTable (tWsscfgDot11RSNAConfigEntry *
                               pWsscfgDot11RSNAConfigEntry)
{
    tWsscfgDot11RSNAConfigEntry *pGetWsscfgDot11RSNAConfigEntry = NULL;

    pGetWsscfgDot11RSNAConfigEntry =
        (tWsscfgDot11RSNAConfigEntry *)
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11RSNAConfigTable,
                   (tRBElem *) pWsscfgDot11RSNAConfigEntry);

    return pGetWsscfgDot11RSNAConfigEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11RSNAConfigPairwiseCiphersTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11RSNAConfigPairwiseCiphersEntry or NULL
****************************************************************************/
tWsscfgDot11RSNAConfigPairwiseCiphersEntry
    * WsscfgGetFirstDot11RSNAConfigPairwiseCiphersTable ()
{
    tWsscfgDot11RSNAConfigPairwiseCiphersEntry
        * pWsscfgDot11RSNAConfigPairwiseCiphersEntry = NULL;

    pWsscfgDot11RSNAConfigPairwiseCiphersEntry =
        (tWsscfgDot11RSNAConfigPairwiseCiphersEntry *)
        RBTreeGetFirst (gWsscfgGlobals.
                        WsscfgGlbMib.Dot11RSNAConfigPairwiseCiphersTable);

    return pWsscfgDot11RSNAConfigPairwiseCiphersEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11RSNAConfigPairwiseCiphersTable
 Input       :  pCurrentWsscfgDot11RSNAConfigPairwiseCiphersEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11RSNAConfigPairwiseCiphersEntry or NULL
****************************************************************************/
tWsscfgDot11RSNAConfigPairwiseCiphersEntry
    *
    WsscfgGetNextDot11RSNAConfigPairwiseCiphersTable
    (tWsscfgDot11RSNAConfigPairwiseCiphersEntry *
     pCurrentWsscfgDot11RSNAConfigPairwiseCiphersEntry)
{
    tWsscfgDot11RSNAConfigPairwiseCiphersEntry
        * pNextWsscfgDot11RSNAConfigPairwiseCiphersEntry = NULL;

    pNextWsscfgDot11RSNAConfigPairwiseCiphersEntry =
        (tWsscfgDot11RSNAConfigPairwiseCiphersEntry *)
        RBTreeGetNext (gWsscfgGlobals.
                       WsscfgGlbMib.Dot11RSNAConfigPairwiseCiphersTable,
                       (tRBElem *)
                       pCurrentWsscfgDot11RSNAConfigPairwiseCiphersEntry, NULL);

    return pNextWsscfgDot11RSNAConfigPairwiseCiphersEntry;
}

/****************************************************************************
 Function    :  WsscfgGetDot11RSNAConfigPairwiseCiphersTable
 Input       :  pWsscfgDot11RSNAConfigPairwiseCiphersEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11RSNAConfigPairwiseCiphersEntry or NULL
****************************************************************************/
tWsscfgDot11RSNAConfigPairwiseCiphersEntry
    *
    WsscfgGetDot11RSNAConfigPairwiseCiphersTable
    (tWsscfgDot11RSNAConfigPairwiseCiphersEntry *
     pWsscfgDot11RSNAConfigPairwiseCiphersEntry)
{
    tWsscfgDot11RSNAConfigPairwiseCiphersEntry
        * pGetWsscfgDot11RSNAConfigPairwiseCiphersEntry = NULL;

    pGetWsscfgDot11RSNAConfigPairwiseCiphersEntry =
        (tWsscfgDot11RSNAConfigPairwiseCiphersEntry *)
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.Dot11RSNAConfigPairwiseCiphersTable,
                   (tRBElem *) pWsscfgDot11RSNAConfigPairwiseCiphersEntry);

    return pGetWsscfgDot11RSNAConfigPairwiseCiphersEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstDot11RSNAConfigAuthenticationSuitesTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgDot11RSNAConfigAuthenticationSuitesEntry or NULL
****************************************************************************/
tWsscfgDot11RSNAConfigAuthenticationSuitesEntry
    * WsscfgGetFirstDot11RSNAConfigAuthenticationSuitesTable ()
{
    tWsscfgDot11RSNAConfigAuthenticationSuitesEntry
        * pWsscfgDot11RSNAConfigAuthenticationSuitesEntry = NULL;

    pWsscfgDot11RSNAConfigAuthenticationSuitesEntry =
        (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *)
        RBTreeGetFirst (gWsscfgGlobals.
                        WsscfgGlbMib.Dot11RSNAConfigAuthenticationSuitesTable);

    return pWsscfgDot11RSNAConfigAuthenticationSuitesEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextDot11RSNAConfigAuthenticationSuitesTable
 Input       :  pCurrentWsscfgDot11RSNAConfigAuthenticationSuitesEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11RSNAConfigAuthenticationSuitesEntry or NULL
****************************************************************************/
tWsscfgDot11RSNAConfigAuthenticationSuitesEntry
    *
    WsscfgGetNextDot11RSNAConfigAuthenticationSuitesTable
    (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *
     pCurrentWsscfgDot11RSNAConfigAuthenticationSuitesEntry)
{
    tWsscfgDot11RSNAConfigAuthenticationSuitesEntry
        * pNextWsscfgDot11RSNAConfigAuthenticationSuitesEntry = NULL;

    pNextWsscfgDot11RSNAConfigAuthenticationSuitesEntry =
        (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *)
        RBTreeGetNext (gWsscfgGlobals.
                       WsscfgGlbMib.Dot11RSNAConfigAuthenticationSuitesTable,
                       (tRBElem *)
                       pCurrentWsscfgDot11RSNAConfigAuthenticationSuitesEntry,
                       NULL);

    return pNextWsscfgDot11RSNAConfigAuthenticationSuitesEntry;
}

/* WSS Changes Starts */

/****************************************************************************
 Function    :  WsscfgGetNextDot11RSNAStatsTable
 Input       :  pCurrentWsscfgGetNextDot11RSNAStatsTable
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgDot11RSNAConfigAuthenticationSuitesEntry or NULL
****************************************************************************/
tWsscfgDot11RSNAStatsEntry *
WsscfgGetNextDot11RSNAStatsTable (tWsscfgDot11RSNAStatsEntry *
                                  pCurrentWsscfgDot11RSNAStatsEntry)
{
    tWsscfgDot11RSNAStatsEntry *pNextWsscfgDot11RSNAStatsEntry = NULL;

    pNextWsscfgDot11RSNAStatsEntry =
        (tWsscfgDot11RSNAStatsEntry *)
        RBTreeGetNext (gWsscfgGlobals.WsscfgGlbMib.Dot11RSNAStatsTable,
                       (tRBElem *) pCurrentWsscfgDot11RSNAStatsEntry, NULL);

    return pNextWsscfgDot11RSNAStatsEntry;
}

/****************************************************************************
 Function    :  
 Input       :  
 Description :  This Routine is used to take
                next index from a table
 Returns     :   or NULL
****************************************************************************/
INT1
WsscfgGetNextDot11CountersTable (INT4 i4PrevIndex, INT4 *pi4NextIndex)
{
    tRadioIfDB          RadioIfDB;
    tRadioIfDB         *pNextRadioIfDB = NULL;

    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));

    RadioIfDB.u4VirtualRadioIfIndex = i4PrevIndex;

    pNextRadioIfDB = (tRadioIfDB *) RBTreeGetNext (gRadioIfGlobals.
                                                   RadioIfGlbMib.RadioIfDB,
                                                   (tRBElem *) & RadioIfDB,
                                                   NULL);
    if (NULL == pNextRadioIfDB)
    {
        /* no entry present, just return error */
        return SNMP_FAILURE;
    }

    *pi4NextIndex = pNextRadioIfDB->u4VirtualRadioIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  
 Input       :  
 Description :  This Routine is used to take
                next index from a table
 Returns     :   or NULL
****************************************************************************/
tWsscfgDot11QosCountersEntry *
WsscfgGetNextDot11QosCountersTable (tWsscfgDot11QosCountersEntry *
                                    pCurrentWsscfgDot11QosCountersEntry)
{
    tWsscfgDot11QosCountersEntry *pNextWsscfgDot11QosCountersEntry = NULL;

    pNextWsscfgDot11QosCountersEntry =
        (tWsscfgDot11QosCountersEntry *)
        RBTreeGetNext
        (gWsscfgGlobals.WsscfgGlbMib.Dot11QosCountersTable,
         (tRBElem *) pCurrentWsscfgDot11QosCountersEntry, NULL);

    return pNextWsscfgDot11QosCountersEntry;
}

/****************************************************************************
 Function    :  
 Input       :  
 Description :  This Routine is used to take
                next index from a table
 Returns     :   or NULL
****************************************************************************/
tWsscfgDot11ResourceInfoEntry *
WsscfgGetNextDot11ResourceInfoTable (tWsscfgDot11ResourceInfoEntry *
                                     pCurrentWsscfgDot11ResourceInfoEntry)
{
    tWsscfgDot11ResourceInfoEntry *pNextWsscfgDot11ResourceInfoEntry = NULL;

    pNextWsscfgDot11ResourceInfoEntry =
        (tWsscfgDot11ResourceInfoEntry *)
        RBTreeGetNext
        (gWsscfgGlobals.WsscfgGlbMib.Dot11ResourceInfoTable,
         (tRBElem *) pCurrentWsscfgDot11ResourceInfoEntry, NULL);

    return pNextWsscfgDot11ResourceInfoEntry;
}

/****************************************************************************
 Function    :  
 Input       :  
 Description :  This Routine is used to take
                next index from a table
 Returns     :   or NULL
****************************************************************************/
tWsscfgDot11RegDomainsSupportedEntry
    *
    WsscfgGetNextDot11RegDomainsSupportedTable
    (tWsscfgDot11RegDomainsSupportedEntry *
     pCurrentWsscfgDot11RegDomainsSupportedEntry)
{
    tWsscfgDot11RegDomainsSupportedEntry
        * pNextWsscfgDot11RegDomainsSupportedEntry = NULL;

    pNextWsscfgDot11RegDomainsSupportedEntry =
        (tWsscfgDot11RegDomainsSupportedEntry *)
        RBTreeGetNext (gWsscfgGlobals.
                       WsscfgGlbMib.Dot11RegDomainsSupportedTable,
                       (tRBElem *)
                       pCurrentWsscfgDot11RegDomainsSupportedEntry, NULL);

    return pNextWsscfgDot11RegDomainsSupportedEntry;
}

/****************************************************************************
 Function    :  
 Input       :  
 Description :  This Routine is used to take
                next index from a table
 Returns     :   or NULL
****************************************************************************/
tWsscfgDot11SupportedDataRatesTxEntry
    *
    WsscfgGetNextDot11SupportedDataRatesTxTable
    (tWsscfgDot11SupportedDataRatesTxEntry *
     pCurrentWsscfgDot11SupportedDataRatesTxEntry)
{
    tWsscfgDot11SupportedDataRatesTxEntry
        * pNextWsscfgDot11SupportedDataRatesTxEntry = NULL;

    pNextWsscfgDot11SupportedDataRatesTxEntry =
        (tWsscfgDot11SupportedDataRatesTxEntry *)
        RBTreeGetNext (gWsscfgGlobals.
                       WsscfgGlbMib.Dot11SupportedDataRatesTxTable,
                       (tRBElem *)
                       pCurrentWsscfgDot11SupportedDataRatesTxEntry, NULL);

    return pNextWsscfgDot11SupportedDataRatesTxEntry;
}

/****************************************************************************
 Function    :  
 Input       :  
 Description :  This Routine is used to take
                next index from a table
 Returns     :   or NULL
****************************************************************************/
tWsscfgDot11SupportedDataRatesRxEntry
    *
    WsscfgGetNextDot11SupportedDataRatesRxTable
    (tWsscfgDot11SupportedDataRatesRxEntry *
     pCurrentWsscfgDot11SupportedDataRatesRxEntry)
{
    tWsscfgDot11SupportedDataRatesRxEntry
        * pNextWsscfgDot11SupportedDataRatesRxEntry = NULL;

    pNextWsscfgDot11SupportedDataRatesRxEntry =
        (tWsscfgDot11SupportedDataRatesRxEntry *)
        RBTreeGetNext (gWsscfgGlobals.
                       WsscfgGlbMib.Dot11SupportedDataRatesRxTable,
                       (tRBElem *)
                       pCurrentWsscfgDot11SupportedDataRatesRxEntry, NULL);

    return pNextWsscfgDot11SupportedDataRatesRxEntry;
}

/****************************************************************************
 Function    :  
 Input       :  
 Description :  This Routine is used to take
                next index from a table
 Returns     :   or NULL
****************************************************************************/
tWsscfgDot11PhyHRDSSSEntry *
WsscfgGetNextDot11PhyHRDSSSTable (tWsscfgDot11PhyHRDSSSEntry *
                                  pCurrentWsscfgDot11PhyHRDSSSEntry)
{
    tWsscfgDot11PhyHRDSSSEntry *pNextWsscfgDot11PhyHRDSSSEntry = NULL;

    pNextWsscfgDot11PhyHRDSSSEntry =
        (tWsscfgDot11PhyHRDSSSEntry *)
        RBTreeGetNext (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyHRDSSSTable,
                       (tRBElem *) pCurrentWsscfgDot11PhyHRDSSSEntry, NULL);

    return pNextWsscfgDot11PhyHRDSSSEntry;
}

/* WSS Changes Ends */

/****************************************************************************
 Function    :  WsscfgGetDot11RSNAConfigAuthenticationSuitesTable
 Input       :  pWsscfgDot11RSNAConfigAuthenticationSuitesEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgDot11RSNAConfigAuthenticationSuitesEntry or NULL
****************************************************************************/
tWsscfgDot11RSNAConfigAuthenticationSuitesEntry
    *
    WsscfgGetDot11RSNAConfigAuthenticationSuitesTable
    (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *
     pWsscfgDot11RSNAConfigAuthenticationSuitesEntry)
{
    tWsscfgDot11RSNAConfigAuthenticationSuitesEntry
        * pGetWsscfgDot11RSNAConfigAuthenticationSuitesEntry = NULL;

    pGetWsscfgDot11RSNAConfigAuthenticationSuitesEntry =
        (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *)
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.Dot11RSNAConfigAuthenticationSuitesTable,
                   (tRBElem *) pWsscfgDot11RSNAConfigAuthenticationSuitesEntry);

    return pGetWsscfgDot11RSNAConfigAuthenticationSuitesEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstCapwapDot11WlanTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgCapwapDot11WlanEntry or NULL
****************************************************************************/
tWsscfgCapwapDot11WlanEntry *
WsscfgGetFirstCapwapDot11WlanTable ()
{
    tWsscfgCapwapDot11WlanEntry *pWsscfgCapwapDot11WlanEntry = NULL;

    pWsscfgCapwapDot11WlanEntry = (tWsscfgCapwapDot11WlanEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.CapwapDot11WlanTable);

    return pWsscfgCapwapDot11WlanEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextCapwapDot11WlanTable
 Input       :  pCurrentWsscfgCapwapDot11WlanEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgCapwapDot11WlanEntry or NULL
****************************************************************************/
tWsscfgCapwapDot11WlanEntry *
WsscfgGetNextCapwapDot11WlanTable (tWsscfgCapwapDot11WlanEntry *
                                   pCurrentWsscfgCapwapDot11WlanEntry)
{
    tWsscfgCapwapDot11WlanEntry *pNextWsscfgCapwapDot11WlanEntry = NULL;

    pNextWsscfgCapwapDot11WlanEntry = (tWsscfgCapwapDot11WlanEntry *)
        RBTreeGetNext (gWsscfgGlobals.WsscfgGlbMib.
                       CapwapDot11WlanTable, (tRBElem *)
                       pCurrentWsscfgCapwapDot11WlanEntry, NULL);

    return pNextWsscfgCapwapDot11WlanEntry;
}

/****************************************************************************
 Function    :  WsscfgGetCapwapDot11WlanTable
 Input       :  pWsscfgCapwapDot11WlanEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgCapwapDot11WlanEntry or NULL
****************************************************************************/
tWsscfgCapwapDot11WlanEntry *
WsscfgGetCapwapDot11WlanTable (tWsscfgCapwapDot11WlanEntry *
                               pWsscfgCapwapDot11WlanEntry)
{
    tWsscfgCapwapDot11WlanEntry *pGetWsscfgCapwapDot11WlanEntry = NULL;

    pGetWsscfgCapwapDot11WlanEntry =
        (tWsscfgCapwapDot11WlanEntry *)
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.CapwapDot11WlanTable,
                   (tRBElem *) pWsscfgCapwapDot11WlanEntry);

    return pGetWsscfgCapwapDot11WlanEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstCapwapDot11WlanBindTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgCapwapDot11WlanBindEntry or NULL
****************************************************************************/
tWsscfgCapwapDot11WlanBindEntry *
WsscfgGetFirstCapwapDot11WlanBindTable ()
{
    tWsscfgCapwapDot11WlanBindEntry *pWsscfgCapwapDot11WlanBindEntry = NULL;

    pWsscfgCapwapDot11WlanBindEntry =
        (tWsscfgCapwapDot11WlanBindEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.CapwapDot11WlanBindTable);

    return pWsscfgCapwapDot11WlanBindEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextCapwapDot11WlanBindTable
 Input       :  pCurrentWsscfgCapwapDot11WlanBindEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgCapwapDot11WlanBindEntry or NULL
****************************************************************************/
tWsscfgCapwapDot11WlanBindEntry *
WsscfgGetNextCapwapDot11WlanBindTable (tWsscfgCapwapDot11WlanBindEntry *
                                       pCurrentWsscfgCapwapDot11WlanBindEntry)
{
    tWsscfgCapwapDot11WlanBindEntry
        * pNextWsscfgCapwapDot11WlanBindEntry = NULL;

    pNextWsscfgCapwapDot11WlanBindEntry =
        (tWsscfgCapwapDot11WlanBindEntry *)
        RBTreeGetNext (gWsscfgGlobals.WsscfgGlbMib.
                       CapwapDot11WlanBindTable, (tRBElem *)
                       pCurrentWsscfgCapwapDot11WlanBindEntry, NULL);

    return pNextWsscfgCapwapDot11WlanBindEntry;
}

/****************************************************************************
 Function    :  WsscfgGetCapwapDot11WlanBindTable
 Input       :  pWsscfgCapwapDot11WlanBindEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgCapwapDot11WlanBindEntry or NULL
****************************************************************************/
tWsscfgCapwapDot11WlanBindEntry *
WsscfgGetCapwapDot11WlanBindTable (tWsscfgCapwapDot11WlanBindEntry *
                                   pWsscfgCapwapDot11WlanBindEntry)
{
    tWsscfgCapwapDot11WlanBindEntry *pGetWsscfgCapwapDot11WlanBindEntry = NULL;

    pGetWsscfgCapwapDot11WlanBindEntry =
        (tWsscfgCapwapDot11WlanBindEntry *)
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.
                   CapwapDot11WlanBindTable, (tRBElem *)
                   pWsscfgCapwapDot11WlanBindEntry);

    return pGetWsscfgCapwapDot11WlanBindEntry;
}

/****************************************************************************
 * Function    :  WssCfgValidateTheWtp
 * Input       :
 * Input       :
 * Descritpion :  This Routine will Get
 *                the Value accordingly.
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/
INT4
WssCfgValidateWtpEntry (tApGroupConfigEntry * pFristApGroup,
                        tApGroupConfigEntry * pSecondApGroup,
                        UINT4 u4RadioPolicy)
{
    if ((pFristApGroup != NULL) && (pSecondApGroup != NULL))
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WssCfgValidateWtpEntry -"
                     " WTP Profile Added in Two AP Groups! \n"));
        return OSIX_FAILURE;
    }

    if ((pFristApGroup != NULL) && (pSecondApGroup == NULL))
    {
        if (u4RadioPolicy == 0)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WssCfgValidateWtpEntry -"
                         " WTP Profile Added in First group for both radios! \n"));
            return OSIX_FAILURE;
        }

        if (pFristApGroup->u2RadioPolicy == 0)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WssCfgValidateWtpEntry - "
                         "WTP Profile Added in First group and "
                         "Second Group canot be possiable as both radio's! \n"));
            return OSIX_FAILURE;
        }

        if (pFristApGroup->u2RadioPolicy == u4RadioPolicy)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WssCfgValidateWtpEntry - "
                         "WTP Profile Added in First group and "
                         "Second Group canot be possiable "
                         "due to same radio Policy! \n"));
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssFindTheApGroupFromWtpProfileId
 * Input       :
 * Input       :
 * Descritpion :  This Routine will Get
 *                the Value accordingly.
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/
VOID
WssFindTheApGroupFromWtpProfileId (UINT4 u4WtpProfileId,
                                   tApGroupConfigEntry ** pFristApGroup,
                                   tApGroupConfigEntry ** pSecondApGroup)
{
    tApGroupConfigEntry *pApGroupConfigEntry = NULL;
    tApGroupConfigWTPEntry *pApGroupFirstConfigWTPEntry = NULL;
    tApGroupConfigWTPEntry *pApGroupNextConfigWTPEntry = NULL;

    /* Traverse the list of AP's */
    if (ApGroupGetFirstConfigWTPEntry (&pApGroupFirstConfigWTPEntry) == FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     " None of the AP Profiles are configured \n"));
        return;
    }
    else
    {
        do                        /* AP's */
        {
            pApGroupNextConfigWTPEntry = pApGroupFirstConfigWTPEntry;
            if (pApGroupNextConfigWTPEntry->u2ApGroupWTPIndex == u4WtpProfileId)
            {
                if (*pFristApGroup == NULL)
                {
                    if (ApGroupGetConfigEntry ((UINT2)
                                               pApGroupNextConfigWTPEntry->
                                               u2ApGroupId,
                                               &pApGroupConfigEntry) != FAILURE)
                    {
                        *pFristApGroup = pApGroupConfigEntry;
                        if (pApGroupConfigEntry->u2RadioPolicy == 0)
                        {
                            *pSecondApGroup = NULL;
                            return;
                        }
                    }
                }
                else
                {
                    pApGroupConfigEntry = NULL;
                    if (ApGroupGetConfigEntry
                        ((UINT2) pApGroupNextConfigWTPEntry->u2ApGroupId,
                         &pApGroupConfigEntry) != FAILURE)
                    {
                        *pSecondApGroup = pApGroupConfigEntry;
                        return;
                    }
                }
            }
        }
        while (ApGroupGetNextConfigWTPEntry (pApGroupNextConfigWTPEntry,
                                             &pApGroupFirstConfigWTPEntry) ==
               SUCCESS);
    }
}

/****************************************************************************
 * Function    :  WsscfgApGroupBinding
 * Input       :  
 * Input       :  
 * Descritpion :  This Routine will Get
 *                the Value accordingly.
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/
INT4
WsscfgApGroupBinding (UINT4 u4WtpProfileId, UINT4 u4WlanId,
                      UINT2 u2RadioPolicy, UINT4 *pu4IfAdminStatus)
{
    INT4                i4RetStatus = OSIX_SUCCESS;
    UINT1               u1RadioId = 0;
    UINT1               au1ModelNumber[256];
    tSNMP_OCTET_STRING_TYPE ModelName;
    INT4                i4WtpMacType = 3;
    INT4                i4WlanMacType = 3;
    UINT4               u4NoOfRadio = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4GetRadioType = 0;

    tWsscfgCapwapDot11WlanBindEntry WsscfgSetCapwapDot11WlanBindEntry;
    tWsscfgIsSetCapwapDot11WlanBindEntry WsscfgIsSetCapwapDot11WlanBindEntry;

    MEMSET (&WsscfgSetCapwapDot11WlanBindEntry, 0,
            sizeof (tWsscfgCapwapDot11WlanBindEntry));
    MEMSET (&WsscfgIsSetCapwapDot11WlanBindEntry, 0,
            sizeof (tWsscfgIsSetCapwapDot11WlanBindEntry));
    MEMSET (&au1ModelNumber, 0, 256);
    MEMSET (&ModelName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    ModelName.pu1_OctetList = au1ModelNumber;

    if ((i4RetStatus =
         nmhGetCapwapBaseWtpProfileWtpModelNumber
         (u4WtpProfileId, &ModelName)) != SNMP_SUCCESS)
    {
        i4RetStatus = OSIX_FAILURE;
    }

    if ((i4RetStatus =
         nmhGetFsCapwapWtpMacType (&ModelName, &i4WtpMacType)) != SNMP_SUCCESS)
    {
        i4RetStatus = OSIX_FAILURE;
    }

    if ((i4RetStatus =
         nmhGetCapwapDot11WlanMacType (u4WlanId,
                                       &i4WlanMacType)) != SNMP_SUCCESS)
    {
        i4RetStatus = OSIX_FAILURE;
    }

    if (i4WtpMacType != i4WlanMacType)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "\r\n WsscfgApGroupBinding -"
                     " MAC Type Mismatch. WLAN Binding Failure\r\n"));
        i4RetStatus = OSIX_FAILURE;
    }

    if (u2RadioPolicy == 0)
    {
        if ((i4RetStatus = nmhGetCapwapBaseWtpProfileWtpModelNumber
             (u4WtpProfileId, &ModelName)) != SNMP_SUCCESS)
        {
            i4RetStatus = OSIX_FAILURE;
        }

        if ((i4RetStatus = nmhGetFsNoOfRadio (&ModelName,
                                              &u4NoOfRadio)) != SNMP_SUCCESS)
        {
            i4RetStatus = OSIX_FAILURE;
        }

        for (u1RadioId = 1; u1RadioId <= u4NoOfRadio; u1RadioId++)
        {
            if ((i4RetStatus =
                 nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                 (u4WtpProfileId, u1RadioId, &i4RadioIfIndex)) != SNMP_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "\r\n WsscfgApGroupBinding - "
                             "Radio Index not found, Binding failed\r\n"));
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            if ((i4RetStatus = nmhGetFsDot11RadioType (i4RadioIfIndex,
                                                       &u4GetRadioType)) !=
                SNMP_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "\r\n WsscfgApGroupBinding -"
                             " Getting Radio Type failed\r\n"));
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            WSSCFG_FILL_CAPWAPDOT11WLANBINDTABLE_ARGS ((&WsscfgSetCapwapDot11WlanBindEntry), (&WsscfgIsSetCapwapDot11WlanBindEntry), pu4IfAdminStatus, &i4RadioIfIndex, &u4WlanId);
            /* Set RowStatus as ACTIVE */
            if (WsscfgSetCapwapDot11WlanBindEntry.MibObject.
                i4CapwapDot11WlanBindRowStatus == CREATE_AND_GO)
            {
                WsscfgSetCapwapDot11WlanBindEntry.MibObject.
                    i4CapwapDot11WlanBindRowStatus = CREATE_AND_WAIT;

                if (WsscfgTestAllCapwapDot11WlanBindTable
                    (&u4ErrorCode, &WsscfgSetCapwapDot11WlanBindEntry,
                     &WsscfgIsSetCapwapDot11WlanBindEntry, OSIX_TRUE,
                     OSIX_TRUE) != OSIX_SUCCESS)
                {
                    i4RetStatus = OSIX_FAILURE;
                }

                if (WsscfgSetAllCapwapDot11WlanBindTable
                    (&WsscfgSetCapwapDot11WlanBindEntry,
                     &WsscfgIsSetCapwapDot11WlanBindEntry, OSIX_TRUE,
                     OSIX_TRUE) != OSIX_SUCCESS)
                {
                    i4RetStatus = OSIX_FAILURE;
                }

                WsscfgSetCapwapDot11WlanBindEntry.MibObject.
                    i4CapwapDot11WlanBindRowStatus = ACTIVE;

            }

            if (WsscfgTestAllCapwapDot11WlanBindTable
                (&u4ErrorCode, &WsscfgSetCapwapDot11WlanBindEntry,
                 &WsscfgIsSetCapwapDot11WlanBindEntry, OSIX_TRUE,
                 OSIX_TRUE) != OSIX_SUCCESS)
            {
                i4RetStatus = OSIX_FAILURE;
            }

            if (WsscfgSetAllCapwapDot11WlanBindTable
                (&WsscfgSetCapwapDot11WlanBindEntry,
                 &WsscfgIsSetCapwapDot11WlanBindEntry, OSIX_TRUE,
                 OSIX_TRUE) != OSIX_SUCCESS)
            {
                i4RetStatus = OSIX_FAILURE;
            }
        }
    }
    else if ((u2RadioPolicy == 1) || (u2RadioPolicy == 2))
    {
        if ((i4RetStatus = nmhGetCapwapBaseWtpProfileWtpModelNumber
             (u4WtpProfileId, &ModelName)) != SNMP_SUCCESS)

        {
            i4RetStatus = OSIX_FAILURE;
        }

        if ((i4RetStatus = nmhGetFsNoOfRadio (&ModelName,
                                              &u4NoOfRadio)) != SNMP_SUCCESS)
        {
            i4RetStatus = OSIX_FAILURE;
        }

        for (u1RadioId = 1; u1RadioId <= u4NoOfRadio; u1RadioId++)
        {
            if ((i4RetStatus =
                 nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                 (u4WtpProfileId, u1RadioId, &i4RadioIfIndex)) != SNMP_SUCCESS)
            {
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            if ((i4RetStatus = nmhGetFsDot11RadioType (i4RadioIfIndex,
                                                       &u4GetRadioType)) !=
                SNMP_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "\r\n WsscfgApGroupBinding - Getting Radio Type failed\r\n"));
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            if (WSSCFG_RADIO_TYPE_COMP ((INT4) u2RadioPolicy))
            {
                continue;
            }
            else
            {
                WSSCFG_FILL_CAPWAPDOT11WLANBINDTABLE_ARGS ((&WsscfgSetCapwapDot11WlanBindEntry), (&WsscfgIsSetCapwapDot11WlanBindEntry), pu4IfAdminStatus, &i4RadioIfIndex, &u4WlanId);
                /* Set RowStatus as ACTIVE */
                if (WsscfgSetCapwapDot11WlanBindEntry.MibObject.
                    i4CapwapDot11WlanBindRowStatus == CREATE_AND_GO)
                {
                    WsscfgSetCapwapDot11WlanBindEntry.MibObject.
                        i4CapwapDot11WlanBindRowStatus = CREATE_AND_WAIT;

                    if (WsscfgTestAllCapwapDot11WlanBindTable
                        (&u4ErrorCode, &WsscfgSetCapwapDot11WlanBindEntry,
                         &WsscfgIsSetCapwapDot11WlanBindEntry, OSIX_TRUE,
                         OSIX_TRUE) != OSIX_SUCCESS)
                    {
                        i4RetStatus = OSIX_FAILURE;
                    }

                    if (WsscfgSetAllCapwapDot11WlanBindTable
                        (&WsscfgSetCapwapDot11WlanBindEntry,
                         &WsscfgIsSetCapwapDot11WlanBindEntry, OSIX_TRUE,
                         OSIX_TRUE) != OSIX_SUCCESS)
                    {
                        i4RetStatus = OSIX_FAILURE;
                    }

                    WsscfgSetCapwapDot11WlanBindEntry.MibObject.
                        i4CapwapDot11WlanBindRowStatus = ACTIVE;

                }
                if (WsscfgTestAllCapwapDot11WlanBindTable
                    (&u4ErrorCode, &WsscfgSetCapwapDot11WlanBindEntry,
                     &WsscfgIsSetCapwapDot11WlanBindEntry, OSIX_TRUE,
                     OSIX_TRUE) != OSIX_SUCCESS)
                {
                    i4RetStatus = OSIX_FAILURE;
                }

                if (WsscfgSetAllCapwapDot11WlanBindTable
                    (&WsscfgSetCapwapDot11WlanBindEntry,
                     &WsscfgIsSetCapwapDot11WlanBindEntry, OSIX_TRUE,
                     OSIX_TRUE) != OSIX_SUCCESS)
                {
                    i4RetStatus = OSIX_FAILURE;
                }

            }
        }
    }
    else
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgApGroupBinding - "
                     "Binding Failed!.. Invalid Ap Group Binding..\n"));
    }
    return i4RetStatus;
}

/****************************************************************************
 * Function    :  WsscfgGetApGroupEnable
 * Input       :  The Indices
 * Input       :  pi4ApGroupEnable
 * Descritpion :  This Routine will Get
 *                the Value accordingly.
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/
INT4
WsscfgGetApGroupEnable (UINT4 *pi4ApGroupEnable)
{
    *pi4ApGroupEnable = gApGroupGlobals.u4ApGroupEnableStatus;
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgSetApGroupEnable
 * Input       :  The Indices
 * Input       :  pi4ApGroupEnable
 * Descritpion :  This Routine will Set
 *                the Value accordingly.
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/
INT4
WsscfgSetApGroupEnable (UINT4 u4ApGroupEnable)
{
    UINT4               u4PrevStatus = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4WlanId = 0;
    UINT2               u2RadioPolicy = 0;
    tWsscfgCapwapDot11WlanBindEntry *pWsscfgCapwapDot11WlanBindEntry = NULL;
    UINT4               u4AdminStatus = CREATE_AND_GO;

    tApGroupConfigEntry *pApGroupFirstConfigEntry = NULL;
    tApGroupConfigEntry *pApGroupNextConfigEntry = NULL;
    tApGroupConfigWTPEntry *pApGroupFirstConfigWTPEntry = NULL;
    tApGroupConfigWTPEntry *pApGroupNextConfigWTPEntry = NULL;
    tApGroupConfigWLANEntry *pApGroupFirstConfigWLANEntry = NULL;
    tApGroupConfigWLANEntry *pApGroupNextConfigWLANEntry = NULL;

    WsscfgGetApGroupEnable (&u4PrevStatus);

    if (u4PrevStatus == u4ApGroupEnable)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgSetApGroupEnable --"
                     " Already in Requested State\n"));
        return OSIX_SUCCESS;
    }

    if (u4ApGroupEnable == CLI_APGROUP_ENABLE)
    {
        /* Check existing static bindings, if Present through Error */
        pWsscfgCapwapDot11WlanBindEntry =
            RBTreeGetFirst (gWsscfgGlobals.
                            WsscfgGlbMib.CapwapDot11WlanBindTable);

        if (pWsscfgCapwapDot11WlanBindEntry)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC, " WsscfgSetApGroupEnable "
                         "Bindings are present. please disable the"
                         " bindings to enable ApGroup feature \n"));
            return OSIX_FAILURE;
        }
        else
        {
            if (ApGroupGetFirstConfigEntry
                (&pApGroupFirstConfigEntry) == FAILURE)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgSetApGroupEnable"
                             " No ap Groups Present \n"));
            }
            else
            {
                do                /* APGroup */
                {
                    pApGroupNextConfigEntry = pApGroupFirstConfigEntry;

                    /* Traverse the list of AP's */
                    if (ApGroupGetFirstConfigWTPEntry
                        (&pApGroupFirstConfigWTPEntry) == FAILURE)
                    {
                        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgSetApGroupEnable -"
                                     " None of the WTP profiles "
                                     "mapped to AP Groups \n"));
                        gApGroupGlobals.u4ApGroupEnableStatus = u4ApGroupEnable;
                    }
                    else
                    {
                        do        /* WTP profiles */
                        {
                            pApGroupNextConfigWTPEntry =
                                pApGroupFirstConfigWTPEntry;
                            if (pApGroupNextConfigWTPEntry->u2ApGroupId ==
                                pApGroupNextConfigEntry->u2ApGroupId)
                            {
                                if (ApGroupGetFirstConfigWLANEntry
                                    (&pApGroupFirstConfigWLANEntry) == FAILURE)
                                {
                                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                                 "WsscfgSetApGroupEnable - "
                                                 " None of the WLAN Profiles"
                                                 " are confiugred  \r\n"));
                                }
                                else
                                {
                                    /* Get the Group Radio Policy */
                                    do    /* WLAN Profiles */
                                    {
                                        pApGroupNextConfigWLANEntry =
                                            pApGroupFirstConfigWLANEntry;
                                        if (pApGroupNextConfigWLANEntry->
                                            u2ApGroupId ==
                                            pApGroupNextConfigEntry->
                                            u2ApGroupId)
                                        {
                                            u4WtpProfileId =
                                                (UINT4)
                                                pApGroupNextConfigWTPEntry->
                                                u2ApGroupWTPIndex;
                                            u4WlanId =
                                                pApGroupNextConfigWLANEntry->
                                                u4WlanId;
                                            u2RadioPolicy =
                                                pApGroupNextConfigEntry->
                                                u2RadioPolicy;
                                            WsscfgApGroupBinding
                                                (u4WtpProfileId, u4WlanId,
                                                 u2RadioPolicy, &u4AdminStatus);
                                        }
                                    }
                                    while (ApGroupGetNextWLANConfigEntry
                                           (pApGroupNextConfigWLANEntry,
                                            &pApGroupFirstConfigWLANEntry) ==
                                           SUCCESS);
                                }
                            }
                        }
                        while (ApGroupGetNextConfigWTPEntry
                               (pApGroupNextConfigWTPEntry,
                                &pApGroupFirstConfigWTPEntry) == SUCCESS);
                    }

                }
                while (ApGroupGetNextConfigEntry
                       (pApGroupNextConfigEntry,
                        &pApGroupFirstConfigEntry) == SUCCESS);
            }
            gApGroupGlobals.u4ApGroupEnableStatus = (UINT4) u4ApGroupEnable;
        }
    }
    else if (u4ApGroupEnable == CLI_APGROUP_DISABLE)
    {
        u4AdminStatus = DESTROY;
        if (ApGroupGetFirstConfigEntry (&pApGroupFirstConfigEntry) == FAILURE)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgSetApGroupEnable "
                         "No ap Groups Present \n"));
        }
        else
        {
            do                    /* AP Group */
            {
                pApGroupNextConfigEntry = pApGroupFirstConfigEntry;

                /* Traverse the list of AP's */
                if (ApGroupGetFirstConfigWTPEntry
                    (&pApGroupFirstConfigWTPEntry) == FAILURE)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetApGroupEnable - "
                                 " None of the AP Profiles are configured\n"));
                    /* return OSIX_FAILURE; */
                }
                else
                {
                    do            /* WTP Profiles */
                    {
                        pApGroupNextConfigWTPEntry =
                            pApGroupFirstConfigWTPEntry;
                        if (pApGroupNextConfigWTPEntry->u2ApGroupId ==
                            pApGroupNextConfigEntry->u2ApGroupId)
                        {
                            if (ApGroupGetFirstConfigWLANEntry
                                (&pApGroupFirstConfigWLANEntry) == FAILURE)
                            {
                                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                             "WsscfgSetApGroupEnable - "
                                             " None of the WLAN Profiles"
                                             " are confiugred  "));
                                /* return OSIX_FAILURE; */
                            }
                            else
                            {
                                /* Get the Group Radio Policy */
                                do    /* WLAN Profiles */
                                {
                                    pApGroupNextConfigWLANEntry =
                                        pApGroupFirstConfigWLANEntry;
                                    if (pApGroupNextConfigWLANEntry->
                                        u2ApGroupId ==
                                        pApGroupNextConfigEntry->u2ApGroupId)
                                    {
                                        u4WtpProfileId =
                                            (UINT4) pApGroupNextConfigWTPEntry->
                                            u2ApGroupWTPIndex;
                                        u4WlanId =
                                            pApGroupNextConfigWLANEntry->
                                            u4WlanId;
                                        u2RadioPolicy =
                                            pApGroupNextConfigEntry->
                                            u2RadioPolicy;
                                        WsscfgApGroupBinding (u4WtpProfileId,
                                                              u4WlanId,
                                                              u2RadioPolicy,
                                                              &u4AdminStatus);
                                    }
                                }
                                while (ApGroupGetNextWLANConfigEntry
                                       (pApGroupNextConfigWLANEntry,
                                        &pApGroupFirstConfigWLANEntry) ==
                                       SUCCESS);
                            }
                        }
                    }
                    while (ApGroupGetNextConfigWTPEntry
                           (pApGroupNextConfigWTPEntry,
                            &pApGroupFirstConfigWTPEntry) == SUCCESS);
                }
            }
            while (ApGroupGetNextConfigEntry
                   (pApGroupNextConfigEntry,
                    &pApGroupFirstConfigEntry) == SUCCESS);
        }
        gApGroupGlobals.u4ApGroupEnableStatus = u4ApGroupEnable;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgGetApGroupName
 * Input       :  The Indices
 *                
 *                u4GroupIndex
 *                pRetValGroupName
 * Descritpion :  This Routine will Get
 *                the Value accordingly.
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT1
WsscfgGetApGroupName (UINT4 u4GroupIndex,
                      tSNMP_OCTET_STRING_TYPE * pRetValGroupName)
{
    tApGroupConfigEntry *pApGroupConfigEntry = NULL;

    if (ApGroupGetConfigEntry ((UINT2) u4GroupIndex,
                               &pApGroupConfigEntry) == FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "ApGroupGetConfigEntry " " returned Failure \n "));
        return OSIX_FAILURE;
    }

    MEMCPY (pRetValGroupName->pu1_OctetList,
            pApGroupConfigEntry->au1ApGroupName,
            pApGroupConfigEntry->u4GroupNameLen);

    pRetValGroupName->i4_Length = pApGroupConfigEntry->u4GroupNameLen;
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgSetApGroupName
 * Input       :  The Indices
 * 
 *                u4GroupIndex
 *                pSetValGroupName
 * Descritpion :  This Routine will Get
 *                the Value accordingly.
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ******************************************************************************/
INT1
WsscfgSetApGroupName (INT4 i4GroupIndex,
                      tSNMP_OCTET_STRING_TYPE * pSetValGroupName)
{
    tApGroupConfigEntry *pApGroupConfigEntry = NULL;
    INT4                i4NameLen = 0;

    i4NameLen = pSetValGroupName->i4_Length;

    if (ApGroupGetConfigEntry ((UINT2) i4GroupIndex,
                               &pApGroupConfigEntry) == FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "ApGroupGetConfigEntry " " returned Failure \n "));
        return FAILURE;
    }

    if (pApGroupConfigEntry != NULL)
    {
        MEMSET (pApGroupConfigEntry->au1ApGroupName, 0,
                sizeof (pApGroupConfigEntry->au1ApGroupName));

        pApGroupConfigEntry->u4GroupNameLen = i4NameLen;

        MEMCPY (pApGroupConfigEntry->au1ApGroupName,
                pSetValGroupName->pu1_OctetList,
                pApGroupConfigEntry->u4GroupNameLen);

        pApGroupConfigEntry->au1ApGroupName
            [pApGroupConfigEntry->u4GroupNameLen] = '\0';
        return SUCCESS;
    }
    return FAILURE;
}

/****************************************************************************
 * Function    :  WsscfgGetApGroupRowStatus
 * Input       :  The Indices
 *
 *                i4GroupIndex
 *                pRetValFsGroupRowStatus
 * Descritpion :  This Routine will Get
 *                the Value accordingly.
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *******************************************************************************/
INT1
WsscfgGetApGroupRowStatus (INT4 i4GroupIndex, INT4 *pRetValFsGroupRowStatus)
{
    tApGroupConfigEntry *pApGroupConfigEntry = NULL;
    if (ApGroupGetConfigEntry ((UINT2) i4GroupIndex,
                               &pApGroupConfigEntry) == FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "ApGroupGetConfigEntry " " returned Failure \n "));
        return FAILURE;
    }

    *pRetValFsGroupRowStatus = pApGroupConfigEntry->i4ApGroupRowstatus;
    return SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgGetApGroupWTPRowStatus
 * Input       :  The Indices
 *
 *                i4GroupIndex
 *                pRetValFsGroupWTPRowStatus
 * Descritpion :  This Routine will Get
 *                the Value accordingly.
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *******************************************************************************/
INT1
WsscfgGetApGroupWTPRowStatus (UINT4 i4GroupIndex,
                              UINT4 u4CapwapBaseWtpProfileId,
                              INT4 *pRetValFsGroupWTPRowStatus)
{
    tApGroupConfigWTPEntry *pApGroupWTPConfigEntry = NULL;
    if (ApGroupGetWTPConfigEntry
        ((UINT2) i4GroupIndex, (UINT2) u4CapwapBaseWtpProfileId,
         &pApGroupWTPConfigEntry) == FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "ApGroupGetConfigEntry " " returned Failure \n "));
        return FAILURE;
    }

    *pRetValFsGroupWTPRowStatus = pApGroupWTPConfigEntry->i4ApGroupWTPRowstatus;
    return SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgGetApGroupWLANRowStatus
 * Input       :  The Indices
 *
 *                i4GroupIndex
 *                pRetValFsGroupWLANRowStatus
 * Descritpion :  This Routine will Get
 *                the Value accordingly.
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *******************************************************************************/
INT1
WsscfgGetApGroupWLANRowStatus (UINT4 i4GroupIndex,
                               UINT4 u4CapwapBaseWtpProfileId,
                               INT4 *pRetValFsGroupWLANRowStatus)
{
    tApGroupConfigWLANEntry *pApGroupWLANConfigEntry = NULL;
    if (ApGroupGetConfigWLANEntry
        ((UINT2) i4GroupIndex, (UINT2) u4CapwapBaseWtpProfileId,
         &pApGroupWLANConfigEntry) == FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "ApGroupGetConfigEntry " " returned Failure \n "));
        return FAILURE;
    }

    *pRetValFsGroupWLANRowStatus =
        pApGroupWLANConfigEntry->i4ApGroupWLANRowstatus;
    return SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgDeleteApGroup
 * Input       :  The Indices
 *
 *                i4GroupIndex
 *
 * Descritpion :  This Routine will Get
 *                the Value accordingly.
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 * *******************************************************************************/
INT1
WsscfgDeleteApGroup (UINT4 u4GroupIndex)
{
    tApGroupConfigEntry *pApGroupConfigEntry = NULL;
    tApGroupConfigWTPEntry *pApGroupFirstConfigWTPEntry = NULL;
    tApGroupConfigWTPEntry *pApGroupNextConfigWTPEntry = NULL;
    tApGroupConfigWLANEntry *pApGroupFirstConfigWLANEntry = NULL;
    tApGroupConfigWLANEntry *pApGroupNextConfigWLANEntry = NULL;
    UINT1               i1RetStatus = 0;

    if (ApGroupGetConfigEntry ((UINT2) u4GroupIndex,
                               &pApGroupConfigEntry) == FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "ApGroupGetConfigEntry " " returned Failure \n "));
        return FAILURE;
    }

    /* Traverse and remove the WTP Entries */
    if (ApGroupGetFirstConfigWTPEntry (&pApGroupFirstConfigWTPEntry) == FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     " None of the AP Profiles are configured\n"));
    }
    else
    {
        do
        {
            pApGroupNextConfigWTPEntry = pApGroupFirstConfigWTPEntry;
            if (pApGroupNextConfigWTPEntry->u2ApGroupId == (UINT2) u4GroupIndex)
            {
                i1RetStatus =
                    nmhSetFsApGroupWTPRowStatus ((UINT4)
                                                 pApGroupNextConfigWTPEntry->
                                                 u2ApGroupWTPIndex,
                                                 (UINT4)
                                                 pApGroupNextConfigWTPEntry->
                                                 u2ApGroupId, (INT4) DESTROY);
                if (i1RetStatus != SNMP_SUCCESS)
                {
                    i1RetStatus = OSIX_FAILURE;
                }
            }
        }
        while (ApGroupGetNextConfigWTPEntry (pApGroupNextConfigWTPEntry,
                                             &pApGroupFirstConfigWTPEntry) ==
               SUCCESS);
    }

    if (ApGroupGetFirstConfigWLANEntry (&pApGroupFirstConfigWLANEntry) ==
        FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     " None of the WLAN Profiles are confiugred"
                     " in AP Group  "));
    }
    else
    {
        do
        {
            pApGroupNextConfigWLANEntry = pApGroupFirstConfigWLANEntry;

            if (pApGroupNextConfigWLANEntry->u2ApGroupId ==
                (UINT2) u4GroupIndex)
            {
                if ((i1RetStatus = nmhSetFsApGroupWLANRowStatus
                     (pApGroupNextConfigWLANEntry->u2ApGroupId,
                      pApGroupNextConfigWLANEntry->u4WlanId,
                      DESTROY)) != SNMP_SUCCESS)
                {
                    i1RetStatus = 1;
                }
            }
        }
        while (ApGroupGetNextWLANConfigEntry (pApGroupNextConfigWLANEntry,
                                              &pApGroupFirstConfigWLANEntry) ==
               SUCCESS);
    }

    /* Remove the Group from RBTree */
    if (RBTreeRemove (gApGroupGlobals.ApGroupConfigEntryData,
                      pApGroupConfigEntry) == RB_FAILURE)
    {
        return FAILURE;
    }

    if (APGROUP_MEM_RELEASE_MEM_BLK (APGROUP_CONFIG_ENTRY_MEMPOOL_ID,
                                     (UINT1 *) pApGroupConfigEntry) ==
        MEM_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "ApGroupMemRelaeseBlock " " returned Failure \n "));
        return FAILURE;
    }
    if (gApGroupGlobals.u4NoOfAPGroups)
    {
        gApGroupGlobals.u4NoOfAPGroups--;
    }
    return SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgDeleteApGroupWTP
* Input       :  The Indices
*
*                u4GroupWTPIndex
*                
*
* Descritpion :  This Routine will Get
*                the Value accordingly.
* Returns     :  OSIX_SUCCESS or OSIX_FAILURE
*******************************************************************************/
INT1
WsscfgDeleteApGroupWTP (UINT4 u4GroupId, UINT4 u4GroupWTPIndex)
{
    tApGroupConfigWTPEntry *pApGroupConfigWTPEntry = NULL;

    tApGroupConfigWLANEntry *pApGroupFirstConfigWLANEntry = NULL;
    tApGroupConfigWLANEntry *pApGroupNextConfigWLANEntry = NULL;

    UINT4               u4ApGroupEnable = 0;
    UINT4               u4RadioPolicy = 0;
    UINT4               u4AdminStatus = DESTROY;

    if (ApGroupGetWTPConfigEntry ((UINT2) u4GroupId,
                                  (UINT2) u4GroupWTPIndex,
                                  &pApGroupConfigWTPEntry) == FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "ApGroupGetWTPConfigEntry " " returned Failure \n "));
        return FAILURE;
    }

    /* Clear the bindings */
    WsscfgGetApGroupEnable (&u4ApGroupEnable);
    if (u4ApGroupEnable == CLI_APGROUP_ENABLE)
    {
        if (ApGroupGetFirstConfigWLANEntry
            (&pApGroupFirstConfigWLANEntry) == FAILURE)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         " None of the WLAN Profiles are confiugred"
                         " in AP Group  "));
        }
        else
        {
            do
            {
                pApGroupNextConfigWLANEntry = pApGroupFirstConfigWLANEntry;
                if (pApGroupNextConfigWLANEntry->u2ApGroupId == u4GroupId)
                {
                    WsscfgGetApGroupRadioPolicy (u4GroupId, &u4RadioPolicy);
                    WsscfgApGroupBinding (u4GroupWTPIndex,
                                          pApGroupNextConfigWLANEntry->u4WlanId,
                                          u4RadioPolicy, &u4AdminStatus);
                }
            }
            while (ApGroupGetNextWLANConfigEntry
                   (pApGroupNextConfigWLANEntry,
                    &pApGroupFirstConfigWLANEntry) == SUCCESS);
        }
    }

    if (RBTreeRemove (gApGroupGlobals.ApGroupConfigWTPEntryData,
                      pApGroupConfigWTPEntry) == RB_FAILURE)
    {
        return FAILURE;
    }

    if (APGROUPWTP_MEM_RELEASE_MEM_BLK (APGROUP_CONFIG_WTP_ENTRY_MEMPOOL_ID,
                                        (UINT1 *) pApGroupConfigWTPEntry) ==
        MEM_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "ApGroupMemRelaeseBlock " " returned Failure \n "));
        return FAILURE;
    }
    return SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgDeleteApGroupInterfaceMapping
 * Input       :  The Indices
 *
 *                 u4ApGroupId
 *
 * Descritpion :  This Routine will Get
 *                the Value accordingly.
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ***************************************************************************/
INT1
WsscfgDeleteApGroupInterfaceMapping (UINT4 u4ApGroupId)
{
    tApGroupConfigEntry *pApGroupConfigEntry = NULL;
    if (ApGroupGetConfigEntry ((UINT2) u4ApGroupId,
                               &pApGroupConfigEntry) == FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "ApGroupGetConfigEntry " " returned Failure \n "));
        return FAILURE;
    }
    if (pApGroupConfigEntry->u2InterfaceVlan != 0)
    {
        pApGroupConfigEntry->u2InterfaceVlan = 0;
    }
    return SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgGetApGroupNameDescription
 * Input       :  The Indices
 *
 *                u4GroupIndex
 *                pRetValApGroupNameDescription
 *
 * Descritpion :  This Routine will Get
 *                the Value accordingly.
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *******************************************************************************/
INT1
WsscfgGetApGroupNameDescription (UINT4 u4GroupIndex,
                                 tSNMP_OCTET_STRING_TYPE
                                 * pRetValGroupNameDescription)
{
    tApGroupConfigEntry *pApGroupConfigEntry = NULL;

    if (ApGroupGetConfigEntry ((UINT2) u4GroupIndex,
                               &pApGroupConfigEntry) == FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "ApGroupGetConfigEntry " " returned Failure \n "));
        return FAILURE;
    }

    MEMCPY (pRetValGroupNameDescription->pu1_OctetList,
            pApGroupConfigEntry->au1ApGroupNameDes,
            pApGroupConfigEntry->u4GroupNameDesLen);

    pRetValGroupNameDescription->i4_Length =
        pApGroupConfigEntry->u4GroupNameDesLen;

    return SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgSetApGroupNameDescription
* Input       :  The Indices
*
*                u4GroupIndex
*                pSetValGroupNameDescription
* Descritpion :  This Routine will Get
*                the Value accordingly.
* Returns     :  OSIX_SUCCESS or OSIX_FAILURE
******************************************************************************/
INT1
WsscfgSetApGroupNameDescription (UINT4 u4GroupIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValGroupNameDescription)
{
    tApGroupConfigEntry *pApGroupConfigEntry = NULL;
    INT4                i4NameDescriptionLen;

    if (ApGroupGetConfigEntry ((UINT2) u4GroupIndex,
                               &pApGroupConfigEntry) == FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "ApGroupGetConfigEntry " " returned Failure \n "));
        return FAILURE;
    }

    i4NameDescriptionLen = pSetValGroupNameDescription->i4_Length;

    MEMSET (pApGroupConfigEntry->au1ApGroupNameDes, 0,
            sizeof (pApGroupConfigEntry->au1ApGroupNameDes));

    MEMCPY (pApGroupConfigEntry->au1ApGroupNameDes,
            pSetValGroupNameDescription->pu1_OctetList, i4NameDescriptionLen);

    pApGroupConfigEntry->u4GroupNameDesLen = i4NameDescriptionLen;
    return SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgGetApGroupRadioPolicy
 * Input       :  The Indices
 *
 *                u4GroupIndex
 *                pRetValRadioPolicy
 * Descritpion :  This Routine will Get
 *                the Value accordingly.
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ******************************************************************************/
INT1
WsscfgGetApGroupRadioPolicy (UINT4 u4GroupIndex, UINT4 *pRetValRadioPolicy)
{
    tApGroupConfigEntry *pApGroupConfigEntry = NULL;
    if (ApGroupGetConfigEntry ((UINT2) u4GroupIndex,
                               &pApGroupConfigEntry) == FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "ApGroupGetConfigEntry " " returned Failure \n "));
        return FAILURE;
    }
    *pRetValRadioPolicy = (UINT4) pApGroupConfigEntry->u2RadioPolicy;
    return SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgSetApGroupRadioPolicy
 * Input       :  The Indices
 *
 *                u4GroupIndex
 *                pSetValRadioPolicy
 * Descritpion :  This Routine will Set
 *                the Value accordingly.
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ******************************************************************************/
INT1
WsscfgSetApGroupRadioPolicy (UINT4 u4GroupIndex, UINT4 *pSetValRadioPolicy)
{
    tApGroupConfigEntry *pApGroupConfigEntry = NULL;
    UINT4               u4ApGroupEnable = 0;
    tApGroupConfigWTPEntry *pApGroupFirstConfigWTPEntry = NULL;
    tApGroupConfigWTPEntry *pApGroupNextConfigWTPEntry = NULL;

    if (ApGroupGetConfigEntry ((UINT2) u4GroupIndex,
                               &pApGroupConfigEntry) == FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "ApGroupGetConfigEntry " " returned Failure \n "));
        return FAILURE;
    }

    WsscfgGetApGroupEnable (&u4ApGroupEnable);
    if (u4ApGroupEnable == CLI_APGROUP_ENABLE)
    {
        if (ApGroupGetFirstConfigWTPEntry
            (&pApGroupFirstConfigWTPEntry) == FAILURE)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         " None of the AP Profiles are configured\n"));
        }
        else
        {
            do
            {
                pApGroupNextConfigWTPEntry = pApGroupFirstConfigWTPEntry;
                if (pApGroupNextConfigWTPEntry->u2ApGroupId == u4GroupIndex)
                {
                    return FAILURE;
                }

            }
            while (ApGroupGetNextConfigWTPEntry (pApGroupNextConfigWTPEntry,
                                                 &pApGroupFirstConfigWTPEntry)
                   == SUCCESS);
        }
    }
    pApGroupConfigEntry->u2RadioPolicy = *pSetValRadioPolicy;
    return SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgSetApGroupInterfaceVlan
 *
 * Input       :  The Indices
 *                GroupIndex
 *                pSetValinterfaceVlan
 *
 * Descritpion :  This Routine will Set
 *                the Value accordingly.
 *
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ******************************************************************************/
INT1
WsscfgSetApGroupInterfaceVlan (UINT4 u4GroupIndex, UINT4 u4SetValInterfaceVlan)
{
    tApGroupConfigEntry *pApGroupConfigEntry = NULL;
    UINT4               u4ApGroupEnable = 0;

    if (ApGroupGetConfigEntry ((UINT2) u4GroupIndex,
                               &pApGroupConfigEntry) == FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "ApGroupGetConfigEntry " " returned Failure \n "));
        return FAILURE;
    }

    WsscfgGetApGroupEnable (&u4ApGroupEnable);
    if (u4ApGroupEnable == CLI_APGROUP_ENABLE)
    {
        if (pApGroupConfigEntry->u2InterfaceVlan != 0)
        {
            return FAILURE;
        }
    }
    pApGroupConfigEntry->u2InterfaceVlan = (UINT2) u4SetValInterfaceVlan;
    return SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgGetApGroupInterfaceVlan
 *
 * Input       :  The Indices
 *                u4GroupIndex
 *                pRetValInterfaceVlan
 *
 * Descritpion :  This Routine will Get
 *                the Value accordingly.
 *
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ******************************************************************************/
INT1
WsscfgGetApGroupInterfaceVlan (UINT4 u4GroupIndex, UINT4 *pRetValInterfaceVlan)
{
    tApGroupConfigEntry *pApGroupConfigEntry = NULL;
    if (ApGroupGetConfigEntry ((UINT2) u4GroupIndex,
                               &pApGroupConfigEntry) == FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "ApGroupGetConfigEntry " " returned Failure \n "));
        return FAILURE;
    }
    *pRetValInterfaceVlan = (UINT4) pApGroupConfigEntry->u2InterfaceVlan;
    return SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11aNetworkEnable
 Input       :  The Indices
 Input       :  pi4FsDot11aNetworkEnable
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetFsDot11aNetworkEnable (INT4 *pi4FsDot11aNetworkEnable)
{
    *pi4FsDot11aNetworkEnable =
        gWsscfgGlobals.WsscfgGlbMib.i4FsDot11aNetworkEnable;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetFsDot11aNetworkEnable
 Input       :  i4FsDot11aNetworkEnable
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgSetFsDot11aNetworkEnable (INT4 i4FsDot11aNetworkEnable)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    UINT1               u4RadioType = CLI_RADIO_TYPEA;

    gWsscfgGlobals.WsscfgGlbMib.i4FsDot11aNetworkEnable =
        i4FsDot11aNetworkEnable;

    nmhSetCmnNew (FsDot11aNetworkEnable, 0, WsscfgMainTaskLock,
                  WsscfgMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gWsscfgGlobals.WsscfgGlbMib.i4FsDot11aNetworkEnable);

    if (WssCfgNetworkStatusChange
        (i4FsDot11aNetworkEnable, u4RadioType) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11bNetworkEnable
 Input       :  The Indices
 Input       :  pi4FsDot11bNetworkEnable
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetFsDot11bNetworkEnable (INT4 *pi4FsDot11bNetworkEnable)
{
    *pi4FsDot11bNetworkEnable =
        gWsscfgGlobals.WsscfgGlbMib.i4FsDot11bNetworkEnable;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetFsDot11bNetworkEnable
 Input       :  i4FsDot11bNetworkEnable
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgSetFsDot11bNetworkEnable (INT4 i4FsDot11bNetworkEnable)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    UINT1               u4RadioType = CLI_RADIO_TYPEB;

    gWsscfgGlobals.WsscfgGlbMib.i4FsDot11bNetworkEnable =
        i4FsDot11bNetworkEnable;

    nmhSetCmnNew (FsDot11bNetworkEnable, 0, WsscfgMainTaskLock,
                  WsscfgMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gWsscfgGlobals.WsscfgGlbMib.i4FsDot11bNetworkEnable);
    if (WssCfgNetworkStatusChange
        (i4FsDot11bNetworkEnable, u4RadioType) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11gSupport
 Input       :  The Indices
 Input       :  pi4FsDot11gSupport
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetFsDot11gSupport (INT4 *pi4FsDot11gSupport)
{
    *pi4FsDot11gSupport = gWsscfgGlobals.WsscfgGlbMib.i4FsDot11gSupport;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetFsDot11gSupport
 Input       :  i4FsDot11gSupport
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgSetFsDot11gSupport (INT4 i4FsDot11gSupport)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    UINT4               u4RadioType = 0;

    if (gWsscfgGlobals.WsscfgGlbMib.i4FsDot11gSupport == i4FsDot11gSupport)
    {
        return OSIX_SUCCESS;
    }
    gWsscfgGlobals.WsscfgGlbMib.i4FsDot11gSupport = i4FsDot11gSupport;

    nmhSetCmnNew (FsDot11gSupport, 0, WsscfgMainTaskLock,
                  WsscfgMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gWsscfgGlobals.WsscfgGlbMib.i4FsDot11gSupport);

    if (i4FsDot11gSupport == OSIX_TRUE)
    {
        u4RadioType = CLI_RADIO_TYPEBG;
    }
    else
    {
        u4RadioType = CLI_RADIO_TYPEB;
    }

    if (WssCfgSetFsDot11RadioTypeTable (u4RadioType) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11anSupport
 Input       :  The Indices
 Input       :  pi4FsDot11anSupport
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetFsDot11anSupport (INT4 *pi4FsDot11anSupport)
{
    *pi4FsDot11anSupport = gWsscfgGlobals.WsscfgGlbMib.i4FsDot11anSupport;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetFsDot11anSupport
 Input       :  i4FsDot11anSupport
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgSetFsDot11anSupport (INT4 i4FsDot11anSupport)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    gWsscfgGlobals.WsscfgGlbMib.i4FsDot11anSupport = i4FsDot11anSupport;

    nmhSetCmnNew (FsDot11anSupport, 0, WsscfgMainTaskLock,
                  WsscfgMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gWsscfgGlobals.WsscfgGlbMib.i4FsDot11anSupport);
    if (i4FsDot11anSupport == OSIX_TRUE)
    {
        if (WssCfgSetFsDot11RadioTypeTable (CLI_RADIO_TYPEAN) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (WssCfgSetFsDot11RadioTypeTable (CLI_RADIO_TYPEA) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11bnSupport
 Input       :  The Indices
 Input       :  pi4FsDot11bnSupport
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetFsDot11bnSupport (INT4 *pi4FsDot11bnSupport)
{
    *pi4FsDot11bnSupport = gWsscfgGlobals.WsscfgGlbMib.i4FsDot11bnSupport;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetFsDot11bnSupport
 Input       :  i4FsDot11bnSupport
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgSetFsDot11bnSupport (INT4 i4FsDot11bnSupport)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    INT4                i4FsDot11gSupport = 0;

    gWsscfgGlobals.WsscfgGlbMib.i4FsDot11bnSupport = i4FsDot11bnSupport;

    nmhSetCmnNew (FsDot11bnSupport, 0, WsscfgMainTaskLock,
                  WsscfgMainTaskUnLock, 0, 0, 0, i4SetOption, "%i",
                  gWsscfgGlobals.WsscfgGlbMib.i4FsDot11bnSupport);
    if (WsscfgGetFsDot11gSupport (&i4FsDot11gSupport) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (i4FsDot11gSupport == OSIX_TRUE)
    {
        if (i4FsDot11bnSupport == OSIX_TRUE)
        {
            if (WssCfgSetFsDot11RadioTypeTable (CLI_RADIO_TYPEBGN) !=
                OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
        }
        else
        {
            if (WssCfgSetFsDot11RadioTypeTable (CLI_RADIO_TYPEBG) !=
                OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
        }
    }
    else
    {
        if (WssCfgSetFsDot11RadioTypeTable (CLI_RADIO_TYPEB) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11ManagmentSSID
 Input       :  The Indices
 Input       :  pu4FsDot11ManagmentSSID
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetFsDot11ManagmentSSID (UINT4 *pu4FsDot11ManagmentSSID)
{
    *pu4FsDot11ManagmentSSID =
        (UINT4) (gWsscfgGlobals.WsscfgGlbMib.u2FsDot11ManagmentSSID);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetFsDot11ManagmentSSID
 Input       :  u4FsDot11ManagmentSSID
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgSetFsDot11ManagmentSSID (UINT4 u4FsDot11ManagmentSSID)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    /* set in WLAN DB */
    UINT1               au1ManagmentSSID[WSSWLAN_SSID_NAME_LEN];
#ifdef WLC_WANTED
    INT4                i4WlanIfIndex = 0;
#endif
    MEMSET (au1ManagmentSSID, 0, WSSWLAN_SSID_NAME_LEN);

#ifdef WLC_WANTED
    if (nmhGetCapwapDot11WlanProfileIfIndex
        (u4FsDot11ManagmentSSID, &i4WlanIfIndex) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    WssWlanGetSSIDfromIfIndex ((UINT4) i4WlanIfIndex, au1ManagmentSSID);
    if (WssWlanSetManagmentSSID (au1ManagmentSSID) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }
/* Updating CLI DB */
#endif /* WLC_WANTED */
    gWsscfgGlobals.WsscfgGlbMib.u2FsDot11ManagmentSSID =
        (UINT2) u4FsDot11ManagmentSSID;

    nmhSetCmnNew (FsDot11ManagmentSSID, 0, WsscfgMainTaskLock,
                  WsscfgMainTaskUnLock, 0, 0, 0, i4SetOption, "%u",
                  gWsscfgGlobals.WsscfgGlbMib.u2FsDot11ManagmentSSID);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11CountryString
 Input       :  The Indices
 Input       :  pFsDot11CountryString
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetFsDot11CountryString (UINT1 *pFsDot11CountryString)
{
    MEMCPY (pFsDot11CountryString,
            gWsscfgGlobals.WsscfgGlbMib.au1FsDot11CountryString,
            gWsscfgGlobals.WsscfgGlbMib.i4FsDot11CountryStringLen);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetFsDot11CountryString
 Input       :  pFsDot11CountryString
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgSetFsDot11CountryString (UINT1 *pFsDot11CountryString)
{
    INT4                i4SetOption = SNMP_SUCCESS;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4CurrentProfileId = 0;
    UINT4               u4ErrorCode = 0;
    tCapwapFsCapwapWtpConfigEntry CapwapSetFsCapwapWtpConfigEntry;
    tCapwapIsSetFsCapwapWtpConfigEntry CapwapIsSetFsCapwapWtpConfigEntry;
    tSNMP_OCTET_STRING_TYPE CountryString;
#ifdef WLC_WANTED
#ifdef RFMGMT_WANTED
    tSNMP_OCTET_STRING_TYPE UnusedChannels;
    UINT1               au1TmpChannels[RFMGMT_MAX_CHANNEL];
    UINT1               au1UnusedChannels[RFMGMT_MAX_CHANNEL];
    UINT2               u2Channel = 0;
    UINT4               u4Index = 0;
    UINT1               u1Flag = OSIX_FALSE;
    UINT1               u1Index = 0;

    MEMSET (au1TmpChannels, 0, RFMGMT_MAX_CHANNEL);
    MEMSET (au1UnusedChannels, 0, RFMGMT_MAX_CHANNEL);
    MEMSET (&UnusedChannels, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    UnusedChannels.pu1_OctetList = au1UnusedChannels;
#endif
#endif

    MEMSET (&CountryString, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&CapwapSetFsCapwapWtpConfigEntry, 0,
            sizeof (tCapwapFsCapwapWtpConfigEntry));
    MEMSET (&CapwapIsSetFsCapwapWtpConfigEntry, 0,
            sizeof (tCapwapIsSetFsCapwapWtpConfigEntry));

    CountryString.pu1_OctetList =
        gWsscfgGlobals.WsscfgGlbMib.au1FsDot11CountryString;

    CountryString.i4_Length = RADIO_COUNTRY_LENGTH;

    MEMCPY (gWsscfgGlobals.WsscfgGlbMib.au1FsDot11CountryString,
            pFsDot11CountryString, RADIO_COUNTRY_LENGTH);

    gWsscfgGlobals.WsscfgGlbMib.i4FsDot11CountryStringLen =
        RADIO_COUNTRY_LENGTH;

    nmhSetCmnNew (FsDot11CountryString, 0, WsscfgMainTaskLock,
                  WsscfgMainTaskUnLock, 0, 0, 0, i4SetOption, "%s",
                  &CountryString);

    if (nmhGetFirstIndexCapwapBaseWtpProfileTable (&u4WtpProfileId) !=
        SNMP_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    MEMCPY (CapwapSetFsCapwapWtpConfigEntry.MibObject.
            au1FsWtpCountryString, pFsDot11CountryString, RADIO_COUNTRY_LENGTH);
    CapwapSetFsCapwapWtpConfigEntry.MibObject.
        i4FsWtpCountryStringLen = RADIO_COUNTRY_LENGTH;
    CapwapSetFsCapwapWtpConfigEntry.
        MibObject.i4FsCapwapWtpConfigRowStatus = ACTIVE;
    CapwapIsSetFsCapwapWtpConfigEntry.bFsWtpCountryString = OSIX_TRUE;
    CapwapIsSetFsCapwapWtpConfigEntry.bCapwapBaseWtpProfileId = OSIX_TRUE;
    CapwapIsSetFsCapwapWtpConfigEntry.bFsCapwapWtpConfigRowStatus = OSIX_TRUE;

    do
    {
        u4CurrentProfileId = u4WtpProfileId;
        CapwapSetFsCapwapWtpConfigEntry.
            MibObject.u4CapwapBaseWtpProfileId = u4WtpProfileId;
        if (CapwapTestAllFsCapwapWtpConfigTable
            (&u4ErrorCode, &CapwapSetFsCapwapWtpConfigEntry,
             &CapwapIsSetFsCapwapWtpConfigEntry, OSIX_TRUE,
             OSIX_TRUE) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        if (CapwapSetAllFsCapwapWtpConfigTable
            (&CapwapSetFsCapwapWtpConfigEntry,
             &CapwapIsSetFsCapwapWtpConfigEntry, OSIX_TRUE,
             OSIX_TRUE) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable
           (u4CurrentProfileId, &u4WtpProfileId));

#ifdef WLC_WANTED
#ifdef RFMGMT_WANTED

    for (u1Index = 0; u1Index < RADIO_EXCEPTION_MAX; u1Index++)
    {
        if (STRCMP (pFsDot11CountryString,
                    gRadioIfExcList[u1Index].au1CountryStr) == 0)
        {
            break;
        }
    }

    if (u1Index != RADIO_EXCEPTION_MAX)
    {
        if (nmhGetFsRrmUnusedChannels (RFMGMT_RADIO_TYPEB,
                                       &UnusedChannels) == SNMP_SUCCESS)
        {
            for (u2Channel = 0; u2Channel < RFMGMT_MAX_CHANNELB; u2Channel++)
            {
                if (gRadioIfExcList[u1Index].
                    au1Dot11bChannelExcList[u2Channel] == 0)
                {
                    au1TmpChannels[u2Channel] =
                        gau1Dot11bRegDomainChannelList[u2Channel];
                }
            }
            MEMCPY (UnusedChannels.pu1_OctetList, au1TmpChannels,
                    UnusedChannels.i4_Length);
            nmhSetFsRrmUnusedChannels (RFMGMT_RADIO_TYPEB, &UnusedChannels);
        }
        MEMSET (au1TmpChannels, 0, RFMGMT_MAX_CHANNEL);
        MEMSET (au1UnusedChannels, 0, RFMGMT_MAX_CHANNEL);
        if (nmhGetFsRrmUnusedChannels (RFMGMT_RADIO_TYPEG,
                                       &UnusedChannels) == SNMP_SUCCESS)
        {
            for (u2Channel = 0; u2Channel < RFMGMT_MAX_CHANNELB; u2Channel++)
            {
                if (gRadioIfExcList[u1Index].
                    au1Dot11bChannelExcList[u2Channel] == 0)
                {
                    au1TmpChannels[u2Channel] =
                        gau1Dot11bRegDomainChannelList[u2Channel];
                }
            }
            MEMCPY (UnusedChannels.pu1_OctetList, au1TmpChannels,
                    UnusedChannels.i4_Length);
            nmhSetFsRrmUnusedChannels (RFMGMT_RADIO_TYPEG, &UnusedChannels);
        }
        MEMSET (au1TmpChannels, 0, RFMGMT_MAX_CHANNEL);
        MEMSET (au1UnusedChannels, 0, RFMGMT_MAX_CHANNEL);
        if (nmhGetFsRrmUnusedChannels (RFMGMT_RADIO_TYPEA,
                                       &UnusedChannels) == SNMP_SUCCESS)
        {
            for (u2Channel = 0; u2Channel < RFMGMT_MAX_CHANNELA; u2Channel++)
            {
                if (gRadioIfExcList[u1Index].
                    au1Dot11aChannelExcList[u2Channel] == 0)
                {
                    au1TmpChannels[u2Channel] =
                        gau1Dot11aRegDomainChannelList[u2Channel];
                }
            }
            MEMCPY (UnusedChannels.pu1_OctetList, au1TmpChannels,
                    UnusedChannels.i4_Length);
            nmhSetFsRrmUnusedChannels (RFMGMT_RADIO_TYPEA, &UnusedChannels);
        }
    }
    else
    {
        for (u4Index = 0; u4Index < RADIOIF_SUPPORTED_COUNTRY_LIST; u4Index++)
        {
            if (STRCMP (pFsDot11CountryString,
                        gWsscfgSupportedCountryList[u4Index].au1CountryCode) ==
                0)
            {
                u4Index = (UINT4) gRegDomainCountryMapping[u4Index].u2RegDomain;
                break;
            }
        }
        if (STRCMP (pFsDot11CountryString, RADIOIF_COUNTRY_CODE_JAPAN) == 0)
        {
            u1Flag = OSIX_TRUE;
        }

        if (u4Index != RADIOIF_SUPPORTED_COUNTRY_LIST)
        {
            if (nmhGetFsRrmUnusedChannels (RFMGMT_RADIO_TYPEB,
                                           &UnusedChannels) == SNMP_SUCCESS)
            {
                for (u2Channel = 0; u2Channel < RFMGMT_MAX_CHANNELB;
                     u2Channel++)
                {
                    if (gau1Dot11bChannelRegDomainCheckList[u4Index][u2Channel]
                        == 0)
                    {
                        au1TmpChannels[u2Channel] =
                            gau1Dot11bRegDomainChannelList[u2Channel];
                    }
                }
                MEMCPY (UnusedChannels.pu1_OctetList, au1TmpChannels,
                        UnusedChannels.i4_Length);
                nmhSetFsRrmUnusedChannels (RFMGMT_RADIO_TYPEB, &UnusedChannels);
            }
            MEMSET (au1TmpChannels, 0, RFMGMT_MAX_CHANNEL);
            MEMSET (au1UnusedChannels, 0, RFMGMT_MAX_CHANNEL);
            if (nmhGetFsRrmUnusedChannels (RFMGMT_RADIO_TYPEG,
                                           &UnusedChannels) == SNMP_SUCCESS)
            {
                for (u2Channel = 0; u2Channel < RFMGMT_MAX_CHANNELB;
                     u2Channel++)
                {
                    if (gau1Dot11bChannelRegDomainCheckList[u4Index][u2Channel]
                        == 0)
                    {
                        au1TmpChannels[u2Channel] =
                            gau1Dot11bRegDomainChannelList[u2Channel];
                    }
                }
                MEMCPY (UnusedChannels.pu1_OctetList, au1TmpChannels,
                        UnusedChannels.i4_Length);
                nmhSetFsRrmUnusedChannels (RFMGMT_RADIO_TYPEG, &UnusedChannels);
            }
            MEMSET (au1TmpChannels, 0, RFMGMT_MAX_CHANNEL);
            MEMSET (au1UnusedChannels, 0, RFMGMT_MAX_CHANNEL);
            if (nmhGetFsRrmUnusedChannels (RFMGMT_RADIO_TYPEA,
                                           &UnusedChannels) == SNMP_SUCCESS)
            {
                for (u2Channel = 0; u2Channel < RFMGMT_MAX_CHANNELA;
                     u2Channel++)
                {
                    if ((gau1Dot11aChannelCheckList[u2Channel] == 0) &&
                        (u1Flag == OSIX_FALSE))
                    {
                        au1TmpChannels[u2Channel] =
                            gau1Dot11aRegDomainChannelList[u2Channel];
                    }
                }
                MEMCPY (UnusedChannels.pu1_OctetList, au1TmpChannels,
                        UnusedChannels.i4_Length);
                nmhSetFsRrmUnusedChannels (RFMGMT_RADIO_TYPEA, &UnusedChannels);
            }
        }
    }
#endif
#endif

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFsSecurityWebAuthType
 Input       :  The Indices
 Input       :  pi4FsSecurityWebAuthType
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetFsSecurityWebAuthType (INT4 *pi4FsSecurityWebAuthType)
{
    *pi4FsSecurityWebAuthType =
        gWsscfgGlobals.WsscfgGlbMib.i4FsSecurityWebAuthType;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsDot11StationConfigTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsDot11StationConfigEntry or NULL
****************************************************************************/
tWsscfgFsDot11StationConfigEntry *
WsscfgGetFirstFsDot11StationConfigTable ()
{
    tWsscfgFsDot11StationConfigEntry *pWsscfgFsDot11StationConfigEntry = NULL;

    pWsscfgFsDot11StationConfigEntry =
        (tWsscfgFsDot11StationConfigEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.FsDot11StationConfigTable);

    return pWsscfgFsDot11StationConfigEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsDot11StationConfigTable
 Input       :  pCurrentWsscfgFsDot11StationConfigEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsDot11StationConfigEntry or NULL
****************************************************************************/
tWsscfgFsDot11StationConfigEntry
    * WsscfgGetNextFsDot11StationConfigTable (tWsscfgFsDot11StationConfigEntry *
                                              pCurrentWsscfgFsDot11StationConfigEntry)
{
    tWsscfgFsDot11StationConfigEntry
        * pNextWsscfgFsDot11StationConfigEntry = NULL;

    pNextWsscfgFsDot11StationConfigEntry =
        (tWsscfgFsDot11StationConfigEntry *)
        RBTreeGetNext
        (gWsscfgGlobals.WsscfgGlbMib.FsDot11StationConfigTable,
         (tRBElem *) pCurrentWsscfgFsDot11StationConfigEntry, NULL);

    return pNextWsscfgFsDot11StationConfigEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11StationConfigTable
 Input       :  pWsscfgFsDot11StationConfigEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsDot11StationConfigEntry or NULL
****************************************************************************/
tWsscfgFsDot11StationConfigEntry *
WsscfgGetFsDot11StationConfigTable (tWsscfgFsDot11StationConfigEntry *
                                    pWsscfgFsDot11StationConfigEntry)
{
    tWsscfgFsDot11StationConfigEntry
        * pGetWsscfgFsDot11StationConfigEntry = NULL;

    pGetWsscfgFsDot11StationConfigEntry =
        (tWsscfgFsDot11StationConfigEntry *)
        RBTreeGet
        (gWsscfgGlobals.WsscfgGlbMib.FsDot11StationConfigTable,
         (tRBElem *) pWsscfgFsDot11StationConfigEntry);

    return pGetWsscfgFsDot11StationConfigEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsDot11ExternalWebAuthProfileTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsDot11ExternalWebAuthProfileEntry or NULL
****************************************************************************/
INT4
WsscfgGetFirstFsDot11ExternalWebAuthProfileTable ()
{
    tWsscfgCapwapDot11WlanEntry *pWsscfgCapwapDot11WlanEntry = NULL;

    pWsscfgCapwapDot11WlanEntry = WsscfgGetFirstCapwapDot11WlanTable ();
    if (pWsscfgCapwapDot11WlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    return pWsscfgCapwapDot11WlanEntry->MibObject.
        i4CapwapDot11WlanProfileIfIndex;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsDot11ExternalWebAuthProfileTable
 Input       :  pCurrentWsscfgFsDot11ExternalWebAuthProfileEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsDot11ExternalWebAuthProfileEntry or NULL
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetNextFsDot11ExternalWebAuthProfileTable
    (tWsscfgFsDot11ExternalWebAuthProfileEntry *
     pCurrentWsscfgFsDot11ExternalWebAuthProfileEntry)
{
    tWsscfgCapwapDot11WlanEntry WsscfgCapwapDot11WlanEntry;
    tWsscfgCapwapDot11WlanEntry *pNextWsscfgCapwapDot11WlanEntry = NULL;
    tWssWlanDB          wssWlanDB;

    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&WsscfgCapwapDot11WlanEntry, 0,
            sizeof (tWsscfgCapwapDot11WlanEntry));

    wssWlanDB.WssWlanAttributeDB.u4WlanIfIndex =
        (UINT4) pCurrentWsscfgFsDot11ExternalWebAuthProfileEntry->MibObject.
        i4IfIndex;
    wssWlanDB.WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &wssWlanDB) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    WsscfgCapwapDot11WlanEntry.MibObject.u4CapwapDot11WlanProfileId =
        (UINT4) wssWlanDB.WssWlanAttributeDB.u2WlanProfileId;

    pNextWsscfgCapwapDot11WlanEntry =
        WsscfgGetNextCapwapDot11WlanTable (&WsscfgCapwapDot11WlanEntry);

    if (pNextWsscfgCapwapDot11WlanEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    /* Assign the next index */
    return pNextWsscfgCapwapDot11WlanEntry->MibObject.
        i4CapwapDot11WlanProfileIfIndex;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsDot11CapabilityProfileTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsDot11CapabilityProfileEntry or NULL
****************************************************************************/
tWsscfgFsDot11CapabilityProfileEntry
    * WsscfgGetFirstFsDot11CapabilityProfileTable ()
{
    tWsscfgFsDot11CapabilityProfileEntry
        * pWsscfgFsDot11CapabilityProfileEntry = NULL;

    pWsscfgFsDot11CapabilityProfileEntry =
        (tWsscfgFsDot11CapabilityProfileEntry *)
        RBTreeGetFirst (gWsscfgGlobals.
                        WsscfgGlbMib.FsDot11CapabilityProfileTable);

    return pWsscfgFsDot11CapabilityProfileEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsDot11CapabilityProfileTable
 Input       :  pCurrentWsscfgFsDot11CapabilityProfileEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsDot11CapabilityProfileEntry or NULL
****************************************************************************/
tWsscfgFsDot11CapabilityProfileEntry
    *
    WsscfgGetNextFsDot11CapabilityProfileTable
    (tWsscfgFsDot11CapabilityProfileEntry *
     pCurrentWsscfgFsDot11CapabilityProfileEntry)
{
    tWsscfgFsDot11CapabilityProfileEntry
        * pNextWsscfgFsDot11CapabilityProfileEntry = NULL;

    pNextWsscfgFsDot11CapabilityProfileEntry =
        (tWsscfgFsDot11CapabilityProfileEntry *)
        RBTreeGetNext (gWsscfgGlobals.
                       WsscfgGlbMib.FsDot11CapabilityProfileTable,
                       (tRBElem *)
                       pCurrentWsscfgFsDot11CapabilityProfileEntry, NULL);

    return pNextWsscfgFsDot11CapabilityProfileEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11CapabilityProfileTable
 Input       :  pWsscfgFsDot11CapabilityProfileEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsDot11CapabilityProfileEntry or NULL
****************************************************************************/
tWsscfgFsDot11CapabilityProfileEntry
    *
    WsscfgGetFsDot11CapabilityProfileTable
    (tWsscfgFsDot11CapabilityProfileEntry *
     pWsscfgFsDot11CapabilityProfileEntry)
{
    tWsscfgFsDot11CapabilityProfileEntry
        * pGetWsscfgFsDot11CapabilityProfileEntry = NULL;

    pGetWsscfgFsDot11CapabilityProfileEntry =
        (tWsscfgFsDot11CapabilityProfileEntry *)
        RBTreeGet
        (gWsscfgGlobals.WsscfgGlbMib.FsDot11CapabilityProfileTable,
         (tRBElem *) pWsscfgFsDot11CapabilityProfileEntry);

    return pGetWsscfgFsDot11CapabilityProfileEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsDot11AuthenticationProfileTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsDot11AuthenticationProfileEntry or NULL
****************************************************************************/
tWsscfgFsDot11AuthenticationProfileEntry
    * WsscfgGetFirstFsDot11AuthenticationProfileTable ()
{
    tWsscfgFsDot11AuthenticationProfileEntry
        * pWsscfgFsDot11AuthenticationProfileEntry = NULL;

    pWsscfgFsDot11AuthenticationProfileEntry =
        (tWsscfgFsDot11AuthenticationProfileEntry *)
        RBTreeGetFirst (gWsscfgGlobals.
                        WsscfgGlbMib.FsDot11AuthenticationProfileTable);

    return pWsscfgFsDot11AuthenticationProfileEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsDot11AuthenticationProfileTable
 Input       :  pCurrentWsscfgFsDot11AuthenticationProfileEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsDot11AuthenticationProfileEntry or NULL
****************************************************************************/
tWsscfgFsDot11AuthenticationProfileEntry
    *
    WsscfgGetNextFsDot11AuthenticationProfileTable
    (tWsscfgFsDot11AuthenticationProfileEntry *
     pCurrentWsscfgFsDot11AuthenticationProfileEntry)
{
    tWsscfgFsDot11AuthenticationProfileEntry
        * pNextWsscfgFsDot11AuthenticationProfileEntry = NULL;

    pNextWsscfgFsDot11AuthenticationProfileEntry =
        (tWsscfgFsDot11AuthenticationProfileEntry *)
        RBTreeGetNext (gWsscfgGlobals.
                       WsscfgGlbMib.FsDot11AuthenticationProfileTable,
                       (tRBElem *)
                       pCurrentWsscfgFsDot11AuthenticationProfileEntry, NULL);

    return pNextWsscfgFsDot11AuthenticationProfileEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11AuthenticationProfileTable
 Input       :  pWsscfgFsDot11AuthenticationProfileEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsDot11AuthenticationProfileEntry or NULL
****************************************************************************/
tWsscfgFsDot11AuthenticationProfileEntry
    *
    WsscfgGetFsDot11AuthenticationProfileTable
    (tWsscfgFsDot11AuthenticationProfileEntry *
     pWsscfgFsDot11AuthenticationProfileEntry)
{
    tWsscfgFsDot11AuthenticationProfileEntry
        * pGetWsscfgFsDot11AuthenticationProfileEntry = NULL;

    pGetWsscfgFsDot11AuthenticationProfileEntry =
        (tWsscfgFsDot11AuthenticationProfileEntry *)
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.FsDot11AuthenticationProfileTable,
                   (tRBElem *) pWsscfgFsDot11AuthenticationProfileEntry);

    return pGetWsscfgFsDot11AuthenticationProfileEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsSecurityWebAuthGuestInfoTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetFirstFsSecurityWebAuthGuestInfoTable (tSNMP_OCTET_STRING_TYPE *
                                               pFsSecurityWebAuthUName)
{
#ifdef WLC_WANTED

    tWssIfWebAuthDB    *pWssIfWebAuthDB = NULL;

    pWssIfWebAuthDB = (tWssIfWebAuthDB *) RBTreeGetFirst (gWssStaWebAuthDB);

    if (pWssIfWebAuthDB == NULL)
    {
        return OSIX_FAILURE;
    }
    pFsSecurityWebAuthUName->i4_Length = STRLEN (pWssIfWebAuthDB->au1UserName);
    MEMCPY (pFsSecurityWebAuthUName->pu1_OctetList,
            pWssIfWebAuthDB->au1UserName, pFsSecurityWebAuthUName->i4_Length);
#else
    UNUSED_PARAM (pFsSecurityWebAuthUName);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsSecurityWebAuthGuestInfoTable
 Input       :  pFsSecurityWebAuthUName
 Description :  This Routine is used to take
                next index from a table
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4                WsscfgGetNextFsSecurityWebAuthGuestInfoTable
    (tSNMP_OCTET_STRING_TYPE * pFsSecurityWebAuthUName,
     tSNMP_OCTET_STRING_TYPE * pNextFsSecurityWebAuthUName)
{
#ifdef WLC_WANTED
    tWssIfWebAuthDB     WssIfWebAuthDB;
    tWssIfWebAuthDB    *pNextWebAuthDB = NULL;

    MEMSET (&WssIfWebAuthDB, 0, sizeof (tWssIfWebAuthDB));
    /* assign index */
    MEMCPY (WssIfWebAuthDB.au1UserName,
            pFsSecurityWebAuthUName->pu1_OctetList,
            pFsSecurityWebAuthUName->i4_Length);

    /* look through radio-if database */
    pNextWebAuthDB =
        (tWssIfWebAuthDB *) RBTreeGetNext (gWssStaWebAuthDB,
                                           (tRBElem *) & WssIfWebAuthDB, NULL);
    if (pNextWebAuthDB == NULL)
    {
        return OSIX_FAILURE;
    }

    pNextFsSecurityWebAuthUName->i4_Length =
        (INT4) (STRLEN (pNextWebAuthDB->au1UserName));
    MEMCPY (pNextFsSecurityWebAuthUName->pu1_OctetList,
            pNextWebAuthDB->au1UserName,
            pNextFsSecurityWebAuthUName->i4_Length);

#else
    UNUSED_PARAM (pFsSecurityWebAuthUName);
    UNUSED_PARAM (pNextFsSecurityWebAuthUName);
#endif

    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgGetFirstFsStationQosParamsTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsStationQosParamsEntry or NULL
****************************************************************************/
tWsscfgFsStationQosParamsEntry *
WsscfgGetFirstFsStationQosParamsTable ()
{
    tWsscfgFsStationQosParamsEntry *pWsscfgFsStationQosParamsEntry = NULL;

    pWsscfgFsStationQosParamsEntry =
        (tWsscfgFsStationQosParamsEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.FsStationQosParamsTable);

    return pWsscfgFsStationQosParamsEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsStationQosParamsTable
 Input       :  pCurrentWsscfgFsStationQosParamsEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsStationQosParamsEntry or NULL
****************************************************************************/
tWsscfgFsStationQosParamsEntry *
WsscfgGetNextFsStationQosParamsTable (tWsscfgFsStationQosParamsEntry *
                                      pCurrentWsscfgFsStationQosParamsEntry)
{
    tWsscfgFsStationQosParamsEntry *pNextWsscfgFsStationQosParamsEntry = NULL;

    pNextWsscfgFsStationQosParamsEntry =
        (tWsscfgFsStationQosParamsEntry *)
        RBTreeGetNext
        (gWsscfgGlobals.WsscfgGlbMib.FsStationQosParamsTable,
         (tRBElem *) pCurrentWsscfgFsStationQosParamsEntry, NULL);

    return pNextWsscfgFsStationQosParamsEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFsStationQosParamsTable
 Input       :  pWsscfgFsStationQosParamsEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsStationQosParamsEntry or NULL
****************************************************************************/
tWsscfgFsStationQosParamsEntry *
WsscfgGetFsStationQosParamsTable (tWsscfgFsStationQosParamsEntry *
                                  pWsscfgFsStationQosParamsEntry)
{
    tWsscfgFsStationQosParamsEntry *pGetWsscfgFsStationQosParamsEntry = NULL;

    pGetWsscfgFsStationQosParamsEntry =
        (tWsscfgFsStationQosParamsEntry *)
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsStationQosParamsTable,
                   (tRBElem *) pWsscfgFsStationQosParamsEntry);

    return pGetWsscfgFsStationQosParamsEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsVlanIsolationTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsVlanIsolationEntry or NULL
****************************************************************************/
tWsscfgFsVlanIsolationEntry *
WsscfgGetFirstFsVlanIsolationTable ()
{
    tWsscfgFsVlanIsolationEntry *pWsscfgFsVlanIsolationEntry = NULL;

    pWsscfgFsVlanIsolationEntry =
        (tWsscfgFsVlanIsolationEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.FsVlanIsolationTable);

    return pWsscfgFsVlanIsolationEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsVlanIsolationTable
 Input       :  pCurrentWsscfgFsVlanIsolationEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsVlanIsolationEntry or NULL
****************************************************************************/
tWsscfgFsVlanIsolationEntry *
WsscfgGetNextFsVlanIsolationTable (tWsscfgFsVlanIsolationEntry *
                                   pCurrentWsscfgFsVlanIsolationEntry)
{
    tWsscfgFsVlanIsolationEntry *pNextWsscfgFsVlanIsolationEntry = NULL;

    pNextWsscfgFsVlanIsolationEntry =
        (tWsscfgFsVlanIsolationEntry *)
        RBTreeGetNext
        (gWsscfgGlobals.WsscfgGlbMib.FsVlanIsolationTable, (tRBElem *)
         pCurrentWsscfgFsVlanIsolationEntry, NULL);

    return pNextWsscfgFsVlanIsolationEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFsVlanIsolationTable
 Input       :  pWsscfgFsVlanIsolationEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsVlanIsolationEntry or NULL
****************************************************************************/
tWsscfgFsVlanIsolationEntry *
WsscfgGetFsVlanIsolationTable (tWsscfgFsVlanIsolationEntry *
                               pWsscfgFsVlanIsolationEntry)
{
    tWsscfgFsVlanIsolationEntry *pGetWsscfgFsVlanIsolationEntry = NULL;

    pGetWsscfgFsVlanIsolationEntry =
        (tWsscfgFsVlanIsolationEntry *)
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsVlanIsolationTable,
                   (tRBElem *) pWsscfgFsVlanIsolationEntry);

    return pGetWsscfgFsVlanIsolationEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsDot11RadioConfigTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsDot11RadioConfigEntry or NULL
****************************************************************************/
INT4
WsscfgGetFirstFsDot11RadioConfigTable (INT4 *pi4IfIndex)
{
    tRadioIfDB         *pRadioIfDB = NULL;

    /* look through radio-if database */
    pRadioIfDB =
        (tRadioIfDB *) RBTreeGetFirst (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB);
    if (pRadioIfDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4IfIndex = (INT4) (pRadioIfDB->u4VirtualRadioIfIndex);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsDot11RadioConfigTable
 Input       :  pCurrentWsscfgFsDot11RadioConfigEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsDot11RadioConfigEntry or NULL
****************************************************************************/
INT4
WsscfgGetNextFsDot11RadioConfigTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tRadioIfDB          RadioIfDB;
    tRadioIfDB         *pNextRadioIfDB = NULL;

    /* get the next radio-if */
    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));
    /* assign index */
    RadioIfDB.u4VirtualRadioIfIndex = (UINT4) i4IfIndex;
    /* look through radio-if database */
    pNextRadioIfDB =
        (tRadioIfDB *) RBTreeGetNext (gRadioIfGlobals.
                                      RadioIfGlbMib.RadioIfDB,
                                      (tRBElem *) & RadioIfDB, NULL);
    if (pNextRadioIfDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4NextIfIndex = (INT4) (pNextRadioIfDB->u4VirtualRadioIfIndex);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11RadioConfigTable
 Input       :  pWsscfgFsDot11RadioConfigEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsDot11RadioConfigEntry or NULL
****************************************************************************/
tWsscfgFsDot11RadioConfigEntry *
WsscfgGetFsDot11RadioConfigTable (tWsscfgFsDot11RadioConfigEntry *
                                  pWsscfgFsDot11RadioConfigEntry)
{
    tWsscfgFsDot11RadioConfigEntry *pGetWsscfgFsDot11RadioConfigEntry = NULL;

    pGetWsscfgFsDot11RadioConfigEntry =
        (tWsscfgFsDot11RadioConfigEntry *)
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsDot11RadioConfigTable,
                   (tRBElem *) pWsscfgFsDot11RadioConfigEntry);

    return pGetWsscfgFsDot11RadioConfigEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsDot11QosProfileTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsDot11QosProfileEntry or NULL
****************************************************************************/
tWsscfgFsDot11QosProfileEntry *
WsscfgGetFirstFsDot11QosProfileTable ()
{
    tWsscfgFsDot11QosProfileEntry *pWsscfgFsDot11QosProfileEntry = NULL;

    pWsscfgFsDot11QosProfileEntry =
        (tWsscfgFsDot11QosProfileEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.FsDot11QosProfileTable);

    return pWsscfgFsDot11QosProfileEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsDot11QosProfileTable
 Input       :  pCurrentWsscfgFsDot11QosProfileEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsDot11QosProfileEntry or NULL
****************************************************************************/
tWsscfgFsDot11QosProfileEntry *
WsscfgGetNextFsDot11QosProfileTable (tWsscfgFsDot11QosProfileEntry *
                                     pCurrentWsscfgFsDot11QosProfileEntry)
{
    tWsscfgFsDot11QosProfileEntry *pNextWsscfgFsDot11QosProfileEntry = NULL;

    pNextWsscfgFsDot11QosProfileEntry =
        (tWsscfgFsDot11QosProfileEntry *)
        RBTreeGetNext
        (gWsscfgGlobals.WsscfgGlbMib.FsDot11QosProfileTable,
         (tRBElem *) pCurrentWsscfgFsDot11QosProfileEntry, NULL);

    return pNextWsscfgFsDot11QosProfileEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11QosProfileTable
 Input       :  pWsscfgFsDot11QosProfileEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsDot11QosProfileEntry or NULL
****************************************************************************/
tWsscfgFsDot11QosProfileEntry *
WsscfgGetFsDot11QosProfileTable (tWsscfgFsDot11QosProfileEntry *
                                 pWsscfgFsDot11QosProfileEntry)
{
    tWsscfgFsDot11QosProfileEntry *pGetWsscfgFsDot11QosProfileEntry = NULL;

    pGetWsscfgFsDot11QosProfileEntry =
        (tWsscfgFsDot11QosProfileEntry *)
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsDot11QosProfileTable,
                   (tRBElem *) pWsscfgFsDot11QosProfileEntry);

    return pGetWsscfgFsDot11QosProfileEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsDot11WlanCapabilityProfileTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsDot11WlanCapabilityProfileEntry or NULL
****************************************************************************/
tWsscfgFsDot11WlanCapabilityProfileEntry
    * WsscfgGetFirstFsDot11WlanCapabilityProfileTable ()
{
    tWsscfgFsDot11WlanCapabilityProfileEntry
        * pWsscfgFsDot11WlanCapabilityProfileEntry = NULL;

    pWsscfgFsDot11WlanCapabilityProfileEntry =
        (tWsscfgFsDot11WlanCapabilityProfileEntry *)
        RBTreeGetFirst (gWsscfgGlobals.
                        WsscfgGlbMib.FsDot11WlanCapabilityProfileTable);

    return pWsscfgFsDot11WlanCapabilityProfileEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsDot11WlanCapabilityProfileTable
 Input       :  pCurrentWsscfgFsDot11WlanCapabilityProfileEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsDot11WlanCapabilityProfileEntry or NULL
****************************************************************************/
tWsscfgFsDot11WlanCapabilityProfileEntry
    *
    WsscfgGetNextFsDot11WlanCapabilityProfileTable
    (tWsscfgFsDot11WlanCapabilityProfileEntry *
     pCurrentWsscfgFsDot11WlanCapabilityProfileEntry)
{
    tWsscfgFsDot11WlanCapabilityProfileEntry
        * pNextWsscfgFsDot11WlanCapabilityProfileEntry = NULL;

    pNextWsscfgFsDot11WlanCapabilityProfileEntry =
        (tWsscfgFsDot11WlanCapabilityProfileEntry *)
        RBTreeGetNext (gWsscfgGlobals.
                       WsscfgGlbMib.FsDot11WlanCapabilityProfileTable,
                       (tRBElem *)
                       pCurrentWsscfgFsDot11WlanCapabilityProfileEntry, NULL);

    return pNextWsscfgFsDot11WlanCapabilityProfileEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11WlanCapabilityProfileTable
 Input       :  pWsscfgFsDot11WlanCapabilityProfileEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsDot11WlanCapabilityProfileEntry or NULL
****************************************************************************/
tWsscfgFsDot11WlanCapabilityProfileEntry
    *
    WsscfgGetFsDot11WlanCapabilityProfileTable
    (tWsscfgFsDot11WlanCapabilityProfileEntry *
     pWsscfgFsDot11WlanCapabilityProfileEntry)
{
    tWsscfgFsDot11WlanCapabilityProfileEntry
        * pGetWsscfgFsDot11WlanCapabilityProfileEntry = NULL;

    pGetWsscfgFsDot11WlanCapabilityProfileEntry =
        (tWsscfgFsDot11WlanCapabilityProfileEntry *)
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.FsDot11WlanCapabilityProfileTable,
                   (tRBElem *) pWsscfgFsDot11WlanCapabilityProfileEntry);

    return pGetWsscfgFsDot11WlanCapabilityProfileEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsDot11WlanAuthenticationProfileTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsDot11WlanAuthenticationProfileEntry or NULL
****************************************************************************/
tWsscfgFsDot11WlanAuthenticationProfileEntry
    * WsscfgGetFirstFsDot11WlanAuthenticationProfileTable ()
{
    tWsscfgFsDot11WlanAuthenticationProfileEntry
        * pWsscfgFsDot11WlanAuthenticationProfileEntry = NULL;

    pWsscfgFsDot11WlanAuthenticationProfileEntry =
        (tWsscfgFsDot11WlanAuthenticationProfileEntry *)
        RBTreeGetFirst (gWsscfgGlobals.
                        WsscfgGlbMib.FsDot11WlanAuthenticationProfileTable);

    return pWsscfgFsDot11WlanAuthenticationProfileEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsDot11WlanAuthenticationProfileTable
 Input       :  pCurrentWsscfgFsDot11WlanAuthenticationProfileEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsDot11WlanAuthenticationProfileEntry or NULL
****************************************************************************/
tWsscfgFsDot11WlanAuthenticationProfileEntry
    *
    WsscfgGetNextFsDot11WlanAuthenticationProfileTable
    (tWsscfgFsDot11WlanAuthenticationProfileEntry *
     pCurrentWsscfgFsDot11WlanAuthenticationProfileEntry)
{
    tWsscfgFsDot11WlanAuthenticationProfileEntry
        * pNextWsscfgFsDot11WlanAuthenticationProfileEntry = NULL;

    pNextWsscfgFsDot11WlanAuthenticationProfileEntry =
        (tWsscfgFsDot11WlanAuthenticationProfileEntry *)
        RBTreeGetNext (gWsscfgGlobals.
                       WsscfgGlbMib.FsDot11WlanAuthenticationProfileTable,
                       (tRBElem *)
                       pCurrentWsscfgFsDot11WlanAuthenticationProfileEntry,
                       NULL);

    return pNextWsscfgFsDot11WlanAuthenticationProfileEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11WlanAuthenticationProfileTable
 Input       :  pWsscfgFsDot11WlanAuthenticationProfileEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsDot11WlanAuthenticationProfileEntry or NULL
****************************************************************************/
tWsscfgFsDot11WlanAuthenticationProfileEntry
    *
    WsscfgGetFsDot11WlanAuthenticationProfileTable
    (tWsscfgFsDot11WlanAuthenticationProfileEntry *
     pWsscfgFsDot11WlanAuthenticationProfileEntry)
{
    tWsscfgFsDot11WlanAuthenticationProfileEntry
        * pGetWsscfgFsDot11WlanAuthenticationProfileEntry = NULL;

    pGetWsscfgFsDot11WlanAuthenticationProfileEntry =
        (tWsscfgFsDot11WlanAuthenticationProfileEntry *)
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.FsDot11WlanAuthenticationProfileTable,
                   (tRBElem *) pWsscfgFsDot11WlanAuthenticationProfileEntry);

    return pGetWsscfgFsDot11WlanAuthenticationProfileEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsDot11WlanQosProfileTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsDot11WlanQosProfileEntry or NULL
****************************************************************************/
tWsscfgFsDot11WlanQosProfileEntry *
WsscfgGetFirstFsDot11WlanQosProfileTable ()
{
    tWsscfgFsDot11WlanQosProfileEntry
        * pWsscfgFsDot11WlanQosProfileEntry = NULL;

    pWsscfgFsDot11WlanQosProfileEntry =
        (tWsscfgFsDot11WlanQosProfileEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.FsDot11WlanQosProfileTable);

    return pWsscfgFsDot11WlanQosProfileEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsDot11WlanQosProfileTable
 Input       :  pCurrentWsscfgFsDot11WlanQosProfileEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsDot11WlanQosProfileEntry or NULL
****************************************************************************/
tWsscfgFsDot11WlanQosProfileEntry
    *
WsscfgGetNextFsDot11WlanQosProfileTable (tWsscfgFsDot11WlanQosProfileEntry *
                                         pCurrentWsscfgFsDot11WlanQosProfileEntry)
{
    tWsscfgFsDot11WlanQosProfileEntry
        * pNextWsscfgFsDot11WlanQosProfileEntry = NULL;

    pNextWsscfgFsDot11WlanQosProfileEntry =
        (tWsscfgFsDot11WlanQosProfileEntry *)
        RBTreeGetNext
        (gWsscfgGlobals.WsscfgGlbMib.FsDot11WlanQosProfileTable,
         (tRBElem *) pCurrentWsscfgFsDot11WlanQosProfileEntry, NULL);

    return pNextWsscfgFsDot11WlanQosProfileEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11WlanQosProfileTable
 Input       :  pWsscfgFsDot11WlanQosProfileEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsDot11WlanQosProfileEntry or NULL
****************************************************************************/
tWsscfgFsDot11WlanQosProfileEntry *
WsscfgGetFsDot11WlanQosProfileTable (tWsscfgFsDot11WlanQosProfileEntry *
                                     pWsscfgFsDot11WlanQosProfileEntry)
{
    tWsscfgFsDot11WlanQosProfileEntry
        * pGetWsscfgFsDot11WlanQosProfileEntry = NULL;

    pGetWsscfgFsDot11WlanQosProfileEntry =
        (tWsscfgFsDot11WlanQosProfileEntry *)
        RBTreeGet
        (gWsscfgGlobals.WsscfgGlbMib.FsDot11WlanQosProfileTable,
         (tRBElem *) pWsscfgFsDot11WlanQosProfileEntry);

    return pGetWsscfgFsDot11WlanQosProfileEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsDot11RadioQosTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsDot11RadioQosEntry or NULL
****************************************************************************/
INT4
WsscfgGetFirstFsDot11RadioQosTable (INT4 *pi4IfIndex)
{
    tRadioIfDB         *pRadioIfDB = NULL;

    pRadioIfDB = (tRadioIfDB *) RBTreeGetFirst (gRadioIfGlobals.
                                                RadioIfGlbMib.RadioIfDB);

    if (pRadioIfDB == NULL)
    {
        return OSIX_FAILURE;
    }
    *pi4IfIndex = (INT4) (pRadioIfDB->u4VirtualRadioIfIndex);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgGetNextFsDot11RadioQosTable
 Input       :  pCurrentWsscfgFsDot11RadioQosEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsDot11RadioQosEntry or NULL
****************************************************************************/
INT1
WsscfgGetNextFsDot11RadioQosTable (INT4 i4CurrentIndex, INT4 *pi4NextIndex)
{
    tRadioIfDB         *pRadioIfDB = NULL;
    tRadioIfDB          RadioIfDB;
    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));
    RadioIfDB.u4VirtualRadioIfIndex = (UINT4) i4CurrentIndex;
    pRadioIfDB =
        (tRadioIfDB *) RBTreeGetNext (gRadioIfGlobals.
                                      RadioIfGlbMib.RadioIfDB,
                                      (tRBElem *) & RadioIfDB, NULL);
    if (pRadioIfDB != NULL)
    {
        *pi4NextIndex = (INT4) pRadioIfDB->u4VirtualRadioIfIndex;
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11RadioQosTable
 Input       :  pWsscfgFsDot11RadioQosEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsDot11RadioQosEntry or NULL
****************************************************************************/
tWsscfgFsDot11RadioQosEntry *
WsscfgGetFsDot11RadioQosTable (tWsscfgFsDot11RadioQosEntry *
                               pWsscfgFsDot11RadioQosEntry)
{
    tWsscfgFsDot11RadioQosEntry *pGetWsscfgFsDot11RadioQosEntry = NULL;

    pGetWsscfgFsDot11RadioQosEntry =
        (tWsscfgFsDot11RadioQosEntry *)
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsDot11RadioQosTable,
                   (tRBElem *) pWsscfgFsDot11RadioQosEntry);

    return pGetWsscfgFsDot11RadioQosEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsDot11QAPTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsDot11QAPEntry or NULL
****************************************************************************/
tWsscfgFsDot11QAPEntry *
WsscfgGetFirstFsDot11QAPTable ()
{
    tWsscfgFsDot11QAPEntry *pWsscfgFsDot11QAPEntry = NULL;

    pWsscfgFsDot11QAPEntry =
        (tWsscfgFsDot11QAPEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.FsDot11QAPTable);

    return pWsscfgFsDot11QAPEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsDot11QAPTable
 Input       :  pCurrentWsscfgFsDot11QAPEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsDot11QAPEntry or NULL
****************************************************************************/
tWsscfgFsDot11QAPEntry *
WsscfgGetNextFsDot11QAPTable (tWsscfgFsDot11QAPEntry *
                              pCurrentWsscfgFsDot11QAPEntry)
{
    tWsscfgFsDot11QAPEntry *pNextWsscfgFsDot11QAPEntry = NULL;

    pNextWsscfgFsDot11QAPEntry =
        (tWsscfgFsDot11QAPEntry *)
        RBTreeGetNext (gWsscfgGlobals.WsscfgGlbMib.FsDot11QAPTable,
                       (tRBElem *) pCurrentWsscfgFsDot11QAPEntry, NULL);

    return pNextWsscfgFsDot11QAPEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11QAPTable
 Input       :  pWsscfgFsDot11QAPEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsDot11QAPEntry or NULL
****************************************************************************/
tWsscfgFsDot11QAPEntry *
WsscfgGetFsDot11QAPTable (tWsscfgFsDot11QAPEntry * pWsscfgFsDot11QAPEntry)
{
    tWsscfgFsDot11QAPEntry *pGetWsscfgFsDot11QAPEntry = NULL;

    pGetWsscfgFsDot11QAPEntry =
        (tWsscfgFsDot11QAPEntry *) RBTreeGet (gWsscfgGlobals.
                                              WsscfgGlbMib.FsDot11QAPTable,
                                              (tRBElem *)
                                              pWsscfgFsDot11QAPEntry);

    return pGetWsscfgFsDot11QAPEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsQAPProfileTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsQAPProfileEntry or NULL
****************************************************************************/
tWsscfgFsQAPProfileEntry *
WsscfgGetFirstFsQAPProfileTable ()
{
    tWsscfgFsQAPProfileEntry *pWsscfgFsQAPProfileEntry = NULL;

    pWsscfgFsQAPProfileEntry =
        (tWsscfgFsQAPProfileEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.FsQAPProfileTable);

    return pWsscfgFsQAPProfileEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsQAPProfileTable
 Input       :  pCurrentWsscfgFsQAPProfileEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsQAPProfileEntry or NULL
****************************************************************************/
tWsscfgFsQAPProfileEntry *
WsscfgGetNextFsQAPProfileTable (tWsscfgFsQAPProfileEntry *
                                pCurrentWsscfgFsQAPProfileEntry)
{
    tWsscfgFsQAPProfileEntry *pNextWsscfgFsQAPProfileEntry = NULL;

    pNextWsscfgFsQAPProfileEntry =
        (tWsscfgFsQAPProfileEntry *)
        RBTreeGetNext (gWsscfgGlobals.WsscfgGlbMib.FsQAPProfileTable,
                       (tRBElem *) pCurrentWsscfgFsQAPProfileEntry, NULL);

    return pNextWsscfgFsQAPProfileEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFsQAPProfileTable
 Input       :  pWsscfgFsQAPProfileEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsQAPProfileEntry or NULL
****************************************************************************/
tWsscfgFsQAPProfileEntry *
WsscfgGetFsQAPProfileTable (tWsscfgFsQAPProfileEntry * pWsscfgFsQAPProfileEntry)
{
    tWsscfgFsQAPProfileEntry *pGetWsscfgFsQAPProfileEntry = NULL;

    pGetWsscfgFsQAPProfileEntry =
        (tWsscfgFsQAPProfileEntry *) RBTreeGet (gWsscfgGlobals.
                                                WsscfgGlbMib.FsQAPProfileTable,
                                                (tRBElem *)
                                                pWsscfgFsQAPProfileEntry);

    return pGetWsscfgFsQAPProfileEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsDot11CapabilityMappingTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsDot11CapabilityMappingEntry or NULL
****************************************************************************/
tWsscfgFsDot11CapabilityMappingEntry
    * WsscfgGetFirstFsDot11CapabilityMappingTable ()
{
    tWsscfgFsDot11CapabilityMappingEntry
        * pWsscfgFsDot11CapabilityMappingEntry = NULL;

    pWsscfgFsDot11CapabilityMappingEntry =
        (tWsscfgFsDot11CapabilityMappingEntry *)
        RBTreeGetFirst (gWsscfgGlobals.
                        WsscfgGlbMib.FsDot11CapabilityMappingTable);

    return pWsscfgFsDot11CapabilityMappingEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsDot11CapabilityMappingTable
 Input       :  pCurrentWsscfgFsDot11CapabilityMappingEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsDot11CapabilityMappingEntry or NULL
****************************************************************************/
tWsscfgFsDot11CapabilityMappingEntry
    *
    WsscfgGetNextFsDot11CapabilityMappingTable
    (tWsscfgFsDot11CapabilityMappingEntry *
     pCurrentWsscfgFsDot11CapabilityMappingEntry)
{
    tWsscfgFsDot11CapabilityMappingEntry
        * pNextWsscfgFsDot11CapabilityMappingEntry = NULL;

    pNextWsscfgFsDot11CapabilityMappingEntry =
        (tWsscfgFsDot11CapabilityMappingEntry *)
        RBTreeGetNext (gWsscfgGlobals.
                       WsscfgGlbMib.FsDot11CapabilityMappingTable,
                       (tRBElem *)
                       pCurrentWsscfgFsDot11CapabilityMappingEntry, NULL);

    return pNextWsscfgFsDot11CapabilityMappingEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11CapabilityMappingTable
 Input       :  pWsscfgFsDot11CapabilityMappingEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsDot11CapabilityMappingEntry or NULL
****************************************************************************/
tWsscfgFsDot11CapabilityMappingEntry
    *
    WsscfgGetFsDot11CapabilityMappingTable
    (tWsscfgFsDot11CapabilityMappingEntry *
     pWsscfgFsDot11CapabilityMappingEntry)
{
    tWsscfgFsDot11CapabilityMappingEntry
        * pGetWsscfgFsDot11CapabilityMappingEntry = NULL;

    pGetWsscfgFsDot11CapabilityMappingEntry =
        (tWsscfgFsDot11CapabilityMappingEntry *)
        RBTreeGet
        (gWsscfgGlobals.WsscfgGlbMib.FsDot11CapabilityMappingTable,
         (tRBElem *) pWsscfgFsDot11CapabilityMappingEntry);

    return pGetWsscfgFsDot11CapabilityMappingEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsDot11AuthMappingTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsDot11AuthMappingEntry or NULL
****************************************************************************/
tWsscfgFsDot11AuthMappingEntry *
WsscfgGetFirstFsDot11AuthMappingTable ()
{
    tWsscfgFsDot11AuthMappingEntry *pWsscfgFsDot11AuthMappingEntry = NULL;

    pWsscfgFsDot11AuthMappingEntry =
        (tWsscfgFsDot11AuthMappingEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.FsDot11AuthMappingTable);

    return pWsscfgFsDot11AuthMappingEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsDot11AuthMappingTable
 Input       :  pCurrentWsscfgFsDot11AuthMappingEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsDot11AuthMappingEntry or NULL
****************************************************************************/
tWsscfgFsDot11AuthMappingEntry *
WsscfgGetNextFsDot11AuthMappingTable (tWsscfgFsDot11AuthMappingEntry *
                                      pCurrentWsscfgFsDot11AuthMappingEntry)
{
    tWsscfgFsDot11AuthMappingEntry *pNextWsscfgFsDot11AuthMappingEntry = NULL;

    pNextWsscfgFsDot11AuthMappingEntry =
        (tWsscfgFsDot11AuthMappingEntry *)
        RBTreeGetNext
        (gWsscfgGlobals.WsscfgGlbMib.FsDot11AuthMappingTable,
         (tRBElem *) pCurrentWsscfgFsDot11AuthMappingEntry, NULL);

    return pNextWsscfgFsDot11AuthMappingEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11AuthMappingTable
 Input       :  pWsscfgFsDot11AuthMappingEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsDot11AuthMappingEntry or NULL
****************************************************************************/
tWsscfgFsDot11AuthMappingEntry *
WsscfgGetFsDot11AuthMappingTable (tWsscfgFsDot11AuthMappingEntry *
                                  pWsscfgFsDot11AuthMappingEntry)
{
    tWsscfgFsDot11AuthMappingEntry *pGetWsscfgFsDot11AuthMappingEntry = NULL;

    pGetWsscfgFsDot11AuthMappingEntry =
        (tWsscfgFsDot11AuthMappingEntry *)
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsDot11AuthMappingTable,
                   (tRBElem *) pWsscfgFsDot11AuthMappingEntry);

    return pGetWsscfgFsDot11AuthMappingEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsDot11QosMappingTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsDot11QosMappingEntry or NULL
****************************************************************************/
tWsscfgFsDot11QosMappingEntry *
WsscfgGetFirstFsDot11QosMappingTable ()
{
    tWsscfgFsDot11QosMappingEntry *pWsscfgFsDot11QosMappingEntry = NULL;

    pWsscfgFsDot11QosMappingEntry =
        (tWsscfgFsDot11QosMappingEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.FsDot11QosMappingTable);

    return pWsscfgFsDot11QosMappingEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsDot11QosMappingTable
 Input       :  pCurrentWsscfgFsDot11QosMappingEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsDot11QosMappingEntry or NULL
****************************************************************************/
tWsscfgFsDot11QosMappingEntry *
WsscfgGetNextFsDot11QosMappingTable (tWsscfgFsDot11QosMappingEntry *
                                     pCurrentWsscfgFsDot11QosMappingEntry)
{
    tWsscfgFsDot11QosMappingEntry *pNextWsscfgFsDot11QosMappingEntry = NULL;

    pNextWsscfgFsDot11QosMappingEntry =
        (tWsscfgFsDot11QosMappingEntry *)
        RBTreeGetNext
        (gWsscfgGlobals.WsscfgGlbMib.FsDot11QosMappingTable,
         (tRBElem *) pCurrentWsscfgFsDot11QosMappingEntry, NULL);

    return pNextWsscfgFsDot11QosMappingEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsDot11ClientSummaryTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsDot11ClientSummaryEntry or NULL
****************************************************************************/
tWssIfAuthStateDB  *
WsscfgGetFirstFsDot11ClientSummaryTable ()
{
    tWssIfAuthStateDB  *pWssIfAuthStateDB = NULL;
#ifdef WLC_WANTED

    pWssIfAuthStateDB = (tWssIfAuthStateDB *) RBTreeGetFirst (gWssStaStateDB);
#endif
    return pWssIfAuthStateDB;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsDot11ClientSummaryTable
 Input       :  pCurrentWsscfgFsDot11QosMappingEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsDot11ClientSummaryEntry or NULL
****************************************************************************/
tWssIfAuthStateDB  *
WsscfgGetNextFsDot11ClientSummaryTable (tWssIfAuthStateDB *
                                        pCurrentWsscfgAuthStateDB)
{
    tWssIfAuthStateDB  *pNextWssIfAuthStateDB = NULL;
#ifdef WLC_WANTED
    pNextWssIfAuthStateDB =
        (tWssIfAuthStateDB *)
        RBTreeGetNext (gWssStaStateDB, (tRBElem *) pCurrentWsscfgAuthStateDB,
                       NULL);
#else
    UNUSED_PARAM (pCurrentWsscfgAuthStateDB);
#endif
    return pNextWssIfAuthStateDB;

}

/****************************************************************************
 Function    :  WsscfgGetFsDot11QosMappingTable
 Input       :  pWsscfgFsDot11QosMappingEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsDot11QosMappingEntry or NULL
****************************************************************************/
tWsscfgFsDot11QosMappingEntry *
WsscfgGetFsDot11QosMappingTable (tWsscfgFsDot11QosMappingEntry *
                                 pWsscfgFsDot11QosMappingEntry)
{
    tWsscfgFsDot11QosMappingEntry *pGetWsscfgFsDot11QosMappingEntry = NULL;

    pGetWsscfgFsDot11QosMappingEntry =
        (tWsscfgFsDot11QosMappingEntry *)
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsDot11QosMappingTable,
                   (tRBElem *) pWsscfgFsDot11QosMappingEntry);

    return pGetWsscfgFsDot11QosMappingEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsDot11AntennasListTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsDot11AntennasListEntry or NULL
****************************************************************************/
INT4
WsscfgGetFirstFsDot11AntennasListTable (INT4 *pi4IfIndex, INT4 *pi4AntIndex)
{

    tRadioIfDB         *pRadioIfDB = NULL;

    /* look through radio-if database */
    pRadioIfDB =
        (tRadioIfDB *) RBTreeGetFirst (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB);
    if (pRadioIfDB == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4IfIndex = pRadioIfDB->u4VirtualRadioIfIndex;
    *pi4AntIndex = 1;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsDot11AntennasListTable
 Input       :  pCurrentWsscfgFsDot11AntennasListEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsDot11AntennasListEntry or NULL
****************************************************************************/
INT4
WsscfgGetNextFsDot11AntennasListTable (INT4 i4IfIndex,
                                       INT4 *pi4NextIfIndex,
                                       INT4 i4Dot11AntennaListIndex,
                                       INT4 *pi4NextDot11AntennaListIndex)
{
    tRadioIfDB         *pRadioIfDB = NULL;
    tRadioIfDB          RadioIfDB;

    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));

    /* To get the Current Tx Antenna for the current radio,\
     * this RBTree Get is used. */
    RadioIfDB.u4VirtualRadioIfIndex = i4IfIndex;

    pRadioIfDB = (tRadioIfDB *) RBTreeGet (gRadioIfGlobals.
                                           RadioIfGlbMib.RadioIfDB,
                                           (tRBElem *) & RadioIfDB);
    if (pRadioIfDB != NULL)
    {
        if (i4Dot11AntennaListIndex ==
            pRadioIfDB->RadioIfConfigParam.u1CurrentTxAntenna)
        {
            pRadioIfDB =
                (tRadioIfDB *) RBTreeGetNext (gRadioIfGlobals.
                                              RadioIfGlbMib.RadioIfDB,
                                              (tRBElem *) & RadioIfDB, NULL);
            if (pRadioIfDB == NULL)
            {
                return SNMP_FAILURE;
            }
        }
        else
        {
            /* Since this radio contains multiple antenna, and this
             * antenna is not a key in RBTree, directly increment and
             *return the antenna index value is done here */
            *pi4NextDot11AntennaListIndex = i4Dot11AntennaListIndex + 1;
            *pi4NextIfIndex = i4IfIndex;
            return SNMP_SUCCESS;
        }
        *pi4NextDot11AntennaListIndex = 1;
        *pi4NextIfIndex = pRadioIfDB->u4VirtualRadioIfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11AntennasListTable
 Input       :  pWsscfgFsDot11AntennasListEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsDot11AntennasListEntry or NULL
****************************************************************************/
tWsscfgFsDot11AntennasListEntry *
WsscfgGetFsDot11AntennasListTable (tWsscfgFsDot11AntennasListEntry *
                                   pWsscfgFsDot11AntennasListEntry)
{
    tWsscfgFsDot11AntennasListEntry *pGetWsscfgFsDot11AntennasListEntry = NULL;

    pGetWsscfgFsDot11AntennasListEntry =
        (tWsscfgFsDot11AntennasListEntry *)
        RBTreeGet
        (gWsscfgGlobals.WsscfgGlbMib.FsDot11AntennasListTable,
         (tRBElem *) pWsscfgFsDot11AntennasListEntry);

    return pGetWsscfgFsDot11AntennasListEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsDot11WlanTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsDot11WlanEntry or NULL
****************************************************************************/
tWsscfgFsDot11WlanEntry *
WsscfgGetFirstFsDot11WlanTable ()
{
    tWsscfgFsDot11WlanEntry *pWsscfgFsDot11WlanEntry = NULL;

    pWsscfgFsDot11WlanEntry =
        (tWsscfgFsDot11WlanEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.FsDot11WlanTable);

    return pWsscfgFsDot11WlanEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsDot11WlanTable
 Input       :  pCurrentWsscfgFsDot11WlanEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsDot11WlanEntry or NULL
****************************************************************************/
tWsscfgFsDot11WlanEntry *
WsscfgGetNextFsDot11WlanTable (tWsscfgFsDot11WlanEntry *
                               pCurrentWsscfgFsDot11WlanEntry)
{
    tWsscfgFsDot11WlanEntry *pNextWsscfgFsDot11WlanEntry = NULL;

    pNextWsscfgFsDot11WlanEntry =
        (tWsscfgFsDot11WlanEntry *)
        RBTreeGetNext (gWsscfgGlobals.WsscfgGlbMib.FsDot11WlanTable,
                       (tRBElem *) pCurrentWsscfgFsDot11WlanEntry, NULL);

    return pNextWsscfgFsDot11WlanEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11WlanTable
 Input       :  pWsscfgFsDot11WlanEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsDot11WlanEntry or NULL
****************************************************************************/
tWsscfgFsDot11WlanEntry *
WsscfgGetFsDot11WlanTable (tWsscfgFsDot11WlanEntry * pWsscfgFsDot11WlanEntry)
{
    tWsscfgFsDot11WlanEntry *pGetWsscfgFsDot11WlanEntry = NULL;

    pGetWsscfgFsDot11WlanEntry =
        (tWsscfgFsDot11WlanEntry *) RBTreeGet (gWsscfgGlobals.
                                               WsscfgGlbMib.FsDot11WlanTable,
                                               (tRBElem *)
                                               pWsscfgFsDot11WlanEntry);

    return pGetWsscfgFsDot11WlanEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsDot11WlanBindTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsDot11WlanBindEntry or NULL
****************************************************************************/
tWsscfgFsDot11WlanBindEntry *
WsscfgGetFirstFsDot11WlanBindTable ()
{
    tWsscfgFsDot11WlanBindEntry *pWsscfgFsDot11WlanBindEntry = NULL;

    pWsscfgFsDot11WlanBindEntry =
        (tWsscfgFsDot11WlanBindEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.FsDot11WlanBindTable);

    return pWsscfgFsDot11WlanBindEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsDot11WlanBindTable
 Input       :  pCurrentWsscfgFsDot11WlanBindEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsDot11WlanBindEntry or NULL
****************************************************************************/
tWsscfgFsDot11WlanBindEntry *
WsscfgGetNextFsDot11WlanBindTable (tWsscfgFsDot11WlanBindEntry *
                                   pCurrentWsscfgFsDot11WlanBindEntry)
{
    tWsscfgFsDot11WlanBindEntry *pNextWsscfgFsDot11WlanBindEntry = NULL;

    pNextWsscfgFsDot11WlanBindEntry =
        (tWsscfgFsDot11WlanBindEntry *)
        RBTreeGetNext
        (gWsscfgGlobals.WsscfgGlbMib.FsDot11WlanBindTable, (tRBElem *)
         pCurrentWsscfgFsDot11WlanBindEntry, NULL);

    return pNextWsscfgFsDot11WlanBindEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11WlanBindTable
 Input       :  pWsscfgFsDot11WlanBindEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsDot11WlanBindEntry or NULL
****************************************************************************/
tWsscfgFsDot11WlanBindEntry *
WsscfgGetFsDot11WlanBindTable (tWsscfgFsDot11WlanBindEntry *
                               pWsscfgFsDot11WlanBindEntry)
{
    tWsscfgFsDot11WlanBindEntry *pGetWsscfgFsDot11WlanBindEntry = NULL;

    pGetWsscfgFsDot11WlanBindEntry =
        (tWsscfgFsDot11WlanBindEntry *)
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsDot11WlanBindTable,
                   (tRBElem *) pWsscfgFsDot11WlanBindEntry);

    return pGetWsscfgFsDot11WlanBindEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsDot11nConfigTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsDot11nConfigEntry or NULL
****************************************************************************/
INT1
WsscfgGetFirstFsDot11nConfigTable (INT4 *pi4IfIndex)
{
    tRadioIfDB         *pRadioIfDB = NULL;
    /*
     * Get the first interface index from the radio db
     */
    pRadioIfDB =
        (tRadioIfDB *) RBTreeGetFirst (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB);
    if (pRadioIfDB != NULL)
    {
        *pi4IfIndex = (INT4) (pRadioIfDB->u4VirtualRadioIfIndex);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsDot11nConfigTable
 Input       :  pCurrentWsscfgFsDot11nConfigEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  The SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/
INT1
WsscfgGetNextFsDot11nConfigTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tRadioIfDB          RadioIfDB;
    tRadioIfDB         *pNextRadioIfDB = NULL;

    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));

    /* assign index */
    RadioIfDB.u4VirtualRadioIfIndex = (UINT4) i4IfIndex;

    pNextRadioIfDB =
        (tRadioIfDB *) RBTreeGetNext (gRadioIfGlobals.
                                      RadioIfGlbMib.RadioIfDB,
                                      (tRBElem *) & RadioIfDB, NULL);

    if (pNextRadioIfDB != NULL)
    {
        *pi4NextIfIndex = (INT4) (pNextRadioIfDB->u4VirtualRadioIfIndex);
        return SNMP_SUCCESS;
    }

    /* no entry present, just return error */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11nConfigTable
 Input       :  pWsscfgFsDot11nConfigEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsDot11nConfigEntry or NULL
****************************************************************************/
tWsscfgFsDot11nConfigEntry *
WsscfgGetFsDot11nConfigTable (tWsscfgFsDot11nConfigEntry *
                              pWsscfgFsDot11nConfigEntry)
{
    tWsscfgFsDot11nConfigEntry *pGetWsscfgFsDot11nConfigEntry = NULL;

    pGetWsscfgFsDot11nConfigEntry =
        (tWsscfgFsDot11nConfigEntry *)
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsDot11nConfigTable,
                   (tRBElem *) pWsscfgFsDot11nConfigEntry);

    return pGetWsscfgFsDot11nConfigEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsDot11nMCSDataRateTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsDot11nMCSDataRateEntry or NULL
****************************************************************************/
INT1
WsscfgGetFirstFsDot11nMCSDataRateTable (INT4 *pi4IfIndex)
{
    tRadioIfDB         *pRadioIfDB = NULL;
    /*
     * Get the first interface index from the radio db
     */
    pRadioIfDB =
        (tRadioIfDB *) RBTreeGetFirst (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB);
    if (pRadioIfDB != NULL)
    {
        *pi4IfIndex = (INT4) (pRadioIfDB->u4VirtualRadioIfIndex);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsDot11nMCSDataRateTable
 Input       :  pCurrentWsscfgFsDot11nMCSDataRateEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsDot11nMCSDataRateEntry or NULL
****************************************************************************/
INT1
WsscfgGetNextFsDot11nMCSDataRateTable (INT4 i4IfIndex,
                                       INT4 *pi4NextIfIndex,
                                       INT4 i4FsDot11nMCSDataRateIndex,
                                       INT4 *pi4NextFsDot11nMCSDataRateIndex)
{
    tRadioIfDB          RadioIfDB;
    tRadioIfDB         *pNextRadioIfDB = NULL;

    if (i4FsDot11nMCSDataRateIndex != (CLI_MAX_MCS_RATE_INDEX - 1))
    {
        *pi4NextIfIndex = i4IfIndex;
        *pi4NextFsDot11nMCSDataRateIndex = i4FsDot11nMCSDataRateIndex + 1;
        return SNMP_SUCCESS;
    }

    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));

    /* assign index */
    RadioIfDB.u4VirtualRadioIfIndex = (UINT4) i4IfIndex;

    pNextRadioIfDB =
        (tRadioIfDB *) RBTreeGetNext (gRadioIfGlobals.
                                      RadioIfGlbMib.RadioIfDB,
                                      (tRBElem *) & RadioIfDB, NULL);

    if (pNextRadioIfDB != NULL)
    {
        *pi4NextIfIndex = (INT4) (pNextRadioIfDB->u4VirtualRadioIfIndex);
        *pi4NextFsDot11nMCSDataRateIndex = 0;
        return SNMP_SUCCESS;
    }

    /* no entry present, just return error */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  WsscfgGetFsDot11nMCSDataRateTable
 Input       :  pWsscfgFsDot11nMCSDataRateEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsDot11nMCSDataRateEntry or NULL
****************************************************************************/
tWsscfgFsDot11nMCSDataRateEntry *
WsscfgGetFsDot11nMCSDataRateTable (tWsscfgFsDot11nMCSDataRateEntry *
                                   pWsscfgFsDot11nMCSDataRateEntry)
{
    tWsscfgFsDot11nMCSDataRateEntry *pGetWsscfgFsDot11nMCSDataRateEntry = NULL;

    pGetWsscfgFsDot11nMCSDataRateEntry =
        (tWsscfgFsDot11nMCSDataRateEntry *)
        RBTreeGet
        (gWsscfgGlobals.WsscfgGlbMib.FsDot11nMCSDataRateTable,
         (tRBElem *) pWsscfgFsDot11nMCSDataRateEntry);

    return pGetWsscfgFsDot11nMCSDataRateEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsWtpImageUpgradeTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsWtpImageUpgradeEntry or NULL
****************************************************************************/
tWsscfgFsWtpImageUpgradeEntry *
WsscfgGetFirstFsWtpImageUpgradeTable ()
{
    tWsscfgFsWtpImageUpgradeEntry *pWsscfgFsWtpImageUpgradeEntry = NULL;

    pWsscfgFsWtpImageUpgradeEntry =
        (tWsscfgFsWtpImageUpgradeEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.FsWtpImageUpgradeTable);

    return pWsscfgFsWtpImageUpgradeEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsWtpImageUpgradeTable
 Input       :  pCurrentWsscfgFsWtpImageUpgradeEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsWtpImageUpgradeEntry or NULL
****************************************************************************/
tWsscfgFsWtpImageUpgradeEntry *
WsscfgGetNextFsWtpImageUpgradeTable (tWsscfgFsWtpImageUpgradeEntry *
                                     pCurrentWsscfgFsWtpImageUpgradeEntry)
{
    tWsscfgFsWtpImageUpgradeEntry *pNextWsscfgFsWtpImageUpgradeEntry = NULL;

    pNextWsscfgFsWtpImageUpgradeEntry =
        (tWsscfgFsWtpImageUpgradeEntry *)
        RBTreeGetNext
        (gWsscfgGlobals.WsscfgGlbMib.FsWtpImageUpgradeTable,
         (tRBElem *) pCurrentWsscfgFsWtpImageUpgradeEntry, NULL);

    return pNextWsscfgFsWtpImageUpgradeEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFsWtpImageUpgradeTable
 Input       :  pWsscfgFsWtpImageUpgradeEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsWtpImageUpgradeEntry or NULL
****************************************************************************/
tWsscfgFsWtpImageUpgradeEntry *
WsscfgGetFsWtpImageUpgradeTable (tWsscfgFsWtpImageUpgradeEntry *
                                 pWsscfgFsWtpImageUpgradeEntry)
{
    tWsscfgFsWtpImageUpgradeEntry *pGetWsscfgFsWtpImageUpgradeEntry = NULL;

    pGetWsscfgFsWtpImageUpgradeEntry =
        (tWsscfgFsWtpImageUpgradeEntry *)
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsWtpImageUpgradeTable,
                   (tRBElem *) pWsscfgFsWtpImageUpgradeEntry);

    return pGetWsscfgFsWtpImageUpgradeEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFirstFsRrmConfigTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pWsscfgFsRrmConfigEntry or NULL
****************************************************************************/
tWsscfgFsRrmConfigEntry *
WsscfgGetFirstFsRrmConfigTable ()
{
    tWsscfgFsRrmConfigEntry *pWsscfgFsRrmConfigEntry = NULL;

    pWsscfgFsRrmConfigEntry =
        (tWsscfgFsRrmConfigEntry *)
        RBTreeGetFirst (gWsscfgGlobals.WsscfgGlbMib.FsRrmConfigTable);

    return pWsscfgFsRrmConfigEntry;
}

/****************************************************************************
 Function    :  WsscfgGetNextFsRrmConfigTable
 Input       :  pCurrentWsscfgFsRrmConfigEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextWsscfgFsRrmConfigEntry or NULL
****************************************************************************/
tWsscfgFsRrmConfigEntry *
WsscfgGetNextFsRrmConfigTable (tWsscfgFsRrmConfigEntry *
                               pCurrentWsscfgFsRrmConfigEntry)
{
    tWsscfgFsRrmConfigEntry *pNextWsscfgFsRrmConfigEntry = NULL;

    pNextWsscfgFsRrmConfigEntry =
        (tWsscfgFsRrmConfigEntry *)
        RBTreeGetNext (gWsscfgGlobals.WsscfgGlbMib.FsRrmConfigTable,
                       (tRBElem *) pCurrentWsscfgFsRrmConfigEntry, NULL);

    return pNextWsscfgFsRrmConfigEntry;
}

/****************************************************************************
 Function    :  WsscfgGetFsRrmConfigTable
 Input       :  pWsscfgFsRrmConfigEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetWsscfgFsRrmConfigEntry or NULL
****************************************************************************/
tWsscfgFsRrmConfigEntry *
WsscfgGetFsRrmConfigTable (tWsscfgFsRrmConfigEntry * pWsscfgFsRrmConfigEntry)
{
    tWsscfgFsRrmConfigEntry *pGetWsscfgFsRrmConfigEntry = NULL;

    pGetWsscfgFsRrmConfigEntry =
        (tWsscfgFsRrmConfigEntry *)
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsRrmConfigTable,
                   (tRBElem *) pWsscfgFsRrmConfigEntry);

    return pGetWsscfgFsRrmConfigEntry;
}
