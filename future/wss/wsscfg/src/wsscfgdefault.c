/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: wsscfgdefault.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
*
* Description: This file contains the routines to initialize the
*              protocol structure for the module Wsscfg 
*********************************************************************/

#include "wsscfginc.h"

/****************************************************************************
* Function    : WsscfgInitializeDot11WEPKeyMappingsTable
* Input       : pWsscfgDot11WEPKeyMappingsEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
WsscfgInitializeDot11WEPKeyMappingsTable (tWsscfgDot11WEPKeyMappingsEntry *
                                          pWsscfgDot11WEPKeyMappingsEntry)
{
    if (pWsscfgDot11WEPKeyMappingsEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((WsscfgInitializeMibDot11WEPKeyMappingsTable
         (pWsscfgDot11WEPKeyMappingsEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeDot11GroupAddressesTable
* Input       : pWsscfgDot11GroupAddressesEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
WsscfgInitializeDot11GroupAddressesTable (tWsscfgDot11GroupAddressesEntry *
                                          pWsscfgDot11GroupAddressesEntry)
{
    if (pWsscfgDot11GroupAddressesEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((WsscfgInitializeMibDot11GroupAddressesTable
         (pWsscfgDot11GroupAddressesEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    : WsscfgInitializeFsQAPProfileTable
 * Input       : pWsscfgFsQAPProfileEntry
 * Description : It Intialize the given structure
 * Output      : None
 * Returns     : OSIX_SUCCESS or OSIX_FAILURE
 * *****************************************************************************/

INT4
WsscfgInitializeFsQAPProfileTable (tWsscfgFsQAPProfileEntry *
                                   pWsscfgFsQAPProfileEntry)
{
    if (pWsscfgFsQAPProfileEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((WsscfgInitializeMibFsQAPProfileTable
         (pWsscfgFsQAPProfileEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeCapwapDot11WlanTable
* Input       : pWsscfgCapwapDot11WlanEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
WsscfgInitializeCapwapDot11WlanTable (tWsscfgCapwapDot11WlanEntry *
                                      pWsscfgCapwapDot11WlanEntry)
{
    if (pWsscfgCapwapDot11WlanEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((WsscfgInitializeMibCapwapDot11WlanTable
         (pWsscfgCapwapDot11WlanEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeCapwapDot11WlanBindTable
* Input       : pWsscfgCapwapDot11WlanBindEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
WsscfgInitializeCapwapDot11WlanBindTable (tWsscfgCapwapDot11WlanBindEntry *
                                          pWsscfgCapwapDot11WlanBindEntry)
{
    if (pWsscfgCapwapDot11WlanBindEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((WsscfgInitializeMibCapwapDot11WlanBindTable
         (pWsscfgCapwapDot11WlanBindEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeFsDot11CapabilityProfileTable
* Input       : pWsscfgFsDot11CapabilityProfileEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
WsscfgInitializeFsDot11CapabilityProfileTable
    (tWsscfgFsDot11CapabilityProfileEntry *
     pWsscfgFsDot11CapabilityProfileEntry)
{
    if (pWsscfgFsDot11CapabilityProfileEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((WsscfgInitializeMibFsDot11CapabilityProfileTable
         (pWsscfgFsDot11CapabilityProfileEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeFsDot11AuthenticationProfileTable
* Input       : pWsscfgFsDot11AuthenticationProfileEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
WsscfgInitializeFsDot11AuthenticationProfileTable
    (tWsscfgFsDot11AuthenticationProfileEntry *
     pWsscfgFsDot11AuthenticationProfileEntry)
{
    if (pWsscfgFsDot11AuthenticationProfileEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((WsscfgInitializeMibFsDot11AuthenticationProfileTable
         (pWsscfgFsDot11AuthenticationProfileEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeFsDot11RadioConfigTable
* Input       : pWsscfgFsDot11RadioConfigEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
WsscfgInitializeFsDot11RadioConfigTable (tWsscfgFsDot11RadioConfigEntry *
                                         pWsscfgFsDot11RadioConfigEntry)
{
    if (pWsscfgFsDot11RadioConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((WsscfgInitializeMibFsDot11RadioConfigTable
         (pWsscfgFsDot11RadioConfigEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeFsDot11QosProfileTable
* Input       : pWsscfgFsDot11QosProfileEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
WsscfgInitializeFsDot11QosProfileTable (tWsscfgFsDot11QosProfileEntry *
                                        pWsscfgFsDot11QosProfileEntry)
{
    if (pWsscfgFsDot11QosProfileEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((WsscfgInitializeMibFsDot11QosProfileTable
         (pWsscfgFsDot11QosProfileEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeFsDot11WlanCapabilityProfileTable
* Input       : pWsscfgFsDot11WlanCapabilityProfileEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
WsscfgInitializeFsDot11WlanCapabilityProfileTable
    (tWsscfgFsDot11WlanCapabilityProfileEntry *
     pWsscfgFsDot11WlanCapabilityProfileEntry)
{
    if (pWsscfgFsDot11WlanCapabilityProfileEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((WsscfgInitializeMibFsDot11WlanCapabilityProfileTable
         (pWsscfgFsDot11WlanCapabilityProfileEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeFsDot11WlanAuthenticationProfileTable
* Input       : pWsscfgFsDot11WlanAuthenticationProfileEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
WsscfgInitializeFsDot11WlanAuthenticationProfileTable
    (tWsscfgFsDot11WlanAuthenticationProfileEntry *
     pWsscfgFsDot11WlanAuthenticationProfileEntry)
{
    if (pWsscfgFsDot11WlanAuthenticationProfileEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((WsscfgInitializeMibFsDot11WlanAuthenticationProfileTable
         (pWsscfgFsDot11WlanAuthenticationProfileEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeFsDot11WlanQosProfileTable
* Input       : pWsscfgFsDot11WlanQosProfileEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
WsscfgInitializeFsDot11WlanQosProfileTable (tWsscfgFsDot11WlanQosProfileEntry *
                                            pWsscfgFsDot11WlanQosProfileEntry)
{
    if (pWsscfgFsDot11WlanQosProfileEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((WsscfgInitializeMibFsDot11WlanQosProfileTable
         (pWsscfgFsDot11WlanQosProfileEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeFsDot11CapabilityMappingTable
* Input       : pWsscfgFsDot11CapabilityMappingEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
WsscfgInitializeFsDot11CapabilityMappingTable
    (tWsscfgFsDot11CapabilityMappingEntry *
     pWsscfgFsDot11CapabilityMappingEntry)
{
    if (pWsscfgFsDot11CapabilityMappingEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((WsscfgInitializeMibFsDot11CapabilityMappingTable
         (pWsscfgFsDot11CapabilityMappingEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeFsDot11AuthMappingTable
* Input       : pWsscfgFsDot11AuthMappingEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
WsscfgInitializeFsDot11AuthMappingTable (tWsscfgFsDot11AuthMappingEntry *
                                         pWsscfgFsDot11AuthMappingEntry)
{
    if (pWsscfgFsDot11AuthMappingEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((WsscfgInitializeMibFsDot11AuthMappingTable
         (pWsscfgFsDot11AuthMappingEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeFsDot11QosMappingTable
* Input       : pWsscfgFsDot11QosMappingEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
WsscfgInitializeFsDot11QosMappingTable (tWsscfgFsDot11QosMappingEntry *
                                        pWsscfgFsDot11QosMappingEntry)
{
    if (pWsscfgFsDot11QosMappingEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((WsscfgInitializeMibFsDot11QosMappingTable
         (pWsscfgFsDot11QosMappingEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeFsDot11WlanTable
* Input       : pWsscfgFsDot11WlanEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
WsscfgInitializeFsDot11WlanTable (tWsscfgFsDot11WlanEntry *
                                  pWsscfgFsDot11WlanEntry)
{
    if (pWsscfgFsDot11WlanEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((WsscfgInitializeMibFsDot11WlanTable (pWsscfgFsDot11WlanEntry))
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeFsDot11WlanBindTable
* Input       : pWsscfgFsDot11WlanBindEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
WsscfgInitializeFsDot11WlanBindTable (tWsscfgFsDot11WlanBindEntry *
                                      pWsscfgFsDot11WlanBindEntry)
{
    if (pWsscfgFsDot11WlanBindEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((WsscfgInitializeMibFsDot11WlanBindTable
         (pWsscfgFsDot11WlanBindEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeFsWtpImageUpgradeTable
* Input       : pWsscfgFsWtpImageUpgradeEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
WsscfgInitializeFsWtpImageUpgradeTable (tWsscfgFsWtpImageUpgradeEntry *
                                        pWsscfgFsWtpImageUpgradeEntry)
{
    if (pWsscfgFsWtpImageUpgradeEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((WsscfgInitializeMibFsWtpImageUpgradeTable
         (pWsscfgFsWtpImageUpgradeEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeFsRrmConfigTable
* Input       : pWsscfgFsRrmConfigEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
WsscfgInitializeFsRrmConfigTable (tWsscfgFsRrmConfigEntry *
                                  pWsscfgFsRrmConfigEntry)
{
    if (pWsscfgFsRrmConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((WsscfgInitializeMibFsRrmConfigTable (pWsscfgFsRrmConfigEntry))
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : 
* Input       : pWsscfgCapwapDot11WlanEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
WsscfgInitializeDot11OperationTable (tWsscfgDot11OperationEntry *
                                     pWsscfgDot11OperationEntry)
{
    if (pWsscfgDot11OperationEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    if ((WsscfgInitializeMibDot11OperationTable
         (pWsscfgDot11OperationEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
