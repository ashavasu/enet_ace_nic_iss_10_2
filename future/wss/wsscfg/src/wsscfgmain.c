/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: wsscfgmain.c,v 1.2 2017/05/23 14:16:56 siva Exp $
* Description: This file contains the wsscfg task main loop
*              and the initialization routines.
*
*******************************************************************/
#define __WSSCFGMAIN_C__
#include "wsscfginc.h"
#include "wssstawlcmacr.h"

tOsixSemId          configSemId;
/* Proto types of the functions private to this file only */

PRIVATE UINT4 WsscfgMainMemInit PROTO ((VOID));
PRIVATE VOID WsscfgMainMemClear PROTO ((VOID));

/****************************************************************************
*                                                                           *
* Function     : WsscfgMainTask                                             *
*                                                                           *
* Description  : Main function of WSSCFG.                                   *
*                                                                           *
* Input        : pTaskId                                                    *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : VOID                                                       *
*                                                                           *
*****************************************************************************/

PUBLIC UINT4
WsscfgMainTask ()
{
    if (WsscfgMainTaskInit () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    WsscfgInitProfileTables ();
    RegisterSTD802 ();
    RegisterSTDWLA ();
    RegisterFSWLAN ();
    RegisterFS11AC ();
#ifdef WLC_WANTED
    RegisterFSWSSL ();
#endif
#ifndef RFMGMT_WANTED
    RegisterFSRRM ();
#endif
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : WsscfgInitProfileTables                           */
/*  Description     : The function initializes the profile table        */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : None                                              */
/************************************************************************/
VOID
WsscfgInitProfileTables ()
{
    INT4                i4RetStatus = 0;
    i4RetStatus = WsscfgInitQAPProfileTable ();
    if (i4RetStatus != OSIX_SUCCESS)
    {
        /*Do Nothing */
    }
}

/************************************************************************/
/*  Function Name   : CapwapProcessClearConfigResp                      */
/*  Description     : The function initializes AP profile table         */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS|OSIX_FAILURE                         */
/************************************************************************/
INT4
WsscfgInitQAPProfileTable ()
{
    tWsscfgFsQAPProfileEntry WsscfgSetFsQAPProfileEntry;
    tWsscfgIsSetFsQAPProfileEntry WsscfgIsSetFsQAPProfileEntry;

    MEMSET (&WsscfgSetFsQAPProfileEntry, 0, sizeof (tWsscfgFsQAPProfileEntry));
    MEMSET (&WsscfgIsSetFsQAPProfileEntry, 0,
            sizeof (tWsscfgIsSetFsQAPProfileEntry));

    INT4                i4C = CLI_WSSCFG_BEST_EFFORT, i4Max =
        CLI_WSSCFG_BACKGROUND;
    UINT1               au1FsQAPProfileName[] = "default";

    for (i4C = CLI_WSSCFG_BEST_EFFORT; i4C <= i4Max; i4C++)
    {
        WsscfgSetFsQAPProfileEntry.MibObject.i4FsQAPProfileIndex = i4C;
        WsscfgIsSetFsQAPProfileEntry.bFsQAPProfileIndex = OSIX_TRUE;

        WsscfgSetFsQAPProfileEntry.MibObject.i4FsQAPProfileRowStatus =
            CREATE_AND_GO;
        WsscfgIsSetFsQAPProfileEntry.bFsQAPProfileRowStatus = OSIX_TRUE;

        MEMCPY (&(WsscfgSetFsQAPProfileEntry.MibObject.
                  au1FsQAPProfileName), &(au1FsQAPProfileName),
                STRLEN (au1FsQAPProfileName));
        WsscfgIsSetFsQAPProfileEntry.bFsQAPProfileName = OSIX_TRUE;

        WsscfgSetFsQAPProfileEntry.MibObject.i4FsQAPProfileNameLen =
            (INT4) (STRLEN (au1FsQAPProfileName));

        if (WsscfgSetAllFsQAPProfileTable
            (&WsscfgSetFsQAPProfileEntry,
             &WsscfgIsSetFsQAPProfileEntry) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : WsscfgMainTaskInit                                         *
*                                                                           *
* Description  : WSSCFG initialization routine.                             *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*                                                                           *
*****************************************************************************/

PUBLIC UINT4
WsscfgMainTaskInit (VOID)
{
    tWsscfgFsDot11QosProfileEntry WsscfgSetFsDot11QosProfileEntry;
    tWsscfgIsSetFsDot11QosProfileEntry WsscfgIsSetFsDot11QosProfileEntry;
    tWsscfgFsDot11AuthenticationProfileEntry
        WsscfgSetFsDot11AuthenticationProfileEntry;
    tWsscfgIsSetFsDot11AuthenticationProfileEntry
        WsscfgIsSetFsDot11AuthenticationProfileEntry;
    tWsscfgFsDot11CapabilityProfileEntry WsscfgSetFsDot11CapabilityProfileEntry;
    tWsscfgIsSetFsDot11CapabilityProfileEntry
        WsscfgIsSetFsDot11CapabilityProfileEntry;

    tSNMP_OCTET_STRING_TYPE WebAuthWebSuccMessage;
    UINT1               WebSuccMessage[WEB_AUTH_MAX_MSG_LEN];
    tSNMP_OCTET_STRING_TYPE WebAuthWebFailMessage;
    UINT1               WebFailMessage[WEB_AUTH_MAX_MSG_LEN];
    tSNMP_OCTET_STRING_TYPE WebAuthWebMessage;
    UINT1               WebMessage[WEB_AUTH_MAX_MSG_LEN];
    tSNMP_OCTET_STRING_TYPE WebAuthLoadBalMessage;
    UINT1               WebLoadBalMessage[WEB_AUTH_MAX_MSG_LEN];
    tSNMP_OCTET_STRING_TYPE WebAuthButtonText;
    UINT1               WebButtonText[WEB_AUTH_MAX_MSG_LEN];
    tSNMP_OCTET_STRING_TYPE WebAuthTitle;
    UINT1               WebTitle[WEB_AUTH_MAX_MSG_LEN];
    tSNMP_OCTET_STRING_TYPE WebAuthLogoFilename;
    UINT1               WebLogoFilename[WEB_AUTH_MAX_MSG_LEN];
    INT4                i4AuthDisplayColor = 0;
    INT4                i4AuthDisplayLang = 0;
    INT4                i4SetValFsSecurityWebAuthType = 0;

    MEMSET (&WebSuccMessage, 0, WEB_AUTH_MAX_MSG_LEN);
    STRCPY (WebSuccMessage, "Authenticated Successfully");
    MEMSET (&WebAuthWebSuccMessage, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    WebAuthWebSuccMessage.pu1_OctetList = WebSuccMessage;
    WebAuthWebSuccMessage.i4_Length = (INT4) (STRLEN (WebSuccMessage));

    i4AuthDisplayColor = 1;
    i4AuthDisplayLang = 1;
    i4SetValFsSecurityWebAuthType = WEB_AUTH_TYPE_INTERNAL;

    MEMSET (&WebLogoFilename, 0, WEB_AUTH_MAX_MSG_LEN);
    STRCPY (WebLogoFilename, "aricent_logo.jpg");
    MEMSET (&WebAuthLogoFilename, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    WebAuthLogoFilename.pu1_OctetList = WebLogoFilename;
    WebAuthLogoFilename.i4_Length = (INT4) (STRLEN (WebLogoFilename));

    MEMSET (&WebFailMessage, 0, WEB_AUTH_MAX_MSG_LEN);
    STRCPY (WebFailMessage, "Authentication Failed");
    MEMSET (&WebAuthWebFailMessage, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    WebAuthWebFailMessage.pu1_OctetList = WebFailMessage;
    WebAuthWebFailMessage.i4_Length = (INT4) (STRLEN (WebFailMessage));

    MEMSET (&WebMessage, 0, WEB_AUTH_MAX_MSG_LEN);
    STRCPY (WebMessage, "Hello welcome aboard!");
    MEMSET (&WebAuthWebMessage, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    WebAuthWebMessage.pu1_OctetList = WebMessage;
    WebAuthWebMessage.i4_Length = (INT4) (STRLEN (WebMessage));

    MEMSET (&WebLoadBalMessage, 0, WEB_AUTH_MAX_MSG_LEN);
    STRCPY (WebLoadBalMessage, "Sorry!!!Maximum no of users reached");
    MEMSET (&WebAuthLoadBalMessage, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    WebAuthLoadBalMessage.pu1_OctetList = WebLoadBalMessage;
    WebAuthLoadBalMessage.i4_Length = (INT4) (STRLEN (WebLoadBalMessage));

    MEMSET (&WebButtonText, 0, WEB_AUTH_MAX_MSG_LEN);
    STRCPY (WebButtonText, "Submit");
    MEMSET (&WebAuthButtonText, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    WebAuthButtonText.pu1_OctetList = WebButtonText;
    WebAuthButtonText.i4_Length = (INT4) (STRLEN (WebButtonText));

    MEMSET (&WebTitle, 0, WEB_AUTH_MAX_MSG_LEN);
    STRCPY (WebTitle, "Web Authentication");
    MEMSET (&WebAuthTitle, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    WebAuthTitle.pu1_OctetList = WebTitle;
    WebAuthTitle.i4_Length = (INT4) (STRLEN (WebTitle));

    MEMSET (&WsscfgSetFsDot11QosProfileEntry, 0,
            sizeof (tWsscfgFsDot11QosProfileEntry));
    MEMSET (&WsscfgIsSetFsDot11QosProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11QosProfileEntry));
    MEMSET (&WsscfgSetFsDot11AuthenticationProfileEntry, 0,
            sizeof (tWsscfgFsDot11AuthenticationProfileEntry));
    MEMSET (&WsscfgIsSetFsDot11AuthenticationProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11AuthenticationProfileEntry));
    MEMSET (&WsscfgSetFsDot11CapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11CapabilityProfileEntry));
    MEMSET (&WsscfgIsSetFsDot11CapabilityProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11CapabilityProfileEntry));

    WSSCFG_TRC_FUNC ((WSSCFG_FN_ENTRY, "FUNC:WsscfgMainTaskInit\n"));
#ifdef WSSCFG_TRACE_WANTED
    WSSCFG_TRC_FLAG = ~((UINT4) 0);
#endif

    MEMSET (&gWsscfgGlobals, 0, sizeof (gWsscfgGlobals));

    MEMCPY (gWsscfgGlobals.au1TaskSemName, WSSCFG_MUT_EXCL_SEM_NAME,
            OSIX_NAME_LEN);
    /* Initializing default value for WLAN Trap Status Object as per MIB defintion */
    gWsscfgGlobals.u4StationTrapStatus = WSS_WLAN_STATION_DEFAULT_TRAP_STATUS;

    if (OsixCreateSem
        (WSSCFG_MUT_EXCL_SEM_NAME, WSSCFG_SEM_CREATE_INIT_CNT, 0,
         &gWsscfgGlobals.wsscfgTaskSemId) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_MAIN_TRC, "Seamphore Creation failure for %s \n",
                     WSSCFG_MUT_EXCL_SEM_NAME));
        return OSIX_FAILURE;
    }
    /* Create buffer pools for data structures */
    if (WsscfgUtlCreateRBTree () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (WsscfgMainMemInit () == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_MAIN_TRC, "Memory Pool Creation Failed\n"));
        return OSIX_FAILURE;
    }
    /* Create Config Lock  */
    if (OsixCreateSem
        (WSSCFG_LOCK_SEM_NAME, WSSCFG_SEM_CREATE_INIT_CNT, 0,
         &configSemId) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_MAIN_TRC, "Config Lock Creation failure for %s \n",
                     WSSCFG_LOCK_SEM_NAME));
        return OSIX_FAILURE;

    }

    if (WsscfgUtlAddDefaultRBTree () != OSIX_SUCCESS)
    {
        WSSCFG_TRC ((WSSCFG_MAIN_TRC, "WsscfgAddDefaultRBTree : "
                     "Failed ,Default RB Tree Add Failed\n"));
        return OSIX_FAILURE;

    }

    WsscfgSetFsDot11QosProfileEntry.MibObject.i4FsDot11QosRowStatus
        = CREATE_AND_GO;
    WsscfgIsSetFsDot11QosProfileEntry.bFsDot11QosRowStatus = OSIX_TRUE;

    WsscfgSetFsDot11QosProfileEntry.MibObject.
        i4FsDot11QosProfileNameLen = (INT4) (STRLEN (WSS_DEF_PROFILE_NAME));

    MEMCPY (WsscfgSetFsDot11QosProfileEntry.
            MibObject.au1FsDot11QosProfileName, WSS_DEF_PROFILE_NAME,
            WsscfgSetFsDot11QosProfileEntry.
            MibObject.i4FsDot11QosProfileNameLen);
    WsscfgIsSetFsDot11QosProfileEntry.bFsDot11QosProfileName = OSIX_TRUE;

    WsscfgSetFsDot11QosProfileEntry.MibObject.i4FsDot11QosTraffic =
        CLI_WSSCFG_BEST_EFFORT;
    WsscfgIsSetFsDot11QosProfileEntry.bFsDot11QosTraffic = OSIX_TRUE;

    WsscfgSetFsDot11QosProfileEntry.
        MibObject.i4FsDot11QosPassengerTrustMode = CLI_WSSCFG_QOSPROF_DISABLE;
    WsscfgIsSetFsDot11QosProfileEntry.bFsDot11QosPassengerTrustMode = OSIX_TRUE;

    WsscfgSetFsDot11QosProfileEntry.MibObject.i4FsDot11QosRateLimit =
        CLI_WSSCFG_QOSPROF_DISABLE;
    WsscfgIsSetFsDot11QosProfileEntry.bFsDot11QosRateLimit = OSIX_TRUE;

    WsscfgSetFsDot11QosProfileEntry.MibObject.i4FsDot11UpStreamCIR =
        CLI_DEF_CIR_VAL;
    WsscfgIsSetFsDot11QosProfileEntry.bFsDot11UpStreamCIR = OSIX_TRUE;

    WsscfgSetFsDot11QosProfileEntry.MibObject.i4FsDot11UpStreamCBS =
        CLI_DEF_CBS_VAL;
    WsscfgIsSetFsDot11QosProfileEntry.bFsDot11UpStreamCBS = OSIX_TRUE;

    WsscfgSetFsDot11QosProfileEntry.MibObject.i4FsDot11UpStreamEIR =
        CLI_DEF_EIR_VAL;
    WsscfgIsSetFsDot11QosProfileEntry.bFsDot11UpStreamEIR = OSIX_TRUE;

    WsscfgSetFsDot11QosProfileEntry.MibObject.i4FsDot11UpStreamEBS =
        CLI_DEF_EBS_VAL;
    WsscfgIsSetFsDot11QosProfileEntry.bFsDot11UpStreamEBS = OSIX_TRUE;

    WsscfgSetFsDot11QosProfileEntry.MibObject.i4FsDot11DownStreamCIR =
        CLI_DEF_CIR_VAL;
    WsscfgIsSetFsDot11QosProfileEntry.bFsDot11DownStreamCIR = OSIX_TRUE;

    WsscfgSetFsDot11QosProfileEntry.MibObject.i4FsDot11DownStreamCBS =
        CLI_DEF_CBS_VAL;
    WsscfgIsSetFsDot11QosProfileEntry.bFsDot11DownStreamCBS = OSIX_TRUE;

    WsscfgSetFsDot11QosProfileEntry.MibObject.i4FsDot11DownStreamEIR =
        CLI_DEF_EIR_VAL;
    WsscfgIsSetFsDot11QosProfileEntry.bFsDot11DownStreamEIR = OSIX_TRUE;

    WsscfgSetFsDot11QosProfileEntry.MibObject.i4FsDot11DownStreamEBS =
        CLI_DEF_EBS_VAL;
    WsscfgIsSetFsDot11QosProfileEntry.bFsDot11DownStreamEBS = OSIX_TRUE;

    if (WsscfgSetAllFsDot11QosProfileTable (&WsscfgSetFsDot11QosProfileEntry,
                                            &WsscfgIsSetFsDot11QosProfileEntry,
                                            OSIX_TRUE,
                                            OSIX_TRUE) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&WsscfgSetFsDot11QosProfileEntry, 0,
            sizeof (tWsscfgFsDot11QosProfileEntry));
    WsscfgSetFsDot11QosProfileEntry.MibObject.i4FsDot11QosProfileNameLen
        = (INT4) (STRLEN (WSS_DEF_PROFILE_NAME));
    MEMCPY (WsscfgSetFsDot11QosProfileEntry.MibObject.au1FsDot11QosProfileName,
            WSS_DEF_PROFILE_NAME, WsscfgSetFsDot11QosProfileEntry.MibObject.
            i4FsDot11QosProfileNameLen);
    if (WsscfgGetAllFsDot11QosProfileTable (&WsscfgSetFsDot11QosProfileEntry)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    WsscfgSetFsDot11AuthenticationProfileEntry.
        MibObject.i4FsDot11AuthenticationRowStatus = CREATE_AND_GO;
    WsscfgIsSetFsDot11AuthenticationProfileEntry.bFsDot11AuthenticationRowStatus
        = OSIX_TRUE;
    WsscfgSetFsDot11AuthenticationProfileEntry.
        MibObject.i4FsDot11AuthenticationProfileNameLen =
        (INT4) (STRLEN (WSS_DEF_PROFILE_NAME));

    MEMCPY (WsscfgSetFsDot11AuthenticationProfileEntry.
            MibObject.au1FsDot11AuthenticationProfileName, WSS_DEF_PROFILE_NAME,
            WsscfgSetFsDot11AuthenticationProfileEntry.
            MibObject.i4FsDot11AuthenticationProfileNameLen);
    WsscfgIsSetFsDot11AuthenticationProfileEntry.
        bFsDot11AuthenticationProfileName = OSIX_TRUE;
    WsscfgSetFsDot11AuthenticationProfileEntry.MibObject.
        i4FsDot11AuthenticationAlgorithm = CLI_AUTH_ALGO_OPEN;
    WsscfgIsSetFsDot11AuthenticationProfileEntry.
        bFsDot11AuthenticationAlgorithm = OSIX_TRUE;

    if (WsscfgSetAllFsDot11AuthenticationProfileTable
        (&WsscfgSetFsDot11AuthenticationProfileEntry,
         &WsscfgIsSetFsDot11AuthenticationProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    WsscfgSetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11CapabilityRowStatus = CREATE_AND_GO;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11CapabilityRowStatus = OSIX_TRUE;
    WsscfgSetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11CapabilityProfileNameLen =
        (INT4) (STRLEN (WSS_DEF_PROFILE_NAME));
    MEMCPY (WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
            au1FsDot11CapabilityProfileName, WSS_DEF_PROFILE_NAME,
            WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
            i4FsDot11CapabilityProfileNameLen);
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11CapabilityProfileName =
        OSIX_TRUE;

    WsscfgSetFsDot11CapabilityProfileEntry.MibObject.
        i4FsDot11CFPollable = WSSCFG_DISABLED;
    WsscfgSetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11CFPollRequest = WSSCFG_DISABLED;
    WsscfgSetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11PrivacyOptionImplemented = WSSCFG_DISABLED;
    WsscfgSetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11ShortPreambleOptionImplemented = WSSCFG_DISABLED;
    WsscfgSetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11PBCCOptionImplemented = WSSCFG_DISABLED;
    WsscfgSetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11ChannelAgilityPresent = WSSCFG_DISABLED;
    WsscfgSetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11QosOptionImplemented = WSSCFG_DISABLED;
    WsscfgSetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11SpectrumManagementRequired = WSSCFG_DISABLED;
    WsscfgSetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11ShortSlotTimeOptionImplemented = WSSCFG_DISABLED;
    WsscfgSetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11APSDOptionImplemented = WSSCFG_DISABLED;
    WsscfgSetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11DSSSOFDMOptionEnabled = WSSCFG_DISABLED;
    WsscfgSetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11DelayedBlockAckOptionImplemented = WSSCFG_DISABLED;
    WsscfgSetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11ImmediateBlockAckOptionImplemented = WSSCFG_DISABLED;
    WsscfgSetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11QAckOptionImplemented = WSSCFG_DISABLED;
    WsscfgSetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11QueueRequestOptionImplemented = WSSCFG_DISABLED;
    WsscfgSetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11TXOPRequestOptionImplemented = WSSCFG_DISABLED;
    WsscfgSetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11RSNAOptionImplemented = WSSCFG_DISABLED;
    WsscfgSetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11RSNAPreauthenticationImplemented = WSSCFG_DISABLED;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11CFPollable = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11CFPollRequest = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11PrivacyOptionImplemented
        = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11ShortPreambleOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11PBCCOptionImplemented =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11ChannelAgilityPresent =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11QosOptionImplemented =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11SpectrumManagementRequired = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11ShortSlotTimeOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11APSDOptionImplemented =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11DSSSOFDMOptionEnabled =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11DelayedBlockAckOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11ImmediateBlockAckOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11QAckOptionImplemented =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11QueueRequestOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11TXOPRequestOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.bFsDot11RSNAOptionImplemented =
        OSIX_TRUE;
    WsscfgIsSetFsDot11CapabilityProfileEntry.
        bFsDot11RSNAPreauthenticationImplemented = OSIX_TRUE;

    if (WsscfgSetAllFsDot11CapabilityProfileTable
        (&WsscfgSetFsDot11CapabilityProfileEntry,
         &WsscfgIsSetFsDot11CapabilityProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /*Set the default values for WebAuthentication */
    if (nmhSetFsSecurityWebAuthType (i4SetValFsSecurityWebAuthType)
        != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (nmhSetFsSecurityWebAuthWebSuccMessage (&WebAuthWebSuccMessage)
        != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsSecurityWebAuthWebFailMessage (&WebAuthWebFailMessage)
        != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsSecurityWebAuthWebMessage (&WebAuthWebMessage) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsSecurityWebAuthWebButtonText (&WebAuthButtonText) !=
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsSecurityWebAuthWebLoadBalInfo (&WebAuthLoadBalMessage)
        != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsSecurityWebAuthWebTitle (&WebAuthTitle) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsSecurityWebAuthWebLogoFileName (&WebAuthLogoFilename)
        != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsSecurityWebAuthDisplayLang (i4AuthDisplayLang) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (nmhSetFsSecurityWebAuthColor (i4AuthDisplayColor) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    WSSCFG_TRC_FUNC ((WSSCFG_FN_EXIT, "FUNC:WsscfgMainTaskInit\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : WsscfgMainDeInit                                           */
/*                                                                           */
/* Description  : Deleting the resources when task init fails.               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
WsscfgMainDeInit (VOID)
{

    WsscfgMainMemClear ();

    if (gWsscfgGlobals.wsscfgTaskSemId)
    {
        OsixSemDel (gWsscfgGlobals.wsscfgTaskSemId);
    }
    if (gWsscfgGlobals.wsscfgQueId)
    {
        OsixQueDel (gWsscfgGlobals.wsscfgQueId);
    }
    TmrDeleteTimerList (gWsscfgGlobals.wsscfgTmrLst);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : WsscfgMainTaskLock                                         */
/*                                                                           */
/* Description  : Lock the Wsscfg Main Task                                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
WsscfgMainTaskLock (VOID)
{
    WSSCFG_TRC_FUNC ((WSSCFG_FN_ENTRY, "FUNC:WsscfgMainTaskLock\n"));

    if (OsixSemTake (gWsscfgGlobals.wsscfgTaskSemId) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_MAIN_TRC, "TakeSem failure for %s \n",
                     WSSCFG_MUT_EXCL_SEM_NAME));
        return SNMP_FAILURE;
    }

    WSSCFG_TRC_FUNC ((WSSCFG_FN_EXIT, "EXIT:WsscfgMainTaskLock\n"));
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : WsscfgMainTaskUnLock                                       */
/*                                                                           */
/* Description  : UnLock the WSSCFG Task                                     */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
WsscfgMainTaskUnLock (VOID)
{
    WSSCFG_TRC_FUNC ((WSSCFG_FN_ENTRY, "FUNC:WsscfgMainTaskUnLock\n"));

    OsixSemGive (gWsscfgGlobals.wsscfgTaskSemId);

    WSSCFG_TRC_FUNC ((WSSCFG_FN_EXIT, "EXIT:WsscfgMainTaskUnLock\n"));
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : WsscfgMainMemInit                                          */
/*                                                                           */
/* Description  : Allocates all the memory that is required for WSSCFG       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT4
WsscfgMainMemInit (VOID)
{
    if (WsscfgSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : WsscfgMainMemClear                                         */
/*                                                                           */
/* Description  : Clears all the Memory                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
WsscfgMainMemClear (VOID)
{
    WsscfgSizingMemDeleteMemPools ();
}

/*****************************************************************************/
/*                                                                           */
/* Function     : WssCfgLock                                                 */
/*                                                                           */
/* Description  : Take Config Lock                                           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
INT4
WssCfgLock (VOID)
{
    if (OsixSemTake (configSemId) != OSIX_SUCCESS)
    {
        WSSCFG_TRC ((WSSCFG_MAIN_TRC, "Config Lock Take failure for %s \n",
                     WSSCFG_LOCK_SEM_NAME));
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : WssCfgUnLock                                               */
/*                                                                           */
/* Description  : Give Config Lock                                           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
INT4
WssCfgUnLock (VOID)
{
    if (OsixSemGive (configSemId) != OSIX_SUCCESS)
    {
        WSSCFG_TRC ((WSSCFG_MAIN_TRC, "Config Lock Give failure for %s \n",
                     WSSCFG_LOCK_SEM_NAME));
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  wsscfgmain.c                     */
/*-----------------------------------------------------------------------*/
