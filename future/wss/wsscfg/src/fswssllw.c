/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fswssllw.c,v 1.3 2018/01/04 09:54:33 siva Exp $
 *
 * Description: Protocol Low Level Routines
 *********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fswssllw.h"
# include  "wsscfginc.h"
# include  "wsscfgcli.h"
# include  "fwldefn.h"
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsWlanDhcpServerStatus
 Input       :  The Indices

                The Object 
                retValFsWlanDhcpServerStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWlanDhcpServerStatus (INT4 *pi4RetValFsWlanDhcpServerStatus)
{
    *pi4RetValFsWlanDhcpServerStatus = WssIfDhcpServerStatusGet ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWlanDhcpRelayStatus
 Input       :  The Indices

                The Object 
                retValFsWlanDhcpRelayStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWlanDhcpRelayStatus (INT4 *pi4RetValFsWlanDhcpRelayStatus)
{
    *pi4RetValFsWlanDhcpRelayStatus = WssIfDhcpRelayStatusGet ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWlanDhcpNextSrvIpAddr
 Input       :  The Indices

                The Object 
                retValFsWlanDhcpNextSrvIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWlanDhcpNextSrvIpAddr (UINT4 *pu4RetValFsWlanDhcpNextSrvIpAddr)
{
    *pu4RetValFsWlanDhcpNextSrvIpAddr = WssIfDhcpNextSrvIpAddrGet ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWlanFirewallStatus
 Input       :  The Indices

                The Object 
                retValFsWlanFirewallStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWlanFirewallStatus (INT4 *pi4RetValFsWlanFirewallStatus)
{
    *pi4RetValFsWlanFirewallStatus = WssIfWtpFirewallStatusGet ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWlanNATStatus
 Input       :  The Indices

                The Object 
                retValFsWlanNATStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWlanNATStatus (INT4 *pi4RetValFsWlanNATStatus)
{
    *pi4RetValFsWlanNATStatus = WssIfWtpNATStatusGet ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWlanDNSServerIpAddr
 Input       :  The Indices

                The Object 
                retValFsWlanDNSServerIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWlanDNSServerIpAddr (UINT4 *pu4RetValFsWlanDNSServerIpAddr)
{
    *pu4RetValFsWlanDNSServerIpAddr = WssIfWlanDNSServerIpAddrGet ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWlanDefaultRouterIpAddr
 Input       :  The Indices

                The Object 
                retValFsWlanDefaultRouterIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWlanDefaultRouterIpAddr (UINT4 *pu4RetValFsWlanDefaultRouterIpAddr)
{
    *pu4RetValFsWlanDefaultRouterIpAddr = WssIfWlanDefaultRouterIpAddrGet ();
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsWlanDhcpServerStatus
 Input       :  The Indices

                The Object 
                setValFsWlanDhcpServerStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWlanDhcpServerStatus (INT4 i4SetValFsWlanDhcpServerStatus)
{
    WssIfDhcpServerStatusSet (i4SetValFsWlanDhcpServerStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsWlanDhcpRelayStatus
 Input       :  The Indices

                The Object 
                setValFsWlanDhcpRelayStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWlanDhcpRelayStatus (INT4 i4SetValFsWlanDhcpRelayStatus)
{
    WssIfDhcpRelayStatusSet (i4SetValFsWlanDhcpRelayStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsWlanDhcpNextSrvIpAddr
 Input       :  The Indices

                The Object 
                setValFsWlanDhcpNextSrvIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWlanDhcpNextSrvIpAddr (UINT4 u4SetValFsWlanDhcpNextSrvIpAddr)
{

    WssIfDhcpNextSrvIpAddrSet (u4SetValFsWlanDhcpNextSrvIpAddr);

    /* For all the profiles for which DHCP Relay is enabled send the
     * next server IP information */
    if (WssifStartDhcpRelayUpdateReq (0, 0,
                                      u4SetValFsWlanDhcpNextSrvIpAddr) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsWlanFirewallStatus
 Input       :  The Indices

                The Object 
                setValFsWlanFirewallStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWlanFirewallStatus (INT4 i4SetValFsWlanFirewallStatus)
{
    WssIfWtpFirewallStatusSet (i4SetValFsWlanFirewallStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsWlanNATStatus
 Input       :  The Indices

                The Object 
                setValFsWlanNATStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWlanNATStatus (INT4 i4SetValFsWlanNATStatus)
{
    WssIfWtpNATStatusSet (i4SetValFsWlanNATStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsWlanDNSServerIpAddr
 Input       :  The Indices

                The Object 
                setValFsWlanDNSServerIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWlanDNSServerIpAddr (UINT4 u4SetValFsWlanDNSServerIpAddr)
{
    WssIfWlanDNSServerIpAddrSet (u4SetValFsWlanDNSServerIpAddr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsWlanDefaultRouterIpAddr
 Input       :  The Indices

                The Object 
                setValFsWlanDefaultRouterIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWlanDefaultRouterIpAddr (UINT4 u4SetValFsWlanDefaultRouterIpAddr)
{
    WssIfWlanDefaultRouterIpAddrSet (u4SetValFsWlanDefaultRouterIpAddr);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsWlanDhcpServerStatus
 Input       :  The Indices

                The Object 
                testValFsWlanDhcpServerStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWlanDhcpServerStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsWlanDhcpServerStatus)
{
    INT4                i4RetValFsWlanDhcpRelayStatus =
        WssIfDhcpRelayStatusGet ();
    if (i4RetValFsWlanDhcpRelayStatus == 1)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsWlanDhcpServerStatus != 1) &&
        (i4TestValFsWlanDhcpServerStatus != 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWlanDhcpRelayStatus
 Input       :  The Indices

                The Object 
                testValFsWlanDhcpRelayStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWlanDhcpRelayStatus (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsWlanDhcpRelayStatus)
{
    INT4                i4RetValFsWlanDhcpServerStatus =
        WssIfDhcpServerStatusGet ();
    if (i4RetValFsWlanDhcpServerStatus == 1)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsWlanDhcpRelayStatus != 1) &&
        (i4TestValFsWlanDhcpRelayStatus != 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWlanDhcpNextSrvIpAddr
 Input       :  The Indices

                The Object 
                testValFsWlanDhcpNextSrvIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWlanDhcpNextSrvIpAddr (UINT4 *pu4ErrorCode,
                                  UINT4 u4TestValFsWlanDhcpNextSrvIpAddr)
{
    if (u4TestValFsWlanDhcpNextSrvIpAddr != 0)
    {
        if (CFA_IS_LOOPBACK (u4TestValFsWlanDhcpNextSrvIpAddr))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if (!((CFA_IS_ADDR_CLASS_A (u4TestValFsWlanDhcpNextSrvIpAddr) ?
               (IP_IS_ZERO_NETWORK (u4TestValFsWlanDhcpNextSrvIpAddr) ? 0 : 1) :
               0) || (CFA_IS_ADDR_CLASS_B (u4TestValFsWlanDhcpNextSrvIpAddr))
              || (CFA_IS_ADDR_CLASS_C (u4TestValFsWlanDhcpNextSrvIpAddr))
              || (CFA_IS_ADDR_CLASS_E (u4TestValFsWlanDhcpNextSrvIpAddr))))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsWlanFirewallStatus
 Input       :  The Indices

                The Object 
                testValFsWlanFirewallStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWlanFirewallStatus (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsWlanFirewallStatus)
{
    if ((i4TestValFsWlanFirewallStatus != 0) &&
        (i4TestValFsWlanFirewallStatus != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWlanNATStatus
 Input       :  The Indices

                The Object 
                testValFsWlanNATStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWlanNATStatus (UINT4 *pu4ErrorCode, INT4 i4TestValFsWlanNATStatus)
{
    if ((i4TestValFsWlanNATStatus != WAN_TYPE_PRIVATE) &&
        (i4TestValFsWlanNATStatus != WAN_TYPE_PUBLIC))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWlanDNSServerIpAddr
 Input       :  The Indices

                The Object 
                testValFsWlanDNSServerIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWlanDNSServerIpAddr (UINT4 *pu4ErrorCode,
                                UINT4 u4TestValFsWlanDNSServerIpAddr)
{
    if (u4TestValFsWlanDNSServerIpAddr != 0)
    {
        if (CFA_IS_LOOPBACK (u4TestValFsWlanDNSServerIpAddr))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if (!((CFA_IS_ADDR_CLASS_A (u4TestValFsWlanDNSServerIpAddr) ?
               (IP_IS_ZERO_NETWORK (u4TestValFsWlanDNSServerIpAddr) ? 0 : 1) :
               0) || (CFA_IS_ADDR_CLASS_B (u4TestValFsWlanDNSServerIpAddr))
              || (CFA_IS_ADDR_CLASS_C (u4TestValFsWlanDNSServerIpAddr))
              || (CFA_IS_ADDR_CLASS_E (u4TestValFsWlanDNSServerIpAddr))))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsWlanDefaultRouterIpAddr
 Input       :  The Indices

                The Object 
                testValFsWlanDefaultRouterIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWlanDefaultRouterIpAddr (UINT4 *pu4ErrorCode,
                                    UINT4 u4TestValFsWlanDefaultRouterIpAddr)
{
    if (u4TestValFsWlanDefaultRouterIpAddr != 0)
    {
        if (CFA_IS_LOOPBACK (u4TestValFsWlanDefaultRouterIpAddr))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if (!((CFA_IS_ADDR_CLASS_A (u4TestValFsWlanDefaultRouterIpAddr) ?
               (IP_IS_ZERO_NETWORK (u4TestValFsWlanDefaultRouterIpAddr) ? 0 : 1)
               : 0)
              || (CFA_IS_ADDR_CLASS_B (u4TestValFsWlanDefaultRouterIpAddr))
              || (CFA_IS_ADDR_CLASS_C (u4TestValFsWlanDefaultRouterIpAddr))
              || (CFA_IS_ADDR_CLASS_E (u4TestValFsWlanDefaultRouterIpAddr))))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsWlanDhcpServerStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsWlanDhcpServerStatus (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsWlanDhcpRelayStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsWlanDhcpRelayStatus (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsWlanDhcpNextSrvIpAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsWlanDhcpNextSrvIpAddr (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsWlanFirewallStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsWlanFirewallStatus (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsWlanNATStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsWlanNATStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsWlanDNSServerIpAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsWlanDNSServerIpAddr (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsWlanDefaultRouterIpAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsWlanDefaultRouterIpAddr (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWlanRadioIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWlanRadioIfTable
 Input       :  The Indices
                IfIndex
                CapwapDot11WlanProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWlanRadioIfTable (INT4 i4IfIndex,
                                            UINT4 u4CapwapDot11WlanProfileId)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4CapwapDot11WlanProfileId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsWlanRadioIfTable
 Input       :  The Indices
                IfIndex
                CapwapDot11WlanProfileId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsWlanRadioIfTable (INT4 *pi4IfIndex,
                                    UINT4 *pu4CapwapDot11WlanProfileId)
{
    tWsscfgCapwapDot11WlanBindEntry *pWsscfgCapwapDot11WlanBindEntry = NULL;

    pWsscfgCapwapDot11WlanBindEntry = WsscfgGetFirstCapwapDot11WlanBindTable ();

    if (pWsscfgCapwapDot11WlanBindEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pi4IfIndex = pWsscfgCapwapDot11WlanBindEntry->MibObject.i4IfIndex;

    *pu4CapwapDot11WlanProfileId =
        pWsscfgCapwapDot11WlanBindEntry->MibObject.u4CapwapDot11WlanProfileId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsWlanRadioIfTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                CapwapDot11WlanProfileId
                nextCapwapDot11WlanProfileId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsWlanRadioIfTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                   UINT4 u4CapwapDot11WlanProfileId,
                                   UINT4 *pu4NextCapwapDot11WlanProfileId)
{
    tWsscfgCapwapDot11WlanBindEntry WsscfgCapwapDot11WlanBindEntry;
    tWsscfgCapwapDot11WlanBindEntry
        * pNextWsscfgCapwapDot11WlanBindEntry = NULL;

    MEMSET (&WsscfgCapwapDot11WlanBindEntry, 0,
            sizeof (tWsscfgCapwapDot11WlanBindEntry));

    /* Assign the index */
    WsscfgCapwapDot11WlanBindEntry.MibObject.i4IfIndex = i4IfIndex;

    WsscfgCapwapDot11WlanBindEntry.MibObject.
        u4CapwapDot11WlanProfileId = u4CapwapDot11WlanProfileId;

    pNextWsscfgCapwapDot11WlanBindEntry =
        WsscfgGetNextCapwapDot11WlanBindTable (&WsscfgCapwapDot11WlanBindEntry);

    if (pNextWsscfgCapwapDot11WlanBindEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pi4NextIfIndex = pNextWsscfgCapwapDot11WlanBindEntry->MibObject.i4IfIndex;

    *pu4NextCapwapDot11WlanProfileId =
        pNextWsscfgCapwapDot11WlanBindEntry->MibObject.
        u4CapwapDot11WlanProfileId;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsWlanRadioIfIpAddr
 Input       :  The Indices
                IfIndex
                CapwapDot11WlanProfileId

                The Object 
                retValFsWlanRadioIfIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWlanRadioIfIpAddr (INT4 i4IfIndex, UINT4 u4CapwapDot11WlanProfileId,
                           UINT4 *pu4RetValFsWlanRadioIfIpAddr)
{
    tWssWlanDB         *pWssWlanDB;

    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));

    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId =
        (UINT2) u4CapwapDot11WlanProfileId;
    pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return SNMP_FAILURE;
    }

    /* Get WlanInternalId from BssInterfaceDB */
    pWssWlanDB->WssWlanIsPresentDB.bWlanInterfaceIp = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return SNMP_FAILURE;
    }
    *pu4RetValFsWlanRadioIfIpAddr =
        pWssWlanDB->WssWlanAttributeDB.WlanInterfaceIp.u4IpAddr;

    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWlanRadioIfIpSubnetMask
 Input       :  The Indices
                IfIndex
                CapwapDot11WlanProfileId

                The Object 
                retValFsWlanRadioIfIpSubnetMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWlanRadioIfIpSubnetMask (INT4 i4IfIndex,
                                 UINT4 u4CapwapDot11WlanProfileId,
                                 UINT4 *pu4RetValFsWlanRadioIfIpSubnetMask)
{
    tWssWlanDB         *pWssWlanDB;

    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));

    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId =
        (UINT2) u4CapwapDot11WlanProfileId;
    pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return SNMP_FAILURE;
    }

    /* Get WlanInternalId from BssInterfaceDB */
    pWssWlanDB->WssWlanIsPresentDB.bWlanInterfaceIp = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return SNMP_FAILURE;
    }
    *pu4RetValFsWlanRadioIfIpSubnetMask =
        pWssWlanDB->WssWlanAttributeDB.WlanInterfaceIp.u4SubMask;

    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWlanRadioIfAdminStatus
 Input       :  The Indices
                IfIndex
                CapwapDot11WlanProfileId

                The Object 
                retValFsWlanRadioIfAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWlanRadioIfAdminStatus (INT4 i4IfIndex,
                                UINT4 u4CapwapDot11WlanProfileId,
                                INT4 *pi4RetValFsWlanRadioIfAdminStatus)
{
    tWssWlanDB         *pWssWlanDB;

    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));

    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId =
        (UINT2) u4CapwapDot11WlanProfileId;
    pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return SNMP_FAILURE;
    }

    /* Get WlanInternalId from BssInterfaceDB */
    pWssWlanDB->WssWlanIsPresentDB.bWlanInterfaceIp = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return SNMP_FAILURE;
    }
    *pi4RetValFsWlanRadioIfAdminStatus =
        (INT4) pWssWlanDB->WssWlanAttributeDB.WlanInterfaceIp.u1AdminStatus;

    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWlanRadioIfRowStatus
 Input       :  The Indices
                IfIndex
                CapwapDot11WlanProfileId

                The Object 
                retValFsWlanRadioIfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWlanRadioIfRowStatus (INT4 i4IfIndex, UINT4 u4CapwapDot11WlanProfileId,
                              INT4 *pi4RetValFsWlanRadioIfRowStatus)
{
    tWssWlanDB         *pWssWlanDB;

    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));

    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId =
        (UINT2) u4CapwapDot11WlanProfileId;
    pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    pWssWlanDB->WssWlanIsPresentDB.bWlanId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return SNMP_FAILURE;
    }

    /* Get WlanInternalId from BssInterfaceDB */
    pWssWlanDB->WssWlanIsPresentDB.bWlanInterfaceIp = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return SNMP_FAILURE;
    }
    *pi4RetValFsWlanRadioIfRowStatus =
        (INT4) pWssWlanDB->WssWlanAttributeDB.WlanInterfaceIp.u1RowStatus;

    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsWlanRadioIfIpAddr
 Input       :  The Indices
                IfIndex
                CapwapDot11WlanProfileId

                The Object 
                setValFsWlanRadioIfIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWlanRadioIfIpAddr (INT4 i4IfIndex, UINT4 u4CapwapDot11WlanProfileId,
                           UINT4 u4SetValFsWlanRadioIfIpAddr)
{
    tWssWlanDB         *pWssWlanDB;
    INT4                i4RetValFsWlanRadioIfRowStatus = 0;
    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        return SNMP_FAILURE;
    }
    nmhGetFsWlanRadioIfRowStatus (i4IfIndex, u4CapwapDot11WlanProfileId,
                                  &i4RetValFsWlanRadioIfRowStatus);
    if (i4RetValFsWlanRadioIfRowStatus == ACTIVE)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return SNMP_FAILURE;
    }
    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));

    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId =
        (UINT2) u4CapwapDot11WlanProfileId;
    pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return SNMP_FAILURE;
    }
    /* Get WlanInternalId from BssInterfaceDB */
    pWssWlanDB->WssWlanIsPresentDB.bWlanInterfaceIp = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return SNMP_FAILURE;
    }

    pWssWlanDB->WssWlanAttributeDB.WlanInterfaceIp.u4IpAddr =
        u4SetValFsWlanRadioIfIpAddr;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_BSS_IFINDEX_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return SNMP_FAILURE;
    }
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsWlanRadioIfIpSubnetMask
 Input       :  The Indices
                IfIndex
                CapwapDot11WlanProfileId

                The Object 
                setValFsWlanRadioIfIpSubnetMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWlanRadioIfIpSubnetMask (INT4 i4IfIndex,
                                 UINT4 u4CapwapDot11WlanProfileId,
                                 UINT4 u4SetValFsWlanRadioIfIpSubnetMask)
{
    tWssWlanDB         *pWssWlanDB;
    INT4                i4RetValFsWlanRadioIfRowStatus = 0;
    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        return SNMP_FAILURE;
    }
    nmhGetFsWlanRadioIfRowStatus (i4IfIndex, u4CapwapDot11WlanProfileId,
                                  &i4RetValFsWlanRadioIfRowStatus);
    if (i4RetValFsWlanRadioIfRowStatus == ACTIVE)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return SNMP_FAILURE;
    }

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));

    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId =
        (UINT2) u4CapwapDot11WlanProfileId;
    pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return SNMP_FAILURE;
    }
    /* Get WlanInternalId from BssInterfaceDB */
    pWssWlanDB->WssWlanIsPresentDB.bWlanInterfaceIp = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return SNMP_FAILURE;
    }

    pWssWlanDB->WssWlanAttributeDB.WlanInterfaceIp.u4SubMask =
        u4SetValFsWlanRadioIfIpSubnetMask;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_BSS_IFINDEX_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return SNMP_FAILURE;
    }
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsWlanRadioIfAdminStatus
 Input       :  The Indices
                IfIndex
                CapwapDot11WlanProfileId

                The Object 
                setValFsWlanRadioIfAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWlanRadioIfAdminStatus (INT4 i4IfIndex,
                                UINT4 u4CapwapDot11WlanProfileId,
                                INT4 i4SetValFsWlanRadioIfAdminStatus)
{
    tWssWlanDB         *pWssWlanDB;

    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));

    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId =
        (UINT2) u4CapwapDot11WlanProfileId;
    pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return SNMP_FAILURE;
    }

    /* Get WlanInternalId from BssInterfaceDB */
    pWssWlanDB->WssWlanIsPresentDB.bWlanInterfaceIp = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return SNMP_FAILURE;
    }
    pWssWlanDB->WssWlanAttributeDB.WlanInterfaceIp.u1AdminStatus =
        (UINT1) i4SetValFsWlanRadioIfAdminStatus;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_BSS_IFINDEX_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return SNMP_FAILURE;
    }
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsWlanRadioIfRowStatus
 Input       :  The Indices
                IfIndex
                CapwapDot11WlanProfileId

                The Object 
                setValFsWlanRadioIfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWlanRadioIfRowStatus (INT4 i4IfIndex, UINT4 u4CapwapDot11WlanProfileId,
                              INT4 i4SetValFsWlanRadioIfRowStatus)
{
    tWssWlanDB         *pWssWlanDB;
    UINT4               u4RetValFsWlanRadioIfIpSubnetMask = 0;
    UINT4               u4RetValFsWlanRadioIfIpAddr = 0;
    INT4                i4RetValFsWlanRadioIfRowStatus = 0;
    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));

    pWssWlanDB->WssWlanAttributeDB.u2WlanProfileId = (UINT2)
        u4CapwapDot11WlanProfileId;
    pWssWlanDB->WssWlanAttributeDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    pWssWlanDB->WssWlanIsPresentDB.bBssIfIndex = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return SNMP_FAILURE;
    }

    /* Get WlanInternalId from BssInterfaceDB */
    pWssWlanDB->WssWlanIsPresentDB.bWlanInterfaceIp = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return SNMP_FAILURE;
    }
    pWssWlanDB->WssWlanAttributeDB.WlanInterfaceIp.u1RowStatus =
        i4SetValFsWlanRadioIfRowStatus;
    pWssWlanDB->WssWlanAttributeDB.WlanInterfaceIp.u1AdminStatus = 2;    /*Default admin status is down(2) */
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_BSS_IFINDEX_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return SNMP_FAILURE;
    }
    if (nmhGetFsWlanRadioIfIpSubnetMask (i4IfIndex, u4CapwapDot11WlanProfileId,
                                         &u4RetValFsWlanRadioIfIpSubnetMask) ==
        SNMP_SUCCESS)
    {
        if (nmhGetFsWlanRadioIfIpAddr (i4IfIndex, u4CapwapDot11WlanProfileId,
                                       &u4RetValFsWlanRadioIfIpAddr) ==
            SNMP_SUCCESS)
        {
            if ((u4RetValFsWlanRadioIfIpAddr != 0) &&
                (u4RetValFsWlanRadioIfIpSubnetMask != 0))
            {
                nmhGetFsWlanRadioIfRowStatus (i4IfIndex,
                                              u4CapwapDot11WlanProfileId,
                                              &i4RetValFsWlanRadioIfRowStatus);
                if (i4RetValFsWlanRadioIfRowStatus == ACTIVE)
                {
                    WssIfWlanRadioIfIpCheck (i4IfIndex,
                                             u4CapwapDot11WlanProfileId);
                }
            }
        }
    }
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
Function    :  nmhTestv2FsWlanRadioIfIpAddr
Input       :  The Indices
IfIndex
CapwapDot11WlanProfileId

The Object 
testValFsWlanRadioIfIpAddr
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWlanRadioIfIpAddr (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                              UINT4 u4CapwapDot11WlanProfileId,
                              UINT4 u4TestValFsWlanRadioIfIpAddr)
{
    INT4                i4RowStatus = 0;

    if (nmhGetCapwapDot11WlanBindRowStatus (i4IfIndex,
                                            u4CapwapDot11WlanProfileId,
                                            &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (u4TestValFsWlanRadioIfIpAddr != 0)
    {
        if (CFA_IS_LOOPBACK (u4TestValFsWlanRadioIfIpAddr))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if (!((CFA_IS_ADDR_CLASS_A (u4TestValFsWlanRadioIfIpAddr) ?
               (IP_IS_ZERO_NETWORK (u4TestValFsWlanRadioIfIpAddr) ? 0 : 1) : 0)
              || (CFA_IS_ADDR_CLASS_B (u4TestValFsWlanRadioIfIpAddr))
              || (CFA_IS_ADDR_CLASS_C (u4TestValFsWlanRadioIfIpAddr))
              || (CFA_IS_ADDR_CLASS_E (u4TestValFsWlanRadioIfIpAddr))))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2FsWlanRadioIfIpSubnetMask
Input       :  The Indices
IfIndex
                CapwapDot11WlanProfileId

                The Object 
                testValFsWlanRadioIfIpSubnetMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWlanRadioIfIpSubnetMask (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                    UINT4 u4CapwapDot11WlanProfileId,
                                    UINT4 u4TestValFsWlanRadioIfIpSubnetMask)
{
    UINT1               u1RetValue;
    INT4                i4RowStatus = 0;

    if (nmhGetCapwapDot11WlanBindRowStatus (i4IfIndex,
                                            u4CapwapDot11WlanProfileId,
                                            &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    u1RetValue = CfaGetCidrSubnetMaskIndex (u4TestValFsWlanRadioIfIpSubnetMask);
    /* The valid subnet address is from 0 to 32 */
    if (u1RetValue != 0)
    {
        if ((u1RetValue >= CFA_MAX_CIDR))
        {
            /* Loopback interface mask should be 32 bit mask. */
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4CapwapDot11WlanProfileId);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsWlanRadioIfAdminStatus
 Input       :  The Indices
                IfIndex
                CapwapDot11WlanProfileId

                The Object 
                testValFsWlanRadioIfAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWlanRadioIfAdminStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                   UINT4 u4CapwapDot11WlanProfileId,
                                   INT4 i4TestValFsWlanRadioIfAdminStatus)
{
    INT4                i4RowStatus = 0;

    if (nmhGetCapwapDot11WlanBindRowStatus (i4IfIndex,
                                            u4CapwapDot11WlanProfileId,
                                            &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsWlanRadioIfAdminStatus != 1) &&
        (i4TestValFsWlanRadioIfAdminStatus != 2))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4CapwapDot11WlanProfileId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWlanRadioIfRowStatus
 Input       :  The Indices
                IfIndex
                CapwapDot11WlanProfileId

                The Object 
                testValFsWlanRadioIfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWlanRadioIfRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                 UINT4 u4CapwapDot11WlanProfileId,
                                 INT4 i4TestValFsWlanRadioIfRowStatus)
{
    INT4                i4RowStatus = 0;

    if (nmhGetCapwapDot11WlanBindRowStatus (i4IfIndex,
                                            u4CapwapDot11WlanProfileId,
                                            &i4RowStatus) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsWlanRadioIfRowStatus)
    {
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
        case ACTIVE:
        case DESTROY:
        case NOT_IN_SERVICE:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsWlanRadioIfTable
 Input       :  The Indices
                IfIndex
                CapwapDot11WlanProfileId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsWlanRadioIfTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDot11WtpDhcpSrvSubnetPoolConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot11WtpDhcpSrvSubnetPoolConfigTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot11WtpDhcpSrvSubnetPoolConfigTable (UINT4
                                                                u4CapwapBaseWtpProfileId,
                                                                INT4
                                                                i4FsDot11WtpDhcpSrvSubnetPoolIndex)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsDot11WtpDhcpSrvSubnetPoolIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable (UINT4
                                                        *pu4CapwapBaseWtpProfileId,
                                                        INT4
                                                        *pi4FsDot11WtpDhcpSrvSubnetPoolIndex)
{
    tWssIfWtpDhcpPoolDB WssIfWtpDhcpPoolDB;
    INT4                i4RetVal = 0;

    MEMSET (&WssIfWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));
    i4RetVal =
        WssIfWtpDhcpPoolGetFirstEntry (pu4CapwapBaseWtpProfileId,
                                       pi4FsDot11WtpDhcpSrvSubnetPoolIndex);

    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
                nextCapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex
                nextFsDot11WtpDhcpSrvSubnetPoolIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable (UINT4
                                                       u4CapwapBaseWtpProfileId,
                                                       UINT4
                                                       *pu4NextCapwapBaseWtpProfileId,
                                                       INT4
                                                       i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                                       INT4
                                                       *pi4NextFsDot11WtpDhcpSrvSubnetPoolIndex)
{
    INT4                i4RetVal = 0;
    i4RetVal =
        WssIfWtpDhcpPoolGetNextEntry (u4CapwapBaseWtpProfileId,
                                      pu4NextCapwapBaseWtpProfileId,
                                      (UINT4)
                                      i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                      (UINT4 *)
                                      pi4NextFsDot11WtpDhcpSrvSubnetPoolIndex);
    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDot11WtpDhcpSrvSubnetSubnet
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                retValFsDot11WtpDhcpSrvSubnetSubnet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot11WtpDhcpSrvSubnetSubnet (UINT4 u4CapwapBaseWtpProfileId,
                                     INT4 i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                     UINT4
                                     *pu4RetValFsDot11WtpDhcpSrvSubnetSubnet)
{
    tWssIfWtpDhcpPoolDB WssifWtpDhcpPoolDB;
    MEMSET (&WssifWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));
    WssifWtpDhcpPoolDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssifWtpDhcpPoolDB.u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;
    if (WssIfWtpDhcpPoolGetEntry (&WssifWtpDhcpPoolDB) != OSIX_FAILURE)
    {
        *pu4RetValFsDot11WtpDhcpSrvSubnetSubnet = WssifWtpDhcpPoolDB.u4Subnet;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot11WtpDhcpSrvSubnetMask
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                retValFsDot11WtpDhcpSrvSubnetMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot11WtpDhcpSrvSubnetMask (UINT4 u4CapwapBaseWtpProfileId,
                                   INT4 i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                   UINT4 *pu4RetValFsDot11WtpDhcpSrvSubnetMask)
{
    tWssIfWtpDhcpPoolDB WssifWtpDhcpPoolDB;
    MEMSET (&WssifWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));
    WssifWtpDhcpPoolDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssifWtpDhcpPoolDB.u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;
    if (WssIfWtpDhcpPoolGetEntry (&WssifWtpDhcpPoolDB) != OSIX_FAILURE)
    {
        *pu4RetValFsDot11WtpDhcpSrvSubnetMask = WssifWtpDhcpPoolDB.u4Mask;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot11WtpDhcpSrvSubnetStartIpAddress
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                retValFsDot11WtpDhcpSrvSubnetStartIpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot11WtpDhcpSrvSubnetStartIpAddress (UINT4 u4CapwapBaseWtpProfileId,
                                             INT4
                                             i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                             UINT4
                                             *pu4RetValFsDot11WtpDhcpSrvSubnetStartIpAddress)
{
    tWssIfWtpDhcpPoolDB WssifWtpDhcpPoolDB;
    MEMSET (&WssifWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));
    WssifWtpDhcpPoolDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssifWtpDhcpPoolDB.u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;
    if (WssIfWtpDhcpPoolGetEntry (&WssifWtpDhcpPoolDB) != OSIX_FAILURE)
    {
        *pu4RetValFsDot11WtpDhcpSrvSubnetStartIpAddress =
            WssifWtpDhcpPoolDB.u4StartIp;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot11WtpDhcpSrvSubnetEndIpAddress
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                retValFsDot11WtpDhcpSrvSubnetEndIpAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot11WtpDhcpSrvSubnetEndIpAddress (UINT4 u4CapwapBaseWtpProfileId,
                                           INT4
                                           i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                           UINT4
                                           *pu4RetValFsDot11WtpDhcpSrvSubnetEndIpAddress)
{
    tWssIfWtpDhcpPoolDB WssifWtpDhcpPoolDB;
    MEMSET (&WssifWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));
    WssifWtpDhcpPoolDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssifWtpDhcpPoolDB.u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;
    if (WssIfWtpDhcpPoolGetEntry (&WssifWtpDhcpPoolDB) != OSIX_FAILURE)
    {
        *pu4RetValFsDot11WtpDhcpSrvSubnetEndIpAddress =
            WssifWtpDhcpPoolDB.u4EndIp;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot11WtpDhcpSrvSubnetLeaseTime
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                retValFsDot11WtpDhcpSrvSubnetLeaseTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot11WtpDhcpSrvSubnetLeaseTime (UINT4 u4CapwapBaseWtpProfileId,
                                        INT4 i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                        INT4
                                        *pi4RetValFsDot11WtpDhcpSrvSubnetLeaseTime)
{
    tWssIfWtpDhcpPoolDB WssifWtpDhcpPoolDB;
    MEMSET (&WssifWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));
    WssifWtpDhcpPoolDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssifWtpDhcpPoolDB.u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;
    if (WssIfWtpDhcpPoolGetEntry (&WssifWtpDhcpPoolDB) != OSIX_FAILURE)
    {
        *pi4RetValFsDot11WtpDhcpSrvSubnetLeaseTime =
            (INT4) WssifWtpDhcpPoolDB.u4LeaseExprTime;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot11WtpDhcpSrvDefaultRouter
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                retValFsDot11WtpDhcpSrvDefaultRouter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot11WtpDhcpSrvDefaultRouter (UINT4 u4CapwapBaseWtpProfileId,
                                      INT4 i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                      UINT4
                                      *pu4RetValFsDot11WtpDhcpSrvDefaultRouter)
{
    tWssIfWtpDhcpPoolDB WssifWtpDhcpPoolDB;
    MEMSET (&WssifWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));
    WssifWtpDhcpPoolDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssifWtpDhcpPoolDB.u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;
    if (WssIfWtpDhcpPoolGetEntry (&WssifWtpDhcpPoolDB) == OSIX_SUCCESS)
    {
        *pu4RetValFsDot11WtpDhcpSrvDefaultRouter =
            WssifWtpDhcpPoolDB.u4DefaultIp;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot11WtpDhcpSrvDnsServerIp
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                retValFsDot11WtpDhcpSrvDnsServerIp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot11WtpDhcpSrvDnsServerIp (UINT4 u4CapwapBaseWtpProfileId,
                                    INT4 i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                    UINT4
                                    *pu4RetValFsDot11WtpDhcpSrvDnsServerIp)
{
    tWssIfWtpDhcpPoolDB WssifWtpDhcpPoolDB;
    MEMSET (&WssifWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));
    WssifWtpDhcpPoolDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssifWtpDhcpPoolDB.u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;
    if (WssIfWtpDhcpPoolGetEntry (&WssifWtpDhcpPoolDB) == OSIX_SUCCESS)
    {
        *pu4RetValFsDot11WtpDhcpSrvDnsServerIp =
            WssifWtpDhcpPoolDB.u4DnsServerIp;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot11WtpDhcpSrvSubnetPoolRowStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                retValFsDot11WtpDhcpSrvSubnetPoolRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot11WtpDhcpSrvSubnetPoolRowStatus (UINT4 u4CapwapBaseWtpProfileId,
                                            INT4
                                            i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                            INT4
                                            *pi4RetValFsDot11WtpDhcpSrvSubnetPoolRowStatus)
{
    tWssIfWtpDhcpPoolDB WssifWtpDhcpPoolDB;
    MEMSET (&WssifWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));
    WssifWtpDhcpPoolDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssifWtpDhcpPoolDB.u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;
    if (WssIfWtpDhcpPoolGetEntry (&WssifWtpDhcpPoolDB) != OSIX_FAILURE)
    {
        *pi4RetValFsDot11WtpDhcpSrvSubnetPoolRowStatus =
            WssifWtpDhcpPoolDB.i4RowStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDot11WtpDhcpSrvSubnetSubnet
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                setValFsDot11WtpDhcpSrvSubnetSubnet
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot11WtpDhcpSrvSubnetSubnet (UINT4 u4CapwapBaseWtpProfileId,
                                     INT4 i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                     UINT4
                                     u4SetValFsDot11WtpDhcpSrvSubnetSubnet)
{
    tWssIfWtpDhcpPoolDB WssifWtpDhcpPoolDB;
    MEMSET (&WssifWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));
    WssifWtpDhcpPoolDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssifWtpDhcpPoolDB.u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;

    if (WssIfWtpDhcpPoolGetEntry (&WssifWtpDhcpPoolDB) == OSIX_SUCCESS)
    {
        WssifWtpDhcpPoolDB.u4Subnet = u4SetValFsDot11WtpDhcpSrvSubnetSubnet;
        if (WssIfWtpDhcpPoolSetEntry (&WssifWtpDhcpPoolDB) == OSIX_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDot11WtpDhcpSrvSubnetMask
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                setValFsDot11WtpDhcpSrvSubnetMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot11WtpDhcpSrvSubnetMask (UINT4 u4CapwapBaseWtpProfileId,
                                   INT4 i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                   UINT4 u4SetValFsDot11WtpDhcpSrvSubnetMask)
{
    tWssIfWtpDhcpPoolDB WssifWtpDhcpPoolDB;
    MEMSET (&WssifWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));
    WssifWtpDhcpPoolDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssifWtpDhcpPoolDB.u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;
    if (WssIfWtpDhcpPoolGetEntry (&WssifWtpDhcpPoolDB) == OSIX_SUCCESS)
    {
        WssifWtpDhcpPoolDB.u4Mask = u4SetValFsDot11WtpDhcpSrvSubnetMask;
        if (WssIfWtpDhcpPoolSetEntry (&WssifWtpDhcpPoolDB) == OSIX_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDot11WtpDhcpSrvSubnetStartIpAddress
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                setValFsDot11WtpDhcpSrvSubnetStartIpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot11WtpDhcpSrvSubnetStartIpAddress (UINT4 u4CapwapBaseWtpProfileId,
                                             INT4
                                             i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                             UINT4
                                             u4SetValFsDot11WtpDhcpSrvSubnetStartIpAddress)
{
    tWssIfWtpDhcpPoolDB WssifWtpDhcpPoolDB;
    MEMSET (&WssifWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));
    WssifWtpDhcpPoolDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssifWtpDhcpPoolDB.u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;
    if (WssIfWtpDhcpPoolGetEntry (&WssifWtpDhcpPoolDB) == OSIX_SUCCESS)
    {

        WssifWtpDhcpPoolDB.u4StartIp =
            u4SetValFsDot11WtpDhcpSrvSubnetStartIpAddress;
        if (WssIfWtpDhcpPoolSetEntry (&WssifWtpDhcpPoolDB) == OSIX_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDot11WtpDhcpSrvSubnetEndIpAddress
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                setValFsDot11WtpDhcpSrvSubnetEndIpAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot11WtpDhcpSrvSubnetEndIpAddress (UINT4 u4CapwapBaseWtpProfileId,
                                           INT4
                                           i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                           UINT4
                                           u4SetValFsDot11WtpDhcpSrvSubnetEndIpAddress)
{
    tWssIfWtpDhcpPoolDB WssifWtpDhcpPoolDB;
    MEMSET (&WssifWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));
    WssifWtpDhcpPoolDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssifWtpDhcpPoolDB.u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;
    if (WssIfWtpDhcpPoolGetEntry (&WssifWtpDhcpPoolDB) == OSIX_SUCCESS)
    {

        WssifWtpDhcpPoolDB.u4EndIp =
            u4SetValFsDot11WtpDhcpSrvSubnetEndIpAddress;
        if (WssIfWtpDhcpPoolSetEntry (&WssifWtpDhcpPoolDB) == OSIX_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDot11WtpDhcpSrvSubnetLeaseTime
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                setValFsDot11WtpDhcpSrvSubnetLeaseTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot11WtpDhcpSrvSubnetLeaseTime (UINT4 u4CapwapBaseWtpProfileId,
                                        INT4 i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                        INT4
                                        i4SetValFsDot11WtpDhcpSrvSubnetLeaseTime)
{
    tWssIfWtpDhcpPoolDB WssifWtpDhcpPoolDB;
    MEMSET (&WssifWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));
    WssifWtpDhcpPoolDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssifWtpDhcpPoolDB.u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;
    if (WssIfWtpDhcpPoolGetEntry (&WssifWtpDhcpPoolDB) == OSIX_SUCCESS)
    {

        WssifWtpDhcpPoolDB.u4LeaseExprTime =
            (UINT4) i4SetValFsDot11WtpDhcpSrvSubnetLeaseTime;
        if (WssIfWtpDhcpPoolSetEntry (&WssifWtpDhcpPoolDB) == OSIX_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDot11WtpDhcpSrvDefaultRouter
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                setValFsDot11WtpDhcpSrvDefaultRouter
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot11WtpDhcpSrvDefaultRouter (UINT4 u4CapwapBaseWtpProfileId,
                                      INT4 i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                      UINT4
                                      u4SetValFsDot11WtpDhcpSrvDefaultRouter)
{
    tWssIfWtpDhcpPoolDB WssifWtpDhcpPoolDB;
    MEMSET (&WssifWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));
    WssifWtpDhcpPoolDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssifWtpDhcpPoolDB.u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;
    if (WssIfWtpDhcpPoolGetEntry (&WssifWtpDhcpPoolDB) == OSIX_SUCCESS)
    {
        WssifWtpDhcpPoolDB.u4DefaultIp = u4SetValFsDot11WtpDhcpSrvDefaultRouter;
        if (WssIfWtpDhcpPoolSetEntry (&WssifWtpDhcpPoolDB) == OSIX_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDot11WtpDhcpSrvDnsServerIp
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                setValFsDot11WtpDhcpSrvDnsServerIp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot11WtpDhcpSrvDnsServerIp (UINT4 u4CapwapBaseWtpProfileId,
                                    INT4 i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                    UINT4 u4SetValFsDot11WtpDhcpSrvDnsServerIp)
{
    tWssIfWtpDhcpPoolDB WssifWtpDhcpPoolDB;
    MEMSET (&WssifWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));
    WssifWtpDhcpPoolDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssifWtpDhcpPoolDB.u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;
    if (WssIfWtpDhcpPoolGetEntry (&WssifWtpDhcpPoolDB) == OSIX_SUCCESS)
    {
        WssifWtpDhcpPoolDB.u4DnsServerIp = u4SetValFsDot11WtpDhcpSrvDnsServerIp;
        if (WssIfWtpDhcpPoolSetEntry (&WssifWtpDhcpPoolDB) == OSIX_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsDot11WtpDhcpSrvSubnetPoolRowStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                setValFsDot11WtpDhcpSrvSubnetPoolRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot11WtpDhcpSrvSubnetPoolRowStatus (UINT4 u4CapwapBaseWtpProfileId,
                                            INT4
                                            i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                            INT4
                                            i4SetValFsDot11WtpDhcpSrvSubnetPoolRowStatus)
{
    tWssIfWtpDhcpPoolDB WssifWtpDhcpPoolDB;
    tWssIfWtpDhcpConfigDB WssIfWtpDhcpConfigDB;

    MEMSET (&WssifWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));
    MEMSET (&WssIfWtpDhcpConfigDB, 0, sizeof (tWssIfWtpDhcpConfigDB));

    /* Get the server Status for the given profile id */
    WssIfWtpDhcpConfigDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    if ((WssIfWtpDhcpConfigGetEntry (&WssIfWtpDhcpConfigDB) != OSIX_SUCCESS))
    {
        return OSIX_FAILURE;
    }

    WssifWtpDhcpPoolDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssifWtpDhcpPoolDB.u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;

    if (WssIfWtpDhcpPoolGetEntry (&WssifWtpDhcpPoolDB) == OSIX_SUCCESS)
    {
        WssifWtpDhcpPoolDB.i4RowStatus =
            i4SetValFsDot11WtpDhcpSrvSubnetPoolRowStatus;
        if (i4SetValFsDot11WtpDhcpSrvSubnetPoolRowStatus == DESTROY)
        {
            WssifStartDhcpPoolUpdateReq (u4CapwapBaseWtpProfileId,
                                         (UINT1) WssIfWtpDhcpConfigDB.
                                         i4WtpDhcpServerStatus,
                                         i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                         DESTROY);
            if (WssIfWtpDhcpPoolDeleteEntry (&WssifWtpDhcpPoolDB) ==
                OSIX_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
        else if (WssIfWtpDhcpPoolSetEntry (&WssifWtpDhcpPoolDB) == OSIX_SUCCESS)
        {
            WssifStartDhcpPoolUpdateReq (u4CapwapBaseWtpProfileId,
                                         (UINT1) WssIfWtpDhcpConfigDB.
                                         i4WtpDhcpServerStatus,
                                         i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                         ACTIVE);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        if ((i4SetValFsDot11WtpDhcpSrvSubnetPoolRowStatus != CREATE_AND_GO) &&
            (i4SetValFsDot11WtpDhcpSrvSubnetPoolRowStatus != CREATE_AND_WAIT))
        {
            return SNMP_FAILURE;
        }
        else
        {
            if (i4SetValFsDot11WtpDhcpSrvSubnetPoolRowStatus == CREATE_AND_WAIT)
            {
                WssifWtpDhcpPoolDB.i4RowStatus = NOT_READY;
                if (WssIfWtpDhcpPoolAddEntry (&WssifWtpDhcpPoolDB) ==
                    OSIX_SUCCESS)
                {
                    return SNMP_SUCCESS;
                }
            }
            else
            {
                if ((WssifWtpDhcpPoolDB.u4WtpProfileId == 0) ||
                    (WssifWtpDhcpPoolDB.u4DhcpPoolId == 0) ||
                    (WssifWtpDhcpPoolDB.u4Subnet == 0) ||
                    (WssifWtpDhcpPoolDB.u4Mask == 0) ||
                    (WssifWtpDhcpPoolDB.u4StartIp == 0) ||
                    (WssifWtpDhcpPoolDB.u4EndIp == 0) ||
                    (WssifWtpDhcpPoolDB.u4LeaseExprTime == 0) ||
                    (WssifWtpDhcpPoolDB.u4DefaultIp == 0) ||
                    (WssifWtpDhcpPoolDB.u4DnsServerIp == 0))
                {
                    return SNMP_FAILURE;
                }
                WssifWtpDhcpPoolDB.i4RowStatus = ACTIVE;
                if (WssIfWtpDhcpPoolAddEntry (&WssifWtpDhcpPoolDB) ==
                    OSIX_SUCCESS)
                {
                    return SNMP_SUCCESS;
                }
                WssifStartDhcpPoolUpdateReq (u4CapwapBaseWtpProfileId,
                                             (UINT1) WssIfWtpDhcpConfigDB.
                                             i4WtpDhcpServerStatus,
                                             i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                             ACTIVE);
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDot11WtpDhcpSrvSubnetSubnet
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                testValFsDot11WtpDhcpSrvSubnetSubnet
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot11WtpDhcpSrvSubnetSubnet (UINT4 *pu4ErrorCode,
                                        UINT4 u4CapwapBaseWtpProfileId,
                                        INT4 i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                        UINT4
                                        u4TestValFsDot11WtpDhcpSrvSubnetSubnet)
{
    tWssIfWtpDhcpPoolDB WssIfWtpDhcpPoolDB;
    tWssIfWtpDhcpPoolDB WssIfWtpDhcpPoolIterator;
    tWssIfWtpDhcpPoolDB *pWssIfWtpDhcpPoolDB = &WssIfWtpDhcpPoolDB;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4NextWtpProfileId = 0;
    UINT4               u4DhcpPoolId = 0;
    UINT4               u4NextDhcpPoolId = 0;

    MEMSET (&WssIfWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));
    MEMSET (&WssIfWtpDhcpPoolIterator, 0, sizeof (tWssIfWtpDhcpPoolDB));

    pWssIfWtpDhcpPoolDB->u4WtpProfileId = u4CapwapBaseWtpProfileId;
    pWssIfWtpDhcpPoolDB->u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;
    if (WssIfWtpDhcpPoolGetEntry (pWssIfWtpDhcpPoolDB) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pWssIfWtpDhcpPoolDB->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (!((IP_IS_ADDR_CLASS_A (u4TestValFsDot11WtpDhcpSrvSubnetSubnet) ?
           (IP_IS_ZERO_NETWORK (u4TestValFsDot11WtpDhcpSrvSubnetSubnet) ? 0 : 1)
           : 0) || (IP_IS_ADDR_CLASS_B (u4TestValFsDot11WtpDhcpSrvSubnetSubnet))
          || (IP_IS_ADDR_CLASS_C (u4TestValFsDot11WtpDhcpSrvSubnetSubnet))))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u4WtpProfileId = u4CapwapBaseWtpProfileId;
    while (WssIfWtpDhcpPoolGetNextEntry (u4WtpProfileId, &u4NextWtpProfileId,
                                         u4DhcpPoolId,
                                         &u4NextDhcpPoolId) == OSIX_SUCCESS)
    {
        /* If the profile id does not match then the configured pool is safe */
        if (u4WtpProfileId != u4NextWtpProfileId)
        {
            break;
        }

        WssIfWtpDhcpPoolIterator.u4DhcpPoolId = u4NextDhcpPoolId;
        WssIfWtpDhcpPoolIterator.u4WtpProfileId = u4NextWtpProfileId;
        /*//WssIfWtpDhcpPoolDB.u4WtpProfileId = u4WtpProfileId; */
        if (WssIfWtpDhcpPoolGetEntry (&WssIfWtpDhcpPoolIterator) ==
            OSIX_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (WssIfWtpDhcpPoolIterator.u4Subnet ==
            u4TestValFsDot11WtpDhcpSrvSubnetSubnet)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        u4WtpProfileId = u4NextWtpProfileId;
        u4DhcpPoolId = u4NextDhcpPoolId;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot11WtpDhcpSrvSubnetMask
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                testValFsDot11WtpDhcpSrvSubnetMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot11WtpDhcpSrvSubnetMask (UINT4 *pu4ErrorCode,
                                      UINT4 u4CapwapBaseWtpProfileId,
                                      INT4 i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                      UINT4
                                      u4TestValFsDot11WtpDhcpSrvSubnetMask)
{
    tWssIfWtpDhcpPoolDB WssIfWtpDhcpPoolDB;
    tWssIfWtpDhcpPoolDB *pWssIfWtpDhcpPoolDB = &WssIfWtpDhcpPoolDB;

    MEMSET (&WssIfWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));

    pWssIfWtpDhcpPoolDB->u4WtpProfileId = u4CapwapBaseWtpProfileId;
    pWssIfWtpDhcpPoolDB->u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;
    if (WssIfWtpDhcpPoolGetEntry (pWssIfWtpDhcpPoolDB) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pWssIfWtpDhcpPoolDB->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (!
        (~
         (u4TestValFsDot11WtpDhcpSrvSubnetMask |
          (u4TestValFsDot11WtpDhcpSrvSubnetMask - 1))) == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot11WtpDhcpSrvSubnetStartIpAddress
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                testValFsDot11WtpDhcpSrvSubnetStartIpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot11WtpDhcpSrvSubnetStartIpAddress (UINT4 *pu4ErrorCode,
                                                UINT4 u4CapwapBaseWtpProfileId,
                                                INT4
                                                i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                                UINT4
                                                u4TestValFsDot11WtpDhcpSrvSubnetStartIpAddress)
{
    tWssIfWtpDhcpPoolDB WssIfWtpDhcpPoolDB;
    tWssIfWtpDhcpPoolDB *pWssIfWtpDhcpPoolDB = &WssIfWtpDhcpPoolDB;

    MEMSET (&WssIfWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));

    pWssIfWtpDhcpPoolDB->u4WtpProfileId = u4CapwapBaseWtpProfileId;
    pWssIfWtpDhcpPoolDB->u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;
    if (WssIfWtpDhcpPoolGetEntry (pWssIfWtpDhcpPoolDB) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pWssIfWtpDhcpPoolDB->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (!((IP_IS_ADDR_CLASS_A (u4TestValFsDot11WtpDhcpSrvSubnetStartIpAddress) ?
           (IP_IS_ZERO_NETWORK (u4TestValFsDot11WtpDhcpSrvSubnetStartIpAddress)
            ? 0 : 1) : 0)
          ||
          (IP_IS_ADDR_CLASS_B (u4TestValFsDot11WtpDhcpSrvSubnetStartIpAddress))
          ||
          (IP_IS_ADDR_CLASS_C
           (u4TestValFsDot11WtpDhcpSrvSubnetStartIpAddress))))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((u4TestValFsDot11WtpDhcpSrvSubnetStartIpAddress & pWssIfWtpDhcpPoolDB->
         u4Mask) != pWssIfWtpDhcpPoolDB->u4Subnet)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot11WtpDhcpSrvSubnetEndIpAddress
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                testValFsDot11WtpDhcpSrvSubnetEndIpAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot11WtpDhcpSrvSubnetEndIpAddress (UINT4 *pu4ErrorCode,
                                              UINT4 u4CapwapBaseWtpProfileId,
                                              INT4
                                              i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                              UINT4
                                              u4TestValFsDot11WtpDhcpSrvSubnetEndIpAddress)
{
    tWssIfWtpDhcpPoolDB WssIfWtpDhcpPoolDB;
    tWssIfWtpDhcpPoolDB *pWssIfWtpDhcpPoolDB = &WssIfWtpDhcpPoolDB;

    MEMSET (&WssIfWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));

    pWssIfWtpDhcpPoolDB->u4WtpProfileId = u4CapwapBaseWtpProfileId;
    pWssIfWtpDhcpPoolDB->u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;
    if (WssIfWtpDhcpPoolGetEntry (pWssIfWtpDhcpPoolDB) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pWssIfWtpDhcpPoolDB->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (!((IP_IS_ADDR_CLASS_A (u4TestValFsDot11WtpDhcpSrvSubnetEndIpAddress) ?
           (IP_IS_ZERO_NETWORK (u4TestValFsDot11WtpDhcpSrvSubnetEndIpAddress) ?
            0 : 1) : 0)
          || (IP_IS_ADDR_CLASS_B (u4TestValFsDot11WtpDhcpSrvSubnetEndIpAddress))
          ||
          (IP_IS_ADDR_CLASS_C (u4TestValFsDot11WtpDhcpSrvSubnetEndIpAddress))))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((u4TestValFsDot11WtpDhcpSrvSubnetEndIpAddress & pWssIfWtpDhcpPoolDB->
         u4Mask) != pWssIfWtpDhcpPoolDB->u4Subnet)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (u4TestValFsDot11WtpDhcpSrvSubnetEndIpAddress ==
        (pWssIfWtpDhcpPoolDB->u4Subnet & pWssIfWtpDhcpPoolDB->u4Mask) +
        (0xffffffff & (~pWssIfWtpDhcpPoolDB->u4Mask)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (u4TestValFsDot11WtpDhcpSrvSubnetEndIpAddress <=
        pWssIfWtpDhcpPoolDB->u4StartIp)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot11WtpDhcpSrvSubnetLeaseTime
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                testValFsDot11WtpDhcpSrvSubnetLeaseTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot11WtpDhcpSrvSubnetLeaseTime (UINT4 *pu4ErrorCode,
                                           UINT4 u4CapwapBaseWtpProfileId,
                                           INT4
                                           i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                           INT4
                                           i4TestValFsDot11WtpDhcpSrvSubnetLeaseTime)
{
    tWssIfWtpDhcpPoolDB WssIfWtpDhcpPoolDB;
    tWssIfWtpDhcpPoolDB *pWssIfWtpDhcpPoolDB = &WssIfWtpDhcpPoolDB;

    MEMSET (&WssIfWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));

    pWssIfWtpDhcpPoolDB->u4WtpProfileId = u4CapwapBaseWtpProfileId;
    pWssIfWtpDhcpPoolDB->u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;
    if (WssIfWtpDhcpPoolGetEntry (pWssIfWtpDhcpPoolDB) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pWssIfWtpDhcpPoolDB->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (i4TestValFsDot11WtpDhcpSrvSubnetLeaseTime < 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot11WtpDhcpSrvDefaultRouter
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                testValFsDot11WtpDhcpSrvDefaultRouter
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot11WtpDhcpSrvDefaultRouter (UINT4 *pu4ErrorCode,
                                         UINT4 u4CapwapBaseWtpProfileId,
                                         INT4
                                         i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                         UINT4
                                         u4TestValFsDot11WtpDhcpSrvDefaultRouter)
{
    tWssIfWtpDhcpPoolDB WssIfWtpDhcpPoolDB;
    tWssIfWtpDhcpPoolDB *pWssIfWtpDhcpPoolDB = &WssIfWtpDhcpPoolDB;

    MEMSET (&WssIfWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));

    pWssIfWtpDhcpPoolDB->u4WtpProfileId = u4CapwapBaseWtpProfileId;
    pWssIfWtpDhcpPoolDB->u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;
    if (WssIfWtpDhcpPoolGetEntry (pWssIfWtpDhcpPoolDB) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pWssIfWtpDhcpPoolDB->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4TestValFsDot11WtpDhcpSrvDefaultRouter != 0)
    {
        if (CFA_IS_LOOPBACK (u4TestValFsDot11WtpDhcpSrvDefaultRouter))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if (!((CFA_IS_ADDR_CLASS_A (u4TestValFsDot11WtpDhcpSrvDefaultRouter) ?
               (IP_IS_ZERO_NETWORK (u4TestValFsDot11WtpDhcpSrvDefaultRouter) ? 0
                : 1) : 0)
              || (CFA_IS_ADDR_CLASS_B (u4TestValFsDot11WtpDhcpSrvDefaultRouter))
              || (CFA_IS_ADDR_CLASS_C (u4TestValFsDot11WtpDhcpSrvDefaultRouter))
              ||
              (CFA_IS_ADDR_CLASS_E (u4TestValFsDot11WtpDhcpSrvDefaultRouter))))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot11WtpDhcpSrvDnsServerIp
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                testValFsDot11WtpDhcpSrvDnsServerIp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot11WtpDhcpSrvDnsServerIp (UINT4 *pu4ErrorCode,
                                       UINT4 u4CapwapBaseWtpProfileId,
                                       INT4 i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                       UINT4
                                       u4TestValFsDot11WtpDhcpSrvDnsServerIp)
{
    tWssIfWtpDhcpPoolDB WssIfWtpDhcpPoolDB;
    tWssIfWtpDhcpPoolDB *pWssIfWtpDhcpPoolDB = &WssIfWtpDhcpPoolDB;

    MEMSET (&WssIfWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));

    pWssIfWtpDhcpPoolDB->u4WtpProfileId = u4CapwapBaseWtpProfileId;
    pWssIfWtpDhcpPoolDB->u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;
    if (WssIfWtpDhcpPoolGetEntry (pWssIfWtpDhcpPoolDB) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pWssIfWtpDhcpPoolDB->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4TestValFsDot11WtpDhcpSrvDnsServerIp != 0)
    {
        if (CFA_IS_LOOPBACK (u4TestValFsDot11WtpDhcpSrvDnsServerIp))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if (!((CFA_IS_ADDR_CLASS_A (u4TestValFsDot11WtpDhcpSrvDnsServerIp) ?
               (IP_IS_ZERO_NETWORK (u4TestValFsDot11WtpDhcpSrvDnsServerIp) ? 0 :
                1) : 0)
              || (CFA_IS_ADDR_CLASS_B (u4TestValFsDot11WtpDhcpSrvDnsServerIp))
              || (CFA_IS_ADDR_CLASS_C (u4TestValFsDot11WtpDhcpSrvDnsServerIp))
              || (CFA_IS_ADDR_CLASS_E (u4TestValFsDot11WtpDhcpSrvDnsServerIp))))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot11WtpDhcpSrvSubnetPoolRowStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex

                The Object 
                testValFsDot11WtpDhcpSrvSubnetPoolRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot11WtpDhcpSrvSubnetPoolRowStatus (UINT4 *pu4ErrorCode,
                                               UINT4 u4CapwapBaseWtpProfileId,
                                               INT4
                                               i4FsDot11WtpDhcpSrvSubnetPoolIndex,
                                               INT4
                                               i4TestValFsDot11WtpDhcpSrvSubnetPoolRowStatus)
{
    tWssIfWtpDhcpPoolDB WssIfWtpDhcpPoolDB;
    tWssIfWtpDhcpPoolDB *pWssIfWtpDhcpPoolDB = &WssIfWtpDhcpPoolDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    MEMSET (&WssIfWtpDhcpPoolDB, 0, sizeof (tWssIfWtpDhcpPoolDB));

    CAPWAP_IF_CAP_DB_ALLOC (pWssIfCapwapDB);
    if (pWssIfCapwapDB == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4CapwapBaseWtpProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                 pWssIfCapwapDB) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
        return SNMP_FAILURE;
    }

    pWssIfWtpDhcpPoolDB->u4WtpProfileId = u4CapwapBaseWtpProfileId;
    pWssIfWtpDhcpPoolDB->u4DhcpPoolId =
        (UINT4) i4FsDot11WtpDhcpSrvSubnetPoolIndex;
    if (WssIfWtpDhcpPoolGetEntry (pWssIfWtpDhcpPoolDB) != OSIX_FAILURE)
    {
        if ((i4TestValFsDot11WtpDhcpSrvSubnetPoolRowStatus == ACTIVE) ||
            (i4TestValFsDot11WtpDhcpSrvSubnetPoolRowStatus == DESTROY) ||
            (i4TestValFsDot11WtpDhcpSrvSubnetPoolRowStatus == NOT_IN_SERVICE))
        {
            CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        if ((i4TestValFsDot11WtpDhcpSrvSubnetPoolRowStatus == CREATE_AND_WAIT)
            || (i4TestValFsDot11WtpDhcpSrvSubnetPoolRowStatus == CREATE_AND_GO))
        {
            CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot11WtpDhcpSrvSubnetPoolConfigTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsDot11WtpDhcpSrvSubnetPoolIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot11WtpDhcpSrvSubnetPoolConfigTable (UINT4 *pu4ErrorCode,
                                                tSnmpIndexList * pSnmpIndexList,
                                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWtpNATTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWtpNATTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpNATConfigIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWtpNATTable (UINT4 u4CapwapBaseWtpProfileId,
                                       INT4 i4FsWtpNATConfigIndex)
{
    UNUSED_PARAM (i4FsWtpNATConfigIndex);
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsWtpNATTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpNATConfigIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsWtpNATTable (UINT4 *pu4CapwapBaseWtpProfileId,
                               INT4 *pi4FsWtpNATConfigIndex)
{
    if (WssIfNATConfigGetFirstEntry (pu4CapwapBaseWtpProfileId,
                                     pi4FsWtpNATConfigIndex) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsWtpNATTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
                nextCapwapBaseWtpProfileId
                FsWtpNATConfigIndex
                nextFsWtpNATConfigIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsWtpNATTable (UINT4 u4CapwapBaseWtpProfileId,
                              UINT4 *pu4NextCapwapBaseWtpProfileId,
                              INT4 i4FsWtpNATConfigIndex,
                              INT4 *pi4NextFsWtpNATConfigIndex)
{
    if (WssIfNATConfigGetNextEntry (&i4FsWtpNATConfigIndex,
                                    &u4CapwapBaseWtpProfileId,
                                    pu4NextCapwapBaseWtpProfileId,
                                    pi4NextFsWtpNATConfigIndex) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsWtpIfMainWanType
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpNATConfigIndex

                The Object 
                retValFsWtpIfMainWanType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWtpIfMainWanType (UINT4 u4CapwapBaseWtpProfileId,
                          INT4 i4FsWtpNATConfigIndex,
                          INT4 *pi4RetValFsWtpIfMainWanType)
{
    tWssIfNATConfigEntryDB WssifNATConfigEntry;

    MEMSET (&WssifNATConfigEntry, 0, sizeof (tWssIfNATConfigEntryDB));

    WssifNATConfigEntry.i4NATConfigEntryIndex = i4FsWtpNATConfigIndex;
    WssifNATConfigEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    if (WssIfNATConfigGetEntry (&WssifNATConfigEntry) != OSIX_FAILURE)
    {
        *pi4RetValFsWtpIfMainWanType = WssifNATConfigEntry.i1WanType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsWtpNATConfigRowStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpNATConfigIndex

                The Object 
                retValFsWtpNATConfigRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWtpNATConfigRowStatus (UINT4 u4CapwapBaseWtpProfileId,
                               INT4 i4FsWtpNATConfigIndex,
                               INT4 *pi4RetValFsWtpNATConfigRowStatus)
{
    tWssIfNATConfigEntryDB WssifNATConfigEntry;

    MEMSET (&WssifNATConfigEntry, 0, sizeof (tWssIfNATConfigEntryDB));

    WssifNATConfigEntry.i4NATConfigEntryIndex = i4FsWtpNATConfigIndex;
    WssifNATConfigEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    if (WssIfNATConfigGetEntry (&WssifNATConfigEntry) != OSIX_FAILURE)
    {
        *pi4RetValFsWtpNATConfigRowStatus = WssifNATConfigEntry.i4RowStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsWtpIfMainWanType
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpNATConfigIndex

                The Object 
                setValFsWtpIfMainWanType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWtpIfMainWanType (UINT4 u4CapwapBaseWtpProfileId,
                          INT4 i4FsWtpNATConfigIndex,
                          INT4 i4SetValFsWtpIfMainWanType)
{
    tWssIfNATConfigEntryDB WssifNATConfigEntry;

    MEMSET (&WssifNATConfigEntry, 0, sizeof (tWssIfNATConfigEntryDB));

    WssifNATConfigEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssifNATConfigEntry.i4NATConfigEntryIndex = i4FsWtpNATConfigIndex;

    if (WssIfNATConfigGetEntry (&WssifNATConfigEntry) == OSIX_SUCCESS)
    {
        WssifNATConfigEntry.i1WanType = i4SetValFsWtpIfMainWanType;
        if (WssIfNATConfigSetEntry (&WssifNATConfigEntry) == OSIX_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsWtpNATConfigRowStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpNATConfigIndex

                The Object 
                setValFsWtpNATConfigRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWtpNATConfigRowStatus (UINT4 u4CapwapBaseWtpProfileId,
                               INT4 i4FsWtpNATConfigIndex,
                               INT4 i4SetValFsWtpNATConfigRowStatus)
{
    tWssIfNATConfigEntryDB WssifNATConfigEntry;

    MEMSET (&WssifNATConfigEntry, 0, sizeof (tWssIfNATConfigEntryDB));

    WssifNATConfigEntry.i4NATConfigEntryIndex = i4FsWtpNATConfigIndex;
    WssifNATConfigEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    if (WssIfNATConfigGetEntry (&WssifNATConfigEntry) == OSIX_SUCCESS)
    {
        WssifNATConfigEntry.i4RowStatus = i4SetValFsWtpNATConfigRowStatus;

        if (i4SetValFsWtpNATConfigRowStatus == DESTROY)
        {
            WssifNatUpdateReq (u4CapwapBaseWtpProfileId,
                               i4FsWtpNATConfigIndex,
                               i4SetValFsWtpNATConfigRowStatus);
            if (WssIfNATConfigDeleteEntry (&WssifNATConfigEntry) ==
                OSIX_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
        else if (WssIfNATConfigSetEntry (&WssifNATConfigEntry) == OSIX_SUCCESS)
        {
            WssifNatUpdateReq (u4CapwapBaseWtpProfileId, i4FsWtpNATConfigIndex,
                               i4SetValFsWtpNATConfigRowStatus);
            {
                return SNMP_SUCCESS;
            }
        }
    }
    else
    {
        if (i4SetValFsWtpNATConfigRowStatus == CREATE_AND_WAIT)
        {
            WssifNATConfigEntry.i4RowStatus = NOT_READY;
            if (WssIfNATConfigAddEntry (&WssifNATConfigEntry) == OSIX_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
        else if (i4SetValFsWtpNATConfigRowStatus == CREATE_AND_GO)
        {
            WssifNATConfigEntry.i4RowStatus = ACTIVE;
            if (WssIfNATConfigAddEntry (&WssifNATConfigEntry) == OSIX_SUCCESS)
            {
                WssifNatUpdateReq (u4CapwapBaseWtpProfileId,
                                   i4FsWtpNATConfigIndex,
                                   i4SetValFsWtpNATConfigRowStatus);
                return SNMP_SUCCESS;
            }
        }

    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsWtpIfMainWanType
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpNATConfigIndex

                The Object 
                testValFsWtpIfMainWanType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWtpIfMainWanType (UINT4 *pu4ErrorCode,
                             UINT4 u4CapwapBaseWtpProfileId,
                             INT4 i4FsWtpNATConfigIndex,
                             INT4 i4TestValFsWtpIfMainWanType)
{
    tWssIfNATConfigEntryDB WssifNATConfigEntry;
    MEMSET (&WssifNATConfigEntry, 0, sizeof (tWssIfNATConfigEntryDB));
    WssifNATConfigEntry.i4NATConfigEntryIndex = i4FsWtpNATConfigIndex;
    WssifNATConfigEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    if (WssIfNATConfigGetEntry (&WssifNATConfigEntry) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (WssifNATConfigEntry.i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsWtpIfMainWanType != WAN_TYPE_PRIVATE) &&
        (i4TestValFsWtpIfMainWanType != WAN_TYPE_PUBLIC))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWtpNATConfigRowStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpNATConfigIndex

                The Object 
                testValFsWtpNATConfigRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWtpNATConfigRowStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4CapwapBaseWtpProfileId,
                                  INT4 i4FsWtpNATConfigIndex,
                                  INT4 i4TestValFsWtpNATConfigRowStatus)
{
    if ((i4FsWtpNATConfigIndex < NAT_CONFIG_INDEX_LOWER_LIMIT) ||
        (i4FsWtpNATConfigIndex > NAT_CONFIG_INDEX_UPPER_LIMIT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    tWssIfNATConfigEntryDB WssIfNATConfigEntry;
    tWssIfNATConfigEntryDB *pWssIfNATConfigEntry = &WssIfNATConfigEntry;

    MEMSET (&WssIfNATConfigEntry, 0, sizeof (tWssIfNATConfigEntryDB));

    pWssIfNATConfigEntry->i4NATConfigEntryIndex = i4FsWtpNATConfigIndex;
    pWssIfNATConfigEntry->u4WtpProfileId = u4CapwapBaseWtpProfileId;
    if (WssIfNATConfigGetEntry (pWssIfNATConfigEntry) != OSIX_FAILURE)
    {
        if ((i4TestValFsWtpNATConfigRowStatus == ACTIVE) ||
            (i4TestValFsWtpNATConfigRowStatus == DESTROY) ||
            (i4TestValFsWtpNATConfigRowStatus == NOT_IN_SERVICE))
        {
            if (i4TestValFsWtpNATConfigRowStatus == ACTIVE)
            {
                if ((pWssIfNATConfigEntry->
                     i4NATConfigEntryIndex == 0) ||
                    (pWssIfNATConfigEntry->i1WanType == 0) ||
                    (pWssIfNATConfigEntry->u4WtpProfileId == 0))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
                if ((pWssIfNATConfigEntry->i4RowStatus != NOT_IN_SERVICE) &&
                    (pWssIfNATConfigEntry->i4RowStatus != NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
                    return SNMP_FAILURE;
                }
            }

            return SNMP_SUCCESS;
        }
    }
    else
    {
        if ((i4TestValFsWtpNATConfigRowStatus == CREATE_AND_WAIT) ||
            (i4TestValFsWtpNATConfigRowStatus == CREATE_AND_GO))
            return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsWtpNATTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpNATConfigIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsWtpNATTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWtpDhcpConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWtpDhcpConfigTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWtpDhcpConfigTable (UINT4 u4CapwapBaseWtpProfileId)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsWtpDhcpConfigTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsWtpDhcpConfigTable (UINT4 *pu4CapwapBaseWtpProfileId)
{
    if (WssIfWtpDhcpConfigGetFirstEntry (pu4CapwapBaseWtpProfileId)
        == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsWtpDhcpConfigTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
                nextCapwapBaseWtpProfileId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsWtpDhcpConfigTable (UINT4 u4CapwapBaseWtpProfileId,
                                     UINT4 *pu4NextCapwapBaseWtpProfileId)
{
    if (WssIfWtpDhcpConfigGetNextEntry (&u4CapwapBaseWtpProfileId,
                                        pu4NextCapwapBaseWtpProfileId) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsWtpDhcpServerStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId

                The Object 
                retValFsWtpDhcpServerStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWtpDhcpServerStatus (UINT4 u4CapwapBaseWtpProfileId,
                             INT4 *pi4RetValFsWtpDhcpServerStatus)
{
    tWssIfWtpDhcpConfigDB WssIfWtpDhcpConfigDB;

    MEMSET (&WssIfWtpDhcpConfigDB, 0, sizeof (tWssIfWtpDhcpConfigDB));

    WssIfWtpDhcpConfigDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    if (WssIfWtpDhcpConfigGetEntry (&WssIfWtpDhcpConfigDB) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsWtpDhcpServerStatus =
        WssIfWtpDhcpConfigDB.i4WtpDhcpServerStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWtpDhcpRelayStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId

                The Object 
                retValFsWtpDhcpRelayStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWtpDhcpRelayStatus (UINT4 u4CapwapBaseWtpProfileId,
                            INT4 *pi4RetValFsWtpDhcpRelayStatus)
{
    tWssIfWtpDhcpConfigDB WssIfWtpDhcpConfigDB;
    MEMSET (&WssIfWtpDhcpConfigDB, 0, sizeof (tWssIfWtpDhcpConfigDB));
    WssIfWtpDhcpConfigDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    if (WssIfWtpDhcpConfigGetEntry (&WssIfWtpDhcpConfigDB) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsWtpDhcpRelayStatus = WssIfWtpDhcpConfigDB.i4WtpDhcpRelayStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsWtpDhcpServerStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId

                The Object 
                setValFsWtpDhcpServerStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWtpDhcpServerStatus (UINT4 u4CapwapBaseWtpProfileId,
                             INT4 i4SetValFsWtpDhcpServerStatus)
{
    tWssIfWtpDhcpConfigDB WssIfWtpDhcpConfigDB;

    MEMSET (&WssIfWtpDhcpConfigDB, 0, sizeof (tWssIfWtpDhcpConfigDB));
    WssIfWtpDhcpConfigDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;

    if (WssIfWtpDhcpConfigGetEntry (&WssIfWtpDhcpConfigDB) != OSIX_FAILURE)
    {
        WssIfWtpDhcpConfigDB.i4WtpDhcpServerStatus =
            i4SetValFsWtpDhcpServerStatus;
        if (WssIfWtpDhcpConfigSetEntry (&WssIfWtpDhcpConfigDB) == OSIX_SUCCESS)
        {
            WssifStartDhcpPoolUpdateReq (u4CapwapBaseWtpProfileId,
                                         (UINT1) i4SetValFsWtpDhcpServerStatus,
                                         0, ACTIVE);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsWtpDhcpRelayStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId

                The Object 
                setValFsWtpDhcpRelayStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWtpDhcpRelayStatus (UINT4 u4CapwapBaseWtpProfileId,
                            INT4 i4SetValFsWtpDhcpRelayStatus)
{
    tWssIfWtpDhcpConfigDB WssIfWtpDhcpConfigDB;
    UINT4               u4NextIpAddress = 0;

    MEMSET (&WssIfWtpDhcpConfigDB, 0, sizeof (tWssIfWtpDhcpConfigDB));
    WssIfWtpDhcpConfigDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    if (WssIfWtpDhcpConfigGetEntry (&WssIfWtpDhcpConfigDB) != OSIX_FAILURE)
    {
        WssIfWtpDhcpConfigDB.i4WtpDhcpRelayStatus =
            i4SetValFsWtpDhcpRelayStatus;
        if (WssIfWtpDhcpConfigSetEntry (&WssIfWtpDhcpConfigDB) == OSIX_SUCCESS)
        {
            u4NextIpAddress = WssIfDhcpNextSrvIpAddrGet ();
            WssifStartDhcpRelayUpdateReq (u4CapwapBaseWtpProfileId,
                                          i4SetValFsWtpDhcpRelayStatus,
                                          u4NextIpAddress);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsWtpDhcpServerStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId

                The Object 
                testValFsWtpDhcpServerStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWtpDhcpServerStatus (UINT4 *pu4ErrorCode,
                                UINT4 u4CapwapBaseWtpProfileId,
                                INT4 i4TestValFsWtpDhcpServerStatus)
{
    tWssIfWtpDhcpConfigDB WssIfWtpDhcpConfigDB;
    MEMSET (&WssIfWtpDhcpConfigDB, 0, sizeof (tWssIfWtpDhcpConfigDB));
    WssIfWtpDhcpConfigDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    if (WssIfWtpDhcpConfigGetEntry (&WssIfWtpDhcpConfigDB) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsWtpDhcpServerStatus != 1) &&
        (i4TestValFsWtpDhcpServerStatus != 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((WssIfWtpDhcpConfigDB.i4WtpDhcpRelayStatus == 1) &&
        (i4TestValFsWtpDhcpServerStatus == 1))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWtpDhcpRelayStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId

                The Object 
                testValFsWtpDhcpRelayStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWtpDhcpRelayStatus (UINT4 *pu4ErrorCode,
                               UINT4 u4CapwapBaseWtpProfileId,
                               INT4 i4TestValFsWtpDhcpRelayStatus)
{
    tWssIfWtpDhcpConfigDB WssIfWtpDhcpConfigDB;
    MEMSET (&WssIfWtpDhcpConfigDB, 0, sizeof (tWssIfWtpDhcpConfigDB));
    WssIfWtpDhcpConfigDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    if (WssIfWtpDhcpConfigGetEntry (&WssIfWtpDhcpConfigDB) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsWtpDhcpRelayStatus != 1) &&
        (i4TestValFsWtpDhcpRelayStatus != 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((WssIfWtpDhcpConfigDB.i4WtpDhcpServerStatus == 1) &&
        (i4TestValFsWtpDhcpRelayStatus == 1))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsWtpDhcpConfigTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsWtpDhcpConfigTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWtpFirewallConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWtpFirewallConfigTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWtpFirewallConfigTable (UINT4
                                                  u4CapwapBaseWtpProfileId)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsWtpFirewallConfigTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsWtpFirewallConfigTable (UINT4 *pu4CapwapBaseWtpProfileId)
{
    if (WssIfWtpLRConfigGetFirstEntry (pu4CapwapBaseWtpProfileId)
        == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsWtpFirewallConfigTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
                nextCapwapBaseWtpProfileId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsWtpFirewallConfigTable (UINT4 u4CapwapBaseWtpProfileId,
                                         UINT4 *pu4NextCapwapBaseWtpProfileId)
{
    if (WssIfWtpLRConfigGetNextEntry (&u4CapwapBaseWtpProfileId,
                                      pu4NextCapwapBaseWtpProfileId) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsWtpFirewallStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId

                The Object 
                retValFsWtpFirewallStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWtpFirewallStatus (UINT4 u4CapwapBaseWtpProfileId,
                           INT4 *pi4RetValFsWtpFirewallStatus)
{
    tWssIfWtpLRConfigDB WssIfWtpLRConfigDB;
    MEMSET (&WssIfWtpLRConfigDB, 0, sizeof (tWssIfWtpLRConfigDB));
    WssIfWtpLRConfigDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    if (WssIfWtpLRConfigGetEntry (&WssIfWtpLRConfigDB) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsWtpFirewallStatus = WssIfWtpLRConfigDB.i4WtpFirewallStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWtpFirewallRowStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId

                The Object 
                retValFsWtpFirewallRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWtpFirewallRowStatus (UINT4 u4CapwapBaseWtpProfileId,
                              INT4 *pi4RetValFsWtpFirewallRowStatus)
{
    tWssIfWtpLRConfigDB WssIfWtpLRConfigDB;
    MEMSET (&WssIfWtpLRConfigDB, 0, sizeof (tWssIfWtpLRConfigDB));
    WssIfWtpLRConfigDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    if (WssIfWtpLRConfigGetEntry (&WssIfWtpLRConfigDB) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsWtpFirewallRowStatus = WssIfWtpLRConfigDB.i4RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsWtpFirewallStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId

                The Object 
                setValFsWtpFirewallStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWtpFirewallStatus (UINT4 u4CapwapBaseWtpProfileId,
                           INT4 i4SetValFsWtpFirewallStatus)
{
    tWssIfWtpLRConfigDB WssIfWtpLRConfigDB;
    MEMSET (&WssIfWtpLRConfigDB, 0, sizeof (tWssIfWtpLRConfigDB));
    WssIfWtpLRConfigDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    if (WssIfWtpLRConfigGetEntry (&WssIfWtpLRConfigDB) != OSIX_FAILURE)
    {
        WssIfWtpLRConfigDB.i4WtpFirewallStatus = i4SetValFsWtpFirewallStatus;
        if (WssIfWtpLRConfigSetEntry (&WssIfWtpLRConfigDB) == OSIX_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsWtpFirewallRowStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId

                The Object 
                setValFsWtpFirewallRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWtpFirewallRowStatus (UINT4 u4CapwapBaseWtpProfileId,
                              INT4 i4SetValFsWtpFirewallRowStatus)
{
    tWssIfWtpLRConfigDB WssIfWtpLRConfigEntry;

    MEMSET (&WssIfWtpLRConfigEntry, 0, sizeof (tWssIfWtpLRConfigDB));
    WssIfWtpLRConfigEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;

    if (WssIfWtpLRConfigGetEntry (&WssIfWtpLRConfigEntry) != OSIX_FAILURE)
    {
        WssIfWtpLRConfigEntry.i4RowStatus = i4SetValFsWtpFirewallRowStatus;
        if (i4SetValFsWtpFirewallRowStatus == DESTROY)
        {
            WssifFireWallUpdateReq (u4CapwapBaseWtpProfileId,
                                    i4SetValFsWtpFirewallRowStatus);
            if (WssIfWtpLRConfigDeleteEntry (&WssIfWtpLRConfigEntry) ==
                OSIX_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
        else
        {
            if (WssIfWtpLRConfigSetEntry (&WssIfWtpLRConfigEntry)
                == OSIX_SUCCESS)
            {
                WssifFireWallUpdateReq (u4CapwapBaseWtpProfileId,
                                        i4SetValFsWtpFirewallRowStatus);
                return SNMP_SUCCESS;
            }
        }
    }
    else if (i4SetValFsWtpFirewallRowStatus == CREATE_AND_WAIT)
    {
        WssIfWtpLRConfigEntry.i4RowStatus = NOT_READY;
        if (WssIfWtpLRConfigAddEntry (&WssIfWtpLRConfigEntry) == OSIX_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    else if (i4SetValFsWtpFirewallRowStatus == CREATE_AND_GO)
    {
        WssIfWtpLRConfigEntry.i4RowStatus = ACTIVE;
        if (WssIfWtpLRConfigAddEntry (&WssIfWtpLRConfigEntry) == OSIX_SUCCESS)
        {
            WssifFireWallUpdateReq (u4CapwapBaseWtpProfileId,
                                    i4SetValFsWtpFirewallRowStatus);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsWtpFirewallStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId

                The Object 
                testValFsWtpFirewallStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWtpFirewallStatus (UINT4 *pu4ErrorCode,
                              UINT4 u4CapwapBaseWtpProfileId,
                              INT4 i4TestValFsWtpFirewallStatus)
{
    tWssIfWtpLRConfigDB WssIfWtpLRConfigEntry;
    MEMSET (&WssIfWtpLRConfigEntry, 0, sizeof (tWssIfWtpLRConfigDB));

    WssIfWtpLRConfigEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    if (WssIfWtpLRConfigGetEntry (&WssIfWtpLRConfigEntry) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (WssIfWtpLRConfigEntry.i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsWtpFirewallStatus != 1) &&
        (i4TestValFsWtpFirewallStatus != 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWtpFirewallRowStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId

                The Object 
                testValFsWtpFirewallRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWtpFirewallRowStatus (UINT4 *pu4ErrorCode,
                                 UINT4 u4CapwapBaseWtpProfileId,
                                 INT4 i4TestValFsWtpFirewallRowStatus)
{
    tWssIfWtpLRConfigDB WssIfWtpLRConfigDB;

    MEMSET (&WssIfWtpLRConfigDB, 0, sizeof (tWssIfWtpLRConfigDB));

    WssIfWtpLRConfigDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;

    if (WssIfWtpLRConfigGetEntry (&WssIfWtpLRConfigDB) == OSIX_FAILURE)
    {
        if ((i4TestValFsWtpFirewallRowStatus == CREATE_AND_WAIT) ||
            (i4TestValFsWtpFirewallRowStatus == CREATE_AND_GO))
        {
            return SNMP_SUCCESS;
        }
    }
    else
    {
        if (i4TestValFsWtpFirewallRowStatus == ACTIVE)
        {
            if ((WssIfWtpLRConfigDB.i4RowStatus != NOT_IN_SERVICE) &&
                (WssIfWtpLRConfigDB.i4RowStatus != NOT_READY))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
        if ((i4TestValFsWtpFirewallRowStatus == NOT_IN_SERVICE) ||
            (i4TestValFsWtpFirewallRowStatus == NOT_READY))
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsWtpFirewallConfigTable
Input       :  The Indices
CapwapBaseWtpProfileId
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsWtpFirewallConfigTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWtpIpRouteConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWtpIpRouteConfigTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIpRouteConfigSubnet
                FsWtpIpRouteConfigNetMask
                FsWtpIpRouteConfigGateway
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWtpIpRouteConfigTable (UINT4 u4CapwapBaseWtpProfileId,
                                                 UINT4
                                                 u4FsWtpIpRouteConfigSubnet,
                                                 UINT4
                                                 u4FsWtpIpRouteConfigNetMask,
                                                 UINT4
                                                 u4FsWtpIpRouteConfigGateway)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (u4FsWtpIpRouteConfigSubnet);
    UNUSED_PARAM (u4FsWtpIpRouteConfigNetMask);
    UNUSED_PARAM (u4FsWtpIpRouteConfigGateway);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsWtpIpRouteConfigTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIpRouteConfigSubnet
                FsWtpIpRouteConfigNetMask
                FsWtpIpRouteConfigGateway
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsWtpIpRouteConfigTable (UINT4 *pu4CapwapBaseWtpProfileId,
                                         UINT4 *pu4FsWtpIpRouteConfigSubnet,
                                         UINT4 *pu4FsWtpIpRouteConfigNetMask,
                                         UINT4 *pu4FsWtpIpRouteConfigGateway)
{
    if (WssIfWtpIpRouteConfigGetFirstEntry (pu4CapwapBaseWtpProfileId,
                                            pu4FsWtpIpRouteConfigSubnet,
                                            pu4FsWtpIpRouteConfigNetMask,
                                            pu4FsWtpIpRouteConfigGateway) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsWtpIpRouteConfigTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
                nextCapwapBaseWtpProfileId
                FsWtpIpRouteConfigSubnet
                nextFsWtpIpRouteConfigSubnet
                FsWtpIpRouteConfigNetMask
                nextFsWtpIpRouteConfigNetMask
                FsWtpIpRouteConfigGateway
                nextFsWtpIpRouteConfigGateway
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsWtpIpRouteConfigTable (UINT4 u4CapwapBaseWtpProfileId,
                                        UINT4 *pu4NextCapwapBaseWtpProfileId,
                                        UINT4 u4FsWtpIpRouteConfigSubnet,
                                        UINT4 *pu4NextFsWtpIpRouteConfigSubnet,
                                        UINT4 u4FsWtpIpRouteConfigNetMask,
                                        UINT4 *pu4NextFsWtpIpRouteConfigNetMask,
                                        UINT4 u4FsWtpIpRouteConfigGateway,
                                        UINT4 *pu4NextFsWtpIpRouteConfigGateway)
{
    if (WssIfWtpIpRouteConfigGetNextEntry (&u4CapwapBaseWtpProfileId,
                                           pu4NextCapwapBaseWtpProfileId,
                                           &u4FsWtpIpRouteConfigSubnet,
                                           pu4NextFsWtpIpRouteConfigSubnet,
                                           &u4FsWtpIpRouteConfigNetMask,
                                           pu4NextFsWtpIpRouteConfigNetMask,
                                           &u4FsWtpIpRouteConfigGateway,
                                           pu4NextFsWtpIpRouteConfigGateway) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsWtpIpRouteConfigRowStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIpRouteConfigSubnet
                FsWtpIpRouteConfigNetMask
                FsWtpIpRouteConfigGateway

                The Object 
                retValFsWtpIpRouteConfigRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWtpIpRouteConfigRowStatus (UINT4 u4CapwapBaseWtpProfileId,
                                   UINT4 u4FsWtpIpRouteConfigSubnet,
                                   UINT4 u4FsWtpIpRouteConfigNetMask,
                                   UINT4 u4FsWtpIpRouteConfigGateway,
                                   INT4 *pi4RetValFsWtpIpRouteConfigRowStatus)
{
    tWssIfWtpIpRouteConfigDB WssIfWtpIpRouteConfigEntry;

    MEMSET (&WssIfWtpIpRouteConfigEntry, 0, sizeof (tWssIfWtpIpRouteConfigDB));

    WssIfWtpIpRouteConfigEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpIpRouteConfigEntry.u4Subnet = u4FsWtpIpRouteConfigSubnet;
    WssIfWtpIpRouteConfigEntry.u4Mask = u4FsWtpIpRouteConfigNetMask;
    WssIfWtpIpRouteConfigEntry.u4Gateway = u4FsWtpIpRouteConfigGateway;

    if (WssIfWtpIpRouteConfigGetEntry (&WssIfWtpIpRouteConfigEntry)
        == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsWtpIpRouteConfigRowStatus =
        WssIfWtpIpRouteConfigEntry.i4RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsWtpIpRouteConfigRowStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIpRouteConfigSubnet
                FsWtpIpRouteConfigNetMask
                FsWtpIpRouteConfigGateway

                The Object 
                setValFsWtpIpRouteConfigRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWtpIpRouteConfigRowStatus (UINT4 u4CapwapBaseWtpProfileId,
                                   UINT4 u4FsWtpIpRouteConfigSubnet,
                                   UINT4 u4FsWtpIpRouteConfigNetMask,
                                   UINT4 u4FsWtpIpRouteConfigGateway,
                                   INT4 i4SetValFsWtpIpRouteConfigRowStatus)
{
    tWssIfWtpIpRouteConfigDB WssIfWtpIpRouteConfigEntry;
    MEMSET (&WssIfWtpIpRouteConfigEntry, 0, sizeof (tWssIfWtpIpRouteConfigDB));

    WssIfWtpIpRouteConfigEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpIpRouteConfigEntry.u4Subnet = u4FsWtpIpRouteConfigSubnet;
    WssIfWtpIpRouteConfigEntry.u4Mask = u4FsWtpIpRouteConfigNetMask;
    WssIfWtpIpRouteConfigEntry.u4Gateway = u4FsWtpIpRouteConfigGateway;

    if (WssIfWtpIpRouteConfigGetEntry (&WssIfWtpIpRouteConfigEntry)
        != OSIX_FAILURE)
    {
        WssIfWtpIpRouteConfigEntry.i4RowStatus =
            i4SetValFsWtpIpRouteConfigRowStatus;
        if (i4SetValFsWtpIpRouteConfigRowStatus == DESTROY)
        {
            WssifRouteTable (u4CapwapBaseWtpProfileId,
                             u4FsWtpIpRouteConfigSubnet,
                             u4FsWtpIpRouteConfigNetMask,
                             u4FsWtpIpRouteConfigGateway,
                             (UINT4) i4SetValFsWtpIpRouteConfigRowStatus);
            if (WssIfWtpIpRouteConfigDeleteEntry (&WssIfWtpIpRouteConfigEntry)
                == OSIX_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
        else if (WssIfWtpIpRouteConfigSetEntry (&WssIfWtpIpRouteConfigEntry)
                 == OSIX_SUCCESS)
        {
            if (i4SetValFsWtpIpRouteConfigRowStatus == ACTIVE)
            {
                WssifRouteTable (u4CapwapBaseWtpProfileId,
                                 u4FsWtpIpRouteConfigSubnet,
                                 u4FsWtpIpRouteConfigNetMask,
                                 u4FsWtpIpRouteConfigGateway,
                                 (UINT4) i4SetValFsWtpIpRouteConfigRowStatus);
            }
            return SNMP_SUCCESS;
        }
    }
    else
    {
        if (i4SetValFsWtpIpRouteConfigRowStatus == CREATE_AND_WAIT)
        {
            WssIfWtpIpRouteConfigEntry.i4RowStatus = NOT_READY;
            if (WssIfWtpIpRouteConfigAddEntry (&WssIfWtpIpRouteConfigEntry)
                == OSIX_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        }
        else if (i4SetValFsWtpIpRouteConfigRowStatus == CREATE_AND_GO)
        {
            WssIfWtpIpRouteConfigEntry.i4RowStatus = ACTIVE;
            if (WssIfWtpIpRouteConfigAddEntry (&WssIfWtpIpRouteConfigEntry)
                == OSIX_SUCCESS)
            {
                WssifRouteTable (u4CapwapBaseWtpProfileId,
                                 u4FsWtpIpRouteConfigSubnet,
                                 u4FsWtpIpRouteConfigNetMask,
                                 u4FsWtpIpRouteConfigGateway,
                                 (UINT4) i4SetValFsWtpIpRouteConfigRowStatus);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsWtpIpRouteConfigRowStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIpRouteConfigSubnet
                FsWtpIpRouteConfigNetMask
                FsWtpIpRouteConfigGateway

                The Object 
                testValFsWtpIpRouteConfigRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWtpIpRouteConfigRowStatus (UINT4 *pu4ErrorCode,
                                      UINT4 u4CapwapBaseWtpProfileId,
                                      UINT4 u4FsWtpIpRouteConfigSubnet,
                                      UINT4 u4FsWtpIpRouteConfigNetMask,
                                      UINT4 u4FsWtpIpRouteConfigGateway,
                                      INT4 i4TestValFsWtpIpRouteConfigRowStatus)
{
    tWssIfWtpIpRouteConfigDB WssIfWtpIpRouteConfigDB;
    MEMSET (&WssIfWtpIpRouteConfigDB, 0, sizeof (tWssIfWtpIpRouteConfigDB));
    WssIfWtpIpRouteConfigDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpIpRouteConfigDB.u4Subnet = u4FsWtpIpRouteConfigSubnet;
    WssIfWtpIpRouteConfigDB.u4Mask = u4FsWtpIpRouteConfigNetMask;
    WssIfWtpIpRouteConfigDB.u4Gateway = u4FsWtpIpRouteConfigGateway;

    if (WssIfWtpIpRouteConfigGetEntry (&WssIfWtpIpRouteConfigDB)
        == OSIX_SUCCESS)
    {
        if (i4TestValFsWtpIpRouteConfigRowStatus == ACTIVE)
        {
            if ((WssIfWtpIpRouteConfigDB.i4RowStatus != NOT_IN_SERVICE) &&
                (WssIfWtpIpRouteConfigDB.i4RowStatus != NOT_READY))
            {
                *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
                return SNMP_FAILURE;
            }
        }
        return SNMP_SUCCESS;
    }
    else
    {
        if ((i4TestValFsWtpIpRouteConfigRowStatus == CREATE_AND_WAIT) ||
            (i4TestValFsWtpIpRouteConfigRowStatus == CREATE_AND_GO))
            return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsWtpIpRouteConfigTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIpRouteConfigSubnet
                FsWtpIpRouteConfigNetMask
                FsWtpIpRouteConfigGateway
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsWtpIpRouteConfigTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWtpFwlFilterTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsWtpFwlFilterTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWtpFwlFilterTable (UINT4 u4CapwapBaseWtpProfileId,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsWtpFwlFilterName)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pFsWtpFwlFilterName);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsWtpFwlFilterTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsWtpFwlFilterTable (UINT4 *pu4CapwapBaseWtpProfileId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsWtpFwlFilterName)
{
    INT4                i4RetVal = 0;

    i4RetVal = WssIfWtpFwlFilterGetFirstEntry (pu4CapwapBaseWtpProfileId,
                                               pFsWtpFwlFilterName->
                                               pu1_OctetList);

    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pFsWtpFwlFilterName->i4_Length =
        STRLEN (pFsWtpFwlFilterName->pu1_OctetList);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsWtpFwlFilterTable
Input       :  The Indices
CapwapBaseWtpProfileId
nextCapwapBaseWtpProfileId
FsWtpFwlFilterName
nextFsWtpFwlFilterName
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsWtpFwlFilterTable (UINT4 u4CapwapBaseWtpProfileId,
                                    UINT4 *pu4NextCapwapBaseWtpProfileId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsWtpFwlFilterName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextFsWtpFwlFilterName)
{
    INT4                i4RetVal = 0;

    i4RetVal =
        WssIfWtpFwlFilterGetNextEntry (u4CapwapBaseWtpProfileId,
                                       pu4NextCapwapBaseWtpProfileId,
                                       pFsWtpFwlFilterName->pu1_OctetList,
                                       pNextFsWtpFwlFilterName->pu1_OctetList);

    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pNextFsWtpFwlFilterName->i4_Length =
        STRLEN (pNextFsWtpFwlFilterName->pu1_OctetList);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsWtpFwlFilterSrcAddress
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
retValFsWtpFwlFilterSrcAddress
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpFwlFilterSrcAddress (UINT4 u4CapwapBaseWtpProfileId,
                                tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsWtpFwlFilterSrcAddress)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));
    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;
    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {
        pRetValFsWtpFwlFilterSrcAddress->i4_Length =
            STRLEN (WssIfWtpFwlFilter.au1SrcAddr);
        MEMCPY (pRetValFsWtpFwlFilterSrcAddress->pu1_OctetList,
                WssIfWtpFwlFilter.au1SrcAddr,
                pRetValFsWtpFwlFilterSrcAddress->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsWtpFwlFilterDestAddress
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
retValFsWtpFwlFilterDestAddress
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpFwlFilterDestAddress (UINT4 u4CapwapBaseWtpProfileId,
                                 tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsWtpFwlFilterDestAddress)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));
    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;
    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {
        pRetValFsWtpFwlFilterDestAddress->i4_Length =
            STRLEN (WssIfWtpFwlFilter.au1DestAddr);
        MEMCPY (pRetValFsWtpFwlFilterDestAddress->pu1_OctetList,
                WssIfWtpFwlFilter.au1DestAddr,
                pRetValFsWtpFwlFilterDestAddress->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsWtpFwlFilterProtocol
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
retValFsWtpFwlFilterProtocol
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpFwlFilterProtocol (UINT4 u4CapwapBaseWtpProfileId,
                              tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                              INT4 *pi4RetValFsWtpFwlFilterProtocol)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));
    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;
    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {
        *pi4RetValFsWtpFwlFilterProtocol = WssIfWtpFwlFilter.u1Proto;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsWtpFwlFilterSrcPort
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
retValFsWtpFwlFilterSrcPort
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpFwlFilterSrcPort (UINT4 u4CapwapBaseWtpProfileId,
                             tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsWtpFwlFilterSrcPort)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));
    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;
    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {
        pRetValFsWtpFwlFilterSrcPort->i4_Length =
            STRLEN (WssIfWtpFwlFilter.au1SrcPort);
        MEMCPY (pRetValFsWtpFwlFilterSrcPort->pu1_OctetList,
                WssIfWtpFwlFilter.au1SrcPort,
                pRetValFsWtpFwlFilterSrcPort->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsWtpFwlFilterDestPort
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
retValFsWtpFwlFilterDestPort
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpFwlFilterDestPort (UINT4 u4CapwapBaseWtpProfileId,
                              tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsWtpFwlFilterDestPort)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));
    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;
    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {
        pRetValFsWtpFwlFilterDestPort->i4_Length =
            STRLEN (WssIfWtpFwlFilter.au1DestPort);
        MEMCPY (pRetValFsWtpFwlFilterDestPort->pu1_OctetList,
                WssIfWtpFwlFilter.au1DestPort,
                pRetValFsWtpFwlFilterDestPort->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsWtpFwlFilterTos
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
retValFsWtpFwlFilterTos
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpFwlFilterTos (UINT4 u4CapwapBaseWtpProfileId,
                         tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                         INT4 *pi4RetValFsWtpFwlFilterTos)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));
    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;
    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {
        *pi4RetValFsWtpFwlFilterTos = (INT4) WssIfWtpFwlFilter.u1Tos;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsWtpFwlFilterAccounting
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
retValFsWtpFwlFilterAccounting
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpFwlFilterAccounting (UINT4 u4CapwapBaseWtpProfileId,
                                tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                                INT4 *pi4RetValFsWtpFwlFilterAccounting)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));
    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;
    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {
        *pi4RetValFsWtpFwlFilterAccounting =
            (INT4) WssIfWtpFwlFilter.u1FilterAccounting;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsWtpFwlFilterHitClear
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
retValFsWtpFwlFilterHitClear
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpFwlFilterHitClear (UINT4 u4CapwapBaseWtpProfileId,
                              tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                              INT4 *pi4RetValFsWtpFwlFilterHitClear)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));
    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;
    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {
        /* The get routine always returns 'false' for the
         * MIB object WtpFwlFilterHitClear */
        *pi4RetValFsWtpFwlFilterHitClear = AP_FWL_FALSE;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsWtpFwlFilterHitsCount
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
retValFsWtpFwlFilterHitsCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpFwlFilterHitsCount (UINT4 u4CapwapBaseWtpProfileId,
                               tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                               UINT4 *pu4RetValFsWtpFwlFilterHitsCount)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));
    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;
    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {
        *pu4RetValFsWtpFwlFilterHitsCount = WssIfWtpFwlFilter.u4FilterHitCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsWtpFwlFilterAddrType
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
retValFsWtpFwlFilterAddrType
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpFwlFilterAddrType (UINT4 u4CapwapBaseWtpProfileId,
                              tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                              INT4 *pi4RetValFsWtpFwlFilterAddrType)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));
    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;
    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {
        *pi4RetValFsWtpFwlFilterAddrType = (INT4) WssIfWtpFwlFilter.u2AddrType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsWtpFwlFilterFlowId
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
retValFsWtpFwlFilterFlowId
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpFwlFilterFlowId (UINT4 u4CapwapBaseWtpProfileId,
                            tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                            UINT4 *pu4RetValFsWtpFwlFilterFlowId)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));
    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;
    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {
        *pu4RetValFsWtpFwlFilterFlowId = WssIfWtpFwlFilter.u4FlowId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsWtpFwlFilterDscp
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
retValFsWtpFwlFilterDscp
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpFwlFilterDscp (UINT4 u4CapwapBaseWtpProfileId,
                          tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                          INT4 *pi4RetValFsWtpFwlFilterDscp)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));
    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;
    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {
        *pi4RetValFsWtpFwlFilterDscp = WssIfWtpFwlFilter.i4Dscp;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsWtpFwlFilterRowStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
retValFsWtpFwlFilterRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpFwlFilterRowStatus (UINT4 u4CapwapBaseWtpProfileId,
                               tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                               INT4 *pi4RetValFsWtpFwlFilterRowStatus)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));
    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;
    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {
        *pi4RetValFsWtpFwlFilterRowStatus = WssIfWtpFwlFilter.i4RowStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsWtpFwlFilterSrcAddress
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
setValFsWtpFwlFilterSrcAddress
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpFwlFilterSrcAddress (UINT4 u4CapwapBaseWtpProfileId,
                                tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsWtpFwlFilterSrcAddress)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;
    UINT4               u4SrcStartAddr = SNMP_FAILURE;
    UINT4               u4SrcEndAddr = AP_FWL_ZERO;
    INT1                i1Status = AP_FWL_ZERO;
    UINT1               au1SrcAddr[AP_FWL_MAX_ADDR_LEN] = { AP_FWL_ZERO };

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));

    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;

    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {
        MEMCPY (WssIfWtpFwlFilter.au1SrcAddr,
                pSetValFsWtpFwlFilterSrcAddress->pu1_OctetList,
                STRLEN ((pSetValFsWtpFwlFilterSrcAddress->pu1_OctetList) - 1));
    }
    else
    {
        return SNMP_FAILURE;
    }

    pSetValFsWtpFwlFilterSrcAddress->i4_Length = STRLEN
        (pSetValFsWtpFwlFilterSrcAddress->pu1_OctetList);
    WssIfWtpFwlFilter.au1SrcAddr[pSetValFsWtpFwlFilterSrcAddress->i4_Length]
        = AP_FWL_END_OF_STRING;

    MEMCPY (au1SrcAddr, pSetValFsWtpFwlFilterSrcAddress->pu1_OctetList,
            pSetValFsWtpFwlFilterSrcAddress->i4_Length);
    au1SrcAddr[pSetValFsWtpFwlFilterSrcAddress->i4_Length] =
        AP_FWL_END_OF_STRING;

    if (AP_FWL_IP_VERSION_4 == WssIfWtpFwlFilter.u2AddrType)
    {
        i1Status =
            ApFwlParseIpAddr (au1SrcAddr, &u4SrcStartAddr, &u4SrcEndAddr);
        if (i1Status == AP_FWL_SUCCESS)
        {
            WssIfWtpFwlFilter.SrcStartAddr.v4Addr = u4SrcStartAddr;
            WssIfWtpFwlFilter.SrcEndAddr.v4Addr = u4SrcEndAddr;
            WssIfWtpFwlFilter.SrcStartAddr.u4AddrType = AP_FWL_IP_VERSION_4;
            WssIfWtpFwlFilter.SrcEndAddr.u4AddrType = AP_FWL_IP_VERSION_4;
            WssIfWtpFwlFilter.i4RowStatus = AP_FWL_NOT_IN_SERVICE;
            if (OSIX_SUCCESS == WssIfWtpFwlFilterSetEntry (&WssIfWtpFwlFilter))
            {
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                i1Status = SNMP_FAILURE;
            }

        }
        else
        {
            i1Status = SNMP_FAILURE;
        }
    }
    else
    {
        i1Status = SNMP_FAILURE;
    }
    return i1Status;
}

/****************************************************************************
Function    :  nmhSetFsWtpFwlFilterDestAddress
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
setValFsWtpFwlFilterDestAddress
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpFwlFilterDestAddress (UINT4 u4CapwapBaseWtpProfileId,
                                 tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValFsWtpFwlFilterDestAddress)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;
    UINT4               u4DestStartAddr = SNMP_FAILURE;
    UINT4               u4DestEndAddr = AP_FWL_ZERO;
    INT1                i1Status = AP_FWL_ZERO;
    UINT1               au1DestAddr[AP_FWL_MAX_ADDR_LEN] = { AP_FWL_ZERO };

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));

    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;
    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {
        MEMCPY (WssIfWtpFwlFilter.au1DestAddr,
                pSetValFsWtpFwlFilterDestAddress->pu1_OctetList,
                STRLEN ((pSetValFsWtpFwlFilterDestAddress->pu1_OctetList) - 1));
    }
    else
    {
        return SNMP_FAILURE;
    }

    pSetValFsWtpFwlFilterDestAddress->i4_Length = STRLEN
        (pSetValFsWtpFwlFilterDestAddress->pu1_OctetList);
    WssIfWtpFwlFilter.au1DestAddr[pSetValFsWtpFwlFilterDestAddress->i4_Length]
        = AP_FWL_END_OF_STRING;

    MEMCPY (au1DestAddr, pSetValFsWtpFwlFilterDestAddress->pu1_OctetList,
            pSetValFsWtpFwlFilterDestAddress->i4_Length);
    au1DestAddr[pSetValFsWtpFwlFilterDestAddress->i4_Length] =
        AP_FWL_END_OF_STRING;

    if (AP_FWL_IP_VERSION_4 == WssIfWtpFwlFilter.u2AddrType)
    {
        i1Status =
            ApFwlParseIpAddr (au1DestAddr, &u4DestStartAddr, &u4DestEndAddr);
        if (i1Status == AP_FWL_SUCCESS)
        {
            WssIfWtpFwlFilter.DestStartAddr.v4Addr = u4DestStartAddr;
            WssIfWtpFwlFilter.DestEndAddr.v4Addr = u4DestEndAddr;
            WssIfWtpFwlFilter.DestStartAddr.u4AddrType = AP_FWL_IP_VERSION_4;
            WssIfWtpFwlFilter.DestEndAddr.u4AddrType = AP_FWL_IP_VERSION_4;
            WssIfWtpFwlFilter.i4RowStatus = AP_FWL_NOT_IN_SERVICE;

            if (OSIX_SUCCESS == WssIfWtpFwlFilterSetEntry (&WssIfWtpFwlFilter))
            {
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                i1Status = SNMP_FAILURE;
            }

        }
        else
        {
            i1Status = SNMP_FAILURE;
        }
    }
    else
    {
        i1Status = SNMP_FAILURE;
    }
    return i1Status;
}

/****************************************************************************
Function    :  nmhSetFsWtpFwlFilterProtocol
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
setValFsWtpFwlFilterProtocol
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpFwlFilterProtocol (UINT4 u4CapwapBaseWtpProfileId,
                              tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                              INT4 i4SetValFsWtpFwlFilterProtocol)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));

    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;
    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {
        WssIfWtpFwlFilter.u1Proto = (UINT1) i4SetValFsWtpFwlFilterProtocol;
        if ((WssIfWtpFwlFilter.u1Proto != FWL_TCP)
            && (WssIfWtpFwlFilter.u1Proto != FWL_UDP)
            && (WssIfWtpFwlFilter.u1Proto != AP_FWL_DEFAULT_PROTO))
        {

            WssIfWtpFwlFilter.u2SrcMaxPort = AP_FWL_DEFAULT_PORT;
            WssIfWtpFwlFilter.u2SrcMinPort = AP_FWL_DEFAULT_PORT;
            STRCPY (WssIfWtpFwlFilter.au1SrcPort, AP_FWL_NULL_STRING);

            WssIfWtpFwlFilter.u2DestMaxPort = AP_FWL_DEFAULT_PORT;
            WssIfWtpFwlFilter.u2DestMinPort = AP_FWL_DEFAULT_PORT;
            STRCPY (WssIfWtpFwlFilter.au1DestPort, AP_FWL_NULL_STRING);
        }
        WssIfWtpFwlFilter.i4RowStatus = AP_FWL_NOT_IN_SERVICE;
        if (OSIX_SUCCESS == WssIfWtpFwlFilterSetEntry (&WssIfWtpFwlFilter))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsWtpFwlFilterSrcPort
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
setValFsWtpFwlFilterSrcPort
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpFwlFilterSrcPort (UINT4 u4CapwapBaseWtpProfileId,
                             tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsWtpFwlFilterSrcPort)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;
    UINT2               u2SrcMaxPort = AP_FWL_ZERO;
    UINT2               u2SrcMinPort = AP_FWL_ZERO;
    INT1                i1Status = AP_FWL_ZERO;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));

    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;

    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {
        MEMCPY (WssIfWtpFwlFilter.au1SrcPort,
                pSetValFsWtpFwlFilterSrcPort->pu1_OctetList,
                STRLEN ((pSetValFsWtpFwlFilterSrcPort->pu1_OctetList) - 1));
    }
    else
    {
        return SNMP_FAILURE;
    }

    pSetValFsWtpFwlFilterSrcPort->i4_Length = STRLEN
        (pSetValFsWtpFwlFilterSrcPort->pu1_OctetList);
    WssIfWtpFwlFilter.au1SrcPort[pSetValFsWtpFwlFilterSrcPort->i4_Length]
        = AP_FWL_END_OF_STRING;

    i1Status = ApFwlParseMinAndMaxPort (WssIfWtpFwlFilter.au1SrcPort,
                                        &u2SrcMaxPort, &u2SrcMinPort);
    if (i1Status == SNMP_SUCCESS)
    {
        WssIfWtpFwlFilter.u2SrcMaxPort = u2SrcMaxPort;
        WssIfWtpFwlFilter.u2SrcMinPort = u2SrcMinPort;
        if (OSIX_SUCCESS == WssIfWtpFwlFilterSetEntry (&WssIfWtpFwlFilter))
        {
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            i1Status = SNMP_FAILURE;
        }
    }
    else
    {
        i1Status = SNMP_FAILURE;
    }
    return i1Status;
}

/****************************************************************************
Function    :  nmhSetFsWtpFwlFilterDestPort
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
setValFsWtpFwlFilterDestPort
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpFwlFilterDestPort (UINT4 u4CapwapBaseWtpProfileId,
                              tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValFsWtpFwlFilterDestPort)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;
    UINT2               u2DestMaxPort = 0;
    UINT2               u2DestMinPort = 0;
    INT1                i1Status = 0;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));

    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;

    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {
        MEMCPY (WssIfWtpFwlFilter.au1DestPort,
                pSetValFsWtpFwlFilterDestPort->pu1_OctetList,
                STRLEN ((pSetValFsWtpFwlFilterDestPort->pu1_OctetList) - 1));
    }
    else
    {
        return SNMP_FAILURE;
    }

    pSetValFsWtpFwlFilterDestPort->i4_Length = STRLEN
        (pSetValFsWtpFwlFilterDestPort->pu1_OctetList);
    WssIfWtpFwlFilter.au1DestPort[pSetValFsWtpFwlFilterDestPort->i4_Length]
        = AP_FWL_END_OF_STRING;

    i1Status = ApFwlParseMinAndMaxPort (WssIfWtpFwlFilter.au1DestPort,
                                        &u2DestMaxPort, &u2DestMinPort);
    if (i1Status == SNMP_SUCCESS)
    {
        WssIfWtpFwlFilter.u2DestMaxPort = u2DestMaxPort;
        WssIfWtpFwlFilter.u2DestMinPort = u2DestMinPort;
        if (OSIX_SUCCESS == WssIfWtpFwlFilterSetEntry (&WssIfWtpFwlFilter))
        {
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            i1Status = SNMP_FAILURE;
        }
    }
    else
    {
        i1Status = SNMP_FAILURE;
    }
    return i1Status;
}

/****************************************************************************
Function    :  nmhSetFsWtpFwlFilterTos
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
setValFsWtpFwlFilterTos
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpFwlFilterTos (UINT4 u4CapwapBaseWtpProfileId,
                         tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                         INT4 i4SetValFsWtpFwlFilterTos)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pFsWtpFwlFilterName);
    UNUSED_PARAM (i4SetValFsWtpFwlFilterTos);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsWtpFwlFilterAccounting
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
setValFsWtpFwlFilterAccounting
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpFwlFilterAccounting (UINT4 u4CapwapBaseWtpProfileId,
                                tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                                INT4 i4SetValFsWtpFwlFilterAccounting)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pFsWtpFwlFilterName);
    UNUSED_PARAM (i4SetValFsWtpFwlFilterAccounting);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsWtpFwlFilterHitClear
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
setValFsWtpFwlFilterHitClear
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpFwlFilterHitClear (UINT4 u4CapwapBaseWtpProfileId,
                              tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                              INT4 i4SetValFsWtpFwlFilterHitClear)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pFsWtpFwlFilterName);
    UNUSED_PARAM (i4SetValFsWtpFwlFilterHitClear);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsWtpFwlFilterAddrType
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
setValFsWtpFwlFilterAddrType
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpFwlFilterAddrType (UINT4 u4CapwapBaseWtpProfileId,
                              tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                              INT4 i4SetValFsWtpFwlFilterAddrType)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));

    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;

    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {
        WssIfWtpFwlFilter.u2AddrType = (UINT2) i4SetValFsWtpFwlFilterAddrType;
        if (OSIX_SUCCESS == WssIfWtpFwlFilterSetEntry (&WssIfWtpFwlFilter))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsWtpFwlFilterFlowId
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
setValFsWtpFwlFilterFlowId
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpFwlFilterFlowId (UINT4 u4CapwapBaseWtpProfileId,
                            tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                            UINT4 u4SetValFsWtpFwlFilterFlowId)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pFsWtpFwlFilterName);
    UNUSED_PARAM (u4SetValFsWtpFwlFilterFlowId);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsWtpFwlFilterDscp
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
setValFsWtpFwlFilterDscp
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpFwlFilterDscp (UINT4 u4CapwapBaseWtpProfileId,
                          tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                          INT4 i4SetValFsWtpFwlFilterDscp)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pFsWtpFwlFilterName);
    UNUSED_PARAM (i4SetValFsWtpFwlFilterDscp);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsWtpFwlFilterRowStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
setValFsWtpFwlFilterRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpFwlFilterRowStatus (UINT4 u4CapwapBaseWtpProfileId,
                               tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                               INT4 i4SetValFsWtpFwlFilterRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Status = 1;
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));

    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;

    u4Status = WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter);

    if ((OSIX_FAILURE != u4Status) &&
        ((WssIfWtpFwlFilter.i4RowStatus) == i4SetValFsWtpFwlFilterRowStatus))
    {
        return SNMP_SUCCESS;
    }

    /* The Filter Node can be created only when the row status is given
     * CREATE_AND_WAIT. The node will become ACTIVE when the appropriate fields 
     * are SET. The node will be deleted when the Row Status is DESTROY.
     */
    switch (i4SetValFsWtpFwlFilterRowStatus)
    {
        case CREATE_AND_WAIT:
            if (OSIX_SUCCESS == u4Status)
            {
                /*if the node is already exiting */
                i1Status = SNMP_SUCCESS;
                break;
            }
            WssIfWtpFwlFilter.i4RowStatus = NOT_READY;

            if (OSIX_FAILURE == WssIfWtpFwlFilterAddEntry (&WssIfWtpFwlFilter))
            {
                i1Status = SNMP_FAILURE;
            }
            else
            {
                i1Status = SNMP_SUCCESS;
            }
            break;
        case DESTROY:
            if (OSIX_SUCCESS != u4Status)
            {
                /*if the node is not already existing */
                break;
            }
            if (OSIX_FAILURE ==
                WssIfWtpFwlFilterDeleteEntry (&WssIfWtpFwlFilter))
            {
                i1Status = SNMP_FAILURE;
            }
            else
            {
                i1Status = SNMP_SUCCESS;
            }
            break;
        case ACTIVE:
            if (OSIX_SUCCESS != u4Status)
            {
                /*if the node is not already existing */
                break;
            }
            WssIfWtpFwlFilter.i4RowStatus = ACTIVE;
            if (OSIX_SUCCESS == WssIfWtpFwlFilterSetEntry (&WssIfWtpFwlFilter))
            {
                i1Status = SNMP_SUCCESS;
                WssifFirewallFilterTable (WssIfWtpFwlFilter.u4WtpProfileId,
                                          WssIfWtpFwlFilter.au1FilterName,
                                          WssIfWtpFwlFilter.au1SrcAddr,
                                          WssIfWtpFwlFilter.au1DestAddr,
                                          WssIfWtpFwlFilter.u1Proto,
                                          WssIfWtpFwlFilter.au1SrcPort,
                                          WssIfWtpFwlFilter.au1DestPort,
                                          WssIfWtpFwlFilter.u2AddrType,
                                          (UINT4) WssIfWtpFwlFilter.
                                          i4RowStatus);
            }
            else
            {
                i1Status = SNMP_FAILURE;
            }
            break;
        case NOT_IN_SERVICE:
            if (OSIX_SUCCESS != u4Status)
            {
                /*if the node is not already existing */
                break;
            }
            WssIfWtpFwlFilter.i4RowStatus = NOT_IN_SERVICE;
            if (OSIX_SUCCESS == WssIfWtpFwlFilterSetEntry (&WssIfWtpFwlFilter))
            {
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                i1Status = SNMP_FAILURE;
            }
            break;
        default:
            i1Status = SNMP_FAILURE;
            break;
    }
    return i1Status;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsWtpFwlFilterSrcAddress
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
testValFsWtpFwlFilterSrcAddress
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpFwlFilterSrcAddress (UINT4 *pu4ErrorCode,
                                   UINT4 u4CapwapBaseWtpProfileId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsWtpFwlFilterName,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsWtpFwlFilterSrcAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4SrcStartAddr = AP_FWL_ZERO;
    UINT4               u4SrcEndAddr = AP_FWL_ZERO;
    UINT1               au1SrcAddr[AP_FWL_MAX_ADDR_LEN] = { AP_FWL_ZERO };
    /* Added new variables to validate IP address */
    INT4                i4RetVal = AP_FWL_ZERO;
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    if ((pFsWtpFwlFilterName->pu1_OctetList == NULL) ||
        (pTestValFsWtpFwlFilterSrcAddress->pu1_OctetList == NULL))
    {
        return SNMP_FAILURE;
    }

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));

    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;

    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {

        if (WssIfWtpFwlFilter.i4RowStatus == ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        MEMCPY (au1SrcAddr, pTestValFsWtpFwlFilterSrcAddress->pu1_OctetList,
                MEM_MAX_BYTES (pTestValFsWtpFwlFilterSrcAddress->i4_Length,
                               (INT4) sizeof (au1SrcAddr)));
        au1SrcAddr[pTestValFsWtpFwlFilterSrcAddress->i4_Length] =
            AP_FWL_END_OF_STRING;

        if (WssIfWtpFwlFilter.u2AddrType == AP_FWL_IP_VERSION_4)
        {

            /* Parse the string and find Start and End Source IP address  */

            i1Status =
                ApFwlParseIpAddr (au1SrcAddr, &u4SrcStartAddr, &u4SrcEndAddr);

            if (AP_FWL_SUCCESS == i1Status)
            {
                if ((u4SrcStartAddr == AP_FWL_ZERO) &&
                    (u4SrcEndAddr == FWL_BROADCAST_ADDR))
                {
                    /* User has configured "any" option through CLI
                     * so return SUCCESS */
                    return SNMP_SUCCESS;
                }
                i4RetVal = ApFwlValidateIpAddress (u4SrcStartAddr);
                if ((i4RetVal == FWL_BCAST_ADDR) ||
                    (i4RetVal == FWL_ZERO_NETW_ADDR) ||
                    (i4RetVal == FWL_LOOPBACK_ADDR) ||
                    (i4RetVal == FWL_INVALID_ADDR) ||
                    (i4RetVal == FWL_CLASS_BCASTADDR)
                    || (i4RetVal == FWL_CLASSE_ADDR))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
                i4RetVal = ApFwlValidateIpAddress (u4SrcEndAddr);
                if ((i4RetVal == FWL_BCAST_ADDR) ||
                    (i4RetVal == FWL_ZERO_NETW_ADDR) ||
                    (i4RetVal == FWL_LOOPBACK_ADDR) ||
                    (i4RetVal == FWL_INVALID_ADDR) ||
                    (i4RetVal == FWL_CLASS_BCASTADDR)
                    || (i4RetVal == FWL_CLASSE_ADDR))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                i1Status = SNMP_FAILURE;
            }
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return i1Status;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpFwlFilterDestAddress
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
testValFsWtpFwlFilterDestAddress
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpFwlFilterDestAddress (UINT4 *pu4ErrorCode,
                                    UINT4 u4CapwapBaseWtpProfileId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsWtpFwlFilterName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValFsWtpFwlFilterDestAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4DestStartAddr = AP_FWL_ZERO;
    UINT4               u4DestEndAddr = AP_FWL_ZERO;
    UINT1               au1DestAddr[AP_FWL_MAX_ADDR_LEN] = { AP_FWL_ZERO };
    /* Added new variables to validate IP address */
    INT4                i4RetVal = AP_FWL_ZERO;
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    if ((pFsWtpFwlFilterName->pu1_OctetList == NULL) ||
        (pTestValFsWtpFwlFilterDestAddress->pu1_OctetList == NULL))
    {
        return SNMP_FAILURE;
    }

    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));

    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;

    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {

        if (WssIfWtpFwlFilter.i4RowStatus == ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        MEMCPY (au1DestAddr, pTestValFsWtpFwlFilterDestAddress->pu1_OctetList,
                MEM_MAX_BYTES (pTestValFsWtpFwlFilterDestAddress->i4_Length,
                               (INT4) sizeof (au1DestAddr)));
        au1DestAddr[pTestValFsWtpFwlFilterDestAddress->i4_Length] =
            AP_FWL_END_OF_STRING;

        if (WssIfWtpFwlFilter.u2AddrType == AP_FWL_IP_VERSION_4)
        {

            /* Parse the string and find Start and End Source IP address  */

            i1Status =
                ApFwlParseIpAddr (au1DestAddr, &u4DestStartAddr,
                                  &u4DestEndAddr);

            if (AP_FWL_SUCCESS == i1Status)
            {
                if ((u4DestStartAddr == AP_FWL_ZERO) &&
                    (u4DestEndAddr == FWL_BROADCAST_ADDR))
                {
                    /* User has configured "any" option through CLI
                     * so return SUCCESS */
                    return SNMP_SUCCESS;
                }
                i4RetVal = ApFwlValidateIpAddress (u4DestStartAddr);
                if ((i4RetVal == FWL_BCAST_ADDR) ||
                    (i4RetVal == FWL_ZERO_NETW_ADDR) ||
                    (i4RetVal == FWL_LOOPBACK_ADDR) ||
                    (i4RetVal == FWL_INVALID_ADDR) ||
                    (i4RetVal == FWL_CLASS_BCASTADDR)
                    || (i4RetVal == FWL_CLASSE_ADDR))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
                i4RetVal = ApFwlValidateIpAddress (u4DestEndAddr);
                if ((i4RetVal == FWL_BCAST_ADDR) ||
                    (i4RetVal == FWL_ZERO_NETW_ADDR) ||
                    (i4RetVal == FWL_LOOPBACK_ADDR) ||
                    (i4RetVal == FWL_INVALID_ADDR) ||
                    (i4RetVal == FWL_CLASS_BCASTADDR)
                    || (i4RetVal == FWL_CLASSE_ADDR))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                i1Status = SNMP_FAILURE;
            }
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return i1Status;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpFwlFilterProtocol
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
testValFsWtpFwlFilterProtocol
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpFwlFilterProtocol (UINT4 *pu4ErrorCode,
                                 UINT4 u4CapwapBaseWtpProfileId,
                                 tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                                 INT4 i4TestValFsWtpFwlFilterProtocol)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;
    INT1                i1Status = SNMP_FAILURE;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    if (pFsWtpFwlFilterName->pu1_OctetList == NULL)
    {
        return SNMP_FAILURE;
    }
    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));

    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;
    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {
        if (WssIfWtpFwlFilter.i4RowStatus == ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        i1Status = ApFwlValidateProtocol (i4TestValFsWtpFwlFilterProtocol);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return i1Status;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpFwlFilterSrcPort
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
testValFsWtpFwlFilterSrcPort
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpFwlFilterSrcPort (UINT4 *pu4ErrorCode,
                                UINT4 u4CapwapBaseWtpProfileId,
                                tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsWtpFwlFilterSrcPort)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               au1SrcPort[AP_FWL_MAX_PORT_LEN] = { AP_FWL_ZERO };
    UINT2               u2SrcMaxPort = AP_FWL_ZERO;
    UINT2               u2SrcMinPort = AP_FWL_ZERO;
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    if (pFsWtpFwlFilterName->pu1_OctetList == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pTestValFsWtpFwlFilterSrcPort->pu1_OctetList == NULL)
    {
        return SNMP_FAILURE;
    }
    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));

    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;

#ifdef SNMP_2_WANTED
    if (OSIX_FAILURE ==
        SNMPCheckForNVTChars (pTestValFsWtpFwlFilterSrcPort->pu1_OctetList,
                              pTestValFsWtpFwlFilterSrcPort->i4_Length))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif
    if ((pTestValFsWtpFwlFilterSrcPort->i4_Length >= AP_FWL_MAX_PORT_LEN)
        || (pTestValFsWtpFwlFilterSrcPort->i4_Length <= AP_FWL_ZERO))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    if (pTestValFsWtpFwlFilterSrcPort->i4_Length < AP_FWL_MAX_PORT_LEN)
    {
        MEMCPY (au1SrcPort, pTestValFsWtpFwlFilterSrcPort->pu1_OctetList,
                MEM_MAX_BYTES (pTestValFsWtpFwlFilterSrcPort->i4_Length,
                               (INT4) sizeof (au1SrcPort)));
        au1SrcPort[pTestValFsWtpFwlFilterSrcPort->i4_Length] =
            AP_FWL_END_OF_STRING;

        if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
        {
            if (WssIfWtpFwlFilter.i4RowStatus == ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            i1Status = ApFwlParseMinAndMaxPort (au1SrcPort, &u2SrcMaxPort,
                                                &u2SrcMinPort);
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    }
    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    return i1Status;

}

/****************************************************************************
Function    :  nmhTestv2FsWtpFwlFilterDestPort
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
testValFsWtpFwlFilterDestPort
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpFwlFilterDestPort (UINT4 *pu4ErrorCode,
                                 UINT4 u4CapwapBaseWtpProfileId,
                                 tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValFsWtpFwlFilterDestPort)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT1               au1DestPort[AP_FWL_MAX_PORT_LEN] = { AP_FWL_ZERO };
    UINT2               u2DestMaxPort = AP_FWL_ZERO;
    UINT2               u2DestMinPort = AP_FWL_ZERO;
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    if (pFsWtpFwlFilterName->pu1_OctetList == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pTestValFsWtpFwlFilterDestPort->pu1_OctetList == NULL)
    {
        return SNMP_FAILURE;
    }
    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));

    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;

#ifdef SNMP_2_WANTED
    if (OSIX_FAILURE ==
        SNMPCheckForNVTChars (pTestValFsWtpFwlFilterDestPort->pu1_OctetList,
                              pTestValFsWtpFwlFilterDestPort->i4_Length))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif
    if ((pTestValFsWtpFwlFilterDestPort->i4_Length >= AP_FWL_MAX_PORT_LEN)
        || (pTestValFsWtpFwlFilterDestPort->i4_Length <= AP_FWL_ZERO))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    if (pTestValFsWtpFwlFilterDestPort->i4_Length < AP_FWL_MAX_PORT_LEN)
    {
        MEMCPY (au1DestPort, pTestValFsWtpFwlFilterDestPort->pu1_OctetList,
                MEM_MAX_BYTES (pTestValFsWtpFwlFilterDestPort->i4_Length,
                               (INT4) sizeof (au1DestPort)));
        au1DestPort[pTestValFsWtpFwlFilterDestPort->i4_Length] = '\0';

        if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
        {
            if (WssIfWtpFwlFilter.i4RowStatus == ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            i1Status = ApFwlParseMinAndMaxPort (au1DestPort, &u2DestMaxPort,
                                                &u2DestMinPort);
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    }
    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    return i1Status;

}

/****************************************************************************
Function    :  nmhTestv2FsWtpFwlFilterTos
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
testValFsWtpFwlFilterTos
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpFwlFilterTos (UINT4 *pu4ErrorCode, UINT4 u4CapwapBaseWtpProfileId,
                            tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                            INT4 i4TestValFsWtpFwlFilterTos)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pFsWtpFwlFilterName);
    UNUSED_PARAM (i4TestValFsWtpFwlFilterTos);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpFwlFilterAccounting
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
testValFsWtpFwlFilterAccounting
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpFwlFilterAccounting (UINT4 *pu4ErrorCode,
                                   UINT4 u4CapwapBaseWtpProfileId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsWtpFwlFilterName,
                                   INT4 i4TestValFsWtpFwlFilterAccounting)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pFsWtpFwlFilterName);
    UNUSED_PARAM (i4TestValFsWtpFwlFilterAccounting);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpFwlFilterHitClear
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
testValFsWtpFwlFilterHitClear
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpFwlFilterHitClear (UINT4 *pu4ErrorCode,
                                 UINT4 u4CapwapBaseWtpProfileId,
                                 tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                                 INT4 i4TestValFsWtpFwlFilterHitClear)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pFsWtpFwlFilterName);
    UNUSED_PARAM (i4TestValFsWtpFwlFilterHitClear);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpFwlFilterAddrType
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
testValFsWtpFwlFilterAddrType
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpFwlFilterAddrType (UINT4 *pu4ErrorCode,
                                 UINT4 u4CapwapBaseWtpProfileId,
                                 tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                                 INT4 i4TestValFsWtpFwlFilterAddrType)
{
    INT1                i1Status = SNMP_FAILURE;
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilter;

    MEMSET (&WssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    if (pFsWtpFwlFilterName->pu1_OctetList == NULL)
    {
        return SNMP_FAILURE;
    }
    WssIfWtpFwlFilter.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilter.au1FilterName, pFsWtpFwlFilterName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlFilterName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlFilter.au1FilterName)));

    WssIfWtpFwlFilter.au1FilterName[pFsWtpFwlFilterName->i4_Length] =
        AP_FWL_END_OF_STRING;

    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilter))
    {
        if ((i4TestValFsWtpFwlFilterAddrType == AP_FWL_IP_VERSION_4)
            || (i4TestValFsWtpFwlFilterAddrType == AP_FWL_IP_VERSION_6))
        {
            if (WssIfWtpFwlFilter.i4RowStatus == ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            i1Status = SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    }
    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    return i1Status;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpFwlFilterFlowId
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
testValFsWtpFwlFilterFlowId
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpFwlFilterFlowId (UINT4 *pu4ErrorCode,
                               UINT4 u4CapwapBaseWtpProfileId,
                               tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                               UINT4 u4TestValFsWtpFwlFilterFlowId)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pFsWtpFwlFilterName);
    UNUSED_PARAM (u4TestValFsWtpFwlFilterFlowId);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpFwlFilterDscp
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
testValFsWtpFwlFilterDscp
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpFwlFilterDscp (UINT4 *pu4ErrorCode,
                             UINT4 u4CapwapBaseWtpProfileId,
                             tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                             INT4 i4TestValFsWtpFwlFilterDscp)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pFsWtpFwlFilterName);
    UNUSED_PARAM (i4TestValFsWtpFwlFilterDscp);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpFwlFilterRowStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName

The Object 
testValFsWtpFwlFilterRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpFwlFilterRowStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4CapwapBaseWtpProfileId,
                                  tSNMP_OCTET_STRING_TYPE * pFsWtpFwlFilterName,
                                  INT4 i4TestValFsWtpFwlFilterRowStatus)
{
    tWssIfWtpFwlFilterDB WssIfWtpFwlFilterDB;

    INT1                i1Status = SNMP_FAILURE;
    INT4                i4RowStatus = 0;
    INT1                i1RetVal = 0;

    MEMSET (&WssIfWtpFwlFilterDB, 0, sizeof (tWssIfWtpFwlFilterDB));

    i1RetVal = nmhGetCapwapBaseWtpProfileRowStatus (u4CapwapBaseWtpProfileId,
                                                    &i4RowStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    WssIfWtpFwlFilterDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlFilterDB.au1FilterName,
            pFsWtpFwlFilterName->pu1_OctetList, pFsWtpFwlFilterName->i4_Length);

    i1Status = (INT4) WssIfWtpFwlFilterGetEntry (&WssIfWtpFwlFilterDB);

    switch (i4TestValFsWtpFwlFilterRowStatus)
    {
        case CREATE_AND_WAIT:
            /* If the index doesnt exist already, then it can be created. */
            if (i1Status == OSIX_FAILURE)
            {
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1Status = SNMP_FAILURE;
            }
            break;
            /* 
             * If on the fly modification of filters is needed then the 
             * following RowStatus Values is not required.
             */
        case NOT_IN_SERVICE:
            /* The row can be made FWL_NOT_IN_SERVICE if the row exist. */
            if (i1Status != OSIX_FAILURE)
            {
                if (WssIfWtpFwlFilterDB.i4RowStatus == FWL_ACTIVE)
                {
                    i1Status = SNMP_SUCCESS;
                }
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            }
            break;
        case ACTIVE:
            /* The row can be made FWL_ACTIVE if the row exists and the
             * RowStatus is FWL_NOT_IN_SERVICE and the necessary fileds are 
             * set.
             */
            if (i1Status != OSIX_FAILURE)
            {
                if (NOT_IN_SERVICE == WssIfWtpFwlFilterDB.i4RowStatus)
                {
                    i1Status = SNMP_SUCCESS;
                }
                else
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                }
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            }
            break;
        case DESTROY:
            /* The row can be deleted if it exists. */
            if (i1Status == OSIX_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            }
            else
            {
                i1Status = SNMP_SUCCESS;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            i1Status = SNMP_FAILURE;
            break;
    }                            /* end of Switch */
    return i1Status;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsWtpFwlFilterTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlFilterName
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsWtpFwlFilterTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWtpFwlRuleTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsWtpFwlRuleTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlRuleName
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWtpFwlRuleTable (UINT4 u4CapwapBaseWtpProfileId,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsWtpFwlRuleName)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pFsWtpFwlRuleName);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsWtpFwlRuleTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlRuleName
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsWtpFwlRuleTable (UINT4 *pu4CapwapBaseWtpProfileId,
                                   tSNMP_OCTET_STRING_TYPE * pFsWtpFwlRuleName)
{

    INT4                i4RetVal = 0;

    i4RetVal = WssIfWtpFwlRuleGetFirstEntry (pu4CapwapBaseWtpProfileId,
                                             pFsWtpFwlRuleName->pu1_OctetList);

    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pFsWtpFwlRuleName->i4_Length = STRLEN (pFsWtpFwlRuleName->pu1_OctetList);

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsWtpFwlRuleTable
Input       :  The Indices
CapwapBaseWtpProfileId
nextCapwapBaseWtpProfileId
FsWtpFwlRuleName
nextFsWtpFwlRuleName
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsWtpFwlRuleTable (UINT4 u4CapwapBaseWtpProfileId,
                                  UINT4 *pu4NextCapwapBaseWtpProfileId,
                                  tSNMP_OCTET_STRING_TYPE * pFsWtpFwlRuleName,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pNextFsWtpFwlRuleName)
{

    INT4                i4RetVal = 0;

    i4RetVal =
        WssIfWtpFwlRuleGetNextEntry (u4CapwapBaseWtpProfileId,
                                     pu4NextCapwapBaseWtpProfileId,
                                     pFsWtpFwlRuleName->pu1_OctetList,
                                     pNextFsWtpFwlRuleName->pu1_OctetList);

    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pNextFsWtpFwlRuleName->i4_Length =
        STRLEN (pNextFsWtpFwlRuleName->pu1_OctetList);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsWtpFwlRuleFilterSet
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlRuleName

The Object 
retValFsWtpFwlRuleFilterSet
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpFwlRuleFilterSet (UINT4 u4CapwapBaseWtpProfileId,
                             tSNMP_OCTET_STRING_TYPE * pFsWtpFwlRuleName,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsWtpFwlRuleFilterSet)
{

    tWssIfWtpFwlRuleDB  WssIfWtpFwlRule;

    MEMSET (&WssIfWtpFwlRule, 0, sizeof (tWssIfWtpFwlRuleDB));

    WssIfWtpFwlRule.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlRule.au1RuleName, pFsWtpFwlRuleName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlRuleName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlRule.au1RuleName)));

    WssIfWtpFwlRule.au1RuleName[pFsWtpFwlRuleName->i4_Length] =
        AP_FWL_END_OF_STRING;

    if (OSIX_SUCCESS == WssIfWtpFwlRuleGetEntry (&WssIfWtpFwlRule))
    {
        /*  get the filter set as a list of names with '&' or ',' operation. */
        pRetValFsWtpFwlRuleFilterSet->i4_Length = (INT4)
            STRLEN (WssIfWtpFwlRule.au1FilterSet);
        MEMCPY (pRetValFsWtpFwlRuleFilterSet->pu1_OctetList,
                WssIfWtpFwlRule.au1FilterSet,
                pRetValFsWtpFwlRuleFilterSet->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsWtpFwlRuleRowStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlRuleName

The Object 
retValFsWtpFwlRuleRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpFwlRuleRowStatus (UINT4 u4CapwapBaseWtpProfileId,
                             tSNMP_OCTET_STRING_TYPE * pFsWtpFwlRuleName,
                             INT4 *pi4RetValFsWtpFwlRuleRowStatus)
{

    tWssIfWtpFwlRuleDB  WssIfWtpFwlRule;

    MEMSET (&WssIfWtpFwlRule, 0, sizeof (tWssIfWtpFwlRuleDB));

    WssIfWtpFwlRule.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlRule.au1RuleName, pFsWtpFwlRuleName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlRuleName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlRule.au1RuleName)));

    WssIfWtpFwlRule.au1RuleName[pFsWtpFwlRuleName->i4_Length] =
        AP_FWL_END_OF_STRING;

    if (OSIX_SUCCESS == WssIfWtpFwlRuleGetEntry (&WssIfWtpFwlRule))
    {
        /*  get the filter set as a list of names with '&' or ',' operation. */
        *pi4RetValFsWtpFwlRuleRowStatus = WssIfWtpFwlRule.i4RowStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsWtpFwlRuleFilterSet
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlRuleName

The Object 
setValFsWtpFwlRuleFilterSet
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpFwlRuleFilterSet (UINT4 u4CapwapBaseWtpProfileId,
                             tSNMP_OCTET_STRING_TYPE * pFsWtpFwlRuleName,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsWtpFwlRuleFilterSet)
{
    INT1                i1Status = SNMP_FAILURE;
    tWssIfWtpFwlRuleDB  WssIfWtpFwlRule;
    UINT1               au1FilterSet[AP_FWL_MAX_FILTER_SET_LEN] =
        { AP_FWL_ZERO };

    MEMSET (&WssIfWtpFwlRule, 0, sizeof (tWssIfWtpFwlRuleDB));

    WssIfWtpFwlRule.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlRule.au1RuleName, pFsWtpFwlRuleName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlRuleName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlRule.au1RuleName)));

    WssIfWtpFwlRule.au1RuleName[pFsWtpFwlRuleName->i4_Length] =
        AP_FWL_END_OF_STRING;

    if (OSIX_SUCCESS == WssIfWtpFwlRuleGetEntry (&WssIfWtpFwlRule))
    {
        MEMCPY (au1FilterSet, pSetValFsWtpFwlRuleFilterSet->pu1_OctetList,
                MEM_MAX_BYTES (pSetValFsWtpFwlRuleFilterSet->i4_Length,
                               (INT4) sizeof (au1FilterSet)));
        au1FilterSet[pSetValFsWtpFwlRuleFilterSet->i4_Length] =
            AP_FWL_END_OF_STRING;
        MEMCPY (WssIfWtpFwlRule.au1FilterSet,
                pSetValFsWtpFwlRuleFilterSet->pu1_OctetList,
                MEM_MAX_BYTES (pSetValFsWtpFwlRuleFilterSet->i4_Length,
                               (INT4) sizeof (WssIfWtpFwlRule.au1FilterSet)));
        WssIfWtpFwlRule.au1FilterSet[pSetValFsWtpFwlRuleFilterSet->i4_Length]
            = AP_FWL_END_OF_STRING;
    }
    else
    {
        i1Status = SNMP_FAILURE;
        UNUSED_PARAM (i1Status);
        return i1Status;
    }

    i1Status = ApFwlParseFilterSet (au1FilterSet, &WssIfWtpFwlRule,
                                    u4CapwapBaseWtpProfileId);
    if (i1Status == SNMP_FAILURE)
    {
        i1Status = SNMP_FAILURE;
    }
    else
    {
        WssIfWtpFwlRule.i4RowStatus = ACTIVE;
        if (OSIX_SUCCESS == WssIfWtpFwlRuleSetEntry (&WssIfWtpFwlRule))
        {
            i1Status = SNMP_SUCCESS;
        }
    }
    UNUSED_PARAM (i1Status);
    return i1Status;
}

/****************************************************************************
Function    :  nmhSetFsWtpFwlRuleRowStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlRuleName

The Object 
setValFsWtpFwlRuleRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpFwlRuleRowStatus (UINT4 u4CapwapBaseWtpProfileId,
                             tSNMP_OCTET_STRING_TYPE * pFsWtpFwlRuleName,
                             INT4 i4SetValFsWtpFwlRuleRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Status = 1;
    tWssIfWtpFwlRuleDB  WssIfWtpFwlRule;

    MEMSET (&WssIfWtpFwlRule, 0, sizeof (tWssIfWtpFwlRuleDB));

    WssIfWtpFwlRule.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlRule.au1RuleName, pFsWtpFwlRuleName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlRuleName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlRule.au1RuleName)));

    WssIfWtpFwlRule.au1RuleName[pFsWtpFwlRuleName->i4_Length] =
        AP_FWL_END_OF_STRING;

    u4Status = WssIfWtpFwlRuleGetEntry (&WssIfWtpFwlRule);

    if ((OSIX_FAILURE != u4Status) &&
        ((WssIfWtpFwlRule.i4RowStatus) == i4SetValFsWtpFwlRuleRowStatus))
    {
        return SNMP_SUCCESS;
    }

    /* The Rule Node can be created only when the row status is given
     * CREATE_AND_WAIT. The node will become ACTIVE when the appropriate fields 
     * are SET. The node will be deleted when the Row Status is DESTROY.
     */
    switch (i4SetValFsWtpFwlRuleRowStatus)
    {
        case CREATE_AND_WAIT:
            if (OSIX_SUCCESS == u4Status)
            {
                /*if the node is already exiting */
                i1Status = SNMP_SUCCESS;
                break;
            }
            WssIfWtpFwlRule.i4RowStatus = NOT_READY;

            if (OSIX_FAILURE == WssIfWtpFwlRuleAddEntry (&WssIfWtpFwlRule))
            {
                i1Status = SNMP_FAILURE;
            }
            else
            {
                i1Status = SNMP_SUCCESS;
            }
            break;
        case CREATE_AND_GO:
            if (OSIX_SUCCESS == u4Status)
            {
                /*if the node is already exiting */
                i1Status = SNMP_SUCCESS;
                break;
            }
            WssIfWtpFwlRule.i4RowStatus = NOT_READY;

            if (OSIX_FAILURE == WssIfWtpFwlRuleAddEntry (&WssIfWtpFwlRule))
            {
                i1Status = SNMP_FAILURE;
            }
            else
            {
                i1Status = SNMP_SUCCESS;
            }
            break;
        case DESTROY:
            if (OSIX_SUCCESS != u4Status)
            {
                /*if the node is not already existing */
                break;
            }
            if (OSIX_FAILURE == WssIfWtpFwlRuleDeleteEntry (&WssIfWtpFwlRule))
            {
                i1Status = SNMP_FAILURE;
            }
            else
            {
                i1Status = SNMP_SUCCESS;
            }
            break;
        case ACTIVE:
            if (OSIX_SUCCESS != u4Status)
            {
                /*if the node is not already existing */
                break;
            }
            WssIfWtpFwlRule.i4RowStatus = ACTIVE;
            if (OSIX_SUCCESS == WssIfWtpFwlRuleSetEntry (&WssIfWtpFwlRule))
            {
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                i1Status = SNMP_FAILURE;
            }
            break;
        case NOT_IN_SERVICE:
            if (OSIX_SUCCESS != u4Status)
            {
                /*if the node is not already existing */
                break;
            }
            WssIfWtpFwlRule.i4RowStatus = NOT_IN_SERVICE;
            if (OSIX_SUCCESS == WssIfWtpFwlRuleSetEntry (&WssIfWtpFwlRule))
            {
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                i1Status = SNMP_FAILURE;
            }
            break;
        default:
            i1Status = SNMP_FAILURE;
            break;
    }
    return i1Status;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsWtpFwlRuleFilterSet
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlRuleName

The Object 
testValFsWtpFwlRuleFilterSet
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpFwlRuleFilterSet (UINT4 *pu4ErrorCode,
                                UINT4 u4CapwapBaseWtpProfileId,
                                tSNMP_OCTET_STRING_TYPE * pFsWtpFwlRuleName,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsWtpFwlRuleFilterSet)
{

    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Status = 1;
    tWssIfWtpFwlRuleDB  WssIfWtpFwlRule;

    MEMSET (&WssIfWtpFwlRule, 0, sizeof (tWssIfWtpFwlRuleDB));

    WssIfWtpFwlRule.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlRule.au1RuleName, pFsWtpFwlRuleName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlRuleName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlRule.au1RuleName)));

    WssIfWtpFwlRule.au1RuleName[pFsWtpFwlRuleName->i4_Length] =
        AP_FWL_END_OF_STRING;

#ifdef SNMP_2_WANTED
    if (OSIX_FAILURE ==
        SNMPCheckForNVTChars (pTestValFsWtpFwlRuleFilterSet->pu1_OctetList,
                              pTestValFsWtpFwlRuleFilterSet->i4_Length))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif
    if ((pTestValFsWtpFwlRuleFilterSet->i4_Length >= AP_FWL_MAX_FILTER_SET_LEN)
        || (pTestValFsWtpFwlRuleFilterSet->i4_Length <= AP_FWL_ZERO))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /* Validate the Filter set based on Row status and the length of FilterSet.
     * If the RowStatus is FWL_NOT_IN_SERVICE and the Filter Set Length is less
     * than FWL_MAX_FILTER_NAME_LEN and the FilterSet is not already set then 
     * return SUCCESS, else FAILURE.
     */
    u4Status = WssIfWtpFwlRuleGetEntry (&WssIfWtpFwlRule);
    if (OSIX_SUCCESS == u4Status)
    {
        if (WssIfWtpFwlRule.i4RowStatus == ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        i1Status = SNMP_FAILURE;
    }
    return i1Status;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpFwlRuleRowStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlRuleName

The Object 
testValFsWtpFwlRuleRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpFwlRuleRowStatus (UINT4 *pu4ErrorCode,
                                UINT4 u4CapwapBaseWtpProfileId,
                                tSNMP_OCTET_STRING_TYPE * pFsWtpFwlRuleName,
                                INT4 i4TestValFsWtpFwlRuleRowStatus)
{
    tWssIfWtpFwlRuleDB  WssIfWtpFwlRuleDB;
    INT4                i4RowStatus = 0;
    INT1                i1Status = SNMP_FAILURE;
    INT1                i1RetVal = 0;

    MEMSET (&WssIfWtpFwlRuleDB, 0, sizeof (tWssIfWtpFwlRuleDB));

    i1RetVal = nmhGetCapwapBaseWtpProfileRowStatus (u4CapwapBaseWtpProfileId,
                                                    &i4RowStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    WssIfWtpFwlRuleDB.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (WssIfWtpFwlRuleDB.au1RuleName, pFsWtpFwlRuleName->pu1_OctetList,
            pFsWtpFwlRuleName->i4_Length);

    i1Status = (INT4) WssIfWtpFwlRuleGetEntry (&WssIfWtpFwlRuleDB);

    switch (i4TestValFsWtpFwlRuleRowStatus)
    {
        case CREATE_AND_WAIT:
            /* If the index doesnt exist already, then it can be created. */
            if (i1Status == OSIX_FAILURE)
            {
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1Status = SNMP_FAILURE;
            }
            break;
            /* 
             * If on the fly modification of filters is needed then the 
             * following RowStatus Values is not required.
             */
        case CREATE_AND_GO:
            /* If the index doesnt exist already, then it can be created. */
            if (i1Status == OSIX_FAILURE)
            {
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1Status = SNMP_FAILURE;
            }
            break;
            /* 
             * If on the fly modification of filters is needed then the 
             * following RowStatus Values is not required.
             */
        case NOT_IN_SERVICE:
            /* The row can be made FWL_NOT_IN_SERVICE if the row exist. */
            if (i1Status != OSIX_FAILURE)
            {
                if (WssIfWtpFwlRuleDB.i4RowStatus == FWL_ACTIVE)
                {
                    i1Status = SNMP_SUCCESS;
                }
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            }
            break;
        case ACTIVE:
            /* The row can be made FWL_ACTIVE if the row exists and the
             * RowStatus is FWL_NOT_IN_SERVICE and the necessary fileds are 
             * set.
             */
            if (i1Status != OSIX_FAILURE)
            {
                if (NOT_IN_SERVICE == WssIfWtpFwlRuleDB.i4RowStatus)
                {
                    i1Status = SNMP_SUCCESS;
                }
                else
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                }
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            }
            break;
        case DESTROY:
            /* The row can be deleted if it exists. */
            if (i1Status == OSIX_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            }
            else
            {
                i1Status = SNMP_SUCCESS;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            i1Status = SNMP_FAILURE;
            break;
    }                            /* end of Switch */
    return i1Status;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsWtpFwlRuleTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlRuleName
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsWtpFwlRuleTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWtpFwlAclTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsWtpFwlAclTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlAclIfIndex
FsWtpFwlAclName
FsWtpFwlAclDirection
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWtpFwlAclTable (UINT4 u4CapwapBaseWtpProfileId,
                                          INT4 i4FsWtpFwlAclIfIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsWtpFwlAclName,
                                          INT4 i4FsWtpFwlAclDirection)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpFwlAclIfIndex);
    UNUSED_PARAM (pFsWtpFwlAclName);
    UNUSED_PARAM (i4FsWtpFwlAclDirection);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsWtpFwlAclTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlAclIfIndex
FsWtpFwlAclName
FsWtpFwlAclDirection
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsWtpFwlAclTable (UINT4 *pu4CapwapBaseWtpProfileId,
                                  INT4 *pi4FsWtpFwlAclIfIndex,
                                  tSNMP_OCTET_STRING_TYPE * pFsWtpFwlAclName,
                                  INT4 *pi4FsWtpFwlAclDirection)
{
    INT4                i4RetVal = 0;

    i4RetVal = WssIfWtpFwlAclGetFirstEntry (pu4CapwapBaseWtpProfileId,
                                            pFsWtpFwlAclName->pu1_OctetList,
                                            pi4FsWtpFwlAclIfIndex,
                                            (UINT1 *) pi4FsWtpFwlAclDirection);

    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pFsWtpFwlAclName->i4_Length = STRLEN (pFsWtpFwlAclName->pu1_OctetList);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsWtpFwlAclTable
Input       :  The Indices
CapwapBaseWtpProfileId
nextCapwapBaseWtpProfileId
FsWtpFwlAclIfIndex
nextFsWtpFwlAclIfIndex
FsWtpFwlAclName
nextFsWtpFwlAclName
FsWtpFwlAclDirection
nextFsWtpFwlAclDirection
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsWtpFwlAclTable (UINT4 u4CapwapBaseWtpProfileId,
                                 UINT4 *pu4NextCapwapBaseWtpProfileId,
                                 INT4 i4FsWtpFwlAclIfIndex,
                                 INT4 *pi4NextFsWtpFwlAclIfIndex,
                                 tSNMP_OCTET_STRING_TYPE * pFsWtpFwlAclName,
                                 tSNMP_OCTET_STRING_TYPE * pNextFsWtpFwlAclName,
                                 INT4 i4FsWtpFwlAclDirection,
                                 INT4 *pi4NextFsWtpFwlAclDirection)
{
    INT4                i4RetVal = 0;

    i4RetVal = WssIfWtpFwlAclGetNextEntry (u4CapwapBaseWtpProfileId,
                                           pu4NextCapwapBaseWtpProfileId,
                                           pFsWtpFwlAclName->pu1_OctetList,
                                           pNextFsWtpFwlAclName->pu1_OctetList,
                                           i4FsWtpFwlAclIfIndex,
                                           pi4NextFsWtpFwlAclIfIndex,
                                           i4FsWtpFwlAclDirection,
                                           (UINT1 *)
                                           pi4NextFsWtpFwlAclDirection);

    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pNextFsWtpFwlAclName->i4_Length =
        STRLEN (pNextFsWtpFwlAclName->pu1_OctetList);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsWtpFwlAclAction
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlAclIfIndex
FsWtpFwlAclName
FsWtpFwlAclDirection

The Object 
retValFsWtpFwlAclAction
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpFwlAclAction (UINT4 u4CapwapBaseWtpProfileId,
                         INT4 i4FsWtpFwlAclIfIndex,
                         tSNMP_OCTET_STRING_TYPE * pFsWtpFwlAclName,
                         INT4 i4FsWtpFwlAclDirection,
                         INT4 *pi4RetValFsWtpFwlAclAction)
{
    INT1                i1Status = SNMP_FAILURE;
    tWssIfWtpFwlAclDB   WssIfWtpFwlAcl;

    MEMSET (&WssIfWtpFwlAcl, 0, sizeof (tWssIfWtpFwlAclDB));

    WssIfWtpFwlAcl.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpFwlAcl.i4AclIfIndex = i4FsWtpFwlAclIfIndex;
    MEMCPY (WssIfWtpFwlAcl.au1AclName, pFsWtpFwlAclName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlAclName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlAcl.au1AclName)));
    WssIfWtpFwlAcl.au1AclName[pFsWtpFwlAclName->i4_Length] =
        AP_FWL_END_OF_STRING;
    WssIfWtpFwlAcl.u1AclDirection = i4FsWtpFwlAclDirection;

    /* search the interface index */
    if (OSIX_SUCCESS != WssIfWtpFwlAclGetEntry (&WssIfWtpFwlAcl))
    {
        return i1Status;
    }
    else
    {
        *pi4RetValFsWtpFwlAclAction = (INT4) WssIfWtpFwlAcl.u1Action;
        i1Status = SNMP_SUCCESS;
    }
    return i1Status;
}

/****************************************************************************
Function    :  nmhGetFsWtpFwlAclSequenceNumber
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlAclIfIndex
FsWtpFwlAclName
FsWtpFwlAclDirection

The Object 
retValFsWtpFwlAclSequenceNumber
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpFwlAclSequenceNumber (UINT4 u4CapwapBaseWtpProfileId,
                                 INT4 i4FsWtpFwlAclIfIndex,
                                 tSNMP_OCTET_STRING_TYPE * pFsWtpFwlAclName,
                                 INT4 i4FsWtpFwlAclDirection,
                                 INT4 *pi4RetValFsWtpFwlAclSequenceNumber)
{

    INT1                i1Status = SNMP_FAILURE;
    tWssIfWtpFwlAclDB   WssIfWtpFwlAcl;

    MEMSET (&WssIfWtpFwlAcl, 0, sizeof (tWssIfWtpFwlAclDB));

    WssIfWtpFwlAcl.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpFwlAcl.i4AclIfIndex = i4FsWtpFwlAclIfIndex;
    MEMCPY (WssIfWtpFwlAcl.au1AclName, pFsWtpFwlAclName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlAclName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlAcl.au1AclName)));
    WssIfWtpFwlAcl.au1AclName[pFsWtpFwlAclName->i4_Length] =
        AP_FWL_END_OF_STRING;
    WssIfWtpFwlAcl.u1AclDirection = i4FsWtpFwlAclDirection;

    /* search the interface index */
    if (OSIX_SUCCESS != WssIfWtpFwlAclGetEntry (&WssIfWtpFwlAcl))
    {
        return i1Status;
    }
    else
    {
        *pi4RetValFsWtpFwlAclSequenceNumber = (INT4) WssIfWtpFwlAcl.u2SeqNum;
        i1Status = SNMP_SUCCESS;
    }
    return i1Status;
}

/****************************************************************************
Function    :  nmhGetFsWtpFwlAclLogTrigger
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlAclIfIndex
FsWtpFwlAclName
FsWtpFwlAclDirection

The Object 
retValFsWtpFwlAclLogTrigger
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpFwlAclLogTrigger (UINT4 u4CapwapBaseWtpProfileId,
                             INT4 i4FsWtpFwlAclIfIndex,
                             tSNMP_OCTET_STRING_TYPE * pFsWtpFwlAclName,
                             INT4 i4FsWtpFwlAclDirection,
                             INT4 *pi4RetValFsWtpFwlAclLogTrigger)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpFwlAclIfIndex);
    UNUSED_PARAM (pFsWtpFwlAclName);
    UNUSED_PARAM (i4FsWtpFwlAclDirection);
    UNUSED_PARAM (pi4RetValFsWtpFwlAclLogTrigger);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsWtpFwlAclFragAction
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlAclIfIndex
FsWtpFwlAclName
FsWtpFwlAclDirection

The Object 
retValFsWtpFwlAclFragAction
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpFwlAclFragAction (UINT4 u4CapwapBaseWtpProfileId,
                             INT4 i4FsWtpFwlAclIfIndex,
                             tSNMP_OCTET_STRING_TYPE * pFsWtpFwlAclName,
                             INT4 i4FsWtpFwlAclDirection,
                             INT4 *pi4RetValFsWtpFwlAclFragAction)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpFwlAclIfIndex);
    UNUSED_PARAM (pFsWtpFwlAclName);
    UNUSED_PARAM (i4FsWtpFwlAclDirection);
    UNUSED_PARAM (pi4RetValFsWtpFwlAclFragAction);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsWtpFwlAclRowStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlAclIfIndex
FsWtpFwlAclName
FsWtpFwlAclDirection

The Object 
retValFsWtpFwlAclRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpFwlAclRowStatus (UINT4 u4CapwapBaseWtpProfileId,
                            INT4 i4FsWtpFwlAclIfIndex,
                            tSNMP_OCTET_STRING_TYPE * pFsWtpFwlAclName,
                            INT4 i4FsWtpFwlAclDirection,
                            INT4 *pi4RetValFsWtpFwlAclRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tWssIfWtpFwlAclDB   WssIfWtpFwlAcl;

    MEMSET (&WssIfWtpFwlAcl, 0, sizeof (tWssIfWtpFwlAclDB));

    WssIfWtpFwlAcl.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpFwlAcl.i4AclIfIndex = i4FsWtpFwlAclIfIndex;
    MEMCPY (WssIfWtpFwlAcl.au1AclName, pFsWtpFwlAclName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlAclName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlAcl.au1AclName)));
    WssIfWtpFwlAcl.au1AclName[pFsWtpFwlAclName->i4_Length] =
        AP_FWL_END_OF_STRING;
    WssIfWtpFwlAcl.u1AclDirection = i4FsWtpFwlAclDirection;

    /* search the interface index */
    if (OSIX_SUCCESS != WssIfWtpFwlAclGetEntry (&WssIfWtpFwlAcl))
    {
        return i1Status;
    }
    else
    {
        *pi4RetValFsWtpFwlAclRowStatus = (INT4) WssIfWtpFwlAcl.i4RowStatus;
        i1Status = SNMP_SUCCESS;
    }
    return i1Status;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsWtpFwlAclAction
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlAclIfIndex
FsWtpFwlAclName
FsWtpFwlAclDirection

The Object 
setValFsWtpFwlAclAction
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpFwlAclAction (UINT4 u4CapwapBaseWtpProfileId,
                         INT4 i4FsWtpFwlAclIfIndex,
                         tSNMP_OCTET_STRING_TYPE * pFsWtpFwlAclName,
                         INT4 i4FsWtpFwlAclDirection,
                         INT4 i4SetValFsWtpFwlAclAction)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4IfaceNum = (UINT4) i4FsWtpFwlAclIfIndex;
    tWssIfWtpFwlAclDB   WssIfWtpFwlAcl;

    MEMSET (&WssIfWtpFwlAcl, 0, sizeof (tWssIfWtpFwlAclDB));

    WssIfWtpFwlAcl.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpFwlAcl.i4AclIfIndex = i4FsWtpFwlAclIfIndex;
    WssIfWtpFwlAcl.u1AclDirection = i4FsWtpFwlAclDirection;
    MEMCPY (WssIfWtpFwlAcl.au1AclName, pFsWtpFwlAclName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlAclName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlAcl.au1AclName)));
    WssIfWtpFwlAcl.au1AclName[pFsWtpFwlAclName->i4_Length] =
        AP_FWL_END_OF_STRING;

    if (u4IfaceNum <= AP_FWL_MAX_NUM_OF_IF)
    {
        if (OSIX_SUCCESS == WssIfWtpFwlAclGetEntry (&WssIfWtpFwlAcl))
        {
            WssIfWtpFwlAcl.u1Action = i4SetValFsWtpFwlAclAction;
            if (OSIX_SUCCESS == WssIfWtpFwlAclSetEntry (&WssIfWtpFwlAcl))
            {
                i1Status = SNMP_SUCCESS;
            }
        }
    }
    return i1Status;
}

/****************************************************************************
Function    :  nmhSetFsWtpFwlAclSequenceNumber
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlAclIfIndex
FsWtpFwlAclName
FsWtpFwlAclDirection

The Object 
setValFsWtpFwlAclSequenceNumber
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpFwlAclSequenceNumber (UINT4 u4CapwapBaseWtpProfileId,
                                 INT4 i4FsWtpFwlAclIfIndex,
                                 tSNMP_OCTET_STRING_TYPE * pFsWtpFwlAclName,
                                 INT4 i4FsWtpFwlAclDirection,
                                 INT4 i4SetValFsWtpFwlAclSequenceNumber)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4IfaceNum = (UINT4) i4FsWtpFwlAclIfIndex;
    tWssIfWtpFwlAclDB   WssIfWtpFwlAcl;

    MEMSET (&WssIfWtpFwlAcl, 0, sizeof (tWssIfWtpFwlAclDB));

    WssIfWtpFwlAcl.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpFwlAcl.i4AclIfIndex = i4FsWtpFwlAclIfIndex;
    WssIfWtpFwlAcl.u1AclDirection = i4FsWtpFwlAclDirection;
    MEMCPY (WssIfWtpFwlAcl.au1AclName, pFsWtpFwlAclName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlAclName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlAcl.au1AclName)));
    WssIfWtpFwlAcl.au1AclName[pFsWtpFwlAclName->i4_Length] =
        AP_FWL_END_OF_STRING;

    if (u4IfaceNum <= AP_FWL_MAX_NUM_OF_IF)
    {
        if (OSIX_SUCCESS == WssIfWtpFwlAclGetEntry (&WssIfWtpFwlAcl))
        {
            WssIfWtpFwlAcl.u2SeqNum = (UINT2) i4SetValFsWtpFwlAclSequenceNumber;
            if (OSIX_SUCCESS == WssIfWtpFwlAclSetEntry (&WssIfWtpFwlAcl))
            {
                i1Status = SNMP_SUCCESS;
            }
        }
    }
    return i1Status;
}

/****************************************************************************
Function    :  nmhSetFsWtpFwlAclLogTrigger
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlAclIfIndex
FsWtpFwlAclName
FsWtpFwlAclDirection

The Object 
setValFsWtpFwlAclLogTrigger
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpFwlAclLogTrigger (UINT4 u4CapwapBaseWtpProfileId,
                             INT4 i4FsWtpFwlAclIfIndex,
                             tSNMP_OCTET_STRING_TYPE * pFsWtpFwlAclName,
                             INT4 i4FsWtpFwlAclDirection,
                             INT4 i4SetValFsWtpFwlAclLogTrigger)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpFwlAclIfIndex);
    UNUSED_PARAM (pFsWtpFwlAclName);
    UNUSED_PARAM (i4FsWtpFwlAclDirection);
    UNUSED_PARAM (i4SetValFsWtpFwlAclLogTrigger);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsWtpFwlAclFragAction
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlAclIfIndex
FsWtpFwlAclName
FsWtpFwlAclDirection

The Object 
setValFsWtpFwlAclFragAction
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpFwlAclFragAction (UINT4 u4CapwapBaseWtpProfileId,
                             INT4 i4FsWtpFwlAclIfIndex,
                             tSNMP_OCTET_STRING_TYPE * pFsWtpFwlAclName,
                             INT4 i4FsWtpFwlAclDirection,
                             INT4 i4SetValFsWtpFwlAclFragAction)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpFwlAclIfIndex);
    UNUSED_PARAM (pFsWtpFwlAclName);
    UNUSED_PARAM (i4FsWtpFwlAclDirection);
    UNUSED_PARAM (i4SetValFsWtpFwlAclFragAction);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsWtpFwlAclRowStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlAclIfIndex
FsWtpFwlAclName
FsWtpFwlAclDirection

The Object 
setValFsWtpFwlAclRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpFwlAclRowStatus (UINT4 u4CapwapBaseWtpProfileId,
                            INT4 i4FsWtpFwlAclIfIndex,
                            tSNMP_OCTET_STRING_TYPE * pFsWtpFwlAclName,
                            INT4 i4FsWtpFwlAclDirection,
                            INT4 i4SetValFsWtpFwlAclRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4Status = 1;
    tWssIfWtpFwlAclDB   WssIfWtpFwlAcl;

    MEMSET (&WssIfWtpFwlAcl, 0, sizeof (tWssIfWtpFwlAclDB));

    WssIfWtpFwlAcl.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpFwlAcl.i4AclIfIndex = i4FsWtpFwlAclIfIndex;
    MEMCPY (WssIfWtpFwlAcl.au1AclName, pFsWtpFwlAclName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlAclName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlAcl.au1AclName)));
    WssIfWtpFwlAcl.au1AclName[pFsWtpFwlAclName->i4_Length] =
        AP_FWL_END_OF_STRING;
    WssIfWtpFwlAcl.u1AclDirection = i4FsWtpFwlAclDirection;

    /* The Acl Node can be created only when the row status is given 
     * CREATE_AND_WAIT. The node will become ACTIVE when the appropriate fields
     * are SET.
     */
    i4Status = WssIfWtpFwlAclGetEntry (&WssIfWtpFwlAcl);
    switch (i4SetValFsWtpFwlAclRowStatus)
    {
        case CREATE_AND_WAIT:
            if (i4Status == OSIX_FAILURE)
            {
                WssIfWtpFwlAcl.i4RowStatus = NOT_READY;
                if (OSIX_SUCCESS == WssIfWtpFwlAclAddEntry (&WssIfWtpFwlAcl))
                {
                    i1Status = SNMP_SUCCESS;
                }
            }
            break;
        case DESTROY:
            if (i4Status == OSIX_SUCCESS)
            {
                WssIfWtpFwlAcl.i4RowStatus = DESTROY;
                if (OSIX_SUCCESS == WssIfWtpFwlAclDeleteEntry (&WssIfWtpFwlAcl))
                {
                    i1Status = SNMP_SUCCESS;
                }
            }
            break;
        case ACTIVE:
            if (i4Status == OSIX_SUCCESS)
            {
                if ((WssIfWtpFwlAcl.u1Action == 1) ||
                    (WssIfWtpFwlAcl.u1Action == 2))
                {
                    WssIfWtpFwlAcl.i4RowStatus = ACTIVE;
                    if (OSIX_SUCCESS ==
                        WssIfWtpFwlAclSetEntry (&WssIfWtpFwlAcl))
                    {
                        i1Status = SNMP_SUCCESS;
                        WssifFirewallAclTable (WssIfWtpFwlAcl.u4WtpProfileId,
                                               WssIfWtpFwlAcl.au1AclName,
                                               WssIfWtpFwlAcl.i4AclIfIndex,
                                               WssIfWtpFwlAcl.u1AclDirection,
                                               WssIfWtpFwlAcl.u1Action,
                                               WssIfWtpFwlAcl.u2SeqNum,
                                               WssIfWtpFwlAcl.i4RowStatus);
                    }
                }
            }
            break;
        case NOT_IN_SERVICE:
            if (i4Status == OSIX_SUCCESS)
            {
                WssIfWtpFwlAcl.i4RowStatus = NOT_IN_SERVICE;
                if (OSIX_SUCCESS == WssIfWtpFwlAclSetEntry (&WssIfWtpFwlAcl))
                {
                    i1Status = SNMP_SUCCESS;
                }
            }
            break;
        default:
            break;
    }

    return i1Status;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsWtpFwlAclAction
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlAclIfIndex
FsWtpFwlAclName
FsWtpFwlAclDirection

The Object 
testValFsWtpFwlAclAction
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpFwlAclAction (UINT4 *pu4ErrorCode, UINT4 u4CapwapBaseWtpProfileId,
                            INT4 i4FsWtpFwlAclIfIndex,
                            tSNMP_OCTET_STRING_TYPE * pFsWtpFwlAclName,
                            INT4 i4FsWtpFwlAclDirection,
                            INT4 i4TestValFsWtpFwlAclAction)
{
    INT1                i1Status = SNMP_SUCCESS;
    tWssIfWtpFwlAclDB   WssIfWtpFwlAcl;

    MEMSET (&WssIfWtpFwlAcl, 0, sizeof (tWssIfWtpFwlAclDB));

    WssIfWtpFwlAcl.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpFwlAcl.i4AclIfIndex = i4FsWtpFwlAclIfIndex;
    WssIfWtpFwlAcl.u1AclDirection = i4FsWtpFwlAclDirection;
    MEMCPY (WssIfWtpFwlAcl.au1AclName, pFsWtpFwlAclName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlAclName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlAcl.au1AclName)));
    WssIfWtpFwlAcl.au1AclName[pFsWtpFwlAclName->i4_Length] =
        AP_FWL_END_OF_STRING;
    if (i4FsWtpFwlAclIfIndex > AP_FWL_MAX_NUM_OF_IF)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsWtpFwlAclAction != AP_FWL_PERMIT) &&
        (i4TestValFsWtpFwlAclAction != AP_FWL_DENY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (OSIX_SUCCESS == WssIfWtpFwlAclGetEntry (&WssIfWtpFwlAcl))
    {
        if (WssIfWtpFwlAcl.i4RowStatus == ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
    return i1Status;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpFwlAclSequenceNumber
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlAclIfIndex
FsWtpFwlAclName
FsWtpFwlAclDirection

The Object 
testValFsWtpFwlAclSequenceNumber
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpFwlAclSequenceNumber (UINT4 *pu4ErrorCode,
                                    UINT4 u4CapwapBaseWtpProfileId,
                                    INT4 i4FsWtpFwlAclIfIndex,
                                    tSNMP_OCTET_STRING_TYPE * pFsWtpFwlAclName,
                                    INT4 i4FsWtpFwlAclDirection,
                                    INT4 i4TestValFsWtpFwlAclSequenceNumber)
{
    INT1                i1Status = SNMP_SUCCESS;
    tWssIfWtpFwlAclDB   WssIfWtpFwlAcl;

    MEMSET (&WssIfWtpFwlAcl, 0, sizeof (tWssIfWtpFwlAclDB));

    WssIfWtpFwlAcl.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpFwlAcl.i4AclIfIndex = i4FsWtpFwlAclIfIndex;
    WssIfWtpFwlAcl.u1AclDirection = i4FsWtpFwlAclDirection;
    MEMCPY (WssIfWtpFwlAcl.au1AclName, pFsWtpFwlAclName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlAclName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlAcl.au1AclName)));
    WssIfWtpFwlAcl.au1AclName[pFsWtpFwlAclName->i4_Length] =
        AP_FWL_END_OF_STRING;
    if (i4FsWtpFwlAclIfIndex > AP_FWL_MAX_NUM_OF_IF)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsWtpFwlAclSequenceNumber < 1) ||
        (i4TestValFsWtpFwlAclSequenceNumber > 65535))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (OSIX_SUCCESS == WssIfWtpFwlAclGetEntry (&WssIfWtpFwlAcl))
    {
        if (WssIfWtpFwlAcl.i4RowStatus == ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
    return i1Status;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpFwlAclLogTrigger
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlAclIfIndex
FsWtpFwlAclName
FsWtpFwlAclDirection

The Object 
testValFsWtpFwlAclLogTrigger
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpFwlAclLogTrigger (UINT4 *pu4ErrorCode,
                                UINT4 u4CapwapBaseWtpProfileId,
                                INT4 i4FsWtpFwlAclIfIndex,
                                tSNMP_OCTET_STRING_TYPE * pFsWtpFwlAclName,
                                INT4 i4FsWtpFwlAclDirection,
                                INT4 i4TestValFsWtpFwlAclLogTrigger)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpFwlAclIfIndex);
    UNUSED_PARAM (pFsWtpFwlAclName);
    UNUSED_PARAM (i4FsWtpFwlAclDirection);
    UNUSED_PARAM (i4TestValFsWtpFwlAclLogTrigger);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpFwlAclFragAction
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlAclIfIndex
FsWtpFwlAclName
FsWtpFwlAclDirection

The Object 
testValFsWtpFwlAclFragAction
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpFwlAclFragAction (UINT4 *pu4ErrorCode,
                                UINT4 u4CapwapBaseWtpProfileId,
                                INT4 i4FsWtpFwlAclIfIndex,
                                tSNMP_OCTET_STRING_TYPE * pFsWtpFwlAclName,
                                INT4 i4FsWtpFwlAclDirection,
                                INT4 i4TestValFsWtpFwlAclFragAction)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpFwlAclIfIndex);
    UNUSED_PARAM (pFsWtpFwlAclName);
    UNUSED_PARAM (i4FsWtpFwlAclDirection);
    UNUSED_PARAM (i4TestValFsWtpFwlAclFragAction);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpFwlAclRowStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlAclIfIndex
FsWtpFwlAclName
FsWtpFwlAclDirection

The Object 
testValFsWtpFwlAclRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpFwlAclRowStatus (UINT4 *pu4ErrorCode,
                               UINT4 u4CapwapBaseWtpProfileId,
                               INT4 i4FsWtpFwlAclIfIndex,
                               tSNMP_OCTET_STRING_TYPE * pFsWtpFwlAclName,
                               INT4 i4FsWtpFwlAclDirection,
                               INT4 i4TestValFsWtpFwlAclRowStatus)
{
    INT4                i4RowStatus = 0;
    INT1                i1RetVal = 0;
    tWssIfWtpFwlAclDB   WssIfWtpFwlAcl;

    MEMSET (&WssIfWtpFwlAcl, 0, sizeof (tWssIfWtpFwlAclDB));

    WssIfWtpFwlAcl.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpFwlAcl.i4AclIfIndex = i4FsWtpFwlAclIfIndex;
    WssIfWtpFwlAcl.u1AclDirection = i4FsWtpFwlAclDirection;
    MEMCPY (WssIfWtpFwlAcl.au1AclName, pFsWtpFwlAclName->pu1_OctetList,
            MEM_MAX_BYTES (pFsWtpFwlAclName->i4_Length,
                           (INT4) sizeof (WssIfWtpFwlAcl.au1AclName)));
    WssIfWtpFwlAcl.au1AclName[pFsWtpFwlAclName->i4_Length] =
        AP_FWL_END_OF_STRING;

    i1RetVal = nmhGetCapwapBaseWtpProfileRowStatus (u4CapwapBaseWtpProfileId,
                                                    &i4RowStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (i4FsWtpFwlAclIfIndex > AP_FWL_MAX_NUM_OF_IF)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (nmhGetFsWtpFwlFilterRowStatus (u4CapwapBaseWtpProfileId,
                                       pFsWtpFwlAclName,
                                       &i4RowStatus) == SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsWtpFwlAclRowStatus != CREATE_AND_WAIT) &&
        (i4TestValFsWtpFwlAclRowStatus != CREATE_AND_GO))
    {
        if (OSIX_FAILURE == WssIfWtpFwlAclGetEntry (&WssIfWtpFwlAcl))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        if (i4TestValFsWtpFwlAclRowStatus == ACTIVE)
        {
            if ((WssIfWtpFwlAcl.u1Action == 0) ||
                (WssIfWtpFwlAcl.u2SeqNum == 0))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsWtpFwlAclTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpFwlAclIfIndex
FsWtpFwlAclName
FsWtpFwlAclDirection
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsWtpFwlAclTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWtpNatDynamicTransTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsWtpNatDynamicTransTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatDynamicTransInterfaceNum
FsWtpNatDynamicTransLocalIp
FsWtpNatDynamicTransLocalPort
FsWtpNatDynamicTransOutsideIp
FsWtpNatDynamicTransOutsidePort
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsWtpNatDynamicTransTable (UINT4
                                                   u4CapwapBaseWtpProfileId,
                                                   INT4
                                                   i4FsWtpNatDynamicTransInterfaceNum,
                                                   UINT4
                                                   u4FsWtpNatDynamicTransLocalIp,
                                                   INT4
                                                   i4FsWtpNatDynamicTransLocalPort,
                                                   UINT4
                                                   u4FsWtpNatDynamicTransOutsideIp,
                                                   INT4
                                                   i4FsWtpNatDynamicTransOutsidePort)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatDynamicTransInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatDynamicTransLocalIp);
    UNUSED_PARAM (i4FsWtpNatDynamicTransLocalPort);
    UNUSED_PARAM (u4FsWtpNatDynamicTransOutsideIp);
    UNUSED_PARAM (i4FsWtpNatDynamicTransOutsidePort);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsWtpNatDynamicTransTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatDynamicTransInterfaceNum
FsWtpNatDynamicTransLocalIp
FsWtpNatDynamicTransLocalPort
FsWtpNatDynamicTransOutsideIp
FsWtpNatDynamicTransOutsidePort
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsWtpNatDynamicTransTable (UINT4 *pu4CapwapBaseWtpProfileId,
                                           INT4
                                           *pi4FsWtpNatDynamicTransInterfaceNum,
                                           UINT4
                                           *pu4FsWtpNatDynamicTransLocalIp,
                                           INT4
                                           *pi4FsWtpNatDynamicTransLocalPort,
                                           UINT4
                                           *pu4FsWtpNatDynamicTransOutsideIp,
                                           INT4
                                           *pi4FsWtpNatDynamicTransOutsidePort)
{
    UNUSED_PARAM (pu4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pi4FsWtpNatDynamicTransInterfaceNum);
    UNUSED_PARAM (pu4FsWtpNatDynamicTransLocalIp);
    UNUSED_PARAM (pi4FsWtpNatDynamicTransLocalPort);
    UNUSED_PARAM (pu4FsWtpNatDynamicTransOutsideIp);
    UNUSED_PARAM (pi4FsWtpNatDynamicTransOutsidePort);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsWtpNatDynamicTransTable
Input       :  The Indices
CapwapBaseWtpProfileId
nextCapwapBaseWtpProfileId
FsWtpNatDynamicTransInterfaceNum
nextFsWtpNatDynamicTransInterfaceNum
FsWtpNatDynamicTransLocalIp
nextFsWtpNatDynamicTransLocalIp
FsWtpNatDynamicTransLocalPort
nextFsWtpNatDynamicTransLocalPort
FsWtpNatDynamicTransOutsideIp
nextFsWtpNatDynamicTransOutsideIp
FsWtpNatDynamicTransOutsidePort
nextFsWtpNatDynamicTransOutsidePort
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsWtpNatDynamicTransTable (UINT4 u4CapwapBaseWtpProfileId,
                                          UINT4 *pu4NextCapwapBaseWtpProfileId,
                                          INT4
                                          i4FsWtpNatDynamicTransInterfaceNum,
                                          INT4
                                          *pi4NextFsWtpNatDynamicTransInterfaceNum,
                                          UINT4 u4FsWtpNatDynamicTransLocalIp,
                                          UINT4
                                          *pu4NextFsWtpNatDynamicTransLocalIp,
                                          INT4 i4FsWtpNatDynamicTransLocalPort,
                                          INT4
                                          *pi4NextFsWtpNatDynamicTransLocalPort,
                                          UINT4 u4FsWtpNatDynamicTransOutsideIp,
                                          UINT4
                                          *pu4NextFsWtpNatDynamicTransOutsideIp,
                                          INT4
                                          i4FsWtpNatDynamicTransOutsidePort,
                                          INT4
                                          *pi4NextFsWtpNatDynamicTransOutsidePort)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pu4NextCapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatDynamicTransInterfaceNum);
    UNUSED_PARAM (pi4NextFsWtpNatDynamicTransInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatDynamicTransLocalIp);
    UNUSED_PARAM (pu4NextFsWtpNatDynamicTransLocalIp);
    UNUSED_PARAM (i4FsWtpNatDynamicTransLocalPort);
    UNUSED_PARAM (pi4NextFsWtpNatDynamicTransLocalPort);
    UNUSED_PARAM (u4FsWtpNatDynamicTransOutsideIp);
    UNUSED_PARAM (pu4NextFsWtpNatDynamicTransOutsideIp);
    UNUSED_PARAM (i4FsWtpNatDynamicTransOutsidePort);
    UNUSED_PARAM (pi4NextFsWtpNatDynamicTransOutsidePort);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsWtpNatDynamicTransTranslatedLocalIp
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatDynamicTransInterfaceNum
FsWtpNatDynamicTransLocalIp
FsWtpNatDynamicTransLocalPort
FsWtpNatDynamicTransOutsideIp
FsWtpNatDynamicTransOutsidePort

The Object 
retValFsWtpNatDynamicTransTranslatedLocalIp
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpNatDynamicTransTranslatedLocalIp (UINT4 u4CapwapBaseWtpProfileId,
                                             INT4
                                             i4FsWtpNatDynamicTransInterfaceNum,
                                             UINT4
                                             u4FsWtpNatDynamicTransLocalIp,
                                             INT4
                                             i4FsWtpNatDynamicTransLocalPort,
                                             UINT4
                                             u4FsWtpNatDynamicTransOutsideIp,
                                             INT4
                                             i4FsWtpNatDynamicTransOutsidePort,
                                             UINT4
                                             *pu4RetValFsWtpNatDynamicTransTranslatedLocalIp)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatDynamicTransInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatDynamicTransLocalIp);
    UNUSED_PARAM (i4FsWtpNatDynamicTransLocalPort);
    UNUSED_PARAM (u4FsWtpNatDynamicTransOutsideIp);
    UNUSED_PARAM (i4FsWtpNatDynamicTransOutsidePort);
    UNUSED_PARAM (pu4RetValFsWtpNatDynamicTransTranslatedLocalIp);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsWtpNatDynamicTransTranslatedLocalPort
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatDynamicTransInterfaceNum
FsWtpNatDynamicTransLocalIp
FsWtpNatDynamicTransLocalPort
FsWtpNatDynamicTransOutsideIp
FsWtpNatDynamicTransOutsidePort

The Object 
retValFsWtpNatDynamicTransTranslatedLocalPort
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpNatDynamicTransTranslatedLocalPort (UINT4 u4CapwapBaseWtpProfileId,
                                               INT4
                                               i4FsWtpNatDynamicTransInterfaceNum,
                                               UINT4
                                               u4FsWtpNatDynamicTransLocalIp,
                                               INT4
                                               i4FsWtpNatDynamicTransLocalPort,
                                               UINT4
                                               u4FsWtpNatDynamicTransOutsideIp,
                                               INT4
                                               i4FsWtpNatDynamicTransOutsidePort,
                                               INT4
                                               *pi4RetValFsWtpNatDynamicTransTranslatedLocalPort)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatDynamicTransInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatDynamicTransLocalIp);
    UNUSED_PARAM (i4FsWtpNatDynamicTransLocalPort);
    UNUSED_PARAM (u4FsWtpNatDynamicTransOutsideIp);
    UNUSED_PARAM (i4FsWtpNatDynamicTransOutsidePort);
    UNUSED_PARAM (pi4RetValFsWtpNatDynamicTransTranslatedLocalPort);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsWtpNatDynamicTransLastUseTime
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatDynamicTransInterfaceNum
FsWtpNatDynamicTransLocalIp
FsWtpNatDynamicTransLocalPort
FsWtpNatDynamicTransOutsideIp
FsWtpNatDynamicTransOutsidePort

The Object 
retValFsWtpNatDynamicTransLastUseTime
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpNatDynamicTransLastUseTime (UINT4 u4CapwapBaseWtpProfileId,
                                       INT4 i4FsWtpNatDynamicTransInterfaceNum,
                                       UINT4 u4FsWtpNatDynamicTransLocalIp,
                                       INT4 i4FsWtpNatDynamicTransLocalPort,
                                       UINT4 u4FsWtpNatDynamicTransOutsideIp,
                                       INT4 i4FsWtpNatDynamicTransOutsidePort,
                                       INT4
                                       *pi4RetValFsWtpNatDynamicTransLastUseTime)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatDynamicTransInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatDynamicTransLocalIp);
    UNUSED_PARAM (i4FsWtpNatDynamicTransLocalPort);
    UNUSED_PARAM (u4FsWtpNatDynamicTransOutsideIp);
    UNUSED_PARAM (i4FsWtpNatDynamicTransOutsidePort);
    UNUSED_PARAM (pi4RetValFsWtpNatDynamicTransLastUseTime);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWtpNatGlobalAddressTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsWtpNatGlobalAddressTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatGlobalAddressInterfaceNum
FsWtpNatGlobalAddressTranslatedLocalIp
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWtpNatGlobalAddressTable (UINT4
                                                    u4CapwapBaseWtpProfileId,
                                                    INT4
                                                    i4FsWtpNatGlobalAddressInterfaceNum,
                                                    UINT4
                                                    u4FsWtpNatGlobalAddressTranslatedLocalIp)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatGlobalAddressInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatGlobalAddressTranslatedLocalIp);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsWtpNatGlobalAddressTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatGlobalAddressInterfaceNum
FsWtpNatGlobalAddressTranslatedLocalIp
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsWtpNatGlobalAddressTable (UINT4 *pu4CapwapBaseWtpProfileId,
                                            INT4
                                            *pi4FsWtpNatGlobalAddressInterfaceNum,
                                            UINT4
                                            *pu4FsWtpNatGlobalAddressTranslatedLocalIp)
{
    UNUSED_PARAM (pu4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pi4FsWtpNatGlobalAddressInterfaceNum);
    UNUSED_PARAM (pu4FsWtpNatGlobalAddressTranslatedLocalIp);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsWtpNatGlobalAddressTable
Input       :  The Indices
CapwapBaseWtpProfileId
nextCapwapBaseWtpProfileId
FsWtpNatGlobalAddressInterfaceNum
nextFsWtpNatGlobalAddressInterfaceNum
FsWtpNatGlobalAddressTranslatedLocalIp
nextFsWtpNatGlobalAddressTranslatedLocalIp
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsWtpNatGlobalAddressTable (UINT4 u4CapwapBaseWtpProfileId,
                                           UINT4 *pu4NextCapwapBaseWtpProfileId,
                                           INT4
                                           i4FsWtpNatGlobalAddressInterfaceNum,
                                           INT4
                                           *pi4NextFsWtpNatGlobalAddressInterfaceNum,
                                           UINT4
                                           u4FsWtpNatGlobalAddressTranslatedLocalIp,
                                           UINT4
                                           *pu4NextFsWtpNatGlobalAddressTranslatedLocalIp)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pu4NextCapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatGlobalAddressInterfaceNum);
    UNUSED_PARAM (pi4NextFsWtpNatGlobalAddressInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatGlobalAddressTranslatedLocalIp);
    UNUSED_PARAM (pu4NextFsWtpNatGlobalAddressTranslatedLocalIp);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsWtpNatGlobalAddressMask
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatGlobalAddressInterfaceNum
FsWtpNatGlobalAddressTranslatedLocalIp

The Object 
retValFsWtpNatGlobalAddressMask
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpNatGlobalAddressMask (UINT4 u4CapwapBaseWtpProfileId,
                                 INT4 i4FsWtpNatGlobalAddressInterfaceNum,
                                 UINT4 u4FsWtpNatGlobalAddressTranslatedLocalIp,
                                 UINT4 *pu4RetValFsWtpNatGlobalAddressMask)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatGlobalAddressInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatGlobalAddressTranslatedLocalIp);
    UNUSED_PARAM (pu4RetValFsWtpNatGlobalAddressMask);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsWtpNatGlobalAddressEntryStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatGlobalAddressInterfaceNum
FsWtpNatGlobalAddressTranslatedLocalIp

The Object 
retValFsWtpNatGlobalAddressEntryStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpNatGlobalAddressEntryStatus (UINT4 u4CapwapBaseWtpProfileId,
                                        INT4
                                        i4FsWtpNatGlobalAddressInterfaceNum,
                                        UINT4
                                        u4FsWtpNatGlobalAddressTranslatedLocalIp,
                                        INT4
                                        *pi4RetValFsWtpNatGlobalAddressEntryStatus)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatGlobalAddressInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatGlobalAddressTranslatedLocalIp);
    UNUSED_PARAM (pi4RetValFsWtpNatGlobalAddressEntryStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsWtpNatGlobalAddressMask
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatGlobalAddressInterfaceNum
FsWtpNatGlobalAddressTranslatedLocalIp

The Object 
setValFsWtpNatGlobalAddressMask
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpNatGlobalAddressMask (UINT4 u4CapwapBaseWtpProfileId,
                                 INT4 i4FsWtpNatGlobalAddressInterfaceNum,
                                 UINT4 u4FsWtpNatGlobalAddressTranslatedLocalIp,
                                 UINT4 u4SetValFsWtpNatGlobalAddressMask)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatGlobalAddressInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatGlobalAddressTranslatedLocalIp);
    UNUSED_PARAM (u4SetValFsWtpNatGlobalAddressMask);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsWtpNatGlobalAddressEntryStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatGlobalAddressInterfaceNum
FsWtpNatGlobalAddressTranslatedLocalIp

The Object 
setValFsWtpNatGlobalAddressEntryStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpNatGlobalAddressEntryStatus (UINT4 u4CapwapBaseWtpProfileId,
                                        INT4
                                        i4FsWtpNatGlobalAddressInterfaceNum,
                                        UINT4
                                        u4FsWtpNatGlobalAddressTranslatedLocalIp,
                                        INT4
                                        i4SetValFsWtpNatGlobalAddressEntryStatus)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatGlobalAddressInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatGlobalAddressTranslatedLocalIp);
    UNUSED_PARAM (i4SetValFsWtpNatGlobalAddressEntryStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsWtpNatGlobalAddressMask
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatGlobalAddressInterfaceNum
FsWtpNatGlobalAddressTranslatedLocalIp

The Object 
testValFsWtpNatGlobalAddressMask
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpNatGlobalAddressMask (UINT4 *pu4ErrorCode,
                                    UINT4 u4CapwapBaseWtpProfileId,
                                    INT4 i4FsWtpNatGlobalAddressInterfaceNum,
                                    UINT4
                                    u4FsWtpNatGlobalAddressTranslatedLocalIp,
                                    UINT4 u4TestValFsWtpNatGlobalAddressMask)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatGlobalAddressInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatGlobalAddressTranslatedLocalIp);
    UNUSED_PARAM (u4TestValFsWtpNatGlobalAddressMask);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpNatGlobalAddressEntryStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatGlobalAddressInterfaceNum
FsWtpNatGlobalAddressTranslatedLocalIp

The Object 
testValFsWtpNatGlobalAddressEntryStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpNatGlobalAddressEntryStatus (UINT4 *pu4ErrorCode,
                                           UINT4 u4CapwapBaseWtpProfileId,
                                           INT4
                                           i4FsWtpNatGlobalAddressInterfaceNum,
                                           UINT4
                                           u4FsWtpNatGlobalAddressTranslatedLocalIp,
                                           INT4
                                           i4TestValFsWtpNatGlobalAddressEntryStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatGlobalAddressInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatGlobalAddressTranslatedLocalIp);
    UNUSED_PARAM (i4TestValFsWtpNatGlobalAddressEntryStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsWtpNatGlobalAddressTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatGlobalAddressInterfaceNum
FsWtpNatGlobalAddressTranslatedLocalIp
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsWtpNatGlobalAddressTable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWtpNatLocalAddressTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsWtpNatLocalAddressTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatLocalAddressInterfaceNumber
FsWtpNatLocalAddressLocalIp
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWtpNatLocalAddressTable (UINT4
                                                   u4CapwapBaseWtpProfileId,
                                                   INT4
                                                   i4FsWtpNatLocalAddressInterfaceNumber,
                                                   UINT4
                                                   u4FsWtpNatLocalAddressLocalIp)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatLocalAddressInterfaceNumber);
    UNUSED_PARAM (u4FsWtpNatLocalAddressLocalIp);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsWtpNatLocalAddressTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatLocalAddressInterfaceNumber
FsWtpNatLocalAddressLocalIp
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsWtpNatLocalAddressTable (UINT4 *pu4CapwapBaseWtpProfileId,
                                           INT4
                                           *pi4FsWtpNatLocalAddressInterfaceNumber,
                                           UINT4
                                           *pu4FsWtpNatLocalAddressLocalIp)
{
    UNUSED_PARAM (pu4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pi4FsWtpNatLocalAddressInterfaceNumber);
    UNUSED_PARAM (pu4FsWtpNatLocalAddressLocalIp);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsWtpNatLocalAddressTable
Input       :  The Indices
CapwapBaseWtpProfileId
nextCapwapBaseWtpProfileId
FsWtpNatLocalAddressInterfaceNumber
nextFsWtpNatLocalAddressInterfaceNumber
FsWtpNatLocalAddressLocalIp
nextFsWtpNatLocalAddressLocalIp
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsWtpNatLocalAddressTable (UINT4 u4CapwapBaseWtpProfileId,
                                          UINT4 *pu4NextCapwapBaseWtpProfileId,
                                          INT4
                                          i4FsWtpNatLocalAddressInterfaceNumber,
                                          INT4
                                          *pi4NextFsWtpNatLocalAddressInterfaceNumber,
                                          UINT4 u4FsWtpNatLocalAddressLocalIp,
                                          UINT4
                                          *pu4NextFsWtpNatLocalAddressLocalIp)
{
    UNUSED_PARAM (pu4NextFsWtpNatLocalAddressLocalIp);
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pu4NextCapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatLocalAddressInterfaceNumber);
    UNUSED_PARAM (pi4NextFsWtpNatLocalAddressInterfaceNumber);
    UNUSED_PARAM (u4FsWtpNatLocalAddressLocalIp);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsWtpNatLocalAddressMask
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatLocalAddressInterfaceNumber
FsWtpNatLocalAddressLocalIp

The Object 
retValFsWtpNatLocalAddressMask
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpNatLocalAddressMask (UINT4 u4CapwapBaseWtpProfileId,
                                INT4 i4FsWtpNatLocalAddressInterfaceNumber,
                                UINT4 u4FsWtpNatLocalAddressLocalIp,
                                UINT4 *pu4RetValFsWtpNatLocalAddressMask)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatLocalAddressInterfaceNumber);
    UNUSED_PARAM (u4FsWtpNatLocalAddressLocalIp);
    UNUSED_PARAM (pu4RetValFsWtpNatLocalAddressMask);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsWtpNatLocalAddressEntryStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatLocalAddressInterfaceNumber
FsWtpNatLocalAddressLocalIp

The Object 
retValFsWtpNatLocalAddressEntryStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpNatLocalAddressEntryStatus (UINT4 u4CapwapBaseWtpProfileId,
                                       INT4
                                       i4FsWtpNatLocalAddressInterfaceNumber,
                                       UINT4 u4FsWtpNatLocalAddressLocalIp,
                                       INT4
                                       *pi4RetValFsWtpNatLocalAddressEntryStatus)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatLocalAddressInterfaceNumber);
    UNUSED_PARAM (u4FsWtpNatLocalAddressLocalIp);
    UNUSED_PARAM (pi4RetValFsWtpNatLocalAddressEntryStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsWtpNatLocalAddressMask
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatLocalAddressInterfaceNumber
FsWtpNatLocalAddressLocalIp

The Object 
setValFsWtpNatLocalAddressMask
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpNatLocalAddressMask (UINT4 u4CapwapBaseWtpProfileId,
                                INT4 i4FsWtpNatLocalAddressInterfaceNumber,
                                UINT4 u4FsWtpNatLocalAddressLocalIp,
                                UINT4 u4SetValFsWtpNatLocalAddressMask)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatLocalAddressInterfaceNumber);
    UNUSED_PARAM (u4FsWtpNatLocalAddressLocalIp);
    UNUSED_PARAM (u4SetValFsWtpNatLocalAddressMask);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsWtpNatLocalAddressEntryStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatLocalAddressInterfaceNumber
FsWtpNatLocalAddressLocalIp

The Object 
setValFsWtpNatLocalAddressEntryStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpNatLocalAddressEntryStatus (UINT4 u4CapwapBaseWtpProfileId,
                                       INT4
                                       i4FsWtpNatLocalAddressInterfaceNumber,
                                       UINT4 u4FsWtpNatLocalAddressLocalIp,
                                       INT4
                                       i4SetValFsWtpNatLocalAddressEntryStatus)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatLocalAddressInterfaceNumber);
    UNUSED_PARAM (u4FsWtpNatLocalAddressLocalIp);
    UNUSED_PARAM (i4SetValFsWtpNatLocalAddressEntryStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsWtpNatLocalAddressMask
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatLocalAddressInterfaceNumber
FsWtpNatLocalAddressLocalIp

The Object 
testValFsWtpNatLocalAddressMask
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpNatLocalAddressMask (UINT4 *pu4ErrorCode,
                                   UINT4 u4CapwapBaseWtpProfileId,
                                   INT4 i4FsWtpNatLocalAddressInterfaceNumber,
                                   UINT4 u4FsWtpNatLocalAddressLocalIp,
                                   UINT4 u4TestValFsWtpNatLocalAddressMask)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatLocalAddressInterfaceNumber);
    UNUSED_PARAM (u4FsWtpNatLocalAddressLocalIp);
    UNUSED_PARAM (u4TestValFsWtpNatLocalAddressMask);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpNatLocalAddressEntryStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatLocalAddressInterfaceNumber
FsWtpNatLocalAddressLocalIp

The Object 
testValFsWtpNatLocalAddressEntryStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpNatLocalAddressEntryStatus (UINT4 *pu4ErrorCode,
                                          UINT4 u4CapwapBaseWtpProfileId,
                                          INT4
                                          i4FsWtpNatLocalAddressInterfaceNumber,
                                          UINT4 u4FsWtpNatLocalAddressLocalIp,
                                          INT4
                                          i4TestValFsWtpNatLocalAddressEntryStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatLocalAddressInterfaceNumber);
    UNUSED_PARAM (u4FsWtpNatLocalAddressLocalIp);
    UNUSED_PARAM (i4TestValFsWtpNatLocalAddressEntryStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsWtpNatLocalAddressTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatLocalAddressInterfaceNumber
FsWtpNatLocalAddressLocalIp
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsWtpNatLocalAddressTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWtpNatStaticTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsWtpNatStaticTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticInterfaceNum
FsWtpNatStaticLocalIp
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWtpNatStaticTable (UINT4 u4CapwapBaseWtpProfileId,
                                             INT4 i4FsWtpNatStaticInterfaceNum,
                                             UINT4 u4FsWtpNatStaticLocalIp)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatStaticInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatStaticLocalIp);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsWtpNatStaticTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticInterfaceNum
FsWtpNatStaticLocalIp
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsWtpNatStaticTable (UINT4 *pu4CapwapBaseWtpProfileId,
                                     INT4 *pi4FsWtpNatStaticInterfaceNum,
                                     UINT4 *pu4FsWtpNatStaticLocalIp)
{
    UNUSED_PARAM (pu4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pi4FsWtpNatStaticInterfaceNum);
    UNUSED_PARAM (pu4FsWtpNatStaticLocalIp);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsWtpNatStaticTable
Input       :  The Indices
CapwapBaseWtpProfileId
nextCapwapBaseWtpProfileId
FsWtpNatStaticInterfaceNum
nextFsWtpNatStaticInterfaceNum
FsWtpNatStaticLocalIp
nextFsWtpNatStaticLocalIp
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsWtpNatStaticTable (UINT4 u4CapwapBaseWtpProfileId,
                                    UINT4 *pu4NextCapwapBaseWtpProfileId,
                                    INT4 i4FsWtpNatStaticInterfaceNum,
                                    INT4 *pi4NextFsWtpNatStaticInterfaceNum,
                                    UINT4 u4FsWtpNatStaticLocalIp,
                                    UINT4 *pu4NextFsWtpNatStaticLocalIp)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pu4NextCapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatStaticInterfaceNum);
    UNUSED_PARAM (pi4NextFsWtpNatStaticInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatStaticLocalIp);
    UNUSED_PARAM (pu4NextFsWtpNatStaticLocalIp);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsWtpNatStaticTranslatedLocalIp
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticInterfaceNum
FsWtpNatStaticLocalIp

The Object 
retValFsWtpNatStaticTranslatedLocalIp
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpNatStaticTranslatedLocalIp (UINT4 u4CapwapBaseWtpProfileId,
                                       INT4 i4FsWtpNatStaticInterfaceNum,
                                       UINT4 u4FsWtpNatStaticLocalIp,
                                       UINT4
                                       *pu4RetValFsWtpNatStaticTranslatedLocalIp)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatStaticInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatStaticLocalIp);
    UNUSED_PARAM (pu4RetValFsWtpNatStaticTranslatedLocalIp);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsWtpNatStaticEntryStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticInterfaceNum
FsWtpNatStaticLocalIp

The Object 
retValFsWtpNatStaticEntryStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpNatStaticEntryStatus (UINT4 u4CapwapBaseWtpProfileId,
                                 INT4 i4FsWtpNatStaticInterfaceNum,
                                 UINT4 u4FsWtpNatStaticLocalIp,
                                 INT4 *pi4RetValFsWtpNatStaticEntryStatus)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatStaticInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatStaticLocalIp);
    UNUSED_PARAM (pi4RetValFsWtpNatStaticEntryStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsWtpNatStaticTranslatedLocalIp
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticInterfaceNum
FsWtpNatStaticLocalIp

The Object 
setValFsWtpNatStaticTranslatedLocalIp
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpNatStaticTranslatedLocalIp (UINT4 u4CapwapBaseWtpProfileId,
                                       INT4 i4FsWtpNatStaticInterfaceNum,
                                       UINT4 u4FsWtpNatStaticLocalIp,
                                       UINT4
                                       u4SetValFsWtpNatStaticTranslatedLocalIp)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatStaticInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatStaticLocalIp);
    UNUSED_PARAM (u4SetValFsWtpNatStaticTranslatedLocalIp);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsWtpNatStaticEntryStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticInterfaceNum
FsWtpNatStaticLocalIp

The Object 
setValFsWtpNatStaticEntryStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpNatStaticEntryStatus (UINT4 u4CapwapBaseWtpProfileId,
                                 INT4 i4FsWtpNatStaticInterfaceNum,
                                 UINT4 u4FsWtpNatStaticLocalIp,
                                 INT4 i4SetValFsWtpNatStaticEntryStatus)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatStaticInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatStaticLocalIp);
    UNUSED_PARAM (i4SetValFsWtpNatStaticEntryStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsWtpNatStaticTranslatedLocalIp
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticInterfaceNum
FsWtpNatStaticLocalIp

The Object 
testValFsWtpNatStaticTranslatedLocalIp
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpNatStaticTranslatedLocalIp (UINT4 *pu4ErrorCode,
                                          UINT4 u4CapwapBaseWtpProfileId,
                                          INT4 i4FsWtpNatStaticInterfaceNum,
                                          UINT4 u4FsWtpNatStaticLocalIp,
                                          UINT4
                                          u4TestValFsWtpNatStaticTranslatedLocalIp)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatStaticInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatStaticLocalIp);
    UNUSED_PARAM (u4TestValFsWtpNatStaticTranslatedLocalIp);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpNatStaticEntryStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticInterfaceNum
FsWtpNatStaticLocalIp

The Object 
testValFsWtpNatStaticEntryStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpNatStaticEntryStatus (UINT4 *pu4ErrorCode,
                                    UINT4 u4CapwapBaseWtpProfileId,
                                    INT4 i4FsWtpNatStaticInterfaceNum,
                                    UINT4 u4FsWtpNatStaticLocalIp,
                                    INT4 i4TestValFsWtpNatStaticEntryStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatStaticInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatStaticLocalIp);
    UNUSED_PARAM (i4TestValFsWtpNatStaticEntryStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsWtpNatStaticTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticInterfaceNum
FsWtpNatStaticLocalIp
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsWtpNatStaticTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWtpNatStaticNaptTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsWtpNatStaticNaptTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticNaptInterfaceNum
FsWtpNatStaticNaptLocalIp
FsWtpNatStaticNaptStartLocalPort
FsWtpNatStaticNaptEndLocalPort
FsWtpNatStaticNaptProtocolNumber
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWtpNatStaticNaptTable (UINT4 u4CapwapBaseWtpProfileId,
                                                 INT4
                                                 i4FsWtpNatStaticNaptInterfaceNum,
                                                 UINT4
                                                 u4FsWtpNatStaticNaptLocalIp,
                                                 INT4
                                                 i4FsWtpNatStaticNaptStartLocalPort,
                                                 INT4
                                                 i4FsWtpNatStaticNaptEndLocalPort,
                                                 INT4
                                                 i4FsWtpNatStaticNaptProtocolNumber)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatStaticNaptInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatStaticNaptLocalIp);
    UNUSED_PARAM (i4FsWtpNatStaticNaptStartLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptEndLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptProtocolNumber);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsWtpNatStaticNaptTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticNaptInterfaceNum
FsWtpNatStaticNaptLocalIp
FsWtpNatStaticNaptStartLocalPort
FsWtpNatStaticNaptEndLocalPort
FsWtpNatStaticNaptProtocolNumber
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsWtpNatStaticNaptTable (UINT4 *pu4CapwapBaseWtpProfileId,
                                         INT4
                                         *pi4FsWtpNatStaticNaptInterfaceNum,
                                         UINT4 *pu4FsWtpNatStaticNaptLocalIp,
                                         INT4
                                         *pi4FsWtpNatStaticNaptStartLocalPort,
                                         INT4
                                         *pi4FsWtpNatStaticNaptEndLocalPort,
                                         INT4
                                         *pi4FsWtpNatStaticNaptProtocolNumber)
{
    UNUSED_PARAM (pu4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pi4FsWtpNatStaticNaptInterfaceNum);
    UNUSED_PARAM (pu4FsWtpNatStaticNaptLocalIp);
    UNUSED_PARAM (pi4FsWtpNatStaticNaptStartLocalPort);
    UNUSED_PARAM (pi4FsWtpNatStaticNaptEndLocalPort);
    UNUSED_PARAM (pi4FsWtpNatStaticNaptProtocolNumber);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsWtpNatStaticNaptTable
Input       :  The Indices
CapwapBaseWtpProfileId
nextCapwapBaseWtpProfileId
FsWtpNatStaticNaptInterfaceNum
nextFsWtpNatStaticNaptInterfaceNum
FsWtpNatStaticNaptLocalIp
nextFsWtpNatStaticNaptLocalIp
FsWtpNatStaticNaptStartLocalPort
nextFsWtpNatStaticNaptStartLocalPort
FsWtpNatStaticNaptEndLocalPort
nextFsWtpNatStaticNaptEndLocalPort
FsWtpNatStaticNaptProtocolNumber
nextFsWtpNatStaticNaptProtocolNumber
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsWtpNatStaticNaptTable (UINT4 u4CapwapBaseWtpProfileId,
                                        UINT4 *pu4NextCapwapBaseWtpProfileId,
                                        INT4 i4FsWtpNatStaticNaptInterfaceNum,
                                        INT4
                                        *pi4NextFsWtpNatStaticNaptInterfaceNum,
                                        UINT4 u4FsWtpNatStaticNaptLocalIp,
                                        UINT4 *pu4NextFsWtpNatStaticNaptLocalIp,
                                        INT4 i4FsWtpNatStaticNaptStartLocalPort,
                                        INT4
                                        *pi4NextFsWtpNatStaticNaptStartLocalPort,
                                        INT4 i4FsWtpNatStaticNaptEndLocalPort,
                                        INT4
                                        *pi4NextFsWtpNatStaticNaptEndLocalPort,
                                        INT4 i4FsWtpNatStaticNaptProtocolNumber,
                                        INT4
                                        *pi4NextFsWtpNatStaticNaptProtocolNumber)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pu4NextCapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatStaticNaptInterfaceNum);
    UNUSED_PARAM (pi4NextFsWtpNatStaticNaptInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatStaticNaptLocalIp);
    UNUSED_PARAM (pu4NextFsWtpNatStaticNaptLocalIp);
    UNUSED_PARAM (i4FsWtpNatStaticNaptStartLocalPort);
    UNUSED_PARAM (pi4NextFsWtpNatStaticNaptStartLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptEndLocalPort);
    UNUSED_PARAM (pi4NextFsWtpNatStaticNaptEndLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptProtocolNumber);
    UNUSED_PARAM (pi4NextFsWtpNatStaticNaptProtocolNumber);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsWtpNatStaticNaptTranslatedLocalIp
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticNaptInterfaceNum
FsWtpNatStaticNaptLocalIp
FsWtpNatStaticNaptStartLocalPort
FsWtpNatStaticNaptEndLocalPort
FsWtpNatStaticNaptProtocolNumber

The Object 
retValFsWtpNatStaticNaptTranslatedLocalIp
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpNatStaticNaptTranslatedLocalIp (UINT4 u4CapwapBaseWtpProfileId,
                                           INT4
                                           i4FsWtpNatStaticNaptInterfaceNum,
                                           UINT4 u4FsWtpNatStaticNaptLocalIp,
                                           INT4
                                           i4FsWtpNatStaticNaptStartLocalPort,
                                           INT4
                                           i4FsWtpNatStaticNaptEndLocalPort,
                                           INT4
                                           i4FsWtpNatStaticNaptProtocolNumber,
                                           UINT4
                                           *pu4RetValFsWtpNatStaticNaptTranslatedLocalIp)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatStaticNaptInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatStaticNaptLocalIp);
    UNUSED_PARAM (i4FsWtpNatStaticNaptStartLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptEndLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptProtocolNumber);
    UNUSED_PARAM (pu4RetValFsWtpNatStaticNaptTranslatedLocalIp);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsWtpNatStaticNaptTranslatedLocalPort
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticNaptInterfaceNum
FsWtpNatStaticNaptLocalIp
FsWtpNatStaticNaptStartLocalPort
FsWtpNatStaticNaptEndLocalPort
FsWtpNatStaticNaptProtocolNumber

The Object 
retValFsWtpNatStaticNaptTranslatedLocalPort
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpNatStaticNaptTranslatedLocalPort (UINT4 u4CapwapBaseWtpProfileId,
                                             INT4
                                             i4FsWtpNatStaticNaptInterfaceNum,
                                             UINT4 u4FsWtpNatStaticNaptLocalIp,
                                             INT4
                                             i4FsWtpNatStaticNaptStartLocalPort,
                                             INT4
                                             i4FsWtpNatStaticNaptEndLocalPort,
                                             INT4
                                             i4FsWtpNatStaticNaptProtocolNumber,
                                             INT4
                                             *pi4RetValFsWtpNatStaticNaptTranslatedLocalPort)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatStaticNaptInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatStaticNaptLocalIp);
    UNUSED_PARAM (i4FsWtpNatStaticNaptStartLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptEndLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptProtocolNumber);
    UNUSED_PARAM (pi4RetValFsWtpNatStaticNaptTranslatedLocalPort);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsWtpNatStaticNaptDescription
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticNaptInterfaceNum
FsWtpNatStaticNaptLocalIp
FsWtpNatStaticNaptStartLocalPort
FsWtpNatStaticNaptEndLocalPort
FsWtpNatStaticNaptProtocolNumber

The Object 
retValFsWtpNatStaticNaptDescription
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpNatStaticNaptDescription (UINT4 u4CapwapBaseWtpProfileId,
                                     INT4 i4FsWtpNatStaticNaptInterfaceNum,
                                     UINT4 u4FsWtpNatStaticNaptLocalIp,
                                     INT4 i4FsWtpNatStaticNaptStartLocalPort,
                                     INT4 i4FsWtpNatStaticNaptEndLocalPort,
                                     INT4 i4FsWtpNatStaticNaptProtocolNumber,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsWtpNatStaticNaptDescription)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatStaticNaptInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatStaticNaptLocalIp);
    UNUSED_PARAM (i4FsWtpNatStaticNaptStartLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptEndLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptProtocolNumber);
    UNUSED_PARAM (pRetValFsWtpNatStaticNaptDescription);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsWtpNatStaticNaptEntryStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticNaptInterfaceNum
FsWtpNatStaticNaptLocalIp
FsWtpNatStaticNaptStartLocalPort
FsWtpNatStaticNaptEndLocalPort
FsWtpNatStaticNaptProtocolNumber

The Object 
retValFsWtpNatStaticNaptEntryStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpNatStaticNaptEntryStatus (UINT4 u4CapwapBaseWtpProfileId,
                                     INT4 i4FsWtpNatStaticNaptInterfaceNum,
                                     UINT4 u4FsWtpNatStaticNaptLocalIp,
                                     INT4 i4FsWtpNatStaticNaptStartLocalPort,
                                     INT4 i4FsWtpNatStaticNaptEndLocalPort,
                                     INT4 i4FsWtpNatStaticNaptProtocolNumber,
                                     INT4
                                     *pi4RetValFsWtpNatStaticNaptEntryStatus)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatStaticNaptInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatStaticNaptLocalIp);
    UNUSED_PARAM (i4FsWtpNatStaticNaptStartLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptEndLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptProtocolNumber);
    UNUSED_PARAM (pi4RetValFsWtpNatStaticNaptEntryStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsWtpNatStaticNaptTranslatedLocalIp
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticNaptInterfaceNum
FsWtpNatStaticNaptLocalIp
FsWtpNatStaticNaptStartLocalPort
FsWtpNatStaticNaptEndLocalPort
FsWtpNatStaticNaptProtocolNumber

The Object 
setValFsWtpNatStaticNaptTranslatedLocalIp
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpNatStaticNaptTranslatedLocalIp (UINT4 u4CapwapBaseWtpProfileId,
                                           INT4
                                           i4FsWtpNatStaticNaptInterfaceNum,
                                           UINT4 u4FsWtpNatStaticNaptLocalIp,
                                           INT4
                                           i4FsWtpNatStaticNaptStartLocalPort,
                                           INT4
                                           i4FsWtpNatStaticNaptEndLocalPort,
                                           INT4
                                           i4FsWtpNatStaticNaptProtocolNumber,
                                           UINT4
                                           u4SetValFsWtpNatStaticNaptTranslatedLocalIp)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatStaticNaptInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatStaticNaptLocalIp);
    UNUSED_PARAM (i4FsWtpNatStaticNaptStartLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptEndLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptProtocolNumber);
    UNUSED_PARAM (u4SetValFsWtpNatStaticNaptTranslatedLocalIp);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsWtpNatStaticNaptTranslatedLocalPort
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticNaptInterfaceNum
FsWtpNatStaticNaptLocalIp
FsWtpNatStaticNaptStartLocalPort
FsWtpNatStaticNaptEndLocalPort
FsWtpNatStaticNaptProtocolNumber

The Object 
setValFsWtpNatStaticNaptTranslatedLocalPort
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpNatStaticNaptTranslatedLocalPort (UINT4 u4CapwapBaseWtpProfileId,
                                             INT4
                                             i4FsWtpNatStaticNaptInterfaceNum,
                                             UINT4 u4FsWtpNatStaticNaptLocalIp,
                                             INT4
                                             i4FsWtpNatStaticNaptStartLocalPort,
                                             INT4
                                             i4FsWtpNatStaticNaptEndLocalPort,
                                             INT4
                                             i4FsWtpNatStaticNaptProtocolNumber,
                                             INT4
                                             i4SetValFsWtpNatStaticNaptTranslatedLocalPort)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatStaticNaptInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatStaticNaptLocalIp);
    UNUSED_PARAM (i4FsWtpNatStaticNaptStartLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptEndLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptProtocolNumber);
    UNUSED_PARAM (i4SetValFsWtpNatStaticNaptTranslatedLocalPort);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsWtpNatStaticNaptDescription
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticNaptInterfaceNum
FsWtpNatStaticNaptLocalIp
FsWtpNatStaticNaptStartLocalPort
FsWtpNatStaticNaptEndLocalPort
FsWtpNatStaticNaptProtocolNumber

The Object 
setValFsWtpNatStaticNaptDescription
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpNatStaticNaptDescription (UINT4 u4CapwapBaseWtpProfileId,
                                     INT4 i4FsWtpNatStaticNaptInterfaceNum,
                                     UINT4 u4FsWtpNatStaticNaptLocalIp,
                                     INT4 i4FsWtpNatStaticNaptStartLocalPort,
                                     INT4 i4FsWtpNatStaticNaptEndLocalPort,
                                     INT4 i4FsWtpNatStaticNaptProtocolNumber,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValFsWtpNatStaticNaptDescription)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatStaticNaptInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatStaticNaptLocalIp);
    UNUSED_PARAM (i4FsWtpNatStaticNaptStartLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptEndLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptProtocolNumber);
    UNUSED_PARAM (pSetValFsWtpNatStaticNaptDescription);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsWtpNatStaticNaptEntryStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticNaptInterfaceNum
FsWtpNatStaticNaptLocalIp
FsWtpNatStaticNaptStartLocalPort
FsWtpNatStaticNaptEndLocalPort
FsWtpNatStaticNaptProtocolNumber

The Object 
setValFsWtpNatStaticNaptEntryStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpNatStaticNaptEntryStatus (UINT4 u4CapwapBaseWtpProfileId,
                                     INT4 i4FsWtpNatStaticNaptInterfaceNum,
                                     UINT4 u4FsWtpNatStaticNaptLocalIp,
                                     INT4 i4FsWtpNatStaticNaptStartLocalPort,
                                     INT4 i4FsWtpNatStaticNaptEndLocalPort,
                                     INT4 i4FsWtpNatStaticNaptProtocolNumber,
                                     INT4 i4SetValFsWtpNatStaticNaptEntryStatus)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatStaticNaptInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatStaticNaptLocalIp);
    UNUSED_PARAM (i4FsWtpNatStaticNaptStartLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptEndLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptProtocolNumber);
    UNUSED_PARAM (i4SetValFsWtpNatStaticNaptEntryStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsWtpNatStaticNaptTranslatedLocalIp
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticNaptInterfaceNum
FsWtpNatStaticNaptLocalIp
FsWtpNatStaticNaptStartLocalPort
FsWtpNatStaticNaptEndLocalPort
FsWtpNatStaticNaptProtocolNumber

The Object 
testValFsWtpNatStaticNaptTranslatedLocalIp
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpNatStaticNaptTranslatedLocalIp (UINT4 *pu4ErrorCode,
                                              UINT4 u4CapwapBaseWtpProfileId,
                                              INT4
                                              i4FsWtpNatStaticNaptInterfaceNum,
                                              UINT4 u4FsWtpNatStaticNaptLocalIp,
                                              INT4
                                              i4FsWtpNatStaticNaptStartLocalPort,
                                              INT4
                                              i4FsWtpNatStaticNaptEndLocalPort,
                                              INT4
                                              i4FsWtpNatStaticNaptProtocolNumber,
                                              UINT4
                                              u4TestValFsWtpNatStaticNaptTranslatedLocalIp)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatStaticNaptInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatStaticNaptLocalIp);
    UNUSED_PARAM (i4FsWtpNatStaticNaptStartLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptEndLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptProtocolNumber);
    UNUSED_PARAM (u4TestValFsWtpNatStaticNaptTranslatedLocalIp);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpNatStaticNaptTranslatedLocalPort
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticNaptInterfaceNum
FsWtpNatStaticNaptLocalIp
FsWtpNatStaticNaptStartLocalPort
FsWtpNatStaticNaptEndLocalPort
FsWtpNatStaticNaptProtocolNumber

The Object 
testValFsWtpNatStaticNaptTranslatedLocalPort
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpNatStaticNaptTranslatedLocalPort (UINT4 *pu4ErrorCode,
                                                UINT4 u4CapwapBaseWtpProfileId,
                                                INT4
                                                i4FsWtpNatStaticNaptInterfaceNum,
                                                UINT4
                                                u4FsWtpNatStaticNaptLocalIp,
                                                INT4
                                                i4FsWtpNatStaticNaptStartLocalPort,
                                                INT4
                                                i4FsWtpNatStaticNaptEndLocalPort,
                                                INT4
                                                i4FsWtpNatStaticNaptProtocolNumber,
                                                INT4
                                                i4TestValFsWtpNatStaticNaptTranslatedLocalPort)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatStaticNaptInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatStaticNaptLocalIp);
    UNUSED_PARAM (i4FsWtpNatStaticNaptStartLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptEndLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptProtocolNumber);
    UNUSED_PARAM (i4TestValFsWtpNatStaticNaptTranslatedLocalPort);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpNatStaticNaptDescription
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticNaptInterfaceNum
FsWtpNatStaticNaptLocalIp
FsWtpNatStaticNaptStartLocalPort
FsWtpNatStaticNaptEndLocalPort
FsWtpNatStaticNaptProtocolNumber

The Object 
testValFsWtpNatStaticNaptDescription
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpNatStaticNaptDescription (UINT4 *pu4ErrorCode,
                                        UINT4 u4CapwapBaseWtpProfileId,
                                        INT4 i4FsWtpNatStaticNaptInterfaceNum,
                                        UINT4 u4FsWtpNatStaticNaptLocalIp,
                                        INT4 i4FsWtpNatStaticNaptStartLocalPort,
                                        INT4 i4FsWtpNatStaticNaptEndLocalPort,
                                        INT4 i4FsWtpNatStaticNaptProtocolNumber,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValFsWtpNatStaticNaptDescription)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatStaticNaptInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatStaticNaptLocalIp);
    UNUSED_PARAM (i4FsWtpNatStaticNaptStartLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptEndLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptProtocolNumber);
    UNUSED_PARAM (pTestValFsWtpNatStaticNaptDescription);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpNatStaticNaptEntryStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticNaptInterfaceNum
FsWtpNatStaticNaptLocalIp
FsWtpNatStaticNaptStartLocalPort
FsWtpNatStaticNaptEndLocalPort
FsWtpNatStaticNaptProtocolNumber

The Object 
testValFsWtpNatStaticNaptEntryStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpNatStaticNaptEntryStatus (UINT4 *pu4ErrorCode,
                                        UINT4 u4CapwapBaseWtpProfileId,
                                        INT4 i4FsWtpNatStaticNaptInterfaceNum,
                                        UINT4 u4FsWtpNatStaticNaptLocalIp,
                                        INT4 i4FsWtpNatStaticNaptStartLocalPort,
                                        INT4 i4FsWtpNatStaticNaptEndLocalPort,
                                        INT4 i4FsWtpNatStaticNaptProtocolNumber,
                                        INT4
                                        i4TestValFsWtpNatStaticNaptEntryStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatStaticNaptInterfaceNum);
    UNUSED_PARAM (u4FsWtpNatStaticNaptLocalIp);
    UNUSED_PARAM (i4FsWtpNatStaticNaptStartLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptEndLocalPort);
    UNUSED_PARAM (i4FsWtpNatStaticNaptProtocolNumber);
    UNUSED_PARAM (i4TestValFsWtpNatStaticNaptEntryStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsWtpNatStaticNaptTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatStaticNaptInterfaceNum
FsWtpNatStaticNaptLocalIp
FsWtpNatStaticNaptStartLocalPort
FsWtpNatStaticNaptEndLocalPort
FsWtpNatStaticNaptProtocolNumber
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsWtpNatStaticNaptTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWtpNatIfTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsWtpNatIfTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatIfInterfaceNumber
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWtpNatIfTable (UINT4 u4CapwapBaseWtpProfileId,
                                         INT4 i4FsWtpNatIfInterfaceNumber)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatIfInterfaceNumber);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsWtpNatIfTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatIfInterfaceNumber
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsWtpNatIfTable (UINT4 *pu4CapwapBaseWtpProfileId,
                                 INT4 *pi4FsWtpNatIfInterfaceNumber)
{
    UNUSED_PARAM (pu4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pi4FsWtpNatIfInterfaceNumber);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsWtpNatIfTable
Input       :  The Indices
CapwapBaseWtpProfileId
nextCapwapBaseWtpProfileId
FsWtpNatIfInterfaceNumber
nextFsWtpNatIfInterfaceNumber
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsWtpNatIfTable (UINT4 u4CapwapBaseWtpProfileId,
                                UINT4 *pu4NextCapwapBaseWtpProfileId,
                                INT4 i4FsWtpNatIfInterfaceNumber,
                                INT4 *pi4NextFsWtpNatIfInterfaceNumber)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pu4NextCapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatIfInterfaceNumber);
    UNUSED_PARAM (pi4NextFsWtpNatIfInterfaceNumber);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsWtpNatIfNat
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatIfInterfaceNumber

The Object 
retValFsWtpNatIfNat
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpNatIfNat (UINT4 u4CapwapBaseWtpProfileId,
                     INT4 i4FsWtpNatIfInterfaceNumber,
                     INT4 *pi4RetValFsWtpNatIfNat)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatIfInterfaceNumber);
    UNUSED_PARAM (pi4RetValFsWtpNatIfNat);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsWtpNatIfNapt
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatIfInterfaceNumber

The Object 
retValFsWtpNatIfNapt
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpNatIfNapt (UINT4 u4CapwapBaseWtpProfileId,
                      INT4 i4FsWtpNatIfInterfaceNumber,
                      INT4 *pi4RetValFsWtpNatIfNapt)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatIfInterfaceNumber);
    UNUSED_PARAM (pi4RetValFsWtpNatIfNapt);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsWtpNatIfTwoWayNat
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatIfInterfaceNumber

The Object 
retValFsWtpNatIfTwoWayNat
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpNatIfTwoWayNat (UINT4 u4CapwapBaseWtpProfileId,
                           INT4 i4FsWtpNatIfInterfaceNumber,
                           INT4 *pi4RetValFsWtpNatIfTwoWayNat)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatIfInterfaceNumber);
    UNUSED_PARAM (pi4RetValFsWtpNatIfTwoWayNat);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsWtpNatIfEntryStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatIfInterfaceNumber

The Object 
retValFsWtpNatIfEntryStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpNatIfEntryStatus (UINT4 u4CapwapBaseWtpProfileId,
                             INT4 i4FsWtpNatIfInterfaceNumber,
                             INT4 *pi4RetValFsWtpNatIfEntryStatus)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatIfInterfaceNumber);
    UNUSED_PARAM (pi4RetValFsWtpNatIfEntryStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsWtpNatIfNat
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatIfInterfaceNumber

The Object 
setValFsWtpNatIfNat
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpNatIfNat (UINT4 u4CapwapBaseWtpProfileId,
                     INT4 i4FsWtpNatIfInterfaceNumber,
                     INT4 i4SetValFsWtpNatIfNat)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatIfInterfaceNumber);
    UNUSED_PARAM (i4SetValFsWtpNatIfNat);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsWtpNatIfNapt
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatIfInterfaceNumber

The Object 
setValFsWtpNatIfNapt
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpNatIfNapt (UINT4 u4CapwapBaseWtpProfileId,
                      INT4 i4FsWtpNatIfInterfaceNumber,
                      INT4 i4SetValFsWtpNatIfNapt)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatIfInterfaceNumber);
    UNUSED_PARAM (i4SetValFsWtpNatIfNapt);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsWtpNatIfTwoWayNat
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatIfInterfaceNumber

The Object 
setValFsWtpNatIfTwoWayNat
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpNatIfTwoWayNat (UINT4 u4CapwapBaseWtpProfileId,
                           INT4 i4FsWtpNatIfInterfaceNumber,
                           INT4 i4SetValFsWtpNatIfTwoWayNat)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatIfInterfaceNumber);
    UNUSED_PARAM (i4SetValFsWtpNatIfTwoWayNat);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsWtpNatIfEntryStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatIfInterfaceNumber

The Object 
setValFsWtpNatIfEntryStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpNatIfEntryStatus (UINT4 u4CapwapBaseWtpProfileId,
                             INT4 i4FsWtpNatIfInterfaceNumber,
                             INT4 i4SetValFsWtpNatIfEntryStatus)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatIfInterfaceNumber);
    UNUSED_PARAM (i4SetValFsWtpNatIfEntryStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsWtpNatIfNat
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatIfInterfaceNumber

The Object 
testValFsWtpNatIfNat
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpNatIfNat (UINT4 *pu4ErrorCode, UINT4 u4CapwapBaseWtpProfileId,
                        INT4 i4FsWtpNatIfInterfaceNumber,
                        INT4 i4TestValFsWtpNatIfNat)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatIfInterfaceNumber);
    UNUSED_PARAM (i4TestValFsWtpNatIfNat);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpNatIfNapt
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatIfInterfaceNumber

The Object 
testValFsWtpNatIfNapt
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpNatIfNapt (UINT4 *pu4ErrorCode, UINT4 u4CapwapBaseWtpProfileId,
                         INT4 i4FsWtpNatIfInterfaceNumber,
                         INT4 i4TestValFsWtpNatIfNapt)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatIfInterfaceNumber);
    UNUSED_PARAM (i4TestValFsWtpNatIfNapt);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpNatIfTwoWayNat
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatIfInterfaceNumber

The Object 
testValFsWtpNatIfTwoWayNat
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpNatIfTwoWayNat (UINT4 *pu4ErrorCode,
                              UINT4 u4CapwapBaseWtpProfileId,
                              INT4 i4FsWtpNatIfInterfaceNumber,
                              INT4 i4TestValFsWtpNatIfTwoWayNat)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatIfInterfaceNumber);
    UNUSED_PARAM (i4TestValFsWtpNatIfTwoWayNat);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpNatIfEntryStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatIfInterfaceNumber

The Object 
testValFsWtpNatIfEntryStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpNatIfEntryStatus (UINT4 *pu4ErrorCode,
                                UINT4 u4CapwapBaseWtpProfileId,
                                INT4 i4FsWtpNatIfInterfaceNumber,
                                INT4 i4TestValFsWtpNatIfEntryStatus)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpNatIfInterfaceNumber);
    UNUSED_PARAM (i4TestValFsWtpNatIfEntryStatus);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsWtpNatIfTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpNatIfInterfaceNumber
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsWtpNatIfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWtpVlanStaticTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsWtpVlanStaticTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpVlanContextId
FsWtpVlanIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWtpVlanStaticTable (UINT4 u4CapwapBaseWtpProfileId,
                                              INT4 i4FsWtpVlanContextId,
                                              UINT4 u4FsWtpVlanIndex)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpVlanContextId);
    UNUSED_PARAM (u4FsWtpVlanIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsWtpVlanStaticTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpVlanContextId
FsWtpVlanIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsWtpVlanStaticTable (UINT4 *pu4CapwapBaseWtpProfileId,
                                      INT4 *pi4FsWtpVlanContextId,
                                      UINT4 *pu4FsWtpVlanIndex)
{
    UNUSED_PARAM (pu4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pi4FsWtpVlanContextId);
    UNUSED_PARAM (pu4FsWtpVlanIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsWtpVlanStaticTable
Input       :  The Indices
CapwapBaseWtpProfileId
nextCapwapBaseWtpProfileId
FsWtpVlanContextId
nextFsWtpVlanContextId
FsWtpVlanIndex
nextFsWtpVlanIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsWtpVlanStaticTable (UINT4 u4CapwapBaseWtpProfileId,
                                     UINT4 *pu4NextCapwapBaseWtpProfileId,
                                     INT4 i4FsWtpVlanContextId,
                                     INT4 *pi4NextFsWtpVlanContextId,
                                     UINT4 u4FsWtpVlanIndex,
                                     UINT4 *pu4NextFsWtpVlanIndex)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpVlanContextId);
    UNUSED_PARAM (u4FsWtpVlanIndex);
    UNUSED_PARAM (pu4NextCapwapBaseWtpProfileId);
    UNUSED_PARAM (pi4NextFsWtpVlanContextId);
    UNUSED_PARAM (pu4NextFsWtpVlanIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsWtpVlanStaticName
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpVlanContextId
FsWtpVlanIndex

The Object 
retValFsWtpVlanStaticName
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpVlanStaticName (UINT4 u4CapwapBaseWtpProfileId,
                           INT4 i4FsWtpVlanContextId, UINT4 u4FsWtpVlanIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValFsWtpVlanStaticName)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpVlanContextId);
    UNUSED_PARAM (u4FsWtpVlanIndex);
    UNUSED_PARAM (pRetValFsWtpVlanStaticName);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsWtpVlanStaticRowStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpVlanContextId
FsWtpVlanIndex

The Object 
retValFsWtpVlanStaticRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpVlanStaticRowStatus (UINT4 u4CapwapBaseWtpProfileId,
                                INT4 i4FsWtpVlanContextId,
                                UINT4 u4FsWtpVlanIndex,
                                INT4 *pi4RetValFsWtpVlanStaticRowStatus)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpVlanContextId);
    UNUSED_PARAM (u4FsWtpVlanIndex);
    UNUSED_PARAM (pi4RetValFsWtpVlanStaticRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsWtpVlanStaticName
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpVlanContextId
FsWtpVlanIndex

The Object 
setValFsWtpVlanStaticName
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpVlanStaticName (UINT4 u4CapwapBaseWtpProfileId,
                           INT4 i4FsWtpVlanContextId, UINT4 u4FsWtpVlanIndex,
                           tSNMP_OCTET_STRING_TYPE * pSetValFsWtpVlanStaticName)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpVlanContextId);
    UNUSED_PARAM (u4FsWtpVlanIndex);
    UNUSED_PARAM (pSetValFsWtpVlanStaticName);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsWtpVlanStaticRowStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpVlanContextId
FsWtpVlanIndex

The Object 
setValFsWtpVlanStaticRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpVlanStaticRowStatus (UINT4 u4CapwapBaseWtpProfileId,
                                INT4 i4FsWtpVlanContextId,
                                UINT4 u4FsWtpVlanIndex,
                                INT4 i4SetValFsWtpVlanStaticRowStatus)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpVlanContextId);
    UNUSED_PARAM (u4FsWtpVlanIndex);
    UNUSED_PARAM (i4SetValFsWtpVlanStaticRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsWtpVlanStaticName
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpVlanContextId
FsWtpVlanIndex

The Object 
testValFsWtpVlanStaticName
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpVlanStaticName (UINT4 *pu4ErrorCode,
                              UINT4 u4CapwapBaseWtpProfileId,
                              INT4 i4FsWtpVlanContextId, UINT4 u4FsWtpVlanIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValFsWtpVlanStaticName)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpVlanContextId);
    UNUSED_PARAM (u4FsWtpVlanIndex);
    UNUSED_PARAM (pTestValFsWtpVlanStaticName);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsWtpVlanStaticRowStatus
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpVlanContextId
FsWtpVlanIndex

The Object 
testValFsWtpVlanStaticRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpVlanStaticRowStatus (UINT4 *pu4ErrorCode,
                                   UINT4 u4CapwapBaseWtpProfileId,
                                   INT4 i4FsWtpVlanContextId,
                                   UINT4 u4FsWtpVlanIndex,
                                   INT4 i4TestValFsWtpVlanStaticRowStatus)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpVlanContextId);
    UNUSED_PARAM (u4FsWtpVlanIndex);
    UNUSED_PARAM (i4TestValFsWtpVlanStaticRowStatus);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsWtpVlanStaticTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpVlanContextId
FsWtpVlanIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsWtpVlanStaticTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWtpVlanStaticPortConfigTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsWtpVlanStaticPortConfigTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpVlanContextId
FsWtpVlanIndex
FsWtpTpPort
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWtpVlanStaticPortConfigTable (UINT4
                                                        u4CapwapBaseWtpProfileId,
                                                        INT4
                                                        i4FsWtpVlanContextId,
                                                        UINT4 u4FsWtpVlanIndex,
                                                        INT4 i4FsWtpTpPort)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpVlanContextId);
    UNUSED_PARAM (u4FsWtpVlanIndex);
    UNUSED_PARAM (i4FsWtpTpPort);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsWtpVlanStaticPortConfigTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpVlanContextId
FsWtpVlanIndex
FsWtpTpPort
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsWtpVlanStaticPortConfigTable (UINT4
                                                *pu4CapwapBaseWtpProfileId,
                                                INT4 *pi4FsWtpVlanContextId,
                                                UINT4 *pu4FsWtpVlanIndex,
                                                INT4 *pi4FsWtpTpPort)
{
    UNUSED_PARAM (pu4CapwapBaseWtpProfileId);
    UNUSED_PARAM (pi4FsWtpVlanContextId);
    UNUSED_PARAM (pu4FsWtpVlanIndex);
    UNUSED_PARAM (pi4FsWtpTpPort);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsWtpVlanStaticPortConfigTable
Input       :  The Indices
CapwapBaseWtpProfileId
nextCapwapBaseWtpProfileId
FsWtpVlanContextId
nextFsWtpVlanContextId
FsWtpVlanIndex
nextFsWtpVlanIndex
FsWtpTpPort
nextFsWtpTpPort
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsWtpVlanStaticPortConfigTable (UINT4 u4CapwapBaseWtpProfileId,
                                               UINT4
                                               *pu4NextCapwapBaseWtpProfileId,
                                               INT4 i4FsWtpVlanContextId,
                                               INT4 *pi4NextFsWtpVlanContextId,
                                               UINT4 u4FsWtpVlanIndex,
                                               UINT4 *pu4NextFsWtpVlanIndex,
                                               INT4 i4FsWtpTpPort,
                                               INT4 *pi4NextFsWtpTpPort)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpVlanContextId);
    UNUSED_PARAM (u4FsWtpVlanIndex);
    UNUSED_PARAM (i4FsWtpTpPort);
    UNUSED_PARAM (pu4NextCapwapBaseWtpProfileId);
    UNUSED_PARAM (pi4NextFsWtpVlanContextId);
    UNUSED_PARAM (pu4NextFsWtpVlanIndex);
    UNUSED_PARAM (pi4NextFsWtpTpPort);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsWtpVlanStaticPort
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpVlanContextId
FsWtpVlanIndex
FsWtpTpPort

The Object 
retValFsWtpVlanStaticPort
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsWtpVlanStaticPort (UINT4 u4CapwapBaseWtpProfileId,
                           INT4 i4FsWtpVlanContextId, UINT4 u4FsWtpVlanIndex,
                           INT4 i4FsWtpTpPort,
                           INT4 *pi4RetValFsWtpVlanStaticPort)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpVlanContextId);
    UNUSED_PARAM (u4FsWtpVlanIndex);
    UNUSED_PARAM (i4FsWtpTpPort);
    UNUSED_PARAM (pi4RetValFsWtpVlanStaticPort);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsWtpVlanStaticPort
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpVlanContextId
FsWtpVlanIndex
FsWtpTpPort

The Object 
setValFsWtpVlanStaticPort
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsWtpVlanStaticPort (UINT4 u4CapwapBaseWtpProfileId,
                           INT4 i4FsWtpVlanContextId, UINT4 u4FsWtpVlanIndex,
                           INT4 i4FsWtpTpPort, INT4 i4SetValFsWtpVlanStaticPort)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpVlanContextId);
    UNUSED_PARAM (u4FsWtpVlanIndex);
    UNUSED_PARAM (i4FsWtpTpPort);
    UNUSED_PARAM (i4SetValFsWtpVlanStaticPort);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsWtpVlanStaticPort
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpVlanContextId
FsWtpVlanIndex
FsWtpTpPort

The Object 
testValFsWtpVlanStaticPort
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsWtpVlanStaticPort (UINT4 *pu4ErrorCode,
                              UINT4 u4CapwapBaseWtpProfileId,
                              INT4 i4FsWtpVlanContextId, UINT4 u4FsWtpVlanIndex,
                              INT4 i4FsWtpTpPort,
                              INT4 i4TestValFsWtpVlanStaticPort)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpVlanContextId);
    UNUSED_PARAM (u4FsWtpVlanIndex);
    UNUSED_PARAM (i4FsWtpTpPort);
    UNUSED_PARAM (i4TestValFsWtpVlanStaticPort);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsWtpVlanStaticPortConfigTable
Input       :  The Indices
CapwapBaseWtpProfileId
FsWtpVlanContextId
FsWtpVlanIndex
FsWtpTpPort
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsWtpVlanStaticPortConfigTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsWtpIfMainTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWtpIfMainTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWtpIfMainTable (UINT4 u4CapwapBaseWtpProfileId,
                                          INT4 i4FsWtpIfMainIndex)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpIfMainIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsWtpIfMainTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsWtpIfMainTable (UINT4 *pu4CapwapBaseWtpProfileId,
                                  INT4 *pi4FsWtpIfMainIndex)
{
    INT4                i4RetVal = 0;

    i4RetVal = WssIfWtpL3SubIfGetFirstEntry (pu4CapwapBaseWtpProfileId,
                                             (UINT4 *) pi4FsWtpIfMainIndex);

    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsWtpIfMainTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
                nextCapwapBaseWtpProfileId
                FsWtpIfMainIndex
                nextFsWtpIfMainIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsWtpIfMainTable (UINT4 u4CapwapBaseWtpProfileId,
                                 UINT4 *pu4NextCapwapBaseWtpProfileId,
                                 INT4 i4FsWtpIfMainIndex,
                                 INT4 *pi4NextFsWtpIfMainIndex)
{
    INT4                i4RetVal = 0;

    i4RetVal =
        WssIfWtpL3SubIfGetNextEntry (u4CapwapBaseWtpProfileId,
                                     pu4NextCapwapBaseWtpProfileId,
                                     (UINT4) i4FsWtpIfMainIndex,
                                     (UINT4 *) pi4NextFsWtpIfMainIndex);

    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsWtpIfMainType
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                retValFsWtpIfMainType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWtpIfMainType (UINT4 u4CapwapBaseWtpProfileId, INT4 i4FsWtpIfMainIndex,
                       INT4 *pi4RetValFsWtpIfMainType)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;

    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));

    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = (UINT4) i4FsWtpIfMainIndex;

    if (OSIX_SUCCESS == WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry))
    {
        *pi4RetValFsWtpIfMainType = WssIfWtpL3SubIfEntry.i4IfType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsWtpIfMainMtu
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                retValFsWtpIfMainMtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWtpIfMainMtu (UINT4 u4CapwapBaseWtpProfileId, INT4 i4FsWtpIfMainIndex,
                      INT4 *pi4RetValFsWtpIfMainMtu)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpIfMainIndex);
    UNUSED_PARAM (pi4RetValFsWtpIfMainMtu);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWtpIfMainAdminStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                retValFsWtpIfMainAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWtpIfMainAdminStatus (UINT4 u4CapwapBaseWtpProfileId,
                              INT4 i4FsWtpIfMainIndex,
                              INT4 *pi4RetValFsWtpIfMainAdminStatus)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;

    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));

    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = (UINT4) i4FsWtpIfMainIndex;

    if (OSIX_SUCCESS == WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry))
    {
        *pi4RetValFsWtpIfMainAdminStatus = WssIfWtpL3SubIfEntry.u1IfAdminStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsWtpIfMainOperStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                retValFsWtpIfMainOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWtpIfMainOperStatus (UINT4 u4CapwapBaseWtpProfileId,
                             INT4 i4FsWtpIfMainIndex,
                             INT4 *pi4RetValFsWtpIfMainOperStatus)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpIfMainIndex);
    UNUSED_PARAM (pi4RetValFsWtpIfMainOperStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWtpIfMainEncapType
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                retValFsWtpIfMainEncapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWtpIfMainEncapType (UINT4 u4CapwapBaseWtpProfileId,
                            INT4 i4FsWtpIfMainIndex,
                            INT4 *pi4RetValFsWtpIfMainEncapType)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpIfMainIndex);
    UNUSED_PARAM (pi4RetValFsWtpIfMainEncapType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWtpIfMainPhyPort
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                retValFsWtpIfMainPhyPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWtpIfMainPhyPort (UINT4 u4CapwapBaseWtpProfileId,
                          INT4 i4FsWtpIfMainIndex,
                          INT4 *pi4RetValFsWtpIfMainPhyPort)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;

    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));

    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = (UINT4) i4FsWtpIfMainIndex;

    if (OSIX_SUCCESS == WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry))
    {
        /*  get the filter set as a list of names with '&' or ',' operation. */
        *pi4RetValFsWtpIfMainPhyPort = (INT4) WssIfWtpL3SubIfEntry.u4PhyPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsWtpIfMainBrgPortType
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                retValFsWtpIfMainBrgPortType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWtpIfMainBrgPortType (UINT4 u4CapwapBaseWtpProfileId,
                              INT4 i4FsWtpIfMainIndex,
                              INT4 *pi4RetValFsWtpIfMainBrgPortType)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpIfMainIndex);
    UNUSED_PARAM (pi4RetValFsWtpIfMainBrgPortType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWtpIfMainRowStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                retValFsWtpIfMainRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWtpIfMainRowStatus (UINT4 u4CapwapBaseWtpProfileId,
                            INT4 i4FsWtpIfMainIndex,
                            INT4 *pi4RetValFsWtpIfMainRowStatus)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;

    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));

    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = (UINT4) i4FsWtpIfMainIndex;

    if (OSIX_SUCCESS == WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry))
    {
        *pi4RetValFsWtpIfMainRowStatus = WssIfWtpL3SubIfEntry.i4RowStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsWtpIfMainSubType
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                retValFsWtpIfMainSubType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWtpIfMainSubType (UINT4 u4CapwapBaseWtpProfileId,
                          INT4 i4FsWtpIfMainIndex,
                          INT4 *pi4RetValFsWtpIfMainSubType)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpIfMainIndex);
    UNUSED_PARAM (pi4RetValFsWtpIfMainSubType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWtpIfMainNetworkType
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                retValFsWtpIfMainNetworkType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWtpIfMainNetworkType (UINT4 u4CapwapBaseWtpProfileId,
                              INT4 i4FsWtpIfMainIndex,
                              INT4 *pi4RetValFsWtpIfMainNetworkType)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;

    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));

    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = (UINT4) i4FsWtpIfMainIndex;

    if (OSIX_SUCCESS == WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry))
    {
        *pi4RetValFsWtpIfMainNetworkType = WssIfWtpL3SubIfEntry.i4IfNwType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsWtpIfWanType
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                retValFsWtpIfWanType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWtpIfWanType (UINT4 u4CapwapBaseWtpProfileId, INT4 i4FsWtpIfMainIndex,
                      INT4 *pi4RetValFsWtpIfWanType)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpIfMainIndex);
    UNUSED_PARAM (pi4RetValFsWtpIfWanType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWtpIfMainEncapDot1qVlanId
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                retValFsWtpIfMainEncapDot1qVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWtpIfMainEncapDot1qVlanId (UINT4 u4CapwapBaseWtpProfileId,
                                   INT4 i4FsWtpIfMainIndex,
                                   INT4 *pi4RetValFsWtpIfMainEncapDot1qVlanId)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;

    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));

    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = (UINT4) i4FsWtpIfMainIndex;

    if (OSIX_SUCCESS == WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry))
    {
        *pi4RetValFsWtpIfMainEncapDot1qVlanId =
            (INT4) WssIfWtpL3SubIfEntry.u2VlanId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsWtpIfMainType
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                setValFsWtpIfMainType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWtpIfMainType (UINT4 u4CapwapBaseWtpProfileId, INT4 i4FsWtpIfMainIndex,
                       INT4 i4SetValFsWtpIfMainType)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;
    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));
    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = (UINT4) i4FsWtpIfMainIndex;
    if (WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry) == OSIX_SUCCESS)
    {
        WssIfWtpL3SubIfEntry.i4IfType = i4SetValFsWtpIfMainType;
        if (WssIfWtpL3SubIfSetEntry (&WssIfWtpL3SubIfEntry) == OSIX_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsWtpIfMainMtu
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                setValFsWtpIfMainMtu
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWtpIfMainMtu (UINT4 u4CapwapBaseWtpProfileId, INT4 i4FsWtpIfMainIndex,
                      INT4 i4SetValFsWtpIfMainMtu)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpIfMainIndex);
    UNUSED_PARAM (i4SetValFsWtpIfMainMtu);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsWtpIfMainAdminStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                setValFsWtpIfMainAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWtpIfMainAdminStatus (UINT4 u4CapwapBaseWtpProfileId,
                              INT4 i4FsWtpIfMainIndex,
                              INT4 i4SetValFsWtpIfMainAdminStatus)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;
    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));
    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = (UINT4) i4FsWtpIfMainIndex;
    if (WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry) == OSIX_SUCCESS)
    {
        WssIfWtpL3SubIfEntry.u1IfAdminStatus =
            (UINT1) i4SetValFsWtpIfMainAdminStatus;
        if (WssIfWtpL3SubIfSetEntry (&WssIfWtpL3SubIfEntry) == OSIX_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsWtpIfMainEncapType
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                setValFsWtpIfMainEncapType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWtpIfMainEncapType (UINT4 u4CapwapBaseWtpProfileId,
                            INT4 i4FsWtpIfMainIndex,
                            INT4 i4SetValFsWtpIfMainEncapType)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpIfMainIndex);
    UNUSED_PARAM (i4SetValFsWtpIfMainEncapType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsWtpIfMainPhyPort
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                setValFsWtpIfMainPhyPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWtpIfMainPhyPort (UINT4 u4CapwapBaseWtpProfileId,
                          INT4 i4FsWtpIfMainIndex,
                          INT4 i4SetValFsWtpIfMainPhyPort)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;
    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));
    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = (UINT4) i4FsWtpIfMainIndex;

    if (WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry) == OSIX_SUCCESS)
    {
        WssIfWtpL3SubIfEntry.u4PhyPort = (UINT4) i4SetValFsWtpIfMainPhyPort;
        if (WssIfWtpL3SubIfSetEntry (&WssIfWtpL3SubIfEntry) == OSIX_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsWtpIfMainBrgPortType
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                setValFsWtpIfMainBrgPortType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWtpIfMainBrgPortType (UINT4 u4CapwapBaseWtpProfileId,
                              INT4 i4FsWtpIfMainIndex,
                              INT4 i4SetValFsWtpIfMainBrgPortType)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpIfMainIndex);
    UNUSED_PARAM (i4SetValFsWtpIfMainBrgPortType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsWtpIfMainRowStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                setValFsWtpIfMainRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWtpIfMainRowStatus (UINT4 u4CapwapBaseWtpProfileId,
                            INT4 i4FsWtpIfMainIndex,
                            INT4 i4SetValFsWtpIfMainRowStatus)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;

    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));
    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = (UINT4) i4FsWtpIfMainIndex;

    switch (i4SetValFsWtpIfMainRowStatus)
    {
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
            WssIfWtpL3SubIfEntry.i4RowStatus = i4SetValFsWtpIfMainRowStatus;

            if (WssIfWtpL3SubIfAddEntry (&WssIfWtpL3SubIfEntry) != OSIX_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            break;
        case ACTIVE:
            if (WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry) == OSIX_SUCCESS)
            {
                WssIfWtpL3SubIfEntry.i4RowStatus = i4SetValFsWtpIfMainRowStatus;
                /* Send Update Req to add the L3 SUB IFACE entry on WTP */
                WssIfL3SubIfUpdateReq (&WssIfWtpL3SubIfEntry);

                if (WssIfWtpL3SubIfSetEntry (&WssIfWtpL3SubIfEntry) !=
                    OSIX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
            break;

        case NOT_IN_SERVICE:
            if (WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry) == OSIX_SUCCESS)
            {
                WssIfWtpL3SubIfEntry.i4RowStatus = i4SetValFsWtpIfMainRowStatus;

                if (WssIfWtpL3SubIfSetEntry (&WssIfWtpL3SubIfEntry) !=
                    OSIX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
            break;

        case DESTROY:
            if (WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry) == OSIX_SUCCESS)
            {
                WssIfWtpL3SubIfEntry.i4RowStatus = i4SetValFsWtpIfMainRowStatus;
                /* Send Update Req to delete the L3 SUB IFACE entry on WTP */
                WssIfL3SubIfUpdateReq (&WssIfWtpL3SubIfEntry);

                if (WssIfWtpL3SubIfDeleteEntry (&WssIfWtpL3SubIfEntry) !=
                    OSIX_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
            break;
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsWtpIfMainSubType
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                setValFsWtpIfMainSubType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWtpIfMainSubType (UINT4 u4CapwapBaseWtpProfileId,
                          INT4 i4FsWtpIfMainIndex,
                          INT4 i4SetValFsWtpIfMainSubType)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpIfMainIndex);
    UNUSED_PARAM (i4SetValFsWtpIfMainSubType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsWtpIfMainNetworkType
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                setValFsWtpIfMainNetworkType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWtpIfMainNetworkType (UINT4 u4CapwapBaseWtpProfileId,
                              INT4 i4FsWtpIfMainIndex,
                              INT4 i4SetValFsWtpIfMainNetworkType)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;
    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));
    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = (UINT4) i4FsWtpIfMainIndex;

    if (WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry) == OSIX_SUCCESS)
    {
        WssIfWtpL3SubIfEntry.i4IfNwType = i4SetValFsWtpIfMainNetworkType;
        if (WssIfWtpL3SubIfSetEntry (&WssIfWtpL3SubIfEntry) == OSIX_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsWtpIfWanType
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                setValFsWtpIfWanType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWtpIfWanType (UINT4 u4CapwapBaseWtpProfileId, INT4 i4FsWtpIfMainIndex,
                      INT4 i4SetValFsWtpIfWanType)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpIfMainIndex);
    UNUSED_PARAM (i4SetValFsWtpIfWanType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsWtpIfMainEncapDot1qVlanId
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                setValFsWtpIfMainEncapDot1qVlanId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWtpIfMainEncapDot1qVlanId (UINT4 u4CapwapBaseWtpProfileId,
                                   INT4 i4FsWtpIfMainIndex,
                                   INT4 i4SetValFsWtpIfMainEncapDot1qVlanId)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;
    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));
    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = (UINT4) i4FsWtpIfMainIndex;

    if (WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry) == OSIX_SUCCESS)
    {
        WssIfWtpL3SubIfEntry.u2VlanId =
            (UINT2) i4SetValFsWtpIfMainEncapDot1qVlanId;
        if (WssIfWtpL3SubIfSetEntry (&WssIfWtpL3SubIfEntry) == OSIX_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsWtpIfMainType
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                testValFsWtpIfMainType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWtpIfMainType (UINT4 *pu4ErrorCode, UINT4 u4CapwapBaseWtpProfileId,
                          INT4 i4FsWtpIfMainIndex,
                          INT4 i4TestValFsWtpIfMainType)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;
    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));

    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = i4FsWtpIfMainIndex;

    if (WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry) == OSIX_FAILURE)
    {
        CLI_SET_ERR (CLI_WSS_L3SUBIF_NO_CREATION);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (WssIfWtpL3SubIfEntry.i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Curerntly L3 SUB IF only supported */
    if (i4TestValFsWtpIfMainType != AP_L3SUBIF)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWtpIfMainMtu
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                testValFsWtpIfMainMtu
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWtpIfMainMtu (UINT4 *pu4ErrorCode, UINT4 u4CapwapBaseWtpProfileId,
                         INT4 i4FsWtpIfMainIndex, INT4 i4TestValFsWtpIfMainMtu)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpIfMainIndex);
    UNUSED_PARAM (i4TestValFsWtpIfMainMtu);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsWtpIfMainAdminStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                testValFsWtpIfMainAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWtpIfMainAdminStatus (UINT4 *pu4ErrorCode,
                                 UINT4 u4CapwapBaseWtpProfileId,
                                 INT4 i4FsWtpIfMainIndex,
                                 INT4 i4TestValFsWtpIfMainAdminStatus)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;
    UNUSED_PARAM (nmhTestv2FsWtpIfMainAdminStatus);

    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));

    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = i4FsWtpIfMainIndex;

    if (WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry) == OSIX_FAILURE)
    {
        CLI_SET_ERR (CLI_WSS_L3SUBIF_NO_CREATION);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (WssIfWtpL3SubIfEntry.i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsWtpIfMainAdminStatus != AP_IF_UP) &&
        (i4TestValFsWtpIfMainAdminStatus != AP_IF_DOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsWtpIfMainPhyPort
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                testValFsWtpIfMainAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWtpIfMainPhyPort (UINT4 *pu4ErrorCode,
                             UINT4 u4CapwapBaseWtpProfileId,
                             INT4 i4FsWtpIfMainIndex,
                             INT4 i4TestValFsWtpIfMainPhyPort)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;

    UNUSED_PARAM (i4TestValFsWtpIfMainPhyPort);
    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));

    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = i4FsWtpIfMainIndex;

    if (WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry) == OSIX_FAILURE)
    {
        CLI_SET_ERR (CLI_WSS_L3SUBIF_NO_CREATION);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (WssIfWtpL3SubIfEntry.i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Currently phy port range is given from 1 to 10. This needs to be
     * changed based on the AP Interface configuration */
    if ((i4TestValFsWtpIfMainPhyPort < AP_L3PHY_PORT_MIN) ||
        (i4TestValFsWtpIfMainPhyPort > AP_L3PHY_PORT_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWtpIfMainEncapType
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                testValFsWtpIfMainEncapType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWtpIfMainEncapType (UINT4 *pu4ErrorCode,
                               UINT4 u4CapwapBaseWtpProfileId,
                               INT4 i4FsWtpIfMainIndex,
                               INT4 i4TestValFsWtpIfMainEncapType)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpIfMainIndex);
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsWtpIfMainEncapType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWtpIfMainBrgPortType
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                testValFsWtpIfMainBrgPortType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWtpIfMainBrgPortType (UINT4 *pu4ErrorCode,
                                 UINT4 u4CapwapBaseWtpProfileId,
                                 INT4 i4FsWtpIfMainIndex,
                                 INT4 i4TestValFsWtpIfMainBrgPortType)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpIfMainIndex);
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsWtpIfMainBrgPortType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWtpIfMainRowStatus
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                testValFsWtpIfMainRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWtpIfMainRowStatus (UINT4 *pu4ErrorCode,
                               UINT4 u4CapwapBaseWtpProfileId,
                               INT4 i4FsWtpIfMainIndex,
                               INT4 i4TestValFsWtpIfMainRowStatus)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;
    tWssIfWtpL3SubIfDB *pWssIfWtpL3SubIfEntry = &WssIfWtpL3SubIfEntry;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));

    CAPWAP_IF_CAP_DB_ALLOC (pWssIfCapwapDB);
    if (pWssIfCapwapDB == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4CapwapBaseWtpProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID,
                                 pWssIfCapwapDB) != OSIX_SUCCESS)
    {
        CLI_SET_ERR (CLI_WSS_L3SUBIF_NO_CREATION);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
        return SNMP_FAILURE;
    }

    pWssIfWtpL3SubIfEntry->u4WtpProfileId = u4CapwapBaseWtpProfileId;
    pWssIfWtpL3SubIfEntry->u4IfIndex = (UINT4) i4FsWtpIfMainIndex;
    if (WssIfWtpL3SubIfGetEntry (pWssIfWtpL3SubIfEntry) != OSIX_FAILURE)
    {
        if ((i4TestValFsWtpIfMainRowStatus == ACTIVE) ||
            (i4TestValFsWtpIfMainRowStatus == DESTROY) ||
            (i4TestValFsWtpIfMainRowStatus == NOT_IN_SERVICE))
        {
            CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
            return SNMP_SUCCESS;
        }
    }
    else
    {
        if ((i4TestValFsWtpIfMainRowStatus == CREATE_AND_WAIT) ||
            (i4TestValFsWtpIfMainRowStatus == CREATE_AND_GO))
        {
            CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    CAPWAP_IF_CAP_DB_RELEASE (pWssIfCapwapDB);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsWtpIfMainSubType
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                testValFsWtpIfMainSubType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWtpIfMainSubType (UINT4 *pu4ErrorCode,
                             UINT4 u4CapwapBaseWtpProfileId,
                             INT4 i4FsWtpIfMainIndex,
                             INT4 i4TestValFsWtpIfMainSubType)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpIfMainIndex);
    UNUSED_PARAM (i4TestValFsWtpIfMainSubType);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWtpIfMainNetworkType
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                testValFsWtpIfMainNetworkType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWtpIfMainNetworkType (UINT4 *pu4ErrorCode,
                                 UINT4 u4CapwapBaseWtpProfileId,
                                 INT4 i4FsWtpIfMainIndex,
                                 INT4 i4TestValFsWtpIfMainNetworkType)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;
    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));

    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = i4FsWtpIfMainIndex;

    if (WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry) == OSIX_FAILURE)
    {
        CLI_SET_ERR (CLI_WSS_L3SUBIF_NO_CREATION);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (WssIfWtpL3SubIfEntry.i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (WssIfWtpL3SubIfEntry.u1IfAdminStatus == 1)
    {
        CLI_SET_ERR (CLI_WSS_IF_ADMIN_STATUS_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsWtpIfMainNetworkType != NETWORK_TYPE_WAN) &&
        (i4TestValFsWtpIfMainNetworkType != NETWORK_TYPE_LAN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWtpIfWanType
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                testValFsWtpIfWanType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWtpIfWanType (UINT4 *pu4ErrorCode, UINT4 u4CapwapBaseWtpProfileId,
                         INT4 i4FsWtpIfMainIndex, INT4 i4TestValFsWtpIfWanType)
{
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (i4FsWtpIfMainIndex);
    UNUSED_PARAM (i4TestValFsWtpIfWanType);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWtpIfMainEncapDot1qVlanId
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                testValFsWtpIfMainEncapDot1qVlanId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWtpIfMainEncapDot1qVlanId (UINT4 *pu4ErrorCode,
                                      UINT4 u4CapwapBaseWtpProfileId,
                                      INT4 i4FsWtpIfMainIndex,
                                      INT4 i4TestValFsWtpIfMainEncapDot1qVlanId)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;
    tWssIfWtpL3SubIfDB *pWssIfWtpL3SubIfEntry = &WssIfWtpL3SubIfEntry;

    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));

    pWssIfWtpL3SubIfEntry->u4WtpProfileId = u4CapwapBaseWtpProfileId;
    pWssIfWtpL3SubIfEntry->u4IfIndex = i4FsWtpIfMainIndex;

    if (WssIfWtpL3SubIfGetEntry (pWssIfWtpL3SubIfEntry) == OSIX_FAILURE)
    {
        CLI_SET_ERR (CLI_WSS_L3SUBIF_NO_CREATION);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pWssIfWtpL3SubIfEntry->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsWtpIfMainEncapDot1qVlanId < 1) ||
        (i4TestValFsWtpIfMainEncapDot1qVlanId > VLAN_MAX_VLAN_ID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsWtpIfMainTable
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsWtpIfMainTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsWtpIfIpAddr
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                retValFsWtpIfIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWtpIfIpAddr (UINT4 u4CapwapBaseWtpProfileId, INT4 i4FsWtpIfMainIndex,
                     UINT4 *pu4RetValFsWtpIfIpAddr)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;
    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));
    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = (UINT4) i4FsWtpIfMainIndex;

    if (WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry) == OSIX_SUCCESS)
    {
        *pu4RetValFsWtpIfIpAddr = WssIfWtpL3SubIfEntry.u4IpAddr;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsWtpIfIpSubnetMask
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                retValFsWtpIfIpSubnetMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWtpIfIpSubnetMask (UINT4 u4CapwapBaseWtpProfileId,
                           INT4 i4FsWtpIfMainIndex,
                           UINT4 *pu4RetValFsWtpIfIpSubnetMask)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;
    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));
    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = (UINT4) i4FsWtpIfMainIndex;

    if (WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry) == OSIX_SUCCESS)
    {
        *pu4RetValFsWtpIfIpSubnetMask = WssIfWtpL3SubIfEntry.u4SubnetMask;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsWtpIfIpBroadcastAddr
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                retValFsWtpIfIpBroadcastAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsWtpIfIpBroadcastAddr (UINT4 u4CapwapBaseWtpProfileId,
                              INT4 i4FsWtpIfMainIndex,
                              UINT4 *pu4RetValFsWtpIfIpBroadcastAddr)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;
    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));
    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = (UINT4) i4FsWtpIfMainIndex;

    if (WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry) == OSIX_SUCCESS)
    {
        *pu4RetValFsWtpIfIpBroadcastAddr = WssIfWtpL3SubIfEntry.u4BroadcastAddr;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsWtpIfIpAddr
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                setValFsWtpIfIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWtpIfIpAddr (UINT4 u4CapwapBaseWtpProfileId, INT4 i4FsWtpIfMainIndex,
                     UINT4 u4SetValFsWtpIfIpAddr)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;
    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));
    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = (UINT4) i4FsWtpIfMainIndex;

    if (WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry) == OSIX_SUCCESS)
    {
        WssIfWtpL3SubIfEntry.u4IpAddr = u4SetValFsWtpIfIpAddr;
        if (WssIfWtpL3SubIfSetEntry (&WssIfWtpL3SubIfEntry) == OSIX_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsWtpIfIpSubnetMask
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                setValFsWtpIfIpSubnetMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWtpIfIpSubnetMask (UINT4 u4CapwapBaseWtpProfileId,
                           INT4 i4FsWtpIfMainIndex,
                           UINT4 u4SetValFsWtpIfIpSubnetMask)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;
    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));
    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = (UINT4) i4FsWtpIfMainIndex;

    if (WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry) == OSIX_SUCCESS)
    {
        WssIfWtpL3SubIfEntry.u4SubnetMask = u4SetValFsWtpIfIpSubnetMask;
        if (WssIfWtpL3SubIfSetEntry (&WssIfWtpL3SubIfEntry) == OSIX_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsWtpIfIpBroadcastAddr
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                setValFsWtpIfIpBroadcastAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsWtpIfIpBroadcastAddr (UINT4 u4CapwapBaseWtpProfileId,
                              INT4 i4FsWtpIfMainIndex,
                              UINT4 u4SetValFsWtpIfIpBroadcastAddr)
{
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;
    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));
    WssIfWtpL3SubIfEntry.u4WtpProfileId = u4CapwapBaseWtpProfileId;
    WssIfWtpL3SubIfEntry.u4IfIndex = (UINT4) i4FsWtpIfMainIndex;

    if (WssIfWtpL3SubIfGetEntry (&WssIfWtpL3SubIfEntry) == OSIX_SUCCESS)
    {
        WssIfWtpL3SubIfEntry.u4BroadcastAddr = u4SetValFsWtpIfIpBroadcastAddr;
        if (WssIfWtpL3SubIfSetEntry (&WssIfWtpL3SubIfEntry) == OSIX_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsWtpIfIpAddr
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                testValFsWtpIfIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWtpIfIpAddr (UINT4 *pu4ErrorCode, UINT4 u4CapwapBaseWtpProfileId,
                        INT4 i4FsWtpIfMainIndex, UINT4 u4TestValFsWtpIfIpAddr)
{
    UINT4               u4BroadcastAddr = 0;
    UINT4               u4NetworkIpAddr = 0;
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;
    tWssIfWtpL3SubIfDB *pWssIfWtpL3SubIfEntry = &WssIfWtpL3SubIfEntry;

    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));

    pWssIfWtpL3SubIfEntry->u4WtpProfileId = u4CapwapBaseWtpProfileId;
    pWssIfWtpL3SubIfEntry->u4IfIndex = i4FsWtpIfMainIndex;

    if (WssIfWtpL3SubIfGetEntry (pWssIfWtpL3SubIfEntry) == OSIX_FAILURE)
    {
        CLI_SET_ERR (CLI_WSS_L3SUBIF_NO_CREATION);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pWssIfWtpL3SubIfEntry->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4TestValFsWtpIfIpAddr != 0)
    {
        if (CFA_IS_LOOPBACK (u4TestValFsWtpIfIpAddr))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_WSS_LOOPBACK_IPADDR);
            return SNMP_FAILURE;
        }
        if (!((CFA_IS_ADDR_CLASS_A (u4TestValFsWtpIfIpAddr) ?
               (IP_IS_ZERO_NETWORK (u4TestValFsWtpIfIpAddr) ? 0 : 1) : 0) ||
              (CFA_IS_ADDR_CLASS_B (u4TestValFsWtpIfIpAddr)) ||
              (CFA_IS_ADDR_CLASS_C (u4TestValFsWtpIfIpAddr)) ||
              (CFA_IS_ADDR_CLASS_E (u4TestValFsWtpIfIpAddr))))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_WSS_INVALID_IP_ADDR);
            return SNMP_FAILURE;
        }
    }

    /*when netmask is 31 bit mask we need NOT to check ip address with  broadcast address */
    if ((pWssIfWtpL3SubIfEntry->u4SubnetMask != 0xffffffff)
        && (pWssIfWtpL3SubIfEntry->u4SubnetMask != IP_ADDR_31BIT_MASK))
    {
        u4BroadcastAddr = u4TestValFsWtpIfIpAddr |
            (~(pWssIfWtpL3SubIfEntry->u4SubnetMask));
        if (u4TestValFsWtpIfIpAddr == u4BroadcastAddr)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_WSS_INVALID_IP_ADDR);
            return SNMP_FAILURE;
        }

        /* ipaddress should not be Network Address */
        u4NetworkIpAddr = u4TestValFsWtpIfIpAddr &
            (~(pWssIfWtpL3SubIfEntry->u4SubnetMask));
        if (u4NetworkIpAddr == 0)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_WSS_INVALID_IP_ADDR);
            return SNMP_FAILURE;
        }
    }

    /* Validate the IP with Subnet configured, if any */
    if (FsWssCliValidateIfIpSubnet (u4CapwapBaseWtpProfileId,
                                    (UINT4) i4FsWtpIfMainIndex,
                                    u4TestValFsWtpIfIpAddr,
                                    pWssIfWtpL3SubIfEntry->u4SubnetMask) ==
        CLI_FAILURE)
    {
        CLI_SET_ERR (CLI_WSS_IP_ADDR_EXISTS);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWtpIfIpSubnetMask
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                testValFsWtpIfIpSubnetMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWtpIfIpSubnetMask (UINT4 *pu4ErrorCode,
                              UINT4 u4CapwapBaseWtpProfileId,
                              INT4 i4FsWtpIfMainIndex,
                              UINT4 u4TestValFsWtpIfIpSubnetMask)
{
    UINT4               u4BroadCastAddr = 0;
    UINT4               u4NetworkIpAddr = 0;
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;
    tWssIfWtpL3SubIfDB *pWssIfWtpL3SubIfEntry = &WssIfWtpL3SubIfEntry;

    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));

    pWssIfWtpL3SubIfEntry->u4WtpProfileId = u4CapwapBaseWtpProfileId;
    pWssIfWtpL3SubIfEntry->u4IfIndex = i4FsWtpIfMainIndex;

    if (WssIfWtpL3SubIfGetEntry (pWssIfWtpL3SubIfEntry) == OSIX_FAILURE)
    {
        CLI_SET_ERR (CLI_WSS_L3SUBIF_NO_CREATION);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pWssIfWtpL3SubIfEntry->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pWssIfWtpL3SubIfEntry->u4IpAddr == 0) ||
        (pWssIfWtpL3SubIfEntry->u4SubnetMask == u4TestValFsWtpIfIpSubnetMask))
    {
        return SNMP_SUCCESS;
    }

    if (u4TestValFsWtpIfIpSubnetMask == 0)
    {
        if (pWssIfWtpL3SubIfEntry->u4IpAddr == 0)
        {
            /* Un-numbered interface case */
            return SNMP_SUCCESS;
        }
        else
        {
            /* Valid IP and 0.0.0.0 mask */
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (pWssIfWtpL3SubIfEntry->u4IpAddr == 0)
        {
            /* 0.0.0.0 IP valid mask */
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (u4TestValFsWtpIfIpSubnetMask != 0xffffffff)
    {
        /* ipaddress should not be Broadcast Address */
        u4BroadCastAddr = pWssIfWtpL3SubIfEntry->u4IpAddr |
            (~(u4TestValFsWtpIfIpSubnetMask));
        if (pWssIfWtpL3SubIfEntry->u4IpAddr == u4BroadCastAddr)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        /* ipaddress should not be Network Address */
        u4NetworkIpAddr = pWssIfWtpL3SubIfEntry->u4IpAddr &
            (~(u4TestValFsWtpIfIpSubnetMask));
        if (u4NetworkIpAddr == 0)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    /* Validate the IP with Subnet configured, if any */
    if (FsWssCliValidateIfIpSubnet (u4CapwapBaseWtpProfileId,
                                    (UINT4) i4FsWtpIfMainIndex,
                                    pWssIfWtpL3SubIfEntry->u4IpAddr,
                                    u4TestValFsWtpIfIpSubnetMask) ==
        CLI_FAILURE)
    {
        CLI_SET_ERR (CLI_WSS_IP_ADDR_EXISTS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsWtpIfIpBroadcastAddr
 Input       :  The Indices
                CapwapBaseWtpProfileId
                FsWtpIfMainIndex

                The Object 
                testValFsWtpIfIpBroadcastAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsWtpIfIpBroadcastAddr (UINT4 *pu4ErrorCode,
                                 UINT4 u4CapwapBaseWtpProfileId,
                                 INT4 i4FsWtpIfMainIndex,
                                 UINT4 u4TestValFsWtpIfIpBroadcastAddr)
{
    UINT4               u4BrdCstAddr = 0;
    tWssIfWtpL3SubIfDB  WssIfWtpL3SubIfEntry;
    tWssIfWtpL3SubIfDB *pWssIfWtpL3SubIfEntry = &WssIfWtpL3SubIfEntry;

    MEMSET (&WssIfWtpL3SubIfEntry, 0, sizeof (tWssIfWtpL3SubIfDB));

    pWssIfWtpL3SubIfEntry->u4WtpProfileId = u4CapwapBaseWtpProfileId;
    pWssIfWtpL3SubIfEntry->u4IfIndex = i4FsWtpIfMainIndex;

    if (WssIfWtpL3SubIfGetEntry (pWssIfWtpL3SubIfEntry) == OSIX_FAILURE)
    {
        CLI_SET_ERR (CLI_WSS_L3SUBIF_NO_CREATION);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pWssIfWtpL3SubIfEntry->i4RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pWssIfWtpL3SubIfEntry->u4IpAddr == 0) ||
        (pWssIfWtpL3SubIfEntry->u4BroadcastAddr ==
         u4TestValFsWtpIfIpBroadcastAddr))
    {
        return SNMP_SUCCESS;
    }

    if (!((CFA_IS_ADDR_CLASS_A (u4TestValFsWtpIfIpBroadcastAddr)) ||
          (CFA_IS_ADDR_CLASS_B (u4TestValFsWtpIfIpBroadcastAddr)) ||
          (CFA_IS_ADDR_CLASS_C (u4TestValFsWtpIfIpBroadcastAddr)) ||
          (CFA_IS_ADDR_CLASS_E (u4TestValFsWtpIfIpBroadcastAddr))))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u4BrdCstAddr =
        (pWssIfWtpL3SubIfEntry->
         u4IpAddr) | (~(pWssIfWtpL3SubIfEntry->u4SubnetMask));

    if (u4BrdCstAddr != u4TestValFsWtpIfIpBroadcastAddr)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
