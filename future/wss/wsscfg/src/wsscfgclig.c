#ifndef __WSSCFGCLIG_C__
#define __WSSCFGCLIG_C__
/********************************************************************
* Copyright (C) 2013  Aricent Inc . All Rights Reserved
*
* $Id: wsscfgclig.c,v 1.8.2.1 2018/03/19 14:21:09 siva Exp $
*
* Description: This file contains the Wsscfg CLI related routines 
*********************************************************************/

#include "wsscfginc.h"
#include "capwapclilwg.h"
#include "wsscfgcli.h"
#include "capwapcli.h"
#include "cli.h"
/*#include "wssstawlcprot.h"*/
#include "tftpc.h"
#include "wssifpmdb.h"
#include "wsscfgprotg.h"
#include "fs11aclw.h"

extern tWssClientSummary gaWssClientSummary[MAX_STA_SUPP_PER_WLC];
extern UINT4        gu4ClientWalkIndex;

/****************************************************************************
 * Function    :  cli_process_Wsscfg_cmd
 * Description :  This function is exported to CLI module to handle the
                WSSCFG cli commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
cli_process_Wsscfg_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4               u4Count = 0;
    UINT4              *args[WSSCFG_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4              *u4NullPtr[] = { NULL };
#ifdef WPA_WANTED
    UINT4               u4ProfileId = 0;
    UINT4               u4WlanId = 0;
    INT4                i4WpaEnable = 0;
#endif
    UINT4               u4ErrCode = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT4               u4CmdType = 0;
    INT4                i4Inst;
    INT4                i4EnteredRadioType = 0;
    INT4                i4FsSecurityWebAuthType = 0;
    UINT4               Ipaddress = 0;
    UINT4              *pIpaddress = NULL;
    UINT4               u4FileLen = 0;
    UINT4              *pu4FileLen = NULL;
    /*  UINT1 *pu1DstImageName; */
    UINT4               u4currentProfileId;
    UINT1               au1WtpName[256];
    UINT1               au1ProfName[256];
    UINT4               u4WtpNameLen = 0;
    tMacAddr            BaseMacAddr;
    MEMSET (BaseMacAddr, 0, sizeof (tMacAddr));
    INT4                i4ApGroupEnabledStatus = 0;
    INT4                i4rowStatus = 0;
    UINT4               u4ApGroupId = 0;
    tSNMP_OCTET_STRING_TYPE ApGroupName;
    tSNMP_OCTET_STRING_TYPE ApGroupNameDesc;

    tWsscfgDot11StationConfigEntry WsscfgSetDot11StationConfigEntry;
    tWsscfgIsSetDot11StationConfigEntry WsscfgIsSetDot11StationConfigEntry;

    tWsscfgDot11AuthenticationAlgorithmsEntry
        WsscfgSetDot11AuthenticationAlgorithmsEntry;
    tWsscfgIsSetDot11AuthenticationAlgorithmsEntry
        WsscfgIsSetDot11AuthenticationAlgorithmsEntry;

    tWsscfgDot11WEPDefaultKeysEntry WsscfgSetDot11WEPDefaultKeysEntry;
    tWsscfgIsSetDot11WEPDefaultKeysEntry WsscfgIsSetDot11WEPDefaultKeysEntry;

    tWsscfgDot11WEPKeyMappingsEntry WsscfgSetDot11WEPKeyMappingsEntry;
    tWsscfgIsSetDot11WEPKeyMappingsEntry WsscfgIsSetDot11WEPKeyMappingsEntry;

    tWsscfgDot11PrivacyEntry WsscfgSetDot11PrivacyEntry;
    tWsscfgIsSetDot11PrivacyEntry WsscfgIsSetDot11PrivacyEntry;

    tWsscfgDot11MultiDomainCapabilityEntry
        WsscfgSetDot11MultiDomainCapabilityEntry;
    tWsscfgIsSetDot11MultiDomainCapabilityEntry
        WsscfgIsSetDot11MultiDomainCapabilityEntry;

    tWsscfgDot11SpectrumManagementEntry WsscfgSetDot11SpectrumManagementEntry;
    tWsscfgIsSetDot11SpectrumManagementEntry
        WsscfgIsSetDot11SpectrumManagementEntry;

    tWsscfgDot11RegulatoryClassesEntry WsscfgSetDot11RegulatoryClassesEntry;
    tWsscfgIsSetDot11RegulatoryClassesEntry
        WsscfgIsSetDot11RegulatoryClassesEntry;

    tWsscfgDot11OperationEntry WsscfgSetDot11OperationEntry;
    tWsscfgIsSetDot11OperationEntry WsscfgIsSetDot11OperationEntry;

    tWsscfgDot11GroupAddressesEntry WsscfgSetDot11GroupAddressesEntry;
    tWsscfgIsSetDot11GroupAddressesEntry WsscfgIsSetDot11GroupAddressesEntry;

    tWsscfgDot11EDCAEntry WsscfgSetDot11EDCAEntry;
    tWsscfgIsSetDot11EDCAEntry WsscfgIsSetDot11EDCAEntry;

    tWsscfgDot11QAPEDCAEntry WsscfgSetDot11QAPEDCAEntry;
    tWsscfgIsSetDot11QAPEDCAEntry WsscfgIsSetDot11QAPEDCAEntry;

    tWsscfgDot11PhyOperationEntry WsscfgSetDot11PhyOperationEntry;
    tWsscfgIsSetDot11PhyOperationEntry WsscfgIsSetDot11PhyOperationEntry;

    tWsscfgDot11PhyAntennaEntry WsscfgSetDot11PhyAntennaEntry;
    tWsscfgIsSetDot11PhyAntennaEntry WsscfgIsSetDot11PhyAntennaEntry;

    tWsscfgDot11PhyTxPowerEntry WsscfgSetDot11PhyTxPowerEntry;
    tWsscfgIsSetDot11PhyTxPowerEntry WsscfgIsSetDot11PhyTxPowerEntry;

    tWsscfgDot11PhyFHSSEntry WsscfgSetDot11PhyFHSSEntry;
    tWsscfgIsSetDot11PhyFHSSEntry WsscfgIsSetDot11PhyFHSSEntry;

    tWsscfgDot11PhyDSSSEntry WsscfgSetDot11PhyDSSSEntry;
    tWsscfgIsSetDot11PhyDSSSEntry WsscfgIsSetDot11PhyDSSSEntry;

    tWsscfgDot11PhyIREntry WsscfgSetDot11PhyIREntry;
    tWsscfgIsSetDot11PhyIREntry WsscfgIsSetDot11PhyIREntry;

    tWsscfgDot11AntennasListEntry WsscfgSetDot11AntennasListEntry;
    tWsscfgIsSetDot11AntennasListEntry WsscfgIsSetDot11AntennasListEntry;

    tWsscfgDot11PhyOFDMEntry WsscfgSetDot11PhyOFDMEntry;
    tWsscfgIsSetDot11PhyOFDMEntry WsscfgIsSetDot11PhyOFDMEntry;

    tWsscfgDot11HoppingPatternEntry WsscfgSetDot11HoppingPatternEntry;
    tWsscfgIsSetDot11HoppingPatternEntry WsscfgIsSetDot11HoppingPatternEntry;

    tWsscfgDot11PhyERPEntry WsscfgSetDot11PhyERPEntry;
    tWsscfgIsSetDot11PhyERPEntry WsscfgIsSetDot11PhyERPEntry;

    tWsscfgDot11RSNAConfigEntry WsscfgSetDot11RSNAConfigEntry;
    tWsscfgIsSetDot11RSNAConfigEntry WsscfgIsSetDot11RSNAConfigEntry;

    tWsscfgDot11RSNAConfigPairwiseCiphersEntry
        WsscfgSetDot11RSNAConfigPairwiseCiphersEntry;
    tWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry
        WsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry;

    tWsscfgDot11RSNAConfigAuthenticationSuitesEntry
        WsscfgSetDot11RSNAConfigAuthenticationSuitesEntry;
    tWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry
        WsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry;

    tWsscfgFsDot11StationConfigEntry WsscfgSetFsDot11StationConfigEntry;
    tWsscfgIsSetFsDot11StationConfigEntry WsscfgIsSetFsDot11StationConfigEntry;

    tWsscfgFsDot11CapabilityProfileEntry WsscfgSetFsDot11CapabilityProfileEntry;
    tWsscfgIsSetFsDot11CapabilityProfileEntry
        WsscfgIsSetFsDot11CapabilityProfileEntry;

    tWsscfgFsDot11AuthenticationProfileEntry
        WsscfgSetFsDot11AuthenticationProfileEntry;
    tWsscfgIsSetFsDot11AuthenticationProfileEntry
        WsscfgIsSetFsDot11AuthenticationProfileEntry;

    tWsscfgFsSecurityWebAuthGuestInfoEntry
        WsscfgSetFsSecurityWebAuthGuestInfoEntry;
    tWsscfgIsSetFsSecurityWebAuthGuestInfoEntry
        WsscfgIsSetFsSecurityWebAuthGuestInfoEntry;

    tWsscfgFsStationQosParamsEntry WsscfgSetFsStationQosParamsEntry;
    tWsscfgIsSetFsStationQosParamsEntry WsscfgIsSetFsStationQosParamsEntry;

    tWsscfgFsVlanIsolationEntry WsscfgSetFsVlanIsolationEntry;
    tWsscfgIsSetFsVlanIsolationEntry WsscfgIsSetFsVlanIsolationEntry;

    tWsscfgFsDot11RadioConfigEntry WsscfgSetFsDot11RadioConfigEntry;
    tWsscfgIsSetFsDot11RadioConfigEntry WsscfgIsSetFsDot11RadioConfigEntry;

    tWsscfgFsDot11QosProfileEntry WsscfgSetFsDot11QosProfileEntry;
    tWsscfgIsSetFsDot11QosProfileEntry WsscfgIsSetFsDot11QosProfileEntry;

    tWsscfgFsDot11WlanCapabilityProfileEntry
        WsscfgSetFsDot11WlanCapabilityProfileEntry;
    tWsscfgIsSetFsDot11WlanCapabilityProfileEntry
        WsscfgIsSetFsDot11WlanCapabilityProfileEntry;

    tWsscfgFsDot11WlanAuthenticationProfileEntry
        WsscfgSetFsDot11WlanAuthenticationProfileEntry;
    tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry
        WsscfgIsSetFsDot11WlanAuthenticationProfileEntry;

    tWsscfgFsDot11WlanQosProfileEntry WsscfgSetFsDot11WlanQosProfileEntry;
    tWsscfgIsSetFsDot11WlanQosProfileEntry
        WsscfgIsSetFsDot11WlanQosProfileEntry;

    tWsscfgFsDot11RadioQosEntry WsscfgSetFsDot11RadioQosEntry;
    tWsscfgIsSetFsDot11RadioQosEntry WsscfgIsSetFsDot11RadioQosEntry;

    tWsscfgFsDot11QAPEntry WsscfgSetFsDot11QAPEntry;
    tWsscfgIsSetFsDot11QAPEntry WsscfgIsSetFsDot11QAPEntry;

    tWsscfgFsQAPProfileEntry WsscfgSetFsQAPProfileEntry;
    tWsscfgIsSetFsQAPProfileEntry WsscfgIsSetFsQAPProfileEntry;

    tWsscfgFsDot11CapabilityMappingEntry WsscfgSetFsDot11CapabilityMappingEntry;
    tWsscfgIsSetFsDot11CapabilityMappingEntry
        WsscfgIsSetFsDot11CapabilityMappingEntry;

    tWsscfgFsDot11AuthMappingEntry WsscfgSetFsDot11AuthMappingEntry;
    tWsscfgIsSetFsDot11AuthMappingEntry WsscfgIsSetFsDot11AuthMappingEntry;

    tWsscfgFsDot11QosMappingEntry WsscfgSetFsDot11QosMappingEntry;
    tWsscfgIsSetFsDot11QosMappingEntry WsscfgIsSetFsDot11QosMappingEntry;

    tWsscfgFsDot11AntennasListEntry WsscfgSetFsDot11AntennasListEntry;
    tWsscfgIsSetFsDot11AntennasListEntry WsscfgIsSetFsDot11AntennasListEntry;

    tWsscfgFsDot11WlanEntry WsscfgSetFsDot11WlanEntry;
    tWsscfgIsSetFsDot11WlanEntry WsscfgIsSetFsDot11WlanEntry;

    tWsscfgFsDot11WlanBindEntry WsscfgSetFsDot11WlanBindEntry;
    tWsscfgIsSetFsDot11WlanBindEntry WsscfgIsSetFsDot11WlanBindEntry;

    tWsscfgFsWtpImageUpgradeEntry WsscfgSetFsWtpImageUpgradeEntry;
    tWsscfgIsSetFsWtpImageUpgradeEntry WsscfgIsSetFsWtpImageUpgradeEntry;

    tWsscfgCapwapDot11WlanEntry WsscfgSetCapwapDot11WlanEntry;
    tWsscfgIsSetCapwapDot11WlanEntry WsscfgIsSetCapwapDot11WlanEntry;

    tWsscfgCapwapDot11WlanBindEntry WsscfgSetCapwapDot11WlanBindEntry;
    tWsscfgIsSetCapwapDot11WlanBindEntry WsscfgIsSetCapwapDot11WlanBindEntry;

    tWsscfgFsRrmConfigEntry WsscfgSetFsRrmConfigEntry;
    tWsscfgIsSetFsRrmConfigEntry WsscfgIsSetFsRrmConfigEntry;

    tWsscfgFsDot11nConfigEntry WsscfgSetFsDot11nConfigEntry;
    tWsscfgIsSetFsDot11nConfigEntry WsscfgIsSetFsDot11nConfigEntry;

    tWsscfgFsDot11nMCSDataRateEntry WsscfgSetFsDot11nMCSDataRateEntry;
    tWsscfgIsSetFsDot11nMCSDataRateEntry WsscfgIsSetFsDot11nMCSDataRateEntry;

    UINT4               u4RadioIfIndex = 0;
    UINT4               u4WlanProfileId = 0;
    INT4                i4WlanIfIndex = 0;
    UINT4               u4WtpProfileId = 0;
    UINT1               u1RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT1               u1RadioIndex = 0;
    UINT4               u4RadioIfType = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4NoOfRadio = 0;
    UINT1               au1ModelNumber[256];
    UINT4               u4GetRadioType = 0;
    UINT2               u2WtpProfileId = 0;
    UINT1               au1PromptName[32];
    CHR1               *pu1PromptName = NULL;
    CHR1               *saveptr = NULL;
    UINT1              *pu1Name = NULL;
    UINT4               u4Len = 0;
    UINT4               u4RowStatus = 0;
    tSNMP_OCTET_STRING_TYPE ModelName;
    tSNMP_OCTET_STRING_TYPE ProfileName;
    tSNMP_OCTET_STRING_TYPE OperRate;
    tSNMP_OCTET_STRING_TYPE FsCapwapWtpModelNumber;
    tSNMP_OCTET_STRING_TYPE SetValFsCapwapImageName;
    tIPvXAddr           IpAddress;
    INT1                ai1FileName[WTP_CONFIG_FILENAME_LEN + 1];
    INT4                i4WtpMacType = 3;
    INT4                i4WlanMacType = 3;
    UINT4               currentProfileId = 0, nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;
    UINT1               au1OperRate[RADIOIF_OPER_RATE];
    INT4               *pi4WlanIfIndex = NULL;
    INT4               *pi4RadioIfIndex = NULL;
    UINT4              *pu4RowStatus = NULL;
    INT1                i1RetStatus = 0;
    UINT4               u4ApGrpVlanId = 0;

    pi4WlanIfIndex = &i4WlanIfIndex;
    pi4RadioIfIndex = &i4RadioIfIndex;
    pu4RowStatus = &u4RowStatus;

    MEMSET (&au1OperRate, 0, RADIOIF_OPER_RATE);
    MEMSET (&au1ModelNumber, 0, 256);
    MEMSET (&ModelName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    ModelName.pu1_OctetList = au1ModelNumber;
    MEMSET (au1PromptName, 0, sizeof (32));
    MEMSET (&FsCapwapWtpModelNumber, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&SetValFsCapwapImageName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    pu1PromptName = (CHR1 *) au1PromptName;
    OperRate.pu1_OctetList = au1OperRate;

    UNUSED_PARAM (u4CmdType);
    CliRegisterLock (CliHandle, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        /* Coverity fix - It is analayzed that all the
         * Capwap CLI commands function use the first argument as NULL.
         * So the extraction of the first index is not required in this case  */
    }

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == WSSCFG_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);
    switch (u4Command)
    {
        case CLI_WSSCFG_CAPWAPDOT11WLANTABLE:
            MEMSET (&WsscfgSetCapwapDot11WlanEntry, 0,
                    sizeof (tWsscfgCapwapDot11WlanEntry));
            MEMSET (&WsscfgIsSetCapwapDot11WlanEntry, 0,
                    sizeof (tWsscfgIsSetCapwapDot11WlanEntry));

            if ((args[5] != NULL) && (*args[5] == CLI_WLAN_ENABLE)
                && (args[6] != NULL))
            {
                CliPrintf (CliHandle, "\r\nInvalid input\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }
            if (args[6] != NULL)
            {
                if (STRLEN (args[6]) > CLI_MAX_SSID_LEN)
                {
                    CliPrintf (CliHandle, "\r\nSSID name is too long\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }
            else
            {
                if (args[4] != NULL)
                {
                    if (*args[4] == CREATE_AND_WAIT)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n SSID missing. Mandatory for Profile Creation\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                }
            }

            WSSCFG_FILL_CAPWAPDOT11WLANTABLE_ARGS ((&WsscfgSetCapwapDot11WlanEntry), (&WsscfgIsSetCapwapDot11WlanEntry), args[0], args[1], args[2], args[3], args[4]);

            i4RetStatus =
                WsscfgCliSetCapwapDot11WlanTable (CliHandle,
                                                  (&WsscfgSetCapwapDot11WlanEntry),
                                                  (&WsscfgIsSetCapwapDot11WlanEntry));
            if (i4RetStatus != OSIX_FAILURE)
            {
                if (args[5] != NULL)
                {
                    if (args[0] != NULL)
                    {
                        if (nmhGetCapwapDot11WlanProfileIfIndex
                            (CLI_PTR_TO_U4 (*args[0]),
                             &i4WlanIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Index does not exist \r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                        if (i4WlanIfIndex == 0)
                        {
                            CliPrintf (CliHandle, "\r\n Invalid index\r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                    }
                    if ((i4RetStatus =
                         nmhTestv2IfMainAdminStatus (&u4ErrorCode,
                                                     i4WlanIfIndex,
                                                     CLI_PTR_TO_U4 (*args
                                                                    [5])))
                        != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "\r\n Invalid input\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    if ((i4RetStatus =
                         nmhSetIfMainAdminStatus (i4WlanIfIndex,
                                                  CLI_PTR_TO_U4 (*args[5])))
                        != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Admin Status change failed\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }

                }
            }
            break;
        case CLI_WSSCFG_APGROUPNAME:
        {
            MEMSET (&ApGroupName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

            if ((args[1] != NULL))
            {
                ApGroupName.pu1_OctetList = (UINT1 *) args[1];
                ApGroupName.i4_Length = STRLEN (args[1]);
            }
            if ((args[2] != NULL))
            {
                i1RetStatus =
                    nmhSetFsApGroupRowStatus (CLI_PTR_TO_U4 (*args[0]),
                                              CLI_PTR_TO_I4 (*args[2]));
            }

            if ((args[0] != NULL))
            {
                if (RBTreeCount (gApGroupGlobals.ApGroupConfigEntryData,
                                 &u4Count) != RB_SUCCESS)
                {
                    i4RetStatus = OSIX_FAILURE;
                }
                if ((u4Count > CLI_MAX_APGROUPS))
                {
                    CliPrintf (CliHandle, "\r\n AP Group more than 50."
                               " delete one Group to add a new one\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }

            if ((args[1] != NULL))
            {
                if (STRLEN (args[1]) > CLI_MAX_GROUPNAME_LEN)
                {
                    CliPrintf (CliHandle, "\r\n Group name is too long\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }
            if ((i1RetStatus = nmhSetFsApGroupName (CLI_PTR_TO_U4 (*args[0]),
                                                    &ApGroupName)) !=
                SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\n Adding AP GROUP failed.\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
            }
        }
            break;

        case CLI_WSSCFG_APGROUPNAMEDELETE:
        {
            MEMSET (&ApGroupName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

            if ((args[1] != NULL))
            {
                ApGroupName.pu1_OctetList = (UINT1 *) args[1];
                ApGroupName.i4_Length = STRLEN (args[1]);
            }

            if ((args[0] != NULL))
            {
                if ((CLI_PTR_TO_U4 (*args[0]) > CLI_MAX_APGROUPS))
                {
                    CliPrintf (CliHandle, "\r\n Invalid Group id value."
                               " should not be more than 50\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                }

            }

            if ((args[1] != NULL))
            {
                if (STRLEN (args[1]) > CLI_MAX_GROUPNAME_LEN)
                {
                    CliPrintf (CliHandle, "\r\n Group name is too long\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }

            if ((args[2] != NULL))
            {
                if ((i1RetStatus =
                     nmhSetFsApGroupRowStatus (CLI_PTR_TO_U4 (*args[0]),
                                               CLI_PTR_TO_I4 (*args[2]))) !=
                    SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }
        }
            break;

        case CLI_WSSCFG_APGROUPDESC:
        {
            MEMSET (&ApGroupName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            ApGroupName.pu1_OctetList = (UINT1 *) args[0];
            ApGroupName.i4_Length = STRLEN (args[0]);
            MEMSET (&ApGroupNameDesc, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            ApGroupNameDesc.pu1_OctetList = (UINT1 *) args[1];
            ApGroupNameDesc.i4_Length = STRLEN (args[1]);

            i1RetStatus =
                nmhGetApGroupIdFromGroupName (&ApGroupName, &u4ApGroupId);
            if (i1RetStatus == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n Invalid Group Name\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            if (i1RetStatus == SNMP_SUCCESS)
            {
                if ((args[0] != NULL))
                {
                    if (STRLEN (args[0]) > CLI_MAX_GROUPNAME_LEN)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Group name is too long\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                }

                if (args[1] != NULL)
                {
                    if (STRLEN (args[1]) > CLI_MAX_GROUPDESC_LEN)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Group Description is too long\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    i4RetStatus =
                        nmhSetFsApGroupNameDescription (u4ApGroupId,
                                                        &ApGroupNameDesc);
                }
            }
        }
            break;

        case CLI_WSSCFG_APGROUPPROFILEMAPPING:
        {
            MEMSET (&ApGroupName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            ApGroupName.pu1_OctetList = (UINT1 *) args[0];
            ApGroupName.i4_Length = STRLEN (args[0]);

            i1RetStatus =
                nmhGetApGroupIdFromGroupName (&ApGroupName, &u4ApGroupId);
            if (i1RetStatus == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n Invalid Group Name\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            if ((args[0] != NULL))
            {
                if (STRLEN (args[0]) > CLI_MAX_GROUPNAME_LEN)
                {
                    CliPrintf (CliHandle, "\r\n Group name is too long\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }

            if ((args[1] != NULL))
            {
                if (CapwapGetWtpProfileIdFromProfileName ((UINT1 *) args[1],
                                                          &u4WtpProfileId) !=
                    OSIX_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }

            if (nmhGetFsApGroupInterfaceVlan (u4ApGroupId, &u4ApGrpVlanId) ==
                SNMP_SUCCESS)
            {
                if (u4ApGrpVlanId == 0)
                {
                    CliPrintf (CliHandle,
                               "\r\n Failed Interface Vlan is not configured\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }

            if ((args[2] != NULL))
            {
                i1RetStatus =
                    nmhSetFsApGroupWTPRowStatus (u4ApGroupId, u4WtpProfileId,
                                                 CLI_PTR_TO_I4 (*args[2]));
            }
        }
            break;

        case CLI_WSSCFG_APGROUPPROFILEMAPPINGDELETE:
        {
            MEMSET (&ApGroupName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            ApGroupName.pu1_OctetList = (UINT1 *) args[0];
            ApGroupName.i4_Length = STRLEN (args[0]);

            i1RetStatus =
                nmhGetApGroupIdFromGroupName (&ApGroupName, &u4ApGroupId);
            if (i1RetStatus == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n Invalid Group Name\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            if ((args[0] != NULL))
            {
                if (STRLEN (args[0]) > CLI_MAX_GROUPNAME_LEN)
                {
                    CliPrintf (CliHandle, "\r\n Group name is too long\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }

            if ((args[1] != NULL))
            {
            }
            if (CapwapGetWtpProfileIdFromProfileName ((UINT1 *) args[1],
                                                      &u4WtpProfileId) !=
                OSIX_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            if ((args[2] != NULL))
            {
                i1RetStatus =
                    nmhSetFsApGroupWTPRowStatus (u4ApGroupId, u4WtpProfileId,
                                                 CLI_PTR_TO_I4 (*args[2]));
                if (i1RetStatus == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Failed Add the WTP Profile into the Group\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }
        }
            break;

        case CLI_WSSCFG_APGROUPINTERFACEMAPPING:
        {
            MEMSET (&ApGroupName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            ApGroupName.pu1_OctetList = (UINT1 *) args[0];
            ApGroupName.i4_Length = STRLEN (args[0]);

            i1RetStatus =
                nmhGetApGroupIdFromGroupName (&ApGroupName, &u4ApGroupId);
            if (i1RetStatus == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n Invalid Group Name\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            if ((args[0] != NULL))
            {
                if (STRLEN (args[0]) > CLI_MAX_GROUPNAME_LEN)
                {
                    CliPrintf (CliHandle, "\r\n Group name is too long\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }

            if ((args[1] != NULL))
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (CLI_PTR_TO_U4 (*args[1]), &i4WlanIfIndex) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n Index does not exist \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (i4WlanIfIndex == 0)
                {
                    CliPrintf (CliHandle, "\r\n Invalid index\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }
            if (nmhGetFsApGroupInterfaceVlan (u4ApGroupId, &u4ApGrpVlanId) ==
                SNMP_SUCCESS)
            {
                if (u4ApGrpVlanId == 0)
                {
                    CliPrintf (CliHandle,
                               "\r\n Failed Interface Vlan is not configured\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }

            if ((args[2] != NULL))
            {
                if (nmhTestv2FsApGroupWLANRowStatus (&u4ErrCode,
                                                     u4ApGroupId,
                                                     CLI_PTR_TO_U4 (*args[1]),
                                                     CLI_PTR_TO_U4 (*args[2]))
                    == SNMP_SUCCESS)
                {

                    i1RetStatus = nmhSetFsApGroupWLANRowStatus
                        (u4ApGroupId, CLI_PTR_TO_U4 (*args[1]),
                         CLI_PTR_TO_I4 (*args[2]));
                    if (i1RetStatus == SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Failed Add the WLAN into the Group!\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r\n Failed Add the WLAN into the Group!"
                               "Maximum Wlan are configured \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

            }

        }
            break;

        case CLI_WSSCFG_APGROUPINTERFACEMAPPINGDELETE:
        {
            MEMSET (&ApGroupName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            ApGroupName.pu1_OctetList = (UINT1 *) args[0];
            ApGroupName.i4_Length = STRLEN (args[0]);

            i1RetStatus =
                nmhGetApGroupIdFromGroupName (&ApGroupName, &u4ApGroupId);
            if (i1RetStatus == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n Invalid Group Name\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            if ((args[0] != NULL))
            {
                if (STRLEN (args[0]) > CLI_MAX_GROUPNAME_LEN)
                {
                    CliPrintf (CliHandle, "\r\n Group name is too long\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }

            if ((args[1] != NULL))
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (CLI_PTR_TO_U4 (*args[1]), &i4WlanIfIndex) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n Index does not exist \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

                if (i4WlanIfIndex == 0)
                {
                    CliPrintf (CliHandle, "\r\n Invalid index\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }

            i1RetStatus = nmhSetFsApGroupWLANRowStatus (u4ApGroupId,
                                                        CLI_PTR_TO_U4 (*args
                                                                       [1]),
                                                        CLI_PTR_TO_U4 (*args
                                                                       [2]));
            if (i1RetStatus == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n Failed Delete the WLAN into the Group\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }
        }
            break;

        case CLI_WSSCFG_APGROUPRADIOPOLICY:
        {
            MEMSET (&ApGroupName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            ApGroupName.pu1_OctetList = (UINT1 *) args[0];
            ApGroupName.i4_Length = STRLEN (args[0]);

            i1RetStatus =
                nmhGetApGroupIdFromGroupName (&ApGroupName, &u4ApGroupId);
            if (i1RetStatus == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n Invalid Group Name\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            if ((args[0] != NULL))
            {
                if (STRLEN (args[0]) > CLI_MAX_GROUPNAME_LEN)
                {
                    CliPrintf (CliHandle, "\r\n Group name is too long\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }

            if ((args[1] != NULL))
            {
                i1RetStatus =
                    nmhSetFsApGroupRadioPolicy (u4ApGroupId,
                                                CLI_PTR_TO_U4 (*args[1]));
                if (i1RetStatus == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n Failed to Update the RadioPolicy "
                               "due to group is Operational\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

            }
        }
            break;

        case CLI_WSSCFG_APGROUPINTERFACEVLAN:
        {
            MEMSET (&ApGroupName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

            ApGroupName.pu1_OctetList = (UINT1 *) args[0];
            ApGroupName.i4_Length = STRLEN (args[0]);

            i1RetStatus =
                nmhGetApGroupIdFromGroupName (&ApGroupName, &u4ApGroupId);
            if (i1RetStatus == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n Invalid Group Name\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            if ((args[0] != NULL))
            {
                if (STRLEN (args[0]) > CLI_MAX_GROUPNAME_LEN)
                {
                    CliPrintf (CliHandle, "\r\n Group name is too long\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }

            if ((args[1] != NULL))
            {
                if (*(args[1]) >= 1 || *(args[1]) <= 4094)
                {
                    i1RetStatus =
                        nmhSetFsApGroupInterfaceVlan (u4ApGroupId,
                                                      CLI_PTR_TO_U4 (*args[1]));
                    if (i1RetStatus == SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Failed to Set interface Vlan"
                                   " due to AP group is operational\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                    }
                }
            }

        }
            break;

        case CLI_WSSCFG_WLANMULTICASTMODE:
        {
            if ((args[0] != NULL) && (args[1] != NULL))
            {
                if (nmhTestv2FsWlanMulticastMode (&u4ErrCode,
                                                  CLI_PTR_TO_U4 (*args[0]),
                                                  CLI_PTR_TO_I4 (*args[1])) ==
                    SNMP_SUCCESS)
                {

                    if (nmhSetFsWlanMulticastMode (CLI_PTR_TO_U4 (*args[0]),
                                                   CLI_PTR_TO_I4 (*args[1])) ==
                        SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nMulticast Mode Config Failed\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                    }
                }
            }
        }
            break;

        case CLI_WSSCFG_WLANMULTICASTLENGTH:
        {
            if ((args[0] != NULL) && (args[1] != NULL))
            {
                if (nmhTestv2FsWlanMulticastSnoopTableLength (&u4ErrCode,
                                                              CLI_PTR_TO_U4
                                                              (*args[0]),
                                                              CLI_PTR_TO_U4
                                                              (*args[1])) ==
                    SNMP_SUCCESS)
                {

                    if (nmhSetFsWlanMulticastSnoopTableLength
                        (CLI_PTR_TO_U4 (*args[0]),
                         CLI_PTR_TO_U4 (*args[1])) == SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nMulticast Snoop table length Config Failed\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                    }
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r\nSnoop table length is Invalied\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                }
            }
        }
            break;

        case CLI_WSSCFG_WLANMULTICASTTIMER:
        {
            if ((args[0] != NULL) && (args[1] != NULL))
            {
                if (nmhTestv2FsWlanMulticastSnoopTimer (&u4ErrCode,
                                                        CLI_PTR_TO_U4 (*args
                                                                       [0]),
                                                        CLI_PTR_TO_U4 (*args
                                                                       [1])) ==
                    SNMP_SUCCESS)
                {

                    if (nmhSetFsWlanMulticastSnoopTimer
                        (CLI_PTR_TO_U4 (*args[0]),
                         CLI_PTR_TO_U4 (*args[1])) == SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nMulticast Snoop table timer Config Failed\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                    }
                }
            }
        }
            break;

        case CLI_WSSCFG_WLANMULTICASTTIMEOUT:
        {
            if ((args[0] != NULL) && (args[1] != NULL))
            {
                if (nmhTestv2FsWlanMulticastSnoopTimeout (&u4ErrCode,
                                                          CLI_PTR_TO_U4 (*args
                                                                         [0]),
                                                          CLI_PTR_TO_U4 (*args
                                                                         [1]))
                    == SNMP_SUCCESS)
                {

                    if (nmhSetFsWlanMulticastSnoopTimeout
                        (CLI_PTR_TO_U4 (*args[0]),
                         CLI_PTR_TO_U4 (*args[1])) == SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nMulticast Snoop table timeout Config Failed\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                    }
                }
            }
        }
            break;

        case CLI_WSSCFG_CAPWAPDOT11WLANBINDTABLE:
            MEMSET (&WsscfgSetCapwapDot11WlanBindEntry, 0,
                    sizeof (tWsscfgCapwapDot11WlanBindEntry));
            MEMSET (&WsscfgIsSetCapwapDot11WlanBindEntry, 0,
                    sizeof (tWsscfgIsSetCapwapDot11WlanBindEntry));

            if (nmhGetFsApGroupEnabledStatus (&i4ApGroupEnabledStatus) ==
                SNMP_SUCCESS)
            {
                if (i4ApGroupEnabledStatus == CLI_APGROUP_ENABLE)
                {
                    CliPrintf (CliHandle,
                               "\r\n AP Group Feature is enabled. Binding Failed \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }

            if (args[2] != NULL)
            {
                /* When Profile name is received as input get the profile id
                 * from mapping table and pass the profile name as NULL */
                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[2], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n WTP Profile not found. Binding failed\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

                if ((i4RetStatus =
                     nmhGetCapwapBaseWtpProfileWtpModelNumber
                     (u4WtpProfileId, &ModelName)) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Model entry missing. Binding failed\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if ((i4RetStatus =
                     nmhGetFsCapwapWtpMacType (&ModelName,
                                               &i4WtpMacType)) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting WTP Mac Type failed.\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

                if ((i4RetStatus =
                     nmhGetCapwapDot11WlanMacType ((*args[4]),
                                                   &i4WlanMacType)) !=
                    SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting WLAN Mac Type failed.\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

                if (i4WtpMacType != i4WlanMacType)
                {
                    CliPrintf (CliHandle,
                               "\r\n MAC Type Mismatch. WLAN Binding Failure\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

                if (*args[3] == 0)
                {
                    if ((i4RetStatus =
                         nmhGetCapwapBaseWtpProfileWtpModelNumber
                         (u4WtpProfileId, &ModelName)) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Model entry missing. Binding failed\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }

                    if ((i4RetStatus =
                         nmhGetFsNoOfRadio (&ModelName,
                                            &u4NoOfRadio)) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Getting Radio count failed.\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }

                    for (u1RadioId = 1; u1RadioId <= u4NoOfRadio; u1RadioId++)
                    {
                        if ((i4RetStatus =
                             nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                             (u4WtpProfileId, u1RadioId,
                              &i4RadioIfIndex)) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Radio Index not found, Binding failed\r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }

                        if ((i4RetStatus =
                             nmhGetFsDot11RadioType (i4RadioIfIndex,
                                                     &u4GetRadioType)) !=
                            SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Getting Radio Type failed\r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
#ifdef WPA_WANTED
                        if (u4GetRadioType == CLI_RADIO_TYPEAN ||
                            u4GetRadioType == CLI_RADIO_TYPEBGN ||
                            u4GetRadioType == CLI_RADIO_TYPEAC)
                        {
                            u4WlanId = *args[4];
                            if (nmhGetCapwapDot11WlanProfileIfIndex
                                (u4WlanId,
                                 ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
                            {
                                i4RetStatus = OSIX_FAILURE;
                                break;
                            }
                            if (WpaGetFsRSNAEnabled (u4ProfileId, &i4WpaEnable)
                                == SNMP_SUCCESS)
                            {
                                if (i4WpaEnable == WPA_ENABLED)
                                {
                                    CliPrintf (CliHandle,
                                               "\r\n WPA is not supported for AN,BGN and AC modes\n");
                                    i4RetStatus = OSIX_FAILURE;
                                    break;
                                }
                            }
                        }
#endif
                        if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[1]))
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Radio Type conflicting\r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                        else
                        {
                            WSSCFG_FILL_CAPWAPDOT11WLANBINDTABLE_ARGS ((&WsscfgSetCapwapDot11WlanBindEntry), (&WsscfgIsSetCapwapDot11WlanBindEntry), args[0], pi4RadioIfIndex, args[4]);
                            /* Set RowStatus as ACTIVE */
                            if (WsscfgSetCapwapDot11WlanBindEntry.MibObject.
                                i4CapwapDot11WlanBindRowStatus == CREATE_AND_GO)
                            {
                                WsscfgSetCapwapDot11WlanBindEntry.MibObject.
                                    i4CapwapDot11WlanBindRowStatus =
                                    CREATE_AND_WAIT;

                                i4RetStatus =
                                    WsscfgCliSetCapwapDot11WlanBindTable
                                    (CliHandle,
                                     (&WsscfgSetCapwapDot11WlanBindEntry),
                                     (&WsscfgIsSetCapwapDot11WlanBindEntry));
                                WsscfgSetCapwapDot11WlanBindEntry.MibObject.
                                    i4CapwapDot11WlanBindRowStatus = ACTIVE;

                            }

                            i4RetStatus =
                                WsscfgCliSetCapwapDot11WlanBindTable
                                (CliHandle,
                                 (&WsscfgSetCapwapDot11WlanBindEntry),
                                 (&WsscfgIsSetCapwapDot11WlanBindEntry));
                        }
                    }
                }
                else
                {
                    if ((i4RetStatus =
                         nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                         (u4WtpProfileId, *args[3],
                          &i4RadioIfIndex)) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n AP Name missing. Binding failed\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }

                    if ((i4RetStatus =
                         nmhGetFsDot11RadioType (i4RadioIfIndex,
                                                 &u4GetRadioType)) !=
                        SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Getting Radio Type failed\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
#ifdef WPA_WANTED
                    if (u4GetRadioType == CLI_RADIO_TYPEAN ||
                        u4GetRadioType == CLI_RADIO_TYPEBGN ||
                        u4GetRadioType == CLI_RADIO_TYPEAC)
                    {
                        u4WlanId = *args[4];
                        if (nmhGetCapwapDot11WlanProfileIfIndex
                            (u4WlanId, ((INT4 *) &u4ProfileId)) != SNMP_SUCCESS)
                        {
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }

                        if (WpaGetFsRSNAEnabled (u4ProfileId, &i4WpaEnable) ==
                            SNMP_SUCCESS)
                        {
                            if (i4WpaEnable == WPA_ENABLED)
                            {
                                CliPrintf (CliHandle,
                                           "\r\n WPA is not supported for AN,BGN and AC modes\n");
                                i4RetStatus = OSIX_FAILURE;
                                break;
                            }
                        }
                    }
#endif
                    /*if (u4GetRadioType != (INT4)*args[1]) */
                    if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[1]))
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Radio Type conflicting\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    else
                    {
                        WSSCFG_FILL_CAPWAPDOT11WLANBINDTABLE_ARGS ((&WsscfgSetCapwapDot11WlanBindEntry), (&WsscfgIsSetCapwapDot11WlanBindEntry), args[0], pi4RadioIfIndex, args[4]);

                        /* Set RowStatus as ACTIVE */
                        if (WsscfgSetCapwapDot11WlanBindEntry.MibObject.
                            i4CapwapDot11WlanBindRowStatus == CREATE_AND_GO)
                        {
                            WsscfgSetCapwapDot11WlanBindEntry.MibObject.
                                i4CapwapDot11WlanBindRowStatus =
                                CREATE_AND_WAIT;

                            i4RetStatus =
                                WsscfgCliSetCapwapDot11WlanBindTable
                                (CliHandle,
                                 (&WsscfgSetCapwapDot11WlanBindEntry),
                                 (&WsscfgIsSetCapwapDot11WlanBindEntry));
                            WsscfgSetCapwapDot11WlanBindEntry.MibObject.
                                i4CapwapDot11WlanBindRowStatus = ACTIVE;

                        }
                        i4RetStatus =
                            WsscfgCliSetCapwapDot11WlanBindTable
                            (CliHandle,
                             (&WsscfgSetCapwapDot11WlanBindEntry),
                             (&WsscfgIsSetCapwapDot11WlanBindEntry));
                    }
                }
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r\n AP Name missing. Binding failed\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
            }
            break;

        case CLI_WSSCFG_DOT11STATIONCONFIGTABLE:
            MEMSET (&WsscfgSetDot11StationConfigEntry, 0,
                    sizeof (tWsscfgDot11StationConfigEntry));
            MEMSET (&WsscfgIsSetDot11StationConfigEntry, 0,
                    sizeof (tWsscfgIsSetDot11StationConfigEntry));

            /* WSS Changes Starts */
            if (args[25] != NULL)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (CLI_PTR_TO_U4 (*args[25]), &i4WlanIfIndex) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n Index does not exist \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                else if (i4WlanIfIndex == 0)
                {
                    CliPrintf (CliHandle, "\r\n Invalid index\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                else
                {
                    WSSCFG_FILL_DOT11STATIONCONFIGTABLE_ARGS ((&WsscfgSetDot11StationConfigEntry), (&WsscfgIsSetDot11StationConfigEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17], args[18], args[19], args[20], args[21], pi4WlanIfIndex);
                    i4RetStatus =
                        WsscfgCliSetDot11StationConfigTable (CliHandle,
                                                             (&WsscfgSetDot11StationConfigEntry),
                                                             (&WsscfgIsSetDot11StationConfigEntry));
                }
            }
            else if (args[22] != NULL)
            {

                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[22], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                u1RadioId = CLI_PTR_TO_U4 (*args[23]);

                if (u1RadioId == 0)
                {
                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "\r\nInvalid Model number\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }

                    if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFailed to fetch number of radios\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }

                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {

                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d: Invalid Index \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d: Radio Type could not be obtained \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[24]))
                        {
                            continue;
                        }
                        else
                        {
                            if (args[26] != NULL)
                            {
                                if (args[9] != NULL)
                                {
                                    nmhGetDot11OperationalRateSet
                                        (i4RadioIfIndex, &OperRate);
                                    if (WssCfgSetOperRate
                                        (i4RadioIfIndex, &OperRate, *args[9],
                                         *args[26]) == CLI_FAILURE)
                                    {
                                        CLI_FATAL_ERROR (CliHandle);
                                        i4RetStatus = CLI_FAILURE;
                                        break;
                                    }
                                    *args[10] = OperRate.i4_Length;
                                }
                            }
                            WSSCFG_FILL_DOT11STATIONCONFIGTABLE_ARGS ((&WsscfgSetDot11StationConfigEntry), (&WsscfgIsSetDot11StationConfigEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], OperRate.pu1_OctetList, args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17], args[18], args[19], args[20], args[21], pi4RadioIfIndex);

                            if (WsscfgCliSetDot11StationConfigTable
                                (CliHandle,
                                 (&WsscfgSetDot11StationConfigEntry),
                                 (&WsscfgIsSetDot11StationConfigEntry)) !=
                                OSIX_SUCCESS)
                            {

                                CliPrintf (CliHandle,
                                           "\r\nFor Radio %d: Configuration update failed\r\n",
                                           u1RadioIndex);
                                continue;

                            }
                        }
                    }
                }
                else
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u1RadioId,
                         &i4RadioIfIndex) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d: Invalid Index\r\n",
                                   u1RadioId);
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }

                    if (nmhGetFsDot11RadioType
                        (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d: Radio Type could not be obtained\r\n",
                                   i4RadioIfIndex);
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }

                    if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[24]))
                    {
                        CliPrintf (CliHandle, "\r\nRadio Type Mismatch\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }
                    if (args[26] != NULL)
                    {
                        if (args[9] != NULL)
                        {
                            nmhGetDot11OperationalRateSet (i4RadioIfIndex,
                                                           &OperRate);
                            if (WssCfgSetOperRate (i4RadioIfIndex,
                                                   &OperRate, *args[9],
                                                   *args[26]) == CLI_FAILURE)
                            {
                                CLI_FATAL_ERROR (CliHandle);
                                i4RetStatus = CLI_FAILURE;
                                break;
                            }
                            *args[10] = OperRate.i4_Length;
                        }
                    }

                    WSSCFG_FILL_DOT11STATIONCONFIGTABLE_ARGS ((&WsscfgSetDot11StationConfigEntry), (&WsscfgIsSetDot11StationConfigEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], OperRate.pu1_OctetList, args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17], args[18], args[19], args[20], args[21], pi4RadioIfIndex);

                    i4RetStatus =
                        WsscfgCliSetDot11StationConfigTable (CliHandle,
                                                             (&WsscfgSetDot11StationConfigEntry),
                                                             (&WsscfgIsSetDot11StationConfigEntry));

                }

            }
            else if (NULL == args[22])
            {
                if (nmhGetFirstIndexFsCapwapWirelessBindingTable
                    (&nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                else
                {
                    do
                    {
                        currentProfileId = nextProfileId;
                        u4currentBindingId = u4nextBindingId;
                        if ((currentProfileId == 0)
                            || (u4currentBindingId == 0))
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                        u4GetRadioType = (INT4) *args[24];
                        i4RetStatus = WssCfgGetDot11RadioIfIndex (nextProfileId,
                                                                  u4nextBindingId,
                                                                  u4GetRadioType,
                                                                  &i4RadioIfIndex);

                        if (i4RetStatus == OSIX_FAILURE)
                        {
                            continue;
                        }
                        else
                        {
                            if (args[26] != NULL)
                            {
                                if (args[9] != NULL)
                                {
                                    nmhGetDot11OperationalRateSet
                                        (i4RadioIfIndex, &OperRate);
                                    if (WssCfgSetOperRate
                                        (i4RadioIfIndex, &OperRate, *args[9],
                                         *args[26]) == CLI_FAILURE)
                                    {
                                        CLI_FATAL_ERROR (CliHandle);
                                        i4RetStatus = CLI_FAILURE;
                                        break;
                                    }
                                    *args[10] = OperRate.i4_Length;
                                }
                            }
                            WSSCFG_FILL_DOT11STATIONCONFIGTABLE_ARGS ((&WsscfgSetDot11StationConfigEntry), (&WsscfgIsSetDot11StationConfigEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], OperRate.pu1_OctetList, args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17], args[18], args[19], args[20], args[21], pi4RadioIfIndex);

                            if (WsscfgCliSetDot11StationConfigTable (CliHandle,
                                                                     (&WsscfgSetDot11StationConfigEntry),
                                                                     (&WsscfgIsSetDot11StationConfigEntry))
                                != OSIX_SUCCESS)
                            {

                                CliPrintf (CliHandle, "\r\n Ap(%d) Radio"
                                           "%d :  Configuration failed\r\n",
                                           currentProfileId,
                                           u4currentBindingId);
                                continue;
                            }
                        }
                    }
                    while (nmhGetNextIndexFsCapwapWirelessBindingTable
                           (currentProfileId, &nextProfileId,
                            u4currentBindingId,
                            &u4nextBindingId) == SNMP_SUCCESS);
                }
            }
            break;

        case CLI_WSSCFG_DOT11AUTHENTICATIONALGORITHMSTABLE:
            MEMSET (&WsscfgSetDot11AuthenticationAlgorithmsEntry, 0,
                    sizeof (tWsscfgDot11AuthenticationAlgorithmsEntry));
            MEMSET (&WsscfgIsSetDot11AuthenticationAlgorithmsEntry, 0,
                    sizeof (tWsscfgIsSetDot11AuthenticationAlgorithmsEntry));

            if (args[3] != NULL)
            {
                nmhGetCapwapDot11WlanProfileIfIndex (CLI_PTR_TO_U4
                                                     (*args[3]),
                                                     &i4WlanIfIndex);
            }

            WSSCFG_FILL_DOT11AUTHENTICATIONALGORITHMSTABLE_ARGS ((&WsscfgSetDot11AuthenticationAlgorithmsEntry), (&WsscfgIsSetDot11AuthenticationAlgorithmsEntry), args[0], args[1], args[2], pi4WlanIfIndex);

            i4RetStatus =
                WsscfgCliSetDot11AuthenticationAlgorithmsTable (CliHandle,
                                                                (&WsscfgSetDot11AuthenticationAlgorithmsEntry),
                                                                (&WsscfgIsSetDot11AuthenticationAlgorithmsEntry));
            break;
        case CLI_WSSCFG_DOT11WEBAUTHREDIRECTFILE:
            i4RetStatus =
                WsscfgCliSetWebauthRedirectFile (CliHandle, (UINT1 *) args[0],
                                                 *args[1]);
            break;
        case CLI_WSSCFG_DOT11LOGINAUTHMODE:
            i4RetStatus =
                WsscfgCliSetWlanLoginAuthentication (CliHandle, *args[0],
                                                     *args[1]);
            break;

        case CLI_WSSCFG_DOT11WEPDEFAULTKEYSTABLE:
            MEMSET (&WsscfgSetDot11WEPDefaultKeysEntry, 0,
                    sizeof (tWsscfgDot11WEPDefaultKeysEntry));
            MEMSET (&WsscfgIsSetDot11WEPDefaultKeysEntry, 0,
                    sizeof (tWsscfgIsSetDot11WEPDefaultKeysEntry));

            if (args[5] != NULL)
            {
                nmhGetCapwapDot11WlanProfileIfIndex (CLI_PTR_TO_U4
                                                     (*args[0]),
                                                     &i4WlanIfIndex);
            }
            WSSCFG_FILL_DOT11WEPDEFAULTKEYSTABLE_ARGS ((&WsscfgSetDot11WEPDefaultKeysEntry), (&WsscfgIsSetDot11WEPDefaultKeysEntry), args[0], args[1], args[2], pi4WlanIfIndex);

            i4RetStatus =
                WsscfgCliSetDot11WEPDefaultKeysTable (CliHandle,
                                                      (&WsscfgSetDot11WEPDefaultKeysEntry),
                                                      (&WsscfgIsSetDot11WEPDefaultKeysEntry));
            break;

        case CLI_WSSCFG_DOT11WEPKEYMAPPINGSTABLE:
            MEMSET (&WsscfgSetDot11WEPKeyMappingsEntry, 0,
                    sizeof (tWsscfgDot11WEPKeyMappingsEntry));
            MEMSET (&WsscfgIsSetDot11WEPKeyMappingsEntry, 0,
                    sizeof (tWsscfgIsSetDot11WEPKeyMappingsEntry));

            WSSCFG_FILL_DOT11WEPKEYMAPPINGSTABLE_ARGS ((&WsscfgSetDot11WEPKeyMappingsEntry), (&WsscfgIsSetDot11WEPKeyMappingsEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6]);

            i4RetStatus =
                WsscfgCliSetDot11WEPKeyMappingsTable (CliHandle,
                                                      (&WsscfgSetDot11WEPKeyMappingsEntry),
                                                      (&WsscfgIsSetDot11WEPKeyMappingsEntry));
            break;

        case CLI_WSSCFG_DOT11PRIVACYTABLE:
            MEMSET (&WsscfgSetDot11PrivacyEntry, 0,
                    sizeof (tWsscfgDot11PrivacyEntry));
            MEMSET (&WsscfgIsSetDot11PrivacyEntry, 0,
                    sizeof (tWsscfgIsSetDot11PrivacyEntry));

            WSSCFG_FILL_DOT11PRIVACYTABLE_ARGS ((&WsscfgSetDot11PrivacyEntry),
                                                (&WsscfgIsSetDot11PrivacyEntry),
                                                args[0], args[1], args[2],
                                                args[3], args[4], args[5],
                                                args[6]);

            i4RetStatus =
                WsscfgCliSetDot11PrivacyTable (CliHandle,
                                               (&WsscfgSetDot11PrivacyEntry),
                                               (&WsscfgIsSetDot11PrivacyEntry));
            break;

        case CLI_WSSCFG_DOT11MULTIDOMAINCAPABILITYTABLE:
            MEMSET (&WsscfgSetDot11MultiDomainCapabilityEntry, 0,
                    sizeof (tWsscfgDot11MultiDomainCapabilityEntry));
            MEMSET (&WsscfgIsSetDot11MultiDomainCapabilityEntry, 0,
                    sizeof (tWsscfgIsSetDot11MultiDomainCapabilityEntry));

            if (args[4] == NULL)
            {
                break;
            }
            else
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[6], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n WTP Profile not found\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                u1RadioId = CLI_PTR_TO_U4 (*args[5]);
                if (u1RadioId == 0)
                {
                    /*Coverity Change Begins */
                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "\r\n Invalid Model number\r\n");
                    }
                    /*Coverity Change Ends */
                    nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio);

                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {

                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d : Radio Type could not be obtained\r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[6]))
                        {
                            continue;

                        }
                        else
                        {
                            /*Coverity Change Begins */
                            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex (u4WtpProfileId, u1RadioIndex, &i4RadioIfIndex) != SNMP_SUCCESS)
                            {
                                CliPrintf (CliHandle,
                                           "\r\n For Radio %d: Invalid Index\r\n",
                                           u1RadioIndex);
                            }
                            /*Coverity Change Ends */
                            WSSCFG_FILL_DOT11MULTIDOMAINCAPABILITYTABLE_ARGS
                                ((&WsscfgSetDot11MultiDomainCapabilityEntry),
                                 (&WsscfgIsSetDot11MultiDomainCapabilityEntry),
                                 args[0], args[1], args[2], args[3],
                                 pi4RadioIfIndex);
                        }
                    }
                }
                else
                {
                    /*Coverity Change Begins */
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u1RadioId,
                         &i4RadioIfIndex) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n For Radio %d: Invalid Index\r\n",
                                   u1RadioId);
                    }
                    /*Coverity Change Ends */
                    if (nmhGetFsDot11RadioType
                        (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d: Radio Type could not be obtained\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }

                    if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[24]))
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    else
                    {
                        WSSCFG_FILL_DOT11MULTIDOMAINCAPABILITYTABLE_ARGS ((&WsscfgSetDot11MultiDomainCapabilityEntry), (&WsscfgIsSetDot11MultiDomainCapabilityEntry), args[0], args[1], args[2], args[3], pi4RadioIfIndex);

                    }
                }
            }

            i4RetStatus =
                WsscfgCliSetDot11MultiDomainCapabilityTable (CliHandle,
                                                             (&WsscfgSetDot11MultiDomainCapabilityEntry),
                                                             (&WsscfgIsSetDot11MultiDomainCapabilityEntry));
            break;

        case CLI_WSSCFG_DOT11SPECTRUMMANAGEMENTTABLE:
            MEMSET (&WsscfgSetDot11SpectrumManagementEntry, 0,
                    sizeof (tWsscfgDot11SpectrumManagementEntry));
            MEMSET (&WsscfgIsSetDot11SpectrumManagementEntry, 0,
                    sizeof (tWsscfgIsSetDot11SpectrumManagementEntry));

            if (args[3] != NULL)
            {
                nmhGetCapwapDot11WlanProfileIfIndex (CLI_PTR_TO_U4
                                                     (*args[3]),
                                                     &i4WlanIfIndex);
            }
            WSSCFG_FILL_DOT11SPECTRUMMANAGEMENTTABLE_ARGS ((&WsscfgSetDot11SpectrumManagementEntry), (&WsscfgIsSetDot11SpectrumManagementEntry), args[0], args[1], args[2], pi4WlanIfIndex);

            i4RetStatus =
                WsscfgCliSetDot11SpectrumManagementTable (CliHandle,
                                                          (&WsscfgSetDot11SpectrumManagementEntry),
                                                          (&WsscfgIsSetDot11SpectrumManagementEntry));
            break;

        case CLI_WSSCFG_DOT11REGULATORYCLASSESTABLE:
            MEMSET (&WsscfgSetDot11RegulatoryClassesEntry, 0,
                    sizeof (tWsscfgDot11RegulatoryClassesEntry));
            MEMSET (&WsscfgIsSetDot11RegulatoryClassesEntry, 0,
                    sizeof (tWsscfgIsSetDot11RegulatoryClassesEntry));

            WSSCFG_FILL_DOT11REGULATORYCLASSESTABLE_ARGS ((&WsscfgSetDot11RegulatoryClassesEntry), (&WsscfgIsSetDot11RegulatoryClassesEntry), args[0], args[1], args[2], args[3]);

            i4RetStatus =
                WsscfgCliSetDot11RegulatoryClassesTable (CliHandle,
                                                         (&WsscfgSetDot11RegulatoryClassesEntry),
                                                         (&WsscfgIsSetDot11RegulatoryClassesEntry));
            break;

        case CLI_WSSCFG_DOT11OPERATIONTABLE:
            MEMSET (&WsscfgSetDot11OperationEntry, 0,
                    sizeof (tWsscfgDot11OperationEntry));
            MEMSET (&WsscfgIsSetDot11OperationEntry, 0,
                    sizeof (tWsscfgIsSetDot11OperationEntry));

            if (args[3] != NULL)
            {
                i4RetStatus =
                    WsscfgCliSetFragmentationThreshold (CliHandle,
                                                        (INT4) *args[20],
                                                        *args[3],
                                                        (UINT1 *) args[18],
                                                        args[19]);
                break;
            }
            if (args[18] == NULL)
            {
                if (nmhGetFirstIndexFsCapwapWirelessBindingTable
                    (&nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                else
                {
                    do
                    {
                        currentProfileId = nextProfileId;
                        u4currentBindingId = u4nextBindingId;
                        if ((currentProfileId == 0)
                            || (u4currentBindingId == 0))
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                        u4GetRadioType = (INT4) *args[20];
                        i4RetStatus = WssCfgGetDot11RadioIfIndex (nextProfileId,
                                                                  u4nextBindingId,
                                                                  u4GetRadioType,
                                                                  &i4RadioIfIndex);

                        if (i4RetStatus == OSIX_FAILURE)
                        {
                            continue;

                        }
                        else
                        {
                            WSSCFG_FILL_DOT11OPERATIONTABLE_ARGS ((&WsscfgSetDot11OperationEntry), (&WsscfgIsSetDot11OperationEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], pi4RadioIfIndex);

                            if (WsscfgCliSetDot11OperationTable
                                (CliHandle,
                                 (&WsscfgSetDot11OperationEntry),
                                 (&WsscfgIsSetDot11OperationEntry)) !=
                                OSIX_SUCCESS)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nFor Radio %d :  Configuration failed \r\n",
                                           u4nextBindingId);
                                continue;
                            }
                        }
                    }
                    while (nmhGetNextIndexFsCapwapWirelessBindingTable
                           (currentProfileId, &nextProfileId,
                            u4currentBindingId,
                            &u4nextBindingId) == SNMP_SUCCESS);
                }
            }
            else
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[18], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                u1RadioId = CLI_PTR_TO_U4 (*args[19]);

                if (u1RadioId == 0)
                {
                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "\r\nInvalid Model number\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFailed to fetch number of radios\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {
                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d: Invalid Index \r\n",
                                       u1RadioIndex);
                            continue;
                        }
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d: Radio Type could not be obtained \r\n",
                                       u1RadioIndex);
                            continue;
                        }
                        if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[20]))
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Radio Type conflicting\r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                        else
                        {
                            WSSCFG_FILL_DOT11OPERATIONTABLE_ARGS ((&WsscfgSetDot11OperationEntry), (&WsscfgIsSetDot11OperationEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], pi4RadioIfIndex);

                            if (WsscfgCliSetDot11OperationTable
                                (CliHandle,
                                 (&WsscfgSetDot11OperationEntry),
                                 (&WsscfgIsSetDot11OperationEntry)) !=
                                OSIX_SUCCESS)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nFor Radio %d:  Configuration update failed \r\n",
                                           u1RadioIndex);
                                continue;
                            }
                        }
                    }
                }
                else
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u1RadioId,
                         &i4RadioIfIndex) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "Radio Id Not Present\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                    }
                    if (nmhGetFsDot11RadioType
                        (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d: Radio Type could not be obtained\r\n",
                                   i4RadioIfIndex);
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[20]))
                    {
                        CliPrintf (CliHandle, "Conflicting Radio Type\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    else
                    {
                        WSSCFG_FILL_DOT11OPERATIONTABLE_ARGS ((&WsscfgSetDot11OperationEntry), (&WsscfgIsSetDot11OperationEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], pi4RadioIfIndex);

                        i4RetStatus =
                            WsscfgCliSetDot11OperationTable (CliHandle,
                                                             (&WsscfgSetDot11OperationEntry),
                                                             (&WsscfgIsSetDot11OperationEntry));
                    }
                }
            }

            break;

        case CLI_WSSCFG_DOT11GROUPADDRESSESTABLE:
            MEMSET (&WsscfgSetDot11GroupAddressesEntry, 0,
                    sizeof (tWsscfgDot11GroupAddressesEntry));
            MEMSET (&WsscfgIsSetDot11GroupAddressesEntry, 0,
                    sizeof (tWsscfgIsSetDot11GroupAddressesEntry));

            WSSCFG_FILL_DOT11GROUPADDRESSESTABLE_ARGS ((&WsscfgSetDot11GroupAddressesEntry), (&WsscfgIsSetDot11GroupAddressesEntry), args[0], args[1], args[2], args[3]);

            i4RetStatus =
                WsscfgCliSetDot11GroupAddressesTable (CliHandle,
                                                      (&WsscfgSetDot11GroupAddressesEntry),
                                                      (&WsscfgIsSetDot11GroupAddressesEntry));
            break;

        case CLI_WSSCFG_DOT11EDCATABLE:
            MEMSET (&WsscfgSetDot11EDCAEntry, 0,
                    sizeof (tWsscfgDot11EDCAEntry));
            MEMSET (&WsscfgIsSetDot11EDCAEntry, 0,
                    sizeof (tWsscfgIsSetDot11EDCAEntry));

            /* WSS Changes Starts */
            if (args[8] != NULL)
            {

                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[8], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n WTP Profile not found.\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                u1RadioId = CLI_PTR_TO_U4 (*args[9]);

                if (u1RadioId == 0)
                {
                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "\r\nInvalid Model number\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }

                    if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFailed to fetch number of radios\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }

                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {

                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d: Invalid Index \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d: Radio Type could not be obtained \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[10]))
                        {
                            continue;
                        }
                        else
                        {
                            WSSCFG_FILL_DOT11EDCATABLE_ARGS ((&WsscfgSetDot11EDCAEntry), (&WsscfgIsSetDot11EDCAEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], pi4RadioIfIndex);
                            if ((WsscfgCliSetDot11EDCATable
                                 (CliHandle, (&WsscfgSetDot11EDCAEntry),
                                  (&WsscfgIsSetDot11EDCAEntry))) !=
                                OSIX_SUCCESS)
                            {

                                CliPrintf (CliHandle,
                                           "\r\nFor Radio %d: Configuration update failed\r\n",
                                           u1RadioIndex);
                                continue;

                            }
                        }
                    }
                }
                else
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u1RadioId,
                         &i4RadioIfIndex) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d: Invalid Index\r\n",
                                   u1RadioId);
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }

                    if (nmhGetFsDot11RadioType
                        (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d: Radio Type could not be obtained\r\n",
                                   i4RadioIfIndex);
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[10]))
                    {
                        break;
                    }
                    else
                    {
                        WSSCFG_FILL_DOT11EDCATABLE_ARGS ((&WsscfgSetDot11EDCAEntry), (&WsscfgIsSetDot11EDCAEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], pi4RadioIfIndex);
                        i4RetStatus =
                            WsscfgCliSetDot11EDCATable (CliHandle,
                                                        (&WsscfgSetDot11EDCAEntry),
                                                        (&WsscfgIsSetDot11EDCAEntry));
                    }

                }

            }
            else if (args[8] == NULL)
            {
                if (nmhGetFirstIndexFsCapwapWirelessBindingTable
                    (&nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                else
                {
                    do
                    {
                        currentProfileId = nextProfileId;
                        u4currentBindingId = u4nextBindingId;
                        if ((currentProfileId == 0)
                            || (u4currentBindingId == 0))
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                        u4GetRadioType = (INT4) *args[10];
                        i4RetStatus = WssCfgGetDot11RadioIfIndex (nextProfileId,
                                                                  u4nextBindingId,
                                                                  u4GetRadioType,
                                                                  &i4RadioIfIndex);

                        if (i4RetStatus == OSIX_FAILURE)
                        {
                            continue;
                        }
                        else
                        {
                            WSSCFG_FILL_DOT11EDCATABLE_ARGS ((&WsscfgSetDot11EDCAEntry), (&WsscfgIsSetDot11EDCAEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], pi4RadioIfIndex);

                            if ((WsscfgCliSetDot11EDCATable
                                 (CliHandle, (&WsscfgSetDot11EDCAEntry),
                                  (&WsscfgIsSetDot11EDCAEntry))) !=
                                OSIX_SUCCESS)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nFor Ap (%d) Radio %d: Configuration update failed\r\n",
                                           currentProfileId,
                                           u4currentBindingId);
                                continue;
                            }
                        }

                    }
                    while (nmhGetNextIndexFsCapwapWirelessBindingTable
                           (currentProfileId, &nextProfileId,
                            u4currentBindingId,
                            &u4nextBindingId) == SNMP_SUCCESS);
                }

            }
            break;

        case CLI_WSSCFG_DOT11QAPEDCATABLE:
            MEMSET (&WsscfgSetDot11QAPEDCAEntry, 0,
                    sizeof (tWsscfgDot11QAPEDCAEntry));
            MEMSET (&WsscfgIsSetDot11QAPEDCAEntry, 0,
                    sizeof (tWsscfgIsSetDot11QAPEDCAEntry));

            WSSCFG_FILL_DOT11QAPEDCATABLE_ARGS ((&WsscfgSetDot11QAPEDCAEntry),
                                                (&WsscfgIsSetDot11QAPEDCAEntry),
                                                args[0], args[1], args[2],
                                                args[3], args[4], args[5],
                                                args[6], args[7]);

            i4RetStatus =
                WsscfgCliSetDot11QAPEDCATable (CliHandle,
                                               (&WsscfgSetDot11QAPEDCAEntry),
                                               (&WsscfgIsSetDot11QAPEDCAEntry));
            break;

        case CLI_WSSCFG_DOT11PHYOPERATIONTABLE:
            MEMSET (&WsscfgSetDot11PhyOperationEntry, 0,
                    sizeof (tWsscfgDot11PhyOperationEntry));
            MEMSET (&WsscfgIsSetDot11PhyOperationEntry, 0,
                    sizeof (tWsscfgIsSetDot11PhyOperationEntry));

            WSSCFG_FILL_DOT11PHYOPERATIONTABLE_ARGS ((&WsscfgSetDot11PhyOperationEntry), (&WsscfgIsSetDot11PhyOperationEntry), args[0], args[1]);

            i4RetStatus =
                WsscfgCliSetDot11PhyOperationTable (CliHandle,
                                                    (&WsscfgSetDot11PhyOperationEntry),
                                                    (&WsscfgIsSetDot11PhyOperationEntry));
            break;

        case CLI_WSSCFG_DOT11PHYANTENNATABLE:
            MEMSET (&WsscfgSetDot11PhyAntennaEntry, 0,
                    sizeof (tWsscfgDot11PhyAntennaEntry));
            MEMSET (&WsscfgIsSetDot11PhyAntennaEntry, 0,
                    sizeof (tWsscfgIsSetDot11PhyAntennaEntry));

            WSSCFG_FILL_DOT11PHYANTENNATABLE_ARGS ((&WsscfgSetDot11PhyAntennaEntry), (&WsscfgIsSetDot11PhyAntennaEntry), args[0], args[1], args[2]);

            i4RetStatus =
                WsscfgCliSetDot11PhyAntennaTable (CliHandle,
                                                  (&WsscfgSetDot11PhyAntennaEntry),
                                                  (&WsscfgIsSetDot11PhyAntennaEntry));
            break;

        case CLI_WSSCFG_DOT11PHYTXPOWERTABLE:
            MEMSET (&WsscfgSetDot11PhyTxPowerEntry, 0,
                    sizeof (tWsscfgDot11PhyTxPowerEntry));
            MEMSET (&WsscfgIsSetDot11PhyTxPowerEntry, 0,
                    sizeof (tWsscfgIsSetDot11PhyTxPowerEntry));

            /* When Profile name is received as input get the profile id
             * from mapping table */

            if (CapwapGetWtpProfileIdFromProfileName
                ((UINT1 *) args[2], &u4WtpProfileId) != OSIX_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            if (args[4] != NULL)
            {
                u1RadioId = CLI_PTR_TO_U4 (*args[4]);
            }

            if (u1RadioId == 0)
            {

                if ((nmhGetCapwapBaseWtpProfileRowStatus
                     (u4WtpProfileId, (INT4 *) &u4RowStatus) != SNMP_SUCCESS)
                    || (u4RowStatus != ACTIVE))
                {
                    CliPrintf (CliHandle,
                               "\r\n WTP Profile table not active. \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                    (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\nInvalid Model Number\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if ((i4RetStatus =
                     nmhGetFsNoOfRadio (&ModelName,
                                        &u4NoOfRadio)) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nGetting Number of Radios Failed\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

                for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                     u1RadioIndex++)
                {

                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u1RadioIndex,
                         &i4RadioIfIndex) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d : Getting RadioIf Index Failed\r\n",
                                   u1RadioIndex);
                        continue;
                    }

                    if (nmhGetFsDot11RadioType (i4RadioIfIndex, &u4GetRadioType)
                        != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d: Getting RadioIf Type Failed \r\n",
                                   u1RadioIndex);
                        continue;
                    }
                    if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[3]))
                    {
                        continue;
                    }
                    else
                    {

                        WSSCFG_FILL_DOT11PHYTXPOWERTABLE_ARGS ((&WsscfgSetDot11PhyTxPowerEntry), (&WsscfgIsSetDot11PhyTxPowerEntry), args[0], pi4RadioIfIndex);

                        if (WsscfgCliSetDot11PhyTxPowerTable
                            (CliHandle, (&WsscfgSetDot11PhyTxPowerEntry),
                             (&WsscfgIsSetDot11PhyTxPowerEntry)) !=
                            OSIX_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d : Values can't be successfully set\r\n",
                                       u1RadioIndex);
                            continue;

                        }
                    }
                }
            }
            else
            {
                if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                    (u4WtpProfileId, u1RadioId,
                     &i4RadioIfIndex) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nFor Radio %d: Getting RadioIf Index Failed \r\n",
                               u1RadioId);
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

                if (nmhGetFsDot11RadioType (i4RadioIfIndex, &u4GetRadioType)
                    != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nFor Radio %d: Getting RadioIf Type Failed \r\n",
                               u1RadioId);
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[3]))
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                else
                {
                    WSSCFG_FILL_DOT11PHYTXPOWERTABLE_ARGS ((&WsscfgSetDot11PhyTxPowerEntry), (&WsscfgIsSetDot11PhyTxPowerEntry), args[0], pi4RadioIfIndex);

                    if (WsscfgCliSetDot11PhyTxPowerTable
                        (CliHandle, (&WsscfgSetDot11PhyTxPowerEntry),
                         (&WsscfgIsSetDot11PhyTxPowerEntry)) != OSIX_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d: Values can't be successfully set\r\n",
                                   u1RadioId);
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;

                    }
                }
            }
            break;

        case CLI_WSSCFG_DOT11PHYFHSSTABLE:
            MEMSET (&WsscfgSetDot11PhyFHSSEntry, 0,
                    sizeof (tWsscfgDot11PhyFHSSEntry));
            MEMSET (&WsscfgIsSetDot11PhyFHSSEntry, 0,
                    sizeof (tWsscfgIsSetDot11PhyFHSSEntry));

            WSSCFG_FILL_DOT11PHYFHSSTABLE_ARGS ((&WsscfgSetDot11PhyFHSSEntry),
                                                (&WsscfgIsSetDot11PhyFHSSEntry),
                                                args[0], args[1], args[2],
                                                args[3], args[4], args[5],
                                                args[6], args[7], args[8],
                                                args[9], args[10], args[11],
                                                args[12]);

            i4RetStatus =
                WsscfgCliSetDot11PhyFHSSTable (CliHandle,
                                               (&WsscfgSetDot11PhyFHSSEntry),
                                               (&WsscfgIsSetDot11PhyFHSSEntry));
            break;

        case CLI_WSSCFG_DOT11PHYDSSSTABLE:
            MEMSET (&WsscfgSetDot11PhyDSSSEntry, 0,
                    sizeof (tWsscfgDot11PhyDSSSEntry));
            MEMSET (&WsscfgIsSetDot11PhyDSSSEntry, 0,
                    sizeof (tWsscfgIsSetDot11PhyDSSSEntry));

            /* When Profile name is received as input get the profile id
             * from mapping table and pass the profile name as NULL */
            if (CapwapGetWtpProfileIdFromProfileName
                ((UINT1 *) args[4], &u4WtpProfileId) != OSIX_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            if (args[5] != NULL)
            {
                u1RadioId = CLI_PTR_TO_U4 (*args[5]);
            }

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u1RadioId, &i4RadioIfIndex) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r\nFor Radio %d : Getting RadioIf Index Failed\r\n",
                           u1RadioId);
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            if (nmhGetFsDot11RadioType (i4RadioIfIndex, &u4RadioIfType)
                != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r\nFor Radio %d : Getting RadioIf Type Failed\r\n",
                           u1RadioId);
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }
            if (args[6] != NULL)
            {
                if ((u4RadioIfType == CLI_RADIO_TYPEB)
                    || (u4RadioIfType == CLI_RADIO_TYPEBG)
                    || (u4RadioIfType == CLI_RADIO_TYPEBGN))
                {
                    if (*args[6] != CLI_RADIO_TYPEB)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d : Radio Type Mismatch\r\n",
                                   u1RadioId);
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;

                    }
                }
                else if (u4RadioIfType == CLI_RADIO_TYPEG)
                {
                    if (*args[6] != CLI_RADIO_TYPEG)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d : Radio Type Mismatch\r\n",
                                   u1RadioId);
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;

                    }
                }
            }
            else
            {
                if ((u4RadioIfType == CLI_RADIO_TYPEA) ||
                    (u4RadioIfType == CLI_RADIO_TYPEAN))
                {
                    CliPrintf (CliHandle,
                               "\r\nFor Radio %d : Radio Type Mismatch\r\n",
                               u1RadioId);
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }
            WSSCFG_FILL_DOT11PHYDSSSTABLE_ARGS ((&WsscfgSetDot11PhyDSSSEntry),
                                                (&WsscfgIsSetDot11PhyDSSSEntry),
                                                args[0], args[1], args[2],
                                                pi4RadioIfIndex);

            if (WsscfgCliSetDot11PhyDSSSTable
                (CliHandle, (&WsscfgSetDot11PhyDSSSEntry),
                 (&WsscfgIsSetDot11PhyDSSSEntry)) != OSIX_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r\nFor Radio %d : Values cant be successfully set\r\n",
                           u1RadioId);
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;

            }

            break;

        case CLI_WSSCFG_DOT11PHYIRTABLE:
            MEMSET (&WsscfgSetDot11PhyIREntry, 0,
                    sizeof (tWsscfgDot11PhyIREntry));
            MEMSET (&WsscfgIsSetDot11PhyIREntry, 0,
                    sizeof (tWsscfgIsSetDot11PhyIREntry));

            WSSCFG_FILL_DOT11PHYIRTABLE_ARGS ((&WsscfgSetDot11PhyIREntry),
                                              (&WsscfgIsSetDot11PhyIREntry),
                                              args[0], args[1], args[2],
                                              args[3], args[4]);

            i4RetStatus =
                WsscfgCliSetDot11PhyIRTable (CliHandle,
                                             (&WsscfgSetDot11PhyIREntry),
                                             (&WsscfgIsSetDot11PhyIREntry));
            break;

        case CLI_WSSCFG_DOT11ANTENNASLISTTABLE:
            MEMSET (&WsscfgSetDot11AntennasListEntry, 0,
                    sizeof (tWsscfgDot11AntennasListEntry));
            MEMSET (&WsscfgIsSetDot11AntennasListEntry, 0,
                    sizeof (tWsscfgIsSetDot11AntennasListEntry));

            /* WSS Changes Starts */
            if (args[5] != NULL)
            {

                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[5], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n WTP Profile not found \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                u1RadioId = CLI_PTR_TO_U4 (*args[6]);

                if (u1RadioId == 0)
                {
                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "\r\nInvalid Model number\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }

                    if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFailed to fetch number of radios\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;

                    }

                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {

                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d: Invalid Index \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d: Radio Type could not be obtained \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[7]))
                        {
                            continue;
                        }
                        else
                        {
                            WSSCFG_FILL_DOT11ANTENNASLISTTABLE_ARGS ((&WsscfgSetDot11AntennasListEntry), (&WsscfgIsSetDot11AntennasListEntry), args[0], args[1], args[2], args[3], pi4RadioIfIndex);
                            if ((WsscfgCliSetDot11AntennasListTable
                                 (CliHandle,
                                  (&WsscfgSetDot11AntennasListEntry),
                                  (&WsscfgIsSetDot11AntennasListEntry)))
                                != OSIX_SUCCESS)
                            {

                                CliPrintf (CliHandle,
                                           "\r\nFor Radio %d: Configuration update failed\r\n",
                                           u1RadioIndex);
                                continue;

                            }
                        }
                    }
                }
                else
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u1RadioId,
                         &i4RadioIfIndex) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d: Invalid Index\r\n",
                                   u1RadioId);
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }

                    if (nmhGetFsDot11RadioType
                        (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d: Radio Type could not be obtained\r\n",
                                   i4RadioIfIndex);
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[7]))
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    else
                    {
                        WSSCFG_FILL_DOT11ANTENNASLISTTABLE_ARGS ((&WsscfgSetDot11AntennasListEntry), (&WsscfgIsSetDot11AntennasListEntry), args[0], args[1], args[2], args[3], pi4RadioIfIndex);
                        i4RetStatus =
                            WsscfgCliSetDot11AntennasListTable (CliHandle,
                                                                (&WsscfgSetDot11AntennasListEntry),
                                                                (&WsscfgIsSetDot11AntennasListEntry));
                    }

                }

            }
            else if (args[5] == NULL)
            {
                if (nmhGetFirstIndexFsCapwapWirelessBindingTable
                    (&nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                else
                {
                    do
                    {
                        currentProfileId = nextProfileId;
                        u4currentBindingId = u4nextBindingId;
                        if ((currentProfileId == 0)
                            || (u4currentBindingId == 0))
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                        u4GetRadioType = (INT4) *args[7];
                        i4RetStatus = WssCfgGetDot11RadioIfIndex (nextProfileId,
                                                                  u4nextBindingId,
                                                                  u4GetRadioType,
                                                                  &i4RadioIfIndex);

                        if (i4RetStatus == OSIX_FAILURE)
                        {
                            continue;
                        }
                        else
                        {
                            WSSCFG_FILL_DOT11ANTENNASLISTTABLE_ARGS ((&WsscfgSetDot11AntennasListEntry), (&WsscfgIsSetDot11AntennasListEntry), args[0], args[1], args[2], args[3], pi4RadioIfIndex);
                            if ((WsscfgCliSetDot11AntennasListTable
                                 (CliHandle, (&WsscfgSetDot11AntennasListEntry),
                                  (&WsscfgIsSetDot11AntennasListEntry))) !=
                                OSIX_SUCCESS)
                            {
                                CliPrintf (CliHandle, "\r\n Ap(%d) Radio"
                                           "%d :  Configuration failed\r\n",
                                           currentProfileId,
                                           u4currentBindingId);
                                continue;
                            }
                        }

                    }
                    while (nmhGetNextIndexFsCapwapWirelessBindingTable
                           (currentProfileId, &nextProfileId,
                            u4currentBindingId,
                            &u4nextBindingId) == SNMP_SUCCESS);
                }

            }
            break;

        case CLI_WSSCFG_DOT11PHYOFDMTABLE:
            MEMSET (&WsscfgSetDot11PhyOFDMEntry, 0,
                    sizeof (tWsscfgDot11PhyOFDMEntry));
            MEMSET (&WsscfgIsSetDot11PhyOFDMEntry, 0,
                    sizeof (tWsscfgIsSetDot11PhyOFDMEntry));

            /* When Profile name is received as input get the profile id
             * from mapping table and pass the profile name as NULL */
            if (CapwapGetWtpProfileIdFromProfileName
                ((UINT1 *) args[5], &u4WtpProfileId) != OSIX_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            if (args[6] != NULL)
            {
                u1RadioId = CLI_PTR_TO_U4 (*args[6]);
            }

            if (u1RadioId == 0)
            {

                if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                    (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nGetting Model Number Failed\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                    OSIX_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\nGetting Number of Radios Failed\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;

                }

                for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                     u1RadioIndex++)
                {

                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u1RadioIndex,
                         &i4RadioIfIndex) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d : Getting RadioIf Index Failed\r\n",
                                   u1RadioIndex);
                        continue;
                    }

                    if (nmhGetFsDot11RadioType (i4RadioIfIndex, &u4RadioIfType)
                        != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d : Getting Radio Type Failed\r\n",
                                   u1RadioIndex);
                        continue;
                    }

                    if ((u4RadioIfType == CLI_RADIO_TYPEA)
                        || (u4RadioIfType == CLI_RADIO_TYPEAN))
                    {
                        WSSCFG_FILL_DOT11PHYOFDMTABLE_ARGS ((&WsscfgSetDot11PhyOFDMEntry), (&WsscfgIsSetDot11PhyOFDMEntry), args[0], args[1], args[2], args[3], pi4RadioIfIndex);

                        if (WsscfgCliSetDot11PhyOFDMTable
                            (CliHandle, (&WsscfgSetDot11PhyOFDMEntry),
                             (&WsscfgIsSetDot11PhyOFDMEntry)) != OSIX_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d : Values can't be successfully set\r\n",
                                       u1RadioIndex);
                            continue;

                        }
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            else
            {
                if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                    (u4WtpProfileId, u1RadioId,
                     &i4RadioIfIndex) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nFor Radio %d : Getting Virtual Radio If Index Failed\r\n",
                               u1RadioId);
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

                if (nmhGetFsDot11RadioType (i4RadioIfIndex, &u4RadioIfType)
                    != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nFor Radio %d : Getting RadioIf Type Failed\r\n",
                               u1RadioId);
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

                if ((u4RadioIfType == CLI_RADIO_TYPEA)
                    || (u4RadioIfType == CLI_RADIO_TYPEAN))
                {
                    WSSCFG_FILL_DOT11PHYOFDMTABLE_ARGS ((&WsscfgSetDot11PhyOFDMEntry), (&WsscfgIsSetDot11PhyOFDMEntry), args[0], args[1], args[2], args[3], pi4RadioIfIndex);

                    if (WsscfgCliSetDot11PhyOFDMTable
                        (CliHandle, (&WsscfgSetDot11PhyOFDMEntry),
                         (&WsscfgIsSetDot11PhyOFDMEntry)) != OSIX_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d : Configuration failed\r\n",
                                   u1RadioId);
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r\nFor Radio %d : Radio Type Mismatch\r\n",
                               u1RadioId);
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }
            break;

        case CLI_WSSCFG_DOT11HOPPINGPATTERNTABLE:
            MEMSET (&WsscfgSetDot11HoppingPatternEntry, 0,
                    sizeof (tWsscfgDot11HoppingPatternEntry));
            MEMSET (&WsscfgIsSetDot11HoppingPatternEntry, 0,
                    sizeof (tWsscfgIsSetDot11HoppingPatternEntry));

            WSSCFG_FILL_DOT11HOPPINGPATTERNTABLE_ARGS ((&WsscfgSetDot11HoppingPatternEntry), (&WsscfgIsSetDot11HoppingPatternEntry), args[0], args[1], args[2]);

            i4RetStatus =
                WsscfgCliSetDot11HoppingPatternTable (CliHandle,
                                                      (&WsscfgSetDot11HoppingPatternEntry),
                                                      (&WsscfgIsSetDot11HoppingPatternEntry));
            break;

        case CLI_WSSCFG_DOT11PHYERPTABLE:
            MEMSET (&WsscfgSetDot11PhyERPEntry, 0,
                    sizeof (tWsscfgDot11PhyERPEntry));
            MEMSET (&WsscfgIsSetDot11PhyERPEntry, 0,
                    sizeof (tWsscfgIsSetDot11PhyERPEntry));

            WSSCFG_FILL_DOT11PHYERPTABLE_ARGS ((&WsscfgSetDot11PhyERPEntry),
                                               (&WsscfgIsSetDot11PhyERPEntry),
                                               args[0], args[1], args[2],
                                               args[3], args[4]);

            i4RetStatus =
                WsscfgCliSetDot11PhyERPTable (CliHandle,
                                              (&WsscfgSetDot11PhyERPEntry),
                                              (&WsscfgIsSetDot11PhyERPEntry));
            break;

        case CLI_WSSCFG_DOT11RSNACONFIGTABLE:
            MEMSET (&WsscfgSetDot11RSNAConfigEntry, 0,
                    sizeof (tWsscfgDot11RSNAConfigEntry));
            MEMSET (&WsscfgIsSetDot11RSNAConfigEntry, 0,
                    sizeof (tWsscfgIsSetDot11RSNAConfigEntry));

            WSSCFG_FILL_DOT11RSNACONFIGTABLE_ARGS ((&WsscfgSetDot11RSNAConfigEntry), (&WsscfgIsSetDot11RSNAConfigEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17], args[18], args[19], args[20], args[21]);

            i4RetStatus =
                WsscfgCliSetDot11RSNAConfigTable (CliHandle,
                                                  (&WsscfgSetDot11RSNAConfigEntry),
                                                  (&WsscfgIsSetDot11RSNAConfigEntry));
            break;

        case CLI_WSSCFG_DOT11RSNACONFIGPAIRWISECIPHERSTABLE:
            MEMSET (&WsscfgSetDot11RSNAConfigPairwiseCiphersEntry, 0,
                    sizeof (tWsscfgDot11RSNAConfigPairwiseCiphersEntry));
            MEMSET (&WsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry, 0,
                    sizeof (tWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry));

            WSSCFG_FILL_DOT11RSNACONFIGPAIRWISECIPHERSTABLE_ARGS ((&WsscfgSetDot11RSNAConfigPairwiseCiphersEntry), (&WsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry), args[0], args[1], args[2]);

            i4RetStatus =
                WsscfgCliSetDot11RSNAConfigPairwiseCiphersTable (CliHandle,
                                                                 (&WsscfgSetDot11RSNAConfigPairwiseCiphersEntry),
                                                                 (&WsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry));
            break;

        case CLI_WSSCFG_DOT11RSNACONFIGAUTHENTICATIONSUITESTABLE:
            MEMSET (&WsscfgSetDot11RSNAConfigAuthenticationSuitesEntry, 0,
                    sizeof (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry));
            MEMSET (&WsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry,
                    0,
                    sizeof
                    (tWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry));

            WSSCFG_FILL_DOT11RSNACONFIGAUTHENTICATIONSUITESTABLE_ARGS ((&WsscfgSetDot11RSNAConfigAuthenticationSuitesEntry), (&WsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry), args[0], args[1]);

            i4RetStatus =
                WsscfgCliSetDot11RSNAConfigAuthenticationSuitesTable
                (CliHandle,
                 (&WsscfgSetDot11RSNAConfigAuthenticationSuitesEntry),
                 (&WsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry));
            break;
        case CLI_WSSCFG_FSDOT11STATIONCONFIGTABLE:
            MEMSET (&WsscfgSetFsDot11StationConfigEntry, 0,
                    sizeof (tWsscfgFsDot11StationConfigEntry));
            MEMSET (&WsscfgIsSetFsDot11StationConfigEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11StationConfigEntry));

            if (args[2] != NULL)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (CLI_PTR_TO_U4 (*args[2]), &i4WlanIfIndex) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nGetting Wlan Profile Index failed\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = CLI_FAILURE;
                    break;

                }
            }
            if (args[4] != NULL)
            {
                if (WssCfgGetWlanIfIndexfromSSID
                    ((UINT1 *) args[4],
                     (UINT4 *) &i4WlanIfIndex) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\nGetting index failed\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = CLI_FAILURE;
                    break;

                }
            }
            WSSCFG_FILL_FSDOT11STATIONCONFIGTABLE_ARGS ((&WsscfgSetFsDot11StationConfigEntry), (&WsscfgIsSetFsDot11StationConfigEntry), args[0], args[1], args[5], pi4WlanIfIndex);

            i4RetStatus =
                WsscfgCliSetFsDot11StationConfigTable (CliHandle,
                                                       (&WsscfgSetFsDot11StationConfigEntry),
                                                       (&WsscfgIsSetFsDot11StationConfigEntry));
            break;

        case CLI_WSSCFG_FSDOT11CAPABILITYPROFILETABLE:
            MEMSET (&WsscfgSetFsDot11CapabilityProfileEntry, 0,
                    sizeof (tWsscfgFsDot11CapabilityProfileEntry));
            MEMSET (&WsscfgIsSetFsDot11CapabilityProfileEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11CapabilityProfileEntry));

            CLI_GET_CAPABID ((INT1 *) au1PromptName);
            pu1Name = (UINT1 *) STRTOK_R (pu1PromptName, "/", &saveptr);
            if (pu1Name == NULL)
            {
                CliPrintf (CliHandle, "\r\nGetting Prompt Name Failed\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = CLI_FAILURE;
                break;

            }
            u4Len = STRLEN (pu1Name);

            WSSCFG_FILL_FSDOT11CAPABILITYPROFILETABLE_ARGS ((&WsscfgSetFsDot11CapabilityProfileEntry), (&WsscfgIsSetFsDot11CapabilityProfileEntry), pu1Name, &u4Len, args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17], args[18], args[19], args[20]);

            i4RetStatus =
                WsscfgCliSetFsDot11CapabilityProfileTable (CliHandle,
                                                           (&WsscfgSetFsDot11CapabilityProfileEntry),
                                                           (&WsscfgIsSetFsDot11CapabilityProfileEntry));
            break;
        case CLI_WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE:
            MEMSET (&WsscfgSetFsDot11AuthenticationProfileEntry, 0,
                    sizeof (tWsscfgFsDot11AuthenticationProfileEntry));
            MEMSET (&WsscfgIsSetFsDot11AuthenticationProfileEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11AuthenticationProfileEntry));

            CLI_GET_AUTHID ((INT1 *) au1PromptName);
            pu1Name = (UINT1 *) STRTOK_R (pu1PromptName, "/", &saveptr);
            if (pu1Name == NULL)
            {
                CliPrintf (CliHandle, "\r\nGetting Prompt Name failed\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = CLI_FAILURE;
                break;

            }
            u4Len = STRLEN (pu1Name);

            WSSCFG_FILL_FSDOT11AUTHENTICATIONPROFILETABLE_ARGS ((&WsscfgSetFsDot11AuthenticationProfileEntry), (&WsscfgIsSetFsDot11AuthenticationProfileEntry), pu1Name, &u4Len, args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9]);

            i4RetStatus =
                WsscfgCliSetFsDot11AuthenticationProfileTable (CliHandle,
                                                               (&WsscfgSetFsDot11AuthenticationProfileEntry),
                                                               (&WsscfgIsSetFsDot11AuthenticationProfileEntry));
            break;

        case CLI_WSSCFG_FSSECURITYWEBAUTHGUESTINFOTABLE:
            MEMSET (&WsscfgSetFsSecurityWebAuthGuestInfoEntry, 0,
                    sizeof (tWsscfgFsSecurityWebAuthGuestInfoEntry));
            MEMSET (&WsscfgIsSetFsSecurityWebAuthGuestInfoEntry, 0,
                    sizeof (tWsscfgIsSetFsSecurityWebAuthGuestInfoEntry));
            if (args[4] != NULL)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (CLI_PTR_TO_U4 (*args[4]), &i4WlanIfIndex) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n Index does not exist \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (i4WlanIfIndex == 0)
                {
                    CliPrintf (CliHandle, "\r\n Invalid index\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                /* The below check would validate the WLAN ID Rowstatus and if
                 * exists, it would change the rowstatus value to NULL. Hence,
                 * the username will never be destroyed. The same check is not
                 * present when deleting it via GUI */
                if (CLI_PTR_TO_U4 (*args[6]) != DESTROY)
                {
                    if (nmhGetCapwapDot11WlanRowStatus
                        (*args[4], &i4rowStatus) == SNMP_SUCCESS)
                    {
                        args[6] = NULL;
                    }
                }

            }
            WSSCFG_FILL_FSSECURITYWEBAUTHGUESTINFOTABLE_ARGS ((&WsscfgSetFsSecurityWebAuthGuestInfoEntry), (&WsscfgIsSetFsSecurityWebAuthGuestInfoEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6]);
            i4RetStatus =
                WsscfgCliSetFsSecurityWebAuthGuestInfoTable (CliHandle,
                                                             (&WsscfgSetFsSecurityWebAuthGuestInfoEntry),
                                                             (&WsscfgIsSetFsSecurityWebAuthGuestInfoEntry));
            break;

        case CLI_WSSCFG_FSSTATIONQOSPARAMSTABLE:
            MEMSET (&WsscfgSetFsStationQosParamsEntry, 0,
                    sizeof (tWsscfgFsStationQosParamsEntry));
            MEMSET (&WsscfgIsSetFsStationQosParamsEntry, 0,
                    sizeof (tWsscfgIsSetFsStationQosParamsEntry));

            WSSCFG_FILL_FSSTATIONQOSPARAMSTABLE_ARGS ((&WsscfgSetFsStationQosParamsEntry), (&WsscfgIsSetFsStationQosParamsEntry), args[0], args[1], args[2], args[3]);

            i4RetStatus =
                WsscfgCliSetFsStationQosParamsTable (CliHandle,
                                                     (&WsscfgSetFsStationQosParamsEntry),
                                                     (&WsscfgIsSetFsStationQosParamsEntry));
            break;

        case CLI_WSSCFG_FSDOT11QAPTABLE:
            MEMSET (&WsscfgSetFsDot11QAPEntry, 0,
                    sizeof (tWsscfgFsDot11QAPEntry));
            MEMSET (&WsscfgIsSetFsDot11QAPEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11QAPEntry));

            /* WSS Changes Starts */
            if (args[5] != NULL)
            {

                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[5], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n WTP Profile not found.\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                u1RadioId = CLI_PTR_TO_U4 (*args[6]);

                if (u1RadioId == 0)
                {
                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "\r\nInvalid Model number\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }

                    if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFailed to fetch number of radios\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;

                    }

                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {

                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d: Invalid Index \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d: Radio Type could not be obtained \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[7]))
                        {
                            continue;
                        }
                        else
                        {
                            WSSCFG_FILL_FSDOT11QAPTABLE_ARGS ((&WsscfgSetFsDot11QAPEntry), (&WsscfgIsSetFsDot11QAPEntry), args[0], args[1], args[2], pi4RadioIfIndex, args[4]);
                            if ((WsscfgCliSetFsDot11QAPTable
                                 (CliHandle, (&WsscfgSetFsDot11QAPEntry),
                                  (&WsscfgIsSetFsDot11QAPEntry))) !=
                                OSIX_SUCCESS)
                            {

                                CliPrintf (CliHandle,
                                           "\r\nFor Radio %d: Configuration update failed\r\n",
                                           u1RadioIndex);
                                continue;

                            }
                        }
                    }
                }
                else
                {
                    nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u1RadioId, &i4RadioIfIndex);

                    if (nmhGetFsDot11RadioType
                        (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d: Radio Type could not be obtained\r\n",
                                   i4RadioIfIndex);
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[7]))
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    else
                    {
                        WSSCFG_FILL_FSDOT11QAPTABLE_ARGS ((&WsscfgSetFsDot11QAPEntry), (&WsscfgIsSetFsDot11QAPEntry), args[0], args[1], args[2], pi4RadioIfIndex, args[4]);
                        i4RetStatus =
                            WsscfgCliSetFsDot11QAPTable (CliHandle,
                                                         (&WsscfgSetFsDot11QAPEntry),
                                                         (&WsscfgIsSetFsDot11QAPEntry));
                    }

                }

            }
            else if (args[5] == NULL)
            {
                if (nmhGetFirstIndexFsCapwapWirelessBindingTable
                    (&nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                else
                {
                    do
                    {
                        currentProfileId = nextProfileId;
                        u4currentBindingId = u4nextBindingId;
                        if ((currentProfileId == 0)
                            || (u4currentBindingId == 0))
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                        u4GetRadioType = (INT4) *args[7];
                        i4RetStatus = WssCfgGetDot11RadioIfIndex (nextProfileId,
                                                                  u4nextBindingId,
                                                                  u4GetRadioType,
                                                                  &i4RadioIfIndex);

                        if (i4RetStatus == OSIX_FAILURE)
                        {
                            continue;
                        }
                        else
                        {

                            WSSCFG_FILL_FSDOT11QAPTABLE_ARGS ((&WsscfgSetFsDot11QAPEntry), (&WsscfgIsSetFsDot11QAPEntry), args[0], args[1], args[2], pi4RadioIfIndex, args[4]);
                            if ((WsscfgCliSetFsDot11QAPTable
                                 (CliHandle, (&WsscfgSetFsDot11QAPEntry),
                                  (&WsscfgIsSetFsDot11QAPEntry))) !=
                                OSIX_SUCCESS)
                            {
                                CliPrintf (CliHandle, "\r\n Ap(%d) Radio"
                                           "%d :  Configuration failed\r\n",
                                           currentProfileId,
                                           u4currentBindingId);
                                continue;
                            }

                        }

                    }
                    while (nmhGetNextIndexFsCapwapWirelessBindingTable
                           (currentProfileId, &nextProfileId,
                            u4currentBindingId,
                            &u4nextBindingId) == SNMP_SUCCESS);
                }

            }
            break;

        case CLI_WSSCFG_FSVLANISOLATIONTABLE:
            MEMSET (&WsscfgSetFsVlanIsolationEntry, 0,
                    sizeof (tWsscfgFsVlanIsolationEntry));
            MEMSET (&WsscfgIsSetFsVlanIsolationEntry, 0,
                    sizeof (tWsscfgIsSetFsVlanIsolationEntry));

            if (args[1] != NULL)
            {
                u4WlanProfileId = CLI_PTR_TO_U4 (*args[1]);
                nmhGetCapwapDot11WlanProfileIfIndex (u4WlanProfileId,
                                                     &i4WlanIfIndex);
            }

            WSSCFG_FILL_FSVLANISOLATIONTABLE_ARGS ((&WsscfgSetFsVlanIsolationEntry), (&WsscfgIsSetFsVlanIsolationEntry), args[0], pi4WlanIfIndex);

            i4RetStatus =
                WsscfgCliSetFsVlanIsolationTable (CliHandle,
                                                  (&WsscfgSetFsVlanIsolationEntry),
                                                  (&WsscfgIsSetFsVlanIsolationEntry));
            break;

        case CLI_WSSCFG_FSRRMCONFIGTABLE:
            MEMSET (&WsscfgSetFsRrmConfigEntry, 0,
                    sizeof (tWsscfgFsRrmConfigEntry));
            MEMSET (&WsscfgIsSetFsRrmConfigEntry, 0,
                    sizeof (tWsscfgIsSetFsRrmConfigEntry));
            if ((args[2] != NULL)
                && (*(UINT4 *) args[2] == CLI_RRM_DCA_CHANNEL_ONCE))
            {
                CliPrintf (CliHandle, "\r\nParameter Not Supported\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }
            WSSCFG_FILL_FSRRMCONFIGTABLE_ARGS ((&WsscfgSetFsRrmConfigEntry),
                                               (&WsscfgIsSetFsRrmConfigEntry),
                                               args[0], args[1], args[2],
                                               args[3], args[4], args[5]);

            i4RetStatus =
                WsscfgCliSetFsRrmConfigTable (CliHandle,
                                              (&WsscfgSetFsRrmConfigEntry),
                                              (&WsscfgIsSetFsRrmConfigEntry));
            break;

        case CLI_WSSCFG_ADMIN_STATUS_CHANGE:
            if ((args[0] == NULL) && (args[1] == NULL))
            {
                CliPrintf (CliHandle, "\r\nInvalid input\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }
            else
            {
                /* When Profile name is received as input get the profile id
                 * from mapping table and pass the profile name as NULL */
                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[1], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (*args[2] == 0)
                {
                    if ((i4RetStatus =
                         nmhGetCapwapBaseWtpProfileWtpModelNumber
                         (u4WtpProfileId, &ModelName)) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "\r\nEntry not found\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }

                    if ((i4RetStatus =
                         nmhGetFsNoOfRadio (&ModelName,
                                            &u4NoOfRadio)) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "\r\nAccess to DB failed\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }

                    for (u1RadioId = 1; u1RadioId <= u4NoOfRadio; u1RadioId++)
                    {
                        if ((i4RetStatus =
                             nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                             (u4WtpProfileId, u1RadioId,
                              &i4RadioIfIndex)) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFailed to get ifindex entry\r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = CLI_FAILURE;
                            break;
                        }

                        if ((i4RetStatus =
                             nmhGetFsDot11RadioType (i4RadioIfIndex,
                                                     &u4GetRadioType)) !=
                            SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFailed to get radio type\r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = CLI_FAILURE;
                            break;
                        }
                        if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[3]))
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Radio Type conflicting\r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                        else
                        {
                            if ((i4RetStatus =
                                 nmhTestv2IfMainAdminStatus (&u4ErrorCode,
                                                             i4RadioIfIndex,
                                                             *args[0])) !=
                                SNMP_SUCCESS)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nInvalid Admin Status received\r\n");
                                CLI_FATAL_ERROR (CliHandle);
                                i4RetStatus = CLI_FAILURE;
                                break;
                            }
                            if ((i4RetStatus =
                                 nmhSetIfMainAdminStatus (i4RadioIfIndex,
                                                          *args[0])) !=
                                SNMP_SUCCESS)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nAdmin Status Configuration failed\r\n");
                                CLI_FATAL_ERROR (CliHandle);
                                i4RetStatus = CLI_FAILURE;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    if ((i4RetStatus =
                         nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                         (u4WtpProfileId, *args[2],
                          &i4RadioIfIndex)) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFailed to get ifindex entry\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }

                    if ((i4RetStatus =
                         nmhGetFsDot11RadioType (i4RadioIfIndex,
                                                 &u4GetRadioType)) !=
                        SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFailed to get radio type\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }
                    if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[3]))
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Radio Type conflicting\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }

                    if ((i4RetStatus =
                         nmhTestv2IfMainAdminStatus (&u4ErrorCode,
                                                     i4RadioIfIndex,
                                                     *args[0])) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nInvalid Admin Status received\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }
                    if ((i4RetStatus =
                         nmhSetIfMainAdminStatus (i4RadioIfIndex,
                                                  *args[0])) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nAdmin Status Configuration failed\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }
                }
                break;
            }

        case CLI_WSSCFG_FSDOT11RADIOCONFIGTABLE:
            MEMSET (&WsscfgSetFsDot11RadioConfigEntry, 0,
                    sizeof (tWsscfgFsDot11RadioConfigEntry));
            MEMSET (&WsscfgIsSetFsDot11RadioConfigEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11RadioConfigEntry));

            /* When Profile name is received as input get the profile id
             * from mapping table and pass the profile name as NULL */
            if (CapwapGetWtpProfileIdFromProfileName
                ((UINT1 *) args[2], &u4WtpProfileId) != OSIX_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\n WTP Profile not found.\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, *args[3], &i4RadioIfIndex) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r\n: Failed to get the interface index \r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            /* Adding below to create table for radio type */
            if ((nmhGetFsDot11RowStatus
                 (i4RadioIfIndex, (INT4 *) &u4RowStatus)) != SNMP_SUCCESS)
            {
                u4RowStatus = CREATE_AND_GO;
                WSSCFG_FILL_FSDOT11RADIOCONFIGTABLE_ARGS ((&WsscfgSetFsDot11RadioConfigEntry), (&WsscfgIsSetFsDot11RadioConfigEntry), NULL, pu4RowStatus, pi4RadioIfIndex);

                i4RetStatus =
                    WsscfgCliSetFsDot11RadioConfigTable (CliHandle,
                                                         (&WsscfgSetFsDot11RadioConfigEntry),
                                                         (&WsscfgIsSetFsDot11RadioConfigEntry));
                if (i4RetStatus == OSIX_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }

            WSSCFG_FILL_FSDOT11RADIOCONFIGTABLE_ARGS ((&WsscfgSetFsDot11RadioConfigEntry), (&WsscfgIsSetFsDot11RadioConfigEntry), args[0], args[1], pi4RadioIfIndex);

            i4RetStatus =
                WsscfgCliSetFsDot11RadioConfigTable (CliHandle,
                                                     (&WsscfgSetFsDot11RadioConfigEntry),
                                                     (&WsscfgIsSetFsDot11RadioConfigEntry));
            break;

        case CLI_WSSCFG_FSDOT11QOSPROFILETABLE:
            MEMSET (&WsscfgSetFsDot11QosProfileEntry, 0,
                    sizeof (tWsscfgFsDot11QosProfileEntry));
            MEMSET (&WsscfgIsSetFsDot11QosProfileEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11QosProfileEntry));

            CLI_GET_QOSID ((INT1 *) au1PromptName);
            pu1Name = (UINT1 *) STRTOK_R (pu1PromptName, "/", &saveptr);
            if (pu1Name == NULL)
            {
                CliPrintf (CliHandle, "\r\nGetting Prompt Name Failed\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = CLI_FAILURE;
                break;
            }
            u4Len = STRLEN (pu1Name);
            WSSCFG_FILL_FSDOT11QOSPROFILETABLE_ARGS ((&WsscfgSetFsDot11QosProfileEntry), (&WsscfgIsSetFsDot11QosProfileEntry), pu1Name, &u4Len, args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13]);

            i4RetStatus =
                WsscfgCliSetFsDot11QosProfileTable (CliHandle,
                                                    (&WsscfgSetFsDot11QosProfileEntry),
                                                    (&WsscfgIsSetFsDot11QosProfileEntry));
            break;

        case CLI_WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE:
            MEMSET (&WsscfgSetFsDot11WlanCapabilityProfileEntry, 0,
                    sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));
            MEMSET (&WsscfgIsSetFsDot11WlanCapabilityProfileEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11WlanCapabilityProfileEntry));

            if (args[20] != NULL)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (CLI_PTR_TO_U4 (*args[20]), &i4WlanIfIndex) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (i4WlanIfIndex == 0)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }

            WSSCFG_FILL_FSDOT11WLANCAPABILITYPROFILETABLE_ARGS ((&WsscfgSetFsDot11WlanCapabilityProfileEntry), (&WsscfgIsSetFsDot11WlanCapabilityProfileEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17], args[18], pi4WlanIfIndex);

            i4RetStatus =
                WsscfgCliSetFsDot11WlanCapabilityProfileTable (CliHandle,
                                                               (&WsscfgSetFsDot11WlanCapabilityProfileEntry),
                                                               (&WsscfgIsSetFsDot11WlanCapabilityProfileEntry));
            break;

        case CLI_WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE:
            MEMSET (&WsscfgSetFsDot11WlanAuthenticationProfileEntry, 0,
                    sizeof (tWsscfgFsDot11WlanAuthenticationProfileEntry));
            MEMSET (&WsscfgIsSetFsDot11WlanAuthenticationProfileEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry));

            if (args[9] != NULL)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (CLI_PTR_TO_U4 (*args[9]), &i4WlanIfIndex) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (i4WlanIfIndex == 0)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }

            WSSCFG_FILL_FSDOT11WLANAUTHENTICATIONPROFILETABLE_ARGS ((&WsscfgSetFsDot11WlanAuthenticationProfileEntry), (&WsscfgIsSetFsDot11WlanAuthenticationProfileEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], pi4WlanIfIndex);

            i4RetStatus =
                WsscfgCliSetFsDot11WlanAuthenticationProfileTable
                (CliHandle,
                 (&WsscfgSetFsDot11WlanAuthenticationProfileEntry),
                 (&WsscfgIsSetFsDot11WlanAuthenticationProfileEntry));
            break;

        case CLI_WSSCFG_FSDOT11WLANQOSPROFILETABLE:
            MEMSET (&WsscfgSetFsDot11WlanQosProfileEntry, 0,
                    sizeof (tWsscfgFsDot11WlanQosProfileEntry));
            MEMSET (&WsscfgIsSetFsDot11WlanQosProfileEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11WlanQosProfileEntry));

            if (args[12] != NULL)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (CLI_PTR_TO_U4 (*args[12]), &i4WlanIfIndex) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (i4WlanIfIndex == 0)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }

            WSSCFG_FILL_FSDOT11WLANQOSPROFILETABLE_ARGS ((&WsscfgSetFsDot11WlanQosProfileEntry), (&WsscfgIsSetFsDot11WlanQosProfileEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], pi4WlanIfIndex);

            i4RetStatus =
                WsscfgCliSetFsDot11WlanQosProfileTable (CliHandle,
                                                        (&WsscfgSetFsDot11WlanQosProfileEntry),
                                                        (&WsscfgIsSetFsDot11WlanQosProfileEntry));
            if (i4RetStatus == CLI_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
            }
            break;

        case CLI_WSSCFG_FSDOT11RADIOQOSTABLE:
            MEMSET (&WsscfgSetFsDot11RadioQosEntry, 0,
                    sizeof (tWsscfgFsDot11RadioQosEntry));
            MEMSET (&WsscfgIsSetFsDot11RadioQosEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11RadioQosEntry));

            /* WSS Changes Starts */
            if (args[5] != NULL)
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[5], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n WTP Profile not found.\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                u1RadioId = CLI_PTR_TO_U4 (*args[6]);

                if (u1RadioId == 0)
                {
                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "\r\nInvalid Model number\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }

                    if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFailed to fetch number of radios\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;

                    }

                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {

                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             (INT4 *) &u4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d: Invalid Index \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (nmhGetFsDot11RadioType
                            (u4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d: Radio Type could not be obtained \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[7]))
                        {
                            continue;
                        }
                        else
                        {
                            WsscfgSetFsDot11RadioQosEntry.
                                MibObject.au1FsDot11TaggingPolicy[0] =
                                (UINT1) (*args[3]);
                            WsscfgIsSetFsDot11RadioQosEntry.
                                bFsDot11TaggingPolicy = OSIX_TRUE;
                            if (u4RadioIfIndex != 0)
                            {
                                WsscfgSetFsDot11RadioQosEntry.
                                    MibObject.i4IfIndex = u4RadioIfIndex;
                                WsscfgIsSetFsDot11RadioQosEntry.bIfIndex =
                                    OSIX_TRUE;
                            }
                            else
                            {
                                WsscfgIsSetFsDot11RadioQosEntry.bIfIndex =
                                    OSIX_FALSE;
                            }
                            /*                            WSSCFG_FILL_FSDOT11RADIOQOSTABLE_ARGS((&WsscfgSetFsDot11RadioQosEntry),
                               (&WsscfgIsSetFsDot11RadioQosEntry),args[3], NULL, &u4RadioIfIndex);        */

                            if ((WsscfgCliSetFsDot11RadioQosTable
                                 (CliHandle,
                                  &WsscfgSetFsDot11RadioQosEntry,
                                  &WsscfgIsSetFsDot11RadioQosEntry)) !=
                                OSIX_SUCCESS)
                            {

                                CliPrintf (CliHandle,
                                           "\r\nFor Radio %d: Configuration update failed\r\n",
                                           u1RadioIndex);
                                continue;

                            }
                        }
                    }
                }
                else
                {
                    nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u1RadioId, (INT4 *) &u4RadioIfIndex);

                    if (nmhGetFsDot11RadioType
                        (u4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d: Radio Type could not be obtained\r\n",
                                   i4RadioIfIndex);
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[7]))
                    {
                        CliPrintf (CliHandle, "\r\nRadio Type Mismatch\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    else
                    {
                        WsscfgSetFsDot11RadioQosEntry.
                            MibObject.au1FsDot11TaggingPolicy[0] =
                            (UINT1) (*args[3]);
                        WsscfgIsSetFsDot11RadioQosEntry.
                            bFsDot11TaggingPolicy = OSIX_TRUE;
                        if (u4RadioIfIndex != 0)
                        {
                            WsscfgSetFsDot11RadioQosEntry.MibObject.
                                i4IfIndex = u4RadioIfIndex;
                            WsscfgIsSetFsDot11RadioQosEntry.bIfIndex =
                                OSIX_TRUE;
                        }
                        else
                        {
                            WsscfgIsSetFsDot11RadioQosEntry.bIfIndex =
                                OSIX_FALSE;
                        }

                        i4RetStatus =
                            WsscfgCliSetFsDot11RadioQosTable (CliHandle,
                                                              (&WsscfgSetFsDot11RadioQosEntry),
                                                              (&WsscfgIsSetFsDot11RadioQosEntry));
                    }

                }

            }
            else if (args[5] == NULL)
            {
                if (nmhGetFirstIndexFsCapwapWirelessBindingTable
                    (&nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                else
                {
                    do
                    {
                        currentProfileId = nextProfileId;
                        u4currentBindingId = u4nextBindingId;
                        if ((currentProfileId == 0)
                            || (u4currentBindingId == 0))
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                        u4GetRadioType = (INT4) *args[7];
                        i4RetStatus = WssCfgGetDot11RadioIfIndex (nextProfileId,
                                                                  u4nextBindingId,
                                                                  u4GetRadioType,
                                                                  &i4RadioIfIndex);

                        if (i4RetStatus == OSIX_FAILURE)
                        {
                            continue;
                        }
                        else
                        {
                            WsscfgSetFsDot11RadioQosEntry.MibObject.
                                au1FsDot11TaggingPolicy[0] = (UINT1) (*args[3]);
                            WsscfgIsSetFsDot11RadioQosEntry.
                                bFsDot11TaggingPolicy = OSIX_TRUE;
                            if (i4RadioIfIndex != 0)
                            {
                                WsscfgSetFsDot11RadioQosEntry.MibObject.
                                    i4IfIndex = i4RadioIfIndex;
                                WsscfgIsSetFsDot11RadioQosEntry.bIfIndex =
                                    OSIX_TRUE;
                            }
                            else
                            {
                                WsscfgIsSetFsDot11RadioQosEntry.bIfIndex =
                                    OSIX_FALSE;
                            }

                            if ((WsscfgCliSetFsDot11RadioQosTable
                                 (CliHandle, &WsscfgSetFsDot11RadioQosEntry,
                                  &WsscfgIsSetFsDot11RadioQosEntry)) !=
                                OSIX_SUCCESS)
                            {
                                CliPrintf (CliHandle, "\r\n Ap(%d) Radio"
                                           "%d :  Configuration failed\r\n",
                                           currentProfileId,
                                           u4currentBindingId);
                                continue;
                            }

                        }

                    }
                    while (nmhGetNextIndexFsCapwapWirelessBindingTable
                           (currentProfileId, &nextProfileId,
                            u4currentBindingId,
                            &u4nextBindingId) == SNMP_SUCCESS);
                }

            }
            break;

        case CLI_WSSCFG_FSQAPPROFILETABLE:
            MEMSET (&WsscfgSetFsQAPProfileEntry, 0,
                    sizeof (tWsscfgFsQAPProfileEntry));
            MEMSET (&WsscfgIsSetFsQAPProfileEntry, 0,
                    sizeof (tWsscfgIsSetFsQAPProfileEntry));
            if (args[1] != NULL)
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[1], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n WTP Profile not found.\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }

            WSSCFG_FILL_FSQAPPROFILETABLE_ARGS ((&WsscfgSetFsQAPProfileEntry),
                                                (&WsscfgIsSetFsQAPProfileEntry),
                                                args[0], args[1], args[2],
                                                args[3], args[4], args[5],
                                                args[6], args[7], args[8],
                                                args[9], args[10]);

            i4RetStatus =
                WsscfgCliSetFsQAPProfileTable (CliHandle,
                                               (&WsscfgSetFsQAPProfileEntry),
                                               (&WsscfgIsSetFsQAPProfileEntry));
            break;

        case CLI_WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE:
            MEMSET (&WsscfgSetFsDot11CapabilityMappingEntry, 0,
                    sizeof (tWsscfgFsDot11CapabilityMappingEntry));
            MEMSET (&WsscfgIsSetFsDot11CapabilityMappingEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11CapabilityMappingEntry));

            if (args[3] != NULL)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (CLI_PTR_TO_U4 (*args[3]), &i4WlanIfIndex) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (i4WlanIfIndex == 0)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }
            WSSCFG_FILL_FSDOT11CAPABILITYMAPPINGTABLE_ARGS ((&WsscfgSetFsDot11CapabilityMappingEntry), (&WsscfgIsSetFsDot11CapabilityMappingEntry), args[0], args[1], args[2], pi4WlanIfIndex);

            i4RetStatus =
                WsscfgCliSetFsDot11CapabilityMappingTable (CliHandle,
                                                           (&WsscfgSetFsDot11CapabilityMappingEntry),
                                                           (&WsscfgIsSetFsDot11CapabilityMappingEntry));
            break;

        case CLI_WSSCFG_FSDOT11AUTHMAPPINGTABLE:
            MEMSET (&WsscfgSetFsDot11AuthMappingEntry, 0,
                    sizeof (tWsscfgFsDot11AuthMappingEntry));
            MEMSET (&WsscfgIsSetFsDot11AuthMappingEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11AuthMappingEntry));

            if (args[3] != NULL)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (CLI_PTR_TO_U4 (*args[3]), &i4WlanIfIndex) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (i4WlanIfIndex == 0)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }
            WSSCFG_FILL_FSDOT11AUTHMAPPINGTABLE_ARGS ((&WsscfgSetFsDot11AuthMappingEntry), (&WsscfgIsSetFsDot11AuthMappingEntry), args[0], args[1], args[2], pi4WlanIfIndex);

            i4RetStatus =
                WsscfgCliSetFsDot11AuthMappingTable (CliHandle,
                                                     (&WsscfgSetFsDot11AuthMappingEntry),
                                                     (&WsscfgIsSetFsDot11AuthMappingEntry));
            break;

        case CLI_WSSCFG_FSDOT11QOSMAPPINGTABLE:
            MEMSET (&WsscfgSetFsDot11QosMappingEntry, 0,
                    sizeof (tWsscfgFsDot11QosMappingEntry));
            MEMSET (&WsscfgIsSetFsDot11QosMappingEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11QosMappingEntry));
            if (args[3] != NULL)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (CLI_PTR_TO_U4 (*args[3]), &i4WlanIfIndex) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n Index does not exist \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (i4WlanIfIndex == 0)
                {
                    CliPrintf (CliHandle, "\r\n Invalid index\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }
            WSSCFG_FILL_FSDOT11QOSMAPPINGTABLE_ARGS ((&WsscfgSetFsDot11QosMappingEntry), (&WsscfgIsSetFsDot11QosMappingEntry), args[0], args[1], args[2], pi4WlanIfIndex);
            i4RetStatus =
                WsscfgCliSetFsDot11QosMappingTable (CliHandle,
                                                    (&WsscfgSetFsDot11QosMappingEntry),
                                                    (&WsscfgIsSetFsDot11QosMappingEntry));
            break;

        case CLI_WSSCFG_FSDOT11ANTENNASLISTTABLE:
            MEMSET (&WsscfgSetFsDot11AntennasListEntry, 0,
                    sizeof (tWsscfgFsDot11AntennasListEntry));
            MEMSET (&WsscfgIsSetFsDot11AntennasListEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11AntennasListEntry));

            /* WSS Changes Starts */
            if (args[4] != NULL)
            {

                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[4], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n WTP Profile not found. Binding failed\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                u1RadioId = CLI_PTR_TO_U4 (*args[5]);

                if (u1RadioId == 0)
                {
                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "\r\nInvalid Model number\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }

                    if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFailed to fetch number of radios\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;

                    }
                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {

                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d: Invalid Index \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d: Radio Type could not be obtained \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[6]))
                        {
                            continue;
                        }
                        else
                        {
                            WSSCFG_FILL_FSDOT11ANTENNASLISTTABLE_ARGS ((&WsscfgSetFsDot11AntennasListEntry), (&WsscfgIsSetFsDot11AntennasListEntry), args[0], args[1], pi4RadioIfIndex, args[3]);
                            if ((WsscfgCliSetFsDot11AntennasListTable
                                 (CliHandle,
                                  (&WsscfgSetFsDot11AntennasListEntry),
                                  (&WsscfgIsSetFsDot11AntennasListEntry)))
                                != OSIX_SUCCESS)
                            {

                                CliPrintf (CliHandle,
                                           "\r\nFor Radio %d: Configuration update failed\r\n",
                                           u1RadioIndex);
                                continue;

                            }
                        }
                    }
                }
                else
                {
                    nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u1RadioId, &i4RadioIfIndex);

                    if (nmhGetFsDot11RadioType
                        (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d: Radio Type could not be obtained\r\n",
                                   i4RadioIfIndex);
                    }
                    if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[6]))
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    else
                    {
                        WSSCFG_FILL_FSDOT11ANTENNASLISTTABLE_ARGS ((&WsscfgSetFsDot11AntennasListEntry), (&WsscfgIsSetFsDot11AntennasListEntry), args[0], args[1], pi4RadioIfIndex, args[3]);
                        i4RetStatus =
                            WsscfgCliSetFsDot11AntennasListTable
                            (CliHandle,
                             (&WsscfgSetFsDot11AntennasListEntry),
                             (&WsscfgIsSetFsDot11AntennasListEntry));
                    }

                }

            }
            else if (args[4] == NULL)
            {
                if (nmhGetFirstIndexFsCapwapWirelessBindingTable
                    (&nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                else
                {
                    do
                    {
                        currentProfileId = nextProfileId;
                        u4currentBindingId = u4nextBindingId;
                        if ((currentProfileId == 0)
                            || (u4currentBindingId == 0))
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                        u4GetRadioType = (INT4) *args[7];
                        i4RetStatus = WssCfgGetDot11RadioIfIndex (nextProfileId,
                                                                  u4nextBindingId,
                                                                  u4GetRadioType,
                                                                  &i4RadioIfIndex);

                        if (i4RetStatus == OSIX_FAILURE)
                        {
                            continue;
                        }
                        else
                        {
                            WSSCFG_FILL_FSDOT11ANTENNASLISTTABLE_ARGS ((&WsscfgSetFsDot11AntennasListEntry), (&WsscfgIsSetFsDot11AntennasListEntry), args[0], args[1], pi4RadioIfIndex, args[3]);
                            if ((WsscfgCliSetFsDot11AntennasListTable
                                 (CliHandle,
                                  (&WsscfgSetFsDot11AntennasListEntry),
                                  (&WsscfgIsSetFsDot11AntennasListEntry))) !=
                                OSIX_SUCCESS)
                            {
                                CliPrintf (CliHandle, "\r\n Ap(%d) Radio"
                                           "%d :  Configuration failed\r\n",
                                           currentProfileId,
                                           u4currentBindingId);
                                continue;
                            }
                        }

                    }
                    while (nmhGetNextIndexFsCapwapWirelessBindingTable
                           (currentProfileId, &nextProfileId,
                            u4currentBindingId,
                            &u4nextBindingId) == SNMP_SUCCESS);
                }
            }
            break;

        case CLI_WSSCFG_FSDOT11WLANTABLE:
            MEMSET (&WsscfgSetFsDot11WlanEntry, 0,
                    sizeof (tWsscfgFsDot11WlanEntry));
            MEMSET (&WsscfgIsSetFsDot11WlanEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11WlanEntry));

            WSSCFG_FILL_FSDOT11WLANTABLE_ARGS ((&WsscfgSetFsDot11WlanEntry),
                                               (&WsscfgIsSetFsDot11WlanEntry),
                                               args[0], args[1], args[2]);

            i4RetStatus =
                WsscfgCliSetFsDot11WlanTable (CliHandle,
                                              (&WsscfgSetFsDot11WlanEntry),
                                              (&WsscfgIsSetFsDot11WlanEntry));
            break;

        case CLI_WSSCFG_FSDOT11WLANBINDTABLE:
            MEMSET (&WsscfgSetFsDot11WlanBindEntry, 0,
                    sizeof (tWsscfgFsDot11WlanBindEntry));
            MEMSET (&WsscfgIsSetFsDot11WlanBindEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11WlanBindEntry));

            WSSCFG_FILL_FSDOT11WLANBINDTABLE_ARGS ((&WsscfgSetFsDot11WlanBindEntry), (&WsscfgIsSetFsDot11WlanBindEntry), args[0], args[1], args[2], args[3], args[4]);

            i4RetStatus =
                WsscfgCliSetFsDot11WlanBindTable (CliHandle,
                                                  (&WsscfgSetFsDot11WlanBindEntry),
                                                  (&WsscfgIsSetFsDot11WlanBindEntry));
            break;

        case CLI_WSSCFG_MAX_CLIENTCOUNT:

            if (args[1] != NULL)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (CLI_PTR_TO_U4 (*args[1]), &i4WlanIfIndex) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n Index does not exist \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                else if (i4WlanIfIndex == 0)
                {
                    CliPrintf (CliHandle, "\r\n Invalid index\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                else

                {
                    i4RetStatus =
                        WssCfgCliSetMaxClientCountPerSSID (CliHandle, *args[0],
                                                           *args[1]);
                }
            }
            else
            {
                i4RetStatus =
                    WssCfgCliSetMaxClientCountPerRadio (CliHandle, *args[0],
                                                        (UINT1 *) args[2],
                                                        *args[3]);
            }
            break;
        case CLI_WSSCFG_FSWTPIMAGEUPGRADETABLE:
            MEMSET (&WsscfgSetFsWtpImageUpgradeEntry, 0,
                    sizeof (tWsscfgFsWtpImageUpgradeEntry));
            MEMSET (&WsscfgIsSetFsWtpImageUpgradeEntry, 0,
                    sizeof (tWsscfgIsSetFsWtpImageUpgradeEntry));
            Ipaddress = sizeof (UINT4);
            pIpaddress = &Ipaddress;

            MEMSET (&ai1FileName, 0, sizeof (CLI_MAX_PROFILE_NAME));
            MEMSET (&au1WtpName, 0, sizeof (au1WtpName));
            if (WsscfgUtilImageTransfer
                ((INT1 *) args[8], ai1FileName, &IpAddress) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to copy remote file to flash\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = CLI_FAILURE;
                break;
            }

            if (*args[2] == WSSCFG_VERSION_AP_NAME_NONE)
            {
                i4RetStatus = CLI_SUCCESS;
                break;
            }
            u4FileLen = STRLEN (ai1FileName);
            pu4FileLen = &u4FileLen;

            if (args[3] != NULL)
            {
                WSSCFG_FILL_FSWTPIMAGEUPGRADETABLE_ARGS ((&WsscfgSetFsWtpImageUpgradeEntry), (&WsscfgIsSetFsWtpImageUpgradeEntry), args[0], args[1], args[2], args[3], args[4], ai1FileName, pu4FileLen, args[7], &IpAddress.au1Addr, pIpaddress, args[10], args[11], args[12]);

                i4RetStatus =
                    WsscfgCliSetFsWtpImageUpgradeTable (CliHandle,
                                                        (&WsscfgSetFsWtpImageUpgradeEntry),
                                                        (&WsscfgIsSetFsWtpImageUpgradeEntry));
            }
            else if (*args[2] == WSSCFG_VERSION_AP_NAME_ALL)
            {
                ProfileName.pu1_OctetList = au1WtpName;
                if (nmhGetFirstIndexCapwapBaseWtpProfileTable
                    (&u4WtpProfileId) != SNMP_SUCCESS)
                {
                    SetValFsCapwapImageName.pu1_OctetList =
                        (UINT1 *) ai1FileName;
                    SetValFsCapwapImageName.i4_Length = STRLEN (ai1FileName);
                    FsCapwapWtpModelNumber.pu1_OctetList = (UINT1 *) args[11];
                    FsCapwapWtpModelNumber.i4_Length = STRLEN (args[11]);

                    if (nmhSetFsCapwapImageName
                        (&FsCapwapWtpModelNumber,
                         &SetValFsCapwapImageName) == SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n WTP model DB Updation Failed\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }
                    i4RetStatus = CLI_SUCCESS;
                    break;
                }

                do
                {
                    if (nmhGetCapwapBaseWtpProfileName (u4WtpProfileId,
                                                        &ProfileName) !=
                        SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Profile Name Retrieval Failed for all profile Id %d\r\n",
                                   u4WtpProfileId);
                    }
                    u4WtpNameLen = ProfileName.i4_Length;
                    WSSCFG_FILL_FSWTPIMAGEUPGRADETABLE_ARGS ((&WsscfgSetFsWtpImageUpgradeEntry), (&WsscfgIsSetFsWtpImageUpgradeEntry), args[0], args[1], args[2], au1WtpName, &u4WtpNameLen, ai1FileName, pu4FileLen, args[7], &IpAddress.au1Addr, pIpaddress, args[10], args[11], args[12]);
                    if (WsscfgCliWtpModelProfileCmp
                        (u4WtpProfileId,
                         &WsscfgSetFsWtpImageUpgradeEntry) == CLI_SUCCESS)
                    {
                        i4RetStatus =
                            WsscfgCliSetFsWtpImageUpgradeTable (CliHandle,
                                                                (&WsscfgSetFsWtpImageUpgradeEntry),
                                                                (&WsscfgIsSetFsWtpImageUpgradeEntry));
                    }
                    u4currentProfileId = u4WtpProfileId;
                }
                while (nmhGetNextIndexCapwapBaseWtpProfileTable
                       (u4currentProfileId, &u4WtpProfileId));
            }

            break;
        case CLI_WSSCFG_SHOWWTPIMAGEUPGRADETABLE:
            MEMSET (&au1WtpName, 0, sizeof (au1WtpName));

            if (args[1] != NULL)
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[1], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                i4RetStatus =
                    WsscfgCliShowWtpImageUpgradeTable (CliHandle,
                                                       u4WtpProfileId,
                                                       (UINT1 *) args[1]);
            }
            else if (*args[0] == WSSCFG_VERSION_AP_NAME_ALL)
            {
                MEMSET (&WsscfgSetFsWtpImageUpgradeEntry, 0,
                        sizeof (tWsscfgFsWtpImageUpgradeEntry));
                MEMSET (&WsscfgIsSetFsWtpImageUpgradeEntry, 0,
                        sizeof (tWsscfgIsSetFsWtpImageUpgradeEntry));
                ProfileName.pu1_OctetList = au1WtpName;
                if (nmhGetFirstIndexCapwapBaseWtpProfileTable
                    (&u4WtpProfileId) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }

                do
                {
                    MEMSET (&au1ProfName, 0, sizeof (au1ProfName));
                    if (nmhGetCapwapBaseWtpProfileName (u4WtpProfileId,
                                                        &ProfileName) !=
                        SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Profile Name Retrieval Failed for all profile Id %d\r\n",
                                   u4WtpProfileId);
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }
                    u4WtpNameLen = ProfileName.i4_Length;
                    WSSCFG_FILL_FSWTPIMAGEUPGRADETABLE_ARGS ((&WsscfgSetFsWtpImageUpgradeEntry), (&WsscfgIsSetFsWtpImageUpgradeEntry), u4NullPtr[0], u4NullPtr[0], u4NullPtr[0], u4NullPtr[0], u4NullPtr[0], u4NullPtr[0], u4NullPtr[0], u4NullPtr[0], u4NullPtr[0], u4NullPtr[0], u4NullPtr[0], args[3], args[4]);
#if 0
                    WSSCFG_FILL_FSWTPIMAGEUPGRADETABLE_ARGS ((&WsscfgSetFsWtpImageUpgradeEntry), (&WsscfgIsSetFsWtpImageUpgradeEntry), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, args[3], args[4]);
#endif
                    if (WsscfgCliWtpModelProfileCmp
                        (u4WtpProfileId,
                         &WsscfgSetFsWtpImageUpgradeEntry) == CLI_SUCCESS)
                    {
                        MEMCPY (au1ProfName, au1WtpName, u4WtpNameLen);
                        i4RetStatus =
                            WsscfgCliShowWtpImageUpgradeTable (CliHandle,
                                                               u4WtpProfileId,
                                                               au1ProfName);
                    }
                    u4currentProfileId = u4WtpProfileId;
                }
                while (nmhGetNextIndexCapwapBaseWtpProfileTable
                       (u4currentProfileId, &u4WtpProfileId));
            }

            break;
        case CLI_WSSCFG_FSDOT11NCONFIGTABLE:
            MEMSET (&WsscfgSetFsDot11nConfigEntry, 0,
                    sizeof (tWsscfgFsDot11nConfigEntry));
            MEMSET (&WsscfgIsSetFsDot11nConfigEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11nConfigEntry));
            if ((args[5] == NULL) && (*(args[6]) == CLI_WTP_RADIO_ALL))
            {
                if (nmhGetFirstIndexFsCapwapWirelessBindingTable
                    (&nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                else
                {
                    do
                    {
                        currentProfileId = nextProfileId;
                        u4currentBindingId = u4nextBindingId;
                        if ((currentProfileId == 0)
                            || (u4currentBindingId == 0))
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                        u4GetRadioType = (INT4) *args[4];
                        i4RetStatus = WssCfgGetDot11RadioIfIndex (nextProfileId,
                                                                  u4nextBindingId,
                                                                  u4GetRadioType,
                                                                  &i4RadioIfIndex);

                        if (i4RetStatus == OSIX_FAILURE)
                        {
                            continue;
                        }
                        else
                        {
                            if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                                        &u4GetRadioType) !=
                                SNMP_SUCCESS)
                            {
                                CLI_FATAL_ERROR (CliHandle);
                                i4RetStatus = OSIX_FAILURE;
                                break;
                            }
                            i4EnteredRadioType = (*(args[4]));
                            if (WSSCFG_RADIO_TYPE_COMP (i4EnteredRadioType))
                            {
                                CliPrintf (CliHandle,
                                           "\r\n Radio type mismatch\r\n");
                                i4RetStatus = OSIX_FAILURE;
                                break;

                            }

                            if (WSSCFG_RADIO_TYPEN_AC_COMP (u4GetRadioType))
                            {
                                WSSCFG_FILL_FSDOT11NCONFIGTABLE_ARGS ((&WsscfgSetFsDot11nConfigEntry), (&WsscfgIsSetFsDot11nConfigEntry), args[0], args[1], args[2], pi4RadioIfIndex);
                                if (WsscfgCliSetFsDot11nConfigTable
                                    (CliHandle, (&WsscfgSetFsDot11nConfigEntry),
                                     (&WsscfgIsSetFsDot11nConfigEntry)) !=
                                    OSIX_SUCCESS)
                                {
                                    CliPrintf (CliHandle,
                                               "\r\nFor Radio %d:  DB Updation failed \r\n",
                                               u1RadioIndex);
                                    continue;
                                }
                            }
                            else
                            {
                                CLI_FATAL_ERROR (CliHandle);
                                i4RetStatus = CLI_FAILURE;
                                break;

                            }

                        }

                    }
                    while (nmhGetNextIndexFsCapwapWirelessBindingTable
                           (currentProfileId, &nextProfileId,
                            u4currentBindingId,
                            &u4nextBindingId) == SNMP_SUCCESS);
                }
            }
            else
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[5], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n WTP Profile not found\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if ((*(args[6]) == CLI_WTP_RADIO_ALL))
                {
                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Ap %d: Invalid Model number\r\n",
                                   u4WtpProfileId);
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }
                    if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFailed to fetch number of radios\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }
                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {
                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor AP %d Radio %d: Invalid Index\r\n",
                                       u2WtpProfileId, u1RadioIndex);
                            continue;
                        }
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d: Radio Type could not be obtained \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (WSSCFG_RADIO_TYPEN_AC_COMP (u4GetRadioType))
                        {
                            i4EnteredRadioType = (*(args[4]));
                            if (WSSCFG_RADIO_TYPE_COMP (i4EnteredRadioType))
                            {
                                WSSCFG_FILL_FSDOT11NCONFIGTABLE_ARGS ((&WsscfgSetFsDot11nConfigEntry), (&WsscfgIsSetFsDot11nConfigEntry), args[0], args[1], args[2], pi4RadioIfIndex);
                                if (WsscfgCliSetFsDot11nConfigTable
                                    (CliHandle,
                                     (&WsscfgSetFsDot11nConfigEntry),
                                     (&WsscfgIsSetFsDot11nConfigEntry)) !=
                                    OSIX_SUCCESS)
                                {
                                    CliPrintf (CliHandle,
                                               "\r\nFor Radio %d :  DB Updation failed \r\n",
                                               u1RadioIndex);
                                    continue;
                                }
                            }
                        }
                        else
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = CLI_FAILURE;
                            break;
                        }
                    }
                    i4RetStatus = OSIX_SUCCESS;
                }
                else
                {
                    u1RadioId = CLI_PTR_TO_U4 (*(args[6]));
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u1RadioId,
                         &i4RadioIfIndex) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Ap %d Radio %d: Invalid IfIndex\r\n",
                                   u2WtpProfileId, u1RadioIndex);
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }
                    if (nmhGetFsDot11RadioType
                        (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d:  Radio Type could not successfully obtained\r\n",
                                   u1RadioIndex);
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }
                    if (WSSCFG_RADIO_TYPEN_AC_COMP (u4GetRadioType))
                    {
                        i4EnteredRadioType = (*(args[4]));
                        if (WSSCFG_RADIO_TYPE_COMP (i4EnteredRadioType))
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Radio type mismatch\r\n");
                            i4RetStatus = CLI_FAILURE;
                            break;
                        }
                        WSSCFG_FILL_FSDOT11NCONFIGTABLE_ARGS ((&WsscfgSetFsDot11nConfigEntry), (&WsscfgIsSetFsDot11nConfigEntry), args[0], args[1], args[2], pi4RadioIfIndex);
                        i4RetStatus =
                            WsscfgCliSetFsDot11nConfigTable (CliHandle,
                                                             (&WsscfgSetFsDot11nConfigEntry),
                                                             (&WsscfgIsSetFsDot11nConfigEntry));

                        if (i4RetStatus != OSIX_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d: DB Updation failed \r\n",
                                       u1RadioIndex);
                        }
                    }
                    else
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }

                }
            }
            break;

        case CLI_WSSCFG_FSDOT11NMCSDATARATETABLE:
            MEMSET (&WsscfgSetFsDot11nMCSDataRateEntry, 0,
                    sizeof (tWsscfgFsDot11nMCSDataRateEntry));
            MEMSET (&WsscfgIsSetFsDot11nMCSDataRateEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11nMCSDataRateEntry));

            /*Check the radio type and confirm that the nSupport is enabled */
            if ((args[4] == NULL) && (*(args[5]) == CLI_WTP_RADIO_ALL))
            {
                if (nmhGetFirstIndexFsCapwapWirelessBindingTable
                    (&nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                else
                {
                    do
                    {
                        currentProfileId = nextProfileId;
                        u4currentBindingId = u4nextBindingId;
                        if ((currentProfileId == 0)
                            || (u4currentBindingId == 0))
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                        u4GetRadioType = (INT4) *args[3];
                        i4RetStatus = WssCfgGetDot11RadioIfIndex (nextProfileId,
                                                                  u4nextBindingId,
                                                                  u4GetRadioType,
                                                                  &i4RadioIfIndex);
                        if (i4RetStatus == OSIX_FAILURE)
                        {
                            continue;
                        }
                        else
                        {
                            if (WSSCFG_RADIO_TYPEN_AC_COMP (u4GetRadioType))
                            {
                                WSSCFG_FILL_FSDOT11NMCSDATARATETABLE_ARGS ((&WsscfgSetFsDot11nMCSDataRateEntry), (&WsscfgIsSetFsDot11nMCSDataRateEntry), args[0], args[1], pi4RadioIfIndex);

                                if (WsscfgCliSetFsDot11nMCSDataRateTable
                                    (CliHandle,
                                     (&WsscfgSetFsDot11nMCSDataRateEntry),
                                     (&WsscfgIsSetFsDot11nMCSDataRateEntry)) !=
                                    OSIX_SUCCESS)
                                {
                                    CliPrintf (CliHandle,
                                               "\r\nFor Radio %d :  DB Updation failed \r\n",
                                               u1RadioIndex);
                                    continue;
                                }
                            }
                            else
                            {
                                CLI_FATAL_ERROR (CliHandle);
                                i4RetStatus = CLI_FAILURE;
                                break;
                            }

                        }

                    }
                    while (nmhGetNextIndexFsCapwapWirelessBindingTable
                           (currentProfileId, &nextProfileId,
                            u4currentBindingId,
                            &u4nextBindingId) == SNMP_SUCCESS);
                }
            }
            else
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    ((UINT1 *) args[4], &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n WTP Profile not found. Binding failed\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if ((*(args[5]) == CLI_WTP_RADIO_ALL))
                {
                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Ap (%d): Invalid Model number\r\n",
                                   u4WtpProfileId);
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }
                    if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFailed to fetch number of radios\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }
                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {
                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor AP %d Radio %d: Invalid Radio If Index \r\n",
                                       u2WtpProfileId, u1RadioIndex);
                            continue;
                        }
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d:  Radio Type not successfully obtained\r\n",
                                       u1RadioIndex);
                            continue;
                        }
                        if (WSSCFG_RADIO_TYPEN_AC_COMP (u4GetRadioType))
                        {
                            i4EnteredRadioType = *(args[3]);
                            if ((WSSCFG_RADIO_TYPE_COMP (i4EnteredRadioType)))
                            {
                                WSSCFG_FILL_FSDOT11NMCSDATARATETABLE_ARGS ((&WsscfgSetFsDot11nMCSDataRateEntry), (&WsscfgIsSetFsDot11nMCSDataRateEntry), args[0], args[1], pi4RadioIfIndex);

                                if (WsscfgCliSetFsDot11nMCSDataRateTable
                                    (CliHandle,
                                     (&WsscfgSetFsDot11nMCSDataRateEntry),
                                     (&WsscfgIsSetFsDot11nMCSDataRateEntry))
                                    != OSIX_SUCCESS)
                                {
                                    CliPrintf (CliHandle,
                                               "\r\nFor Radio %d :  DB Updation failed \r\n",
                                               u1RadioIndex);
                                    continue;
                                }
                            }
                        }
                        else
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = CLI_FAILURE;
                            break;
                        }

                    }
                    i4RetStatus = OSIX_SUCCESS;
                }
                else
                {
                    u1RadioId = CLI_PTR_TO_U4 (*(args[5]));
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u1RadioId,
                         &i4RadioIfIndex) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor AP %d Radio %d: Invalid RadioIf Index \r\n",
                                   u2WtpProfileId, u1RadioIndex);
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }
                    if (nmhGetFsDot11RadioType
                        (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d:  Radio Type not successfully obtained\r\n",
                                   u1RadioIndex);
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }
                    if (WSSCFG_RADIO_TYPEN_AC_COMP (u4GetRadioType))
                    {
                        WSSCFG_FILL_FSDOT11NMCSDATARATETABLE_ARGS ((&WsscfgSetFsDot11nMCSDataRateEntry), (&WsscfgIsSetFsDot11nMCSDataRateEntry), args[0], args[1], pi4RadioIfIndex);

                        i4RetStatus =
                            WsscfgCliSetFsDot11nMCSDataRateTable (CliHandle,
                                                                  (&WsscfgSetFsDot11nMCSDataRateEntry),
                                                                  (&WsscfgIsSetFsDot11nMCSDataRateEntry));
                        if (i4RetStatus != OSIX_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d :  DB Updation failed \r\n",
                                       u1RadioIndex);
                        }
                    }
                    else
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }

                }
            }
            break;

        case CLI_WSSCFG_FSDOT11ANETWORKENABLE:

            i4RetStatus =
                WsscfgCliSetFsDot11aNetworkEnable (CliHandle, args[0]);
            break;

        case CLI_WSSCFG_FSDOT11BNETWORKENABLE:

            i4RetStatus =
                WsscfgCliSetFsDot11bNetworkEnable (CliHandle, args[0]);
            break;

        case CLI_WSSCFG_FSDOT11GSUPPORT:

            i4RetStatus = WsscfgCliSetFsDot11gSupport (CliHandle, args[0]);
            break;

        case CLI_WSSCFG_FSDOT11ANSUPPORT:

            i4RetStatus = WsscfgCliSetFsDot11anSupport (CliHandle, args[0]);
            break;

        case CLI_WSSCFG_FSDOT11BNSUPPORT:

            i4RetStatus = WsscfgCliSetFsDot11bnSupport (CliHandle, args[0]);
            break;

        case CLI_WSSCFG_FSDOT11MANAGMENTSSID:

            i4RetStatus = WsscfgCliSetFsDot11ManagmentSSID (CliHandle, args[0]);
            break;

        case CLI_WSSCFG_FSDOT11COUNTRYSTRING:

            i4RetStatus = WsscfgCliSetFsDot11CountryString (CliHandle, args[0]);
            break;

        case CLI_WSSCFG_FSSECURITYWEBAUTHTYPE:
            i4RetStatus =
                WsscfgCliSetFsSecurityWebAuthType (CliHandle, args[0], args[1]);
            break;

        case CLI_WSSCFG_FSSECURITYWEBAUTHURL:
            i4RetStatus =
                WsscfgCliSetFsSecurityWebAuthUrl (CliHandle, (UINT1 *) args[0],
                                                  args[1]);
            break;

        case CLI_WSSCFG_FSSECURITYWEBAUTHREDIRECTURL:

            i4RetStatus =
                WsscfgCliSetFsSecurityWebAuthRedirectUrl (CliHandle, args[0]);
            break;

        case CLI_WSSCFG_FSSECURITYWEBADDR:

            i4RetStatus = WsscfgCliSetFsSecurityWebAddr (CliHandle, args[0]);
            break;

        case CLI_WSSCFG_FSSECURITYWEBAUTHWEBTITLE:

            if (nmhGetFsSecurityWebAuthType (&i4FsSecurityWebAuthType) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }
            if ((i4FsSecurityWebAuthType == 0) ||
                (i4FsSecurityWebAuthType == WEB_AUTH_TYPE_INTERNAL))
            {
                CliPrintf (CliHandle,
                           "\r\nFAILURE :Unable to Customize INTERNAL Webtitle\r\n");
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            i4RetStatus =
                WsscfgCliSetFsSecurityWebAuthWebTitle (CliHandle,
                                                       (UINT1 *) args[0]);
            break;
        case CLI_WSSCFG_CLEAR_FSSECURITYWEBAUTHWEBTITLE:
            i4RetStatus = WsscfgCliClearFsSecurityWebAuthWebTitle (CliHandle);
            break;
        case CLI_WSSCFG_FSSECURITYWEBAUTHWEBMESSAGE:

            i4RetStatus =
                WsscfgCliSetFsSecurityWebAuthWebMessage (CliHandle,
                                                         (UINT1 *) args[0]);
            break;

        case CLI_WSSCFG_CLEAR_FSSECURITYWEBAUTHWEBMESSAGE:
            i4RetStatus = WsscfgCliClearFsSecurityWebAuthWebMessage (CliHandle);
            break;

        case CLI_WSSCFG_FSSECURITYWEBAUTHWEBLOGOFILENAME:
            if ((*args[0] == WSS_AUTH_STATUS_ENABLE) && (args[1] == NULL))
            {
                CliPrintf (CliHandle, "\r\nInvalid input\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
            }
            else
            {
                i4RetStatus =
                    WsscfgCliSetFsSecurityWebAuthWebLogoFileName
                    (CliHandle, args[0], (UINT1 *) args[1]);
            }
            break;

        case CLI_WSSCFG_FSSECURITYWEBAUTHWEBSUCCMESSAGE:
            if (nmhGetFsSecurityWebAuthType (&i4FsSecurityWebAuthType) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }
            if ((i4FsSecurityWebAuthType == 0) ||
                (i4FsSecurityWebAuthType == WEB_AUTH_TYPE_INTERNAL))
            {
                CliPrintf (CliHandle,
                           "\r\nFAILURE :Unable to customize INTERNAL Successmessage\n");
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            i4RetStatus =
                WsscfgCliSetFsSecurityWebAuthWebSuccMessage (CliHandle,
                                                             (UINT1 *) args[0]);
            break;
        case CLI_WSSCFG_CLEAR_FSSECURITYWEBAUTHWEBSUCCMESSAGE:
            i4RetStatus =
                WsscfgCliClearFsSecurityWebAuthWebSuccMessage (CliHandle);
            break;

        case CLI_WSSCFG_FSSECURITYWEBAUTHWEBFAILMESSAGE:
            if (nmhGetFsSecurityWebAuthType (&i4FsSecurityWebAuthType) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }
            if ((i4FsSecurityWebAuthType == 0) ||
                (i4FsSecurityWebAuthType == WEB_AUTH_TYPE_INTERNAL))
            {
                CliPrintf (CliHandle,
                           "\r\nFAILURE :Unable to customize INTERNAL Failuremessage\n");
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            i4RetStatus =
                WsscfgCliSetFsSecurityWebAuthWebFailMessage (CliHandle,
                                                             (UINT1 *) args[0]);
            break;

        case CLI_WSSCFG_CLEAR_FSSECURITYWEBAUTHWEBFAILMESSAGE:
            i4RetStatus =
                WsscfgCliClearFsSecurityWebAuthWebFailMessage (CliHandle);
            break;
        case CLI_WSSCFG_FSSECURITYWEBAUTHWEBBUTTONTEXT:
            if (nmhGetFsSecurityWebAuthType (&i4FsSecurityWebAuthType) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }
            if ((i4FsSecurityWebAuthType == 0) ||
                (i4FsSecurityWebAuthType == WEB_AUTH_TYPE_INTERNAL))
            {
                CliPrintf (CliHandle,
                           "\r\nFAILURE : Unable to customize the INTERNAL buttontext type\r\n");
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            i4RetStatus =
                WsscfgCliSetFsSecurityWebAuthWebButtonText (CliHandle,
                                                            (UINT1 *) args[0]);
            break;

        case CLI_WSSCFG_CLEAR_FSSECURITYWEBAUTHWEBBUTTONTEXT:
            i4RetStatus =
                WsscfgCliClearFsSecurityWebAuthWebButtonText (CliHandle);
            break;
        case CLI_WSSCFG_FSSECURITYWEBAUTHWEBLOADBALINFO:

            i4RetStatus =
                WsscfgCliSetFsSecurityWebAuthWebLoadBalInfo (CliHandle,
                                                             (UINT1 *) args[0]);
            break;
        case CLI_WSSCFG_CLEAR_FSSECURITYWEBAUTHWEBLOADBALINFO:
            i4RetStatus =
                WsscfgCliClearFsSecurityWebAuthWebLoadBalInfo (CliHandle);
            break;
        case CLI_WSSCFG_FSSECURITYWEBAUTHDISPLAYLANG:
            if (nmhGetFsSecurityWebAuthType (&i4FsSecurityWebAuthType) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }
            if ((i4FsSecurityWebAuthType == 0) ||
                (i4FsSecurityWebAuthType == WEB_AUTH_TYPE_INTERNAL))
            {
                CliPrintf (CliHandle,
                           "\r\nFAILURE : Unable to customize the INTERNAL Language Type\r\n");
                i4RetStatus = OSIX_FAILURE;
                break;
            }
            i4RetStatus =
                WsscfgCliSetFsSecurityWebAuthDisplayLang (CliHandle, args[0]);
            break;

        case CLI_WSSCFG_FSSECURITYWEBAUTHCOLOR:
            if (nmhGetFsSecurityWebAuthType (&i4FsSecurityWebAuthType) !=
                SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }
            if ((i4FsSecurityWebAuthType == 0) ||
                (i4FsSecurityWebAuthType == WEB_AUTH_TYPE_INTERNAL))
            {
                CliPrintf (CliHandle,
                           "\r\nFAILURE : Unable to customize the INTERNAL Color Type\r\n");
                i4RetStatus = OSIX_FAILURE;
                break;
            }
            i4RetStatus =
                WsscfgCliSetFsSecurityWebAuthColor (CliHandle, args[0]);
            break;

        case CLI_WSSCFG_CREATE_WLAN_CAPABILITY:
            i4RetStatus =
                WsscfgWlanCapabilityCreate (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_WSSCFG_DELETE_WLAN_CAPABILITY:
            i4RetStatus =
                WsscfgWlanCapabilityDelete (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_WSSCFG_CREATE_WLAN_QOS:
            i4RetStatus = WsscfgWlanQosCreate (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_WSSCFG_DELETE_WLAN_QOS:
            i4RetStatus = WsscfgWlanQosDelete (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_WSSCFG_CREATE_WLAN_AUTH:
            i4RetStatus = WsscfgWlanAuthCreate (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_WSSCFG_DELETE_WLAN_AUTH:
            i4RetStatus = WsscfgWlanAuthDelete (CliHandle, (UINT1 *) args[0]);
            break;
        case CLI_WSSCFG_RADIO_CLEAR_COUNTERS:
            i4RetStatus =
                WsscfgClearRadioCounters (CliHandle, (UINT1 *) args[0],
                                          args[1]);
            break;
        case CLI_WSSCFG_CLIENT_CLEAR_COUNTERS:
            if (NULL != args[0])
            {
                StrToMac ((UINT1 *) args[0], BaseMacAddr);
            }
            i4RetStatus = WsscfgClearClientCounters (CliHandle, BaseMacAddr);
            break;
        case CLI_WSSCFG_WLAN_TRAP_STATUS:

            i4RetStatus =
                WsscfgCliSetFsWlanStationTrapStatus (CliHandle, args[0]);
            break;
        case CLI_WSSCFG_CONFIG_DSCP:
            i4RetStatus =
                WsscfgCliSetDscp (CliHandle, (UINT4) *args[0], (UINT4) *args[1],
                                  (UINT1 *) args[2], (UINT4) *args[3],
                                  (UINT4) *args[4]);
            break;

#ifdef BAND_SELECT_WANTED
        case CLI_WSSCFG_BAND_SELECT_STATUS:
            if (args[1] != NULL)
            {
                i4RetStatus =
                    WsscfgCliSetBandSelect (CliHandle, (UINT1) *args[0],
                                            *args[1]);
            }
            else
            {
                i4RetStatus =
                    WsscfgCliSetBandSelect (CliHandle, (UINT1) *args[0], 0);
            }
            break;
        case CLI_WSSCFG_ASSOC_REJECT_COUNT:
            i4RetStatus =
                WsscfgCliSetAssocRejectCount (CliHandle, (UINT1) *args[0],
                                              *args[1]);
            break;
        case CLI_WSSCFG_ASSOC_RESET_TIMER:
            i4RetStatus =
                WsscfgCliSetAssocResetTimer (CliHandle, (UINT1) *args[0],
                                             *args[1]);
            break;
        case CLI_WSSCFG_PROBE_AGEOUT_TIMER:
            i4RetStatus =
                WsscfgCliSetProbeAgeoutTimer (CliHandle, (UINT1) *args[0],
                                              *args[1]);
            break;
#endif
        case CLI_WSSCFG_LEGACY_RATES:
            i4RetStatus = WsscfgCliSetLegacyRates (CliHandle, (UINT1) *args[0]);
            break;
        case CLI_WSSCFG_ALLOW_CHANNWIDTH:
            WssCfgCliSetStatusAllowedChannelWidth (CliHandle, (INT4) *args[4],
                                                   (UINT1) *args[2],
                                                   (UINT1 *) args[3],
                                                   (UINT4 *) args[6]);

            break;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_WSSCFG_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", WsscfgCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);

    WSSCFG_UNLOCK;

    return i4RetStatus;

}

/****************************************************************************
* Function    :  WsscfgCliSetDot11StationConfigTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11StationConfigEntry
*            pWsscfgIsSetDot11StationConfigEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11StationConfigTable (tCliHandle CliHandle,
                                     tWsscfgDot11StationConfigEntry *
                                     pWsscfgSetDot11StationConfigEntry,
                                     tWsscfgIsSetDot11StationConfigEntry *
                                     pWsscfgIsSetDot11StationConfigEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11StationConfigTable
        (&u4ErrorCode, pWsscfgSetDot11StationConfigEntry,
         pWsscfgIsSetDot11StationConfigEntry) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Input Validation failed \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11StationConfigTable
        (pWsscfgSetDot11StationConfigEntry,
         pWsscfgIsSetDot11StationConfigEntry) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n DB Updation failed \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetWebauthRedirectFile
* Description :
* Input       :  CliHandle
*            pWsscfgSetDot11AuthenticationAlgorithmsEntry
*            pWsscfgIsSetDot11AuthenticationAlgorithmsEntry
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetWebauthRedirectFile (tCliHandle CliHandle, UINT1 *pu1FileName,
                                 UINT4 u4WlanId)
{
    UINT4               u4ErrorCode;
    INT4                i4Length;
    INT4                i4IfIndex = 0;
    tSNMP_OCTET_STRING_TYPE FsDot11WlanWebAuthRedirectFileName;

    if (nmhGetCapwapDot11WlanProfileIfIndex (u4WlanId, &i4IfIndex)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMSET (&FsDot11WlanWebAuthRedirectFileName, 0,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    FsDot11WlanWebAuthRedirectFileName.pu1_OctetList = pu1FileName;
    i4Length = STRLEN (pu1FileName);
    FsDot11WlanWebAuthRedirectFileName.i4_Length = i4Length;
    if (nmhTestv2FsDot11WlanWebAuthRedirectFileName (&u4ErrorCode, i4IfIndex,
                                                     &FsDot11WlanWebAuthRedirectFileName)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    else
    {
        if (nmhSetFsDot11WlanWebAuthRedirectFileName (i4IfIndex,
                                                      &FsDot11WlanWebAuthRedirectFileName)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetWlanLoginAuthentication 
* Description :
* Input       :  CliHandle
*            pWsscfgSetDot11AuthenticationAlgorithmsEntry
*            pWsscfgIsSetDot11AuthenticationAlgorithmsEntry
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetWlanLoginAuthentication (tCliHandle CliHandle, UINT4 u4LoginMode,
                                     UINT4 u4WlanId)
{
    UINT4               u4ErrorCode;
    INT4                i4IfIndex = 0;

    if (nmhGetCapwapDot11WlanProfileIfIndex (u4WlanId, &i4IfIndex)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhTestv2FsDot11WlanLoginAuthentication (&u4ErrorCode, i4IfIndex,
                                                 u4LoginMode) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    else
    {
        if (nmhSetFsDot11WlanLoginAuthentication (i4IfIndex,
                                                  u4LoginMode) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11AuthenticationAlgorithmsTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11AuthenticationAlgorithmsEntry
*            pWsscfgIsSetDot11AuthenticationAlgorithmsEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11AuthenticationAlgorithmsTable (tCliHandle CliHandle,
                                                tWsscfgDot11AuthenticationAlgorithmsEntry
                                                *
                                                pWsscfgSetDot11AuthenticationAlgorithmsEntry,
                                                tWsscfgIsSetDot11AuthenticationAlgorithmsEntry
                                                *
                                                pWsscfgIsSetDot11AuthenticationAlgorithmsEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11AuthenticationAlgorithmsTable
        (&u4ErrorCode, pWsscfgSetDot11AuthenticationAlgorithmsEntry,
         pWsscfgIsSetDot11AuthenticationAlgorithmsEntry) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\n Input Validation failed\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11AuthenticationAlgorithmsTable
        (pWsscfgSetDot11AuthenticationAlgorithmsEntry,
         pWsscfgIsSetDot11AuthenticationAlgorithmsEntry) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\nDB Updation failed\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11WEPDefaultKeysTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11WEPDefaultKeysEntry
*            pWsscfgIsSetDot11WEPDefaultKeysEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11WEPDefaultKeysTable (tCliHandle CliHandle,
                                      tWsscfgDot11WEPDefaultKeysEntry *
                                      pWsscfgSetDot11WEPDefaultKeysEntry,
                                      tWsscfgIsSetDot11WEPDefaultKeysEntry *
                                      pWsscfgIsSetDot11WEPDefaultKeysEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11WEPDefaultKeysTable
        (&u4ErrorCode, pWsscfgSetDot11WEPDefaultKeysEntry,
         pWsscfgIsSetDot11WEPDefaultKeysEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11WEPDefaultKeysTable
        (pWsscfgSetDot11WEPDefaultKeysEntry,
         pWsscfgIsSetDot11WEPDefaultKeysEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11WEPKeyMappingsTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11WEPKeyMappingsEntry
*            pWsscfgIsSetDot11WEPKeyMappingsEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11WEPKeyMappingsTable (tCliHandle CliHandle,
                                      tWsscfgDot11WEPKeyMappingsEntry *
                                      pWsscfgSetDot11WEPKeyMappingsEntry,
                                      tWsscfgIsSetDot11WEPKeyMappingsEntry *
                                      pWsscfgIsSetDot11WEPKeyMappingsEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11WEPKeyMappingsTable
        (&u4ErrorCode, pWsscfgSetDot11WEPKeyMappingsEntry,
         pWsscfgIsSetDot11WEPKeyMappingsEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11WEPKeyMappingsTable
        (pWsscfgSetDot11WEPKeyMappingsEntry,
         pWsscfgIsSetDot11WEPKeyMappingsEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11PrivacyTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11PrivacyEntry
*            pWsscfgIsSetDot11PrivacyEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11PrivacyTable (tCliHandle CliHandle,
                               tWsscfgDot11PrivacyEntry *
                               pWsscfgSetDot11PrivacyEntry,
                               tWsscfgIsSetDot11PrivacyEntry *
                               pWsscfgIsSetDot11PrivacyEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11PrivacyTable
        (&u4ErrorCode, pWsscfgSetDot11PrivacyEntry,
         pWsscfgIsSetDot11PrivacyEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11PrivacyTable
        (pWsscfgSetDot11PrivacyEntry,
         pWsscfgIsSetDot11PrivacyEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11MultiDomainCapabilityTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11MultiDomainCapabilityEntry
*            pWsscfgIsSetDot11MultiDomainCapabilityEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11MultiDomainCapabilityTable (tCliHandle CliHandle,
                                             tWsscfgDot11MultiDomainCapabilityEntry
                                             *
                                             pWsscfgSetDot11MultiDomainCapabilityEntry,
                                             tWsscfgIsSetDot11MultiDomainCapabilityEntry
                                             *
                                             pWsscfgIsSetDot11MultiDomainCapabilityEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11MultiDomainCapabilityTable
        (&u4ErrorCode, pWsscfgSetDot11MultiDomainCapabilityEntry,
         pWsscfgIsSetDot11MultiDomainCapabilityEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11MultiDomainCapabilityTable
        (pWsscfgSetDot11MultiDomainCapabilityEntry,
         pWsscfgIsSetDot11MultiDomainCapabilityEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11SpectrumManagementTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11SpectrumManagementEntry
*            pWsscfgIsSetDot11SpectrumManagementEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11SpectrumManagementTable (tCliHandle CliHandle,
                                          tWsscfgDot11SpectrumManagementEntry *
                                          pWsscfgSetDot11SpectrumManagementEntry,
                                          tWsscfgIsSetDot11SpectrumManagementEntry
                                          *
                                          pWsscfgIsSetDot11SpectrumManagementEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11SpectrumManagementTable
        (&u4ErrorCode, pWsscfgSetDot11SpectrumManagementEntry,
         pWsscfgIsSetDot11SpectrumManagementEntry) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Invalid Inputs\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11SpectrumManagementTable
        (pWsscfgSetDot11SpectrumManagementEntry,
         pWsscfgIsSetDot11SpectrumManagementEntry) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Database Updation failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11RegulatoryClassesTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11RegulatoryClassesEntry
*            pWsscfgIsSetDot11RegulatoryClassesEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11RegulatoryClassesTable (tCliHandle CliHandle,
                                         tWsscfgDot11RegulatoryClassesEntry *
                                         pWsscfgSetDot11RegulatoryClassesEntry,
                                         tWsscfgIsSetDot11RegulatoryClassesEntry
                                         *
                                         pWsscfgIsSetDot11RegulatoryClassesEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11RegulatoryClassesTable
        (&u4ErrorCode, pWsscfgSetDot11RegulatoryClassesEntry,
         pWsscfgIsSetDot11RegulatoryClassesEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11RegulatoryClassesTable
        (pWsscfgSetDot11RegulatoryClassesEntry,
         pWsscfgIsSetDot11RegulatoryClassesEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgCliSetFragmentationThreshold 
* Description :
* Input       :  CliHandle
*                u4RadioType
                 pu1ProfileName
                 pu4RadioId
                 u4FragThreshold 
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT1
WsscfgCliSetFragmentationThreshold (tCliHandle CliHandle, UINT4 u4RadioType,
                                    UINT4 u4FragThreshold,
                                    UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    /*If no Profile name is provided AP MPDU Length is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId, u4currentBindingId,
                 &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (nmhTestv2FsDot11FragmentationThreshold (&u4ErrCode,
                                                            i4RadioIfIndex,
                                                            u4FragThreshold) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11FragmentationThreshold (i4RadioIfIndex,
                                                     u4FragThreshold);
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11AC_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }

        /*If AP name is provided and not the radio id, all radio id's of the
         *          * AP is updated with the AP MPDU Length. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId, &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {

                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId, u4currentBindingId,
                         &i4RadioIfIndex) == SNMP_SUCCESS)
                    {

                        if (nmhTestv2FsDot11FragmentationThreshold (&u4ErrCode,
                                                                    i4RadioIfIndex,
                                                                    u4FragThreshold)
                            == SNMP_FAILURE)
                        {
                            return CLI_FAILURE;
                        }

                        nmhSetFsDot11FragmentationThreshold (i4RadioIfIndex,
                                                             u4FragThreshold);

                    }
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

        }
        /*Only for the particular radio id of the AP, MPDU Length is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (nmhTestv2FsDot11FragmentationThreshold (&u4ErrCode,
                                                            i4RadioIfIndex,
                                                            u4FragThreshold) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11FragmentationThreshold (i4RadioIfIndex,
                                                     u4FragThreshold);
            }
            else
            {
                CLI_SET_ERR (CLI_11AC_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    UNUSED_PARAM (u4RadioType);
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetCapwapDot11WlanTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetCapwapDot11WlanEntry
*            pWsscfgIsSetCapwapDot11WlanEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetCapwapDot11WlanTable (tCliHandle CliHandle,
                                  tWsscfgCapwapDot11WlanEntry *
                                  pWsscfgSetCapwapDot11WlanEntry,
                                  tWsscfgIsSetCapwapDot11WlanEntry *
                                  pWsscfgIsSetCapwapDot11WlanEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllCapwapDot11WlanTable
        (&u4ErrorCode, pWsscfgSetCapwapDot11WlanEntry,
         pWsscfgIsSetCapwapDot11WlanEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllCapwapDot11WlanTable
        (pWsscfgSetCapwapDot11WlanEntry,
         pWsscfgIsSetCapwapDot11WlanEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n DB Updation failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetCapwapDot11WlanBindTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetCapwapDot11WlanBindEntry
*            pWsscfgIsSetCapwapDot11WlanBindEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetCapwapDot11WlanBindTable (tCliHandle CliHandle,
                                      tWsscfgCapwapDot11WlanBindEntry *
                                      pWsscfgSetCapwapDot11WlanBindEntry,
                                      tWsscfgIsSetCapwapDot11WlanBindEntry *
                                      pWsscfgIsSetCapwapDot11WlanBindEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllCapwapDot11WlanBindTable
        (&u4ErrorCode, pWsscfgSetCapwapDot11WlanBindEntry,
         pWsscfgIsSetCapwapDot11WlanBindEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Validation failed for Binding table\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllCapwapDot11WlanBindTable
        (pWsscfgSetCapwapDot11WlanBindEntry,
         pWsscfgIsSetCapwapDot11WlanBindEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Set failed for Binding table\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11OperationTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11OperationEntry
*            pWsscfgIsSetDot11OperationEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11OperationTable (tCliHandle CliHandle,
                                 tWsscfgDot11OperationEntry *
                                 pWsscfgSetDot11OperationEntry,
                                 tWsscfgIsSetDot11OperationEntry *
                                 pWsscfgIsSetDot11OperationEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11OperationTable
        (&u4ErrorCode, pWsscfgSetDot11OperationEntry,
         pWsscfgIsSetDot11OperationEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11OperationTable
        (pWsscfgSetDot11OperationEntry,
         pWsscfgIsSetDot11OperationEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11GroupAddressesTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11GroupAddressesEntry
*            pWsscfgIsSetDot11GroupAddressesEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11GroupAddressesTable (tCliHandle CliHandle,
                                      tWsscfgDot11GroupAddressesEntry *
                                      pWsscfgSetDot11GroupAddressesEntry,
                                      tWsscfgIsSetDot11GroupAddressesEntry *
                                      pWsscfgIsSetDot11GroupAddressesEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11GroupAddressesTable
        (&u4ErrorCode, pWsscfgSetDot11GroupAddressesEntry,
         pWsscfgIsSetDot11GroupAddressesEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11GroupAddressesTable
        (pWsscfgSetDot11GroupAddressesEntry,
         pWsscfgIsSetDot11GroupAddressesEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11EDCATable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11EDCAEntry
*            pWsscfgIsSetDot11EDCAEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11EDCATable (tCliHandle CliHandle,
                            tWsscfgDot11EDCAEntry * pWsscfgSetDot11EDCAEntry,
                            tWsscfgIsSetDot11EDCAEntry *
                            pWsscfgIsSetDot11EDCAEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11EDCATable
        (&u4ErrorCode, pWsscfgSetDot11EDCAEntry,
         pWsscfgIsSetDot11EDCAEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11EDCATable
        (pWsscfgSetDot11EDCAEntry, pWsscfgIsSetDot11EDCAEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11QAPEDCATable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11QAPEDCAEntry
*            pWsscfgIsSetDot11QAPEDCAEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11QAPEDCATable (tCliHandle CliHandle,
                               tWsscfgDot11QAPEDCAEntry *
                               pWsscfgSetDot11QAPEDCAEntry,
                               tWsscfgIsSetDot11QAPEDCAEntry *
                               pWsscfgIsSetDot11QAPEDCAEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11QAPEDCATable
        (&u4ErrorCode, pWsscfgSetDot11QAPEDCAEntry,
         pWsscfgIsSetDot11QAPEDCAEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11QAPEDCATable
        (pWsscfgSetDot11QAPEDCAEntry,
         pWsscfgIsSetDot11QAPEDCAEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11PhyOperationTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11PhyOperationEntry
*            pWsscfgIsSetDot11PhyOperationEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11PhyOperationTable (tCliHandle CliHandle,
                                    tWsscfgDot11PhyOperationEntry *
                                    pWsscfgSetDot11PhyOperationEntry,
                                    tWsscfgIsSetDot11PhyOperationEntry *
                                    pWsscfgIsSetDot11PhyOperationEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11PhyOperationTable
        (&u4ErrorCode, pWsscfgSetDot11PhyOperationEntry,
         pWsscfgIsSetDot11PhyOperationEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11PhyOperationTable
        (pWsscfgSetDot11PhyOperationEntry,
         pWsscfgIsSetDot11PhyOperationEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11PhyAntennaTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11PhyAntennaEntry
*            pWsscfgIsSetDot11PhyAntennaEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11PhyAntennaTable (tCliHandle CliHandle,
                                  tWsscfgDot11PhyAntennaEntry *
                                  pWsscfgSetDot11PhyAntennaEntry,
                                  tWsscfgIsSetDot11PhyAntennaEntry *
                                  pWsscfgIsSetDot11PhyAntennaEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11PhyAntennaTable
        (&u4ErrorCode, pWsscfgSetDot11PhyAntennaEntry,
         pWsscfgIsSetDot11PhyAntennaEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11PhyAntennaTable
        (pWsscfgSetDot11PhyAntennaEntry,
         pWsscfgIsSetDot11PhyAntennaEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11PhyTxPowerTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11PhyTxPowerEntry
*            pWsscfgIsSetDot11PhyTxPowerEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11PhyTxPowerTable (tCliHandle CliHandle,
                                  tWsscfgDot11PhyTxPowerEntry *
                                  pWsscfgSetDot11PhyTxPowerEntry,
                                  tWsscfgIsSetDot11PhyTxPowerEntry *
                                  pWsscfgIsSetDot11PhyTxPowerEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11PhyTxPowerTable
        (&u4ErrorCode, pWsscfgSetDot11PhyTxPowerEntry,
         pWsscfgIsSetDot11PhyTxPowerEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11PhyTxPowerTable
        (pWsscfgSetDot11PhyTxPowerEntry,
         pWsscfgIsSetDot11PhyTxPowerEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11PhyFHSSTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11PhyFHSSEntry
*            pWsscfgIsSetDot11PhyFHSSEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11PhyFHSSTable (tCliHandle CliHandle,
                               tWsscfgDot11PhyFHSSEntry *
                               pWsscfgSetDot11PhyFHSSEntry,
                               tWsscfgIsSetDot11PhyFHSSEntry *
                               pWsscfgIsSetDot11PhyFHSSEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11PhyFHSSTable
        (&u4ErrorCode, pWsscfgSetDot11PhyFHSSEntry,
         pWsscfgIsSetDot11PhyFHSSEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11PhyFHSSTable
        (pWsscfgSetDot11PhyFHSSEntry,
         pWsscfgIsSetDot11PhyFHSSEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11PhyDSSSTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11PhyDSSSEntry
*            pWsscfgIsSetDot11PhyDSSSEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11PhyDSSSTable (tCliHandle CliHandle,
                               tWsscfgDot11PhyDSSSEntry *
                               pWsscfgSetDot11PhyDSSSEntry,
                               tWsscfgIsSetDot11PhyDSSSEntry *
                               pWsscfgIsSetDot11PhyDSSSEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11PhyDSSSTable
        (&u4ErrorCode, pWsscfgSetDot11PhyDSSSEntry,
         pWsscfgIsSetDot11PhyDSSSEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11PhyDSSSTable
        (pWsscfgSetDot11PhyDSSSEntry,
         pWsscfgIsSetDot11PhyDSSSEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11PhyIRTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11PhyIREntry
*            pWsscfgIsSetDot11PhyIREntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11PhyIRTable (tCliHandle CliHandle,
                             tWsscfgDot11PhyIREntry * pWsscfgSetDot11PhyIREntry,
                             tWsscfgIsSetDot11PhyIREntry *
                             pWsscfgIsSetDot11PhyIREntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11PhyIRTable
        (&u4ErrorCode, pWsscfgSetDot11PhyIREntry,
         pWsscfgIsSetDot11PhyIREntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11PhyIRTable
        (pWsscfgSetDot11PhyIREntry,
         pWsscfgIsSetDot11PhyIREntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11AntennasListTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11AntennasListEntry
*            pWsscfgIsSetDot11AntennasListEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11AntennasListTable (tCliHandle CliHandle,
                                    tWsscfgDot11AntennasListEntry *
                                    pWsscfgSetDot11AntennasListEntry,
                                    tWsscfgIsSetDot11AntennasListEntry *
                                    pWsscfgIsSetDot11AntennasListEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11AntennasListTable
        (&u4ErrorCode, pWsscfgSetDot11AntennasListEntry,
         pWsscfgIsSetDot11AntennasListEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11AntennasListTable
        (pWsscfgSetDot11AntennasListEntry,
         pWsscfgIsSetDot11AntennasListEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11PhyOFDMTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11PhyOFDMEntry
*            pWsscfgIsSetDot11PhyOFDMEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11PhyOFDMTable (tCliHandle CliHandle,
                               tWsscfgDot11PhyOFDMEntry *
                               pWsscfgSetDot11PhyOFDMEntry,
                               tWsscfgIsSetDot11PhyOFDMEntry *
                               pWsscfgIsSetDot11PhyOFDMEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11PhyOFDMTable
        (&u4ErrorCode, pWsscfgSetDot11PhyOFDMEntry,
         pWsscfgIsSetDot11PhyOFDMEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11PhyOFDMTable
        (pWsscfgSetDot11PhyOFDMEntry,
         pWsscfgIsSetDot11PhyOFDMEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11HoppingPatternTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11HoppingPatternEntry
*            pWsscfgIsSetDot11HoppingPatternEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11HoppingPatternTable (tCliHandle CliHandle,
                                      tWsscfgDot11HoppingPatternEntry *
                                      pWsscfgSetDot11HoppingPatternEntry,
                                      tWsscfgIsSetDot11HoppingPatternEntry *
                                      pWsscfgIsSetDot11HoppingPatternEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11HoppingPatternTable
        (&u4ErrorCode, pWsscfgSetDot11HoppingPatternEntry,
         pWsscfgIsSetDot11HoppingPatternEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11HoppingPatternTable
        (pWsscfgSetDot11HoppingPatternEntry,
         pWsscfgIsSetDot11HoppingPatternEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11PhyERPTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11PhyERPEntry
*            pWsscfgIsSetDot11PhyERPEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11PhyERPTable (tCliHandle CliHandle,
                              tWsscfgDot11PhyERPEntry *
                              pWsscfgSetDot11PhyERPEntry,
                              tWsscfgIsSetDot11PhyERPEntry *
                              pWsscfgIsSetDot11PhyERPEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11PhyERPTable
        (&u4ErrorCode, pWsscfgSetDot11PhyERPEntry,
         pWsscfgIsSetDot11PhyERPEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11PhyERPTable
        (pWsscfgSetDot11PhyERPEntry,
         pWsscfgIsSetDot11PhyERPEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11RSNAConfigTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11RSNAConfigEntry
*            pWsscfgIsSetDot11RSNAConfigEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11RSNAConfigTable (tCliHandle CliHandle,
                                  tWsscfgDot11RSNAConfigEntry *
                                  pWsscfgSetDot11RSNAConfigEntry,
                                  tWsscfgIsSetDot11RSNAConfigEntry *
                                  pWsscfgIsSetDot11RSNAConfigEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11RSNAConfigTable
        (&u4ErrorCode, pWsscfgSetDot11RSNAConfigEntry,
         pWsscfgIsSetDot11RSNAConfigEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11RSNAConfigTable
        (pWsscfgSetDot11RSNAConfigEntry,
         pWsscfgIsSetDot11RSNAConfigEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11RSNAConfigPairwiseCiphersTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry
*            pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11RSNAConfigPairwiseCiphersTable (tCliHandle CliHandle,
                                                 tWsscfgDot11RSNAConfigPairwiseCiphersEntry
                                                 *
                                                 pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry,
                                                 tWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry
                                                 *
                                                 pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11RSNAConfigPairwiseCiphersTable
        (&u4ErrorCode, pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry,
         pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11RSNAConfigPairwiseCiphersTable
        (pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry,
         pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11RSNAConfigAuthenticationSuitesTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry
*            pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11RSNAConfigAuthenticationSuitesTable (tCliHandle CliHandle,
                                                      tWsscfgDot11RSNAConfigAuthenticationSuitesEntry
                                                      *
                                                      pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry,
                                                      tWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry
                                                      *
                                                      pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11RSNAConfigAuthenticationSuitesTable
        (&u4ErrorCode,
         pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry,
         pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11RSNAConfigAuthenticationSuitesTable
        (pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry,
         pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11StationConfigTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11StationConfigEntry
*            pWsscfgIsSetFsDot11StationConfigEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11StationConfigTable (tCliHandle CliHandle,
                                       tWsscfgFsDot11StationConfigEntry *
                                       pWsscfgSetFsDot11StationConfigEntry,
                                       tWsscfgIsSetFsDot11StationConfigEntry *
                                       pWsscfgIsSetFsDot11StationConfigEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11StationConfigTable
        (&u4ErrorCode, pWsscfgSetFsDot11StationConfigEntry,
         pWsscfgIsSetFsDot11StationConfigEntry) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Input Validation failed \r\n");
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11StationConfigTable
        (pWsscfgSetFsDot11StationConfigEntry,
         pWsscfgIsSetFsDot11StationConfigEntry) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n DB Updation failed \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11CapabilityProfileTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11CapabilityProfileEntry
*            pWsscfgIsSetFsDot11CapabilityProfileEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11CapabilityProfileTable (tCliHandle CliHandle,
                                           tWsscfgFsDot11CapabilityProfileEntry
                                           *
                                           pWsscfgSetFsDot11CapabilityProfileEntry,
                                           tWsscfgIsSetFsDot11CapabilityProfileEntry
                                           *
                                           pWsscfgIsSetFsDot11CapabilityProfileEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11CapabilityProfileTable
        (&u4ErrorCode, pWsscfgSetFsDot11CapabilityProfileEntry,
         pWsscfgIsSetFsDot11CapabilityProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {

        CLI_FATAL_ERROR (CliHandle);
        CliPrintf (CliHandle, "\nInvalid Inputs\n");

        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11CapabilityProfileTable
        (pWsscfgSetFsDot11CapabilityProfileEntry,
         pWsscfgIsSetFsDot11CapabilityProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        CliPrintf (CliHandle, "\nCannot update database\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11AuthenticationProfileTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11AuthenticationProfileEntry
*            pWsscfgIsSetFsDot11AuthenticationProfileEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11AuthenticationProfileTable (tCliHandle CliHandle,
                                               tWsscfgFsDot11AuthenticationProfileEntry
                                               *
                                               pWsscfgSetFsDot11AuthenticationProfileEntry,
                                               tWsscfgIsSetFsDot11AuthenticationProfileEntry
                                               *
                                               pWsscfgIsSetFsDot11AuthenticationProfileEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11AuthenticationProfileTable
        (&u4ErrorCode, pWsscfgSetFsDot11AuthenticationProfileEntry,
         pWsscfgIsSetFsDot11AuthenticationProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        CliPrintf (CliHandle, "\nInvalid Inputs\n");
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11AuthenticationProfileTable
        (pWsscfgSetFsDot11AuthenticationProfileEntry,
         pWsscfgIsSetFsDot11AuthenticationProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        CliPrintf (CliHandle, "\nCannot update database\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthGuestInfoTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsSecurityWebAuthGuestInfoEntry
*            pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthGuestInfoTable (tCliHandle CliHandle,
                                             tWsscfgFsSecurityWebAuthGuestInfoEntry
                                             *
                                             pWsscfgSetFsSecurityWebAuthGuestInfoEntry,
                                             tWsscfgIsSetFsSecurityWebAuthGuestInfoEntry
                                             *
                                             pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsSecurityWebAuthGuestInfoTable
        (&u4ErrorCode, pWsscfgSetFsSecurityWebAuthGuestInfoEntry,
         pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsSecurityWebAuthGuestInfoTable
        (pWsscfgSetFsSecurityWebAuthGuestInfoEntry,
         pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsStationQosParamsTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsStationQosParamsEntry
*            pWsscfgIsSetFsStationQosParamsEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsStationQosParamsTable (tCliHandle CliHandle,
                                     tWsscfgFsStationQosParamsEntry *
                                     pWsscfgSetFsStationQosParamsEntry,
                                     tWsscfgIsSetFsStationQosParamsEntry *
                                     pWsscfgIsSetFsStationQosParamsEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsStationQosParamsTable
        (&u4ErrorCode, pWsscfgSetFsStationQosParamsEntry,
         pWsscfgIsSetFsStationQosParamsEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsStationQosParamsTable
        (pWsscfgSetFsStationQosParamsEntry,
         pWsscfgIsSetFsStationQosParamsEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11RadioConfigTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11RadioConfigEntry
*            pWsscfgIsSetFsDot11RadioConfigEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11RadioConfigTable (tCliHandle CliHandle,
                                     tWsscfgFsDot11RadioConfigEntry *
                                     pWsscfgSetFsDot11RadioConfigEntry,
                                     tWsscfgIsSetFsDot11RadioConfigEntry *
                                     pWsscfgIsSetFsDot11RadioConfigEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11RadioConfigTable
        (&u4ErrorCode, pWsscfgSetFsDot11RadioConfigEntry,
         pWsscfgIsSetFsDot11RadioConfigEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11RadioConfigTable
        (pWsscfgSetFsDot11RadioConfigEntry,
         pWsscfgIsSetFsDot11RadioConfigEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsVlanIsolationTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsVlanIsolationEntry
*            pWsscfgIsSetFsVlanIsolationEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsVlanIsolationTable (tCliHandle CliHandle,
                                  tWsscfgFsVlanIsolationEntry *
                                  pWsscfgSetFsVlanIsolationEntry,
                                  tWsscfgIsSetFsVlanIsolationEntry *
                                  pWsscfgIsSetFsVlanIsolationEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsVlanIsolationTable
        (&u4ErrorCode, pWsscfgSetFsVlanIsolationEntry,
         pWsscfgIsSetFsVlanIsolationEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (WsscfgSetAllFsVlanIsolationTable
        (pWsscfgSetFsVlanIsolationEntry,
         pWsscfgIsSetFsVlanIsolationEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11QosProfileTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11QosProfileEntry
*            pWsscfgIsSetFsDot11QosProfileEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11QosProfileTable (tCliHandle CliHandle,
                                    tWsscfgFsDot11QosProfileEntry *
                                    pWsscfgSetFsDot11QosProfileEntry,
                                    tWsscfgIsSetFsDot11QosProfileEntry *
                                    pWsscfgIsSetFsDot11QosProfileEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11QosProfileTable
        (&u4ErrorCode, pWsscfgSetFsDot11QosProfileEntry,
         pWsscfgIsSetFsDot11QosProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\nInvalid Inputs\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11QosProfileTable
        (pWsscfgSetFsDot11QosProfileEntry,
         pWsscfgIsSetFsDot11QosProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\nDatabase Updation failed\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11WlanCapabilityProfileTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11WlanCapabilityProfileEntry
*            pWsscfgIsSetFsDot11WlanCapabilityProfileEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11WlanCapabilityProfileTable (tCliHandle CliHandle,
                                               tWsscfgFsDot11WlanCapabilityProfileEntry
                                               *
                                               pWsscfgSetFsDot11WlanCapabilityProfileEntry,
                                               tWsscfgIsSetFsDot11WlanCapabilityProfileEntry
                                               *
                                               pWsscfgIsSetFsDot11WlanCapabilityProfileEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11WlanCapabilityProfileTable
        (&u4ErrorCode, pWsscfgSetFsDot11WlanCapabilityProfileEntry,
         pWsscfgIsSetFsDot11WlanCapabilityProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "Invalid Inputs\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11WlanCapabilityProfileTable
        (pWsscfgSetFsDot11WlanCapabilityProfileEntry,
         pWsscfgIsSetFsDot11WlanCapabilityProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "Updation failed\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11WlanAuthenticationProfileTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11WlanAuthenticationProfileEntry
*            pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11WlanAuthenticationProfileTable (tCliHandle CliHandle,
                                                   tWsscfgFsDot11WlanAuthenticationProfileEntry
                                                   *
                                                   pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
                                                   tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry
                                                   *
                                                   pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11WlanAuthenticationProfileTable
        (&u4ErrorCode,
         pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
         pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Invalid input\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11WlanAuthenticationProfileTable
        (pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
         pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Database Updation failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11WlanQosProfileTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11WlanQosProfileEntry
*            pWsscfgIsSetFsDot11WlanQosProfileEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11WlanQosProfileTable (tCliHandle CliHandle,
                                        tWsscfgFsDot11WlanQosProfileEntry *
                                        pWsscfgSetFsDot11WlanQosProfileEntry,
                                        tWsscfgIsSetFsDot11WlanQosProfileEntry *
                                        pWsscfgIsSetFsDot11WlanQosProfileEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11WlanQosProfileTable
        (&u4ErrorCode, pWsscfgSetFsDot11WlanQosProfileEntry,
         pWsscfgIsSetFsDot11WlanQosProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11WlanQosProfileTable
        (pWsscfgSetFsDot11WlanQosProfileEntry,
         pWsscfgIsSetFsDot11WlanQosProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11RadioQosTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11RadioQosEntry
*            pWsscfgIsSetFsDot11RadioQosEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11RadioQosTable (tCliHandle CliHandle,
                                  tWsscfgFsDot11RadioQosEntry *
                                  pWsscfgSetFsDot11RadioQosEntry,
                                  tWsscfgIsSetFsDot11RadioQosEntry *
                                  pWsscfgIsSetFsDot11RadioQosEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11RadioQosTable
        (&u4ErrorCode, pWsscfgSetFsDot11RadioQosEntry,
         pWsscfgIsSetFsDot11RadioQosEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11RadioQosTable
        (pWsscfgSetFsDot11RadioQosEntry,
         pWsscfgIsSetFsDot11RadioQosEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11QAPTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11QAPEntry
*            pWsscfgIsSetFsDot11QAPEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11QAPTable (tCliHandle CliHandle,
                             tWsscfgFsDot11QAPEntry * pWsscfgSetFsDot11QAPEntry,
                             tWsscfgIsSetFsDot11QAPEntry *
                             pWsscfgIsSetFsDot11QAPEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11QAPTable
        (&u4ErrorCode, pWsscfgSetFsDot11QAPEntry,
         pWsscfgIsSetFsDot11QAPEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11QAPTable
        (pWsscfgSetFsDot11QAPEntry,
         pWsscfgIsSetFsDot11QAPEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsQAPProfileTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsQAPProfileEntry
*            pWsscfgIsSetFsQAPProfileEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsQAPProfileTable (tCliHandle CliHandle,
                               tWsscfgFsQAPProfileEntry *
                               pWsscfgSetFsQAPProfileEntry,
                               tWsscfgIsSetFsQAPProfileEntry *
                               pWsscfgIsSetFsQAPProfileEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsQAPProfileTable
        (&u4ErrorCode, pWsscfgSetFsQAPProfileEntry,
         pWsscfgIsSetFsQAPProfileEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsQAPProfileTable
        (pWsscfgSetFsQAPProfileEntry,
         pWsscfgIsSetFsQAPProfileEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11CapabilityMappingTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11CapabilityMappingEntry
*            pWsscfgIsSetFsDot11CapabilityMappingEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11CapabilityMappingTable (tCliHandle CliHandle,
                                           tWsscfgFsDot11CapabilityMappingEntry
                                           *
                                           pWsscfgSetFsDot11CapabilityMappingEntry,
                                           tWsscfgIsSetFsDot11CapabilityMappingEntry
                                           *
                                           pWsscfgIsSetFsDot11CapabilityMappingEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11CapabilityMappingTable
        (&u4ErrorCode, pWsscfgSetFsDot11CapabilityMappingEntry,
         pWsscfgIsSetFsDot11CapabilityMappingEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nInvalid Inputs\r\n");
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11CapabilityMappingTable
        (pWsscfgSetFsDot11CapabilityMappingEntry,
         pWsscfgIsSetFsDot11CapabilityMappingEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nDB Updation Failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11AuthMappingTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11AuthMappingEntry
*            pWsscfgIsSetFsDot11AuthMappingEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11AuthMappingTable (tCliHandle CliHandle,
                                     tWsscfgFsDot11AuthMappingEntry *
                                     pWsscfgSetFsDot11AuthMappingEntry,
                                     tWsscfgIsSetFsDot11AuthMappingEntry *
                                     pWsscfgIsSetFsDot11AuthMappingEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11AuthMappingTable
        (&u4ErrorCode, pWsscfgSetFsDot11AuthMappingEntry,
         pWsscfgIsSetFsDot11AuthMappingEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nInvalid Inputs\r\n");
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11AuthMappingTable
        (pWsscfgSetFsDot11AuthMappingEntry,
         pWsscfgIsSetFsDot11AuthMappingEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nDB Updation Failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11QosMappingTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11QosMappingEntry
*            pWsscfgIsSetFsDot11QosMappingEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11QosMappingTable (tCliHandle CliHandle,
                                    tWsscfgFsDot11QosMappingEntry *
                                    pWsscfgSetFsDot11QosMappingEntry,
                                    tWsscfgIsSetFsDot11QosMappingEntry *
                                    pWsscfgIsSetFsDot11QosMappingEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11QosMappingTable
        (&u4ErrorCode, pWsscfgSetFsDot11QosMappingEntry,
         pWsscfgIsSetFsDot11QosMappingEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nInvalid Inputs\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11QosMappingTable
        (pWsscfgSetFsDot11QosMappingEntry,
         pWsscfgIsSetFsDot11QosMappingEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nDB Updation Failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11AntennasListTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11AntennasListEntry
*            pWsscfgIsSetFsDot11AntennasListEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11AntennasListTable (tCliHandle CliHandle,
                                      tWsscfgFsDot11AntennasListEntry *
                                      pWsscfgSetFsDot11AntennasListEntry,
                                      tWsscfgIsSetFsDot11AntennasListEntry *
                                      pWsscfgIsSetFsDot11AntennasListEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11AntennasListTable
        (&u4ErrorCode, pWsscfgSetFsDot11AntennasListEntry,
         pWsscfgIsSetFsDot11AntennasListEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11AntennasListTable
        (pWsscfgSetFsDot11AntennasListEntry,
         pWsscfgIsSetFsDot11AntennasListEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * * Function    :  WsscfgCliSetFsDot11nConfigTable
 * * Description :
 * * Input       :  CliHandle 
 * *            pWsscfgSetFsDot11nConfigEntry
 * *            pWsscfgIsSetFsDot11nConfigEntry
 * * Output      :  None 
 * * Returns     :  CLI_SUCCESS/CLI_FAILURE
 * ****************************************************************************/
INT4
WsscfgCliSetFsDot11nConfigTable (tCliHandle CliHandle,
                                 tWsscfgFsDot11nConfigEntry *
                                 pWsscfgSetFsDot11nConfigEntry,
                                 tWsscfgIsSetFsDot11nConfigEntry *
                                 pWsscfgIsSetFsDot11nConfigEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11nConfigTable
        (&u4ErrorCode, pWsscfgSetFsDot11nConfigEntry,
         pWsscfgIsSetFsDot11nConfigEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11nConfigTable
        (pWsscfgSetFsDot11nConfigEntry,
         pWsscfgIsSetFsDot11nConfigEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * * Function    :  WsscfgCliSetFsDot11nMCSDataRateTable
 * * Description :
 * * Input       :  CliHandle 
 * *            pWsscfgSetFsDot11nMCSDataRateEntry
 * *            pWsscfgIsSetFsDot11nMCSDataRateEntry
 * * Output      :  None 
 * * Returns     :  CLI_SUCCESS/CLI_FAILURE
 * ****************************************************************************/
INT4
WsscfgCliSetFsDot11nMCSDataRateTable (tCliHandle CliHandle,
                                      tWsscfgFsDot11nMCSDataRateEntry *
                                      pWsscfgSetFsDot11nMCSDataRateEntry,
                                      tWsscfgIsSetFsDot11nMCSDataRateEntry *
                                      pWsscfgIsSetFsDot11nMCSDataRateEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11nMCSDataRateTable
        (&u4ErrorCode, pWsscfgSetFsDot11nMCSDataRateEntry,
         pWsscfgIsSetFsDot11nMCSDataRateEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11nMCSDataRateTable
        (pWsscfgSetFsDot11nMCSDataRateEntry,
         pWsscfgIsSetFsDot11nMCSDataRateEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsWtpImageUpgradeTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsWtpImageUpgradeEntry
*            pWsscfgIsSetFsWtpImageUpgradeEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsWtpImageUpgradeTable (tCliHandle CliHandle,
                                    tWsscfgFsWtpImageUpgradeEntry *
                                    pWsscfgSetFsWtpImageUpgradeEntry,
                                    tWsscfgIsSetFsWtpImageUpgradeEntry *
                                    pWsscfgIsSetFsWtpImageUpgradeEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsWtpImageUpgradeTable
        (&u4ErrorCode, pWsscfgSetFsWtpImageUpgradeEntry,
         pWsscfgIsSetFsWtpImageUpgradeEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsWtpImageUpgradeTable
        (pWsscfgSetFsWtpImageUpgradeEntry,
         pWsscfgIsSetFsWtpImageUpgradeEntry, OSIX_FALSE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11WlanTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11WlanEntry
*            pWsscfgIsSetFsDot11WlanEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11WlanTable (tCliHandle CliHandle,
                              tWsscfgFsDot11WlanEntry *
                              pWsscfgSetFsDot11WlanEntry,
                              tWsscfgIsSetFsDot11WlanEntry *
                              pWsscfgIsSetFsDot11WlanEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11WlanTable
        (&u4ErrorCode, pWsscfgSetFsDot11WlanEntry,
         pWsscfgIsSetFsDot11WlanEntry, OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11WlanTable
        (pWsscfgSetFsDot11WlanEntry, pWsscfgIsSetFsDot11WlanEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11WlanBindTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11WlanBindEntry
*            pWsscfgIsSetFsDot11WlanBindEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11WlanBindTable (tCliHandle CliHandle,
                                  tWsscfgFsDot11WlanBindEntry *
                                  pWsscfgSetFsDot11WlanBindEntry,
                                  tWsscfgIsSetFsDot11WlanBindEntry *
                                  pWsscfgIsSetFsDot11WlanBindEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11WlanBindTable
        (&u4ErrorCode, pWsscfgSetFsDot11WlanBindEntry,
         pWsscfgIsSetFsDot11WlanBindEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11WlanBindTable
        (pWsscfgSetFsDot11WlanBindEntry,
         pWsscfgIsSetFsDot11WlanBindEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetMaxClientCountPerSSID
* Description :
* Input       :  CliHandle 
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgCliSetMaxClientCountPerSSID (tCliHandle CliHandle, UINT4 u4MaxClientCount,
                                   UINT4 u4WlanProfileId)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsWlanSSIDStatsMaxClientCount (&u4ErrorCode, u4WlanProfileId,
                                                u4MaxClientCount) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Getting radio index failed \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsWlanSSIDStatsMaxClientCount (u4WlanProfileId,
                                             u4MaxClientCount) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetMaxClientCountperRadio
* Description :
* Input       :  CliHandle
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgCliSetMaxClientCountPerRadio (tCliHandle CliHandle,
                                    UINT4 u4MaxClientCount,
                                    UINT1 *pu1ProfileName, UINT4 u4RadioId)
{
    INT4                i4RadioIfIndex = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4CapwapBaseWtpProfileId = 0;

    if (pu1ProfileName != NULL)
    {

        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile entry not found\r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
        (u4CapwapBaseWtpProfileId, u4RadioId, &i4RadioIfIndex) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nRadio Index not found\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsWlanRadioMaxClientCount (&u4ErrorCode, i4RadioIfIndex,
                                            u4MaxClientCount) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Getting radio index failed \r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsWlanRadioMaxClientCount (i4RadioIfIndex,
                                         u4MaxClientCount) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11aNetworkEnable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11aNetworkEnable (tCliHandle CliHandle,
                                   UINT4 *pFsDot11aNetworkEnable)
{
    UINT4               u4ErrorCode;
    INT4                i4FsDot11aNetworkEnable = 0;

    WSSCFG_FILL_FSDOT11ANETWORKENABLE (i4FsDot11aNetworkEnable,
                                       pFsDot11aNetworkEnable);

    if (WsscfgTestFsDot11aNetworkEnable
        (&u4ErrorCode, i4FsDot11aNetworkEnable) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Invalid Configuration received\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetFsDot11aNetworkEnable (i4FsDot11aNetworkEnable) !=
        OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Network Configuration failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11bNetworkEnable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11bNetworkEnable (tCliHandle CliHandle,
                                   UINT4 *pFsDot11bNetworkEnable)
{
    UINT4               u4ErrorCode;
    INT4                i4FsDot11bNetworkEnable = 0;

    WSSCFG_FILL_FSDOT11BNETWORKENABLE (i4FsDot11bNetworkEnable,
                                       pFsDot11bNetworkEnable);

    if (WsscfgTestFsDot11bNetworkEnable
        (&u4ErrorCode, i4FsDot11bNetworkEnable) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsDot11bNetworkEnable (i4FsDot11bNetworkEnable) !=
        OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Network Configuration failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11gSupport
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11gSupport (tCliHandle CliHandle, UINT4 *pFsDot11gSupport)
{
    UINT4               u4ErrorCode;
    INT4                i4FsDot11gSupport = 0;

    WSSCFG_FILL_FSDOT11GSUPPORT (i4FsDot11gSupport, pFsDot11gSupport);

    if (WsscfgTestFsDot11gSupport (&u4ErrorCode, i4FsDot11gSupport) !=
        OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsDot11gSupport (i4FsDot11gSupport) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11anSupport
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11anSupport (tCliHandle CliHandle, UINT4 *pFsDot11anSupport)
{
    UINT4               u4ErrorCode;
    INT4                i4FsDot11anSupport = 0;

    WSSCFG_FILL_FSDOT11ANSUPPORT (i4FsDot11anSupport, pFsDot11anSupport);

    if (WsscfgTestFsDot11anSupport (&u4ErrorCode, i4FsDot11anSupport)
        != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsDot11anSupport (i4FsDot11anSupport) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11bnSupport
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11bnSupport (tCliHandle CliHandle, UINT4 *pFsDot11bnSupport)
{
    UINT4               u4ErrorCode;
    INT4                i4FsDot11bnSupport = 0;

    WSSCFG_FILL_FSDOT11BNSUPPORT (i4FsDot11bnSupport, pFsDot11bnSupport);

    if (WsscfgTestFsDot11bnSupport (&u4ErrorCode, i4FsDot11bnSupport)
        != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsDot11bnSupport (i4FsDot11bnSupport) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11ManagmentSSID
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11ManagmentSSID (tCliHandle CliHandle,
                                  UINT4 *pFsDot11ManagmentSSID)
{

    UINT4               u4ErrorCode;
    UINT2               u2FsDot11ManagmentSSID = 0;

    WSSCFG_FILL_FSDOT11MANAGMENTSSID (u2FsDot11ManagmentSSID,
                                      pFsDot11ManagmentSSID);

    if (WsscfgTestFsDot11ManagmentSSID
        (&u4ErrorCode, (UINT4) u2FsDot11ManagmentSSID) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Invalid Inputs \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetFsDot11ManagmentSSID ((UINT4) u2FsDot11ManagmentSSID)
        != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n DB Updation failed \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11CountryString
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11CountryString (tCliHandle CliHandle,
                                  UINT4 *pFsDot11CountryString)
{
    UINT4               u4ErrorCode;
    UINT1               au1FsDot11CountryString[3];

    MEMSET (au1FsDot11CountryString, 0, 3);
    if (pFsDot11CountryString == NULL)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    WSSCFG_FILL_FSDOT11COUNTRYSTRING (au1FsDot11CountryString,
                                      pFsDot11CountryString);
    au1FsDot11CountryString[STRLEN (pFsDot11CountryString)] = '\0';

    if (WsscfgTestFsDot11CountryString
        (&u4ErrorCode, au1FsDot11CountryString) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsDot11CountryString (au1FsDot11CountryString) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthType
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthType (tCliHandle CliHandle,
                                   UINT4 *pFsSecurityWebAuthType,
                                   UINT4 *pWlanProfileId)
{
    UINT4               u4ErrorCode;
    INT4                i4FsSecurityWebAuthType = 0;
    INT4                i4WlanIfIndex = 0;

    WSSCFG_FILL_FSSECURITYWEBAUTHTYPE (i4FsSecurityWebAuthType,
                                       pFsSecurityWebAuthType);

    if (nmhGetFsDot11WlanProfileIfIndex (*pWlanProfileId, &i4WlanIfIndex) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDot11ExternalWebAuthMethod
        (&u4ErrorCode, i4WlanIfIndex, i4FsSecurityWebAuthType) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsDot11ExternalWebAuthMethod
        (i4WlanIfIndex, i4FsSecurityWebAuthType) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthUrl
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthUrl (tCliHandle CliHandle,
                                  UINT1 *pFsSecurityWebAuthUrl,
                                  UINT4 *pWlanProfileId)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE WebAuthUrl;
    INT4                i4WlanIfIndex = 0;
    MEMSET (&WebAuthUrl, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    WebAuthUrl.i4_Length = STRLEN (pFsSecurityWebAuthUrl);
    WebAuthUrl.pu1_OctetList = pFsSecurityWebAuthUrl;

    if (nmhGetFsDot11WlanProfileIfIndex (*pWlanProfileId, &i4WlanIfIndex) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsDot11ExternalWebAuthUrl
        (&u4ErrorCode, i4WlanIfIndex, &WebAuthUrl) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsDot11ExternalWebAuthUrl (i4WlanIfIndex, &WebAuthUrl) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    UNUSED_PARAM (u4ErrorCode);
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthRedirectUrl
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthRedirectUrl (tCliHandle CliHandle,
                                          UINT4 *pFsSecurityWebAuthRedirectUrl)
{
    UINT4               u4ErrorCode;
    UINT1               au1FsSecurityWebAuthRedirectUrl[256];

    MEMSET (au1FsSecurityWebAuthRedirectUrl, 0, 256);

    WSSCFG_FILL_FSSECURITYWEBAUTHREDIRECTURL
        (au1FsSecurityWebAuthRedirectUrl, pFsSecurityWebAuthRedirectUrl);

    if (WsscfgTestFsSecurityWebAuthRedirectUrl
        (&u4ErrorCode, au1FsSecurityWebAuthRedirectUrl) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsSecurityWebAuthRedirectUrl
        (au1FsSecurityWebAuthRedirectUrl) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAddr
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAddr (tCliHandle CliHandle, UINT4 *pFsSecurityWebAddr)
{
    UINT4               u4ErrorCode;
    INT4                i4FsSecurityWebAddr = 0;

    WSSCFG_FILL_FSSECURITYWEBADDR (i4FsSecurityWebAddr, pFsSecurityWebAddr);

    if (WsscfgTestFsSecurityWebAddr (&u4ErrorCode, i4FsSecurityWebAddr)
        != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsSecurityWebAddr (i4FsSecurityWebAddr) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthWebTitle
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthWebTitle (tCliHandle CliHandle,
                                       UINT1 *pFsSecurityWebAuthWebTitle)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE WebAuthTitle;
    MEMSET (&WebAuthTitle, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    WebAuthTitle.i4_Length = STRLEN (pFsSecurityWebAuthWebTitle);
    WebAuthTitle.pu1_OctetList = pFsSecurityWebAuthWebTitle;

    if (WsscfgTestFsSecurityWebAuthWebTitle
        (&u4ErrorCode, &WebAuthTitle) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsSecurityWebAuthWebTitle (&WebAuthTitle) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliClearFsSecurityWebAuthWebTitle
* Description :
* Input       :  CliHandle 

* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliClearFsSecurityWebAuthWebTitle (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);

    if ((STRCMP
         ((gWsscfgGlobals.WsscfgGlbMib.au1FsSecurityWebAuthWebTitle),
          "")) != OSIX_FALSE)
    {
        MEMSET (gWsscfgGlobals.WsscfgGlbMib.
                au1FsSecurityWebAuthWebTitle, 0,
                sizeof (gWsscfgGlobals.
                        WsscfgGlbMib.au1FsSecurityWebAuthWebTitle));
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthWebMessage
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthWebMessage (tCliHandle CliHandle,
                                         UINT1 *pFsSecurityWebAuthWebMessage)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE WebAuthMessage;
    MEMSET (&WebAuthMessage, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    WebAuthMessage.i4_Length = STRLEN (pFsSecurityWebAuthWebMessage);
    WebAuthMessage.pu1_OctetList = pFsSecurityWebAuthWebMessage;

    if (WsscfgTestFsSecurityWebAuthWebMessage
        (&u4ErrorCode, &WebAuthMessage) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsSecurityWebAuthWebMessage (&WebAuthMessage) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliClearFsSecurityWebAuthWebMessage
* Description :
* Input       :  CliHandle 
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliClearFsSecurityWebAuthWebMessage (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);

    if ((STRCMP
         ((gWsscfgGlobals.WsscfgGlbMib.au1FsSecurityWebAuthWebMessage),
          "")) != OSIX_FALSE)
    {
        MEMSET (gWsscfgGlobals.WsscfgGlbMib.
                au1FsSecurityWebAuthWebMessage, 0,
                sizeof (gWsscfgGlobals.
                        WsscfgGlbMib.au1FsSecurityWebAuthWebMessage));
    }

    return CLI_SUCCESS;

}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthWebLogoFileName
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthWebLogoFileName (tCliHandle CliHandle,
                                              UINT4 *pWebLogoStatus,
                                              UINT1
                                              *pFsSecurityWebAuthWebLogoFileName)
{
    UINT4               u4ErrorCode;
    INT4                i4WebLogoStatus;
    tSNMP_OCTET_STRING_TYPE WebAuthLogo;
    MEMSET (&WebAuthLogo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    i4WebLogoStatus = *(INT4 *) pWebLogoStatus;

    if (i4WebLogoStatus == WSS_AUTH_STATUS_ENABLE)
    {
        WebAuthLogo.i4_Length = STRLEN (pFsSecurityWebAuthWebLogoFileName);
        WebAuthLogo.pu1_OctetList = pFsSecurityWebAuthWebLogoFileName;
        if (WsscfgTestFsSecurityWebAuthWebLogoFileName
            (&u4ErrorCode, &WebAuthLogo) != OSIX_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (WsscfgSetFsSecurityWebAuthWebLogoFileName (&WebAuthLogo) !=
            OSIX_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if ((strlen
             ((char *) gWsscfgGlobals.
              WsscfgGlbMib.au1FsSecurityWebAuthWebLogoFileName)) != 0)
        {
            /* Reset the Logo filename value to 0,if the command is to DISABLE */
            MEMSET (gWsscfgGlobals.
                    WsscfgGlbMib.au1FsSecurityWebAuthWebLogoFileName,
                    0,
                    sizeof (gWsscfgGlobals.
                            WsscfgGlbMib.au1FsSecurityWebAuthWebLogoFileName));
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthWebSuccMessage
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthWebSuccMessage (tCliHandle CliHandle,
                                             UINT1
                                             *pFsSecurityWebAuthWebSuccMessage)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE WebAuthSuccMsg;
    MEMSET (&WebAuthSuccMsg, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    WebAuthSuccMsg.i4_Length = STRLEN (pFsSecurityWebAuthWebSuccMessage);
    WebAuthSuccMsg.pu1_OctetList = pFsSecurityWebAuthWebSuccMessage;

    if (WsscfgTestFsSecurityWebAuthWebSuccMessage
        (&u4ErrorCode, &WebAuthSuccMsg) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsSecurityWebAuthWebSuccMessage (&WebAuthSuccMsg) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgCliClearFsSecurityWebAuthWebSuccMessage 
* Description :
* Input       :  CliHandle 
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliClearFsSecurityWebAuthWebSuccMessage (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);

    if ((STRCMP ((gWsscfgGlobals.WsscfgGlbMib.
                  au1FsSecurityWebAuthWebSuccMessage), "")) != OSIX_FALSE)
    {

        MEMSET (gWsscfgGlobals.
                WsscfgGlbMib.au1FsSecurityWebAuthWebSuccMessage, 0,
                sizeof (gWsscfgGlobals.
                        WsscfgGlbMib.au1FsSecurityWebAuthWebSuccMessage));
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthWebFailMessage
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthWebFailMessage (tCliHandle CliHandle,
                                             UINT1
                                             *pFsSecurityWebAuthWebFailMessage)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE WebAuthFailMsg;
    MEMSET (&WebAuthFailMsg, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    WebAuthFailMsg.i4_Length = STRLEN (pFsSecurityWebAuthWebFailMessage);
    WebAuthFailMsg.pu1_OctetList = pFsSecurityWebAuthWebFailMessage;

    if (WsscfgTestFsSecurityWebAuthWebFailMessage
        (&u4ErrorCode, &WebAuthFailMsg) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsSecurityWebAuthWebFailMessage (&WebAuthFailMsg) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsWlanStationTrapStatus
* Description :
* Input       :  CliHandle
*            pFsWlanStationTrapStatus
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
WsscfgCliSetFsWlanStationTrapStatus (tCliHandle CliHandle,
                                     UINT4 *pFsWlanStationTrapStatus)
{
    UINT4               u4ErrorCode;
    INT4                i4FsWlanStationTrapStatus = 0;

    i4FsWlanStationTrapStatus = *(INT4 *) (pFsWlanStationTrapStatus);
    if (nmhTestv2FsWlanStationTrapStatus
        (&u4ErrorCode, i4FsWlanStationTrapStatus) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsWlanStationTrapStatus (i4FsWlanStationTrapStatus) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/****************************************************************************
* Function    :  WsscfgCliSetDscp
* Description :
* Input       :  CliHandle
*            pFsWlanStationTrapStatus
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
WsscfgCliSetDscp (tCliHandle CliHandle, UINT4 u4RadioId, UINT4 u4WlanId,
                  UINT1 *pu1ProfileName,
                  UINT4 u4DscpInPriority, UINT4 u4OutDscp)
{
    INT4                i4RadioIfIndex = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4WlanProfileId = 0;
    UINT4               u4NextWlanProfielId = 0;
    UINT4               u4GetWlanId = 0;
    INT4                i4NextIndex = 0;

    /*if Ethernet interface needed to be updated */
    if (u4RadioId == 0)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) == OSIX_SUCCESS)
        {
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, 1, &i4RadioIfIndex) != SNMP_FAILURE)
            {
                if (WsscfgCliSetDscpTable
                    (CliHandle, i4RadioIfIndex, 0, u4DscpInPriority,
                     u4OutDscp) == CLI_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
            else
            {
                CliPrintf (CliHandle, "\r\nFailed in Fetching RadioIfIndex\n");
            }
        }
        else
        {
            CliPrintf (CliHandle, "\r\nProfile Name not found\n");
        }

    }
    else
    {
        /* if a particular wlan interface in an AP radio needed to be updated (Particular WLAN in AP) */
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) == OSIX_SUCCESS)
        {
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) != SNMP_FAILURE)
            {
                while (nmhGetNextIndexCapwapDot11WlanBindTable
                       (i4RadioIfIndex, &i4NextIndex, u4WlanProfileId,
                        &u4NextWlanProfielId) != SNMP_FAILURE)
                {
                    if (i4RadioIfIndex != i4NextIndex)
                    {
                        CliPrintf (CliHandle, "\r\nWlan Id not found\n");
                        break;
                    }
                    u4WlanProfileId = u4NextWlanProfielId;
                    nmhGetCapwapDot11WlanBindWlanId (i4RadioIfIndex,
                                                     u4WlanProfileId,
                                                     &u4GetWlanId);
                    if (u4WlanId == u4GetWlanId)
                    {
                        if (WsscfgCliSetDscpTable
                            (CliHandle, i4RadioIfIndex, u4WlanProfileId,
                             u4DscpInPriority, u4OutDscp) == SNMP_FAILURE)
                        {
                            return CLI_FAILURE;
                        }
                        /* Call appropriate function */
                        break;
                    }
                }

            }
            else
            {
                CliPrintf (CliHandle, "\r\nFailed in Getting RadioIfIndex\n");
            }

        }
        else
        {
            CliPrintf (CliHandle, "\r\nProfile Name not found\n");
        }
    }
    /* if ethernet interface of all AP needed to be updated */
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDscpTable
* Description :
* Input       :  CliHandle
*            pFsWlanStationTrapStatus
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
WsscfgCliSetDscpTable (tCliHandle CliHandle, INT4 i4IfIndex,
                       UINT4 u4WlanProfileId, UINT4 u4DscpInPriority,
                       UINT4 u4OutDscp)
{
    INT4                i4RowStatus = 0;
    UINT4               u4ErrorCode = 0;

    if (nmhGetFsDot11DscpRowStatus (i4IfIndex, u4WlanProfileId, &i4RowStatus) !=
        SNMP_FAILURE)
    {
        i4RowStatus = NOT_IN_SERVICE;
        if (nmhTestv2FsDot11DscpRowStatus
            (&u4ErrorCode, i4IfIndex, u4WlanProfileId,
             i4RowStatus) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nFailed in Valdiation of RowStatus\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsDot11DscpRowStatus (i4IfIndex, u4WlanProfileId, i4RowStatus)
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nFailed in Set of RowStatus\n");
            return CLI_FAILURE;
        }
        if (nmhTestv2FsDot11DscpInPriority
            (&u4ErrorCode, i4IfIndex, u4WlanProfileId,
             u4DscpInPriority) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nFailed in Validatiom of InPriority\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsDot11DscpInPriority
            (i4IfIndex, u4WlanProfileId, u4DscpInPriority) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nFailed in Set of InPriority\n");
            return CLI_FAILURE;

        }
        if (nmhTestv2FsDot11OutDscp
            (&u4ErrorCode, i4IfIndex, u4WlanProfileId,
             u4OutDscp) != SNMP_SUCCESS)
        {

            CliPrintf (CliHandle, "\r\nFailed in Validation of OutDscp\n");
            return CLI_FAILURE;

        }
        if (nmhSetFsDot11OutDscp (i4IfIndex, u4WlanProfileId, u4OutDscp) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nFailed in Set of OutDscp\n");
            return CLI_FAILURE;

        }
        i4RowStatus = ACTIVE;
        if (nmhTestv2FsDot11DscpRowStatus
            (&u4ErrorCode, i4IfIndex, u4WlanProfileId,
             i4RowStatus) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nFailed in Validation of RowStatus\n");
            return CLI_FAILURE;

        }
        if (nmhSetFsDot11DscpRowStatus (i4IfIndex, u4WlanProfileId, i4RowStatus)
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nFailed in Set of RowStatus\n");
            return CLI_FAILURE;

        }
    }
    else
    {
        i4RowStatus = CREATE_AND_WAIT;
        if (nmhTestv2FsDot11DscpRowStatus (&u4ErrorCode, i4IfIndex,
                                           u4WlanProfileId,
                                           i4RowStatus) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nFailed in Validation of RowStatus\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsDot11DscpRowStatus (i4IfIndex,
                                        u4WlanProfileId,
                                        i4RowStatus) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nFailed in Set of RowStatus\n");
            return CLI_FAILURE;
        }
        if (nmhTestv2FsDot11DscpInPriority (&u4ErrorCode,
                                            i4IfIndex, u4WlanProfileId,
                                            u4DscpInPriority) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nFailed in Validation on Inpriority\n");
            return CLI_FAILURE;
        }
        if (nmhTestv2FsDot11OutDscp (&u4ErrorCode,
                                     i4IfIndex, u4WlanProfileId,
                                     u4OutDscp) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nFailed in Validation of OutDscp\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsDot11DscpRowStatus (i4IfIndex,
                                        u4WlanProfileId,
                                        i4RowStatus) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nFailed in Set of RowStatus\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsDot11DscpInPriority (i4IfIndex,
                                         u4WlanProfileId,
                                         u4DscpInPriority) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nFailed in Set of InPriority\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsDot11OutDscp (i4IfIndex,
                                  u4WlanProfileId, u4OutDscp) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nFailed in Set of OutDscp\n");
            return CLI_FAILURE;
        }
    }
    i4RowStatus = ACTIVE;
    if (nmhSetFsDot11DscpRowStatus (i4IfIndex,
                                    u4WlanProfileId,
                                    i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nFailed in Set of RowStatus\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

#ifdef BAND_SELECT_WANTED

/****************************************************************************
* Function    :  WsscfgCliSetBandSelect
* Description :
* Input       :  CliHandle
*            pFsWlanStationTrapStatus
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
WsscfgCliSetBandSelect (tCliHandle CliHandle, UINT1 u1BandSelectStatus,
                        UINT4 u4WlanId)
{
    UINT4               u4ErrorCode;
    if (u4WlanId != 0)
    {
        if (nmhTestv2FsBandSelectPerWlanStatus
            (&u4ErrorCode, u4WlanId, (INT4) u1BandSelectStatus) != SNMP_FAILURE)
        {
            nmhSetFsBandSelectPerWlanStatus (u4WlanId,
                                             (INT4) u1BandSelectStatus);
            return CLI_SUCCESS;
        }
        else
        {
            CliPrintf (CliHandle, "Invalid profile id\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsBandSelectStatus
            (&u4ErrorCode, (INT4) u1BandSelectStatus) != SNMP_FAILURE)
        {
            nmhSetFsBandSelectStatus (u1BandSelectStatus);
            return CLI_SUCCESS;
        }
        else
        {
            CliPrintf (CliHandle, "Invalid band select status");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
}

/****************************************************************************
* Function    :  WsscfgCliSetAssocRejectCount
* Description :
* Input       :  CliHandle
*            pFsWlanStationTrapStatus
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
WsscfgCliSetAssocRejectCount (tCliHandle CliHandle, UINT1 u1RejectCount,
                              UINT4 u4WlanId)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsBandSelectAssocCount
        (&u4ErrorCode, u4WlanId, (UINT4) u1RejectCount) != SNMP_FAILURE)
    {
        nmhSetFsBandSelectAssocCount (u4WlanId, (UINT4) u1RejectCount);
        return CLI_SUCCESS;
    }
    else
    {
        CliPrintf (CliHandle, "Invalid profile id\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
}

/****************************************************************************
* Function    :  WsscfgCliSetAssocResetTimer
* Description :
* Input       :  CliHandle
*            pFsWlanStationTrapStatus
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
WsscfgCliSetAssocResetTimer (tCliHandle CliHandle, UINT1 u1ResetTimer,
                             UINT4 u4WlanId)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsBandSelectAssocCountFlushTimer
        (&u4ErrorCode, u4WlanId, (UINT4) u1ResetTimer) != SNMP_FAILURE)
    {
        nmhSetFsBandSelectAssocCountFlushTimer (u4WlanId, (UINT4) u1ResetTimer);
        return CLI_SUCCESS;
    }
    else
    {
        CliPrintf (CliHandle, "Invalid profile id\n ");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
}

/****************************************************************************
* Function    :  WsscfgCliSetProbeAgeoutTimer
* Description :
* Input       :  CliHandle
*            pFsWlanStationTrapStatus
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
WsscfgCliSetProbeAgeoutTimer (tCliHandle CliHandle, UINT1 u1AgeOutTimer,
                              UINT4 u4WlanId)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsBandSelectAgeOutSuppTimer
        (&u4ErrorCode, u4WlanId, (UINT4) u1AgeOutTimer) != SNMP_FAILURE)
    {
        nmhSetFsBandSelectAgeOutSuppTimer (u4WlanId, (UINT4) u1AgeOutTimer);
        return CLI_SUCCESS;
    }
    else
    {
        CliPrintf (CliHandle, "Invalid profile id\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
}

#endif
/****************************************************************************
* Function    :  WsscfgCliSetLegacyRates
* Description :
* Input       :  CliHandle
*            pFsWlanStationTrapStatus
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
WsscfgCliSetLegacyRates (tCliHandle CliHandle, UINT1 u1LegacyRate)
{
    UINT4               u4ErrorCode;
    if (nmhTestv2FsBandSelectLegacyRate (&u4ErrorCode, (INT4) u1LegacyRate) !=
        SNMP_FAILURE)
    {
        nmhSetFsBandSelectLegacyRate ((INT4) u1LegacyRate);
        return CLI_SUCCESS;
    }
    else
    {
        CliPrintf (CliHandle, "Unable to set Legacy rate\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
}

/****************************************************************************
* Function    :  WsscfgCliShowLegacyRate
* Description :
* Input       :  CliHandle
* Output      :  None
* Returns     :  CLI_SUCCESS
****************************************************************************/

INT4
WsscfgCliShowLegacyRate (tCliHandle CliHandle)
{
    INT4                i4LegacyRateStatus = 0;
    if (nmhGetFsBandSelectLegacyRate (&i4LegacyRateStatus) != SNMP_FAILURE)
    {
        if (i4LegacyRateStatus == 1)
        {
            CliPrintf (CliHandle, "Legacy Rate Status : Enabled \r\n\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Legacy Rate Status : Disabled \r\n\r\n");
        }
    }
    return CLI_SUCCESS;
}

#ifdef BAND_SELECT_WANTED
/****************************************************************************
* Function    :  WsscfgCliShowBandSelect
* Description :
* Input       :  CliHandle
*            pFsWlanStationTrapStatus
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
WsscfgCliShowBandSelect (tCliHandle CliHandle, UINT4 u4WlanProfileId)
{
    UINT4               u4NextWlanProfileId = 0;
    UINT4               u4FirstWlanProfileId = 0;
    INT4                i4RetValCapwapDot11WlanRowStatus = 0;
    INT4                i4BandSelectPerWlanStatus = 0;
    INT4                i4BandSelectStatus = 0;
    UINT4               u4BandSelectAssocCount = 0;
    UINT4               u4BandSelectAgeOutSuppTimer = 0;
    UINT4               u4BandSelectAssocCountFlushTimer = 0;

    CliPrintf (CliHandle, "\r\nGLOBAL STATUS :\r\n");
    if (nmhGetFsBandSelectStatus (&i4BandSelectStatus) != SNMP_FAILURE)
    {
        if (i4BandSelectStatus == 1)
        {
            CliPrintf (CliHandle, "Band Select Status : Enabled \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Band Select Status : Disabled \r\n");
        }
    }
    if (u4WlanProfileId == 0)
    {
        if (nmhGetFirstIndexFsWlanBandSelectTable (&u4FirstWlanProfileId) !=
            SNMP_FAILURE)
        {
            do
            {
                u4NextWlanProfileId = u4FirstWlanProfileId;

                CliPrintf (CliHandle, "Wlan ID : %d\r\n", u4NextWlanProfileId);
                if (nmhGetFsBandSelectPerWlanStatus (u4NextWlanProfileId,
                                                     &i4BandSelectPerWlanStatus)
                    != SNMP_FAILURE)
                {
                    if (i4BandSelectPerWlanStatus == 1)
                    {
                        CliPrintf (CliHandle, "Band Select Status             "
                                   "     : Enabled \r\n");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "Band Select Status              "
                                   "    : Disabled \r\n");
                    }
                }
                if (nmhGetFsBandSelectAssocCount (u4NextWlanProfileId,
                                                  &u4BandSelectAssocCount) !=
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "Band Select Associatiom Count       :"
                               " %d\r\n", u4BandSelectAssocCount);
                }
                if (nmhGetFsBandSelectAgeOutSuppTimer (u4NextWlanProfileId,
                                                       &u4BandSelectAgeOutSuppTimer)
                    != SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "Band Select Age Out Support Timer   :"
                               " %d\r\n", u4BandSelectAgeOutSuppTimer);
                }
                if (nmhGetFsBandSelectAssocCountFlushTimer (u4NextWlanProfileId,
                                                            &u4BandSelectAssocCountFlushTimer)
                    != SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "Band Select Assoc Count Flush Timer "
                               ": %d\r\n\r\n",
                               u4BandSelectAssocCountFlushTimer);
                }
            }
            while (nmhGetNextIndexFsWlanBandSelectTable (u4NextWlanProfileId,
                                                         &u4FirstWlanProfileId));
        }
    }
    else
    {
        if (nmhGetCapwapDot11WlanRowStatus (u4WlanProfileId,
                                            &i4RetValCapwapDot11WlanRowStatus)
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "WLAN profile ID is wrong\r\n");
            return CLI_FAILURE;
        }
        else
        {
            CliPrintf (CliHandle, "Wlan ID : %d\r\n", u4WlanProfileId);
        }
        if (nmhGetFsBandSelectPerWlanStatus (u4WlanProfileId,
                                             &i4BandSelectPerWlanStatus) !=
            SNMP_FAILURE)
        {
            if (i4BandSelectPerWlanStatus == 1)
            {
                CliPrintf (CliHandle, "Band Select Status               "
                           "   : Enabled \r\n");
            }
            else
            {
                CliPrintf (CliHandle, "Band Select Status                "
                           "  : Disabled \r\n");
            }
        }
        if (nmhGetFsBandSelectAssocCount (u4WlanProfileId,
                                          &u4BandSelectAssocCount) !=
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "Band Select Associatiom Count       : %d\r\n",
                       u4BandSelectAssocCount);
        }
        if (nmhGetFsBandSelectAgeOutSuppTimer (u4WlanProfileId,
                                               &u4BandSelectAgeOutSuppTimer) !=
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Band Select Age Out Support Timer  "
                       " : %d\r\n", u4BandSelectAgeOutSuppTimer);
        }
        if (nmhGetFsBandSelectAssocCountFlushTimer (u4WlanProfileId,
                                                    &u4BandSelectAssocCountFlushTimer)
            != SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "Band Select Assoc Count Flush Timer : "
                       "%d\r\n\r\n", u4BandSelectAssocCountFlushTimer);
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliShowBandSelectProbeDetails
* Description :
* Input       :  CliHandle
*            
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
WsscfgCliShowBandSelectProbeDetails (tCliHandle CliHandle)
{
    tWssStaBandSteerDB  WssStaBandSteerDB;
    UINT1               au1stationMacAddress[20];
    UINT1               au1BssIdMacAddr[20];

    MEMSET (&WssStaBandSteerDB, 0, sizeof (tWssStaBandSteerDB));
    MEMSET (au1stationMacAddress, 0, 20);
    MEMSET (au1BssIdMacAddr, 0, 20);

    if (WssStaUpdateBandSteerProcessDB (WSSSTA_GET_FIRST_BAND_STEER_DB,
                                        &WssStaBandSteerDB) == OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "No Entry found\r\n");
        return CLI_FAILURE;
    }
    PrintMacAddress ((UINT1 *) &WssStaBandSteerDB.stationMacAddress,
                     au1stationMacAddress);
    CliPrintf (CliHandle, "Station MAC : %s\n", au1stationMacAddress);
    PrintMacAddress ((UINT1 *) &WssStaBandSteerDB.BssIdMacAddr,
                     au1BssIdMacAddr);
    CliPrintf (CliHandle, "BSSID       : %s\n", au1BssIdMacAddr);
    while (WssStaUpdateBandSteerProcessDB (WSSSTA_GET_NEXT_BAND_STEER_DB,
                                           &WssStaBandSteerDB) != OSIX_FAILURE)
    {
        PrintMacAddress ((UINT1 *) &WssStaBandSteerDB.stationMacAddress,
                         au1stationMacAddress);
        CliPrintf (CliHandle, "Station MAC : %s\n", au1stationMacAddress);
        PrintMacAddress ((UINT1 *) &WssStaBandSteerDB.BssIdMacAddr,
                         au1BssIdMacAddr);
        CliPrintf (CliHandle, "BSSID       : %s\n", au1BssIdMacAddr);
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgCliShowBandSelectAssocDetails 
* Description :
* Input       :  CliHandle
*            pFsWlanStationTrapStatus
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
WsscfgCliShowBandSelectAssocDetails (tCliHandle CliHandle)
{
    tWssStaBandSteerDB  WssStaBandSteerDB;
    UINT1               au1stationMacAddress[20];
    UINT1               au1BssIdMacAddr[20];

    MEMSET (&WssStaBandSteerDB, 0, sizeof (tWssStaBandSteerDB));
    MEMSET (au1stationMacAddress, 0, 20);
    MEMSET (au1BssIdMacAddr, 0, 20);

    if (WssStaUpdateBandSteerProcessDB (WSSSTA_GET_FIRST_BAND_STEER_DB,
                                        &WssStaBandSteerDB) == OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "No entry found\r\n");
        return CLI_FAILURE;
    }
    PrintMacAddress ((UINT1 *) &WssStaBandSteerDB.stationMacAddress,
                     au1stationMacAddress);
    CliPrintf (CliHandle, "Station MAC : %s\n", au1stationMacAddress);
    PrintMacAddress ((UINT1 *) &WssStaBandSteerDB.BssIdMacAddr,
                     au1BssIdMacAddr);
    CliPrintf (CliHandle, "BSSID       : %s\n", au1BssIdMacAddr);
    CliPrintf (CliHandle, "Assoc Count : %d\n",
               WssStaBandSteerDB.u1StaAssocCount);
    while (WssStaUpdateBandSteerProcessDB
           (WSSSTA_GET_NEXT_BAND_STEER_DB, &WssStaBandSteerDB) != OSIX_FAILURE)
    {
        PrintMacAddress ((UINT1 *) &WssStaBandSteerDB.stationMacAddress,
                         au1stationMacAddress);
        CliPrintf (CliHandle, "Station MAC : %s\n", au1stationMacAddress);
        PrintMacAddress ((UINT1 *) &WssStaBandSteerDB.BssIdMacAddr,
                         au1BssIdMacAddr);
        CliPrintf (CliHandle, "BSSID       : %s\n", au1BssIdMacAddr);
        CliPrintf (CliHandle, "Assoc Count : %d\n",
                   WssStaBandSteerDB.u1StaAssocCount);
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliEnableDebug
* Description :
* Input       :  CliHandle
*            pFsWlanStationTrapStatus
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliEnableDebug (tCliHandle CliHandle, INT4 i4EnableDebug)
{
    UINT4               u4ErrCode = 0;
    INT4                i4OldDebugMask = 0;

    nmhGetFsStationDebugOption (&i4OldDebugMask);

    i4EnableDebug = i4EnableDebug | i4OldDebugMask;

    if (nmhTestv2FsStationDebugOption (&u4ErrCode, i4EnableDebug) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Invalid Inputs\r\n");
        return CLI_FAILURE;
    }
    nmhSetFsStationDebugOption (i4EnableDebug);

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliDisableDebug
* Description :
* Input       :  CliHandle
*            pFsWlanStationTrapStatus
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliDisableDebug (tCliHandle CliHandle, INT4 i4DisableDebug)
{
    UINT4               u4ErrCode = 0;
    INT4                i4OldDebugMask = 0;

    nmhGetFsStationDebugOption (&i4OldDebugMask);
    i4DisableDebug = i4OldDebugMask & (~i4DisableDebug);

    if (nmhTestv2FsStationDebugOption (&u4ErrCode, i4DisableDebug) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n Invalid Inputs");
        return CLI_FAILURE;
    }

    nmhSetFsStationDebugOption (i4DisableDebug);
    return CLI_FAILURE;
}
#endif
/****************************************************************************
* Function    : WsscfgCliClearFsSecurityWebAuthWebFailMessage 
* Description :
* Input       :  CliHandle 
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliClearFsSecurityWebAuthWebFailMessage (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    if ((STRCMP ((gWsscfgGlobals.WsscfgGlbMib.
                  au1FsSecurityWebAuthWebFailMessage), "")) != OSIX_FALSE)
    {
        MEMSET (gWsscfgGlobals.
                WsscfgGlbMib.au1FsSecurityWebAuthWebFailMessage, 0,
                sizeof (gWsscfgGlobals.
                        WsscfgGlbMib.au1FsSecurityWebAuthWebFailMessage));
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgClearRadioCounters
* Description :
* Input       :  CliHandle 
*                pu1ProfileName 
*                pu4RadioId
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgClearRadioCounters (tCliHandle CliHandle, UINT1 *pu1ProfileName,
                          UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    tWssIfPMDB          WssIfPMDB;
    MEMSET (&WssIfPMDB, 0, sizeof (tWssIfPMDB));

    /*If no Profile name is provided, radio stats cleared for all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId, u4currentBindingId,
                 &i4RadioIfIndex) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\nRadio Index not found\n");
                return CLI_FAILURE;
            }

            if (nmhSetFsWlanRadioClearStats (i4RadioIfIndex,
                                             PM_STATS_CLEAR_SET) !=
                SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        /*If AP name is provided and not the radio id, all radio id's of the
         *          * AP is updated to clear the radio stats*/
        if (pu4RadioId == 0)
        {
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId, u4currentBindingId,
                         &i4RadioIfIndex) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "\r\nRadio Index not found\n");
                        return CLI_FAILURE;
                    }
                    if (nmhSetFsWlanRadioClearStats (i4RadioIfIndex,
                                                     PM_STATS_CLEAR_SET) !=
                        SNMP_SUCCESS)
                    {
                        return CLI_FAILURE;
                    }
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4currentProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

        }
        /*Only for the particular radio id of the AP, clear the radio stats. */
        else
        {
            u4RadioId = CLI_PTR_TO_U4 (*pu4RadioId);

            if (CapwapGetWtpProfileIdFromProfileName
                (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\nRadio Index not found\n");
                return CLI_FAILURE;
            }
            if (nmhSetFsWlanRadioClearStats (i4RadioIfIndex,
                                             PM_STATS_CLEAR_SET) !=
                SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgClearClientCounters
* Description :
* Input       :  CliHandle 
*                staMacAddr 
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgClearClientCounters (tCliHandle CliHandle, tMacAddr staMacAddr)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsWlanClientStatsClear (&u4ErrorCode, staMacAddr,
                                         PM_STATS_CLEAR_SET) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsWlanClientStatsClear (staMacAddr, PM_STATS_CLEAR_SET) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthWebButtonText
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthWebButtonText (tCliHandle CliHandle,
                                            UINT1
                                            *pFsSecurityWebAuthWebButtonText)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE WebAuthButonTxt;
    MEMSET (&WebAuthButonTxt, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    WebAuthButonTxt.i4_Length = STRLEN (pFsSecurityWebAuthWebButtonText);
    WebAuthButonTxt.pu1_OctetList = pFsSecurityWebAuthWebButtonText;

    if (WsscfgTestFsSecurityWebAuthWebButtonText
        (&u4ErrorCode, &WebAuthButonTxt) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsSecurityWebAuthWebButtonText (&WebAuthButonTxt) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgCliClearFsSecurityWebAuthWebButtonText 
* Description :
* Input       :  CliHandle 
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliClearFsSecurityWebAuthWebButtonText (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    if ((STRCMP ((gWsscfgGlobals.WsscfgGlbMib.
                  au1FsSecurityWebAuthWebButtonText), "")) != OSIX_FALSE)
    {
        MEMSET (gWsscfgGlobals.
                WsscfgGlbMib.au1FsSecurityWebAuthWebButtonText, 0,
                sizeof (gWsscfgGlobals.
                        WsscfgGlbMib.au1FsSecurityWebAuthWebButtonText));
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthWebLoadBalInfo
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthWebLoadBalInfo (tCliHandle CliHandle,
                                             UINT1
                                             *pFsSecurityWebAuthWebLoadBalInfo)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE WebAuthLdBalMsg;
    MEMSET (&WebAuthLdBalMsg, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    WebAuthLdBalMsg.i4_Length = STRLEN (pFsSecurityWebAuthWebLoadBalInfo);
    WebAuthLdBalMsg.pu1_OctetList = pFsSecurityWebAuthWebLoadBalInfo;

    if (WsscfgTestFsSecurityWebAuthWebLoadBalInfo
        (&u4ErrorCode, &WebAuthLdBalMsg) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsSecurityWebAuthWebLoadBalInfo (&WebAuthLdBalMsg) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliClearFsSecurityWebAuthWebLoadBalInfo
* Description :
* Input       :  CliHandle 
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
WsscfgCliClearFsSecurityWebAuthWebLoadBalInfo (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    if ((STRCMP ((gWsscfgGlobals.WsscfgGlbMib.
                  au1FsSecurityWebAuthWebLoadBalInfo), "")) != OSIX_FALSE)
    {
        MEMSET (gWsscfgGlobals.
                WsscfgGlbMib.au1FsSecurityWebAuthWebLoadBalInfo, 0,
                sizeof (gWsscfgGlobals.
                        WsscfgGlbMib.au1FsSecurityWebAuthWebLoadBalInfo));
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthDisplayLang
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthDisplayLang (tCliHandle CliHandle,
                                          UINT4 *pFsSecurityWebAuthDisplayLang)
{
    UINT4               u4ErrorCode;
    INT4                i4FsSecurityWebAuthDisplayLang = 0;

    WSSCFG_FILL_FSSECURITYWEBAUTHDISPLAYLANG
        (i4FsSecurityWebAuthDisplayLang, pFsSecurityWebAuthDisplayLang);

    if (WsscfgTestFsSecurityWebAuthDisplayLang
        (&u4ErrorCode, i4FsSecurityWebAuthDisplayLang) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetFsSecurityWebAuthDisplayLang
        (i4FsSecurityWebAuthDisplayLang) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthColor
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthColor (tCliHandle CliHandle,
                                    UINT4 *pFsSecurityWebAuthColor)
{
    UINT4               u4ErrorCode;
    INT4                i4FsSecurityWebAuthColor = 0;

    WSSCFG_FILL_FSSECURITYWEBAUTHCOLOR (i4FsSecurityWebAuthColor,
                                        pFsSecurityWebAuthColor);

    if (WsscfgTestFsSecurityWebAuthColor
        (&u4ErrorCode, i4FsSecurityWebAuthColor) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetFsSecurityWebAuthColor (i4FsSecurityWebAuthColor) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsRrmConfigTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsRrmConfigEntry
*            pWsscfgIsSetFsRrmConfigEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsRrmConfigTable (tCliHandle CliHandle,
                              tWsscfgFsRrmConfigEntry *
                              pWsscfgSetFsRrmConfigEntry,
                              tWsscfgIsSetFsRrmConfigEntry *
                              pWsscfgIsSetFsRrmConfigEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsRrmConfigTable
        (&u4ErrorCode, pWsscfgSetFsRrmConfigEntry,
         pWsscfgIsSetFsRrmConfigEntry, OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n RRM Config validation failed \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsRrmConfigTable
        (pWsscfgSetFsRrmConfigEntry, pWsscfgIsSetFsRrmConfigEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgWlanCapabilityCreate
 * Description :  This function will change the prompt name from config 
                terminal to capability profile name

 * Input       :  CliHandle - CLI Handler
                  pu1Prompt - Prompt Name

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgWlanCapabilityCreate (tCliHandle CliHandle, UINT1 *pu1Prompt)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE CapabilityName;
    INT4                i4FsDot11CapabilityRowStatus;

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    MEMSET (&CapabilityName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    CapabilityName.i4_Length = STRLEN (pu1Prompt);

    CapabilityName.pu1_OctetList = pu1Prompt;

    /* Check if the profile is already present */
    if ((nmhGetFsDot11CapabilityRowStatus (&CapabilityName,
                                           &i4FsDot11CapabilityRowStatus))
        != SNMP_SUCCESS)
    {
        /* Profile NOT present - CREATE */

        if (nmhTestv2FsDot11CapabilityRowStatus
            (&u4ErrCode, &CapabilityName, CREATE_AND_GO) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n Entry Creation failed \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return OSIX_FAILURE;
        }

        /* CREATE Profile */
        if (nmhSetFsDot11CapabilityRowStatus
            (&CapabilityName, CREATE_AND_GO) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n Row Status Creation failed \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return OSIX_FAILURE;
        }
    }

    /* ENTER Capability profile Mode */
    SNPRINTF ((CHR1 *) au1Cmd, MAX_PROMPT_LEN, "%s%s",
              WSSCFG_CLI_CAPABILITY_MODE, pu1Prompt);
    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "/r%% Unable to enter into capability profile mode\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : WssCfgGetCapabilityCfgPrompt                       */
/*                                                                           */
/*     DESCRIPTION      : This function Checks for capability profile and    */
/*                        return the prompt to be displayed.                 */
/*                                                                           */
/*     INPUT            : pi1ModeName - Mode to be configured.               */
/*                                                                           */
/*     OUTPUT           : pi1DispStr  - Prompt to be displayed.              */
/*                                                                           */
/*     RETURNS          : TRUE or FALSE                                      */
/*                                                                           */
/*****************************************************************************/
INT1
WssCfgGetCapabilityCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (WSSCFG_CLI_CAPABILITY_MODE);
    INT4                i4FsDot11CapabilityRowStatus;
    tSNMP_OCTET_STRING_TYPE CapabilityName;

    MEMSET (&CapabilityName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, WSSCFG_CLI_CAPABILITY_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    CapabilityName.i4_Length = STRLEN (pi1ModeName);
    CapabilityName.pu1_OctetList = (UINT1 *) pi1ModeName;
    /* 
     * No need to take lock here, since it is taken by
     * Cli in cli_process_vlan_cmd.
     */
    if ((nmhGetFsDot11CapabilityRowStatus (&CapabilityName,
                                           &i4FsDot11CapabilityRowStatus))
        != SNMP_SUCCESS)
    {
        return FALSE;
    }

    CLI_SET_CAPABID (pi1ModeName);
    CLI_SET_CXT_ID (0);
    STRCPY (pi1DispStr, "(config-capab)#");

    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : WssCfgGetCapabilityCfgPrompt                       */
/*                                                                           */
/*     DESCRIPTION      : This function Checks for capability profile and    */
/*                        return the prompt to be displayed.                 */
/*                                                                           */
/*     INPUT            : pi1ModeName - Mode to be configured.               */
/*                                                                           */
/*     OUTPUT           : pi1DispStr  - Prompt to be displayed.              */
/*                                                                           */
/*     RETURNS          : TRUE or FALSE                                      */
/*                                                                           */
/*****************************************************************************/
INT1
WssCfgGetSqlCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (SQL_CLI_CONFIG_MODE);
    tSNMP_OCTET_STRING_TYPE RadiusName;

    MEMSET (&RadiusName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }
    if (STRNCMP (pi1ModeName, SQL_CLI_CONFIG_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    RadiusName.i4_Length = STRLEN (pi1ModeName);
    RadiusName.pu1_OctetList = (UINT1 *) pi1ModeName;
    /* 
     * No need to take lock here, since it is taken by
     * Cli in cli_process_vlan_cmd.
     */

    CLI_SET_RADIUSID (pi1ModeName);
    CLI_SET_CXT_ID (0);
    STRCPY (pi1DispStr, "(radius-config)#");

    UNUSED_PARAM (pi1ModeName);
    UNUSED_PARAM (pi1DispStr);
    return TRUE;

}

/****************************************************************************
 * Function    :  WsscfgWlanCapabilityDelete
 * Description :  This function will delete the entry for the given profile
                  name

 * Input       :  CliHandle - CLI Handler
                  pu1Prompt - Prompt Name

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgWlanCapabilityDelete (tCliHandle CliHandle, UINT1 *pu1Prompt)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE CapabilityName;

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    MEMSET (&CapabilityName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    CapabilityName.i4_Length = STRLEN (pu1Prompt);

    CapabilityName.pu1_OctetList = pu1Prompt;

    if (nmhTestv2FsDot11CapabilityRowStatus
        (&u4ErrCode, &CapabilityName, DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return OSIX_FAILURE;
    }

    /* DELETE Profile */
    if (nmhSetFsDot11CapabilityRowStatus (&CapabilityName, DESTROY)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n Row Status Deletion failed \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgWlanQosCreate
 * Description :  This function will change the prompt name from config 
                terminal to Qos profile name

 * Input       :  CliHandle - CLI Handler
                  pu1Prompt - Prompt Name

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgWlanQosCreate (tCliHandle CliHandle, UINT1 *pu1Prompt)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT4               u4ErrCode;
    INT4                i4FsDot11QosRowStatus;
    tSNMP_OCTET_STRING_TYPE QosProfileName;

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    MEMSET (&QosProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    QosProfileName.i4_Length = STRLEN (pu1Prompt);
    QosProfileName.pu1_OctetList = pu1Prompt;

    /* Check if the profile is already present */
    if ((nmhGetFsDot11QosRowStatus (&QosProfileName,
                                    &i4FsDot11QosRowStatus)) != SNMP_SUCCESS)
    {
        /* Profile NOT present - CREATE */

        if (nmhTestv2FsDot11QosRowStatus (&u4ErrCode, &QosProfileName,
                                          CREATE_AND_GO) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n Entry Creation failed \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return OSIX_FAILURE;
        }

        /* CREATE Profile */
        if (nmhSetFsDot11QosRowStatus (&QosProfileName, CREATE_AND_GO)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n Row Status Creation failed \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return OSIX_FAILURE;
        }
    }

    /* ENTER Qos profile Mode */
    SNPRINTF ((CHR1 *) au1Cmd, MAX_PROMPT_LEN, "%s%s",
              WSSCFG_CLI_QOS_MODE, pu1Prompt);
    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "/r%% Unable to enter into Qos profile mode\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : WssCfgGetQosCfgPrompt                              */
/*                                                                           */
/*     DESCRIPTION      : This function Checks for qos profile and           */
/*                        return the prompt to be displayed.                 */
/*                                                                           */
/*     INPUT            : pi1ModeName - Mode to be configured.               */
/*                                                                           */
/*     OUTPUT           : pi1DispStr  - Prompt to be displayed.              */
/*                                                                           */
/*     RETURNS          : TRUE or FALSE                                      */
/*                                                                           */
/*****************************************************************************/
INT1
WssCfgGetQosCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (WSSCFG_CLI_QOS_MODE);
    INT4                i4FsDot11QosRowStatus;
    tSNMP_OCTET_STRING_TYPE QosProfileName;

    MEMSET (&QosProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, WSSCFG_CLI_QOS_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    QosProfileName.i4_Length = STRLEN (pi1ModeName);
    QosProfileName.pu1_OctetList = (UINT1 *) pi1ModeName;

    if ((nmhGetFsDot11QosRowStatus (&QosProfileName,
                                    &i4FsDot11QosRowStatus)) != SNMP_SUCCESS)
    {
        return FALSE;
    }

    CLI_SET_QOSID (pi1ModeName);
    CLI_SET_CXT_ID (0);
    STRCPY (pi1DispStr, "(config-qos)#");

    return TRUE;
}

/****************************************************************************
 * Function    :  WsscfgWlanQosDelete
 * Description :  This function will delete the entry for the given profile
                  name

 * Input       :  CliHandle - CLI Handler
                  pu1Prompt - Prompt Name

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgWlanQosDelete (tCliHandle CliHandle, UINT1 *pu1Prompt)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE QosProfileName;

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    MEMSET (&QosProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    QosProfileName.i4_Length = STRLEN (pu1Prompt);

    QosProfileName.pu1_OctetList = pu1Prompt;

    if (nmhTestv2FsDot11QosRowStatus (&u4ErrCode, &QosProfileName,
                                      DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return OSIX_FAILURE;
    }

    /* DELETE Profile */
    if (nmhSetFsDot11QosRowStatus (&QosProfileName, DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n Row Status Deletion failed \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgWlanAuthCreate
 * Description :  This function will change the prompt name from config 
                terminal to Qos profile name

 * Input       :  CliHandle - CLI Handler
                  pu1Prompt - Prompt Name

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgWlanAuthCreate (tCliHandle CliHandle, UINT1 *pu1Prompt)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT4               u4ErrCode;
    INT4                i4FsDot11AuthRowStatus;
    tSNMP_OCTET_STRING_TYPE AuthProfileName;

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    MEMSET (&AuthProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    AuthProfileName.i4_Length = STRLEN (pu1Prompt);
    AuthProfileName.pu1_OctetList = pu1Prompt;

    /* Check if the profile is already present */
    if ((nmhGetFsDot11AuthenticationRowStatus (&AuthProfileName,
                                               &i4FsDot11AuthRowStatus))
        != SNMP_SUCCESS)
    {
        /* Profile NOT present - CREATE */

        if (nmhTestv2FsDot11AuthenticationRowStatus
            (&u4ErrCode, &AuthProfileName, CREATE_AND_GO) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n Entry Creation failed \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return OSIX_FAILURE;
        }

        /* CREATE Profile */
        if (nmhSetFsDot11AuthenticationRowStatus
            (&AuthProfileName, CREATE_AND_GO) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n Row Status Creation failed \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return OSIX_FAILURE;
        }
    }

    /* ENTER Qos profile Mode */
    SNPRINTF ((CHR1 *) au1Cmd, MAX_PROMPT_LEN, "%s%s",
              WSSCFG_CLI_AUTH_MODE, pu1Prompt);
    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "/r%% Unable to enter into Auth profile mode\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : WssCfgGetAuthCfgPrompt                             */
/*                                                                           */
/*     DESCRIPTION      : This function Checks for Auth Profile and          */
/*                        return the prompt to be displayed.                 */
/*                                                                           */
/*     INPUT            : pi1ModeName - Mode to be configured.               */
/*                                                                           */
/*     OUTPUT           : pi1DispStr  - Prompt to be displayed.              */
/*                                                                           */
/*     RETURNS          : TRUE or FALSE                                      */
/*                                                                           */
/*****************************************************************************/
INT1
WssCfgGetAuthCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (WSSCFG_CLI_AUTH_MODE);
    INT4                i4FsDot11AuthRowStatus;
    tSNMP_OCTET_STRING_TYPE AuthProfileName;

    MEMSET (&AuthProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, WSSCFG_CLI_AUTH_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    AuthProfileName.i4_Length = STRLEN (pi1ModeName);
    AuthProfileName.pu1_OctetList = (UINT1 *) pi1ModeName;

    if ((nmhGetFsDot11AuthenticationRowStatus (&AuthProfileName,
                                               &i4FsDot11AuthRowStatus))
        != SNMP_SUCCESS)
    {
        return FALSE;
    }

    CLI_SET_QOSID (pi1ModeName);
    CLI_SET_CXT_ID (0);

    STRCPY (pi1DispStr, "(config-auth)#");
    return TRUE;
}

/****************************************************************************
 * Function    :  WsscfgWlanAuthDelete
 * Description :  This function will delete the entry for the given profile
                  name

 * Input       :  CliHandle - CLI Handler
                  pu1Prompt - Prompt Name

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgWlanAuthDelete (tCliHandle CliHandle, UINT1 *pu1Prompt)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE AuthProfileName;

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    MEMSET (&AuthProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    AuthProfileName.i4_Length = STRLEN (pu1Prompt);

    AuthProfileName.pu1_OctetList = pu1Prompt;

    if (nmhTestv2FsDot11AuthenticationRowStatus
        (&u4ErrCode, &AuthProfileName, DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return OSIX_FAILURE;
    }

    /* DELETE Profile */
    if (nmhSetFsDot11AuthenticationRowStatus
        (&AuthProfileName, DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n Row Status Deletion failed \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/* SHOW COMMANDS*/
/****************************************************************************
 * Function    :  cli_process_Wsscfg_show_cmd
 * Description :  This function is exported to CLI module to handle the
                WSSCFG cli commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
cli_process_wsscfg_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[WSSCFG_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RetVal = CLI_FAILURE;
    UINT4               u4CmdType = 0;
    UINT1               u1DisplayAll = 0;
    INT4                i4Inst;
    tMacAddr            BaseMacAddr;
    MEMSET (BaseMacAddr, 0, sizeof (tMacAddr));

    UNUSED_PARAM (u4CmdType);

    /* check if the capwap module is started. i.e "no shutdown". else all the
     * capwap related commands are considered invalid */
    if ((u4Command == CLI_WSSCFG_SHOW_AP_CONFIG) ||
        (u4Command == CLI_WSSCFG_SHOW_NETWORK_CONFIG) ||
        (u4Command == CLI_WSSCFG_SHOW_NETWORK_SUMMARY) ||
        (u4Command == CLI_WSSCFG_SHOW_AP_WLAN_CONFIG) ||
        (u4Command == CLI_WSSCFG_SHOW_AP_WLAN_STATS))
    {
        if (CapwapCliCheckModuleStatusShow (CliHandle) != CLI_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }

    CliRegisterLock (CliHandle, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        /* Coverity fix - It is analayzed that all the
         * Capwap CLI commands function use the first argument as NULL.
         * So the extraction of the first index is not required in this case  */
    }

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == WSSCFG_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);
    switch (u4Command)
    {
        case CLI_WSSCFG_SHOW_AP_CONFIG:
            i4RetVal =
                WssCfgShowApConfiguration (CliHandle, *args[0],
                                           (UINT1 *) args[1], *args[2]);

            break;

        case CLI_WSSCFG_SHOW_NETWORK_CONFIG:
            if (args[1] == NULL)
            {
                i4RetVal =
                    WssCfgShowNetworkConfiguration (CliHandle, *args[0],
                                                    NULL, 0);
            }
            else if (args[2] == NULL)
            {

                i4RetVal =
                    WssCfgShowNetworkConfiguration (CliHandle, *args[0],
                                                    (UINT1 *) args[1], 0);
            }
            else
            {
                i4RetVal =
                    WssCfgShowNetworkConfiguration (CliHandle, *args[0],
                                                    (UINT1 *) args[1],
                                                    *args[2]);
            }
            break;

        case CLI_WSSCFG_SHOW_NETWORK_SUMMARY:
            i4RetVal = WssCfgShowNetworkSummary (CliHandle, args[0]);
            break;
        case CLI_WSSCFG_SHOW_AP_WLAN_CONFIG:
            i4RetVal =
                WssCfgShowApWlanConfig (CliHandle, args[0],
                                        (UINT1 *) args[1], args[2]);
            break;
        case CLI_WSSCFG_SHOW_WLAN_SUMMARY:
            i4RetVal = WssCfgShowWlanSummary (CliHandle);
            break;
        case CLI_WSSCFG_SHOW_WLAN_MULTICAST_SUMMARY:
            i4RetVal = WssCfgShowWlanMulticastSummary (CliHandle);
            break;
        case CLI_WSSCFG_SHOW_CLIENT_SUMMARY:
            i4RetVal = WssCfgShowClientSummary (CliHandle);
            break;
        case CLI_WSSCFG_SHOW_CLIENT_STATS:
            if (NULL != args[0])
            {
                StrToMac ((UINT1 *) args[0], BaseMacAddr);
            }
            else
            {
                u1DisplayAll = 1;
            }
            i4RetVal =
                WssCfgShowClientStats (CliHandle, BaseMacAddr, u1DisplayAll);
            break;
        case CLI_WSSCFG_SHOW_WEBAUTH_CONFIG:
            i4RetVal = WssCfgShowWebAuthConfig (CliHandle);
            break;
        case CLI_WSSCFG_SHOW_NETUSER_SUMMARY:
            i4RetVal = WssCfgShowNetUserSummary (CliHandle);
            break;
        case CLI_WSSCFG_SHOW_WLAN_CONFIG:
            i4RetVal = WssCfgShowWlanConfig (CliHandle, *args[0]);
            break;
        case CLI_WSSCFG_SHOW_APGROUPID:
            i4RetVal = WssCfgShowApGroupConfig (CliHandle, *args[0]);
            break;
        case CLI_WSSCFG_SHOW_APGROUP:
            i4RetVal = WssCfgShowApGroupSummary (CliHandle);
            break;
        case CLI_WSSCFG_SHOW_WLAN_MULTICAST_CONFIG:
            i4RetVal =
                WssCfgShowWlanMulticastConfig (CliHandle,
                                               CLI_PTR_TO_U4 (*args[0]));
            break;
        case CLI_WSSCFG_SHOW_COUNTRY_SUPPORTED:
            i4RetVal = WssCfgShowCountrySupported (CliHandle);
            break;
        case CLI_WSSCFG_SHOW_CAPABILITY_PROFILE:
            i4RetVal =
                WssCfgShowCapabilityProfile (CliHandle, (UINT1 *) args[0]);
            break;
        case CLI_WSSCFG_SHOW_QOS_PROFILE:
            i4RetVal = WssCfgShowQosProfile (CliHandle, (UINT1 *) args[0]);
            break;
        case CLI_WSSCFG_SHOW_AUTHENTICATION_PROFILE:
            i4RetVal =
                WssCfgShowAuthenticationProfile (CliHandle, (UINT1 *) args[0]);
            break;
        case CLI_WSSCFG_SHOW_WLAN_STATS:
            i4RetVal = WssCfgShowWlanStatistics (CliHandle, (UINT4 *) args[0]);
            break;
        case CLI_WSSCFG_SHOW_AP_WLAN_STATS:
            i4RetVal =
                WssCfgShowApWlanStatistics (CliHandle, (UINT1 *) args[0],
                                            args[1], args[2]);
            break;
        case CLI_WSSCFG_SHOW_DIFFSERV_STATS:
            i4RetVal = WssCfgShowDiffServStatistics (CliHandle);
            break;
        case CLI_WSSCFG_SHOW_LEGACY_RATE:
            i4RetVal = WsscfgCliShowLegacyRate (CliHandle);
            break;
#ifdef BAND_SELECT_WANTED
        case CLI_WSSCFG_SHOW_BAND_SELECT:
            i4RetVal = WsscfgCliShowBandSelect (CliHandle, *args[0]);
            break;

        case CLI_WSSCFG_SHOW_BAND_SELECT_PROBE:
            i4RetVal = WsscfgCliShowBandSelectProbeDetails (CliHandle);
            break;

        case CLI_WSSCFG_SHOW_BAND_SELECT_ASSOC:
            i4RetVal = WsscfgCliShowBandSelectAssocDetails (CliHandle);
            break;

        case CLI_WSSCFG_DEBUG_BAND_SELECT:
            i4RetVal = WsscfgCliEnableDebug (CliHandle, (*(INT4 *) args[0]));
            break;
        case CLI_WSSCFG_NO_DEBUG_BAND_SELECT:
            i4RetVal = WsscfgCliDisableDebug (CliHandle, (*(INT4 *) args[0]));
            break;
#endif
        default:
            break;

    }
    if ((i4RetVal == CLI_FAILURE) && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_WSSCFG_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", WsscfgCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetVal);

    CliUnRegisterLock (CliHandle);

    WSSCFG_UNLOCK;

    return i4RetVal;

}

/****************************************************************************
* Function    : WssCfgShowNetworkConfiguration 
* Description :
* Input       :  CliHandle 
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowNetworkConfiguration (tCliHandle CliHandle,
                                UINT4 u4FsRadioType,
                                UINT1 *pu1CapwapBaseWtpProfileName,
                                UINT4 u4RadioId)
{
    UINT4               u4WtpProfileId = 0;
    UINT1               au1WtpModelNumber[32];
    UINT1               u1RadioIndex = 0;
    INT4                i4RadioIfIndex = 0;
    UINT1               u1MCSIndex = 0;
    INT4                i4LdpcOption = 0;
    INT4                i4PowerSaveOption = 0;
    INT4                i4GreenfieldOption = 0;
    INT4                i4TxStbcOption = 0;
    INT4                i4RxStbcOption = 0;
    INT4                i4delayBlockAck = 0;
    INT4                i4DssCckModeOption = 0;
    INT4                i4FortyMhzIntolerantOption = 0;
    INT4                i4LSigTxopProtectionOption = 0;
    UINT4               u4MaxMpduOption = 0;
    UINT4               u4ampduFactor = 0;
    INT4                i4pcoOption = 0;
    UINT4               u4pcoTransTime = 0;
    INT4                i4mcsFeedbackOption = 0;
    INT4                i4htcFieldOption = 0;
    INT4                i4RDRespondOption = 0;
    INT4                i4ImpTranBeamRecOption = 0;
    INT4                i4RecStagSoundOption = 0;
    INT4                i4TranStagSoundOption = 0;
    INT4                i4RecvNDPOption = 0;
    INT4                i4TranNDPOption = 0;
    INT4                i4ImpTranBeamOption = 0;
    INT4                i4CalibrationOption = 0;
    INT4                i4ExplCsiTranBeamOption = 0;
    INT4                i4ExplNoncompSteOption = 0;
    INT4                i4ExplcompSteOption = 0;
    INT4                i4ExplTranBeamCsiOption = 0;
    INT4                i4ExplNCompBeamOption = 0;
    INT4                i4ExplCompBeamOption = 0;
    UINT4               u4numCsiAntSupOption = 0;
    UINT4               u4numNCompAntSupOption = 0;
    UINT4               u4ChannelEstOption = 0;
    UINT4               u4PrimaryChannelOption = 0;
    UINT4               u4SecondaryChannelOption = 0;
    INT4                i4RifsModeOption = 0;
    INT4                i4htProtectionOption = 0;
    INT4                i4htNonObssStaOption = 0;
    INT4                i4DualCtsProtectionOption = 0;
    INT4                i4StbcBeaconOption = 0;
    INT4                i4PcoPhaseOption = 0;
    INT4                i4MaxAmsduLenghth = 0;
    INT4                i4PcoConfigOption = 0;
    UINT4               u4MinGroupingOption = 0;
    UINT4               u4numCompAntSupOption = 0;
    UINT4               u4MaxnumCsiAntSupOption = 0;
    INT4                i4htOpChannelWidthOption = 0;
    UINT4               u4mcsFeedbackOption = 0;
    INT4                i4TxRxMcsSetOption = 0;
    INT4                i4mcsSetOption = 0;

    /* tCapwapCapwapBaseWtpProfileEntry CapwapCapwapBaseWtpProfileEntry; */
    tWsscfgDot11PhyOFDMEntry WsscfgGetDot11PhyOFDMEntry;
    tWsscfgFsRrmConfigEntry WsscfgGetFsRrmConfigEntry;
    tWsscfgDot11PhyTxPowerEntry WsscfgGetDot11PhyTxPowerEntry;
    tWsscfgDot11OperationEntry WsscfgGetDot11OperationEntry;
    tWsscfgFsDot11WlanCapabilityProfileEntry
        WsscfgGetFsDot11WlanCapabilityProfileEntry;
    tWsscfgDot11StationConfigEntry WsscfgGetDot11StationConfigEntry;
    tWsscfgFsDot11nConfigEntry WsscfgGetFsDot11nConfigEntry;
    tWsscfgFsDot11nMCSDataRateEntry WsscfgGetFsDot11nMCSDataRateEntry;
    tRadioIfGetDB       RadioIfGetDB;
    tWsscfgDot11PhyDSSSEntry WsscfgGetDot11PhyDSSSEntry;
    UINT1               au1ModelNumber[256];
    tSNMP_OCTET_STRING_TYPE ModelName;
    INT4                i4FsDot11anSupport = 0;
    INT4                i4FsDot11aNetworkEnable = 0;
    INT4                i4FsDot11bnSupport = 0;
    INT4                i4FsDot11gSupport = 0;
    INT4                i4FsDot11bNetworkEnable = 0;
    INT4                i4AdminStatus = 0;
    UINT4               u4RadioIfType = 0;
    UINT4               u4NoOfRadio = 0;
    UINT4               u4GetRadioType = 0;

    MEMSET (au1WtpModelNumber, 0, 32);
    MEMSET (&ModelName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WsscfgGetDot11PhyOFDMEntry, 0, sizeof (tWsscfgDot11PhyOFDMEntry));
    MEMSET (&WsscfgGetFsRrmConfigEntry, 0, sizeof (tWsscfgFsRrmConfigEntry));
    MEMSET (&WsscfgGetDot11PhyTxPowerEntry, 0,
            sizeof (tWsscfgDot11PhyTxPowerEntry));
    MEMSET (&WsscfgGetDot11OperationEntry, 0,
            sizeof (tWsscfgDot11OperationEntry));
    MEMSET (&WsscfgGetFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));
    MEMSET (&WsscfgGetDot11StationConfigEntry, 0,
            sizeof (tWsscfgDot11StationConfigEntry));
    MEMSET (&WsscfgGetFsDot11nConfigEntry, 0,
            sizeof (tWsscfgFsDot11nConfigEntry));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    UNUSED_PARAM (u4RadioId);
    ModelName.pu1_OctetList = au1ModelNumber;

    if (pu1CapwapBaseWtpProfileName == NULL)
    {
        if (u4FsRadioType == CLI_RADIO_TYPEA)
        {
            if (nmhGetFsDot11anSupport (&i4FsDot11anSupport) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }

            if (nmhGetFsDot11aNetworkEnable (&i4FsDot11aNetworkEnable)
                == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            CliPrintf (CliHandle, "\r\nDot11anSupport      : %d",
                       i4FsDot11anSupport);
            CliPrintf (CliHandle, "\r\nDot11aNetworkEnable : %d",
                       i4FsDot11aNetworkEnable);
            return CLI_SUCCESS;
        }

        else if (u4FsRadioType == CLI_RADIO_TYPEB)
        {
            if (nmhGetFsDot11bnSupport (&i4FsDot11bnSupport) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhGetFsDot11gSupport (&i4FsDot11gSupport) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhGetFsDot11bNetworkEnable (&i4FsDot11bNetworkEnable)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            CliPrintf (CliHandle, "\r\nDot11bSupport       : %d\r\n",
                       i4FsDot11bnSupport);
            CliPrintf (CliHandle, "\r\nDot11bNetworkEnable : %d\r\n",
                       i4FsDot11bNetworkEnable);
            CliPrintf (CliHandle, "\r\nDot11gSupport       : %d\r\n",
                       i4FsDot11gSupport);
            return CLI_SUCCESS;
        }

    }
    else if (pu1CapwapBaseWtpProfileName != NULL)
    {
        /* When Profile name is received as input get the profile id
         * from mapping table and pass the profile name as NULL */
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1CapwapBaseWtpProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nInvalid WTP Profile\r\n");
            return CLI_FAILURE;
        }
        if (nmhGetCapwapBaseWtpProfileWtpModelNumber
            (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nInvalid Model number\r\n");
            return CLI_FAILURE;
        }

        if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) != OSIX_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nFailed to fetch number of radios\r\n");

            return CLI_FAILURE;

        }

        if (u4RadioId == 0)
        {

            for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio; u1RadioIndex++)
            {

                if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                    (u4WtpProfileId, u1RadioIndex,
                     &i4RadioIfIndex) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nFor Radio %d : Virtual Radio interface Index not successfully obtained,Operation Failed for the particular Radio index \r\n",
                               u1RadioIndex);
                    return CLI_FAILURE;

                }

                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_SUCCESS;
                }
                if (WSSCFG_RADIO_TYPE_COMP (u4FsRadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_SUCCESS;
                }

                WsscfgGetDot11StationConfigEntry.MibObject.i4IfIndex =
                    i4RadioIfIndex;
                WsscfgGetDot11PhyTxPowerEntry.MibObject.i4IfIndex =
                    i4RadioIfIndex;
                WsscfgGetDot11OperationEntry.MibObject.i4IfIndex =
                    i4RadioIfIndex;
                WsscfgGetFsDot11nConfigEntry.MibObject.i4IfIndex =
                    i4RadioIfIndex;

                if ((WsscfgGetAllDot11StationConfigTable
                     (&WsscfgGetDot11StationConfigEntry)) != OSIX_SUCCESS)
                {
                    return CLI_FAILURE;
                }

                if ((WsscfgGetAllDot11OperationTable
                     (&WsscfgGetDot11OperationEntry)) != OSIX_SUCCESS)
                {
                    return CLI_FAILURE;
                }

                if ((WsscfgGetAllDot11PhyTxPowerTable
                     (&WsscfgGetDot11PhyTxPowerEntry)) != OSIX_SUCCESS)
                {
                    return CLI_FAILURE;
                }

                if (nmhGetFsDot11RadioType (i4RadioIfIndex, &u4RadioIfType)
                    != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nRadio Type not successfully obtained , (Invalid Input, Check Profile name) \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPEN_AC_COMP (u4RadioIfType))
                {
                    if ((WsscfgGetAllFsDot11nConfigTable
                         (&WsscfgGetFsDot11nConfigEntry)) != OSIX_SUCCESS)
                    {
                        return CLI_FAILURE;
                    }

                    CliPrintf (CliHandle,
                               "\r\n11nSupport                                                         : Enable");
                    if (nmhGetDot11LDPCCodingOptionActivated
                        (i4RadioIfIndex, &i4LdpcOption) == SNMP_SUCCESS)
                    {
                        if (i4LdpcOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nLDPC Option Configured                                           : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nLDPC Option Configured                                           : Enable");
                        }
                    }
                    CliPrintf (CliHandle,
                               "\r\nChannel Width                                                       : %d",
                               WsscfgGetFsDot11nConfigEntry.MibObject.
                               i4FsDot11nConfigChannelWidth);

                    if (WsscfgGetFsDot11nConfigEntry.
                        MibObject.i4FsDot11nConfigShortGIfor20MHz != 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nShort Gi20                                                           : Enable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nShort Gi20                                                           : Disable");
                    }

                    if (WsscfgGetFsDot11nConfigEntry.
                        MibObject.i4FsDot11nConfigShortGIfor40MHz != 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nShort Gi40                                                           : Enable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nShort Gi40                                                           : Disable");
                    }

                    if (nmhGetFsDot11nConfigMIMOPowerSave
                        (i4RadioIfIndex, &i4PowerSaveOption) == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nPower Save Mode Configured                                           : %d",
                                   i4PowerSaveOption);
                    }
                    if (nmhGetDot11HTGreenfieldOptionActivated
                        (i4RadioIfIndex, &i4GreenfieldOption) == SNMP_SUCCESS)
                    {
                        if (i4GreenfieldOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nGreen Field Option Configured                                    : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nGreen Field OPtion Configured                                    : Enable");
                        }
                    }
                    if (nmhGetDot11TxSTBCOptionActivated
                        (i4RadioIfIndex, &i4TxStbcOption) == SNMP_SUCCESS)
                    {
                        if (i4TxStbcOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nTx STBC Option Configured                                        : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nTx STBC Option COnfigured                                        : Enable");
                        }
                    }
                    if (nmhGetDot11RxSTBCOptionActivated
                        (i4RadioIfIndex, &i4RxStbcOption) == SNMP_SUCCESS)
                    {
                        if (i4RxStbcOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nRx STBC Option Configured                                        : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nRx STBC Option Configured                                        : Enable");
                        }
                    }
                    if (nmhGetFsDot11nConfigDelayedBlockAckOptionActivated
                        (i4RadioIfIndex, &i4delayBlockAck) == SNMP_SUCCESS)
                    {
                        if (i4delayBlockAck == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nHT Delayed Block Ack Option Configured                           : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nHT Delayed Block Ack Option Configured                           : Enable");
                        }
                    }
                    if (nmhGetFsDot11nConfigMaxAMSDULengthConfig
                        (i4RadioIfIndex, &i4MaxAmsduLenghth) == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nMaximum AMSDU Length Configured                                      : %d",
                                   i4MaxAmsduLenghth);
                    }
                    if (nmhGetFsDot11nConfigtHTDSSCCKModein40MHzConfig
                        (i4RadioIfIndex, &i4DssCckModeOption) == SNMP_SUCCESS)
                    {
                        if (i4DssCckModeOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nDSS/CCk Mode in 40 MHz Configured                                : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nDSS?CCK Mode in 40 MHz Configured                                : Enable");
                        }
                    }
                    if (nmhGetDot11LSIGTXOPFullProtectionActivated
                        (i4RadioIfIndex,
                         &i4LSigTxopProtectionOption) == SNMP_SUCCESS)
                    {
                        if (i4LSigTxopProtectionOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nL-SIG TXOP Protection Support Configured                         : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nL-SIG TXOP Protection Support Configured                         : Enable");
                        }
                    }
                    if (nmhGetFsDot11nConfigMaxRxAMPDUFactorConfig
                        (i4RadioIfIndex, &u4MaxMpduOption) == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nMaximum MPDU Factor Confidured                                       : %d",
                                   u4MaxMpduOption);
                    }

                    if (nmhGetFsDot11nConfigMinimumMPDUStartSpacingConfig
                        (i4RadioIfIndex, &u4ampduFactor) == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nMaximum MPDU Start Spacing Configured                                : %d",
                                   u4ampduFactor);
                    }
                    if (nmhGetFsDot11nConfigPCOOptionActivated
                        (i4RadioIfIndex, &i4pcoOption) == SNMP_SUCCESS)
                    {
                        if (i4pcoOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nPCO Option Configured                                            : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nPCO Option Configured                                            : Enable");
                        }
                    }
                    if (nmhGetFsDot11nConfigTransitionTimeConfig
                        (i4RadioIfIndex, &u4pcoTransTime) == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nPCO Transition time Configured                                       : %d",
                                   u4pcoTransTime);
                    }
                    if (nmhGetFsDot11nConfigMCSFeedbackOptionActivated
                        (i4RadioIfIndex, &i4mcsFeedbackOption) == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nMCS Feedback Value Configured                                       : %d",
                                   u4mcsFeedbackOption);
                    }
                    if (nmhGetFsDot11nConfigHTControlFieldSupported
                        (i4RadioIfIndex, &i4htcFieldOption) == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nHTC Support Configured                                              : %d",
                                   i4htcFieldOption);
                    }
                    if (nmhGetFsDot11nConfigRDResponderOptionActivated
                        (i4RadioIfIndex, &i4RDRespondOption) == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nRD Responder Configured                                             : %d",
                                   i4RDRespondOption);
                    }
                    if (nmhGetFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated (i4RadioIfIndex, &i4ImpTranBeamRecOption) == SNMP_SUCCESS)
                    {
                        if (i4ImpTranBeamRecOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nImplicit Transmit Beamforming Receiving Configured              : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nImplicit Transmit Beamforming Receiving Configured              : Enable");
                        }
                    }
                    if (nmhGetFsDot11nConfigReceiveStaggerSoundingOptionActivated (i4RadioIfIndex, &i4RecStagSoundOption) == SNMP_SUCCESS)
                    {
                        if (i4RecStagSoundOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nReceiving Staggered Sounding Configured                         : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nReceiving Staggered Sounding Configured                         : Enable");
                        }
                    }
                    if (nmhGetFsDot11nConfigTransmitStaggerSoundingOptionActivated (i4RadioIfIndex, &i4TranStagSoundOption) == SNMP_SUCCESS)
                    {
                        if (i4TranStagSoundOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nTransmit Staggered Sounding Configured                          : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nTransmit Staggered Sounding Configured                          : Enable");
                        }
                    }
                    if (nmhGetFsDot11nConfigReceiveNDPOptionActivated
                        (i4RadioIfIndex, &i4RecvNDPOption) == SNMP_SUCCESS)
                    {
                        if (i4RecvNDPOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nReceive NDP Configured                                          : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nReceive NDP Configured                                          : Enable");
                        }
                    }

                    if (nmhGetFsDot11nConfigTransmitNDPOptionActivated
                        (i4RadioIfIndex, &i4TranNDPOption) == SNMP_SUCCESS)
                    {
                        if (i4TranNDPOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Transmit NDP Configured                                        : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Transmit NDP Configured                                        : Enable");
                        }
                    }
                    if (nmhGetFsDot11nConfigImplicitTransmitBeamformingOptionActivated (i4RadioIfIndex, &i4ImpTranBeamOption) == SNMP_SUCCESS)
                    {
                        if (i4ImpTranBeamOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nImplicit Transmit Beamforming Configured                        : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nImplicit Transmit Beamforming Configured                        : Enable");
                        }
                    }
                    if (nmhGetFsDot11nConfigCalibrationOptionActivated
                        (i4RadioIfIndex, &i4CalibrationOption) == SNMP_SUCCESS)
                    {
                        if (i4CalibrationOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nCalibration Option Configured                                   : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nCalibration Option Configured                                   : Enable");
                        }
                    }
                    if (nmhGetFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated (i4RadioIfIndex, &i4ExplCsiTranBeamOption) == SNMP_SUCCESS)
                    {
                        if (i4ExplCsiTranBeamOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nExplicit CSI Transmit Beamforming Configured                    : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nExplicit CSI Transmit Beamforming Configured                    : Enable");
                        }
                    }
                    if (nmhGetFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated (i4RadioIfIndex, &i4ExplNoncompSteOption) == SNMP_SUCCESS)
                    {
                        if (i4ExplNoncompSteOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nExplicit Noncompressed Steering Configured                      : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nExplicit Noncompressed Steering Configured                      : Enable");
                        }
                    }
                    if (nmhGetFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated (i4RadioIfIndex, &i4ExplcompSteOption) == SNMP_SUCCESS)
                    {
                        if (i4ExplcompSteOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nExplicit Compressed Steering Configured                         : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nExplicit Compressed Steering                                    : Enable");
                        }
                    }
                    if (nmhGetFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated (i4RadioIfIndex, &i4ExplTranBeamCsiOption) == SNMP_SUCCESS)
                    {
                        if (i4ExplTranBeamCsiOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nExplicit Transmit Beamforming CSI Configured                    : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nExplicit Transmit Beamforming CSI Configured                    : Enable");
                        }
                    }
                    if (nmhGetFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated (i4RadioIfIndex, &i4ExplNCompBeamOption) == SNMP_SUCCESS)
                    {
                        if (i4ExplNCompBeamOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nExplicit Noncompressed Beamforming Feedback Configured          : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nExplicit Noncompressed Beamforming Feedback Configured          : Enable");
                        }
                    }
                    if (nmhGetFsDot11nConfigMinimalGroupingActivated
                        (i4RadioIfIndex, &u4MinGroupingOption) == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nMinimal Grouping Configured                                         : %d",
                                   u4MinGroupingOption);
                    }
                    if (nmhGetFsDot11nConfigNumberBeamFormingCSISupportAntenna
                        (i4RadioIfIndex, &u4numCsiAntSupOption) == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nCSI Number of Beamformer Antennas Configured                        : %d",
                                   u4numCsiAntSupOption);
                    }
                    if (nmhGetFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna (i4RadioIfIndex, &u4numNCompAntSupOption) == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nNoncompressed Steering Number of Beamformer Antennas Configured     : %d",
                                   u4numNCompAntSupOption);
                    }
                    if (nmhGetFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna (i4RadioIfIndex, &u4numCompAntSupOption) == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\ncompressed Steering Number of Beamformer Antennas Configured        : %d",
                                   u4numCompAntSupOption);
                    }
                    if (nmhGetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated (i4RadioIfIndex, &u4MaxnumCsiAntSupOption) == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nCSI Max Number of Rows Beamformer Configured                        : %d",
                                   u4MaxnumCsiAntSupOption);
                    }
                    if (nmhGetFsDot11nConfigChannelEstimationCapabilityActivated
                        (i4RadioIfIndex, &u4ChannelEstOption) == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nChannel Estimation Configured                                       : %d",
                                   u4ChannelEstOption);
                    }
                    if (nmhGetFsDot11nConfigCurrentPrimaryChannel
                        (i4RadioIfIndex,
                         &u4PrimaryChannelOption) == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nPrimary Channel Configured                                          : %d",
                                   u4PrimaryChannelOption);
                    }
                    if (nmhGetFsDot11nConfigCurrentSecondaryChannel
                        (i4RadioIfIndex,
                         &u4SecondaryChannelOption) == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nSecondary Channel Configured                                        : %d",
                                   u4SecondaryChannelOption);
                    }
                    if (nmhGetFsDot11nConfigSTAChannelWidthConfig
                        (i4RadioIfIndex,
                         &i4htOpChannelWidthOption) == SNMP_SUCCESS)
                    {
                        if (i4htOpChannelWidthOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nSTA Channel Width Configured                                    : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nSTA Channel Width Configured                                    : Enable");
                        }
                    }
                    if (nmhGetFsDot11nConfigOBSSNonHTSTAsPresentConfig
                        (i4RadioIfIndex, &i4htNonObssStaOption) == SNMP_SUCCESS)
                    {
                        if (i4htNonObssStaOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nOBSS Non-HT STAs Configured                                     : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nOBSS Non-HT STAs Configured                                     : Enable");
                        }
                    }
                    if (nmhGetDot11FortyMHzIntolerant
                        (i4RadioIfIndex,
                         &i4FortyMhzIntolerantOption) == SNMP_SUCCESS)
                    {
                        if (i4FortyMhzIntolerantOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n40 MHz Intolerant Option                                        : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\n40 MHz Intolerant Option                                        : Enable");
                        }
                    }

                    if (nmhGetDot11HTProtection
                        (i4RadioIfIndex, &i4htProtectionOption) == SNMP_SUCCESS)
                    {
                        if (i4htProtectionOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nHT Protection                                                   : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nHT Protection                                                   : Enable");
                        }
                    }

                    if (nmhGetDot11RIFSMode (i4RadioIfIndex, &i4RifsModeOption)
                        == SNMP_SUCCESS)
                    {
                        if (i4RifsModeOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nRIFS Mode                                                       : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nRIFS Mode                                                       : Enable");
                        }
                    }
                    if (nmhGetDot11DualCTSProtection
                        (i4RadioIfIndex,
                         &i4DualCtsProtectionOption) == SNMP_SUCCESS)
                    {
                        if (i4DualCtsProtectionOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nDual CTS Protection                                             : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nDual CTS Protection                                             : Enable");
                        }
                    }

                    if (nmhGetFsDot11nConfigSTBCBeaconConfig
                        (i4RadioIfIndex, &i4StbcBeaconOption) == SNMP_SUCCESS)
                    {
                        if (i4StbcBeaconOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nSTBC Beacon                                                     : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nSTBC Beacon                                                     : Enable");
                        }
                    }
                    if (nmhGetDot11PCOActivated
                        (i4RadioIfIndex, &i4PcoConfigOption) == SNMP_SUCCESS)
                    {
                        if (i4PcoConfigOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n PCO Configured                                                 : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\n PCO Configured                                                 : Enable");
                        }
                    }
                    if (nmhGetFsDot11nConfigPCOPhaseConfig
                        (i4RadioIfIndex, &i4PcoPhaseOption) == SNMP_SUCCESS)
                    {
                        if (i4PcoPhaseOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n PCO Phase Configured                                           : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nPCO Phase Configured                                            : Enable");
                        }
                    }
                    if (nmhGetDot11TxMCSSetDefined
                        (i4RadioIfIndex, &i4mcsSetOption) == SNMP_SUCCESS)
                    {
                        if (i4mcsSetOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n MCS SEt Defined Option                                         : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\n MCS SEt Defined Option                                         : Enable");
                        }
                    }
                    if (nmhGetFsDot11nConfigTxRxMCSSetNotEqual
                        (i4RadioIfIndex, &i4TxRxMcsSetOption) == SNMP_SUCCESS)
                    {
                        if (i4TxRxMcsSetOption == 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Tx Rx MCs Set Option                                           : Disable");
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Tx Rx MCs Set Option                                           : Enable");
                        }
                    }

                    for (u1MCSIndex = 0; u1MCSIndex < CLI_MAX_MCS_RATE_INDEX;
                         u1MCSIndex++)
                    {
                        MEMSET (&WsscfgGetFsDot11nMCSDataRateEntry, 0,
                                sizeof (tWsscfgFsDot11nMCSDataRateEntry));
                        WsscfgGetFsDot11nMCSDataRateEntry.
                            MibObject.i4FsDot11nMCSDataRateIndex = u1MCSIndex;
                        WsscfgGetFsDot11nMCSDataRateEntry.MibObject.
                            i4IfIndex = i4RadioIfIndex;
                        if (WsscfgGetAllFsDot11nMCSDataRateTable
                            (&WsscfgGetFsDot11nMCSDataRateEntry) !=
                            OSIX_SUCCESS)
                        {
                            return CLI_FAILURE;
                        }
                        if (WsscfgGetFsDot11nMCSDataRateEntry.
                            MibObject.i4FsDot11nMCSDataRate != 0)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nMCS %02u Rate Enable Status    : Enable",
                                       u1MCSIndex);
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "\r\nMCS %02u Rate Enable Status    : Disable",
                                       u1MCSIndex);
                        }
                    }

                }

                else
                {
                    CliPrintf (CliHandle,
                               "\r\n11nSupport                                                : Disable");
                }
                if (nmhGetIfMainAdminStatus
                    (i4RadioIfIndex, &i4AdminStatus) != SNMP_SUCCESS)
                {
                }
                if (i4AdminStatus == CFA_IF_UP)
                {
                    CliPrintf (CliHandle,
                               "\r\nAdmin status                                              : Enable");
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r\nAdmin status                                                            : Disable");
                }
                if (u4FsRadioType == CLI_RADIO_TYPEA)
                {
                    WsscfgGetDot11PhyOFDMEntry.MibObject.i4IfIndex =
                        i4RadioIfIndex;
                    if (WsscfgGetAllDot11PhyOFDMTable
                        (&WsscfgGetDot11PhyOFDMEntry) != OSIX_SUCCESS)
                    {
                    }
                    CliPrintf (CliHandle,
                               "\r\nCurrent Channel                                                    : %d",
                               WsscfgGetDot11PhyOFDMEntry.
                               MibObject.i4Dot11CurrentFrequency);
                }
                if (u4FsRadioType == CLI_RADIO_TYPEB)
                {
                    WsscfgGetDot11PhyDSSSEntry.MibObject.i4IfIndex =
                        i4RadioIfIndex;
                    if (WsscfgGetAllDot11PhyDSSSTable
                        (&WsscfgGetDot11PhyDSSSEntry) != OSIX_SUCCESS)
                    {
                    }
                    CliPrintf (CliHandle,
                               "\r\nCurrent Channel                                                    : %d",
                               WsscfgGetDot11PhyDSSSEntry.
                               MibObject.i4Dot11CurrentChannel);
                }

                CliPrintf (CliHandle,
                           "\r\nCountry Name                                                 : %s",
                           WsscfgGetDot11StationConfigEntry.MibObject.
                           au1Dot11CountryString);
                CliPrintf (CliHandle,
                           "\r\nMac Beacon Period                                            : %d",
                           WsscfgGetDot11StationConfigEntry.MibObject.
                           i4Dot11BeaconPeriod);
                CliPrintf (CliHandle,
                           "\r\nMedium Occupancy Limit                                       : %d",
                           WsscfgGetDot11StationConfigEntry.MibObject.
                           i4Dot11MediumOccupancyLimit);
                CliPrintf (CliHandle,
                           "\r\nCF Pollable                                                  : %d",
                           WsscfgGetDot11StationConfigEntry.MibObject.
                           i4Dot11CFPollable);
                CliPrintf (CliHandle,
                           "\r\nCFP Period                                                   : %d",
                           WsscfgGetDot11StationConfigEntry.MibObject.
                           i4Dot11CFPPeriod);
                CliPrintf (CliHandle,
                           "\r\nCFP Max Duration                                             : %d",
                           WsscfgGetDot11StationConfigEntry.MibObject.
                           i4Dot11CFPMaxDuration);
                CliPrintf (CliHandle,
                           "\r\nDTIM Period                                                  : %d",
                           WsscfgGetDot11StationConfigEntry.MibObject.
                           i4Dot11DTIMPeriod);
                CliPrintf (CliHandle,
                           "\r\nFragmentation Threshold                                      : %d",
                           WsscfgGetDot11OperationEntry.MibObject.
                           i4Dot11FragmentationThreshold);
                CliPrintf (CliHandle,
                           "\r\nRTS Threshold                                                : %d",
                           WsscfgGetDot11OperationEntry.MibObject.
                           i4Dot11RTSThreshold);
                CliPrintf (CliHandle,
                           "\r\nLong Retry Limit                                             : %d",
                           WsscfgGetDot11OperationEntry.MibObject.
                           i4Dot11LongRetryLimit);
                CliPrintf (CliHandle,
                           "\r\nShort Retry Limit                                            : %d",
                           WsscfgGetDot11OperationEntry.MibObject.
                           i4Dot11ShortRetryLimit);
                CliPrintf (CliHandle,
                           "\r\nMax Transmit MSDULifetime                                    : %d",
                           WsscfgGetDot11OperationEntry.MibObject.
                           u4Dot11MaxTransmitMSDULifetime);
                CliPrintf (CliHandle,
                           "\r\nMax Receive Lifetime                                         : %d",
                           WsscfgGetDot11OperationEntry.MibObject.
                           u4Dot11MaxReceiveLifetime);
                CliPrintf (CliHandle,
                           "\r\nCurrent Tx Power Level                                       : %d",
                           WsscfgGetDot11PhyTxPowerEntry.MibObject.
                           i4Dot11CurrentTxPowerLevel);
                CliPrintf (CliHandle, "\r\n");
            }
        }

        else
        {
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r\nFor Radio %d : Virtual Radio interface Index not successfully obtained,Operation Failed for the particular Radio index \r\n",
                           u1RadioIndex);
                return CLI_FAILURE;

            }

            if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                        &u4GetRadioType) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\n Getting Radio Type failed \r\n");
                return CLI_SUCCESS;
            }
            if (WSSCFG_RADIO_TYPE_COMP (u4FsRadioType))
            {
                CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                return CLI_SUCCESS;
            }

            WsscfgGetDot11StationConfigEntry.MibObject.i4IfIndex =
                i4RadioIfIndex;
            WsscfgGetDot11PhyTxPowerEntry.MibObject.i4IfIndex = i4RadioIfIndex;
            WsscfgGetDot11OperationEntry.MibObject.i4IfIndex = i4RadioIfIndex;
            WsscfgGetFsDot11nConfigEntry.MibObject.i4IfIndex = i4RadioIfIndex;

            if ((WsscfgGetAllDot11StationConfigTable
                 (&WsscfgGetDot11StationConfigEntry)) != OSIX_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if ((WsscfgGetAllDot11OperationTable
                 (&WsscfgGetDot11OperationEntry)) != OSIX_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if ((WsscfgGetAllDot11PhyTxPowerTable
                 (&WsscfgGetDot11PhyTxPowerEntry)) != OSIX_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhGetFsDot11RadioType (i4RadioIfIndex, &u4RadioIfType)
                != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r\nRadio Type not successfully obtained , (Invalid Input, Check Profile name) \r\n");
                return CLI_FAILURE;
            }

            if (WSSCFG_RADIO_TYPEN_AC_COMP (u4RadioIfType))
            {
                if ((WsscfgGetAllFsDot11nConfigTable
                     (&WsscfgGetFsDot11nConfigEntry)) != OSIX_SUCCESS)
                {
                    return CLI_FAILURE;
                }

                CliPrintf (CliHandle,
                           "\r\n11nSupport                                                    : Enable");

                if (nmhGetDot11LDPCCodingOptionActivated
                    (i4RadioIfIndex, &i4LdpcOption) == SNMP_SUCCESS)
                {
                    if (i4LdpcOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nLDPC Option Configured                                              : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nLDPC Option Configured                                              : Enable");
                    }
                }
                CliPrintf (CliHandle,
                           "\r\nChannel Width                                                 : %d",
                           WsscfgGetFsDot11nConfigEntry.MibObject.
                           i4FsDot11nConfigChannelWidth);

                if (WsscfgGetFsDot11nConfigEntry.
                    MibObject.i4FsDot11nConfigShortGIfor20MHz != 0)
                {
                    CliPrintf (CliHandle,
                               "\r\nShort Gi20                                                              : Enable");
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r\nShort Gi20                                                              : Disable");
                }

                if (WsscfgGetFsDot11nConfigEntry.
                    MibObject.i4FsDot11nConfigShortGIfor40MHz != 0)
                {
                    CliPrintf (CliHandle,
                               "\r\nShort Gi40                                                              : Enable");
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r\nShort Gi40                                                              : Disable");
                }

                if (nmhGetFsDot11nConfigMIMOPowerSave
                    (i4RadioIfIndex, &i4PowerSaveOption) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nPower Save Mode Configured                                              : %d",
                               i4PowerSaveOption);
                }
                if (nmhGetDot11HTGreenfieldOptionActivated
                    (i4RadioIfIndex, &i4GreenfieldOption) == SNMP_SUCCESS)
                {
                    if (i4GreenfieldOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nGreen Field Option Configured                                       : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nGreen Field OPtion Configured                                       : Enable");
                    }
                }
                if (nmhGetDot11TxSTBCOptionActivated
                    (i4RadioIfIndex, &i4TxStbcOption) == SNMP_SUCCESS)
                {
                    if (i4TxStbcOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nTx STBC Option Configured                                           : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nTx STBC Option COnfigured                                           : Enable");
                    }
                }

                if (nmhGetDot11RxSTBCOptionActivated
                    (i4RadioIfIndex, &i4RxStbcOption) == SNMP_SUCCESS)
                {
                    if (i4RxStbcOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nRx STBC Option Configured                                           : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nRx STBC Option Configured                                           : Enable");
                    }
                }
                if (nmhGetFsDot11nConfigDelayedBlockAckOptionActivated
                    (i4RadioIfIndex, &i4delayBlockAck) == SNMP_SUCCESS)
                {
                    if (i4delayBlockAck == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nHT Delayed Block Ack Option Configured                              : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nHT Delayed Block Ack Option Configured                              : Enable");
                    }
                }
                if (nmhGetFsDot11nConfigMaxAMSDULengthConfig
                    (i4RadioIfIndex, &i4MaxAmsduLenghth) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nMaximum AMSDU Length Configured                                         : %d",
                               i4MaxAmsduLenghth);
                }
                if (nmhGetFsDot11nConfigtHTDSSCCKModein40MHzConfig
                    (i4RadioIfIndex, &i4DssCckModeOption) == SNMP_SUCCESS)
                {
                    if (i4DssCckModeOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nDSS/CCk Mode in 40 MHz Configured                                   : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nDSS?CCK Mode in 40 MHz Configured                                   : Enable");
                    }
                }
                if (nmhGetDot11LSIGTXOPFullProtectionActivated
                    (i4RadioIfIndex,
                     &i4LSigTxopProtectionOption) == SNMP_SUCCESS)
                {
                    if (i4LSigTxopProtectionOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nL-SIG TXOP Protection Support Configured                            : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nL-SIG TXOP Protection Support Configured                            : Enable");
                    }
                }
                if (nmhGetFsDot11nConfigMaxRxAMPDUFactorConfig
                    (i4RadioIfIndex, &u4MaxMpduOption) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nMaximum MPDU Factor Confidured                                          : %d",
                               u4MaxMpduOption);
                }

                if (nmhGetFsDot11nConfigMinimumMPDUStartSpacingConfig
                    (i4RadioIfIndex, &u4ampduFactor) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nMaximum MPDU Start Spacing Configured                                   : %d",
                               u4ampduFactor);
                }
                if (nmhGetFsDot11nConfigPCOOptionActivated
                    (i4RadioIfIndex, &i4pcoOption) == SNMP_SUCCESS)
                {
                    if (i4pcoOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nPCO Option Configured                                               : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nPCO Option Configured                                               : Enable");
                    }
                }
                if (nmhGetFsDot11nConfigTransitionTimeConfig
                    (i4RadioIfIndex, &u4pcoTransTime) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nPCO Transition time Configured                                          : %d",
                               u4pcoTransTime);
                }
                if (nmhGetFsDot11nConfigMCSFeedbackOptionActivated
                    (i4RadioIfIndex, &i4mcsFeedbackOption) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nMCS Feedback Value Configured                                           : %d",
                               u4mcsFeedbackOption);
                }
                if (nmhGetFsDot11nConfigHTControlFieldSupported
                    (i4RadioIfIndex, &i4htcFieldOption) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nHTC Support Configured                                                  : %d",
                               i4htcFieldOption);
                }
                if (nmhGetFsDot11nConfigRDResponderOptionActivated
                    (i4RadioIfIndex, &i4RDRespondOption) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nRD Responder Configured                                                 : %d",
                               i4RDRespondOption);
                }
                if (nmhGetFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated (i4RadioIfIndex, &i4ImpTranBeamRecOption) == SNMP_SUCCESS)
                {
                    if (i4ImpTranBeamRecOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nImplicit Transmit Beamforming Receiving Configured                 : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nImplicit Transmit Beamforming Receiving Configured                 : Enable");
                    }
                }
                if (nmhGetFsDot11nConfigReceiveStaggerSoundingOptionActivated
                    (i4RadioIfIndex, &i4RecStagSoundOption) == SNMP_SUCCESS)
                {
                    if (i4RecStagSoundOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nReceiving Staggered Sounding Configured                            : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nReceiving Staggered Sounding Configured                            : Enable");
                    }
                }
                if (nmhGetFsDot11nConfigTransmitStaggerSoundingOptionActivated
                    (i4RadioIfIndex, &i4TranStagSoundOption) == SNMP_SUCCESS)
                {
                    if (i4TranStagSoundOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nTransmit Staggered Sounding Configured                             : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nTransmit Staggered Sounding Configured                             : Enable");
                    }
                }
                if (nmhGetFsDot11nConfigReceiveNDPOptionActivated
                    (i4RadioIfIndex, &i4RecvNDPOption) == SNMP_SUCCESS)
                {
                    if (i4RecvNDPOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nReceive NDP Configured                                             : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nReceive NDP Configured                                             : Enable");
                    }
                }

                if (nmhGetFsDot11nConfigTransmitNDPOptionActivated
                    (i4RadioIfIndex, &i4TranNDPOption) == SNMP_SUCCESS)
                {
                    if (i4TranNDPOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Transmit NDP Configured                                           : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Transmit NDP Configured                                           : Enable");
                    }
                }
                if (nmhGetFsDot11nConfigImplicitTransmitBeamformingOptionActivated (i4RadioIfIndex, &i4ImpTranBeamOption) == SNMP_SUCCESS)
                {
                    if (i4ImpTranBeamOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nImplicit Transmit Beamforming Configured                           : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nImplicit Transmit Beamforming Configured                           : Enable");
                    }
                }
                if (nmhGetFsDot11nConfigCalibrationOptionActivated
                    (i4RadioIfIndex, &i4CalibrationOption) == SNMP_SUCCESS)
                {
                    if (i4CalibrationOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nCalibration Option Configured                                      : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nCalibration Option Configured                                      : Enable");
                    }
                }
                if (nmhGetFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated (i4RadioIfIndex, &i4ExplCsiTranBeamOption) == SNMP_SUCCESS)
                {
                    if (i4ExplCsiTranBeamOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nExplicit CSI Transmit Beamforming Configured                       : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nExplicit CSI Transmit Beamforming Configured                       : Enable");
                    }
                }
                if (nmhGetFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated (i4RadioIfIndex, &i4ExplNoncompSteOption) == SNMP_SUCCESS)
                {
                    if (i4ExplNoncompSteOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nExplicit Noncompressed Steering Configured                         : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nExplicit Noncompressed Steering Configured                         : Enable");
                    }
                }
                if (nmhGetFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated (i4RadioIfIndex, &i4ExplcompSteOption) == SNMP_SUCCESS)
                {
                    if (i4ExplcompSteOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nExplicit Compressed Steering Configured                            : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nExplicit Compressed Steering                                       : Enable");
                    }
                }
                if (nmhGetFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated (i4RadioIfIndex, &i4ExplTranBeamCsiOption) == SNMP_SUCCESS)
                {
                    if (i4ExplTranBeamCsiOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nExplicit Transmit Beamforming CSI Configured                       : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nExplicit Transmit Beamforming CSI Configured                       : Enable");
                    }
                }
                if (nmhGetFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated (i4RadioIfIndex, &i4ExplNCompBeamOption) == SNMP_SUCCESS)
                {
                    if (i4ExplNCompBeamOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nExplicit Noncompressed Beamforming Feedback Configured             : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nExplicit Noncompressed Beamforming Feedback Configured             : Enable");
                    }
                }
                if (nmhGetFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated (i4RadioIfIndex, &i4ExplCompBeamOption) == SNMP_SUCCESS)
                {
                    if (i4ExplCompBeamOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nExplicit Compressed Beamforming Feedback Configured                : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nExplicit Compressed Beamforming Feedback Configured                : Enable");
                    }
                }
                if (nmhGetFsDot11nConfigMinimalGroupingActivated
                    (i4RadioIfIndex, &u4MinGroupingOption) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nMinimal Grouping Configured                                            : %d",
                               u4MinGroupingOption);
                }
                if (nmhGetFsDot11nConfigNumberBeamFormingCSISupportAntenna
                    (i4RadioIfIndex, &u4numCsiAntSupOption) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nCSI Number of Beamformer Antennas Configured                           : %d",
                               u4numCsiAntSupOption);
                }
                if (nmhGetFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna (i4RadioIfIndex, &u4numNCompAntSupOption) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nNoncompressed Steering Number of Beamformer Antennas Configured        : %d",
                               u4numNCompAntSupOption);
                }
                if (nmhGetFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna (i4RadioIfIndex, &u4numCompAntSupOption) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\ncompressed Steering Number of Beamformer Antennas Configured           : %d",
                               u4numCompAntSupOption);
                }
                if (nmhGetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated (i4RadioIfIndex, &u4MaxnumCsiAntSupOption) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nCSI Max Number of Rows Beamformer Configured                           : %d",
                               u4MaxnumCsiAntSupOption);
                }
                if (nmhGetFsDot11nConfigChannelEstimationCapabilityActivated
                    (i4RadioIfIndex, &u4ChannelEstOption) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nChannel Estimation Configured                                         : %d",
                               u4ChannelEstOption);
                }
                if (nmhGetFsDot11nConfigCurrentPrimaryChannel
                    (i4RadioIfIndex, &u4PrimaryChannelOption) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nPrimary Channel Configured                                            : %d",
                               u4PrimaryChannelOption);
                }
                if (nmhGetFsDot11nConfigSTAChannelWidthConfig
                    (i4RadioIfIndex, &i4htOpChannelWidthOption) == SNMP_SUCCESS)
                {
                    if (i4htOpChannelWidthOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nSTA Channel Width Configured                                      : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nSTA Channel Width Configured                                      : Enable");
                    }
                }
                if (nmhGetFsDot11nConfigOBSSNonHTSTAsPresentConfig
                    (i4RadioIfIndex, &i4htNonObssStaOption) == SNMP_SUCCESS)
                {
                    if (i4htNonObssStaOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nOBSS Non-HT STAs Configured                                       : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nOBSS Non-HT STAs Configured                                       : Enable");
                    }
                }
                if (nmhGetDot11FortyMHzIntolerant
                    (i4RadioIfIndex,
                     &i4FortyMhzIntolerantOption) == SNMP_SUCCESS)
                {
                    if (i4FortyMhzIntolerantOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n40 MHz Intolerant Option                                          : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\n40 MHz Intolerant Option                                          : Enable");
                    }
                }

                if (nmhGetDot11HTProtection
                    (i4RadioIfIndex, &i4htProtectionOption) == SNMP_SUCCESS)
                {
                    if (i4htProtectionOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nHT Protection                                                     : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nHT Protection                                                     : Enable");
                    }
                }

                if (nmhGetDot11RIFSMode (i4RadioIfIndex, &i4RifsModeOption)
                    == SNMP_SUCCESS)
                {
                    if (i4RifsModeOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nRIFS Mode                                                         : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nRIFS Mode                                                         : Enable");
                    }
                }

                if (nmhGetDot11DualCTSProtection
                    (i4RadioIfIndex,
                     &i4DualCtsProtectionOption) == SNMP_SUCCESS)
                {
                    if (i4DualCtsProtectionOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nDual CTS Protection                                               : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nDual CTS Protection                                               : Enable");
                    }
                }

                if (nmhGetFsDot11nConfigSTBCBeaconConfig
                    (i4RadioIfIndex, &i4StbcBeaconOption) == SNMP_SUCCESS)
                {
                    if (i4StbcBeaconOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nSTBC Beacon                                                       : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nSTBC Beacon                                                       : Enable");
                    }
                }
                if (nmhGetDot11PCOActivated (i4RadioIfIndex, &i4PcoConfigOption)
                    == SNMP_SUCCESS)
                {
                    if (i4PcoConfigOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n PCO Configured                                                   : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\n PCO Configured                                                   : Enable");
                    }
                }
                if (nmhGetFsDot11nConfigPCOPhaseConfig
                    (i4RadioIfIndex, &i4PcoPhaseOption) == SNMP_SUCCESS)
                {
                    if (i4PcoPhaseOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n PCO Phase Configured                                             : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nPCO Phase Configured                                              : Enable");
                    }
                }
                if (nmhGetDot11TxMCSSetDefined (i4RadioIfIndex, &i4mcsSetOption)
                    == SNMP_SUCCESS)
                {
                    if (i4mcsSetOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n MCS SEt Defined Option                                            : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\n MCS SEt Defined Option                                            : Enable");
                    }
                }
                if (nmhGetFsDot11nConfigTxRxMCSSetNotEqual
                    (i4RadioIfIndex, &i4TxRxMcsSetOption) == SNMP_SUCCESS)
                {
                    if (i4TxRxMcsSetOption == 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Tx Rx MCs Set Option                                              : Disable");
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Tx Rx MCs Set Option                                              : Enable");
                    }
                }

                for (u1MCSIndex = 0; u1MCSIndex < CLI_MAX_MCS_RATE_INDEX;
                     u1MCSIndex++)
                {
                    MEMSET (&WsscfgGetFsDot11nMCSDataRateEntry, 0,
                            sizeof (tWsscfgFsDot11nMCSDataRateEntry));
                    WsscfgGetFsDot11nMCSDataRateEntry.
                        MibObject.i4FsDot11nMCSDataRateIndex = u1MCSIndex;
                    WsscfgGetFsDot11nMCSDataRateEntry.MibObject.
                        i4IfIndex = i4RadioIfIndex;
                    if (WsscfgGetAllFsDot11nMCSDataRateTable
                        (&WsscfgGetFsDot11nMCSDataRateEntry) != OSIX_SUCCESS)
                    {
                        return CLI_FAILURE;
                    }
                    if (WsscfgGetFsDot11nMCSDataRateEntry.
                        MibObject.i4FsDot11nMCSDataRate != 0)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nMCS %02u Rate Enable Status    : Enable",
                                   u1MCSIndex);
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "\r\nMCS %02u Rate Enable Status    : Disable",
                                   u1MCSIndex);
                    }
                }

            }

            else
            {
                CliPrintf (CliHandle,
                           "\r\n11nSupport                                                   : Disable");
            }
            if (nmhGetIfMainAdminStatus
                (i4RadioIfIndex, &i4AdminStatus) != SNMP_SUCCESS)
            {
            }
            if (i4AdminStatus == CFA_IF_UP)
            {
                CliPrintf (CliHandle,
                           "\r\nAdmin status                                                 : Enable");
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r\nAdmin status                                                               : Disable");
            }
            if (u4FsRadioType == CLI_RADIO_TYPEA)
            {
                WsscfgGetDot11PhyOFDMEntry.MibObject.i4IfIndex = i4RadioIfIndex;
                if (WsscfgGetAllDot11PhyOFDMTable
                    (&WsscfgGetDot11PhyOFDMEntry) != OSIX_SUCCESS)
                {
                }
                CliPrintf (CliHandle,
                           "\r\nCurrent Channel                                                            : %d",
                           WsscfgGetDot11PhyOFDMEntry.
                           MibObject.i4Dot11CurrentFrequency);
            }
            if (u4FsRadioType == CLI_RADIO_TYPEB)
            {
                WsscfgGetDot11PhyDSSSEntry.MibObject.i4IfIndex = i4RadioIfIndex;
                if (WsscfgGetAllDot11PhyDSSSTable
                    (&WsscfgGetDot11PhyDSSSEntry) != OSIX_SUCCESS)
                {
                }
                CliPrintf (CliHandle,
                           "\r\nCurrent Channel                                                            : %d",
                           WsscfgGetDot11PhyDSSSEntry.
                           MibObject.i4Dot11CurrentChannel);
            }

            CliPrintf (CliHandle,
                       "\r\nCountry Name                                                     : %s",
                       WsscfgGetDot11StationConfigEntry.MibObject.
                       au1Dot11CountryString);
            CliPrintf (CliHandle,
                       "\r\nMac Beacon Period                                                : %d",
                       WsscfgGetDot11StationConfigEntry.MibObject.
                       i4Dot11BeaconPeriod);
            CliPrintf (CliHandle,
                       "\r\nMedium Occupancy Limit                                           : %d",
                       WsscfgGetDot11StationConfigEntry.MibObject.
                       i4Dot11MediumOccupancyLimit);
            CliPrintf (CliHandle,
                       "\r\nCF Pollable                                                      : %d",
                       WsscfgGetDot11StationConfigEntry.MibObject.
                       i4Dot11CFPollable);
            CliPrintf (CliHandle,
                       "\r\nCFP Period                                                       : %d",
                       WsscfgGetDot11StationConfigEntry.MibObject.
                       i4Dot11CFPPeriod);
            CliPrintf (CliHandle,
                       "\r\nCFP Max Duration                                                 : %d",
                       WsscfgGetDot11StationConfigEntry.MibObject.
                       i4Dot11CFPMaxDuration);
            CliPrintf (CliHandle,
                       "\r\nDTIM Period                                                      : %d",
                       WsscfgGetDot11StationConfigEntry.MibObject.
                       i4Dot11DTIMPeriod);
            CliPrintf (CliHandle,
                       "\r\nFragmentation Threshold                                          : %d",
                       WsscfgGetDot11OperationEntry.MibObject.
                       i4Dot11FragmentationThreshold);
            CliPrintf (CliHandle,
                       "\r\nRTS Threshold                                                    : %d",
                       WsscfgGetDot11OperationEntry.MibObject.
                       i4Dot11RTSThreshold);
            CliPrintf (CliHandle,
                       "\r\nLong Retry Limit                                                 : %d",
                       WsscfgGetDot11OperationEntry.MibObject.
                       i4Dot11LongRetryLimit);
            CliPrintf (CliHandle,
                       "\r\nShort Retry Limit                                                : %d",
                       WsscfgGetDot11OperationEntry.MibObject.
                       i4Dot11ShortRetryLimit);
            CliPrintf (CliHandle,
                       "\r\nMax Transmit MSDULifetime                                        : %d",
                       WsscfgGetDot11OperationEntry.MibObject.
                       u4Dot11MaxTransmitMSDULifetime);
            CliPrintf (CliHandle,
                       "\r\nMax Receive Lifetime                                             : %d",
                       WsscfgGetDot11OperationEntry.MibObject.
                       u4Dot11MaxReceiveLifetime);
            CliPrintf (CliHandle,
                       "\r\nCurrent Tx Power Level                                           : %d",
                       WsscfgGetDot11PhyTxPowerEntry.MibObject.
                       i4Dot11CurrentTxPowerLevel);
            CliPrintf (CliHandle, "\r\n");
        }

    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WssCfgShowApConfiguration 
* Description :
* Input       :  CliHandle 
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowApConfiguration (tCliHandle CliHandle,
                           UINT4 u4FsRadioType,
                           UINT1 *pu1CapwapBaseWtpProfileName, UINT4 u4RadioId)
{
    UINT1               au1WtpName[256];
    UINT1               au1MacAddr[21];
    UINT1               au1Country[3];
    tMacAddr            MacAddr;
    tMacAddr            RadiobaseMACAddress;
    UINT4               u4InOctets = 0;
    UINT4               u4OutOctets = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4GetRadioType = 0;
    INT4                i4MaxClientCount = 0;
    INT4                i4InterfaceType = 0;
    UINT1               au1TempMac[21];
    UINT1               au1IpId[4];
    UINT1               u1antennaid = 0;
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    tSNMP_OCTET_STRING_TYPE CapwapBaseWtpProfileWtpMacAddress;
    tSNMP_OCTET_STRING_TYPE WtpProfileName;
    tSNMP_OCTET_STRING_TYPE CountryString;
    tSNMP_OCTET_STRING_TYPE IpAddress;
    tSNMP_OCTET_STRING_TYPE RadioBand;
    UINT1               au1RadioBand[8];
    UINT4               u4CapwapBaseWtpProfileId = 0;
    tWsscfgDot11PhyOFDMEntry WsscfgGetDot11PhyOFDMEntry;
    tWsscfgFsRrmConfigEntry WsscfgGetFsRrmConfigEntry;
    tWsscfgDot11PhyTxPowerEntry WsscfgGetDot11PhyTxPowerEntry;
    tWsscfgDot11OperationEntry WsscfgGetDot11OperationEntry;
    tWsscfgFsDot11WlanCapabilityProfileEntry
        WsscfgGetFsDot11WlanCapabilityProfileEntry;
    tWsscfgDot11AntennasListEntry WsscfgGetDot11AntennasListEntry;
    tWsscfgFsDot11AntennasListEntry WsscfgGetFsDot11AntennasListEntry;
    tWsscfgDot11StationConfigEntry WsscfgGetDot11StationConfigEntry;
    tRadioIfGetDB       RadioIfGetDB;
    tWsscfgDot11PhyDSSSEntry WsscfgGetDot11PhyDSSSEntry;
    tWsscfgDot11QAPEDCAEntry WsscfgDot11QAPEDCAEntry;
    tWsscfgFsStationQosParamsEntry WsscfgFsStationQosParamsEntry;
    tWssPmWlanRadioStatsProcessRBDB WsscfgGetFsWlanRadioEntry;
    UINT4               u4IfIndex = 0;
    INT4                i4AdminStatus = 0;
    tSNMP_OCTET_STRING_TYPE InterfaceName;
    MEMSET (&InterfaceName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WsscfgGetDot11PhyOFDMEntry, 0, sizeof (tWsscfgDot11PhyOFDMEntry));
    MEMSET (&WsscfgGetFsRrmConfigEntry, 0, sizeof (tWsscfgFsRrmConfigEntry));
    MEMSET (&WsscfgGetDot11PhyTxPowerEntry, 0,
            sizeof (tWsscfgDot11PhyTxPowerEntry));
    MEMSET (&WsscfgGetDot11OperationEntry, 0,
            sizeof (tWsscfgDot11OperationEntry));
    MEMSET (&WsscfgGetFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));
    MEMSET (&WsscfgGetDot11AntennasListEntry, 0,
            sizeof (tWsscfgDot11AntennasListEntry));
    MEMSET (&WsscfgDot11QAPEDCAEntry, 0, sizeof (tWsscfgDot11QAPEDCAEntry));
    MEMSET (&WsscfgFsStationQosParamsEntry, 0,
            sizeof (tWsscfgFsStationQosParamsEntry));
    MEMSET (&WsscfgGetFsDot11AntennasListEntry, 0,
            sizeof (tWsscfgFsDot11AntennasListEntry));
    MEMSET (&WsscfgGetDot11StationConfigEntry, 0,
            sizeof (tWsscfgDot11StationConfigEntry));
    MEMSET (&WsscfgGetDot11AntennasListEntry, 0,
            sizeof (tWsscfgDot11AntennasListEntry));
    MEMSET (&WsscfgGetFsDot11AntennasListEntry, 0,
            sizeof (tWsscfgFsDot11AntennasListEntry));
    MEMSET (&WsscfgGetFsWlanRadioEntry, 0,
            sizeof (tWssPmWlanRadioStatsProcessRBDB));

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1WtpName, 0, 256);
    MEMSET (&IpAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RadioBand, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1IpId, 0, 4);
    MEMSET (&WsscfgGetDot11PhyDSSSEntry, 0, sizeof (tWsscfgDot11PhyDSSSEntry));
    MEMSET (au1MacAddr, 0, 21);
    MEMSET (au1TempMac, 0, 21);
    MEMSET (au1Country, 0, 3);
    MEMSET (au1InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&CountryString, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (MacAddr, 0, sizeof (tMacAddr));
    MEMSET (RadiobaseMACAddress, 0, sizeof (tMacAddr));
    MEMSET (&CapwapBaseWtpProfileWtpMacAddress, 0,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1RadioBand, 0, 8);
    IpAddress.pu1_OctetList = (UINT1 *) &u4IpAddr;

    CapwapBaseWtpProfileWtpMacAddress.pu1_OctetList = au1MacAddr;
    CountryString.pu1_OctetList = au1Country;
    InterfaceName.pu1_OctetList = au1InterfaceName;
    RadioBand.pu1_OctetList = au1RadioBand;
    if (pu1CapwapBaseWtpProfileName != NULL)
    {

        if (CapwapGetWtpProfileIdFromProfileName
            (pu1CapwapBaseWtpProfileName,
             &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nProfile entry not found\r\n");
            return CLI_FAILURE;
        }
    }
    WtpProfileName.pu1_OctetList = au1WtpName;
    if (nmhGetCapwapBaseWtpProfileName
        (u4CapwapBaseWtpProfileId, &WtpProfileName) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
        (u4CapwapBaseWtpProfileId, u4RadioId,
         (INT4 *) &u4IfIndex) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nRadio Index not found\n");
        return CLI_FAILURE;
    }

    if (nmhGetFsDot11RadioType ((INT4) u4IfIndex,
                                &u4GetRadioType) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Getting Radio Type failed \r\n");
        return CLI_SUCCESS;
    }
    if (WSSCFG_RADIO_TYPE_COMP (u4FsRadioType))
    {
        CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\nAP Name                   : %s",
               WtpProfileName.pu1_OctetList);
    CliPrintf (CliHandle, "\r\nAP Profile ID             : %d",
               u4CapwapBaseWtpProfileId);

    if (nmhGetIfType (u4IfIndex, &i4InterfaceType) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nRadio If Type             : %d",
                   i4InterfaceType);
    }
    if (nmhGetIfName (u4IfIndex, &InterfaceName) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nRadio If Name             : %s",
                   InterfaceName.pu1_OctetList);
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bOperationalRate = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {

    }
    if (nmhGetFsWlanRadioRadiobaseMACAddress
        ((INT4) u4IfIndex, &RadiobaseMACAddress) == SNMP_SUCCESS)
    {

        PrintMacAddress ((UINT1 *) &RadiobaseMACAddress, au1TempMac);
        CliPrintf (CliHandle, "\r\nRadioIf Base MacAddress   : %s", au1TempMac);
    }

    if (nmhGetFsWlanRadioIfInOctets
        ((INT4) u4IfIndex, &u4InOctets) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nRadio If InOctets         : %u", u4InOctets);
    }

    if (nmhGetFsWlanRadioIfOutOctets
        ((INT4) u4IfIndex, &u4OutOctets) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nRadio If OutOctets        : %u",
                   u4OutOctets);
    }
    if (nmhGetFsWlanRadioMaxClientCount
        ((INT4) u4IfIndex, &i4MaxClientCount) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nMax Client Count          : %d",
                   i4MaxClientCount);
    }

    if (nmhGetFsWlanRadioRadioBand
        ((INT4) u4IfIndex, &RadioBand) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nRadio band                : %s",
                   au1RadioBand);
    }
    if (nmhGetCapwapBaseWtpProfileWtpMacAddress
        (u4CapwapBaseWtpProfileId,
         &CapwapBaseWtpProfileWtpMacAddress) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nMac Address               : %s",
               CapwapBaseWtpProfileWtpMacAddress.pu1_OctetList);

    WsscfgGetDot11StationConfigEntry.MibObject.i4IfIndex = u4IfIndex;
    WsscfgGetDot11PhyTxPowerEntry.MibObject.i4IfIndex = u4IfIndex;
    WsscfgGetDot11OperationEntry.MibObject.i4IfIndex = u4IfIndex;
    WsscfgGetDot11AntennasListEntry.MibObject.i4IfIndex = u4IfIndex;
    WsscfgGetFsDot11AntennasListEntry.MibObject.i4IfIndex = u4IfIndex;
    WsscfgFsStationQosParamsEntry.MibObject.i4IfIndex = u4IfIndex;
    WsscfgDot11QAPEDCAEntry.MibObject.i4IfIndex = u4IfIndex;
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;

    if (WsscfgGetAllDot11StationConfigTable
        (&WsscfgGetDot11StationConfigEntry) != OSIX_SUCCESS)
    {
    }

    if (WsscfgGetAllDot11OperationTable (&WsscfgGetDot11OperationEntry) !=
        OSIX_SUCCESS)
    {
    }

    if (WsscfgGetAllDot11PhyTxPowerTable
        (&WsscfgGetDot11PhyTxPowerEntry) != OSIX_SUCCESS)
    {
    }
    if (WsscfgGetAllDot11AntennasListTable (&WsscfgGetDot11AntennasListEntry) !=
        OSIX_SUCCESS)
    {
    }
    if (WsscfgGetAllFsDot11AntennasListTable
        (&WsscfgGetFsDot11AntennasListEntry) != OSIX_SUCCESS)
    {
    }

    if (WssCfgGetRadioParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
    }

    if (WsscfgGetAllDot11QAPEDCATable (&WsscfgDot11QAPEDCAEntry) !=
        OSIX_SUCCESS)
    {
    }

    if (WsscfgGetAllFsStationQosParamsTable (&WsscfgFsStationQosParamsEntry) !=
        OSIX_SUCCESS)
    {
    }

    if (nmhGetIfMainAdminStatus (u4IfIndex, &i4AdminStatus) != SNMP_SUCCESS)
    {
    }
    if (i4AdminStatus == CFA_IF_UP)
    {
        CliPrintf (CliHandle, "\r\nAdmin status              : Enable");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nAdmin status              : Disable");
    }
    if (u4FsRadioType == CLI_RADIO_TYPEA)
    {
        WsscfgGetDot11PhyOFDMEntry.MibObject.i4IfIndex = u4IfIndex;
        if (WsscfgGetAllDot11PhyOFDMTable (&WsscfgGetDot11PhyOFDMEntry)
            != OSIX_SUCCESS)
        {
        }
        CliPrintf (CliHandle, "\r\nCurrent Channel           : %d",
                   WsscfgGetDot11PhyOFDMEntry.
                   MibObject.i4Dot11CurrentFrequency);
        CliPrintf (CliHandle, "\r\nRSSI threshold            : %d",
                   WsscfgGetDot11PhyOFDMEntry.MibObject.i4Dot11TIThreshold);

    }
    if (u4FsRadioType == CLI_RADIO_TYPEB)
    {
        WsscfgGetDot11PhyDSSSEntry.MibObject.i4IfIndex = u4IfIndex;
        if (WsscfgGetAllDot11PhyDSSSTable (&WsscfgGetDot11PhyDSSSEntry)
            != OSIX_SUCCESS)
        {
        }
        CliPrintf (CliHandle, "\r\nCurrent Channel           : %d",
                   WsscfgGetDot11PhyDSSSEntry.MibObject.i4Dot11CurrentChannel);
        CliPrintf (CliHandle, "\r\nEnergy Threshold          : %d",
                   WsscfgGetDot11PhyDSSSEntry.MibObject.i4Dot11EDThreshold);
    }

    CliPrintf (CliHandle, "\r\nCurrent Tx Power          : %d dBm",
               RadioIfGetDB.RadioIfGetAllDB.i2CurrentTxPower);

    nmhGetFsWtpCountryString (u4CapwapBaseWtpProfileId, &CountryString);
    CliPrintf (CliHandle, "\r\nCountry Name              : %s",
               CountryString.pu1_OctetList);

    CliPrintf (CliHandle, "\r\nMac Beacon Period         : %d",
               WsscfgGetDot11StationConfigEntry.MibObject.i4Dot11BeaconPeriod);
    CliPrintf (CliHandle, "\r\nDTIM Period               : %d",
               WsscfgGetDot11StationConfigEntry.MibObject.i4Dot11DTIMPeriod);
    CliPrintf (CliHandle, "\r\nFragmentation Threshold   : %d",
               WsscfgGetDot11OperationEntry.
               MibObject.i4Dot11FragmentationThreshold);
    CliPrintf (CliHandle, "\r\nRTS Threshold             : %d",
               WsscfgGetDot11OperationEntry.MibObject.i4Dot11RTSThreshold);
    CliPrintf (CliHandle, "\r\nLong Retry Limit          : %d",
               WsscfgGetDot11OperationEntry.MibObject.i4Dot11LongRetryLimit);
    CliPrintf (CliHandle, "\r\nShort Retry Limit         : %d",
               WsscfgGetDot11OperationEntry.MibObject.i4Dot11ShortRetryLimit);
    CliPrintf (CliHandle, "\r\nMax Transmit MSDULifetime : %d",
               WsscfgGetDot11OperationEntry.
               MibObject.u4Dot11MaxTransmitMSDULifetime);
    CliPrintf (CliHandle, "\r\nMax Receive Lifetime      : %d",
               WsscfgGetDot11OperationEntry.
               MibObject.u4Dot11MaxReceiveLifetime);
    CliPrintf (CliHandle, "\r\nCurrent Tx Power Level    : %d",
               WsscfgGetDot11PhyTxPowerEntry.
               MibObject.i4Dot11CurrentTxPowerLevel);
    CliPrintf (CliHandle, "\r\nQEDCA txop-limit          : %d",
               WsscfgDot11QAPEDCAEntry.MibObject.i4Dot11QAPEDCATableTXOPLimit);
    CliPrintf (CliHandle, "\r\nQEDCA aifs                : %d",
               WsscfgDot11QAPEDCAEntry.MibObject.i4Dot11QAPEDCATableAIFSN);
    CliPrintf (CliHandle, "\r\nQEDCA CWmax               : %d",
               WsscfgDot11QAPEDCAEntry.MibObject.i4Dot11QAPEDCATableCWmax);
    CliPrintf (CliHandle, "\r\nQEDCA CWmin               : %d",
               WsscfgDot11QAPEDCAEntry.MibObject.i4Dot11QAPEDCATableCWmin);
    CliPrintf (CliHandle, "\r\nQos Priority              : %d",
               WsscfgFsStationQosParamsEntry.MibObject.i4FsStaQoSPriority);
    CliPrintf (CliHandle, "\r\nQos dscp                  : %d",
               WsscfgFsStationQosParamsEntry.MibObject.i4FsStaQoSDscp);

    if (WsscfgGetDot11AntennasListEntry.MibObject.
        i4Dot11DiversitySelectionRx == CLI_ANTENNA_DIVERSITY_ENABLE)
    {
        CliPrintf (CliHandle, "\r\nAntenna - Diversity       : Enable");
    }
    else if (WsscfgGetDot11AntennasListEntry.MibObject.
             i4Dot11DiversitySelectionRx == CLI_ANTENNA_DIVERSITY_DISABLE)
    {
        CliPrintf (CliHandle, "\r\nAntenna - Diversity       : Disable");
    }

    if (WsscfgGetFsDot11AntennasListEntry.MibObject.
        i4FsAntennaMode == CLI_ANTENNA_MODE_LEFTA)
    {
        CliPrintf (CliHandle, "\r\nAntenna - Combiner        : sectorA");
    }
    else if (WsscfgGetFsDot11AntennasListEntry.MibObject.
             i4FsAntennaMode == CLI_ANTENNA_MODE_RIGHTA)
    {
        CliPrintf (CliHandle, "\r\nAntenna - Combiner        : sectorB");
    }
    else if (WsscfgGetFsDot11AntennasListEntry.MibObject.
             i4FsAntennaMode == CLI_ANTENNA_MODE_OMNI)
    {
        CliPrintf (CliHandle, "\r\nAntenna - Combiner        : omni");
    }
    else if (WsscfgGetFsDot11AntennasListEntry.MibObject.
             i4FsAntennaMode == CLI_ANTENNA_MODE_MIMO)
    {
        CliPrintf (CliHandle, "\r\nAntenna - Combiner        : mimo");
    }
    CliPrintf (CliHandle, "\r\nAntenna Count             : %d",
               RadioIfGetDB.RadioIfGetAllDB.u1CurrentTxAntenna);

    for (u1antennaid = 1; u1antennaid <= RadioIfGetDB.RadioIfGetAllDB.
         u1CurrentTxAntenna; u1antennaid++)
    {
        WsscfgGetFsDot11AntennasListEntry.MibObject.i4Dot11AntennaListIndex =
            u1antennaid;
        if (WsscfgGetAllFsDot11AntennasListTable
            (&WsscfgGetFsDot11AntennasListEntry) != OSIX_SUCCESS)
        {
        }

        if (WsscfgGetFsDot11AntennasListEntry.MibObject.i4FsAntennaSelection ==
            CLI_ANTENNA_INTERNAL)
        {
            CliPrintf (CliHandle,
                       "\r\nAntenna %d                 : Internal\n",
                       u1antennaid);
        }
        else if (WsscfgGetFsDot11AntennasListEntry.MibObject.
                 i4FsAntennaSelection == CLI_ANTENNA_EXTERNAL)
        {
            CliPrintf (CliHandle,
                       "\r\nAntenna %d                 : External\n",
                       u1antennaid);
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WssCfgShowApWlanConfig 
* Description :
* Input       :  CliHandle 
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowApWlanConfig (tCliHandle CliHandle,
                        UINT4 *pu4FsRadioType,
                        UINT1 *pu1CapwapBaseWtpProfileName, UINT4 *pu4RadioId)
{
    UINT1               u1Count = 0;
    UINT1               au1ModelNumber[256];
    UINT1               u1WlanId = 0;
    UINT4               u4RadioId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4GetRadioType = 0;
    UINT4               u4NoOfRadio = 0;
    UINT4               u4BssIfIndex = 0;
    INT4                i4RadioIfIndex = 0;
    tMacAddr            BssId;
    UINT1               au1SSID[32];
    UINT1               au1MacAddrBssid[32];
    UINT1              *pu1MacAddrBssId = NULL;
    UINT1              *pu1BssId = NULL;
    tSNMP_OCTET_STRING_TYPE MacAddressBssId;
    tSNMP_OCTET_STRING_TYPE ModelNumber;

    MEMSET (BssId, 0, sizeof (tMacAddr));
    MEMSET (au1SSID, 0, sizeof (au1SSID));
    MEMSET (&au1ModelNumber, 0, sizeof (au1ModelNumber));
    MEMSET (&MacAddressBssId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ModelNumber, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1MacAddrBssid, 0, 32);

    pu1BssId = BssId;
    pu1MacAddrBssId = au1MacAddrBssid;

    if (pu1CapwapBaseWtpProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1CapwapBaseWtpProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n No entry found\r\n");
            return OSIX_FAILURE;
        }

        ModelNumber.pu1_OctetList = au1ModelNumber;
        if (nmhGetCapwapBaseWtpProfileWtpModelNumber
            (u4WtpProfileId, &ModelNumber) == SNMP_SUCCESS)
        {
            if (nmhGetFsNoOfRadio (&ModelNumber, &u4NoOfRadio) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\n Radio Type not configured\r\n");
                return CLI_FAILURE;
            }
            if (*pu4RadioId == 0)
            {
                for (u4RadioId = 1; u4RadioId <= u4NoOfRadio; u4RadioId++)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u4RadioId,
                         &i4RadioIfIndex) != SNMP_SUCCESS)
                    {
                        continue;
                    }
                    if (nmhGetFsDot11RadioType
                        (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                    {
                        continue;
                    }
                    if (WSSCFG_RADIO_TYPE_COMP ((INT4) *pu4FsRadioType))
                    {
                        continue;
                    }
                    else
                    {
                        CliPrintf (CliHandle, "\r\nRadio Id      : %d\r\n",
                                   u4RadioId);
                        CliPrintf (CliHandle,
                                   "\r\nWlan Id    BSSID               SSID ");
                        CliPrintf (CliHandle, "\r\n");
                        CliPrintf (CliHandle,
                                   "-------    -----               ---- ");
                        CliPrintf (CliHandle, "\r\n");

                        for (u1WlanId = 1, u1Count = 0;
                             u1WlanId <= CLI_WLAN_BSS_MAX_ID; u1WlanId++)
                        {
                            u4BssIfIndex = 0;
                            WssWlanGetDot11BssIfIndex ((UINT4)
                                                       i4RadioIfIndex,
                                                       u1WlanId, &u4BssIfIndex);
                            if (u4BssIfIndex != 0)
                            {
                                WssWlanGetDot11BssId (u4BssIfIndex, pu1BssId);
                                PrintMacAddress (pu1BssId, pu1MacAddrBssId);
                                MEMSET (au1SSID, 0, sizeof (au1SSID));
                                WssWlanGetDot11SSID (u4BssIfIndex, au1SSID);
                                CliPrintf (CliHandle, "\r\n");
                                CliPrintf (CliHandle, "%-10d", u1WlanId);
                                CliPrintf (CliHandle, "%-20s", au1MacAddrBssid);
                                CliPrintf (CliHandle, "%-32s", au1SSID);
                                CliPrintf (CliHandle, "\r\n");
                                u1Count++;
                            }
                        }
                        if (u1Count == 0)
                        {
                            CliPrintf (CliHandle, "\r\n No entry found\r\n");
                        }
                        CliPrintf (CliHandle,
                                   "-------------------------------------------------------\r\n");
                    }
                }
            }
            else
            {
                if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                    (u4WtpProfileId, *pu4RadioId,
                     &i4RadioIfIndex) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid interface index %d \r\n",
                               *pu4RadioId);
                    return CLI_SUCCESS;
                }
                if (nmhGetFsDot11RadioType
                    (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_SUCCESS;
                }

                if (WSSCFG_RADIO_TYPE_COMP ((INT4) *pu4FsRadioType))
                {
                    CliPrintf (CliHandle,
                               "\r\n No binding entry found for the radio type\r\n");
                    return CLI_SUCCESS;
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r\nWlan Id    BSSID              SSID ");
                    CliPrintf (CliHandle, "\r\n");
                    CliPrintf (CliHandle,
                               "-------    ------             ---- ");
                    CliPrintf (CliHandle, "\r\n");
                    for (u1WlanId = 1; u1WlanId <= CLI_WLAN_BSS_MAX_ID;
                         u1WlanId++)
                    {
                        u4BssIfIndex = 0;
                        WssWlanGetDot11BssIfIndex ((UINT4)
                                                   i4RadioIfIndex,
                                                   u1WlanId, &u4BssIfIndex);
                        if (u4BssIfIndex != 0)
                        {
                            WssWlanGetDot11BssId (u4BssIfIndex, pu1BssId);
                            PrintMacAddress (pu1BssId, au1MacAddrBssid);
                            MEMSET (au1SSID, 0, sizeof (au1SSID));
                            WssWlanGetDot11SSID (u4BssIfIndex, au1SSID);
                            CliPrintf (CliHandle, "\r\n");
                            CliPrintf (CliHandle, "%-10d", u1WlanId);
                            CliPrintf (CliHandle, "%-20s", au1MacAddrBssid);
                            CliPrintf (CliHandle, "%-32s", au1SSID);
                            CliPrintf (CliHandle, "\r\n");
                            u1Count++;
                        }
                    }
                    if (u1Count == 0)
                    {
                        CliPrintf (CliHandle, "\r\n No entry found\r\n");
                    }
                }
            }
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r\n AP Name missing\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WssCfgShowWlanMulticastSummary  
* Description :
* Input       :  CliHandle 
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowWlanMulticastSummary (tCliHandle CliHandle)
{
    UINT2               u2WlanCount = 0;
    UINT4               u4WlanProfileId = 0;
    UINT4               u4NextProfileId = 0;
    INT4                i4OutCome = 0;
    INT4                i4WlanMulticastMode = 0;
    UINT4               u4WlanMulticastLength = 0;
    UINT4               u4WlanMulticastTimer = 0;
    UINT4               u4WlanMulticastTimeout = 0;

    WssCfgGetWlanCount (&u2WlanCount);
    CliPrintf (CliHandle, "\r\nNumber of WLANs : %d\n", u2WlanCount);
    CliPrintf (CliHandle,
               "\r\nWLAN ID   Table Length   Snoop Timer   Snoop Timeout    Mode");
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle,
               "-------   ------------   -----------   -------------    ---- ");
    CliPrintf (CliHandle, "\r\n");

    i4OutCome = nmhGetFirstIndexFsWlanMulticastTable (&u4NextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nNo Entry found\r\n");
    }

    do
    {
        u4WlanProfileId = u4NextProfileId;

        if (u4WlanProfileId != 0)
        {
            CliPrintf (CliHandle, "%7d", u4WlanProfileId);
            if (nmhGetFsWlanMulticastSnoopTableLength (u4WlanProfileId,
                                                       &u4WlanMulticastLength)
                == SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "%15d", u4WlanMulticastLength);
            }
            if (nmhGetFsWlanMulticastSnoopTimer (u4WlanProfileId,
                                                 &u4WlanMulticastTimer) ==
                SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "%14d", u4WlanMulticastTimer);
            }
            if (nmhGetFsWlanMulticastSnoopTimeout (u4WlanProfileId,
                                                   &u4WlanMulticastTimeout) ==
                SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "%16d", u4WlanMulticastTimeout);
            }
            if (nmhGetFsWlanMulticastMode
                (u4WlanProfileId, &i4WlanMulticastMode) == SNMP_SUCCESS)
            {
                if (i4WlanMulticastMode == CLI_WLAN_MULTICAST_MODE_DISABLE)
                    CliPrintf (CliHandle, "%12s", "DISABLED");
                else if (i4WlanMulticastMode ==
                         CLI_WLAN_MULTICAST_MODE_TUNNELING)
                    CliPrintf (CliHandle, "%13s", "TUNNELING");
                else if (i4WlanMulticastMode ==
                         CLI_WLAN_MULTICAST_MODE_TRANSLATING)
                    CliPrintf (CliHandle, "%15s", "TRANSLATING");
            }
            CliPrintf (CliHandle, "\r\n");
        }
    }
    while (nmhGetNextIndexFsWlanMulticastTable
           (u4WlanProfileId, &u4NextProfileId) == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WssCfgShowWlanSummary  
* Description :
* Input       :  CliHandle 
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowWlanSummary (tCliHandle CliHandle)
{
    UINT2               u2WlanCount = 0;
    UINT4               u4WlanProfileId = 0;
    UINT4               u4NextProfileId = 0;
    INT4                i4OutCome = 0;
    INT4                i4WlanProfileIfIndex = 0;
    INT4                i4WlanAdminStatus = 0;
    INT4                i4TrapStatus = 0;
    UINT1               au1Dot11DesiredSSID[CLI_MAX_SSID_LEN];
    UINT1              *pu1SSID = NULL;

    MEMSET (au1Dot11DesiredSSID, 0, CLI_MAX_SSID_LEN);

    WssCfgGetWlanCount (&u2WlanCount);
    CliPrintf (CliHandle, "\r\nNumber of WLANs : %d\n", u2WlanCount);
    if (nmhGetFsWlanStationTrapStatus (&i4TrapStatus) == SNMP_SUCCESS)
    {
        if (i4TrapStatus == CLI_WLAN_TRAP_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nWlan Trap Status : Enabled\r\n\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nWlan Trap Status : Disabled\r\n\r\n");
        }
    }

    CliPrintf (CliHandle,
               "\r\nWLAN ID      WLAN Profile Name / SSID        Status ");
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle,
               "-------      ------------------------        ------  ");
    CliPrintf (CliHandle, "\r\n");

    i4OutCome = nmhGetFirstIndexCapwapDot11WlanTable (&u4NextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nNo Entry found\r\n");
    }

    do
    {
        u4WlanProfileId = u4NextProfileId;

        if (u4WlanProfileId != 0)
        {
            if (nmhGetCapwapDot11WlanProfileIfIndex
                (u4WlanProfileId, &i4WlanProfileIfIndex) != SNMP_SUCCESS)
            {
                continue;        /* Continue to next check entry */
            }
            if (i4WlanProfileIfIndex == 0)
            {
                continue;
            }
            if (nmhGetIfMainAdminStatus
                (i4WlanProfileIfIndex, &i4WlanAdminStatus) != SNMP_SUCCESS)
            {
                continue;
            }
            /* Clearing the old SSID value */
            MEMSET (au1Dot11DesiredSSID, 0, CLI_MAX_SSID_LEN);
            WssCfGetDot11DesiredSSID ((UINT4) i4WlanProfileIfIndex,
                                      au1Dot11DesiredSSID);
            pu1SSID = au1Dot11DesiredSSID;

            CliPrintf (CliHandle, "\r\n");

            CliPrintf (CliHandle, "%-12d", u4WlanProfileId);

            CliPrintf (CliHandle, "%-35s", pu1SSID);
            if (i4WlanAdminStatus == CLI_WLAN_ADMIN_ENABLE)
            {
                CliPrintf (CliHandle, "%-22s", "Enabled");
            }
            else
            {
                CliPrintf (CliHandle, "%-22s", "Disabled");
            }
            CliPrintf (CliHandle, "\r\n");
        }
    }
    while (nmhGetNextIndexCapwapDot11WlanTable
           (u4WlanProfileId, &u4NextProfileId) == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WssCfgShowclientStats
* Description :    Displays the statistics of the clients connected
* Input       :  CliHandle
*         staMacAddr
*         u1AllEntries
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
WssCfgShowClientStats (tCliHandle CliHandle, tMacAddr staMacAddr,
                       UINT1 u1AllEntries)
{
    UINT4               u4Index = 0;
    tMacAddr            zeroAddr;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;

    MEMSET (zeroAddr, 0, sizeof (tMacAddr));
    if (u1AllEntries == 1)
    {
        WssStaShowClient ();
        if (gu4ClientWalkIndex == 0)
        {
            CliPrintf (CliHandle, "\r\n No entry found\r\n");
            return CLI_SUCCESS;
        }
        for (u4Index = 0; u4Index < gu4ClientWalkIndex; u4Index++)
        {
            pWssStaWepProcessDB =
                WssStaProcessEntryGet (gaWssClientSummary[u4Index].staMacAddr);
            if (pWssStaWepProcessDB != NULL)
            {
                WssCfgClientStats (CliHandle,
                                   gaWssClientSummary[u4Index].staMacAddr);
            }
        }
    }
    else
    {
        if (MEMCMP (staMacAddr, zeroAddr, CFA_ENET_ADDR_LEN) == 0)
        {
            CliPrintf (CliHandle, "\r\n Invalid MAC address.\r\n");
            return CLI_FAILURE;
        }
        WssCfgClientStats (CliHandle, staMacAddr);
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WssCfgClientStats
* Description :    Displays the statistics of the clients connected
* Input       :  staMacAddr
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
WssCfgClientStats (tCliHandle CliHandle, tMacAddr staMacAddr)
{
    UINT4               u4IdleTimeOut = 0;
    UINT4               u4BytesRcvdCount = 0;
    UINT4               u4BytesSentCount = 0;
    UINT4               u4CurrentTime = 0;
    UINT1              *pu1TempMac = NULL;
    CHR1               *pu1LocalIpString = NULL;
    UINT1               au1TempMac[20];
    UINT4               u4IpAddr;
    UINT4               u4AssocTime = 0;
    UINT4               u4Sec = 0;
    UINT4               u4Min = 0;
    UINT4               u4Hrs = 0;
    UINT4               u4Days = 0;
    INT4                i4AuthState = 0;
    UINT1               au1DateString[100];
    UINT1               au1Dot11DesiredSSID[CLI_MAX_SSID_LEN];
    UINT1               au1ConnTime[256];
    tSNMP_OCTET_STRING_TYPE IpAddress;
    tSNMP_OCTET_STRING_TYPE DesiredSSID;
    tSNMP_OCTET_STRING_TYPE ConnTime;

    MEMSET (au1ConnTime, 0, 256);
    MEMSET (au1TempMac, 0, 20);
    MEMSET (&IpAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&DesiredSSID, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ConnTime, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1DateString, 0, 100);
    MEMSET (au1Dot11DesiredSSID, 0, CLI_MAX_SSID_LEN);
    IpAddress.pu1_OctetList = (UINT1 *) &u4IpAddr;
    DesiredSSID.pu1_OctetList = au1Dot11DesiredSSID;
    ConnTime.pu1_OctetList = au1ConnTime;
    pu1TempMac = au1TempMac;

    if (nmhGetFsWlanClientStatsConnTime (staMacAddr, &ConnTime) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n No entry found\r\n");
        return CLI_FAILURE;
    }
    else
    {
        PrintMacAddress ((UINT1 *) staMacAddr, pu1TempMac);
        CliPrintf (CliHandle, "\r\nStation MAC Address        : %s",
                   pu1TempMac);
        CliPrintf (CliHandle, "\r\nConnection Time            : %s",
                   au1ConnTime);
    }

    if (nmhGetFsWlanClientStatsAssocTime
        (staMacAddr, &u4AssocTime) == SNMP_SUCCESS)
    {
        u4CurrentTime = OsixGetSysUpTime ();
        u4CurrentTime -= u4AssocTime;

        u4Sec = (UINT4) (u4CurrentTime % SECS_IN_MINUTE);
        u4Min = (UINT4) ((u4CurrentTime / SECS_IN_MINUTE) % MINUTES_IN_HOUR);
        u4Hrs = (UINT4) ((u4CurrentTime / SECS_IN_HOUR) % HOURS_IN_DAY);
        u4Days = (UINT4) (u4CurrentTime / SECS_IN_DAY);

        SPRINTF ((CHR1 *) au1DateString, "%d Days %d Hrs, %d Mins, %d Secs",
                 u4Days, u4Hrs, u4Min, u4Sec);
        CliPrintf (CliHandle, "\r\nAssociation Time           : %s",
                   au1DateString);
    }

    if (nmhGetFsWlanClientStatsAuthState
        (staMacAddr, &i4AuthState) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nAuth State                 : %d",
                   i4AuthState);
    }

    if (nmhGetFsWlanClientStatsSSID (staMacAddr, &DesiredSSID) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nSSID Name                  : %s",
                   au1Dot11DesiredSSID);
    }

    if (nmhGetFsWlanClientStatsIdleTimeout
        (staMacAddr, &u4IdleTimeOut) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nIdle TimeOut               : %d sec",
                   u4IdleTimeOut);
    }

    if (nmhGetFsWlanClientStatsBytesReceivedCount
        (staMacAddr, &u4BytesRcvdCount) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nBytes Rcvd Count           : %u",
                   u4BytesRcvdCount);
    }

    if (nmhGetFsWlanClientStatsBytesSentCount
        (staMacAddr, &u4BytesSentCount) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nBytes Sent Count           : %u",
                   u4BytesSentCount);
    }

    if (nmhGetFsWlanClientStatsIpAddress
        (staMacAddr, &IpAddress) == SNMP_SUCCESS)
    {
        CLI_CONVERT_IPADDR_TO_STR (pu1LocalIpString, u4IpAddr);
        CliPrintf (CliHandle, "\r\nIp Address                 : %s",
                   pu1LocalIpString);
    }

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WssCfgShowclientSummary
* Description : Displays the clients connected
* Input       :  CliHandle
*
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
WssCfgShowClientSummary (tCliHandle CliHandle)
{

    UINT4               u4Index = 0;
    tWssWlanDB         *pwssWlanDB = NULL;
    tRadioIfGetDB       radioIfgetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4RadioType = 0;
    UINT1               au1MacAddr[21];
    UINT4               u4WlanIfIndex = 0;
    UINT1               au1Dot11DesiredSSID[CLI_MAX_SSID_LEN];
    CHR1                ac1TimeStr[MAX_TIME_LEN] = "\0";
    UINT4               u4StaCount = 0;

    WssUtilGetTimeStr (ac1TimeStr);
    CliPrintf (CliHandle, "Time : %s\n ", ac1TimeStr);
    if (nmhGetFsDot11StationCount ((INT4 *) &u4StaCount) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nAssoc client count          : %d",
                   u4StaCount);
        CliPrintf (CliHandle, "\r\n");
    }
    CliPrintf (CliHandle,
               "\r\nMac Address        Radio ID   AP Name     SSID        Auth     Protocol ");
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle,
               "------------       --------   -------     ----        ----     -------- ");
    CliPrintf (CliHandle, "\r\n");

    for (u4Index = 0; u4Index < gu4ClientWalkIndex; u4Index++)
    {
        WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
            MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
        pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
        if (pwssWlanDB == NULL)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));
        if (gaWssClientSummary[u4Index].stationState == AUTH_ASSOC)
        {
            MEMSET (au1Dot11DesiredSSID, 0, CLI_MAX_SSID_LEN);
            MEMSET (&radioIfgetDB, 0, sizeof (tRadioIfGetDB));
            /*Coverity Change Begins */
            MEMSET (&au1MacAddr, 0, sizeof (au1MacAddr));
            /*Coverity Change Ends */
            PrintMacAddress ((UINT1 *) &
                             (gaWssClientSummary[u4Index].staMacAddr),
                             au1MacAddr);
            CliPrintf (CliHandle, "\r\n%-24s", au1MacAddr);

            pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex =
                gaWssClientSummary[u4Index].u4BssIfIndex;
            pwssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
            pwssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
            pwssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;

            if (WssIfProcessWssWlanDBMsg
                (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pwssWlanDB))
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "WssCfgShowClientSummary: "
                             "Failed to retrieve data from WssWlanDB\r\n");
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                return OSIX_FAILURE;

            }
            u4WlanIfIndex = pwssWlanDB->WssWlanAttributeDB.u4WlanIfIndex;
            WssCfGetDot11DesiredSSID (u4WlanIfIndex, au1Dot11DesiredSSID);
            radioIfgetDB.RadioIfGetAllDB.pu1AntennaSelection =
                UtlShMemAllocAntennaSelectionBuf ();

            if (radioIfgetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                return CLI_FAILURE;
            }

            radioIfgetDB.RadioIfGetAllDB.u4RadioIfIndex =
                pwssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
            radioIfgetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
            radioIfgetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            radioIfgetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &radioIfgetDB))
            {
                UtlShMemFreeAntennaSelectionBuf (radioIfgetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                return OSIX_FAILURE;
            }
            u4RadioType = radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType;
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                radioIfgetDB.RadioIfGetAllDB.u2WtpInternalId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
                != OSIX_SUCCESS)
            {
                UtlShMemFreeAntennaSelectionBuf (radioIfgetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
                return OSIX_FAILURE;
            }
            CliPrintf (CliHandle, "%-8d",
                       radioIfgetDB.RadioIfGetAllDB.u1RadioId);
            CliPrintf (CliHandle, "%-12s",
                       pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
            CliPrintf (CliHandle, "%-12s", au1Dot11DesiredSSID);
            CliPrintf (CliHandle, "%-8s", "Yes");
            if (u4RadioType == CLI_RADIO_TYPEA)
            {
                CliPrintf (CliHandle, "%-17s", "802.11a");
            }
            else if (u4RadioType == CLI_RADIO_TYPEB)
            {
                CliPrintf (CliHandle, "%-17s", "802.11b");
            }
            else if (u4RadioType == CLI_RADIO_TYPEG)
            {
                CliPrintf (CliHandle, "%-17s", "802.11g");
            }
            else if (u4RadioType == CLI_RADIO_TYPEBG)
            {
                CliPrintf (CliHandle, "%-17s", "802.11bg");
            }
            else if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "%-17s", "802.11an");
            }
            else if (u4RadioType == CLI_RADIO_TYPEBGN)
            {
                CliPrintf (CliHandle, "%-17s", "802.11bgn");
            }
            else if (u4RadioType == CLI_RADIO_TYPEAC)
            {
                CliPrintf (CliHandle, "%-17s", "802.11ac");
            }
            CliPrintf (CliHandle, "\r\n");
            UtlShMemFreeAntennaSelectionBuf (radioIfgetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
        }
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WssCfgShowWebAuthConfig
* Description : 
* Input       :  CliHandle
*
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
WssCfgShowWebAuthConfig (tCliHandle CliHandle)
{
#ifdef WSS_DEBUG_WANTED
    INT4                i4FsSecurityWebAuthType = 0;
    INT4                i4FsSecurityWebAuthDisplay = 0;
    INT4                i4FsSecurityWebAuthColor = 0;
    INT4                i4FsSecurityWebAddr = 0;
    tSNMP_OCTET_STRING_TYPE WebAuthUrl;
    tSNMP_OCTET_STRING_TYPE WebAuthWebTitle;
    tSNMP_OCTET_STRING_TYPE WebAuthWebMessage;
    tSNMP_OCTET_STRING_TYPE WebAuthWebSuccMessage;
    tSNMP_OCTET_STRING_TYPE WebAuthWebFailMessage;
    tSNMP_OCTET_STRING_TYPE WebAuthWebButtonText;
    tSNMP_OCTET_STRING_TYPE WebAuthLoadBalInfo;
    tSNMP_OCTET_STRING_TYPE WebAuthLogoFileName;

    UINT1               au1FsSecurityWebAuthUrl[256];
    UINT1               au1WebAuthType[256];
    UINT1               au1WebAuthColor[256];
    UINT1               au1WebAuthLang[256];
    UINT1               au1FsSecurityWebAuthWebTitle[256];
    UINT1               au1FsSecurityWebAuthWebMessage[256];
    UINT1               au1FsSecurityWebAuthWebSuccMessage[256];
    UINT1               au1FsSecurityWebAuthWebFailMessage[256];
    UINT1               au1FsSecurityWebAuthWebButtonText[256];
    UINT1               au1FsSecurityWebAuthLoadBalInfo[256];
    UINT1               au1FsSecurityWebAuthLogoFileName[256];

    MEMSET (&au1FsSecurityWebAuthUrl, 0, sizeof (au1FsSecurityWebAuthUrl));
    MEMSET (&au1WebAuthType, 0, sizeof (au1WebAuthType));
    MEMSET (&au1WebAuthColor, 0, sizeof (au1WebAuthColor));
    MEMSET (&au1WebAuthLang, 0, sizeof (au1WebAuthLang));
    MEMSET (&au1FsSecurityWebAuthWebTitle, 0,
            sizeof (au1FsSecurityWebAuthWebTitle));
    MEMSET (&au1FsSecurityWebAuthWebMessage, 0,
            sizeof (au1FsSecurityWebAuthWebMessage));
    MEMSET (&au1FsSecurityWebAuthWebSuccMessage, 0,
            sizeof (au1FsSecurityWebAuthWebSuccMessage));
    MEMSET (&au1FsSecurityWebAuthWebFailMessage, 0,
            sizeof (au1FsSecurityWebAuthWebFailMessage));
    MEMSET (&au1FsSecurityWebAuthWebButtonText, 0,
            sizeof (au1FsSecurityWebAuthWebButtonText));
    MEMSET (&au1FsSecurityWebAuthLoadBalInfo, 0,
            sizeof (au1FsSecurityWebAuthLoadBalInfo));
    MEMSET (&au1FsSecurityWebAuthLogoFileName, 0,
            sizeof (au1FsSecurityWebAuthLogoFileName));

    MEMSET (&WebAuthUrl, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WebAuthWebTitle, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WebAuthWebMessage, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WebAuthWebSuccMessage, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WebAuthWebFailMessage, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WebAuthWebButtonText, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WebAuthLoadBalInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WebAuthLogoFileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
#endif

    INT4                i4IfIndex = 0;
    UINT1               au1WebAuthUrl[120];
    INT4                i4WebAuthProfileId = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4WebAuthMethod = 0;
    tWssWlanDB          wssWlanDB;
    tSNMP_OCTET_STRING_TYPE WebAuthUrl;

    MEMSET (&au1WebAuthUrl, 0, sizeof (au1WebAuthUrl));
    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&WebAuthUrl, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    WebAuthUrl.pu1_OctetList = au1WebAuthUrl;

    if (nmhGetFirstIndexFsDot11ExternalWebAuthProfileTable (&i4NextIfIndex) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    do
    {
        wssWlanDB.WssWlanAttributeDB.u4WlanIfIndex = (UINT4) i4NextIfIndex;
        wssWlanDB.WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;

        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &wssWlanDB) !=
            OSIX_SUCCESS)
        {
            return CLI_FAILURE;
        }
        i4WebAuthProfileId =
            (INT4) wssWlanDB.WssWlanAttributeDB.u2WlanProfileId;

        MEMSET (&au1WebAuthUrl, 0, sizeof (au1WebAuthUrl));
        if (nmhGetFsDot11ExternalWebAuthMethod (i4NextIfIndex, &i4WebAuthMethod)
            != SNMP_FAILURE)
        {
            if ((i4WebAuthMethod == WEB_AUTH_TYPE_INTERNAL) ||
                (i4WebAuthMethod == WEB_AUTH_TYPE_EXTERNAL))
            {
                CliPrintf (CliHandle, "WebAuth Type : %s WlanId : %d\n",
                           (i4WebAuthMethod == 1) ? "internal" : "external",
                           i4WebAuthProfileId);
                if (i4WebAuthMethod == WEB_AUTH_TYPE_EXTERNAL)
                {
                    if (nmhGetFsDot11ExternalWebAuthUrl
                        (i4NextIfIndex, &WebAuthUrl) != SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle, "WebAuth URL : %s WlanId : %d\n",
                                   WebAuthUrl.pu1_OctetList,
                                   i4WebAuthProfileId);
                    }
                }
            }
        }

        i4IfIndex = i4NextIfIndex;
    }
    while (nmhGetNextIndexFsDot11ExternalWebAuthProfileTable
           (i4IfIndex, &i4NextIfIndex) == SNMP_SUCCESS);

#ifdef WSS_DEBUG_WANTED
    if (nmhGetFsSecurityWebAuthType (&i4FsSecurityWebAuthType) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if ((i4FsSecurityWebAuthType == 0) ||
        (i4FsSecurityWebAuthType == WEB_AUTH_TYPE_INTERNAL))
    {
        STRCPY (au1WebAuthType, "Internal");
    }
    if (i4FsSecurityWebAuthType == WEB_AUTH_TYPE_CUSTOMIZE)
    {
        STRCPY (au1WebAuthType, "Internal Customizable");
    }
    if (i4FsSecurityWebAuthType == WEB_AUTH_TYPE_EXTERNAL)
    {
        STRCPY (au1WebAuthType, "External");
        WebAuthUrl.pu1_OctetList = au1FsSecurityWebAuthUrl;

        if (nmhGetFsSecurityWebAuthUrl (&WebAuthUrl) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        if (nmhGetFsSecurityWebAddr (&i4FsSecurityWebAddr) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        CliPrintf (CliHandle, "\r\nWebAuth Type: %s\r\n", au1WebAuthType);
        CliPrintf (CliHandle, "\r\nWebAuth URl: %s\r\n",
                   WebAuthUrl.pu1_OctetList);
        CliPrintf (CliHandle, "\r\nWebAddress: %d\r\n", i4FsSecurityWebAddr);
    }
    else
    {
        if (nmhGetFsSecurityWebAuthDisplayLang
            (&i4FsSecurityWebAuthDisplay) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        if (i4FsSecurityWebAuthDisplay == CLI_WEBAUTH_LANG_ENGLISH)
            STRCPY (au1WebAuthLang, "English");
        else if (i4FsSecurityWebAuthDisplay == CLI_WEBAUTH_LANG_CHINESE)
            STRCPY (au1WebAuthLang, "Chinese");
        else if (i4FsSecurityWebAuthDisplay == CLI_WEBAUTH_LANG_JAPANESE)
            STRCPY (au1WebAuthLang, "Japanese");
        else if (i4FsSecurityWebAuthDisplay == CLI_WEBAUTH_LANG_FRENCH)
            STRCPY (au1WebAuthLang, "French");
        else if (i4FsSecurityWebAuthDisplay == CLI_WEBAUTH_LANG_RUSSIAN)
            STRCPY (au1WebAuthLang, "Russian");

        if (nmhGetFsSecurityWebAuthColor (&i4FsSecurityWebAuthColor) !=
            SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        if (i4FsSecurityWebAuthColor == CLI_WEBAUTH_COLOR_ORANGE)
            STRCPY (au1WebAuthColor, "Orange");
        else if (i4FsSecurityWebAuthColor == CLI_WEBAUTH_COLOR_GREY)
            STRCPY (au1WebAuthColor, "Grey");
        else if (i4FsSecurityWebAuthColor == CLI_WEBAUTH_COLOR_YELLOW)
            STRCPY (au1WebAuthColor, "Yellow");

        WebAuthWebTitle.pu1_OctetList = au1FsSecurityWebAuthWebTitle;

        if (nmhGetFsSecurityWebAuthWebTitle (&WebAuthWebTitle) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        WebAuthWebMessage.pu1_OctetList = au1FsSecurityWebAuthWebMessage;

        if (nmhGetFsSecurityWebAuthWebMessage (&WebAuthWebMessage) !=
            SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        WebAuthWebSuccMessage.pu1_OctetList =
            au1FsSecurityWebAuthWebSuccMessage;

        if (nmhGetFsSecurityWebAuthWebSuccMessage
            (&WebAuthWebSuccMessage) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        WebAuthWebFailMessage.pu1_OctetList =
            au1FsSecurityWebAuthWebFailMessage;

        if (nmhGetFsSecurityWebAuthWebFailMessage
            (&WebAuthWebFailMessage) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        WebAuthWebButtonText.pu1_OctetList = au1FsSecurityWebAuthWebButtonText;

        if (nmhGetFsSecurityWebAuthWebButtonText
            (&WebAuthWebButtonText) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        WebAuthLoadBalInfo.pu1_OctetList = au1FsSecurityWebAuthLoadBalInfo;

        if (nmhGetFsSecurityWebAuthWebLoadBalInfo (&WebAuthLoadBalInfo)
            != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        WebAuthLogoFileName.pu1_OctetList = au1FsSecurityWebAuthLogoFileName;

        if (nmhGetFsSecurityWebAuthWebLogoFileName
            (&WebAuthLogoFileName) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        CliPrintf (CliHandle, "\r\nWebAuth Type: %s\r\n", au1WebAuthType);
        CliPrintf (CliHandle, "\r\nWebTitle: %s\r\n",
                   WebAuthWebTitle.pu1_OctetList);
        CliPrintf (CliHandle, "\r\nWebAuth WebMessage: %s\r\n",
                   WebAuthWebMessage.pu1_OctetList);
        CliPrintf (CliHandle, "\r\nWebAuth Logo filename: %s\r\n",
                   WebAuthLogoFileName.pu1_OctetList);
        CliPrintf (CliHandle, "\r\nWebAuth Success Message: %s\r\n",
                   WebAuthWebSuccMessage.pu1_OctetList);
        CliPrintf (CliHandle, "\r\nWebAuth Failure Message: %s\r\n",
                   WebAuthWebFailMessage.pu1_OctetList);
        CliPrintf (CliHandle, "\r\nWebAuth Button Text: %s\r\n",
                   WebAuthWebButtonText.pu1_OctetList);
        CliPrintf (CliHandle, "\r\nWebAuth Load Bal Info: %s\r\n",
                   WebAuthLoadBalInfo.pu1_OctetList);
        CliPrintf (CliHandle, "\r\nWebAuth Display Lang: %s\r\n",
                   au1WebAuthLang);
        CliPrintf (CliHandle, "\r\nWebAuth Color:%s\r\n", au1WebAuthColor);
    }
#endif

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WssCfgShowNetUserSummary 
* Description :
* Input       :  CliHandle 
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowNetUserSummary (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE NextWebAuthUName;
    tSNMP_OCTET_STRING_TYPE WebAuthUName;
    tSNMP_OCTET_STRING_TYPE WebAuthUserEmailId;

    UINT1               au1FsSecurityWebAuthUName[WSS_STA_WEBUSER_LEN_MAX];
    UINT1               au1NextFsSecurityWebAuthUName[WSS_STA_WEBUSER_LEN_MAX];
    UINT1               au1FsSecurityWebAuthUserEmailId[256];
    INT4                i4RetValFsSecurityWlanProfileId = 0;
    INT4                i4RetValFsSecurityWebAuthUserLifetime = 0;

    MEMSET (&au1FsSecurityWebAuthUName, 0, sizeof (au1FsSecurityWebAuthUName));
    MEMSET (&au1NextFsSecurityWebAuthUName, 0,
            sizeof (au1NextFsSecurityWebAuthUName));
    MEMSET (&au1FsSecurityWebAuthUserEmailId, 0,
            sizeof (au1FsSecurityWebAuthUserEmailId));

    MEMSET (&WebAuthUName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextWebAuthUName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WebAuthUserEmailId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    WebAuthUName.pu1_OctetList = au1FsSecurityWebAuthUName;
    NextWebAuthUName.pu1_OctetList = au1NextFsSecurityWebAuthUName;
    WebAuthUserEmailId.pu1_OctetList = au1FsSecurityWebAuthUserEmailId;

    if (nmhGetFirstIndexFsSecurityWebAuthGuestInfoTable
        (&NextWebAuthUName) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\n\r\n\rEntry does not exist \n\r\n\r");
        return OSIX_FAILURE;
    }
    CliPrintf (CliHandle, "\r\nName          WLANID     Lifetime\n\r");
    do
    {
        MEMSET (au1FsSecurityWebAuthUName, 0,
                STRLEN (au1FsSecurityWebAuthUName));

        MEMCPY (au1FsSecurityWebAuthUName, au1NextFsSecurityWebAuthUName,
                STRLEN (au1NextFsSecurityWebAuthUName));
        WebAuthUName.i4_Length = STRLEN (au1FsSecurityWebAuthUName);

        MEMSET (au1NextFsSecurityWebAuthUName, 0,
                STRLEN (au1NextFsSecurityWebAuthUName));
        if (nmhGetFsSecurityWlanProfileId
            (&WebAuthUName, &i4RetValFsSecurityWlanProfileId) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\n\r\n\rUnable to get WlanProfile Id \n\r\n\r");
            return OSIX_FAILURE;
        }
        if (nmhGetFsSecurityWebAuthUserLifetime
            (&WebAuthUName,
             &i4RetValFsSecurityWebAuthUserLifetime) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\n\r\n\rUnable to get Lifetime\n\r\n\r");
            return OSIX_FAILURE;
        }
        CliPrintf (CliHandle, "\r\n%-15s%-13d%d\n\r",
                   WebAuthUName.pu1_OctetList,
                   i4RetValFsSecurityWlanProfileId,
                   i4RetValFsSecurityWebAuthUserLifetime);
    }
    while ((nmhGetNextIndexFsSecurityWebAuthGuestInfoTable
            (&WebAuthUName, &NextWebAuthUName)));

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WssCfgShowWlanMulticastConfig  
* Description :
* Input       :  CliHandle,WLAN profileId 
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowWlanMulticastConfig (tCliHandle CliHandle, UINT4 u4WlanProfileId)
{
    INT4                i4WlanMulticastMode = 0;
    UINT4               u4WlanMulticastLength = 0;
    UINT4               u4WlanMulticastTimer = 0;
    UINT4               u4WlanMulticastTimeout = 0;
    CliPrintf (CliHandle, "\n\r\n\rWLAN Identifier              : %d",
               u4WlanProfileId);
    if (nmhGetFsWlanMulticastSnoopTableLength (u4WlanProfileId,
                                               &u4WlanMulticastLength) ==
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nWLAN Multicast Table length  : %d",
                   u4WlanMulticastLength);
    }
    if (nmhGetFsWlanMulticastSnoopTimer (u4WlanProfileId,
                                         &u4WlanMulticastTimer) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nWLAN Multicast Snoop Timer   : %d",
                   u4WlanMulticastTimer);
    }
    if (nmhGetFsWlanMulticastSnoopTimeout (u4WlanProfileId,
                                           &u4WlanMulticastTimeout) ==
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nWLAN Multicast Snoop Timeout : %d",
                   u4WlanMulticastTimeout);
    }
    if (nmhGetFsWlanMulticastMode (u4WlanProfileId, &i4WlanMulticastMode)
        == SNMP_SUCCESS)
    {
        if (i4WlanMulticastMode == CLI_WLAN_MULTICAST_MODE_DISABLE)
            CliPrintf (CliHandle,
                       "\r\nWLAN Multicast Mode          : DISABLED");
        else if (i4WlanMulticastMode == CLI_WLAN_MULTICAST_MODE_TUNNELING)
            CliPrintf (CliHandle,
                       "\r\nWLAN Multicast Mode          : TUNNELING");
        else if (i4WlanMulticastMode == CLI_WLAN_MULTICAST_MODE_TRANSLATING)
            CliPrintf (CliHandle,
                       "\r\nWLAN Multicast Mode          : TRANSLATING");
    }
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WssCfgShowWlanConfig  
* Description :
* Input       :  CliHandle 
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowWlanConfig (tCliHandle CliHandle, UINT4 u4WlanProfileId)
{
    INT4                i4WlanProfileIfIndex = 0;
    UINT2               u2WlanInternalId = 0;
    INT4                i4SetValFsDot11WlanLoginAuthentication = 0;
    INT4                i4WlanMacType = 3, i4WepKeyType = 0, i4WepKeyLength =
        0, i4WepKeyIndex = 0;
    INT4                i4WlanAdminStatus = 0, i4Authentication = 0, i4WebAuth =
        0;
    UINT1               au1Dot11DesiredSSID[CLI_MAX_SSID_LEN];
    UINT4               u4HideSsid = 0;
    UINT1              *pu1SSID = NULL;
    UINT1               au1TunnelMode[CLI_MAX_SSID_LEN];
    tSNMP_OCTET_STRING_TYPE TunnelMode;
    tSNMP_OCTET_STRING_TYPE WebAuthRedirectFile;
    UINT1               au1RedirectFile[CLI_WEB_AUTH_FILE_NAME];
    tWssWlanDB         *pwssWlanDB = NULL;
    UINT1               au1WepKey[WSS_WLAN_KEY_LENGTH];
    tWsscfgFsDot11WlanAuthenticationProfileEntry
        wsscfgFsDot11WlanAuthenticationProfileEntry;
    UINT4               u4MaxClientCount = 0;
    UINT4               u4StaCount = 0;

    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanDB == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (au1TunnelMode, 0, CLI_MAX_SSID_LEN);
    MEMSET (&TunnelMode, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1RedirectFile, 0, CLI_WEB_AUTH_FILE_NAME);
    MEMSET (&WebAuthRedirectFile, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&wsscfgFsDot11WlanAuthenticationProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanAuthenticationProfileEntry));

    MEMSET (au1Dot11DesiredSSID, 0, CLI_MAX_SSID_LEN);
    MEMSET (au1WepKey, 0, WSS_WLAN_KEY_LENGTH);

    if (WssCfgGetWlanInternlId (u4WlanProfileId, &u2WlanInternalId) !=
        OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\n\r\n\rEntry does not exist \n\r\n\r");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_SUCCESS;
    }
    if (u2WlanInternalId == 0)
    {
        CliPrintf (CliHandle, "\n\r\n\rEntry not found\n\r");
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_SUCCESS;
    }
    else
    {
        if (nmhGetCapwapDot11WlanMacType
            (u4WlanProfileId, &i4WlanMacType) != SNMP_SUCCESS)
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }

        if (i4WlanMacType > 1)
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }

        TunnelMode.pu1_OctetList = au1TunnelMode;
        if (nmhGetCapwapDot11WlanTunnelMode
            (u4WlanProfileId, &TunnelMode) != SNMP_SUCCESS)
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }

        if (nmhGetCapwapDot11WlanProfileIfIndex
            (u4WlanProfileId, &i4WlanProfileIfIndex) != SNMP_SUCCESS)
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }

        if (i4WlanProfileIfIndex == 0)
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }

        WssCfGetDot11DesiredSSID ((UINT4) i4WlanProfileIfIndex,
                                  au1Dot11DesiredSSID);
        if (nmhGetIfMainAdminStatus
            (i4WlanProfileIfIndex, &i4WlanAdminStatus) != SNMP_SUCCESS)
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }

        CliPrintf (CliHandle, "\r\nWLAN Identifier             : %d",
                   u4WlanProfileId);

        u4StaCount = 0;
        if (nmhGetFsWlanSSIDStatsAssocClientCount (u4WlanProfileId,
                                                   &u4StaCount) == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nAssoc client count          : %d",
                       u4StaCount);
        }

        u4MaxClientCount = 0;
        if (nmhGetFsWlanSSIDStatsMaxClientCount (u4WlanProfileId,
                                                 &u4MaxClientCount)
            == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nMax Client count            : %d",
                       u4MaxClientCount);
        }

        if (i4WlanMacType == CLI_WTP_MAC_TYPE_SPLIT)
        {
            CliPrintf (CliHandle, "\r\nMac Type                    : Split");
        }
        else if (i4WlanMacType == CLI_WTP_MAC_TYPE_LOCAL)
        {
            CliPrintf (CliHandle, "\r\nMac Type                    : Local");
        }

        if (TunnelMode.pu1_OctetList[0] == CLI_WLAN_TUNNEL_MODE_BRIDGE)
        {
            CliPrintf (CliHandle, "\r\nTunnel Mode                 : 802.3");
        }
        else if (TunnelMode.pu1_OctetList[0] == CLI_WLAN_TUNNEL_MODE_DOT3)
        {
            CliPrintf (CliHandle, "\r\nTunnel Mode                 : 802.3");
        }
        else if (TunnelMode.pu1_OctetList[0] == CLI_WLAN_TUNNEL_MODE_NATIVE)
        {
            CliPrintf (CliHandle, "\r\nTunnel Mode                 : Native");
        }

        pu1SSID = au1Dot11DesiredSSID;
        CliPrintf (CliHandle, "\r\nSSID Name                   : %s", pu1SSID);

        if (i4WlanAdminStatus == CLI_WLAN_ADMIN_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nAdmin State                 : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nAdmin State                 : Disabled");
        }

        nmhGetFsDot11SupressSSID (i4WlanProfileIfIndex, (INT4 *) &u4HideSsid);
        if (u4HideSsid == CLI_WLAN_HIDE_SSID_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nSupress SSID                : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nStatus                      : Disabled");
        }

        pwssWlanDB->WssWlanAttributeDB.u4WlanIfIndex =
            (UINT4) i4WlanProfileIfIndex;

        pwssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bQosTraffic = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bPassengerTrustMode = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bQosRateLimit = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bQosUpStreamCIR = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bQosUpStreamCBS = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bQosUpStreamEIR = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bQosUpStreamEBS = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bQosDownStreamCIR = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bQosDownStreamCBS = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bQosDownStreamEIR = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bQosDownStreamEBS = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bVlanId = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bBandwidthThresh = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bSsidIsolation = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bPrivacyOptionImplemented = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bShortPreambleOptionImplemented =
            OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bPBCCOptionImplemented = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bChannelAgilityPresent = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bQosOptionImplemented = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bSpectrumManagementRequired = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bShortSlotTimeOptionImplemented =
            OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bAPSDOptionImplemented = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bDSSSOFDMOptionEnabled = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bDelayedBlockAckOptionImplemented =
            OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bImmediateBlockAckOptionImplemented =
            OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bQAckOptionImplemented = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bQueueRequestOptionImplemented =
            OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bTXOPRequestOptionImplemented =
            OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bQosTraffic = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bPowerConstraint = OSIX_TRUE;

        if (WssIfProcessWssWlanDBMsg
            (WSS_WLAN_GET_IFINDEX_ENTRY, pwssWlanDB) != OSIX_SUCCESS)
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }
        CliPrintf (CliHandle, "\r\nVlan Id                     : %d",
                   pwssWlanDB->WssWlanAttributeDB.u2VlanId);
        if (pwssWlanDB->WssWlanAttributeDB.u1SsidIsolation ==
            CLI_WLAN_ISOLATION_ENABLE)
            CliPrintf (CliHandle, "\r\nBandwidth Thresh            : %d",
                       pwssWlanDB->WssWlanAttributeDB.u4BandwidthThresh);
        if (i4WlanAdminStatus == CLI_WLAN_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nSsid Isolation              : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nSsid Isolation              : Disabled");
        }

        if (pwssWlanDB->WssWlanAttributeDB.u1QosRateLimit == CLI_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nQos Rate Limit              : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nQos Rate Limit              : Disabled");
        }
        if ((pwssWlanDB->WssWlanAttributeDB.u1QosTraffic + 1) ==
            CLI_WSSCFG_BACKGROUND)
        {
            CliPrintf (CliHandle, "\r\nQos Traffic                 : Bronze");
        }
        else if ((pwssWlanDB->WssWlanAttributeDB.u1QosTraffic + 1) ==
                 CLI_WSSCFG_VIDEO)
        {
            CliPrintf (CliHandle, "\r\nQos Traffic                 : Gold");
        }
        else if ((pwssWlanDB->WssWlanAttributeDB.u1QosTraffic + 1) ==
                 CLI_WSSCFG_VOICE)
        {
            CliPrintf (CliHandle, "\r\nQos Traffic                 : Platinum");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nQos Traffic                 : Silver");
        }
        if (pwssWlanDB->WssWlanAttributeDB.u1PassengerTrustMode == CLI_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nQos Passenger Trust Mode    : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nQos Passenger Trust Mode    : Disabled");
        }
        if (pwssWlanDB->WssWlanAttributeDB.u1PrivacyOptionImplemented ==
            CLI_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nPrivacy Option              : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nPrivacy Option              : Disabled");
        }
        if (pwssWlanDB->WssWlanAttributeDB.u1ShortPreambleOptionImplemented ==
            CLI_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nShortPreamble Option        : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nShortPreamble Option        : Disabled");
        }
        if (pwssWlanDB->WssWlanAttributeDB.u1PBCCOptionImplemented ==
            CLI_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nPBCC Option                 : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nPBCC Option                 : Disabled");
        }
        if (pwssWlanDB->WssWlanAttributeDB.u1ChannelAgilityPresent ==
            CLI_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nChannel Agility Present     : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nChannel Agility Present     : Disabled");
        }
        if (pwssWlanDB->WssWlanAttributeDB.u1QosOptionImplemented == CLI_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nQos Option                  : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nQos Option                  : Disabled");
        }
        if (pwssWlanDB->WssWlanAttributeDB.u1SpectrumManagementRequired ==
            CLI_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nSpectrum Management         : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nSpectrum Management         : Disabled");
        }
        if (pwssWlanDB->WssWlanAttributeDB.u1ShortSlotTimeOptionImplemented ==
            CLI_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nShortSlotTime Option        : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nShortSlotTime Option        : Disabled");
        }
        if (pwssWlanDB->WssWlanAttributeDB.u1APSDOptionImplemented ==
            CLI_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nAPSD Option                 : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nAPSD Option                 : Disabled");
        }
        if (pwssWlanDB->WssWlanAttributeDB.u1DSSSOFDMOptionEnabled ==
            CLI_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nDSSS OFDM Option            : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nDSSS OFDM Option            : Disabled");
        }
        if (pwssWlanDB->WssWlanAttributeDB.u1DelayedBlockAckOptionImplemented ==
            CLI_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nDelayed Block Ack           : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nDelayed Block Ack           : Disabled");
        }
        if (pwssWlanDB->WssWlanAttributeDB.
            u1ImmediateBlockAckOptionImplemented == CLI_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nImmediate Block Ack        : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nImmediate Block Ack         : Disabled");
        }
        if (pwssWlanDB->WssWlanAttributeDB.u1QAckOptionImplemented ==
            CLI_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nQAck Option                : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nQAck Option                 : Disabled");
        }
        if (pwssWlanDB->WssWlanAttributeDB.u1QueueRequestOptionImplemented ==
            CLI_ENABLE)
        {
            CliPrintf (CliHandle,
                       "\r\nQueue Request Option          : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nQueue Request Option        : Disabled");
        }
        if (pwssWlanDB->WssWlanAttributeDB.u1TXOPRequestOptionImplemented ==
            CLI_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nTXOP Request Option        : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nTXOP Request Option         : Disabled");
        }
        if (pwssWlanDB->WssWlanAttributeDB.u1CfPollable == CLI_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nCf Pollable                : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nCf Pollable                 : Disabled");
        }
        if (pwssWlanDB->WssWlanAttributeDB.u1CfPollRequest == CLI_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nCf Poll Request            : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nCf Poll Request             : Disabled");
        }

        CliPrintf (CliHandle, "\r\nQos UpStream CIR            : %d",
                   pwssWlanDB->WssWlanAttributeDB.u4QosUpStreamCIR);
        CliPrintf (CliHandle, "\r\nQos UpStream CBS            : %d",
                   pwssWlanDB->WssWlanAttributeDB.u4QosUpStreamCBS);
        CliPrintf (CliHandle, "\r\nQos UpStream EIR            : %d",
                   pwssWlanDB->WssWlanAttributeDB.u4QosUpStreamEIR);
        CliPrintf (CliHandle, "\r\nQos UpStream EBS          : %d",
                   pwssWlanDB->WssWlanAttributeDB.u4QosUpStreamEBS);
        CliPrintf (CliHandle, "\r\nQos DownStream CIR          : %d",
                   pwssWlanDB->WssWlanAttributeDB.u4QosDownStreamCIR);
        CliPrintf (CliHandle, "\r\nQos DownStream CBS          : %d",
                   pwssWlanDB->WssWlanAttributeDB.u4QosDownStreamCBS);
        CliPrintf (CliHandle, "\r\nQos DownStream EIR          : %d",
                   pwssWlanDB->WssWlanAttributeDB.u4QosDownStreamEIR);
        CliPrintf (CliHandle, "\r\nQos DownStream EBS          : %d",
                   pwssWlanDB->WssWlanAttributeDB.u4QosDownStreamEBS);

        pwssWlanDB->WssWlanIsPresentDB.bManagmentSSID = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg
            (WSS_WLAN_GET_MANAGMENT_SSID, pwssWlanDB) == OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nManagement SSID            : %d",
                       pwssWlanDB->WssWlanAttributeDB.au1ManagmentSSID[0]);
        }

        wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.i4IfIndex
            = i4WlanProfileIfIndex;
        if (WsscfgGetAllFsDot11WlanAuthenticationProfileTable
            (&wsscfgFsDot11WlanAuthenticationProfileEntry) == OSIX_SUCCESS)
        {
            i4Authentication =
                wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
                i4FsDot11WlanAuthenticationAlgorithm;
            i4WepKeyType =
                wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
                i4FsDot11WlanWepKeyType;
            i4WepKeyLength =
                wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
                i4FsDot11WlanWepKeyLength;
            i4WebAuth =
                wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
                i4FsDot11WlanWebAuthentication;
            i4WepKeyIndex =
                wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
                i4FsDot11WlanWepKeyIndex;
            MEMCPY (au1WepKey,
                    wsscfgFsDot11WlanAuthenticationProfileEntry.MibObject.
                    au1FsDot11WlanWepKey,
                    STRLEN (wsscfgFsDot11WlanAuthenticationProfileEntry.
                            MibObject.au1FsDot11WlanWepKey));
        }
        if (i4Authentication == CLI_AUTH_ALGO_OPEN)
        {
            CliPrintf (CliHandle,
                       "\r\nAuthentication Algorithm   : Open System");
        }
        else if (i4Authentication == CLI_AUTH_ALGO_SHARED)
        {
            CliPrintf (CliHandle, "\r\nAuthentication Algorithm  :"
                       "Static WEP (Shared Key)");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nAuthentication Algorithm  : Invalid");
        }

        if (i4WebAuth == WSS_AUTH_STATUS_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nWeb Authentication        : Enabled");
            nmhGetFsDot11WlanLoginAuthentication (i4WlanProfileIfIndex,
                                                  &i4SetValFsDot11WlanLoginAuthentication);
            if (i4SetValFsDot11WlanLoginAuthentication ==
                WSS_WEB_AUTH_RAD_ENABLE)
            {
                CliPrintf (CliHandle,
                           "\r\nWeb Authentication Type        : Radius");
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r\nWeb Authentication Type        : Local");
            }
        }
        else
        {
            CliPrintf (CliHandle, "\r\nWeb Authentication        : Disabled");
        }

        if (i4Authentication == CLI_AUTH_ALGO_SHARED)
        {
            CliPrintf (CliHandle, "\r\nKey Index                 : %d",
                       i4WepKeyIndex);

            if (i4WepKeyType == CLI_WEP_KEY_TYPE_HEX)
            {
                CliPrintf (CliHandle, "\r\nWeb Authentication         : Hex");
            }
            else
            {
                CliPrintf (CliHandle, "\r\nWeb Authentication         : Ascii");
            }

            CliPrintf (CliHandle, "\r\nKey Length                 : %d",
                       i4WepKeyLength);

            CliPrintf (CliHandle, "\r\nKey :                      : %s",
                       au1WepKey);
        }
        WebAuthRedirectFile.pu1_OctetList = au1RedirectFile;
        if (nmhGetFsDot11WlanWebAuthRedirectFileName (i4WlanProfileIfIndex,
                                                      &WebAuthRedirectFile) !=
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nWeb-Auth Redirect File Name    : %s",
                       WebAuthRedirectFile.pu1_OctetList);
        }
        CliPrintf (CliHandle, "\r\n");

    }
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    : WssCfgShowApGroup
 * Description :
 * Input       :  CliHandle
 *
 * Output      :  None
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 * ****************************************************************************/
INT4
WssCfgShowApGroupConfig (tCliHandle CliHandle, UINT4 u4ApGroupId)
{
    tApGroupConfigEntry *pApGroupConfigEntry = NULL;
    tApGroupConfigWTPEntry *pApGroupFirstConfigWTPEntry = NULL;
    tApGroupConfigWTPEntry *pApGroupNextConfigWTPEntry = NULL;
    tApGroupConfigWLANEntry *pApGroupFirstConfigWLANEntry = NULL;
    tApGroupConfigWLANEntry *pApGroupNextConfigWLANEntry = NULL;

    tSNMP_OCTET_STRING_TYPE ApGroupName;
    tSNMP_OCTET_STRING_TYPE ApGroupNameDesc;
    UINT1               au1ApGroupName[50];
    UINT1               au1ApGroupDesc[50];
    static UINT4        u4RadioPolicy = 0;
    UINT4               u4VlanId = 0;
    UINT1               au1WtpName[256];
    tSNMP_OCTET_STRING_TYPE WtpProfileName;

    MEMSET (au1WtpName, 0, 256);
    WtpProfileName.pu1_OctetList = au1WtpName;
    MEMSET (&ApGroupName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ApGroupNameDesc, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ApGroupName, 0, 32);
    MEMSET (au1ApGroupDesc, 0, 32);
    ApGroupName.pu1_OctetList = au1ApGroupName;
    ApGroupNameDesc.pu1_OctetList = au1ApGroupDesc;

    if (ApGroupGetConfigEntry ((UINT2) u4ApGroupId,
                               &pApGroupConfigEntry) == FAILURE)
    {
        CliPrintf (CliHandle, "\r\nAp Group is not enabled for the group"
                   " %s\n", ApGroupName.pu1_OctetList);
        return FAILURE;
    }

    if (pApGroupConfigEntry == NULL)
    {
        CliPrintf (CliHandle, "\r\nApGroup is not enabled for the wlan "
                   "%d\n", u4ApGroupId);
        return FAILURE;
    }

    CliPrintf (CliHandle, "\r\nGroup Identifier    : %d", u4ApGroupId);
    MEMSET (au1ApGroupName, 0, 32);

    if (nmhGetFsApGroupName (u4ApGroupId, &ApGroupName) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nGroup Name          :  %s",
               ApGroupName.pu1_OctetList);
    MEMSET (au1ApGroupDesc, 0, 32);
    if (nmhGetFsApGroupNameDescription (u4ApGroupId,
                                        &ApGroupNameDesc) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    CliPrintf (CliHandle, "\r\nGroup Description   :  %s",
               ApGroupNameDesc.pu1_OctetList);

    if (nmhGetFsApGroupRadioPolicy (u4ApGroupId,
                                    &u4RadioPolicy) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (nmhGetFsApGroupInterfaceVlan (u4ApGroupId, &u4VlanId) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (u4RadioPolicy == CLI_WTP_RADIO_ALL)
    {
        CliPrintf (CliHandle, "\r\nRadio Policy        :  %s", "All");
    }
    else if (u4RadioPolicy == CLI_RADIO_TYPEB)
    {
        CliPrintf (CliHandle, "\r\nRadio Policy        :  %s", "dot11b");
    }
    else if (u4RadioPolicy == CLI_RADIO_TYPEA)
    {
        CliPrintf (CliHandle, "\r\nRadio Policy        :  %s", "dot11a");
    }

    CliPrintf (CliHandle, "\r\nVlan ID             :  %d", u4VlanId);
    CliPrintf (CliHandle, "\r\n List of Wlans       :");
    CliPrintf (CliHandle, "\r\n --------------------");
    CliPrintf (CliHandle, "\r\n WLAN-ID         SSIDNAME");
    if (ApGroupGetFirstConfigWLANEntry
        (&pApGroupFirstConfigWLANEntry) == FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\nNo Wlans are configured to specified AP Group.");
    }
    else
    {
        do
        {
            pApGroupNextConfigWLANEntry = pApGroupFirstConfigWLANEntry;
            if (pApGroupNextConfigWLANEntry->u2ApGroupId == (UINT2) u4ApGroupId)
            {
                CliPrintf (CliHandle, "\r\n %d             %s",
                           pApGroupNextConfigWLANEntry->u4WlanId,
                           pApGroupNextConfigWLANEntry->au1WlanProfileName);
            }
        }
        while (ApGroupGetNextWLANConfigEntry (pApGroupNextConfigWLANEntry,
                                              &pApGroupFirstConfigWLANEntry) ==
               SUCCESS);
    }

    CliPrintf (CliHandle, "\r\n List of WTP Profiles      :  ");
    CliPrintf (CliHandle, "\r\n --------------------------");
    CliPrintf (CliHandle, "\r\n AP-ProfileId           PROFILENAME");
    if (ApGroupGetFirstConfigWTPEntry (&pApGroupFirstConfigWTPEntry) == FAILURE)
    {
        CliPrintf (CliHandle, "\r\nNo Ap..");
    }
    else
    {
        do
        {
            pApGroupNextConfigWTPEntry = pApGroupFirstConfigWTPEntry;
            if (pApGroupNextConfigWTPEntry->u2ApGroupId == (UINT2) u4ApGroupId)
            {
                if (nmhGetCapwapBaseWtpProfileName
                    ((UINT4) pApGroupNextConfigWTPEntry->
                     u2ApGroupWTPIndex, &WtpProfileName) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n %d                      %s",
                               pApGroupNextConfigWTPEntry->u2ApGroupWTPIndex,
                               WtpProfileName.pu1_OctetList);
                }
            }
        }
        while (ApGroupGetNextConfigWTPEntry (pApGroupNextConfigWTPEntry,
                                             &pApGroupFirstConfigWTPEntry) ==
               SUCCESS);
    }

    CliPrintf (CliHandle, "\r\n --------------------------");
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    : WssCfgShowApGroupSummary
 * Description :
 * Input       :  CliHandle
 * 
 * Output      :  None
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
WssCfgShowApGroupSummary (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE WtpProfileName;
    tApGroupConfigEntry *pApGroupFirstConfigEntry = NULL;
    tApGroupConfigEntry *pApGroupNextConfigEntry = NULL;
    tApGroupConfigWTPEntry *pApGroupFirstConfigWTPEntry = NULL;
    tApGroupConfigWTPEntry *pApGroupNextConfigWTPEntry = NULL;
    tApGroupConfigWLANEntry *pApGroupFirstConfigWLANEntry = NULL;
    tApGroupConfigWLANEntry *pApGroupNextConfigWLANEntry = NULL;
    UINT1               au1WtpName[256];

    INT4                i4ApGroupEnabledStatus = 0;

    MEMSET (au1WtpName, 0, 256);
    WtpProfileName.pu1_OctetList = au1WtpName;

    if (nmhGetFsApGroupEnabledStatus (&i4ApGroupEnabledStatus) == SNMP_SUCCESS)
    {
        if (i4ApGroupEnabledStatus == 1)
        {
            CliPrintf (CliHandle, "\r\n Ap Group Feature Status: Enabled ");
        }
        else if (i4ApGroupEnabledStatus == 2)
        {
            CliPrintf (CliHandle, "\r\n Ap Group Feature Status: Disabled ");
        }
    }

    if ((nmhGetFsApGroupEnabledStatus (&i4ApGroupEnabledStatus)) ==
        SNMP_SUCCESS)
    {
        if (ApGroupGetFirstConfigEntry (&pApGroupFirstConfigEntry) == FAILURE)
        {
            CliPrintf (CliHandle, "\r\nNo Ap Groups are Present ..");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n*------- AP GROUP SUMMARY----*");
            do
            {
                pApGroupNextConfigEntry = pApGroupFirstConfigEntry;
                CliPrintf (CliHandle, "\r\n -------------------------");
                CliPrintf (CliHandle, "\r\n Group Id           : %d ",
                           pApGroupNextConfigEntry->u2ApGroupId);
                CliPrintf (CliHandle, "\r\n Group Name         : %s ",
                           pApGroupNextConfigEntry->au1ApGroupName);
                CliPrintf (CliHandle, "\r\n Group Description  : %s ",
                           pApGroupNextConfigEntry->au1ApGroupNameDes);
                if (pApGroupNextConfigEntry->u2RadioPolicy == CLI_WTP_RADIO_ALL)
                {
                    CliPrintf (CliHandle, "\r\n Radio Policy       : %s ",
                               "ALL");
                }
                else if (pApGroupNextConfigEntry->u2RadioPolicy ==
                         CLI_RADIO_TYPEB)
                {
                    CliPrintf (CliHandle, "\r\n Radio Policy       : %s ",
                               "dot11b");
                }
                else if (pApGroupNextConfigEntry->u2RadioPolicy ==
                         CLI_RADIO_TYPEA)
                {
                    CliPrintf (CliHandle, "\r\n Radio Policy       : %s ",
                               "dot11a");
                }

                CliPrintf (CliHandle, "\r\n Radio Policy       : %d ",
                           pApGroupNextConfigEntry->u2RadioPolicy);
                CliPrintf (CliHandle, "\r\n Interface Vlan     : %d ",
                           pApGroupNextConfigEntry->u2InterfaceVlan);

                CliPrintf (CliHandle, "\r\n List of Wlans      : ");
                CliPrintf (CliHandle, "\r\n --------------------");
                CliPrintf (CliHandle, "\r\n WLAN-ID        SSIDNAME");
                if (ApGroupGetFirstConfigWLANEntry
                    (&pApGroupFirstConfigWLANEntry) == FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n No Wlans are Present..");
                }
                else
                {
                    do
                    {
                        pApGroupNextConfigWLANEntry =
                            pApGroupFirstConfigWLANEntry;
                        if (pApGroupNextConfigWLANEntry->u2ApGroupId ==
                            pApGroupNextConfigEntry->u2ApGroupId)
                        {
                            CliPrintf (CliHandle, "\r\n %d            %s",
                                       pApGroupNextConfigWLANEntry->u4WlanId,
                                       pApGroupNextConfigWLANEntry->
                                       au1WlanProfileName);
                        }
                    }
                    while (ApGroupGetNextWLANConfigEntry
                           (pApGroupNextConfigWLANEntry,
                            &pApGroupFirstConfigWLANEntry) == SUCCESS);
                }
                CliPrintf (CliHandle, "\r\n List of WTP Profiles      :  ");
                CliPrintf (CliHandle, "\r\n ---------------------------");
                CliPrintf (CliHandle, "\r\n AP-ProfileId         PROFILENAME");
                if (ApGroupGetFirstConfigWTPEntry (&pApGroupFirstConfigWTPEntry)
                    == FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n No Ap Profiles are present..");
                }
                else
                {
                    do
                    {
                        pApGroupNextConfigWTPEntry =
                            pApGroupFirstConfigWTPEntry;
                        if (pApGroupNextConfigWTPEntry->u2ApGroupId ==
                            pApGroupNextConfigEntry->u2ApGroupId)
                        {
                            if (nmhGetCapwapBaseWtpProfileName
                                ((UINT4) pApGroupNextConfigWTPEntry->
                                 u2ApGroupWTPIndex,
                                 &WtpProfileName) == SNMP_SUCCESS)
                            {
                                CliPrintf (CliHandle,
                                           "\r\n %d                    %s",
                                           pApGroupNextConfigWTPEntry->
                                           u2ApGroupWTPIndex,
                                           WtpProfileName.pu1_OctetList);
                            }
                        }
                    }
                    while (ApGroupGetNextConfigWTPEntry
                           (pApGroupNextConfigWTPEntry,
                            &pApGroupFirstConfigWTPEntry) == SUCCESS);
                }
                CliPrintf (CliHandle, "\r\n --------------------------");
            }
            while (ApGroupGetNextConfigEntry (pApGroupNextConfigEntry,
                                              &pApGroupFirstConfigEntry) ==
                   SUCCESS);
        }
    }
    return OSIX_SUCCESS;
}

/*******************************************************************************
 * Function Name      : ApGroupCliGetWlanIfIndexFromWlanId                      *
 *                                                                              *
 * Description        : Gets WlanIfIndex From Wlan ID                           *
 *                                                                              *
 * Input              : u4WlanId                                                *
 *                      pi4WlanIfIndex                                          *
 *                                                                              *
 * Output             : NULL                                                    *
 *                                                                              *
 * Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                                 *
 *                                                                              *
 ********************************************************************************/
INT1
ApGroupCliGetWlanIfIndexFromWlanId (UINT4 u4WlanId, INT4 *pi4WlanIfIndex)
{
    tWssWlanDB          WssWlanDB;
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));

    WssWlanDB.WssWlanAttributeDB.u2WlanProfileId = (UINT2) u4WlanId;
    WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
#ifdef WLC_WANTED
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY, &WssWlanDB)
        == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &WssWlanDB)
        == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif
    *pi4WlanIfIndex = (INT4) WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
* Function    : WssCfgShowNetworkSummary  
* Description :
* Input       :  CliHandle 
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowNetworkSummary (tCliHandle CliHandle, UINT4 *pu4FsRadioType)
{
    tSNMP_OCTET_STRING_TYPE WtpProfileName;
    tSNMP_OCTET_STRING_TYPE WtpModelNumber;
    tSNMP_OCTET_STRING_TYPE MacAddress;

    tMacAddr            MacAddr;
    UINT1               au1MacAddress[21];
    UINT4               u4NoOfRadio = 0;
    UINT1               au1WtpName[256];
    tMacAddr            au1MacAddr;
    UINT4               u4GetRadioType = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4AdminStatus = 0;
    INT4                i4OperStatus = 0;
    UINT4               currentProfileId = 0, nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;
    INT4                i4Dot11TxPowerLevel1 = 0;
    INT4                i4Dot11CurrentChannel = 0;
    UINT1               au1WtpModel[256];

    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WtpModelNumber, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&MacAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1MacAddr, 0, sizeof (tMacAddr));
    MEMSET (au1MacAddress, 0, sizeof (au1MacAddress));
    MEMSET (MacAddr, 0, sizeof (tMacAddr));
    MEMSET (au1WtpModel, 0, 256);
    MEMSET (au1WtpName, 0, 256);

    WtpProfileName.pu1_OctetList = au1WtpName;
    WtpModelNumber.pu1_OctetList = au1WtpModel;
    MacAddress.pu1_OctetList = au1MacAddress;

    if (pu4FsRadioType == NULL)
    {
        CliPrintf (CliHandle, "\r\n Radio Type missing \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return OSIX_FAILURE;
    }

    if (nmhGetFirstIndexFsCapwapWirelessBindingTable
        (&nextProfileId, &u4nextBindingId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\n\r\n\rEntry does not exist \n\r\n\r");
        return OSIX_FAILURE;
    }

    CliPrintf (CliHandle,
               "\r\nAP Name     Radio Id     MAC Address       Admin State   Oper State");
    CliPrintf (CliHandle, "    Channel   TxPower Level");
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle,
               "-------     --------     -----------       -----------   ----------  ");
    CliPrintf (CliHandle, "  -------   --------------");

    CliPrintf (CliHandle, "\r\n");

    do
    {
        currentProfileId = nextProfileId;
        u4currentBindingId = u4nextBindingId;

        if (u4currentBindingId == 0)
        {
            return OSIX_FAILURE;
        }

        if (nmhGetCapwapBaseWtpProfileWtpModelNumber
            (currentProfileId, &WtpModelNumber) != SNMP_SUCCESS)
        {
            continue;
        }

        if (nmhGetFsNoOfRadio (&WtpModelNumber, &u4NoOfRadio) != OSIX_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nFailed to fetch number of radios\r\n");
            break;

        }
        if (nmhGetCapwapBaseWtpProfileName
            (currentProfileId, &WtpProfileName) != SNMP_SUCCESS)
        {
            continue;
        }
        MEMSET (au1MacAddress, 0, sizeof (au1MacAddress));
        if (nmhGetCapwapBaseWtpProfileWtpMacAddress
            (currentProfileId, &MacAddress) != SNMP_SUCCESS)
        {
            continue;
        }

        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
            (currentProfileId, u4nextBindingId,
             &i4RadioIfIndex) != SNMP_SUCCESS)
        {
            continue;
        }

        if (nmhGetFsDot11RadioType (i4RadioIfIndex, &u4GetRadioType) !=
            SNMP_FAILURE)
        {
            if (WSSCFG_RADIO_TYPE_COMP ((INT4) *pu4FsRadioType))
            {
                continue;
            }
            else
            {
                if (*pu4FsRadioType == CLI_RADIO_TYPEA)
                {
                    if (nmhGetDot11CurrentFrequency
                        (i4RadioIfIndex,
                         (UINT4 *) &i4Dot11CurrentChannel) != SNMP_SUCCESS)
                    {
                        continue;
                    }
                }
                else if (*pu4FsRadioType == CLI_RADIO_TYPEB)
                {
                    if (nmhGetDot11CurrentChannel
                        (i4RadioIfIndex,
                         (UINT4 *) &i4Dot11CurrentChannel) != SNMP_SUCCESS)
                    {
                        continue;
                    }
                }
                if (nmhGetDot11CurrentTxPowerLevel
                    (i4RadioIfIndex,
                     (UINT4 *) &i4Dot11TxPowerLevel1) != SNMP_SUCCESS)
                {
                    continue;
                }
            }
        }
        else
        {
            continue;
        }
        if (nmhGetIfMainAdminStatus (i4RadioIfIndex, &i4AdminStatus) !=
            SNMP_SUCCESS)
        {
            continue;
        }

        if (nmhGetIfMainOperStatus (i4RadioIfIndex, &i4OperStatus) !=
            SNMP_SUCCESS)
        {
            continue;
        }

        if (WSSCFG_RADIO_TYPE_COMP ((INT4) *pu4FsRadioType))
        {
            return CLI_SUCCESS;
        }
        CliPrintf (CliHandle, "\r\n");

        CliPrintf (CliHandle, "%-15s", WtpProfileName.pu1_OctetList);
        CliPrintf (CliHandle, "%-10d", u4currentBindingId);

        CliPrintf (CliHandle, "%-20s", MacAddress.pu1_OctetList);

        if (i4AdminStatus == 1)
        {
            CliPrintf (CliHandle, "%-17s", "Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "%-17s", "Disabled");
        }

        if (i4OperStatus == 1)
        {
            CliPrintf (CliHandle, "%-11s", "Up");
        }
        else
        {
            CliPrintf (CliHandle, "%-11s", "Down");
        }

        CliPrintf (CliHandle, "%-15d", i4Dot11CurrentChannel);
        CliPrintf (CliHandle, "%-15d", (INT2) i4Dot11TxPowerLevel1);
        CliPrintf (CliHandle, "\r\n");

    }
    while (nmhGetNextIndexFsCapwapWirelessBindingTable
           (currentProfileId, &nextProfileId, u4currentBindingId,
            &u4nextBindingId) == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/****************************************************************************
  * Function    : WssCfgShowCountrySupported
  * Description :
  * Input       :  CliHandle
  *
  *
  * Output      :  None
  * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowCountrySupported (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE CountryName;
    tSNMP_OCTET_STRING_TYPE Country;
    tSNMP_OCTET_STRING_TYPE CountryCode;
    UINT4               u4SupportedCountryIndex = 0;
    UINT4               u4NextSupportedCountryIndex = 0;
    UINT1               au1CountryCode[3];
    UINT1               au1Country[32];
    UINT1               au1CountryName[32];

    MEMSET (&CountryName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1CountryName, 0, 32);
    MEMSET (&Country, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1Country, 0, 32);
    MEMSET (&CountryCode, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1CountryCode, 0, 3);

    CountryName.pu1_OctetList = au1CountryName;
    Country.pu1_OctetList = au1Country;
    CountryCode.pu1_OctetList = au1CountryCode;

    if (nmhGetFsDot11CountryString (&Country) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (nmhGetFirstIndexFsDot11SupportedCountryTable
        (&u4NextSupportedCountryIndex) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\n\r\n\rEntry does not exist \n\r\n\r");
        return OSIX_FAILURE;
    }

    CliPrintf (CliHandle, "\n\r\n\rConfigured Country : %s \n\n\r",
               Country.pu1_OctetList);
    CliPrintf (CliHandle, "\n\r\n\rSupported Country list \n\r\n\r");
    do
    {
        u4SupportedCountryIndex = u4NextSupportedCountryIndex;

        if (u4SupportedCountryIndex == 0)
        {
            return OSIX_FAILURE;
        }
        MEMSET (au1CountryCode, 0, 3);
        if (nmhGetFsDot11CountryCode (u4SupportedCountryIndex, &CountryCode) ==
            SNMP_FAILURE)
        {
            continue;
        }
        MEMSET (au1CountryName, 0, 32);
        if (nmhGetFsDot11CountryName (u4SupportedCountryIndex, &CountryName) ==
            SNMP_FAILURE)
        {
            continue;
        }

        CliPrintf (CliHandle, "%s - %s\r\n", CountryCode.pu1_OctetList,
                   CountryName.pu1_OctetList);
    }
    while (nmhGetNextIndexFsDot11SupportedCountryTable
           (u4SupportedCountryIndex, &u4NextSupportedCountryIndex)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/****************************************************************************
  * Function    : WssCfgShowCapabilityProfile
  * Description :
  * Input       :  CliHandle
  *
  * Output      :  None
  * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowCapabilityProfile (tCliHandle CliHandle,
                             UINT1 *pu1CapwapBaseWtpProfileName)
{
    tWsscfgFsDot11CapabilityProfileEntry Dot11CapabilityProfile;
    MEMSET (&Dot11CapabilityProfile, 0,
            sizeof (tWsscfgFsDot11CapabilityProfileEntry));

    tSNMP_OCTET_STRING_TYPE ProfileName;
    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    ProfileName.i4_Length = STRLEN (pu1CapwapBaseWtpProfileName);
    ProfileName.pu1_OctetList = pu1CapwapBaseWtpProfileName;

    MEMCPY (Dot11CapabilityProfile.
            MibObject.au1FsDot11CapabilityProfileName,
            ProfileName.pu1_OctetList, ProfileName.i4_Length);
    Dot11CapabilityProfile.MibObject.
        i4FsDot11CapabilityProfileNameLen = ProfileName.i4_Length;

    if (WsscfgGetAllFsDot11CapabilityProfileTable
        (&Dot11CapabilityProfile) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n No Entry present\r\n");
        return CLI_SUCCESS;
    }

    if (Dot11CapabilityProfile.MibObject.i4FsDot11CFPollable == 1)
    {
        CliPrintf (CliHandle,
                   "\r\n  CFPollable                   : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n  CFPollable                   : Disabled\r\n");
    }
    if (Dot11CapabilityProfile.MibObject.i4FsDot11CFPollRequest == 1)
    {
        CliPrintf (CliHandle,
                   "\r\n  CFPollRequest                : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n  CFPollRequest                : Disabled\r\n");
    }
    if (Dot11CapabilityProfile.MibObject.i4FsDot11PrivacyOptionImplemented == 1)
    {
        CliPrintf (CliHandle,
                   "\r\n  Privacy Option               : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n  Privacy Option               : Disabled\r\n");
    }
    if (Dot11CapabilityProfile.
        MibObject.i4FsDot11ShortPreambleOptionImplemented == 1)
    {
        CliPrintf (CliHandle,
                   "\r\n  Short Preamble Option        : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n  Short Preamble Option        : Disabled\r\n");
    }
    if (Dot11CapabilityProfile.MibObject.i4FsDot11PBCCOptionImplemented == 1)
    {
        CliPrintf (CliHandle,
                   "\r\n  PBCC Option                  : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n  PBCC Option                  : Disabled\r\n");
    }
    if (Dot11CapabilityProfile.MibObject.i4FsDot11ChannelAgilityPresent == 1)
    {
        CliPrintf (CliHandle,
                   "\r\n  Channel Agility              : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n  Channel Agility              : Disabled\r\n");
    }
    if (Dot11CapabilityProfile.MibObject.i4FsDot11QosOptionImplemented == 1)
    {
        CliPrintf (CliHandle,
                   "\r\n  Qos Option                   : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n  Qos Option                   : Disabled\r\n");
    }
    if (Dot11CapabilityProfile.
        MibObject.i4FsDot11SpectrumManagementRequired == 1)
    {
        CliPrintf (CliHandle,
                   "\r\n  Spectrum Management          : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n  Spectrum Management          : Disabled\r\n");
    }
    if (Dot11CapabilityProfile.
        MibObject.i4FsDot11ShortSlotTimeOptionImplemented == 1)
    {
        CliPrintf (CliHandle,
                   "\r\n  ShortSlot Time Option        : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n  ShortSlot Time Option        : Disabled\r\n");
    }
    if (Dot11CapabilityProfile.MibObject.i4FsDot11APSDOptionImplemented == 1)
    {
        CliPrintf (CliHandle,
                   "\r\n  APSD Option                  : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n  APSD Option                  : Disabled\r\n");
    }
    if (Dot11CapabilityProfile.MibObject.i4FsDot11DSSSOFDMOptionEnabled == 1)
    {
        CliPrintf (CliHandle,
                   "\r\n  DSSSOFDM Option              : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n  DSSSOFDM Option              : Disabled\r\n");
    }
    if (Dot11CapabilityProfile.
        MibObject.i4FsDot11DelayedBlockAckOptionImplemented == 1)
    {
        CliPrintf (CliHandle,
                   "\r\n  Delayed Block Ack Option     : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n  Delayed Block Ack Option     : Disabled\r\n");
    }
    if (Dot11CapabilityProfile.
        MibObject.i4FsDot11ImmediateBlockAckOptionImplemented == 1)
    {
        CliPrintf (CliHandle,
                   "\r\n  Immediate Block Ack Option   : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n  Immediate Block Ack Option   : Disabled\r\n");
    }
    if (Dot11CapabilityProfile.MibObject.i4FsDot11QAckOptionImplemented == 1)
    {
        CliPrintf (CliHandle,
                   "\r\n  QAck Option                  : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n  QAck Option                  : Disabled\r\n");
    }
    if (Dot11CapabilityProfile.
        MibObject.i4FsDot11QueueRequestOptionImplemented == 1)
    {
        CliPrintf (CliHandle,
                   "\r\n  Queue Request Option         : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n  Queue Request Option         : Disabled\r\n");
    }
    if (Dot11CapabilityProfile.
        MibObject.i4FsDot11TXOPRequestOptionImplemented == 1)
    {
        CliPrintf (CliHandle,
                   "\r\n  TXOP Request Option          : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n  TXOP Request Option          : Disabled\r\n");
    }
    if (Dot11CapabilityProfile.MibObject.i4FsDot11RSNAOptionImplemented == 1)
    {
        CliPrintf (CliHandle,
                   "\r\n  RSNA Option                  : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n  RSNA Option                  : Disabled\r\n");
    }
    if (Dot11CapabilityProfile.
        MibObject.i4FsDot11RSNAPreauthenticationImplemented == 1)
    {
        CliPrintf (CliHandle,
                   "\r\n  RSNA Preauthentication       : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n  RSNA Preauthentication       : Disabled\r\n");
    }
    if (Dot11CapabilityProfile.MibObject.i4FsDot11CapabilityRowStatus == 1)
    {
        CliPrintf (CliHandle,
                   "\r\n  Capability RowStatus         : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n  Capability RowStatus         : Disabled\r\n");
    }

    return CLI_SUCCESS;
}

/****************************************************************************
  * Function    : WssCfgShowAuthenticationProfile
  * Description :
  * Input       :  CliHandle
  *
  * Output      :  None
  * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowAuthenticationProfile (tCliHandle CliHandle,
                                 UINT1 *pu1CapwapBaseWtpProfileName)
{
    tWsscfgFsDot11AuthenticationProfileEntry Dot11AuthenticationProfile;
    MEMSET (&Dot11AuthenticationProfile, 0,
            sizeof (tWsscfgFsDot11AuthenticationProfileEntry));

    tSNMP_OCTET_STRING_TYPE WepKeyLen;
    MEMSET (&WepKeyLen, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    tSNMP_OCTET_STRING_TYPE ProfileName;
    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    ProfileName.i4_Length = STRLEN (pu1CapwapBaseWtpProfileName);
    ProfileName.pu1_OctetList = pu1CapwapBaseWtpProfileName;

    MEMCPY (Dot11AuthenticationProfile.
            MibObject.au1FsDot11AuthenticationProfileName,
            ProfileName.pu1_OctetList, ProfileName.i4_Length);
    Dot11AuthenticationProfile.
        MibObject.i4FsDot11AuthenticationProfileNameLen = ProfileName.i4_Length;

    if (WsscfgGetAllFsDot11AuthenticationProfileTable
        (&Dot11AuthenticationProfile) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n No Entry present\r\n");
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\n   Authentication Algorithm    : %d\n",
               Dot11AuthenticationProfile.
               MibObject.i4FsDot11AuthenticationAlgorithm);

    if (Dot11AuthenticationProfile.
        MibObject.i4FsDot11AuthenticationAlgorithm == 2)
    {
        CliPrintf (CliHandle,
                   "\r\n   WEP Key Index                : %d \n",
                   Dot11AuthenticationProfile.MibObject.i4FsDot11WepKeyIndex);
        CliPrintf (CliHandle,
                   "\r\n   WEP Key Type                 : %d\n",
                   Dot11AuthenticationProfile.MibObject.i4FsDot11WepKeyType);
        CliPrintf (CliHandle,
                   "\r\n   WEP Key Length               : %d\n",
                   Dot11AuthenticationProfile.MibObject.i4FsDot11WepKeyLength);
    }

    return CLI_SUCCESS;
}

/****************************************************************************
  * Function    : WssCfgShowQosProfile
  * Description :
  * Input       :  CliHandle
  *
  * Output      :  None
  * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowQosProfile (tCliHandle CliHandle, UINT1 *pu1CapwapBaseWtpProfileName)
{

    tWsscfgFsDot11QosProfileEntry Dot11QosProfile;
    MEMSET (&Dot11QosProfile, 0, sizeof (tWsscfgFsDot11QosProfileEntry));

    tSNMP_OCTET_STRING_TYPE ProfileName;
    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    ProfileName.i4_Length = STRLEN (pu1CapwapBaseWtpProfileName);
    ProfileName.pu1_OctetList = pu1CapwapBaseWtpProfileName;

    MEMCPY (Dot11QosProfile.MibObject.au1FsDot11QosProfileName,
            ProfileName.pu1_OctetList, ProfileName.i4_Length);
    Dot11QosProfile.MibObject.i4FsDot11QosProfileNameLen =
        ProfileName.i4_Length;

    if (WsscfgGetAllFsDot11QosProfileTable (&Dot11QosProfile) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n No Entry present\r\n");
        return CLI_SUCCESS;
    }

    if (Dot11QosProfile.MibObject.i4FsDot11QosTraffic == CLI_ENABLE)
    {
        CliPrintf (CliHandle, "\r\n   Qos Traffic                 : Enabled\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n   Qos Traffic                 : Disabled\n");
    }
    if (Dot11QosProfile.MibObject.i4FsDot11QosPassengerTrustMode == CLI_ENABLE)
    {
        CliPrintf (CliHandle, "\r\n   Qos Passenger Trust Mode    : Enabled\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n   Qos Passenger Trust Mode    : Disabled\n");
    }
    if (Dot11QosProfile.MibObject.i4FsDot11QosRateLimit == CLI_ENABLE)
    {
        CliPrintf (CliHandle, "\r\n   Qos Rate Limit              : Enabled\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n   Qos Rate Limit              : Disabled\n");
    }

    CliPrintf (CliHandle, "\r\n   UpStream CIR                : %d\n",
               Dot11QosProfile.MibObject.i4FsDot11UpStreamCIR);
    CliPrintf (CliHandle, "\r\n   UpStream CBS                : %d\n",
               Dot11QosProfile.MibObject.i4FsDot11UpStreamCBS);
    CliPrintf (CliHandle, "\r\n   UpStream EIR                : %d\n",
               Dot11QosProfile.MibObject.i4FsDot11UpStreamEIR);
    CliPrintf (CliHandle, "\r\n   UpStream EBS                : %d\n",
               Dot11QosProfile.MibObject.i4FsDot11UpStreamEBS);
    CliPrintf (CliHandle, "\r\n   DownStream CIR              : %d\n",
               Dot11QosProfile.MibObject.i4FsDot11DownStreamCIR);
    CliPrintf (CliHandle, "\r\n   DownStream CBS              : %d\n",
               Dot11QosProfile.MibObject.i4FsDot11DownStreamCBS);
    CliPrintf (CliHandle, "\r\n   DownStream EIR              : %d\n",
               Dot11QosProfile.MibObject.i4FsDot11DownStreamEIR);
    CliPrintf (CliHandle, "\r\n   DownStream EBS              : %d\n",
               Dot11QosProfile.MibObject.i4FsDot11DownStreamEBS);

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgGetDot11RadioIfIndex
 * Description :  This function is utility function to give the radio index
 *        for the and WTP Profile Id, WTP Binding ID. This function
 *       also checks the matching of the radio type
 *
 * Input       :  u4WtpProfileId - WTP  Profile ID
 *                u4WtpBindingId - WTP  Binding ID
 *                u4RadioType    - Radio Type
 *
 * Output      :  pi4RadioIfIndex -  Radio Index
 *
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
INT4
WssCfgGetDot11RadioIfIndex (UINT4 u4WtpProfileId, UINT4 u4WtpBindingId,
                            UINT4 u4RadioType, INT4 *pi4RadioIfIndex)
{
    INT4                i4RadioIfIndex = 0;
    INT4                i4RetStatus = OSIX_SUCCESS;
    UINT4               u4GetRadioType = 0;
    INT1                i1RetStatus = 0;

    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex (u4WtpProfileId,
                                                            u4WtpBindingId,
                                                            &i4RadioIfIndex) ==
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    *pi4RadioIfIndex = i4RadioIfIndex;

    i1RetStatus = nmhGetFsDot11RadioType (i4RadioIfIndex, &u4GetRadioType);

    if (i1RetStatus != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
    {
        return OSIX_FAILURE;
    }

    return i4RetStatus;
}

/****************************************************************************
  * Function    : WssCfgShowWlanStatistics
  * Description :
  * Input       :  CliHandle
  *
  * Output      :  None
  * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowWlanStatistics (tCliHandle CliHandle,
                          UINT4 *pu4CurrentCapwapDot11WlanProfileId)
{
    INT4                i4WlanIfIndex = 0;
    INT4                i4OutCome = 0;
    UINT4               u4GetFsWlanBeaconsSentCount = 0;
    UINT4               u4GetFsWlanProbeReqRcvdCount = 0;
    UINT4               u4GetFsWlanProbeRespSentCount = 0;
    UINT4               u4GetFsWlanDataPktRcvdCount = 0;
    UINT4               u4GetFsWlanDataPktSentCount = 0;
    INT4                i4RowStatus = 0;
    tWssWlanDB          wssWlanDB;
    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));

    i4OutCome =
        nmhGetCapwapDot11WlanRowStatus (*pu4CurrentCapwapDot11WlanProfileId,
                                        &i4RowStatus);
    if (i4OutCome == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n No Entry present\r\n");
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle,
               "\r\nWlanProfileId BeaconSent ProbeReqRcvd ProbeRespSent DataRcvd DataSent");
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, "                            in(KB)   in(KB)  ");
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle,
               "------------- ---------- ------------ ------------- -------- --------- ");
    CliPrintf (CliHandle, "\r\n");

    wssWlanDB.WssWlanAttributeDB.u2WlanProfileId =
        (UINT2) *pu4CurrentCapwapDot11WlanProfileId;
    wssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_PROFILE_ENTRY, &wssWlanDB)
        == OSIX_SUCCESS)
    {
        i4WlanIfIndex = (INT4) wssWlanDB.WssWlanAttributeDB.u4WlanIfIndex;
    }

    if (nmhGetFsWlanBeaconsSentCount (i4WlanIfIndex,
                                      &u4GetFsWlanBeaconsSentCount)
        != SNMP_FAILURE)
    {
    }
    if (nmhGetFsWlanProbeReqRcvdCount (i4WlanIfIndex,
                                       &u4GetFsWlanProbeReqRcvdCount)
        != SNMP_FAILURE)
    {
    }
    if (nmhGetFsWlanProbeRespSentCount (i4WlanIfIndex,
                                        &u4GetFsWlanProbeRespSentCount)
        != SNMP_FAILURE)
    {
    }
    if (nmhGetFsWlanDataPktRcvdCount (i4WlanIfIndex,
                                      &u4GetFsWlanDataPktRcvdCount)
        != SNMP_FAILURE)
    {
    }
    if (nmhGetFsWlanDataPktSentCount (i4WlanIfIndex,
                                      &u4GetFsWlanDataPktSentCount)
        != SNMP_FAILURE)
    {
    }
    CliPrintf (CliHandle, "\r\n%-16d", *pu4CurrentCapwapDot11WlanProfileId);
    CliPrintf (CliHandle, "%-13u", u4GetFsWlanBeaconsSentCount);
    CliPrintf (CliHandle, "%-13u", u4GetFsWlanProbeReqRcvdCount);
    CliPrintf (CliHandle, "%-14u", u4GetFsWlanProbeRespSentCount);
    CliPrintf (CliHandle, "%-8u", u4GetFsWlanDataPktRcvdCount);
    CliPrintf (CliHandle, "%-10u", u4GetFsWlanDataPktSentCount);
    CliPrintf (CliHandle, "\r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WssCfgShowApWlanStatistics
* Description :
* Input       :  CliHandle 
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowApWlanStatistics (tCliHandle CliHandle,
                            UINT1 *pu1CapwapBaseWtpProfileName,
                            UINT4 *pu4RadioId, UINT4 *pu4WlanId)
{
    UINT1               u1Count = 0;
    UINT1               u1WlanId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4BssIfIndex = 0;
    UINT4               u4WlanProfileId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4GetFsApWlanBeaconsSentCount = 0;
    UINT4               u4GetFsApWlanProbeReqRcvdCount = 0;
    UINT4               u4GetFsApWlanProbeRespSentCount = 0;
    UINT4               u4GetFsApWlanDataPktRcvdCount = 0;
    UINT4               u4GetFsApWlanDataPktSentCount = 0;
    UINT1               au1SSID[32];
    tWssWlanDB          wssWlanDB;

    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (au1SSID, 0, sizeof (au1SSID));

    if (pu1CapwapBaseWtpProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1CapwapBaseWtpProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n No entry found\r\n");
            return CLI_FAILURE;
        }
        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
            (u4WtpProfileId, *pu4RadioId, &i4RadioIfIndex) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r\n Invalid interface index %d \r\n", *pu4RadioId);
            return CLI_SUCCESS;
        }
        CliPrintf (CliHandle,
                   "\r\nWlanId   WLAN Profile Name / SSID    BeaconSent ProbeReqRcvd ProbeRespSent DataRcvd DataSent");
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle,
                   "                                                                     in(KB)   in(KB) ");
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle,
                   "-------  ------------------------    ---------- ------------ ------------- -------- --------- ");
        CliPrintf (CliHandle, "\r\n");

        if (*pu4WlanId == 0)
        {
            for (u1WlanId = 1, u1Count = 0; u1WlanId <= CLI_WLAN_BSS_MAX_ID;
                 u1WlanId++)
            {
                u4BssIfIndex = 0;
                WssWlanGetDot11BssIfIndex ((UINT4)
                                           i4RadioIfIndex,
                                           u1WlanId, &u4BssIfIndex);
                if (u4BssIfIndex != 0)
                {
                    wssWlanDB.WssWlanAttributeDB.u4BssIfIndex = u4BssIfIndex;
                    wssWlanDB.WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
                    wssWlanDB.WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;

                    if (WssIfProcessWssWlanDBMsg
                        (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                         &wssWlanDB) != OSIX_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n WlanProfileId not found\r\n");
                        continue;
                    }
                    u4WlanProfileId =
                        (UINT4) wssWlanDB.WssWlanAttributeDB.u2WlanProfileId;
                    MEMSET (au1SSID, 0, WSSWLAN_SSID_NAME_LEN);
                    WssWlanGetDot11SSID (u4BssIfIndex, au1SSID);

                    if (nmhGetFsApWlanBeaconsSentCount (i4RadioIfIndex,
                                                        u4WlanProfileId,
                                                        &u4GetFsApWlanBeaconsSentCount)
                        != SNMP_FAILURE)
                    {
                    }
                    if (nmhGetFsApWlanProbeReqRcvdCount (i4RadioIfIndex,
                                                         u4WlanProfileId,
                                                         &u4GetFsApWlanProbeReqRcvdCount)
                        != SNMP_FAILURE)
                    {
                    }
                    if (nmhGetFsApWlanProbeRespSentCount (i4RadioIfIndex,
                                                          u4WlanProfileId,
                                                          &u4GetFsApWlanProbeRespSentCount)
                        != SNMP_FAILURE)
                    {
                    }
                    if (nmhGetFsApWlanDataPktRcvdCount (i4RadioIfIndex,
                                                        u4WlanProfileId,
                                                        &u4GetFsApWlanDataPktRcvdCount)
                        != SNMP_FAILURE)
                    {
                    }
                    if (nmhGetFsApWlanDataPktSentCount (i4RadioIfIndex,
                                                        u4WlanProfileId,
                                                        &u4GetFsApWlanDataPktSentCount)
                        != SNMP_FAILURE)
                    {
                    }
                    CliPrintf (CliHandle, "\r\n%-10d", u1WlanId);
                    CliPrintf (CliHandle, "%-32s", au1SSID);
                    CliPrintf (CliHandle, "%-13u",
                               u4GetFsApWlanBeaconsSentCount);
                    CliPrintf (CliHandle, "%-13u",
                               u4GetFsApWlanProbeReqRcvdCount);
                    CliPrintf (CliHandle, "%-11u",
                               u4GetFsApWlanProbeRespSentCount);
                    CliPrintf (CliHandle, "%-10u",
                               u4GetFsApWlanDataPktRcvdCount);
                    CliPrintf (CliHandle, "%-10u",
                               u4GetFsApWlanDataPktSentCount);
                    CliPrintf (CliHandle, "\r\n");
                    u1Count++;
                }
            }
            if (u1Count == 0)
            {
                CliPrintf (CliHandle, "\r\n No entry found\r\n");
            }
        }
        else
        {
            u4BssIfIndex = 0;
            WssWlanGetDot11BssIfIndex ((UINT4)
                                       i4RadioIfIndex,
                                       (UINT1) *pu4WlanId, &u4BssIfIndex);
            if (u4BssIfIndex != 0)
            {
                wssWlanDB.WssWlanAttributeDB.u4BssIfIndex = u4BssIfIndex;
                wssWlanDB.WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
                wssWlanDB.WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;

                if (WssIfProcessWssWlanDBMsg
                    (WSS_WLAN_GET_BSS_IFINDEX_ENTRY,
                     &wssWlanDB) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n WlanProfileId not found\r\n");
                    return CLI_FAILURE;
                }
                u4WlanProfileId =
                    (UINT4) wssWlanDB.WssWlanAttributeDB.u2WlanProfileId;
                WssWlanGetDot11SSID (u4BssIfIndex, au1SSID);

                if (nmhGetFsApWlanBeaconsSentCount (i4RadioIfIndex,
                                                    u4WlanProfileId,
                                                    &u4GetFsApWlanBeaconsSentCount)
                    != SNMP_FAILURE)
                {
                }
                if (nmhGetFsApWlanProbeReqRcvdCount (i4RadioIfIndex,
                                                     u4WlanProfileId,
                                                     &u4GetFsApWlanProbeReqRcvdCount)
                    != SNMP_FAILURE)
                {
                }
                if (nmhGetFsApWlanProbeRespSentCount (i4RadioIfIndex,
                                                      u4WlanProfileId,
                                                      &u4GetFsApWlanProbeRespSentCount)
                    != SNMP_FAILURE)
                {
                }
                if (nmhGetFsApWlanDataPktRcvdCount (i4RadioIfIndex,
                                                    u4WlanProfileId,
                                                    &u4GetFsApWlanDataPktRcvdCount)
                    != SNMP_FAILURE)
                {
                }
                if (nmhGetFsApWlanDataPktSentCount (i4RadioIfIndex,
                                                    u4WlanProfileId,
                                                    &u4GetFsApWlanDataPktSentCount)
                    != SNMP_FAILURE)
                {
                }
                CliPrintf (CliHandle, "\r\n%-10d", *pu4WlanId);
                CliPrintf (CliHandle, "%-32s", au1SSID);
                CliPrintf (CliHandle, "%-13u", u4GetFsApWlanBeaconsSentCount);
                CliPrintf (CliHandle, "%-13u", u4GetFsApWlanProbeReqRcvdCount);
                CliPrintf (CliHandle, "%-11u", u4GetFsApWlanProbeRespSentCount);
                CliPrintf (CliHandle, "%-10u", u4GetFsApWlanDataPktRcvdCount);
                CliPrintf (CliHandle, "%-10u", u4GetFsApWlanDataPktSentCount);
                CliPrintf (CliHandle, "\r\n");
            }
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r\n AP Name missing\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WssCfgShowDiffServStatistics
* Description :
* Input       :  CliHandle
*
*
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowDiffServStatistics (tCliHandle CliHandle)
{
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    UINT4               u4CapwapDot11WlanProfileId = 0;
    UINT4               u4NextCapwapDot11WlanProfileId = 0;
    UINT4               u4RetValFsDot11DscpInPriority = 0;
    UINT4               u4RetValFsDot11OutDscp = 0;
    UINT1               u1retVal = 0;

    tWssIfCapDB        *pWssIfCapwapDB;
    tRadioIfGetDB       pRadioIfGetDB;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    if (nmhGetFirstIndexFsDot11DscpConfigTable (&i4NextIfIndex,
                                                &u4NextCapwapDot11WlanProfileId)
        != SNMP_FAILURE)
    {
        do
        {
            i4IfIndex = i4NextIfIndex;
            u4CapwapDot11WlanProfileId = u4NextCapwapDot11WlanProfileId;

            pRadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                (UINT4) i4NextIfIndex;
            pRadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
            pRadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            u1retVal =
                WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &pRadioIfGetDB);
            if (u1retVal != OSIX_SUCCESS)
            {
                continue;
            }
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                pRadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
                != OSIX_SUCCESS)
            {
                continue;
            }

            CliPrintf (CliHandle, "\r\nProfile Name    : %s\n",
                       pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
            CliPrintf (CliHandle, "============================\n");
            if (u4NextCapwapDot11WlanProfileId != 0)
            {
                CliPrintf (CliHandle, "Radio Id        : %d\n",
                           pRadioIfGetDB.RadioIfGetAllDB.u1RadioId);
                CliPrintf (CliHandle, "WLAN Profile Id : %d\n",
                           u4NextCapwapDot11WlanProfileId);
            }
            if (nmhGetFsDot11DscpInPriority
                (i4NextIfIndex, u4NextCapwapDot11WlanProfileId,
                 &u4RetValFsDot11DscpInPriority) == SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "In Priority     : %d\n",
                           u4RetValFsDot11DscpInPriority);
            }
            if (nmhGetFsDot11OutDscp
                (i4NextIfIndex, u4NextCapwapDot11WlanProfileId,
                 &u4RetValFsDot11OutDscp) == SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "Out Dscp        : %d\n",
                           u4RetValFsDot11OutDscp);
            }
        }
        while (nmhGetNextIndexFsDot11DscpConfigTable
               (i4IfIndex, &i4NextIfIndex, u4CapwapDot11WlanProfileId,
                &u4NextCapwapDot11WlanProfileId) != SNMP_FAILURE);
    }
    else
    {
        CliPrintf (CliHandle, "\r\nDiffServ not Configured\n");
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    : WssCfgSetOperRate  
 *
 * Description : This is a utility function to configure the rate for the given
 *               interface index
 *
 * Input       : u4ProfileId - WTP  Profile ID
 *               u4WlanId    - radio index
 *                 
 * Output      : u4WlanId -  Radio Index
 *
 * Returns     : OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/

UINT1
WssCfgSetOperRate (INT4 i4RadioIfIndex,
                   tSNMP_OCTET_STRING_TYPE * OperRate, INT4 i4Rate,
                   INT4 i4Status)
{
    UNUSED_PARAM (i4RadioIfIndex);

#if 0
    if (OperRate != NULL)
    {
        if (OperRate->pu1_OctetList != NULL)
        {
            MEMSET (OperRate->pu1_OctetList, 0, CLI_OPER_RATE);
        }
    }
#endif

    if (i4Status == CLI_OPER_RATE_DISABLED)
    {
        if (i4Rate == CLI_OPER_RATEB_1MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_1MB] = 0;
        }
        else if (i4Rate == CLI_OPER_RATEB_2MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_2MB] = 0;
        }
        else if (i4Rate == CLI_OPER_RATEB_5MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_5MB] = 0;
        }
        else if (i4Rate == CLI_OPER_RATEB_11MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_11MB] = 0;
        }
        else if (i4Rate == CLI_OPER_RATEA_6MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_6MB] = 0;
        }
        else if (i4Rate == CLI_OPER_RATEA_9MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_9MB] = 0;
        }
        else if (i4Rate == CLI_OPER_RATEA_12MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_12MB] = 0;
        }
        else if (i4Rate == CLI_OPER_RATEA_18MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_18MB] = 0;
        }
        else if (i4Rate == CLI_OPER_RATEA_24MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_24MB] = 0;
        }
        else if (i4Rate == CLI_OPER_RATEA_36MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_36MB] = 0;
        }
        else if (i4Rate == CLI_OPER_RATEA_48MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_48MB] = 0;
        }
        else if (i4Rate == CLI_OPER_RATEA_54MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_54MB] = 0;
        }
        else
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        if (i4Rate == CLI_OPER_RATEB_1MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_1MB] = (UINT1) i4Rate;
        }
        else if (i4Rate == CLI_OPER_RATEB_2MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_2MB] = (UINT1) i4Rate;
        }
        else if (i4Rate == CLI_OPER_RATEB_5MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_5MB] = (UINT1) i4Rate;
        }
        else if (i4Rate == CLI_OPER_RATEB_11MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_11MB] = (UINT1) i4Rate;
        }
        else if (i4Rate == CLI_OPER_RATEA_6MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_6MB] = (UINT1) i4Rate;
        }
        else if (i4Rate == CLI_OPER_RATEA_9MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_9MB] = (UINT1) i4Rate;
        }
        else if (i4Rate == CLI_OPER_RATEA_12MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_12MB] = (UINT1) i4Rate;
        }
        else if (i4Rate == CLI_OPER_RATEA_18MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_18MB] = (UINT1) i4Rate;
        }
        else if (i4Rate == CLI_OPER_RATEA_24MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_24MB] = (UINT1) i4Rate;
        }
        else if (i4Rate == CLI_OPER_RATEA_36MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_36MB] = (UINT1) i4Rate;
        }
        else if (i4Rate == CLI_OPER_RATEA_48MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_48MB] = (UINT1) i4Rate;
        }
        else if (i4Rate == CLI_OPER_RATEA_54MB)
        {
            OperRate->pu1_OctetList[CLI_OPER_INDEX_54MB] = (UINT1) i4Rate;
        }
        else
        {
            return CLI_FAILURE;
        }
    }
    OperRate->i4_Length = CLI_OPER_RATE;
    return OSIX_SUCCESS;
}

INT4
WsscfgCliWtpModelProfileCmp (UINT4 u4WtpProfileId,
                             tWsscfgFsWtpImageUpgradeEntry *
                             pWsscfgSetFsWtpImageUpgradeEntry)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    pWssIfCapwapDB->CapwapGetDB.u2ProfileId = u4WtpProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfConfigUpdateRsp : Getting Radio index failed\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return CLI_FAILURE;
    }

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_FALSE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpModelNumber = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return CLI_FAILURE;
    }
    if (MEMCMP (pWssIfCapwapDB->CapwapGetDB.au1WtpModelNumber,
                pWsscfgSetFsWtpImageUpgradeEntry->MibObject.
                au1CapwapBaseWtpProfileWtpModelNumber,
                sizeof (pWssIfCapwapDB->CapwapGetDB.au1WtpModelNumber)) != 0)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return CLI_FAILURE;
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return CLI_SUCCESS;

}

/****************************************************************************
 * Function    : WsscfgCliShowWtpImageUpgradeTable 
 * Description :  This function is used to display status of upgrade.

 * Input       : Profile Id , Profile Name 

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliShowWtpImageUpgradeTable (tCliHandle CliHandle,
                                   UINT4 u4WtpProfileId, UINT1 *pu1WtpName)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT2               u2WtpProfileId = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    if (WssIfGetProfileIndex (u4WtpProfileId, &u2WtpProfileId) == OSIX_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return CLI_FAILURE;
    }
    pWssIfCapwapDB->CapwapIsGetAllDB.bImageTransfer = OSIX_TRUE;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpProfileId;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return CLI_FAILURE;
    }
    if (pWssIfCapwapDB->CapwapGetDB.u1ImageTransferFlag == OSIX_TRUE)
    {
        CliPrintf (CliHandle,
                   "\r\n Image upgrade is in Progress for Profile %s\r\n",
                   pu1WtpName);
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n Image upgrade Completed/not initiated for Profile %s\r\n",
                   pu1WtpName);
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    : WssCfgCliSetStatusAllowedChannelWidth
 *
 * Description : This is a utility function to set type of allowd channel 
 *               width per radio index
 *
 * Input       : u4ProfileId - WTP  Profile ID
 *               u4RadioId   - radio index
 *                 
 * Output      : u4WlanId -  Radio Index
 *
 * Returns     : OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/

INT4
WssCfgCliSetStatusAllowedChannelWidth (tCliHandle CliHandle, INT4 i4RadioType,
                                       UINT1 u1AllowChannelWidth,
                                       UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4RetStatus = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               currentProfileId = 0;
    UINT4               u4GetRadioType = 0;
    UINT1               u1RadioIndex = 0;
    UINT4               u4NoOfRadio = 0;
    tSNMP_OCTET_STRING_TYPE ModelName;
    UINT1               au1ModelNumber[256];
    UINT2               u2WtpProfileId = 0;
    UINT4               u4ErrCode = 0;
    MEMSET (&au1ModelNumber, 0, sizeof (au1ModelNumber));

    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            i4RetStatus = OSIX_FAILURE;
        }
        else
        {
            do
            {
                currentProfileId = nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if ((currentProfileId == 0) || (u4currentBindingId == 0))
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                u4GetRadioType = (UINT4) i4RadioType;
                i4RetStatus = WssCfgGetDot11RadioIfIndex (nextProfileId,
                                                          u4nextBindingId,
                                                          u4GetRadioType,
                                                          &i4RadioIfIndex);

                if (i4RetStatus == OSIX_FAILURE)
                {
                    continue;
                }
                else
                {
                    if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                                &u4GetRadioType) !=
                        SNMP_SUCCESS)
                    {
                        return OSIX_FAILURE;
                    }

                    if (WSSCFG_RADIO_TYPEN_AC_COMP (u4GetRadioType))
                    {
                        if (nmhTestv2FsDot11nConfigAllowChannelWidth
                            (&u4ErrCode, i4RadioIfIndex,
                             u1AllowChannelWidth) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle, "\r\nInvalid input \r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = CLI_FAILURE;
                            break;
                        }
                        if (nmhSetFsDot11nConfigAllowChannelWidth
                            (i4RadioIfIndex,
                             u1AllowChannelWidth) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d:  DB Updation failed \r\n",
                                       u1RadioIndex);
                            continue;
                        }
                    }
                    else
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;

                    }
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (currentProfileId, &nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
    }
    else
    {
        ModelName.pu1_OctetList = au1ModelNumber;
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n WTP Profile not found\r\n");
            CLI_FATAL_ERROR (CliHandle);
            i4RetStatus = OSIX_FAILURE;
        }
        if ((*pu4RadioId == CLI_WTP_RADIO_ALL))
        {
            if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r\nFor Ap %d: Invalid Model number\r\n",
                           u4WtpProfileId);
                CLI_FATAL_ERROR (CliHandle);
            }
            if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r\nFailed to fetch number of radios\r\n");
                CLI_FATAL_ERROR (CliHandle);
            }
            for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio; u1RadioIndex++)
            {
                if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                    (u4WtpProfileId, u1RadioIndex,
                     &i4RadioIfIndex) != SNMP_SUCCESS)
                {
                    continue;
                }
                if (nmhGetFsDot11RadioType
                    (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                {
                    continue;
                }

                if (WSSCFG_RADIO_TYPEN_AC_COMP (u4GetRadioType))
                {
                    if (WSSCFG_RADIO_TYPE_COMP (i4RadioType))
                    {
                        if (nmhTestv2FsDot11nConfigAllowChannelWidth
                            (&u4ErrCode, i4RadioIfIndex,
                             u1AllowChannelWidth) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle, "\r\nInvalid input \r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            return CLI_FAILURE;
                        }

                        if (nmhSetFsDot11nConfigAllowChannelWidth
                            (i4RadioIfIndex,
                             u1AllowChannelWidth) != SNMP_SUCCESS)
                        {
                            continue;
                        }
                    }
                }
                else
                {
                    continue;
                }
            }
            i4RetStatus = OSIX_SUCCESS;
        }
        else
        {
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, (UINT1) *pu4RadioId,
                 &i4RadioIfIndex) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r\nFor Ap %d Radio %d: Invalid IfIndex\r\n",
                           u2WtpProfileId, u1RadioIndex);
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = CLI_FAILURE;
            }
            if (nmhGetFsDot11RadioType
                (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r\nFor Radio %d:  Radio Type could not successfully obtained\r\n",
                           u1RadioIndex);
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = CLI_FAILURE;
            }
            if (WSSCFG_RADIO_TYPEN_AC_COMP (u4GetRadioType))
            {
                if (nmhTestv2FsDot11nConfigAllowChannelWidth
                    (&u4ErrCode, i4RadioIfIndex,
                     u1AllowChannelWidth) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\nInvalid input \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
                if (nmhSetFsDot11nConfigAllowChannelWidth
                    (i4RadioIfIndex, u1AllowChannelWidth) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nFor Radio %d:  DB Updation failed \r\n",
                               u1RadioIndex);
                }
            }
            else
            {
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = CLI_FAILURE;
            }
        }
    }
    UNUSED_PARAM (i4RetStatus);
    return i4RetStatus;
}
#endif
