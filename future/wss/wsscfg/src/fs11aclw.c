/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fs11aclw.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: Protocol Low Level Routines
 *********************************************************************/
#include  "lr.h"
#include  "fssnmp.h"
#include  "fs11aclw.h"
#include  "wssifinc.h"
#include  "wsscfglwg.h"
#include  "wsscfgcli.h"
#include  "radioifproto.h"
#include  "fs1acwrp.h"

/* LOW LEVEL Routines for Table : FsDot11ACConfigTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsDot11ACConfigTable
Input       :  The Indices
IfIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot11ACConfigTable (INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsDot11ACConfigTable
Input       :  The Indices
IfIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDot11ACConfigTable (INT4 *pi4IfIndex)
{
    tRadioIfDB         *pRadioIfDB = NULL;
    UINT4               u4RadioType = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4NextIfIndex = 0;

    pRadioIfDB =
        (tRadioIfDB *) RBTreeGetFirst (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB);
    if (pRadioIfDB != NULL)
    {
        i4RadioIfIndex = (INT4) pRadioIfDB->u4VirtualRadioIfIndex;
        do
        {
        if (nmhGetFsDot11RadioType (i4RadioIfIndex, &u4RadioType) 
                != SNMP_SUCCESS)
        {
            CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
            return SNMP_FAILURE;

        }
        if (u4RadioType != RADIO_TYPE_AC)
        {
            if(nmhGetNextIndexFsDot11ACConfigTable(i4RadioIfIndex, 
                            &i4NextIfIndex)==SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
                return SNMP_FAILURE;    
            }
            else
            {
                i4RadioIfIndex = i4NextIfIndex;
            }
        }
        else
        {
            break;
        }

        }while(u4RadioType != RADIO_TYPE_AC);
        
        *pi4IfIndex = i4RadioIfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsDot11ACConfigTable
Input       :  The Indices
ifIndex
nextIfIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot11ACConfigTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    tRadioIfDB          RadioIfDB;
    tRadioIfDB         *pNextRadioIfDB = NULL;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4RadioType = 0;


    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));
    RadioIfDB.u4VirtualRadioIfIndex = (UINT4) i4IfIndex;

    pNextRadioIfDB =
        (tRadioIfDB *) 
        RBTreeGetNext (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                                      (tRBElem *) & RadioIfDB, NULL);
    if (pNextRadioIfDB != NULL)
    {
        i4RadioIfIndex = (INT4) pNextRadioIfDB->u4VirtualRadioIfIndex;
        RadioIfDB.u4VirtualRadioIfIndex = (UINT4) i4RadioIfIndex;
        do
        {
        if (nmhGetFsDot11RadioType (i4RadioIfIndex, &u4RadioType) 
                != SNMP_SUCCESS)
        {
            CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
            return SNMP_FAILURE;

        }
        if (u4RadioType != RADIO_TYPE_AC)
        {
            pNextRadioIfDB = (tRadioIfDB *) 
                RBTreeGetNext (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                                        (tRBElem *) & RadioIfDB, NULL);
            if (pNextRadioIfDB == NULL)
            {
                CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
                return SNMP_FAILURE;
            }
            else
            {
                i4RadioIfIndex = (INT4) pNextRadioIfDB->u4VirtualRadioIfIndex;
                RadioIfDB.u4VirtualRadioIfIndex = (UINT4) i4RadioIfIndex;
            }
        }
        else
        {
            break;
        }
        }while(u4RadioType != RADIO_TYPE_AC);

        *pi4NextIfIndex = (INT4) pNextRadioIfDB->u4VirtualRadioIfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsDot11ACMaxMPDULength
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACMaxMPDULength
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACMaxMPDULength (INT4 i4IfIndex,
                              INT4 *pi4RetValFsDot11ACMaxMPDULength)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppMaxMPDULen = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11ACMaxMPDULength =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppMaxMPDULen;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsDot11ACMaxMPDULengthConfig
Input       :  The Indices
IfIndex

The Object
retValFsDot11ACMaxMPDULengthConfig
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACMaxMPDULengthConfig (INT4 i4IfIndex,
                                    INT4 *pi4RetValFsDot11ACMaxMPDULengthConfig)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bMaxMPDULen = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11ACMaxMPDULengthConfig
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1MaxMPDULen;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTMaxRxAMPDUFactor
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTMaxRxAMPDUFactor
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTMaxRxAMPDUFactor (INT4 i4IfIndex,
                                    UINT4
                                    *pu4RetValFsDot11ACVHTMaxRxAMPDUFactor)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppVhtMaxAMPDU = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsDot11ACVHTMaxRxAMPDUFactor =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
        u1SuppVhtMaxAMPDU;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTMaxRxAMPDUFactorConfig
Input       :  The Indices
IfIndex

The Object
retValFsDot11ACVHTMaxRxAMPDUFactorConfig
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsDot11ACVHTMaxRxAMPDUFactorConfig (INT4 i4IfIndex,
                                          UINT4
                                          *pu4RetValFsDot11ACVHTMaxRxAMPDUFactorConfig)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bVhtMaxAMPDU = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsDot11ACVHTMaxRxAMPDUFactorConfig
        = (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1VhtMaxAMPDU;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTControlFieldSupported
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTControlFieldSupported
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTControlFieldSupported (INT4 i4IfIndex,
                                         INT4
                                         *pi4RetValFsDot11ACVHTControlFieldSupported)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bHtcVhtCapable = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11ACVHTControlFieldSupported
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1HtcVhtCapable;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTTXOPPowerSaveOptionImplemented
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTTXOPPowerSaveOptionImplemented
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTTXOPPowerSaveOptionImplemented (INT4 i4IfIndex,
                                                  INT4
                                                  *pi4RetValFsDot11ACVHTTXOPPowerSaveOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bVhtTxOpPs = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11ACVHTTXOPPowerSaveOptionImplemented
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1VhtTxOpPs;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTRxMCSMap
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTRxMCSMap
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTRxMCSMap (INT4 i4IfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsDot11ACVHTRxMCSMap)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    UINT1               au1Dot11RxMCSValue[FS11AC_SIZE8];
    UINT1               au1RxTemp[FS11AC_SIZE2];
    UINT1               u1loop1 = 0;
    UINT1               u1loop2 = FS11AC_MCS_HALFVALUE;
    UINT1               u1looptemp = 0;
    UINT1               u1shift = FS11AC_MCS_SHIFTVALUE;
    UINT1               u1shiftleft = FS11AC_MCS_SHIFTVALUE;

    MEMSET (au1RxTemp, 0, FS11AC_SIZE2);
    MEMSET (au1Dot11RxMCSValue, 0, FS11AC_SIZE8);

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppVhtCapaMcs = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (au1RxTemp,
            RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.au1SuppVhtCapaMcs,
            sizeof (au1RxTemp));
    for (u1loop1 = 0; u1loop1 < FS11AC_MCS_HALFVALUE; u1loop1++)
    {
        u1looptemp = au1RxTemp[0];
        u1looptemp = (UINT1) (u1looptemp << u1shiftleft);
        u1shiftleft = (UINT1) (u1shiftleft - FS11AC_MCS_SHIFTMINUS);
        u1looptemp = (UINT1) (u1looptemp >> u1shift);
        au1Dot11RxMCSValue[u1loop1] = u1looptemp;
    }
    u1shiftleft = FS11AC_MCS_SHIFTVALUE;
    for (u1loop2 = FS11AC_MCS_HALFVALUE; u1loop2 < FS11AC_MCS_SPATIALVALUE;
         u1loop2++)
    {
        u1looptemp = au1RxTemp[1];
        u1looptemp = (UINT1) (u1looptemp << u1shiftleft);
        u1shiftleft = (UINT1) (u1shiftleft - FS11AC_MCS_SHIFTMINUS);
        u1looptemp = (UINT1) (u1looptemp >> u1shift);
        au1Dot11RxMCSValue[u1loop2] = u1looptemp;
    }
    MEMCPY (pRetValFsDot11ACVHTRxMCSMap->pu1_OctetList,
            au1Dot11RxMCSValue, FS11AC_SIZE8);
    pRetValFsDot11ACVHTRxMCSMap->i4_Length = FS11AC_SIZE8;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTRxHighestDataRateSupported
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTRxHighestDataRateSupported
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTRxHighestDataRateSupported (INT4 i4IfIndex,
                        UINT4
                        *pu4RetValFsDot11ACVHTRxHighestDataRateSupported)
{
    UINT4 u4RetValFsDot11ACVHTRxHighestDataRateSupported= 0;
    INT1 i1Result = 0;
    i1Result = wsscfgGetFsDot11ACVHTRxHighestDataRateSupported(i4IfIndex,
            &u4RetValFsDot11ACVHTRxHighestDataRateSupported);
    if(i1Result == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsDot11ACVHTRxHighestDataRateSupported =
            u4RetValFsDot11ACVHTRxHighestDataRateSupported;
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTTxMCSMap
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTTxMCSMap
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTTxMCSMap (INT4 i4IfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsDot11ACVHTTxMCSMap)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    UINT1               au1Dot11TxMCSValue[FS11AC_SIZE8];
    UINT1               au1TxTemp[FS11AC_SIZE2];
    UINT1               u1loop1 = 0;
    UINT1               u1loop2 = FS11AC_MCS_HALFVALUE;
    UINT1               u1looptemp = 0;
    UINT1               u1shift = FS11AC_MCS_SHIFTVALUE;
    UINT1               u1shiftleft = FS11AC_MCS_SHIFTVALUE;

    MEMSET (au1TxTemp, 0, FS11AC_SIZE2);
    MEMSET (au1Dot11TxMCSValue, 0, FS11AC_SIZE8);

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppVhtCapaMcs = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (au1TxTemp,
            &RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
            au1SuppVhtCapaMcs[4], FS11AC_SIZE2);
    for (u1loop1 = 0; u1loop1 < FS11AC_MCS_HALFVALUE; u1loop1++)
    {
        u1looptemp = au1TxTemp[0];
        u1looptemp = (UINT1) (u1looptemp << u1shiftleft);
        u1shiftleft = (UINT1) (u1shiftleft - FS11AC_MCS_SHIFTMINUS);
        u1looptemp = (UINT1) (u1looptemp >> u1shift);
        au1Dot11TxMCSValue[u1loop1] = u1looptemp;
    }
    u1shiftleft = FS11AC_MCS_SHIFTVALUE;
    for (u1loop2 = FS11AC_MCS_HALFVALUE; u1loop2 < FS11AC_MCS_SPATIALVALUE;
         u1loop2++)
    {
        u1looptemp = au1TxTemp[1];
        u1looptemp = (UINT1) (u1looptemp << u1shiftleft);
        u1shiftleft = (UINT1) (u1shiftleft - FS11AC_MCS_SHIFTMINUS);
        u1looptemp = (UINT1) (u1looptemp >> u1shift);
        au1Dot11TxMCSValue[u1loop2] = u1looptemp;
    }
    MEMCPY (pRetValFsDot11ACVHTTxMCSMap->pu1_OctetList,
            au1Dot11TxMCSValue, FS11AC_SIZE8);
    pRetValFsDot11ACVHTTxMCSMap->i4_Length = FS11AC_SIZE8;

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTTxHighestDataRateSupported
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTTxHighestDataRateSupported
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTTxHighestDataRateSupported (INT4 i4IfIndex,
                                              UINT4
                                              *pu4RetValFsDot11ACVHTTxHighestDataRateSupported)
{
    UINT4 u4RetValFsDot11ACVHTTxHighestDataRateSupported= 0;
    INT1 i1Result = 0;
    i1Result = wsscfgGetFsDot11ACVHTTxHighestDataRateSupported(i4IfIndex,&u4RetValFsDot11ACVHTTxHighestDataRateSupported);
    if(i1Result == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsDot11ACVHTTxHighestDataRateSupported= u4RetValFsDot11ACVHTTxHighestDataRateSupported;
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTOBSSScanCount

Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTOBSSScanCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTOBSSScanCount (INT4 i4IfIndex,
                                 UINT4 *pu4RetValFsDot11ACVHTOBSSScanCount)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsDot11ACVHTOBSSScanCount);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDot11ACCurrentChannelBandwidth
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACCurrentChannelBandwidth
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACCurrentChannelBandwidth (INT4 i4IfIndex,
                                        INT4
                                        *pi4RetValFsDot11ACCurrentChannelBandwidth)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppVhtChannelWidth = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11ACCurrentChannelBandwidth =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.
        u1SuppVhtChannelWidth;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsDot11ACCurrentChannelBandwidthConfig
Input       :  The Indices
IfIndex

The Object
retValFsDot11ACCurrentChannelBandwidthConfig
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACCurrentChannelBandwidthConfig (INT4 i4IfIndex,
                                              INT4
                                              *pi4RetValFsDot11ACCurrentChannelBandwidthConfig)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bVhtChannelWidth = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11ACCurrentChannelBandwidthConfig =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1VhtChannelWidth;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsDot11ACCurrentChannelCenterFrequencyIndex0
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACCurrentChannelCenterFrequencyIndex0
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACCurrentChannelCenterFrequencyIndex0 (INT4 i4IfIndex,
                                                    UINT4
                                                    *pu4RetValFsDot11ACCurrentChannelCenterFrequencyIndex0)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppCenterFcy0 = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsDot11ACCurrentChannelCenterFrequencyIndex0
        =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1SuppCenterFcy0;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsDot11ACCurrentChannelCenterFrequencyIndex0Config
Input       :  The Indices
IfIndex

The Object
retValFsDot11ACCurrentChannelCenterFrequencyIndex0Config
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACCurrentChannelCenterFrequencyIndex0Config (INT4 i4IfIndex,
                                                          UINT4
                                                          *pu4RetValFsDot11ACCurrentChannelCenterFrequencyIndex0Config)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bCenterFcy0 = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB)
        == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsDot11ACCurrentChannelCenterFrequencyIndex0Config =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy0;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsDot11ACCurrentChannelCenterFrequencyIndex1
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACCurrentChannelCenterFrequencyIndex1
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACCurrentChannelCenterFrequencyIndex1 (INT4 i4IfIndex,
                                                    UINT4
                                                    *pu4RetValFsDot11ACCurrentChannelCenterFrequencyIndex1)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bCenterFcy1 = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsDot11ACCurrentChannelCenterFrequencyIndex1
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy1;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTShortGIOptionIn80Implemented
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTShortGIOptionIn80Implemented
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTShortGIOptionIn80Implemented (INT4 i4IfIndex,
                                                INT4
                                                *pi4RetValFsDot11ACVHTShortGIOptionIn80Implemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppShortGi80 = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11ACVHTShortGIOptionIn80Implemented
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppShortGi80;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTShortGIOptionIn80Activated
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTShortGIOptionIn80Activated
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTShortGIOptionIn80Activated (INT4 i4IfIndex,
                                              INT4
                                              *pi4RetValFsDot11ACVHTShortGIOptionIn80Activated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bShortGi80 = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11ACVHTShortGIOptionIn80Activated
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1ShortGi80;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTShortGIOptionIn160and80p80Implemented
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTShortGIOptionIn160and80p80Implemented
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTShortGIOptionIn160and80p80Implemented (INT4 i4IfIndex,
                                                         INT4
                                                         *pi4RetValFsDot11ACVHTShortGIOptionIn160and80p80Implemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bShortGi160 = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11ACVHTShortGIOptionIn160and80p80Implemented
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1ShortGi160;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTShortGIOptionIn160and80p80Activated
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTShortGIOptionIn160and80p80Activated
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTShortGIOptionIn160and80p80Activated (INT4 i4IfIndex,
                                                       INT4
                                                       *pi4RetValFsDot11ACVHTShortGIOptionIn160and80p80Activated)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4RetValFsDot11ACVHTShortGIOptionIn160and80p80Activated);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTLDPCCodingOptionImplemented
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTLDPCCodingOptionImplemented
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTLDPCCodingOptionImplemented (INT4 i4IfIndex,
                                               INT4
                                               *pi4RetValFsDot11ACVHTLDPCCodingOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppRxLpdc = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11ACVHTLDPCCodingOptionImplemented
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppRxLpdc;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTLDPCCodingOptionActivated
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTLDPCCodingOptionActivated
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTLDPCCodingOptionActivated (INT4 i4IfIndex,
                                             INT4
                                             *pi4RetValFsDot11ACVHTLDPCCodingOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRxLpdc = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11ACVHTLDPCCodingOptionActivated
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1RxLpdc;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTTxSTBCOptionImplemented
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTTxSTBCOptionImplemented
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTTxSTBCOptionImplemented (INT4 i4IfIndex,
                                           INT4
                                           *pi4RetValFsDot11ACVHTTxSTBCOptionImplemented)
{
    INT4 i4RetValFsDot11ACVHTTxSTBCOptionImplemented= 0;
    INT1 i1Result = 0;
    i1Result = wsscfgGetFsDot11ACVHTTxSTBCOptionImplemented(i4IfIndex,&i4RetValFsDot11ACVHTTxSTBCOptionImplemented);
    if(i1Result == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsDot11ACVHTTxSTBCOptionImplemented = i4RetValFsDot11ACVHTTxSTBCOptionImplemented;
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTTxSTBCOptionActivated
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTTxSTBCOptionActivated
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTTxSTBCOptionActivated (INT4 i4IfIndex,
                                         INT4
                                         *pi4RetValFsDot11ACVHTTxSTBCOptionActivated)
{
    INT4 i4RetValFsDot11ACVHTTxSTBCOptionActivated = 0;
    INT1 i1Result = 0;
    i1Result = wsscfgGetFsDot11ACVHTTxSTBCOptionActivated(i4IfIndex,&i4RetValFsDot11ACVHTTxSTBCOptionActivated);
    if(i1Result == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsDot11ACVHTTxSTBCOptionActivated = i4RetValFsDot11ACVHTTxSTBCOptionActivated;
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTRxSTBCOptionImplemented
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTRxSTBCOptionImplemented
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTRxSTBCOptionImplemented (INT4 i4IfIndex,
                                           INT4
                                           *pi4RetValFsDot11ACVHTRxSTBCOptionImplemented)
{
    INT4 i4RetValFsDot11ACVHTRxSTBCOptionImplemented= 0;
    INT1 i1Result = 0;
    i1Result = wsscfgGetFsDot11ACVHTRxSTBCOptionImplemented(i4IfIndex,&i4RetValFsDot11ACVHTRxSTBCOptionImplemented);
    if(i1Result == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsDot11ACVHTRxSTBCOptionImplemented = i4RetValFsDot11ACVHTRxSTBCOptionImplemented;
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTRxSTBCOptionActivated
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTRxSTBCOptionActivated
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTRxSTBCOptionActivated (INT4 i4IfIndex,
                                         INT4
                                         *pi4RetValFsDot11ACVHTRxSTBCOptionActivated)
{
    INT4 i4RetValFsDot11ACVHTRxSTBCOptionActivated= 0;
    INT1 i1Result = 0;
    i1Result = wsscfgGetFsDot11ACVHTRxSTBCOptionActivated(i4IfIndex,&i4RetValFsDot11ACVHTRxSTBCOptionActivated);
    if(i1Result == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsDot11ACVHTRxSTBCOptionActivated = i4RetValFsDot11ACVHTRxSTBCOptionActivated;
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTMUMaxUsersImplemented
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTMUMaxUsersImplemented
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTMUMaxUsersImplemented (INT4 i4IfIndex,
                                         UINT4
                                         *pu4RetValFsDot11ACVHTMUMaxUsersImplemented)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsDot11ACVHTMUMaxUsersImplemented);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTMUMaxNSTSPerUserImplemented
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTMUMaxNSTSPerUserImplemented
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTMUMaxNSTSPerUserImplemented (INT4 i4IfIndex,
                                               UINT4
                                               *pu4RetValFsDot11ACVHTMUMaxNSTSPerUserImplemented)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsDot11ACVHTMUMaxNSTSPerUserImplemented);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTMUMaxNSTSTotalImplemented
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTMUMaxNSTSTotalImplemented
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTMUMaxNSTSTotalImplemented (INT4 i4IfIndex,
                                             UINT4
                                             *pu4RetValFsDot11ACVHTMUMaxNSTSTotalImplemented)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsDot11ACVHTMUMaxNSTSTotalImplemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDot11ACSuBeamFormer
Input       :  The Indices
IfIndex

The Object
retValFsDot11ACSuBeamFormer
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACSuBeamFormer (INT4 i4IfIndex,
                             INT4 *pi4RetValFsDot11ACSuBeamFormer)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuBeamFormer = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11ACSuBeamFormer
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuBeamFormer;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDot11ACSuBeamFormee
Input       :  The Indices
IfIndex

The Object
retValFsDot11ACSuBeamFormee
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACSuBeamFormee (INT4 i4IfIndex,
                             INT4 *pi4RetValFsDot11ACSuBeamFormee)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuBeamFormee = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11ACSuBeamFormee
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuBeamFormee;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsDot11ACMuBeamFormer
Input       :  The Indices
IfIndex

The Object
retValFsDot11ACMuBeamFormer
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACMuBeamFormer (INT4 i4IfIndex,
                             INT4 *pi4RetValFsDot11ACMuBeamFormer)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bMuBeamFormer = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11ACMuBeamFormer
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1MuBeamFormer;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsDot11ACMuBeamFormee
Input       :  The Indices
IfIndex

The Object
retValFsDot11ACMuBeamFormee
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACMuBeamFormee (INT4 i4IfIndex,
                             INT4 *pi4RetValFsDot11ACMuBeamFormee)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bMuBeamFormee = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11ACMuBeamFormee
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1MuBeamFormee;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTLinkAdaption
Input       :  The Indices
IfIndex

The Object
retValFsDot11ACVHTLinkAdaption
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTLinkAdaption (INT4 i4IfIndex,
                                INT4 *pi4RetValFsDot11ACVHTLinkAdaption)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bVhtLinkAdaption = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11ACVHTLinkAdaption =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1VhtLinkAdaption;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDot11ACRxAntennaPattern
Input       :  The Indices
IfIndex

The Object
retValFsDot11ACRxAntennaPattern
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACRxAntennaPattern (INT4 i4IfIndex,
                                 INT4 *pi4RetValFsDot11ACRxAntennaPattern)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bVhtRxAntenna = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11ACRxAntennaPattern
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1VhtRxAntenna;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsDot11ACTxAntennaPattern
Input       :  The Indices
IfIndex

The Object
retValFsDot11ACTxAntennaPattern
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACTxAntennaPattern (INT4 i4IfIndex,
                                 INT4 *pi4RetValFsDot11ACTxAntennaPattern)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bVhtTxAntenna = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11ACTxAntennaPattern
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1VhtTxAntenna;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsDot11ACBasicMCSMap
Input       :  The Indices
IfIndex

The Object
retValFsDot11ACBasicMCSMap
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACBasicMCSMap (INT4 i4IfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsDot11ACBasicMCSMap)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    UINT2               u2Dot11BasicMCS = 0;

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperMcsSet = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    u2Dot11BasicMCS
        = RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u2VhtOperMcsSet;
    MEMCPY (pRetValFsDot11ACBasicMCSMap->pu1_OctetList,
            &u2Dot11BasicMCS, sizeof (u2Dot11BasicMCS));
    pRetValFsDot11ACBasicMCSMap->i4_Length = sizeof (u2Dot11BasicMCS);

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTRxMCSMapConfig
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTRxMCSMapConfig
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTRxMCSMapConfig (INT4 i4IfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsDot11ACVHTRxMCSMapConfig)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    UINT1               au1Dot11RxMCSValue[FS11AC_SIZE8];
    UINT1               au1RxTemp[FS11AC_SIZE2];
    UINT1               u1loop1 = 0;
    UINT1               u1loop2 = FS11AC_MCS_HALFVALUE;
    UINT1               u1looptemp = 0;
    UINT1               u1shift = FS11AC_MCS_SHIFTVALUE;
    UINT1               u1shiftleft = FS11AC_MCS_SHIFTVALUE;

    MEMSET (au1RxTemp, 0, FS11AC_SIZE2);
    MEMSET (au1Dot11RxMCSValue, 0, FS11AC_SIZE8);

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (au1RxTemp,
            &RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.au1VhtCapaMcs[0],
            FS11AC_SIZE2);

    for (u1loop1 = 0; u1loop1 < FS11AC_MCS_HALFVALUE; u1loop1++)
    {
        u1looptemp = au1RxTemp[0];
        u1looptemp = (UINT1) (u1looptemp << u1shiftleft);
        u1shiftleft = (UINT1) (u1shiftleft - FS11AC_MCS_SHIFTMINUS);
        u1looptemp = (UINT1) (u1looptemp >> u1shift);
        au1Dot11RxMCSValue[u1loop1] = u1looptemp;
    }
    u1shiftleft = FS11AC_MCS_SHIFTVALUE;
    for (u1loop2 = FS11AC_MCS_HALFVALUE; u1loop2 < FS11AC_MCS_SPATIALVALUE;
         u1loop2++)
    {
        u1looptemp = au1RxTemp[1];
        u1looptemp = (UINT1) (u1looptemp << u1shiftleft);
        u1shiftleft = (UINT1) (u1shiftleft - FS11AC_MCS_SHIFTMINUS);
        u1looptemp = (UINT1) (u1looptemp >> u1shift);
        au1Dot11RxMCSValue[u1loop2] = u1looptemp;
    }
    MEMCPY (pRetValFsDot11ACVHTRxMCSMapConfig->pu1_OctetList,
            au1Dot11RxMCSValue, FS11AC_SIZE8);
    pRetValFsDot11ACVHTRxMCSMapConfig->i4_Length = FS11AC_SIZE8;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsDot11ACVHTTxMCSMapConfig
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACVHTTxMCSMapConfig
Output      :  The Get Low Lev Routine Take the Indices &

store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACVHTTxMCSMapConfig (INT4 i4IfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsDot11ACVHTTxMCSMapConfig)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    UINT1               au1Dot11TxMCSValue[FS11AC_SIZE8];
    UINT1               au1TxTemp[2];
    UINT1               u1loop1 = 0;
    UINT1               u1loop2 = FS11AC_MCS_HALFVALUE;
    UINT1               u1looptemp = 0;
    UINT1               u1shift = FS11AC_MCS_SHIFTVALUE;
    UINT1               u1shiftleft = FS11AC_MCS_SHIFTVALUE;

    MEMSET (au1TxTemp, 0, FS11AC_SIZE2);
    MEMSET (au1Dot11TxMCSValue, 0, FS11AC_SIZE8);

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (au1TxTemp,
            &RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.au1VhtCapaMcs[4],
            FS11AC_SIZE2);
    for (u1loop1 = 0; u1loop1 < FS11AC_MCS_HALFVALUE; u1loop1++)
    {
        u1looptemp = au1TxTemp[0];
        u1looptemp = (UINT1) (u1looptemp << u1shiftleft);
        u1shiftleft = (UINT1) (u1shiftleft - FS11AC_MCS_SHIFTMINUS);
        u1looptemp = (UINT1) (u1looptemp >> u1shift);
        au1Dot11TxMCSValue[u1loop1] = u1looptemp;
    }
    u1shiftleft = FS11AC_MCS_SHIFTVALUE;
    for (u1loop2 = FS11AC_MCS_HALFVALUE; u1loop2 < FS11AC_MCS_SPATIALVALUE;
         u1loop2++)
    {
        u1looptemp = au1TxTemp[1];
        u1looptemp = (UINT1) (u1looptemp << u1shiftleft);
        u1shiftleft = (UINT1) (u1shiftleft - FS11AC_MCS_SHIFTMINUS);
        u1looptemp = (UINT1) (u1looptemp >> u1shift);
        au1Dot11TxMCSValue[u1loop2] = u1looptemp;
    }
    MEMCPY (pRetValFsDot11ACVHTTxMCSMapConfig->pu1_OctetList,
            au1Dot11TxMCSValue, FS11AC_SIZE8);
    pRetValFsDot11ACVHTTxMCSMapConfig->i4_Length = FS11AC_SIZE8;

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsDot11ACCurrentChannelCenterFrequencyIndex1Config
Input       :  The Indices
IfIndex

The Object 
retValFsDot11ACCurrentChannelCenterFrequencyIndex1Config
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsDot11ACCurrentChannelCenterFrequencyIndex1Config (INT4 i4IfIndex,
               UINT4
               *pu4RetValFsDot11ACCurrentChannelCenterFrequencyIndex1Config)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pu4RetValFsDot11ACCurrentChannelCenterFrequencyIndex1Config);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsDot11VHTOptionImplemented
 Input       :  The Indices
                IfIndex

                The Object
                retValFsDot11VHTOptionImplemented
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsDot11VHTOptionImplemented(INT4 i4IfIndex , 
            INT4 *pi4RetValFsDot11VHTOptionImplemented)
{
    INT4 i4RetValFsDot11VHTOptionImplemented = 0;
    INT1 i1Result = 0;
    i1Result = wsscfgGetFsDot11VHTOptionImplemented(i4IfIndex,
               &i4RetValFsDot11VHTOptionImplemented);
    if(i1Result == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsDot11VHTOptionImplemented = 
            i4RetValFsDot11VHTOptionImplemented;
        return SNMP_SUCCESS;
    }
}
/****************************************************************************
 Function    :  nmhGetFsDot11OperatingModeNotificationImplemented
 Input       :  The Indices
                IfIndex

                The Object
                retValFsDot11OperatingModeNotificationImplemented
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsDot11OperatingModeNotificationImplemented(INT4 i4IfIndex , 
        INT4 *pi4RetValFsDot11OperatingModeNotificationImplemented)
{
    INT4 i4RetValFsDot11OperatingModeNotificationImplemented = 0;
    INT1 i1Result = 0;
    i1Result = wsscfgGetFsDot11OperatingModeNotificationImplemented(i4IfIndex,
            &i4RetValFsDot11OperatingModeNotificationImplemented);
    if(i1Result == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsDot11OperatingModeNotificationImplemented = 
            i4RetValFsDot11OperatingModeNotificationImplemented;
        return SNMP_SUCCESS;
    }
}
/****************************************************************************
 Function    :  nmhGetFsDot11ExtendedChannelSwitchActivated
 Input       :  The Indices
                IfIndex

                The Object
                retValFsDot11ExtendedChannelSwitchActivated
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsDot11ExtendedChannelSwitchActivated(INT4 i4IfIndex , INT4 *pi4RetValFsDot11ExtendedChannelSwitchActivated)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4RetValFsDot11ExtendedChannelSwitchActivated);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsDot11FragmentationThreshold
 Input       :  The Indices
                IfIndex

                The Object
                retValFsDot11FragmentationThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsDot11FragmentationThreshold(INT4 i4IfIndex , UINT4 *pu4RetValFsDot11FragmentationThreshold)
{
    UINT4 u4RetValFsDot11FragmentationThreshold= 0;
    INT1 i1Result = 0;
    i1Result = wsscfgGetFsDot11FragmentationThreshold(i4IfIndex,&u4RetValFsDot11FragmentationThreshold);
    if(i1Result == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsDot11FragmentationThreshold = u4RetValFsDot11FragmentationThreshold;
        return SNMP_SUCCESS;
    }
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsDot11ACMaxMPDULength
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACMaxMPDULength
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACMaxMPDULength (INT4 i4IfIndex,
                              INT4 i4SetValFsDot11ACMaxMPDULength)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsDot11ACMaxMPDULength);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACMaxMPDULengthConfig
Input       :  The Indices
IfIndex

The Object
setValFsDot11ACMaxMPDULengthConfig
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACMaxMPDULengthConfig (INT4 i4IfIndex,
                                    INT4 i4SetValFsDot11ACMaxMPDULengthConfig)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bMaxMPDULen = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1MaxMPDULen =
        (UINT1) i4SetValFsDot11ACMaxMPDULengthConfig;
#ifdef WLC_WANTED
    if (RadioIfSetDot11AcCapaParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTMaxRxAMPDUFactor
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTMaxRxAMPDUFactor
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTMaxRxAMPDUFactor (INT4 i4IfIndex,
                                    UINT4 u4SetValFsDot11ACVHTMaxRxAMPDUFactor)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsDot11ACVHTMaxRxAMPDUFactor);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTMaxRxAMPDUFactorConfig
Input       :  The Indices
IfIndex

The Object
setValFsDot11ACVHTMaxRxAMPDUFactorConfig
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTMaxRxAMPDUFactorConfig (INT4 i4IfIndex,
                                          UINT4
                                          u4SetValFsDot11ACVHTMaxRxAMPDUFactorConfig)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bVhtMaxAMPDU = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1VhtMaxAMPDU =
        (UINT1) u4SetValFsDot11ACVHTMaxRxAMPDUFactorConfig;

    if (u4SetValFsDot11ACVHTMaxRxAMPDUFactorConfig < 
            RADIO_DOT11N_AMPDU_MAX_LEN)
    {
        RadioIfGetDB.RadioIfGetAllDB.Dot11NampduParams.u1MaxAmpduLen =
            u4SetValFsDot11ACVHTMaxRxAMPDUFactorConfig;
    }
    else
    {
        RadioIfGetDB.RadioIfGetAllDB.Dot11NampduParams.u1MaxAmpduLen =
            RADIO_DOT11N_AMPDU_MAX_LEN; 
    }
    RadioIfGetDB.RadioIfIsGetAllDB.bMaxAmpduLen = OSIX_TRUE;
#ifdef WLC_WANTED
    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    
    if (RadioIfSetDot11AcCapaParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTControlFieldSupported
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTControlFieldSupported
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTControlFieldSupported (INT4 i4IfIndex,
                                         INT4
                                         i4SetValFsDot11ACVHTControlFieldSupported)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsDot11ACVHTControlFieldSupported);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTTXOPPowerSaveOptionImplemented
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTTXOPPowerSaveOptionImplemented
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTTXOPPowerSaveOptionImplemented (INT4 i4IfIndex,
                                                  INT4
                                                  i4SetValFsDot11ACVHTTXOPPowerSaveOptionImplemented)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsDot11ACVHTTXOPPowerSaveOptionImplemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTRxMCSMap
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTRxMCSMap
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTRxMCSMap (INT4 i4IfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsDot11ACVHTRxMCSMap)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pSetValFsDot11ACVHTRxMCSMap);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTRxHighestDataRateSupported
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTRxHighestDataRateSupported
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTRxHighestDataRateSupported (INT4 i4IfIndex,
                                              UINT4
                                              u4SetValFsDot11ACVHTRxHighestDataRateSupported)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsDot11ACVHTRxHighestDataRateSupported);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTTxMCSMap
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTTxMCSMap
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTTxMCSMap (INT4 i4IfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsDot11ACVHTTxMCSMap)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pSetValFsDot11ACVHTTxMCSMap);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTTxHighestDataRateSupported
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTTxHighestDataRateSupported
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTTxHighestDataRateSupported (INT4 i4IfIndex,
                                              UINT4
                                              u4SetValFsDot11ACVHTTxHighestDataRateSupported)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsDot11ACVHTTxHighestDataRateSupported);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTOBSSScanCount
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTOBSSScanCount
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTOBSSScanCount (INT4 i4IfIndex,
                                 UINT4 u4SetValFsDot11ACVHTOBSSScanCount)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsDot11ACVHTOBSSScanCount);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACCurrentChannelBandwidth
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACCurrentChannelBandwidth
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACCurrentChannelBandwidth (INT4 i4IfIndex,
                                        INT4
                                        i4SetValFsDot11ACCurrentChannelBandwidth)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsDot11ACCurrentChannelBandwidth);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACCurrentChannelBandwidthConfig
Input       :  The Indices
IfIndex

The Object
setValFsDot11ACCurrentChannelBandwidthConfig
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACCurrentChannelBandwidthConfig (INT4 i4IfIndex,
                                              INT4
                                              i4SetValFsDot11ACCurrentChannelBandwidthConfig)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bVhtChannelWidth = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1VhtChannelWidth =
        (UINT1) i4SetValFsDot11ACCurrentChannelBandwidthConfig;

#ifdef WLC_WANTED
    if (RadioIfSetDot11AcOperParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetFsDot11ACCurrentChannelCenterFrequencyIndex0
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACCurrentChannelCenterFrequencyIndex0
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACCurrentChannelCenterFrequencyIndex0 (INT4 i4IfIndex,
                                                    UINT4
                                                    u4SetValFsDot11ACCurrentChannelCenterFrequencyIndex0)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsDot11ACCurrentChannelCenterFrequencyIndex0);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACCurrentChannelCenterFrequencyIndex0Config
Input       :  The Indices
IfIndex

The Object
setValFsDot11ACCurrentChannelCenterFrequencyIndex0Config
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACCurrentChannelCenterFrequencyIndex0Config (INT4 i4IfIndex,
                                                          UINT4
                                                          u4SetValFsDot11ACCurrentChannelCenterFrequencyIndex0Config)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bCenterFcy0 = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1CenterFcy0 =
        (UINT1) u4SetValFsDot11ACCurrentChannelCenterFrequencyIndex0Config;

#ifdef WLC_WANTED
    if (RadioIfSetDot11AcOperParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetFsDot11ACCurrentChannelCenterFrequencyIndex1
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACCurrentChannelCenterFrequencyIndex1
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACCurrentChannelCenterFrequencyIndex1 (INT4 i4IfIndex,
                                                    UINT4
                                                    u4SetValFsDot11ACCurrentChannelCenterFrequencyIndex1)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsDot11ACCurrentChannelCenterFrequencyIndex1);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTShortGIOptionIn80Implemented
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTShortGIOptionIn80Implemented
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTShortGIOptionIn80Implemented (INT4 i4IfIndex,
                                                INT4
                                                i4SetValFsDot11ACVHTShortGIOptionIn80Implemented)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsDot11ACVHTShortGIOptionIn80Implemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTShortGIOptionIn80Activated
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTShortGIOptionIn80Activated
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTShortGIOptionIn80Activated (INT4 i4IfIndex,
                                              INT4
                                              i4SetValFsDot11ACVHTShortGIOptionIn80Activated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bShortGi80 = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1ShortGi80 =
        (UINT1) i4SetValFsDot11ACVHTShortGIOptionIn80Activated;

#ifdef WLC_WANTED
    if (RadioIfSetDot11AcCapaParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTShortGIOptionIn160and80p80Implemented
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTShortGIOptionIn160and80p80Implemented
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTShortGIOptionIn160and80p80Implemented (INT4 i4IfIndex,
                                                         INT4
                                                         i4SetValFsDot11ACVHTShortGIOptionIn160and80p80Implemented)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsDot11ACVHTShortGIOptionIn160and80p80Implemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTShortGIOptionIn160and80p80Activated
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTShortGIOptionIn160and80p80Activated
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTShortGIOptionIn160and80p80Activated (INT4 i4IfIndex,
                                                       INT4
                                                       i4SetValFsDot11ACVHTShortGIOptionIn160and80p80Activated)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsDot11ACVHTShortGIOptionIn160and80p80Activated);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTLDPCCodingOptionImplemented
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTLDPCCodingOptionImplemented
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTLDPCCodingOptionImplemented (INT4 i4IfIndex,
                                               INT4
                                               i4SetValFsDot11ACVHTLDPCCodingOptionImplemented)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsDot11ACVHTLDPCCodingOptionImplemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTLDPCCodingOptionActivated
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTLDPCCodingOptionActivated
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTLDPCCodingOptionActivated (INT4 i4IfIndex,
                                             INT4
                                             i4SetValFsDot11ACVHTLDPCCodingOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bRxLpdc = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1RxLpdc =
        (UINT1) i4SetValFsDot11ACVHTLDPCCodingOptionActivated;

#ifdef WLC_WANTED
    if (RadioIfSetDot11AcCapaParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTTxSTBCOptionImplemented
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTTxSTBCOptionImplemented
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTTxSTBCOptionImplemented (INT4 i4IfIndex,
                                           INT4
                                           i4SetValFsDot11ACVHTTxSTBCOptionImplemented)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsDot11ACVHTTxSTBCOptionImplemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTTxSTBCOptionActivated
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTTxSTBCOptionActivated
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTTxSTBCOptionActivated (INT4 i4IfIndex,
                                         INT4
                                         i4SetValFsDot11ACVHTTxSTBCOptionActivated)
{
    INT1 i1Result = 0;
    i1Result = wsscfgSetFsDot11ACVHTTxSTBCOptionActivated(i4IfIndex,i4SetValFsDot11ACVHTTxSTBCOptionActivated);
    if(i1Result == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTRxSTBCOptionImplemented
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTRxSTBCOptionImplemented
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTRxSTBCOptionImplemented (INT4 i4IfIndex,
                                           INT4
                                           i4SetValFsDot11ACVHTRxSTBCOptionImplemented)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsDot11ACVHTRxSTBCOptionImplemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTRxSTBCOptionActivated
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTRxSTBCOptionActivated
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTRxSTBCOptionActivated (INT4 i4IfIndex,
                                         INT4
                                         i4SetValFsDot11ACVHTRxSTBCOptionActivated)
{
    INT1 i1Result = 0;
    i1Result = wsscfgSetFsDot11ACVHTRxSTBCOptionActivated(i4IfIndex,i4SetValFsDot11ACVHTRxSTBCOptionActivated);
    if(i1Result == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTMUMaxUsersImplemented
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTMUMaxUsersImplemented
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTMUMaxUsersImplemented (INT4 i4IfIndex,
                                         UINT4
                                         u4SetValFsDot11ACVHTMUMaxUsersImplemented)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsDot11ACVHTMUMaxUsersImplemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTMUMaxNSTSPerUserImplemented
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTMUMaxNSTSPerUserImplemented
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTMUMaxNSTSPerUserImplemented (INT4 i4IfIndex,
                                               UINT4
                                               u4SetValFsDot11ACVHTMUMaxNSTSPerUserImplemented)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsDot11ACVHTMUMaxNSTSPerUserImplemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTMUMaxNSTSTotalImplemented
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTMUMaxNSTSTotalImplemented
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTMUMaxNSTSTotalImplemented (INT4 i4IfIndex,
                                             UINT4
                                             u4SetValFsDot11ACVHTMUMaxNSTSTotalImplemented)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsDot11ACVHTMUMaxNSTSTotalImplemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACSuBeamFormer
Input       :  The Indices
IfIndex

The Object
setValFsDot11ACSuBeamFormer
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACSuBeamFormer (INT4 i4IfIndex, INT4 i4SetValFsDot11ACSuBeamFormer)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsDot11ACSuBeamFormer);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACSuBeamFormee
Input       :  The Indices
IfIndex

The Object
setValFsDot11ACSuBeamFormee
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACSuBeamFormee (INT4 i4IfIndex, INT4 i4SetValFsDot11ACSuBeamFormee)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsDot11ACSuBeamFormee);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACMuBeamFormer
Input       :  The Indices
IfIndex

The Object
setValFsDot11ACMuBeamFormer
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACMuBeamFormer (INT4 i4IfIndex, INT4 i4SetValFsDot11ACMuBeamFormer)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsDot11ACMuBeamFormer);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACMuBeamFormee
Input       :  The Indices
IfIndex

The Object
setValFsDot11ACMuBeamFormee
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ***************************************************************************/
INT1
nmhSetFsDot11ACMuBeamFormee (INT4 i4IfIndex, INT4 i4SetValFsDot11ACMuBeamFormee)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsDot11ACMuBeamFormee);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTLinkAdaption
Input       :  The Indices
IfIndex

The Object
setValFsDot11ACVHTLinkAdaption
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTLinkAdaption (INT4 i4IfIndex,
                                INT4 i4SetValFsDot11ACVHTLinkAdaption)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsDot11ACVHTLinkAdaption);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACRxAntennaPattern
Input       :  The Indices
IfIndex
The Object
setValFsDot11ACRxAntennaPattern
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACRxAntennaPattern (INT4 i4IfIndex,
                                 INT4 i4SetValFsDot11ACRxAntennaPattern)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsDot11ACRxAntennaPattern);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACTxAntennaPattern
Input       :  The Indices
IfIndex

The Object
setValFsDot11ACTxAntennaPattern
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACTxAntennaPattern (INT4 i4IfIndex,
                                 INT4 i4SetValFsDot11ACTxAntennaPattern)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsDot11ACTxAntennaPattern);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACBasicMCSMap
Input       :  The Indices
IfIndex

The Object
setValFsDot11ACBasicMCSMap
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACBasicMCSMap (INT4 i4IfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsDot11ACBasicMCSMap)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pSetValFsDot11ACBasicMCSMap);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTRxMCSMapConfig
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTRxMCSMapConfig
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTRxMCSMapConfig (INT4 i4IfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValFsDot11ACVHTRxMCSMapConfig)
{

    tRadioIfGetDB       RadioIfGetDB;
    UINT1               au1McsValue[FS11AC_SIZE8];
    UINT1               au1McsTempValue[FS11AC_SIZE2];
    UINT1               u1loop = 0;
    MEMSET (au1McsValue, 0, FS11AC_SIZE8);
    MEMSET (au1McsTempValue, 0, FS11AC_SIZE2);

    MEMCPY (au1McsValue, pSetValFsDot11ACVHTRxMCSMapConfig->pu1_OctetList,
            FS11AC_SIZE8);

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;
    au1McsTempValue[0] = 0;
    for (u1loop = FS11AC_MCS_HALFVALUE; u1loop > 0; u1loop--)
    {
        au1McsTempValue[0] = au1McsTempValue[0] | au1McsValue[u1loop - 1];
        if (u1loop != 1)
        {
            au1McsTempValue[0] = (UINT1) 
                        (au1McsTempValue[0] << FS11AC_MCS_SHIFTMINUS);
        }
    }
    au1McsTempValue[1] = 0;
    for (u1loop = FS11AC_MCS_SPATIALVALUE; u1loop > FS11AC_MCS_HALFVALUE;
         u1loop--)
    {
        au1McsTempValue[1] = au1McsTempValue[1] | au1McsValue[u1loop - 1];
        if (u1loop != FS11AC_MCS_HALFVALUE + 1)
        {
            au1McsTempValue[1] = (UINT1) 
                (au1McsTempValue[1] << FS11AC_MCS_SHIFTMINUS);
        }
    }
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (&RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.au1VhtCapaMcs[0],
            au1McsTempValue, FS11AC_SIZE2);

#ifdef WLC_WANTED
    if (RadioIfSetDot11AcCapaParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetFsDot11ACVHTTxMCSMapConfig
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACVHTTxMCSMapConfig
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACVHTTxMCSMapConfig (INT4 i4IfIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValFsDot11ACVHTTxMCSMapConfig)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               au1McsValue[FS11AC_SIZE8];
    UINT1               au1McsTempValue[FS11AC_SIZE2];
    UINT1               u1loop = 0;
    MEMSET (au1McsValue, 0, FS11AC_SIZE8);
    MEMSET (au1McsTempValue, 0, FS11AC_SIZE2);

    MEMCPY (au1McsValue, pSetValFsDot11ACVHTTxMCSMapConfig->pu1_OctetList,
            FS11AC_SIZE8);

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;
    au1McsTempValue[0] = 0;
    for (u1loop = FS11AC_MCS_HALFVALUE; u1loop > 0; u1loop--)
    {
        au1McsTempValue[0] = au1McsTempValue[0] | au1McsValue[u1loop - 1];
        if (u1loop != 1)
        {
            au1McsTempValue[0] = (UINT1) 
                    (au1McsTempValue[0] << FS11AC_MCS_SHIFTMINUS);
        }
    }
    au1McsTempValue[1] = 0;
    for (u1loop = FS11AC_MCS_SPATIALVALUE; u1loop > FS11AC_MCS_HALFVALUE;
         u1loop--)
    {
        au1McsTempValue[1] = au1McsTempValue[1] | au1McsValue[u1loop - 1];
        if (u1loop != FS11AC_MCS_HALFVALUE + 1)
        {
            au1McsTempValue[1] = (UINT1) 
                    (au1McsTempValue[1] << FS11AC_MCS_SHIFTMINUS);
        }
    }
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (&RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.au1VhtCapaMcs[4],
            au1McsTempValue, FS11AC_SIZE2);

#ifdef WLC_WANTED
    if (RadioIfSetDot11AcCapaParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetFsDot11ACCurrentChannelCenterFrequencyIndex1Config
Input       :  The Indices
IfIndex

The Object 
setValFsDot11ACCurrentChannelCenterFrequencyIndex1Config
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDot11ACCurrentChannelCenterFrequencyIndex1Config (INT4 i4IfIndex,
                                                          UINT4
                                                          u4SetValFsDot11ACCurrentChannelCenterFrequencyIndex1Config)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsDot11ACCurrentChannelCenterFrequencyIndex1Config);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDot11VHTOptionImplemented
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11VHTOptionImplemented
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsDot11VHTOptionImplemented(INT4 i4IfIndex , INT4 i4SetValFsDot11VHTOptionImplemented)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsDot11VHTOptionImplemented);
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetFsDot11OperatingModeNotificationImplemented
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11OperatingModeNotificationImplemented
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsDot11OperatingModeNotificationImplemented(INT4 i4IfIndex , INT4 i4SetValFsDot11OperatingModeNotificationImplemented)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsDot11OperatingModeNotificationImplemented);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsDot11ExtendedChannelSwitchActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11ExtendedChannelSwitchActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsDot11ExtendedChannelSwitchActivated(INT4 i4IfIndex , INT4 i4SetValFsDot11ExtendedChannelSwitchActivated)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValFsDot11ExtendedChannelSwitchActivated);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsDot11FragmentationThreshold
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11FragmentationThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsDot11FragmentationThreshold(INT4 i4IfIndex , UINT4 u4SetValFsDot11FragmentationThreshold)
{
    INT1 i1Result = 0;
    i1Result = wsscfgSetFsDot11FragmentationThreshold(i4IfIndex,u4SetValFsDot11FragmentationThreshold);
    if(i1Result == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsDot11ACMaxMPDULength
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACMaxMPDULength
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACMaxMPDULength (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                 INT4 i4TestValFsDot11ACMaxMPDULength)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsDot11ACMaxMPDULength);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACMaxMPDULengthConfig
Input       :  The Indices
IfIndex

The Object
testValFsDot11ACMaxMPDULengthConfig
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACMaxMPDULengthConfig (UINT4 *pu4ErrorCode,
                                       INT4 i4IfIndex,
                                       INT4
                                       i4TestValFsDot11ACMaxMPDULengthConfig)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT4               u4RadioType = 0;
    INT4                i4MaxMPDUCapable = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if ((i4TestValFsDot11ACMaxMPDULengthConfig != FS11AC_MPDU_ZERO) &&
        (i4TestValFsDot11ACMaxMPDULengthConfig != FS11AC_MPDU_ONE) &&
        (i4TestValFsDot11ACMaxMPDULengthConfig != FS11AC_MPDU_TWO))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (CLI_11AC_MPDU_INVALID_VALUE);

        return SNMP_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppMaxMPDULen = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
        return OSIX_FAILURE;

    }

    if (nmhGetFsDot11RadioType (i4IfIndex, &u4RadioType) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);

        return SNMP_FAILURE;

    }
    if (u4RadioType != RADIO_TYPE_AC)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);

        return SNMP_FAILURE;
    }
    i4MaxMPDUCapable =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppMaxMPDULen;
    if (i4TestValFsDot11ACMaxMPDULengthConfig > i4MaxMPDUCapable)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_11AC_MPDU_INCONSISTENT_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTMaxRxAMPDUFactor
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTMaxRxAMPDUFactor
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTMaxRxAMPDUFactor (UINT4 *pu4ErrorCode,
                                       INT4 i4IfIndex,
                                       UINT4
                                       u4TestValFsDot11ACVHTMaxRxAMPDUFactor)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValFsDot11ACVHTMaxRxAMPDUFactor);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTMaxRxAMPDUFactorConfig
Input       :  The Indices
IfIndex

The Object
testValFsDot11ACVHTMaxRxAMPDUFactorConfig
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTMaxRxAMPDUFactorConfig (UINT4 *pu4ErrorCode,
                                             INT4 i4IfIndex,
                                             UINT4
                                             u4TestValFsDot11ACVHTMaxRxAMPDUFactorConfig)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT4               u4RadioType = 0;
    UINT4               u4MaxAMPDUCapable = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if (u4TestValFsDot11ACVHTMaxRxAMPDUFactorConfig > FS11AC_AMPDU_MAX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (CLI_11AC_AMPDU_INVALID_VALUE);

        return SNMP_FAILURE;
    }

    if (nmhGetFsDot11RadioType (i4IfIndex, &u4RadioType) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
        return SNMP_FAILURE;
    }
    if (u4RadioType != RADIO_TYPE_AC)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);

        return SNMP_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppVhtMaxAMPDU = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
        return OSIX_FAILURE;

    }
    u4MaxAMPDUCapable =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
        u1SuppVhtMaxAMPDU;
    if (u4TestValFsDot11ACVHTMaxRxAMPDUFactorConfig > u4MaxAMPDUCapable)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_11AC_AMPDU_INCONSISTENT_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTControlFieldSupported
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTControlFieldSupported
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTControlFieldSupported (UINT4 *pu4ErrorCode,
                                            INT4 i4IfIndex,
                                            INT4
                                            i4TestValFsDot11ACVHTControlFieldSupported)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsDot11ACVHTControlFieldSupported);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTTXOPPowerSaveOptionImplemented
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTTXOPPowerSaveOptionImplemented
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTTXOPPowerSaveOptionImplemented (UINT4 *pu4ErrorCode,
                                                     INT4 i4IfIndex,
                                                     INT4
                                                     i4TestValFsDot11ACVHTTXOPPowerSaveOptionImplemented)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsDot11ACVHTTXOPPowerSaveOptionImplemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTRxMCSMap
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTRxMCSMap
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTRxMCSMap (UINT4 *pu4ErrorCode,
                               INT4 i4IfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsDot11ACVHTRxMCSMap)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pTestValFsDot11ACVHTRxMCSMap);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTRxHighestDataRateSupported
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTRxHighestDataRateSupported
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTRxHighestDataRateSupported (UINT4 *pu4ErrorCode,
                                                 INT4 i4IfIndex,
                                                 UINT4
                                                 u4TestValFsDot11ACVHTRxHighestDataRateSupported)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValFsDot11ACVHTRxHighestDataRateSupported);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTTxMCSMap
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTTxMCSMap
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTTxMCSMap (UINT4 *pu4ErrorCode,
                               INT4 i4IfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsDot11ACVHTTxMCSMap)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pTestValFsDot11ACVHTTxMCSMap);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTTxHighestDataRateSupported
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTTxHighestDataRateSupported
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTTxHighestDataRateSupported (UINT4 *pu4ErrorCode,
                                                 INT4 i4IfIndex,
                                                 UINT4
                                                 u4TestValFsDot11ACVHTTxHighestDataRateSupported)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValFsDot11ACVHTTxHighestDataRateSupported);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTOBSSScanCount
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTOBSSScanCount
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTOBSSScanCount (UINT4 *pu4ErrorCode,
                                    INT4 i4IfIndex,
                                    UINT4 u4TestValFsDot11ACVHTOBSSScanCount)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValFsDot11ACVHTOBSSScanCount);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACCurrentChannelBandwidth
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACCurrentChannelBandwidth
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACCurrentChannelBandwidth (UINT4 *pu4ErrorCode,
                                           INT4 i4IfIndex,
                                           INT4
                                           i4TestValFsDot11ACCurrentChannelBandwidth)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsDot11ACCurrentChannelBandwidth);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACCurrentChannelBandwidthConfig
Input       :  The Indices
IfIndex

The Object
testValFsDot11ACCurrentChannelBandwidthConfig
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACCurrentChannelBandwidthConfig (UINT4 *pu4ErrorCode,
              INT4 i4IfIndex,
              INT4
              i4TestValFsDot11ACCurrentChannelBandwidthConfig)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT4               u4RadioType = 0;
    INT4                i4ChannelCapable = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    if ((i4TestValFsDot11ACCurrentChannelBandwidthConfig < FS11AC_CHAN_MIN) ||
        (i4TestValFsDot11ACCurrentChannelBandwidthConfig > FS11AC_CHAN_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (CLI_11AC_CHANWIDTH_INVALID_VALUE);

        return SNMP_FAILURE;
    }

    if (nmhGetFsDot11RadioType (i4IfIndex, &u4RadioType) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
        return SNMP_FAILURE;
    }
    if (u4RadioType != RADIO_TYPE_AC)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
        return SNMP_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppVhtChannelWidth = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
        return OSIX_FAILURE;

    }
    i4ChannelCapable =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.
        u1SuppVhtChannelWidth;
    if (i4TestValFsDot11ACCurrentChannelBandwidthConfig > i4ChannelCapable)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_11AC_CHANWIDTH_INCONSISTENT_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACCurrentChannelCenterFrequencyIndex0
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACCurrentChannelCenterFrequencyIndex0
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACCurrentChannelCenterFrequencyIndex0 (UINT4 *pu4ErrorCode,
                 INT4 i4IfIndex,
                 UINT4
                 u4TestValFsDot11ACCurrentChannelCenterFrequencyIndex0)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValFsDot11ACCurrentChannelCenterFrequencyIndex0);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACCurrentChannelCenterFrequencyIndex0Config
Input       :  The Indices
IfIndex

The Object
testValFsDot11ACCurrentChannelCenterFrequencyIndex0Config
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACCurrentChannelCenterFrequencyIndex0Config (UINT4
           *pu4ErrorCode,
           INT4 i4IfIndex,
           UINT4
           u4TestValFsDot11ACCurrentChannelCenterFrequencyIndex0Config)
{

    UINT4               u4RadioType = 0;
    UINT4               u4ChannelCapable = 0;
    if (u4TestValFsDot11ACCurrentChannelCenterFrequencyIndex0Config >
        FS11AC_CENTERFRQ_MAX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (CLI_11AC_CHANINDEX_INVALID_VALUE);
        return SNMP_FAILURE;
    }

    if (nmhGetFsDot11RadioType (i4IfIndex, &u4RadioType) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
        return SNMP_FAILURE;
    }
    if (u4RadioType != RADIO_TYPE_AC)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
        return SNMP_FAILURE;
    }

/*  After the implementation of HT 20/40 co-existence,
 *  center frequency index0 validation needs to be
 *  added based on allowed channel list. */

    if (nmhGetFsDot11ACCurrentChannelCenterFrequencyIndex0 (i4IfIndex,
                                                            &u4ChannelCapable)
        == SNMP_SUCCESS)
    {
        if (u4TestValFsDot11ACCurrentChannelCenterFrequencyIndex0Config >
            u4ChannelCapable)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11AC_CHANINDEX_INCONSISTENT_VALUE);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACCurrentChannelCenterFrequencyIndex1
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACCurrentChannelCenterFrequencyIndex1
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACCurrentChannelCenterFrequencyIndex1 (UINT4 *pu4ErrorCode,
                     INT4 i4IfIndex,
                     UINT4
                     u4TestValFsDot11ACCurrentChannelCenterFrequencyIndex1)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValFsDot11ACCurrentChannelCenterFrequencyIndex1);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTShortGIOptionIn80Implemented
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTShortGIOptionIn80Implemented
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTShortGIOptionIn80Implemented (UINT4 *pu4ErrorCode,
                     INT4 i4IfIndex,
                     INT4
                     i4TestValFsDot11ACVHTShortGIOptionIn80Implemented)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsDot11ACVHTShortGIOptionIn80Implemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTShortGIOptionIn80Activated
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTShortGIOptionIn80Activated
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTShortGIOptionIn80Activated (UINT4 *pu4ErrorCode,
                          INT4 i4IfIndex,
                          INT4
                          i4TestValFsDot11ACVHTShortGIOptionIn80Activated)
{
    UINT4               u4RadioType = 0;
    INT4                i4FsDot11ACVHTShortGIOptionIn80Implemented = 0;
    if ((i4TestValFsDot11ACVHTShortGIOptionIn80Activated != 0) &&
        (i4TestValFsDot11ACVHTShortGIOptionIn80Activated != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (CLI_11AC_SHORTGI_INVALID_VALUE);

        return SNMP_FAILURE;
    }
    if (nmhGetFsDot11RadioType (i4IfIndex, &u4RadioType) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
        return SNMP_FAILURE;

    }
    if (u4RadioType != RADIO_TYPE_AC)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
        return SNMP_FAILURE;
    }
    if (nmhGetFsDot11ACVHTShortGIOptionIn80Implemented (i4IfIndex,
                     &i4FsDot11ACVHTShortGIOptionIn80Implemented)
        == SNMP_SUCCESS)
    {
        if ((i4FsDot11ACVHTShortGIOptionIn80Implemented == 0) &&
            (i4TestValFsDot11ACVHTShortGIOptionIn80Activated == 1))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11AC_SHORTGI_INCONSISTENT_VALUE);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTShortGIOptionIn160and80p80Implemented
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTShortGIOptionIn160and80p80Implemented
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTShortGIOptionIn160and80p80Implemented (UINT4 *pu4ErrorCode,
              INT4 i4IfIndex,
              INT4
              i4TestValFsDot11ACVHTShortGIOptionIn160and80p80Implemented)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsDot11ACVHTShortGIOptionIn160and80p80Implemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTShortGIOptionIn160and80p80Activated
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTShortGIOptionIn160and80p80Activated
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTShortGIOptionIn160and80p80Activated (UINT4 *pu4ErrorCode,
        INT4 i4IfIndex,
        INT4
        i4TestValFsDot11ACVHTShortGIOptionIn160and80p80Activated)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsDot11ACVHTShortGIOptionIn160and80p80Activated);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTLDPCCodingOptionImplemented
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTLDPCCodingOptionImplemented
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTLDPCCodingOptionImplemented (UINT4 *pu4ErrorCode,
                  INT4 i4IfIndex,
                  INT4
                  i4TestValFsDot11ACVHTLDPCCodingOptionImplemented)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsDot11ACVHTLDPCCodingOptionImplemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTLDPCCodingOptionActivated
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTLDPCCodingOptionActivated
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTLDPCCodingOptionActivated (UINT4 *pu4ErrorCode,
       INT4 i4IfIndex,
       INT4
       i4TestValFsDot11ACVHTLDPCCodingOptionActivated)
{
    UINT4               u4RadioType = 0;
    INT4                i4FsDot11ACVHTLDPCCodingOptionImplemented = 0;
    if (nmhGetFsDot11RadioType (i4IfIndex, &u4RadioType) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
        return SNMP_FAILURE;

    }
    if (u4RadioType != RADIO_TYPE_AC)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);

        return SNMP_FAILURE;
    }
    if ((i4TestValFsDot11ACVHTLDPCCodingOptionActivated != 0) &&
        (i4TestValFsDot11ACVHTLDPCCodingOptionActivated != 1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (CLI_11AC_LDPC_INVALID_VALUE);

        return SNMP_FAILURE;
    }
    if (nmhGetFsDot11ACVHTLDPCCodingOptionImplemented (i4IfIndex,
                       &i4FsDot11ACVHTLDPCCodingOptionImplemented)
        == SNMP_SUCCESS)
    {
        if ((i4FsDot11ACVHTLDPCCodingOptionImplemented == 0) &&
            (i4TestValFsDot11ACVHTLDPCCodingOptionActivated == 1))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_11AC_LDPC_INCONSISTENT_VALUE);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTTxSTBCOptionImplemented
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTTxSTBCOptionImplemented
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTTxSTBCOptionImplemented (UINT4 *pu4ErrorCode,
            INT4 i4IfIndex,
            INT4
            i4TestValFsDot11ACVHTTxSTBCOptionImplemented)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsDot11ACVHTTxSTBCOptionImplemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTTxSTBCOptionActivated
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTTxSTBCOptionActivated
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTTxSTBCOptionActivated (UINT4 *pu4ErrorCode,
            INT4 i4IfIndex,
            INT4
            i4TestValFsDot11ACVHTTxSTBCOptionActivated)
{
    UINT4 u4ErrorCode = 0;
    INT1  i1Result =0;
    i1Result = wsscfgTestv2FsDot11ACVHTTxSTBCOptionActivated(&u4ErrorCode ,
            i4IfIndex , i4TestValFsDot11ACVHTTxSTBCOptionActivated);
    if(i1Result == SNMP_FAILURE)
    {
        *pu4ErrorCode = u4ErrorCode;
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTRxSTBCOptionImplemented
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTRxSTBCOptionImplemented
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTRxSTBCOptionImplemented (UINT4 *pu4ErrorCode,
            INT4 i4IfIndex,
            INT4
            i4TestValFsDot11ACVHTRxSTBCOptionImplemented)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsDot11ACVHTRxSTBCOptionImplemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTRxSTBCOptionActivated
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTRxSTBCOptionActivated
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTRxSTBCOptionActivated (UINT4 *pu4ErrorCode,
            INT4 i4IfIndex,
            INT4
            i4TestValFsDot11ACVHTRxSTBCOptionActivated)
{
    UINT4 u4ErrorCode = 0;
    INT1  i1Result =0;
    i1Result = wsscfgTestv2FsDot11ACVHTRxSTBCOptionActivated(&u4ErrorCode ,
            i4IfIndex , i4TestValFsDot11ACVHTRxSTBCOptionActivated);
    if(i1Result == SNMP_FAILURE)
    {
        *pu4ErrorCode = u4ErrorCode;
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTMUMaxUsersImplemented
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTMUMaxUsersImplemented
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTMUMaxUsersImplemented (UINT4 *pu4ErrorCode,
                      INT4 i4IfIndex,
                      UINT4
                      u4TestValFsDot11ACVHTMUMaxUsersImplemented)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValFsDot11ACVHTMUMaxUsersImplemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTMUMaxNSTSPerUserImplemented
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTMUMaxNSTSPerUserImplemented
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTMUMaxNSTSPerUserImplemented (UINT4 *pu4ErrorCode,
                    INT4 i4IfIndex,
                    UINT4
                    u4TestValFsDot11ACVHTMUMaxNSTSPerUserImplemented)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValFsDot11ACVHTMUMaxNSTSPerUserImplemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTMUMaxNSTSTotalImplemented
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTMUMaxNSTSTotalImplemented
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTMUMaxNSTSTotalImplemented (UINT4 *pu4ErrorCode,
                                                INT4 i4IfIndex,
                                                UINT4
                                                u4TestValFsDot11ACVHTMUMaxNSTSTotalImplemented)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValFsDot11ACVHTMUMaxNSTSTotalImplemented);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACSuBeamFormer
Input       :  The Indices
IfIndex

The Object
testValFsDot11ACSuBeamFormer
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACSuBeamFormer (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                INT4 i4TestValFsDot11ACSuBeamFormer)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsDot11ACSuBeamFormer);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACSuBeamFormee
Input       :  The Indices
IfIndex

The Object
testValFsDot11ACSuBeamFormee
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACSuBeamFormee (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                INT4 i4TestValFsDot11ACSuBeamFormee)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsDot11ACSuBeamFormee);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACMuBeamFormer
Input       :  The Indices
IfIndex

The Object
testValFsDot11ACMuBeamFormer
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACMuBeamFormer (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                INT4 i4TestValFsDot11ACMuBeamFormer)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsDot11ACMuBeamFormer);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACMuBeamFormee
Input       :  The Indices
IfIndex

The Object
testValFsDot11ACMuBeamFormee
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACMuBeamFormee (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                INT4 i4TestValFsDot11ACMuBeamFormee)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsDot11ACMuBeamFormee);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTLinkAdaption
Input       :  The Indices
IfIndex

The Object
testValFsDot11ACVHTLinkAdaption
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTLinkAdaption (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                   INT4 i4TestValFsDot11ACVHTLinkAdaption)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsDot11ACVHTLinkAdaption);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACRxAntennaPattern
Input       :  The Indices
IfIndex

The Object
testValFsDot11ACRxAntennaPattern
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACRxAntennaPattern (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                    INT4 i4TestValFsDot11ACRxAntennaPattern)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsDot11ACRxAntennaPattern);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACTxAntennaPattern
Input       :  The Indices
IfIndex

The Object
testValFsDot11ACTxAntennaPattern
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACTxAntennaPattern (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                    INT4 i4TestValFsDot11ACTxAntennaPattern)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsDot11ACTxAntennaPattern);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACBasicMCSMap
Input       :  The Indices
IfIndex

The Object
testValFsDot11ACBasicMCSMap
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACBasicMCSMap (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsDot11ACBasicMCSMap)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pTestValFsDot11ACBasicMCSMap);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTRxMCSMapConfig
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTRxMCSMapConfig
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTRxMCSMapConfig (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValFsDot11ACVHTRxMCSMapConfig)
{
    UINT4               u4RadioType = 0;
    UINT1               au1MCSValue[FS11AC_SIZE8];
    UINT1               u1loop = 0;
    tSNMP_OCTET_STRING_TYPE RetValFsDot11ACVHTRxMCSMap;
    UINT1               au1dbMCSValue[FS11AC_SIZE8];

    MEMSET (au1MCSValue, 0, FS11AC_SIZE8);
    MEMSET (au1dbMCSValue, 0, FS11AC_SIZE8);
    MEMSET (&RetValFsDot11ACVHTRxMCSMap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    RetValFsDot11ACVHTRxMCSMap.pu1_OctetList = au1dbMCSValue;
    MEMCPY (au1MCSValue, pTestValFsDot11ACVHTRxMCSMapConfig->pu1_OctetList,
            FS11AC_SIZE8);

    if (nmhGetFsDot11RadioType (i4IfIndex, &u4RadioType) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);

        return SNMP_FAILURE;
    }

    if (u4RadioType != RADIO_TYPE_AC)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);

        return SNMP_FAILURE;
    }

    for (u1loop = 0; u1loop < FS11AC_MCS_SPATIALVALUE; u1loop++)
    {
        if (u1loop < FS11AC_MCS_CONFIGURABLESS)
        {
            if ((au1MCSValue[u1loop] != FS11AC_MCS_RATE7) &&
                (au1MCSValue[u1loop] != FS11AC_MCS_RATE8) &&
                (au1MCSValue[u1loop] != FS11AC_MCS_RATE9) &&
                (au1MCSValue[u1loop] != FS11AC_MCS_SSRESERVED))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_11AC_RX_INVALID_VALUE);
                return SNMP_FAILURE;
            }
        }
        else
        {
            if (au1MCSValue[u1loop] != FS11AC_MCS_SSRESERVED)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_11AC_RX_INVALID_VALUE);
                return SNMP_FAILURE;
            }
        }
    }
    if (nmhGetFsDot11ACVHTRxMCSMap (i4IfIndex, &RetValFsDot11ACVHTRxMCSMap)
        == SNMP_SUCCESS)
    {
        for (u1loop = 0; u1loop < FS11AC_MCS_CONFIGURABLESS; u1loop++)
        {
            if ((au1MCSValue[u1loop] != FS11AC_MCS_SSRESERVED) &&
                (au1MCSValue[u1loop] > au1dbMCSValue[u1loop]))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                CLI_SET_ERR (CLI_11AC_RX_INCONSISTENT_VALUE);

                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACVHTTxMCSMapConfig
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACVHTTxMCSMapConfig
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACVHTTxMCSMapConfig (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValFsDot11ACVHTTxMCSMapConfig)
{
    UINT4               u4RadioType = 0;
    UINT1               au1MCSValue[FS11AC_SIZE8];
    UINT1               u1loop = 0;
    tSNMP_OCTET_STRING_TYPE RetValFsDot11ACVHTTxMCSMap;
    UINT1               au1dbMCSValue[FS11AC_SIZE8];

    MEMSET (au1MCSValue, 0, FS11AC_SIZE8);
    MEMSET (au1dbMCSValue, 0, FS11AC_SIZE8);
    MEMSET (&RetValFsDot11ACVHTTxMCSMap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    RetValFsDot11ACVHTTxMCSMap.pu1_OctetList = au1dbMCSValue;

    MEMCPY (au1MCSValue, pTestValFsDot11ACVHTTxMCSMapConfig->pu1_OctetList,
            FS11AC_SIZE8);

    if (nmhGetFsDot11RadioType (i4IfIndex, &u4RadioType) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);

        return SNMP_FAILURE;
    }
    if (u4RadioType != RADIO_TYPE_AC)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);

        return SNMP_FAILURE;
    }

    for (u1loop = 0; u1loop < FS11AC_MCS_SPATIALVALUE; u1loop++)
    {
        if (u1loop < FS11AC_MCS_CONFIGURABLESS)
        {
            if ((au1MCSValue[u1loop] != FS11AC_MCS_RATE7) &&
                (au1MCSValue[u1loop] != FS11AC_MCS_RATE8) &&
                (au1MCSValue[u1loop] != FS11AC_MCS_RATE9) &&
                (au1MCSValue[u1loop] != FS11AC_MCS_SSRESERVED))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_11AC_TX_INVALID_VALUE);
                return SNMP_FAILURE;
            }
        }
        else
        {
            if (au1MCSValue[u1loop] != FS11AC_MCS_SSRESERVED)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_11AC_TX_INVALID_VALUE);
                return SNMP_FAILURE;
            }
        }
    }
    if (nmhGetFsDot11ACVHTTxMCSMap (i4IfIndex, &RetValFsDot11ACVHTTxMCSMap)
        == SNMP_SUCCESS)
    {
        for (u1loop = 0; u1loop < FS11AC_MCS_CONFIGURABLESS; u1loop++)
        {
            if ((au1MCSValue[u1loop] != FS11AC_MCS_SSRESERVED) &&
                (au1MCSValue[u1loop] > au1dbMCSValue[u1loop]))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                CLI_SET_ERR (CLI_11AC_TX_INCONSISTENT_VALUE);

                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsDot11ACCurrentChannelCenterFrequencyIndex1Config
Input       :  The Indices
IfIndex

The Object 
testValFsDot11ACCurrentChannelCenterFrequencyIndex1Config
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsDot11ACCurrentChannelCenterFrequencyIndex1Config (UINT4
               *pu4ErrorCode,
               INT4 i4IfIndex,
               UINT4
               u4TestValFsDot11ACCurrentChannelCenterFrequencyIndex1Config)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValFsDot11ACCurrentChannelCenterFrequencyIndex1Config);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot11VHTOptionImplemented
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11VHTOptionImplemented
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsDot11VHTOptionImplemented(UINT4 *pu4ErrorCode , 
        INT4 i4IfIndex , INT4 i4TestValFsDot11VHTOptionImplemented)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsDot11VHTOptionImplemented);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsDot11OperatingModeNotificationImplemented
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11OperatingModeNotificationImplemented
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsDot11OperatingModeNotificationImplemented(UINT4 *pu4ErrorCode , 
        INT4 i4IfIndex , INT4 i4TestValFsDot11OperatingModeNotificationImplemented)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsDot11OperatingModeNotificationImplemented);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsDot11ExtendedChannelSwitchActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11ExtendedChannelSwitchActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsDot11ExtendedChannelSwitchActivated(UINT4 *pu4ErrorCode , 
        INT4 i4IfIndex , INT4 i4TestValFsDot11ExtendedChannelSwitchActivated)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValFsDot11ExtendedChannelSwitchActivated);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsDot11FragmentationThreshold
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11FragmentationThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsDot11FragmentationThreshold(UINT4 *pu4ErrorCode , 
        INT4 i4IfIndex , UINT4 u4TestValFsDot11FragmentationThreshold)
{
    UINT4 u4ErrorCode = 0;
    INT1  i1Result =0;
    i1Result = wsscfgTestv2FsDot11FragmentationThreshold(&u4ErrorCode ,
            i4IfIndex , u4TestValFsDot11FragmentationThreshold);
    if(i1Result == SNMP_FAILURE)
    {
        *pu4ErrorCode = u4ErrorCode;
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsDot11ACConfigTable
Input       :  The Indices
IfIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsDot11ACConfigTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}


/* LOW LEVEL Routines for Table : FsDot11VHTStationConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot11VHTStationConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsDot11VHTStationConfigTable(INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot11VHTStationConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsDot11VHTStationConfigTable(INT4 *pi4IfIndex)
{
    tRadioIfDB         *pRadioIfDB = NULL;
    UINT4               u4RadioType = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4NextIfIndex = 0;

    pRadioIfDB =
        (tRadioIfDB *) RBTreeGetFirst (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB);
    if (pRadioIfDB != NULL)
    {
        i4RadioIfIndex = (INT4) pRadioIfDB->u4VirtualRadioIfIndex;
        do
        {
        if (nmhGetFsDot11RadioType (i4RadioIfIndex, &u4RadioType) != SNMP_SUCCESS)
        {
            CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
            return SNMP_FAILURE;

        }
        if (u4RadioType != RADIO_TYPE_AC)
        {
            if(nmhGetNextIndexFsDot11ACConfigTable(i4RadioIfIndex, &i4NextIfIndex)==SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
                return SNMP_FAILURE;
            }
            else
            {
                i4RadioIfIndex = i4NextIfIndex;
            }
        }
        else
        {
            break;
        }

        }while(u4RadioType != RADIO_TYPE_AC);

        *pi4IfIndex = i4RadioIfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}
/****************************************************************************
 Function    :  nmhGetNextIndexFsDot11VHTStationConfigTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsDot11VHTStationConfigTable(INT4 i4IfIndex ,
                INT4 *pi4NextIfIndex )
{

    tRadioIfDB          RadioIfDB;
    tRadioIfDB         *pNextRadioIfDB = NULL;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4RadioType = 0;

    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));
    RadioIfDB.u4VirtualRadioIfIndex = (UINT4) i4IfIndex;

    pNextRadioIfDB =
        (tRadioIfDB *) 
        RBTreeGetNext (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                                      (tRBElem *) & RadioIfDB, NULL);
    if (pNextRadioIfDB != NULL)
    {
        i4RadioIfIndex = (INT4) pNextRadioIfDB->u4VirtualRadioIfIndex;
        RadioIfDB.u4VirtualRadioIfIndex = (UINT4) i4RadioIfIndex;
        do
        {
        if (nmhGetFsDot11RadioType (i4RadioIfIndex, &u4RadioType) 
                != SNMP_SUCCESS)
        {
            CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
            return SNMP_FAILURE;

        }
        if (u4RadioType != RADIO_TYPE_AC)
        {
            pNextRadioIfDB = (tRadioIfDB *) 
                RBTreeGetNext (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                                        (tRBElem *) & RadioIfDB, NULL);
            if (pNextRadioIfDB == NULL)
            {
                CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
                return SNMP_FAILURE;
            }
            else
            {
                i4RadioIfIndex = (INT4) pNextRadioIfDB->u4VirtualRadioIfIndex;
                RadioIfDB.u4VirtualRadioIfIndex = (UINT4) i4RadioIfIndex;
            }
        }
        else
        {
            break;
        }
        }while(u4RadioType != RADIO_TYPE_AC);

        *pi4NextIfIndex = (INT4) pNextRadioIfDB->u4VirtualRadioIfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDot11VHTRxHighestDataRateConfig
 Input       :  The Indices
                IfIndex

                The Object
                retValFsDot11VHTRxHighestDataRateSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsDot11VHTRxHighestDataRateConfig(INT4 i4IfIndex , 
            UINT4 *pu4RetValFsDot11VHTRxHighestDataRateConfig)
{
    UINT4 u4RetValFsDot11VHTRxHighestDataRateSupported = 0;
    INT1 i1Result = 0;
    i1Result = wsscfgGetFsDot11VHTRxHighestDataRateConfig(i4IfIndex,
                &u4RetValFsDot11VHTRxHighestDataRateSupported);
    if(i1Result == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsDot11VHTRxHighestDataRateConfig = 
            u4RetValFsDot11VHTRxHighestDataRateSupported;
        return SNMP_SUCCESS;
    }
}
/****************************************************************************
 Function    :  nmhGetFsDot11VHTTxHighestDataRateConfig
 Input       :  The Indices
                IfIndex

                The Object
                retValFsDot11VHTTxHighestDataRateSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsDot11VHTTxHighestDataRateConfig(INT4 i4IfIndex , 
            UINT4 *pu4RetValFsDot11VHTTxHighestDataRateConfig)
{
    UINT4 u4RetValFsDot11VHTTxHighestDataRateSupported = 0;
    INT1 i1Result = 0;
    i1Result = wsscfgGetFsDot11VHTTxHighestDataRateConfig(i4IfIndex,
            &u4RetValFsDot11VHTTxHighestDataRateSupported);
    if(i1Result == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsDot11VHTTxHighestDataRateConfig = 
            u4RetValFsDot11VHTTxHighestDataRateSupported;
        return SNMP_SUCCESS;
    }
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDot11VHTRxHighestDataRateConfig 
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11VHTRxHighestDataRateSupported
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsDot11VHTRxHighestDataRateConfig(INT4 i4IfIndex , 
            UINT4 u4SetValFsDot11VHTRxHighestDataRateConfig)
{
        UNUSED_PARAM (i4IfIndex);
        UNUSED_PARAM (u4SetValFsDot11VHTRxHighestDataRateConfig);
        return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsDot11VHTTxHighestDataRateConfig 
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11VHTTxHighestDataRateSupported
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsDot11VHTTxHighestDataRateConfig(INT4 i4IfIndex , 
            UINT4 u4SetValFsDot11VHTTxHighestDataRateConfig)
{
        UNUSED_PARAM (i4IfIndex);
        UNUSED_PARAM (u4SetValFsDot11VHTTxHighestDataRateConfig);
        return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDot11VHTRxHighestDataRateConfig 
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11VHTRxHighestDataRateSupported
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsDot11VHTRxHighestDataRateConfig(UINT4 *pu4ErrorCode , 
            INT4 i4IfIndex , UINT4 u4TestValFsDot11VHTRxHighestDataRateConfig)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValFsDot11VHTRxHighestDataRateConfig);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsDot11VHTTxHighestDataRateConfig
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11VHTTxHighestDataRateSupported
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsDot11VHTTxHighestDataRateConfig(UINT4 *pu4ErrorCode ,
        INT4 i4IfIndex , UINT4 u4TestValFsDot11VHTTxHighestDataRateConfig)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValFsDot11VHTTxHighestDataRateConfig);
    return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot11VHTStationConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsDot11VHTStationConfigTable(UINT4 *pu4ErrorCode,
        tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsDot11PhyVHTTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot11PhyVHTTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsDot11PhyVHTTable(INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot11PhyVHTTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsDot11PhyVHTTable(INT4 *pi4IfIndex)
{
    tRadioIfDB         *pRadioIfDB = NULL;
    UINT4               u4RadioType = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4NextIfIndex = 0;

    pRadioIfDB =
        (tRadioIfDB *) 
        RBTreeGetFirst (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB);
    if (pRadioIfDB != NULL)
    {
        i4RadioIfIndex = (INT4) pRadioIfDB->u4VirtualRadioIfIndex;
        do
        {
        if (nmhGetFsDot11RadioType (i4RadioIfIndex, &u4RadioType) 
                != SNMP_SUCCESS)
        {
            CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
            return SNMP_FAILURE;

        }
        if (u4RadioType != RADIO_TYPE_AC)
        {
            if(nmhGetNextIndexFsDot11ACConfigTable(i4RadioIfIndex, 
                        &i4NextIfIndex)==SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
                return SNMP_FAILURE;
            }
            else
            {
                i4RadioIfIndex = i4NextIfIndex;
            }
        }
        else
        {
            break;
        }

        }while(u4RadioType != RADIO_TYPE_AC);

        *pi4IfIndex = i4RadioIfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetNextIndexFsDot11PhyVHTTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsDot11PhyVHTTable(INT4 i4IfIndex ,INT4 *pi4NextIfIndex )
{

    tRadioIfDB          RadioIfDB;
    tRadioIfDB         *pNextRadioIfDB = NULL;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4RadioType = 0;


    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfDB));
    RadioIfDB.u4VirtualRadioIfIndex = (UINT4) i4IfIndex;

    pNextRadioIfDB =
        (tRadioIfDB *) 
        RBTreeGetNext (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                                      (tRBElem *) & RadioIfDB, NULL);
    if (pNextRadioIfDB != NULL)
    {
        i4RadioIfIndex = (INT4) pNextRadioIfDB->u4VirtualRadioIfIndex;
        RadioIfDB.u4VirtualRadioIfIndex = (UINT4) i4RadioIfIndex;
        do
        {
        if (nmhGetFsDot11RadioType (i4RadioIfIndex, &u4RadioType) 
                != SNMP_SUCCESS)
        {
            CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
            return SNMP_FAILURE;

        }
        if (u4RadioType != RADIO_TYPE_AC)
        {
            pNextRadioIfDB = (tRadioIfDB *) 
                RBTreeGetNext (gRadioIfGlobals.RadioIfGlbMib.RadioIfDB,
                                        (tRBElem *) & RadioIfDB, NULL);
            if (pNextRadioIfDB == NULL)
            {
                CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
                return SNMP_FAILURE;
            }
            else
            {
                i4RadioIfIndex = (INT4) pNextRadioIfDB->u4VirtualRadioIfIndex;
                RadioIfDB.u4VirtualRadioIfIndex = (UINT4) i4RadioIfIndex;
            }
        }
        else
        {
            break;
        }
        }while(u4RadioType != RADIO_TYPE_AC);

        *pi4NextIfIndex = (INT4) pNextRadioIfDB->u4VirtualRadioIfIndex;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDot11NumberOfSpatialStreamsImplemented
 Input       :  The Indices
                IfIndex

                The Object
                retValFsDot11NumberOfSpatialStreamsImplemented
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsDot11NumberOfSpatialStreamsImplemented(INT4 i4IfIndex , 
        UINT4 *pu4RetValFsDot11NumberOfSpatialStreamsImplemented)
{
    UINT4 u4RetValFsDot11NumberOfSpatialStreamsImplemented = 0;
    INT1 i1Result = 0;
    i1Result = wsscfgGetFsDot11NumberOfSpatialStreamsImplemented(i4IfIndex,
                &u4RetValFsDot11NumberOfSpatialStreamsImplemented);
    if(i1Result == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsDot11NumberOfSpatialStreamsImplemented = 
            u4RetValFsDot11NumberOfSpatialStreamsImplemented;
        return SNMP_SUCCESS;
    }
}
/****************************************************************************
 Function    :  nmhGetFsDot11NumberOfSpatialStreamsActivated
 Input       :  The Indices
                IfIndex

                The Object
                retValFsDot11NumberOfSpatialStreamsActivated
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsDot11NumberOfSpatialStreamsActivated(INT4 i4IfIndex , UINT4 
            *pu4RetValFsDot11NumberOfSpatialStreamsActivated)
{
    UINT4 u4RetValFsDot11NumberOfSpatialStreamsActivated = 0;
    INT1 i1Result = 0;
    i1Result = wsscfgGetFsDot11NumberOfSpatialStreamsActivated(i4IfIndex,
                &u4RetValFsDot11NumberOfSpatialStreamsActivated);
    if(i1Result == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsDot11NumberOfSpatialStreamsActivated = 
            u4RetValFsDot11NumberOfSpatialStreamsActivated;
        return SNMP_SUCCESS;
    }
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDot11NumberOfSpatialStreamsImplemented
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11NumberOfSpatialStreamsImplemented
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsDot11NumberOfSpatialStreamsImplemented(INT4 i4IfIndex , 
        UINT4 u4SetValFsDot11NumberOfSpatialStreamsImplemented)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SetValFsDot11NumberOfSpatialStreamsImplemented);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsDot11NumberOfSpatialStreamsActivated
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11NumberOfSpatialStreamsActivated
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsDot11NumberOfSpatialStreamsActivated(INT4 i4IfIndex , 
        UINT4 u4SetValFsDot11NumberOfSpatialStreamsActivated)
{
    INT1 i1Result = 0;
    i1Result = wsscfgSetFsDot11NumberOfSpatialStreamsActivated(i4IfIndex,
            u4SetValFsDot11NumberOfSpatialStreamsActivated);
    if(i1Result == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}
/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsDot11NumberOfSpatialStreamsImplemented
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11NumberOfSpatialStreamsImplemented
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsDot11NumberOfSpatialStreamsImplemented(UINT4 *pu4ErrorCode ,
        INT4 i4IfIndex , UINT4 u4TestValFsDot11NumberOfSpatialStreamsImplemented)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4TestValFsDot11NumberOfSpatialStreamsImplemented);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsDot11NumberOfSpatialStreamsActivated
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11NumberOfSpatialStreamsActivated
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsDot11NumberOfSpatialStreamsActivated(UINT4 *pu4ErrorCode ,
        INT4 i4IfIndex , UINT4 u4TestValFsDot11NumberOfSpatialStreamsActivated)
{
    UINT4 u4ErrorCode = 0;
    INT1  i1Result =0;
    i1Result = wsscfgTestv2FsDot11NumberOfSpatialStreamsActivated(&u4ErrorCode,
            i4IfIndex , u4TestValFsDot11NumberOfSpatialStreamsActivated);
    if(i1Result == SNMP_FAILURE)
    {
        *pu4ErrorCode = u4ErrorCode;
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot11PhyVHTTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsDot11PhyVHTTable(UINT4 *pu4ErrorCode, 
        tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

