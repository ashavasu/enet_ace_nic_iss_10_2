/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: wsscfgtmr.c,v 1.1.1.1 2016/09/20 10:42:39 siva Exp $
 *
 * Description : This file contains procedures containing Wsscfg Timer
 *               related operations
 *****************************************************************************/
#include "wsscfginc.h"

PRIVATE tWsscfgTmrDesc gaWsscfgTmrDesc[WSSCFG_MAX_TMRS];

/****************************************************************************
*                                                                           *
* Function     : WsscfgTmrInitTmrDesc                                       *
*                                                                           *
* Description  : Initialize Wsscfg Timer Decriptors                         *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID 
WsscfgTmrInitTmrDesc()
{

    WSSCFG_TRC_FUNC((WSSCFG_FN_ENTRY, "FUNC:WsscfgTmrInitTmrDesc\n"));

    WSSCFG_TRC_FUNC((WSSCFG_FN_EXIT, "FUNC:EXIT WsscfgTmrInitTmrDesc\n"));

}

/****************************************************************************
*                                                                           *
* Function     : WsscfgTmrStartTmr                                          *
*                                                                           *
* Description  : Starts Wsscfg Timer                                        *
*                                                                           *
* Input        : pWsscfgTmr - pointer to WsscfgTmr structure                *
*                eWsscfgTmrId - WSSCFG timer ID                             *
*                u4Secs     - ticks in seconds                              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID 
WsscfgTmrStartTmr( tWsscfgTmr * pWsscfgTmr, enWsscfgTmrId eWsscfgTmrId,
    UINT4 u4Secs)
{

    WSSCFG_TRC_FUNC((WSSCFG_FN_ENTRY, "FUNC:WsscfgTmrStartTmr\n"));

    pWsscfgTmr->eWsscfgTmrId = eWsscfgTmrId;

    if (TmrStartTimer (gWsscfgGlobals.wsscfgTmrLst, &(pWsscfgTmr->tmrNode),
         (UINT4) NO_OF_TICKS_PER_SEC * u4Secs) == TMR_FAILURE) {
        WSSCFG_TRC((WSSCFG_TMR_TRC, "Tmr Start Failure\n"));
    }

    WSSCFG_TRC_FUNC((WSSCFG_FN_EXIT, "FUNC: Exit WsscfgTmrStartTmr\n"));
    return;
}

/****************************************************************************
*                                                                           *
* Function     : WsscfgTmrRestartTmr                                        *
*                                                                           *
* Description  :  ReStarts Wsscfg Timer                                     *
*                                                                           *
* Input        : pWsscfgTmr - pointer to WsscfgTmr structure                *
*                eWsscfgTmrId - WSSCFG timer ID                             *
*                u4Secs     - ticks in seconds                              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID 
WsscfgTmrRestartTmr( tWsscfgTmr * pWsscfgTmr,
    enWsscfgTmrId eWsscfgTmrId, UINT4 u4Secs)
{

    WSSCFG_TRC_FUNC((WSSCFG_FN_ENTRY, "FUNC:WsscfgTmrRestartTmr\n"));

    TmrStopTimer(gWsscfgGlobals.wsscfgTmrLst, &(pWsscfgTmr->tmrNode));
    WsscfgTmrStartTmr(pWsscfgTmr, eWsscfgTmrId, u4Secs);

    WSSCFG_TRC_FUNC((WSSCFG_FN_EXIT,
                     "FUNC: Exit WsscfgTmrStartTmr\n"));
    return;
}

/****************************************************************************
*                                                                           *
* Function     : WsscfgTmrStopTmr                                           *
*                                                                           *
* Description  : Restarts Wsscfg Timer                                      *
*                                                                           *
* Input        : pWsscfgTmr - pointer to WsscfgTmr structure                *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID 
WsscfgTmrStopTmr( tWsscfgTmr * pWsscfgTmr)
{

    WSSCFG_TRC_FUNC((WSSCFG_FN_ENTRY, "FUNC:WsscfgTmrStopTmr\n"));

    TmrStopTimer(gWsscfgGlobals.wsscfgTmrLst, &(pWsscfgTmr->tmrNode));

    WSSCFG_TRC_FUNC((WSSCFG_FN_EXIT, "FUNC: Exit WsscfgTmrStopTmr\n"));
    return;
}

/****************************************************************************
*                                                                           *
* Function     : WsscfgTmrHandleExpiry                                      *
*                                                                           *
* Description  : This procedure is invoked when the event indicating a time *
*                expiry occurs. This procedure finds the expired timers and *
*                invokes the corresponding timer routines.                  *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/
PUBLIC VOID 
WsscfgTmrHandleExpiry( VOID)
{
    tTmrAppTimer *pExpiredTimers = NULL;
    enWsscfgTmrId eWsscfgTmrId = WSSCFG_TMR1;
    INT2 i2Offset = 0;

    WSSCFG_TRC_FUNC((WSSCFG_FN_ENTRY, "FUNC: WsscfgTmrHandleExpiry\n"));
    while((pExpiredTimers = TmrGetNextExpiredTimer(gWsscfgGlobals.wsscfgTmrLst))
            != NULL) {

        eWsscfgTmrId = ((tWsscfgTmr *) pExpiredTimers)->eWsscfgTmrId;
        i2Offset = gaWsscfgTmrDesc[eWsscfgTmrId].i2Offset;

        if (i2Offset < 0) {

            /* The timer function does not take any parameter */
            (*(gaWsscfgTmrDesc[eWsscfgTmrId].pTmrExpFunc)) (NULL);

        } else {

            /* The timer function requires a parameter */
            (*(gaWsscfgTmrDesc[eWsscfgTmrId].pTmrExpFunc))
                ((UINT1 *) pExpiredTimers - i2Offset);
        }
    }
    WSSCFG_TRC_FUNC((WSSCFG_FN_EXIT,
                "FUNC: Exit WsscfgTmrHandleExpiry\n"));
    return;

}


/*-----------------------------------------------------------------------*/
/*                       End of the file  wsscfgtmr.c                      */
/*-----------------------------------------------------------------------*/
