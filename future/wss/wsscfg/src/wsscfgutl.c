/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
 *  $Id: wsscfgutl.c,v 1.9 2018/01/09 10:59:06 siva Exp $
*
* Description: This file contains utility functions used by protocol Wsscfg
*********************************************************************/
#ifndef _WSSCFGUTLC_
#define _WSSCFGUTLC_

#include "wsscfginc.h"
#include "wsscfgcli.h"
#include "tftpc.h"
#include "capwapcli.h"
#include "capwapconst.h"
#include "wssifpmdb.h"
#include "wsspm.h"
#include "fs11aclw.h"
#include "wssstawlcmacr.h"
#include "fswssllw.h"
#ifdef WLC_WANTED
#include "wsswlanwlcproto.h"
#endif
extern UINT4        gu4ClientWalkIndex;
extern tWssClientSummary gaWssClientSummary[MAX_STA_SUPP_PER_WLC];
extern INT4         CapwapGetWtpProfileNameFromProfileId (UINT1 *, UINT4);
PUBLIC tWssIfWtpFwlFilterDB *ApFwlDbaseSearchFilter (tWssIfWtpFwlFilterDB *
                                                     pWssIfWtpFwlFilter,
                                                     UINT4
                                                     u4CapwapBaseWtpProfileId,
                                                     UINT1
                                                     au1FilterName
                                                     [AP_FWL_MAX_FILTER_NAME_LEN]);
PUBLIC INT1         ApFwlCheckFilterCombination (UINT2 u2Filter1_MinValue,
                                                 UINT2 u2Filter1_MaxValue,
                                                 UINT2 u2Filter2_MinValue,
                                                 UINT2 u2Filter2_MaxValue,
                                                 UINT2 *pu2MinValue,
                                                 UINT2 *pu2MaxValue);

PUBLIC INT1
 
 
 
 
ApFwlValidateRuleCombination (tWssIfWtpFwlFilterDB * pFilterNode,
                              tWssIfWtpFwlRuleDB * pRuleNode);
/****************************************************************************
  * Function    :  WssCfgGetWtpDot11StatsEntry
  * Input       :   pWssIfCapDB
  * Descritpion :  This Routine checks set value and invoke corresponding
  *                module
  * Output      :  None
  * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
***************************************************************************/
INT4
WssCfgGetWtpDot11StatsEntry (tWssIfPMDB * pWssIfPMDB)
{

    if (WssIfProcessPMDBMsg
        (WSS_PM_CLI_CAPWAP_SHOW_AP_IEEE_80211_STATS_GET,
         pWssIfPMDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 ** Function    :  WssCfgGetSSIDStatsEntry
 ** Input       :   pWssIfPMDB
 ** Descritpion :  This Routine checks set value and invoke corresponding
 **                module
 ** Output      :  None
 ** Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/
INT4
WssCfgGetSSIDStatsEntry (tWssIfPMDB * pWssIfPMDB)
{

    if (WssIfProcessPMDBMsg
        (WSS_PM_CLI_SSID_STATS_GET, pWssIfPMDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgGetRadioStatsEntry
 * Input       :   pWssIfPMDB
 * Descritpion :  This Routine checks set value and invoke corresponding
 *                module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/
INT4
WssCfgGetRadioStatsEntry (tWssIfPMDB * pWssIfPMDB)
{

    if (WssIfProcessPMDBMsg
        (WSS_PM_WTP_EVT_VSP_RADIO_STATS_GET, pWssIfPMDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgGetClientStatsEntry
 * Input       :   pWssIfPMDB
 * Descritpion :  This Routine checks set value and invoke corresponding
 *                module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 * ***************************************************************************/
INT4
WssCfgGetClientStatsEntry (tWssIfPMDB * pWssIfPMDB)
{

    if (WssIfProcessPMDBMsg
        (WSS_PM_WTP_EVT_VSP_CLIENT_STATS_GET, pWssIfPMDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11StationConfigTable
 Input       :  pWsscfgGetDot11StationConfigEntry
                pWsscfgdsDot11StationConfigEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11StationConfigTable (tWsscfgDot11StationConfigEntry *
                                        pWsscfgGetDot11StationConfigEntry,
                                        tWsscfgDot11StationConfigEntry *
                                        pWsscfgdsDot11StationConfigEntry)
{
    tWssWlanDB         *pWssWlanMsgDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    INT4                i4RetValIfMainType = 0;

    if (nmhGetIfMainType
        (pWsscfgGetDot11StationConfigEntry->MibObject.i4IfIndex,
         &i4RetValIfMainType) == SNMP_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllDot11StationConfigTable:"
                     "nmhGetIfMainType Returns Failure\r\n"));
        return OSIX_FAILURE;
    }

    if ((i4RetValIfMainType == CFA_RADIO) ||
        (i4RetValIfMainType == CFA_CAPWAP_VIRT_RADIO))
    {
        MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
        RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
            UtlShMemAllocAntennaSelectionBuf ();

        if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC, "\r%% Memory allocation for Antenna"
                         " selection failed.\r\n"));
            return OSIX_FAILURE;
        }
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex
            = (UINT4) pWsscfgGetDot11StationConfigEntry->MibObject.i4IfIndex;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;

        if (WssCfgGetRadioParams (&RadioIfGetDB) == OSIX_FAILURE)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllDot11StationConfigTable:"
                         "Returns Failure\r\n"));
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            return OSIX_FAILURE;
        }
        pWsscfgGetDot11StationConfigEntry->MibObject.i4Dot11BeaconPeriod =
            (INT4) RadioIfGetDB.RadioIfGetAllDB.u2BeaconPeriod;

        pWsscfgGetDot11StationConfigEntry->MibObject.i4Dot11DTIMPeriod =
            (INT4) RadioIfGetDB.RadioIfGetAllDB.u1DTIMPeriod;

        MEMCPY (pWsscfgGetDot11StationConfigEntry->
                MibObject.au1Dot11CountryString,
                RadioIfGetDB.RadioIfGetAllDB.au1CountryString,
                sizeof (RadioIfGetDB.RadioIfGetAllDB.au1CountryString));
        pWsscfgGetDot11StationConfigEntry->
            MibObject.i4Dot11CountryStringLen =
            (INT4) STRLEN (RadioIfGetDB.RadioIfGetAllDB.au1CountryString);

        MEMCPY (pWsscfgGetDot11StationConfigEntry->
                MibObject.au1Dot11OperationalRateSet,
                RadioIfGetDB.RadioIfGetAllDB.au1OperationalRate,
                sizeof (RadioIfGetDB.RadioIfGetAllDB.au1OperationalRate));

        pWsscfgGetDot11StationConfigEntry->
            MibObject.i4Dot11OperationalRateSetLen = RADIOIF_OPER_RATE;
        /*   (INT4) STRLEN(RadioIfGetDB.RadioIfGetAllDB.au1OperationalRate); */
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
    }
    else if (i4RetValIfMainType == CFA_CAPWAP_DOT11_PROFILE)
    {
        pWssWlanMsgDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
        if (pWssWlanMsgDB == NULL)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgGetAllDot11StationConfigTable:- "
                         "UtlShMemAllocWlanDbBuf returned failure\n"));
            return OSIX_FAILURE;
        }

        MEMSET (pWssWlanMsgDB, 0, sizeof (tWssWlanDB));

        pWssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex =
            (UINT4) pWsscfgGetDot11StationConfigEntry->MibObject.i4IfIndex;
        pWssWlanMsgDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

        /* only DesiredSsid & DtimPeriod supported for now. need to add here
           whenever support for new elements are implemented */
        pWssWlanMsgDB->WssWlanIsPresentDB.bDesiredSsid = OSIX_TRUE;
        pWssWlanMsgDB->WssWlanIsPresentDB.bDtimPeriod = OSIX_TRUE;

        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                      pWssWlanMsgDB) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgGetAllDot11StationConfigTable:"
                         "Usupported/Invalid nmhGetIfMainType Failure\r\n"));
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
            return OSIX_FAILURE;
        }

        MEMCPY (pWsscfgGetDot11StationConfigEntry->MibObject.
                au1Dot11DesiredSSID,
                pWssWlanMsgDB->WssWlanAttributeDB.au1DesiredSsid,
                WSS_WLAN_SSID_NAME_LEN);
        pWsscfgGetDot11StationConfigEntry->MibObject.i4Dot11DesiredSSIDLen =
            (INT4) STRLEN (pWssWlanMsgDB->WssWlanAttributeDB.au1DesiredSsid);
        pWsscfgGetDot11StationConfigEntry->MibObject.i4Dot11DTIMPeriod =
            pWssWlanMsgDB->WssWlanAttributeDB.i2DtimPeriod;
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
    }
    else
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllDot11StationConfigTable:"
                     "Usupported/Invalid nmhGetIfMainType Failure\r\n"));
        return OSIX_FAILURE;
    }

    UNUSED_PARAM (pWsscfgdsDot11StationConfigEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11AuthenticationAlgorithmsTable
 Input       :  pWsscfgGetDot11AuthenticationAlgorithmsEntry
                pWsscfgdsDot11AuthenticationAlgorithmsEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllUtlDot11AuthenticationAlgorithmsTable
    (tWsscfgDot11AuthenticationAlgorithmsEntry *
     pWsscfgGetDot11AuthenticationAlgorithmsEntry,
     tWsscfgDot11AuthenticationAlgorithmsEntry *
     pWsscfgdsDot11AuthenticationAlgorithmsEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11AuthenticationAlgorithmsEntry);
    UNUSED_PARAM (pWsscfgdsDot11AuthenticationAlgorithmsEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11WEPDefaultKeysTable
 Input       :  pWsscfgGetDot11WEPDefaultKeysEntry
                pWsscfgdsDot11WEPDefaultKeysEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11WEPDefaultKeysTable (tWsscfgDot11WEPDefaultKeysEntry *
                                         pWsscfgGetDot11WEPDefaultKeysEntry,
                                         tWsscfgDot11WEPDefaultKeysEntry *
                                         pWsscfgdsDot11WEPDefaultKeysEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11WEPDefaultKeysEntry);
    UNUSED_PARAM (pWsscfgdsDot11WEPDefaultKeysEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11WEPKeyMappingsTable
 Input       :  pWsscfgGetDot11WEPKeyMappingsEntry
                pWsscfgdsDot11WEPKeyMappingsEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11WEPKeyMappingsTable (tWsscfgDot11WEPKeyMappingsEntry *
                                         pWsscfgGetDot11WEPKeyMappingsEntry,
                                         tWsscfgDot11WEPKeyMappingsEntry *
                                         pWsscfgdsDot11WEPKeyMappingsEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11WEPKeyMappingsEntry);
    UNUSED_PARAM (pWsscfgdsDot11WEPKeyMappingsEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11PrivacyTable
 Input       :  pWsscfgGetDot11PrivacyEntry
                pWsscfgdsDot11PrivacyEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11PrivacyTable (tWsscfgDot11PrivacyEntry *
                                  pWsscfgGetDot11PrivacyEntry,
                                  tWsscfgDot11PrivacyEntry *
                                  pWsscfgdsDot11PrivacyEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11PrivacyEntry);
    UNUSED_PARAM (pWsscfgdsDot11PrivacyEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11MultiDomainCapabilityTable
 Input       :  pWsscfgGetDot11MultiDomainCapabilityEntry
                pWsscfgdsDot11MultiDomainCapabilityEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllUtlDot11MultiDomainCapabilityTable
    (tWsscfgDot11MultiDomainCapabilityEntry *
     pWsscfgGetDot11MultiDomainCapabilityEntry,
     tWsscfgDot11MultiDomainCapabilityEntry *
     pWsscfgdsDot11MultiDomainCapabilityEntry)
{
    tRadioIfGetDB       RadioIfGetDB;
    INT4                i4Dot11MultiDomainCapabilityIndex = 0;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        (UINT4) pWsscfgGetDot11MultiDomainCapabilityEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;

    if (WssCfgGetRadioParams (&RadioIfGetDB) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllUtlDot11MultiDomainCapabilityTable:"
                     "Returns Failure\r\n"));
        return OSIX_FAILURE;
    }
    i4Dot11MultiDomainCapabilityIndex =
        pWsscfgGetDot11MultiDomainCapabilityEntry->MibObject.
        i4Dot11MultiDomainCapabilityIndex;

    pWsscfgGetDot11MultiDomainCapabilityEntry->MibObject.
        i4Dot11FirstChannelNumber =
        (INT4) (RadioIfGetDB.RadioIfGetAllDB.
                MultiDomainInfo[i4Dot11MultiDomainCapabilityIndex -
                                1].u2FirstChannelNo);
    pWsscfgGetDot11MultiDomainCapabilityEntry->MibObject.
        i4Dot11NumberofChannels =
        (INT4) (RadioIfGetDB.RadioIfGetAllDB.
                MultiDomainInfo[i4Dot11MultiDomainCapabilityIndex -
                                1].u2NoOfChannels);
    pWsscfgGetDot11MultiDomainCapabilityEntry->MibObject.
        i4Dot11MaximumTransmitPowerLevel =
        (INT4) (RadioIfGetDB.RadioIfGetAllDB.
                MultiDomainInfo[i4Dot11MultiDomainCapabilityIndex -
                                1].u2MaxTxPowerLevel);
    UNUSED_PARAM (pWsscfgdsDot11MultiDomainCapabilityEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11SpectrumManagementTable
 Input       :  pWsscfgGetDot11SpectrumManagementEntry
                pWsscfgdsDot11SpectrumManagementEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11SpectrumManagementTable (tWsscfgDot11SpectrumManagementEntry
                                             *
                                             pWsscfgGetDot11SpectrumManagementEntry,
                                             tWsscfgDot11SpectrumManagementEntry
                                             *
                                             pWsscfgdsDot11SpectrumManagementEntry)
{
    tWssWlanDB         *pWssWlanDB = NULL;
    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllUtlDot11SpectrumManagementTable:- "
                     "UtlShMemAllocWlanDbBuf returned failure\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));

    pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex =
        (UINT4) pWsscfgGetDot11SpectrumManagementEntry->MibObject.i4IfIndex;
    pWssWlanDB->WssWlanIsPresentDB.bMitigationRequirement = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, pWssWlanDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }
    pWsscfgGetDot11SpectrumManagementEntry->
        MibObject.i4Dot11MitigationRequirement =
        pWssWlanDB->WssWlanAttributeDB.u1MitigationRequirement;

    UNUSED_PARAM (pWsscfgdsDot11SpectrumManagementEntry);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11RegulatoryClassesTable
 Input       :  pWsscfgGetDot11RegulatoryClassesEntry
                pWsscfgdsDot11RegulatoryClassesEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11RegulatoryClassesTable (tWsscfgDot11RegulatoryClassesEntry *
                                            pWsscfgGetDot11RegulatoryClassesEntry,
                                            tWsscfgDot11RegulatoryClassesEntry *
                                            pWsscfgdsDot11RegulatoryClassesEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11RegulatoryClassesEntry);
    UNUSED_PARAM (pWsscfgdsDot11RegulatoryClassesEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11OperationTable
 Input       :  pWsscfgGetDot11OperationEntry
                pWsscfgdsDot11OperationEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11OperationTable (tWsscfgDot11OperationEntry *
                                    pWsscfgGetDot11OperationEntry,
                                    tWsscfgDot11OperationEntry *
                                    pWsscfgdsDot11OperationEntry)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n"));
        return OSIX_FAILURE;
    }

    UNUSED_PARAM (pWsscfgdsDot11OperationEntry);

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        (UINT4) pWsscfgGetDot11OperationEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;

    if (WssCfgGetRadioParams (&RadioIfGetDB) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllUtlDot11StationConfigTable:"
                     "WssCfgGetRadioParams returns Failure\r\n"));
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }
    pWsscfgGetDot11OperationEntry->MibObject.u4Dot11MaxTransmitMSDULifetime =
        RadioIfGetDB.RadioIfGetAllDB.u4TxMsduLifetime;
    pWsscfgGetDot11OperationEntry->MibObject.u4Dot11MaxReceiveLifetime =
        RadioIfGetDB.RadioIfGetAllDB.u4RxMsduLifetime;
    pWsscfgGetDot11OperationEntry->MibObject.i4Dot11RTSThreshold =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.u2RTSThreshold;
    pWsscfgGetDot11OperationEntry->MibObject.i4Dot11ShortRetryLimit =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.u1ShortRetry;
    pWsscfgGetDot11OperationEntry->MibObject.i4Dot11LongRetryLimit =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.u1LongRetry;
    pWsscfgGetDot11OperationEntry->MibObject.i4Dot11FragmentationThreshold =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.u2FragmentationThreshold;

    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    UNUSED_PARAM (pWsscfgGetDot11OperationEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11CountersTable
 Input       :  pWsscfgGetDot11CountersEntry
                pWsscfgdsDot11CountersEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11CountersTable (tWsscfgDot11CountersEntry *
                                   pWsscfgGetDot11CountersEntry,
                                   tWsscfgDot11CountersEntry *
                                   pWsscfgdsDot11CountersEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11CountersEntry);
    UNUSED_PARAM (pWsscfgdsDot11CountersEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11GroupAddressesTable
 Input       :  pWsscfgGetDot11GroupAddressesEntry
                pWsscfgdsDot11GroupAddressesEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11GroupAddressesTable (tWsscfgDot11GroupAddressesEntry *
                                         pWsscfgGetDot11GroupAddressesEntry,
                                         tWsscfgDot11GroupAddressesEntry *
                                         pWsscfgdsDot11GroupAddressesEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11GroupAddressesEntry);
    UNUSED_PARAM (pWsscfgdsDot11GroupAddressesEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11EDCATable
 Input       :  pWsscfgGetDot11EDCAEntry
                pWsscfgdsDot11EDCAEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11EDCATable (tWsscfgDot11EDCAEntry * pWsscfgGetDot11EDCAEntry,
                               tWsscfgDot11EDCAEntry * pWsscfgdsDot11EDCAEntry)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               u1QosIndex = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex
        = (UINT4) pWsscfgGetDot11EDCAEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u1QosIndex =
        (UINT1) (pWsscfgGetDot11EDCAEntry->MibObject.i4Dot11EDCATableIndex);
    RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
    if (WssCfgGetEDCAParams (&RadioIfGetDB) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllUtlDot11EDCATable:"
                     "WssCfgGetRadioParams  Returns Failure\r\n"));
        return OSIX_FAILURE;
    }
    u1QosIndex = RadioIfGetDB.RadioIfGetAllDB.u1QosIndex;
    pWsscfgGetDot11EDCAEntry->MibObject.i4Dot11EDCATableCWmin =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.au2CwMin[u1QosIndex - 1];
    pWsscfgGetDot11EDCAEntry->MibObject.i4Dot11EDCATableCWmax =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.au2CwMax[u1QosIndex - 1];
    pWsscfgGetDot11EDCAEntry->MibObject.i4Dot11EDCATableAIFSN =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.au1Aifsn[u1QosIndex - 1];
    pWsscfgGetDot11EDCAEntry->MibObject.i4Dot11EDCATableTXOPLimit =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.au2TxOpLimit[u1QosIndex - 1];
    pWsscfgGetDot11EDCAEntry->MibObject.i4Dot11EDCATableMandatory =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.au1AdmissionControl[u1QosIndex - 1];

    UNUSED_PARAM (pWsscfgdsDot11EDCAEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11QAPEDCATable
 Input       :  pWsscfgGetDot11QAPEDCAEntry
                pWsscfgdsDot11QAPEDCAEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11QAPEDCATable (tWsscfgDot11QAPEDCAEntry *
                                  pWsscfgGetDot11QAPEDCAEntry,
                                  tWsscfgDot11QAPEDCAEntry *
                                  pWsscfgdsDot11QAPEDCAEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11QAPEDCAEntry);
    UNUSED_PARAM (pWsscfgdsDot11QAPEDCAEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11QosCountersTable
 Input       :  pWsscfgGetDot11QosCountersEntry
                pWsscfgdsDot11QosCountersEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11QosCountersTable (tWsscfgDot11QosCountersEntry *
                                      pWsscfgGetDot11QosCountersEntry,
                                      tWsscfgDot11QosCountersEntry *
                                      pWsscfgdsDot11QosCountersEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11QosCountersEntry);
    UNUSED_PARAM (pWsscfgdsDot11QosCountersEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11ResourceInfoTable
 Input       :  pWsscfgGetDot11ResourceInfoEntry
                pWsscfgdsDot11ResourceInfoEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11ResourceInfoTable (tWsscfgDot11ResourceInfoEntry *
                                       pWsscfgGetDot11ResourceInfoEntry,
                                       tWsscfgDot11ResourceInfoEntry *
                                       pWsscfgdsDot11ResourceInfoEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11ResourceInfoEntry);
    UNUSED_PARAM (pWsscfgdsDot11ResourceInfoEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11PhyOperationTable
 Input       :  pWsscfgGetDot11PhyOperationEntry
                pWsscfgdsDot11PhyOperationEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11PhyOperationTable (tWsscfgDot11PhyOperationEntry *
                                       pWsscfgGetDot11PhyOperationEntry,
                                       tWsscfgDot11PhyOperationEntry *
                                       pWsscfgdsDot11PhyOperationEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11PhyOperationEntry);
    UNUSED_PARAM (pWsscfgdsDot11PhyOperationEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11PhyAntennaTable
 Input       :  pWsscfgGetDot11PhyAntennaEntry
                pWsscfgdsDot11PhyAntennaEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11PhyAntennaTable (tWsscfgDot11PhyAntennaEntry *
                                     pWsscfgGetDot11PhyAntennaEntry,
                                     tWsscfgDot11PhyAntennaEntry *
                                     pWsscfgdsDot11PhyAntennaEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11PhyAntennaEntry);
    UNUSED_PARAM (pWsscfgdsDot11PhyAntennaEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11PhyTxPowerTable
 Input       :  pWsscfgGetDot11PhyTxPowerEntry
                pWsscfgdsDot11PhyTxPowerEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11PhyTxPowerTable (tWsscfgDot11PhyTxPowerEntry *
                                     pWsscfgGetDot11PhyTxPowerEntry,
                                     tWsscfgDot11PhyTxPowerEntry *
                                     pWsscfgdsDot11PhyTxPowerEntry)
{
    if (WssCfgGetTxPowerTable (pWsscfgGetDot11PhyTxPowerEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllDot11PhyTxPowerTable:"
                     "WssCfgGetTxPowerTable  Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    UNUSED_PARAM (pWsscfgdsDot11PhyTxPowerEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11PhyFHSSTable
 Input       :  pWsscfgGetDot11PhyFHSSEntry
                pWsscfgdsDot11PhyFHSSEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11PhyFHSSTable (tWsscfgDot11PhyFHSSEntry *
                                  pWsscfgGetDot11PhyFHSSEntry,
                                  tWsscfgDot11PhyFHSSEntry *
                                  pWsscfgdsDot11PhyFHSSEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11PhyFHSSEntry);
    UNUSED_PARAM (pWsscfgdsDot11PhyFHSSEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11PhyDSSSTable
 Input       :  pWsscfgGetDot11PhyDSSSEntry
                pWsscfgdsDot11PhyDSSSEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11PhyDSSSTable (tWsscfgDot11PhyDSSSEntry *
                                  pWsscfgGetDot11PhyDSSSEntry,
                                  tWsscfgDot11PhyDSSSEntry *
                                  pWsscfgdsDot11PhyDSSSEntry)
{
    if (WssCfgGetDSSSTable (pWsscfgGetDot11PhyDSSSEntry) == OSIX_FAILURE)
    {

        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllDot11PhyDSSSTable :"
                     " WssCfgGetDSSSTable  Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    UNUSED_PARAM (pWsscfgdsDot11PhyDSSSEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11PhyIRTable
 Input       :  pWsscfgGetDot11PhyIREntry
                pWsscfgdsDot11PhyIREntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11PhyIRTable (tWsscfgDot11PhyIREntry *
                                pWsscfgGetDot11PhyIREntry,
                                tWsscfgDot11PhyIREntry *
                                pWsscfgdsDot11PhyIREntry)
{
    UNUSED_PARAM (pWsscfgGetDot11PhyIREntry);
    UNUSED_PARAM (pWsscfgdsDot11PhyIREntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11RegDomainsSupportedTable
 Input       :  pWsscfgGetDot11RegDomainsSupportedEntry
                pWsscfgdsDot11RegDomainsSupportedEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllUtlDot11RegDomainsSupportedTable
    (tWsscfgDot11RegDomainsSupportedEntry *
     pWsscfgGetDot11RegDomainsSupportedEntry,
     tWsscfgDot11RegDomainsSupportedEntry *
     pWsscfgdsDot11RegDomainsSupportedEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11RegDomainsSupportedEntry);
    UNUSED_PARAM (pWsscfgdsDot11RegDomainsSupportedEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11AntennasListTable
 Input       :  pWsscfgGetDot11AntennasListEntry
                pWsscfgdsDot11AntennasListEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11AntennasListTable (tWsscfgDot11AntennasListEntry *
                                       pWsscfgGetDot11AntennasListEntry,
                                       tWsscfgDot11AntennasListEntry *
                                       pWsscfgdsDot11AntennasListEntry)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n"));
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        (UINT4) pWsscfgGetDot11AntennasListEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;

    if (WssCfgGetRadioParams (&RadioIfGetDB) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11AntennasListTable:"
                     "WssCfgGetRadioParams  Returns Failure\r\n"));
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    pWsscfgGetDot11AntennasListEntry->MibObject.i4Dot11AntennaListIndex =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.u1QosIndex;
    pWsscfgGetDot11AntennasListEntry->MibObject.i4Dot11DiversitySelectionRx =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.u1DiversitySupport;
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    UNUSED_PARAM (pWsscfgdsDot11AntennasListEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11SupportedDataRatesTxTable
 Input       :  pWsscfgGetDot11SupportedDataRatesTxEntry
                pWsscfgdsDot11SupportedDataRatesTxEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllUtlDot11SupportedDataRatesTxTable
    (tWsscfgDot11SupportedDataRatesTxEntry *
     pWsscfgGetDot11SupportedDataRatesTxEntry,
     tWsscfgDot11SupportedDataRatesTxEntry *
     pWsscfgdsDot11SupportedDataRatesTxEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11SupportedDataRatesTxEntry);
    UNUSED_PARAM (pWsscfgdsDot11SupportedDataRatesTxEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11SupportedDataRatesRxTable
 Input       :  pWsscfgGetDot11SupportedDataRatesRxEntry
                pWsscfgdsDot11SupportedDataRatesRxEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllUtlDot11SupportedDataRatesRxTable
    (tWsscfgDot11SupportedDataRatesRxEntry *
     pWsscfgGetDot11SupportedDataRatesRxEntry,
     tWsscfgDot11SupportedDataRatesRxEntry *
     pWsscfgdsDot11SupportedDataRatesRxEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11SupportedDataRatesRxEntry);
    UNUSED_PARAM (pWsscfgdsDot11SupportedDataRatesRxEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11PhyOFDMTable
 Input       :  pWsscfgGetDot11PhyOFDMEntry
                pWsscfgdsDot11PhyOFDMEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11PhyOFDMTable (tWsscfgDot11PhyOFDMEntry *
                                  pWsscfgGetDot11PhyOFDMEntry,
                                  tWsscfgDot11PhyOFDMEntry *
                                  pWsscfgdsDot11PhyOFDMEntry)
{

    if (WssCfgGetOFDMTable (pWsscfgGetDot11PhyOFDMEntry) == OSIX_FAILURE)
    {

        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllDot11PhyOFDMTable:"
                     "WssCfgGetOFDMTable  Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    UNUSED_PARAM (pWsscfgdsDot11PhyOFDMEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11PhyHRDSSSTable
 Input       :  pWsscfgGetDot11PhyHRDSSSEntry
                pWsscfgdsDot11PhyHRDSSSEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11PhyHRDSSSTable (tWsscfgDot11PhyHRDSSSEntry *
                                    pWsscfgGetDot11PhyHRDSSSEntry,
                                    tWsscfgDot11PhyHRDSSSEntry *
                                    pWsscfgdsDot11PhyHRDSSSEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11PhyHRDSSSEntry);
    UNUSED_PARAM (pWsscfgdsDot11PhyHRDSSSEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11HoppingPatternTable
 Input       :  pWsscfgGetDot11HoppingPatternEntry
                pWsscfgdsDot11HoppingPatternEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11HoppingPatternTable (tWsscfgDot11HoppingPatternEntry *
                                         pWsscfgGetDot11HoppingPatternEntry,
                                         tWsscfgDot11HoppingPatternEntry *
                                         pWsscfgdsDot11HoppingPatternEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11HoppingPatternEntry);
    UNUSED_PARAM (pWsscfgdsDot11HoppingPatternEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11PhyERPTable
 Input       :  pWsscfgGetDot11PhyERPEntry
                pWsscfgdsDot11PhyERPEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11PhyERPTable (tWsscfgDot11PhyERPEntry *
                                 pWsscfgGetDot11PhyERPEntry,
                                 tWsscfgDot11PhyERPEntry *
                                 pWsscfgdsDot11PhyERPEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11PhyERPEntry);
    UNUSED_PARAM (pWsscfgdsDot11PhyERPEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11RSNAConfigTable
 Input       :  pWsscfgGetDot11RSNAConfigEntry
                pWsscfgdsDot11RSNAConfigEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11RSNAConfigTable (tWsscfgDot11RSNAConfigEntry *
                                     pWsscfgGetDot11RSNAConfigEntry,
                                     tWsscfgDot11RSNAConfigEntry *
                                     pWsscfgdsDot11RSNAConfigEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11RSNAConfigEntry);
    UNUSED_PARAM (pWsscfgdsDot11RSNAConfigEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11RSNAConfigPairwiseCiphersTable
 Input       :  pWsscfgGetDot11RSNAConfigPairwiseCiphersEntry
                pWsscfgdsDot11RSNAConfigPairwiseCiphersEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllUtlDot11RSNAConfigPairwiseCiphersTable
    (tWsscfgDot11RSNAConfigPairwiseCiphersEntry *
     pWsscfgGetDot11RSNAConfigPairwiseCiphersEntry,
     tWsscfgDot11RSNAConfigPairwiseCiphersEntry *
     pWsscfgdsDot11RSNAConfigPairwiseCiphersEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11RSNAConfigPairwiseCiphersEntry);
    UNUSED_PARAM (pWsscfgdsDot11RSNAConfigPairwiseCiphersEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11RSNAConfigAuthenticationSuitesTable
 Input       :  pWsscfgGetDot11RSNAConfigAuthenticationSuitesEntry
                pWsscfgdsDot11RSNAConfigAuthenticationSuitesEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllUtlDot11RSNAConfigAuthenticationSuitesTable
    (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *
     pWsscfgGetDot11RSNAConfigAuthenticationSuitesEntry,
     tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *
     pWsscfgdsDot11RSNAConfigAuthenticationSuitesEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11RSNAConfigAuthenticationSuitesEntry);
    UNUSED_PARAM (pWsscfgdsDot11RSNAConfigAuthenticationSuitesEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlDot11RSNAStatsTable
 Input       :  pWsscfgGetDot11RSNAStatsEntry
                pWsscfgdsDot11RSNAStatsEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlDot11RSNAStatsTable (tWsscfgDot11RSNAStatsEntry *
                                    pWsscfgGetDot11RSNAStatsEntry,
                                    tWsscfgDot11RSNAStatsEntry *
                                    pWsscfgdsDot11RSNAStatsEntry)
{
    UNUSED_PARAM (pWsscfgGetDot11RSNAStatsEntry);
    UNUSED_PARAM (pWsscfgdsDot11RSNAStatsEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11StationConfigTable
 * Input       :   pWsscfgOldDot11StationConfigEntry
                   pWsscfgDot11StationConfigEntry
                   pWsscfgIsSetDot11StationConfigEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateDot11StationConfigTable (tWsscfgDot11StationConfigEntry *
                                         pWsscfgOldDot11StationConfigEntry,
                                         tWsscfgDot11StationConfigEntry *
                                         pWsscfgDot11StationConfigEntry,
                                         tWsscfgIsSetDot11StationConfigEntry *
                                         pWsscfgIsSetDot11StationConfigEntry)
{
    UNUSED_PARAM (pWsscfgOldDot11StationConfigEntry);

    if (WssCfgSetStationConfigTable (pWsscfgDot11StationConfigEntry,
                                     pWsscfgIsSetDot11StationConfigEntry) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11AuthenticationAlgorithmsTable
 * Input       :   pWsscfgOldDot11AuthenticationAlgorithmsEntry
                   pWsscfgDot11AuthenticationAlgorithmsEntry
                   pWsscfgIsSetDot11AuthenticationAlgorithmsEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgUtilUpdateDot11AuthenticationAlgorithmsTable
    (tWsscfgDot11AuthenticationAlgorithmsEntry *
     pWsscfgOldDot11AuthenticationAlgorithmsEntry,
     tWsscfgDot11AuthenticationAlgorithmsEntry *
     pWsscfgDot11AuthenticationAlgorithmsEntry,
     tWsscfgIsSetDot11AuthenticationAlgorithmsEntry *
     pWsscfgIsSetDot11AuthenticationAlgorithmsEntry)
{

    UNUSED_PARAM (pWsscfgOldDot11AuthenticationAlgorithmsEntry);

    if (WssCfgSetAuthenticationAlgorithmTable
        (pWsscfgDot11AuthenticationAlgorithmsEntry,
         pWsscfgIsSetDot11AuthenticationAlgorithmsEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11WEPDefaultKeysTable
 * Input       :   pWsscfgOldDot11WEPDefaultKeysEntry
                   pWsscfgDot11WEPDefaultKeysEntry
                   pWsscfgIsSetDot11WEPDefaultKeysEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateDot11WEPDefaultKeysTable (tWsscfgDot11WEPDefaultKeysEntry *
                                          pWsscfgOldDot11WEPDefaultKeysEntry,
                                          tWsscfgDot11WEPDefaultKeysEntry *
                                          pWsscfgDot11WEPDefaultKeysEntry,
                                          tWsscfgIsSetDot11WEPDefaultKeysEntry *
                                          pWsscfgIsSetDot11WEPDefaultKeysEntry)
{
    UNUSED_PARAM (pWsscfgOldDot11WEPDefaultKeysEntry);

    if (WssCfgSetWEPDefaultKeysTable (pWsscfgDot11WEPDefaultKeysEntry,
                                      pWsscfgIsSetDot11WEPDefaultKeysEntry) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11WEPKeyMappingsTable
 * Input       :   pWsscfgOldDot11WEPKeyMappingsEntry
                   pWsscfgDot11WEPKeyMappingsEntry
                   pWsscfgIsSetDot11WEPKeyMappingsEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateDot11WEPKeyMappingsTable (tWsscfgDot11WEPKeyMappingsEntry *
                                          pWsscfgOldDot11WEPKeyMappingsEntry,
                                          tWsscfgDot11WEPKeyMappingsEntry *
                                          pWsscfgDot11WEPKeyMappingsEntry,
                                          tWsscfgIsSetDot11WEPKeyMappingsEntry *
                                          pWsscfgIsSetDot11WEPKeyMappingsEntry)
{
    UNUSED_PARAM (pWsscfgOldDot11WEPKeyMappingsEntry);

    if (WssCfgSetWEPKeyMappingsTable (pWsscfgDot11WEPKeyMappingsEntry,
                                      pWsscfgIsSetDot11WEPKeyMappingsEntry) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11PrivacyTable
 * Input       :   pWsscfgOldDot11PrivacyEntry
                   pWsscfgDot11PrivacyEntry
                   pWsscfgIsSetDot11PrivacyEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateDot11PrivacyTable (tWsscfgDot11PrivacyEntry *
                                   pWsscfgOldDot11PrivacyEntry,
                                   tWsscfgDot11PrivacyEntry *
                                   pWsscfgDot11PrivacyEntry,
                                   tWsscfgIsSetDot11PrivacyEntry *
                                   pWsscfgIsSetDot11PrivacyEntry)
{

    UNUSED_PARAM (pWsscfgOldDot11PrivacyEntry);
    UNUSED_PARAM (pWsscfgDot11PrivacyEntry);
    UNUSED_PARAM (pWsscfgIsSetDot11PrivacyEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11MultiDomainCapabilityTable
 * Input       :   pWsscfgOldDot11MultiDomainCapabilityEntry
                   pWsscfgDot11MultiDomainCapabilityEntry
                   pWsscfgIsSetDot11MultiDomainCapabilityEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgUtilUpdateDot11MultiDomainCapabilityTable
    (tWsscfgDot11MultiDomainCapabilityEntry *
     pWsscfgOldDot11MultiDomainCapabilityEntry,
     tWsscfgDot11MultiDomainCapabilityEntry *
     pWsscfgDot11MultiDomainCapabilityEntry,
     tWsscfgIsSetDot11MultiDomainCapabilityEntry *
     pWsscfgIsSetDot11MultiDomainCapabilityEntry)
{

    UNUSED_PARAM (pWsscfgOldDot11MultiDomainCapabilityEntry);

    if (WssCfgSetMultiDomainCapabilityTable
        (pWsscfgDot11MultiDomainCapabilityEntry,
         pWsscfgIsSetDot11MultiDomainCapabilityEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11SpectrumManagementTable
 * Input       :   pWsscfgOldDot11SpectrumManagementEntry
                   pWsscfgDot11SpectrumManagementEntry
                   pWsscfgIsSetDot11SpectrumManagementEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgUtilUpdateDot11SpectrumManagementTable
    (tWsscfgDot11SpectrumManagementEntry *
     pWsscfgOldDot11SpectrumManagementEntry,
     tWsscfgDot11SpectrumManagementEntry * pWsscfgDot11SpectrumManagementEntry,
     tWsscfgIsSetDot11SpectrumManagementEntry *
     pWsscfgIsSetDot11SpectrumManagementEntry)
{

    UNUSED_PARAM (pWsscfgOldDot11SpectrumManagementEntry);
    if (WsscfgSetDot11SpectrumManagementTable
        (pWsscfgDot11SpectrumManagementEntry,
         pWsscfgIsSetDot11SpectrumManagementEntry) != OSIX_SUCCESS)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgUtilUpdateDot11SpectrumManagementTable:\
                    WsscfgSetDot11SpectrumManagementTable returns failure\r\n"));
        return OSIX_FAILURE;

    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11RegulatoryClassesTable
 * Input       :   pWsscfgOldDot11RegulatoryClassesEntry
                   pWsscfgDot11RegulatoryClassesEntry
                   pWsscfgIsSetDot11RegulatoryClassesEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateDot11RegulatoryClassesTable (tWsscfgDot11RegulatoryClassesEntry
                                             *
                                             pWsscfgOldDot11RegulatoryClassesEntry,
                                             tWsscfgDot11RegulatoryClassesEntry
                                             *
                                             pWsscfgDot11RegulatoryClassesEntry,
                                             tWsscfgIsSetDot11RegulatoryClassesEntry
                                             *
                                             pWsscfgIsSetDot11RegulatoryClassesEntry)
{

    UNUSED_PARAM (pWsscfgOldDot11RegulatoryClassesEntry);
    UNUSED_PARAM (pWsscfgDot11RegulatoryClassesEntry);
    UNUSED_PARAM (pWsscfgIsSetDot11RegulatoryClassesEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11OperationTable
 * Input       :   pWsscfgOldDot11OperationEntry
                   pWsscfgDot11OperationEntry
                   pWsscfgIsSetDot11OperationEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateDot11OperationTable (tWsscfgDot11OperationEntry *
                                     pWsscfgOldDot11OperationEntry,
                                     tWsscfgDot11OperationEntry *
                                     pWsscfgDot11OperationEntry,
                                     tWsscfgIsSetDot11OperationEntry *
                                     pWsscfgIsSetDot11OperationEntry)
{
    UNUSED_PARAM (pWsscfgOldDot11OperationEntry);

    if (WssCfgSetDot11OperationTable (pWsscfgDot11OperationEntry,
                                      pWsscfgIsSetDot11OperationEntry) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11GroupAddressesTable
 * Input       :   pWsscfgOldDot11GroupAddressesEntry
                   pWsscfgDot11GroupAddressesEntry
                   pWsscfgIsSetDot11GroupAddressesEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateDot11GroupAddressesTable (tWsscfgDot11GroupAddressesEntry *
                                          pWsscfgOldDot11GroupAddressesEntry,
                                          tWsscfgDot11GroupAddressesEntry *
                                          pWsscfgDot11GroupAddressesEntry,
                                          tWsscfgIsSetDot11GroupAddressesEntry *
                                          pWsscfgIsSetDot11GroupAddressesEntry)
{

    UNUSED_PARAM (pWsscfgOldDot11GroupAddressesEntry);
    UNUSED_PARAM (pWsscfgDot11GroupAddressesEntry);
    UNUSED_PARAM (pWsscfgIsSetDot11GroupAddressesEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11EDCATable
 * Input       :   pWsscfgOldDot11EDCAEntry
                   pWsscfgDot11EDCAEntry
                   pWsscfgIsSetDot11EDCAEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateDot11EDCATable (tWsscfgDot11EDCAEntry *
                                pWsscfgOldDot11EDCAEntry,
                                tWsscfgDot11EDCAEntry * pWsscfgDot11EDCAEntry,
                                tWsscfgIsSetDot11EDCAEntry *
                                pWsscfgIsSetDot11EDCAEntry)
{
    UNUSED_PARAM (pWsscfgOldDot11EDCAEntry);

    if (WssCfgSetEDCATable (pWsscfgDot11EDCAEntry, pWsscfgIsSetDot11EDCAEntry)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11QAPEDCATable
 * Input       :   pWsscfgOldDot11QAPEDCAEntry
                   pWsscfgDot11QAPEDCAEntry
                   pWsscfgIsSetDot11QAPEDCAEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateDot11QAPEDCATable (tWsscfgDot11QAPEDCAEntry *
                                   pWsscfgOldDot11QAPEDCAEntry,
                                   tWsscfgDot11QAPEDCAEntry *
                                   pWsscfgDot11QAPEDCAEntry,
                                   tWsscfgIsSetDot11QAPEDCAEntry *
                                   pWsscfgIsSetDot11QAPEDCAEntry)
{

    UNUSED_PARAM (pWsscfgOldDot11QAPEDCAEntry);
    UNUSED_PARAM (pWsscfgDot11QAPEDCAEntry);
    UNUSED_PARAM (pWsscfgIsSetDot11QAPEDCAEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11PhyOperationTable
 * Input       :   pWsscfgOldDot11PhyOperationEntry
                   pWsscfgDot11PhyOperationEntry
                   pWsscfgIsSetDot11PhyOperationEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateDot11PhyOperationTable (tWsscfgDot11PhyOperationEntry *
                                        pWsscfgOldDot11PhyOperationEntry,
                                        tWsscfgDot11PhyOperationEntry *
                                        pWsscfgDot11PhyOperationEntry,
                                        tWsscfgIsSetDot11PhyOperationEntry *
                                        pWsscfgIsSetDot11PhyOperationEntry)
{

    UNUSED_PARAM (pWsscfgOldDot11PhyOperationEntry);
    UNUSED_PARAM (pWsscfgDot11PhyOperationEntry);
    UNUSED_PARAM (pWsscfgIsSetDot11PhyOperationEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11PhyAntennaTable
 * Input       :   pWsscfgOldDot11PhyAntennaEntry
                   pWsscfgDot11PhyAntennaEntry
                   pWsscfgIsSetDot11PhyAntennaEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateDot11PhyAntennaTable (tWsscfgDot11PhyAntennaEntry *
                                      pWsscfgOldDot11PhyAntennaEntry,
                                      tWsscfgDot11PhyAntennaEntry *
                                      pWsscfgDot11PhyAntennaEntry,
                                      tWsscfgIsSetDot11PhyAntennaEntry *
                                      pWsscfgIsSetDot11PhyAntennaEntry)
{

    UNUSED_PARAM (pWsscfgOldDot11PhyAntennaEntry);
    UNUSED_PARAM (pWsscfgDot11PhyAntennaEntry);
    UNUSED_PARAM (pWsscfgIsSetDot11PhyAntennaEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlCapwapDot11WlanTable
 Input       :  pWsscfgGetCapwapDot11WlanEntry
                pWsscfgdsCapwapDot11WlanEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlCapwapDot11WlanTable (tWsscfgCapwapDot11WlanEntry *
                                     pWsscfgGetCapwapDot11WlanEntry,
                                     tWsscfgCapwapDot11WlanEntry *
                                     pWsscfgdsCapwapDot11WlanEntry)
{
    UNUSED_PARAM (pWsscfgGetCapwapDot11WlanEntry);
    UNUSED_PARAM (pWsscfgdsCapwapDot11WlanEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlCapwapDot11WlanBindTable
 Input       :  pWsscfgGetCapwapDot11WlanBindEntry
                pWsscfgdsCapwapDot11WlanBindEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlCapwapDot11WlanBindTable (tWsscfgCapwapDot11WlanBindEntry *
                                         pWsscfgGetCapwapDot11WlanBindEntry,
                                         tWsscfgCapwapDot11WlanBindEntry *
                                         pWsscfgdsCapwapDot11WlanBindEntry)
{
    UNUSED_PARAM (pWsscfgGetCapwapDot11WlanBindEntry);
    UNUSED_PARAM (pWsscfgdsCapwapDot11WlanBindEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11PhyTxPowerTable
 * Input       :   pWsscfgOldDot11PhyTxPowerEntry
                   pWsscfgDot11PhyTxPowerEntry
                   pWsscfgIsSetDot11PhyTxPowerEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateDot11PhyTxPowerTable (tWsscfgDot11PhyTxPowerEntry *
                                      pWsscfgOldDot11PhyTxPowerEntry,
                                      tWsscfgDot11PhyTxPowerEntry *
                                      pWsscfgDot11PhyTxPowerEntry,
                                      tWsscfgIsSetDot11PhyTxPowerEntry *
                                      pWsscfgIsSetDot11PhyTxPowerEntry)
{
    UNUSED_PARAM (pWsscfgOldDot11PhyTxPowerEntry);

    if (WssCfgSetTxPowerTable (pWsscfgDot11PhyTxPowerEntry,
                               pWsscfgIsSetDot11PhyTxPowerEntry) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11PhyFHSSTable
 * Input       :   pWsscfgOldDot11PhyFHSSEntry
                   pWsscfgDot11PhyFHSSEntry
                   pWsscfgIsSetDot11PhyFHSSEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateDot11PhyFHSSTable (tWsscfgDot11PhyFHSSEntry *
                                   pWsscfgOldDot11PhyFHSSEntry,
                                   tWsscfgDot11PhyFHSSEntry *
                                   pWsscfgDot11PhyFHSSEntry,
                                   tWsscfgIsSetDot11PhyFHSSEntry *
                                   pWsscfgIsSetDot11PhyFHSSEntry)
{

    UNUSED_PARAM (pWsscfgOldDot11PhyFHSSEntry);
    UNUSED_PARAM (pWsscfgDot11PhyFHSSEntry);
    UNUSED_PARAM (pWsscfgIsSetDot11PhyFHSSEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11PhyDSSSTable
 * Input       :   pWsscfgOldDot11PhyDSSSEntry
                   pWsscfgDot11PhyDSSSEntry
                   pWsscfgIsSetDot11PhyDSSSEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateDot11PhyDSSSTable (tWsscfgDot11PhyDSSSEntry *
                                   pWsscfgOldDot11PhyDSSSEntry,
                                   tWsscfgDot11PhyDSSSEntry *
                                   pWsscfgDot11PhyDSSSEntry,
                                   tWsscfgIsSetDot11PhyDSSSEntry *
                                   pWsscfgIsSetDot11PhyDSSSEntry)
{

    UNUSED_PARAM (pWsscfgOldDot11PhyDSSSEntry);

    if (WssCfgSetDSSSTable (pWsscfgDot11PhyDSSSEntry,
                            pWsscfgIsSetDot11PhyDSSSEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11PhyIRTable
 * Input       :   pWsscfgOldDot11PhyIREntry
                   pWsscfgDot11PhyIREntry
                   pWsscfgIsSetDot11PhyIREntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateDot11PhyIRTable (tWsscfgDot11PhyIREntry *
                                 pWsscfgOldDot11PhyIREntry,
                                 tWsscfgDot11PhyIREntry *
                                 pWsscfgDot11PhyIREntry,
                                 tWsscfgIsSetDot11PhyIREntry *
                                 pWsscfgIsSetDot11PhyIREntry)
{

    UNUSED_PARAM (pWsscfgOldDot11PhyIREntry);
    UNUSED_PARAM (pWsscfgDot11PhyIREntry);
    UNUSED_PARAM (pWsscfgIsSetDot11PhyIREntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11AntennasListTable
 * Input       :   pWsscfgOldDot11AntennasListEntry
                   pWsscfgDot11AntennasListEntry
                   pWsscfgIsSetDot11AntennasListEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateDot11AntennasListTable (tWsscfgDot11AntennasListEntry *
                                        pWsscfgOldDot11AntennasListEntry,
                                        tWsscfgDot11AntennasListEntry *
                                        pWsscfgDot11AntennasListEntry,
                                        tWsscfgIsSetDot11AntennasListEntry *
                                        pWsscfgIsSetDot11AntennasListEntry)
{
    UNUSED_PARAM (pWsscfgOldDot11AntennasListEntry);

    if (WssCfgSetDot11AntennasList (pWsscfgDot11AntennasListEntry,
                                    pWsscfgIsSetDot11AntennasListEntry) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11PhyOFDMTable
 * Input       :   pWsscfgOldDot11PhyOFDMEntry
                   pWsscfgDot11PhyOFDMEntry
                   pWsscfgIsSetDot11PhyOFDMEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateDot11PhyOFDMTable (tWsscfgDot11PhyOFDMEntry *
                                   pWsscfgOldDot11PhyOFDMEntry,
                                   tWsscfgDot11PhyOFDMEntry *
                                   pWsscfgDot11PhyOFDMEntry,
                                   tWsscfgIsSetDot11PhyOFDMEntry *
                                   pWsscfgIsSetDot11PhyOFDMEntry)
{
    UNUSED_PARAM (pWsscfgOldDot11PhyOFDMEntry);

    if (WssCfgSetOFDMTable (pWsscfgDot11PhyOFDMEntry,
                            pWsscfgIsSetDot11PhyOFDMEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11HoppingPatternTable
 * Input       :   pWsscfgOldDot11HoppingPatternEntry
                   pWsscfgDot11HoppingPatternEntry
                   pWsscfgIsSetDot11HoppingPatternEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateDot11HoppingPatternTable (tWsscfgDot11HoppingPatternEntry *
                                          pWsscfgOldDot11HoppingPatternEntry,
                                          tWsscfgDot11HoppingPatternEntry *
                                          pWsscfgDot11HoppingPatternEntry,
                                          tWsscfgIsSetDot11HoppingPatternEntry *
                                          pWsscfgIsSetDot11HoppingPatternEntry)
{

    UNUSED_PARAM (pWsscfgOldDot11HoppingPatternEntry);
    UNUSED_PARAM (pWsscfgDot11HoppingPatternEntry);
    UNUSED_PARAM (pWsscfgIsSetDot11HoppingPatternEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11PhyERPTable
 * Input       :   pWsscfgOldDot11PhyERPEntry
                   pWsscfgDot11PhyERPEntry
                   pWsscfgIsSetDot11PhyERPEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateDot11PhyERPTable (tWsscfgDot11PhyERPEntry *
                                  pWsscfgOldDot11PhyERPEntry,
                                  tWsscfgDot11PhyERPEntry *
                                  pWsscfgDot11PhyERPEntry,
                                  tWsscfgIsSetDot11PhyERPEntry *
                                  pWsscfgIsSetDot11PhyERPEntry)
{

    UNUSED_PARAM (pWsscfgOldDot11PhyERPEntry);
    UNUSED_PARAM (pWsscfgDot11PhyERPEntry);
    UNUSED_PARAM (pWsscfgIsSetDot11PhyERPEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11RSNAConfigTable
 * Input       :   pWsscfgOldDot11RSNAConfigEntry
                   pWsscfgDot11RSNAConfigEntry
                   pWsscfgIsSetDot11RSNAConfigEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateDot11RSNAConfigTable (tWsscfgDot11RSNAConfigEntry *
                                      pWsscfgOldDot11RSNAConfigEntry,
                                      tWsscfgDot11RSNAConfigEntry *
                                      pWsscfgDot11RSNAConfigEntry,
                                      tWsscfgIsSetDot11RSNAConfigEntry *
                                      pWsscfgIsSetDot11RSNAConfigEntry)
{

    UNUSED_PARAM (pWsscfgOldDot11RSNAConfigEntry);
    UNUSED_PARAM (pWsscfgDot11RSNAConfigEntry);
    UNUSED_PARAM (pWsscfgIsSetDot11RSNAConfigEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11RSNAConfigPairwiseCiphersTable
 * Input       :   pWsscfgOldDot11RSNAConfigPairwiseCiphersEntry
                   pWsscfgDot11RSNAConfigPairwiseCiphersEntry
                   pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgUtilUpdateDot11RSNAConfigPairwiseCiphersTable
    (tWsscfgDot11RSNAConfigPairwiseCiphersEntry *
     pWsscfgOldDot11RSNAConfigPairwiseCiphersEntry,
     tWsscfgDot11RSNAConfigPairwiseCiphersEntry *
     pWsscfgDot11RSNAConfigPairwiseCiphersEntry,
     tWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry *
     pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry)
{

    UNUSED_PARAM (pWsscfgOldDot11RSNAConfigPairwiseCiphersEntry);
    UNUSED_PARAM (pWsscfgDot11RSNAConfigPairwiseCiphersEntry);
    UNUSED_PARAM (pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateDot11RSNAConfigAuthenticationSuitesTable
 * Input       :   pWsscfgOldDot11RSNAConfigAuthenticationSuitesEntry
                   pWsscfgDot11RSNAConfigAuthenticationSuitesEntry
                   pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgUtilUpdateDot11RSNAConfigAuthenticationSuitesTable
    (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *
     pWsscfgOldDot11RSNAConfigAuthenticationSuitesEntry,
     tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *
     pWsscfgDot11RSNAConfigAuthenticationSuitesEntry,
     tWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry *
     pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry)
{

    UNUSED_PARAM (pWsscfgOldDot11RSNAConfigAuthenticationSuitesEntry);
    UNUSED_PARAM (pWsscfgDot11RSNAConfigAuthenticationSuitesEntry);
    UNUSED_PARAM (pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateCapwapDot11WlanTable
 * Input       :   pWsscfgOldCapwapDot11WlanEntry
                   pWsscfgCapwapDot11WlanEntry
                   pWsscfgIsSetCapwapDot11WlanEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateCapwapDot11WlanTable (tWsscfgCapwapDot11WlanEntry *
                                      pWsscfgOldCapwapDot11WlanEntry,
                                      tWsscfgCapwapDot11WlanEntry *
                                      pWsscfgCapwapDot11WlanEntry,
                                      tWsscfgIsSetCapwapDot11WlanEntry *
                                      pWsscfgIsSetCapwapDot11WlanEntry)
{
    UNUSED_PARAM (pWsscfgOldCapwapDot11WlanEntry);

    if (WssCfgSetWlanProfile (pWsscfgCapwapDot11WlanEntry,
                              pWsscfgIsSetCapwapDot11WlanEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateCapwapDot11WlanBindTable
 * Input       :   pWsscfgOldCapwapDot11WlanBindEntry
                   pWsscfgCapwapDot11WlanBindEntry
                   pWsscfgIsSetCapwapDot11WlanBindEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateCapwapDot11WlanBindTable (tWsscfgCapwapDot11WlanBindEntry *
                                          pWsscfgOldCapwapDot11WlanBindEntry,
                                          tWsscfgCapwapDot11WlanBindEntry *
                                          pWsscfgCapwapDot11WlanBindEntry,
                                          tWsscfgIsSetCapwapDot11WlanBindEntry *
                                          pWsscfgIsSetCapwapDot11WlanBindEntry)
{
    UNUSED_PARAM (pWsscfgOldCapwapDot11WlanBindEntry);

    if (WssCfgSetWlanBinding (pWsscfgCapwapDot11WlanBindEntry,
                              pWsscfgIsSetCapwapDot11WlanBindEntry) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsDot11StationConfigTable
 Input       :  pWsscfgGetFsDot11StationConfigEntry
                pWsscfgdsFsDot11StationConfigEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlFsDot11StationConfigTable (tWsscfgFsDot11StationConfigEntry *
                                          pWsscfgGetFsDot11StationConfigEntry,
                                          tWsscfgFsDot11StationConfigEntry *
                                          pWsscfgdsFsDot11StationConfigEntry)
{
#ifdef WLC_WANTED
    tWssWlanDB          WssWlanDB;

    MEMSET (&WssWlanDB, 0, sizeof (WssWlanDB));

    WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex =
        (UINT4) pWsscfgGetFsDot11StationConfigEntry->MibObject.i4IfIndex;
    WssWlanDB.WssWlanIsPresentDB.bVlanId = OSIX_TRUE;
    WssWlanDB.WssWlanIsPresentDB.bSupressSsid = OSIX_TRUE;
    WssWlanDB.WssWlanIsPresentDB.bBandwidthThresh = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &WssWlanDB) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    pWsscfgGetFsDot11StationConfigEntry->MibObject.i4FsDot11VlanId =
        WssWlanDB.WssWlanAttributeDB.u2VlanId;
    pWsscfgGetFsDot11StationConfigEntry->MibObject.i4FsDot11SupressSSID =
        WssWlanDB.WssWlanAttributeDB.u1SupressSsid;
    pWsscfgGetFsDot11StationConfigEntry->MibObject.i4FsDot11BandwidthThresh =
        (UINT4) WssWlanDB.WssWlanAttributeDB.u4BandwidthThresh;
    UNUSED_PARAM (pWsscfgdsFsDot11StationConfigEntry);
#else
    UNUSED_PARAM (pWsscfgGetFsDot11StationConfigEntry);
    UNUSED_PARAM (pWsscfgdsFsDot11StationConfigEntry);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsDot11ExternalWebAuthProfileTable
 Input       :  pWsscfgGetFsDot11ExternalWebAuthProfileEntry
                pWsscfgdsFsDot11ExternalWebAuthProfileEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllUtlFsDot11ExternalWebAuthProfileTable
    (tWsscfgFsDot11ExternalWebAuthProfileEntry *
     pWsscfgGetFsDot11ExternalWebAuthProfileEntry)
{
#ifdef WLC_WANTED
    tWssWlanDB          WssWlanDB;

    MEMSET (&WssWlanDB, 0, sizeof (WssWlanDB));

    WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex =
        (UINT4) pWsscfgGetFsDot11ExternalWebAuthProfileEntry->MibObject.
        i4IfIndex;
    WssWlanDB.WssWlanIsPresentDB.bExternalWebAuthMethod = OSIX_TRUE;
    WssWlanDB.WssWlanIsPresentDB.bExternalWebAuthUrl = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &WssWlanDB) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    pWsscfgGetFsDot11ExternalWebAuthProfileEntry->MibObject.
        i4FsDot11ExternalWebAuthMethod =
        WssWlanDB.WssWlanAttributeDB.u1ExternalWebAuthMethod;

    MEMCPY (pWsscfgGetFsDot11ExternalWebAuthProfileEntry->MibObject.
            au1FsDot11ExternalWebAuthUrl,
            WssWlanDB.WssWlanAttributeDB.au1ExternalWebAuthUrl,
            STRLEN (WssWlanDB.WssWlanAttributeDB.au1ExternalWebAuthUrl));
#else
    UNUSED_PARAM (pWsscfgGetFsDot11ExternalWebAuthProfileEntry);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsDot11CapabilityProfileTable
 Input       :  pWsscfgGetFsDot11CapabilityProfileEntry
                pWsscfgdsFsDot11CapabilityProfileEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllUtlFsDot11CapabilityProfileTable
    (tWsscfgFsDot11CapabilityProfileEntry *
     pWsscfgGetFsDot11CapabilityProfileEntry,
     tWsscfgFsDot11CapabilityProfileEntry *
     pWsscfgdsFsDot11CapabilityProfileEntry)
{
    UNUSED_PARAM (pWsscfgGetFsDot11CapabilityProfileEntry);
    UNUSED_PARAM (pWsscfgdsFsDot11CapabilityProfileEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsDot11AuthenticationProfileTable
 Input       :  pWsscfgGetFsDot11AuthenticationProfileEntry
                pWsscfgdsFsDot11AuthenticationProfileEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllUtlFsDot11AuthenticationProfileTable
    (tWsscfgFsDot11AuthenticationProfileEntry *
     pWsscfgGetFsDot11AuthenticationProfileEntry,
     tWsscfgFsDot11AuthenticationProfileEntry *
     pWsscfgdsFsDot11AuthenticationProfileEntry)
{
    UNUSED_PARAM (pWsscfgGetFsDot11AuthenticationProfileEntry);
    UNUSED_PARAM (pWsscfgdsFsDot11AuthenticationProfileEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsSecurityWebAuthGuestInfoTable
 Input       :  pWsscfgGetFsSecurityWebAuthGuestInfoEntry
                pWsscfgdsFsSecurityWebAuthGuestInfoEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllUtlFsSecurityWebAuthGuestInfoTable
    (tWsscfgFsSecurityWebAuthGuestInfoEntry *
     pWsscfgGetFsSecurityWebAuthGuestInfoEntry,
     tWsscfgFsSecurityWebAuthGuestInfoEntry *
     pWsscfgdsFsSecurityWebAuthGuestInfoEntry)
{

#ifdef WLC_WANTED
    tWssifauthDBMsgStruct *pwssStaDB = NULL;

    pwssStaDB =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pwssStaDB == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pwssStaDB, 0, sizeof (tWssifauthDBMsgStruct));
    pWsscfgGetFsSecurityWebAuthGuestInfoEntry->MibObject.
        i4FsSecurityWebAuthUNameLen =
        STRLEN (pWsscfgGetFsSecurityWebAuthGuestInfoEntry->MibObject.
                au1FsSecurityWebAuthUName);
    MEMCPY (pwssStaDB->WssStaWebAuthDB.au1UserName,
            pWsscfgGetFsSecurityWebAuthGuestInfoEntry->MibObject.
            au1FsSecurityWebAuthUName,
            pWsscfgGetFsSecurityWebAuthGuestInfoEntry->MibObject.
            i4FsSecurityWebAuthUNameLen);
    if (WssIfProcessWssAuthDBMsg (WSS_STA_WEBAUTH_GET_USER, pwssStaDB) !=
        OSIX_SUCCESS)
    {
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
        return OSIX_FAILURE;
    }

    pWsscfgGetFsSecurityWebAuthGuestInfoEntry->
        MibObject.i4FsSecurityWlanProfileId =
        (INT4) (pwssStaDB->WssStaWebAuthDB.u4WlanId);
    pWsscfgGetFsSecurityWebAuthGuestInfoEntry->
        MibObject.i4FsSecurityWebAuthUserLifetime =
        (INT4) (pwssStaDB->WssStaWebAuthDB.u4MaxUserLifetime);

    pWsscfgGetFsSecurityWebAuthGuestInfoEntry->
        MibObject.i4FsSecurityWebAuthUserEmailIdLen =
        STRLEN (pwssStaDB->WssStaWebAuthDB.au1UserEmailId);

    MEMCPY (pWsscfgGetFsSecurityWebAuthGuestInfoEntry->
            MibObject.au1FsSecurityWebAuthUserEmailId,
            pwssStaDB->WssStaWebAuthDB.au1UserEmailId,
            pWsscfgGetFsSecurityWebAuthGuestInfoEntry->
            MibObject.i4FsSecurityWebAuthUserEmailIdLen);

    pWsscfgGetFsSecurityWebAuthGuestInfoEntry->
        MibObject.i4FsSecurityWebAuthGuestInfoRowStatus =
        (INT4) (pwssStaDB->WssStaWebAuthDB.u1RowStatus);

    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pwssStaDB);
    UNUSED_PARAM (pWsscfgdsFsSecurityWebAuthGuestInfoEntry);
#else
    UNUSED_PARAM (pWsscfgGetFsSecurityWebAuthGuestInfoEntry);
    UNUSED_PARAM (pWsscfgdsFsSecurityWebAuthGuestInfoEntry);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsStationQosParamsTable
 Input       :  pWsscfgGetFsStationQosParamsEntry
                pWsscfgdsFsStationQosParamsEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlFsStationQosParamsTable (tWsscfgFsStationQosParamsEntry *
                                        pWsscfgGetFsStationQosParamsEntry,
                                        tWsscfgFsStationQosParamsEntry *
                                        pWsscfgdsFsStationQosParamsEntry)
{
    UNUSED_PARAM (pWsscfgGetFsStationQosParamsEntry);
    UNUSED_PARAM (pWsscfgdsFsStationQosParamsEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsVlanIsolationTable
 Input       :  pWsscfgGetFsVlanIsolationEntry
                pWsscfgdsFsVlanIsolationEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlFsVlanIsolationTable (tWsscfgFsVlanIsolationEntry *
                                     pWsscfgGetFsVlanIsolationEntry,
                                     tWsscfgFsVlanIsolationEntry *
                                     pWsscfgdsFsVlanIsolationEntry)
{

    tWssWlanDB          WssWlanDB;

    MEMSET (&WssWlanDB, 0, sizeof (WssWlanDB));

    WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex =
        (UINT4) pWsscfgGetFsVlanIsolationEntry->MibObject.i4IfIndex;
    WssWlanDB.WssWlanIsPresentDB.bSsidIsolation = OSIX_TRUE;

    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &WssWlanDB) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    pWsscfgGetFsVlanIsolationEntry->MibObject.i4FsVlanIsolation =
        WssWlanDB.WssWlanAttributeDB.u1SsidIsolation;

    UNUSED_PARAM (pWsscfgdsFsVlanIsolationEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsDot11RadioConfigTable
 Input       :  pWsscfgGetFsDot11RadioConfigEntry
                pWsscfgdsFsDot11RadioConfigEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlFsDot11RadioConfigTable (tWsscfgFsDot11RadioConfigEntry *
                                        pWsscfgGetFsDot11RadioConfigEntry,
                                        tWsscfgFsDot11RadioConfigEntry *
                                        pWsscfgdsFsDot11RadioConfigEntry)
{
    UNUSED_PARAM (pWsscfgdsFsDot11RadioConfigEntry);
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n"));
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        (UINT4) pWsscfgGetFsDot11RadioConfigEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bBssIdCount = OSIX_TRUE;

    if (WssCfgGetRadioParams (&RadioIfGetDB) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllUtlFsDot11RadioConfigTable:"
                     "WssIfProcessRadioIfDBMsg  Returns Failure\r\n"));
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }
    pWsscfgGetFsDot11RadioConfigEntry->
        MibObject.i4FsDot11RadioNoOfBssIdSupported =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.u1BssIdCount;
    pWsscfgGetFsDot11RadioConfigEntry->MibObject.u4FsDot11RadioType =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType;
    pWsscfgGetFsDot11RadioConfigEntry->
        MibObject.i4FsDot11RadioAntennaType =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.u1RadioAntennaType;
    pWsscfgGetFsDot11RadioConfigEntry->
        MibObject.i4FsDot11RadioFailureStatus =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.u1FailureStatus;
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsDot11QosProfileTable
 Input       :  pWsscfgGetFsDot11QosProfileEntry
                pWsscfgdsFsDot11QosProfileEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlFsDot11QosProfileTable (tWsscfgFsDot11QosProfileEntry *
                                       pWsscfgGetFsDot11QosProfileEntry,
                                       tWsscfgFsDot11QosProfileEntry *
                                       pWsscfgdsFsDot11QosProfileEntry)
{
    UNUSED_PARAM (pWsscfgGetFsDot11QosProfileEntry);
    UNUSED_PARAM (pWsscfgdsFsDot11QosProfileEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsDot11WlanCapabilityProfileTable
 Input       :  pWsscfgGetFsDot11WlanCapabilityProfileEntry
                pWsscfgdsFsDot11WlanCapabilityProfileEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllUtlFsDot11WlanCapabilityProfileTable
    (tWsscfgFsDot11WlanCapabilityProfileEntry *
     pWsscfgGetFsDot11WlanCapabilityProfileEntry,
     tWsscfgFsDot11WlanCapabilityProfileEntry *
     pWsscfgdsFsDot11WlanCapabilityProfileEntry)
{
    UNUSED_PARAM (pWsscfgdsFsDot11WlanCapabilityProfileEntry);
    UNUSED_PARAM (pWsscfgGetFsDot11WlanCapabilityProfileEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsDot11WlanAuthenticationProfileTable
 Input       :  pWsscfgGetFsDot11WlanAuthenticationProfileEntry
                pWsscfgdsFsDot11WlanAuthenticationProfileEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllUtlFsDot11WlanAuthenticationProfileTable
    (tWsscfgFsDot11WlanAuthenticationProfileEntry *
     pWsscfgGetFsDot11WlanAuthenticationProfileEntry,
     tWsscfgFsDot11WlanAuthenticationProfileEntry *
     pWsscfgdsFsDot11WlanAuthenticationProfileEntry)
{
    UNUSED_PARAM (pWsscfgGetFsDot11WlanAuthenticationProfileEntry);
    UNUSED_PARAM (pWsscfgdsFsDot11WlanAuthenticationProfileEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsDot11WlanQosProfileTable
 Input       :  pWsscfgGetFsDot11WlanQosProfileEntry
                pWsscfgdsFsDot11WlanQosProfileEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlFsDot11WlanQosProfileTable (tWsscfgFsDot11WlanQosProfileEntry *
                                           pWsscfgGetFsDot11WlanQosProfileEntry,
                                           tWsscfgFsDot11WlanQosProfileEntry *
                                           pWsscfgdsFsDot11WlanQosProfileEntry)
{
    UNUSED_PARAM (pWsscfgGetFsDot11WlanQosProfileEntry);
    UNUSED_PARAM (pWsscfgdsFsDot11WlanQosProfileEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsDot11RadioQosTable
 Input       :  pWsscfgGetFsDot11RadioQosEntry
                pWsscfgdsFsDot11RadioQosEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlFsDot11RadioQosTable (tWsscfgFsDot11RadioQosEntry *
                                     pWsscfgGetFsDot11RadioQosEntry,
                                     tWsscfgFsDot11RadioQosEntry *
                                     pWsscfgdsFsDot11RadioQosEntry)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex
        = (UINT4) pWsscfgGetFsDot11RadioQosEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u1QosIndex = 1;
    RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;

    if (WssCfgGetEDCAParams (&RadioIfGetDB) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllUtlDot11EDCATable:"
                     "WssCfgGetRadioParams  Returns Failure\r\n"));
        return OSIX_FAILURE;
    }
    pWsscfgGetFsDot11RadioQosEntry->MibObject.au1FsDot11TaggingPolicy[0] =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.u1TaggingPolicy;

    UNUSED_PARAM (pWsscfgdsFsDot11RadioQosEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsDot11QAPTable
 Input       :  pWsscfgGetFsDot11QAPEntry
                pWsscfgdsFsDot11QAPEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlFsDot11QAPTable (tWsscfgFsDot11QAPEntry *
                                pWsscfgGetFsDot11QAPEntry,
                                tWsscfgFsDot11QAPEntry *
                                pWsscfgdsFsDot11QAPEntry)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               u1QosIndex = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex
        = (UINT4) pWsscfgGetFsDot11QAPEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u1QosIndex =
        (UINT1) (pWsscfgGetFsDot11QAPEntry->MibObject.i4Dot11EDCATableIndex);
    RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;

    if (WssCfgGetEDCAParams (&RadioIfGetDB) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllUtlDot11EDCATable:"
                     "WssCfgGetRadioParams  Returns Failure\r\n"));
        return OSIX_FAILURE;
    }
    u1QosIndex = RadioIfGetDB.RadioIfGetAllDB.u1QosIndex;
    pWsscfgGetFsDot11QAPEntry->MibObject.i4FsDot11DscpValue =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.au1Dscp[u1QosIndex - 1];
    pWsscfgGetFsDot11QAPEntry->MibObject.i4FsDot11PriorityValue =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.au1QosPrio[u1QosIndex - 1];

    UNUSED_PARAM (pWsscfgdsFsDot11QAPEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsQAPProfileTable
 Input       :  pWsscfgGetFsQAPProfileEntry
                pWsscfgdsFsQAPProfileEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlFsQAPProfileTable (tWsscfgFsQAPProfileEntry *
                                  pWsscfgGetFsQAPProfileEntry,
                                  tWsscfgFsQAPProfileEntry *
                                  pWsscfgdsFsQAPProfileEntry)
{
    UNUSED_PARAM (pWsscfgdsFsQAPProfileEntry);
    UNUSED_PARAM (pWsscfgGetFsQAPProfileEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsDot11CapabilityMappingTable
 Input       :  pWsscfgGetFsDot11CapabilityMappingEntry
                pWsscfgdsFsDot11CapabilityMappingEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllUtlFsDot11CapabilityMappingTable
    (tWsscfgFsDot11CapabilityMappingEntry *
     pWsscfgGetFsDot11CapabilityMappingEntry,
     tWsscfgFsDot11CapabilityMappingEntry *
     pWsscfgdsFsDot11CapabilityMappingEntry)
{
    UNUSED_PARAM (pWsscfgGetFsDot11CapabilityMappingEntry);
    UNUSED_PARAM (pWsscfgdsFsDot11CapabilityMappingEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsDot11AuthMappingTable
 Input       :  pWsscfgGetFsDot11AuthMappingEntry
                pWsscfgdsFsDot11AuthMappingEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlFsDot11AuthMappingTable (tWsscfgFsDot11AuthMappingEntry *
                                        pWsscfgGetFsDot11AuthMappingEntry,
                                        tWsscfgFsDot11AuthMappingEntry *
                                        pWsscfgdsFsDot11AuthMappingEntry)
{
    UNUSED_PARAM (pWsscfgGetFsDot11AuthMappingEntry);
    UNUSED_PARAM (pWsscfgdsFsDot11AuthMappingEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsDot11QosMappingTable
 Input       :  pWsscfgGetFsDot11QosMappingEntry
                pWsscfgdsFsDot11QosMappingEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlFsDot11QosMappingTable (tWsscfgFsDot11QosMappingEntry *
                                       pWsscfgGetFsDot11QosMappingEntry,
                                       tWsscfgFsDot11QosMappingEntry *
                                       pWsscfgdsFsDot11QosMappingEntry)
{
    UNUSED_PARAM (pWsscfgGetFsDot11QosMappingEntry);
    UNUSED_PARAM (pWsscfgdsFsDot11QosMappingEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsDot11ClientSummaryTable
 Input       :  pWsscfgGetFsDot11ClientSummaryEntry
                pWsscfgdsFsDot11ClientSummaryEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlFsDot11ClientSummaryTable (tWsscfgFsDot11ClientSummaryEntry *
                                          pWsscfgGetFsDot11ClientSummaryEntry,
                                          tWsscfgFsDot11ClientSummaryEntry *
                                          pWsscfgdsFsDot11ClientSummaryEntry)
{
    UNUSED_PARAM (pWsscfgGetFsDot11ClientSummaryEntry);
    UNUSED_PARAM (pWsscfgdsFsDot11ClientSummaryEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsDot11AntennasListTable
 Input       :  pWsscfgGetFsDot11AntennasListEntry
                pWsscfgdsFsDot11AntennasListEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlFsDot11AntennasListTable (tWsscfgFsDot11AntennasListEntry *
                                         pWsscfgGetFsDot11AntennasListEntry,
                                         tWsscfgFsDot11AntennasListEntry *
                                         pWsscfgdsFsDot11AntennasListEntry)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n"));
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        (UINT4) pWsscfgGetFsDot11AntennasListEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bAntennaSelection
        [(pWsscfgGetFsDot11AntennasListEntry->MibObject.
          i4Dot11AntennaListIndex) - 1] = OSIX_TRUE;

    if (WssCfgGetRadioParams (&RadioIfGetDB) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11AntennasListTable:"
                     "WssCfgGetRadioParams Returns Failure\r\n"));
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    pWsscfgGetFsDot11AntennasListEntry->MibObject.i4FsAntennaMode =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.u1AntennaMode;

    pWsscfgGetFsDot11AntennasListEntry->MibObject.i4FsAntennaSelection =
        RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection
        [(pWsscfgGetFsDot11AntennasListEntry->MibObject.
          i4Dot11AntennaListIndex) - 1];
    UNUSED_PARAM (pWsscfgdsFsDot11AntennasListEntry);
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsDot11WlanTable
 Input       :  pWsscfgGetFsDot11WlanEntry
                pWsscfgdsFsDot11WlanEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlFsDot11WlanTable (tWsscfgFsDot11WlanEntry *
                                 pWsscfgGetFsDot11WlanEntry,
                                 tWsscfgFsDot11WlanEntry *
                                 pWsscfgdsFsDot11WlanEntry)
{
    UNUSED_PARAM (pWsscfgGetFsDot11WlanEntry);
    UNUSED_PARAM (pWsscfgdsFsDot11WlanEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsDot11WlanBindTable
 Input       :  pWsscfgGetFsDot11WlanBindEntry
                pWsscfgdsFsDot11WlanBindEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlFsDot11WlanBindTable (tWsscfgFsDot11WlanBindEntry *
                                     pWsscfgGetFsDot11WlanBindEntry,
                                     tWsscfgFsDot11WlanBindEntry *
                                     pWsscfgdsFsDot11WlanBindEntry)
{
    UNUSED_PARAM (pWsscfgGetFsDot11WlanBindEntry);
    UNUSED_PARAM (pWsscfgdsFsDot11WlanBindEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsDot11nConfigTable
 Input       :  pWsscfgGetFsDot11nConfigEntry
                pWsscfgdsFsDot11nConfigEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlFsDot11nConfigTable (tWsscfgFsDot11nConfigEntry *
                                    pWsscfgGetFsDot11nConfigEntry,
                                    tWsscfgFsDot11nConfigEntry *
                                    pWsscfgdsFsDot11nConfigEntry)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               u1ChannelWidth;
    UINT4               u4Dot11RadioType;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n"));
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        (UINT4) pWsscfgGetFsDot11nConfigEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;

    if (WssCfgGetRadioParams (&RadioIfGetDB) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllUtlFsDot11nConfigTable :"
                     " WssCfgGetRadioParams Returns Failure\r\n"));
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    u1ChannelWidth =
        RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1ChannelWidth;
    u4Dot11RadioType = RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType;

    if (!(WSSCFG_RADIO_TYPEN_AC_COMP (u4Dot11RadioType)))
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllUtlFsDot11nConfigTable :"
                     "Not a valid radio type\r\n"));
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    if (u1ChannelWidth != OSIX_FALSE && u1ChannelWidth != OSIX_TRUE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllUtlFsDot11nConfigTable :"
                     "Channel width not one of 20 or 40MHz\r\n"));
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    pWsscfgGetFsDot11nConfigEntry->MibObject.i4FsDot11nConfigChannelWidth =
        RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1ChannelWidth;
    pWsscfgGetFsDot11nConfigEntry->
        MibObject.i4FsDot11nConfigShortGIfor20MHz =
        RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1ShortGi20;
    pWsscfgGetFsDot11nConfigEntry->
        MibObject.i4FsDot11nConfigShortGIfor40MHz =
        RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1ShortGi40;
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    UNUSED_PARAM (pWsscfgdsFsDot11nConfigEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsDot11nMCSDataRateTable
 Input       :  pWsscfgGetFsDot11nMCSDataRateEntry
                pWsscfgdsFsDot11nMCSDataRateEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlFsDot11nMCSDataRateTable (tWsscfgFsDot11nMCSDataRateEntry *
                                         pWsscfgGetFsDot11nMCSDataRateEntry,
                                         tWsscfgFsDot11nMCSDataRateEntry *
                                         pWsscfgdsFsDot11nMCSDataRateEntry)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               u1ChannelWidth;
    UINT4               u4Dot11RadioType;
    UINT4               u4GetRadioType = 0;
    UINT4               i4DataRateIndex = 0;
    UINT4               i4ArrayDataRateIndex = 0;
    UINT1               u1DataRate = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n"));
        return OSIX_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        (UINT4) pWsscfgGetFsDot11nMCSDataRateEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;

    if (WssCfgGetRadioParams (&RadioIfGetDB) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllUtlFsDot11nMCSDataRateTable:"
                     "WssCfgGetRadioParams Returns Failure\r\n"));
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    u1ChannelWidth = RadioIfGetDB.RadioIfGetAllDB.u1ChannelWidth;
    u4Dot11RadioType = RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType;

    if (WSSCFG_RADIO_TYPE_COMP (u4Dot11RadioType))
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllUtlFsDot11nMCSDataRateTable:"
                     "Not a valid radio type\r\n"));
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    if (u1ChannelWidth != OSIX_FALSE && u1ChannelWidth != OSIX_TRUE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllUtlFsDot11nMCSDataRateTable:"
                     "Channel width not one of 20 or 40MHz\r\n"));
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    i4DataRateIndex = pWsscfgGetFsDot11nMCSDataRateEntry->MibObject.
        i4FsDot11nMCSDataRateIndex % RADIO_FIND_ARRAY_INDEX;
    i4ArrayDataRateIndex = pWsscfgGetFsDot11nMCSDataRateEntry->MibObject.
        i4FsDot11nMCSDataRateIndex / RADIO_FIND_ARRAY_INDEX;

    u1DataRate = 0x1 << i4DataRateIndex;
    u1DataRate =
        u1DataRate & RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
        au1RxMcsBitmask[i4ArrayDataRateIndex];

    if (u1DataRate != OSIX_FALSE)
    {
        u1DataRate = OSIX_TRUE;
    }

    pWsscfgGetFsDot11nMCSDataRateEntry->MibObject.i4FsDot11nMCSDataRate =
        (UINT4) u1DataRate;

    UNUSED_PARAM (pWsscfgdsFsDot11nMCSDataRateEntry);
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsWtpImageUpgradeTable
 Input       :  pWsscfgGetFsWtpImageUpgradeEntry
                pWsscfgdsFsWtpImageUpgradeEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlFsWtpImageUpgradeTable (tWsscfgFsWtpImageUpgradeEntry *
                                       pWsscfgGetFsWtpImageUpgradeEntry,
                                       tWsscfgFsWtpImageUpgradeEntry *
                                       pWsscfgdsFsWtpImageUpgradeEntry)
{
    UNUSED_PARAM (pWsscfgGetFsWtpImageUpgradeEntry);
    UNUSED_PARAM (pWsscfgdsFsWtpImageUpgradeEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsDot11StationConfigTable
 * Input       :   pWsscfgOldFsDot11StationConfigEntry
                   pWsscfgFsDot11StationConfigEntry
                   pWsscfgIsSetFsDot11StationConfigEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateFsDot11StationConfigTable (tWsscfgFsDot11StationConfigEntry *
                                           pWsscfgOldFsDot11StationConfigEntry,
                                           tWsscfgFsDot11StationConfigEntry *
                                           pWsscfgFsDot11StationConfigEntry,
                                           tWsscfgIsSetFsDot11StationConfigEntry
                                           *
                                           pWsscfgIsSetFsDot11StationConfigEntry)
{
    UNUSED_PARAM (pWsscfgOldFsDot11StationConfigEntry);

    if (WssCfgSetFsDot11StationConfigTable (pWsscfgFsDot11StationConfigEntry,
                                            pWsscfgIsSetFsDot11StationConfigEntry)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsDot11CapabilityProfileTable
 * Input       :   pWsscfgOldFsDot11CapabilityProfileEntry
                   pWsscfgFsDot11CapabilityProfileEntry
                   pWsscfgIsSetFsDot11CapabilityProfileEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgUtilUpdateFsDot11CapabilityProfileTable
    (tWsscfgFsDot11CapabilityProfileEntry *
     pWsscfgOldFsDot11CapabilityProfileEntry,
     tWsscfgFsDot11CapabilityProfileEntry *
     pWsscfgFsDot11CapabilityProfileEntry,
     tWsscfgIsSetFsDot11CapabilityProfileEntry *
     pWsscfgIsSetFsDot11CapabilityProfileEntry)
{

    UNUSED_PARAM (pWsscfgOldFsDot11CapabilityProfileEntry);

    if (WssCfgSetCapabilityProfile (pWsscfgFsDot11CapabilityProfileEntry,
                                    pWsscfgIsSetFsDot11CapabilityProfileEntry)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsDot11AuthenticationProfileTable
 * Input       :   pWsscfgOldFsDot11AuthenticationProfileEntry
                   pWsscfgFsDot11AuthenticationProfileEntry
                   pWsscfgIsSetFsDot11AuthenticationProfileEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgUtilUpdateFsDot11AuthenticationProfileTable
    (tWsscfgFsDot11AuthenticationProfileEntry *
     pWsscfgOldFsDot11AuthenticationProfileEntry,
     tWsscfgFsDot11AuthenticationProfileEntry *
     pWsscfgFsDot11AuthenticationProfileEntry,
     tWsscfgIsSetFsDot11AuthenticationProfileEntry *
     pWsscfgIsSetFsDot11AuthenticationProfileEntry)
{

    UNUSED_PARAM (pWsscfgOldFsDot11AuthenticationProfileEntry);
    UNUSED_PARAM (pWsscfgFsDot11AuthenticationProfileEntry);
    UNUSED_PARAM (pWsscfgIsSetFsDot11AuthenticationProfileEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsSecurityWebAuthGuestInfoTable
 * Input       :   pWsscfgOldFsSecurityWebAuthGuestInfoEntry
                   pWsscfgFsSecurityWebAuthGuestInfoEntry
                   pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgUtilUpdateFsSecurityWebAuthGuestInfoTable
    (tWsscfgFsSecurityWebAuthGuestInfoEntry *
     pWsscfgOldFsSecurityWebAuthGuestInfoEntry,
     tWsscfgFsSecurityWebAuthGuestInfoEntry *
     pWsscfgFsSecurityWebAuthGuestInfoEntry,
     tWsscfgIsSetFsSecurityWebAuthGuestInfoEntry *
     pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry)
{

    UNUSED_PARAM (pWsscfgOldFsSecurityWebAuthGuestInfoEntry);
    if (WssCfgSetFsSecurityWebAuthGuestInfoTable
        (pWsscfgFsSecurityWebAuthGuestInfoEntry,
         pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsStationQosParamsTable
 * Input       :   pWsscfgOldFsStationQosParamsEntry
                   pWsscfgFsStationQosParamsEntry
                   pWsscfgIsSetFsStationQosParamsEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateFsStationQosParamsTable (tWsscfgFsStationQosParamsEntry *
                                         pWsscfgOldFsStationQosParamsEntry,
                                         tWsscfgFsStationQosParamsEntry *
                                         pWsscfgFsStationQosParamsEntry,
                                         tWsscfgIsSetFsStationQosParamsEntry *
                                         pWsscfgIsSetFsStationQosParamsEntry)
{

    UNUSED_PARAM (pWsscfgOldFsStationQosParamsEntry);
    UNUSED_PARAM (pWsscfgFsStationQosParamsEntry);
    UNUSED_PARAM (pWsscfgIsSetFsStationQosParamsEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsVlanIsolationTable
 * Input       :   pWsscfgOldFsVlanIsolationEntry
                   pWsscfgFsVlanIsolationEntry
                   pWsscfgIsSetFsVlanIsolationEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateFsVlanIsolationTable (tWsscfgFsVlanIsolationEntry *
                                      pWsscfgOldFsVlanIsolationEntry,
                                      tWsscfgFsVlanIsolationEntry *
                                      pWsscfgFsVlanIsolationEntry,
                                      tWsscfgIsSetFsVlanIsolationEntry *
                                      pWsscfgIsSetFsVlanIsolationEntry)
{

    UNUSED_PARAM (pWsscfgOldFsVlanIsolationEntry);
    UNUSED_PARAM (pWsscfgFsVlanIsolationEntry);
    UNUSED_PARAM (pWsscfgIsSetFsVlanIsolationEntry);
    if (WssCfgSetVlanIsolation (pWsscfgFsVlanIsolationEntry,
                                pWsscfgIsSetFsVlanIsolationEntry) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsDot11RadioConfigTable
 * Input       :   pWsscfgOldFsDot11RadioConfigEntry
                   pWsscfgFsDot11RadioConfigEntry
                   pWsscfgIsSetFsDot11RadioConfigEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateFsDot11RadioConfigTable (tWsscfgFsDot11RadioConfigEntry *
                                         pWsscfgOldFsDot11RadioConfigEntry,
                                         tWsscfgFsDot11RadioConfigEntry *
                                         pWsscfgFsDot11RadioConfigEntry,
                                         tWsscfgIsSetFsDot11RadioConfigEntry *
                                         pWsscfgIsSetFsDot11RadioConfigEntry)
{
    UNUSED_PARAM (pWsscfgOldFsDot11RadioConfigEntry);

    if (WssCfgSetFsDot11RadioConfigTable (pWsscfgFsDot11RadioConfigEntry,
                                          pWsscfgIsSetFsDot11RadioConfigEntry)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsDot11QosProfileTable
 * Input       :   pWsscfgOldFsDot11QosProfileEntry
                   pWsscfgFsDot11QosProfileEntry
                   pWsscfgIsSetFsDot11QosProfileEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateFsDot11QosProfileTable (tWsscfgFsDot11QosProfileEntry *
                                        pWsscfgOldFsDot11QosProfileEntry,
                                        tWsscfgFsDot11QosProfileEntry *
                                        pWsscfgFsDot11QosProfileEntry,
                                        tWsscfgIsSetFsDot11QosProfileEntry *
                                        pWsscfgIsSetFsDot11QosProfileEntry)
{

    UNUSED_PARAM (pWsscfgOldFsDot11QosProfileEntry);
    UNUSED_PARAM (pWsscfgFsDot11QosProfileEntry);
    UNUSED_PARAM (pWsscfgIsSetFsDot11QosProfileEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsDot11WlanCapabilityProfileTable
 * Input       :   pWsscfgOldFsDot11WlanCapabilityProfileEntry
                   pWsscfgFsDot11WlanCapabilityProfileEntry
                   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgUtilUpdateFsDot11WlanCapabilityProfileTable
    (tWsscfgFsDot11WlanCapabilityProfileEntry *
     pWsscfgOldFsDot11WlanCapabilityProfileEntry,
     tWsscfgFsDot11WlanCapabilityProfileEntry *
     pWsscfgFsDot11WlanCapabilityProfileEntry,
     tWsscfgIsSetFsDot11WlanCapabilityProfileEntry *
     pWsscfgIsSetFsDot11WlanCapabilityProfileEntry)
{

    UNUSED_PARAM (pWsscfgOldFsDot11WlanCapabilityProfileEntry);

    if (WsscfgSetFsDot11WlanCapabilityProfile
        (pWsscfgFsDot11WlanCapabilityProfileEntry,
         pWsscfgIsSetFsDot11WlanCapabilityProfileEntry) != OSIX_SUCCESS)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgUtilUpdateFsDot11WlanCapabilityProfileTable:\
                            WsscfgSetFsDot11WlanCapabilityProfile returns" " failure.\r\n"));
        return OSIX_FAILURE;

    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsDot11WlanAuthenticationProfileTable
 * Input       :   pWsscfgOldFsDot11WlanAuthenticationProfileEntry
                   pWsscfgFsDot11WlanAuthenticationProfileEntry
                   pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgUtilUpdateFsDot11WlanAuthenticationProfileTable
    (tWsscfgFsDot11WlanAuthenticationProfileEntry *
     pWsscfgOldFsDot11WlanAuthenticationProfileEntry,
     tWsscfgFsDot11WlanAuthenticationProfileEntry *
     pWsscfgFsDot11WlanAuthenticationProfileEntry,
     tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry *
     pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry)
{

    UNUSED_PARAM (pWsscfgOldFsDot11WlanAuthenticationProfileEntry);
    if (WsscfgSetFsDot11WlanAuthenticationProfile
        (pWsscfgFsDot11WlanAuthenticationProfileEntry,
         pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry) != OSIX_SUCCESS)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgUtilUpdateFsDot11WlanAuthenticationProfileTable:\
                            WsscfgSetFsDot11WlanAuthenticationProfile function" " returns failure.\r\n"));
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsDot11WlanQosProfileTable
 * Input       :   pWsscfgOldFsDot11WlanQosProfileEntry
                   pWsscfgFsDot11WlanQosProfileEntry
                   pWsscfgIsSetFsDot11WlanQosProfileEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateFsDot11WlanQosProfileTable (tWsscfgFsDot11WlanQosProfileEntry *
                                            pWsscfgOldFsDot11WlanQosProfileEntry,
                                            tWsscfgFsDot11WlanQosProfileEntry *
                                            pWsscfgFsDot11WlanQosProfileEntry,
                                            tWsscfgIsSetFsDot11WlanQosProfileEntry
                                            *
                                            pWsscfgIsSetFsDot11WlanQosProfileEntry)
{

    UNUSED_PARAM (pWsscfgOldFsDot11WlanQosProfileEntry);
    if (WsscfgSetFsDot11WlanQosProfile (pWsscfgFsDot11WlanQosProfileEntry,
                                        pWsscfgIsSetFsDot11WlanQosProfileEntry)
        != OSIX_SUCCESS)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgUtilUpdateFsDot11WlanQosProfileTable\
                    WsscfgUtilUpdateFsDot11WlanQosProfile returns failure.\r\n"));
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsDot11RadioQosTable
 * Input       :   pWsscfgOldFsDot11RadioQosEntry
                   pWsscfgFsDot11RadioQosEntry
                   pWsscfgIsSetFsDot11RadioQosEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateFsDot11RadioQosTable (tWsscfgFsDot11RadioQosEntry *
                                      pWsscfgOldFsDot11RadioQosEntry,
                                      tWsscfgFsDot11RadioQosEntry *
                                      pWsscfgFsDot11RadioQosEntry,
                                      tWsscfgIsSetFsDot11RadioQosEntry *
                                      pWsscfgIsSetFsDot11RadioQosEntry)
{

    UNUSED_PARAM (pWsscfgOldFsDot11RadioQosEntry);
    if (WssCfgSetRadioQosTable (pWsscfgFsDot11RadioQosEntry,
                                pWsscfgIsSetFsDot11RadioQosEntry) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsDot11QAPTable
 * Input       :   pWsscfgOldFsDot11QAPEntry
                   pWsscfgFsDot11QAPEntry
                   pWsscfgIsSetFsDot11QAPEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateFsDot11QAPTable (tWsscfgFsDot11QAPEntry *
                                 pWsscfgOldFsDot11QAPEntry,
                                 tWsscfgFsDot11QAPEntry *
                                 pWsscfgFsDot11QAPEntry,
                                 tWsscfgIsSetFsDot11QAPEntry *
                                 pWsscfgIsSetFsDot11QAPEntry)
{

    UNUSED_PARAM (pWsscfgOldFsDot11QAPEntry);
    if (WssCfgSetQAPTable (pWsscfgFsDot11QAPEntry,
                           pWsscfgIsSetFsDot11QAPEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsQAPProfileTable
 * Input       :   pWsscfgOldFsQAPProfileEntry
                   pWsscfgFsQAPProfileEntry
                   pWsscfgIsSetFsQAPProfileEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateFsQAPProfileTable (tWsscfgFsQAPProfileEntry *
                                   pWsscfgOldFsQAPProfileEntry,
                                   tWsscfgFsQAPProfileEntry *
                                   pWsscfgFsQAPProfileEntry,
                                   tWsscfgIsSetFsQAPProfileEntry *
                                   pWsscfgIsSetFsQAPProfileEntry)
{

    UNUSED_PARAM (pWsscfgOldFsQAPProfileEntry);
    UNUSED_PARAM (pWsscfgFsQAPProfileEntry);
    UNUSED_PARAM (pWsscfgIsSetFsQAPProfileEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsDot11CapabilityMappingTable
 * Input       :   pWsscfgOldFsDot11CapabilityMappingEntry
                   pWsscfgFsDot11CapabilityMappingEntry
                   pWsscfgIsSetFsDot11CapabilityMappingEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgUtilUpdateFsDot11CapabilityMappingTable
    (tWsscfgFsDot11CapabilityMappingEntry *
     pWsscfgOldFsDot11CapabilityMappingEntry,
     tWsscfgFsDot11CapabilityMappingEntry *
     pWsscfgFsDot11CapabilityMappingEntry,
     tWsscfgIsSetFsDot11CapabilityMappingEntry *
     pWsscfgIsSetFsDot11CapabilityMappingEntry)
{
    tWsscfgFsDot11CapabilityProfileEntry WsscfgGetFsDot11CapabilityProfileEntry;
    tWsscfgFsDot11CapabilityProfileEntry WsscfgFsDot11CapabilityProfileEntry;

    tWsscfgFsDot11WlanCapabilityProfileEntry
        WsscfgSetFsDot11WlanCapabilityProfileEntry;
    tWsscfgIsSetFsDot11WlanCapabilityProfileEntry
        WsscfgIsSetFsDot11WlanCapabilityProfileEntry;
    UNUSED_PARAM (pWsscfgIsSetFsDot11CapabilityMappingEntry);
    MEMSET (&WsscfgGetFsDot11CapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11CapabilityProfileEntry));
    MEMSET (&WsscfgFsDot11CapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11CapabilityProfileEntry));
    MEMSET (&WsscfgSetFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));
    MEMSET (&WsscfgIsSetFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11WlanCapabilityProfileEntry));

    UNUSED_PARAM (pWsscfgOldFsDot11CapabilityMappingEntry);

    MEMCPY (WsscfgGetFsDot11CapabilityProfileEntry.
            MibObject.au1FsDot11CapabilityProfileName,
            pWsscfgFsDot11CapabilityMappingEntry->
            MibObject.au1FsDot11CapabilityMappingProfileName,
            pWsscfgFsDot11CapabilityMappingEntry->
            MibObject.i4FsDot11CapabilityMappingProfileNameLen);

    WsscfgGetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11CapabilityProfileNameLen =
        pWsscfgFsDot11CapabilityMappingEntry->
        MibObject.i4FsDot11CapabilityMappingProfileNameLen;

    if (WsscfgGetAllFsDot11CapabilityProfileTable
        (&WsscfgGetFsDot11CapabilityProfileEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    WsscfgSetFsDot11WlanCapabilityProfileEntry.
        MibObject.i4FsDot11WlanCFPollable =
        WsscfgGetFsDot11CapabilityProfileEntry.MibObject.i4FsDot11CFPollable;

    WsscfgSetFsDot11WlanCapabilityProfileEntry.
        MibObject.i4FsDot11WlanCFPollRequest =
        WsscfgGetFsDot11CapabilityProfileEntry.MibObject.i4FsDot11CFPollRequest;

    WsscfgSetFsDot11WlanCapabilityProfileEntry.
        MibObject.i4FsDot11WlanPrivacyOptionImplemented =
        WsscfgGetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11PrivacyOptionImplemented;

    WsscfgSetFsDot11WlanCapabilityProfileEntry.
        MibObject.i4FsDot11WlanShortPreambleOptionImplemented =
        WsscfgGetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11ShortPreambleOptionImplemented;

    WsscfgSetFsDot11WlanCapabilityProfileEntry.
        MibObject.i4FsDot11WlanPBCCOptionImplemented =
        WsscfgGetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11PBCCOptionImplemented;

    WsscfgSetFsDot11WlanCapabilityProfileEntry.
        MibObject.i4FsDot11WlanChannelAgilityPresent =
        WsscfgGetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11ChannelAgilityPresent;

    WsscfgSetFsDot11WlanCapabilityProfileEntry.
        MibObject.i4FsDot11WlanQosOptionImplemented =
        WsscfgGetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11QosOptionImplemented;

    WsscfgSetFsDot11WlanCapabilityProfileEntry.
        MibObject.i4FsDot11WlanSpectrumManagementRequired =
        WsscfgGetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11SpectrumManagementRequired;

    WsscfgSetFsDot11WlanCapabilityProfileEntry.
        MibObject.i4FsDot11WlanShortSlotTimeOptionImplemented =
        WsscfgGetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11ShortSlotTimeOptionImplemented;

    WsscfgSetFsDot11WlanCapabilityProfileEntry.
        MibObject.i4FsDot11WlanAPSDOptionImplemented =
        WsscfgGetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11APSDOptionImplemented;

    WsscfgSetFsDot11WlanCapabilityProfileEntry.
        MibObject.i4FsDot11WlanDSSSOFDMOptionEnabled =
        WsscfgGetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11DSSSOFDMOptionEnabled;

    WsscfgSetFsDot11WlanCapabilityProfileEntry.
        MibObject.i4FsDot11WlanDelayedBlockAckOptionImplemented =
        WsscfgGetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11DelayedBlockAckOptionImplemented;

    WsscfgSetFsDot11WlanCapabilityProfileEntry.
        MibObject.i4FsDot11WlanImmediateBlockAckOptionImplemented =
        WsscfgGetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11ImmediateBlockAckOptionImplemented;

    WsscfgSetFsDot11WlanCapabilityProfileEntry.
        MibObject.i4FsDot11WlanQAckOptionImplemented =
        WsscfgGetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11QAckOptionImplemented;

    WsscfgSetFsDot11WlanCapabilityProfileEntry.
        MibObject.i4FsDot11WlanQueueRequestOptionImplemented =
        WsscfgGetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11QueueRequestOptionImplemented;

    WsscfgSetFsDot11WlanCapabilityProfileEntry.
        MibObject.i4FsDot11WlanTXOPRequestOptionImplemented =
        WsscfgGetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11TXOPRequestOptionImplemented;
    WsscfgSetFsDot11WlanCapabilityProfileEntry.
        MibObject.i4FsDot11WlanRSNAOptionImplemented =
        WsscfgGetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11RSNAOptionImplemented;

    WsscfgSetFsDot11WlanCapabilityProfileEntry.
        MibObject.i4FsDot11WlanRSNAPreauthenticationImplemented =
        WsscfgGetFsDot11CapabilityProfileEntry.
        MibObject.i4FsDot11RSNAPreauthenticationImplemented;

    WsscfgSetFsDot11WlanCapabilityProfileEntry.
        MibObject.i4FsDot11WlanCapabilityRowStatus =
        pWsscfgFsDot11CapabilityMappingEntry->
        MibObject.i4FsDot11CapabilityMappingRowStatus;

    WsscfgSetFsDot11WlanCapabilityProfileEntry.MibObject.i4IfIndex =
        pWsscfgFsDot11CapabilityMappingEntry->MibObject.i4IfIndex;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanCFPollable = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.bFsDot11WlanCFPollRequest
        = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanPrivacyOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanShortPreambleOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanPBCCOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanChannelAgilityPresent = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanQosOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanSpectrumManagementRequired = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanShortSlotTimeOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanAPSDOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanDSSSOFDMOptionEnabled = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanDelayedBlockAckOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanImmediateBlockAckOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanQAckOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanQueueRequestOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanTXOPRequestOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanRSNAOptionImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanRSNAPreauthenticationImplemented = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.
        bFsDot11WlanCapabilityRowStatus = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanCapabilityProfileEntry.bIfIndex = OSIX_TRUE;

    if (WsscfgSetAllFsDot11WlanCapabilityProfileTable
        (&WsscfgSetFsDot11WlanCapabilityProfileEntry,
         &WsscfgIsSetFsDot11WlanCapabilityProfileEntry, OSIX_TRUE, OSIX_TRUE) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsDot11AuthMappingTable
 * Input       :   pWsscfgOldFsDot11AuthMappingEntry
                   pWsscfgFsDot11AuthMappingEntry
                   pWsscfgIsSetFsDot11AuthMappingEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateFsDot11AuthMappingTable (tWsscfgFsDot11AuthMappingEntry *
                                         pWsscfgOldFsDot11AuthMappingEntry,
                                         tWsscfgFsDot11AuthMappingEntry *
                                         pWsscfgFsDot11AuthMappingEntry,
                                         tWsscfgIsSetFsDot11AuthMappingEntry *
                                         pWsscfgIsSetFsDot11AuthMappingEntry)
{

    tWsscfgFsDot11AuthenticationProfileEntry
        WsscfgGetFsDot11AuthenticationProfileEntry;
    tWsscfgFsDot11AuthenticationProfileEntry
        WsscfgFsDot11AuthenticationProfileEntry;

    tWsscfgFsDot11WlanAuthenticationProfileEntry
        WsscfgSetFsDot11WlanAuthenticationProfileEntry;
    tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry
        WsscfgIsSetFsDot11WlanAuthenticationProfileEntry;
    UNUSED_PARAM (pWsscfgIsSetFsDot11AuthMappingEntry);

    UNUSED_PARAM (pWsscfgOldFsDot11AuthMappingEntry);

    MEMSET (&WsscfgGetFsDot11AuthenticationProfileEntry, 0,
            sizeof (tWsscfgFsDot11AuthenticationProfileEntry));
    MEMSET (&WsscfgFsDot11AuthenticationProfileEntry, 0,
            sizeof (tWsscfgFsDot11AuthenticationProfileEntry));

    MEMSET (&WsscfgSetFsDot11WlanAuthenticationProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanAuthenticationProfileEntry));
    MEMSET (&WsscfgIsSetFsDot11WlanAuthenticationProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry));

    MEMCPY (WsscfgGetFsDot11AuthenticationProfileEntry.
            MibObject.au1FsDot11AuthenticationProfileName,
            pWsscfgFsDot11AuthMappingEntry->
            MibObject.au1FsDot11AuthMappingProfileName,
            pWsscfgFsDot11AuthMappingEntry->
            MibObject.i4FsDot11AuthMappingProfileNameLen);

    WsscfgGetFsDot11AuthenticationProfileEntry.
        MibObject.i4FsDot11AuthenticationProfileNameLen =
        pWsscfgFsDot11AuthMappingEntry->
        MibObject.i4FsDot11AuthMappingProfileNameLen;
    if (WsscfgGetAllFsDot11AuthenticationProfileTable
        (&WsscfgGetFsDot11AuthenticationProfileEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    WsscfgSetFsDot11WlanAuthenticationProfileEntry.
        MibObject.i4FsDot11WlanAuthenticationAlgorithm =
        WsscfgGetFsDot11AuthenticationProfileEntry.
        MibObject.i4FsDot11AuthenticationAlgorithm;

    WsscfgSetFsDot11WlanAuthenticationProfileEntry.
        MibObject.i4FsDot11WlanWepKeyIndex =
        WsscfgGetFsDot11AuthenticationProfileEntry.
        MibObject.i4FsDot11WepKeyIndex;

    WsscfgSetFsDot11WlanAuthenticationProfileEntry.
        MibObject.i4FsDot11WlanWepKeyType =
        WsscfgGetFsDot11AuthenticationProfileEntry.
        MibObject.i4FsDot11WepKeyType;

    WsscfgSetFsDot11WlanAuthenticationProfileEntry.
        MibObject.i4FsDot11WlanWepKeyLength =
        WsscfgGetFsDot11AuthenticationProfileEntry.
        MibObject.i4FsDot11WepKeyLength;

    WsscfgSetFsDot11WlanAuthenticationProfileEntry.
        MibObject.i4FsDot11WlanWebAuthentication =
        WsscfgGetFsDot11AuthenticationProfileEntry.
        MibObject.i4FsDot11WebAuthentication;

    WsscfgSetFsDot11WlanAuthenticationProfileEntry.
        MibObject.i4FsDot11WlanAuthenticationRowStatus =
        pWsscfgFsDot11AuthMappingEntry->MibObject.i4FsDot11AuthMappingRowStatus;

    WsscfgSetFsDot11WlanAuthenticationProfileEntry.MibObject.i4IfIndex
        = pWsscfgFsDot11AuthMappingEntry->MibObject.i4IfIndex;

    WsscfgSetFsDot11WlanAuthenticationProfileEntry.
        MibObject.i4FsDot11WlanWepKeyLen =
        WsscfgGetFsDot11AuthenticationProfileEntry.MibObject.i4FsDot11WepKeyLen;

    MEMCPY (WsscfgSetFsDot11WlanAuthenticationProfileEntry.
            MibObject.au1FsDot11WlanWepKey,
            WsscfgGetFsDot11AuthenticationProfileEntry.
            MibObject.au1FsDot11WepKey,
            WsscfgGetFsDot11AuthenticationProfileEntry.
            MibObject.i4FsDot11WepKeyLen);
    WsscfgIsSetFsDot11WlanAuthenticationProfileEntry.
        bFsDot11WlanAuthenticationAlgorithm = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanAuthenticationProfileEntry.bFsDot11WlanWepKeyIndex =
        OSIX_TRUE;
    WsscfgIsSetFsDot11WlanAuthenticationProfileEntry.bFsDot11WlanWepKeyType =
        OSIX_TRUE;
    WsscfgIsSetFsDot11WlanAuthenticationProfileEntry.bFsDot11WlanWepKeyLength =
        OSIX_TRUE;
    WsscfgIsSetFsDot11WlanAuthenticationProfileEntry.bFsDot11WlanWepKey =
        OSIX_TRUE;
    WsscfgIsSetFsDot11WlanAuthenticationProfileEntry.
        bFsDot11WlanWebAuthentication = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanAuthenticationProfileEntry.
        bFsDot11WlanAuthenticationRowStatus = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanAuthenticationProfileEntry.bIfIndex = OSIX_TRUE;

    if (WsscfgSetAllFsDot11WlanAuthenticationProfileTable
        (&WsscfgSetFsDot11WlanAuthenticationProfileEntry,
         &WsscfgIsSetFsDot11WlanAuthenticationProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsDot11QosMappingTable
 * Input       :   pWsscfgOldFsDot11QosMappingEntry
                   pWsscfgFsDot11QosMappingEntry
                   pWsscfgIsSetFsDot11QosMappingEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateFsDot11QosMappingTable (tWsscfgFsDot11QosMappingEntry *
                                        pWsscfgOldFsDot11QosMappingEntry,
                                        tWsscfgFsDot11QosMappingEntry *
                                        pWsscfgFsDot11QosMappingEntry,
                                        tWsscfgIsSetFsDot11QosMappingEntry *
                                        pWsscfgIsSetFsDot11QosMappingEntry)
{
    tWsscfgFsDot11QosProfileEntry WsscfgGetFsDot11QosProfileEntry;
    tWsscfgFsDot11QosProfileEntry WsscfgFsDot11QosProfileEntry;

    tWsscfgFsDot11WlanQosProfileEntry WsscfgSetFsDot11WlanQosProfileEntry;
    tWsscfgIsSetFsDot11WlanQosProfileEntry
        WsscfgIsSetFsDot11WlanQosProfileEntry;

    UNUSED_PARAM (pWsscfgOldFsDot11QosMappingEntry);
    UNUSED_PARAM (pWsscfgIsSetFsDot11QosMappingEntry);

    MEMSET (&WsscfgGetFsDot11QosProfileEntry, 0,
            sizeof (tWsscfgFsDot11QosProfileEntry));
    MEMSET (&WsscfgFsDot11QosProfileEntry, 0,
            sizeof (tWsscfgFsDot11QosProfileEntry));
    MEMSET (&WsscfgSetFsDot11WlanQosProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanQosProfileEntry));
    MEMSET (&WsscfgIsSetFsDot11WlanQosProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11WlanQosProfileEntry));

    MEMCPY (WsscfgGetFsDot11QosProfileEntry.
            MibObject.au1FsDot11QosProfileName,
            pWsscfgFsDot11QosMappingEntry->
            MibObject.au1FsDot11QosMappingProfileName,
            pWsscfgFsDot11QosMappingEntry->
            MibObject.i4FsDot11QosMappingProfileNameLen);
    WsscfgGetFsDot11QosProfileEntry.MibObject.
        i4FsDot11QosProfileNameLen =
        pWsscfgFsDot11QosMappingEntry->
        MibObject.i4FsDot11QosMappingProfileNameLen;

    if (WsscfgGetAllFsDot11QosProfileTable
        (&WsscfgGetFsDot11QosProfileEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    WsscfgSetFsDot11WlanQosProfileEntry.MibObject.
        i4FsDot11WlanQosTraffic =
        WsscfgGetFsDot11QosProfileEntry.MibObject.i4FsDot11QosTraffic;

    WsscfgSetFsDot11WlanQosProfileEntry.
        MibObject.i4FsDot11WlanQosPassengerTrustMode =
        WsscfgGetFsDot11QosProfileEntry.
        MibObject.i4FsDot11QosPassengerTrustMode;

    WsscfgSetFsDot11WlanQosProfileEntry.
        MibObject.i4FsDot11WlanQosRowStatus =
        pWsscfgFsDot11QosMappingEntry->MibObject.i4FsDot11QosMappingRowStatus;

    WsscfgSetFsDot11WlanQosProfileEntry.MibObject.i4IfIndex =
        pWsscfgFsDot11QosMappingEntry->MibObject.i4IfIndex;

    WsscfgSetFsDot11WlanQosProfileEntry.
        MibObject.i4FsDot11WlanQosRateLimit =
        WsscfgGetFsDot11QosProfileEntry.MibObject.i4FsDot11QosRateLimit;

    WsscfgSetFsDot11WlanQosProfileEntry.
        MibObject.i4FsDot11WlanUpStreamCIR =
        WsscfgGetFsDot11QosProfileEntry.MibObject.i4FsDot11UpStreamCIR;

    WsscfgSetFsDot11WlanQosProfileEntry.
        MibObject.i4FsDot11WlanUpStreamCBS =
        WsscfgGetFsDot11QosProfileEntry.MibObject.i4FsDot11UpStreamCBS;

    WsscfgSetFsDot11WlanQosProfileEntry.
        MibObject.i4FsDot11WlanUpStreamEIR =
        WsscfgGetFsDot11QosProfileEntry.MibObject.i4FsDot11UpStreamEIR;

    WsscfgSetFsDot11WlanQosProfileEntry.
        MibObject.i4FsDot11WlanUpStreamEBS =
        WsscfgGetFsDot11QosProfileEntry.MibObject.i4FsDot11UpStreamEBS;

    WsscfgSetFsDot11WlanQosProfileEntry.
        MibObject.i4FsDot11WlanDownStreamCIR =
        WsscfgGetFsDot11QosProfileEntry.MibObject.i4FsDot11DownStreamCIR;

    WsscfgSetFsDot11WlanQosProfileEntry.
        MibObject.i4FsDot11WlanDownStreamCBS =
        WsscfgGetFsDot11QosProfileEntry.MibObject.i4FsDot11DownStreamCBS;

    WsscfgSetFsDot11WlanQosProfileEntry.
        MibObject.i4FsDot11WlanDownStreamEIR =
        WsscfgGetFsDot11QosProfileEntry.MibObject.i4FsDot11DownStreamEIR;

    WsscfgSetFsDot11WlanQosProfileEntry.
        MibObject.i4FsDot11WlanDownStreamEBS =
        WsscfgGetFsDot11QosProfileEntry.MibObject.i4FsDot11DownStreamEBS;

    WsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanQosTraffic = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanQosPassengerTrustMode
        = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanQosRowStatus = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanQosProfileEntry.bIfIndex = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanQosRateLimit = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanUpStreamCIR = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanUpStreamCBS = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanUpStreamEIR = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanUpStreamEBS = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanDownStreamCIR = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanDownStreamCBS = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanDownStreamEIR = OSIX_TRUE;
    WsscfgIsSetFsDot11WlanQosProfileEntry.bFsDot11WlanDownStreamEBS = OSIX_TRUE;

    if (WsscfgSetAllFsDot11WlanQosProfileTable
        (&WsscfgSetFsDot11WlanQosProfileEntry,
         &WsscfgIsSetFsDot11WlanQosProfileEntry, OSIX_TRUE, OSIX_TRUE) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsDot11AntennasListTable
 * Input       :   pWsscfgOldFsDot11AntennasListEntry
                   pWsscfgFsDot11AntennasListEntry
                   pWsscfgIsSetFsDot11AntennasListEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateFsDot11AntennasListTable (tWsscfgFsDot11AntennasListEntry *
                                          pWsscfgOldFsDot11AntennasListEntry,
                                          tWsscfgFsDot11AntennasListEntry *
                                          pWsscfgFsDot11AntennasListEntry,
                                          tWsscfgIsSetFsDot11AntennasListEntry *
                                          pWsscfgIsSetFsDot11AntennasListEntry)
{
    UNUSED_PARAM (pWsscfgOldFsDot11AntennasListEntry);
    if (WssCfgSetFsDot11AntennasListTable (pWsscfgFsDot11AntennasListEntry,
                                           pWsscfgIsSetFsDot11AntennasListEntry)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsDot11WlanTable
 * Input       :   pWsscfgOldFsDot11WlanEntry
                   pWsscfgFsDot11WlanEntry
                   pWsscfgIsSetFsDot11WlanEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateFsDot11WlanTable (tWsscfgFsDot11WlanEntry *
                                  pWsscfgOldFsDot11WlanEntry,
                                  tWsscfgFsDot11WlanEntry *
                                  pWsscfgFsDot11WlanEntry,
                                  tWsscfgIsSetFsDot11WlanEntry *
                                  pWsscfgIsSetFsDot11WlanEntry)
{

    UNUSED_PARAM (pWsscfgOldFsDot11WlanEntry);
    UNUSED_PARAM (pWsscfgFsDot11WlanEntry);
    UNUSED_PARAM (pWsscfgIsSetFsDot11WlanEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsDot11WlanBindTable
 * Input       :   pWsscfgOldFsDot11WlanBindEntry
                   pWsscfgFsDot11WlanBindEntry
                   pWsscfgIsSetFsDot11WlanBindEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateFsDot11WlanBindTable (tWsscfgFsDot11WlanBindEntry *
                                      pWsscfgOldFsDot11WlanBindEntry,
                                      tWsscfgFsDot11WlanBindEntry *
                                      pWsscfgFsDot11WlanBindEntry,
                                      tWsscfgIsSetFsDot11WlanBindEntry *
                                      pWsscfgIsSetFsDot11WlanBindEntry)
{

    UNUSED_PARAM (pWsscfgOldFsDot11WlanBindEntry);
    UNUSED_PARAM (pWsscfgFsDot11WlanBindEntry);
    UNUSED_PARAM (pWsscfgIsSetFsDot11WlanBindEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsDot11nConfigTable
 * Input       :   pWsscfgOldFsDot11nConfigEntry
                   pWsscfgFsDot11nConfigEntry
                   pWsscfgIsSetFsDot11nConfigEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateFsDot11nConfigTable (tWsscfgFsDot11nConfigEntry *
                                     pWsscfgOldFsDot11nConfigEntry,
                                     tWsscfgFsDot11nConfigEntry *
                                     pWsscfgFsDot11nConfigEntry,
                                     tWsscfgIsSetFsDot11nConfigEntry *
                                     pWsscfgIsSetFsDot11nConfigEntry)
{
    UNUSED_PARAM (pWsscfgOldFsDot11nConfigEntry);

    if (WssCfgSetFsDot11nFsDot11nConfigTable (pWsscfgFsDot11nConfigEntry,
                                              pWsscfgIsSetFsDot11nConfigEntry)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsDot11nMCSDataRateTable
 * Input       :   pWsscfgOldFsDot11nMCSDataRateEntry
                   pWsscfgFsDot11nMCSDataRateEntry
                   pWsscfgIsSetFsDot11nMCSDataRateEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateFsDot11nMCSDataRateTable (tWsscfgFsDot11nMCSDataRateEntry *
                                          pWsscfgOldFsDot11nMCSDataRateEntry,
                                          tWsscfgFsDot11nMCSDataRateEntry *
                                          pWsscfgFsDot11nMCSDataRateEntry,
                                          tWsscfgIsSetFsDot11nMCSDataRateEntry *
                                          pWsscfgIsSetFsDot11nMCSDataRateEntry)
{
    UNUSED_PARAM (pWsscfgOldFsDot11nMCSDataRateEntry);

    if (WssCfgSetFsDot11nMCSDataRateTable (pWsscfgFsDot11nMCSDataRateEntry,
                                           pWsscfgIsSetFsDot11nMCSDataRateEntry)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllUtlFsRrmConfigTable
 Input       :  pWsscfgGetFsRrmConfigEntry
                pWsscfgdsFsRrmConfigEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllUtlFsRrmConfigTable (tWsscfgFsRrmConfigEntry *
                                 pWsscfgGetFsRrmConfigEntry,
                                 tWsscfgFsRrmConfigEntry *
                                 pWsscfgdsFsRrmConfigEntry)
{
    UNUSED_PARAM (pWsscfgGetFsRrmConfigEntry);
    UNUSED_PARAM (pWsscfgdsFsRrmConfigEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsWtpImageUpgradeTable
 * Input       :   pWsscfgOldFsWtpImageUpgradeEntry
                   pWsscfgFsWtpImageUpgradeEntry
                   pWsscfgIsSetFsWtpImageUpgradeEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateFsWtpImageUpgradeTable (tWsscfgFsWtpImageUpgradeEntry *
                                        pWsscfgOldFsWtpImageUpgradeEntry,
                                        tWsscfgFsWtpImageUpgradeEntry *
                                        pWsscfgFsWtpImageUpgradeEntry,
                                        tWsscfgIsSetFsWtpImageUpgradeEntry *
                                        pWsscfgIsSetFsWtpImageUpgradeEntry)
{
    UNUSED_PARAM (pWsscfgOldFsWtpImageUpgradeEntry);
    UNUSED_PARAM (pWsscfgIsSetFsWtpImageUpgradeEntry);

#ifdef WLC_WANTED
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;

    UINT1              *pu1CapwapBaseWtpProfileName = NULL;
    UINT4               u4WtpProfileId = 0;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgUtilUpdateFsWtpImageUpgradeTable:- "
                     "UtlShMemAllocWlcBuf returned failure\n"));
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return OSIX_FAILURE;
    }

    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    if ((pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpImageVersion != OSIX_TRUE) &&
        (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpUpgradeDev != OSIX_TRUE) &&
        (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpName != OSIX_TRUE) &&
        (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpImageName != OSIX_TRUE) &&
        (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpServerIP != OSIX_TRUE))
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_SUCCESS;
    }
    pu1CapwapBaseWtpProfileName =
        pWsscfgFsWtpImageUpgradeEntry->MibObject.au1FsWtpName;

    if (CapwapGetWtpProfileIdFromProfileName (pu1CapwapBaseWtpProfileName,
                                              &u4WtpProfileId) != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    pWssIfCapwapDB->CapwapGetDB.u2ProfileId = (UINT2) u4WtpProfileId;
    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_INTERNAL_ID, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfConfigUpdateRsp : Getting Radio index failed\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.u2InternalId =
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId;

    MEMCPY (pWlcHdlrMsgStruct->CapwapConfigUpdateReq.imageId.data,
            pWsscfgFsWtpImageUpgradeEntry->MibObject.au1FsWtpImageName,
            STRLEN (pWsscfgFsWtpImageUpgradeEntry->MibObject.
                    au1FsWtpImageName));

    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.imageId.u2MsgEleLen
        =
        (UINT2) STRLEN (pWsscfgFsWtpImageUpgradeEntry->MibObject.
                        au1FsWtpImageName);

    pWlcHdlrMsgStruct->CapwapConfigUpdateReq.imageId.isOptional = OSIX_TRUE;

    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_CAPWAP_CONF_UPDATE_REQ,
                                pWlcHdlrMsgStruct) == OSIX_FAILURE)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {

        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }

    pWssIfCapwapDB->CapwapIsGetAllDB.bImageName = OSIX_TRUE;

    /* When all profile to be upgraded then wtp model is changed *
     * else wtp model need not be updated */

    if (pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpUpgradeDev ==
        WSSCFG_VERSION_AP_NAME_ALL)
    {
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpModelNumber = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_FALSE;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {

            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpProfileId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpModelNumber = OSIX_TRUE;
        MEMCPY (pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.au1ModelNumber,
                pWssIfCapwapDB->CapwapGetDB.au1WtpModelNumber,
                STRLEN (pWssIfCapwapDB->CapwapGetDB.au1WtpModelNumber));

        MEMCPY (pWssIfCapwapDB->CapwapGetDB.WtpModelEntry.au1WtpImageName,
                pWsscfgFsWtpImageUpgradeEntry->MibObject.au1FsWtpImageName,
                STRLEN (pWsscfgFsWtpImageUpgradeEntry->MibObject.
                        au1FsWtpImageName));

        if (WssIfProcessCapwapDBMsg
            (WSS_CAPWAP_SET_WTP_MODEL_ENTRY, pWssIfCapwapDB) != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
            return OSIX_FAILURE;
        }
    }
    pWssIfCapwapDB->CapwapIsGetAllDB.bImageName = OSIX_TRUE;
    MEMCPY (pWssIfCapwapDB->CapwapGetDB.au1WtpImageIdentifier,
            pWsscfgFsWtpImageUpgradeEntry->MibObject.au1FsWtpImageName,
            STRLEN (pWsscfgFsWtpImageUpgradeEntry->MibObject.
                    au1FsWtpImageName));

    if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_SET_DB, pWssIfCapwapDB)
        != OSIX_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return OSIX_FAILURE;
    }
    pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpRowStatus = NOT_IN_SERVICE;
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#endif
#ifdef WTP_WANTED
    UNUSED_PARAM (pWsscfgFsWtpImageUpgradeEntry);
#endif /* WTP_WANTED */
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsRrmConfigTable
 * Input       :   pWsscfgOldFsRrmConfigEntry
                   pWsscfgFsRrmConfigEntry
                   pWsscfgIsSetFsRrmConfigEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgUtilUpdateFsRrmConfigTable (tWsscfgFsRrmConfigEntry *
                                  pWsscfgOldFsRrmConfigEntry,
                                  tWsscfgFsRrmConfigEntry *
                                  pWsscfgFsRrmConfigEntry,
                                  tWsscfgIsSetFsRrmConfigEntry *
                                  pWsscfgIsSetFsRrmConfigEntry)
{

    UNUSED_PARAM (pWsscfgOldFsRrmConfigEntry);
    UNUSED_PARAM (pWsscfgFsRrmConfigEntry);
    UNUSED_PARAM (pWsscfgIsSetFsRrmConfigEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilImageTransfer
 * Input       :  FileName, Ip Address
 * Descritpion :  This Routine gets the specified file through TFTP
 * Output      :  None
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
WsscfgUtilImageTransfer (INT1 *pTftpStr, INT1 *pi1FileName,
                         tIPvXAddr * pIpAddress)
{
    /*  UINT1 *pu1DstImageName; */
    INT1               *pi1SrcImageName;
    UINT1               au1WtpName[256];
    UINT1               au1LocalDstFile[300];
    UINT1               u1DestLen = 0;
    FILE               *fp = NULL;
    /*this au1temp array is added for host name support,
     * here this array is dummy, to provide host name support
     * this array can be used*/
    UINT1               au1temp[255];

    MEMSET (&au1WtpName, 0, sizeof (UINT1));
    MEMSET (&au1LocalDstFile, 0, sizeof (UINT1));
    MEMSET (pIpAddress, 0, sizeof (tIPvXAddr));
    MEMSET (au1temp, 0, 255);

    if (CliGetTftpParams ((INT1 *) pTftpStr, pi1FileName, pIpAddress,
                          (UINT1 *) au1temp) == CLI_FAILURE)
    {
        return SNMP_FAILURE;
    }

    u1DestLen =
        (UINT1) (STRLEN (pi1FileName) + STRLEN (IMAGE_DATA_FLASH_CONF_LOC) + 1);
    SNPRINTF ((CHR1 *) au1LocalDstFile, u1DestLen, "%s%s",
              IMAGE_DATA_FLASH_CONF_LOC, pi1FileName);

    fp = fopen ((const char *) au1LocalDstFile, "rb");
    if (NULL == fp)
    {
        pi1SrcImageName = pi1FileName;
        if ((tftpcRecvFile (*pIpAddress, (const UINT1 *) pi1SrcImageName,
                            au1LocalDstFile)) != TFTPC_OK)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "Unable to copy remote file to flash\r\n");
            return SNMP_FAILURE;
        }
    }
    else
    {
        fclose (fp);
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
  * Function    :  WlanAuthMappingShowRunningConfig
  * Input       :  CliHandle
  * Descritpion :  This Routine shows the configured CLI commands of the
  *                WLAN module, by comparing with the default values.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS or CLI_FAILURE
***************************************************************************/
INT4
WlanAuthMappingShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    UINT1               au1ProfileName[256] = { 0 };
    tSNMP_OCTET_STRING_TYPE ProfileName = { NULL, 0 };
    tWssWlanDB         *pWssWlanMsgDB = NULL;
    UINT2               u2ProfileId = 0;
    pWssWlanMsgDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanMsgDB == NULL)
    {
        return OSIX_FAILURE;
    }

    ProfileName.pu1_OctetList = au1ProfileName;
    ProfileName.i4_Length = 256;
    MEMSET (pWssWlanMsgDB, 0, sizeof (tWssWlanDB));

    if (nmhGetFirstIndexFsDot11AuthMappingTable (&i4NextIfIndex) !=
        SNMP_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
        return CLI_SUCCESS;
    }
    do
    {
        CliPrintf (CliHandle, "!\r\n\r\n");
        i4IfIndex = i4NextIfIndex;
        pWssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex = (UINT4) i4IfIndex;
        pWssWlanMsgDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                      pWssWlanMsgDB) != OSIX_SUCCESS)
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
            return CLI_FAILURE;
        }
        u2ProfileId = pWssWlanMsgDB->WssWlanAttributeDB.u2WlanProfileId;

        nmhGetFsDot11AuthMappingProfileName (i4IfIndex, &ProfileName);
        CliPrintf (CliHandle, "wlan %d auth-profile %s\r\n", u2ProfileId,
                   au1ProfileName);

    }
    while (nmhGetNextIndexFsDot11AuthMappingTable (i4IfIndex, &i4NextIfIndex)
           == SNMP_SUCCESS);

    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
    return CLI_SUCCESS;
}

/****************************************************************************
  * Function    :  Dot11ConfigTableShowRunningConfig
  * Input       :  CliHandle
  * Descritpion :  This Routine shows the configured CLI commands of the
  *                WLAN module, by comparing with the default values.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS or CLI_FAILURE
***************************************************************************/
INT4
Dot11ConfigTableShowRunningConfig (tCliHandle CliHandle)
{

    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    UINT4               u4RadioType = 0;
    INT4                i4ChanWidth = 0;
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2WtpInternalId = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT1               u1RadioId = 0;
    INT4                i4ShortGI20 = 0;
    INT4                i4ShortGI40 = 0;
    UINT1               u1Flag = 0;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    if (nmhGetFirstIndexFsDot11nConfigTable (&i4NextIfIndex) != SNMP_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return CLI_SUCCESS;
    }
    CliPrintf (CliHandle, "!\r\n\r\n");
    do
    {
        u1Flag = 0;
        i4IfIndex = i4NextIfIndex;

        if (nmhGetFsDot11RadioType (i4IfIndex, &u4RadioType) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11ConfigTableShowRunningConfig :"
                       "Failed to get the RadioType for the given IfIndex",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (WSSCFG_RADIO_TYPEN_AC_COMP (u4RadioType)
            && u4RadioType != CLI_RADIO_TYPEAC)
        {
            if (nmhGetFsDot11nConfigChannelWidth (i4IfIndex, &i4ChanWidth) !=
                SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "Dot11ConfigTableShowRunningConfig :"
                           "Failed to get the channel width for the given IfIndex",
                           i4IfIndex);
                u1Flag = 1;
                continue;
            }
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = i4IfIndex;
            RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB) !=
                OSIX_SUCCESS)
            {
                CliPrintf (CliHandle, "Dot11ConfigTableShowRunningConfig :"
                           "Failed to get the RadioParams for the given IfIndex",
                           i4IfIndex);
                u1Flag = 1;
                continue;
            }
            u2WtpInternalId = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
            u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;

            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpInternalId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
                != OSIX_SUCCESS)
            {
                u1Flag = 1;
                continue;
            }
            if (i4ChanWidth != OSIX_FALSE)
            {
                CliPrintf (CliHandle,
                           "dot11 %s chan_width %d %s dot11radio %d\r\n",
                           (u4RadioType == CLI_RADIO_TYPEAN) ? "an" : "bgn",
                           i4ChanWidth,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            nmhGetFsDot11nConfigShortGIfor20MHz (i4IfIndex, &i4ShortGI20);
            nmhGetFsDot11nConfigShortGIfor40MHz (i4IfIndex, &i4ShortGI40);
            if (i4ShortGI20 != OSIX_FALSE)
            {
                CliPrintf (CliHandle,
                           "dot11 %s short guard-interval 20 enable %s "
                           "dot11radio %d\r\n",
                           (u4RadioType == CLI_RADIO_TYPEAN) ? "an" : "bgn",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            if (i4ShortGI40 != OSIX_FALSE)
            {

                CliPrintf (CliHandle,
                           "dot11 %s short guard-interval 40 enable %s "
                           "dot11radio %d\r\n",
                           (u4RadioType == CLI_RADIO_TYPEAN) ? "an" : "bgn",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }
    }
    while (nmhGetNextIndexFsDot11nConfigTable (i4IfIndex, &i4NextIfIndex) ==
           SNMP_SUCCESS);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UNUSED_PARAM (u1Flag);
    return CLI_SUCCESS;
}

/****************************************************************************
  * Function    :  Dot11NMCSShowRunningConfig
  * Input       :  CliHandle
  * Descritpion :  This Routine shows the configured CLI commands of the
  *                WLAN module, by comparing with the default values.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS or CLI_FAILURE
***************************************************************************/
INT4
Dot11NMCSShowRunningConfig (tCliHandle CliHandle)
{

    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4DataRateIndex = 0;
    INT4                i4NextDataRateIndex = 0;
    INT4                i4DataRate = 0;
    UINT4               u4RadioType = 0;
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2WtpInternalId = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT1               u1RadioId = 0;
    UINT1               u1Flag = 0;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    if (nmhGetFirstIndexFsDot11nMCSDataRateTable
        (&i4NextIfIndex, &i4DataRateIndex) != SNMP_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return CLI_SUCCESS;
    }
    do
    {
        u1Flag = 0;
        CliPrintf (CliHandle, "!\r\n\r\n");
        i4IfIndex = i4NextIfIndex;
        i4DataRateIndex = i4NextDataRateIndex;
        if (nmhGetFsDot11RadioType (i4IfIndex, &u4RadioType) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11NMCSShowRunningConfig :"
                       "Failed to get the RadioType for the given IfIndex",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (WSSCFG_RADIO_TYPEN_AC_COMP (u4RadioType))
        {
            if (nmhGetFsDot11nMCSDataRate
                (i4IfIndex, i4DataRateIndex, &i4DataRate) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "Dot11NMCSShowRunningConfig :"
                           "Failed to get the DataRate for the given IfIndex",
                           i4IfIndex);
                u1Flag = 1;
                continue;
            }
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = i4IfIndex;
            RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB) !=
                OSIX_SUCCESS)
            {
                CliPrintf (CliHandle, "Dot11NMCSShowRunningConfig :"
                           "Failed to get the RadioParams for the given IfIndex",
                           i4IfIndex);
                u1Flag = 1;
                continue;
            }
            u2WtpInternalId = RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
            u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;

            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpInternalId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
                != OSIX_SUCCESS)
            {
                u1Flag = 1;
                continue;

            }
            if ((i4DataRate != CLI_NSUPPORTMCSTXRATE_ENABLE)
                && (i4DataRateIndex < 24))
            {
                CliPrintf (CliHandle, "dot11 %s nSupport mcs tx %d enable %s "
                           "dot11radio %d\r\n",
                           (u4RadioType == CLI_RADIO_TYPEAN) ? "an" : "bgn",
                           i4DataRateIndex, pWssIfCapwapDB->CapwapGetDB.
                           au1ProfileName, u1RadioId);
            }
        }
    }
    while (nmhGetNextIndexFsDot11nMCSDataRateTable (i4IfIndex, &i4NextIfIndex,
                                                    i4DataRateIndex,
                                                    &i4NextDataRateIndex) ==
           SNMP_SUCCESS);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UNUSED_PARAM (u1Flag);

    return CLI_SUCCESS;
}

/****************************************************************************
  * Function    :  Dot11NShowRunningConfig
  * Input       :  CliHandle
  * Descritpion :  This Routine shows the configured CLI commands of the
  *                WLAN module, by comparing with the default values.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS or CLI_FAILURE
***************************************************************************/
INT4
Dot11NShowRunningConfig (tCliHandle CliHandle)
{

    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    tRadioIfGetDB       RadioIfGetDB;
    UINT4               u4RadioType = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT1               u1RadioId = 0;
    UINT1               u1Flag = 0;
    INT4                i4LdpcOption = 0;
    INT4                i4ShortGITwentyOption = 0;
    INT4                i4Short40GI = 0;
    UINT4               u4pcoTransTime = 0;
    INT4                i4mcsFeedbackOption = 0;
    INT4                i4htcFieldOption = 0;
    INT4                i4RDRespondOption = 0;
    INT4                i4ImpTranBeamRecOption = 0;
    INT4                i4RecStagSoundOption = 0;
    INT4                i4TranStagSoundOption = 0;
    INT4                i4RecvNDPOption = 0;
    INT4                i4TranNDPOption = 0;
    INT4                i4ImpTranBeamOption = 0;
    INT4                i4CalibrationOption = 0;
    INT4                i4ExplCsiTranBeamOption = 0;
    INT4                i4ExplNoncompSteOption = 0;
    INT4                i4ExplcompSteOption = 0;
    INT4                i4ExplTranBeamCsiOption = 0;
    INT4                i4ExplNCompBeamOption = 0;
    INT4                i4ExplCompBeamOption = 0;
    UINT4               u4numCsiAntSupOption = 0;
    UINT4               u4numNCompAntSupOption = 0;
    UINT4               u4ChannelEstOption = 0;
    UINT4               u4PrimaryChannelOption = 0;
    INT4                i4htNonObssStaOption = 0;
    INT4                i4StbcBeaconOption = 0;
    INT4                i4PcoPhaseOption = 0;
    INT4                i4PcoConfigOption = 0;
    UINT4               u4MinGroupingOption = 0;
    UINT4               u4numCompAntSupOption = 0;
    INT4                i4htOpChannelWidthOption = 0;
    INT4                i4TxRxMcsSetOption = 0;
    INT4                i4ampduStatus = 0;
    UINT4               u4ampduSubFrame = 0;
    UINT4               u4ampduLimit = 0;
    UINT4               u4amsduLimit = 0;
    INT4                i4amsduStatus = 0;
    INT4                i4mcsSetOption = 0;
    INT4                i4MIMOPower = 0;
    INT4                i4HTGreenfield = 0;
    INT4                i4TxSTBC = 0;
    INT4                i4RxSTBC = 0;
    INT4                i4DelayedOption = 0;
    INT4                i4MaxAmsdu = 0;
    INT4                i4HtDSSCCK = 0;
    INT4                i4fortyInto = 0;
    INT4                i4LSIGFullPro = 0;
    UINT4               u4MpduSS = 0;
    UINT4               u4RxAMPDU = 0;
    UINT4               u4AmpduFactor = 0;
    INT4                i4PcoOption = 0;
    UINT4               u4SecondaryChannelOption = 0;
    INT4                i4htDot11RIFSMode = 0;
    INT4                i4htDot11HTProtection = 0;
    INT4                i4htNonGFhtSta = 0;
    INT4                i4htDot11DualCTSProtection = 0;
    INT4                i4DefMIMOPower = 3;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    if (nmhGetFirstIndexFsDot11nConfigTable (&i4NextIfIndex) != SNMP_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return CLI_SUCCESS;
    }
    do
    {
        u1Flag = 0;
        i4IfIndex = i4NextIfIndex;
        if (nmhGetFsDot11RadioType (i4IfIndex, &u4RadioType) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig:"
                       "Failed to get the RadioType for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }

        if (u4RadioType != CLI_RADIO_TYPEBGN && u4RadioType != CLI_RADIO_TYPEAN)
        {
            u1Flag = 1;
            continue;
        }
        CliPrintf (CliHandle, "!\r\n\r\n");
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = i4IfIndex;
        RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB) !=
            OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11ConfigTableShowRunningConfig :"
                       "Failed to get the RadioParams for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;

        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            u1Flag = 1;
            continue;
        }

        if ((nmhGetDot11LDPCCodingOptionActivated (i4IfIndex,
                                                   &i4LdpcOption)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the LDPC Option for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4LdpcOption == RADIO_DOT11N_LDPC_ENABLE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport ldpc enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport ldpc enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigMIMOPowerSave (i4IfIndex,
                                                &i4MIMOPower)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the MIMO Power Save for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4MIMOPower != OSIX_FALSE && i4MIMOPower != i4DefMIMOPower)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport power-save-mode %d %s"
                           " dot11radio %d\r\n", i4MIMOPower,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport power-save-mode %d %s"
                           " dot11radio %d\r\n", i4MIMOPower,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetDot11HTGreenfieldOptionActivated (i4IfIndex,
                                                     &i4HTGreenfield)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the HT Green Field for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4HTGreenfield != 0)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport greenfield enable %s"
                           " dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport greenfield enable %s"
                           " dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetDot11ShortGIOptionInTwentyActivated (i4IfIndex,
                                                        &i4ShortGITwentyOption))
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the  guard-interval 20  for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }

        if (i4ShortGITwentyOption != RADIO_DOT11N_SGI_ENABLE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport guard-interval 20 disable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport guard-interval 20 disable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetDot11ShortGIOptionInFortyActivated (i4IfIndex,
                                                       &i4Short40GI)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the guard-interval 40  for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }

        if (i4Short40GI != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport guard-interval 40 enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport guard-interval 40 enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetDot11TxSTBCOptionActivated (i4IfIndex,
                                               &i4TxSTBC)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Tx STBC for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }

        if (i4TxSTBC != RADIO_DOT11N_TX_STBC_ENABLE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport Tx STBC disable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport Tx STBC disable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetDot11RxSTBCOptionActivated (i4IfIndex,
                                               &i4RxSTBC)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Rx STBC for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4RxSTBC != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport rx-stbc enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport rx-stbc enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigDelayedBlockAckOptionActivated (i4IfIndex,
                                                                 &i4DelayedOption))
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Delayed Block Ack Option for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4DelayedOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport delayed-block-ack enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport delayed-block-ack enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigMaxAMSDULengthConfig (i4IfIndex,
                                                       &i4MaxAmsdu)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Maximum AMSDU Length for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4MaxAmsdu != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport max-amsdu-length enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport max-amsdu-length enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }
        if ((nmhGetFsDot11nConfigtHTDSSCCKModein40MHzConfig (i4IfIndex,
                                                             &i4HtDSSCCK)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the HT DSSCCK Mode in 40MHz for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4HtDSSCCK != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport dss-cck-mode enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport dss-cck-mode enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }
        if ((nmhGetDot11FortyMHzIntolerant (i4IfIndex,
                                            &i4fortyInto)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the 40MHz Intolerant for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4fortyInto != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport forty-mhz-intolerant enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport forty-mhz-intolerant enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }
        if ((nmhGetDot11LSIGTXOPFullProtectionActivated (i4IfIndex,
                                                         &i4LSIGFullPro)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the LSIGTXOP Full Protection for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4LSIGFullPro != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport l-sig-txop-protection enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport l-sig-txop-protection enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }
        if ((nmhGetFsDot11nConfigMinimumMPDUStartSpacingConfig (i4IfIndex,
                                                                &u4MpduSS)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Minimum MPDU Start Spacing for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (u4RxAMPDU != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport ampdu-factor %d %s"
                           "dot11radio %d\r\n", u4MpduSS,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport ampdu-factor %d %s"
                           "dot11radio %d\r\n", u4MpduSS,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }
        if ((nmhGetFsDot11nConfigMaxRxAMPDUFactorConfig (i4IfIndex,
                                                         &u4AmpduFactor)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Maximum Ampdu Factor for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (u4AmpduFactor != RADIO_DOT11N_AMPDU_MAX_LEN)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport max-mpdu %d %s"
                           "dot11radio %d\r\n", u4AmpduFactor,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport max-mpdu %d %s"
                           "dot11radio %d\r\n", u4AmpduFactor,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigPCOOptionActivated (i4IfIndex,
                                                     &i4PcoOption)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the PCO Option for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4PcoOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport pco enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport pco enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigTransitionTimeConfig (i4IfIndex,
                                                       &u4pcoTransTime)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the PCO Transition time for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (u4pcoTransTime != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport pco-transition-time %d %s"
                           "dot11radio %d\r\n", u4pcoTransTime,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport pco-transition-time %d %s"
                           "dot11radio %d\r\n", u4pcoTransTime,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigMCSFeedbackOptionActivated (i4IfIndex,
                                                             &i4mcsFeedbackOption))
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the MCS feadback for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4mcsFeedbackOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport mcs-feedback %d %s"
                           "dot11radio %d\r\n",
                           i4mcsFeedbackOption,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport mcs-feedback %d %s"
                           "dot11radio %d\r\n",
                           i4mcsFeedbackOption,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigHTControlFieldSupported (i4IfIndex,
                                                          &i4htcFieldOption)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the +HTC Support for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4htcFieldOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport ht-contol-field enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport ht-contol-field enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigRDResponderOptionActivated (i4IfIndex,
                                                             &i4RDRespondOption))
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the RD Responder for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4RDRespondOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport reverse-direction-responder enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport reverse-direction-responder enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated
             (i4IfIndex, &i4ImpTranBeamRecOption)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the implicit transmit beamforming recv for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4ImpTranBeamRecOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport implicit-transmit-beamforming-recv enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport implicit-transmit-beamforming-recv enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigReceiveStaggerSoundingOptionActivated
             (i4IfIndex, &i4RecStagSoundOption)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Receiving Staggered Sounding for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4RecStagSoundOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport recv-stagger-sounding enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport recv-stagger-sounding enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigTransmitStaggerSoundingOptionActivated
             (i4IfIndex, &i4TranStagSoundOption)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Receiving Staggered Sounding for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4TranStagSoundOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport transmit-stagger-sounding enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport transmit-stagger-sounding enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigReceiveNDPOptionActivated (i4IfIndex,
                                                            &i4RecvNDPOption))
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Receive Null data Framing for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4RecvNDPOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport recv-null-data-frames enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport recv-null-data-frames enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigTransmitNDPOptionActivated (i4IfIndex,
                                                             &i4TranNDPOption))
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Transmit Null data Framing for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4TranNDPOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport transmit-null-data-frames enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport transmit-null-data-frames enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigImplicitTransmitBeamformingOptionActivated
             (i4IfIndex, &i4ImpTranBeamOption)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Implicit Transmit Beamforming for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4ImpTranBeamOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport implicit-transmit-beamforming enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport implicit-transmit-beamforming enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigCalibrationOptionActivated (i4IfIndex,
                                                             &i4CalibrationOption))
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Calibration Option for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4CalibrationOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport calibration enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport calibration enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated
             (i4IfIndex, &i4ExplCsiTranBeamOption)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Explicit CSI Transmit Beamforming for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4ExplCsiTranBeamOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport explicit-CSI-transmit-beamforming enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport explicit-CSI-transmit-beamforming enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated (i4IfIndex, &i4ExplNoncompSteOption)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Explicit Noncompressed SteeringOption for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4ExplNoncompSteOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport explicit-non-compressed-steering enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport explicit-non-compressed-steering enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated (i4IfIndex, &i4ExplcompSteOption)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Explicit compressed SteeringOption for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4ExplcompSteOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport explicit-compressed-steering enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport explicit-compressed-steering enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated (i4IfIndex, &i4ExplTranBeamCsiOption)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Explicit CSI Transmit Beamforming Feadback for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4ExplTranBeamCsiOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport explicit-transmit-beamforming-csi enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport explicit-transmit-beamforming-csi enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated
             (i4IfIndex, &i4ExplNCompBeamOption)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Explicit Noncompressed Beamforming Feadback Option for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4ExplNCompBeamOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport explicit-non-compressed-beamforming enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport explicit-non-compressed-beamforming enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated (i4IfIndex, &i4ExplCompBeamOption)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Explicit Compressed Beamforming Feadback for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4ExplNCompBeamOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport explicit-compressed-beamforming enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport explicit-compressed-beamforming enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigMinimalGroupingActivated (i4IfIndex,
                                                           &u4MinGroupingOption))
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Minimal Grouping Option for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (u4MinGroupingOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport minimal-grouping enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport minimal-grouping enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigNumberBeamFormingCSISupportAntenna (i4IfIndex,
                                                                     &u4numCsiAntSupOption))
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the CSI Number of Beamformer Antennas Supported for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (u4numCsiAntSupOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport csi-antennas-supported %d %s"
                           "dot11radio %d\r\n", u4numCsiAntSupOption,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport csi-antennas-supported %d %s"
                           "dot11radio %d\r\n", u4numCsiAntSupOption,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna (i4IfIndex, &u4numNCompAntSupOption)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Noncompressed Steering Number of Beamformer Antennas Supported for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (u4numNCompAntSupOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport noncompressed-antennas-supported %d %s"
                           "dot11radio %d\r\n", u4numNCompAntSupOption,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport noncompressed-antennas-supported %d %s"
                           "dot11radio %d\r\n", u4numNCompAntSupOption,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna
             (i4IfIndex, &u4numCompAntSupOption)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Compressed Steering Number of Beamformer Antennas Supported for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (u4numCompAntSupOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport compressed-antennas-supported %d %s"
                           "dot11radio %d\r\n", u4numCompAntSupOption,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport compressed-antennas-supported %d %s"
                           "dot11radio %d\r\n", u4numCompAntSupOption,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }
        if ((nmhGetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated
             (i4IfIndex, &u4numCompAntSupOption)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Maximum CSI Number of Beamformer Antennas Supported related for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (u4numCompAntSupOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport max-csi-antennas-supported %d %s"
                           "dot11radio %d\r\n", u4numCompAntSupOption,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport max-csi-antennas-supported %d %s"
                           "dot11radio %d\r\n", u4numCompAntSupOption,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigChannelEstimationCapabilityActivated
             (i4IfIndex, &u4ChannelEstOption)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the channel estimation value for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (u4ChannelEstOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport channel-estimation-value %d %s"
                           "dot11radio %d\r\n", u4ChannelEstOption,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport channel-estimation-value %d %s"
                           "dot11radio %d\r\n", u4ChannelEstOption,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigCurrentPrimaryChannel (i4IfIndex,
                                                        &u4PrimaryChannelOption))
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Primary Channel Value related for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (u4PrimaryChannelOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport primary-channel-value %d %s"
                           "dot11radio %d\r\n", u4PrimaryChannelOption,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport primary-channel-value %d %s"
                           "dot11radio %d\r\n", u4PrimaryChannelOption,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigCurrentSecondaryChannel (i4IfIndex,
                                                          &u4SecondaryChannelOption))
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Secondary Channel Value related for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (u4SecondaryChannelOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport secondary-channel-value %d %s"
                           "dot11radio %d\r\n", u4SecondaryChannelOption,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport secondary-channel-value %d %s"
                           "dot11radio %d\r\n", u4SecondaryChannelOption,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigSTAChannelWidthConfig (i4IfIndex,
                                                        &i4htOpChannelWidthOption))
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the HT Operation Channel width related for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4htOpChannelWidthOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport ht-operation-channel-width enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport ht-operation-channel-width enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetDot11RIFSMode (i4IfIndex,
                                  &i4htDot11RIFSMode)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the RIFS Mode for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4htDot11RIFSMode != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport rifs-mode enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport rifs-mode enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetDot11RIFSMode (i4IfIndex,
                                  &i4htDot11RIFSMode)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the RIFS Mode for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4htDot11RIFSMode != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport rifs-mode enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport rifs-mode enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetDot11HTProtection (i4IfIndex,
                                      &i4htDot11HTProtection)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the HT Protection for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4htDot11HTProtection != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport ht-protection enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport ht-protection enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigNonGreenfieldHTSTAsPresentConfig (i4IfIndex,
                                                                   &i4htNonGFhtSta))
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Nongreenfield HT STAs present for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4htNonGFhtSta != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport nongreenfield-sta-present enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport nongreenfield-sta-present enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigOBSSNonHTSTAsPresentConfig (i4IfIndex,
                                                             &i4htNonObssStaOption))
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the HT non obss sta for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4htNonObssStaOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport ht-non-obss-sta enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport ht-non-obss-sta enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetDot11DualCTSProtection (i4IfIndex,
                                           &i4htDot11DualCTSProtection)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Dual CTS Protection for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4htDot11DualCTSProtection != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport dual-cts-protection enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport dual-cts-protection enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigSTBCBeaconConfig (i4IfIndex,
                                                   &i4StbcBeaconOption)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the STBC Beacon  for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4StbcBeaconOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport stbc-beacon enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport stbc-beacon enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetDot11PCOActivated (i4IfIndex,
                                      &i4PcoConfigOption)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the PCO active for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4PcoConfigOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport pco-active enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport pco-active enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigPCOPhaseConfig (i4IfIndex,
                                                 &i4PcoPhaseOption)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the PCO Phase for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4PcoPhaseOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport pco-phase enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport pco-phase enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetDot11TxMCSSetDefined (i4IfIndex,
                                         &i4mcsSetOption)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the MCS Set related for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4mcsSetOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle,
                           "dot11 a nSupport mcs-set-defined enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "dot11 b nSupport mcs-set-defined enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nConfigTxRxMCSSetNotEqual (i4IfIndex,
                                                     &i4TxRxMcsSetOption)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the MCS Set related for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4TxRxMcsSetOption != OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport tx-Rx-mcs-set enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport tx-Rx-mcs-set enable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }
        if ((nmhGetFsDot11nAMPDUStatus (i4IfIndex,
                                        &i4ampduStatus)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the AMPDU status related configuration for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4ampduStatus == OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport ampdu disable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport ampdu disable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }

        }

        if ((nmhGetFsDot11nAMPDUSubFrame (i4IfIndex,
                                          &u4ampduSubFrame)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the AMPDU subframe related for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (u4ampduSubFrame != 32)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport ampdu-subframe %d %s"
                           "dot11radio %d\r\n",
                           u4ampduSubFrame,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport ampdu-subframe %d %s"
                           "dot11radio %d\r\n",
                           u4ampduSubFrame,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

        if ((nmhGetFsDot11nAMPDULimit (i4IfIndex,
                                       &u4ampduLimit)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the AMPDU limit related for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (u4ampduLimit != 65535)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport ampdu-limit %d %s "
                           "dot11radio %d\r\n",
                           u4ampduLimit,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport ampdu-limit %d %s "
                           "dot11radio %d\r\n",
                           u4ampduLimit,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }
        if ((nmhGetFsDot11nAMSDUStatus (i4IfIndex,
                                        &i4amsduStatus)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the AMPDU status related configuration for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4amsduStatus == OSIX_FALSE)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport amsdu disable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport amsdu disable %s"
                           "dot11radio %d\r\n",
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }

        }

        if ((nmhGetFsDot11nAMSDULimit (i4IfIndex,
                                       &u4amsduLimit)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11nShowRunningConfig :"
                       "Failed to get the Maximum CSI Number of Beamformer Antennas Supported related for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (u4amsduLimit != 4096)
        {
            if (u4RadioType == CLI_RADIO_TYPEAN)
            {
                CliPrintf (CliHandle, "dot11 a nSupport amsdu-limit %d %s"
                           "dot11radio %d\r\n",
                           u4amsduLimit,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
            else
            {
                CliPrintf (CliHandle, "dot11 b nSupport amsdu-limit %d %s"
                           "dot11radio %d\r\n",
                           u4amsduLimit,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }

    }
    while (nmhGetNextIndexFsDot11nConfigTable (i4IfIndex, &i4NextIfIndex) ==
           SNMP_SUCCESS);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UNUSED_PARAM (u1Flag);
    CliPrintf (CliHandle, "!\r\n\r\n");
    return CLI_SUCCESS;
}

/****************************************************************************
  * Function    :  Dot11AcShowRunningConfig
  * Input       :  CliHandle
  * Descritpion :  This Routine shows the configured CLI commands of the
  *                WLAN module, by comparing with the default values.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS or CLI_FAILURE
***************************************************************************/
INT4
Dot11AcShowRunningConfig (tCliHandle CliHandle)
{

    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    tRadioIfGetDB       RadioIfGetDB;
    UINT4               u4RadioType = 0;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT1               u1RadioId = 0;
    UINT1               u1Flag = 0;
    UINT1               u1Loop = 0;
    INT4                i4MpduLength = 0;
    UINT4               u4AmpduFactor = 0;
    INT4                i4ChannelWidth = 0;
    INT4                i4RxStbc = 0;
    INT4                i4TxStbc = 0;
    UINT4               u4Sts = 0;
    UINT4               u4CenterFreqIndex0 = 0;
    INT4                i4ShortGI80 = 0;
    INT4                i4LdpcOption = 0;
    UINT1               au1RxMcs[FS11AC_SIZE8];
    UINT1               au1TxMcs[FS11AC_SIZE8];
    UINT1               au1RxDefaultMcs[FS11AC_SIZE8];
    UINT1               au1TxDefaultMcs[FS11AC_SIZE8];
    tSNMP_OCTET_STRING_TYPE RxMap;
    tSNMP_OCTET_STRING_TYPE TxMap;
    tSNMP_OCTET_STRING_TYPE RxDefaultMap;
    tSNMP_OCTET_STRING_TYPE TxDefaultMap;
    UINT1               au1VhtCapaMcs
        [DOT11AC_VHT_CAP_MCS_LEN] = RADIO_11AC_MCS_ENABLE;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    MEMSET (au1RxMcs, 0, FS11AC_SIZE8);
    MEMSET (au1TxMcs, 0, FS11AC_SIZE8);
    MEMSET (au1RxDefaultMcs, 0, FS11AC_SIZE8);
    MEMSET (au1TxDefaultMcs, 0, FS11AC_SIZE8);
    MEMSET (&RxMap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TxMap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RxDefaultMap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TxDefaultMap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    RxMap.pu1_OctetList = au1RxMcs;
    TxMap.pu1_OctetList = au1TxMcs;
    RxDefaultMap.pu1_OctetList = au1RxDefaultMcs;
    TxDefaultMap.pu1_OctetList = au1TxDefaultMcs;

    if (nmhGetFirstIndexFsDot11ACConfigTable (&i4NextIfIndex) != SNMP_SUCCESS)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return CLI_SUCCESS;
    }
    do
    {
        u1Flag = 0;
        i4IfIndex = i4NextIfIndex;
        if (nmhGetFsDot11RadioType (i4IfIndex, &u4RadioType) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11AcShowRunningConfig:"
                       "Failed to get the RadioType for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }

        if (u4RadioType != CLI_RADIO_TYPEAC)
        {
            u1Flag = 1;
            continue;
        }
        CliPrintf (CliHandle, "!\r\n\r\n");
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = i4IfIndex;
        RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB) !=
            OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11ConfigTableShowRunningConfig :"
                       "Failed to get the RadioParams for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB)
            != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11ConfigTableShowRunningConfig :"
                       "Failed to get the 11AC RadioParams for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_FALSE;

        u1RadioId = RadioIfGetDB.RadioIfGetAllDB.u1RadioId;

        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            u1Flag = 1;
            continue;
        }

        if ((nmhGetFsDot11ACMaxMPDULengthConfig (i4IfIndex,
                                                 &i4MpduLength)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11AcShowRunningConfig :"
                       "Failed to get the MaxMPDU for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4MpduLength != OSIX_FALSE)
        {
            CliPrintf (CliHandle, "dot11a acsupport max-mpdu %d %s"
                       "dot11radio %d\r\n",
                       i4MpduLength, pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                       u1RadioId);
        }
        if ((nmhGetFsDot11ACVHTMaxRxAMPDUFactorConfig (i4IfIndex,
                                                       &u4AmpduFactor)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11AcShowRunningConfig :"
                       "Failed to get the AMPDUFactor for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (u4AmpduFactor != OSIX_FALSE)
        {
            CliPrintf (CliHandle, "dot11a acsupport ampdu-factor %d %s"
                       " dot11radio %d\r\n",
                       u4AmpduFactor,
                       pWssIfCapwapDB->CapwapGetDB.au1ProfileName, u1RadioId);
        }
        if ((nmhGetFsDot11ACCurrentChannelBandwidthConfig (i4IfIndex,
                                                           &i4ChannelWidth)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11AcShowRunningConfig :"
                       "Failed to get the 11ac ChannelBandwidth"
                       " for the given IfIndex\r\n", i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4ChannelWidth != OSIX_FALSE)
        {
            if (i4ChannelWidth != RADIO_11AC_CHANWIDTH_DEFAULT)
            {
                CliPrintf (CliHandle,
                           "dot11a acsupport chan_width %d %s dot11radio %d\r\n",
                           i4ChannelWidth,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }
        if ((nmhGetFsDot11ACCurrentChannelCenterFrequencyIndex0Config
             (i4IfIndex, &u4CenterFreqIndex0)) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11AcShowRunningConfig :"
                       "Failed to get the ChannelCenterFrequency0"
                       " for the given IfIndex\r\n", i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (u4CenterFreqIndex0 != OSIX_FALSE)
        {
            if (u4CenterFreqIndex0 != RADIO_11AC_CENTER_FCY_DEFAULT)
            {
                CliPrintf (CliHandle, "dot11a acsupport chan_index %d %s"
                           " dot11radio %d\r\n", u4CenterFreqIndex0,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           u1RadioId);
            }
        }
        if ((nmhGetFsDot11ACVHTShortGIOptionIn80Activated (i4IfIndex,
                                                           &i4ShortGI80)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11AcShowRunningConfig :"
                       "Failed to get the ShortGI80 for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4ShortGI80 == RADIO_11AC_SHORTGI80_ENABLE)
        {
            CliPrintf (CliHandle, "dot11a acsupport short guard-interval eighty"
                       " %s %s dot11radio %d\r\n", (i4ShortGI80 == 1) ?
                       "enable" : "disable", pWssIfCapwapDB->CapwapGetDB.
                       au1ProfileName, u1RadioId);
        }
        if ((nmhGetFsDot11ACVHTLDPCCodingOptionActivated (i4IfIndex,
                                                          &i4LdpcOption)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11AcShowRunningConfig :"
                       "Failed to get the LDPC for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4LdpcOption != OSIX_FALSE)
        {
            CliPrintf (CliHandle, "dot11a acsupport ldpc-option %s %s"
                       " dot11radio %d\r\n",
                       (i4LdpcOption == 1) ? "enable" : "disable",
                       pWssIfCapwapDB->CapwapGetDB.au1ProfileName, u1RadioId);
        }
        if ((MEMCMP (au1VhtCapaMcs,
                     (RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                      au1VhtCapaMcs), DOT11AC_VHT_CAP_MCS_LEN)) != 0)
        {
            if ((nmhGetFsDot11ACVHTRxMCSMapConfig (i4IfIndex,
                                                   &RxMap)) == SNMP_SUCCESS)
            {
                if ((nmhGetFsDot11ACVHTRxMCSMap (i4IfIndex, &RxDefaultMap)) ==
                    SNMP_SUCCESS)
                {
                    for (u1Loop = RADIO_VALUE_0; u1Loop < RADIO_VALUE_8;
                         u1Loop++)
                    {
                        if (au1RxMcs[u1Loop] != au1RxDefaultMcs[u1Loop])
                        {
                            if (au1RxMcs[u1Loop] == FS11AC_MCS_RATE7)
                            {
                                CliPrintf (CliHandle, "dot11a acSupport"
                                           " mcs rx rate8 ss %d disable %s dot11radio %d\r\n",
                                           u1Loop + 1,
                                           pWssIfCapwapDB->CapwapGetDB.
                                           au1ProfileName, u1RadioId);
                            }
                            if (au1RxMcs[u1Loop] == FS11AC_MCS_RATE8)
                            {
                                CliPrintf (CliHandle, "dot11a acSupport"
                                           " mcs rx rate9 ss %d disable %s dot11radio %d\r\n",
                                           u1Loop + 1,
                                           pWssIfCapwapDB->CapwapGetDB.
                                           au1ProfileName, u1RadioId);

                                CliPrintf (CliHandle, "dot11a acSupport"
                                           " mcs rx rate8 ss %d enable %s dot11radio %d\r\n",
                                           u1Loop + 1,
                                           pWssIfCapwapDB->CapwapGetDB.
                                           au1ProfileName, u1RadioId);
                            }
                            if (au1RxMcs[u1Loop] == FS11AC_MCS_DISABLED)
                            {
                                CliPrintf (CliHandle, "dot11a acSupport"
                                           " mcs rx rate ss %d disable %s dot11radio %d\r\n",
                                           u1Loop + 1,
                                           pWssIfCapwapDB->CapwapGetDB.
                                           au1ProfileName, u1RadioId);
                            }
                        }
                    }
                }
            }
            else
            {
                CliPrintf (CliHandle, "Dot11AcShowRunningConfig :"
                           "Failed to get the RxMCS for the given IfIndex\r\n",
                           i4IfIndex);
                u1Flag = 1;
                continue;
            }

            if ((nmhGetFsDot11ACVHTTxMCSMapConfig (i4IfIndex,
                                                   &TxMap)) == SNMP_SUCCESS)
            {
                if ((nmhGetFsDot11ACVHTTxMCSMap (i4IfIndex, &TxDefaultMap)) ==
                    SNMP_SUCCESS)
                {
                    for (u1Loop = RADIO_VALUE_0; u1Loop < RADIO_VALUE_8;
                         u1Loop++)
                    {
                        if (au1TxMcs[u1Loop] != au1TxDefaultMcs[u1Loop])
                        {
                            if (au1TxMcs[u1Loop] == FS11AC_MCS_RATE7)
                            {
                                CliPrintf (CliHandle, "dot11a acSupport"
                                           " mcs tx rate8 ss %d disable %s dot11radio %d\r\n",
                                           u1Loop + 1,
                                           pWssIfCapwapDB->CapwapGetDB.
                                           au1ProfileName, u1RadioId);
                            }
                            if (au1TxMcs[u1Loop] == FS11AC_MCS_RATE8)
                            {
                                CliPrintf (CliHandle, "dot11a acSupport"
                                           " mcs tx rate9 ss %d disable %s dot11radio %d\r\n",
                                           u1Loop + 1,
                                           pWssIfCapwapDB->CapwapGetDB.
                                           au1ProfileName, u1RadioId);

                                CliPrintf (CliHandle, "dot11a acSupport"
                                           " mcs tx rate8 ss %d enable %s dot11radio %d\r\n",
                                           u1Loop + 1,
                                           pWssIfCapwapDB->CapwapGetDB.
                                           au1ProfileName, u1RadioId);
                            }
                            if (au1TxMcs[u1Loop] == FS11AC_MCS_DISABLED)
                            {
                                CliPrintf (CliHandle, "dot11a acSupport"
                                           " mcs tx rate ss %d disable %s dot11radio %d\r\n",
                                           u1Loop + 1,
                                           pWssIfCapwapDB->CapwapGetDB.
                                           au1ProfileName, u1RadioId);
                            }

                        }
                    }
                }
            }
            else
            {
                CliPrintf (CliHandle, "Dot11AcShowRunningConfig :"
                           "Failed to get the TxMCS for the given IfIndex\r\n",
                           i4IfIndex);
                u1Flag = 1;
                continue;
            }
        }
        if ((nmhGetFsDot11ACVHTRxSTBCOptionActivated (i4IfIndex,
                                                      &i4RxStbc)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11AcShowRunningConfig :"
                       "Failed to get the Rx-STBC for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4RxStbc != OSIX_FALSE)
        {
            CliPrintf (CliHandle, "dot11a acsupport rx-stbc %s %s"
                       " dot11radio %d\r\n",
                       (i4RxStbc == 1) ? "enable" : "disable",
                       pWssIfCapwapDB->CapwapGetDB.au1ProfileName, u1RadioId);
        }
        if ((nmhGetFsDot11ACVHTTxSTBCOptionActivated (i4IfIndex,
                                                      &i4TxStbc)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11AcShowRunningConfig :"
                       "Failed to get the Tx-STBC for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (i4TxStbc != OSIX_FALSE)
        {
            CliPrintf (CliHandle, "dot11a acsupport tx-stbc %s %s"
                       " dot11radio %d\r\n",
                       (i4TxStbc == 1) ? "enable" : "disable",
                       pWssIfCapwapDB->CapwapGetDB.au1ProfileName, u1RadioId);
        }
        if ((nmhGetFsDot11NumberOfSpatialStreamsActivated (i4IfIndex,
                                                           &u4Sts)) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "Dot11AcShowRunningConfig :"
                       "Failed to get the Number of Space-time streams for the given IfIndex\r\n",
                       i4IfIndex);
            u1Flag = 1;
            continue;
        }
        if (u4Sts != RADIO_VALUE_3)
        {
            CliPrintf (CliHandle, "dot11a acsupport space-time-stream %d %s"
                       " dot11radio %d\r\n",
                       u4Sts, pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                       u1RadioId);
        }

    }
    while (nmhGetNextIndexFsDot11ACConfigTable (i4IfIndex, &i4NextIfIndex) ==
           SNMP_SUCCESS);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    UNUSED_PARAM (u1Flag);
    CliPrintf (CliHandle, "!\r\n\r\n");
    return CLI_SUCCESS;
}

/****************************************************************************
  * Function    :  WlanLegacyRateShowRunningConfig
  * Input       :  CliHandle
  * Descritpion :  This Routine shows the configured CLI commands of the
  *                WLAN module for the Legacy Rate Feature, by comparing 
                   with the default values.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS or CLI_FAILURE
***************************************************************************/
INT4
WlanLegacyRateShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4LegacyRate = 0;
    if (nmhGetFsBandSelectLegacyRate (&i4LegacyRate) != SNMP_FAILURE)
    {
        if (i4LegacyRate == 1)
        {
            CliPrintf (CliHandle, " dot11b legacy-rates enable \r\n");
        }
    }
    return CLI_SUCCESS;
}

#ifdef BAND_SELECT_WANTED
/****************************************************************************
  * Function    :  WlanBandSelectShowRunningConfig
  * Input       :  CliHandle
  * Descritpion :  This Routine shows the configured CLI commands of the
  *                WLAN module for the Band Select Feature, by comparing 
                   with the default values.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS or CLI_FAILURE
***************************************************************************/
INT4
WlanBandSelectShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4BandSelectStatus = 0;
    UINT4               u4NextWlanProfileId = 0;
    UINT4               u4WlanProfileId = 0;
    INT4                i4BandSelectPerWlanStatus = 0;
    UINT4               u4BandSelectAssocCount = 0;
    UINT4               u4BandSelectAgeOutSuppTimer = 0;
    UINT4               u4BandSelectAssocCountFlushTimer = 0;

    if (nmhGetFsBandSelectStatus (&i4BandSelectStatus) != SNMP_FAILURE)
    {
        if (i4BandSelectStatus == 1)
        {
            CliPrintf (CliHandle, "wlan band-select enable \r\n");
        }
    }
    if (nmhGetFirstIndexFsWlanBandSelectTable (&u4NextWlanProfileId) !=
        SNMP_FAILURE)
    {
        while (u4NextWlanProfileId != u4WlanProfileId)
        {
            if (nmhGetFsBandSelectPerWlanStatus
                (u4NextWlanProfileId,
                 &i4BandSelectPerWlanStatus) != SNMP_FAILURE)
            {
                if (i4BandSelectPerWlanStatus == 1)
                {
                    CliPrintf (CliHandle, "wlan band-select enable %d \r\n",
                               u4NextWlanProfileId);
                }
            }
            if (nmhGetFsBandSelectAssocCount
                (u4NextWlanProfileId, &u4BandSelectAssocCount) != SNMP_FAILURE)
            {
                if (u4BandSelectAssocCount != 5)
                {
                    CliPrintf (CliHandle,
                               "wlan association-reject count %d %d\r\n",
                               u4BandSelectAssocCount, u4NextWlanProfileId);
                }
            }
            if (nmhGetFsBandSelectAgeOutSuppTimer
                (u4NextWlanProfileId,
                 &u4BandSelectAgeOutSuppTimer) != SNMP_FAILURE)
            {
                if (u4BandSelectAgeOutSuppTimer != 5)
                {
                    CliPrintf (CliHandle,
                               "wlan probe-req age-out timer %d %d\r\n",
                               u4BandSelectAgeOutSuppTimer,
                               u4NextWlanProfileId);
                }
            }
            if (nmhGetFsBandSelectAssocCountFlushTimer
                (u4NextWlanProfileId,
                 &u4BandSelectAssocCountFlushTimer) != SNMP_FAILURE)
            {
                if (u4BandSelectAssocCountFlushTimer != 5)
                {
                    CliPrintf (CliHandle,
                               "wlan association reset-timer %d %d\r\n",
                               u4BandSelectAssocCountFlushTimer,
                               u4NextWlanProfileId);
                }
            }
            u4WlanProfileId = u4NextWlanProfileId;
            nmhGetNextIndexFsWlanBandSelectTable (u4WlanProfileId,
                                                  &u4NextWlanProfileId);
        }
    }

    return CLI_SUCCESS;
}
#endif

/****************************************************************************
  * Function    :  WlanCapabProfShowRunningConfig
  * Input       :  CliHandle
  * Descritpion :  This Routine shows the configured CLI commands of the
  *                WLAN module, by comparing with the default values.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS or CLI_FAILURE
***************************************************************************/
INT4
WlanCapabProfShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    UINT1               au1ProfileName[256] = { 0 };
    tSNMP_OCTET_STRING_TYPE ProfileName = { NULL, 0 };
    tWssWlanDB         *pWssWlanMsgDB = NULL;
    UINT2               u2ProfileId = 0;
    pWssWlanMsgDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanMsgDB == NULL)
    {
        return CLI_FAILURE;
    }
    MEMSET (pWssWlanMsgDB, 0, sizeof (tWssWlanDB));
    ProfileName.pu1_OctetList = au1ProfileName;
    ProfileName.i4_Length = 256;

    if (nmhGetFirstIndexFsDot11CapabilityMappingTable (&i4NextIfIndex)
        != SNMP_SUCCESS)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
        return CLI_SUCCESS;
    }
    do
    {
        CliPrintf (CliHandle, "!\r\n\r\n");
        i4IfIndex = i4NextIfIndex;

        nmhGetFsDot11CapabilityMappingProfileName (i4IfIndex, &ProfileName);
        pWssWlanMsgDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
        pWssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex = (UINT4) i4IfIndex;
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                      pWssWlanMsgDB) != OSIX_SUCCESS)
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
            return CLI_FAILURE;
        }
        u2ProfileId = pWssWlanMsgDB->WssWlanAttributeDB.u2WlanProfileId;
        CliPrintf (CliHandle, "wlan %d capab-profile %s\r\n",
                   u2ProfileId, au1ProfileName);
    }
    while (nmhGetNextIndexFsDot11CapabilityMappingTable (i4IfIndex,
                                                         &i4NextIfIndex) ==
           SNMP_SUCCESS);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
    return CLI_SUCCESS;
}

/****************************************************************************
  * Function    :  WlanWepKeyAuthShowRunningConfig
  * Input       :  CliHandle
  * Descritpion :  This Routine shows the configured CLI commands of the
  *                WLAN module, by comparing with the default values.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS or CLI_FAILURE
***************************************************************************/
INT4
WlanWepKeyAuthShowRunningConfig (tCliHandle CliHandle)
{
#ifdef WLC_WANTED
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4AlgorithmsIndex = 0;
    INT4                i4NextAlgorithmsIndex = 0;
    INT4                i4Dot11AuthenticationAlgorithm = 0;
    INT4                i4Dot11AlgorithmEnable = 0;
    UINT2               u2WlanProfileId = 0;
    UINT1               u1WebAuthStat = 0;
    tWssWlanDB         *pWssWlanMsgDB = NULL;

    if (nmhGetFirstIndexDot11AuthenticationAlgorithmsTable (&i4NextIfIndex,
                                                            (UINT4 *)
                                                            &i4NextAlgorithmsIndex)
        != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }
    do
    {
        pWssWlanMsgDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
        if (pWssWlanMsgDB == NULL)
        {
            return OSIX_FAILURE;
        }

        MEMSET (pWssWlanMsgDB, 0, sizeof (tWssWlanDB));
        CliPrintf (CliHandle, "!\r\n\r\n");

        i4IfIndex = i4NextIfIndex;
        i4AlgorithmsIndex = i4NextAlgorithmsIndex;

        nmhGetDot11AuthenticationAlgorithm (i4IfIndex, i4AlgorithmsIndex,
                                            &i4Dot11AuthenticationAlgorithm);
        nmhGetDot11AuthenticationAlgorithmsActivated (i4IfIndex,
                                                      i4AlgorithmsIndex,
                                                      &i4Dot11AlgorithmEnable);
        pWssWlanMsgDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
        pWssWlanMsgDB->WssWlanIsPresentDB.bWebAuthStatus = OSIX_TRUE;
        pWssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex = (UINT4) i4IfIndex;
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                      pWssWlanMsgDB) != OSIX_SUCCESS)
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
            return CLI_FAILURE;
        }
        u2WlanProfileId = pWssWlanMsgDB->WssWlanAttributeDB.u2WlanProfileId;
        u1WebAuthStat = pWssWlanMsgDB->WssWlanAttributeDB.u1WebAuthStatus;
        if ((i4Dot11AuthenticationAlgorithm != CLI_AUTH_ALGO_OPEN) &&
            (u1WebAuthStat == WSS_AUTH_STATUS_DISABLE))
        {
            CliPrintf (CliHandle,
                       "wlan security static-wep-key authentication %s "
                       "web-auth %s %d", "shared-key", "disable",
                       u2WlanProfileId);
        }
        else if (u1WebAuthStat != WSS_AUTH_STATUS_DISABLE)
        {
            CliPrintf (CliHandle,
                       "wlan security static-wep-key authentication open "
                       "web-auth %s %d", "enable", u2WlanProfileId);
        }
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
    }
    while (nmhGetNextIndexDot11AuthenticationAlgorithmsTable
           (i4IfIndex, &i4NextIfIndex, i4AlgorithmsIndex,
            (UINT4 *) &i4NextAlgorithmsIndex) == SNMP_SUCCESS);
#else
    UNUSED_PARAM (CliHandle);
#endif
    return CLI_SUCCESS;

}

INT4
WepkeyEncrypShowRunningConfig (tCliHandle CliHandle)
{
    UINT1               au1PrevProfileName[256] = { 0 };
    UINT1               au1ProfileName[256] = { 0 };
    tSNMP_OCTET_STRING_TYPE PrevProfileName = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE ProfileName = { NULL, 0 };
    INT4                i4WepKeyLen = 0;
    INT4                i4AuthAlgo = 0, i4WebAuth = 0;
    INT4                i4WepKeyType = 0, i4KeyIndex = 0;
    UINT1               au1DfltProfileName[] = "default";

    ProfileName.pu1_OctetList = au1ProfileName;
    ProfileName.i4_Length = 256;
    PrevProfileName.pu1_OctetList = au1PrevProfileName;
    PrevProfileName.i4_Length = 256;

    if (nmhGetFirstIndexFsDot11AuthenticationProfileTable (&ProfileName)
        != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        if (MEMCMP (au1ProfileName, au1DfltProfileName,
                    STRLEN (au1DfltProfileName)) != 0)
        {
            CliPrintf (CliHandle, "!\r\n\r\n");

            CliPrintf (CliHandle, "authentication profile %s\r\n",
                       au1ProfileName);
            nmhGetFsDot11WepKeyLength (&ProfileName, &i4WepKeyLen);
            nmhGetFsDot11WepKeyType (&ProfileName, &i4WepKeyType);
            nmhGetFsDot11WepKeyIndex (&ProfileName, &i4KeyIndex);

            nmhGetFsDot11AuthenticationAlgorithm (&ProfileName, &i4AuthAlgo);
            nmhGetFsDot11WebAuthentication (&ProfileName, &i4WebAuth);
            if (i4AuthAlgo != CLI_AUTH_ALGO_OPEN)
            {
                CliPrintf (CliHandle,
                           " set static wep-key-encryption %d %s %d\r\n",
                           i4WepKeyLen, (i4WepKeyType == 2) ? "ascii" : "hex",
                           i4KeyIndex);
            }

            if (i4AuthAlgo == CLI_AUTH_ALGO_OPEN
                && i4WebAuth != WSS_AUTH_STATUS_DISABLE)
            {
                CliPrintf (CliHandle,
                           " set static-wep-key authentication open web-auth "
                           "enable\r\n");
            }
            else if (i4AuthAlgo == CLI_AUTH_ALGO_SHARED)
            {
                CliPrintf (CliHandle,
                           " set static-wep-key authentication shared "
                           "web-auth %s\r\n",
                           (i4WebAuth ==
                            WSS_AUTH_STATUS_ENABLE) ? "enable" : "disable");
            }
            CliPrintf (CliHandle, " exit\r\n\r\n");
        }
        MEMCPY (PrevProfileName.pu1_OctetList, ProfileName.pu1_OctetList,
                ProfileName.i4_Length);
        PrevProfileName.i4_Length = ProfileName.i4_Length;

        /* clear the old values */
        MEMSET (au1ProfileName, 0, sizeof (au1ProfileName));
    }
    while (nmhGetNextIndexFsDot11AuthenticationProfileTable (&PrevProfileName,
                                                             &ProfileName));
    return CLI_SUCCESS;
}

/****************************************************************************
  * Function    :  WssShowRunningConfig 
  * Input       :  CliHandle 
  * Descritpion :  This Routine shows the configured CLI commands of the 
  *                WSS module, by comapring with the default values.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS or CLI_FAILURE
***************************************************************************/
INT4
WssShowRunningConfig (tCliHandle CliHandle)
{
#ifdef CLI_WANTED
    /* first show the capwap commands */
    CapwapShowRunningConfig (CliHandle);

    /* show the wsscfg commands */
    WsscfgShowRunningConfig (CliHandle);
#else
    UNUSED_PARAM (CliHandle);
#endif
    return CLI_SUCCESS;
}

/****************************************************************************
  * Function    :  WebAuthShowRunningConfig 
  * Input       :  CliHandle 
  * Descritpion :  This Routine shows the configured CLI commands of the 
  *                External WebAuth , by comapring with the default values.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS/CLI_FAILURE
***************************************************************************/
INT4
WebAuthShowRunningConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE WebAuthUrl;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4WebAuthMethod = 0;
    INT4                i4WebAuthProfileId = 0;
    UINT1               au1WebAuthUrl[120];
    tWssWlanDB          wssWlanDB;

    MEMSET (&WebAuthUrl, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1WebAuthUrl, 0, 120);
    WebAuthUrl.pu1_OctetList = au1WebAuthUrl;
    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));

    if (nmhGetFirstIndexFsDot11ExternalWebAuthProfileTable (&i4NextIfIndex) !=
        SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        wssWlanDB.WssWlanAttributeDB.u4WlanIfIndex = (UINT4) i4NextIfIndex;
        wssWlanDB.WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &wssWlanDB) !=
            OSIX_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        i4WebAuthProfileId =
            (INT4) wssWlanDB.WssWlanAttributeDB.u2WlanProfileId;

        MEMSET (&au1WebAuthUrl, 0, sizeof (au1WebAuthUrl));
        if (nmhGetFsDot11ExternalWebAuthMethod (i4NextIfIndex, &i4WebAuthMethod)
            != SNMP_FAILURE)
        {
            if ((i4WebAuthMethod == WEB_AUTH_TYPE_INTERNAL) ||
                (i4WebAuthMethod == WEB_AUTH_TYPE_EXTERNAL))
            {
                CliPrintf (CliHandle, "WebAuth Type %s Wlan %d\n",
                           (i4WebAuthMethod == 1) ? "internal" : "external",
                           i4WebAuthProfileId);
                if (i4WebAuthMethod == WEB_AUTH_TYPE_EXTERNAL)
                {
                    if (nmhGetFsDot11ExternalWebAuthUrl
                        (i4NextIfIndex, &WebAuthUrl) != SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle, "WebAuth URL %s Wlan %d\n",
                                   WebAuthUrl.pu1_OctetList,
                                   i4WebAuthProfileId);
                    }
                }
            }
        }
        i4IfIndex = i4NextIfIndex;
    }
    while (nmhGetNextIndexFsDot11ExternalWebAuthProfileTable
           (i4IfIndex, &i4NextIfIndex) == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgShowRunningConfig 
* Description :
* Input       : NONE
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgShowRunningConfig (tCliHandle CliHandle)
{

    if (WlanShowRunningFsWlanStationTrapStatus (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgShowRunningScalarsConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgShowRunningFsQAPProfileConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (WsscfgShowRunningFsDot11RadioConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgWlanShowRunningConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (WlanShowRunningWlanQosProfileConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (WsscfgWlanMacShowRunningConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgApWlanShowRunningConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgApGroupShowRunningConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgCapabilityShowRunningConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WlanShowRunningWtpStationConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WlanShowRunningVlanIsolationConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WepkeyEncrypShowRunningConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (WlanShowRunningQosProfileConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (WlanWepKeyAuthShowRunningConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (WlanAuthMappingShowRunningConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WlanCapabProfShowRunningConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WlanShowRunningQosProfileMappingConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WlanShowRunningSpectrumConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WlanShowRunningAuthInfoConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (Dot11ConfigTableShowRunningConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (Dot11NMCSShowRunningConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (Dot11NShowRunningConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (Dot11AcShowRunningConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (WssBatchConfigShowRunningConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

#ifdef BAND_SELECT_WANTED
    if (WlanBandSelectShowRunningConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (WlanBandSelectShowRunningConfig (CliHandle) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
#endif
    return CLI_SUCCESS;
}

INT4
WsscfgShowRunningScalarsConfig (tCliHandle CliHandle)
{
#ifndef RFMGMT_WANTED
    INT4                i4WtpProfileRowVal = 0;
    INT4                i4WtpProfileOtherRowVal = 0;
#endif
    INT4                i4RetValFsDot11gSupport = 0;
    UINT4               u4RetValFsDot11ManagmentSSID = 0;
    INT4                i4RetValFsSecurityWebAuthType = 0;
    INT4                i4RetValFsSecurityWebAuthColor = 0;
    INT4                i4RetValFsSecurityWebAuthDisplayLang = 0;
    INT4                i4RetValFsDot11bnSupport = 0;
    INT4                i4RetValFsSecurityWebAddr = 0;
    INT4                i4DfltFsSecurityWebAuthType = WEB_AUTH_TYPE_INTERNAL;
    UINT1               au1DefLogo[] = "aricent_logo.jpg";
    UINT1               au1DefUrl[] = "";
    UINT1               au1DefTitle[] = "Web Authentication";
    UINT1               au1DefMes[] = "Hello welcome aboard!";
    UINT1               au1DefSucMes[] = "Authenticated Successfully";
    UINT1               au1DefFailMes[] = "Authentication Failed";
    UINT1               au1DefButText[] = "Submit";
    UINT1               au1DefLoadBal[] = "Sorry!!!Maximum no of users reached";

    tSNMP_OCTET_STRING_TYPE RetValFsSecurityWebAuthWebLogoFileName =
        { NULL, 0 };
    UINT1               au1FsSecurityWebAuthWebLogoFileName[256];
    tSNMP_OCTET_STRING_TYPE RetValFsSecurityWebAuthWebTitle = { NULL, 0 };
    UINT1               au1FsSecurityWebAuthTitleName[256];
    tSNMP_OCTET_STRING_TYPE RetValFsSecurityWebAuthWebMessage = { NULL, 0 };
    UINT1               au1FsSecurityWebAuthWebMessage[256];
    tSNMP_OCTET_STRING_TYPE RetValFsSecurityWebAuthWebSuccMessage = { NULL, 0 };
    UINT1               au1FsSecurityWebAuthWebSuccMessage[256];
    tSNMP_OCTET_STRING_TYPE RetValFsSecurityWebAuthWebFailMessage = { NULL, 0 };
    UINT1               au1FsSecurityWebAuthWebFailMessage[256];
    tSNMP_OCTET_STRING_TYPE RetValFsSecurityWebAuthWebButtonText = { NULL, 0 };
    UINT1               au1FsSecurityWebAuthWebButtonText[256];
    tSNMP_OCTET_STRING_TYPE RetValFsSecurityWebAuthWebLoadBalInfo = { NULL, 0 };
    UINT1               au1FsSecurityWebAuthWebLoadBalInfo[256];
    tSNMP_OCTET_STRING_TYPE RetValFsSecurityWebAuthUrl = { NULL, 0 };
    UINT1               au1FsSecurityWebAuthUrl[256];

    MEMSET (&au1FsSecurityWebAuthWebLogoFileName, 0, 256);
    RetValFsSecurityWebAuthWebLogoFileName.pu1_OctetList =
        au1FsSecurityWebAuthWebLogoFileName;
    RetValFsSecurityWebAuthWebLogoFileName.i4_Length = 256;

    MEMSET (&au1FsSecurityWebAuthTitleName, 0, 256);
    RetValFsSecurityWebAuthWebTitle.pu1_OctetList =
        au1FsSecurityWebAuthTitleName;
    RetValFsSecurityWebAuthWebTitle.i4_Length = 256;

    MEMSET (&au1FsSecurityWebAuthWebMessage, 0, 256);
    RetValFsSecurityWebAuthWebMessage.pu1_OctetList =
        au1FsSecurityWebAuthWebMessage;
    RetValFsSecurityWebAuthWebMessage.i4_Length = 256;

    MEMSET (&au1FsSecurityWebAuthWebSuccMessage, 0, 256);
    RetValFsSecurityWebAuthWebSuccMessage.pu1_OctetList =
        au1FsSecurityWebAuthWebSuccMessage;
    RetValFsSecurityWebAuthWebSuccMessage.i4_Length = 256;

    MEMSET (&au1FsSecurityWebAuthWebFailMessage, 0, 256);
    RetValFsSecurityWebAuthWebFailMessage.pu1_OctetList =
        au1FsSecurityWebAuthWebFailMessage;
    RetValFsSecurityWebAuthWebFailMessage.i4_Length = 256;

    MEMSET (&au1FsSecurityWebAuthWebButtonText, 0, 256);
    RetValFsSecurityWebAuthWebButtonText.pu1_OctetList =
        au1FsSecurityWebAuthWebButtonText;
    RetValFsSecurityWebAuthWebButtonText.i4_Length = 256;

    MEMSET (&au1FsSecurityWebAuthWebLoadBalInfo, 0, 256);
    RetValFsSecurityWebAuthWebLoadBalInfo.pu1_OctetList =
        au1FsSecurityWebAuthWebLoadBalInfo;
    RetValFsSecurityWebAuthWebLoadBalInfo.i4_Length = 256;

    MEMSET (&au1FsSecurityWebAuthUrl, 0, 256);
    RetValFsSecurityWebAuthUrl.pu1_OctetList = au1FsSecurityWebAuthUrl;
    RetValFsSecurityWebAuthUrl.i4_Length = 256;

    CliPrintf (CliHandle, "!\r\n\r\n");

#ifndef RFMGMT_WANTED
    i4WtpProfileRowVal = 0;
    nmhGetFsRrmRowStatus (CLI_RADIO_TYPEA, &i4WtpProfileRowVal);
    if (i4WtpProfileRowVal == ACTIVE)
    {
        i4WtpProfileRowVal = CLI_RRM_DCA_MODE_GLOBAL;
        nmhGetFsRrmDcaMode (CLI_RADIO_TYPEA, &i4WtpProfileRowVal);
        if (i4WtpProfileRowVal == CLI_RRM_DCA_MODE_GLOBAL)
        {
            i4WtpProfileOtherRowVal = CLI_RRM_DCA_CHANNEL_OFF;
            nmhGetFsRrmDcaChannelSelectionMode (CLI_RADIO_TYPEA,
                                                &i4WtpProfileOtherRowVal);
            if (i4WtpProfileOtherRowVal != CLI_RRM_DCA_CHANNEL_OFF)
            {
                CliPrintf (CliHandle, "dot11 a channel global %s \r\n\r\n",
                           (i4WtpProfileOtherRowVal ==
                            CLI_RRM_DCA_CHANNEL_AUTO) ? "auto"
                           : (i4WtpProfileOtherRowVal ==
                              CLI_RRM_DCA_CHANNEL_ONCE) ? "once" : "");
            }
        }
        /* per-ap configurations will be printed in radioconfig table */

        i4WtpProfileRowVal = CLI_RRM_DCA_MODE_GLOBAL;
        nmhGetFsRrmTpcMode (CLI_RADIO_TYPEA, &i4WtpProfileRowVal);
        if (i4WtpProfileRowVal == CLI_RRM_DCA_MODE_GLOBAL)
        {
            i4WtpProfileOtherRowVal = CLI_RRM_DCA_CHANNEL_OFF;
            nmhGetFsRrmTpcSelectionMode (CLI_RADIO_TYPEA,
                                         &i4WtpProfileOtherRowVal);
            if (i4WtpProfileOtherRowVal != CLI_RRM_DCA_CHANNEL_OFF)
            {
                CliPrintf (CliHandle, "dot11 a txPower global %s \r\n\r\n",
                           (i4WtpProfileOtherRowVal ==
                            CLI_RRM_DCA_CHANNEL_AUTO) ? "auto"
                           : (i4WtpProfileOtherRowVal ==
                              CLI_RRM_DCA_CHANNEL_ONCE) ? "once" : "");
            }
        }
        /* per-ap configurations will be printed in radioconfig table */
    }

    i4WtpProfileRowVal = 0;
    nmhGetFsRrmRowStatus (CLI_RADIO_TYPEB, &i4WtpProfileRowVal);
    if (i4WtpProfileRowVal == ACTIVE)
    {
        i4WtpProfileRowVal = CLI_RRM_DCA_MODE_GLOBAL;
        nmhGetFsRrmDcaMode (CLI_RADIO_TYPEB, &i4WtpProfileRowVal);
        if (i4WtpProfileRowVal == CLI_RRM_DCA_MODE_GLOBAL)
        {
            i4WtpProfileOtherRowVal = CLI_RRM_DCA_CHANNEL_OFF;
            nmhGetFsRrmDcaChannelSelectionMode (CLI_RADIO_TYPEB,
                                                &i4WtpProfileOtherRowVal);
            if (i4WtpProfileOtherRowVal != CLI_RRM_DCA_CHANNEL_OFF)
            {
                CliPrintf (CliHandle, "dot11 b channel global %s \r\n\r\n",
                           (i4WtpProfileOtherRowVal ==
                            CLI_RRM_DCA_CHANNEL_AUTO) ? "auto"
                           : (i4WtpProfileOtherRowVal ==
                              CLI_RRM_DCA_CHANNEL_ONCE) ? "once" : "");
            }
        }
        /* per-ap configurations will be printed in radioconfig table */

        i4WtpProfileRowVal = CLI_RRM_DCA_MODE_GLOBAL;
        nmhGetFsRrmTpcMode (CLI_RADIO_TYPEB, &i4WtpProfileRowVal);
        if (i4WtpProfileRowVal == CLI_RRM_DCA_MODE_GLOBAL)
        {
            i4WtpProfileOtherRowVal = CLI_RRM_DCA_CHANNEL_OFF;
            nmhGetFsRrmTpcSelectionMode (CLI_RADIO_TYPEB,
                                         &i4WtpProfileOtherRowVal);
            if (i4WtpProfileOtherRowVal != CLI_RRM_DCA_CHANNEL_OFF)
            {
                CliPrintf (CliHandle, "dot11 b txPower global %s \r\n\r\n",
                           (i4WtpProfileOtherRowVal ==
                            CLI_RRM_DCA_CHANNEL_AUTO) ? "auto"
                           : (i4WtpProfileOtherRowVal ==
                              CLI_RRM_DCA_CHANNEL_ONCE) ? "once" : "");
            }
        }
        /* per-ap configurations will be printed in radioconfig table */
    }
#endif
    if (nmhGetFsDot11gSupport (&i4RetValFsDot11gSupport) != SNMP_FAILURE)
    {
        if (i4RetValFsDot11gSupport != 0)
        {
            CliPrintf (CliHandle, "dot11b gSupport enable \r\n\r\n");
        }
    }

    if (nmhGetFsDot11bnSupport (&i4RetValFsDot11bnSupport) != SNMP_FAILURE)
    {
        if (i4RetValFsDot11bnSupport != 0)
        {
            CliPrintf (CliHandle, "dot11 nSupport enable \r\n\r\n");
        }
    }

    if (nmhGetFsDot11ManagmentSSID (&u4RetValFsDot11ManagmentSSID) !=
        SNMP_FAILURE)
    {
        if (u4RetValFsDot11ManagmentSSID != 0)
        {
            CliPrintf (CliHandle, "set management ssid %d \r\n\r\n",
                       u4RetValFsDot11ManagmentSSID);
        }
    }

    if (nmhGetFsSecurityWebAuthType (&i4RetValFsSecurityWebAuthType) !=
        SNMP_FAILURE)
    {
        if (i4RetValFsSecurityWebAuthType != 0 &&
            i4RetValFsSecurityWebAuthType != i4DfltFsSecurityWebAuthType)
        {
            CliPrintf (CliHandle, "custom-web webauth_type %s \r\n\r\n",
                       (i4RetValFsSecurityWebAuthType ==
                        WEB_AUTH_TYPE_CUSTOMIZE) ? "customize" : "external");
        }
    }

    if (nmhGetFsSecurityWebAuthWebLogoFileName
        (&RetValFsSecurityWebAuthWebLogoFileName) != SNMP_FAILURE)
    {
        if (MEMCMP (RetValFsSecurityWebAuthWebLogoFileName.pu1_OctetList,
                    au1DefLogo, STRLEN (au1DefLogo)) != 0)
        {
            CliPrintf (CliHandle,
                       "custom-web weblogo enable filename %s \r\n\r\n",
                       RetValFsSecurityWebAuthWebLogoFileName.pu1_OctetList);
        }
    }

    if (nmhGetFsSecurityWebAuthWebTitle (&RetValFsSecurityWebAuthWebTitle)
        != SNMP_FAILURE)
    {
        if (MEMCMP (RetValFsSecurityWebAuthWebTitle.pu1_OctetList,
                    au1DefTitle, STRLEN (au1DefTitle)) != 0)
        {
            CliPrintf (CliHandle, "custom-web webtitle %s \r\n\r\n",
                       RetValFsSecurityWebAuthWebTitle.pu1_OctetList);
        }
    }

    if (nmhGetFsSecurityWebAuthWebMessage (&RetValFsSecurityWebAuthWebMessage)
        != SNMP_FAILURE)
    {
        if (MEMCMP (RetValFsSecurityWebAuthWebMessage.pu1_OctetList,
                    au1DefMes, STRLEN (au1DefMes)) != 0)
        {
            CliPrintf (CliHandle, "custom-web webmessage %s \r\n\r\n",
                       RetValFsSecurityWebAuthWebMessage.pu1_OctetList);
        }
    }

    if (nmhGetFsSecurityWebAuthWebSuccMessage
        (&RetValFsSecurityWebAuthWebSuccMessage) != SNMP_FAILURE)
    {
        if (MEMCMP (RetValFsSecurityWebAuthWebSuccMessage.pu1_OctetList,
                    au1DefSucMes, STRLEN (au1DefSucMes)) != 0)
        {
            CliPrintf (CliHandle, "custom-web successmessage %s\r\n\r\n",
                       RetValFsSecurityWebAuthWebSuccMessage.pu1_OctetList);
        }
    }

    if (nmhGetFsSecurityWebAuthWebFailMessage
        (&RetValFsSecurityWebAuthWebFailMessage) != SNMP_FAILURE)
    {
        if (MEMCMP (RetValFsSecurityWebAuthWebFailMessage.pu1_OctetList,
                    au1DefFailMes, STRLEN (au1DefFailMes)) != 0)
        {
            CliPrintf (CliHandle, "custom-web failuremessage %s \r\n\r\n",
                       RetValFsSecurityWebAuthWebFailMessage.pu1_OctetList);
        }
    }

    if (nmhGetFsSecurityWebAuthWebButtonText
        (&RetValFsSecurityWebAuthWebButtonText) != SNMP_FAILURE)
    {
        if (MEMCMP (RetValFsSecurityWebAuthWebButtonText.pu1_OctetList,
                    au1DefButText, STRLEN (au1DefButText)) != 0)
        {
            CliPrintf (CliHandle, "custom-web buttontext %s \r\n\r\n",
                       RetValFsSecurityWebAuthWebButtonText.pu1_OctetList);
        }
    }

    if (nmhGetFsSecurityWebAuthWebLoadBalInfo
        (&RetValFsSecurityWebAuthWebLoadBalInfo) != SNMP_FAILURE)
    {
        if (MEMCMP (RetValFsSecurityWebAuthWebLoadBalInfo.pu1_OctetList,
                    au1DefLoadBal, STRLEN (au1DefLoadBal)) != 0)
        {
            CliPrintf (CliHandle, "custom-web loadbalancemessage %s \r\n\r\n",
                       RetValFsSecurityWebAuthWebLoadBalInfo.pu1_OctetList);
        }
    }

    if (nmhGetFsSecurityWebAuthDisplayLang
        (&i4RetValFsSecurityWebAuthDisplayLang) != SNMP_FAILURE)
    {
        if (i4RetValFsSecurityWebAuthDisplayLang != WEB_AUTH_LANG_ENGLISH)
        {
            CliPrintf (CliHandle, "custom-web language %d \r\n\r\n",
                       i4RetValFsSecurityWebAuthDisplayLang);
        }
    }

    if (nmhGetFsSecurityWebAuthColor (&i4RetValFsSecurityWebAuthColor)
        != SNMP_FAILURE)
    {
        if (i4RetValFsSecurityWebAuthColor != CLI_WEBAUTH_COLOR_ORANGE)
        {
            CliPrintf (CliHandle, "custom-web color %d \r\n\r\n",
                       i4RetValFsSecurityWebAuthColor);
        }
    }

    if (nmhGetFsSecurityWebAuthUrl (&RetValFsSecurityWebAuthUrl)
        != SNMP_FAILURE)
    {
        if (MEMCMP (RetValFsSecurityWebAuthUrl.pu1_OctetList,
                    au1DefUrl, STRLEN (au1DefUrl)) != 0)
        {
            CliPrintf (CliHandle, "custom-web ext-webauth-url %s \r\n\r\n",
                       RetValFsSecurityWebAuthUrl.pu1_OctetList);
        }
    }

    if (nmhGetFsSecurityWebAddr (&i4RetValFsSecurityWebAddr) != SNMP_FAILURE)
    {
        if (i4RetValFsSecurityWebAddr != 0)
        {
            CliPrintf (CliHandle,
                       "custom-web ext-webserver server_IP_address %d.%d.%d.%d \r\n\r\n",
                       ((i4RetValFsSecurityWebAddr) & (0xFF000000)) >> 24,
                       ((i4RetValFsSecurityWebAddr) & (0x00FF0000)) >> 16,
                       ((i4RetValFsSecurityWebAddr) & (0x0000FF00)) >> 8,
                       ((i4RetValFsSecurityWebAddr) & (0x000000FF)));
        }
    }

    CliPrintf (CliHandle, "!\r\n\r\n");
    return CLI_SUCCESS;
}

INT4
WsscfgShowRunningFsDot11RadioConfig (tCliHandle CliHandle)
{
    UINT4               u4WtpProfileId = 0;
    UINT4               u4PrevWtpProfileId = 0;
    UINT4               u4WtpRadioId = 0;
    UINT4               u4PrevWtpRadioId = 0;
    UINT4               u4WtpRadioTypeVal = 0;
    INT4                i4WtpProfileRowVal = 0;
    UINT4               u4WtpProfileRowVal = 0;
    INT4                i4WtpRadioIfIndx = 0;
    UINT1               au1WtpProfStringVal[256] = { 0 };
    UINT1               au1WtpStringVal[256] = { 0 };
    UINT1               au11TaggingPolicy[8] = { 0 };
    tSNMP_OCTET_STRING_TYPE WtpOctetProfString = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE WtpOctetString = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE FsDot11WlanTaggingPolicy = { NULL, 0 };

    WtpOctetProfString.pu1_OctetList = au1WtpProfStringVal;
    WtpOctetString.pu1_OctetList = au1WtpStringVal;
    FsDot11WlanTaggingPolicy.pu1_OctetList = au11TaggingPolicy;
    if (nmhGetFirstIndexFsCapwapWirelessBindingTable (&u4WtpProfileId,
                                                      &u4WtpRadioId) !=
        SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        CliPrintf (CliHandle, "!\r\n\r\n");

        i4WtpProfileRowVal = 0;
        if (nmhGetCapwapBaseWirelessBindingType (u4WtpProfileId, u4WtpRadioId,
                                                 &i4WtpProfileRowVal) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (i4WtpProfileRowVal == CAPWAP_DOT11_BIND_TYPE)
        {
            i4WtpRadioIfIndx = 0;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4WtpRadioId,
                 &i4WtpRadioIfIndx) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;

            }
            if (i4WtpRadioIfIndx != 0)
            {
                /* i4WtpProfileRowVal = 0;
                   nmhGetFsDot11RowStatus(i4WtpRadioIfIndx, &i4WtpProfileRowVal);
                   if (i4WtpProfileRowVal == ACTIVE) 
                 */
                /* not checking the rowstatus, as it seems the table is always 
                 * active if present. and doesnt return any value */
                {
                    u4WtpRadioTypeVal = 0;
                    if (nmhGetFsDot11RadioType
                        (i4WtpRadioIfIndx, &u4WtpRadioTypeVal) != SNMP_SUCCESS)
                    {
                        return CLI_FAILURE;
                    }
                    if (u4WtpRadioTypeVal != 0)
                    {
                        MEMSET (au1WtpProfStringVal, 0,
                                sizeof (au1WtpProfStringVal));
                        WtpOctetProfString.i4_Length = 256;
                        if ((nmhGetCapwapBaseWtpProfileName (u4WtpProfileId,
                                                             &WtpOctetProfString))
                            != SNMP_SUCCESS)
                        {
                            return CLI_FAILURE;
                        }
                        if (WtpOctetProfString.i4_Length != 0)
                        {
                            CliPrintf (CliHandle,
                                       "dot11 %s %s dot11radio %d \r\n\r\n",
                                       (u4WtpRadioTypeVal ==
                                        CLI_RADIO_TYPEA) ? "a"
                                       : (u4WtpRadioTypeVal ==
                                          CLI_RADIO_TYPEB) ? "b"
                                       : (u4WtpRadioTypeVal ==
                                          CLI_RADIO_TYPEG) ? "g" : "",
                                       au1WtpProfStringVal, u4WtpRadioId);
                        }
                    }

                    i4WtpProfileRowVal = CLI_NETWORK_ENABLE;
                    if ((nmhGetIfMainAdminStatus (i4WtpRadioIfIndx,
                                                  &i4WtpProfileRowVal)) !=
                        SNMP_SUCCESS)
                    {
                        return CLI_FAILURE;
                    }
                    if (i4WtpProfileRowVal != CLI_NETWORK_ENABLE)
                    {
                        CliPrintf (CliHandle,
                                   "dot11 %s disable %s dot11radio %d \r\n\r\n",
                                   (u4WtpRadioTypeVal ==
                                    CLI_RADIO_TYPEA) ? "a" : (u4WtpRadioTypeVal
                                                              ==
                                                              CLI_RADIO_TYPEB) ?
                                   "b" : (u4WtpRadioTypeVal ==
                                          CLI_RADIO_TYPEG) ? "g" : "",
                                   au1WtpProfStringVal, u4WtpRadioId);
                    }

                    i4WtpProfileRowVal = WSSIF_RADIOIF_DEF_BEACON_PERIOD;
                    nmhGetDot11BeaconPeriod (i4WtpRadioIfIndx,
                                             (UINT4 *) &i4WtpProfileRowVal);
                    if (i4WtpProfileRowVal != WSSIF_RADIOIF_DEF_BEACON_PERIOD)
                    {
                        CliPrintf (CliHandle,
                                   "dot11 %s beaconperiod %u %s dot11radio %d \r\n\r\n",
                                   (u4WtpRadioTypeVal ==
                                    CLI_RADIO_TYPEA) ? "a" : (u4WtpRadioTypeVal
                                                              ==
                                                              CLI_RADIO_TYPEB) ?
                                   "b" : (u4WtpRadioTypeVal ==
                                          CLI_RADIO_TYPEG) ? "g" : "",
                                   i4WtpProfileRowVal, au1WtpProfStringVal,
                                   u4WtpRadioId);
                    }

                    MEMSET (au1WtpStringVal, 0, sizeof (au1WtpStringVal));
                    WtpOctetString.i4_Length = 256;
                    nmhGetDot11OperationalRateSet (i4WtpRadioIfIndx,
                                                   &WtpOctetString);
                    if (WtpOctetString.i4_Length != 0)
                    {
                        CliPrintf (CliHandle,
                                   "dot11 %s rate supported %d %s dot11radio %d \r\n\r\n",
                                   (u4WtpRadioTypeVal ==
                                    CLI_RADIO_TYPEA) ? "a" : (u4WtpRadioTypeVal
                                                              ==
                                                              CLI_RADIO_TYPEB) ?
                                   "b" : (u4WtpRadioTypeVal ==
                                          CLI_RADIO_TYPEG) ? "g" : "",
                                   *(UINT1 *) (au1WtpStringVal),
                                   au1WtpProfStringVal, u4WtpRadioId);
                    }

                    if (u4WtpRadioTypeVal == CLI_RADIO_TYPEB)
                    {
                        /* only once for a wtp profile. all the radio-if of a
                         * profile has same energy thresh value */
                        if (u4PrevWtpProfileId != u4WtpProfileId)
                        {
                            i4WtpProfileRowVal = 0;
                            nmhGetDot11EDThreshold (i4WtpRadioIfIndx,
                                                    &i4WtpProfileRowVal);
                            if (i4WtpProfileRowVal != 0)
                            {
                                CliPrintf (CliHandle,
                                           "dot11b energy threshold %d ap %s \r\n\r\n",
                                           (-(i4WtpProfileRowVal)),
                                           au1WtpProfStringVal);
                            }

                            i4WtpProfileRowVal = 0;
                            nmhGetDot11CurrentChannel (i4WtpRadioIfIndx,
                                                       (UINT4 *)
                                                       &i4WtpProfileRowVal);
                            if (i4WtpProfileRowVal != 0)
                            {
                                CliPrintf (CliHandle,
                                           "dot11 b channel ap %s %d \r\n\r\n",
                                           au1WtpProfStringVal,
                                           i4WtpProfileRowVal);
                            }
                        }
                    }

                    if (u4WtpRadioTypeVal == CLI_RADIO_TYPEA)
                    {
                        /* only once for a wtp profile. all the radio-if of a
                         * profile has same rssi thresh value */
                        if (u4PrevWtpProfileId != u4WtpProfileId)
                        {
                            i4WtpProfileRowVal = 0;
                            nmhGetDot11TIThreshold (i4WtpRadioIfIndx,
                                                    &i4WtpProfileRowVal);
                            if (i4WtpProfileRowVal != 0)
                            {
                                CliPrintf (CliHandle,
                                           "dot11a rssi threshold %d ap %s \r\n\r\n",
                                           (-(i4WtpProfileRowVal)),
                                           au1WtpProfStringVal);
                            }

                            i4WtpProfileRowVal = 0;
                            if (nmhGetDot11CurrentFrequency
                                (i4WtpRadioIfIndx,
                                 (UINT4 *) &i4WtpProfileRowVal) != SNMP_SUCCESS)
                            {
                                return CLI_FAILURE;
                            }
                            if (i4WtpProfileRowVal != 0)
                            {
                                CliPrintf (CliHandle,
                                           "dot11 a channel ap %s %d \r\n\r\n",
                                           au1WtpProfStringVal,
                                           i4WtpProfileRowVal);
                            }
                        }
                    }

                    i4WtpProfileRowVal = RADIOIF_DEF_RTS_THRESHOLD;
                    nmhGetDot11RTSThreshold (i4WtpRadioIfIndx,
                                             (UINT4 *) &i4WtpProfileRowVal);
                    if (i4WtpProfileRowVal != RADIOIF_DEF_RTS_THRESHOLD)
                    {
                        CliPrintf (CliHandle,
                                   "dot11 %s rts_threshold %d %s dot11radio %d \r\n\r\n",
                                   (u4WtpRadioTypeVal ==
                                    CLI_RADIO_TYPEA) ? "a" : (u4WtpRadioTypeVal
                                                              ==
                                                              CLI_RADIO_TYPEB) ?
                                   "b" : (u4WtpRadioTypeVal ==
                                          CLI_RADIO_TYPEG) ? "g" : "",
                                   i4WtpProfileRowVal, au1WtpProfStringVal,
                                   u4WtpRadioId);
                    }

                    i4WtpProfileRowVal = RADIOIF_DEF_SHORT_RETRY;
                    nmhGetDot11ShortRetryLimit (i4WtpRadioIfIndx,
                                                (UINT4 *) &i4WtpProfileRowVal);
                    if (i4WtpProfileRowVal != RADIOIF_DEF_SHORT_RETRY)
                    {
                        CliPrintf (CliHandle,
                                   "dot11 %s short_retry %d %s dot11radio %d \r\n\r\n",
                                   (u4WtpRadioTypeVal ==
                                    CLI_RADIO_TYPEA) ? "a" : (u4WtpRadioTypeVal
                                                              ==
                                                              CLI_RADIO_TYPEB) ?
                                   "b" : (u4WtpRadioTypeVal ==
                                          CLI_RADIO_TYPEG) ? "g" : "",
                                   i4WtpProfileRowVal, au1WtpProfStringVal,
                                   u4WtpRadioId);
                    }

                    i4WtpProfileRowVal = RADIOIF_DEF_LONG_RETRY;
                    nmhGetDot11LongRetryLimit (i4WtpRadioIfIndx,
                                               (UINT4 *) &i4WtpProfileRowVal);
                    if (i4WtpProfileRowVal != RADIOIF_DEF_LONG_RETRY)
                    {
                        CliPrintf (CliHandle,
                                   "dot11 %s long_retry %d %s dot11radio %d \r\n\r\n",
                                   (u4WtpRadioTypeVal ==
                                    CLI_RADIO_TYPEA) ? "a" : (u4WtpRadioTypeVal
                                                              ==
                                                              CLI_RADIO_TYPEB) ?
                                   "b" : (u4WtpRadioTypeVal ==
                                          CLI_RADIO_TYPEG) ? "g" : "",
                                   i4WtpProfileRowVal, au1WtpProfStringVal,
                                   u4WtpRadioId);
                    }

                    i4WtpProfileRowVal = RADIOIF_DEF_FRAG_THRESHOLD;
                    nmhGetDot11FragmentationThreshold (i4WtpRadioIfIndx,
                                                       (UINT4 *)
                                                       &i4WtpProfileRowVal);
                    if (i4WtpProfileRowVal != RADIOIF_DEF_FRAG_THRESHOLD)
                    {
                        CliPrintf (CliHandle,
                                   "dot11 %s fragmentation_threshold %d %s dot11radio %d \r\n\r\n",
                                   (u4WtpRadioTypeVal ==
                                    CLI_RADIO_TYPEA) ? "a" : (u4WtpRadioTypeVal
                                                              ==
                                                              CLI_RADIO_TYPEB) ?
                                   "b" : (u4WtpRadioTypeVal ==
                                          CLI_RADIO_TYPEG) ? "g" : "",
                                   i4WtpProfileRowVal, au1WtpProfStringVal,
                                   u4WtpRadioId);
                    }

                    u4WtpProfileRowVal = RADIOIF_DEF_RX_LIFETIME;
                    nmhGetDot11MaxReceiveLifetime (i4WtpRadioIfIndx,
                                                   &u4WtpProfileRowVal);
                    if (u4WtpProfileRowVal != RADIOIF_DEF_RX_LIFETIME)
                    {
                        CliPrintf (CliHandle,
                                   "dot11 %s receive_lifetime %u %s dot11radio %d \r\n\r\n",
                                   (u4WtpRadioTypeVal ==
                                    CLI_RADIO_TYPEA) ? "a" : (u4WtpRadioTypeVal
                                                              ==
                                                              CLI_RADIO_TYPEB) ?
                                   "b" : (u4WtpRadioTypeVal ==
                                          CLI_RADIO_TYPEG) ? "g" : "",
                                   u4WtpProfileRowVal, au1WtpProfStringVal,
                                   u4WtpRadioId);
                    }

                    u4WtpProfileRowVal = RADIOIF_DEF_TX_LIFETIME;
                    nmhGetDot11MaxTransmitMSDULifetime (i4WtpRadioIfIndx,
                                                        &u4WtpProfileRowVal);
                    if (u4WtpProfileRowVal != RADIOIF_DEF_TX_LIFETIME)
                    {
                        CliPrintf (CliHandle,
                                   "dot11 %s transmit_msdu_lifetime %u %s dot11radio %d \r\n\r\n",
                                   (u4WtpRadioTypeVal ==
                                    CLI_RADIO_TYPEA) ? "a" : (u4WtpRadioTypeVal
                                                              ==
                                                              CLI_RADIO_TYPEB) ?
                                   "b" : (u4WtpRadioTypeVal ==
                                          CLI_RADIO_TYPEG) ? "g" : "",
                                   u4WtpProfileRowVal, au1WtpProfStringVal,
                                   u4WtpRadioId);
                    }

                    /* only once for a wtp profile. */
                    if (u4PrevWtpProfileId != u4WtpProfileId)
                    {
                        /* all the radio-if of a
                         * profile has same txpowerlevel value */
                        i4WtpProfileRowVal = 0;
                        nmhGetDot11CurrentTxPowerLevel (i4WtpRadioIfIndx,
                                                        (UINT4 *)
                                                        &i4WtpProfileRowVal);
                        if (i4WtpProfileRowVal != 0)
                        {
                            CliPrintf (CliHandle,
                                       "dot11 %s txPower ap %s %d \r\n\r\n",
                                       (u4WtpRadioTypeVal ==
                                        CLI_RADIO_TYPEA) ? "a"
                                       : (u4WtpRadioTypeVal ==
                                          CLI_RADIO_TYPEB) ? "b"
                                       : (u4WtpRadioTypeVal ==
                                          CLI_RADIO_TYPEG) ? "g" : "",
                                       au1WtpProfStringVal, i4WtpProfileRowVal);
                        }
                    }

                    if (WsscfgShowRunningDot11AntennasConfig (CliHandle,
                                                              u4WtpRadioTypeVal,
                                                              au1WtpProfStringVal,
                                                              u4WtpRadioId,
                                                              i4WtpRadioIfIndx)
                        != CLI_SUCCESS)
                    {
                        return CLI_FAILURE;
                    }

                    if (WsscfgShowRunningFsDot11EDCSConfig (CliHandle,
                                                            u4WtpRadioTypeVal,
                                                            au1WtpProfStringVal,
                                                            u4WtpRadioId,
                                                            i4WtpRadioIfIndx) !=
                        CLI_SUCCESS)
                    {
                        return CLI_FAILURE;
                    }
                    if (nmhGetFsDot11TaggingPolicy
                        (i4WtpRadioIfIndx,
                         &FsDot11WlanTaggingPolicy) == SNMP_SUCCESS)
                    {

                        if (FsDot11WlanTaggingPolicy.pu1_OctetList[0] != 0)
                        {
                            /*IF not default condition */
                            switch (FsDot11WlanTaggingPolicy.pu1_OctetList[0])
                            {
                                    /*DSCP = 96 */
                                case 96:
                                    CliPrintf (CliHandle,
                                               "dot11 b qos tagging-policy dscp %s dot11radio %d \r\n\r\n",
                                               au1WtpProfStringVal,
                                               u4WtpRadioId);
                                    break;
                                    /*pbit = 16 */
                                case 16:
                                    CliPrintf (CliHandle,
                                               "dot11 b qos tagging-policy pbit %s dot11radio %d \r\n\r\n",
                                               au1WtpProfStringVal,
                                               u4WtpRadioId);
                                    break;
                                default:
                                    CliPrintf (CliHandle,
                                               "invalid tagging policy \r\n\r\n");
                            }
                        }
                    }

                }                /* i4WtpProfileRowVal == ACTIVE */
            }
        }

        u4PrevWtpProfileId = u4WtpProfileId;
        u4PrevWtpRadioId = u4WtpRadioId;
        /* clear the old values */
        u4WtpProfileId = 0;
        u4WtpRadioId = 0;
    }
    while (nmhGetNextIndexFsCapwapWirelessBindingTable (u4PrevWtpProfileId,
                                                        &u4WtpProfileId,
                                                        u4PrevWtpRadioId,
                                                        &u4WtpRadioId) ==
           SNMP_SUCCESS);

    CliPrintf (CliHandle, "!\r\n\r\n");
    return CLI_SUCCESS;
}

INT4
WsscfgShowRunningFsDot11EDCSConfig (tCliHandle CliHandle,
                                    INT4 u4WtpRadioTypeVal,
                                    UINT1 au1WtpProfStringVal[],
                                    UINT4 u4WtpRadioId, INT4 i4IfIndex)
{
    INT4                i4RadioIfIndex = 0;
    INT4                i4PrevRadioIfIndex = 0;
    INT4                i4Dot11EDCATableIndex = 0;
    INT4                i4PrevDot11EDCATableIndex = 0;
    INT4                i4WtpProfileRowVal = 0;

    if (nmhGetFirstIndexDot11EDCATable (&i4RadioIfIndex,
                                        (UINT4 *) &i4Dot11EDCATableIndex) !=
        SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        if (i4RadioIfIndex == i4IfIndex)
        {
            i4WtpProfileRowVal =
                WSSIF_RADIOIF_DEF_CWMIN_FOR_INDEX (i4Dot11EDCATableIndex);
            nmhGetDot11EDCATableCWmin (i4IfIndex, i4Dot11EDCATableIndex,
                                       (UINT4 *) &i4WtpProfileRowVal);
            if (i4WtpProfileRowVal !=
                WSSIF_RADIOIF_DEF_CWMIN_FOR_INDEX (i4Dot11EDCATableIndex))
            {
                CliPrintf (CliHandle,
                           "dot11 %s edca cw-min %d index %d %s dot11radio %d \r\n\r\n",
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEA) ? "a" :
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEB) ? "b" :
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEG) ? "g" : "",
                           i4WtpProfileRowVal, i4Dot11EDCATableIndex,
                           au1WtpProfStringVal, u4WtpRadioId);
            }

            i4WtpProfileRowVal =
                WSSIF_RADIOIF_DEF_CWMAX_FOR_INDEX (i4Dot11EDCATableIndex);
            nmhGetDot11EDCATableCWmax (i4IfIndex, i4Dot11EDCATableIndex,
                                       (UINT4 *) &i4WtpProfileRowVal);
            if (i4WtpProfileRowVal !=
                WSSIF_RADIOIF_DEF_CWMAX_FOR_INDEX (i4Dot11EDCATableIndex))
            {
                CliPrintf (CliHandle,
                           "dot11 %s edca cw-max %d index %d %s dot11radio %d \r\n\r\n",
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEA) ? "a" :
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEB) ? "b" :
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEG) ? "g" : "",
                           i4WtpProfileRowVal, i4Dot11EDCATableIndex,
                           au1WtpProfStringVal, u4WtpRadioId);
            }

            i4WtpProfileRowVal =
                WSSIF_RADIOIF_DEF_AIFSN_FOR_INDEX (i4Dot11EDCATableIndex);
            nmhGetDot11EDCATableAIFSN (i4IfIndex, i4Dot11EDCATableIndex,
                                       (UINT4 *) &i4WtpProfileRowVal);
            if (i4WtpProfileRowVal !=
                WSSIF_RADIOIF_DEF_AIFSN_FOR_INDEX (i4Dot11EDCATableIndex))
            {
                CliPrintf (CliHandle,
                           "dot11 %s edca aifs %d index %d %s dot11radio %d \r\n\r\n",
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEA) ? "a" :
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEB) ? "b" :
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEG) ? "g" : "",
                           i4WtpProfileRowVal, i4Dot11EDCATableIndex,
                           au1WtpProfStringVal, u4WtpRadioId);
            }

            i4WtpProfileRowVal =
                WSSIF_RADIOIF_DEF_TXOP_FOR_INDEX (i4Dot11EDCATableIndex);
            nmhGetDot11EDCATableTXOPLimit (i4IfIndex, i4Dot11EDCATableIndex,
                                           (UINT4 *) &i4WtpProfileRowVal);
            if (i4WtpProfileRowVal !=
                WSSIF_RADIOIF_DEF_TXOP_FOR_INDEX (i4Dot11EDCATableIndex))
            {
                CliPrintf (CliHandle,
                           "dot11 %s edca txop-limit %d index %d %s dot11radio %d \r\n\r\n",
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEA) ? "a" :
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEB) ? "b" :
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEG) ? "g" : "",
                           i4WtpProfileRowVal, i4Dot11EDCATableIndex,
                           au1WtpProfStringVal, u4WtpRadioId);
            }

            i4WtpProfileRowVal =
                WSSIF_RADIOIF_DEF_QOS_PRIO_FOR_INDEX (i4Dot11EDCATableIndex);
            nmhGetFsDot11PriorityValue (i4IfIndex, i4Dot11EDCATableIndex,
                                        &i4WtpProfileRowVal);
            if (i4WtpProfileRowVal !=
                WSSIF_RADIOIF_DEF_QOS_PRIO_FOR_INDEX (i4Dot11EDCATableIndex))
            {
                CliPrintf (CliHandle,
                           "dot11 %s qos priority %d index %d %s dot11radio %d \r\n\r\n",
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEA) ? "a" :
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEB) ? "b" :
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEG) ? "g" : "",
                           i4WtpProfileRowVal, i4Dot11EDCATableIndex,
                           au1WtpProfStringVal, u4WtpRadioId);
            }

            i4WtpProfileRowVal =
                WSSIF_RADIOIF_DEF_QOS_DSCP_FOR_INDEX (i4Dot11EDCATableIndex);
            nmhGetFsDot11DscpValue (i4IfIndex, i4Dot11EDCATableIndex,
                                    &i4WtpProfileRowVal);
            if (i4WtpProfileRowVal !=
                WSSIF_RADIOIF_DEF_QOS_DSCP_FOR_INDEX (i4Dot11EDCATableIndex))
            {
                CliPrintf (CliHandle,
                           "dot11 %s qos dscp %d index %d %s dot11radio %d \r\n\r\n",
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEA) ? "a" :
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEB) ? "b" :
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEG) ? "g" : "",
                           i4WtpProfileRowVal, i4Dot11EDCATableIndex,
                           au1WtpProfStringVal, u4WtpRadioId);
            }

            i4WtpProfileRowVal =
                WSSIF_RADIOIF_DEF_ADMISSION_FOR_INDEX (i4Dot11EDCATableIndex);
            nmhGetDot11EDCATableMandatory (i4IfIndex, i4Dot11EDCATableIndex,
                                           &i4WtpProfileRowVal);
            if (i4WtpProfileRowVal !=
                WSSIF_RADIOIF_DEF_ADMISSION_FOR_INDEX (i4Dot11EDCATableIndex))
            {
                CliPrintf (CliHandle,
                           "dot11 %s edca admission-control %s index %d %s dot11radio %d \r\n\r\n",
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEA) ? "a" :
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEB) ? "b" :
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEG) ? "g" : "",
                           (i4WtpProfileRowVal ==
                            CLI_ADMISSION_TRUE) ? "true" : "false",
                           i4Dot11EDCATableIndex, au1WtpProfStringVal,
                           u4WtpRadioId);
            }
        }

        /* backup & reset the values */
        i4PrevRadioIfIndex = i4RadioIfIndex;
        i4PrevDot11EDCATableIndex = i4Dot11EDCATableIndex;

        i4RadioIfIndex = 0;
        i4Dot11EDCATableIndex = 0;
    }
    while (nmhGetNextIndexDot11EDCATable (i4PrevRadioIfIndex,
                                          &i4RadioIfIndex,
                                          i4PrevDot11EDCATableIndex,
                                          (UINT4 *) &i4Dot11EDCATableIndex) ==
           SNMP_SUCCESS);

    return CLI_SUCCESS;
}

INT4
WsscfgShowRunningFsQAPProfileConfig (tCliHandle CliHandle)
{
    INT4                i4FsQAPProfileIndex = 0;
    INT4                i4PrevFsQAPProfileIndex = 0;
    INT4                i4WtpProfileRowVal = 0;
    INT4                i4DefaultProfFlag = FALSE;
    UINT1               au1DfltWtpProfStringVal[] = "default";
    UINT1               au1WtpProfStringVal[256] = { 0 };
    UINT1               au1PrevWtpProfStringVal[256] = { 0 };
    tSNMP_OCTET_STRING_TYPE WtpProfileName = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE PrevWtpProfileName = { NULL, 0 };

    WtpProfileName.pu1_OctetList = au1WtpProfStringVal;
    PrevWtpProfileName.pu1_OctetList = au1PrevWtpProfStringVal;

    CliPrintf (CliHandle, "!\r\n\r\n");

    if (nmhGetFirstIndexFsQAPProfileTable (&WtpProfileName,
                                           &i4FsQAPProfileIndex) !=
        SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        if (MEMCMP
            (au1DfltWtpProfStringVal, au1WtpProfStringVal,
             WtpProfileName.i4_Length) == 0)
        {
            i4DefaultProfFlag = TRUE;
        }

        i4WtpProfileRowVal =
            WSSIF_RADIOIF_DEF_CWMIN_FOR_INDEX (i4FsQAPProfileIndex);
        nmhGetFsQAPProfileCWmin (&WtpProfileName, i4FsQAPProfileIndex,
                                 &i4WtpProfileRowVal);
        if ((i4DefaultProfFlag == FALSE) || (i4WtpProfileRowVal != 7
                                             /*WSSIF_RADIOIF_DEF_CWMIN_FOR_INDEX(i4FsQAPProfileIndex) */
            ))
        {
            CliPrintf (CliHandle,
                       "set dot11 edca cw-min %d index %d %s \r\n\r\n",
                       i4WtpProfileRowVal, i4FsQAPProfileIndex,
                       au1WtpProfStringVal);
        }

        i4WtpProfileRowVal =
            WSSIF_RADIOIF_DEF_CWMAX_FOR_INDEX (i4FsQAPProfileIndex);
        nmhGetFsQAPProfileCWmax (&WtpProfileName, i4FsQAPProfileIndex,
                                 &i4WtpProfileRowVal);
        if ((i4DefaultProfFlag == FALSE) || (i4WtpProfileRowVal != 15
                                             /*WSSIF_RADIOIF_DEF_CWMAX_FOR_INDEX(i4FsQAPProfileIndex) */
            ))
        {
            CliPrintf (CliHandle,
                       "set dot11 edca cw-max %d index %d %s \r\n\r\n",
                       i4WtpProfileRowVal, i4FsQAPProfileIndex,
                       au1WtpProfStringVal);
        }

        i4WtpProfileRowVal =
            WSSIF_RADIOIF_DEF_AIFSN_FOR_INDEX (i4FsQAPProfileIndex);
        nmhGetFsQAPProfileAIFSN (&WtpProfileName, i4FsQAPProfileIndex,
                                 &i4WtpProfileRowVal);
        if ((i4DefaultProfFlag == FALSE) || (i4WtpProfileRowVal != 3
                                             /*WSSIF_RADIOIF_DEF_AIFSN_FOR_INDEX(i4FsQAPProfileIndex) */
            ))
        {
            CliPrintf (CliHandle,
                       "set dot11 edca aifs %d index %d %s \r\n\r\n",
                       i4WtpProfileRowVal, i4FsQAPProfileIndex,
                       au1WtpProfStringVal);
        }

        i4WtpProfileRowVal =
            WSSIF_RADIOIF_DEF_TXOP_FOR_INDEX (i4FsQAPProfileIndex);
        nmhGetFsQAPProfileTXOPLimit (&WtpProfileName, i4FsQAPProfileIndex,
                                     &i4WtpProfileRowVal);
        if ((i4DefaultProfFlag == FALSE) || (i4WtpProfileRowVal != 3
                                             /*WSSIF_RADIOIF_DEF_TXOP_FOR_INDEX(i4FsQAPProfileIndex) */
            ))
        {
            CliPrintf (CliHandle,
                       "set dot11 edca txop-limit %d index %d %s \r\n\r\n",
                       i4WtpProfileRowVal, i4FsQAPProfileIndex,
                       au1WtpProfStringVal);
        }

        i4WtpProfileRowVal =
            WSSIF_RADIOIF_DEF_QOS_PRIO_FOR_INDEX (i4FsQAPProfileIndex);
        nmhGetFsQAPProfilePriorityValue (&WtpProfileName, i4FsQAPProfileIndex,
                                         &i4WtpProfileRowVal);
        if ((i4DefaultProfFlag == FALSE) || (i4WtpProfileRowVal != 3
                                             /*WSSIF_RADIOIF_DEF_QOS_PRIO_FOR_INDEX(i4FsQAPProfileIndex) */
            ))
        {
            CliPrintf (CliHandle,
                       "set dot11 qos priority %d index %d %s \r\n\r\n",
                       i4WtpProfileRowVal, i4FsQAPProfileIndex,
                       au1WtpProfStringVal);
        }

        i4WtpProfileRowVal =
            WSSIF_RADIOIF_DEF_QOS_DSCP_FOR_INDEX (i4FsQAPProfileIndex);
        nmhGetFsQAPProfileDscpValue (&WtpProfileName, i4FsQAPProfileIndex,
                                     &i4WtpProfileRowVal);
        if ((i4DefaultProfFlag == FALSE) || (i4WtpProfileRowVal != 3
                                             /*WSSIF_RADIOIF_DEF_QOS_DSCP_FOR_INDEX(i4FsQAPProfileIndex) */
            ))
        {
            CliPrintf (CliHandle,
                       "set dot11 qos dscp %d index %d %s \r\n\r\n",
                       i4WtpProfileRowVal, i4FsQAPProfileIndex,
                       au1WtpProfStringVal);
        }

        /* backup & reset the values */
        MEMCPY (PrevWtpProfileName.pu1_OctetList, WtpProfileName.pu1_OctetList,
                WtpProfileName.i4_Length);
        PrevWtpProfileName.i4_Length = WtpProfileName.i4_Length;
        i4PrevFsQAPProfileIndex = i4FsQAPProfileIndex;

        MEMSET (au1WtpProfStringVal, 0, sizeof (au1WtpProfStringVal));
        WtpProfileName.i4_Length = 256;
        i4FsQAPProfileIndex = 0;
        i4DefaultProfFlag = FALSE;
    }
    while (nmhGetNextIndexFsQAPProfileTable (&PrevWtpProfileName,
                                             &WtpProfileName,
                                             i4PrevFsQAPProfileIndex,
                                             &i4FsQAPProfileIndex) ==
           SNMP_SUCCESS);

    CliPrintf (CliHandle, "!\r\n\r\n");

    return CLI_SUCCESS;
}

INT4
WsscfgShowRunningDot11AntennasConfig (tCliHandle CliHandle,
                                      INT4 u4WtpRadioTypeVal,
                                      UINT1 au1WtpProfStringVal[],
                                      UINT4 u4WtpRadioId, INT4 i4IfIndex)
{
    INT4                i4RadioIfIndex = 0;
    INT4                i4PrevRadioIfIndex = 0;
    INT4                i4Dot11AntennaListIndex = 0;
    INT4                i4PrevDot11AntennaListIndex = 0;
    INT4                i4WtpProfileRowVal = 0;
    INT4                i4LoopIter = 0;
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if (nmhGetFirstIndexDot11AntennasListTable (&i4RadioIfIndex,
                                                (UINT4 *)
                                                &i4Dot11AntennaListIndex) !=
        SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        if (i4RadioIfIndex == i4IfIndex)
        {
            i4WtpProfileRowVal = RADIO_ANTENNA_DIVERSITY_DISABLED;
            nmhGetDot11DiversitySelectionRxImplemented (i4IfIndex,
                                                        i4Dot11AntennaListIndex,
                                                        &i4WtpProfileRowVal);
            if (i4WtpProfileRowVal != RADIO_ANTENNA_DIVERSITY_DISABLED)
            {
                CliPrintf (CliHandle,
                           "dot11 %s antenna_diversity enable %s dot11radio %d \r\n\r\n",
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEA) ? "a" :
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEB) ? "b" :
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEG) ? "g" : "",
                           au1WtpProfStringVal, u4WtpRadioId);
            }

            i4WtpProfileRowVal = RADIOIF_DEF_ANT_MODE;
            nmhGetFsAntennaMode (i4IfIndex, i4Dot11AntennaListIndex,
                                 &i4WtpProfileRowVal);
            if (i4WtpProfileRowVal != RADIOIF_DEF_ANT_MODE)
            {
                CliPrintf (CliHandle,
                           "dot11 %s antenna_mode %s %s dot11radio %d \r\n\r\n",
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEA) ? "a" :
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEB) ? "b" :
                           (u4WtpRadioTypeVal == CLI_RADIO_TYPEG) ? "g" : "",
                           (i4WtpProfileRowVal ==
                            CLI_ANTENNA_MODE_LEFTA) ? "sectorA"
                           : (i4WtpProfileRowVal ==
                              CLI_ANTENNA_MODE_RIGHTA) ? "sectorB"
                           : (i4WtpProfileRowVal ==
                              CLI_ANTENNA_MODE_MIMO) ? "mimo" : "",
                           au1WtpProfStringVal, u4WtpRadioId);
            }

            /* run thro' all the antenna's, expensive but simple */
            RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
                UtlShMemAllocAntennaSelectionBuf ();

            if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
            {
                return OSIX_FAILURE;
            }
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
            RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxAntenna = OSIX_TRUE;
            /* Get the Radio IF DB info to fill the structure  */
            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB) !=
                OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "WsscfgShowRunningDot11AntennasConfig: RadioIf DB access failed. \r\n");
                UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                                 pu1AntennaSelection);
                return OSIX_FAILURE;
            }
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);

            for (i4LoopIter = 0;
                 i4LoopIter < RadioIfGetDB.RadioIfGetAllDB.u1CurrentTxAntenna;
                 i4LoopIter++)
            {
                i4WtpProfileRowVal = RADIO_ANTENNA_INTERNAL;
                nmhGetFsAntennaSelection (i4IfIndex, (i4LoopIter + 1),
                                          &i4WtpProfileRowVal);
                if (i4WtpProfileRowVal != RADIO_ANTENNA_INTERNAL)
                {
                    CliPrintf (CliHandle,
                               "dot11 %s antenna_selection %s %s dot11radio %d antenna_index %d \r\n\r\n",
                               (u4WtpRadioTypeVal == CLI_RADIO_TYPEA) ? "a" :
                               (u4WtpRadioTypeVal ==
                                CLI_RADIO_TYPEG) ? "g" : "",
                               (i4WtpProfileRowVal ==
                                CLI_ANTENNA_EXTERNAL) ? "external" : "",
                               au1WtpProfStringVal, u4WtpRadioId,
                               (i4LoopIter + 1));
                }
            }
        }
        /* backup & reset the values */
        i4PrevRadioIfIndex = i4RadioIfIndex;
        i4PrevDot11AntennaListIndex = i4Dot11AntennaListIndex;

        i4RadioIfIndex = 0;
        i4Dot11AntennaListIndex = 0;
    }
    while (nmhGetNextIndexDot11AntennasListTable (i4PrevRadioIfIndex,
                                                  &i4RadioIfIndex,
                                                  i4PrevDot11AntennaListIndex,
                                                  (UINT4 *)
                                                  &i4Dot11AntennaListIndex) ==
           SNMP_SUCCESS);

    return CLI_SUCCESS;
}

INT4
WsscfgWlanShowRunningConfig (tCliHandle CliHandle)
{
#ifdef WLC_WANTED
    UINT4               u4NextProfileId = 0, u4WlanProfileId = 0;
    INT4                i4WlanIfIndex = 0, i4OutCome = 0, i4AdminStatus = 0;
    INT4                i4RowStatus = 0;
    UINT1               au1ManagmentSSID[WSSWLAN_SSID_NAME_LEN];
    UINT2               u2WlanProfileId = 0;
    INT4                i4WlanMulticastMode = 0;
    UINT4               u4WlanMulticastTableLength = 0;
    UINT4               u4WlanSnoopTimer = 0;
    UINT4               u4WlanSnoopTimeout = 0;

    i4OutCome = nmhGetFirstIndexCapwapDot11WlanTable (&u4NextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        CONFIG_UNLOCK ();
        return CLI_SUCCESS;
    }
    do
    {
        CliPrintf (CliHandle, "!\r\n\r\n");

        u4WlanProfileId = u4NextProfileId;
        if (nmhGetCapwapDot11WlanProfileIfIndex (u4WlanProfileId,
                                                 &i4WlanIfIndex) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhGetCapwapDot11WlanRowStatus (u4WlanProfileId,
                                            &i4RowStatus) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhGetIfMainAdminStatus (i4WlanIfIndex, &i4AdminStatus)
            != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        WssWlanGetSSIDfromIfIndex (((UINT4) i4WlanIfIndex), au1ManagmentSSID);
        WssWlanGetWlanIdfromIfIndex (((UINT4) i4WlanIfIndex), &u2WlanProfileId);
        if ((i4RowStatus == CREATE_AND_WAIT) || (i4RowStatus == CREATE_AND_GO)
            || (i4RowStatus == NOT_READY))
        {
            CliPrintf (CliHandle, "wlan create %d %s\r\n", u2WlanProfileId,
                       au1ManagmentSSID);
        }
        if (i4AdminStatus == CLI_WLAN_ENABLE)
        {
            CliPrintf (CliHandle, "wlan enable %d %s\r\n", u2WlanProfileId,
                       au1ManagmentSSID);
        }
        if (nmhGetFsWlanMulticastMode (u2WlanProfileId, &i4WlanMulticastMode)
            != SNMP_FAILURE)
        {
            if (i4WlanMulticastMode != CLI_WLAN_MULTICAST_MODE_DISABLE)
            {
                CliPrintf (CliHandle, "wlan multicast mode %d %d\r\n",
                           i4WlanMulticastMode, u2WlanProfileId);
            }
        }
        if (nmhGetFsWlanMulticastSnoopTableLength (u2WlanProfileId,
                                                   &u4WlanMulticastTableLength)
            != SNMP_FAILURE)
        {
            if (u4WlanMulticastTableLength != DEFAULT_MULTICAST_TABLE_LENGTH)
            {
                CliPrintf (CliHandle, "wlan multicast length %d %d\r\n",
                           u4WlanMulticastTableLength, u2WlanProfileId);
            }
        }
        if (nmhGetFsWlanMulticastSnoopTimer (u2WlanProfileId,
                                             &u4WlanSnoopTimer) != SNMP_FAILURE)
        {
            if (u4WlanSnoopTimer != DEFAULT_MULTICAST_SNOOP_TIMER)
            {
                CliPrintf (CliHandle, "wlan multicast timer %d %d\r\n",
                           u4WlanSnoopTimer, u2WlanProfileId);
            }
        }
        if (nmhGetFsWlanMulticastSnoopTimeout (u2WlanProfileId,
                                               &u4WlanSnoopTimeout) !=
            SNMP_FAILURE)
        {
            if (u4WlanSnoopTimeout != DEFAULT_MULTICAST_SNOOP_TIMEOUT)
            {
                CliPrintf (CliHandle, "wlan multicast timeout %d %d\r\n",
                           u4WlanSnoopTimeout, u2WlanProfileId);
            }
        }
    }
    while (nmhGetNextIndexCapwapDot11WlanTable
           (u4WlanProfileId, &u4NextProfileId) == SNMP_SUCCESS);
#else
    UNUSED_PARAM (CliHandle);
#endif
    return CLI_SUCCESS;
}

INT4
WsscfgWlanMacShowRunningConfig (tCliHandle CliHandle)
{
#ifdef WLC_WANTED
    UINT4               u4NextProfileId = 0, u4WlanProfileId = 0;
    INT4                i4WlanIfIndex = 0, i4OutCome = 0, i4MacType = 0;
    UINT2               u2WlanProfileId = 0;
    UINT1               au1TunnelMode[CLI_MAX_SSID_LEN];
    tSNMP_OCTET_STRING_TYPE TunnelMode;

    MEMSET (au1TunnelMode, 0, CLI_MAX_SSID_LEN);
    MEMSET (&TunnelMode, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    i4OutCome = nmhGetFirstIndexCapwapDot11WlanTable (&u4NextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        CONFIG_UNLOCK ();
        return CLI_SUCCESS;
    }

    do
    {
        CliPrintf (CliHandle, "!\r\n\r\n");

        u4WlanProfileId = u4NextProfileId;
        if (nmhGetCapwapDot11WlanProfileIfIndex (u4WlanProfileId,
                                                 &i4WlanIfIndex) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        WssWlanGetWlanIdfromIfIndex (((UINT4) i4WlanIfIndex), &u2WlanProfileId);
        if (nmhGetCapwapDot11WlanMacType (u4WlanProfileId, &i4MacType) !=
            SNMP_SUCCESS)
        {
        }
        if (i4MacType == CLI_WTP_MAC_TYPE_SPLIT)
        {
            CliPrintf (CliHandle, "wlan mac split tunnel %d\r\n",
                       u2WlanProfileId);
        }
        else if (i4MacType == CLI_WTP_MAC_TYPE_LOCAL)
        {
            TunnelMode.pu1_OctetList = au1TunnelMode;
            if (nmhGetCapwapDot11WlanTunnelMode
                (u4WlanProfileId, &TunnelMode) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle, "wlan mac local %s %d\r\n",
                       TunnelMode.pu1_OctetList, u2WlanProfileId);
        }

    }
    while (nmhGetNextIndexCapwapDot11WlanTable
           (u4WlanProfileId, &u4NextProfileId) == SNMP_SUCCESS);
#else
    UNUSED_PARAM (CliHandle);
#endif
    return CLI_SUCCESS;
}

INT4
WsscfgApWlanShowRunningConfig (tCliHandle CliHandle)
{
#ifdef WLC_WANTED
    INT4                i4NextRadioIfIndex = 0, i4RadioIfIndex =
        0, i4WlanIfIndex = 0;
    INT4                i4OutCome = 0, i4AdminStatus = 0;
    UINT4               u4NextWlanProfileId = 0, u4WlanProfileId = 0;
    UINT4               u4GetRadioType = 0;
    UINT2               u2WlanProfileId = 0;
    tRadioIfGetDB       radioIfgetDB;
    UINT1               u1RadioId = 0;
    UINT1               au1ManagmentSSID[WSSWLAN_SSID_NAME_LEN];
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    MEMSET (&radioIfgetDB, 0, sizeof (tRadioIfGetDB));
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    radioIfgetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (radioIfgetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return CLI_FAILURE;
    }

    i4OutCome = nmhGetFirstIndexCapwapDot11WlanBindTable (&i4NextRadioIfIndex,
                                                          &u4NextWlanProfileId);

    if (i4OutCome == SNMP_FAILURE)
    {
        UtlShMemFreeAntennaSelectionBuf (radioIfgetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        CONFIG_UNLOCK ();
        return CLI_SUCCESS;
    }
    do
    {
        CliPrintf (CliHandle, "!\r\n\r\n");

        i4RadioIfIndex = i4NextRadioIfIndex;
        u4WlanProfileId = u4NextWlanProfileId;

        radioIfgetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4RadioIfIndex;
        radioIfgetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
        radioIfgetDB.RadioIfIsGetAllDB.bAdminStatus = OSIX_TRUE;
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                      &radioIfgetDB) != OSIX_SUCCESS)
        {
            UtlShMemFreeAntennaSelectionBuf (radioIfgetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return CLI_FAILURE;
        }
        u1RadioId = radioIfgetDB.RadioIfGetAllDB.u1RadioId;
        i4AdminStatus = radioIfgetDB.RadioIfGetAllDB.u1AdminStatus;
        if (nmhGetCapwapDot11WlanProfileIfIndex (u4WlanProfileId,
                                                 &i4WlanIfIndex) !=
            SNMP_SUCCESS)
        {
            UtlShMemFreeAntennaSelectionBuf (radioIfgetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return CLI_FAILURE;
        }

        WssWlanGetWlanIdfromIfIndex (((UINT4) i4WlanIfIndex), &u2WlanProfileId);
        WssWlanGetSSIDfromIfIndex (((UINT4) i4WlanIfIndex), au1ManagmentSSID);
        if (nmhGetIfMainAdminStatus (i4WlanIfIndex, &i4AdminStatus)
            != SNMP_SUCCESS)
        {
            UtlShMemFreeAntennaSelectionBuf (radioIfgetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return CLI_FAILURE;
        }

        if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                    &u4GetRadioType) != SNMP_SUCCESS)
        {
            UtlShMemFreeAntennaSelectionBuf (radioIfgetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return CLI_FAILURE;
        }
        radioIfgetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4RadioIfIndex;
        radioIfgetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
        radioIfgetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        if (WssCfgGetRadioParams (&radioIfgetDB) != OSIX_SUCCESS)
        {
            UtlShMemFreeAntennaSelectionBuf (radioIfgetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return CLI_FAILURE;
        }
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            radioIfgetDB.RadioIfGetAllDB.u2WtpInternalId;
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            UtlShMemFreeAntennaSelectionBuf (radioIfgetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }

        if (i4AdminStatus == ACTIVE)
        {
            if ((u4GetRadioType == CLI_RADIO_TYPEA)
                || (u4GetRadioType == CLI_RADIO_TYPEAN)
                || (u4GetRadioType == CLI_RADIO_TYPEAC))
            {
                u4GetRadioType = CLI_RADIO_TYPEA;
            }
            else
                u4GetRadioType = CLI_RADIO_TYPEB;

            CliPrintf (CliHandle, "ap wlan enable %s %d %s dot11radio %d\r\n",
                       (u4GetRadioType ==
                        CLI_RADIO_TYPEA) ? "dot11a" : "dot11b", u2WlanProfileId,
                       pWssIfCapwapDB->CapwapGetDB.au1ProfileName, u1RadioId);
        }

    }
    while (nmhGetNextIndexCapwapDot11WlanBindTable (i4RadioIfIndex,
                                                    &i4NextRadioIfIndex,
                                                    u4WlanProfileId,
                                                    &u4NextWlanProfileId) ==
           SNMP_SUCCESS);
    UtlShMemFreeAntennaSelectionBuf (radioIfgetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
#else
    UNUSED_PARAM (CliHandle);
#endif
    return CLI_SUCCESS;

}

INT4
WsscfgCapabilityShowRunningConfig (tCliHandle CliHandle)
{
    UINT1               au1PrevWtpModelName[256] = { 0 };
    UINT1               au1WtpModelName[256] = { 0 };
    tSNMP_OCTET_STRING_TYPE WtpModelName = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE PrevWtpModelName = { NULL, 0 };
    INT4                i4Pollable = 0, i4CFPollRequest = 0, i4PrivacyOption =
        0;
    INT4                i4PreambleOption = 0, i4PBCCOption = 0, i4Dot11Channel =
        0;
    INT4                i4QosOption = 0, i4Spectrum = 0, i4ShortSlot =
        0, i4APSDOption = 0;
    INT4                i4DSSSOFDMOption = 0, i4DelayedBlock =
        0, i4ImmediateBlock = 0;
    INT4                i4qack = 0, i4QueueRequest = 0, i4TXOPRequest = 0;
    INT4                i4RSNAPreauthentication = 0, i4RSNAOption = 0;
    UINT1               au1DfltWtpModelName[] = "default";

    WtpModelName.pu1_OctetList = au1WtpModelName;
    WtpModelName.i4_Length = 256;
    PrevWtpModelName.pu1_OctetList = au1PrevWtpModelName;
    PrevWtpModelName.i4_Length = 256;

    if (nmhGetFirstIndexFsDot11CapabilityProfileTable (&WtpModelName)
        != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }
    do
    {
        if (MEMCMP (au1WtpModelName, au1DfltWtpModelName,
                    STRLEN (au1DfltWtpModelName)) != 0)
        {
            CliPrintf (CliHandle, "!\r\n\r\n");

            CliPrintf (CliHandle, "capability profile %s\r\n", au1WtpModelName);
            nmhGetFsDot11CFPollable (&WtpModelName, &i4Pollable);
            if (i4Pollable != 0)
            {
                CliPrintf (CliHandle, " set cf-pollable enable\r\n");
            }
            nmhGetFsDot11CFPollRequest (&WtpModelName, &i4CFPollRequest);
            if (i4CFPollRequest != 0)
            {
                CliPrintf (CliHandle, " set cf-poll request enable\r\n");
            }
            nmhGetFsDot11PrivacyOptionImplemented (&WtpModelName,
                                                   &i4PrivacyOption);
            if (i4PrivacyOption != 0)
            {
                CliPrintf (CliHandle, " set privacy enable\r\n");
            }
            nmhGetFsDot11ShortPreambleOptionImplemented (&WtpModelName,
                                                         &i4PreambleOption);
            if (i4PreambleOption != 0)
            {
                CliPrintf (CliHandle, " set short-preamble enable\r\n");
            }
            nmhGetFsDot11PBCCOptionImplemented (&WtpModelName, &i4PBCCOption);
            if (i4PBCCOption != 0)
            {
                CliPrintf (CliHandle, " set pbcc enable\r\n");
            }
            nmhGetFsDot11ChannelAgilityPresent (&WtpModelName, &i4Dot11Channel);
            if (i4Dot11Channel != 0)
            {
                CliPrintf (CliHandle, " set channel-agility enable\r\n");
            }
            nmhGetFsDot11QosOptionImplemented (&WtpModelName, &i4QosOption);
            if (i4QosOption != 0)
            {
                CliPrintf (CliHandle, " set qos enable\r\n");
            }
            nmhGetFsDot11SpectrumManagementRequired (&WtpModelName,
                                                     &i4Spectrum);
            if (i4Spectrum != 0)
            {
                CliPrintf (CliHandle, " set spectrum-management enable\r\n");
            }
            nmhGetFsDot11ShortSlotTimeOptionImplemented (&WtpModelName,
                                                         &i4ShortSlot);
            if (i4ShortSlot != 0)
            {
                CliPrintf (CliHandle, " set short-slot-time enable\r\n");
            }
            nmhGetFsDot11APSDOptionImplemented (&WtpModelName, &i4APSDOption);
            if (i4APSDOption != 0)
            {
                CliPrintf (CliHandle, " set auto-power-save enable\r\n");
            }
            nmhGetFsDot11DSSSOFDMOptionEnabled (&WtpModelName,
                                                &i4DSSSOFDMOption);
            if (i4DSSSOFDMOption != 0)
            {
                CliPrintf (CliHandle, " set dsss-ofdm-modulation enable\r\n");
            }
            nmhGetFsDot11DelayedBlockAckOptionImplemented (&WtpModelName,
                                                           &i4DelayedBlock);
            if (i4DelayedBlock != 0)
            {
                CliPrintf (CliHandle, " set delayed-block-ack enable\r\n");
            }
            nmhGetFsDot11ImmediateBlockAckOptionImplemented (&WtpModelName,
                                                             &i4ImmediateBlock);
            if (i4ImmediateBlock != 0)
            {
                CliPrintf (CliHandle, " set immediate-block-ack enable\r\n");
            }

            nmhGetFsDot11QAckOptionImplemented (&WtpModelName, &i4qack);
            if (i4qack != 0)
            {
                CliPrintf (CliHandle, " set q-ack enable\r\n");
            }
            nmhGetFsDot11QueueRequestOptionImplemented (&WtpModelName,
                                                        &i4QueueRequest);
            if (i4QueueRequest != 0)
            {
                CliPrintf (CliHandle, " set queue-request enable\r\n");
            }
            nmhGetFsDot11TXOPRequestOptionImplemented (&WtpModelName,
                                                       &i4TXOPRequest);
            if (i4TXOPRequest != 0)
            {
                CliPrintf (CliHandle, " set txop-request enable\r\n");
            }
            nmhGetFsDot11RSNAOptionImplemented (&WtpModelName, &i4RSNAOption);
            if (i4RSNAOption != 0)
            {
                nmhGetFsDot11RSNAPreauthenticationImplemented (&WtpModelName,
                                                               &i4RSNAPreauthentication);
                CliPrintf (CliHandle, " set rsna enable %s\n",
                           (i4RSNAPreauthentication !=
                            0) ? "rsna-pre-authentication enable" : "");

            }
            CliPrintf (CliHandle, "exit\r\n\r\n");
        }

        MEMCPY (PrevWtpModelName.pu1_OctetList, WtpModelName.pu1_OctetList,
                WtpModelName.i4_Length);
        PrevWtpModelName.i4_Length = WtpModelName.i4_Length;

        /* clear the old values */
        MEMSET (au1WtpModelName, 0, sizeof (au1WtpModelName));

    }
    while (nmhGetNextIndexFsDot11CapabilityProfileTable (&PrevWtpModelName,
                                                         &WtpModelName) ==
           SNMP_SUCCESS);
    return CLI_SUCCESS;
}

/****************************************************************************
  * Function    :  WlanShowRunningWtpStationConfig
  * Input       :  CliHandle 
  * Descritpion :  This Routine shows the configured CLI commands of the 
  *                WLAN module, by comapring with the default values.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS or CLI_FAILURE
***************************************************************************/
INT4
WlanShowRunningWtpStationConfig (tCliHandle CliHandle)
{
#ifdef WLC_WANTED
    INT4                i4WlanIndex = 0;
    INT4                i4WlanDot11VlanId = 0;
    INT4                i4WlanDot11SuppSSID = 0;
    INT4                i4WlanDot11BandwidthThresh = 0;
    INT4                i4PrevWlanIndex = 0;
    UINT1               u1CfaType = 0;
    UINT2               u2WlanProfileId = 0;

    if (nmhGetFirstIndexDot11StationConfigTable (&i4WlanIndex) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        CfaGetIfType (i4WlanIndex, &u1CfaType);
        if (u1CfaType == CFA_CAPWAP_VIRT_RADIO)
        {
            i4PrevWlanIndex = i4WlanIndex;
            i4WlanIndex = 0;
            continue;
        }

        CliPrintf (CliHandle, "!\r\n\r\n");

        i4WlanDot11VlanId = 0;
        nmhGetFsDot11VlanId (i4WlanIndex, &i4WlanDot11VlanId);

        WssWlanGetWlanIdfromIfIndex (((UINT4) i4WlanIndex), &u2WlanProfileId);

        if (i4WlanDot11VlanId != 0)
        {
            CliPrintf (CliHandle, "wlan %d vlan %d \r\n\r\n",
                       u2WlanProfileId, i4WlanDot11VlanId);
        }

        i4WlanDot11SuppSSID = 0;
        nmhGetFsDot11SupressSSID (i4WlanIndex, &i4WlanDot11SuppSSID);
        if (i4WlanDot11SuppSSID != 0)
        {
            CliPrintf (CliHandle, "wlan hide ssid %d  \r\n\r\n",
                       i4WlanDot11SuppSSID);
        }

        i4WlanDot11BandwidthThresh = 0;
        nmhGetFsDot11BandwidthThresh (i4WlanIndex, &i4WlanDot11BandwidthThresh);

        if (i4WlanDot11BandwidthThresh != WSS_WLAN_DEF_BANDWIDTH_THRESH)    /*Macro to be added */
        {
            CliPrintf (CliHandle, "wlan %d bandwidth-thresh %d \r\n\r\n",
                       u2WlanProfileId, i4WlanDot11BandwidthThresh);
        }

        i4PrevWlanIndex = i4WlanIndex;
        i4WlanIndex = 0;
    }
    while (nmhGetNextIndexDot11StationConfigTable (i4PrevWlanIndex,
                                                   &i4WlanIndex) ==
           SNMP_SUCCESS);

    CliPrintf (CliHandle, "!\r\n\r\n");
#else
    UNUSED_PARAM (CliHandle);
#endif
    return CLI_SUCCESS;
}

/****************************************************************************
  * Function    :  WlanShowRunningVlanIsolationConfig
  * Input       :  CliHandle 
  * Descritpion :  This Routine shows the configured CLI commands of the 
  *                WLAN module, by comapring with the default values.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS or CLI_FAILURE
***************************************************************************/

INT4
WlanShowRunningVlanIsolationConfig (tCliHandle CliHandle)
{
#ifdef WLC_WANTED
    INT4                i4WlanIndex = 0;
    UINT4               u4WlanProfileID = 0;
    INT4                i4WlanDot11VlanIsolation = 0;
    UINT4               u4PrevWlanProfileID = 0;

    if (nmhGetFirstIndexCapwapDot11WlanTable (&u4WlanProfileID) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        CliPrintf (CliHandle, "!\r\n\r\n");

        if (nmhGetCapwapDot11WlanProfileIfIndex (u4WlanProfileID, &i4WlanIndex)
            != SNMP_SUCCESS)
        {
        }

        i4WlanDot11VlanIsolation = 0;
        nmhGetFsVlanIsolation (i4WlanIndex, &i4WlanDot11VlanIsolation);
        CliPrintf (CliHandle, "wlan vlan isolation %d i4WlanIndex %d \r\n\r\n",
                   i4WlanDot11VlanIsolation, i4WlanIndex);

        u4PrevWlanProfileID = u4WlanProfileID;
        u4WlanProfileID = 0;
    }
    while (nmhGetNextIndexCapwapDot11WlanTable (u4PrevWlanProfileID,
                                                &u4WlanProfileID) ==
           SNMP_SUCCESS);

    CliPrintf (CliHandle, "!\r\n\r\n");
#else
    UNUSED_PARAM (CliHandle);
#endif
    return CLI_SUCCESS;
}

/****************************************************************************
  * Function    :  WlanShowRunningWlanQosProfileConfig
  * Input       :  CliHandle 
  * Descritpion :  This Routine shows the configured CLI commands of the 
  *                WLAN module, by comapring with the default values.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS or CLI_FAILURE
***************************************************************************/
INT4
WlanShowRunningWlanQosProfileConfig (tCliHandle CliHandle)
{

    UINT4               u4NextProfileId = 0, u4WlanProfileId = 0;
    INT4                i4WlanIfIndex = 0, i4OutCome = 0;
    INT4                i4FsDot11WlanQosTraffic = 0,
        i4FsDot11WlanQosPassengerTrustMode = 0,
        i4FsDot11WlanQosRateLimit = 0,
        i4FsDot11WlanUpStreamCIR = 0,
        i4FsDot11WlanUpStreamCBS = 0,
        i4FsDot11WlanUpStreamEIR = 0,
        i4FsDot11WlanUpStreamEBS = 0,
        i4FsDot11WlanDownStreamCIR = 0,
        i4FsDot11WlanDownStreamCBS = 0,
        i4FsDot11WlanDownStreamEIR = 0,
        i4FsDot11WlanDownStreamEBS = 0, i4FsDot11WlanQosRowStatus = 0;

    i4OutCome = nmhGetFirstIndexCapwapDot11WlanTable (&u4NextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        CONFIG_UNLOCK ();
        return CLI_SUCCESS;
    }
    do
    {
        CliPrintf (CliHandle, "!\r\n\r\n");
        u4WlanProfileId = u4NextProfileId;
        if (nmhGetCapwapDot11WlanProfileIfIndex (u4WlanProfileId,
                                                 &i4WlanIfIndex) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhGetFsDot11WlanQosTraffic (i4WlanIfIndex,
                                         &i4FsDot11WlanQosTraffic) ==
            SNMP_SUCCESS)
        {
            if (i4FsDot11WlanQosTraffic != 1)
            {
                /*If not default condition */
                switch (i4FsDot11WlanQosTraffic)
                {
                    case 2:
                        CliPrintf (CliHandle,
                                   "set priority-level gold wlan%d \r\n\r\n",
                                   u4WlanProfileId);
                        break;
                    case 3:
                        CliPrintf (CliHandle,
                                   "set priority-level platinum wlan%d \r\n\r\n",
                                   u4WlanProfileId);
                        break;
                    case 4:
                        CliPrintf (CliHandle,
                                   "set priority-level bronze wlan%d\r\n\r\n",
                                   u4WlanProfileId);
                        break;
                    default:
                        CliPrintf (CliHandle, "invalid priority\r\n\r\n");
                }
            }
        }
        else
        {
            return CLI_FAILURE;
        }
        if (nmhGetFsDot11WlanQosPassengerTrustMode (i4WlanIfIndex,
                                                    &i4FsDot11WlanQosPassengerTrustMode)
            == SNMP_SUCCESS)
        {
            if (i4FsDot11WlanQosPassengerTrustMode != 2)
            {
                /*iF NOT DEFAULT CONDITION */
                if (i4FsDot11WlanQosPassengerTrustMode == 1)
                {
                    /*enable = 1 */
                    CliPrintf (CliHandle,
                               "set passenger trust mode enable wlan%d \r\n\r\n",
                               u4WlanProfileId);
                }
                else
                {
                    CliPrintf (CliHandle, "invalid passenger mode\r\n\r\n");
                }
            }
        }
        else
        {
            return CLI_FAILURE;
        }
        if (nmhGetFsDot11WlanQosRateLimit (i4WlanIfIndex,
                                           &i4FsDot11WlanQosRateLimit) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhGetFsDot11WlanUpStreamCIR (i4WlanIfIndex,
                                          &i4FsDot11WlanUpStreamCIR) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhGetFsDot11WlanUpStreamCBS (i4WlanIfIndex,
                                          &i4FsDot11WlanUpStreamCBS) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhGetFsDot11WlanUpStreamEIR (i4WlanIfIndex,
                                          &i4FsDot11WlanUpStreamEIR) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhGetFsDot11WlanUpStreamEBS (i4WlanIfIndex,
                                          &i4FsDot11WlanUpStreamEBS) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhGetFsDot11WlanDownStreamCIR (i4WlanIfIndex,
                                            &i4FsDot11WlanDownStreamCIR) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhGetFsDot11WlanDownStreamCBS (i4WlanIfIndex,
                                            &i4FsDot11WlanDownStreamCBS) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhGetFsDot11WlanDownStreamEIR (i4WlanIfIndex,
                                            &i4FsDot11WlanDownStreamEIR) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhGetFsDot11WlanDownStreamEBS (i4WlanIfIndex,
                                            &i4FsDot11WlanDownStreamEBS) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhGetFsDot11WlanQosRowStatus (i4WlanIfIndex,
                                           &i4FsDot11WlanQosRowStatus) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    while (nmhGetNextIndexCapwapDot11WlanTable
           (u4WlanProfileId, &u4NextProfileId) == SNMP_SUCCESS);
    return CLI_SUCCESS;
}

/****************************************************************************
  * Function    :  WlanShowRunningQosProfileConfig
  * Input       :  CliHandle 
  * Descritpion :  This Routine shows the configured CLI commands of the 
  *                WLAN module, by comapring with the default values.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS or CLI_FAILURE
***************************************************************************/
INT4
WlanShowRunningQosProfileConfig (tCliHandle CliHandle)
{

    tSNMP_OCTET_STRING_TYPE CurrentProfileName = { NULL, 0 };
    tSNMP_OCTET_STRING_TYPE NextProfileName = { NULL, 0 };
    UINT1               au1CurProfileName[256];
    UINT1               au1NextProfileName[256];
    UINT1               au1DfltQosProfStringVal[] = "default";
    INT4                i4RetValFsDot11QosRateLimit = 0;
    INT4                i4RetValFsDot11UpStreamCIR = 0;
    INT4                i4RetValFsDot11UpStreamCBS = 0;
    INT4                i4RetValFsDot11UpStreamEIR = 0;
    INT4                i4RetValFsDot11UpStreamEBS = 0;
    INT4                i4RetValFsDot11DownStreamCIR = 0;
    INT4                i4RetValFsDot11DownStreamCBS = 0;
    INT4                i4RetValFsDot11DownStreamEIR = 0;
    INT4                i4RetValFsDot11DownStreamEBS = 0;
    INT4                i4RetValFsDot11QosPassengerTrustMode = 0;
    INT4                i4RetValFsDot11QosTraffic = 0;
    INT4                i4RetValFsDot11QosRowStatus = 0;
    INT2                i2CfgModeChanged = 0;

    CurrentProfileName.pu1_OctetList = au1CurProfileName;
    CurrentProfileName.i4_Length = 256;
    NextProfileName.pu1_OctetList = au1NextProfileName;
    NextProfileName.i4_Length = 256;

    MEMSET (&au1CurProfileName, 0, 256);
    MEMSET (&au1NextProfileName, 0, 256);

    if (nmhGetFirstIndexFsDot11QosProfileTable (&NextProfileName) !=
        SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        CliPrintf (CliHandle, "!\r\n\r\n");

        STRCPY (CurrentProfileName.pu1_OctetList,
                NextProfileName.pu1_OctetList);
        CurrentProfileName.i4_Length = NextProfileName.i4_Length;

        nmhGetFsDot11QosRowStatus (&CurrentProfileName,
                                   &i4RetValFsDot11QosRowStatus);
        nmhGetFsDot11QosRateLimit (&CurrentProfileName,
                                   &i4RetValFsDot11QosRateLimit);
        nmhGetFsDot11UpStreamCIR (&CurrentProfileName,
                                  &i4RetValFsDot11UpStreamCIR);
        nmhGetFsDot11UpStreamCBS (&CurrentProfileName,
                                  &i4RetValFsDot11UpStreamCBS);
        nmhGetFsDot11UpStreamEIR (&CurrentProfileName,
                                  &i4RetValFsDot11UpStreamEIR);
        nmhGetFsDot11UpStreamEBS (&CurrentProfileName,
                                  &i4RetValFsDot11UpStreamEBS);
        nmhGetFsDot11DownStreamCIR (&CurrentProfileName,
                                    &i4RetValFsDot11DownStreamCIR);
        nmhGetFsDot11DownStreamCBS (&CurrentProfileName,
                                    &i4RetValFsDot11DownStreamCBS);
        nmhGetFsDot11DownStreamEIR (&CurrentProfileName,
                                    &i4RetValFsDot11DownStreamEIR);
        nmhGetFsDot11DownStreamEBS (&CurrentProfileName,
                                    &i4RetValFsDot11DownStreamEBS);
        if (i4RetValFsDot11QosRateLimit == 0)
        {
            CliPrintf (CliHandle, "qos profile %s\r\n\r\n",
                       CurrentProfileName.pu1_OctetList);
            i2CfgModeChanged = 1;
            CliPrintf (CliHandle, "set qos rate-limit disable\r\n\r\n");
        }
        else
        {
            if (MEMCMP
                (CurrentProfileName.pu1_OctetList, au1DfltQosProfStringVal,
                 STRLEN (au1DfltQosProfStringVal)) != 0)
            {
                CliPrintf (CliHandle, "qos profile %s\r\n\r\n",
                           CurrentProfileName.pu1_OctetList);
                i2CfgModeChanged = 1;
                CliPrintf (CliHandle,
                           "set qos rate-limit enable upstream %d %d %d %d "
                           "downstream %d %d %d %d\r\n\r\n",
                           i4RetValFsDot11UpStreamCIR,
                           i4RetValFsDot11UpStreamCBS,
                           i4RetValFsDot11UpStreamEIR,
                           i4RetValFsDot11UpStreamEBS,
                           i4RetValFsDot11DownStreamCIR,
                           i4RetValFsDot11DownStreamCBS,
                           i4RetValFsDot11DownStreamEIR,
                           i4RetValFsDot11DownStreamEBS);
            }
            else
            {
                if ((i4RetValFsDot11UpStreamCIR != CLI_DEF_CIR_VAL) ||
                    (i4RetValFsDot11UpStreamCBS != CLI_DEF_CBS_VAL) ||
                    (i4RetValFsDot11UpStreamEIR != CLI_DEF_EIR_VAL) ||
                    (i4RetValFsDot11UpStreamEBS != CLI_DEF_EBS_VAL) ||
                    (i4RetValFsDot11DownStreamCIR != CLI_DEF_CIR_VAL) ||
                    (i4RetValFsDot11DownStreamCBS != CLI_DEF_CBS_VAL) ||
                    (i4RetValFsDot11DownStreamEIR != CLI_DEF_EIR_VAL) ||
                    (i4RetValFsDot11DownStreamEBS != CLI_DEF_EBS_VAL))
                {
                    CliPrintf (CliHandle, "qos profile %s\r\n\r\n",
                               CurrentProfileName.pu1_OctetList);
                    i2CfgModeChanged = 1;
                    CliPrintf (CliHandle,
                               "set qos rate-limit enable upstream %d %d %d %d "
                               "downstream %d %d %d %d\r\n\r\n",
                               i4RetValFsDot11UpStreamCIR,
                               i4RetValFsDot11UpStreamCBS,
                               i4RetValFsDot11UpStreamEIR,
                               i4RetValFsDot11UpStreamEBS,
                               i4RetValFsDot11DownStreamCIR,
                               i4RetValFsDot11DownStreamCBS,
                               i4RetValFsDot11DownStreamEIR,
                               i4RetValFsDot11DownStreamEBS);
                }
            }
        }

        nmhGetFsDot11QosPassengerTrustMode (&CurrentProfileName,
                                            &i4RetValFsDot11QosPassengerTrustMode);

        if ((i4RetValFsDot11QosPassengerTrustMode == CLI_WSSCFG_ENABLE) ||
            (i4RetValFsDot11QosPassengerTrustMode == CLI_WSSCFG_DISABLE))
        {
            if (MEMCMP
                (CurrentProfileName.pu1_OctetList, au1DfltQosProfStringVal,
                 STRLEN (au1DfltQosProfStringVal)) == 0)
            {
                if (i4RetValFsDot11QosPassengerTrustMode != CLI_WSSCFG_ENABLE)
                {
                    if (i2CfgModeChanged == 0)
                    {
                        CliPrintf (CliHandle, "qos profile %s\r\n\r\n",
                                   CurrentProfileName.pu1_OctetList);
                        i2CfgModeChanged = 1;
                    }
                    CliPrintf (CliHandle,
                               "set passenger trust mode disable\r\n\r\n");
                }
            }
            else
            {
                if (i2CfgModeChanged == 0)
                {
                    CliPrintf (CliHandle, "qos profile %s\r\n\r\n",
                               CurrentProfileName.pu1_OctetList);
                    i2CfgModeChanged = 1;
                }
                CliPrintf (CliHandle, "set passenger trust mode %s\r\n\r\n",
                           (i4RetValFsDot11QosPassengerTrustMode ==
                            CLI_WSSCFG_ENABLE) ? "enable" : "disable");
            }
        }

        nmhGetFsDot11QosTraffic (&CurrentProfileName,
                                 &i4RetValFsDot11QosTraffic);

        if ((i4RetValFsDot11QosTraffic == CLI_WSSCFG_BEST_EFFORT) ||
            (i4RetValFsDot11QosTraffic == CLI_WSSCFG_VIDEO) ||
            (i4RetValFsDot11QosTraffic == CLI_WSSCFG_VOICE) ||
            (i4RetValFsDot11QosTraffic == CLI_WSSCFG_BACKGROUND))
        {
            if (MEMCMP
                (CurrentProfileName.pu1_OctetList, au1DfltQosProfStringVal,
                 STRLEN (au1DfltQosProfStringVal)) == 0)
            {
                if (i4RetValFsDot11QosTraffic != CLI_WSSCFG_VOICE)
                {
                    if (i2CfgModeChanged == 0)
                    {
                        CliPrintf (CliHandle, "qos profile %s\r\n\r\n",
                                   CurrentProfileName.pu1_OctetList);
                    }

                    CliPrintf (CliHandle, "set traffic %s\r\n\r\n",
                               (i4RetValFsDot11QosTraffic ==
                                CLI_WSSCFG_BEST_EFFORT) ? "best-effort"
                               : (i4RetValFsDot11QosTraffic ==
                                  CLI_WSSCFG_VIDEO) ? "video" : "background");
                }
            }
            else
            {
                if (i2CfgModeChanged == 0)
                {
                    CliPrintf (CliHandle, "qos profile %s\r\n\r\n",
                               CurrentProfileName.pu1_OctetList);
                }

                CliPrintf (CliHandle, "set traffic %s\r\n\r\n",
                           (i4RetValFsDot11QosTraffic ==
                            CLI_WSSCFG_BEST_EFFORT) ? "best-effort"
                           : (i4RetValFsDot11QosTraffic ==
                              CLI_WSSCFG_VIDEO) ? "video"
                           : (i4RetValFsDot11QosTraffic ==
                              CLI_WSSCFG_VOICE) ? "voice" : "background");
            }
        }
    }
    while (nmhGetNextIndexFsDot11QosProfileTable
           (&CurrentProfileName, &NextProfileName) == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/****************************************************************************
  * Function    :  WlanShowRunningQosProfileMappingConfig
  * Input       :  CliHandle 
  * Descritpion :  This Routine shows the configured CLI commands of the 
  *                WLAN module, by comapring with the default values.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS or CLI_FAILURE
***************************************************************************/
INT4
WlanShowRunningQosProfileMappingConfig (tCliHandle CliHandle)
{
#ifdef WLC_WANTED
    INT4                i4NextIfIndex = 0;
    INT4                i4CurrentIfIndex = 0;
    INT4                i4RetValFsDot11QosMappingRowStatus = 0;
    tSNMP_OCTET_STRING_TYPE RetValFsDot11QosMappingProfileName = { NULL, 0 };
    UINT1               au1CurProfileName[256];
    UINT2               u2WlanProfileId = 0;

    RetValFsDot11QosMappingProfileName.pu1_OctetList = au1CurProfileName;
    RetValFsDot11QosMappingProfileName.i4_Length = 256;

    MEMSET (&au1CurProfileName, 0, 256);

    if (nmhGetFirstIndexFsDot11QosMappingTable (&i4NextIfIndex) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        CliPrintf (CliHandle, "!\r\n\r\n");

        i4CurrentIfIndex = i4NextIfIndex;
        nmhGetFsDot11QosMappingRowStatus (i4CurrentIfIndex,
                                          &i4RetValFsDot11QosMappingRowStatus);

        nmhGetFsDot11QosMappingProfileName (i4CurrentIfIndex,
                                            &RetValFsDot11QosMappingProfileName);

        WssWlanGetWlanIdfromIfIndex (((UINT4) i4CurrentIfIndex),
                                     &u2WlanProfileId);

        CliPrintf (CliHandle, "wlan %d qos-profile %s", u2WlanProfileId,
                   RetValFsDot11QosMappingProfileName.pu1_OctetList);

    }
    while (nmhGetNextIndexFsDot11QosMappingTable
           (i4CurrentIfIndex, &i4NextIfIndex) == SNMP_SUCCESS);

#else
    UNUSED_PARAM (CliHandle);
#endif
    return CLI_SUCCESS;
}

/****************************************************************************
  * Function    :  WlanShowRunningSpectrumConfig
  * Input       :  CliHandle 
  * Descritpion :  This Routine shows the configured CLI commands of the 
  *                WLAN module, by comapring with the default values.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS or CLI_FAILURE
***************************************************************************/
INT4
WlanShowRunningSpectrumConfig (tCliHandle CliHandle)
{
#ifdef WLC_WANTED
    INT4                i4CurIfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4Dot11SpectrumMgmtNextIndex = 0;
    INT4                i4Dot11SpectrumMgmtCurrentIndex = 0;
    UINT1               au1CurProfileName[256];
    INT4                i4RetValDot11MitigationRequirement = 0;
    UINT2               u2WlanProfileId = 0;

    MEMSET (&au1CurProfileName, 0, 256);

    if (nmhGetFirstIndexDot11SpectrumManagementTable (&i4NextIfIndex,
                                                      (UINT4 *)
                                                      &i4Dot11SpectrumMgmtNextIndex)
        != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        CliPrintf (CliHandle, "!\r\n\r\n");
        i4CurIfIndex = i4NextIfIndex;
        i4Dot11SpectrumMgmtCurrentIndex = i4Dot11SpectrumMgmtNextIndex;

        nmhGetDot11MitigationRequirement (i4CurIfIndex,
                                          i4Dot11SpectrumMgmtCurrentIndex,
                                          &i4RetValDot11MitigationRequirement);
        WssWlanGetWlanIdfromIfIndex (((UINT4) i4CurIfIndex), &u2WlanProfileId);

        CliPrintf (CliHandle, "dot11h powerconstraint %d wlan %d \r\n\r\n",
                   i4RetValDot11MitigationRequirement, u2WlanProfileId);

    }
    while (nmhGetNextIndexDot11SpectrumManagementTable
           (i4CurIfIndex, &i4NextIfIndex, i4Dot11SpectrumMgmtCurrentIndex,
            (UINT4 *) &i4Dot11SpectrumMgmtNextIndex) == SNMP_SUCCESS);

#else
    UNUSED_PARAM (CliHandle);
#endif
    return CLI_SUCCESS;
}

/****************************************************************************
  * Function    :  WlanShowRunningAuthInfoConfig
  * Input       :  CliHandle 
  * Descritpion :  This Routine shows the configured CLI commands of the 
  *                WLAN module, by comapring with the default values.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS or CLI_FAILURE
***************************************************************************/
INT4
WlanShowRunningAuthInfoConfig (tCliHandle CliHandle)
{

    tSNMP_OCTET_STRING_TYPE NextWebAuthUName;
    tSNMP_OCTET_STRING_TYPE WebAuthUName;
    UINT1               au1FsSecurityWebAuthUName[WSS_STA_WEBUSER_LEN_MAX];
    UINT1               au1NextFsSecurityWebAuthUName[WSS_STA_WEBUSER_LEN_MAX];
    INT4                i4RetValFsSecurityWlanProfileId = 0;
    INT4                i4RetValFsSecurityWebAuthUserLifetime = 0;

    MEMSET (&au1FsSecurityWebAuthUName, 0, sizeof (au1FsSecurityWebAuthUName));
    MEMSET (&au1NextFsSecurityWebAuthUName, 0,
            sizeof (au1NextFsSecurityWebAuthUName));
    MEMSET (&WebAuthUName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextWebAuthUName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    WebAuthUName.pu1_OctetList = au1FsSecurityWebAuthUName;
    NextWebAuthUName.pu1_OctetList = au1NextFsSecurityWebAuthUName;

    MEMSET (&au1FsSecurityWebAuthUName, 0, WSS_STA_WEBUSER_LEN_MAX);
    MEMSET (&au1NextFsSecurityWebAuthUName, 0, WSS_STA_WEBUSER_LEN_MAX);

    if (nmhGetFirstIndexFsSecurityWebAuthGuestInfoTable (&NextWebAuthUName)
        != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        CliPrintf (CliHandle, "!\r\n\r\n");
        WebAuthUName = NextWebAuthUName;

        nmhGetFsSecurityWlanProfileId
            (&WebAuthUName, &i4RetValFsSecurityWlanProfileId);
        nmhGetFsSecurityWebAuthUserLifetime
            (&WebAuthUName, &i4RetValFsSecurityWebAuthUserLifetime);

        CliPrintf (CliHandle, "username %s wlan_id %d lifetime %d \r\n\r\n",
                   WebAuthUName.pu1_OctetList, i4RetValFsSecurityWlanProfileId,
                   i4RetValFsSecurityWebAuthUserLifetime);

    }
    while (nmhGetNextIndexFsSecurityWebAuthGuestInfoTable
           (&WebAuthUName, &NextWebAuthUName) == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/****************************************************************************
  * Function    :  WssCfgGetWlanStatistics
  * Input       :   pWssIfCapDB
  * Descritpion :  This Routine checks set value and invoke corresponding
  *                module
  * Output      :  None
  * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
***************************************************************************/
INT4
WssCfgGetWlanStatistics (UINT2 u2type, tWssIfPMDB * pWssIfPMDB)
{
    if (WssIfProcessPMDBMsg (u2type, pWssIfPMDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
  * Function    :  WlanShowRunningFsWlanStationTrapStatus
  * Input       :  CliHandle
  * Descritpion :  This Routine shows the configured CLI commands of the
  *                WLAN module, by comapring with the default values.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS or CLI_FAILURE
***************************************************************************/

INT4
WlanShowRunningFsWlanStationTrapStatus (tCliHandle CliHandle)
{
    INT4                i4TrapStatus = 0;

    nmhGetFsWlanStationTrapStatus (&i4TrapStatus);

    if (WSS_WLAN_STATION_DEFAULT_TRAP_STATUS == WSS_WLAN_STATION_TRAP_DISABLE)
    {
        if (i4TrapStatus != WSS_WLAN_STATION_DEFAULT_TRAP_STATUS)
        {
            CliPrintf (CliHandle, "wlan traps enable \r\n\r\n");
        }
    }
    else if (WSS_WLAN_STATION_DEFAULT_TRAP_STATUS ==
             WSS_WLAN_STATION_TRAP_ENABLE)
    {
        if (i4TrapStatus != WSS_WLAN_STATION_DEFAULT_TRAP_STATUS)
        {
            CliPrintf (CliHandle, "no wlan traps enable \r\n\r\n");
        }
    }
    CliPrintf (CliHandle, "!\r\n\r\n");
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgUtilUpdateFsDot11ExternalWebAuthProfileTable
 * Input       :   pWsscfgOldFsDot11ExternalWebAuthConfigEntry
                   pWsscfgFsDot11ExternalWebAuthConfigEntry
                   pWsscfgIsSetFsDot11ExternalWebAuthConfigEntry
 * Descritpion :  This Routine checks set value 
with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgUtilUpdateFsDot11ExternalWebAuthProfileTable
    (tWsscfgFsDot11ExternalWebAuthProfileEntry *
     pWsscfgFsDot11ExternalWebAuthProfileEntry,
     tWsscfgIsSetFsDot11ExternalWebAuthProfileEntry *
     pWsscfgIsSetFsDot11ExternalWebAuthProfileEntry)
{

    if (WssCfgSetFsDot11ExternalWebAuthProfileTable
        (pWsscfgFsDot11ExternalWebAuthProfileEntry,
         pWsscfgIsSetFsDot11ExternalWebAuthProfileEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
  * Function    :  WssCfgMaxClientCountCheck
  * Input       :  CliHandle
  * Descritpion :  This Routine shows the configured CLI commands of the
  *                WLAN module, by comapring with the default values.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS or CLI_FAILURE
***************************************************************************/

INT4
WssCfgMaxClientCountCheck (UINT4 u4RadioIndex,
                           UINT4 u4FsWlanRadioMaxClientCount)
{
#ifdef WLC_WANTED
    tMacAddr            au1NewStaMacArray[32];
    tMacAddr            u1TempMacArray;
    UINT1               u1CurrIndex = 0;
    UINT1               u1NextIndex = 0;
    UINT4               u4Temp = 0;
    UINT4               u4AssocTimeCurrent = 0;
    UINT4               u4AssocTimeNext = 0;
    tWssWlanDB         *pwssWlanDB = NULL;
    UINT4               u4StaCount = 0;
    UINT4               u4RadioIfIndex1 = 0;
    UINT1               u1Index = 0;
    UINT1               u1Loop = 0;
    UINT1               u1EntryFlag = 0;
    MEMSET (au1NewStaMacArray, 0, sizeof (tMacAddr) * 32);

    if (u4FsWlanRadioMaxClientCount == 0)
    {
        return OSIX_SUCCESS;
    }

    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanDB == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));

    WssStaShowClient ();

    for (u1Index = 0; u1Index < gu4ClientWalkIndex; u1Index++)
    {
        pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex =
            gaWssClientSummary[u1Index].u4BssIfIndex;
        pwssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;

        if (WssIfProcessWssWlanDBMsg
            (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pwssWlanDB))
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }
        u4RadioIfIndex1 = pwssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
        if (u4RadioIndex == u4RadioIfIndex1)
        {
            /*Storing all client mac's */
            MEMCPY (au1NewStaMacArray[u4StaCount],
                    gaWssClientSummary[u1Index].staMacAddr, sizeof (tMacAddr));
            u4StaCount++;
        }
    }
    if (u1EntryFlag == gu4ClientWalkIndex)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_SUCCESS;
    }
    if (u4StaCount == 0)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_SUCCESS;
    }
    /* If the  Newly configured value is greater than connected station count,
     * we dont send disassoc */
    if (u4FsWlanRadioMaxClientCount > u4StaCount)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_SUCCESS;
    }
    /*Need to Sort Array on the basis of connected time */
    for (u1CurrIndex = 0; u1CurrIndex < u4StaCount; ++u1CurrIndex)
    {
        if (nmhGetFsWlanClientStatsAssocTime
            (au1NewStaMacArray[u1CurrIndex],
             &u4AssocTimeCurrent) == SNMP_FAILURE)
        {
            continue;
        }
        for (u1NextIndex = u1CurrIndex + 1; u1NextIndex < u4StaCount;
             ++u1NextIndex)
        {
            if (nmhGetFsWlanClientStatsAssocTime
                (au1NewStaMacArray[u1NextIndex],
                 &u4AssocTimeNext) == SNMP_FAILURE)
            {
                continue;
            }
            if (u4AssocTimeCurrent > u4AssocTimeNext)
            {
                MEMSET (u1TempMacArray, 0, sizeof (tMacAddr));
                MEMCPY (u1TempMacArray, au1NewStaMacArray[u1CurrIndex],
                        sizeof (tMacAddr));
                u4Temp = u4AssocTimeCurrent;
                u4AssocTimeCurrent = u4AssocTimeNext;
                u4AssocTimeNext = u4Temp;
                MEMCPY (au1NewStaMacArray[u1CurrIndex],
                        au1NewStaMacArray[u1NextIndex], sizeof (tMacAddr));
                MEMCPY (au1NewStaMacArray[u1NextIndex], u1TempMacArray,
                        sizeof (tMacAddr));
            }
        }
    }
    /*Sending DeAuth */
    for (u1Loop = u4FsWlanRadioMaxClientCount; u1Loop < u4StaCount; u1Loop++)
    {
        WssCfgSendDeAuthForRadio (au1NewStaMacArray[u1Loop], u4RadioIndex);
    }
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
#else
    UNUSED_PARAM (u4RadioIndex);
    UNUSED_PARAM (u4FsWlanRadioMaxClientCount);
#endif
    return OSIX_SUCCESS;

}

/****************************************************************************
  * Function    :  WssCfgSendDeAuthForRadio 
  * Input       :  Station MAC, Radio IfIndex
  * Descritpion :  This utility shall trigger disassoc to wireless client 
  *                when maximum client threshold is reached per radio
  *                WLAN module, by comapring with the default values.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS or CLI_FAILURE
***************************************************************************/
VOID
WssCfgSendDeAuthForRadio (UINT1 *pu1StaMac, UINT4 u4RadioIndex)
{
#ifdef WLC_WANTED
    tWssMacMsgStruct   *pWssMacFrame = NULL;
    tWssStaWepProcessDB *pWssStaWepProcessDB = NULL;
    unWlcHdlrMsgStruct *pWlcHdlrMsgStruct = NULL;
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    pWlcHdlrMsgStruct = (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    if (pWlcHdlrMsgStruct == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssIfProcessWssAuthDBMsg :-"
                    "UtlShMemAllocWlcBuf returned failure\n");
        return;
    }
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    pWssMacFrame =
        (tWssMacMsgStruct *) (VOID *) UtlShMemAllocMacMsgStructBuf ();
    if (pWssMacFrame == NULL)
    {
        WSSSTA_TRC (WSSSTA_FAILURE_TRC,
                    "WssIfProcessWssAuthDBMsg :-"
                    "Failed to allocate "
                    "memory for disassoc message frame \r\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return;
    }
    MEMSET (pWssMacFrame, 0, sizeof (tWssMacMsgStruct));

    pWssStaWepProcessDB = WssStaProcessEntryGet (pu1StaMac);
    if (pWssStaWepProcessDB == NULL)
    {
        WSSSTA_TRC (WSSSTA_MGMT_TRC, "Failed to obtain the station "
                    "mac address \r\n");
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacFrame);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return;
    }
    WssStaDeAuthFn (pWssStaWepProcessDB->u4BssIfIndex);

    MEMCPY (pWssMacFrame->unMacMsg.DisassocMacFrame.
            MacMgmtFrmHdr.u1SA, pWssStaWepProcessDB->BssIdMacAddr,
            sizeof (tMacAddr));
    MEMCPY (pWssMacFrame->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.u1DA,
            pu1StaMac, sizeof (tMacAddr));
    MEMCPY (pWssMacFrame->unMacMsg.DisassocMacFrame.MacMgmtFrmHdr.u1BssId,
            pWssStaWepProcessDB->BssIdMacAddr, sizeof (tMacAddr));
    pWssMacFrame->unMacMsg.DisassocMacFrame.u2SessId =
        pWssStaWepProcessDB->u2Sessid;
    pWssMacFrame->unMacMsg.DisassocMacFrame.ReasonCode.u2MacFrameReasonCode =
        WSSSTA_DISASS_TOO_MANY_MS;

    MEMCPY (&(pWlcHdlrMsgStruct->WssMacMsgStruct), pWssMacFrame,
            sizeof (tWssMacMsgStruct));
    pWlcHdlrMsgStruct->WssMacMsgStruct.msgType = 0x07;
    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_MAC_DISASSOC_MSG,
                                pWlcHdlrMsgStruct) == OSIX_FAILURE)
    {
        WSSSTA_TRC (WSSSTA_MGMT_TRC, "Station is not connected "
                    "at present\r\n");
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacFrame);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return;
    }
    MEMSET (pWlcHdlrMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4RadioIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, (&RadioIfGetDB))
        != OSIX_SUCCESS)
    {
        return;
    }

    pWlcHdlrMsgStruct->StationConfReq.wssStaConfig.u2SessId =
        pWssStaWepProcessDB->u2Sessid;
    pWlcHdlrMsgStruct->StationConfReq.wssStaConfig.DelSta.isPresent = TRUE;
    pWlcHdlrMsgStruct->StationConfReq.wssStaConfig.DelSta.u2MessageType =
        DELETE_STATION;
    pWlcHdlrMsgStruct->StationConfReq.wssStaConfig.DelSta.u2MessageLength =
        WSSSTA_DELSTA_LEN;
    pWlcHdlrMsgStruct->StationConfReq.wssStaConfig.DelSta.u1RadioId =
        RadioIfGetDB.RadioIfGetAllDB.u1RadioId;
    pWlcHdlrMsgStruct->StationConfReq.wssStaConfig.DelSta.u1MacAddrLen =
        sizeof (tMacAddr);
    MEMCPY (pWlcHdlrMsgStruct->StationConfReq.wssStaConfig.DelSta.StaMacAddr,
            pu1StaMac, sizeof (tMacAddr));
    if (WssIfProcessWlcHdlrMsg (WSS_WLCHDLR_STATION_CONF_REQ, pWlcHdlrMsgStruct)
        == OSIX_FAILURE)
    {
        UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacFrame);
        UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
        return;
    }

    UtlShMemFreeMacMsgStructBuf ((UINT1 *) pWssMacFrame);
    UtlShMemFreeWlcBuf ((UINT1 *) pWlcHdlrMsgStruct);
#else
    UNUSED_PARAM (pu1StaMac);
    UNUSED_PARAM (u4RadioIndex);
#endif
    return;
}

/****************************************************************************
  * Function    :  WssCfgMaxClientCountCheckForSSID
  * Input       :  CliHandle
  * Descritpion :  This Routine shows the configured CLI commands of the
  *                WLAN module, by comapring with the default values.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS or CLI_FAILURE
***************************************************************************/

INT4
WssCfgMaxClientCountCheckForSSID (UINT2 u2WlanProfileId,
                                  UINT4 u4FsWlanMaxClientCount)
{
#ifdef WLC_WANTED
    tMacAddr            au1NewStaMacArray[32];
    tMacAddr            u1TempMacArray;
    UINT1               u1CurrIndex = 0;
    UINT1               u1NextIndex = 0;
    UINT4               u4Temp = 0;
    UINT4               u4AssocTimeCurrent = 0;
    UINT4               u4AssocTimeNext = 0;
    tWssWlanDB         *pwssWlanDB = NULL;
    UINT4               u4StaCount = 0;
    UINT4               u4RadioIfIndex = 0;
    UINT2               u2WlanProfileId1 = 0;
    UINT1               u1Index = 0;
    UINT1               u1Loop = 0;
    UINT1               u1EntryFlag = 0;
    MEMSET (au1NewStaMacArray, 0, sizeof (tMacAddr) * 32);

    if (u4FsWlanMaxClientCount == 0)
    {
        return OSIX_SUCCESS;
    }

    pwssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pwssWlanDB == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pwssWlanDB, 0, sizeof (tWssWlanDB));

    WssStaShowClient ();

    for (u1Index = 0; u1Index < gu4ClientWalkIndex; u1Index++)
    {
        pwssWlanDB->WssWlanAttributeDB.u4BssIfIndex =
            gaWssClientSummary[u1Index].u4BssIfIndex;
        pwssWlanDB->WssWlanIsPresentDB.bWlanInternalId = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
        pwssWlanDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;

        if (WssIfProcessWssWlanDBMsg
            (WSS_WLAN_GET_BSS_IFINDEX_ENTRY, pwssWlanDB))
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
            return OSIX_FAILURE;
        }
        u2WlanProfileId1 = pwssWlanDB->WssWlanAttributeDB.u2WlanProfileId;
        if (u2WlanProfileId == u2WlanProfileId1)
        {
            u4RadioIfIndex = pwssWlanDB->WssWlanAttributeDB.u4RadioIfIndex;
            /*Storing all client mac's */
            MEMCPY (au1NewStaMacArray[u4StaCount],
                    gaWssClientSummary[u1Index].staMacAddr, sizeof (tMacAddr));
            u4StaCount++;
        }
    }
    if (u1EntryFlag == gu4ClientWalkIndex)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_SUCCESS;
    }
    if (u4StaCount == 0)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_SUCCESS;
    }
    /* If the  Newly configured value is greater than connected station count,
     * we dont send disassoc */
    if (u4FsWlanMaxClientCount > u4StaCount)
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
        return OSIX_SUCCESS;
    }
    /*Need to Sort Array on the basis of connected time */
    for (u1CurrIndex = 0; u1CurrIndex < u4StaCount; ++u1CurrIndex)
    {
        if (nmhGetFsWlanClientStatsAssocTime
            (au1NewStaMacArray[u1CurrIndex],
             &u4AssocTimeCurrent) == SNMP_FAILURE)
        {
            continue;
        }
        for (u1NextIndex = u1CurrIndex + 1; u1NextIndex < u4StaCount;
             ++u1NextIndex)
        {
            if (nmhGetFsWlanClientStatsAssocTime
                (au1NewStaMacArray[u1NextIndex],
                 &u4AssocTimeNext) == SNMP_FAILURE)
            {
                continue;
            }
            if (u4AssocTimeCurrent > u4AssocTimeNext)
            {
                MEMSET (u1TempMacArray, 0, sizeof (tMacAddr));
                MEMCPY (u1TempMacArray, au1NewStaMacArray[u1CurrIndex],
                        sizeof (tMacAddr));
                u4Temp = u4AssocTimeCurrent;
                u4AssocTimeCurrent = u4AssocTimeNext;
                u4AssocTimeNext = u4Temp;
                MEMCPY (au1NewStaMacArray[u1CurrIndex],
                        au1NewStaMacArray[u1NextIndex], sizeof (tMacAddr));
                MEMCPY (au1NewStaMacArray[u1NextIndex], u1TempMacArray,
                        sizeof (tMacAddr));
            }
        }
    }
    /*Sending DeAuth */
    for (u1Loop = u4FsWlanMaxClientCount; u1Loop < u4StaCount; u1Loop++)
    {
        WssCfgSendDeAuthForRadio (au1NewStaMacArray[u1Loop], u4RadioIfIndex);
    }
    UtlShMemFreeWlanDbBuf ((UINT1 *) pwssWlanDB);
#else
    UNUSED_PARAM (u2WlanProfileId);
    UNUSED_PARAM (u4FsWlanMaxClientCount);
#endif
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgGetWebAuthRedirectFileName
 Input       :  pwssWlanDB
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetWebAuthRedirectFileName (tWssWlanDB * pwssWlanDB)
{
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                  pwssWlanDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetWebAuthRedirectFileName
 Input       :  pwssWlanDB
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetWebAuthRedirectFileName (tWssWlanDB * pwssWlanDB)
{
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY,
                                  pwssWlanDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetLoginAuthentication 
 Input       :  pwssWlanDB
 Description :  This routine gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetLoginAuthentication (tWssWlanDB * pwssWlanDB)
{
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                  pwssWlanDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetLoginAuthentication 
 Input       :  pwssWlanDB
 Description :  This routine sets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetLoginAuthentication (tWssWlanDB * pwssWlanDB)
{
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_SET_IFINDEX_ENTRY,
                                  pwssWlanDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
  * Function    :  WssBatchConfigShowRunningConfig 
  * Input       :  CliHandle
  * Descritpion :  This Routine shows the configured CLI commands of the
  *                AP configuratio present in WLC.
  * Output      :  CLI commands
  * Returns     :  CLI_SUCCESS or CLI_FAILURE
***************************************************************************/
INT4
WssBatchConfigShowRunningConfig (tCliHandle CliHandle)
{
#ifdef WLC_WANTED
    UINT4               u4WtpProfileId = 0;
    UINT4               u4WtpNextProfileId = 0;
    UINT4               u4WlanRadioIfIpAddr = 0;
    UINT4               u4WlanRadioIfIpSubnetMask = 0;
    INT4                i4ScalarStatus = 0;
    INT4                i4DhcpPoolId = 0;
    INT4                i4NextDhcpPoolId = 0;
    INT4                i4NatNextPoolId = 0;
    INT4                i4WtpIfMainWanType = 0;
    INT4                i4NatPoolId = 0;
    INT4                i4RadioNextIfIndex = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4hours = 0;
    INT4                i4minutes = 0;
    INT4                i4Tmphours = 0;
    INT4                i4Tmpminutes = 0;
    INT4                i4days = 0;
    INT4                i4RetValue = 0;
    INT4                i4DhcpSrvSubnetLeaseTime = 0;
    UINT4               u4DhcpSrvSubnetMask = 0;
    UINT4               u4DhcpSrvSubnetSubnet = 0;
    UINT4               u4DhcpSrvSubnetStartIpAddress = 0;
    UINT4               u4DhcpSrvSubnetEndIpAddress = 0;
    UINT4               u4DhcpSrvDefaultRouter = 0;
    UINT4               u4DhcpSrvDnsServerIp = 0;
    UINT4               u4IpAddr = 0;
    UINT1               au1WtpName[256];
    UINT4               u4WtpIpRouteConfigSubnet = 0;
    UINT4               u4WtpNextIpRouteConfigSubnet = 0;
    UINT4               u4WtpIpRouteConfigNetMask = 0;
    UINT4               u4WtpNextIpRouteConfigNetMask = 0;
    UINT4               u4WtpIpRouteConfigGateway = 0;
    UINT4               u4WtpNextIpRouteConfigGateway = 0;
    UINT4               u4WtpIfIndex = 0;
    UINT4               u4WtpNextIfIndex = 0;
    UINT4               u4SubnetMask = 0;
    INT4                i4IfPhyPort = 0;
    INT4                i4VlanId = 0;
    INT4                i4IfAdminStatus = 0;
    INT4                i4IfNwType = 0;
    INT4                i4RowStatus = 0;
    tSNMP_OCTET_STRING_TYPE WtpProfileName;
    tRadioIfGetDB       RadioIfGetDB;
    CHR1               *pu1IpAddr = NULL;
    CHR1               *pu1NetMask = NULL;
    CHR1               *pu1Gateway = NULL;
    UINT1               au1IpAddr[WSSCFG_MAX_IP_LENGTH];
    UINT1               au1NetMask[WSSCFG_MAX_IP_LENGTH];
    UINT1               au1Gateway[WSSCFG_MAX_IP_LENGTH];
    CHR1               *pu1StartIp = NULL;
    CHR1               *pu1EndIp = NULL;
    CHR1               *pu1DefRouter = NULL;
    CHR1               *pu1DnsServer = NULL;
    CHR1               *pu1IpNetMask = NULL;
    UINT1               au1StartIp[WSSCFG_MAX_IP_LENGTH];
    UINT1               au1EndIp[WSSCFG_MAX_IP_LENGTH];
    UINT1               au1DefRouter[WSSCFG_MAX_IP_LENGTH];
    UINT1               au1DnsServer[WSSCFG_MAX_IP_LENGTH];
    UINT1               au1IpNetMask[WSSCFG_MAX_IP_LENGTH];
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1WtpName, 0, 256);
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (au1IpAddr, 0, WSSCFG_MAX_IP_LENGTH);
    MEMSET (au1NetMask, 0, WSSCFG_MAX_IP_LENGTH);
    MEMSET (au1Gateway, 0, WSSCFG_MAX_IP_LENGTH);
    MEMSET (au1StartIp, 0, WSSCFG_MAX_IP_LENGTH);
    MEMSET (au1EndIp, 0, WSSCFG_MAX_IP_LENGTH);
    MEMSET (au1DefRouter, 0, WSSCFG_MAX_IP_LENGTH);
    MEMSET (au1DnsServer, 0, WSSCFG_MAX_IP_LENGTH);
    MEMSET (au1IpNetMask, 0, WSSCFG_MAX_IP_LENGTH);

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));

    WtpProfileName.pu1_OctetList = au1WtpName;

    nmhGetFsWlanDhcpServerStatus (&i4ScalarStatus);
    if (i4ScalarStatus == 0)
    {
        CliPrintf (CliHandle, "ap dhcp server disable\r\n");
    }
    nmhGetFsWlanDhcpRelayStatus (&i4ScalarStatus);
    if (i4ScalarStatus == 1)
    {
        CliPrintf (CliHandle, "ap dhcp relay enable\r\n");
    }
    nmhGetFsWlanDhcpNextSrvIpAddr (&u4IpAddr);
    if (u4IpAddr != 0)
    {
        CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4IpAddr);
        CliPrintf (CliHandle, "ap ip dhcp next-server %s\r\n", pu1IpAddr);
    }
    nmhGetFsWlanFirewallStatus (&i4ScalarStatus);
    if (i4ScalarStatus == 1)
    {
        CliPrintf (CliHandle, "ap firewall status enable\r\n");
    }
    nmhGetFsWlanDNSServerIpAddr (&u4IpAddr);
    if (u4IpAddr != 0)
    {
        CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4IpAddr);
        CliPrintf (CliHandle, "ap dns-server %s\r\n", pu1IpAddr);
    }
    nmhGetFsWlanDefaultRouterIpAddr (&u4IpAddr);
    if (u4IpAddr != 0)
    {
        CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4IpAddr);
        CliPrintf (CliHandle, "ap default-router %s\r\n", pu1IpAddr);
    }
    if (nmhGetFirstIndexFsWtpFirewallConfigTable (&u4WtpNextProfileId)
        == SNMP_SUCCESS)
    {
        do
        {
            u4WtpProfileId = u4WtpNextProfileId;
            if (nmhGetFsWtpFirewallStatus (u4WtpNextProfileId, &i4RetValue) ==
                SNMP_SUCCESS)
            {
                if (nmhGetCapwapBaseWtpProfileName
                    (u4WtpNextProfileId, &WtpProfileName) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "ap firewall status enable %s\r\n",
                               WtpProfileName.pu1_OctetList);
                }
            }

        }
        while (nmhGetNextIndexFsWtpFirewallConfigTable (u4WtpProfileId,
                                                        &u4WtpNextProfileId) ==
               SNMP_SUCCESS);
    }
    if (nmhGetFirstIndexFsWtpDhcpConfigTable (&u4WtpNextProfileId)
        == SNMP_SUCCESS)
    {
        do
        {
            if (nmhGetFsWtpDhcpServerStatus (u4WtpNextProfileId,
                                             &i4ScalarStatus) == SNMP_SUCCESS)
            {
                if (i4ScalarStatus == 0)
                {
                    if (nmhGetCapwapBaseWtpProfileName
                        (u4WtpNextProfileId, &WtpProfileName) == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "ap dhcp server disable %s\r\n",
                                   WtpProfileName.pu1_OctetList);
                    }
                }
            }
            if (nmhGetFsWtpDhcpRelayStatus (u4WtpNextProfileId,
                                            &i4ScalarStatus) == SNMP_SUCCESS)
            {
                if (i4ScalarStatus == 1)
                {
                    if (nmhGetCapwapBaseWtpProfileName
                        (u4WtpNextProfileId, &WtpProfileName) == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "ap dhcp relay enable %s\r\n",
                                   WtpProfileName.pu1_OctetList);
                    }
                }
            }
            CliPrintf (CliHandle, "\r\n");
            u4WtpProfileId = u4WtpNextProfileId;
        }
        while (nmhGetNextIndexFsWtpDhcpConfigTable (u4WtpProfileId,
                                                    &u4WtpNextProfileId) ==
               SNMP_SUCCESS);

    }
    if (nmhGetFirstIndexFsWtpIpRouteConfigTable (&u4WtpNextProfileId,
                                                 &u4WtpNextIpRouteConfigSubnet,
                                                 &u4WtpNextIpRouteConfigNetMask,
                                                 &u4WtpNextIpRouteConfigGateway)
        == SNMP_SUCCESS)
    {
        while ((u4WtpIpRouteConfigSubnet != u4WtpNextIpRouteConfigSubnet) ||
               (u4WtpIpRouteConfigNetMask != u4WtpNextIpRouteConfigNetMask) ||
               (u4WtpIpRouteConfigGateway != u4WtpNextIpRouteConfigGateway) ||
               (u4WtpProfileId != u4WtpNextProfileId))
        {
            if (nmhGetFsWtpIpRouteConfigRowStatus (u4WtpNextProfileId,
                                                   u4WtpNextIpRouteConfigSubnet,
                                                   u4WtpNextIpRouteConfigNetMask,
                                                   u4WtpNextIpRouteConfigGateway,
                                                   &i4RetValue) == SNMP_SUCCESS)
            {
                if (nmhGetCapwapBaseWtpProfileName
                    (u4WtpNextProfileId, &WtpProfileName) == SNMP_SUCCESS)
                {
                    MEMSET (au1IpAddr, 0, WSSCFG_MAX_IP_LENGTH);
                    MEMSET (au1NetMask, 0, WSSCFG_MAX_IP_LENGTH);
                    MEMSET (au1Gateway, 0, WSSCFG_MAX_IP_LENGTH);
                    CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr,
                                               u4WtpNextIpRouteConfigSubnet);
                    MEMCPY (au1IpAddr, pu1IpAddr, STRLEN (pu1IpAddr - 1));
                    CLI_CONVERT_IPADDR_TO_STR (pu1NetMask,
                                               u4WtpNextIpRouteConfigNetMask);
                    MEMCPY (au1IpNetMask, pu1NetMask, STRLEN (pu1NetMask - 1));
                    CLI_CONVERT_IPADDR_TO_STR (pu1Gateway,
                                               u4WtpNextIpRouteConfigGateway);
                    MEMCPY (au1Gateway, pu1Gateway, STRLEN (pu1Gateway - 1));
                    CliPrintf (CliHandle, "ap ip route %s %s %s %s\r\n",
                               au1IpAddr, au1IpNetMask, au1Gateway,
                               WtpProfileName.pu1_OctetList);
                    CliPrintf (CliHandle, "\r\n");
                }

            }
            u4WtpProfileId = u4WtpNextProfileId;
            u4WtpIpRouteConfigSubnet = u4WtpNextIpRouteConfigSubnet;
            u4WtpIpRouteConfigNetMask = u4WtpNextIpRouteConfigNetMask;
            u4WtpIpRouteConfigGateway = u4WtpNextIpRouteConfigGateway;
            if (nmhGetNextIndexFsWtpIpRouteConfigTable (u4WtpProfileId,
                                                        &u4WtpNextProfileId,
                                                        u4WtpIpRouteConfigSubnet,
                                                        &u4WtpNextIpRouteConfigSubnet,
                                                        u4WtpIpRouteConfigNetMask,
                                                        &u4WtpNextIpRouteConfigNetMask,
                                                        u4WtpIpRouteConfigGateway,
                                                        &u4WtpNextIpRouteConfigGateway)
                == SNMP_SUCCESS)
            {
                continue;
            }
        }
    }
    if (nmhGetFirstIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable
        (&u4WtpNextProfileId, &i4NextDhcpPoolId) == SNMP_SUCCESS)
    {
        while ((u4WtpProfileId != u4WtpNextProfileId) ||
               (i4DhcpPoolId != i4NextDhcpPoolId))
        {

            if (nmhGetCapwapBaseWtpProfileName
                (u4WtpNextProfileId, &WtpProfileName) != SNMP_SUCCESS)
            {
            }
            if (nmhGetFsDot11WtpDhcpSrvSubnetSubnet
                (u4WtpNextProfileId, i4NextDhcpPoolId,
                 &u4DhcpSrvSubnetSubnet) == SNMP_SUCCESS)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4DhcpSrvSubnetSubnet);
            }
            if (nmhGetFsDot11WtpDhcpSrvSubnetMask
                (u4WtpNextProfileId, i4NextDhcpPoolId,
                 &u4DhcpSrvSubnetMask) == SNMP_SUCCESS)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1NetMask, u4DhcpSrvSubnetMask);
                MEMCPY (au1NetMask, pu1NetMask, STRLEN (pu1NetMask - 1));
            }
            if (nmhGetFsDot11WtpDhcpSrvSubnetStartIpAddress
                (u4WtpNextProfileId, i4NextDhcpPoolId,
                 &u4DhcpSrvSubnetStartIpAddress) == SNMP_SUCCESS)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1StartIp,
                                           u4DhcpSrvSubnetStartIpAddress);
                MEMCPY (au1StartIp, pu1StartIp, STRLEN (pu1StartIp - 1));
            }
            if (nmhGetFsDot11WtpDhcpSrvSubnetEndIpAddress
                (u4WtpNextProfileId, i4NextDhcpPoolId,
                 &u4DhcpSrvSubnetEndIpAddress) == SNMP_SUCCESS)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1EndIp,
                                           u4DhcpSrvSubnetEndIpAddress);
                MEMCPY (au1EndIp, pu1EndIp, STRLEN (pu1EndIp - 1));
            }
            if (nmhGetFsDot11WtpDhcpSrvDefaultRouter
                (u4WtpNextProfileId, i4NextDhcpPoolId,
                 &u4DhcpSrvDefaultRouter) == SNMP_SUCCESS)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1DefRouter,
                                           u4DhcpSrvDefaultRouter);
                MEMCPY (au1DefRouter, pu1DefRouter, STRLEN (pu1DefRouter - 1));
            }
            if (nmhGetFsDot11WtpDhcpSrvDnsServerIp
                (u4WtpNextProfileId, i4NextDhcpPoolId,
                 &u4DhcpSrvDnsServerIp) == SNMP_SUCCESS)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1DnsServer, u4DhcpSrvDnsServerIp);
                MEMCPY (au1DnsServer, pu1DnsServer, STRLEN (pu1DnsServer - 1));
            }
            nmhGetFsDot11WtpDhcpSrvSubnetLeaseTime (u4WtpNextProfileId,
                                                    i4NextDhcpPoolId,
                                                    &i4DhcpSrvSubnetLeaseTime);
            CliPrintf (CliHandle, "ap ip dhcp pool %d\r\n", i4NextDhcpPoolId);
            if ((u4DhcpSrvSubnetStartIpAddress != 0) &&
                (u4DhcpSrvSubnetEndIpAddress != 0) &&
                (u4DhcpSrvSubnetMask != 0))
            {
                CliPrintf (CliHandle, "ap network %s %s %s %s\r\n", au1StartIp,
                           au1NetMask, au1EndIp, WtpProfileName.pu1_OctetList);
            }

            i4Tmpminutes = (i4DhcpSrvSubnetLeaseTime / 60);
            i4Tmphours = i4Tmpminutes / 60;
            i4days = i4Tmphours / 24;
            i4hours = i4Tmphours - (i4days * 24);
            i4minutes = i4Tmpminutes - (i4Tmphours * 60);
            CliPrintf (CliHandle, "ap lease %d %d %d %s\r\n", i4days,
                       i4hours, i4minutes, WtpProfileName.pu1_OctetList);
            if (u4DhcpSrvDefaultRouter != 0)
            {
                CliPrintf (CliHandle, "ap option 3 ip %s %s\r\n", au1DefRouter,
                           WtpProfileName.pu1_OctetList);
            }
            if (u4DhcpSrvDnsServerIp != 0)
            {
                CliPrintf (CliHandle, "ap option 6 ip %s %s\r\n", au1DnsServer,
                           WtpProfileName.pu1_OctetList);
            }
            CliPrintf (CliHandle, "\r\n");
            u4WtpProfileId = u4WtpNextProfileId;
            i4DhcpPoolId = i4NextDhcpPoolId;
            if (nmhGetNextIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable
                (u4WtpProfileId, &u4WtpNextProfileId, i4DhcpPoolId,
                 &i4NextDhcpPoolId) == SNMP_SUCCESS)
            {
                continue;
            }
        }
    }

    if (nmhGetFirstIndexFsWtpNATTable (&u4WtpNextProfileId, &i4NatNextPoolId) ==
        SNMP_SUCCESS)
    {
        do
        {
            if (nmhGetCapwapBaseWtpProfileName
                (u4WtpNextProfileId, &WtpProfileName) != SNMP_SUCCESS)
            {
            }
            if (nmhGetFsWtpIfMainWanType (u4WtpNextProfileId,
                                          i4NatNextPoolId,
                                          &i4WtpIfMainWanType) == SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "ap ip nat pool %d\r\n", i4NatNextPoolId);
                if (i4WtpIfMainWanType == 1)
                {
                    CliPrintf (CliHandle, "no ap network-type wan %s \r\n",
                               WtpProfileName.pu1_OctetList);
                }
                else if (i4WtpIfMainWanType == 2)
                {
                    CliPrintf (CliHandle, "ap network-type wan %s\r\n",
                               WtpProfileName.pu1_OctetList);
                }
            }
            u4WtpProfileId = u4WtpNextProfileId;
            i4NatPoolId = i4NatNextPoolId;
        }
        while (nmhGetNextIndexFsWtpNATTable
               (u4WtpProfileId, &u4WtpNextProfileId, i4NatPoolId,
                &i4NatNextPoolId) == SNMP_SUCCESS);
    }
    i4RadioNextIfIndex = 0;
    u4WtpNextProfileId = 0;
    u4WtpProfileId = 0;
    i4RadioIfIndex = 0;
    if (nmhGetFirstIndexFsWlanRadioIfTable
        (&i4RadioNextIfIndex, &u4WtpNextProfileId) == SNMP_SUCCESS)
    {
        do
        {
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4)
                i4RadioNextIfIndex;
            RadioIfGetDB.RadioIfIsGetAllDB.bRadioId = OSIX_TRUE;
            RadioIfGetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB) !=
                OSIX_SUCCESS)
            {
            }
            MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
            pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
            pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
            pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
                RadioIfGetDB.RadioIfGetAllDB.u2WtpInternalId;
            if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
                != OSIX_SUCCESS)
            {
                WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
                return OSIX_FAILURE;
            }

            u4WtpProfileId = u4WtpNextProfileId;
            i4RadioIfIndex = i4RadioNextIfIndex;
            if (nmhGetFsWlanRadioIfIpAddr
                (i4RadioNextIfIndex, u4WtpNextProfileId,
                 &u4WlanRadioIfIpAddr) == SNMP_SUCCESS)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4WlanRadioIfIpAddr);

                MEMCPY (au1IpAddr, pu1IpAddr, STRLEN (pu1IpAddr - 1));

            }
            if (nmhGetFsWlanRadioIfIpSubnetMask
                (i4RadioNextIfIndex, u4WtpNextProfileId,
                 &u4WlanRadioIfIpSubnetMask) == SNMP_SUCCESS)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1IpNetMask,
                                           u4WlanRadioIfIpSubnetMask);
                MEMCPY (au1IpNetMask, pu1IpNetMask, STRLEN (pu1IpNetMask - 1));

            }
            if ((u4WlanRadioIfIpAddr != 0) && (u4WlanRadioIfIpSubnetMask != 0))
            {
                CliPrintf (CliHandle,
                           "ap wlan ip-address %s %s %s dot11radio %d wlan %d\r\n",
                           au1IpAddr, au1IpNetMask,
                           pWssIfCapwapDB->CapwapGetDB.au1ProfileName,
                           RadioIfGetDB.RadioIfGetAllDB.u1RadioId,
                           u4WtpNextProfileId);
            }
        }
        while (nmhGetNextIndexFsWlanRadioIfTable
               (i4RadioIfIndex, &i4RadioNextIfIndex, u4WtpProfileId,
                &u4WtpNextProfileId) == SNMP_SUCCESS);
    }

    /* Show L3 SUB Interface configuration */
    if (nmhGetFirstIndexFsWtpIfMainTable (&u4WtpNextProfileId,
                                          (INT4 *) &u4WtpNextIfIndex) ==
        SNMP_SUCCESS)
    {
        do
        {
            u4WtpProfileId = u4WtpNextProfileId;
            u4WtpIfIndex = u4WtpNextIfIndex;

            if (nmhGetCapwapBaseWtpProfileName
                (u4WtpNextProfileId, &WtpProfileName) == SNMP_SUCCESS)
            {
                if ((nmhGetFsWtpIfMainRowStatus
                     (u4WtpNextProfileId, u4WtpNextIfIndex,
                      &i4RowStatus) == SNMP_SUCCESS) && (i4RowStatus == ACTIVE))
                {
                    nmhGetFsWtpIfMainAdminStatus (u4WtpNextProfileId,
                                                  u4WtpNextIfIndex,
                                                  &i4IfAdminStatus);
                    nmhGetFsWtpIfMainNetworkType (u4WtpNextProfileId,
                                                  u4WtpNextIfIndex,
                                                  &i4IfNwType);
                    nmhGetFsWtpIfMainPhyPort (u4WtpNextProfileId,
                                              u4WtpNextIfIndex, &i4IfPhyPort);
                    nmhGetFsWtpIfMainEncapDot1qVlanId (u4WtpNextProfileId,
                                                       u4WtpNextIfIndex,
                                                       &i4VlanId);
                    nmhGetFsWtpIfIpAddr (u4WtpNextProfileId, u4WtpNextIfIndex,
                                         &u4IpAddr);
                    nmhGetFsWtpIfIpSubnetMask (u4WtpNextProfileId,
                                               u4WtpNextIfIndex, &u4SubnetMask);

                    CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4IpAddr);
                    CLI_CONVERT_IPADDR_TO_STR (pu1NetMask, u4SubnetMask);
                    CliPrintf (CliHandle, "ap l3sub phyport %d vlan %d %s\r\n",
                               i4IfPhyPort, i4VlanId,
                               WtpProfileName.pu1_OctetList);
                    CliPrintf (CliHandle, "ap l3sub ip-address %s %s %s\r\n",
                               pu1IpAddr, pu1NetMask,
                               WtpProfileName.pu1_OctetList);

                    if (i4IfAdminStatus == AP_IF_UP)
                    {
                        CliPrintf (CliHandle, "no ap l3sub shutdown %s\r\n",
                                   WtpProfileName.pu1_OctetList);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "ap l3sub shutdown %s \r\n",
                                   WtpProfileName.pu1_OctetList);
                    }

                    if (i4IfNwType == NETWORK_TYPE_WAN)
                    {

                        CliPrintf (CliHandle,
                                   "ap l3sub network-type wan %s\r\n",
                                   WtpProfileName.pu1_OctetList);
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "no ap l3sub network-type wan %s\r\n",
                                   WtpProfileName.pu1_OctetList);
                    }
                }
            }
        }
        while (nmhGetNextIndexFsWtpIfMainTable (u4WtpProfileId,
                                                &u4WtpNextProfileId,
                                                (INT4) u4WtpIfIndex,
                                                (INT4 *) &u4WtpNextIfIndex) ==
               SNMP_SUCCESS);
    }

    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
#else
    UNUSED_PARAM (CliHandle);
#endif
    return CLI_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : ApFwlParseIpAddr                                 */
/*                                                                          */
/*    Description        : Gets the string containing address and separates */
/*                         them as start and end IP.                        */
/*                                                                          */
/*    Input(s)           : au1Addr             -- Start and End addr        */
/*                                               -- or Addr and Mask          */
/*                                                                          */
/*    Output(s)          : pu4StartAddr  -- address seperated from string   */
/*                         pu4EndAddr    -- address seperated from string   */
/*                                                                          */
/*    Returns            : OSIX_SUCCESS if parsing is done else OSIX_FAILURE  */
/*                                                                          */
/****************************************************************************/

PUBLIC INT1
ApFwlParseIpAddr (UINT1 au1Addr[WSSCFG_MAX_ADDR_LEN],
                  UINT4 *pu4StartAddress, UINT4 *pu4EndAddress)
{
    UINT1               au1Operator[WSSCFG_MAX_OPERATOR_LEN] = { 0 };
    UINT1               au1DummyIpAddr[WSSCFG_MAX_ADDR_LEN] = { 0 };
    UINT4               u4ByteAddr = 0;
    UINT4               u4Addr = 0;
    UINT4               u4Mask = 0;
    UINT2               u2AddrLen = 0;
    INT1                i1Status = OSIX_SUCCESS;
    UINT1               u1Count = 0;
    UINT1               u1StringIndex = 0;
    UINT1               u1MultFactor = WSSCFG_TEN;
    UINT1               u1OperatorIndex = 0;
    UINT1               u1DummyIpIndex = 0;

    MEMSET (au1Operator, 0, sizeof (au1Operator));
    MEMSET (au1DummyIpAddr, 0, sizeof (au1DummyIpAddr));

    *pu4StartAddress = 0;
    *pu4EndAddress = 0;
    u2AddrLen = (UINT2) (STRLEN ((INT1 *) au1Addr));

    UNUSED_PARAM (u2AddrLen);
    /* extract the operators >, <, =, >=, <= */
    while (((((au1Addr[u1StringIndex] - WSSCFG_ASCII_VALUE) < 0) ||
             ((au1Addr[u1StringIndex] - WSSCFG_ASCII_VALUE) >
              WSSCFG_CHECK_VAL_9))
            && (au1Addr[u1StringIndex] != WSSCFG_END_OF_STRING))
           && (u1StringIndex < WSSCFG_MAX_ADDR_LEN - 1))
    {
        if (u1StringIndex < WSSCFG_MAX_OPERATOR_LEN - 1)
        {
            au1Operator[u1StringIndex] = au1Addr[u1StringIndex];
            u1StringIndex++;
            au1Operator[u1StringIndex] = WSSCFG_END_OF_STRING;
        }
    }
    u1OperatorIndex = u1StringIndex;

    /* extract the IP address */
    while (((((au1Addr[u1StringIndex] - WSSCFG_ASCII_VALUE) >= 0) &&
             ((au1Addr[u1StringIndex] - WSSCFG_ASCII_VALUE) <=
              WSSCFG_CHECK_VAL_9)) || (au1Addr[u1StringIndex] == '.'))
           && (u1StringIndex < WSSCFG_MAX_ADDR_LEN - 1))
    {
        au1DummyIpAddr[u1DummyIpIndex] = au1Addr[u1StringIndex];
        u1StringIndex++;
        u1DummyIpIndex++;
        au1DummyIpAddr[u1DummyIpIndex] = WSSCFG_END_OF_STRING;
    }

    /* IP address string to UINT4 conversion, big endian notation */

    u4Addr = INET_ADDR (au1DummyIpAddr);

    MEMSET (au1DummyIpAddr, 0, sizeof (au1DummyIpAddr));
    u1DummyIpIndex = 0;

    if (u4Addr == WSSCFG_BROADCAST_ADDR)
    {
        i1Status = OSIX_FAILURE;
    }
    else
    {
        /* Check whether the next character to IP address
         * is mask or operator */

        /* In case of mask */

        if ((au1Addr[u1StringIndex] == '/') &&
            (u1StringIndex < WSSCFG_MAX_ADDR_LEN - 1))
        {
            u1StringIndex++;

            while ((au1Addr[u1StringIndex] != '\0') &&
                   (u1StringIndex < WSSCFG_MAX_ADDR_LEN - 1))
            {
                if ((((au1Addr[u1StringIndex] - WSSCFG_ASCII_VALUE) >= 0) &&
                     ((au1Addr[u1StringIndex] - WSSCFG_ASCII_VALUE) <=
                      WSSCFG_CHECK_VAL_9)))
                {
                    u4ByteAddr = (u4ByteAddr * u1MultFactor) +
                        ((UINT4) (au1Addr[u1StringIndex] - WSSCFG_ASCII_VALUE));
                    u1StringIndex++;
                    u1Count++;
                }
                else
                {
                    return OSIX_FAILURE;
                }
            }                    /* end of while */

            /* The MASK Value can be only upto 32 */

            if ((u1Count < WSSCFG_MIN_MASK_VAL)
                && (u4ByteAddr <= WSSCFG_MAX_MASK_VAL))
            {
                for (u1StringIndex = 0; u1StringIndex < u4ByteAddr;
                     u1StringIndex++)
                {
                    u4Mask = ((u4Mask >> 1) | WSSCFG_32ND_BIT_SET);
                }

            }
            else
            {
                i1Status = OSIX_FAILURE;
            }
        }                        /* end of if au1Addr[u1StringIndex] == '/' */

        /* Incase of Operator */

        /* extract the operators <, <=, and second IP address */

        else if (((au1Addr[u1StringIndex] == '<')
                  || (au1Addr[u1StringIndex] == '>'))
                 && ((u1StringIndex < WSSCFG_MAX_ADDR_LEN - 1)
                     && (u1OperatorIndex < WSSCFG_MAX_OPERATOR_LEN - 1)))
        {
            au1Operator[u1OperatorIndex] = au1Addr[u1StringIndex];
            u1StringIndex++;
            u1OperatorIndex++;
            if ((au1Addr[u1StringIndex] == '=')
                && ((u1StringIndex < WSSCFG_MAX_ADDR_LEN - 1) &&
                    (u1OperatorIndex < WSSCFG_MAX_OPERATOR_LEN - 1)))

            {
                au1Operator[u1OperatorIndex] = au1Addr[u1StringIndex];
                u1StringIndex++;
                u1OperatorIndex++;
                au1Operator[u1OperatorIndex] = WSSCFG_END_OF_STRING;
            }
            else
            {
                if (u1OperatorIndex < WSSCFG_MAX_OPERATOR_LEN - 1)
                {
                    au1Operator[u1OperatorIndex] = WSSCFG_END_OF_STRING;
                }
            }

            /* extract the second IP address */

            while (((((au1Addr[u1StringIndex] - WSSCFG_ASCII_VALUE) >= 0) &&
                     ((au1Addr[u1StringIndex] - WSSCFG_ASCII_VALUE) <=
                      WSSCFG_CHECK_VAL_9)) ||
                    (au1Addr[u1StringIndex] == '.')) &&
                   (u1StringIndex < WSSCFG_MAX_ADDR_LEN - 1) &&
                   (u1DummyIpIndex < WSSCFG_MAX_ADDR_LEN - 1))

            {
                au1DummyIpAddr[u1DummyIpIndex] = au1Addr[u1StringIndex];
                u1StringIndex++;
                u1DummyIpIndex++;
                au1DummyIpAddr[u1DummyIpIndex] = WSSCFG_END_OF_STRING;
            }

            *pu4EndAddress = OSIX_HTONL (INET_ADDR (au1DummyIpAddr));

            if (*pu4EndAddress == WSSCFG_BROADCAST_ADDR)
            {
                i1Status = OSIX_FAILURE;
            }
        }                        /* end of else if au1Addr[u1StringIndex] == '<' */
        else
        {
            i1Status = OSIX_FAILURE;
        }

    }                            /* end of If pu4Addr != OSIX_FAILURE */

    u1OperatorIndex = 0;

    switch (au1Operator[u1OperatorIndex])
    {
        case '>':
            u1OperatorIndex++;
            if (au1Operator[u1OperatorIndex] == '=')
            {
                *pu4StartAddress = u4Addr;
                u1OperatorIndex++;

                /* In case of Range specified */

                if (au1Operator[u1OperatorIndex] == '<')
                {
                    u1OperatorIndex++;
                    if (au1Operator[u1OperatorIndex] == '\0')
                    {
                        *pu4EndAddress = *pu4EndAddress - 1;
                    }
                }

                /* In case of mask specified */

                else if (au1Operator[u1OperatorIndex] == '\0')
                {

                    if ((u4ByteAddr == 0) ||
                        (u4ByteAddr == WSSCFG_MAX_MASK_VAL))
                    {
                        /* range cannot carry a mask of 0 or 32 */
                        i1Status = OSIX_FAILURE;
                    }
                    else
                    {
                        *pu4EndAddress = ((u4Addr & u4Mask) | (~u4Mask)) - 1;
                    }
                }
                else
                {
                    i1Status = OSIX_FAILURE;
                }
            }
            else
            {
                *pu4StartAddress = (u4Addr) + 1;

                /* Incase of Range Specified */

                if (au1Operator[u1OperatorIndex] == '<')
                {
                    u1OperatorIndex++;
                    if (au1Operator[u1OperatorIndex] == '\0')
                    {
                        *pu4EndAddress = *pu4EndAddress - 1;
                    }
                }

                /* Incase of Mask specified */

                else if (au1Operator[u1OperatorIndex] == '\0')
                {

                    if ((u4ByteAddr == 0) ||
                        (u4ByteAddr == WSSCFG_MAX_MASK_VAL))
                    {
                        /* range cannot carry a mask of 0 or 32 */
                        i1Status = OSIX_FAILURE;
                    }
                    else
                    {
                        *pu4EndAddress = ((u4Addr & u4Mask) | (~u4Mask)) - 1;
                    }
                }
                else
                {
                    i1Status = OSIX_FAILURE;
                }

            }
            break;
        case '<':
            u1OperatorIndex++;
            if (au1Operator[u1OperatorIndex] == '=')
            {
                u1OperatorIndex++;

                /* In case of Range specified */

                if (au1Operator[u1OperatorIndex] == '>')
                {
                    u1OperatorIndex++;
                    if (au1Operator[u1OperatorIndex] == '=')
                    {
                        /* The pu4EndAddress contains the 
                         * parse result of second IP and the u4Addr contains
                         * the parse result of First IP, in range scenario,
                         * considering the operators, the second IP parsed
                         * will the Start IP Address and the first IP Parsed
                         * will be the end IP Address, hence the expressions
                         * below is a misnormal */

                        *pu4StartAddress = *pu4EndAddress;
                        *pu4EndAddress = u4Addr;
                    }
                    else if (au1Operator[u1OperatorIndex] == '\0')
                    {
                        /* The pu4EndAddress contains the 
                         * parse result of second IP and the u4Addr contains
                         * the parse result of First IP, in range scenario,
                         * considering the operators, the second IP parsed
                         * will the Start IP Address and the first IP Parsed
                         * will be the end IP Address, hence the expressions
                         * below is a misnormal */

                        *pu4StartAddress = *pu4EndAddress + 1;
                        *pu4EndAddress = u4Addr;
                    }
                    else
                    {
                        i1Status = OSIX_FAILURE;
                    }
                }

                /* In case of mask specified */

                else if (au1Operator[u1OperatorIndex] == '\0')
                {

                    if ((u4ByteAddr == 0) ||
                        (u4ByteAddr == WSSCFG_MAX_MASK_VAL))
                    {
                        /* range cannot carry a mask of 0 or 32 */
                        i1Status = OSIX_FAILURE;
                    }
                    else
                    {
                        *pu4StartAddress = (u4Addr & u4Mask) + 1;
                        *pu4EndAddress = u4Addr;
                    }
                }
                else
                {
                    i1Status = OSIX_FAILURE;
                }
            }
            else
            {

                /* Incase of Range Specified */

                if (au1Operator[u1OperatorIndex] == '>')
                {
                    u1OperatorIndex++;
                    if (au1Operator[u1OperatorIndex] == '=')
                    {
                        /* The pu4EndAddress contains the
                         * parse result of second IP and the u4Addr contains
                         * the parse result of First IP, in range scenario,
                         * considering the operators, the second IP parsed
                         * will the Start IP Address and the first IP Parsed
                         * will be the end IP Address, hence the expressions
                         * below is a misnormal */

                        *pu4StartAddress = *pu4EndAddress;
                        *pu4EndAddress = u4Addr - 1;
                    }
                    else if (au1Operator[u1OperatorIndex] == '\0')
                    {
                        /* The pu4EndAddress contains the
                         * parse result of second IP and the u4Addr contains
                         * the parse result of First IP, in range scenario,
                         * considering the operators, the second IP parsed
                         * will the Start IP Address and the first IP Parsed
                         * will be the end IP Address, hence the expressions
                         * below is a misnormal */

                        *pu4StartAddress = *pu4EndAddress + 1;
                        *pu4EndAddress = u4Addr - 1;
                    }
                    else
                    {
                        i1Status = OSIX_FAILURE;
                    }
                }

                /* Incase of Mask specified */

                else if (au1Operator[u1OperatorIndex] == '\0')
                {

                    if ((u4ByteAddr == 0) ||
                        (u4ByteAddr == WSSCFG_MAX_MASK_VAL))
                    {
                        /* range cannot carry a mask of 0 or 32 */
                        i1Status = OSIX_FAILURE;
                    }
                    else
                    {
                        *pu4StartAddress = (u4Addr & u4Mask) + 1;
                        *pu4EndAddress = u4Addr - 1;

                    }
                }
                else
                {
                    i1Status = OSIX_FAILURE;
                }

            }
            break;
        case '\0':
            /* Fall through */
        case '=':
            u1OperatorIndex++;
            if (au1Operator[u1OperatorIndex] == '\0')
            {
                /* have same src and dest addr in case of mask 32 */

                if (u4ByteAddr == WSSCFG_MAX_MASK_VAL)
                {
                    *pu4StartAddress = *pu4EndAddress = u4Addr;
                }

                /* in case of 'any' string parsed as '0.0.0.0/0' 
                 * is given, then it means all IP addresses */

                else if ((u4ByteAddr == 0) && (u4Addr == 0))
                {
                    *pu4StartAddress = (u4Addr & u4Mask);
                    *pu4EndAddress = ((u4Addr & u4Mask) | (~u4Mask));
                }
                else
                {
                    *pu4StartAddress = (u4Addr & u4Mask) + 1;
                    *pu4EndAddress = ((u4Addr & u4Mask) | (~u4Mask)) - 1;
                }
            }
            else
            {
                i1Status = OSIX_FAILURE;
            }
            break;
        default:
            i1Status = OSIX_FAILURE;
            break;
    }                            /* End of switch au1Operator[u1OperatorIndex] */

    if ((*pu4StartAddress) > (*pu4EndAddress))
    {
        i1Status = OSIX_FAILURE;
    }

    if (i1Status == OSIX_FAILURE)
    {
        *pu4StartAddress = 0;
        *pu4EndAddress = 0;
    }

    return (i1Status);

}                                /* End of function -- FwlParseIpAddr */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : ApFwlParseMinAndMaxPort                          */
/*                                                                          */
/*    Description        : Parses the port into Min and Max Port Value      */
/*                                                                          */
/*    Input(s)           : au1Port  -- Port value with operators            */
/*                                                                          */
/*    Output(s)          : pu2MaxPort  -- Max Port value                    */
/*                         pu2MinPort  -- Min port value                    */
/*                                                                          */
/*    Returns            : SUCCESS if Values is valid, otherwise FAILURE    */
/*                                                                          */
/****************************************************************************/
PUBLIC INT1
ApFwlParseMinAndMaxPort (UINT1 au1Port[WSSCFG_MAX_PORT_LEN],
                         UINT2 *pu2MaxPort, UINT2 *pu2MinPort)
{
    UINT2               u2PortLen = 0;
    INT1                i1Status = 0;
    UINT1               au1Operator[WSSCFG_MAX_OPERATOR_LEN] = { 0 };
    UINT2               u2Index1 = 0;
    UINT2               u2Index2 = 0;
    UINT4               u4Num = 0;
    UINT4               u4Min = 0;
    INT1                i1NonDigitCnt = 0;

    /* Initialise local variables */
    i1NonDigitCnt = 0;

    u2Index1 = 0;
    u2Index2 = 0;
    u4Num = 0;
    u4Min = 0;
    i1Status = SNMP_SUCCESS;

    u2PortLen = (UINT2) STRLEN ((INT1 *) au1Port);

    /* Validating w.r.t. string length */
    if ((u2PortLen < WSSCFG_MIN_PORT_LEN) || (u2PortLen > WSSCFG_MAX_PORT_LEN))
    {
        return SNMP_FAILURE;
    }
    au1Operator[u2Index1] = au1Port[u2Index1];
    u2Index1++;
    au1Operator[u2Index1] = WSSCFG_END_OF_STRING;

    /* This function gets an input string as <operator> <Number> . It should be
     * parsed to get the Max and Min Value based on the operator.
     */
    if (((au1Port[u2Index1] - WSSCFG_ASCII_VALUE) < 0) ||
        ((au1Port[u2Index1] - WSSCFG_ASCII_VALUE) > WSSCFG_CHECK_VAL_9))
    {
        /* extract the operators >, <, =. */
        au1Operator[u2Index1] = au1Port[u2Index1];
        u2Index1++;
        au1Operator[u2Index1] = WSSCFG_END_OF_STRING;
    }

    /* then extract the operators <=, >= , != */
    u2Index1 = (UINT2) STRLEN ((INT1 *) au1Operator);
    /* Get the Port value one by one. Convert the string into an integer and
     * store it in u4Num.  */
    for (u2Index2 = u2Index1; u2Index2 < u2PortLen; u2Index2++)
    {
        if (((au1Port[u2Index2] - WSSCFG_ASCII_VALUE) >= 0) &&
            ((au1Port[u2Index2] - WSSCFG_ASCII_VALUE) <= WSSCFG_CHECK_VAL_9))
        {
            /* No. of digits check. Max no. of digits = 5. */
            if (u2Index2 >= (WSSCFG_FIVE + u2Index1))
            {
                return SNMP_FAILURE;
            }
            else
            {
                u4Num =
                    (u4Num * WSSCFG_DIV_FACTOR_10) +
                    (UINT4) (au1Port[u2Index2] - WSSCFG_ASCII_VALUE);
            }
        }
        else
        {
            if ((au1Port[u2Index1] == '-') &&
                (i1NonDigitCnt == 0) && (u2Index2))
            {
                /* Validation of Port Number */
                if ((u4Num > WSSCFG_MAX_PORT_VALUE)
                    || (u4Num < WSSCFG_MIN_PORT_VALUE))
                {
                    return SNMP_FAILURE;
                }
                else            /* First Number is valid */
                {
                    i1NonDigitCnt++;
                    /* Copy the First number into Min */
                    u4Min = u4Num;
                    /* Init Digit counter and 'u4Num' for Second number */
                    u2Index2 = 0;
                    u4Num = 0;
                }
            }
            else
            {
                return SNMP_FAILURE;
            }
        }                        /*End-of-main-Else */
    }                            /* end of for loop */

    /* Validation of Port Number */
    if ((u4Num > WSSCFG_MAX_PORT_VALUE) || (u4Num < WSSCFG_MIN_PORT_VALUE))
    {
        i1Status = SNMP_FAILURE;
        return SNMP_FAILURE;
    }
    else                        /* Number valid */
    {
        if (!(0 == i1NonDigitCnt))
        {
            if (u4Min > u4Num)
            {
                return SNMP_FAILURE;    /* Invalid (Min > Max) */
            }

            if (u4Min == u4Num)
            {
                SPRINTF ((CHR1 *) au1Port, "%u", u4Min);
            }

            /* Update the Output params */
            *pu2MinPort = (UINT2) u4Min;
            *pu2MaxPort = (UINT2) u4Num;
        }
        else
        {
            (*pu2MinPort) = (*pu2MaxPort) = (UINT2) u4Num;
        }
        u2Index1 = 0;
        u2Index2 = (UINT2) (u2Index1 + 1);

        /* based on the operator fill in the values of MAX and MIN port. if 
         * parsing can be done then return SUCCESS, else FAILURE. 
         */
        switch (au1Operator[u2Index1])
        {
            case '>':
                if (au1Operator[u2Index2] == '=')
                {
                    *pu2MinPort = (UINT2) u4Num;
                }
                else if (au1Operator[u2Index2] == '\0')
                {
                    /* Check if invalid MAX PORT (>65535) is given as input.If 
                     * so return error, because >65535 is an invalid port 
                     * number */
                    if (u4Num == WSSCFG_MAX_PORT_VALUE)
                    {
                        return SNMP_FAILURE;

                    }
                    *pu2MinPort = (UINT2) (u4Num + 1);
                }
                else
                {
                    return SNMP_FAILURE;
                }
                *pu2MaxPort = WSSCFG_MAX_PORT;
                break;
            case '<':
                if (au1Operator[u2Index2] == '=')
                {
                    *pu2MaxPort = (UINT2) (u4Num);
                }
                else if (au1Operator[u2Index2] == '\0')
                {
                    /* Check if invalid MIN PORT (<1) is given as input.
                     * If so return error, because <1 is an invalid port 
                     * number */
                    if (u4Num == WSSCFG_MIN_PORT_VALUE)
                    {
                        return SNMP_FAILURE;

                    }
                    *pu2MaxPort = (UINT2) (u4Num - 1);
                }
                else
                {
                    return SNMP_FAILURE;
                }
                *pu2MinPort = WSSCFG_MIN_PORT;
                break;
            case '!':
                *pu2MinPort = (UINT2) (u4Num - 1);
                *pu2MaxPort = (UINT2) (u4Num + 1);
                break;
            case '=':
                if (au1Operator[u2Index2] == '\0')
                {
                    *pu2MinPort = (UINT2) u4Num;
                    *pu2MaxPort = (UINT2) u4Num;
                }
                else
                {
                    return SNMP_FAILURE;
                }
                break;
            default:
                return (SNMP_FAILURE);
        }                        /* end of Switch */
    }                            /*End-of-Else-Block */

    return (SNMP_SUCCESS);
    UNUSED_PARAM (i1Status);
}                                /* end of function -- FwlParseMinAndMaxPort */

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FwlValidateIpAddress                               */
/*     DESCRIPTION      : This function returns the type of the IpAdress it  */
/*                        belongs.i,e BroadCast,MultiCast etc.               */
/*     INPUT            : u4IpAddr - IpAddress for Validation                */
/*     OUTPUT           : The Type IpAdrress belongs.                        */
/*     RETURNS          : WSSCFG_BCAST_ADDR     or                              */
/*                        WSSCFG_MCAST_ADDR     or                              */
/*                        0_NETW_ADDR or                              */
/*                        WSSCFG_LOOPBACK_ADDR  or                              */
/*                        WSSCFG_UCAST_ADDR     or                              */
/*                        WSSCFG_INVALID_ADDR   or                              */
/*                        WSSCFG_CLASSE_ADDR                                    */
/*****************************************************************************/

PUBLIC INT4
ApFwlValidateIpAddress (UINT4 u4IpAddr)
{
    /*If Address is 255.255.255.255 */
    if ((u4IpAddr & WSSCFG_SET_ALL_BITS) == WSSCFG_BROADCAST_ADDR)
    {
        return (WSSCFG_BCAST_ADDR);
    }

    /*If Address is 127.x.x.x */
    if ((u4IpAddr & WSSCFG_NET_ADDR_LOOP_BACK) == WSSCFG_LOOP_BACK_ADDR)
    {
        return (WSSCFG_LOOPBACK_ADDR);
    }

    /*If Address Mulicast, i.e between 224.x.x.x and 239.x.x.x */
    if ((u4IpAddr & WSSCFG_NET_ADDR_MASK) == WSSCFG_CLASS_D_ADDR)
    {
        return (WSSCFG_MCAST_ADDR);
    }

    /*If Address is 0.0.0.0 */
    if ((u4IpAddr & WSSCFG_SET_ALL_BITS) == 0)
    {
        return (WSSCFG_ZERO_ADDR);
    }

    /*If Address is 0.x.x.x */
    if ((u4IpAddr & WSSCFG_NET_ADDR_LOOP_BACK) == 0)
    {
        return (WSSCFG_ZERO_NETW_ADDR);
    }

    /*If Address is CLASS-E, i.e betwwen 240.x.x.x and 254.x.x.x */
    if ((u4IpAddr & WSSCFG_NET_ADDR_MASK) == WSSCFG_CLASS_E_ADDR)
    {
        return (WSSCFG_CLASSE_ADDR);
    }

    /*If Address 255.x.x.x */
    if ((u4IpAddr & WSSCFG_NET_ADDR_LOOP_BACK) == WSSCFG_NET_ADDR_LOOP_BACK)
    {
        return (WSSCFG_INVALID_ADDR);
    }

    /*If Address CLASS-A and A.0.0.0 or A.255.255.255 */
    if ((u4IpAddr & WSSCFG_32ND_BIT_SET) == 0)
    {
        if ((u4IpAddr & WSSCFG_SET_24_BITS) == 0)
        {
            return (WSSCFG_CLASS_NETADDR);
        }

        else if ((u4IpAddr & WSSCFG_SET_24_BITS) == WSSCFG_SET_24_BITS)
        {
            return (WSSCFG_CLASS_BCASTADDR);
        }
    }
    /*If Address CLASS-B and B.0.0.0 or B.255.255.255 */
    if ((u4IpAddr & WSSCFG_CLASS_B_MASK) == WSSCFG_CLASS_B_ADDR)
    {
        if ((u4IpAddr & WSSCFG_SET_16_BITS) == 0)
        {
            return (WSSCFG_CLASS_NETADDR);
        }

        else if ((u4IpAddr & WSSCFG_SET_16_BITS) == WSSCFG_SET_16_BITS)
        {
            return (WSSCFG_CLASS_BCASTADDR);
        }
    }

    /*If Address CLASS-C and C.0.0.0 or C.255.255.255 */
    if ((u4IpAddr & WSSCFG_CLASS_B_MASK) == WSSCFG_CLASS_C_ADDR)
    {
        if ((u4IpAddr & WSSCFG_SET_8_BITS) == 0)
        {
            return (WSSCFG_CLASS_NETADDR);
        }

        else if ((u4IpAddr & WSSCFG_SET_8_BITS) == WSSCFG_SET_8_BITS)
        {
            return (WSSCFG_CLASS_BCASTADDR);
        }
    }
    return (WSSCFG_UCAST_ADDR);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : ApFwlValidateProtocol                            */
/*                                                                          */
/*    Description        : Validate the Protocol value                      */
/*                                                                          */
/*    Input(s)           : i4Protocol                                       */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : SUCCESS if Values is valid, otherwise FAILURE    */
/*                                                                          */
/****************************************************************************/

PUBLIC INT1
ApFwlValidateProtocol (INT4 i4Protocol)
{
    INT1                i1Status = 0;

    i1Status = SNMP_FAILURE;

    /* this function validates whether the protocol value is one of the 
     * already defined one.
     */
    switch (i4Protocol)
    {
        case WSSCFG_ICMP:
        case WSSCFG_IGMP:
        case WSSCFG_EGP:
        case WSSCFG_GGP:
        case WSSCFG_IGP:
        case WSSCFG_IP:
        case WSSCFG_TCP:
        case WSSCFG_UDP:
        case WSSCFG_NVP:
        case WSSCFG_IRTP:
        case WSSCFG_IDPR:
        case WSSCFG_RSVP:
        case WSSCFG_MHRP:
        case WSSCFG_IGRP:
        case WSSCFG_OSPFIGP:
        case WSSCFG_PIM:
        case WSSCFG_GRE_PPTP:
        case WSSCFG_DEFAULT_PROTO:
            i1Status = SNMP_SUCCESS;
            break;

        default:
            if ((i4Protocol >= 1) && (i4Protocol <= WSSCFG_DEFAULT_PROTO))
            {
                i1Status = SNMP_SUCCESS;
            }
            else
            {
                i1Status = SNMP_FAILURE;
            }
            break;
    }

    return i1Status;
}                                /* End of the function -- FwlValidateProtocol */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : ApFwlParseFilterSet                              */
/*                                                                          */
/*    Description        : Parses the Filter Set and gives the list of      */
/*                         Filters                                          */
/*                                                                          */
/*    Input(s)           : pWtpFwlRuleNode                                  */
/*                                                                          */
/*    Output(s)          : ppWtpFwlRuleNode                                 */
/*                                                                          */
/*    Returns            : SUCCESS if Parsing is done , otherwise FAILURE   */
/*                                                                          */
/****************************************************************************/
PUBLIC INT1
ApFwlParseFilterSet (UINT1 au1FilterSet[AP_FWL_MAX_FILTER_SET_LEN],
                     tWssIfWtpFwlRuleDB * pWtpFwlRuleNode,
                     UINT4 u4CapwapBaseWtpProfileId)
{
    INT1                i1Status = AP_FWL_ZERO;
    INT1                i1SrcStatus = AP_FWL_ZERO;
    INT1                i1DestStatus = AP_FWL_ZERO;
    UINT2               u2Index1 = AP_FWL_ZERO;
    UINT2               u2Index2 = AP_FWL_ZERO;
    UINT2               u2Index3 = AP_FWL_ZERO;
    UINT2               u2CounterAnd = AP_FWL_ZERO;
    UINT2               u2CounterOr = AP_FWL_ZERO;
    UINT2               u2FilterSetLen = AP_FWL_ZERO;
    UINT2               u2MinSrcPort = AP_FWL_ZERO;
    UINT2               u2MaxSrcPort = AP_FWL_ZERO;
    UINT2               u2MinDestPort = 0;
    UINT2               u2MaxDestPort = AP_FWL_ZERO;
    UINT1
         
         
         
         
         
         
          au1FilterName[AP_FWL_MAX_FILTERS_IN_RULE][AP_FWL_MAX_FILTER_NAME_LEN];

    UINT4               u4Count = AP_FWL_ZERO;
    tWssIfWtpFwlFilterDB *pFilterNode = NULL;
    tWssIfWtpFwlFilterDB *pFilterNode1 = NULL;
    tWssIfWtpFwlFilterDB *pFilterNode2 = NULL;
    tWssIfWtpFwlFilterDB *pTempFilter = NULL;
    tWssIfWtpFwlFilterDB *pNextTempFilter = NULL;
    tWssIfWtpFwlFilterDB *pWssIfWtpFwlFilter = NULL;
    tWssIfWtpFwlFilterDB *pWssIfWtpFwlFilter2 = NULL;

    u2Index1 = AP_FWL_ZERO;
    u2Index2 = AP_FWL_ZERO;
    u2Index3 = AP_FWL_ZERO;
    u2FilterSetLen = AP_FWL_ZERO;
    i1Status = SNMP_SUCCESS;
    u2CounterOr = AP_FWL_DEFAULT_COUNT;
    u2CounterAnd = AP_FWL_DEFAULT_COUNT;

    u2FilterSetLen = (UINT2) (STRLEN ((INT1 *) au1FilterSet));

    /* first the Filter set string is extracted into a list of filter names
     * and the combining condition ( & and ,) is also extracted.
     */

    pWssIfWtpFwlFilter =
        (tWssIfWtpFwlFilterDB *)
        MemAllocMemBlk (CAPWAP_WTP_FWL_FILTER_DB_POOLID);
    if (pWssIfWtpFwlFilter == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pWssIfWtpFwlFilter, 0, sizeof (tWssIfWtpFwlFilterDB));

    pWssIfWtpFwlFilter2 =
        (tWssIfWtpFwlFilterDB *)
        MemAllocMemBlk (CAPWAP_WTP_FWL_FILTER_DB_POOLID);
    if (pWssIfWtpFwlFilter2 == NULL)
    {
        MemReleaseMemBlock (CAPWAP_WTP_FWL_FILTER_DB_POOLID,
                            (UINT1 *) pWssIfWtpFwlFilter);
        return SNMP_FAILURE;
    }
    MEMSET (pWssIfWtpFwlFilter2, 0, sizeof (tWssIfWtpFwlFilterDB));

    while ((u2Index1 < u2FilterSetLen) &&
           (u2Index2 < AP_FWL_MAX_FILTER_NAME_LEN) &&
           (u2Index3 < AP_FWL_MAX_FILTERS_IN_RULE))
    {
        u2Index2 = AP_FWL_ZERO;
        /* CHANGE1 : The loop conditon was wrong. It didnt correctly check the
         * operators and didnt parse the Filter Name from the Filter Set.
         */
        while ((u2Index1 < u2FilterSetLen) &&
               (u2Index2 < AP_FWL_MAX_FILTER_NAME_LEN - 1) &&
               (au1FilterSet[u2Index1] != '&') &&
               (au1FilterSet[u2Index1] != ','))
        {
            au1FilterName[u2Index3][u2Index2] = au1FilterSet[u2Index1];
            u2Index2++;
            u2Index1++;
        }                        /* end of while */

        au1FilterName[u2Index3][u2Index2] = AP_FWL_END_OF_STRING;
        if (au1FilterSet[u2Index1] == '&')
        {
            u2CounterAnd++;
        }
        else if (au1FilterSet[u2Index1] == ',')
        {
            u2CounterOr++;
        }
        else
        {
            i1Status = SNMP_SUCCESS;
        }
        u2Index3++;
        u2Index1++;
    }                            /* end of while */
    if (u2Index1 < u2FilterSetLen)
    {
        i1Status = SNMP_FAILURE;
    }
    else
    {                            /* Parsing of filterset is complete */
        /* if both the operators are used, return FAILURE */
        if ((u2CounterAnd != AP_FWL_DEFAULT_COUNT) &&
            (u2CounterOr != AP_FWL_DEFAULT_COUNT))
        {
            i1Status = SNMP_FAILURE;
        }
        else
        {                        /* No & and , operator are combined */
            /* Based on the operator the condition flag is updated. */
            if (u2CounterAnd != AP_FWL_DEFAULT_COUNT)
            {
                pWtpFwlRuleNode->u1FilterConditionFlag = WSSCFG_FILTER_AND;
            }
            else
            {
                pWtpFwlRuleNode->u1FilterConditionFlag = WSSCFG_FILTER_OR;
            }
            /* store the list of pointers corresponding to the filter name */
            for (u2Index1 = AP_FWL_ZERO; u2Index1 < u2Index3; u2Index1++)
            {
                pWtpFwlRuleNode->apFilterInfo[u2Index1] =
                    ApFwlDbaseSearchFilter (pWssIfWtpFwlFilter,
                                            u4CapwapBaseWtpProfileId,
                                            au1FilterName[u2Index1]);

                /* increment the reference count for the coressponding filters
                 * that are applied in the rule table.
                 */
                if (pWtpFwlRuleNode->apFilterInfo[u2Index1] == NULL)
                {
                    i1Status = SNMP_FAILURE;
                    break;
                }
                else
                {
                    /* CHANGE2 : The invalid  combination of rules was not 
                     * checked. So this new function was added to check the 
                     * invalid combination of rules.
                     */
                    pFilterNode = pWtpFwlRuleNode->apFilterInfo[u2Index1];
                    if (pFilterNode->i4RowStatus == ACTIVE)
                    {
                        /*Combination of filters with different address type is
                         * not allowed.Hence check the address type of the 
                         * filter set*/
                        for (u4Count = AP_FWL_ZERO;
                             (((pTempFilter =
                                pWtpFwlRuleNode->apFilterInfo[u4Count]) != NULL)
                              && (u4Count < AP_FWL_MAX_FILTERS_IN_RULE - 1));
                             u4Count++)
                        {
                            pNextTempFilter =
                                pWtpFwlRuleNode->apFilterInfo[u4Count + 1];
                            if (pNextTempFilter != NULL)
                            {
                                if (pTempFilter->u2AddrType !=
                                    pNextTempFilter->u2AddrType)
                                {
                                    i1Status = SNMP_FAILURE;
                                }
                            }
                        }
                        if ((pWtpFwlRuleNode->u1FilterConditionFlag ==
                             WSSCFG_FILTER_AND) && (u2Index1 == u2Index3 - 1))
                        {
                            i1Status =
                                ApFwlValidateRuleCombination (pWtpFwlRuleNode->
                                                              apFilterInfo
                                                              [u2Index1],
                                                              pWtpFwlRuleNode);
                        }
                        if (i1Status == SNMP_SUCCESS)
                        {
                            /* Removed incrementing the filter 
                             * reference count in the parsing function. Now
                             * this will be done in FwlAddFilterSet() during 
                             * the set routine.
                             */
                        }
                        else
                        {
                            /* Invalid Firewall Filter Combination */
                            pWtpFwlRuleNode->apFilterInfo[u2Index1] = NULL;
                            break;
                        }
                    }
                    else
                    {
                        i1Status = SNMP_FAILURE;
                        /* The Respective Filter is not in Active status */
                        pWtpFwlRuleNode->apFilterInfo[u2Index1] = NULL;
                        break;
                    }
                }

            }                    /* end of for */
            /* As the filter reference count is not incremented in 
             * this function, there is no need to decrement it if the filter set
             * is invalid.
             */

            /* Store the Min and Max Port Ranges if the 
             * Filter Combination is AND Type */
            if (pWtpFwlRuleNode->u1FilterConditionFlag != WSSCFG_FILTER_AND)
            {
                /* Rule validation for Filters OR'd is not needed */
            }
            else
            {
                /* Compare the Filter Combination and get their max and min 
                 * port ranges and iterate thru all the filter sets by comparing 
                 * the min and max port values provided by 
                 * ApFwlCheckFilterCombination () */
                u2Index1 = AP_FWL_ZERO;
                pWtpFwlRuleNode->apFilterInfo[u2Index1] =
                    ApFwlDbaseSearchFilter (pWssIfWtpFwlFilter,
                                            u4CapwapBaseWtpProfileId,
                                            au1FilterName[u2Index1]);
                pWtpFwlRuleNode->apFilterInfo[u2Index1 + 1] =
                    ApFwlDbaseSearchFilter (pWssIfWtpFwlFilter2,
                                            u4CapwapBaseWtpProfileId,
                                            au1FilterName[u2Index1 + 1]);
                if ((pWtpFwlRuleNode->apFilterInfo[u2Index1] == NULL)
                    || (pWtpFwlRuleNode->apFilterInfo[u2Index1 + 1] == NULL))
                {
                    /* User given Filter is not present */
                    i1Status = SNMP_FAILURE;

                }
                else
                {
                    pFilterNode1 = pWtpFwlRuleNode->apFilterInfo[u2Index1];
                    pFilterNode2 = pWtpFwlRuleNode->apFilterInfo[u2Index1 + 1];

                    u2Index1 = (UINT2) (u2Index1 + 1);
                    if (pFilterNode == NULL)
                    {
                        /* No Such Filter is present */
                        MemReleaseMemBlock (CAPWAP_WTP_FWL_FILTER_DB_POOLID,
                                            (UINT1 *) pWssIfWtpFwlFilter);
                        MemReleaseMemBlock (CAPWAP_WTP_FWL_FILTER_DB_POOLID,
                                            (UINT1 *) pWssIfWtpFwlFilter2);
                        return SNMP_FAILURE;
                    }

                    if ((pFilterNode->u2SrcMinPort != AP_FWL_DEFAULT_PORT)
                        && (pFilterNode->u2SrcMaxPort != AP_FWL_DEFAULT_PORT))
                    {
                        i1SrcStatus =
                            ApFwlCheckFilterCombination (pFilterNode1->
                                                         u2SrcMinPort,
                                                         pFilterNode1->
                                                         u2SrcMaxPort,
                                                         pFilterNode2->
                                                         u2SrcMinPort,
                                                         pFilterNode2->
                                                         u2SrcMaxPort,
                                                         &u2MinSrcPort,
                                                         &u2MaxSrcPort);
                        if (i1SrcStatus == AP_FWL_FAILURE)
                        {
                            /* Filters Ports Mismatch */
                            MemReleaseMemBlock (CAPWAP_WTP_FWL_FILTER_DB_POOLID,
                                                (UINT1 *) pWssIfWtpFwlFilter);
                            MemReleaseMemBlock (CAPWAP_WTP_FWL_FILTER_DB_POOLID,
                                                (UINT1 *) pWssIfWtpFwlFilter2);
                            return SNMP_FAILURE;
                        }
                    }
                    else
                    {
                        u2MinSrcPort = WSSCFG_MIN_PORT_VALUE;
                        u2MaxSrcPort = WSSCFG_MAX_PORT_VALUE;
                    }

                    if ((pFilterNode->u2DestMinPort != AP_FWL_DEFAULT_PORT)
                        && (pFilterNode->u2DestMaxPort != AP_FWL_DEFAULT_PORT))
                    {
                        i1DestStatus =
                            ApFwlCheckFilterCombination (pFilterNode1->
                                                         u2DestMinPort,
                                                         pFilterNode1->
                                                         u2DestMaxPort,
                                                         pFilterNode2->
                                                         u2DestMinPort,
                                                         pFilterNode2->
                                                         u2DestMaxPort,
                                                         &u2MinDestPort,
                                                         &u2MaxDestPort);
                        if (i1DestStatus == AP_FWL_FAILURE)
                        {
                            /* Filters Ports Mismatch */
                            MemReleaseMemBlock (CAPWAP_WTP_FWL_FILTER_DB_POOLID,
                                                (UINT1 *) pWssIfWtpFwlFilter);
                            MemReleaseMemBlock (CAPWAP_WTP_FWL_FILTER_DB_POOLID,
                                                (UINT1 *) pWssIfWtpFwlFilter2);
                            return SNMP_FAILURE;
                        }
                    }
                    else
                    {
                        u2MinDestPort = WSSCFG_MIN_PORT_VALUE;
                        u2MaxDestPort = WSSCFG_MAX_PORT_VALUE;
                    }

                    u2Index1++;
                    /* Scan thru' the other filters and check whether the 
                     * Rule Combination is valid or not */
                    while ((u2Index1 < u2Index3) &&
                           ((pWtpFwlRuleNode->apFilterInfo[u2Index1]) =
                            ApFwlDbaseSearchFilter (pWssIfWtpFwlFilter,
                                                    u4CapwapBaseWtpProfileId,
                                                    au1FilterName[u2Index1])) !=
                           NULL)
                    {
                        pFilterNode = pWtpFwlRuleNode->apFilterInfo[u2Index1];
                        if ((pFilterNode->u2SrcMinPort != AP_FWL_DEFAULT_PORT)
                            && (pFilterNode->u2SrcMaxPort !=
                                AP_FWL_DEFAULT_PORT))
                        {
                            i1SrcStatus =
                                ApFwlCheckFilterCombination (u2MinSrcPort,
                                                             u2MaxSrcPort,
                                                             pFilterNode->
                                                             u2SrcMinPort,
                                                             pFilterNode->
                                                             u2SrcMaxPort,
                                                             &u2MinSrcPort,
                                                             &u2MaxSrcPort);
                            if (i1SrcStatus == AP_FWL_FAILURE)
                            {
                                /* Filters Port Mismatch */
                                i1Status = SNMP_FAILURE;
                                break;
                            }
                        }
                        else
                        {
                            u2MinSrcPort = WSSCFG_MIN_PORT_VALUE;
                            u2MaxSrcPort = WSSCFG_MAX_PORT_VALUE;
                        }
                        if ((pFilterNode->u2DestMinPort != AP_FWL_DEFAULT_PORT)
                            && (pFilterNode->u2DestMaxPort !=
                                AP_FWL_DEFAULT_PORT))
                        {
                            i1DestStatus =
                                ApFwlCheckFilterCombination (u2MinDestPort,
                                                             u2MaxDestPort,
                                                             pFilterNode->
                                                             u2DestMinPort,
                                                             pFilterNode->
                                                             u2DestMaxPort,
                                                             &u2MinDestPort,
                                                             &u2MaxDestPort);
                            if (i1DestStatus == AP_FWL_FAILURE)
                            {

                                i1Status = SNMP_FAILURE;
                                break;
                            }
                        }
                        else
                        {
                            u2MinDestPort = WSSCFG_MIN_PORT_VALUE;
                            u2MaxDestPort = WSSCFG_MAX_PORT_VALUE;
                        }
                        u2Index1++;
                    }
                    pWtpFwlRuleNode->u2MinSrcPort = u2MinSrcPort;
                    pWtpFwlRuleNode->u2MaxSrcPort = u2MaxSrcPort;
                    pWtpFwlRuleNode->u2MinDestPort = u2MinDestPort;
                    pWtpFwlRuleNode->u2MaxDestPort = u2MaxDestPort;
                }
            }
        }                        /* end of else (No & or , operator combined */
    }
    if (i1Status == SNMP_SUCCESS)
    {
        STRCPY ((INT1 *) pWtpFwlRuleNode->au1FilterSet, au1FilterSet);
    }
    MemReleaseMemBlock (CAPWAP_WTP_FWL_FILTER_DB_POOLID,
                        (UINT1 *) pWssIfWtpFwlFilter);
    MemReleaseMemBlock (CAPWAP_WTP_FWL_FILTER_DB_POOLID,
                        (UINT1 *) pWssIfWtpFwlFilter2);
    return (i1Status);
}                                /* End of Function -- FwlParseFilterSet */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : ApFwlDbaseSearchFilter                           */
/*                                                                          */
/*    Description        : Checks whether the fitler is present in the 
 *                          database, if present returns the pointer to it. */
/*                                                                          */
/*    Input(s)           : Filter name to be searched                       */
/*                                                                          */
/*                                                                          */
/*    Output(s)          : -nil-                                            */
/*    Returns            : Pointer to the filter node if the searched filter 
 *                          is present if not returns NULL                  */
/*                                                                          */
/****************************************************************************/
PUBLIC tWssIfWtpFwlFilterDB *
ApFwlDbaseSearchFilter (tWssIfWtpFwlFilterDB * pWssIfWtpFwlFilter,
                        UINT4 u4CapwapBaseWtpProfileId,
                        UINT1 au1FilterName[AP_FWL_MAX_FILTER_NAME_LEN])
{
#ifdef WLC_WANTED
    pWssIfWtpFwlFilter->u4WtpProfileId = u4CapwapBaseWtpProfileId;
    MEMCPY (pWssIfWtpFwlFilter->au1FilterName, au1FilterName,
            AP_FWL_MAX_FILTER_NAME_LEN);
    pWssIfWtpFwlFilter->au1FilterName[STRLEN (au1FilterName)] =
        AP_FWL_END_OF_STRING;
    if (OSIX_SUCCESS == WssIfWtpFwlFilterGetEntry (pWssIfWtpFwlFilter))
    {
        return pWssIfWtpFwlFilter;
    }
#else
    UNUSED_PARAM (pWssIfWtpFwlFilter);
    UNUSED_PARAM (u4CapwapBaseWtpProfileId);
    UNUSED_PARAM (au1FilterName);
#endif
    return NULL;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : ApFwlCheckFilterCombination                        */
/*                                                                          */
/*    Description        : Checks whether the port ranges specified in the  */
/*                         Rule's AND'ed together are valid or not          */
/*                                                                          */
/*    Input(s)           : u2Filter1_MinValue - Filter 1's Min Port Number    */
/*                         u2Filter1_MaxValue - Filter 1's Max Port Number    */
/*                         u2Filter2_MinValue - Filter 2's Min Port Number    */
/*                         u2Filter2_MaxValue - Filter 1's Max Port Number    */
/*                                                                          */
/*                                                                          */
/*    Output(s)          : pu2MinValue - Valid Min Port No.                 */
/*                         pu2MaxValue - Valid Max Port No.                 */
/*    Returns            : AP_FWL_SUCCESS if parsing is done else AP_FWL_FAILURE  */
/*                                                                          */
/****************************************************************************/
PUBLIC INT1
ApFwlCheckFilterCombination (UINT2 u2Filter1_MinValue,
                             UINT2 u2Filter1_MaxValue,
                             UINT2 u2Filter2_MinValue,
                             UINT2 u2Filter2_MaxValue,
                             UINT2 *pu2MinValue, UINT2 *pu2MaxValue)
{
    if ((u2Filter1_MinValue == WSSCFG_MIN_PORT_VALUE) &&
        (u2Filter2_MinValue == WSSCFG_MIN_PORT_VALUE))
    {
        *pu2MinValue = WSSCFG_MIN_PORT_VALUE;
        if (u2Filter1_MinValue == u2Filter1_MaxValue)
        {
            *pu2MaxValue = u2Filter1_MaxValue;
        }
        else if (u2Filter2_MinValue == u2Filter2_MaxValue)
        {
            *pu2MaxValue = u2Filter2_MaxValue;
        }
        else
        {
            *pu2MaxValue =
                u2Filter1_MaxValue <
                u2Filter2_MaxValue ? u2Filter1_MaxValue : u2Filter2_MaxValue;
        }
        return AP_FWL_SUCCESS;
    }
    if ((u2Filter1_MaxValue == WSSCFG_MAX_PORT_VALUE) &&
        (u2Filter2_MaxValue == WSSCFG_MAX_PORT_VALUE))
    {
        if (u2Filter1_MinValue == u2Filter1_MaxValue)
        {
            *pu2MinValue = u2Filter1_MinValue;
        }
        else if (u2Filter2_MinValue == u2Filter2_MaxValue)
        {
            *pu2MinValue = u2Filter2_MinValue;
        }
        else
        {
            *pu2MinValue =
                u2Filter1_MinValue <
                u2Filter2_MinValue ? u2Filter1_MinValue : u2Filter2_MinValue;
        }
        *pu2MaxValue = WSSCFG_MAX_PORT_VALUE;
        return AP_FWL_SUCCESS;
    }
    if ((u2Filter1_MinValue == u2Filter1_MaxValue) &&
        (u2Filter2_MinValue == u2Filter2_MaxValue))
    {
        if (u2Filter1_MinValue == u2Filter2_MinValue)
        {
            /* Filters with same port range */
            *pu2MinValue = *pu2MaxValue = u2Filter1_MinValue;
            return AP_FWL_SUCCESS;
        }
        else
        {
            /* Filters Combined [Using '='] can't have diff. port ranges */
            return AP_FWL_FAILURE;
        }
    }
    else if ((u2Filter1_MinValue == u2Filter1_MaxValue))
    {
        if ((u2Filter1_MinValue < u2Filter2_MinValue) ||
            (u2Filter1_MinValue > u2Filter2_MaxValue))
        {
            return AP_FWL_FAILURE;
        }
        else
        {
            *pu2MinValue = *pu2MaxValue = u2Filter1_MinValue;
            return AP_FWL_SUCCESS;
        }

    }
    else if ((u2Filter2_MinValue == u2Filter2_MaxValue))
    {
        if ((u2Filter2_MinValue < u2Filter1_MinValue) ||
            (u2Filter2_MinValue > u2Filter1_MaxValue))
        {
            return AP_FWL_FAILURE;
        }
        else
        {
            *pu2MinValue = *pu2MaxValue = u2Filter2_MinValue;
            return AP_FWL_SUCCESS;
        }
    }
    if (u2Filter1_MinValue == u2Filter2_MinValue)
    {
        if (u2Filter1_MaxValue < u2Filter2_MaxValue)
        {
            *pu2MinValue = u2Filter1_MinValue;
            *pu2MaxValue = u2Filter1_MaxValue;
            return AP_FWL_SUCCESS;
        }
        else
        {
            *pu2MinValue = u2Filter2_MinValue;
            *pu2MaxValue = u2Filter2_MaxValue;
            return AP_FWL_SUCCESS;
        }
    }
    if (u2Filter1_MaxValue == u2Filter2_MaxValue)
    {
        if (u2Filter1_MinValue < u2Filter2_MinValue)
        {
            *pu2MinValue = u2Filter1_MinValue;
            *pu2MaxValue = u2Filter1_MaxValue;
            return AP_FWL_SUCCESS;
        }
        else
        {
            *pu2MinValue = u2Filter2_MinValue;
            *pu2MaxValue = u2Filter2_MaxValue;
            return AP_FWL_SUCCESS;
        }
    }
    if (((u2Filter1_MinValue < u2Filter2_MinValue) &&
         (u2Filter1_MaxValue < u2Filter2_MaxValue)) ||
        ((u2Filter1_MinValue > u2Filter2_MinValue) &&
         (u2Filter1_MaxValue > u2Filter2_MaxValue)))
    {
        if (u2Filter1_MinValue < u2Filter2_MinValue)
        {
            if (u2Filter1_MaxValue - u2Filter2_MinValue < AP_FWL_ZERO)
            {
                return AP_FWL_FAILURE;
            }
            else
            {
                *pu2MinValue = u2Filter2_MinValue;
                *pu2MaxValue = u2Filter1_MaxValue;
                return AP_FWL_SUCCESS;
            }
        }
        else
        {
            if (u2Filter2_MaxValue - u2Filter1_MinValue < AP_FWL_ZERO)
            {
                return AP_FWL_FAILURE;
            }
            else
            {
                *pu2MinValue = u2Filter1_MinValue;
                *pu2MaxValue = u2Filter2_MaxValue;
                return AP_FWL_SUCCESS;
            }
        }
    }
    else if ((u2Filter1_MinValue > u2Filter2_MinValue) &&
             (u2Filter1_MaxValue < u2Filter2_MaxValue))
    {
        *pu2MinValue = u2Filter1_MinValue;
        *pu2MaxValue = u2Filter1_MaxValue;
        return AP_FWL_SUCCESS;
    }
    else if ((u2Filter1_MinValue < u2Filter2_MinValue) &&
             (u2Filter1_MaxValue > u2Filter2_MaxValue))
    {
        *pu2MinValue = u2Filter2_MinValue;
        *pu2MaxValue = u2Filter2_MaxValue;
        return AP_FWL_SUCCESS;
    }

    /* Invalid Port Range Combination other than the above ends up here */
    return AP_FWL_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : ApFwlValidateRuleCombination                       */
/*                                                                          */
/*    Description        : Checks whether the combination of filters are    */
/*                         valid.                                           */
/*                                                                          */
/*    Input(s)           : pRuleNode    -- Pointer to the Rule Node         */
/*                         pFilterNode  -- Pointer to the FilterNode        */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : SUCCESS if combination is valid else FAILURE.    */
/*                                                                          */
/****************************************************************************/
PUBLIC INT1
ApFwlValidateRuleCombination (tWssIfWtpFwlFilterDB * pFilterNode,
                              tWssIfWtpFwlRuleDB * pRuleNode)
{
    INT1                i1Status = AP_FWL_ZERO;
    tWssIfWtpFwlFilterDB *pTempFilter = NULL;
    tWssIfWtpFwlFilterDB *pNextTempFilter = NULL;
    UINT4               u4Count = AP_FWL_ZERO;

    i1Status = SNMP_SUCCESS;
    for (u4Count = AP_FWL_ZERO;
         ((pTempFilter = pRuleNode->apFilterInfo[u4Count]) != NULL)
         && (u4Count < AP_FWL_MAX_FILTERS_IN_RULE - 1); u4Count++)
    {
        /* Check whether the filter combination is of same address type
         * Combination of filters with differnet address type is not
         * allowed*/
        if (u4Count != (AP_FWL_MAX_FILTERS_IN_RULE - 1))
        {
            pNextTempFilter = pRuleNode->apFilterInfo[u4Count + 1];
            if (pNextTempFilter != NULL)
            {
                if (pTempFilter->u2AddrType != pNextTempFilter->u2AddrType)
                {
                    i1Status = SNMP_FAILURE;
                }
            }
        }
        if (STRCMP
            ((INT1 *) pTempFilter->au1FilterName,
             (INT1 *) pFilterNode->au1FilterName) == AP_FWL_STRING_EQUAL)
        {
            break;
        }

        /* Check whether the rest of the filters associated with the Rule node
         * is same as of Incoming Filter node [except Src and Dest Port Range]
         * */
        if (pFilterNode->u2AddrType == AP_FWL_IP_VERSION_4)
        {
            if ((pFilterNode->SrcStartAddr.v4Addr != AP_FWL_DEFAULT_ADDRESS) &&
                (pTempFilter->SrcStartAddr.v4Addr != AP_FWL_DEFAULT_ADDRESS))
            {
                if ((pFilterNode->SrcStartAddr.v4Addr <
                     pTempFilter->SrcStartAddr.v4Addr)
                    && (pFilterNode->SrcStartAddr.v4Addr >
                        pTempFilter->SrcEndAddr.v4Addr)
                    && (pFilterNode->SrcEndAddr.v4Addr <
                        pTempFilter->SrcStartAddr.v4Addr)
                    && (pFilterNode->SrcEndAddr.v4Addr >
                        pTempFilter->SrcEndAddr.v4Addr))
                {
                    i1Status = SNMP_FAILURE;
                    break;
                }
            }
            if ((pFilterNode->DestStartAddr.v4Addr != AP_FWL_DEFAULT_ADDRESS) &&
                (pTempFilter->DestStartAddr.v4Addr != AP_FWL_DEFAULT_ADDRESS))
            {
                if ((pFilterNode->DestStartAddr.v4Addr <
                     pTempFilter->DestStartAddr.v4Addr)
                    && (pFilterNode->DestStartAddr.v4Addr >
                        pTempFilter->DestEndAddr.v4Addr)
                    && (pFilterNode->DestEndAddr.v4Addr <
                        pTempFilter->DestStartAddr.v4Addr)
                    && (pFilterNode->DestEndAddr.v4Addr >
                        pTempFilter->DestEndAddr.v4Addr))
                {
                    i1Status = SNMP_FAILURE;
                    break;
                }
            }
        }
        if ((pFilterNode->u1Proto != AP_FWL_DEFAULT_PROTO) &&
            (pTempFilter->u1Proto != AP_FWL_DEFAULT_PROTO))
        {
            if (pFilterNode->u1Proto != pTempFilter->u1Proto)
            {
                i1Status = SNMP_FAILURE;
                break;
            }
        }
        if ((pFilterNode->u1TcpAck != AP_FWL_TCP_ACK_ANY) &&
            (pTempFilter->u1TcpAck != AP_FWL_TCP_ACK_ANY))
        {
            if (pFilterNode->u1TcpAck != pTempFilter->u1TcpAck)
            {
                i1Status = SNMP_FAILURE;
                break;
            }
        }
        if ((pFilterNode->u1TcpRst != AP_FWL_TCP_RST_ANY) &&
            (pTempFilter->u1TcpRst != AP_FWL_TCP_RST_ANY))
        {
            if (pFilterNode->u1TcpRst != pTempFilter->u1TcpRst)
            {
                i1Status = SNMP_FAILURE;
                break;
            }
        }
        if ((pFilterNode->u1Tos != AP_FWL_TOS_ANY) &&
            (pTempFilter->u1Tos != AP_FWL_TOS_ANY))
        {
            if (pFilterNode->u1Tos != pTempFilter->u1Tos)
            {
                i1Status = SNMP_FAILURE;
                break;
            }
        }
    }
    return (i1Status);
}                                /* End of function -- ApFwlValidateRuleCombination */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : ApFwlDbaseSearchAclFilter                        */
/*                                                                          */
/*    Description        : Searches the OutFilter List or The Infilter List */
/*                         for the given filter name.                       */
/*                                                                          */
/*    Input(s)           : au1AclName       -- String containing Filter name*/
/*                         pAclList         -- Pointer to the AclFilter list*/
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Pointer to the filter node if exists, otherwise  */
/*                         returns NULL                                     */
/****************************************************************************/

/*PUBLIC tWssIfWtpFwlAclDB    *ApFwlDbaseSearchAclFilter
  (tWssIfWtpFwlAclDB * pAclList, UINT1 au1AclName[FWL_MAX_ACL_NAME_LEN])
#else
PUBLIC tAclInfo    *
FwlDbaseSearchAclFilter (pAclList, au1AclName)
     tTMO_SLL           *pAclList;
     UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN];
#endif
*/
#if 0
{
    tAclInfo           *pAclNode = NULL;
    UINT1               u1Status = FWL_NOT_MATCH;
    pAclNode = (tAclInfo *) NULL;
    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entering into the fn. FwlDbaseSearchAclFilter \n");
    /* Search the OutFilter List or InFilter List for the Particular filter 
     * name. If found return the pointer to the Node, else NULL.
     */
    TMO_SLL_Scan (pAclList, pAclNode, tAclInfo *)
    {
        if (FWL_STRCMP
            ((INT1 *) au1AclName,
             (INT1 *) pAclNode->au1FilterName) == FWL_STRING_EQUAL)
        {
            u1Status = FWL_MATCH;
            break;
        }
    }                            /* End of the TMO_SLL_Scan */

    if (u1Status == FWL_MATCH)
    {

        FWL_DBG1 (FWL_DBG_DBASE, "\n Acl Name = %s\n", pAclNode->au1FilterName);
        return pAclNode;
    }

    FWL_DBG (FWL_DBG_EXIT,
             "\n Exiting from the fn. FwlDbaseSearchAclFilter \n");
    return NULL;
}                                /* End of the function -- FwlDbaseSearchAclFilter */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlSetActionValueForAclNode                      */
/*                                                                          */
/*    Description        : Sets the action value for the corresponding Acl  */
/*                         Node .                                           */
/*                                                                          */
/*    Input(s)           : pIfaceNode   -- Pointer to the Interface Node    */
/*                         i4Direction  -- Direction                        */
/*                         au1AclName   -- Accesslist Name                  */
/*                         i4Action     -- Action Value                     */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : SNMP_SUCCESS if Value is set , otherwise         */
/*                         SNMP_FAILURE.                                    */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC INT1
FwlSetActionValueForAclNode (tIfaceInfo * pIfaceNode,
                             INT4 i4Direction,
                             UINT1 au1AclName[FWL_MAX_ACL_NAME_LEN],
                             INT4 i4Action)
#else
PUBLIC INT1
FwlSetActionValueForAclNode (pIfaceNode, i4Direction, au1AclName, i4Action)
     tIfaceInfo         *pIfaceNode;
     INT4                i4Direction = FWL_ZERO;
     UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN] = { FWL_ZERO };
INT4                i4Action = FWL_ZERO;
#endif
{
    INT1                i1Status = FWL_ZERO;
    tAclInfo           *pAclNode = NULL;

    i1Status = SNMP_FAILURE;
    pAclNode = (tAclInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY,
             "\n Entering into Function FwlSetActionValueForAclNode\n");
    /* search the interface number .if direction is IN then search the Infilter
     * list and and set the action value for the particular filter name .if 
     * direction is OUT, do the same in outfilter list. 
     */
    if (pIfaceNode->u1RowStatus == FWL_ACTIVE)
    {
        if (i4Direction == FWL_DIRECTION_IN)
        {
            pAclNode =
                FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList, au1AclName);
            if (pAclNode == NULL)
            {
                pAclNode = FwlDbaseSearchAclFilter
                    (&pIfaceNode->inIPv6FilterList, au1AclName);
            }
            MOD_TRC_ARG3 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          " \n Action Value %d is set for Acl Filter %s "
                          "on interface %d in the IN direction\n ",
                          i4Action, au1AclName, pIfaceNode->u4IfaceNum);
        }
        else if (i4Direction == FWL_DIRECTION_OUT)
        {
            pAclNode =
                FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                         au1AclName);
            if (pAclNode == NULL)
            {
                pAclNode =
                    FwlDbaseSearchAclFilter (&pIfaceNode->outIPv6FilterList,
                                             au1AclName);
            }
            MOD_TRC_ARG3 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          " \n Action Value %d is set for Acl Filter %s "
                          "on interface %d in the OUT direction\n ",
                          i4Action, au1AclName, pIfaceNode->u4IfaceNum);
        }
        if (pAclNode == NULL)
        {
            return i1Status;
        }

        if ((pAclNode->u1Action == FWL_PERMIT) && (i4Action == FWL_DENY))
        {
            /* Set the Rule's Action to DENY so that the matching ACL & State
             * Table Entries would be destroyed @ FwlCommitAcl () */
            pAclNode->u1Action = (UINT1) i4Action;
            FwlCommitAcl ();
        }
        /* set the Action Value */
        pAclNode->u1Action = (UINT1) i4Action;
        i1Status = SNMP_SUCCESS;
    }

    FWL_DBG (FWL_DBG_EXIT,
             "\n Exiting from Function FwlSetActionValueForAclNode\n");
    return i1Status;

}                                /* End of the Function  --  FwlSetActionValueForAclNode */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlSetInitForAclNode                             */
/*                                                                          */
/*    Description        : Does Memory allcation for the Iface Node and Acl */
/*                         Node and initialises it.                         */
/*                                                                          */
/*    Input(s)           : u4IfaceNum   -- Interface Number                 */
/*                         i4Direction  -- Direction                        */
/*                         au1AclName   -- Accesslist Name                  */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : SNMP_SUCCESS if Node is Created , otherwise      */
/*                         SNMP_FAILURE.                                    */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC INT1
FwlSetInitForAclNode (UINT4 u4IfaceNum,
                      INT4 i4Direction, UINT1 au1AclName[FWL_MAX_ACL_NAME_LEN])
#else
PUBLIC INT1
FwlSetInitForAclNode (u4IfaceNum, i4Direction, au1AclName[FWL_MAX_ACL_NAME_LEN])
     UINT4               u4IfaceNum;
     INT4                i4Direction;
     UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN];
#endif
{
    tIfaceInfo         *pIfaceNode = NULL;
    tAclInfo           *pAclTmpNode = NULL;
    tAclInfo           *pAclNode = NULL;
    tFilterInfo        *pFilterNode = NULL;
    tRuleInfo          *pRuleFilter = NULL;
    INT1                i1Status = FWL_ZERO;

    i1Status = SNMP_SUCCESS;
    pIfaceNode = (tIfaceInfo *) NULL;
    pAclTmpNode = (tAclInfo *) NULL;
    pAclNode = (tAclInfo *) NULL;
    pRuleFilter = (tRuleInfo *) NULL;
    pFilterNode = (tFilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\nEntering into Function FwlSetInitForAclNode\n");

    /* Search for the interface node. If node is not found then create it. */
    pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
    if (pIfaceNode == NULL)
    {
        /* Interface node Allocation and Initialisation */
        if (FwlIfaceMemAllocate ((UINT1 **) (VOID *) &pIfaceNode) ==
            FWL_SUCCESS)
        {
            FwlSetDefaultIfaceValue (u4IfaceNum, pIfaceNode);
            gu1FwlIfaceFlag = FWL_TRUE;

            /* Add to interface list */
            gFwlAclInfo.apIfaceList[u4IfaceNum] = pIfaceNode;
            i1Status = SNMP_SUCCESS;
            MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                          "\nInterface Index %d is created. \n", u4IfaceNum);
        }
        else
        {
            /* send a trap for memory failure */
            FwlGenerateMemFailureTrap (FWL_IFACE_ALLOC_FAILURE);
            INC_MEM_FAILURE_COUNT;
            gFwlAclInfo.i4MemStatus = (INT4) MEM_FAILURE;
            i1Status = SNMP_FAILURE;
        }
    }
    /* If the Interface Node exits or if the Node is created successfully then
     * Search whether the InFilter Already exits if direction is IN. If not 
     * then create the InFilter Node. If Direction is OUT and the Node doesnt
     * exist then create the Outfilter Node.
     */
    if (i1Status == SNMP_SUCCESS)
    {
        if (i4Direction == FWL_DIRECTION_IN)
        {
            pAclTmpNode = FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList,
                                                   au1AclName);
            if (pAclTmpNode == NULL)
            {
                /*Entry not found in IPv4 list .. check in ipv6 list */
                pAclTmpNode = FwlDbaseSearchAclFilter
                    (&pIfaceNode->inIPv6FilterList, au1AclName);
            }
        }
        else
        {
            pAclTmpNode = FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                                   au1AclName);
            if (pAclTmpNode == NULL)
            {
                /*Entry not found in IPv4 list .. check in ipv6 list */
                pAclTmpNode = FwlDbaseSearchAclFilter
                    (&pIfaceNode->inIPv6FilterList, au1AclName);
            }
        }
        if (pAclTmpNode == NULL)
        {
            /* Acl node Allocation and Initialisation */
            if (FwlAclMemAllocate ((UINT1 **) (VOID *) &pAclNode) ==
                FWL_SUCCESS)
            {
                i1Status = FwlSetDefaultAclFilterValue (au1AclName, pAclNode);
                pRuleFilter = (tRuleInfo *) pAclNode->pAclName;
                if (pRuleFilter == NULL)
                {
                    return FWL_FAILURE;
                }
                pFilterNode =
                    pRuleFilter->apFilterInfo[FWL_RULE_FIRST_FILTER_INDEX];
                if (pFilterNode == NULL)
                {
                    return FWL_FAILURE;
                }
                if (i1Status == SNMP_SUCCESS)
                {
                    /* Add the Out Filter node to the Out Filter list 
                     * if Direction is OUT else add the Node to the InFIlter 
                     * List.
                     */
                    if (i4Direction == FWL_DIRECTION_IN)
                    {
                        if (pFilterNode->u2AddrType == FWL_IP_VERSION_4)
                        {
                            TMO_SLL_Add (&pIfaceNode->inFilterList,
                                         (tTMO_SLL_NODE *) pAclNode);
                        }
                        else
                        {
                            TMO_SLL_Add (&pIfaceNode->inIPv6FilterList,
                                         (tTMO_SLL_NODE *) pAclNode);
                        }
                        MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                      "\n Filter/ Rule %s is added on Interface"
                                      "%d in the IN Direction\n",
                                      au1AclName, u4IfaceNum);
                    }
                    else
                    {            /*Direction is OUT */
                        if (pFilterNode->u2AddrType == FWL_IP_VERSION_4)
                        {
                            TMO_SLL_Add (&pIfaceNode->outFilterList,
                                         (tTMO_SLL_NODE *) pAclNode);
                        }
                        else
                        {
                            TMO_SLL_Add (&pIfaceNode->outIPv6FilterList,
                                         (tTMO_SLL_NODE *) pAclNode);
                        }
                        MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                      "\n Filter/ Rule %s is added on Interface"
                                      "%d in the OUT Direction\n",
                                      au1AclName, u4IfaceNum);
                    }
                    INC_IFACE_CONFIG_COUNT (pIfaceNode);
                }
                else
                {
                    if (gu1FwlIfaceFlag == FWL_TRUE)
                    {
                        FwlDbaseDeleteIface (u4IfaceNum);
                    }

                }

            }
            else
            {
                if (gu1FwlIfaceFlag == FWL_TRUE)
                {
                    FwlDbaseDeleteIface (u4IfaceNum);
                }

                /* send a trap for memory failure */
                FwlGenerateMemFailureTrap (FWL_ACL_ALLOC_FAILURE);
                INC_MEM_FAILURE_COUNT;
                gFwlAclInfo.i4MemStatus = (INT4) MEM_FAILURE;
                i1Status = SNMP_FAILURE;
            }
        }
        else
        {

            i1Status = SNMP_SUCCESS;
        }
    }                            /* end for i1Status checking */
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from Function FwlSetInitForAclNode\n");

    return i1Status;
}                                /* End of the Function  --  FwlSetInitForAclNode */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlDbaseDeleteAcl                                */
/*                                                                          */
/*    Description        : Deletes the AclNode From Acl List                */
/*                                                                          */
/*    Input(s)           : au1AclName      -- String Containing the Name    */
/*                         u4IfaceNum      -- Interface number              */
/*                         i4Direction    -- specifies Direction            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : FWL_SUCCESS if node is deleted, otherwise        */
/*                         FWL_FAILURE.                                     */
/****************************************************************************/

#ifdef __STDC__
PUBLIC UINT4
FwlDbaseDeleteAcl (UINT4 u4IfaceNum,
                   INT4 i4Direction, UINT1 au1AclName[FWL_MAX_FILTER_NAME_LEN])
#else
PUBLIC UINT4
FwlDbaseDeleteAcl (u4IfaceNum, i4Direction, au1AclName)
     UINT4               u4IfaceNum = FWL_ZERO;
     INT4                i4Direction = FWL_ZERO;
     UINT1               au1AclName[FWL_MAX_FILTER_NAME_LEN] = { FWL_ZERO };
#endif
{
    UINT4               u4Status = FWL_SUCCESS;
    UINT4               u4IfaceIndex = FWL_ZERO;
    UINT1               u1StatusFlag = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;
    tInFilterInfo      *pInFilterNode = NULL;
    tOutFilterInfo     *pOutFilterNode = NULL;
    tInIpv6FilterInfo  *pInIpv6FilterNode = NULL;
    tOutIpv6FilterInfo *pOutIpv6FilterNode = NULL;

    /* initialise the interface index and status */
    pIfaceNode = (tIfaceInfo *) NULL;
    pInFilterNode = (tInFilterInfo *) NULL;
    pOutFilterNode = (tOutFilterInfo *) NULL;
    pInIpv6FilterNode = (tInIpv6FilterInfo *) NULL;
    pOutIpv6FilterNode = (tOutIpv6FilterInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY, "\n Entering into the fn. FwlDbaseDeleteAcl \n");
    /* if interface number is less than max value then search for that 
     * interface number. if direction is IN then search the infilter
     * list for the particular ACl name. if found delete it and update 
     * the statistics like decrementing the reference count and filter
     * count.  The while loop will be executed only once when the Interface
     * number is less than FWL_MAX_NUM_OF_IF.
     */
    /* CHANGE1 : If the Acl node is to be deleted globally , then the node
     * should be searched in all interfaces in the particular direction and
     * deleted.
     */
    if (u4IfaceNum < FWL_MAX_NUM_OF_IF)
    {
        u4IfaceIndex = FWL_MAX_NUM_OF_IF - FWL_ONE;
    }
    else if (u4IfaceNum == FWL_MAX_NUM_OF_IF)
    {
        u4IfaceNum = FWL_ZERO;
        u4IfaceIndex = FWL_ZERO;
    }
    while (u4IfaceIndex < FWL_MAX_NUM_OF_IF)
    {
        pIfaceNode = gFwlAclInfo.apIfaceList[u4IfaceNum];
        if (pIfaceNode != NULL)
        {
            if (i4Direction == FWL_DIRECTION_IN)
            {
                /* search the filter in the infilter list and if found,
                 * delete it and update the statistics.
                 */
                pInFilterNode = FwlDbaseSearchAclFilter
                    (&pIfaceNode->inFilterList, au1AclName);
                if (pInFilterNode != NULL)
                {
                    u4Status = FwlDbaseDelNodeAndUpdateStat (pIfaceNode,
                                                             FWL_DIRECTION_IN,
                                                             pInFilterNode,
                                                             FWL_IP_VERSION_4);
                }
                /* Node not found in infilter.Search in inIPv6FilterList */
                else
                {
                    pInIpv6FilterNode = FwlDbaseSearchAclFilter
                        (&pIfaceNode->inIPv6FilterList, au1AclName);
                    /* It is an IPv6 filter. So send the address type 
                     * to delete the corresponding list */
                    u4Status = FwlDbaseDelNodeAndUpdateStat (pIfaceNode,
                                                             FWL_DIRECTION_IN,
                                                             pInIpv6FilterNode,
                                                             FWL_IP_VERSION_6);
                }
                if (u4Status == FWL_SUCCESS)
                {
                    u1StatusFlag++;
                }
            }
            else
            {                    /* Direction is Out */
                /* search the filter in the outfilter list and if found,
                 * delete it and update the statistics.
                 */
                pOutFilterNode = FwlDbaseSearchAclFilter
                    (&pIfaceNode->outFilterList, au1AclName);
                if (pOutFilterNode != NULL)
                {
                    u4Status = FwlDbaseDelNodeAndUpdateStat (pIfaceNode,
                                                             FWL_DIRECTION_OUT,
                                                             pOutFilterNode,
                                                             FWL_IP_VERSION_4);
                }
                else
                {
                    pOutIpv6FilterNode = FwlDbaseSearchAclFilter
                        (&pIfaceNode->outIPv6FilterList, au1AclName);
                    /* It is an IPv6 filter. So send the address type
                     * to delete the corresponding list */

                    u4Status = FwlDbaseDelNodeAndUpdateStat (pIfaceNode,
                                                             FWL_DIRECTION_OUT,
                                                             pOutIpv6FilterNode,
                                                             FWL_IP_VERSION_6);
                }
                if (u4Status == FWL_SUCCESS)
                {
                    u1StatusFlag++;
                }
            }
        }
        u4IfaceNum++;
        u4IfaceIndex++;
    }
    if (u1StatusFlag > FWL_ZERO)
    {
        FwlCommitAcl ();
        u4Status = FWL_SUCCESS;
    }
    FWL_DBG (FWL_DBG_EXIT, "\n Exiting from the fn. FwlDbaseDeleteAcl \n");
    return u4Status;
}                                /* End of the function -- FwlDbaseDeleteAcl */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlSetRowStatusValueForAclNode                   */
/*                                                                          */
/*    Description        : Sets the Row Status Value for the corresponding  */
/*                         Acl Node.                                        */
/*                                                                          */
/*    Input(s)           : u4IfaceNum   -- Interface Number                 */
/*                         i4Direction  -- Direction                        */
/*                         au1AclName   -- Accesslist Name                  */
/*                         u1RowStatus  -- RowStatus Value                  */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            :  None                                            */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
FwlSetRowStatusValueForAclNode (UINT4 u4IfaceNum,
                                INT4 i4Direction,
                                UINT1 au1AclName[FWL_MAX_ACL_NAME_LEN],
                                UINT1 u1RowStatus)
#else
PUBLIC VOID
FwlSetRowStatusValueForAclNode (u4IfaceNum,
                                i4Direction, au1AclName, u1RowStatus)
     UINT4               u4IfaceNum;
     INT4                i4Direction;
     UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN];
     UINT1               u1RowStatus;
#endif
{
    tAclInfo           *pAclNode = NULL;
    tIfaceInfo         *pIfaceNode = NULL;
    UINT4               u4IfaceIndex = FWL_ZERO;

    pAclNode = (tAclInfo *) NULL;
    pIfaceNode = (tIfaceInfo *) NULL;

    FWL_DBG (FWL_DBG_ENTRY,
             "\nEntering into function FwlSetRowStatusValueForAclNode\n");
    if (u4IfaceNum < FWL_MAX_NUM_OF_IF)
    {
        u4IfaceIndex = FWL_MAX_NUM_OF_IF - FWL_ONE;
    }
    else
    {
        u4IfaceIndex = FWL_ONE;
        u4IfaceNum = FWL_ONE;
    }
    while (u4IfaceIndex < FWL_MAX_NUM_OF_IF)
    {
        /* search the interface index. if direction is IN then search the 
         * InFilter list for the corresponding filter name, else the 
         * Outfilter list. 
         */
        pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
        if (pIfaceNode != NULL)
        {
            if (i4Direction == FWL_DIRECTION_IN)
            {
                pAclNode = FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList,
                                                    au1AclName);
                if (pAclNode == NULL)
                {
                    pAclNode = FwlDbaseSearchAclFilter
                        (&pIfaceNode->inIPv6FilterList, au1AclName);
                }
            }
            else
            {
                pAclNode = FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                                    au1AclName);
                if (pAclNode == NULL)
                {
                    pAclNode = FwlDbaseSearchAclFilter
                        (&pIfaceNode->outIPv6FilterList, au1AclName);
                }
            }

            if (pAclNode == NULL)
            {
                return;
            }
            if (u1RowStatus == FWL_ACTIVE)
            {
                if ((pAclNode->u1Action != FWL_INIT) &&
                    (pAclNode->u2SeqNum != FWL_INIT))
                {
                    pAclNode->u1RowStatus = u1RowStatus;
                }
                else
                {
                    MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  " \n RowStatus cannot be made Active i"
                                  "for Acl Filter %s on interface %d "
                                  "since Action Value and Sequence Number "
                                  "value must be set. \n ",
                                  pAclNode->au1FilterName, u4IfaceNum);
                }
            }
            else
            {
                pAclNode->u1RowStatus = u1RowStatus;
            }
        }
        u4IfaceNum++;
        u4IfaceIndex++;
    }                            /*end of while */

    FWL_DBG (FWL_DBG_EXIT,
             "\nExiting from function FwlSetRowStatusValueForAclNode\n");

}                                /* End of the Function  --  FwlSetRowStatusValueForAclNode */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FwlTestRowStatusValueForAclNode                  */
/*                                                                          */
/*    Description        : Tests the Row Status Value for the corresponding */
/*                         Acl Node.                                        */
/*                                                                          */
/*    Input(s)           : u4IfaceNum   -- Interface Number                 */
/*                         i4Direction  -- Direction                        */
/*                         au1AclName   -- Accesslist Name                  */
/*                         u1RowStatus  -- RowStatus Value                  */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            :  None                                            */
/*                                                                          */
/****************************************************************************/

#ifdef __STDC__
PUBLIC INT1
FwlTestRowStatusValueForAclNode (UINT4 u4IfaceCount,
                                 UINT4 u4IfaceIndex,
                                 UINT4 u4IfaceNum,
                                 INT4 i4Direction,
                                 UINT1 au1AclName[FWL_MAX_ACL_NAME_LEN],
                                 UINT1 u1RowStatus)
#else
PUBLIC INT1
FwlTestRowStatusValueForAclNode (u4IfaceCount,
                                 u4IfaceIndex,
                                 u4IfaceNum,
                                 i4Direction, au1AclName, u1RowStatus)
     UINT4               u4IfaceCount;
     UINT4               u4IfaceIndex;
     UINT4               u4IfaceNum;
     INT4                i4Direction;
     UINT1               au1AclName[FWL_MAX_ACL_NAME_LEN];
     UINT1               u1RowStatus;
#endif
{
    INT4                i4Status = FWL_ZERO;
    tIfaceInfo         *pIfaceNode = NULL;
    tAclInfo           *pAclNode = NULL;
    UINT1               u1AddrType = FWL_ZERO;

    i4Status = SNMP_FAILURE;

    while (u4IfaceIndex < FWL_MAX_NUM_OF_IF)
    {
        /* search the interface list for the corresponding interface number */
        pIfaceNode = (tIfaceInfo *) NULL;
        pAclNode = (tAclInfo *) NULL;
        pIfaceNode = FwlDbaseSearchIface (u4IfaceNum);
        if (pIfaceNode != NULL)
        {
            if (i4Direction == FWL_DIRECTION_IN)
            {
                /* search for the infilter node. if found then validate 
                 * the action value based on Row status value.If the
                 * RowStatus is FWL_CREATE_AND_WAIT or NOT READY then return
                 * SUCCESS, else FAILURE. 
                 */
                pAclNode =
                    FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList,
                                             au1AclName);
                if (pAclNode != NULL)
                {
                    /* Node found in inFilterList */
                    u1AddrType = FWL_IP_VERSION_4;
                }
                else
                {
                    /*Node not found in inFilterList.
                     * Search in inIpv6FilterList*/
                    pAclNode =
                        FwlDbaseSearchAclFilter (&pIfaceNode->inIPv6FilterList,
                                                 au1AclName);
                    if (pAclNode != NULL)
                    {
                        u1AddrType = FWL_IP_VERSION_6;
                    }
                }
                if ((FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                              au1AclName) != NULL)
                    ||
                    (FwlDbaseSearchAclFilter
                     (&pIfaceNode->outIPv6FilterList, au1AclName) != NULL))
                {
                    /* Same ACL is present in the Out Direction */
                    CLI_SET_ERR (CLI_FWL_ACL_ALREADY_PRESENT);
                    MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                             "\nAlready an ACL with this name is present in "
                             " OUT Direction \n");
                    return SNMP_FAILURE;
                }
            }                    /*end for IN direction */
            else if (i4Direction == FWL_DIRECTION_OUT)
            {
                /* search for the outfilter node. if found then validate 
                 * the action value based on Row status value.If the
                 * RowStatus is FWL_CREATE_AND_WAIT or NOT READY then return
                 * SUCCESS, else FAILURE. 
                 */
                pAclNode =
                    FwlDbaseSearchAclFilter (&pIfaceNode->outFilterList,
                                             au1AclName);
                if (pAclNode != NULL)
                {
                    /* Node found in outFilterList */
                    u1AddrType = FWL_IP_VERSION_4;
                }
                else
                {
                    /*Node not found in outFilterList.
                     * Search in outIPv6FilterList*/
                    pAclNode =
                        FwlDbaseSearchAclFilter (&pIfaceNode->outIPv6FilterList,
                                                 au1AclName);
                    if (pAclNode != NULL)
                    {
                        u1AddrType = FWL_IP_VERSION_6;
                    }
                }
                if ((FwlDbaseSearchAclFilter (&pIfaceNode->inFilterList,
                                              au1AclName) != NULL) ||
                    (FwlDbaseSearchAclFilter
                     (&pIfaceNode->inIPv6FilterList, au1AclName) != NULL))
                {
                    /* Same ACL is present in the In Direction */
                    CLI_SET_ERR (CLI_FWL_ACL_ALREADY_PRESENT);
                    MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                             "\nAlready an ACL with this name is present in "
                             " IN Direction \n");
                    return SNMP_FAILURE;
                }
            }                    /*end for OUT direction */
            else
            {
                MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                         "\nInvalid Index - Direction value can be 1 or 2\n");
                i4Status = SNMP_FAILURE;
                break;
            }
        }
        else
        {
            if (u4IfaceCount == FWL_ONE)
            {
                u4IfaceNum++;
                u4IfaceIndex++;
                continue;
            }
        }
        switch (u1RowStatus)
        {
            case FWL_CREATE_AND_WAIT:
                if (pIfaceNode == NULL)
                {
                    /* CHANGE2: While creating the interface Index, it must 
                     * ensure that interface index exists in CFA. Else it 
                     * should not create the row. 
                     */
                    i4Status = SecUtilValidateIfIndex ((UINT2) u4IfaceNum);
                    if (i4Status == CFA_SUCCESS)
                    {
                        i4Status = SNMP_SUCCESS;
                    }
                    else
                    {
                        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                 "\nInvalid Interface Number\n");
                        i4Status = SNMP_FAILURE;
                    }
                }
                else
                {
                    if (i4Direction == FWL_DIRECTION_IN)
                    {
                        if (u1AddrType == FWL_IP_VERSION_4)
                        {
                            if (TMO_SLL_Count (&pIfaceNode->inFilterList)
                                == FWL_MAX_NUM_OF_ACL_FILTER)
                            {
                                CLI_SET_ERR (CLI_FWL_ACL_TABLE_FULL);
                                return SNMP_FAILURE;
                            }
                        }
                        else if (u1AddrType == FWL_IP_VERSION_6)
                        {
                            if (TMO_SLL_Count
                                (&pIfaceNode->inIPv6FilterList)
                                == FWL_MAX_NUM_OF_ACL_FILTER)
                            {
                                CLI_SET_ERR (CLI_FWL_ACL_TABLE_FULL);
                                return SNMP_FAILURE;
                            }
                        }
                        else
                        {
                            i4Status = SNMP_FAILURE;
                        }
                    }
                    else
                    {
                        if (u1AddrType == FWL_IP_VERSION_4)
                        {
                            if (TMO_SLL_Count (&pIfaceNode->outFilterList)
                                == FWL_MAX_NUM_OF_ACL_FILTER)
                            {
                                CLI_SET_ERR (CLI_FWL_ACL_TABLE_FULL);
                                return SNMP_FAILURE;
                            }
                        }
                        else if (u1AddrType == FWL_IP_VERSION_6)
                        {
                            if (TMO_SLL_Count
                                (&pIfaceNode->outIPv6FilterList)
                                == FWL_MAX_NUM_OF_ACL_FILTER)
                            {
                                CLI_SET_ERR (CLI_FWL_ACL_TABLE_FULL);
                                return SNMP_FAILURE;
                            }
                        }
                        else
                        {
                            i4Status = SNMP_FAILURE;
                        }
                    }

                    if (pAclNode == NULL)
                    {
                        i4Status = SNMP_SUCCESS;
                    }
                    else
                    {
                        MOD_TRC_ARG2 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                      "\nFilter / Rule %s "
                                      "is already applied on interface %d\n",
                                      au1AclName, u4IfaceNum);
                        i4Status = SNMP_FAILURE;
                    }
                }
                break;
            case FWL_ACTIVE:
                if (pAclNode != NULL)
                {
                    if ((pAclNode->u1Action != FWL_DEFAULT_ACTION) &&
                        (pAclNode->u2SeqNum != FWL_DEFAULT_SEQ_NUM))
                    {
                        i4Status = SNMP_SUCCESS;
                    }
                    else
                    {
                        MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                 "\nIncomplete Acl Filter. "
                                 "RowStatus cannot be ACTIVE\n");
                        i4Status = SNMP_FAILURE;
                    }
                }
                else
                {
                    i4Status = SNMP_FAILURE;
                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\nAcl Filter %s doesnt exist\n", au1AclName);
                }
                break;
            case FWL_NOT_IN_SERVICE:
                /* The row can be made FWL_NOT_IN_SERVICE if the row exists */
                if (pAclNode != NULL)
                {
                    i4Status = SNMP_SUCCESS;
                }
                else
                {
                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\nAcl Filter %s doesnt exist\n", au1AclName);
                    i4Status = SNMP_FAILURE;
                }
                break;
            case FWL_DESTROY:
                if (pAclNode != NULL)
                {
                    i4Status = SNMP_SUCCESS;
                }
                else
                {
                    MOD_TRC_ARG1 (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                                  "\nAcl Filter %s doesnt exist\n", au1AclName);
                    i4Status = SNMP_FAILURE;
                }
                break;
            default:
                i4Status = SNMP_FAILURE;
                MOD_TRC (gFwlAclInfo.u4FwlTrc, MGMT_TRC, "FWL",
                         "\nRowStatus can take the value 1 to 6\n");
                break;
        }                        /*end of switch */
        u4IfaceNum++;
        u4IfaceIndex++;
        if (i4Status == SNMP_FAILURE)
        {
            break;
        }
    }                            /* end of While */

    return (INT1) i4Status;

}                                /* End of the Function  --  FwlTestRowStatusValueForAclNode */
#endif

/*****************************************************************************/
/* Function Name      : ApGroupGetConfigEntry                                */
/*                                                                           */
/* Description        : Gets the pointer to the port table entry             */
/*                                                                           */
/* Input              : WlanIfIndex -                                        */
/*                                                                           */
/* Output             : pConfigInfo - double Pointer to the port table entry */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT1
ApGroupGetConfigEntry (UINT2 u2ApGroupIndex, tApGroupConfigEntry ** pConfigInfo)
{
    tApGroupConfigEntry *pTmpConfigInfo = NULL;
    tApGroupConfigEntry *pApGroupConfigEntry = NULL;

    if ((pApGroupConfigEntry = (tApGroupConfigEntry *)
         APGROUP_MEM_ALLOCATE_MEM_BLK
         (APGROUP_CONFIG_ENTRY_MEMPOOL_ID)) == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "\r%% ApGroupGetConfigEntry: "
                     "Memory allocation Failed.\r\n"));
        return FAILURE;
    }

    MEMSET (pApGroupConfigEntry, 0, sizeof (tApGroupConfigEntry));
    pApGroupConfigEntry->u2ApGroupId = u2ApGroupIndex;

    pTmpConfigInfo = RBTreeGet (gApGroupGlobals.ApGroupConfigEntryData,
                                (tRBElem *) pApGroupConfigEntry);
    if (pTmpConfigInfo == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "\r%% ApGroupGetConfigEntry: AP group not availble.\r\n"));
        APGROUP_MEM_RELEASE_MEM_BLK (APGROUP_CONFIG_ENTRY_MEMPOOL_ID,
                                     pApGroupConfigEntry);
        return FAILURE;
    }

    *pConfigInfo = pTmpConfigInfo;
    APGROUP_MEM_RELEASE_MEM_BLK (APGROUP_CONFIG_ENTRY_MEMPOOL_ID,
                                 pApGroupConfigEntry);
    return SUCCESS;
}

/****************************************************************************
 * Function Name      : ApGroupAddConfigEntry                               *
 *                                                                          *
 * Description        : This function adds the entry to the                 *
 *                   ConfigEntry                                            *
 *                                                                          *
 * Input(s)           : u2PortNum -u2Port Index                             *
 *                                                                          *
 * Output(s)          : None                                                *
 *                                                                          *
 * Return Value(s)    : SNMP_SUCCESS / SNMP_FAILURE                         *
 ***************************************************************************/
INT4
ApGroupAddConfigEntry (tApGroupConfigEntry * pApGroupConfigEntry)
{
    tApGroupConfigEntry *pApGroupEntry = NULL;

    /*Memory Allocation for the node to be added in the tree */
    if ((pApGroupEntry = (tApGroupConfigEntry *)
         APGROUP_MEM_ALLOCATE_MEM_BLK
         (APGROUP_CONFIG_ENTRY_MEMPOOL_ID)) == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "ApGroupAddConfigEntry :- MemAllocMemBlk"
                     " returned Failure \n "));
        return SNMP_FAILURE;
    }

    MEMSET (pApGroupEntry, 0, sizeof (tApGroupConfigEntry));

    pApGroupEntry->u2ApGroupId = pApGroupConfigEntry->u2ApGroupId;
    pApGroupEntry->i4ApGroupRowstatus = pApGroupConfigEntry->i4ApGroupRowstatus;

    if (gApGroupGlobals.u4NoOfAPGroups == 50)
    {
        APGROUP_MEM_RELEASE_MEM_BLK (APGROUP_CONFIG_ENTRY_MEMPOOL_ID,
                                     (UINT1 *) pApGroupEntry);
        return SNMP_FAILURE;
    }
    if (RBTreeAdd (gApGroupGlobals.ApGroupConfigEntryData,
                   (tRBElem *) pApGroupEntry) == RB_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "\r%% Memory allocation for ApGroup"
                     " selection failed.\r\n"));
        APGROUP_MEM_RELEASE_MEM_BLK (APGROUP_CONFIG_ENTRY_MEMPOOL_ID,
                                     (UINT1 *) pApGroupEntry);
        return SNMP_FAILURE;
    }
    gApGroupGlobals.u4NoOfAPGroups++;

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ApGroupDeleteConfigEntry                             */
/*                                                                           */
/* Description        : This function removes the entry from                 */
/*                      ConfigEntry                                          */
/*                                                                           */
/* Input(s)           : u4IfIndex                                            */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SUCCESS /_FAILURE                                    */
/*****************************************************************************/
INT4
ApGroupDeleteConfigEntry (UINT4 u4ApGroupIndex)
{
    tApGroupConfigEntry *pApGroupConfigEntry = NULL;
    if (ApGroupGetConfigEntry ((UINT2) u4ApGroupIndex,
                               &pApGroupConfigEntry) == FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "\r%% ApGroupDeleteConfigEntry "
                     "Memory allocation Failed.\r\n"));
        return FAILURE;
    }

    if (RBTreeRemove (gApGroupGlobals.ApGroupConfigEntryData,
                      pApGroupConfigEntry) == RB_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "\r%% RBTree Remove failed\r\n"));
        return FAILURE;
    }
    APGROUP_MEM_RELEASE_MEM_BLK (APGROUP_CONFIG_ENTRY_MEMPOOL_ID,
                                 pApGroupConfigEntry);
    return SUCCESS;
}

/*******************************************************************************/
/* Function Name      : ApGroupCompareConfigData                               */
/*                                                                             */
/* Description        : This function compares the two PortEntryNode           */
/*                      based on the port number                               */
/*                                                                             */
/* Input(s)           : e1        Pointer to First BindingIfNode               */
/* Input(s)           : e2        Pointer to Second BindingIfNode              */
/*                                                                             */
/* Output(s)          : None                                                   */
/*                                                                             */
/* Return Value(s)    : 1. ApGroup_GREATER - If key of first element is greater*/
/*                                        than key of second element           */
/*                      2. ApGroup_EQUAL   - If key of first element is equal  */
/*                                        to key of second element             */
/*                      3. ApGroup_LESSER  - If key of first element is lesser */
/*                                        than key of second element           */
/*******************************************************************************/
INT4
ApGroupCompareConfigData (tRBElem * e1, tRBElem * e2)
{
    tApGroupConfigEntry *pRBNode1 = (tApGroupConfigEntry *) e1;
    tApGroupConfigEntry *pRBNode2 = (tApGroupConfigEntry *) e2;

    if (pRBNode1->u2ApGroupId < pRBNode2->u2ApGroupId)
    {
        return APGROUP_LESSER;
    }
    else if (pRBNode1->u2ApGroupId > pRBNode2->u2ApGroupId)
    {
        return APGROUP_GREATER;
    }
    return APGROUP_EQUAL;
}

/*****************************************************************************/
/* Function     :  APGroupDBCreate                                           */
/*                                                                           */
/* Description  : This function creates the  RfMgmtExgternalAPDBCreate       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
UINT1
APGroupDBCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset = FSAP_OFFSETOF (tApGroupConfigEntry, APGroupEntryNode);
    if ((gApGroupGlobals.ApGroupConfigEntryData =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               ApGroupCompareConfigData)) == NULL)
    {
        WSSCFG_TRC (("APGroupDBCreate failed \r\n"));
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      " APGroupDBCreate failed "));
        return FAILURE;
    }
    gApGroupGlobals.u4ApGroupEnableStatus = 2;
    return SUCCESS;
}

/*********************************************************************************/
/* Function Name      : ApGroupGetFirstConfigEntry                               */
/*                                                                               */
/* Description        : This function returns pointer to the first config        */
/*                      entry                                                    */
/*                                                                               */
/* Input (s)          : NONE                                                     */
/*                                                                               */
/* Output(s)          : pApGroupConfigEntry - Pointer to the first node of config*/
/*                      entry tree                                               */
/* Return Value(s)    : None                                                     */
/*********************************************************************************/
INT1
ApGroupGetFirstConfigEntry (tApGroupConfigEntry ** pApGroupConfigEntry)
{
    tApGroupConfigEntry *pTmpApGroupConfigEntry = NULL;

    pTmpApGroupConfigEntry = (tApGroupConfigEntry *) RBTreeGetFirst
        (gApGroupGlobals.ApGroupConfigEntryData);

    if (pTmpApGroupConfigEntry == NULL)
    {
        return FAILURE;
    }

    *pApGroupConfigEntry = pTmpApGroupConfigEntry;
    return SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ApGroupGetNextConfigEntry                         */
/*                                                                           */
/* Description        : This function returns pointer to the next remote port*/
/*                      entry                                                */
/*                      If this function called with NULL as pPortEntry      */
/*                      then, pointer to first node is returned as next node */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pApGroupConfigEntry -                                */
/*                      which next node should be returned                   */
/*                                                                           */
/* Output(s)          : pNextApGroupConfigEntry - pointer to the next Node   */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT1
ApGroupGetNextConfigEntry (tApGroupConfigEntry * pApGroupConfigEntry,
                           tApGroupConfigEntry ** pNextApGroupConfigEntry)
{
    tApGroupConfigEntry *pTmpApGroupConfigEntry = NULL;

    if (pApGroupConfigEntry == NULL)
    {
        return FAILURE;
    }
    else
    {
        pTmpApGroupConfigEntry = (tApGroupConfigEntry *) RBTreeGetNext
            (gApGroupGlobals.ApGroupConfigEntryData,
             (tRBElem *) pApGroupConfigEntry, NULL);
    }

    if (pTmpApGroupConfigEntry == NULL)
    {
        return FAILURE;
    }
    *pNextApGroupConfigEntry = pTmpApGroupConfigEntry;
    return SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ApGroupGetWTPConfigEntry                             */
/*                                                                           */
/* Description        : Gets the pointer to the port table entry             */
/*                                                                           */
/* Input              : WlanIfIndex -                                        */
/*                                                                           */
/* Output             : pConfigInfo - double Pointer to the port table entry */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT1
ApGroupGetWTPConfigEntry (UINT2 u2ApGroupId, UINT2 u2ApGroupWTPIndex,
                          tApGroupConfigWTPEntry ** pConfigInfo)
{
    tApGroupConfigWTPEntry *pTmpConfigWTPInfo = NULL;
    tApGroupConfigWTPEntry *pApGroupConfigWTPEntry = NULL;

    if ((pApGroupConfigWTPEntry = (tApGroupConfigWTPEntry *)
         APGROUP_MEM_ALLOCATE_MEM_BLK (APGROUP_CONFIG_WTP_ENTRY_MEMPOOL_ID)) ==
        NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "\r%% ApGroupGetWTPConfigEntry: "
                     "Memory allocation Failed.\r\n"));
        return FAILURE;
    }

    MEMSET (pApGroupConfigWTPEntry, 0, sizeof (tApGroupConfigWTPEntry));
    pApGroupConfigWTPEntry->u2ApGroupId = u2ApGroupId;
    pApGroupConfigWTPEntry->u2ApGroupWTPIndex = u2ApGroupWTPIndex;

    pTmpConfigWTPInfo = RBTreeGet (gApGroupGlobals.ApGroupConfigWTPEntryData,
                                   (tRBElem *) pApGroupConfigWTPEntry);
    if (pTmpConfigWTPInfo == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "\r%% ApGroupGetConfigEntry: AP group not availble.\r\n"));
        APGROUP_MEM_RELEASE_MEM_BLK (APGROUP_CONFIG_WTP_ENTRY_MEMPOOL_ID,
                                     pApGroupConfigWTPEntry);
        return FAILURE;
    }

    *pConfigInfo = pTmpConfigWTPInfo;
    APGROUP_MEM_RELEASE_MEM_BLK (APGROUP_CONFIG_WTP_ENTRY_MEMPOOL_ID,
                                 pApGroupConfigWTPEntry);
    return SUCCESS;
}

/****************************************************************************
 * Function Name      : ApGroupAddConfigWTPEntry                             *
 *                                                                           *
 * Description        : This function adds the entry to the                  *
 *                      ConfigEntry                                          *
 *                                                                         *
 * Input(s)           : u2PortNum -u2Port Index                              *
 *                                                                           *
 * Output(s)          : None                                                *
 *                                                                          *
 * Return Value(s)    : SNMP_SUCCESS / SNMP_FAILURE                          *
 * *****************************************************************************/
INT4
ApGroupAddWTPConfigEntry (tApGroupConfigWTPEntry * pApGroupConfigWTPEntry)
{
    tApGroupConfigWTPEntry *pApGroupWTPEntry = NULL;
    UINT1               au1ProfileName[256];
    UINT4               u4ApGroupEnable = 0;
    UINT4               u4RadioPolicy = 0;
    UINT4               u4VlanId = 0;
    UINT4               u4AdminStatus = CREATE_AND_GO;

    tApGroupConfigEntry *pFristApGroup = NULL;
    tApGroupConfigEntry *pSecondApGroup = NULL;

    tApGroupConfigWLANEntry *pApGroupFirstConfigWLANEntry = NULL;
    tApGroupConfigWLANEntry *pApGroupNextConfigWLANEntry = NULL;

    MEMSET (au1ProfileName, 0, 256);

    /*Memory Allocation for the node to be added in the tree */
    if ((pApGroupWTPEntry = (tApGroupConfigWTPEntry *)
         APGROUP_MEM_ALLOCATE_MEM_BLK
         (APGROUP_CONFIG_WTP_ENTRY_MEMPOOL_ID)) == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "ApGroupAddConfigWTPEntry :- MemAllocMemBlk"
                     " returned Failure \n "));
        return SNMP_FAILURE;
    }
    MEMSET (pApGroupWTPEntry, 0, sizeof (tApGroupConfigWTPEntry));

    pApGroupWTPEntry->u2ApGroupId = pApGroupConfigWTPEntry->u2ApGroupId;
    pApGroupWTPEntry->u2ApGroupWTPIndex =
        pApGroupConfigWTPEntry->u2ApGroupWTPIndex;
    pApGroupWTPEntry->i4ApGroupWTPRowstatus =
        pApGroupConfigWTPEntry->i4ApGroupWTPRowstatus;

    if (CapwapGetWtpProfileNameFromProfileId (au1ProfileName,
                                              pApGroupConfigWTPEntry->
                                              u2ApGroupWTPIndex) ==
        OSIX_SUCCESS)
    {
        MEMCPY (pApGroupWTPEntry->au1WTPProfile,
                au1ProfileName, STRLEN (au1ProfileName));
    }

    WssFindTheApGroupFromWtpProfileId (pApGroupConfigWTPEntry->
                                       u2ApGroupWTPIndex, &pFristApGroup,
                                       &pSecondApGroup);

    if (WsscfgGetApGroupInterfaceVlan (pApGroupConfigWTPEntry->u2ApGroupId,
                                       &u4VlanId) == SUCCESS)
    {
        if (u4VlanId == 0)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "\r%% Group Vlan is not available. \r\n"));
            APGROUP_MEM_RELEASE_MEM_BLK (APGROUP_CONFIG_WTP_ENTRY_MEMPOOL_ID,
                                         (UINT1 *) pApGroupWTPEntry);
            return SNMP_FAILURE;
        }
    }

    /* Get the Group Radio Policy */
    WsscfgGetApGroupRadioPolicy (pApGroupConfigWTPEntry->u2ApGroupId,
                                 &u4RadioPolicy);

    if (WssCfgValidateWtpEntry (pFristApGroup,
                                pSecondApGroup, u4RadioPolicy) != OSIX_SUCCESS)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "\r%% AP Validation checks Failed. \r\n"));
        APGROUP_MEM_RELEASE_MEM_BLK (APGROUP_CONFIG_WTP_ENTRY_MEMPOOL_ID,
                                     (UINT1 *) pApGroupWTPEntry);
        return SNMP_FAILURE;
    }

    if (RBTreeAdd (gApGroupGlobals.ApGroupConfigWTPEntryData,
                   (tRBElem *) pApGroupWTPEntry) == RB_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "\r%% Memory allocation for WTP Entry"
                     " selection failed.\r\n"));
        APGROUP_MEM_RELEASE_MEM_BLK (APGROUP_CONFIG_WTP_ENTRY_MEMPOOL_ID,
                                     (UINT1 *) pApGroupWTPEntry);
        return SNMP_FAILURE;
    }
    /* Binding the group Wlans when ApGroup feature is enabled */
    WsscfgGetApGroupEnable (&u4ApGroupEnable);
    if (u4ApGroupEnable == CLI_APGROUP_ENABLE)
    {
        if (ApGroupGetFirstConfigWLANEntry
            (&pApGroupFirstConfigWLANEntry) == FAILURE)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         " None of the WLAN Profiles are confiugred  "));
        }
        else
        {
            do
            {
                pApGroupNextConfigWLANEntry = pApGroupFirstConfigWLANEntry;
                if (pApGroupNextConfigWLANEntry->u2ApGroupId ==
                    pApGroupConfigWTPEntry->u2ApGroupId)
                {
                    WsscfgApGroupBinding (pApGroupConfigWTPEntry->
                                          u2ApGroupWTPIndex,
                                          pApGroupNextConfigWLANEntry->u4WlanId,
                                          u4RadioPolicy, &u4AdminStatus);
                }
            }
            while (ApGroupGetNextWLANConfigEntry (pApGroupNextConfigWLANEntry,
                                                  &pApGroupFirstConfigWLANEntry)
                   == SUCCESS);
        }
    }
    return SNMP_SUCCESS;
}

/*******************************************************************************/
/* Function Name      : ApGroupCompareWTPConfigData                            */
/*                                                                             */
/* Description        : This function compares the two PortEntryNode           */
/*                      based on the port number                               */
/*                                                                             */
/* Input(s)           : e1        Pointer to First BindingIfNode               */
/* Input(s)           : e2        Pointer to Second BindingIfNode              */
/*                                                                             */
/* Output(s)          : None                                                   */
/*                                                                             */
/* Return Value(s)    : 1. ApGroup_GREATER - If key of first element is greater*/
/*                                        than key of second element           */
/*                      2. ApGroup_EQUAL   - If key of first element is equal  */
/*                                        to key of second element             */
/*                      3. ApGroup_LESSER  - If key of first element is lesser */
/*                                        than key of second element           */
/*******************************************************************************/
INT4
ApGroupCompareConfigWTPData (tRBElem * e1, tRBElem * e2)
{
    tApGroupConfigWTPEntry *pRBNode1 = (tApGroupConfigWTPEntry *) e1;
    tApGroupConfigWTPEntry *pRBNode2 = (tApGroupConfigWTPEntry *) e2;

    if (pRBNode1->u2ApGroupId < pRBNode2->u2ApGroupId)
    {
        return APGROUP_LESSER;
    }
    else if (pRBNode1->u2ApGroupId > pRBNode2->u2ApGroupId)
    {
        return APGROUP_GREATER;
    }
    if (pRBNode1->u2ApGroupWTPIndex < pRBNode2->u2ApGroupWTPIndex)
    {
        return APGROUP_LESSER;
    }
    else if (pRBNode1->u2ApGroupWTPIndex > pRBNode2->u2ApGroupWTPIndex)
    {
        return APGROUP_GREATER;
    }
    return APGROUP_EQUAL;
}

/*****************************************************************************/
/* Function     :  APGroupWTPDBCreate                                        */
/*                                                                           */
/* Description  : This function creates the  RfMgmtExgternalAPDBCreate       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
UINT1
APGroupWTPDBCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;

    u4RBNodeOffset =
        FSAP_OFFSETOF (tApGroupConfigWTPEntry, APGroupWTPEntryNode);
    if ((gApGroupGlobals.ApGroupConfigWTPEntryData =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               ApGroupCompareConfigWTPData)) == NULL)
    {
        WSSCFG_TRC ("APGroupWTPDBCreate failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      " APGroupDBCreate failed "));
        return FAILURE;
    }
    return SUCCESS;
}

/*********************************************************************************/
/* Function Name      : ApGroupGetFirstConfigWTPEntry                            */
/*                                                                               */
/* Description        : This function returns pointer to the first config        */
/*                      entry                                                    */
/*                                                                               */
/* Input (s)          : NONE                                                     */
/*                                                                               */
/* Output(s)          : pApGroupConfigWTPEntry -Pointer to the first node of config*/
/*                      entry tree                                               */
/* Return Value(s)    : None                                                     */
/*********************************************************************************/
INT1
ApGroupGetFirstConfigWTPEntry (tApGroupConfigWTPEntry ** pApGroupConfigWTPEntry)
{
    tApGroupConfigWTPEntry *pTmpApGroupConfigWTPEntry = NULL;

    pTmpApGroupConfigWTPEntry = (tApGroupConfigWTPEntry *) RBTreeGetFirst
        (gApGroupGlobals.ApGroupConfigWTPEntryData);

    if (pTmpApGroupConfigWTPEntry == NULL)
    {
        return FAILURE;
    }

    *pApGroupConfigWTPEntry = pTmpApGroupConfigWTPEntry;
    return SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ApGroupGetNextConfigWTPEntry                         */
/*                                                                           */
/* Description        : This function returns pointer to the next remote port*/
/*                      entry                                                */
/*                      If this function called with NULL as pPortEntry      */
/*                      then, pointer to first node is returned as next node */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pApGroupConfigWTPEntry                               */
/*                      which next node should be returned                   */
/*                                                                           */
/* Output(s)          : pNextApGroupConfigWTPEntry - pointer to the next Node  */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT1
ApGroupGetNextConfigWTPEntry (tApGroupConfigWTPEntry * pApGroupConfigWTPEntry,
                              tApGroupConfigWTPEntry **
                              pNextApGroupConfigWTPEntry)
{
    tApGroupConfigWTPEntry *pTmpApGroupConfigWTPEntry = NULL;

    if (pApGroupConfigWTPEntry == NULL)
    {
        return FAILURE;
    }
    else
    {
        pTmpApGroupConfigWTPEntry = (tApGroupConfigWTPEntry *) RBTreeGetNext
            (gApGroupGlobals.ApGroupConfigWTPEntryData,
             (tRBElem *) pApGroupConfigWTPEntry, NULL);
    }

    if (pTmpApGroupConfigWTPEntry == NULL)
    {
        return FAILURE;
    }
    *pNextApGroupConfigWTPEntry = pTmpApGroupConfigWTPEntry;
    return SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ApGroupGetConfigWLANEntry                                */
/*                                                                           */
/* Description        : Gets the pointer to the port table entry             */
/*                                                                           */
/* Input              : WlanIfIndex -                                        */
/*                                                                           */
/* Output             : pConfigInfo - double Pointer to the port table entry */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

INT1
ApGroupGetConfigWLANEntry (UINT2 u2ApGroupWLANIndex, UINT2 u2WlanProfileId,
                           tApGroupConfigWLANEntry ** pConfigInfo)
{
    tApGroupConfigWLANEntry *pTmpConfigWLANInfo = NULL;
    tApGroupConfigWLANEntry *pApGroupConfigWLANEntry = NULL;

    if ((pApGroupConfigWLANEntry = (tApGroupConfigWLANEntry *)
         APGROUP_MEM_ALLOCATE_MEM_BLK
         (APGROUP_CONFIG_WLAN_ENTRY_MEMPOOL_ID)) == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "\r%% ApGroupGetConfigWLANEntry: "
                     "Memory allocation Failed.\r\n"));
        return FAILURE;
    }

    MEMSET (pApGroupConfigWLANEntry, 0, sizeof (tApGroupConfigWLANEntry));
    pApGroupConfigWLANEntry->u2ApGroupId = u2ApGroupWLANIndex;
    pApGroupConfigWLANEntry->u4WlanId = u2WlanProfileId;

    pTmpConfigWLANInfo = RBTreeGet (gApGroupGlobals.ApGroupConfigWLANEntryData,
                                    (tRBElem *) pApGroupConfigWLANEntry);
    if (pTmpConfigWLANInfo == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "\r ApGroupGetConfigWLANEntry: AP group not availble.\r\n"));
        APGROUP_MEM_RELEASE_MEM_BLK (APGROUP_CONFIG_WLAN_ENTRY_MEMPOOL_ID,
                                     pApGroupConfigWLANEntry);
        return FAILURE;
    }

    *pConfigInfo = pTmpConfigWLANInfo;
    APGROUP_MEM_RELEASE_MEM_BLK (APGROUP_CONFIG_WLAN_ENTRY_MEMPOOL_ID,
                                 pApGroupConfigWLANEntry);
    return SUCCESS;
}

/****************************************************************************
 * Function Name      : ApGroupAddConfigWLANEntry                           *
 *                                                                          *
 * Description        : This function adds the entry to the                 *
 *                 ConfigEntry                                              *
 *                                                                          *
 * Input(s)           : u2PortNum -u2Port Index                             *
 *                                                                          *
 * Output(s)          : None                                                *
 *                                                                          *
 * Return Value(s)    : SNMP_SUCCESS / SNMP_FAILURE                         *
 ****************************************************************************/

INT4
ApGroupAddConfigWLANEntry (tApGroupConfigWLANEntry * pApGroupConfigWLANEntry)
{
    tApGroupConfigWLANEntry *pApGroupWLANEntry = NULL;
    tApGroupConfigWTPEntry *pApGroupFirstConfigWTPEntry = NULL;
    tApGroupConfigWTPEntry *pApGroupNextConfigWTPEntry = NULL;
    tApGroupConfigEntry *pApGroupConfigEntry = NULL;

    UINT4               u4ApGroupEnable = 0;
    UINT4               u4RadioPolicy = 0;
    UINT4               u4AdminStatus = CREATE_AND_GO;

    /*Memory Allocation for the node to be added in the tree */
    if ((pApGroupWLANEntry = (tApGroupConfigWLANEntry *)
         APGROUP_MEM_ALLOCATE_MEM_BLK
         (APGROUP_CONFIG_WLAN_ENTRY_MEMPOOL_ID)) == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "ApGroupAddConfigWLANEntry :- MemAllocMemBlk"
                     " returned Failure \n "));
        return SNMP_FAILURE;
    }

    MEMSET (pApGroupWLANEntry, 0, sizeof (tApGroupConfigWLANEntry));

    pApGroupWLANEntry->u2ApGroupId = pApGroupConfigWLANEntry->u2ApGroupId;
    pApGroupWLANEntry->u4WlanId = pApGroupConfigWLANEntry->u4WlanId;
    pApGroupWLANEntry->u2ApGroupWLANIndex =
        pApGroupConfigWLANEntry->u2ApGroupWLANIndex;
    pApGroupWLANEntry->i4ApGroupWLANRowstatus =
        pApGroupConfigWLANEntry->i4ApGroupWLANRowstatus;
    MEMCPY (pApGroupWLANEntry->au1WlanProfileName,
            pApGroupConfigWLANEntry->au1WlanProfileName,
            sizeof (pApGroupConfigWLANEntry->au1WlanProfileName));

    if (ApGroupGetConfigEntry (pApGroupConfigWLANEntry->u2ApGroupId,
                               &pApGroupConfigEntry) == FAILURE)
    {
        APGROUP_MEM_RELEASE_MEM_BLK (APGROUP_CONFIG_WLAN_ENTRY_MEMPOOL_ID,
                                     (UINT1 *) pApGroupWLANEntry);
        return FAILURE;
    }

    if (RBTreeAdd (gApGroupGlobals.ApGroupConfigWLANEntryData,
                   (tRBElem *) pApGroupWLANEntry) == RB_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "\r%% Memory allocation for Ap Group WLAN"
                     " Entry.\r\n"));
        APGROUP_MEM_RELEASE_MEM_BLK (APGROUP_CONFIG_WLAN_ENTRY_MEMPOOL_ID,
                                     (UINT1 *) pApGroupWLANEntry);
        return SNMP_FAILURE;
    }

    pApGroupConfigEntry->u2NoOfWlan++;
    /* Bind the WLAN to configured AP's in Group */
    WsscfgGetApGroupEnable (&u4ApGroupEnable);
    if (u4ApGroupEnable == CLI_APGROUP_ENABLE)
    {
        if (ApGroupGetFirstConfigWTPEntry
            (&pApGroupFirstConfigWTPEntry) == FAILURE)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         " None of the AP Profiles are configured\n"));
        }
        else
        {
            do
            {
                pApGroupNextConfigWTPEntry = pApGroupFirstConfigWTPEntry;
                if (pApGroupNextConfigWTPEntry->u2ApGroupId ==
                    pApGroupConfigWLANEntry->u2ApGroupId)
                {
                    WsscfgGetApGroupRadioPolicy (pApGroupConfigWLANEntry->
                                                 u2ApGroupId, &u4RadioPolicy);
                    WsscfgApGroupBinding (pApGroupNextConfigWTPEntry->
                                          u2ApGroupWTPIndex,
                                          pApGroupConfigWLANEntry->u4WlanId,
                                          u4RadioPolicy, &u4AdminStatus);
                }

            }
            while (ApGroupGetNextConfigWTPEntry (pApGroupNextConfigWTPEntry,
                                                 &pApGroupFirstConfigWTPEntry)
                   == SUCCESS);
        }
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ApGroupDeleteWLANConfigEntry                         */
/*                                                                           */
/* Description        : This function removes the entry from                 */
/*                      ConfigEntry                                          */
/*                                                                           */
/* Input(s)           : u4IfIndex                                            */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SUCCESS /_FAILURE                                    */
/*****************************************************************************/
INT4
ApGroupDeleteConfigWLANEntry (UINT4 u4ApGroupWLANIndex, UINT4 u4WlanProfileId)
{
    tApGroupConfigWLANEntry *pApGroupConfigWLANEntry = NULL;
    tApGroupConfigWTPEntry *pApGroupFirstConfigWTPEntry = NULL;
    tApGroupConfigWTPEntry *pApGroupNextConfigWTPEntry = NULL;
    tApGroupConfigEntry *pApGroupConfigEntry = NULL;

    UINT4               u4ApGroupEnable = 0;
    UINT4               u4RadioPolicy = 0;
    UINT4               u4AdminStatus = DESTROY;

    if (ApGroupGetConfigWLANEntry ((UINT2) u4ApGroupWLANIndex,
                                   u4WlanProfileId,
                                   &pApGroupConfigWLANEntry) == FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "\r%% Memory allocation for ApGroup" " WLAN Entry .\r\n"));
        return FAILURE;
    }

    /* Clear the bindings */
    WsscfgGetApGroupEnable (&u4ApGroupEnable);
    if (u4ApGroupEnable == CLI_APGROUP_ENABLE)
    {
        if (ApGroupGetFirstConfigWTPEntry
            (&pApGroupFirstConfigWTPEntry) == FAILURE)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         " None of the AP Profiles are configured\n"));
        }
        else
        {
            do
            {
                pApGroupNextConfigWTPEntry = pApGroupFirstConfigWTPEntry;
                if (pApGroupNextConfigWTPEntry->u2ApGroupId ==
                    u4ApGroupWLANIndex)
                {
                    WsscfgGetApGroupRadioPolicy (u4ApGroupWLANIndex,
                                                 &u4RadioPolicy);
                    WsscfgApGroupBinding (pApGroupNextConfigWTPEntry->
                                          u2ApGroupWTPIndex,
                                          u4WlanProfileId, u4RadioPolicy,
                                          &u4AdminStatus);
                }
            }
            while (ApGroupGetNextConfigWTPEntry (pApGroupNextConfigWTPEntry,
                                                 &pApGroupFirstConfigWTPEntry)
                   == SUCCESS);
        }
    }

    if (RBTreeRemove (gApGroupGlobals.ApGroupConfigWLANEntryData,
                      pApGroupConfigWLANEntry) == RB_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "\r%% RBTree Remove failed\r\n"));
        return FAILURE;
    }

    if (ApGroupGetConfigEntry ((UINT2) u4ApGroupWLANIndex,
                               &pApGroupConfigEntry) == FAILURE)
    {
        APGROUP_MEM_RELEASE_MEM_BLK (APGROUP_CONFIG_WLAN_ENTRY_MEMPOOL_ID,
                                     (UINT1 *) pApGroupConfigWLANEntry);
        return FAILURE;
    }

    if (pApGroupConfigEntry->u2NoOfWlan)
    {
        pApGroupConfigEntry->u2NoOfWlan--;
    }

    APGROUP_MEM_RELEASE_MEM_BLK (APGROUP_CONFIG_WLAN_ENTRY_MEMPOOL_ID,
                                 pApGroupConfigWLANEntry);
    return SUCCESS;
}

/*******************************************************************************/
/* Function Name      : ApGroupCompareWLANConfigData                           */
/*                                                                             */
/* Description        : This function compares the two PortEntryNode           */
/*                      based on the port number                               */
/*                                                                             */
/* Input(s)           : e1        Pointer to First BindingIfNode               */
/* Input(s)           : e2        Pointer to Second BindingIfNode              */
/*                                                                             */
/* Output(s)          : None                                                   */
/*                                                                             */
/* Return Value(s)    : 1. ApGroup_GREATER - If key of first element is greater*/
/*                                        than key of second element           */
/*                      2. ApGroup_EQUAL   - If key of first element is equal  */
/*                                        to key of second element             */
/*                      3. ApGroup_LESSER  - If key of first element is lesser */
/*                                        than key of second element           */
/*******************************************************************************/
INT4
ApGroupCompareWLANConfigData (tRBElem * e1, tRBElem * e2)
{
    tApGroupConfigWLANEntry *pRBNode1 = (tApGroupConfigWLANEntry *) e1;
    tApGroupConfigWLANEntry *pRBNode2 = (tApGroupConfigWLANEntry *) e2;

    if (pRBNode1->u2ApGroupId < pRBNode2->u2ApGroupId)
    {
        return APGROUP_LESSER;
    }
    else if (pRBNode1->u2ApGroupId > pRBNode2->u2ApGroupId)
    {
        return APGROUP_GREATER;
    }
    if (pRBNode1->u4WlanId < pRBNode2->u4WlanId)
    {
        return APGROUP_LESSER;
    }
    else if (pRBNode1->u4WlanId > pRBNode2->u4WlanId)
    {
        return APGROUP_GREATER;
    }
    return APGROUP_EQUAL;
}

/*****************************************************************************/
/* Function     :  APGroupWLANDBCreate                                       */
/*                                                                           */
/* Description  : This function creates the  RfMgmtExgternalAPDBCreate       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RFMGMT_SUCCESS or RFMGMT_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
UINT1
APGroupWLANDBCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tApGroupConfigWLANEntry, APGroupWLANEntryNode);
    if ((gApGroupGlobals.ApGroupConfigWLANEntryData =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               ApGroupCompareWLANConfigData)) == NULL)
    {
        WSSCFG_TRC ("APGroupDBCreate failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4RfmSysLogId,
                      " APGroupWLANDBCreate failed "));
        return FAILURE;
    }
    return SUCCESS;
}

/*********************************************************************************/
/* Function Name      : ApGroupGetFirstConfigEntry                               */
/*                                                                               */
/* Description        : This function returns pointer to the first config        */
/*                      entry                                                    */
/*                                                                               */
/* Input (s)          : NONE                                                     */
/*                                                                               */
/* Output(s)          : pApGroupConfigEntry - Pointer to the first node of config*/
/*                      entry tree                                               */
/* Return Value(s)    : None                                                     */
/*********************************************************************************/
INT1
ApGroupGetFirstConfigWLANEntry (tApGroupConfigWLANEntry
                                ** pApGroupConfigWLANEntry)
{
    tApGroupConfigWLANEntry *pTmpApGroupConfigWLANEntry = NULL;

    pTmpApGroupConfigWLANEntry =
        (tApGroupConfigWLANEntry *) RBTreeGetFirst
        (gApGroupGlobals.ApGroupConfigWLANEntryData);

    if (pTmpApGroupConfigWLANEntry == NULL)
    {
        return FAILURE;
    }

    *pApGroupConfigWLANEntry = pTmpApGroupConfigWLANEntry;
    return SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ApGroupGetNextWLANConfigEntry                        */
/*                                                                           */
/* Description        : This function returns pointer to the next remote port*/
/*                      entry                                                */
/*                      If this function called with NULL as pPortEntry      */
/*                      then, pointer to first node is returned as next node */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pApGroupConfigEntry -                                */
/*                      which next node should be returned                   */
/*                                                                           */
/* Output(s)          : pNextApGroupConfigEntry - pointer to the next Node  */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT1
ApGroupGetNextWLANConfigEntry (tApGroupConfigWLANEntry *
                               pApGroupConfigWLANEntry,
                               tApGroupConfigWLANEntry **
                               pNextApGroupConfigWLANEntry)
{
    tApGroupConfigWLANEntry *pTmpApGroupConfigWLANEntry = NULL;

    if (pApGroupConfigWLANEntry == NULL)
    {
        return FAILURE;
    }
    else
    {
        pTmpApGroupConfigWLANEntry = (tApGroupConfigWLANEntry *) RBTreeGetNext
            (gApGroupGlobals.ApGroupConfigWLANEntryData,
             (tRBElem *) pApGroupConfigWLANEntry, NULL);
    }

    if (pTmpApGroupConfigWLANEntry == NULL)
    {
        return FAILURE;
    }
    *pNextApGroupConfigWLANEntry = pTmpApGroupConfigWLANEntry;

    return SUCCESS;
}

INT4
WsscfgApGroupShowRunningConfig (tCliHandle CliHandle)
{
#ifdef WLC_WANTED

    INT4                i4ApGroupEnabledStatus = 0;
    UINT4               u4GroupIndex = 0;
    UINT4               u4NextGroupIndex = 0;
    UINT4               u4IntVlan = 0;
    UINT4               u4RadioPolicy = 0;

    UINT4               u4GroupWlanIndex = 0;
    UINT4               u4NextGroupWlanIndex = 0;
    UINT4               u4WlanId = 0;
    UINT4               u4NextWlanId = 0;

    UINT4               u4GroupWtpIndex = 0;
    UINT4               u4NextGroupWtpIndex = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4NextWtpProfileId = 0;

    tSNMP_OCTET_STRING_TYPE GroupName;
    tSNMP_OCTET_STRING_TYPE GroupDesName;
    UINT1               au1ApGroupName[32];
    UINT1               au1ApGroupNameDes[128];

    UINT1               au1ProfileName[256];
    MEMSET (&GroupName, 0, sizeof (GroupName));
    MEMSET (&GroupDesName, 0, sizeof (GroupName));

    GroupName.pu1_OctetList = au1ApGroupName;
    GroupDesName.pu1_OctetList = au1ApGroupNameDes;
    if (nmhGetFsApGroupEnabledStatus (&i4ApGroupEnabledStatus) == SNMP_SUCCESS)
    {
        if (i4ApGroupEnabledStatus == CLI_APGROUP_DISABLE)
        {
            CliPrintf (CliHandle, "ap group disable \r\n");
        }
        else if (i4ApGroupEnabledStatus == CLI_APGROUP_ENABLE)
        {
            CliPrintf (CliHandle, "ap group enable \r\n");
        }
    }

    if (nmhGetFirstIndexFsApGroupTable (&u4GroupIndex) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    else
    {
        do
        {
            u4NextGroupIndex = u4GroupIndex;

            MEMSET (au1ApGroupName, 0, sizeof (au1ApGroupName));
            nmhGetFsApGroupName (u4NextGroupIndex, &GroupName);
            CliPrintf (CliHandle, "wlan apgroup add %d %s \r\n",
                       u4NextGroupIndex, GroupName.pu1_OctetList);

            MEMSET (au1ApGroupNameDes, 0, sizeof (au1ApGroupName));
            nmhGetFsApGroupNameDescription (u4NextGroupIndex, &GroupDesName);
            CliPrintf (CliHandle, "wlan apgroup descriptions %s %s\r\n",
                       GroupName.pu1_OctetList, GroupDesName.pu1_OctetList);

            if (nmhGetFsApGroupInterfaceVlan (u4NextGroupIndex, &u4IntVlan) !=
                SNMP_SUCCESS)
            {
            }
            CliPrintf (CliHandle, "wlan apgroup interface-vlan %s %d \r\n",
                       GroupName.pu1_OctetList, u4IntVlan);

            nmhGetFsApGroupRadioPolicy (u4NextGroupIndex, &u4RadioPolicy);
            if (u4RadioPolicy == CLI_WTP_RADIO_ALL)
            {
                CliPrintf (CliHandle,
                           "wlan apgroup wlan-radio-policy %s %s \r\n",
                           GroupName.pu1_OctetList, "dot11all");
            }
            else if (u4RadioPolicy == CLI_RADIO_TYPEB)
            {
                CliPrintf (CliHandle,
                           "wlan apgroup wlan-radio-policy %s %s \r\n",
                           GroupName.pu1_OctetList, "dot11b");
            }
            else if (u4RadioPolicy == CLI_RADIO_TYPEA)
            {
                CliPrintf (CliHandle,
                           "wlan apgroup wlan-radio-policy %s %s \r\n",
                           GroupName.pu1_OctetList, "dot11a");
            }

            /* list of AP profiles */
            if (nmhGetFirstIndexFsApGroupWTPTable (&u4GroupWtpIndex,
                                                   &u4WtpProfileId) !=
                SNMP_FAILURE)
            {
                do
                {
                    u4NextGroupWtpIndex = u4GroupWtpIndex;
                    u4NextWtpProfileId = u4WtpProfileId;
                    if (u4NextGroupWtpIndex == u4NextGroupIndex)
                    {
                        MEMSET (au1ProfileName, 0, 256);
                        CapwapGetWtpProfileNameFromProfileId (au1ProfileName,
                                                              u4NextWtpProfileId);
                        CliPrintf (CliHandle,
                                   "wlan apgroup profile-mapping add %s %s \r\n",
                                   GroupName.pu1_OctetList, au1ProfileName);
                    }
                }
                while (nmhGetNextIndexFsApGroupWTPTable (u4NextGroupWtpIndex,
                                                         &u4GroupWtpIndex,
                                                         u4NextWtpProfileId,
                                                         &u4WtpProfileId) ==
                       SNMP_SUCCESS);
            }
            /* list of Wlan Profiles */
            if (nmhGetFirstIndexFsApGroupWLANTable (&u4GroupWlanIndex,
                                                    &u4WlanId) != SNMP_FAILURE)
            {
                do
                {
                    u4NextGroupWlanIndex = u4GroupWlanIndex;
                    u4NextWlanId = u4WlanId;
                    if (u4NextGroupWlanIndex == u4NextGroupIndex)
                    {
                        CliPrintf (CliHandle,
                                   "wlan apgroup interface-mapping add %s %d \r\n",
                                   GroupName.pu1_OctetList, u4NextWlanId);
                    }
                }
                while (nmhGetNextIndexFsApGroupWLANTable (u4NextGroupWlanIndex,
                                                          &u4GroupWlanIndex,
                                                          u4NextWlanId,
                                                          &u4WlanId));
            }
        }
        while (nmhGetNextIndexFsApGroupTable (u4NextGroupIndex, &u4GroupIndex)
               == SNMP_SUCCESS);
    }

#else
    UNUSED_PARAM (CliHandle);
#endif
    return CLI_SUCCESS;
}

#endif
