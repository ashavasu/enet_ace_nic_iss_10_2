#ifndef __WSSCFGWTPCLIG_C__
#define __WSSCFGWTPCLIG_C__
/********************************************************************
* Copyright (C) 2013  Aricent Inc . All Rights Reserved
*
 *  $Id: wsscfgwtpclig.c,v 1.4 2017/11/24 10:37:09 siva Exp $
*
* Description: This file contains the Wsscfg CLI related routines 
*********************************************************************/

#include "wsscfginc.h"
#include "capwapclilwg.h"
#include "capwapdef.h"
#include "wsscfgcli.h"
#include "capwapcli.h"
#include "tftpc.h"
#include "wssifpmdb.h"
#include "wsspm.h"
#include "wssifstawtptdfs.h"

extern tWssClientSummary gaWssClientSummary[MAX_STA_SUPP_PER_WLC];
extern UINT4        gu4ClientWalkIndex;
extern tWssStaWtpDBWalk gaWssStaWtpDBWalk[MAX_STA_SUPP_PER_AP];
extern UINT4        gu4WtpClientWalkIndex;
typedef UINT1       tBitList[16];

/****************************************************************************
 * Function    :  cli_process_Wsscfg_cmd
 * Description :  This function is exported to CLI module to handle the
                WSSCFG cli commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
cli_process_Wsscfg_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[WSSCFG_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT4               u4CmdType = 0;
    INT4                i4Inst;
    UINT1               au1ProfileName[CAPWAP_WTP_DFLT_PROF_NAME_LEN];
    tWsscfgDot11StationConfigEntry WsscfgSetDot11StationConfigEntry;
    tWsscfgIsSetDot11StationConfigEntry WsscfgIsSetDot11StationConfigEntry;

    tWsscfgDot11AuthenticationAlgorithmsEntry
        WsscfgSetDot11AuthenticationAlgorithmsEntry;
    tWsscfgIsSetDot11AuthenticationAlgorithmsEntry
        WsscfgIsSetDot11AuthenticationAlgorithmsEntry;

    tWsscfgDot11WEPDefaultKeysEntry WsscfgSetDot11WEPDefaultKeysEntry;
    tWsscfgIsSetDot11WEPDefaultKeysEntry WsscfgIsSetDot11WEPDefaultKeysEntry;

    tWsscfgDot11WEPKeyMappingsEntry WsscfgSetDot11WEPKeyMappingsEntry;
    tWsscfgIsSetDot11WEPKeyMappingsEntry WsscfgIsSetDot11WEPKeyMappingsEntry;

    tWsscfgDot11PrivacyEntry WsscfgSetDot11PrivacyEntry;
    tWsscfgIsSetDot11PrivacyEntry WsscfgIsSetDot11PrivacyEntry;

    tWsscfgDot11MultiDomainCapabilityEntry
        WsscfgSetDot11MultiDomainCapabilityEntry;
    tWsscfgIsSetDot11MultiDomainCapabilityEntry
        WsscfgIsSetDot11MultiDomainCapabilityEntry;

    tWsscfgDot11SpectrumManagementEntry WsscfgSetDot11SpectrumManagementEntry;
    tWsscfgIsSetDot11SpectrumManagementEntry
        WsscfgIsSetDot11SpectrumManagementEntry;

    tWsscfgDot11RegulatoryClassesEntry WsscfgSetDot11RegulatoryClassesEntry;
    tWsscfgIsSetDot11RegulatoryClassesEntry
        WsscfgIsSetDot11RegulatoryClassesEntry;

    tWsscfgDot11OperationEntry WsscfgSetDot11OperationEntry;
    tWsscfgIsSetDot11OperationEntry WsscfgIsSetDot11OperationEntry;

    tWsscfgDot11GroupAddressesEntry WsscfgSetDot11GroupAddressesEntry;
    tWsscfgIsSetDot11GroupAddressesEntry WsscfgIsSetDot11GroupAddressesEntry;

    tWsscfgDot11EDCAEntry WsscfgSetDot11EDCAEntry;
    tWsscfgIsSetDot11EDCAEntry WsscfgIsSetDot11EDCAEntry;

    tWsscfgDot11QAPEDCAEntry WsscfgSetDot11QAPEDCAEntry;
    tWsscfgIsSetDot11QAPEDCAEntry WsscfgIsSetDot11QAPEDCAEntry;

    tWsscfgDot11PhyOperationEntry WsscfgSetDot11PhyOperationEntry;
    tWsscfgIsSetDot11PhyOperationEntry WsscfgIsSetDot11PhyOperationEntry;

    tWsscfgDot11PhyAntennaEntry WsscfgSetDot11PhyAntennaEntry;
    tWsscfgIsSetDot11PhyAntennaEntry WsscfgIsSetDot11PhyAntennaEntry;

    tWsscfgDot11PhyTxPowerEntry WsscfgSetDot11PhyTxPowerEntry;
    tWsscfgIsSetDot11PhyTxPowerEntry WsscfgIsSetDot11PhyTxPowerEntry;

    tWsscfgDot11PhyFHSSEntry WsscfgSetDot11PhyFHSSEntry;
    tWsscfgIsSetDot11PhyFHSSEntry WsscfgIsSetDot11PhyFHSSEntry;

    tWsscfgDot11PhyDSSSEntry WsscfgSetDot11PhyDSSSEntry;
    tWsscfgIsSetDot11PhyDSSSEntry WsscfgIsSetDot11PhyDSSSEntry;

    tWsscfgDot11PhyIREntry WsscfgSetDot11PhyIREntry;
    tWsscfgIsSetDot11PhyIREntry WsscfgIsSetDot11PhyIREntry;

    tWsscfgDot11AntennasListEntry WsscfgSetDot11AntennasListEntry;
    tWsscfgIsSetDot11AntennasListEntry WsscfgIsSetDot11AntennasListEntry;

    tWsscfgDot11PhyOFDMEntry WsscfgSetDot11PhyOFDMEntry;
    tWsscfgIsSetDot11PhyOFDMEntry WsscfgIsSetDot11PhyOFDMEntry;

    tWsscfgDot11HoppingPatternEntry WsscfgSetDot11HoppingPatternEntry;
    tWsscfgIsSetDot11HoppingPatternEntry WsscfgIsSetDot11HoppingPatternEntry;

    tWsscfgDot11PhyERPEntry WsscfgSetDot11PhyERPEntry;
    tWsscfgIsSetDot11PhyERPEntry WsscfgIsSetDot11PhyERPEntry;

    tWsscfgDot11RSNAConfigEntry WsscfgSetDot11RSNAConfigEntry;
    tWsscfgIsSetDot11RSNAConfigEntry WsscfgIsSetDot11RSNAConfigEntry;

    tWsscfgDot11RSNAConfigPairwiseCiphersEntry
        WsscfgSetDot11RSNAConfigPairwiseCiphersEntry;
    tWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry
        WsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry;

    tWsscfgDot11RSNAConfigAuthenticationSuitesEntry
        WsscfgSetDot11RSNAConfigAuthenticationSuitesEntry;
    tWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry
        WsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry;

    tWsscfgFsDot11StationConfigEntry WsscfgSetFsDot11StationConfigEntry;
    tWsscfgIsSetFsDot11StationConfigEntry WsscfgIsSetFsDot11StationConfigEntry;

    tWsscfgFsDot11CapabilityProfileEntry WsscfgSetFsDot11CapabilityProfileEntry;
    tWsscfgIsSetFsDot11CapabilityProfileEntry
        WsscfgIsSetFsDot11CapabilityProfileEntry;

    tWsscfgFsDot11AuthenticationProfileEntry
        WsscfgSetFsDot11AuthenticationProfileEntry;
    tWsscfgIsSetFsDot11AuthenticationProfileEntry
        WsscfgIsSetFsDot11AuthenticationProfileEntry;

    tWsscfgFsSecurityWebAuthGuestInfoEntry
        WsscfgSetFsSecurityWebAuthGuestInfoEntry;
    tWsscfgIsSetFsSecurityWebAuthGuestInfoEntry
        WsscfgIsSetFsSecurityWebAuthGuestInfoEntry;

    tWsscfgFsStationQosParamsEntry WsscfgSetFsStationQosParamsEntry;
    tWsscfgIsSetFsStationQosParamsEntry WsscfgIsSetFsStationQosParamsEntry;

    tWsscfgFsVlanIsolationEntry WsscfgSetFsVlanIsolationEntry;
    tWsscfgIsSetFsVlanIsolationEntry WsscfgIsSetFsVlanIsolationEntry;

    tWsscfgFsDot11RadioConfigEntry WsscfgSetFsDot11RadioConfigEntry;
    tWsscfgIsSetFsDot11RadioConfigEntry WsscfgIsSetFsDot11RadioConfigEntry;

    tWsscfgFsDot11QosProfileEntry WsscfgSetFsDot11QosProfileEntry;
    tWsscfgIsSetFsDot11QosProfileEntry WsscfgIsSetFsDot11QosProfileEntry;

    tWsscfgFsDot11WlanCapabilityProfileEntry
        WsscfgSetFsDot11WlanCapabilityProfileEntry;
    tWsscfgIsSetFsDot11WlanCapabilityProfileEntry
        WsscfgIsSetFsDot11WlanCapabilityProfileEntry;

    tWsscfgFsDot11WlanAuthenticationProfileEntry
        WsscfgSetFsDot11WlanAuthenticationProfileEntry;
    tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry
        WsscfgIsSetFsDot11WlanAuthenticationProfileEntry;

    tWsscfgFsDot11WlanQosProfileEntry WsscfgSetFsDot11WlanQosProfileEntry;
    tWsscfgIsSetFsDot11WlanQosProfileEntry
        WsscfgIsSetFsDot11WlanQosProfileEntry;

    tWsscfgFsDot11RadioQosEntry WsscfgSetFsDot11RadioQosEntry;
    tWsscfgIsSetFsDot11RadioQosEntry WsscfgIsSetFsDot11RadioQosEntry;

    tWsscfgFsDot11QAPEntry WsscfgSetFsDot11QAPEntry;
    tWsscfgIsSetFsDot11QAPEntry WsscfgIsSetFsDot11QAPEntry;

    tWsscfgFsQAPProfileEntry WsscfgSetFsQAPProfileEntry;
    tWsscfgIsSetFsQAPProfileEntry WsscfgIsSetFsQAPProfileEntry;

    tWsscfgFsDot11CapabilityMappingEntry WsscfgSetFsDot11CapabilityMappingEntry;
    tWsscfgIsSetFsDot11CapabilityMappingEntry
        WsscfgIsSetFsDot11CapabilityMappingEntry;

    tWsscfgFsDot11AuthMappingEntry WsscfgSetFsDot11AuthMappingEntry;
    tWsscfgIsSetFsDot11AuthMappingEntry WsscfgIsSetFsDot11AuthMappingEntry;

    tWsscfgFsDot11QosMappingEntry WsscfgSetFsDot11QosMappingEntry;
    tWsscfgIsSetFsDot11QosMappingEntry WsscfgIsSetFsDot11QosMappingEntry;

    tWsscfgFsDot11AntennasListEntry WsscfgSetFsDot11AntennasListEntry;
    tWsscfgIsSetFsDot11AntennasListEntry WsscfgIsSetFsDot11AntennasListEntry;

    tWsscfgFsDot11WlanEntry WsscfgSetFsDot11WlanEntry;
    tWsscfgIsSetFsDot11WlanEntry WsscfgIsSetFsDot11WlanEntry;

    tWsscfgFsDot11WlanBindEntry WsscfgSetFsDot11WlanBindEntry;
    tWsscfgIsSetFsDot11WlanBindEntry WsscfgIsSetFsDot11WlanBindEntry;

    tWsscfgFsWtpImageUpgradeEntry WsscfgSetFsWtpImageUpgradeEntry;
    tWsscfgIsSetFsWtpImageUpgradeEntry WsscfgIsSetFsWtpImageUpgradeEntry;

    tWsscfgCapwapDot11WlanEntry WsscfgSetCapwapDot11WlanEntry;
    tWsscfgIsSetCapwapDot11WlanEntry WsscfgIsSetCapwapDot11WlanEntry;

    tWsscfgCapwapDot11WlanBindEntry WsscfgSetCapwapDot11WlanBindEntry;
    tWsscfgIsSetCapwapDot11WlanBindEntry WsscfgIsSetCapwapDot11WlanBindEntry;

    tWsscfgFsRrmConfigEntry WsscfgSetFsRrmConfigEntry;
    tWsscfgIsSetFsRrmConfigEntry WsscfgIsSetFsRrmConfigEntry;

    tWsscfgFsDot11nConfigEntry WsscfgSetFsDot11nConfigEntry;
    tWsscfgIsSetFsDot11nConfigEntry WsscfgIsSetFsDot11nConfigEntry;

    tWsscfgFsDot11nMCSDataRateEntry WsscfgSetFsDot11nMCSDataRateEntry;
    tWsscfgIsSetFsDot11nMCSDataRateEntry WsscfgIsSetFsDot11nMCSDataRateEntry;

    UINT4               u4RadioIfIndex = 0;
    UINT4               u4WlanProfileId = 0;
    INT4                i4WlanIfIndex = 0;
    UINT4               u4WtpProfileId = 0;
    UINT1               u1RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT1               u1RadioIndex = 0;
    UINT4               u4RadioIfType = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4NoOfRadio = 0;
    UINT1               au1ModelNumber[256];
    UINT4               u4GetRadioType = 0;
    UINT2               u2WtpProfileId = 0;
    UINT1               au1PromptName[32];
    CHR1               *pu1PromptName = NULL;
    CHR1               *saveptr = NULL;
    UINT1              *pu1Name = NULL;
    UINT4               u4Len = 0;
    INT4                i4RowStatus = 0;
    tSNMP_OCTET_STRING_TYPE ModelName;
    tSNMP_OCTET_STRING_TYPE ProfileName;
    tIPvXAddr           IpAddress;
    UINT1               au1FileName[WTP_CONFIG_FILENAME_LEN + 1];
    INT4                i4WtpMacType = 3;
    INT4                i4WlanMacType = 3;
    UINT4               currentProfileId = 0, nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;

    MEMSET (&au1ModelNumber, 0, 256);
    MEMSET (&ModelName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    ModelName.pu1_OctetList = au1ModelNumber;
    MEMSET (au1PromptName, 0, sizeof (32));
    MEMSET (au1ProfileName, 0, CAPWAP_WTP_DFLT_PROF_NAME_LEN);

    pu1PromptName = (CHR1 *) au1PromptName;

    UNUSED_PARAM (u4CmdType);
    CliRegisterLock (CliHandle, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == WSSCFG_CLI_MAX_ARGS)
            break;
    }

    SPRINTF ((CHR1 *) au1ProfileName, "%s", CAPWAP_WTP_DFLT_PROF_NAME);
    va_end (ap);
    switch (u4Command)
    {
        case CLI_WSSCFG_CAPWAPDOT11WLANTABLE:
            MEMSET (&WsscfgSetCapwapDot11WlanEntry, 0,
                    sizeof (tWsscfgCapwapDot11WlanEntry));
            MEMSET (&WsscfgIsSetCapwapDot11WlanEntry, 0,
                    sizeof (tWsscfgIsSetCapwapDot11WlanEntry));

            if ((args[5] != NULL) && (*args[5] == CLI_WLAN_ENABLE)
                && args[6] != NULL)
            {
                CliPrintf (CliHandle, "\r\nInvalid input\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }
            if (args[6] != NULL)
            {
                if (STRLEN (args[6]) > CLI_MAX_SSID_LEN)
                {
                    CliPrintf (CliHandle, "\r\nSSID name is too long\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }
            else
            {
                if (args[4] != NULL)
                {
                    if (*args[4] == CREATE_AND_WAIT)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n SSID missing. Mandatory for Profile Creation\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                }
            }

            WSSCFG_FILL_CAPWAPDOT11WLANTABLE_ARGS ((&WsscfgSetCapwapDot11WlanEntry), (&WsscfgIsSetCapwapDot11WlanEntry), args[0], args[1], args[2], args[3], args[4]);

            i4RetStatus =
                WsscfgCliSetCapwapDot11WlanTable (CliHandle,
                                                  (&WsscfgSetCapwapDot11WlanEntry),
                                                  (&WsscfgIsSetCapwapDot11WlanEntry));
            if (i4RetStatus != OSIX_FAILURE)
            {
                if (args[5] != NULL)
                {
                    if (args[0] != NULL)
                    {
                        if (nmhGetCapwapDot11WlanProfileIfIndex
                            (CLI_PTR_TO_U4 (*args[0]),
                             &i4WlanIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Index does not exist \r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                        if (i4WlanIfIndex == 0)
                        {
                            CliPrintf (CliHandle, "\r\n Invalid index\r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                    }
                    if ((i4RetStatus =
                         nmhTestv2IfMainAdminStatus (&u4ErrorCode,
                                                     i4WlanIfIndex,
                                                     CLI_PTR_TO_U4 (*args
                                                                    [5])))
                        != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "\r\n Invalid input\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    if ((i4RetStatus =
                         nmhSetIfMainAdminStatus (i4WlanIfIndex,
                                                  CLI_PTR_TO_U4 (*args[5])))
                        != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Admin Status change failed\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    CliPrintf (CliHandle,
                               "\r\n Admin Status change success\r\n");

                }
            }
            break;

        case CLI_WSSCFG_CAPWAPDOT11WLANBINDTABLE:
            MEMSET (&WsscfgSetCapwapDot11WlanBindEntry, 0,
                    sizeof (tWsscfgCapwapDot11WlanBindEntry));
            MEMSET (&WsscfgIsSetCapwapDot11WlanBindEntry, 0,
                    sizeof (tWsscfgIsSetCapwapDot11WlanBindEntry));

            if (args[2] != NULL)
            {
                /* When Profile name is received as input get the profile id
                 * from mapping table and pass the profile name as NULL */
                if (CapwapGetWtpProfileIdFromProfileName
                    (au1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n WTP Profile not found. Binding failed\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

                if ((i4RetStatus =
                     nmhGetCapwapBaseWtpProfileWtpModelNumber
                     (u4WtpProfileId, &ModelName)) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Model entry missing. Binding failed\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if ((i4RetStatus =
                     nmhGetFsCapwapWtpMacType (&ModelName,
                                               &i4WtpMacType)) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting WTP Mac Type failed.\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

                if ((i4RetStatus =
                     nmhGetCapwapDot11WlanMacType ((*args[4]),
                                                   &i4WlanMacType)) !=
                    SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting WLAN Mac Type failed.\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

                if (i4WtpMacType != i4WlanMacType)
                {
                    CliPrintf (CliHandle,
                               "\r\n MAC Type Mismatch. WLAN Binding Failure\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

                if (*args[3] == 0)
                {
                    if ((i4RetStatus =
                         nmhGetCapwapBaseWtpProfileWtpModelNumber
                         (u4WtpProfileId, &ModelName)) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Model entry missing. Binding failed\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }

                    if ((i4RetStatus =
                         nmhGetFsNoOfRadio (&ModelName,
                                            &u4NoOfRadio)) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Getting Radio count failed.\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }

                    for (u1RadioId = 1; u1RadioId <= u4NoOfRadio; u1RadioId++)
                    {
                        if ((i4RetStatus =
                             nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                             (u4WtpProfileId, u1RadioId,
                              &i4RadioIfIndex)) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Radio Index not found, Binding failed\r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }

                        if ((i4RetStatus =
                             nmhGetFsDot11RadioType (i4RadioIfIndex,
                                                     &u4GetRadioType)) !=
                            SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Getting Radio Type failed\r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }

                        if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[1]))
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Radio Type conflicting, binding failed\r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                        else
                        {
                            WSSCFG_FILL_CAPWAPDOT11WLANBINDTABLE_ARGS ((&WsscfgSetCapwapDot11WlanBindEntry), (&WsscfgIsSetCapwapDot11WlanBindEntry), args[0], &i4RadioIfIndex, args[4]);
                            i4RetStatus =
                                WsscfgCliSetCapwapDot11WlanBindTable
                                (CliHandle,
                                 (&WsscfgSetCapwapDot11WlanBindEntry),
                                 (&WsscfgIsSetCapwapDot11WlanBindEntry));
                        }
                    }
                }
                else
                {
                    if ((i4RetStatus =
                         nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                         (u4WtpProfileId, *args[3],
                          &i4RadioIfIndex)) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n AP Name missing. Binding failed\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }

                    if ((i4RetStatus =
                         nmhGetFsDot11RadioType (i4RadioIfIndex,
                                                 &u4GetRadioType)) !=
                        SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Getting Radio Type failed\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }

                    /*if (u4GetRadioType != (INT4)*args[1]) */
                    if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[1]))
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Radio Type conflicting, binding failed\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    else
                    {
                        WSSCFG_FILL_CAPWAPDOT11WLANBINDTABLE_ARGS ((&WsscfgSetCapwapDot11WlanBindEntry), (&WsscfgIsSetCapwapDot11WlanBindEntry), args[0], &i4RadioIfIndex, args[4]);

                        i4RetStatus =
                            WsscfgCliSetCapwapDot11WlanBindTable
                            (CliHandle,
                             (&WsscfgSetCapwapDot11WlanBindEntry),
                             (&WsscfgIsSetCapwapDot11WlanBindEntry));
                    }
                }
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r\n AP Name missing. Binding failed\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
            }
            break;

        case CLI_WSSCFG_DOT11STATIONCONFIGTABLE:
            MEMSET (&WsscfgSetDot11StationConfigEntry, 0,
                    sizeof (tWsscfgDot11StationConfigEntry));
            MEMSET (&WsscfgIsSetDot11StationConfigEntry, 0,
                    sizeof (tWsscfgIsSetDot11StationConfigEntry));

            /* WSS Changes Starts */
            if (args[25] != NULL)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (CLI_PTR_TO_U4 (*args[25]), &i4WlanIfIndex) != SNMP_SUCCESS)
                {
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                else
                {
                    WSSCFG_FILL_DOT11STATIONCONFIGTABLE_ARGS ((&WsscfgSetDot11StationConfigEntry), (&WsscfgIsSetDot11StationConfigEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17], args[18], args[19], args[20], args[21], &i4WlanIfIndex);
                    i4RetStatus =
                        WsscfgCliSetDot11StationConfigTable (CliHandle,
                                                             (&WsscfgSetDot11StationConfigEntry),
                                                             (&WsscfgIsSetDot11StationConfigEntry));
                }
            }
            else if (args[22] != NULL)
            {

                if (CapwapGetWtpProfileIdFromProfileName
                    (au1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    break;
                }
                u1RadioId = CLI_PTR_TO_U4 (*args[23]);

                if (u1RadioId == 0)
                {
                    for (u1RadioIndex = 1;
                         u1RadioIndex <= SYS_DEF_MAX_RADIO_INTERFACES;
                         u1RadioIndex++)
                    {

                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d : Virtual Radio If Index not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (u4RadioIfType == *args[24])
                        {
                            WSSCFG_FILL_DOT11STATIONCONFIGTABLE_ARGS ((&WsscfgSetDot11StationConfigEntry), (&WsscfgIsSetDot11StationConfigEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17], args[18], args[19], args[20], args[21], &i4RadioIfIndex);

                            if (WsscfgCliSetDot11StationConfigTable
                                (CliHandle,
                                 (&WsscfgSetDot11StationConfigEntry),
                                 (&WsscfgIsSetDot11StationConfigEntry)) !=
                                OSIX_SUCCESS)
                            {

                                CliPrintf (CliHandle,
                                           "\r\nFor Radio %d :  Values cant be successfully set,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                           u1RadioIndex);
                                continue;

                            }
                        }
                    }
                }
                else
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u1RadioId,
                         &i4RadioIfIndex) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "\r\nRadio Id Not Present\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        break;
                    }

                    if (nmhGetFsDot11RadioType
                        (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed \r\n");
                        break;
                    }

                    if (u4RadioIfType != *args[24])
                    {
                        CliPrintf (CliHandle, "\r\nRadio Type Mismatch\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        break;
                    }

                    WSSCFG_FILL_DOT11STATIONCONFIGTABLE_ARGS ((&WsscfgSetDot11StationConfigEntry), (&WsscfgIsSetDot11StationConfigEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17], args[18], args[19], args[20], args[21], &i4RadioIfIndex);

                    i4RetStatus =
                        WsscfgCliSetDot11StationConfigTable (CliHandle,
                                                             (&WsscfgSetDot11StationConfigEntry),
                                                             (&WsscfgIsSetDot11StationConfigEntry));

                }

            }
            else if (NULL == args[22])
            {
                for (u2WtpProfileId = 1; u2WtpProfileId <= CLI_MAX_AP;
                     u2WtpProfileId++)
                {

                    for (u1RadioIndex = 1;
                         u1RadioIndex <= SYS_DEF_MAX_RADIO_INTERFACES;
                         u1RadioIndex++)
                    {

                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u2WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nIn Ap (%d) , For Radio %d : Virtual Radio If Index not successfully obtained,Couldnt proceed,operation failed for the particular Radio\r\n",
                                       u2WtpProfileId, u1RadioIndex);
                            continue;
                        }

                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (u4RadioIfType == *args[24])
                        {
                            WSSCFG_FILL_DOT11STATIONCONFIGTABLE_ARGS ((&WsscfgSetDot11StationConfigEntry), (&WsscfgIsSetDot11StationConfigEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17], args[18], args[19], args[20], args[21], &i4RadioIfIndex);

                            if (WsscfgCliSetDot11StationConfigTable
                                (CliHandle,
                                 (&WsscfgSetDot11StationConfigEntry),
                                 (&WsscfgIsSetDot11StationConfigEntry)) !=
                                OSIX_SUCCESS)
                            {

                                CliPrintf (CliHandle,
                                           "\r\nr Ap(%d)For Radio %d :  Values cant be successfully set,Couldnt proceed ,operation failed for the particular Radio\r\n",
                                           u2WtpProfileId, u1RadioIndex);
                                continue;

                            }

                        }

                    }

                }

            }
            break;

        case CLI_WSSCFG_DOT11AUTHENTICATIONALGORITHMSTABLE:
            MEMSET (&WsscfgSetDot11AuthenticationAlgorithmsEntry, 0,
                    sizeof (tWsscfgDot11AuthenticationAlgorithmsEntry));
            MEMSET (&WsscfgIsSetDot11AuthenticationAlgorithmsEntry, 0,
                    sizeof (tWsscfgIsSetDot11AuthenticationAlgorithmsEntry));

            if (args[3] != NULL)
            {
                nmhGetCapwapDot11WlanProfileIfIndex (CLI_PTR_TO_U4
                                                     (*args[3]),
                                                     &i4WlanIfIndex);
            }

            WSSCFG_FILL_DOT11AUTHENTICATIONALGORITHMSTABLE_ARGS ((&WsscfgSetDot11AuthenticationAlgorithmsEntry), (&WsscfgIsSetDot11AuthenticationAlgorithmsEntry), args[0], args[1], args[2], &i4WlanIfIndex);

            i4RetStatus =
                WsscfgCliSetDot11AuthenticationAlgorithmsTable (CliHandle,
                                                                (&WsscfgSetDot11AuthenticationAlgorithmsEntry),
                                                                (&WsscfgIsSetDot11AuthenticationAlgorithmsEntry));
            break;

        case CLI_WSSCFG_DOT11WEPDEFAULTKEYSTABLE:
            MEMSET (&WsscfgSetDot11WEPDefaultKeysEntry, 0,
                    sizeof (tWsscfgDot11WEPDefaultKeysEntry));
            MEMSET (&WsscfgIsSetDot11WEPDefaultKeysEntry, 0,
                    sizeof (tWsscfgIsSetDot11WEPDefaultKeysEntry));

            if (args[5] != NULL)
            {
                nmhGetCapwapDot11WlanProfileIfIndex (CLI_PTR_TO_U4
                                                     (*args[0]),
                                                     &i4WlanIfIndex);
            }
            WSSCFG_FILL_DOT11WEPDEFAULTKEYSTABLE_ARGS ((&WsscfgSetDot11WEPDefaultKeysEntry), (&WsscfgIsSetDot11WEPDefaultKeysEntry), args[0], args[1], args[2], &i4WlanIfIndex);

            i4RetStatus =
                WsscfgCliSetDot11WEPDefaultKeysTable (CliHandle,
                                                      (&WsscfgSetDot11WEPDefaultKeysEntry),
                                                      (&WsscfgIsSetDot11WEPDefaultKeysEntry));
            break;

        case CLI_WSSCFG_DOT11WEPKEYMAPPINGSTABLE:
            MEMSET (&WsscfgSetDot11WEPKeyMappingsEntry, 0,
                    sizeof (tWsscfgDot11WEPKeyMappingsEntry));
            MEMSET (&WsscfgIsSetDot11WEPKeyMappingsEntry, 0,
                    sizeof (tWsscfgIsSetDot11WEPKeyMappingsEntry));

            WSSCFG_FILL_DOT11WEPKEYMAPPINGSTABLE_ARGS ((&WsscfgSetDot11WEPKeyMappingsEntry), (&WsscfgIsSetDot11WEPKeyMappingsEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6]);

            i4RetStatus =
                WsscfgCliSetDot11WEPKeyMappingsTable (CliHandle,
                                                      (&WsscfgSetDot11WEPKeyMappingsEntry),
                                                      (&WsscfgIsSetDot11WEPKeyMappingsEntry));
            break;

        case CLI_WSSCFG_DOT11PRIVACYTABLE:
            MEMSET (&WsscfgSetDot11PrivacyEntry, 0,
                    sizeof (tWsscfgDot11PrivacyEntry));
            MEMSET (&WsscfgIsSetDot11PrivacyEntry, 0,
                    sizeof (tWsscfgIsSetDot11PrivacyEntry));

            WSSCFG_FILL_DOT11PRIVACYTABLE_ARGS ((&WsscfgSetDot11PrivacyEntry),
                                                (&WsscfgIsSetDot11PrivacyEntry),
                                                args[0], args[1], args[2],
                                                args[3], args[4], args[5],
                                                args[6]);

            i4RetStatus =
                WsscfgCliSetDot11PrivacyTable (CliHandle,
                                               (&WsscfgSetDot11PrivacyEntry),
                                               (&WsscfgIsSetDot11PrivacyEntry));
            break;

        case CLI_WSSCFG_DOT11MULTIDOMAINCAPABILITYTABLE:
            MEMSET (&WsscfgSetDot11MultiDomainCapabilityEntry, 0,
                    sizeof (tWsscfgDot11MultiDomainCapabilityEntry));
            MEMSET (&WsscfgIsSetDot11MultiDomainCapabilityEntry, 0,
                    sizeof (tWsscfgIsSetDot11MultiDomainCapabilityEntry));

            if (args[4] == NULL)
            {
                break;
            }
            else
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    (au1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n WTP Profile not found. Binding failed\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                u1RadioId = CLI_PTR_TO_U4 (*args[5]);
                if (u1RadioId == 0)
                {
                    /*Coverity Change Begins */
                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n WTP Model number not successfully obtained,Couldnt proceed ,operation failed\r\n");
                    }
                    /*Coverity Change Ends */
                    nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio);

                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {

                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (u4RadioIfType == *args[6])
                        {
                            /*Coverity Change Begins */
                            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex (u4WtpProfileId, u1RadioIndex, &i4RadioIfIndex) != SNMP_SUCCESS)
                            {
                                CliPrintf (CliHandle,
                                           "\r\n wireless binding radio index not successfully obtained ,operation failed\r\n");
                            }
                            /*Coverity Change Ends */
                            WSSCFG_FILL_DOT11MULTIDOMAINCAPABILITYTABLE_ARGS
                                ((&WsscfgSetDot11MultiDomainCapabilityEntry),
                                 (&WsscfgIsSetDot11MultiDomainCapabilityEntry),
                                 args[0], args[1], args[2], args[3],
                                 &i4RadioIfIndex);
                        }
                    }
                }
                else
                {
                    /*Coverity Change Begins */
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u1RadioId,
                         &i4RadioIfIndex) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n wireless binding radio index not successfully obtained ,operation failed\r\n");
                    }
                    /*Coverity Change Ends */
                    if (nmhGetFsDot11RadioType
                        (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed \r\n");
                        break;
                    }

                    if (u4RadioIfType != *args[24])
                    {
                        break;
                    }
                    else
                    {
                        WSSCFG_FILL_DOT11MULTIDOMAINCAPABILITYTABLE_ARGS ((&WsscfgSetDot11MultiDomainCapabilityEntry), (&WsscfgIsSetDot11MultiDomainCapabilityEntry), args[0], args[1], args[2], args[3], &i4RadioIfIndex);

                    }
                }
            }

            i4RetStatus =
                WsscfgCliSetDot11MultiDomainCapabilityTable (CliHandle,
                                                             (&WsscfgSetDot11MultiDomainCapabilityEntry),
                                                             (&WsscfgIsSetDot11MultiDomainCapabilityEntry));
            break;

        case CLI_WSSCFG_DOT11SPECTRUMMANAGEMENTTABLE:
            MEMSET (&WsscfgSetDot11SpectrumManagementEntry, 0,
                    sizeof (tWsscfgDot11SpectrumManagementEntry));
            MEMSET (&WsscfgIsSetDot11SpectrumManagementEntry, 0,
                    sizeof (tWsscfgIsSetDot11SpectrumManagementEntry));

            if (args[3] != NULL)
            {
                nmhGetCapwapDot11WlanProfileIfIndex (CLI_PTR_TO_U4
                                                     (*args[3]),
                                                     &i4WlanIfIndex);
            }
            WSSCFG_FILL_DOT11SPECTRUMMANAGEMENTTABLE_ARGS ((&WsscfgSetDot11SpectrumManagementEntry), (&WsscfgIsSetDot11SpectrumManagementEntry), args[0], args[1], args[2], &i4WlanIfIndex);

            i4RetStatus =
                WsscfgCliSetDot11SpectrumManagementTable (CliHandle,
                                                          (&WsscfgSetDot11SpectrumManagementEntry),
                                                          (&WsscfgIsSetDot11SpectrumManagementEntry));
            break;

        case CLI_WSSCFG_DOT11REGULATORYCLASSESTABLE:
            MEMSET (&WsscfgSetDot11RegulatoryClassesEntry, 0,
                    sizeof (tWsscfgDot11RegulatoryClassesEntry));
            MEMSET (&WsscfgIsSetDot11RegulatoryClassesEntry, 0,
                    sizeof (tWsscfgIsSetDot11RegulatoryClassesEntry));

            WSSCFG_FILL_DOT11REGULATORYCLASSESTABLE_ARGS ((&WsscfgSetDot11RegulatoryClassesEntry), (&WsscfgIsSetDot11RegulatoryClassesEntry), args[0], args[1], args[2], args[3]);

            i4RetStatus =
                WsscfgCliSetDot11RegulatoryClassesTable (CliHandle,
                                                         (&WsscfgSetDot11RegulatoryClassesEntry),
                                                         (&WsscfgIsSetDot11RegulatoryClassesEntry));
            break;

        case CLI_WSSCFG_DOT11OPERATIONTABLE:
            MEMSET (&WsscfgSetDot11OperationEntry, 0,
                    sizeof (tWsscfgDot11OperationEntry));
            MEMSET (&WsscfgIsSetDot11OperationEntry, 0,
                    sizeof (tWsscfgIsSetDot11OperationEntry));

            if (args[18] == NULL)
            {
                if (nmhGetFirstIndexFsCapwapWirelessBindingTable
                    (&nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                else
                {
                    do
                    {
                        currentProfileId = nextProfileId;
                        u4currentBindingId = u4nextBindingId;
                        if ((currentProfileId == 0)
                            || (u4currentBindingId == 0))
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (nextProfileId, u4nextBindingId,
                             &i4RadioIfIndex) == SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Radio Index not found, Binding failed\r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                        if ((i4RetStatus =
                             nmhGetFsDot11RadioType (i4RadioIfIndex,
                                                     &u4GetRadioType)) !=
                            SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Getting Radio Type failed\r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                        if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[20]))
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Radio Type conflicting, binding failed\r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                        else
                        {
                            WSSCFG_FILL_DOT11OPERATIONTABLE_ARGS ((&WsscfgSetDot11OperationEntry), (&WsscfgIsSetDot11OperationEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], &i4RadioIfIndex);

                            if (WsscfgCliSetDot11OperationTable
                                (CliHandle,
                                 (&WsscfgSetDot11OperationEntry),
                                 (&WsscfgIsSetDot11OperationEntry)) !=
                                OSIX_SUCCESS)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nFor Radio nextBindingId %d :  DB Updation failed \r\n",
                                           u4nextBindingId);
                                continue;
                            }
                        }
                    }
                    while (nmhGetNextIndexFsCapwapWirelessBindingTable
                           (currentProfileId, &nextProfileId,
                            u4currentBindingId,
                            &u4nextBindingId) == SNMP_SUCCESS);
                }
            }
            else
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    (au1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                u1RadioId = CLI_PTR_TO_U4 (*args[19]);

                if (u1RadioId == 0)
                {
                    for (u1RadioIndex = 1;
                         u1RadioIndex <= SYS_DEF_MAX_RADIO_INTERFACES;
                         u1RadioIndex++)
                    {
                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d : Virtual Radio If Index not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }
                        if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[20]))
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Radio Type conflicting, binding failed\r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                        else
                        {
                            WSSCFG_FILL_DOT11OPERATIONTABLE_ARGS ((&WsscfgSetDot11OperationEntry), (&WsscfgIsSetDot11OperationEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], &i4RadioIfIndex);

                            if (WsscfgCliSetDot11OperationTable
                                (CliHandle,
                                 (&WsscfgSetDot11OperationEntry),
                                 (&WsscfgIsSetDot11OperationEntry)) !=
                                OSIX_SUCCESS)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nFor Radio %d :  DB Updation failed \r\n",
                                           u1RadioIndex);
                                continue;
                            }
                        }
                    }
                }
                else
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u1RadioId,
                         &i4RadioIfIndex) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "Radio Id Not Present\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                    }
                    if (nmhGetFsDot11RadioType
                        (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed \r\n");
                        break;
                    }
                    if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[20]))
                    {
                        CliPrintf (CliHandle, "Conflicting Radio Type\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    else
                    {
                        WSSCFG_FILL_DOT11OPERATIONTABLE_ARGS ((&WsscfgSetDot11OperationEntry), (&WsscfgIsSetDot11OperationEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], &i4RadioIfIndex);

                        i4RetStatus =
                            WsscfgCliSetDot11OperationTable (CliHandle,
                                                             (&WsscfgSetDot11OperationEntry),
                                                             (&WsscfgIsSetDot11OperationEntry));
                    }
                }
            }

            break;

        case CLI_WSSCFG_DOT11GROUPADDRESSESTABLE:
            MEMSET (&WsscfgSetDot11GroupAddressesEntry, 0,
                    sizeof (tWsscfgDot11GroupAddressesEntry));
            MEMSET (&WsscfgIsSetDot11GroupAddressesEntry, 0,
                    sizeof (tWsscfgIsSetDot11GroupAddressesEntry));

            WSSCFG_FILL_DOT11GROUPADDRESSESTABLE_ARGS ((&WsscfgSetDot11GroupAddressesEntry), (&WsscfgIsSetDot11GroupAddressesEntry), args[0], args[1], args[2], args[3]);

            i4RetStatus =
                WsscfgCliSetDot11GroupAddressesTable (CliHandle,
                                                      (&WsscfgSetDot11GroupAddressesEntry),
                                                      (&WsscfgIsSetDot11GroupAddressesEntry));
            break;

        case CLI_WSSCFG_DOT11EDCATABLE:
            MEMSET (&WsscfgSetDot11EDCAEntry, 0,
                    sizeof (tWsscfgDot11EDCAEntry));
            MEMSET (&WsscfgIsSetDot11EDCAEntry, 0,
                    sizeof (tWsscfgIsSetDot11EDCAEntry));

            /* WSS Changes Starts */
            if (args[8] != NULL)
            {

                if (CapwapGetWtpProfileIdFromProfileName
                    (au1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n WTP Profile not found. Binding failed\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                u1RadioId = CLI_PTR_TO_U4 (*args[9]);

                if (u1RadioId == 0)
                {
                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nModel No not successfully obtained , (Invalid Input, Check Profile name)\r\n");
                        break;
                    }

                    if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nNo.of Radios present not successfully obtained, Operation Failed\r\n");
                        break;

                    }

                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {

                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d : Virtual Radio If Index not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (u4RadioIfType == *args[10])
                        {
                            WSSCFG_FILL_DOT11EDCATABLE_ARGS ((&WsscfgSetDot11EDCAEntry), (&WsscfgIsSetDot11EDCAEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], &i4RadioIfIndex);
                            if ((WsscfgCliSetDot11EDCATable
                                 (CliHandle, (&WsscfgSetDot11EDCAEntry),
                                  (&WsscfgIsSetDot11EDCAEntry))) !=
                                OSIX_SUCCESS)
                            {

                                CliPrintf (CliHandle,
                                           "\r\nFor Radio %d :  Values cant be successfully set,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                           u1RadioIndex);
                                continue;

                            }
                        }
                    }
                }
                else
                {
                    nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u1RadioId, &i4RadioIfIndex);

                    if (nmhGetFsDot11RadioType
                        (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed \r\n");
                        break;
                    }
                    if (u4RadioIfType != *args[10])
                    {
                        break;
                    }
                    else
                    {
                        WSSCFG_FILL_DOT11EDCATABLE_ARGS ((&WsscfgSetDot11EDCAEntry), (&WsscfgIsSetDot11EDCAEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], &i4RadioIfIndex);
                        i4RetStatus =
                            WsscfgCliSetDot11EDCATable (CliHandle,
                                                        (&WsscfgSetDot11EDCAEntry),
                                                        (&WsscfgIsSetDot11EDCAEntry));
                    }

                }

            }
            else if (args[8] == NULL)
            {
                for (u2WtpProfileId = 1; u2WtpProfileId <= CLI_MAX_AP;
                     u2WtpProfileId++)
                {

                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u2WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Ap (%d) : Model No not successfully obtained, Could not proceed for this AP\r\n",
                                   u2WtpProfileId);
                        continue;
                    }

                    if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nNo.of Radios present not successfully obtained, Operation Failed\r\n");
                        break;

                    }

                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {

                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nIn Ap (%d) , For Radio %d : Virtual Radio If Index not successfully obtained,Couldnt proceed,operation failed for the particular Radio\r\n",
                                       u2WtpProfileId, u1RadioIndex);
                            continue;
                        }

                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (u4RadioIfType == *args[10])
                        {
                            WSSCFG_FILL_DOT11EDCATABLE_ARGS ((&WsscfgSetDot11EDCAEntry), (&WsscfgIsSetDot11EDCAEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], &i4RadioIfIndex);

                            if ((WsscfgCliSetDot11EDCATable
                                 (CliHandle, (&WsscfgSetDot11EDCAEntry),
                                  (&WsscfgIsSetDot11EDCAEntry))) !=
                                OSIX_SUCCESS)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nr Ap(%d)For Radio %d :  Values cant be successfully set,Couldnt proceed ,operation failed for the particular Radio\r\n",
                                           u2WtpProfileId, u1RadioIndex);
                                continue;
                            }
                        }
                    }
                }
            }
            break;

        case CLI_WSSCFG_DOT11QAPEDCATABLE:
            MEMSET (&WsscfgSetDot11QAPEDCAEntry, 0,
                    sizeof (tWsscfgDot11QAPEDCAEntry));
            MEMSET (&WsscfgIsSetDot11QAPEDCAEntry, 0,
                    sizeof (tWsscfgIsSetDot11QAPEDCAEntry));

            WSSCFG_FILL_DOT11QAPEDCATABLE_ARGS ((&WsscfgSetDot11QAPEDCAEntry),
                                                (&WsscfgIsSetDot11QAPEDCAEntry),
                                                args[0], args[1], args[2],
                                                args[3], args[4], args[5],
                                                args[6], args[7]);

            i4RetStatus =
                WsscfgCliSetDot11QAPEDCATable (CliHandle,
                                               (&WsscfgSetDot11QAPEDCAEntry),
                                               (&WsscfgIsSetDot11QAPEDCAEntry));
            break;

        case CLI_WSSCFG_DOT11PHYOPERATIONTABLE:
            MEMSET (&WsscfgSetDot11PhyOperationEntry, 0,
                    sizeof (tWsscfgDot11PhyOperationEntry));
            MEMSET (&WsscfgIsSetDot11PhyOperationEntry, 0,
                    sizeof (tWsscfgIsSetDot11PhyOperationEntry));

            WSSCFG_FILL_DOT11PHYOPERATIONTABLE_ARGS ((&WsscfgSetDot11PhyOperationEntry), (&WsscfgIsSetDot11PhyOperationEntry), args[0], args[1]);

            i4RetStatus =
                WsscfgCliSetDot11PhyOperationTable (CliHandle,
                                                    (&WsscfgSetDot11PhyOperationEntry),
                                                    (&WsscfgIsSetDot11PhyOperationEntry));
            break;

        case CLI_WSSCFG_DOT11PHYANTENNATABLE:
            MEMSET (&WsscfgSetDot11PhyAntennaEntry, 0,
                    sizeof (tWsscfgDot11PhyAntennaEntry));
            MEMSET (&WsscfgIsSetDot11PhyAntennaEntry, 0,
                    sizeof (tWsscfgIsSetDot11PhyAntennaEntry));

            WSSCFG_FILL_DOT11PHYANTENNATABLE_ARGS ((&WsscfgSetDot11PhyAntennaEntry), (&WsscfgIsSetDot11PhyAntennaEntry), args[0], args[1], args[2]);

            i4RetStatus =
                WsscfgCliSetDot11PhyAntennaTable (CliHandle,
                                                  (&WsscfgSetDot11PhyAntennaEntry),
                                                  (&WsscfgIsSetDot11PhyAntennaEntry));
            break;

        case CLI_WSSCFG_DOT11PHYTXPOWERTABLE:
            MEMSET (&WsscfgSetDot11PhyTxPowerEntry, 0,
                    sizeof (tWsscfgDot11PhyTxPowerEntry));
            MEMSET (&WsscfgIsSetDot11PhyTxPowerEntry, 0,
                    sizeof (tWsscfgIsSetDot11PhyTxPowerEntry));

            /* When Profile name is received as input get the profile id
             * from mapping table */

            if (CapwapGetWtpProfileIdFromProfileName
                (au1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            if ((nmhGetCapwapBaseWtpProfileRowStatus
                 (u4WtpProfileId, &i4RowStatus) != SNMP_SUCCESS)
                || (i4RowStatus != ACTIVE))
            {
                CliPrintf (CliHandle,
                           "\r\n WTP Profile table not active. \r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }
            for (u1RadioIndex = 1;
                 u1RadioIndex <= SYS_DEF_MAX_RADIO_INTERFACES; u1RadioIndex++)
            {

                if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                    (u4WtpProfileId, u1RadioIndex,
                     &i4RadioIfIndex) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nFor Radio %d : Virtual Radio If Index not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                               u1RadioIndex);
                    continue;
                }

                if (nmhGetFsDot11RadioType
                    (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                               u1RadioIndex);
                    continue;
                }

                WSSCFG_FILL_DOT11PHYTXPOWERTABLE_ARGS ((&WsscfgSetDot11PhyTxPowerEntry), (&WsscfgIsSetDot11PhyTxPowerEntry), args[0], &i4RadioIfIndex);

                if (WsscfgCliSetDot11PhyTxPowerTable
                    (CliHandle, (&WsscfgSetDot11PhyTxPowerEntry),
                     (&WsscfgIsSetDot11PhyTxPowerEntry)) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nFor Radio %d :  Values cant be successfully set,Couldnt proceed ,operation failed for the particular Radio \r\n",
                               u1RadioIndex);
                    continue;

                }
            }

            break;

        case CLI_WSSCFG_DOT11PHYFHSSTABLE:
            MEMSET (&WsscfgSetDot11PhyFHSSEntry, 0,
                    sizeof (tWsscfgDot11PhyFHSSEntry));
            MEMSET (&WsscfgIsSetDot11PhyFHSSEntry, 0,
                    sizeof (tWsscfgIsSetDot11PhyFHSSEntry));

            WSSCFG_FILL_DOT11PHYFHSSTABLE_ARGS ((&WsscfgSetDot11PhyFHSSEntry),
                                                (&WsscfgIsSetDot11PhyFHSSEntry),
                                                args[0], args[1], args[2],
                                                args[3], args[4], args[5],
                                                args[6], args[7], args[8],
                                                args[9], args[10], args[11],
                                                args[12]);

            i4RetStatus =
                WsscfgCliSetDot11PhyFHSSTable (CliHandle,
                                               (&WsscfgSetDot11PhyFHSSEntry),
                                               (&WsscfgIsSetDot11PhyFHSSEntry));
            break;

        case CLI_WSSCFG_DOT11PHYDSSSTABLE:
            MEMSET (&WsscfgSetDot11PhyDSSSEntry, 0,
                    sizeof (tWsscfgDot11PhyDSSSEntry));
            MEMSET (&WsscfgIsSetDot11PhyDSSSEntry, 0,
                    sizeof (tWsscfgIsSetDot11PhyDSSSEntry));

            /* When Profile name is received as input get the profile id
             * from mapping table and pass the profile name as NULL */
            if (CapwapGetWtpProfileIdFromProfileName
                (au1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            for (u1RadioIndex = 1;
                 u1RadioIndex <= SYS_DEF_MAX_RADIO_INTERFACES; u1RadioIndex++)
            {

                if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                    (u4WtpProfileId, u1RadioIndex,
                     &i4RadioIfIndex) != SNMP_SUCCESS)
                {
                    continue;
                }

                if (nmhGetFsDot11RadioType
                    (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                               u1RadioIndex);
                    continue;
                }

                if (u4RadioIfType == CLI_RADIO_TYPEB)
                {
                    WSSCFG_FILL_DOT11PHYDSSSTABLE_ARGS ((&WsscfgSetDot11PhyDSSSEntry), (&WsscfgIsSetDot11PhyDSSSEntry), args[0], args[1], args[2], &i4RadioIfIndex);

                    if (WsscfgCliSetDot11PhyDSSSTable
                        (CliHandle, (&WsscfgSetDot11PhyDSSSEntry),
                         (&WsscfgIsSetDot11PhyDSSSEntry)) != OSIX_SUCCESS)
                    {
                        continue;

                    }
                }

            }

            break;

        case CLI_WSSCFG_DOT11PHYIRTABLE:
            MEMSET (&WsscfgSetDot11PhyIREntry, 0,
                    sizeof (tWsscfgDot11PhyIREntry));
            MEMSET (&WsscfgIsSetDot11PhyIREntry, 0,
                    sizeof (tWsscfgIsSetDot11PhyIREntry));

            WSSCFG_FILL_DOT11PHYIRTABLE_ARGS ((&WsscfgSetDot11PhyIREntry),
                                              (&WsscfgIsSetDot11PhyIREntry),
                                              args[0], args[1], args[2],
                                              args[3], args[4]);

            i4RetStatus =
                WsscfgCliSetDot11PhyIRTable (CliHandle,
                                             (&WsscfgSetDot11PhyIREntry),
                                             (&WsscfgIsSetDot11PhyIREntry));
            break;

        case CLI_WSSCFG_DOT11ANTENNASLISTTABLE:
            MEMSET (&WsscfgSetDot11AntennasListEntry, 0,
                    sizeof (tWsscfgDot11AntennasListEntry));
            MEMSET (&WsscfgIsSetDot11AntennasListEntry, 0,
                    sizeof (tWsscfgIsSetDot11AntennasListEntry));

            /* WSS Changes Starts */
            if (args[5] != NULL)
            {

                if (CapwapGetWtpProfileIdFromProfileName
                    (au1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n WTP Profile not found. Binding failed\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                u1RadioId = CLI_PTR_TO_U4 (*args[6]);

                if (u1RadioId == 0)
                {
                    for (u1RadioIndex = 1;
                         u1RadioIndex <= SYS_DEF_MAX_RADIO_INTERFACES;
                         u1RadioIndex++)
                    {

                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d : Virtual Radio If Index not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (u4RadioIfType == *args[7])
                        {
                            WSSCFG_FILL_DOT11ANTENNASLISTTABLE_ARGS ((&WsscfgSetDot11AntennasListEntry), (&WsscfgIsSetDot11AntennasListEntry), args[0], args[1], args[2], args[3], &i4RadioIfIndex);
                            if ((WsscfgCliSetDot11AntennasListTable
                                 (CliHandle,
                                  (&WsscfgSetDot11AntennasListEntry),
                                  (&WsscfgIsSetDot11AntennasListEntry)))
                                != OSIX_SUCCESS)
                            {

                                CliPrintf (CliHandle,
                                           "\r\nFor Radio %d :  Values cant be successfully set,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                           u1RadioIndex);
                                continue;

                            }
                        }
                    }
                }
                else
                {
                    nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u1RadioId, &i4RadioIfIndex);

                    if (nmhGetFsDot11RadioType
                        (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed \r\n");
                        break;
                    }
                    if (u4RadioIfType != *args[7])
                    {
                        break;
                    }
                    else
                    {
                        WSSCFG_FILL_DOT11ANTENNASLISTTABLE_ARGS ((&WsscfgSetDot11AntennasListEntry), (&WsscfgIsSetDot11AntennasListEntry), args[0], args[1], args[2], args[3], &i4RadioIfIndex);
                        i4RetStatus =
                            WsscfgCliSetDot11AntennasListTable (CliHandle,
                                                                (&WsscfgSetDot11AntennasListEntry),
                                                                (&WsscfgIsSetDot11AntennasListEntry));
                    }

                }

            }
            else if (args[5] == NULL)
            {
                for (u2WtpProfileId = 1; u2WtpProfileId <= CLI_MAX_AP;
                     u2WtpProfileId++)
                {
                    for (u1RadioIndex = 1;
                         u1RadioIndex <= SYS_DEF_MAX_RADIO_INTERFACES;
                         u1RadioIndex++)
                    {

                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nIn Ap (%d) , For Radio %d : Virtual Radio If Index not successfully obtained,Couldnt proceed,operation failed for the particular Radio\r\n",
                                       u2WtpProfileId, u1RadioIndex);
                            continue;
                        }
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (u4RadioIfType == *args[7])
                        {
                            WSSCFG_FILL_DOT11ANTENNASLISTTABLE_ARGS ((&WsscfgSetDot11AntennasListEntry), (&WsscfgIsSetDot11AntennasListEntry), args[0], args[1], args[2], args[3], &i4RadioIfIndex);
                            if ((WsscfgCliSetDot11AntennasListTable
                                 (CliHandle,
                                  (&WsscfgSetDot11AntennasListEntry),
                                  (&WsscfgIsSetDot11AntennasListEntry)))
                                != OSIX_SUCCESS)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nr Ap(%d)For Radio %d :  Values cant be successfully set,Couldnt proceed ,operation failed for the particular Radio\r\n",
                                           u2WtpProfileId, u1RadioIndex);
                                continue;
                            }
                        }
                    }
                }
            }
            break;

        case CLI_WSSCFG_DOT11PHYOFDMTABLE:
            MEMSET (&WsscfgSetDot11PhyOFDMEntry, 0,
                    sizeof (tWsscfgDot11PhyOFDMEntry));
            MEMSET (&WsscfgIsSetDot11PhyOFDMEntry, 0,
                    sizeof (tWsscfgIsSetDot11PhyOFDMEntry));

            /* When Profile name is received as input get the profile id
             * from mapping table and pass the profile name as NULL */
            if (CapwapGetWtpProfileIdFromProfileName
                (au1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\n WTP Profile not found. \r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            for (u1RadioIndex = 1;
                 u1RadioIndex <= SYS_DEF_MAX_RADIO_INTERFACES; u1RadioIndex++)
            {

                if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                    (u4WtpProfileId, u1RadioIndex,
                     &i4RadioIfIndex) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nFor Radio %d : Virtual Radio If Index not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                               u1RadioIndex);
                    continue;
                }

                if (nmhGetFsDot11RadioType
                    (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                               u1RadioIndex);
                    continue;
                }

                if (u4RadioIfType == CLI_RADIO_TYPEA)
                {
                    WSSCFG_FILL_DOT11PHYOFDMTABLE_ARGS ((&WsscfgSetDot11PhyOFDMEntry), (&WsscfgIsSetDot11PhyOFDMEntry), args[0], args[1], args[2], args[3], &i4RadioIfIndex);

                    if (WsscfgCliSetDot11PhyOFDMTable
                        (CliHandle, (&WsscfgSetDot11PhyOFDMEntry),
                         (&WsscfgIsSetDot11PhyOFDMEntry)) != OSIX_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d :  Values cant be successfully set,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                   u1RadioIndex);
                        continue;

                    }
                }

            }

            break;

        case CLI_WSSCFG_DOT11HOPPINGPATTERNTABLE:
            MEMSET (&WsscfgSetDot11HoppingPatternEntry, 0,
                    sizeof (tWsscfgDot11HoppingPatternEntry));
            MEMSET (&WsscfgIsSetDot11HoppingPatternEntry, 0,
                    sizeof (tWsscfgIsSetDot11HoppingPatternEntry));

            WSSCFG_FILL_DOT11HOPPINGPATTERNTABLE_ARGS ((&WsscfgSetDot11HoppingPatternEntry), (&WsscfgIsSetDot11HoppingPatternEntry), args[0], args[1], args[2]);

            i4RetStatus =
                WsscfgCliSetDot11HoppingPatternTable (CliHandle,
                                                      (&WsscfgSetDot11HoppingPatternEntry),
                                                      (&WsscfgIsSetDot11HoppingPatternEntry));
            break;

        case CLI_WSSCFG_DOT11PHYERPTABLE:
            MEMSET (&WsscfgSetDot11PhyERPEntry, 0,
                    sizeof (tWsscfgDot11PhyERPEntry));
            MEMSET (&WsscfgIsSetDot11PhyERPEntry, 0,
                    sizeof (tWsscfgIsSetDot11PhyERPEntry));

            WSSCFG_FILL_DOT11PHYERPTABLE_ARGS ((&WsscfgSetDot11PhyERPEntry),
                                               (&WsscfgIsSetDot11PhyERPEntry),
                                               args[0], args[1], args[2],
                                               args[3], args[4]);

            i4RetStatus =
                WsscfgCliSetDot11PhyERPTable (CliHandle,
                                              (&WsscfgSetDot11PhyERPEntry),
                                              (&WsscfgIsSetDot11PhyERPEntry));
            break;

        case CLI_WSSCFG_DOT11RSNACONFIGTABLE:
            MEMSET (&WsscfgSetDot11RSNAConfigEntry, 0,
                    sizeof (tWsscfgDot11RSNAConfigEntry));
            MEMSET (&WsscfgIsSetDot11RSNAConfigEntry, 0,
                    sizeof (tWsscfgIsSetDot11RSNAConfigEntry));

            WSSCFG_FILL_DOT11RSNACONFIGTABLE_ARGS ((&WsscfgSetDot11RSNAConfigEntry), (&WsscfgIsSetDot11RSNAConfigEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17], args[18], args[19], args[20], args[21]);

            i4RetStatus =
                WsscfgCliSetDot11RSNAConfigTable (CliHandle,
                                                  (&WsscfgSetDot11RSNAConfigEntry),
                                                  (&WsscfgIsSetDot11RSNAConfigEntry));
            break;

        case CLI_WSSCFG_DOT11RSNACONFIGPAIRWISECIPHERSTABLE:
            MEMSET (&WsscfgSetDot11RSNAConfigPairwiseCiphersEntry, 0,
                    sizeof (tWsscfgDot11RSNAConfigPairwiseCiphersEntry));
            MEMSET (&WsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry, 0,
                    sizeof (tWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry));

            WSSCFG_FILL_DOT11RSNACONFIGPAIRWISECIPHERSTABLE_ARGS ((&WsscfgSetDot11RSNAConfigPairwiseCiphersEntry), (&WsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry), args[0], args[1], args[2]);

            i4RetStatus =
                WsscfgCliSetDot11RSNAConfigPairwiseCiphersTable (CliHandle,
                                                                 (&WsscfgSetDot11RSNAConfigPairwiseCiphersEntry),
                                                                 (&WsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry));
            break;

        case CLI_WSSCFG_DOT11RSNACONFIGAUTHENTICATIONSUITESTABLE:
            MEMSET (&WsscfgSetDot11RSNAConfigAuthenticationSuitesEntry, 0,
                    sizeof (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry));
            MEMSET (&WsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry,
                    0,
                    sizeof
                    (tWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry));

            WSSCFG_FILL_DOT11RSNACONFIGAUTHENTICATIONSUITESTABLE_ARGS ((&WsscfgSetDot11RSNAConfigAuthenticationSuitesEntry), (&WsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry), args[0], args[1]);

            i4RetStatus =
                WsscfgCliSetDot11RSNAConfigAuthenticationSuitesTable
                (CliHandle,
                 (&WsscfgSetDot11RSNAConfigAuthenticationSuitesEntry),
                 (&WsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry));
            break;
        case CLI_WSSCFG_FSDOT11STATIONCONFIGTABLE:
            MEMSET (&WsscfgSetFsDot11StationConfigEntry, 0,
                    sizeof (tWsscfgFsDot11StationConfigEntry));
            MEMSET (&WsscfgIsSetFsDot11StationConfigEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11StationConfigEntry));

            if (args[2] != NULL)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (CLI_PTR_TO_U4 (*args[2]), &i4WlanIfIndex) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\nGetting WlanProfileIndex failed\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = CLI_FAILURE;
                    break;

                }
            }
            if (args[4] != NULL)
            {
                if (WssCfgGetWlanIfIndexfromSSID
                    ((UINT1 *) args[4],
                     (UINT4 *) &i4WlanIfIndex) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\nGetting index failed\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = CLI_FAILURE;
                    break;

                }
            }
            WSSCFG_FILL_FSDOT11STATIONCONFIGTABLE_ARGS
                ((&WsscfgSetFsDot11StationConfigEntry),
                 (&WsscfgIsSetFsDot11StationConfigEntry), args[0], args[1],
                 &i4WlanIfIndex, NULL);

            i4RetStatus =
                WsscfgCliSetFsDot11StationConfigTable (CliHandle,
                                                       (&WsscfgSetFsDot11StationConfigEntry),
                                                       (&WsscfgIsSetFsDot11StationConfigEntry));
            break;

        case CLI_WSSCFG_FSDOT11CAPABILITYPROFILETABLE:
            MEMSET (&WsscfgSetFsDot11CapabilityProfileEntry, 0,
                    sizeof (tWsscfgFsDot11CapabilityProfileEntry));
            MEMSET (&WsscfgIsSetFsDot11CapabilityProfileEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11CapabilityProfileEntry));

            CLI_GET_CAPABID ((INT1 *) au1PromptName);
            pu1Name = (UINT1 *) strtok_r (pu1PromptName, "/", &saveptr);
            u4Len = STRLEN (pu1Name);

            WSSCFG_FILL_FSDOT11CAPABILITYPROFILETABLE_ARGS ((&WsscfgSetFsDot11CapabilityProfileEntry), (&WsscfgIsSetFsDot11CapabilityProfileEntry), pu1Name, &u4Len, args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17], args[18], args[19], args[20]);

            i4RetStatus =
                WsscfgCliSetFsDot11CapabilityProfileTable (CliHandle,
                                                           (&WsscfgSetFsDot11CapabilityProfileEntry),
                                                           (&WsscfgIsSetFsDot11CapabilityProfileEntry));
            break;
        case CLI_WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE:
            MEMSET (&WsscfgSetFsDot11AuthenticationProfileEntry, 0,
                    sizeof (tWsscfgFsDot11AuthenticationProfileEntry));
            MEMSET (&WsscfgIsSetFsDot11AuthenticationProfileEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11AuthenticationProfileEntry));

            CLI_GET_AUTHID ((INT1 *) au1PromptName);
            pu1Name = (UINT1 *) strtok_r (pu1PromptName, "/", &saveptr);
            u4Len = STRLEN (pu1Name);

            WSSCFG_FILL_FSDOT11AUTHENTICATIONPROFILETABLE_ARGS ((&WsscfgSetFsDot11AuthenticationProfileEntry), (&WsscfgIsSetFsDot11AuthenticationProfileEntry), pu1Name, &u4Len, args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9]);

            i4RetStatus =
                WsscfgCliSetFsDot11AuthenticationProfileTable (CliHandle,
                                                               (&WsscfgSetFsDot11AuthenticationProfileEntry),
                                                               (&WsscfgIsSetFsDot11AuthenticationProfileEntry));
            break;

        case CLI_WSSCFG_FSSECURITYWEBAUTHGUESTINFOTABLE:
            MEMSET (&WsscfgSetFsSecurityWebAuthGuestInfoEntry, 0,
                    sizeof (tWsscfgFsSecurityWebAuthGuestInfoEntry));
            MEMSET (&WsscfgIsSetFsSecurityWebAuthGuestInfoEntry, 0,
                    sizeof (tWsscfgIsSetFsSecurityWebAuthGuestInfoEntry));

#ifdef WLC_WANTED
            if (WssStaWebAuthSockInit () != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
#endif
            WSSCFG_FILL_FSSECURITYWEBAUTHGUESTINFOTABLE_ARGS ((&WsscfgSetFsSecurityWebAuthGuestInfoEntry), (&WsscfgIsSetFsSecurityWebAuthGuestInfoEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6]);

            i4RetStatus =
                WsscfgCliSetFsSecurityWebAuthGuestInfoTable (CliHandle,
                                                             (&WsscfgSetFsSecurityWebAuthGuestInfoEntry),
                                                             (&WsscfgIsSetFsSecurityWebAuthGuestInfoEntry));
            break;

        case CLI_WSSCFG_FSSTATIONQOSPARAMSTABLE:
            MEMSET (&WsscfgSetFsStationQosParamsEntry, 0,
                    sizeof (tWsscfgFsStationQosParamsEntry));
            MEMSET (&WsscfgIsSetFsStationQosParamsEntry, 0,
                    sizeof (tWsscfgIsSetFsStationQosParamsEntry));

            WSSCFG_FILL_FSSTATIONQOSPARAMSTABLE_ARGS ((&WsscfgSetFsStationQosParamsEntry), (&WsscfgIsSetFsStationQosParamsEntry), args[0], args[1], args[2], args[3]);

            i4RetStatus =
                WsscfgCliSetFsStationQosParamsTable (CliHandle,
                                                     (&WsscfgSetFsStationQosParamsEntry),
                                                     (&WsscfgIsSetFsStationQosParamsEntry));
            break;

        case CLI_WSSCFG_FSDOT11QAPTABLE:
            MEMSET (&WsscfgSetFsDot11QAPEntry, 0,
                    sizeof (tWsscfgFsDot11QAPEntry));
            MEMSET (&WsscfgIsSetFsDot11QAPEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11QAPEntry));

            /* WSS Changes Starts */
            if (args[5] != NULL)
            {

                if (CapwapGetWtpProfileIdFromProfileName
                    (au1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n WTP Profile not found. Binding failed\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                u1RadioId = CLI_PTR_TO_U4 (*args[6]);

                if (u1RadioId == 0)
                {
                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nModel No not successfully obtained , (Invalid Input, Check Profile name)\r\n");
                        break;
                    }

                    if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nNo.of Radios present not successfully obtained, Operation Failed\r\n");
                        break;

                    }

                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {

                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d : Virtual Radio If Index not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (u4RadioIfType == *args[7])
                        {
                            WSSCFG_FILL_FSDOT11QAPTABLE_ARGS ((&WsscfgSetFsDot11QAPEntry), (&WsscfgIsSetFsDot11QAPEntry), args[0], args[1], args[2], &i4RadioIfIndex, args[4]);
                            if ((WsscfgCliSetFsDot11QAPTable
                                 (CliHandle, (&WsscfgSetFsDot11QAPEntry),
                                  (&WsscfgIsSetFsDot11QAPEntry))) !=
                                OSIX_SUCCESS)
                            {

                                CliPrintf (CliHandle,
                                           "\r\nFor Radio %d :  Values cant be successfully set,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                           u1RadioIndex);
                                continue;

                            }
                        }
                    }
                }
                else
                {
                    nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u1RadioId, &i4RadioIfIndex);

                    if (nmhGetFsDot11RadioType
                        (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed \r\n");
                        break;
                    }
                    if (u4RadioIfType != *args[7])
                    {
                        break;
                    }
                    else
                    {
                        WSSCFG_FILL_FSDOT11QAPTABLE_ARGS ((&WsscfgSetFsDot11QAPEntry), (&WsscfgIsSetFsDot11QAPEntry), args[0], args[1], args[2], &i4RadioIfIndex, args[4]);
                        i4RetStatus =
                            WsscfgCliSetFsDot11QAPTable (CliHandle,
                                                         (&WsscfgSetFsDot11QAPEntry),
                                                         (&WsscfgIsSetFsDot11QAPEntry));
                    }

                }

            }
            else if (args[5] == NULL)
            {
                for (u2WtpProfileId = 1; u2WtpProfileId <= CLI_MAX_AP;
                     u2WtpProfileId++)
                {

                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u2WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Ap (%d) : Model No not successfully obtained, Could not proceed for this AP\r\n",
                                   u2WtpProfileId);
                        continue;
                    }

                    if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nNo.of Radios present not successfully obtained, Operation Failed\r\n");
                        break;

                    }

                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {

                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nIn Ap (%d) , For Radio %d : Virtual Radio If Index not successfully obtained,Couldnt proceed,operation failed for the particular Radio\r\n",
                                       u2WtpProfileId, u1RadioIndex);
                            continue;
                        }

                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (u4RadioIfType == *args[7])
                        {
                            WSSCFG_FILL_FSDOT11QAPTABLE_ARGS ((&WsscfgSetFsDot11QAPEntry), (&WsscfgIsSetFsDot11QAPEntry), args[0], args[1], args[2], &i4RadioIfIndex, args[4]);
                            if ((WsscfgCliSetFsDot11QAPTable
                                 (CliHandle, (&WsscfgSetFsDot11QAPEntry),
                                  (&WsscfgIsSetFsDot11QAPEntry))) !=
                                OSIX_SUCCESS)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nr Ap(%d)For Radio %d :  Values cant be successfully set,Couldnt proceed ,operation failed for the particular Radio\r\n",
                                           u2WtpProfileId, u1RadioIndex);
                                continue;
                            }
                        }
                    }
                }
            }

        case CLI_WSSCFG_FSVLANISOLATIONTABLE:
            MEMSET (&WsscfgSetFsVlanIsolationEntry, 0,
                    sizeof (tWsscfgFsVlanIsolationEntry));
            MEMSET (&WsscfgIsSetFsVlanIsolationEntry, 0,
                    sizeof (tWsscfgIsSetFsVlanIsolationEntry));

            if (args[1] != NULL)
            {
                u4WlanProfileId = CLI_PTR_TO_U4 (*args[1]);
                nmhGetCapwapDot11WlanProfileIfIndex (u4WlanProfileId,
                                                     &i4WlanIfIndex);
            }

            WSSCFG_FILL_FSVLANISOLATIONTABLE_ARGS ((&WsscfgSetFsVlanIsolationEntry), (&WsscfgIsSetFsVlanIsolationEntry), args[0], &i4WlanIfIndex);

            i4RetStatus =
                WsscfgCliSetFsVlanIsolationTable (CliHandle,
                                                  (&WsscfgSetFsVlanIsolationEntry),
                                                  (&WsscfgIsSetFsVlanIsolationEntry));
            break;

        case CLI_WSSCFG_FSRRMCONFIGTABLE:
            MEMSET (&WsscfgSetFsRrmConfigEntry, 0,
                    sizeof (tWsscfgFsRrmConfigEntry));
            MEMSET (&WsscfgIsSetFsRrmConfigEntry, 0,
                    sizeof (tWsscfgIsSetFsRrmConfigEntry));

            WSSCFG_FILL_FSRRMCONFIGTABLE_ARGS ((&WsscfgSetFsRrmConfigEntry),
                                               (&WsscfgIsSetFsRrmConfigEntry),
                                               args[0], args[1], args[2],
                                               args[3], args[4], args[5]);

            i4RetStatus =
                WsscfgCliSetFsRrmConfigTable (CliHandle,
                                              (&WsscfgSetFsRrmConfigEntry),
                                              (&WsscfgIsSetFsRrmConfigEntry));
            break;

        case CLI_WSSCFG_ADMIN_STATUS_CHANGE:
            if ((args[0] == NULL) && (args[1] == NULL))
            {
                CliPrintf (CliHandle, "\r\nInvalid input\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }
            else
            {
                /* When Profile name is received as input get the profile id
                 * from mapping table and pass the profile name as NULL */
                if (CapwapGetWtpProfileIdFromProfileName
                    (au1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n WTP Profile not found. Binding failed\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (*args[2] == 0)
                {
                    for (u1RadioId = 1;
                         u1RadioId <= SYS_DEF_MAX_RADIO_INTERFACES; u1RadioId++)
                    {
                        if ((i4RetStatus =
                             nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                             (u4WtpProfileId, u1RadioId,
                              &i4RadioIfIndex)) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFailed to get ifindex entry\r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = CLI_FAILURE;
                            break;
                        }

                        if ((i4RetStatus =
                             nmhGetFsDot11RadioType (i4RadioIfIndex,
                                                     &u4GetRadioType)) !=
                            SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFailed to get radio type\r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = CLI_FAILURE;
                            break;
                        }
                        if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[3]))
                        {
                            CliPrintf (CliHandle,
                                       "\r\n Radio Type conflicting, binding failed\r\n");
                            CLI_FATAL_ERROR (CliHandle);
                            i4RetStatus = OSIX_FAILURE;
                            break;
                        }
                        else
                        {
                            if ((i4RetStatus =
                                 nmhTestv2IfMainAdminStatus (&u4ErrorCode,
                                                             i4RadioIfIndex,
                                                             *args[0])) !=
                                SNMP_SUCCESS)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nInvalid Admin Status received\r\n");
                                CLI_FATAL_ERROR (CliHandle);
                                i4RetStatus = CLI_FAILURE;
                                break;
                            }
                            if ((i4RetStatus =
                                 nmhSetIfMainAdminStatus (i4RadioIfIndex,
                                                          *args[0])) !=
                                SNMP_SUCCESS)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nAdmin Status Configuration failed\r\n");
                                CLI_FATAL_ERROR (CliHandle);
                                i4RetStatus = CLI_FAILURE;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    if ((i4RetStatus =
                         nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                         (u4WtpProfileId, *args[2],
                          &i4RadioIfIndex)) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFailed to get ifindex entry\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }

                    if ((i4RetStatus =
                         nmhGetFsDot11RadioType (i4RadioIfIndex,
                                                 &u4GetRadioType)) !=
                        SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFailed to get radio type\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }
                    if (WSSCFG_RADIO_TYPE_COMP ((INT4) *args[3]))
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Radio Type conflicting, binding failed\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }

                    if ((i4RetStatus =
                         nmhTestv2IfMainAdminStatus (&u4ErrorCode,
                                                     i4RadioIfIndex,
                                                     *args[0])) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nInvalid Admin Status received\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }
                    if ((i4RetStatus =
                         nmhSetIfMainAdminStatus (i4RadioIfIndex,
                                                  *args[0])) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nAdmin Status Configuration failed\r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }
                }
                break;
            }

        case CLI_WSSCFG_FSDOT11RADIOCONFIGTABLE:
            MEMSET (&WsscfgSetFsDot11RadioConfigEntry, 0,
                    sizeof (tWsscfgFsDot11RadioConfigEntry));
            MEMSET (&WsscfgIsSetFsDot11RadioConfigEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11RadioConfigEntry));

            /* When Profile name is received as input get the profile id
             * from mapping table and pass the profile name as NULL */
            if (CapwapGetWtpProfileIdFromProfileName
                (au1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\n WTP Profile not found.\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, *args[3], &i4RadioIfIndex) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r\n: Failed to get the interface index \r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
                break;
            }

            /* Adding below to create table for radio type */
            if ((nmhGetFsDot11RowStatus
                 (i4RadioIfIndex, (INT4 *) &i4RowStatus)) != SNMP_SUCCESS)
            {
                i4RowStatus = CREATE_AND_GO;
                WSSCFG_FILL_FSDOT11RADIOCONFIGTABLE_ARGS ((&WsscfgSetFsDot11RadioConfigEntry), (&WsscfgIsSetFsDot11RadioConfigEntry), NULL, &i4RowStatus, &i4RadioIfIndex);

                i4RetStatus =
                    WsscfgCliSetFsDot11RadioConfigTable (CliHandle,
                                                         (&WsscfgSetFsDot11RadioConfigEntry),
                                                         (&WsscfgIsSetFsDot11RadioConfigEntry));
                if (i4RetStatus == OSIX_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }

            WSSCFG_FILL_FSDOT11RADIOCONFIGTABLE_ARGS ((&WsscfgSetFsDot11RadioConfigEntry), (&WsscfgIsSetFsDot11RadioConfigEntry), args[0], args[1], &i4RadioIfIndex);

            i4RetStatus =
                WsscfgCliSetFsDot11RadioConfigTable (CliHandle,
                                                     (&WsscfgSetFsDot11RadioConfigEntry),
                                                     (&WsscfgIsSetFsDot11RadioConfigEntry));
            break;

        case CLI_WSSCFG_FSDOT11QOSPROFILETABLE:
            MEMSET (&WsscfgSetFsDot11QosProfileEntry, 0,
                    sizeof (tWsscfgFsDot11QosProfileEntry));
            MEMSET (&WsscfgIsSetFsDot11QosProfileEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11QosProfileEntry));

            CLI_GET_QOSID ((INT1 *) au1PromptName);
            pu1Name = (UINT1 *) strtok_r (pu1PromptName, "/", &saveptr);
            u4Len = STRLEN (pu1Name);
            WSSCFG_FILL_FSDOT11QOSPROFILETABLE_ARGS ((&WsscfgSetFsDot11QosProfileEntry), (&WsscfgIsSetFsDot11QosProfileEntry), pu1Name, &u4Len, args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13]);

            i4RetStatus =
                WsscfgCliSetFsDot11QosProfileTable (CliHandle,
                                                    (&WsscfgSetFsDot11QosProfileEntry),
                                                    (&WsscfgIsSetFsDot11QosProfileEntry));
            break;

        case CLI_WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE:
            MEMSET (&WsscfgSetFsDot11WlanCapabilityProfileEntry, 0,
                    sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));
            MEMSET (&WsscfgIsSetFsDot11WlanCapabilityProfileEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11WlanCapabilityProfileEntry));

            if (args[20] != NULL)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (CLI_PTR_TO_U4 (*args[20]), &i4WlanIfIndex) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (i4WlanIfIndex == 0)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }

            WSSCFG_FILL_FSDOT11WLANCAPABILITYPROFILETABLE_ARGS ((&WsscfgSetFsDot11WlanCapabilityProfileEntry), (&WsscfgIsSetFsDot11WlanCapabilityProfileEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17], args[18], &i4WlanIfIndex);

            i4RetStatus =
                WsscfgCliSetFsDot11WlanCapabilityProfileTable (CliHandle,
                                                               (&WsscfgSetFsDot11WlanCapabilityProfileEntry),
                                                               (&WsscfgIsSetFsDot11WlanCapabilityProfileEntry));
            break;

        case CLI_WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE:
            MEMSET (&WsscfgSetFsDot11WlanAuthenticationProfileEntry, 0,
                    sizeof (tWsscfgFsDot11WlanAuthenticationProfileEntry));
            MEMSET (&WsscfgIsSetFsDot11WlanAuthenticationProfileEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry));

            if (args[9] != NULL)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (CLI_PTR_TO_U4 (*args[9]), &i4WlanIfIndex) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (i4WlanIfIndex == 0)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }

            WSSCFG_FILL_FSDOT11WLANAUTHENTICATIONPROFILETABLE_ARGS ((&WsscfgSetFsDot11WlanAuthenticationProfileEntry), (&WsscfgIsSetFsDot11WlanAuthenticationProfileEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], &i4WlanIfIndex);

            i4RetStatus =
                WsscfgCliSetFsDot11WlanAuthenticationProfileTable
                (CliHandle,
                 (&WsscfgSetFsDot11WlanAuthenticationProfileEntry),
                 (&WsscfgIsSetFsDot11WlanAuthenticationProfileEntry));
            break;

        case CLI_WSSCFG_FSDOT11WLANQOSPROFILETABLE:
            MEMSET (&WsscfgSetFsDot11WlanQosProfileEntry, 0,
                    sizeof (tWsscfgFsDot11WlanQosProfileEntry));
            MEMSET (&WsscfgIsSetFsDot11WlanQosProfileEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11WlanQosProfileEntry));

            if (args[12] != NULL)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (CLI_PTR_TO_U4 (*args[12]), &i4WlanIfIndex) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (i4WlanIfIndex == 0)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }
            /*WSSCFG_FILL_FSDOT11WLANQOSPROFILETABLE_ARGS((&WsscfgSetFsDot11WlanQosProfileEntry),(&WsscfgIsSetFsDot11WlanQosProfileEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], &i4WlanIfIndex); */

            WSSCFG_FILL_FSDOT11WLANQOSPROFILETABLE_ARGS ((&WsscfgSetFsDot11WlanQosProfileEntry), (&WsscfgIsSetFsDot11WlanQosProfileEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], &i4WlanIfIndex);

            i4RetStatus =
                WsscfgCliSetFsDot11WlanQosProfileTable (CliHandle,
                                                        (&WsscfgSetFsDot11WlanQosProfileEntry),
                                                        (&WsscfgIsSetFsDot11WlanQosProfileEntry));
            break;

        case CLI_WSSCFG_FSDOT11RADIOQOSTABLE:
            MEMSET (&WsscfgSetFsDot11RadioQosEntry, 0,
                    sizeof (tWsscfgFsDot11RadioQosEntry));
            MEMSET (&WsscfgIsSetFsDot11RadioQosEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11RadioQosEntry));

            /* WSS Changes Starts */
            if (args[5] != NULL)
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    (au1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n WTP Profile not found. Binding failed\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                u1RadioId = CLI_PTR_TO_U4 (*args[6]);

                if (u1RadioId == 0)
                {
                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nModel No not successfully obtained , (Invalid Input, Check Profile name)\r\n");
                        break;
                    }

                    if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nNo.of Radios present not successfully obtained, Operation Failed\r\n");
                        break;

                    }

                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {

                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             (INT4 *) &u4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d : Virtual Radio If Index not successfully obtained,"
                                       "Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (nmhGetFsDot11RadioType
                            (u4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d :  RadioIf Type not successfully obtained,"
                                       "Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (u4RadioIfType == *args[7])
                        {
                            WsscfgSetFsDot11RadioQosEntry.
                                MibObject.au1FsDot11TaggingPolicy[0] =
                                *(UINT1 *) args[3];
                            WsscfgIsSetFsDot11RadioQosEntry.
                                bFsDot11TaggingPolicy = OSIX_TRUE;
                            if (u4RadioIfIndex != 0)
                            {
                                WsscfgSetFsDot11RadioQosEntry.
                                    MibObject.i4IfIndex = u4RadioIfIndex;
                                WsscfgIsSetFsDot11RadioQosEntry.bIfIndex =
                                    OSIX_TRUE;
                            }
                            else
                            {
                                WsscfgIsSetFsDot11RadioQosEntry.bIfIndex =
                                    OSIX_FALSE;
                            }
                            /*                            WSSCFG_FILL_FSDOT11RADIOQOSTABLE_ARGS((&WsscfgSetFsDot11RadioQosEntry),
                               (&WsscfgIsSetFsDot11RadioQosEntry),args[3], NULL, &u4RadioIfIndex);        */

                            if ((WsscfgCliSetFsDot11RadioQosTable
                                 (CliHandle,
                                  &WsscfgSetFsDot11RadioQosEntry,
                                  &WsscfgIsSetFsDot11RadioQosEntry)) !=
                                OSIX_SUCCESS)
                            {

                                CliPrintf (CliHandle,
                                           "\r\nFor Radio %d :  Values cant be successfully set,"
                                           "Couldnt proceed ,operation failed for the particular Radio \r\n",
                                           u1RadioIndex);
                                continue;

                            }
                        }
                    }
                }
                else
                {
                    nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u1RadioId, (INT4 *) &u4RadioIfIndex);

                    if (nmhGetFsDot11RadioType
                        (u4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed \r\n");
                        break;
                    }
                    if (u4RadioIfType != *args[7])
                    {
                        break;
                    }
                    else
                    {
                        WsscfgSetFsDot11RadioQosEntry.
                            MibObject.au1FsDot11TaggingPolicy[0] =
                            *(UINT1 *) args[3];
                        WsscfgIsSetFsDot11RadioQosEntry.
                            bFsDot11TaggingPolicy = OSIX_TRUE;
                        if (u4RadioIfIndex != 0)
                        {
                            WsscfgSetFsDot11RadioQosEntry.MibObject.
                                i4IfIndex = u4RadioIfIndex;
                            WsscfgIsSetFsDot11RadioQosEntry.bIfIndex =
                                OSIX_TRUE;
                        }
                        else
                        {
                            WsscfgIsSetFsDot11RadioQosEntry.bIfIndex =
                                OSIX_FALSE;
                        }
                        /*                           WSSCFG_FILL_FSDOT11RADIOQOSTABLE_ARGS((&WsscfgSetFsDot11RadioQosEntry),i
                           (&WsscfgIsSetFsDot11RadioQosEntry),args[3], len,&u4RadioIfIndex);   */

                        i4RetStatus =
                            WsscfgCliSetFsDot11RadioQosTable (CliHandle,
                                                              (&WsscfgSetFsDot11RadioQosEntry),
                                                              (&WsscfgIsSetFsDot11RadioQosEntry));
                    }

                }

            }
            else if (args[5] == NULL)
            {
                for (u2WtpProfileId = 1; u2WtpProfileId <= CLI_MAX_AP;
                     u2WtpProfileId++)
                {
                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u2WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Ap (%d) : Model No not successfully obtained,"
                                   " Could not proceed for this AP\r\n",
                                   u2WtpProfileId);
                        continue;
                    }

                    if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nNo.of Radios present not successfully obtained, Operation Failed\r\n");
                        break;

                    }

                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {

                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             (INT4 *) &u4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nIn Ap (%d) , For Radio %d : Virtual Radio If Index not successfully obtained,"
                                       "Couldnt proceed,operation failed for the particular Radio\r\n",
                                       u2WtpProfileId, u1RadioIndex);
                            continue;
                        }

                        if (nmhGetFsDot11RadioType
                            (u4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d :  RadioIf Type not successfully obtained,"
                                       "Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (u4RadioIfType == *args[7])
                        {
                            WsscfgSetFsDot11RadioQosEntry.
                                MibObject.au1FsDot11TaggingPolicy[0] =
                                *(UINT1 *) args[3];
                            WsscfgIsSetFsDot11RadioQosEntry.
                                bFsDot11TaggingPolicy = OSIX_TRUE;
                            if (u4RadioIfIndex != 0)
                            {
                                WsscfgSetFsDot11RadioQosEntry.
                                    MibObject.i4IfIndex = u4RadioIfIndex;
                                WsscfgIsSetFsDot11RadioQosEntry.bIfIndex =
                                    OSIX_TRUE;
                            }
                            else
                            {
                                WsscfgIsSetFsDot11RadioQosEntry.bIfIndex =
                                    OSIX_FALSE;
                            }
                            /*                 WSSCFG_FILL_FSDOT11RADIOQOSTABLE_ARGS((&WsscfgSetFsDot11RadioQosEntry),(&WsscfgIsSetFsDot11RadioQosEntry),
                               args[3], NULL, &u4RadioIfIndex);  */

                            if ((WsscfgCliSetFsDot11RadioQosTable
                                 (CliHandle,
                                  &WsscfgSetFsDot11RadioQosEntry,
                                  &WsscfgIsSetFsDot11RadioQosEntry)) !=
                                OSIX_SUCCESS)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nr Ap(%d)For Radio %d :  Values cant be successfully set,"
                                           "Couldnt proceed ,operation failed for the particular Radio\r\n",
                                           u2WtpProfileId, u1RadioIndex);
                                continue;
                            }
                        }
                    }
                }
            }
            break;

        case CLI_WSSCFG_FSQAPPROFILETABLE:
            MEMSET (&WsscfgSetFsQAPProfileEntry, 0,
                    sizeof (tWsscfgFsQAPProfileEntry));
            MEMSET (&WsscfgIsSetFsQAPProfileEntry, 0,
                    sizeof (tWsscfgIsSetFsQAPProfileEntry));

            WSSCFG_FILL_FSQAPPROFILETABLE_ARGS ((&WsscfgSetFsQAPProfileEntry),
                                                (&WsscfgIsSetFsQAPProfileEntry),
                                                args[0], args[1], args[2],
                                                args[3], args[4], args[5],
                                                args[6], args[7], args[8],
                                                args[9], args[10]);

            i4RetStatus =
                WsscfgCliSetFsQAPProfileTable (CliHandle,
                                               (&WsscfgSetFsQAPProfileEntry),
                                               (&WsscfgIsSetFsQAPProfileEntry));
            break;

        case CLI_WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE:
            MEMSET (&WsscfgSetFsDot11CapabilityMappingEntry, 0,
                    sizeof (tWsscfgFsDot11CapabilityMappingEntry));
            MEMSET (&WsscfgIsSetFsDot11CapabilityMappingEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11CapabilityMappingEntry));

            if (args[3] != NULL)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (CLI_PTR_TO_U4 (*args[3]), &i4WlanIfIndex) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (i4WlanIfIndex == 0)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }
            WSSCFG_FILL_FSDOT11CAPABILITYMAPPINGTABLE_ARGS ((&WsscfgSetFsDot11CapabilityMappingEntry), (&WsscfgIsSetFsDot11CapabilityMappingEntry), args[0], args[1], args[2], &i4WlanIfIndex);

            i4RetStatus =
                WsscfgCliSetFsDot11CapabilityMappingTable (CliHandle,
                                                           (&WsscfgSetFsDot11CapabilityMappingEntry),
                                                           (&WsscfgIsSetFsDot11CapabilityMappingEntry));
            break;

        case CLI_WSSCFG_FSDOT11AUTHMAPPINGTABLE:
            MEMSET (&WsscfgSetFsDot11AuthMappingEntry, 0,
                    sizeof (tWsscfgFsDot11AuthMappingEntry));
            MEMSET (&WsscfgIsSetFsDot11AuthMappingEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11AuthMappingEntry));

            if (args[3] != NULL)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (CLI_PTR_TO_U4 (*args[3]), &i4WlanIfIndex) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (i4WlanIfIndex == 0)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }
            WSSCFG_FILL_FSDOT11AUTHMAPPINGTABLE_ARGS ((&WsscfgSetFsDot11AuthMappingEntry), (&WsscfgIsSetFsDot11AuthMappingEntry), args[0], args[1], args[2], &i4WlanIfIndex);

            i4RetStatus =
                WsscfgCliSetFsDot11AuthMappingTable (CliHandle,
                                                     (&WsscfgSetFsDot11AuthMappingEntry),
                                                     (&WsscfgIsSetFsDot11AuthMappingEntry));
            break;

        case CLI_WSSCFG_FSDOT11QOSMAPPINGTABLE:
            MEMSET (&WsscfgSetFsDot11QosMappingEntry, 0,
                    sizeof (tWsscfgFsDot11QosMappingEntry));
            MEMSET (&WsscfgIsSetFsDot11QosMappingEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11QosMappingEntry));
            if (args[3] != NULL)
            {
                if (nmhGetCapwapDot11WlanProfileIfIndex
                    (CLI_PTR_TO_U4 (*args[3]), &i4WlanIfIndex) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n Index does not exist \r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if (i4WlanIfIndex == 0)
                {
                    CliPrintf (CliHandle, "\r\n Invalid index\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
            }
            WSSCFG_FILL_FSDOT11QOSMAPPINGTABLE_ARGS ((&WsscfgSetFsDot11QosMappingEntry), (&WsscfgIsSetFsDot11QosMappingEntry), args[0], args[1], args[2], &i4WlanIfIndex);
            i4RetStatus =
                WsscfgCliSetFsDot11QosMappingTable (CliHandle,
                                                    (&WsscfgSetFsDot11QosMappingEntry),
                                                    (&WsscfgIsSetFsDot11QosMappingEntry));
            break;

        case CLI_WSSCFG_FSDOT11ANTENNASLISTTABLE:
            MEMSET (&WsscfgSetFsDot11AntennasListEntry, 0,
                    sizeof (tWsscfgFsDot11AntennasListEntry));
            MEMSET (&WsscfgIsSetFsDot11AntennasListEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11AntennasListEntry));

            /* WSS Changes Starts */
            if (args[4] != NULL)
            {

                if (CapwapGetWtpProfileIdFromProfileName
                    (au1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n WTP Profile not found. Binding failed\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                u1RadioId = CLI_PTR_TO_U4 (*args[5]);

                if (u1RadioId == 0)
                {
                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nModel No not successfully obtained , (Invalid Input, Check Profile name)\r\n");
                        break;
                    }

                    if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nNo.of Radios present not successfully obtained, Operation Failed\r\n");
                        break;

                    }
                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {

                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d : Virtual Radio If Index not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (u4RadioIfType == *args[6])
                        {
                            WSSCFG_FILL_FSDOT11ANTENNASLISTTABLE_ARGS ((&WsscfgSetFsDot11AntennasListEntry), (&WsscfgIsSetFsDot11AntennasListEntry), args[0], args[1], &i4RadioIfIndex, args[3]);
                            if ((WsscfgCliSetFsDot11AntennasListTable
                                 (CliHandle,
                                  (&WsscfgSetFsDot11AntennasListEntry),
                                  (&WsscfgIsSetFsDot11AntennasListEntry)))
                                != OSIX_SUCCESS)
                            {

                                CliPrintf (CliHandle,
                                           "\r\nFor Radio %d :  Values cant be successfully set,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                           u1RadioIndex);
                                continue;

                            }
                        }
                    }
                }
                else
                {
                    nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u1RadioId, &i4RadioIfIndex);

                    if (nmhGetFsDot11RadioType
                        (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed \r\n");
                    }
                    if (u4RadioIfType != *args[6])
                    {
                        break;
                    }
                    else
                    {
                        WSSCFG_FILL_FSDOT11ANTENNASLISTTABLE_ARGS ((&WsscfgSetFsDot11AntennasListEntry), (&WsscfgIsSetFsDot11AntennasListEntry), args[0], args[1], &i4RadioIfIndex, args[3]);
                        i4RetStatus =
                            WsscfgCliSetFsDot11AntennasListTable
                            (CliHandle,
                             (&WsscfgSetFsDot11AntennasListEntry),
                             (&WsscfgIsSetFsDot11AntennasListEntry));
                    }

                }

            }
            else if (args[4] == NULL)
            {
                for (u2WtpProfileId = 1; u2WtpProfileId <= CLI_MAX_AP;
                     u2WtpProfileId++)
                {

                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u2WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Ap (%d) : Model No not successfully obtained, Could not proceed for this AP\r\n",
                                   u2WtpProfileId);
                        continue;
                    }

                    if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nNo.of Radios present not successfully obtained, Operation Failed\r\n");
                        break;

                    }
                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {

                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nIn Ap (%d) , For Radio %d : Virtual Radio If Index not successfully obtained,Couldnt proceed,operation failed for the particular Radio\r\n",
                                       u2WtpProfileId, u1RadioIndex);
                            continue;
                        }

                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (u4RadioIfType == *args[7])
                        {
                            WSSCFG_FILL_FSDOT11ANTENNASLISTTABLE_ARGS ((&WsscfgSetFsDot11AntennasListEntry), (&WsscfgIsSetFsDot11AntennasListEntry), args[0], args[1], &i4RadioIfIndex, args[3]);
                            if ((WsscfgCliSetFsDot11AntennasListTable
                                 (CliHandle,
                                  (&WsscfgSetFsDot11AntennasListEntry),
                                  (&WsscfgIsSetFsDot11AntennasListEntry)))
                                != OSIX_SUCCESS)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nr Ap(%d)For Radio %d :  Values cant be successfully set,Couldnt proceed ,operation failed for the particular Radio\r\n",
                                           u2WtpProfileId, u1RadioIndex);
                                continue;
                            }
                        }
                    }
                }
            }
            break;

        case CLI_WSSCFG_FSDOT11WLANTABLE:
            MEMSET (&WsscfgSetFsDot11WlanEntry, 0,
                    sizeof (tWsscfgFsDot11WlanEntry));
            MEMSET (&WsscfgIsSetFsDot11WlanEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11WlanEntry));

            WSSCFG_FILL_FSDOT11WLANTABLE_ARGS ((&WsscfgSetFsDot11WlanEntry),
                                               (&WsscfgIsSetFsDot11WlanEntry),
                                               args[0], args[1], args[2]);

            i4RetStatus =
                WsscfgCliSetFsDot11WlanTable (CliHandle,
                                              (&WsscfgSetFsDot11WlanEntry),
                                              (&WsscfgIsSetFsDot11WlanEntry));
            break;

        case CLI_WSSCFG_FSDOT11WLANBINDTABLE:
            MEMSET (&WsscfgSetFsDot11WlanBindEntry, 0,
                    sizeof (tWsscfgFsDot11WlanBindEntry));
            MEMSET (&WsscfgIsSetFsDot11WlanBindEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11WlanBindEntry));

            WSSCFG_FILL_FSDOT11WLANBINDTABLE_ARGS ((&WsscfgSetFsDot11WlanBindEntry), (&WsscfgIsSetFsDot11WlanBindEntry), args[0], args[1], args[2], args[3], args[4]);

            i4RetStatus =
                WsscfgCliSetFsDot11WlanBindTable (CliHandle,
                                                  (&WsscfgSetFsDot11WlanBindEntry),
                                                  (&WsscfgIsSetFsDot11WlanBindEntry));
            break;

        case CLI_WSSCFG_FSWTPIMAGEUPGRADETABLE:
            MEMSET (&WsscfgSetFsWtpImageUpgradeEntry, 0,
                    sizeof (tWsscfgFsWtpImageUpgradeEntry));
            MEMSET (&WsscfgIsSetFsWtpImageUpgradeEntry, 0,
                    sizeof (tWsscfgIsSetFsWtpImageUpgradeEntry));
            UINT4               Ipaddress = 0;
            UINT4              *pIpaddress = NULL;
            Ipaddress = sizeof (UINT4);
            pIpaddress = &Ipaddress;
            UINT4               u4FileLen = 0;
            UINT4              *pu4FileLen = NULL;
            pu4FileLen = &u4FileLen;
            /*  UINT1 *pu1DstImageName; */
            UINT1              *pu1SrcImageName;
            UINT1               au1WtpName[256];
            UINT4               u4WtpNameLen = 0;
            UINT1               au1LocalDstFile[256];
            UINT1               u1DestLen = 0;
            FILE               *fp = NULL;
            /*this au1temp array is added for host name support,
             * here this array is dummy, to provide host name support
             * this array can be used*/
            UINT1               au1temp[255];

            MEMSET (&au1WtpName, 0, sizeof (UINT1));
            MEMSET (au1temp, 0, 255);
            MEMSET (&au1LocalDstFile, 0, sizeof (UINT1));
            if (CliGetTftpParams
                ((INT1 *) args[8], (INT1 *) au1FileName,
                 &IpAddress, (UINT1 *) au1temp) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid TFTP Parameters \r\n");
                break;
            }

            u4FileLen = STRLEN (au1FileName);

            u1DestLen = (STRLEN (au1FileName) + 6);
            SNPRINTF ((CHR1 *) au1LocalDstFile, u1DestLen, "%s%s",
                      IMAGE_DATA_FLASH_CONF_LOC, au1FileName);

            fp = fopen ((const char *) au1LocalDstFile, "rb");
            if (NULL == fp)
            {
                pu1SrcImageName = au1FileName;
                if ((tftpcRecvFile (IpAddress, pu1SrcImageName,
                                    au1LocalDstFile)) != TFTPC_OK)
                {
                    CliPrintf (CliHandle,
                               "\r%% Unable to copy remote file to flash\r\n");
                    return (CLI_FAILURE);
                }
            }
            else
            {
                fclose (fp);
            }

            if (*args[2] == WSSCFG_VERSION_AP_NAME_NONE)
            {
                i4RetStatus = CLI_SUCCESS;
                break;
            }

            if (args[3] != NULL)
            {
                WSSCFG_FILL_FSWTPIMAGEUPGRADETABLE_ARGS ((&WsscfgSetFsWtpImageUpgradeEntry), (&WsscfgIsSetFsWtpImageUpgradeEntry), args[0], args[1], args[2], args[3], args[4], au1FileName, pu4FileLen, args[7], &IpAddress.au1Addr, pIpaddress, args[10], args[11], args[12]);

                i4RetStatus =
                    WsscfgCliSetFsWtpImageUpgradeTable (CliHandle,
                                                        (&WsscfgSetFsWtpImageUpgradeEntry),
                                                        (&WsscfgIsSetFsWtpImageUpgradeEntry));
            }
            else if (*args[2] == WSSCFG_VERSION_AP_NAME_ALL)
            {
                ProfileName.pu1_OctetList = au1WtpName;
                if (nmhGetFirstIndexCapwapBaseWtpProfileTable
                    (&u4WtpProfileId) != SNMP_SUCCESS)
                {
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }

                do
                {
                    if (nmhGetCapwapBaseWtpProfileName (u4WtpProfileId,
                                                        &ProfileName) !=
                        OSIX_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n Profile Name Retrieval Failed for all profile Id %d\r\n",
                                   u4WtpProfileId);
                    }
                    u4WtpNameLen = STRLEN (au1WtpName);
                    WSSCFG_FILL_FSWTPIMAGEUPGRADETABLE_ARGS ((&WsscfgSetFsWtpImageUpgradeEntry), (&WsscfgIsSetFsWtpImageUpgradeEntry), args[0], args[1], args[2], au1WtpName, &u4WtpNameLen, au1FileName, pu4FileLen, args[7], &IpAddress.au1Addr, pIpaddress, args[10], args[11], args[12]);

                    i4RetStatus =
                        WsscfgCliSetFsWtpImageUpgradeTable (CliHandle,
                                                            (&WsscfgSetFsWtpImageUpgradeEntry),
                                                            (&WsscfgIsSetFsWtpImageUpgradeEntry));

                    currentProfileId = u4WtpProfileId;
                }
                while (nmhGetNextIndexCapwapBaseWtpProfileTable
                       (currentProfileId, &u4WtpProfileId));
            }

            break;
        case CLI_WSSCFG_FSDOT11NCONFIGTABLE:
            MEMSET (&WsscfgSetFsDot11nConfigEntry, 0,
                    sizeof (tWsscfgFsDot11nConfigEntry));
            MEMSET (&WsscfgIsSetFsDot11nConfigEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11nConfigEntry));

            /* Check the radio type and see that nSupport is enabled */
            if (*(args[4]) == CLI_RADIO_TYPEA)
            {
                INT4                i4FsDot11anSupport = -1;
                if (nmhGetFsDot11anSupport (&i4FsDot11anSupport) ==
                    SNMP_SUCCESS)
                {
                    if (i4FsDot11anSupport != CLI_NSUPPORT_ENABLE)
                    {
                        /*Dot11anSupport not enabled */
                        CliPrintf (CliHandle,
                                   "\r\n802.11 anSupport not enabled, Could not proceed\r\n");
                        break;
                    }
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r\nCould not retrieve 802.11 anSupport\r\n");
                    break;
                }
            }
            else if (*(args[4]) == CLI_RADIO_TYPEB)
            {
                INT4                i4FsDot11bnSupport = -1;
                if (nmhGetFsDot11bnSupport (&i4FsDot11bnSupport) ==
                    SNMP_SUCCESS)
                {
                    if (i4FsDot11bnSupport != CLI_NSUPPORT_ENABLE)
                    {
                        /*Dot11bnSupport not enabled */
                        CliPrintf (CliHandle,
                                   "\r\n802.11 bnSupport not enabled, Could not proceed\r\n");
                        break;
                    }
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r\nCould not retrieve 802.11 bnSupport\r\n");
                    break;
                }
            }

            if ((args[5] == NULL) && (*(args[6]) == CLI_WTP_RADIO_ALL))
            {
                for (u4WtpProfileId = 1; u4WtpProfileId <= CLI_MAX_AP;
                     u4WtpProfileId++)
                {
                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Ap (%d) : Model No not successfully obtained, Could not proceed for this AP\r\n",
                                   u4WtpProfileId);
                        continue;
                    }

                    if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nNo.of Radios present not successfully obtained, Operation Failed\r\n");
                        break;
                    }

                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {
                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nIn Ap (%d) , For Radio %d : Virtual Radio If Index not successfully obtained,Couldnt proceed,operation failed for the particular Radio\r\n",
                                       u4WtpProfileId, u1RadioIndex);
                            continue;
                        }

                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (u4RadioIfType == *(args[4]))
                        {
                            WSSCFG_FILL_FSDOT11NCONFIGTABLE_ARGS ((&WsscfgSetFsDot11nConfigEntry), (&WsscfgIsSetFsDot11nConfigEntry), args[0], args[1], args[2], &i4RadioIfIndex);
                            if (WsscfgCliSetFsDot11nConfigTable
                                (CliHandle,
                                 (&WsscfgSetFsDot11nConfigEntry),
                                 (&WsscfgIsSetFsDot11nConfigEntry)) !=
                                OSIX_SUCCESS)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nFor Radio %d :  DB Updation failed \r\n",
                                           u1RadioIndex);
                                continue;
                            }
                        }
                    }
                }
            }
            else
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    (au1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n WTP Profile not found. Binding failed\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if ((*(args[6]) == CLI_WTP_RADIO_ALL))
                {
                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Ap (%d) : Model No not successfully obtained, Could not proceed for this AP\r\n",
                                   u4WtpProfileId);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nNo.of Radios present not successfully obtained, Operation Failed\r\n");
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {
                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nIn Ap (%d) , For Radio %d : Virtual Radio If Index not successfully obtained,Couldnt proceed,operation failed for the particular Radio\r\n",
                                       u2WtpProfileId, u1RadioIndex);
                            continue;
                        }
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }
                        if (u4RadioIfType == *(args[4]))
                        {
                            WSSCFG_FILL_FSDOT11NCONFIGTABLE_ARGS ((&WsscfgSetFsDot11nConfigEntry), (&WsscfgIsSetFsDot11nConfigEntry), args[0], args[1], args[2], &i4RadioIfIndex);
                            if (WsscfgCliSetFsDot11nConfigTable
                                (CliHandle,
                                 (&WsscfgSetFsDot11nConfigEntry),
                                 (&WsscfgIsSetFsDot11nConfigEntry)) !=
                                OSIX_SUCCESS)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nFor Radio %d :  DB Updation failed \r\n",
                                           u1RadioIndex);
                                continue;
                            }
                        }
                    }
                    i4RetStatus = OSIX_SUCCESS;
                }
                else
                {
                    u1RadioId = CLI_PTR_TO_U4 (*(args[6]));
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u1RadioId,
                         &i4RadioIfIndex) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nIn Ap (%d) , For Radio %d : Virtual Radio If Index not successfully obtained,Couldnt proceed,operation failed for the particular Radio\r\n",
                                   u2WtpProfileId, u1RadioIndex);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    if (nmhGetFsDot11RadioType
                        (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed \r\n",
                                   u1RadioIndex);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    if (u4RadioIfType != *(args[4]))
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d :  Radio Type MisMatch, Couldnt proceed \r\n",
                                   u1RadioIndex);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    WSSCFG_FILL_FSDOT11NCONFIGTABLE_ARGS ((&WsscfgSetFsDot11nConfigEntry), (&WsscfgIsSetFsDot11nConfigEntry), args[0], args[1], args[2], &i4RadioIfIndex);
                    i4RetStatus =
                        WsscfgCliSetFsDot11nConfigTable (CliHandle,
                                                         (&WsscfgSetFsDot11nConfigEntry),
                                                         (&WsscfgIsSetFsDot11nConfigEntry));

                    if (i4RetStatus != OSIX_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d :  DB Updation failed \r\n",
                                   u1RadioIndex);
                    }
                }
            }
            break;

        case CLI_WSSCFG_FSDOT11NMCSDATARATETABLE:
            MEMSET (&WsscfgSetFsDot11nMCSDataRateEntry, 0,
                    sizeof (tWsscfgFsDot11nMCSDataRateEntry));
            MEMSET (&WsscfgIsSetFsDot11nMCSDataRateEntry, 0,
                    sizeof (tWsscfgIsSetFsDot11nMCSDataRateEntry));

            /* Check the radio type and confirm that the nSupport is enabled */
            if (*(args[3]) == CLI_RADIO_TYPEA)
            {
                INT4                i4FsDot11anSupport = -1;
                if (nmhGetFsDot11anSupport (&i4FsDot11anSupport) ==
                    SNMP_SUCCESS)
                {
                    if (i4FsDot11anSupport != CLI_NSUPPORT_ENABLE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n802.11 anSupport not enabled, Could not proceed\r\n");
                        break;
                    }
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r\nCould not retrieve 802.11 anSupport\r\n");
                    break;
                }
            }
            else if (*(args[3]) == CLI_RADIO_TYPEB)
            {
                INT4                i4FsDot11bnSupport = -1;
                if (nmhGetFsDot11bnSupport (&i4FsDot11bnSupport) ==
                    SNMP_SUCCESS)
                {
                    if (i4FsDot11bnSupport != CLI_NSUPPORT_ENABLE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n802.11 bnSupport not enabled, Could not proceed\r\n");
                        break;
                    }
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r\nCould not retrieve 802.11 bnSupport\r\n");
                    break;
                }
            }

            if ((args[4] == NULL) && (*(args[5]) == CLI_WTP_RADIO_ALL))
            {
                for (u4WtpProfileId = 1; u4WtpProfileId <= CLI_MAX_AP;
                     u4WtpProfileId++)
                {
                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Ap (%d) : Model No not successfully obtained, Could not proceed for this AP\r\n",
                                   u4WtpProfileId);
                        continue;
                    }

                    if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nNo.of Radios present not successfully obtained, Operation Failed\r\n");
                        break;
                    }

                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {

                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nIn Ap (%d) , For Radio %d : Virtual Radio If Index not successfully obtained,Couldnt proceed,operation failed for the particular Radio\r\n",
                                       u4WtpProfileId, u1RadioIndex);
                            continue;
                        }

                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }

                        if (u4RadioIfType == *(args[3]))
                        {
                            WSSCFG_FILL_FSDOT11NMCSDATARATETABLE_ARGS ((&WsscfgSetFsDot11nMCSDataRateEntry), (&WsscfgIsSetFsDot11nMCSDataRateEntry), args[0], args[1], &i4RadioIfIndex);

                            if (WsscfgCliSetFsDot11nMCSDataRateTable
                                (CliHandle,
                                 (&WsscfgSetFsDot11nMCSDataRateEntry),
                                 (&WsscfgIsSetFsDot11nMCSDataRateEntry))
                                != OSIX_SUCCESS)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nFor Radio %d :  DB Updation failed \r\n",
                                           u1RadioIndex);
                                continue;
                            }
                        }
                    }
                }
            }
            else
            {
                if (CapwapGetWtpProfileIdFromProfileName
                    (au1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n WTP Profile not found. Binding failed\r\n");
                    CLI_FATAL_ERROR (CliHandle);
                    i4RetStatus = OSIX_FAILURE;
                    break;
                }
                if ((*(args[5]) == CLI_WTP_RADIO_ALL))
                {
                    if (nmhGetCapwapBaseWtpProfileWtpModelNumber
                        (u4WtpProfileId, &ModelName) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Ap (%d) : Model No not successfully obtained, Could not proceed for this AP\r\n",
                                   u4WtpProfileId);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                        OSIX_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nNo.of Radios present not successfully obtained, Operation Failed\r\n");
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    for (u1RadioIndex = 1; u1RadioIndex <= u4NoOfRadio;
                         u1RadioIndex++)
                    {
                        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                            (u4WtpProfileId, u1RadioIndex,
                             &i4RadioIfIndex) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nIn Ap (%d) , For Radio %d : Virtual Radio If Index not successfully obtained,Couldnt proceed,operation failed for the particular Radio\r\n",
                                       u2WtpProfileId, u1RadioIndex);
                            continue;
                        }
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                                       u1RadioIndex);
                            continue;
                        }
                        if (u4RadioIfType == *(args[3]))
                        {
                            WSSCFG_FILL_FSDOT11NMCSDATARATETABLE_ARGS ((&WsscfgSetFsDot11nMCSDataRateEntry), (&WsscfgIsSetFsDot11nMCSDataRateEntry), args[0], args[1], &i4RadioIfIndex);

                            if (WsscfgCliSetFsDot11nMCSDataRateTable
                                (CliHandle,
                                 (&WsscfgSetFsDot11nMCSDataRateEntry),
                                 (&WsscfgIsSetFsDot11nMCSDataRateEntry))
                                != OSIX_SUCCESS)
                            {
                                CliPrintf (CliHandle,
                                           "\r\nFor Radio %d :  DB Updation failed \r\n",
                                           u1RadioIndex);
                                continue;
                            }
                        }
                    }
                    i4RetStatus = OSIX_SUCCESS;
                }
                else
                {
                    u1RadioId = CLI_PTR_TO_U4 (*(args[5]));
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u1RadioId,
                         &i4RadioIfIndex) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nIn Ap (%d) , For Radio %d : Virtual Radio If Index not successfully obtained,Couldnt proceed,operation failed for the particular Radio\r\n",
                                   u2WtpProfileId, u1RadioIndex);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    if (nmhGetFsDot11RadioType
                        (i4RadioIfIndex, &u4RadioIfType) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d :  RadioIf Type not successfully obtained,Couldnt proceed \r\n",
                                   u1RadioIndex);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    if (u4RadioIfType != *(args[3]))
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d :  Radio Type MisMatch, Couldnt proceed \r\n",
                                   u1RadioIndex);
                        i4RetStatus = OSIX_FAILURE;
                        break;
                    }
                    WSSCFG_FILL_FSDOT11NMCSDATARATETABLE_ARGS ((&WsscfgSetFsDot11nMCSDataRateEntry), (&WsscfgIsSetFsDot11nMCSDataRateEntry), args[0], args[1], &i4RadioIfIndex);

                    i4RetStatus =
                        WsscfgCliSetFsDot11nMCSDataRateTable (CliHandle,
                                                              (&WsscfgSetFsDot11nMCSDataRateEntry),
                                                              (&WsscfgIsSetFsDot11nMCSDataRateEntry));
                    if (i4RetStatus != OSIX_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nFor Radio %d :  DB Updation failed \r\n",
                                   u1RadioIndex);
                    }
                }
            }
            break;

        case CLI_WSSCFG_FSDOT11ANETWORKENABLE:

            i4RetStatus =
                WsscfgCliSetFsDot11aNetworkEnable (CliHandle, args[0]);
            break;

        case CLI_WSSCFG_FSDOT11BNETWORKENABLE:

            i4RetStatus =
                WsscfgCliSetFsDot11bNetworkEnable (CliHandle, args[0]);
            break;

        case CLI_WSSCFG_FSDOT11GSUPPORT:

            i4RetStatus = WsscfgCliSetFsDot11gSupport (CliHandle, args[0]);
            break;

        case CLI_WSSCFG_FSDOT11ANSUPPORT:

            i4RetStatus = WsscfgCliSetFsDot11anSupport (CliHandle, args[0]);
            break;

        case CLI_WSSCFG_FSDOT11BNSUPPORT:

            i4RetStatus = WsscfgCliSetFsDot11bnSupport (CliHandle, args[0]);
            break;

        case CLI_WSSCFG_FSDOT11MANAGMENTSSID:

            i4RetStatus = WsscfgCliSetFsDot11ManagmentSSID (CliHandle, args[0]);
            break;

        case CLI_WSSCFG_FSDOT11COUNTRYSTRING:

            i4RetStatus = WsscfgCliSetFsDot11CountryString (CliHandle, args[0]);
            break;

        case CLI_WSSCFG_FSSECURITYWEBAUTHWEBTITLE:

            i4RetStatus =
                WsscfgCliSetFsSecurityWebAuthWebTitle (CliHandle,
                                                       (UINT1 *) args[0]);
            break;
        case CLI_WSSCFG_CLEAR_FSSECURITYWEBAUTHWEBTITLE:
            i4RetStatus = WsscfgCliClearFsSecurityWebAuthWebTitle (CliHandle);
            break;
        case CLI_WSSCFG_FSSECURITYWEBAUTHWEBMESSAGE:

            i4RetStatus =
                WsscfgCliSetFsSecurityWebAuthWebMessage (CliHandle,
                                                         (UINT1 *) args[0]);
            break;

        case CLI_WSSCFG_CLEAR_FSSECURITYWEBAUTHWEBMESSAGE:
            i4RetStatus = WsscfgCliClearFsSecurityWebAuthWebMessage (CliHandle);
            break;

        case CLI_WSSCFG_FSSECURITYWEBAUTHWEBLOGOFILENAME:
            if ((*args[0] == WSS_AUTH_STATUS_ENABLE) && (args[1] == NULL))
            {
                CliPrintf (CliHandle, "\r\nInvalid input\r\n");
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus = OSIX_FAILURE;
            }
            else
            {
                i4RetStatus =
                    WsscfgCliSetFsSecurityWebAuthWebLogoFileName
                    (CliHandle, args[0], (UINT1 *) args[1]);
            }
            break;

        case CLI_WSSCFG_FSSECURITYWEBAUTHWEBSUCCMESSAGE:

            i4RetStatus =
                WsscfgCliSetFsSecurityWebAuthWebSuccMessage (CliHandle,
                                                             (UINT1 *) args[0]);
            break;
        case CLI_WSSCFG_CLEAR_FSSECURITYWEBAUTHWEBSUCCMESSAGE:
            i4RetStatus =
                WsscfgCliClearFsSecurityWebAuthWebSuccMessage (CliHandle);
            break;

        case CLI_WSSCFG_FSSECURITYWEBAUTHWEBFAILMESSAGE:

            i4RetStatus =
                WsscfgCliSetFsSecurityWebAuthWebFailMessage (CliHandle,
                                                             (UINT1 *) args[0]);
            break;

        case CLI_WSSCFG_CLEAR_FSSECURITYWEBAUTHWEBFAILMESSAGE:
            i4RetStatus =
                WsscfgCliClearFsSecurityWebAuthWebFailMessage (CliHandle);
            break;
        case CLI_WSSCFG_FSSECURITYWEBAUTHWEBBUTTONTEXT:

            i4RetStatus =
                WsscfgCliSetFsSecurityWebAuthWebButtonText (CliHandle,
                                                            (UINT1 *) args[0]);
            break;

        case CLI_WSSCFG_CLEAR_FSSECURITYWEBAUTHWEBBUTTONTEXT:
            i4RetStatus =
                WsscfgCliClearFsSecurityWebAuthWebButtonText (CliHandle);
            break;
        case CLI_WSSCFG_FSSECURITYWEBAUTHWEBLOADBALINFO:

            i4RetStatus =
                WsscfgCliSetFsSecurityWebAuthWebLoadBalInfo (CliHandle,
                                                             (UINT1 *) args[0]);
            break;
        case CLI_WSSCFG_CLEAR_FSSECURITYWEBAUTHWEBLOADBALINFO:
            i4RetStatus =
                WsscfgCliClearFsSecurityWebAuthWebLoadBalInfo (CliHandle);
            break;
        case CLI_WSSCFG_FSSECURITYWEBAUTHDISPLAYLANG:

            i4RetStatus =
                WsscfgCliSetFsSecurityWebAuthDisplayLang (CliHandle, args[0]);
            break;

        case CLI_WSSCFG_FSSECURITYWEBAUTHCOLOR:

            i4RetStatus =
                WsscfgCliSetFsSecurityWebAuthColor (CliHandle, args[0]);
            break;

        case CLI_WSSCFG_CREATE_WLAN_CAPABILITY:
            i4RetStatus =
                WsscfgWlanCapabilityCreate (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_WSSCFG_DELETE_WLAN_CAPABILITY:
            i4RetStatus =
                WsscfgWlanCapabilityDelete (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_WSSCFG_CREATE_WLAN_QOS:
            i4RetStatus = WsscfgWlanQosCreate (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_WSSCFG_DELETE_WLAN_QOS:
            i4RetStatus = WsscfgWlanQosDelete (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_WSSCFG_CREATE_WLAN_AUTH:
            i4RetStatus = WsscfgWlanAuthCreate (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_WSSCFG_DELETE_WLAN_AUTH:
            i4RetStatus = WsscfgWlanAuthDelete (CliHandle, (UINT1 *) args[0]);
            break;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_WSSCFG_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", WsscfgCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);

    WSSCFG_UNLOCK;

    UNUSED_PARAM (u4IfIndex);
    return i4RetStatus;

}

/****************************************************************************
* Function    :  WsscfgCliSetDot11StationConfigTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11StationConfigEntry
*            pWsscfgIsSetDot11StationConfigEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11StationConfigTable (tCliHandle CliHandle,
                                     tWsscfgDot11StationConfigEntry *
                                     pWsscfgSetDot11StationConfigEntry,
                                     tWsscfgIsSetDot11StationConfigEntry *
                                     pWsscfgIsSetDot11StationConfigEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11StationConfigTable
        (&u4ErrorCode, pWsscfgSetDot11StationConfigEntry,
         pWsscfgIsSetDot11StationConfigEntry) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Input Validation failed \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11StationConfigTable
        (pWsscfgSetDot11StationConfigEntry,
         pWsscfgIsSetDot11StationConfigEntry) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n DB Updation failed \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11AuthenticationAlgorithmsTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11AuthenticationAlgorithmsEntry
*            pWsscfgIsSetDot11AuthenticationAlgorithmsEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11AuthenticationAlgorithmsTable (tCliHandle CliHandle,
                                                tWsscfgDot11AuthenticationAlgorithmsEntry
                                                *
                                                pWsscfgSetDot11AuthenticationAlgorithmsEntry,
                                                tWsscfgIsSetDot11AuthenticationAlgorithmsEntry
                                                *
                                                pWsscfgIsSetDot11AuthenticationAlgorithmsEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11AuthenticationAlgorithmsTable
        (&u4ErrorCode, pWsscfgSetDot11AuthenticationAlgorithmsEntry,
         pWsscfgIsSetDot11AuthenticationAlgorithmsEntry) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\n Input Validation failed\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11AuthenticationAlgorithmsTable
        (pWsscfgSetDot11AuthenticationAlgorithmsEntry,
         pWsscfgIsSetDot11AuthenticationAlgorithmsEntry) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\nDB Updation failed\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11WEPDefaultKeysTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11WEPDefaultKeysEntry
*            pWsscfgIsSetDot11WEPDefaultKeysEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11WEPDefaultKeysTable (tCliHandle CliHandle,
                                      tWsscfgDot11WEPDefaultKeysEntry *
                                      pWsscfgSetDot11WEPDefaultKeysEntry,
                                      tWsscfgIsSetDot11WEPDefaultKeysEntry *
                                      pWsscfgIsSetDot11WEPDefaultKeysEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11WEPDefaultKeysTable
        (&u4ErrorCode, pWsscfgSetDot11WEPDefaultKeysEntry,
         pWsscfgIsSetDot11WEPDefaultKeysEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11WEPDefaultKeysTable
        (pWsscfgSetDot11WEPDefaultKeysEntry,
         pWsscfgIsSetDot11WEPDefaultKeysEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11WEPKeyMappingsTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11WEPKeyMappingsEntry
*            pWsscfgIsSetDot11WEPKeyMappingsEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11WEPKeyMappingsTable (tCliHandle CliHandle,
                                      tWsscfgDot11WEPKeyMappingsEntry *
                                      pWsscfgSetDot11WEPKeyMappingsEntry,
                                      tWsscfgIsSetDot11WEPKeyMappingsEntry *
                                      pWsscfgIsSetDot11WEPKeyMappingsEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11WEPKeyMappingsTable
        (&u4ErrorCode, pWsscfgSetDot11WEPKeyMappingsEntry,
         pWsscfgIsSetDot11WEPKeyMappingsEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11WEPKeyMappingsTable
        (pWsscfgSetDot11WEPKeyMappingsEntry,
         pWsscfgIsSetDot11WEPKeyMappingsEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11PrivacyTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11PrivacyEntry
*            pWsscfgIsSetDot11PrivacyEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11PrivacyTable (tCliHandle CliHandle,
                               tWsscfgDot11PrivacyEntry *
                               pWsscfgSetDot11PrivacyEntry,
                               tWsscfgIsSetDot11PrivacyEntry *
                               pWsscfgIsSetDot11PrivacyEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11PrivacyTable
        (&u4ErrorCode, pWsscfgSetDot11PrivacyEntry,
         pWsscfgIsSetDot11PrivacyEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11PrivacyTable
        (pWsscfgSetDot11PrivacyEntry,
         pWsscfgIsSetDot11PrivacyEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11MultiDomainCapabilityTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11MultiDomainCapabilityEntry
*            pWsscfgIsSetDot11MultiDomainCapabilityEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11MultiDomainCapabilityTable (tCliHandle CliHandle,
                                             tWsscfgDot11MultiDomainCapabilityEntry
                                             *
                                             pWsscfgSetDot11MultiDomainCapabilityEntry,
                                             tWsscfgIsSetDot11MultiDomainCapabilityEntry
                                             *
                                             pWsscfgIsSetDot11MultiDomainCapabilityEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11MultiDomainCapabilityTable
        (&u4ErrorCode, pWsscfgSetDot11MultiDomainCapabilityEntry,
         pWsscfgIsSetDot11MultiDomainCapabilityEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11MultiDomainCapabilityTable
        (pWsscfgSetDot11MultiDomainCapabilityEntry,
         pWsscfgIsSetDot11MultiDomainCapabilityEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11SpectrumManagementTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11SpectrumManagementEntry
*            pWsscfgIsSetDot11SpectrumManagementEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11SpectrumManagementTable (tCliHandle CliHandle,
                                          tWsscfgDot11SpectrumManagementEntry *
                                          pWsscfgSetDot11SpectrumManagementEntry,
                                          tWsscfgIsSetDot11SpectrumManagementEntry
                                          *
                                          pWsscfgIsSetDot11SpectrumManagementEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11SpectrumManagementTable
        (&u4ErrorCode, pWsscfgSetDot11SpectrumManagementEntry,
         pWsscfgIsSetDot11SpectrumManagementEntry) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Invalid Inputs\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11SpectrumManagementTable
        (pWsscfgSetDot11SpectrumManagementEntry,
         pWsscfgIsSetDot11SpectrumManagementEntry) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Database Updation failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11RegulatoryClassesTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11RegulatoryClassesEntry
*            pWsscfgIsSetDot11RegulatoryClassesEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11RegulatoryClassesTable (tCliHandle CliHandle,
                                         tWsscfgDot11RegulatoryClassesEntry *
                                         pWsscfgSetDot11RegulatoryClassesEntry,
                                         tWsscfgIsSetDot11RegulatoryClassesEntry
                                         *
                                         pWsscfgIsSetDot11RegulatoryClassesEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11RegulatoryClassesTable
        (&u4ErrorCode, pWsscfgSetDot11RegulatoryClassesEntry,
         pWsscfgIsSetDot11RegulatoryClassesEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11RegulatoryClassesTable
        (pWsscfgSetDot11RegulatoryClassesEntry,
         pWsscfgIsSetDot11RegulatoryClassesEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetCapwapDot11WlanTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetCapwapDot11WlanEntry
*            pWsscfgIsSetCapwapDot11WlanEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetCapwapDot11WlanTable (tCliHandle CliHandle,
                                  tWsscfgCapwapDot11WlanEntry *
                                  pWsscfgSetCapwapDot11WlanEntry,
                                  tWsscfgIsSetCapwapDot11WlanEntry *
                                  pWsscfgIsSetCapwapDot11WlanEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllCapwapDot11WlanTable
        (&u4ErrorCode, pWsscfgSetCapwapDot11WlanEntry,
         pWsscfgIsSetCapwapDot11WlanEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Invalid Configuration received\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllCapwapDot11WlanTable
        (pWsscfgSetCapwapDot11WlanEntry,
         pWsscfgIsSetCapwapDot11WlanEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n DB Updation failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetCapwapDot11WlanBindTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetCapwapDot11WlanBindEntry
*            pWsscfgIsSetCapwapDot11WlanBindEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetCapwapDot11WlanBindTable (tCliHandle CliHandle,
                                      tWsscfgCapwapDot11WlanBindEntry *
                                      pWsscfgSetCapwapDot11WlanBindEntry,
                                      tWsscfgIsSetCapwapDot11WlanBindEntry *
                                      pWsscfgIsSetCapwapDot11WlanBindEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllCapwapDot11WlanBindTable
        (&u4ErrorCode, pWsscfgSetCapwapDot11WlanBindEntry,
         pWsscfgIsSetCapwapDot11WlanBindEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Validation failed for Binding table\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllCapwapDot11WlanBindTable
        (pWsscfgSetCapwapDot11WlanBindEntry,
         pWsscfgIsSetCapwapDot11WlanBindEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Set failed for Binding table\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11OperationTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11OperationEntry
*            pWsscfgIsSetDot11OperationEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11OperationTable (tCliHandle CliHandle,
                                 tWsscfgDot11OperationEntry *
                                 pWsscfgSetDot11OperationEntry,
                                 tWsscfgIsSetDot11OperationEntry *
                                 pWsscfgIsSetDot11OperationEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11OperationTable
        (&u4ErrorCode, pWsscfgSetDot11OperationEntry,
         pWsscfgIsSetDot11OperationEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11OperationTable
        (pWsscfgSetDot11OperationEntry,
         pWsscfgIsSetDot11OperationEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11GroupAddressesTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11GroupAddressesEntry
*            pWsscfgIsSetDot11GroupAddressesEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11GroupAddressesTable (tCliHandle CliHandle,
                                      tWsscfgDot11GroupAddressesEntry *
                                      pWsscfgSetDot11GroupAddressesEntry,
                                      tWsscfgIsSetDot11GroupAddressesEntry *
                                      pWsscfgIsSetDot11GroupAddressesEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11GroupAddressesTable
        (&u4ErrorCode, pWsscfgSetDot11GroupAddressesEntry,
         pWsscfgIsSetDot11GroupAddressesEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11GroupAddressesTable
        (pWsscfgSetDot11GroupAddressesEntry,
         pWsscfgIsSetDot11GroupAddressesEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11EDCATable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11EDCAEntry
*            pWsscfgIsSetDot11EDCAEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11EDCATable (tCliHandle CliHandle,
                            tWsscfgDot11EDCAEntry * pWsscfgSetDot11EDCAEntry,
                            tWsscfgIsSetDot11EDCAEntry *
                            pWsscfgIsSetDot11EDCAEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11EDCATable
        (&u4ErrorCode, pWsscfgSetDot11EDCAEntry,
         pWsscfgIsSetDot11EDCAEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11EDCATable
        (pWsscfgSetDot11EDCAEntry, pWsscfgIsSetDot11EDCAEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11QAPEDCATable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11QAPEDCAEntry
*            pWsscfgIsSetDot11QAPEDCAEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11QAPEDCATable (tCliHandle CliHandle,
                               tWsscfgDot11QAPEDCAEntry *
                               pWsscfgSetDot11QAPEDCAEntry,
                               tWsscfgIsSetDot11QAPEDCAEntry *
                               pWsscfgIsSetDot11QAPEDCAEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11QAPEDCATable
        (&u4ErrorCode, pWsscfgSetDot11QAPEDCAEntry,
         pWsscfgIsSetDot11QAPEDCAEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11QAPEDCATable
        (pWsscfgSetDot11QAPEDCAEntry,
         pWsscfgIsSetDot11QAPEDCAEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11PhyOperationTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11PhyOperationEntry
*            pWsscfgIsSetDot11PhyOperationEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11PhyOperationTable (tCliHandle CliHandle,
                                    tWsscfgDot11PhyOperationEntry *
                                    pWsscfgSetDot11PhyOperationEntry,
                                    tWsscfgIsSetDot11PhyOperationEntry *
                                    pWsscfgIsSetDot11PhyOperationEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11PhyOperationTable
        (&u4ErrorCode, pWsscfgSetDot11PhyOperationEntry,
         pWsscfgIsSetDot11PhyOperationEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11PhyOperationTable
        (pWsscfgSetDot11PhyOperationEntry,
         pWsscfgIsSetDot11PhyOperationEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11PhyAntennaTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11PhyAntennaEntry
*            pWsscfgIsSetDot11PhyAntennaEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11PhyAntennaTable (tCliHandle CliHandle,
                                  tWsscfgDot11PhyAntennaEntry *
                                  pWsscfgSetDot11PhyAntennaEntry,
                                  tWsscfgIsSetDot11PhyAntennaEntry *
                                  pWsscfgIsSetDot11PhyAntennaEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11PhyAntennaTable
        (&u4ErrorCode, pWsscfgSetDot11PhyAntennaEntry,
         pWsscfgIsSetDot11PhyAntennaEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11PhyAntennaTable
        (pWsscfgSetDot11PhyAntennaEntry,
         pWsscfgIsSetDot11PhyAntennaEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11PhyTxPowerTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11PhyTxPowerEntry
*            pWsscfgIsSetDot11PhyTxPowerEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11PhyTxPowerTable (tCliHandle CliHandle,
                                  tWsscfgDot11PhyTxPowerEntry *
                                  pWsscfgSetDot11PhyTxPowerEntry,
                                  tWsscfgIsSetDot11PhyTxPowerEntry *
                                  pWsscfgIsSetDot11PhyTxPowerEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11PhyTxPowerTable
        (&u4ErrorCode, pWsscfgSetDot11PhyTxPowerEntry,
         pWsscfgIsSetDot11PhyTxPowerEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11PhyTxPowerTable
        (pWsscfgSetDot11PhyTxPowerEntry,
         pWsscfgIsSetDot11PhyTxPowerEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11PhyFHSSTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11PhyFHSSEntry
*            pWsscfgIsSetDot11PhyFHSSEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11PhyFHSSTable (tCliHandle CliHandle,
                               tWsscfgDot11PhyFHSSEntry *
                               pWsscfgSetDot11PhyFHSSEntry,
                               tWsscfgIsSetDot11PhyFHSSEntry *
                               pWsscfgIsSetDot11PhyFHSSEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11PhyFHSSTable
        (&u4ErrorCode, pWsscfgSetDot11PhyFHSSEntry,
         pWsscfgIsSetDot11PhyFHSSEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11PhyFHSSTable
        (pWsscfgSetDot11PhyFHSSEntry,
         pWsscfgIsSetDot11PhyFHSSEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11PhyDSSSTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11PhyDSSSEntry
*            pWsscfgIsSetDot11PhyDSSSEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11PhyDSSSTable (tCliHandle CliHandle,
                               tWsscfgDot11PhyDSSSEntry *
                               pWsscfgSetDot11PhyDSSSEntry,
                               tWsscfgIsSetDot11PhyDSSSEntry *
                               pWsscfgIsSetDot11PhyDSSSEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11PhyDSSSTable
        (&u4ErrorCode, pWsscfgSetDot11PhyDSSSEntry,
         pWsscfgIsSetDot11PhyDSSSEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11PhyDSSSTable
        (pWsscfgSetDot11PhyDSSSEntry,
         pWsscfgIsSetDot11PhyDSSSEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11PhyIRTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11PhyIREntry
*            pWsscfgIsSetDot11PhyIREntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11PhyIRTable (tCliHandle CliHandle,
                             tWsscfgDot11PhyIREntry * pWsscfgSetDot11PhyIREntry,
                             tWsscfgIsSetDot11PhyIREntry *
                             pWsscfgIsSetDot11PhyIREntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11PhyIRTable
        (&u4ErrorCode, pWsscfgSetDot11PhyIREntry,
         pWsscfgIsSetDot11PhyIREntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11PhyIRTable
        (pWsscfgSetDot11PhyIREntry,
         pWsscfgIsSetDot11PhyIREntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11AntennasListTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11AntennasListEntry
*            pWsscfgIsSetDot11AntennasListEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11AntennasListTable (tCliHandle CliHandle,
                                    tWsscfgDot11AntennasListEntry *
                                    pWsscfgSetDot11AntennasListEntry,
                                    tWsscfgIsSetDot11AntennasListEntry *
                                    pWsscfgIsSetDot11AntennasListEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11AntennasListTable
        (&u4ErrorCode, pWsscfgSetDot11AntennasListEntry,
         pWsscfgIsSetDot11AntennasListEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11AntennasListTable
        (pWsscfgSetDot11AntennasListEntry,
         pWsscfgIsSetDot11AntennasListEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11PhyOFDMTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11PhyOFDMEntry
*            pWsscfgIsSetDot11PhyOFDMEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11PhyOFDMTable (tCliHandle CliHandle,
                               tWsscfgDot11PhyOFDMEntry *
                               pWsscfgSetDot11PhyOFDMEntry,
                               tWsscfgIsSetDot11PhyOFDMEntry *
                               pWsscfgIsSetDot11PhyOFDMEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11PhyOFDMTable
        (&u4ErrorCode, pWsscfgSetDot11PhyOFDMEntry,
         pWsscfgIsSetDot11PhyOFDMEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11PhyOFDMTable
        (pWsscfgSetDot11PhyOFDMEntry,
         pWsscfgIsSetDot11PhyOFDMEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11HoppingPatternTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11HoppingPatternEntry
*            pWsscfgIsSetDot11HoppingPatternEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11HoppingPatternTable (tCliHandle CliHandle,
                                      tWsscfgDot11HoppingPatternEntry *
                                      pWsscfgSetDot11HoppingPatternEntry,
                                      tWsscfgIsSetDot11HoppingPatternEntry *
                                      pWsscfgIsSetDot11HoppingPatternEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11HoppingPatternTable
        (&u4ErrorCode, pWsscfgSetDot11HoppingPatternEntry,
         pWsscfgIsSetDot11HoppingPatternEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11HoppingPatternTable
        (pWsscfgSetDot11HoppingPatternEntry,
         pWsscfgIsSetDot11HoppingPatternEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11PhyERPTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11PhyERPEntry
*            pWsscfgIsSetDot11PhyERPEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11PhyERPTable (tCliHandle CliHandle,
                              tWsscfgDot11PhyERPEntry *
                              pWsscfgSetDot11PhyERPEntry,
                              tWsscfgIsSetDot11PhyERPEntry *
                              pWsscfgIsSetDot11PhyERPEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11PhyERPTable
        (&u4ErrorCode, pWsscfgSetDot11PhyERPEntry,
         pWsscfgIsSetDot11PhyERPEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11PhyERPTable
        (pWsscfgSetDot11PhyERPEntry,
         pWsscfgIsSetDot11PhyERPEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11RSNAConfigTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11RSNAConfigEntry
*            pWsscfgIsSetDot11RSNAConfigEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11RSNAConfigTable (tCliHandle CliHandle,
                                  tWsscfgDot11RSNAConfigEntry *
                                  pWsscfgSetDot11RSNAConfigEntry,
                                  tWsscfgIsSetDot11RSNAConfigEntry *
                                  pWsscfgIsSetDot11RSNAConfigEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11RSNAConfigTable
        (&u4ErrorCode, pWsscfgSetDot11RSNAConfigEntry,
         pWsscfgIsSetDot11RSNAConfigEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11RSNAConfigTable
        (pWsscfgSetDot11RSNAConfigEntry,
         pWsscfgIsSetDot11RSNAConfigEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11RSNAConfigPairwiseCiphersTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry
*            pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11RSNAConfigPairwiseCiphersTable (tCliHandle CliHandle,
                                                 tWsscfgDot11RSNAConfigPairwiseCiphersEntry
                                                 *
                                                 pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry,
                                                 tWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry
                                                 *
                                                 pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11RSNAConfigPairwiseCiphersTable
        (&u4ErrorCode, pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry,
         pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11RSNAConfigPairwiseCiphersTable
        (pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry,
         pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetDot11RSNAConfigAuthenticationSuitesTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry
*            pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetDot11RSNAConfigAuthenticationSuitesTable (tCliHandle CliHandle,
                                                      tWsscfgDot11RSNAConfigAuthenticationSuitesEntry
                                                      *
                                                      pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry,
                                                      tWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry
                                                      *
                                                      pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllDot11RSNAConfigAuthenticationSuitesTable
        (&u4ErrorCode,
         pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry,
         pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllDot11RSNAConfigAuthenticationSuitesTable
        (pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry,
         pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11StationConfigTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11StationConfigEntry
*            pWsscfgIsSetFsDot11StationConfigEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11StationConfigTable (tCliHandle CliHandle,
                                       tWsscfgFsDot11StationConfigEntry *
                                       pWsscfgSetFsDot11StationConfigEntry,
                                       tWsscfgIsSetFsDot11StationConfigEntry *
                                       pWsscfgIsSetFsDot11StationConfigEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11StationConfigTable
        (&u4ErrorCode, pWsscfgSetFsDot11StationConfigEntry,
         pWsscfgIsSetFsDot11StationConfigEntry) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Input Validation failed \r\n");
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11StationConfigTable
        (pWsscfgSetFsDot11StationConfigEntry,
         pWsscfgIsSetFsDot11StationConfigEntry) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n DB Updation failed \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11CapabilityProfileTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11CapabilityProfileEntry
*            pWsscfgIsSetFsDot11CapabilityProfileEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11CapabilityProfileTable (tCliHandle CliHandle,
                                           tWsscfgFsDot11CapabilityProfileEntry
                                           *
                                           pWsscfgSetFsDot11CapabilityProfileEntry,
                                           tWsscfgIsSetFsDot11CapabilityProfileEntry
                                           *
                                           pWsscfgIsSetFsDot11CapabilityProfileEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11CapabilityProfileTable
        (&u4ErrorCode, pWsscfgSetFsDot11CapabilityProfileEntry,
         pWsscfgIsSetFsDot11CapabilityProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {

        CLI_FATAL_ERROR (CliHandle);
        CliPrintf (CliHandle, "\nInvalid Inputs\n");

        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11CapabilityProfileTable
        (pWsscfgSetFsDot11CapabilityProfileEntry,
         pWsscfgIsSetFsDot11CapabilityProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        CliPrintf (CliHandle, "\nCannot update database\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11AuthenticationProfileTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11AuthenticationProfileEntry
*            pWsscfgIsSetFsDot11AuthenticationProfileEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11AuthenticationProfileTable (tCliHandle CliHandle,
                                               tWsscfgFsDot11AuthenticationProfileEntry
                                               *
                                               pWsscfgSetFsDot11AuthenticationProfileEntry,
                                               tWsscfgIsSetFsDot11AuthenticationProfileEntry
                                               *
                                               pWsscfgIsSetFsDot11AuthenticationProfileEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11AuthenticationProfileTable
        (&u4ErrorCode, pWsscfgSetFsDot11AuthenticationProfileEntry,
         pWsscfgIsSetFsDot11AuthenticationProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        CliPrintf (CliHandle, "\nInvalid Inputs\n");
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11AuthenticationProfileTable
        (pWsscfgSetFsDot11AuthenticationProfileEntry,
         pWsscfgIsSetFsDot11AuthenticationProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        CliPrintf (CliHandle, "\nCannot update database\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthGuestInfoTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsSecurityWebAuthGuestInfoEntry
*            pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthGuestInfoTable (tCliHandle CliHandle,
                                             tWsscfgFsSecurityWebAuthGuestInfoEntry
                                             *
                                             pWsscfgSetFsSecurityWebAuthGuestInfoEntry,
                                             tWsscfgIsSetFsSecurityWebAuthGuestInfoEntry
                                             *
                                             pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsSecurityWebAuthGuestInfoTable
        (&u4ErrorCode, pWsscfgSetFsSecurityWebAuthGuestInfoEntry,
         pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsSecurityWebAuthGuestInfoTable
        (pWsscfgSetFsSecurityWebAuthGuestInfoEntry,
         pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsStationQosParamsTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsStationQosParamsEntry
*            pWsscfgIsSetFsStationQosParamsEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsStationQosParamsTable (tCliHandle CliHandle,
                                     tWsscfgFsStationQosParamsEntry *
                                     pWsscfgSetFsStationQosParamsEntry,
                                     tWsscfgIsSetFsStationQosParamsEntry *
                                     pWsscfgIsSetFsStationQosParamsEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsStationQosParamsTable
        (&u4ErrorCode, pWsscfgSetFsStationQosParamsEntry,
         pWsscfgIsSetFsStationQosParamsEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsStationQosParamsTable
        (pWsscfgSetFsStationQosParamsEntry,
         pWsscfgIsSetFsStationQosParamsEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11RadioConfigTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11RadioConfigEntry
*            pWsscfgIsSetFsDot11RadioConfigEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11RadioConfigTable (tCliHandle CliHandle,
                                     tWsscfgFsDot11RadioConfigEntry *
                                     pWsscfgSetFsDot11RadioConfigEntry,
                                     tWsscfgIsSetFsDot11RadioConfigEntry *
                                     pWsscfgIsSetFsDot11RadioConfigEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11RadioConfigTable
        (&u4ErrorCode, pWsscfgSetFsDot11RadioConfigEntry,
         pWsscfgIsSetFsDot11RadioConfigEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11RadioConfigTable
        (pWsscfgSetFsDot11RadioConfigEntry,
         pWsscfgIsSetFsDot11RadioConfigEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsVlanIsolationTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsVlanIsolationEntry
*            pWsscfgIsSetFsVlanIsolationEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsVlanIsolationTable (tCliHandle CliHandle,
                                  tWsscfgFsVlanIsolationEntry *
                                  pWsscfgSetFsVlanIsolationEntry,
                                  tWsscfgIsSetFsVlanIsolationEntry *
                                  pWsscfgIsSetFsVlanIsolationEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsVlanIsolationTable
        (&u4ErrorCode, pWsscfgSetFsVlanIsolationEntry,
         pWsscfgIsSetFsVlanIsolationEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (WsscfgSetAllFsVlanIsolationTable
        (pWsscfgSetFsVlanIsolationEntry,
         pWsscfgIsSetFsVlanIsolationEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11QosProfileTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11QosProfileEntry
*            pWsscfgIsSetFsDot11QosProfileEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11QosProfileTable (tCliHandle CliHandle,
                                    tWsscfgFsDot11QosProfileEntry *
                                    pWsscfgSetFsDot11QosProfileEntry,
                                    tWsscfgIsSetFsDot11QosProfileEntry *
                                    pWsscfgIsSetFsDot11QosProfileEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11QosProfileTable
        (&u4ErrorCode, pWsscfgSetFsDot11QosProfileEntry,
         pWsscfgIsSetFsDot11QosProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\nInvalid Inputs\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11QosProfileTable
        (pWsscfgSetFsDot11QosProfileEntry,
         pWsscfgIsSetFsDot11QosProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\nDatabase Updation failed\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11WlanCapabilityProfileTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11WlanCapabilityProfileEntry
*            pWsscfgIsSetFsDot11WlanCapabilityProfileEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11WlanCapabilityProfileTable (tCliHandle CliHandle,
                                               tWsscfgFsDot11WlanCapabilityProfileEntry
                                               *
                                               pWsscfgSetFsDot11WlanCapabilityProfileEntry,
                                               tWsscfgIsSetFsDot11WlanCapabilityProfileEntry
                                               *
                                               pWsscfgIsSetFsDot11WlanCapabilityProfileEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11WlanCapabilityProfileTable
        (&u4ErrorCode, pWsscfgSetFsDot11WlanCapabilityProfileEntry,
         pWsscfgIsSetFsDot11WlanCapabilityProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "Invalid Inputs\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11WlanCapabilityProfileTable
        (pWsscfgSetFsDot11WlanCapabilityProfileEntry,
         pWsscfgIsSetFsDot11WlanCapabilityProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "Updation failed\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11WlanAuthenticationProfileTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11WlanAuthenticationProfileEntry
*            pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11WlanAuthenticationProfileTable (tCliHandle CliHandle,
                                                   tWsscfgFsDot11WlanAuthenticationProfileEntry
                                                   *
                                                   pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
                                                   tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry
                                                   *
                                                   pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11WlanAuthenticationProfileTable
        (&u4ErrorCode,
         pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
         pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Invalid input\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11WlanAuthenticationProfileTable
        (pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
         pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Database Updation failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11WlanQosProfileTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11WlanQosProfileEntry
*            pWsscfgIsSetFsDot11WlanQosProfileEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11WlanQosProfileTable (tCliHandle CliHandle,
                                        tWsscfgFsDot11WlanQosProfileEntry *
                                        pWsscfgSetFsDot11WlanQosProfileEntry,
                                        tWsscfgIsSetFsDot11WlanQosProfileEntry *
                                        pWsscfgIsSetFsDot11WlanQosProfileEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11WlanQosProfileTable
        (&u4ErrorCode, pWsscfgSetFsDot11WlanQosProfileEntry,
         pWsscfgIsSetFsDot11WlanQosProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11WlanQosProfileTable
        (pWsscfgSetFsDot11WlanQosProfileEntry,
         pWsscfgIsSetFsDot11WlanQosProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11RadioQosTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11RadioQosEntry
*            pWsscfgIsSetFsDot11RadioQosEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11RadioQosTable (tCliHandle CliHandle,
                                  tWsscfgFsDot11RadioQosEntry *
                                  pWsscfgSetFsDot11RadioQosEntry,
                                  tWsscfgIsSetFsDot11RadioQosEntry *
                                  pWsscfgIsSetFsDot11RadioQosEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11RadioQosTable
        (&u4ErrorCode, pWsscfgSetFsDot11RadioQosEntry,
         pWsscfgIsSetFsDot11RadioQosEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11RadioQosTable
        (pWsscfgSetFsDot11RadioQosEntry,
         pWsscfgIsSetFsDot11RadioQosEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11QAPTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11QAPEntry
*            pWsscfgIsSetFsDot11QAPEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11QAPTable (tCliHandle CliHandle,
                             tWsscfgFsDot11QAPEntry * pWsscfgSetFsDot11QAPEntry,
                             tWsscfgIsSetFsDot11QAPEntry *
                             pWsscfgIsSetFsDot11QAPEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11QAPTable
        (&u4ErrorCode, pWsscfgSetFsDot11QAPEntry,
         pWsscfgIsSetFsDot11QAPEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11QAPTable
        (pWsscfgSetFsDot11QAPEntry,
         pWsscfgIsSetFsDot11QAPEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsQAPProfileTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsQAPProfileEntry
*            pWsscfgIsSetFsQAPProfileEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsQAPProfileTable (tCliHandle CliHandle,
                               tWsscfgFsQAPProfileEntry *
                               pWsscfgSetFsQAPProfileEntry,
                               tWsscfgIsSetFsQAPProfileEntry *
                               pWsscfgIsSetFsQAPProfileEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsQAPProfileTable
        (&u4ErrorCode, pWsscfgSetFsQAPProfileEntry,
         pWsscfgIsSetFsQAPProfileEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsQAPProfileTable
        (pWsscfgSetFsQAPProfileEntry,
         pWsscfgIsSetFsQAPProfileEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11CapabilityMappingTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11CapabilityMappingEntry
*            pWsscfgIsSetFsDot11CapabilityMappingEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11CapabilityMappingTable (tCliHandle CliHandle,
                                           tWsscfgFsDot11CapabilityMappingEntry
                                           *
                                           pWsscfgSetFsDot11CapabilityMappingEntry,
                                           tWsscfgIsSetFsDot11CapabilityMappingEntry
                                           *
                                           pWsscfgIsSetFsDot11CapabilityMappingEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11CapabilityMappingTable
        (&u4ErrorCode, pWsscfgSetFsDot11CapabilityMappingEntry,
         pWsscfgIsSetFsDot11CapabilityMappingEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nInvalid Inputs\r\n");
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11CapabilityMappingTable
        (pWsscfgSetFsDot11CapabilityMappingEntry,
         pWsscfgIsSetFsDot11CapabilityMappingEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nDB Updation Failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11AuthMappingTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11AuthMappingEntry
*            pWsscfgIsSetFsDot11AuthMappingEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11AuthMappingTable (tCliHandle CliHandle,
                                     tWsscfgFsDot11AuthMappingEntry *
                                     pWsscfgSetFsDot11AuthMappingEntry,
                                     tWsscfgIsSetFsDot11AuthMappingEntry *
                                     pWsscfgIsSetFsDot11AuthMappingEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11AuthMappingTable
        (&u4ErrorCode, pWsscfgSetFsDot11AuthMappingEntry,
         pWsscfgIsSetFsDot11AuthMappingEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nInvalid Inputs\r\n");
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11AuthMappingTable
        (pWsscfgSetFsDot11AuthMappingEntry,
         pWsscfgIsSetFsDot11AuthMappingEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nDB Updation Failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11QosMappingTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11QosMappingEntry
*            pWsscfgIsSetFsDot11QosMappingEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11QosMappingTable (tCliHandle CliHandle,
                                    tWsscfgFsDot11QosMappingEntry *
                                    pWsscfgSetFsDot11QosMappingEntry,
                                    tWsscfgIsSetFsDot11QosMappingEntry *
                                    pWsscfgIsSetFsDot11QosMappingEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11QosMappingTable
        (&u4ErrorCode, pWsscfgSetFsDot11QosMappingEntry,
         pWsscfgIsSetFsDot11QosMappingEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nInvalid Inputs\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11QosMappingTable
        (pWsscfgSetFsDot11QosMappingEntry,
         pWsscfgIsSetFsDot11QosMappingEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nDB Updation Failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11AntennasListTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11AntennasListEntry
*            pWsscfgIsSetFsDot11AntennasListEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11AntennasListTable (tCliHandle CliHandle,
                                      tWsscfgFsDot11AntennasListEntry *
                                      pWsscfgSetFsDot11AntennasListEntry,
                                      tWsscfgIsSetFsDot11AntennasListEntry *
                                      pWsscfgIsSetFsDot11AntennasListEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11AntennasListTable
        (&u4ErrorCode, pWsscfgSetFsDot11AntennasListEntry,
         pWsscfgIsSetFsDot11AntennasListEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11AntennasListTable
        (pWsscfgSetFsDot11AntennasListEntry,
         pWsscfgIsSetFsDot11AntennasListEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * * Function    :  WsscfgCliSetFsDot11nConfigTable
 * * Description :
 * * Input       :  CliHandle 
 * *            pWsscfgSetFsDot11nConfigEntry
 * *            pWsscfgIsSetFsDot11nConfigEntry
 * * Output      :  None 
 * * Returns     :  CLI_SUCCESS/CLI_FAILURE
 * ****************************************************************************/
INT4
WsscfgCliSetFsDot11nConfigTable (tCliHandle CliHandle,
                                 tWsscfgFsDot11nConfigEntry *
                                 pWsscfgSetFsDot11nConfigEntry,
                                 tWsscfgIsSetFsDot11nConfigEntry *
                                 pWsscfgIsSetFsDot11nConfigEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11nConfigTable
        (&u4ErrorCode, pWsscfgSetFsDot11nConfigEntry,
         pWsscfgIsSetFsDot11nConfigEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11nConfigTable
        (pWsscfgSetFsDot11nConfigEntry,
         pWsscfgIsSetFsDot11nConfigEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * * Function    :  WsscfgCliSetFsDot11nMCSDataRateTable
 * * Description :
 * * Input       :  CliHandle 
 * *            pWsscfgSetFsDot11nMCSDataRateEntry
 * *            pWsscfgIsSetFsDot11nMCSDataRateEntry
 * * Output      :  None 
 * * Returns     :  CLI_SUCCESS/CLI_FAILURE
 * ****************************************************************************/
INT4
WsscfgCliSetFsDot11nMCSDataRateTable (tCliHandle CliHandle,
                                      tWsscfgFsDot11nMCSDataRateEntry *
                                      pWsscfgSetFsDot11nMCSDataRateEntry,
                                      tWsscfgIsSetFsDot11nMCSDataRateEntry *
                                      pWsscfgIsSetFsDot11nMCSDataRateEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11nMCSDataRateTable
        (&u4ErrorCode, pWsscfgSetFsDot11nMCSDataRateEntry,
         pWsscfgIsSetFsDot11nMCSDataRateEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11nMCSDataRateTable
        (pWsscfgSetFsDot11nMCSDataRateEntry,
         pWsscfgIsSetFsDot11nMCSDataRateEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsWtpImageUpgradeTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsWtpImageUpgradeEntry
*            pWsscfgIsSetFsWtpImageUpgradeEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsWtpImageUpgradeTable (tCliHandle CliHandle,
                                    tWsscfgFsWtpImageUpgradeEntry *
                                    pWsscfgSetFsWtpImageUpgradeEntry,
                                    tWsscfgIsSetFsWtpImageUpgradeEntry *
                                    pWsscfgIsSetFsWtpImageUpgradeEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsWtpImageUpgradeTable
        (&u4ErrorCode, pWsscfgSetFsWtpImageUpgradeEntry,
         pWsscfgIsSetFsWtpImageUpgradeEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsWtpImageUpgradeTable
        (pWsscfgSetFsWtpImageUpgradeEntry,
         pWsscfgIsSetFsWtpImageUpgradeEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11WlanTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11WlanEntry
*            pWsscfgIsSetFsDot11WlanEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11WlanTable (tCliHandle CliHandle,
                              tWsscfgFsDot11WlanEntry *
                              pWsscfgSetFsDot11WlanEntry,
                              tWsscfgIsSetFsDot11WlanEntry *
                              pWsscfgIsSetFsDot11WlanEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11WlanTable
        (&u4ErrorCode, pWsscfgSetFsDot11WlanEntry,
         pWsscfgIsSetFsDot11WlanEntry, OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11WlanTable
        (pWsscfgSetFsDot11WlanEntry, pWsscfgIsSetFsDot11WlanEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11WlanBindTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsDot11WlanBindEntry
*            pWsscfgIsSetFsDot11WlanBindEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11WlanBindTable (tCliHandle CliHandle,
                                  tWsscfgFsDot11WlanBindEntry *
                                  pWsscfgSetFsDot11WlanBindEntry,
                                  tWsscfgIsSetFsDot11WlanBindEntry *
                                  pWsscfgIsSetFsDot11WlanBindEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsDot11WlanBindTable
        (&u4ErrorCode, pWsscfgSetFsDot11WlanBindEntry,
         pWsscfgIsSetFsDot11WlanBindEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsDot11WlanBindTable
        (pWsscfgSetFsDot11WlanBindEntry,
         pWsscfgIsSetFsDot11WlanBindEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11aNetworkEnable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11aNetworkEnable (tCliHandle CliHandle,
                                   UINT4 *pFsDot11aNetworkEnable)
{
    UINT4               u4ErrorCode;
    INT4                i4FsDot11aNetworkEnable = 0;

    WSSCFG_FILL_FSDOT11ANETWORKENABLE (i4FsDot11aNetworkEnable,
                                       pFsDot11aNetworkEnable);

    if (WsscfgTestFsDot11aNetworkEnable
        (&u4ErrorCode, i4FsDot11aNetworkEnable) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Invalid Configuration received\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetFsDot11aNetworkEnable (i4FsDot11aNetworkEnable) !=
        OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Network Configuration failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11bNetworkEnable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11bNetworkEnable (tCliHandle CliHandle,
                                   UINT4 *pFsDot11bNetworkEnable)
{
    UINT4               u4ErrorCode;
    INT4                i4FsDot11bNetworkEnable = 0;

    WSSCFG_FILL_FSDOT11BNETWORKENABLE (i4FsDot11bNetworkEnable,
                                       pFsDot11bNetworkEnable);

    if (WsscfgTestFsDot11bNetworkEnable
        (&u4ErrorCode, i4FsDot11bNetworkEnable) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsDot11bNetworkEnable (i4FsDot11bNetworkEnable) !=
        OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n Network Configuration failed\r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11gSupport
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11gSupport (tCliHandle CliHandle, UINT4 *pFsDot11gSupport)
{
    UINT4               u4ErrorCode;
    INT4                i4FsDot11gSupport = 0;

    WSSCFG_FILL_FSDOT11GSUPPORT (i4FsDot11gSupport, pFsDot11gSupport);

    if (WsscfgTestFsDot11gSupport (&u4ErrorCode, i4FsDot11gSupport) !=
        OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsDot11gSupport (i4FsDot11gSupport) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11anSupport
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11anSupport (tCliHandle CliHandle, UINT4 *pFsDot11anSupport)
{
    UINT4               u4ErrorCode;
    INT4                i4FsDot11anSupport = 0;

    WSSCFG_FILL_FSDOT11ANSUPPORT (i4FsDot11anSupport, pFsDot11anSupport);

    if (WsscfgTestFsDot11anSupport (&u4ErrorCode, i4FsDot11anSupport)
        != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsDot11anSupport (i4FsDot11anSupport) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11bnSupport
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11bnSupport (tCliHandle CliHandle, UINT4 *pFsDot11bnSupport)
{
    UINT4               u4ErrorCode;
    INT4                i4FsDot11bnSupport = 0;

    WSSCFG_FILL_FSDOT11BNSUPPORT (i4FsDot11bnSupport, pFsDot11bnSupport);

    if (WsscfgTestFsDot11bnSupport (&u4ErrorCode, i4FsDot11bnSupport)
        != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsDot11bnSupport (i4FsDot11bnSupport) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11ManagmentSSID
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11ManagmentSSID (tCliHandle CliHandle,
                                  UINT4 *pFsDot11ManagmentSSID)
{
    UINT4               u4ErrorCode;
    UINT4               u4FsDot11ManagmentSSID = 0;

    WSSCFG_FILL_FSDOT11MANAGMENTSSID (u4FsDot11ManagmentSSID,
                                      pFsDot11ManagmentSSID);

    if (WsscfgTestFsDot11ManagmentSSID
        (&u4ErrorCode, u4FsDot11ManagmentSSID) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsDot11ManagmentSSID (u4FsDot11ManagmentSSID) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsDot11CountryString
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsDot11CountryString (tCliHandle CliHandle,
                                  UINT4 *pFsDot11CountryString)
{
    UINT4               u4ErrorCode;
    UINT1               au1FsDot11CountryString[3];

    MEMSET (au1FsDot11CountryString, 0, 3);
    WSSCFG_FILL_FSDOT11COUNTRYSTRING (au1FsDot11CountryString,
                                      pFsDot11CountryString);
    au1FsDot11CountryString[3 - 1] = '\0';

    if (WsscfgTestFsDot11CountryString
        (&u4ErrorCode, au1FsDot11CountryString) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsDot11CountryString (au1FsDot11CountryString) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

#ifdef DEBUG_WANTED
/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthType
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthType (tCliHandle CliHandle,
                                   UINT4 *pFsSecurityWebAuthType)
{
    UINT4               u4ErrorCode;
    INT4                i4FsSecurityWebAuthType = 0;

    WSSCFG_FILL_FSSECURITYWEBAUTHTYPE (i4FsSecurityWebAuthType,
                                       pFsSecurityWebAuthType);

    if (WsscfgTestFsSecurityWebAuthType
        (&u4ErrorCode, i4FsSecurityWebAuthType) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsSecurityWebAuthType (i4FsSecurityWebAuthType) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthUrl
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthUrl (tCliHandle CliHandle,
                                  UINT1 *pFsSecurityWebAuthUrl)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE WebAuthUrl;
    MEMSET (&WebAuthUrl, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    WebAuthUrl.i4_Length = STRLEN (pFsSecurityWebAuthUrl);
    WebAuthUrl.pu1_OctetList = pFsSecurityWebAuthUrl;
    if (WsscfgTestFsSecurityWebAuthUrl (&u4ErrorCode, &WebAuthUrl) !=
        OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsSecurityWebAuthUrl (&WebAuthUrl) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    UNUSED_PARAM (u4ErrorCode);
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthRedirectUrl
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthRedirectUrl (tCliHandle CliHandle,
                                          UINT4 *pFsSecurityWebAuthRedirectUrl)
{
    UINT4               u4ErrorCode;
    UINT1               au1FsSecurityWebAuthRedirectUrl[256];

    MEMSET (au1FsSecurityWebAuthRedirectUrl, 0, 256);

    WSSCFG_FILL_FSSECURITYWEBAUTHREDIRECTURL
        (au1FsSecurityWebAuthRedirectUrl, pFsSecurityWebAuthRedirectUrl);

    if (WsscfgTestFsSecurityWebAuthRedirectUrl
        (&u4ErrorCode, au1FsSecurityWebAuthRedirectUrl) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsSecurityWebAuthRedirectUrl
        (au1FsSecurityWebAuthRedirectUrl) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAddr
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAddr (tCliHandle CliHandle, UINT4 *pFsSecurityWebAddr)
{
    UINT4               u4ErrorCode;
    INT4                i4FsSecurityWebAddr = 0;

    WSSCFG_FILL_FSSECURITYWEBADDR (i4FsSecurityWebAddr, pFsSecurityWebAddr);

    if (WsscfgTestFsSecurityWebAddr (&u4ErrorCode, i4FsSecurityWebAddr)
        != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsSecurityWebAddr (i4FsSecurityWebAddr) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}
#endif
/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthWebTitle
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthWebTitle (tCliHandle CliHandle,
                                       UINT1 *pFsSecurityWebAuthWebTitle)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE WebAuthTitle;
    MEMSET (&WebAuthTitle, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    WebAuthTitle.i4_Length = STRLEN (pFsSecurityWebAuthWebTitle);
    WebAuthTitle.pu1_OctetList = pFsSecurityWebAuthWebTitle;

    if (WsscfgTestFsSecurityWebAuthWebTitle
        (&u4ErrorCode, &WebAuthTitle) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsSecurityWebAuthWebTitle (&WebAuthTitle) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliClearFsSecurityWebAuthWebTitle
* Description :
* Input       :  CliHandle 

* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliClearFsSecurityWebAuthWebTitle (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);

    if ((STRCMP (gWsscfgGlobals.WsscfgGlbMib.au1FsSecurityWebAuthWebTitle, ""))
        != OSIX_FALSE)
    {
        MEMSET (gWsscfgGlobals.WsscfgGlbMib.
                au1FsSecurityWebAuthWebTitle, 0,
                sizeof (gWsscfgGlobals.
                        WsscfgGlbMib.au1FsSecurityWebAuthWebTitle));
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthWebMessage
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthWebMessage (tCliHandle CliHandle,
                                         UINT1 *pFsSecurityWebAuthWebMessage)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE WebAuthMessage;
    MEMSET (&WebAuthMessage, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    WebAuthMessage.i4_Length = STRLEN (pFsSecurityWebAuthWebMessage);
    WebAuthMessage.pu1_OctetList = pFsSecurityWebAuthWebMessage;

    if (WsscfgTestFsSecurityWebAuthWebMessage
        (&u4ErrorCode, &WebAuthMessage) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsSecurityWebAuthWebMessage (&WebAuthMessage) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliClearFsSecurityWebAuthWebMessage
* Description :
* Input       :  CliHandle 
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliClearFsSecurityWebAuthWebMessage (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);

    if ((STRCMP
         (gWsscfgGlobals.WsscfgGlbMib.au1FsSecurityWebAuthWebMessage,
          "")) != OSIX_FALSE)
    {
        MEMSET (gWsscfgGlobals.WsscfgGlbMib.
                au1FsSecurityWebAuthWebMessage, 0,
                sizeof (gWsscfgGlobals.
                        WsscfgGlbMib.au1FsSecurityWebAuthWebMessage));
    }

    return CLI_SUCCESS;

}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthWebLogoFileName
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthWebLogoFileName (tCliHandle CliHandle,
                                              UINT4 *pWebLogoStatus,
                                              UINT1
                                              *pFsSecurityWebAuthWebLogoFileName)
{
    UINT4               u4ErrorCode;
    INT4                i4WebLogoStatus;
    tSNMP_OCTET_STRING_TYPE WebAuthLogo;
    MEMSET (&WebAuthLogo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    i4WebLogoStatus = *(INT4 *) pWebLogoStatus;

    if (i4WebLogoStatus == WSS_AUTH_STATUS_ENABLE)
    {
        WebAuthLogo.i4_Length = STRLEN (pFsSecurityWebAuthWebLogoFileName);
        WebAuthLogo.pu1_OctetList = pFsSecurityWebAuthWebLogoFileName;
        if (WsscfgTestFsSecurityWebAuthWebLogoFileName
            (&u4ErrorCode, &WebAuthLogo) != OSIX_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (WsscfgSetFsSecurityWebAuthWebLogoFileName (&WebAuthLogo) !=
            OSIX_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if ((STRLEN
             (gWsscfgGlobals.
              WsscfgGlbMib.au1FsSecurityWebAuthWebLogoFileName)) != 0)
        {
            /* Reset the Logo filename value to 0,if the command is to DISABLE */
            MEMSET (gWsscfgGlobals.
                    WsscfgGlbMib.au1FsSecurityWebAuthWebLogoFileName,
                    0,
                    sizeof (gWsscfgGlobals.
                            WsscfgGlbMib.au1FsSecurityWebAuthWebLogoFileName));
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthWebSuccMessage
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthWebSuccMessage (tCliHandle CliHandle,
                                             UINT1
                                             *pFsSecurityWebAuthWebSuccMessage)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE WebAuthSuccMsg;
    MEMSET (&WebAuthSuccMsg, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    WebAuthSuccMsg.i4_Length = STRLEN (pFsSecurityWebAuthWebSuccMessage);
    WebAuthSuccMsg.pu1_OctetList = pFsSecurityWebAuthWebSuccMessage;

    if (WsscfgTestFsSecurityWebAuthWebSuccMessage
        (&u4ErrorCode, &WebAuthSuccMsg) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsSecurityWebAuthWebSuccMessage (&WebAuthSuccMsg) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgCliClearFsSecurityWebAuthWebSuccMessage 
* Description :
* Input       :  CliHandle 
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliClearFsSecurityWebAuthWebSuccMessage (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);

    if ((STRCMP (gWsscfgGlobals.WsscfgGlbMib.
                 au1FsSecurityWebAuthWebSuccMessage, "")) != OSIX_FALSE)
    {

        MEMSET (gWsscfgGlobals.
                WsscfgGlbMib.au1FsSecurityWebAuthWebSuccMessage, 0,
                sizeof (gWsscfgGlobals.
                        WsscfgGlbMib.au1FsSecurityWebAuthWebSuccMessage));
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthWebFailMessage
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthWebFailMessage (tCliHandle CliHandle,
                                             UINT1
                                             *pFsSecurityWebAuthWebFailMessage)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE WebAuthFailMsg;
    MEMSET (&WebAuthFailMsg, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    WebAuthFailMsg.i4_Length = STRLEN (pFsSecurityWebAuthWebFailMessage);
    WebAuthFailMsg.pu1_OctetList = pFsSecurityWebAuthWebFailMessage;

    if (WsscfgTestFsSecurityWebAuthWebFailMessage
        (&u4ErrorCode, &WebAuthFailMsg) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsSecurityWebAuthWebFailMessage (&WebAuthFailMsg) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgCliClearFsSecurityWebAuthWebFailMessage 
* Description :
* Input       :  CliHandle 
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliClearFsSecurityWebAuthWebFailMessage (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    if ((STRCMP (gWsscfgGlobals.WsscfgGlbMib.
                 au1FsSecurityWebAuthWebFailMessage, "")) != OSIX_FALSE)
    {
        MEMSET (gWsscfgGlobals.
                WsscfgGlbMib.au1FsSecurityWebAuthWebFailMessage, 0,
                sizeof (gWsscfgGlobals.
                        WsscfgGlbMib.au1FsSecurityWebAuthWebFailMessage));
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthWebButtonText
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthWebButtonText (tCliHandle CliHandle,
                                            UINT1
                                            *pFsSecurityWebAuthWebButtonText)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE WebAuthButonTxt;
    MEMSET (&WebAuthButonTxt, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    WebAuthButonTxt.i4_Length = STRLEN (pFsSecurityWebAuthWebButtonText);
    WebAuthButonTxt.pu1_OctetList = pFsSecurityWebAuthWebButtonText;

    if (WsscfgTestFsSecurityWebAuthWebButtonText
        (&u4ErrorCode, &WebAuthButonTxt) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsSecurityWebAuthWebButtonText (&WebAuthButonTxt) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgCliClearFsSecurityWebAuthWebButtonText 
* Description :
* Input       :  CliHandle 
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliClearFsSecurityWebAuthWebButtonText (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    if ((STRCMP (gWsscfgGlobals.WsscfgGlbMib.
                 au1FsSecurityWebAuthWebButtonText, "")) != OSIX_FALSE)
    {
        MEMSET (gWsscfgGlobals.
                WsscfgGlbMib.au1FsSecurityWebAuthWebButtonText, 0,
                sizeof (gWsscfgGlobals.
                        WsscfgGlbMib.au1FsSecurityWebAuthWebButtonText));
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthWebLoadBalInfo
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthWebLoadBalInfo (tCliHandle CliHandle,
                                             UINT1
                                             *pFsSecurityWebAuthWebLoadBalInfo)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE WebAuthLdBalMsg;
    MEMSET (&WebAuthLdBalMsg, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    WebAuthLdBalMsg.i4_Length = STRLEN (pFsSecurityWebAuthWebLoadBalInfo);
    WebAuthLdBalMsg.pu1_OctetList = pFsSecurityWebAuthWebLoadBalInfo;

    if (WsscfgTestFsSecurityWebAuthWebLoadBalInfo
        (&u4ErrorCode, &WebAuthLdBalMsg) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (WsscfgSetFsSecurityWebAuthWebLoadBalInfo (&WebAuthLdBalMsg) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliClearFsSecurityWebAuthWebLoadBalInfo
* Description :
* Input       :  CliHandle 
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
WsscfgCliClearFsSecurityWebAuthWebLoadBalInfo (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    if (gWsscfgGlobals.WsscfgGlbMib.au1FsSecurityWebAuthWebLoadBalInfo != NULL)
    {
        MEMSET (gWsscfgGlobals.
                WsscfgGlbMib.au1FsSecurityWebAuthWebLoadBalInfo, 0,
                sizeof (gWsscfgGlobals.
                        WsscfgGlbMib.au1FsSecurityWebAuthWebLoadBalInfo));
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthDisplayLang
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthDisplayLang (tCliHandle CliHandle,
                                          UINT4 *pFsSecurityWebAuthDisplayLang)
{
    UINT4               u4ErrorCode;
    INT4                i4FsSecurityWebAuthDisplayLang = 0;

    WSSCFG_FILL_FSSECURITYWEBAUTHDISPLAYLANG
        (i4FsSecurityWebAuthDisplayLang, pFsSecurityWebAuthDisplayLang);

    if (WsscfgTestFsSecurityWebAuthDisplayLang
        (&u4ErrorCode, i4FsSecurityWebAuthDisplayLang) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetFsSecurityWebAuthDisplayLang
        (i4FsSecurityWebAuthDisplayLang) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsSecurityWebAuthColor
* Description :
* Input       :  CliHandle 
*            pWsscfgSetScalar
*            pWsscfgIsSetScalar
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsSecurityWebAuthColor (tCliHandle CliHandle,
                                    UINT4 *pFsSecurityWebAuthColor)
{
    UINT4               u4ErrorCode;
    INT4                i4FsSecurityWebAuthColor = 0;

    WSSCFG_FILL_FSSECURITYWEBAUTHCOLOR (i4FsSecurityWebAuthColor,
                                        pFsSecurityWebAuthColor);

    if (WsscfgTestFsSecurityWebAuthColor
        (&u4ErrorCode, i4FsSecurityWebAuthColor) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetFsSecurityWebAuthColor (i4FsSecurityWebAuthColor) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCliSetFsRrmConfigTable
* Description :
* Input       :  CliHandle 
*            pWsscfgSetFsRrmConfigEntry
*            pWsscfgIsSetFsRrmConfigEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCliSetFsRrmConfigTable (tCliHandle CliHandle,
                              tWsscfgFsRrmConfigEntry *
                              pWsscfgSetFsRrmConfigEntry,
                              tWsscfgIsSetFsRrmConfigEntry *
                              pWsscfgIsSetFsRrmConfigEntry)
{
    UINT4               u4ErrorCode;

    if (WsscfgTestAllFsRrmConfigTable
        (&u4ErrorCode, pWsscfgSetFsRrmConfigEntry,
         pWsscfgIsSetFsRrmConfigEntry, OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n RRM Config validation failed \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (WsscfgSetAllFsRrmConfigTable
        (pWsscfgSetFsRrmConfigEntry, pWsscfgIsSetFsRrmConfigEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgWlanCapabilityCreate
 * Description :  This function will change the prompt name from config 
                terminal to capability profile name

 * Input       :  CliHandle - CLI Handler
                  pu1Prompt - Prompt Name

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgWlanCapabilityCreate (tCliHandle CliHandle, UINT1 *pu1Prompt)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE CapabilityName;
    INT4                i4FsDot11CapabilityRowStatus;

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    MEMSET (&CapabilityName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    CapabilityName.i4_Length = STRLEN (pu1Prompt);

    CapabilityName.pu1_OctetList = pu1Prompt;

    /* Check if the profile is already present */
    if ((nmhGetFsDot11CapabilityRowStatus (&CapabilityName,
                                           &i4FsDot11CapabilityRowStatus))
        != SNMP_SUCCESS)
    {
        /* Profile NOT present - CREATE */

        if (nmhTestv2FsDot11CapabilityRowStatus
            (&u4ErrCode, &CapabilityName, CREATE_AND_GO) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n Entry Creation failed \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return OSIX_FAILURE;
        }

        /* CREATE Profile */
        if (nmhSetFsDot11CapabilityRowStatus
            (&CapabilityName, CREATE_AND_GO) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n Row Status Creation failed \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return OSIX_FAILURE;
        }
    }

    /* ENTER Capability profile Mode */
    SNPRINTF ((CHR1 *) au1Cmd, MAX_PROMPT_LEN, "%s%s",
              WSSCFG_CLI_CAPABILITY_MODE, pu1Prompt);
    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "/r%% Unable to enter into capability profile mode\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgWlanCapabilityDelete
 * Description :  This function will delete the entry for the given profile
                  name

 * Input       :  CliHandle - CLI Handler
                  pu1Prompt - Prompt Name

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgWlanCapabilityDelete (tCliHandle CliHandle, UINT1 *pu1Prompt)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE CapabilityName;

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    MEMSET (&CapabilityName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    CapabilityName.i4_Length = STRLEN (pu1Prompt);

    CapabilityName.pu1_OctetList = pu1Prompt;

    if (nmhTestv2FsDot11CapabilityRowStatus
        (&u4ErrCode, &CapabilityName, DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return OSIX_FAILURE;
    }

    /* DELETE Profile */
    if (nmhSetFsDot11CapabilityRowStatus (&CapabilityName, DESTROY)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n Row Status Deletion failed \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgWlanQosCreate
 * Description :  This function will change the prompt name from config 
                terminal to Qos profile name

 * Input       :  CliHandle - CLI Handler
                  pu1Prompt - Prompt Name

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgWlanQosCreate (tCliHandle CliHandle, UINT1 *pu1Prompt)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT4               u4ErrCode;
    INT4                i4FsDot11QosRowStatus;
    tSNMP_OCTET_STRING_TYPE QosProfileName;

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    MEMSET (&QosProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    QosProfileName.i4_Length = STRLEN (pu1Prompt);
    QosProfileName.pu1_OctetList = pu1Prompt;

    /* Check if the profile is already present */
    if ((nmhGetFsDot11QosRowStatus (&QosProfileName,
                                    &i4FsDot11QosRowStatus)) != SNMP_SUCCESS)
    {
        /* Profile NOT present - CREATE */

        if (nmhTestv2FsDot11QosRowStatus (&u4ErrCode, &QosProfileName,
                                          CREATE_AND_GO) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n Entry Creation failed \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return OSIX_FAILURE;
        }

        /* CREATE Profile */
        if (nmhSetFsDot11QosRowStatus (&QosProfileName, CREATE_AND_GO)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n Row Status Creation failed \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return OSIX_FAILURE;
        }
    }

    /* ENTER Qos profile Mode */
    SNPRINTF ((CHR1 *) au1Cmd, MAX_PROMPT_LEN, "%s%s",
              WSSCFG_CLI_QOS_MODE, pu1Prompt);
    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "/r%% Unable to enter into Qos profile mode\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgWlanQosDelete
 * Description :  This function will delete the entry for the given profile
                  name

 * Input       :  CliHandle - CLI Handler
                  pu1Prompt - Prompt Name

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgWlanQosDelete (tCliHandle CliHandle, UINT1 *pu1Prompt)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE QosProfileName;

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    MEMSET (&QosProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    QosProfileName.i4_Length = STRLEN (pu1Prompt);

    QosProfileName.pu1_OctetList = pu1Prompt;

    if (nmhTestv2FsDot11QosRowStatus (&u4ErrCode, &QosProfileName,
                                      DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return OSIX_FAILURE;
    }

    /* DELETE Profile */
    if (nmhSetFsDot11QosRowStatus (&QosProfileName, DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n Row Status Deletion failed \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgWlanAuthCreate
 * Description :  This function will change the prompt name from config 
                terminal to Qos profile name

 * Input       :  CliHandle - CLI Handler
                  pu1Prompt - Prompt Name

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgWlanAuthCreate (tCliHandle CliHandle, UINT1 *pu1Prompt)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT4               u4ErrCode;
    INT4                i4FsDot11AuthRowStatus;
    tSNMP_OCTET_STRING_TYPE AuthProfileName;

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    MEMSET (&AuthProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    AuthProfileName.i4_Length = STRLEN (pu1Prompt);
    AuthProfileName.pu1_OctetList = pu1Prompt;

    /* Check if the profile is already present */
    if ((nmhGetFsDot11AuthenticationRowStatus (&AuthProfileName,
                                               &i4FsDot11AuthRowStatus))
        != SNMP_SUCCESS)
    {
        /* Profile NOT present - CREATE */

        if (nmhTestv2FsDot11AuthenticationRowStatus
            (&u4ErrCode, &AuthProfileName, CREATE_AND_GO) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n Entry Creation failed \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return OSIX_FAILURE;
        }

        /* CREATE Profile */
        if (nmhSetFsDot11AuthenticationRowStatus
            (&AuthProfileName, CREATE_AND_GO) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n Row Status Creation failed \r\n");
            CLI_FATAL_ERROR (CliHandle);
            return OSIX_FAILURE;
        }
    }

    /* ENTER Qos profile Mode */
    SNPRINTF ((CHR1 *) au1Cmd, MAX_PROMPT_LEN, "%s%s",
              WSSCFG_CLI_AUTH_MODE, pu1Prompt);
    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "/r%% Unable to enter into Auth profile mode\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgWlanAuthDelete
 * Description :  This function will delete the entry for the given profile
                  name

 * Input       :  CliHandle - CLI Handler
                  pu1Prompt - Prompt Name

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgWlanAuthDelete (tCliHandle CliHandle, UINT1 *pu1Prompt)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE AuthProfileName;

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    MEMSET (&AuthProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    AuthProfileName.i4_Length = STRLEN (pu1Prompt);

    AuthProfileName.pu1_OctetList = pu1Prompt;

    if (nmhTestv2FsDot11AuthenticationRowStatus
        (&u4ErrCode, &AuthProfileName, DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return OSIX_FAILURE;
    }

    /* DELETE Profile */
    if (nmhSetFsDot11AuthenticationRowStatus
        (&AuthProfileName, DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n Row Status Deletion failed \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/* SHOW COMMANDS*/
/****************************************************************************
 * Function    :  cli_process_Wsscfg_show_cmd
 * Description :  This function is exported to CLI module to handle the
                WSSCFG cli commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
cli_process_wsscfg_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[WSSCFG_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT4               u4CmdType = 0;
    INT4                i4Inst;
    INT4                i4RetVal = 0;

    UNUSED_PARAM (u4CmdType);
    /* check if the capwap module is started. i.e "no shutdown". else all the
     * capwap related commands are considered invalid */
    if ((u4Command == CLI_WSSCFG_SHOW_AP_CONFIG) ||
        (u4Command == CLI_WSSCFG_SHOW_NETWORK_CONFIG) ||
        (u4Command == CLI_WSSCFG_SHOW_NETWORK_SUMMARY) ||
        (u4Command == CLI_WSSCFG_SHOW_AP_WLAN_CONFIG))
    {
        if (CapwapCliCheckModuleStatusShow (CliHandle) != CLI_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    CliRegisterLock (CliHandle, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == WSSCFG_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);
    switch (u4Command)
    {
        case CLI_WSSCFG_SHOW_AP_CONFIG:
            i4RetVal =
                WssCfgShowApConfiguration (CliHandle, *args[0],
                                           (UINT1 *) &args[1], *args[2]);

            break;

        case CLI_WSSCFG_SHOW_NETWORK_CONFIG:
            if (args[1] == NULL)
            {
                i4RetVal =
                    WssCfgShowNetworkConfiguration (CliHandle, *args[0],
                                                    NULL, 0);
            }
            else
            {
                i4RetVal =
                    WssCfgShowNetworkConfiguration (CliHandle, *args[0],
                                                    (UINT1 *) args[1],
                                                    *args[2]);
            }
            break;

        case CLI_WSSCFG_SHOW_NETWORK_SUMMARY:
            i4RetVal = WssCfgShowNetworkSummary (CliHandle, args[0]);
            break;
        case CLI_WSSCFG_SHOW_AP_WLAN_CONFIG:
            i4RetVal =
                WssCfgShowApWlanConfig (CliHandle, args[0], (UINT1 *) args[1],
                                        args[2]);
            break;
        case CLI_WSSCFG_SHOW_CLIENT_SUMMARY:
            i4RetVal = WssCfgShowClientSummary (CliHandle);
            break;
        case CLI_WSSCFG_SHOW_WEBAUTH_CONFIG:
            i4RetVal = WssCfgShowWebAuthConfig (CliHandle);
            break;
        case CLI_WSSCFG_SHOW_NETUSER_SUMMARY:
            i4RetVal = WssCfgShowNetUserSummary (CliHandle);
            break;
        case CLI_WSSCFG_SHOW_COUNTRY_SUPPORTED:
            i4RetVal = WssCfgShowCountrySupported (CliHandle);
            break;
        case CLI_WSSCFG_SHOW_WLAN_CONFIG:
            i4RetVal = WssCfgShowWlanConfig (CliHandle, *args[0]);
            break;
        case CLI_WSSCFG_SHOW_WLAN_SUMMARY:
            i4RetVal = WssCfgShowWlanSummary (CliHandle);
            break;
        case CLI_WSSCFG_SHOW_WLAN_STATS:
            i4RetVal = WssCfgShowWlanStatistics (CliHandle, args[0]);
            break;
        default:
            break;
    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_WSSCFG_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", WsscfgCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);

    WSSCFG_UNLOCK;
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4RetVal);

    return i4RetStatus;

}

/****************************************************************************
* Function    : WssCfgShowNetworkConfiguration 
* Description :
* Input       :  CliHandle 
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowNetworkConfiguration (tCliHandle CliHandle,
                                UINT4 u4FsRadioType,
                                UINT1 *pu1CapwapBaseWtpProfileName,
                                UINT4 u4RadioId)
{
    INT4                i4Dot11TxPowerLevel1 = 0;
    INT4               *pi4Dot11TxPowerLevel1 = NULL;
    pi4Dot11TxPowerLevel1 = &i4Dot11TxPowerLevel1;

    INT4                i4Dot11CurrentChannel = 0;
    INT4               *pi4Dot11CurrentChannel = NULL;
    pi4Dot11CurrentChannel = &i4Dot11CurrentChannel;

    UINT4               u4WtpProfileId = 0;
    UINT1               au1WtpModelNumber[32];
    UINT1               u1RadioIndex = 0;
    INT4                i4RadioIfIndex = 0;
    UINT1               u1MCSIndex = 0;
    tWsscfgDot11PhyOFDMEntry WsscfgGetDot11PhyOFDMEntry;
    tWsscfgFsRrmConfigEntry WsscfgGetFsRrmConfigEntry;
    tWsscfgDot11PhyTxPowerEntry WsscfgGetDot11PhyTxPowerEntry;
    tWsscfgDot11OperationEntry WsscfgGetDot11OperationEntry;
    tWsscfgFsDot11WlanCapabilityProfileEntry
        WsscfgGetFsDot11WlanCapabilityProfileEntry;
    tWsscfgDot11StationConfigEntry WsscfgGetDot11StationConfigEntry;
    tWsscfgFsDot11nConfigEntry WsscfgGetFsDot11nConfigEntry;
    tWsscfgFsDot11nMCSDataRateEntry WsscfgGetFsDot11nMCSDataRateEntry;
    tRadioIfGetDB       RadioIfGetDB;
    tWsscfgDot11PhyDSSSEntry WsscfgGetDot11PhyDSSSEntry;
    UINT1               au1ModelNumber[256];
    tSNMP_OCTET_STRING_TYPE ModelName;
    INT4                i4FsDot11anSupport = 0;
    INT4                i4FsDot11aNetworkEnable = 0;
    INT4                i4FsDot11bnSupport = 0;
    INT4                i4FsDot11gSupport = 0;
    INT4                i4FsDot11bNetworkEnable = 0;
    INT4                i4AdminStatus = 0;

    UNUSED_PARAM (u4RadioId);

    MEMSET (au1WtpModelNumber, 0, 32);
    MEMSET (&ModelName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WsscfgGetDot11PhyOFDMEntry, 0, sizeof (tWsscfgDot11PhyOFDMEntry));
    MEMSET (&WsscfgGetFsRrmConfigEntry, 0, sizeof (tWsscfgFsRrmConfigEntry));
    MEMSET (&WsscfgGetDot11PhyTxPowerEntry, 0,
            sizeof (tWsscfgDot11PhyTxPowerEntry));
    MEMSET (&WsscfgGetDot11OperationEntry, 0,
            sizeof (tWsscfgDot11OperationEntry));
    MEMSET (&WsscfgGetFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));
    MEMSET (&WsscfgGetDot11StationConfigEntry, 0,
            sizeof (tWsscfgDot11StationConfigEntry));
    MEMSET (&WsscfgGetFsDot11nConfigEntry, 0,
            sizeof (tWsscfgFsDot11nConfigEntry));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    ModelName.pu1_OctetList = au1ModelNumber;

    if (pu1CapwapBaseWtpProfileName == NULL)
    {
        if (u4FsRadioType == CLI_RADIO_TYPEA)
        {
            if (nmhGetFsDot11anSupport (&i4FsDot11anSupport) == SNMP_FAILURE)
            {
                return OSIX_FAILURE;
            }

            if (nmhGetFsDot11aNetworkEnable (&i4FsDot11aNetworkEnable)
                == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            if (CLI_ENABLE == i4FsDot11anSupport)
            {
                CliPrintf (CliHandle, "\r\n11nSupport        : Enabled");
            }
            else
            {
                CliPrintf (CliHandle, "\r\n11nSupport        : Disabled");
            }
            CliPrintf (CliHandle, "\r\n");

            if (CLI_ENABLE == i4FsDot11aNetworkEnable)
            {
                CliPrintf (CliHandle, "\r\n802.11a Network  : Enabled");
            }
            else
            {
                CliPrintf (CliHandle, "\r\n802.11a Network   : Disabled");
            }
            CliPrintf (CliHandle, "\r\n");
            return CLI_SUCCESS;
        }

        else if (u4FsRadioType == CLI_RADIO_TYPEB)
        {
            if (nmhGetFsDot11bnSupport (&i4FsDot11bnSupport) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhGetFsDot11gSupport (&i4FsDot11gSupport) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhGetFsDot11bNetworkEnable (&i4FsDot11bNetworkEnable)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (CLI_ENABLE == i4FsDot11bnSupport)
            {
                CliPrintf (CliHandle, "\r\n11n Support       : Enabled");
            }
            else
            {
                CliPrintf (CliHandle, "\r\n11n Support       : Disabled");
            }

            CliPrintf (CliHandle, "\r\n");
            if (CLI_ENABLE == i4FsDot11bNetworkEnable)
            {
                CliPrintf (CliHandle, "\r\n802.11b Network   : Enabled");
            }
            else
            {
                CliPrintf (CliHandle, "\r\n802.11b Network   : Disabled");
            }

            CliPrintf (CliHandle, "\r\n");
            if (CLI_ENABLE == i4FsDot11gSupport)
            {
                CliPrintf (CliHandle, "\r\n11g Support       : Enabled");
            }
            else
            {
                CliPrintf (CliHandle, "\r\n11g Support       : Disabled");
            }

            CliPrintf (CliHandle, "\r\n");
            return CLI_SUCCESS;
        }
    }
    else if (pu1CapwapBaseWtpProfileName != NULL)
    {
        /* When Profile name is received as input get the profile id
         * from mapping table and pass the profile name as NULL */
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1CapwapBaseWtpProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r\nProfile Id not successfullyobtained , (Invalid Input, Check Profile name)\r\n");
            return CLI_FAILURE;
        }

        for (u1RadioIndex = 1;
             u1RadioIndex <= SYS_DEF_MAX_RADIO_INTERFACES; u1RadioIndex++)
        {
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u1RadioIndex, &i4RadioIfIndex) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r\nFor Radio %d : Virtual Radio If Index not successfully obtained,Couldnt proceed ,operation failed for the particular Radio \r\n",
                           u1RadioIndex);
                return CLI_FAILURE;

            }

            WsscfgGetDot11StationConfigEntry.MibObject.i4IfIndex =
                i4RadioIfIndex;
            WsscfgGetDot11PhyTxPowerEntry.MibObject.i4IfIndex = i4RadioIfIndex;
            WsscfgGetDot11OperationEntry.MibObject.i4IfIndex = i4RadioIfIndex;
            WsscfgGetFsDot11nConfigEntry.MibObject.i4IfIndex = i4RadioIfIndex;

            if (WsscfgGetAllDot11StationConfigTable
                (&WsscfgGetDot11StationConfigEntry) != OSIX_SUCCESS)
            {
                /* return CLI_FAILURE; */
            }
            if (WsscfgGetAllDot11OperationTable
                (&WsscfgGetDot11OperationEntry) != OSIX_SUCCESS)
            {
                /* return CLI_FAILURE; */
            }
            if (WsscfgGetAllDot11PhyTxPowerTable
                (&WsscfgGetDot11PhyTxPowerEntry) != OSIX_SUCCESS)
            {
                /* return CLI_FAILURE; */
            }
            if (WsscfgGetAllFsDot11nConfigTable
                (&WsscfgGetFsDot11nConfigEntry) != OSIX_SUCCESS)
            {
                /* return CLI_FAILURE; */
            }
            CliPrintf (CliHandle, "\r\nChannel Width             : %d",
                       WsscfgGetFsDot11nConfigEntry.
                       MibObject.i4FsDot11nConfigChannelWidth);

            if (WsscfgGetFsDot11nConfigEntry.
                MibObject.i4FsDot11nConfigShortGIfor20MHz != 0)
            {
                CliPrintf (CliHandle, "\r\nShort Gi20                : Enable");
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r\nShort Gi20                : Disable");
            }

            if (WsscfgGetFsDot11nConfigEntry.
                MibObject.i4FsDot11nConfigShortGIfor40MHz != 0)
            {
                CliPrintf (CliHandle, "\r\nShort Gi40                : Enable");
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r\nShort Gi40                : Disable");
            }

            for (u1MCSIndex = 0; u1MCSIndex < CLI_MAX_MCS_RATE_INDEX;
                 u1MCSIndex++)
            {
                MEMSET (&WsscfgGetFsDot11nMCSDataRateEntry, 0,
                        sizeof (tWsscfgFsDot11nMCSDataRateEntry));
                WsscfgGetFsDot11nMCSDataRateEntry.
                    MibObject.i4FsDot11nMCSDataRateIndex = u1MCSIndex;
                WsscfgGetFsDot11nMCSDataRateEntry.MibObject.
                    i4IfIndex = i4RadioIfIndex;
                if (WsscfgGetAllFsDot11nMCSDataRateTable
                    (&WsscfgGetFsDot11nMCSDataRateEntry) != OSIX_SUCCESS)
                {
                    /* return CLI_FAILURE; */
                }
                if (WsscfgGetFsDot11nMCSDataRateEntry.
                    MibObject.i4FsDot11nMCSDataRate != 0)
                {
                    CliPrintf (CliHandle,
                               "\r\nMCS Rate Enable Status    : Enable");
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r\nMCS Rate Enable Status    : Disable");
                }
            }
            if (nmhGetIfMainAdminStatus
                (i4RadioIfIndex, &i4AdminStatus) != SNMP_SUCCESS)
            {
            }
            if (i4AdminStatus == CFA_IF_UP)
            {
                CliPrintf (CliHandle, "\r\nAdmin status              : Enable");
            }
            else
            {
                CliPrintf (CliHandle,
                           "\r\nAdmin status              : Disable");
            }
            if (u4FsRadioType == CLI_RADIO_TYPEA)
            {
                WsscfgGetDot11PhyOFDMEntry.MibObject.i4IfIndex = i4RadioIfIndex;
                if (WsscfgGetAllDot11PhyOFDMTable
                    (&WsscfgGetDot11PhyOFDMEntry) != OSIX_SUCCESS)
                {
                }
                CliPrintf (CliHandle,
                           "\r\nCurrent Channel           : %d",
                           WsscfgGetDot11PhyOFDMEntry.
                           MibObject.i4Dot11CurrentFrequency);
            }
            if (u4FsRadioType == CLI_RADIO_TYPEB)
            {
                WsscfgGetDot11PhyDSSSEntry.MibObject.i4IfIndex = i4RadioIfIndex;
                if (WsscfgGetAllDot11PhyDSSSTable
                    (&WsscfgGetDot11PhyDSSSEntry) != OSIX_SUCCESS)
                {
                }
                CliPrintf (CliHandle,
                           "\r\nCurrent Channel           : %d",
                           WsscfgGetDot11PhyDSSSEntry.
                           MibObject.i4Dot11CurrentChannel);
            }
            CliPrintf (CliHandle, "\r\nOperational Rate Set      : %s",
                       WsscfgGetDot11StationConfigEntry.
                       MibObject.au1Dot11OperationalRateSet);
            CliPrintf (CliHandle, "\r\nCountry Name              : %s",
                       WsscfgGetDot11StationConfigEntry.
                       MibObject.au1Dot11CountryString);
            CliPrintf (CliHandle, "\r\nMac Becon Period          : %d",
                       WsscfgGetDot11StationConfigEntry.
                       MibObject.i4Dot11BeaconPeriod);
            CliPrintf (CliHandle, "\r\nMedium Occupancy Limit    : %d",
                       WsscfgGetDot11StationConfigEntry.
                       MibObject.i4Dot11MediumOccupancyLimit);
            CliPrintf (CliHandle, "\r\nCF Pollable               : %d",
                       WsscfgGetDot11StationConfigEntry.
                       MibObject.i4Dot11CFPollable);
            CliPrintf (CliHandle, "\r\nCFP Period                : %d",
                       WsscfgGetDot11StationConfigEntry.
                       MibObject.i4Dot11CFPPeriod);
            CliPrintf (CliHandle, "\r\nCFP Max Duration          : %d",
                       WsscfgGetDot11StationConfigEntry.
                       MibObject.i4Dot11CFPMaxDuration);
            CliPrintf (CliHandle, "\r\nDTIM Period               : %d",
                       WsscfgGetDot11StationConfigEntry.
                       MibObject.i4Dot11DTIMPeriod);
            CliPrintf (CliHandle, "\r\nFragmentation Threshold   : %d",
                       WsscfgGetDot11OperationEntry.
                       MibObject.i4Dot11FragmentationThreshold);
            CliPrintf (CliHandle, "\r\nRTS Threshold             : %d",
                       WsscfgGetDot11OperationEntry.
                       MibObject.i4Dot11RTSThreshold);
            CliPrintf (CliHandle, "\r\nLong Retry Limit          : %d",
                       WsscfgGetDot11OperationEntry.
                       MibObject.i4Dot11LongRetryLimit);
            CliPrintf (CliHandle, "\r\nShort Retry Limit         : %d",
                       WsscfgGetDot11OperationEntry.
                       MibObject.i4Dot11ShortRetryLimit);
            CliPrintf (CliHandle, "\r\nMax Transmit MSDULifetime : %d",
                       WsscfgGetDot11OperationEntry.
                       MibObject.u4Dot11MaxTransmitMSDULifetime);
            CliPrintf (CliHandle, "\r\nMax Receive Lifetime      : %d",
                       WsscfgGetDot11OperationEntry.
                       MibObject.u4Dot11MaxReceiveLifetime);
            CliPrintf (CliHandle,
                       "\r\nCurrent Tx Power Level    : %d\n\r",
                       WsscfgGetDot11PhyTxPowerEntry.
                       MibObject.i4Dot11CurrentTxPowerLevel);
        }

    }
    UNUSED_PARAM (pi4Dot11CurrentChannel);
    UNUSED_PARAM (pi4Dot11TxPowerLevel1);
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WssCfgShowApConfiguration 
* Description :
* Input       :  CliHandle 
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowApConfiguration (tCliHandle CliHandle,
                           UINT4 u4FsRadioType,
                           UINT1 *pu1CapwapBaseWtpProfileName, UINT4 u4RadioId)
{
    UINT1               au1MacAddr[20];
    UINT1               au1WtpName[256];
    tMacAddr            MacAddr;
    UINT4               u4IpAddr = 0;
    INT4                i4AddressType = 0;
    CHR1               *pu1IpString = NULL;
    UINT1               au1IpId[4];

    tSNMP_OCTET_STRING_TYPE CapwapBaseWtpProfileWtpMacAddress;
    /* tSNMP_OCTET_STRING_TYPE *pCapwapBaseWtpProfileWtpMacAddress; */
    /* pCapwapBaseWtpProfileWtpMacAddress = &CapwapBaseWtpProfileWtpMacAddress;
     * */

    tSNMP_OCTET_STRING_TYPE WtpProfileName;
    tSNMP_OCTET_STRING_TYPE IpAddress;

    tSNMP_OCTET_STRING_TYPE Dot11CountryString;
    tSNMP_OCTET_STRING_TYPE *pDot11CountryString;
    pDot11CountryString = &Dot11CountryString;

    INT4                i4Dot11BeaconPeriod = 0;
    INT4               *pi4Dot11BeaconPeriod = NULL;
    pi4Dot11BeaconPeriod = &i4Dot11BeaconPeriod;

    INT4                i4Dot11CFPollable = 0;
    INT4               *pi4Dot11CFPollable = NULL;
    pi4Dot11CFPollable = &i4Dot11CFPollable;

    INT4                i4Dot11CFPPeriod = 0;
    INT4               *pi4Dot11CFPPeriod = NULL;
    pi4Dot11CFPPeriod = &i4Dot11CFPPeriod;

    INT4                i4Dot11CFPMaxDuration = 0;
    INT4               *pi4Dot11CFPMaxDuration = NULL;
    pi4Dot11CFPMaxDuration = &i4Dot11CFPMaxDuration;

    INT4                i4Dot11TxPowerLevel1 = 0;
    INT4               *pi4Dot11TxPowerLevel1 = NULL;
    pi4Dot11TxPowerLevel1 = &i4Dot11TxPowerLevel1;

    INT4                i4Dot11TxPowerLevel2 = 0;
    INT4               *pi4Dot11TxPowerLevel2 = NULL;
    pi4Dot11TxPowerLevel2 = &i4Dot11TxPowerLevel2;

    INT4                i4Dot11TxPowerLevel3 = 0;
    INT4               *pi4Dot11TxPowerLevel3 = NULL;
    pi4Dot11TxPowerLevel3 = &i4Dot11TxPowerLevel3;

    INT4                i4Dot11TxPowerLevel4 = 0;
    INT4               *pi4Dot11TxPowerLevel4 = NULL;
    pi4Dot11TxPowerLevel4 = &i4Dot11TxPowerLevel4;

    INT4                i4Dot11TxPowerLevel5 = 0;
    INT4               *pi4Dot11TxPowerLevel5 = NULL;
    pi4Dot11TxPowerLevel5 = &i4Dot11TxPowerLevel5;

    INT4                i4Dot11TxPowerLevel6 = 0;
    INT4               *pi4Dot11TxPowerLevel6 = NULL;
    pi4Dot11TxPowerLevel6 = &i4Dot11TxPowerLevel6;

    INT4                i4Dot11TxPowerLevel7 = 0;
    INT4               *pi4Dot11TxPowerLevel7 = NULL;
    pi4Dot11TxPowerLevel7 = &i4Dot11TxPowerLevel7;

    INT4                i4Dot11TxPowerLevel8 = 0;
    INT4               *pi4Dot11TxPowerLevel8 = NULL;
    pi4Dot11TxPowerLevel8 = &i4Dot11TxPowerLevel8;

    INT4                i4Dot11CurrentTxPowerLevel = 0;
    INT4               *pi4Dot11CurrentTxPowerLevel = NULL;
    pi4Dot11CurrentTxPowerLevel = &i4Dot11CurrentTxPowerLevel;

    INT4                i4Dot11FragmentationThreshold = 0;
    INT4               *pi4Dot11FragmentationThreshold = NULL;
    pi4Dot11FragmentationThreshold = &i4Dot11FragmentationThreshold;

    INT4                i4Dot11TIThreshold = 0;
    INT4               *pi4Dot11TIThreshold = NULL;
    pi4Dot11TIThreshold = &i4Dot11TIThreshold;

    INT4                i4Dot11MaxTransmitMSDULifetime = 0;
    INT4               *pi4Dot11MaxTransmitMSDULifetime = NULL;
    pi4Dot11MaxTransmitMSDULifetime = &i4Dot11MaxTransmitMSDULifetime;

    INT4                i4Dot11RTSThreshold = 0;
    INT4               *pi4Dot11RTSThreshold = NULL;
    pi4Dot11RTSThreshold = &i4Dot11RTSThreshold;

    INT4                i4Dot11LongRetryLimit = 0;
    INT4               *pi4Dot11LongRetryLimit = NULL;
    pi4Dot11LongRetryLimit = &i4Dot11LongRetryLimit;

    INT4                i4Dot11ShortRetryLimit = 0;
    INT4               *pi4Dot11ShortRetryLimit = NULL;
    pi4Dot11ShortRetryLimit = &i4Dot11ShortRetryLimit;

    UINT4               u4CapwapBaseWtpProfileId = 0;
    tWsscfgDot11PhyOFDMEntry WsscfgGetDot11PhyOFDMEntry;
    tWsscfgFsRrmConfigEntry WsscfgGetFsRrmConfigEntry;
    tWsscfgDot11PhyTxPowerEntry WsscfgGetDot11PhyTxPowerEntry;
    tWsscfgDot11OperationEntry WsscfgGetDot11OperationEntry;
    tWsscfgFsDot11WlanCapabilityProfileEntry
        WsscfgGetFsDot11WlanCapabilityProfileEntry;
    tWsscfgDot11AntennasListEntry WsscfgGetDot11AntennasListEntry;
    tWsscfgFsDot11AntennasListEntry WsscfgGetFsDot11AntennasListEntry;
    tWsscfgDot11StationConfigEntry WsscfgGetDot11StationConfigEntry;
    tRadioIfGetDB       RadioIfGetDB;
    tWsscfgDot11PhyDSSSEntry WsscfgGetDot11PhyDSSSEntry;

    MEMSET (&WsscfgGetDot11PhyOFDMEntry, 0, sizeof (tWsscfgDot11PhyOFDMEntry));
    MEMSET (&WsscfgGetFsRrmConfigEntry, 0, sizeof (tWsscfgFsRrmConfigEntry));
    MEMSET (&WsscfgGetDot11PhyTxPowerEntry, 0,
            sizeof (tWsscfgDot11PhyTxPowerEntry));
    MEMSET (&WsscfgGetDot11OperationEntry, 0,
            sizeof (tWsscfgDot11OperationEntry));
    MEMSET (&WsscfgGetFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));
    MEMSET (&WsscfgGetDot11AntennasListEntry, 0,
            sizeof (tWsscfgDot11AntennasListEntry));
    MEMSET (&WsscfgGetFsDot11AntennasListEntry, 0,
            sizeof (tWsscfgFsDot11AntennasListEntry));
    MEMSET (&WsscfgGetDot11StationConfigEntry, 0,
            sizeof (tWsscfgDot11StationConfigEntry));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1WtpName, 0, 256);
    MEMSET (&IpAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1IpId, 0, 4);
    MEMSET (&WsscfgGetDot11PhyDSSSEntry, 0, sizeof (tWsscfgDot11PhyDSSSEntry));

    IpAddress.pu1_OctetList = (UINT1 *) &u4IpAddr;

    UINT4               u4IfIndex = 0;
    INT4                i4AdminStatus = 0;

    MEMSET (MacAddr, 0, sizeof (tMacAddr));
    MEMSET (&CapwapBaseWtpProfileWtpMacAddress, 0,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    CapwapBaseWtpProfileWtpMacAddress.pu1_OctetList = au1MacAddr;

    if (pu1CapwapBaseWtpProfileName != NULL)
    {

        if (CapwapGetWtpProfileIdFromProfileName
            (pu1CapwapBaseWtpProfileName,
             &u4CapwapBaseWtpProfileId) != OSIX_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    WtpProfileName.pu1_OctetList = au1WtpName;
    if (nmhGetCapwapBaseWtpProfileName
        (u4CapwapBaseWtpProfileId, &WtpProfileName) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\r\nAP Name                   : %s",
               WtpProfileName.pu1_OctetList);
    CliPrintf (CliHandle, "\r\nAP Identifier             : %d",
               u4CapwapBaseWtpProfileId);

    if (nmhGetCapwapBaseWtpProfileWtpStaticIpEnable
        (u4CapwapBaseWtpProfileId, &i4AddressType) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nIP Address Type           : STATIC");
        if (nmhGetCapwapBaseWtpProfileWtpStaticIpAddress
            (u4CapwapBaseWtpProfileId, &IpAddress) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r\nIP Address Type           : DHCP");
    }
    CLI_CONVERT_IPADDR_TO_STR (pu1IpString, u4IpAddr);
    CliPrintf (CliHandle, "\r\nIP Address                : %s", pu1IpString);
    if (nmhGetCapwapBaseWtpProfileWtpMacAddress
        (u4CapwapBaseWtpProfileId,
         &CapwapBaseWtpProfileWtpMacAddress) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nMac Address               : %s",
               CapwapBaseWtpProfileWtpMacAddress.pu1_OctetList);

    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
        (u4CapwapBaseWtpProfileId, u4RadioId,
         (INT4 *) &u4IfIndex) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    WsscfgGetDot11StationConfigEntry.MibObject.i4IfIndex = u4IfIndex;
    WsscfgGetDot11PhyTxPowerEntry.MibObject.i4IfIndex = u4IfIndex;
    WsscfgGetDot11OperationEntry.MibObject.i4IfIndex = u4IfIndex;

    if (WsscfgGetAllDot11StationConfigTable
        (&WsscfgGetDot11StationConfigEntry) != OSIX_SUCCESS)
    {
        /* return CLI_FAILURE; */
    }
    if (WsscfgGetAllDot11OperationTable (&WsscfgGetDot11OperationEntry)
        != OSIX_SUCCESS)
    {
        /* return CLI_FAILURE; */
    }
    if (WsscfgGetAllDot11PhyTxPowerTable
        (&WsscfgGetDot11PhyTxPowerEntry) != OSIX_SUCCESS)
    {
        /* return CLI_FAILURE; */
    }
    if (nmhGetIfMainAdminStatus (u4IfIndex, &i4AdminStatus) != SNMP_SUCCESS)
    {
    }
    if (i4AdminStatus == CFA_IF_UP)
    {
        CliPrintf (CliHandle, "\r\nAdmin status              : Enable");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nAdmin status              : Disable");
    }
    if (u4FsRadioType == CLI_RADIO_TYPEA)
    {
        WsscfgGetDot11PhyOFDMEntry.MibObject.i4IfIndex = u4IfIndex;
        if (WsscfgGetAllDot11PhyOFDMTable (&WsscfgGetDot11PhyOFDMEntry)
            != OSIX_SUCCESS)
        {
        }
        CliPrintf (CliHandle, "\r\nCurrent Channel           : %d",
                   WsscfgGetDot11PhyOFDMEntry.
                   MibObject.i4Dot11CurrentFrequency);
        CliPrintf (CliHandle, "\r\ndot11T1Thresold           : %d",
                   WsscfgGetDot11PhyOFDMEntry.MibObject.i4Dot11TIThreshold);
    }
    if (u4FsRadioType == CLI_RADIO_TYPEB)
    {
        WsscfgGetDot11PhyDSSSEntry.MibObject.i4IfIndex = u4IfIndex;
        if (WsscfgGetAllDot11PhyDSSSTable (&WsscfgGetDot11PhyDSSSEntry)
            != OSIX_SUCCESS)
        {
        }
        CliPrintf (CliHandle, "\r\nCurrent Channel           : %d",
                   WsscfgGetDot11PhyDSSSEntry.MibObject.i4Dot11CurrentChannel);
        CliPrintf (CliHandle, "dot11EDThreshold              : %d",
                   WsscfgGetDot11PhyDSSSEntry.MibObject.i4Dot11EDThreshold);
    }
    /*CliPrintf (CliHandle, "\r\nOperational Rate Set      : %s",
       WsscfgGetDot11StationConfigEntry.MibObject.au1Dot11OperationalRateSet); */
    CliPrintf (CliHandle, "\r\nCountry Name              : %s",
               WsscfgGetDot11StationConfigEntry.
               MibObject.au1Dot11CountryString);
    CliPrintf (CliHandle, "\r\nMac Becon Period          : %d",
               WsscfgGetDot11StationConfigEntry.MibObject.i4Dot11BeaconPeriod);
    CliPrintf (CliHandle, "\r\nMedium Occupancy Limit    : %d",
               WsscfgGetDot11StationConfigEntry.
               MibObject.i4Dot11MediumOccupancyLimit);
    CliPrintf (CliHandle, "\r\nCF Pollable               : %d",
               WsscfgGetDot11StationConfigEntry.MibObject.i4Dot11CFPollable);
    CliPrintf (CliHandle, "\r\nCFP Period                : %d",
               WsscfgGetDot11StationConfigEntry.MibObject.i4Dot11CFPPeriod);
    CliPrintf (CliHandle, "\r\nCFP Max Duration          : %d",
               WsscfgGetDot11StationConfigEntry.
               MibObject.i4Dot11CFPMaxDuration);
    CliPrintf (CliHandle, "\r\nDTIM Period               : %d",
               WsscfgGetDot11StationConfigEntry.MibObject.i4Dot11DTIMPeriod);
    CliPrintf (CliHandle, "\r\nFragmentation Threshold   : %d",
               WsscfgGetDot11OperationEntry.
               MibObject.i4Dot11FragmentationThreshold);
    CliPrintf (CliHandle, "\r\nRTS Threshold             : %d",
               WsscfgGetDot11OperationEntry.MibObject.i4Dot11RTSThreshold);
    CliPrintf (CliHandle, "\r\nLong Retry Limit          : %d",
               WsscfgGetDot11OperationEntry.MibObject.i4Dot11LongRetryLimit);
    CliPrintf (CliHandle, "\r\nShort Retry Limit         : %d",
               WsscfgGetDot11OperationEntry.MibObject.i4Dot11ShortRetryLimit);
    CliPrintf (CliHandle, "\r\nMax Transmit MSDULifetime : %d",
               WsscfgGetDot11OperationEntry.
               MibObject.u4Dot11MaxTransmitMSDULifetime);
    CliPrintf (CliHandle, "\r\nMax Receive Lifetime      : %d",
               WsscfgGetDot11OperationEntry.
               MibObject.u4Dot11MaxReceiveLifetime);
    CliPrintf (CliHandle, "\r\nCurrent Tx Power Level    : %d",
               WsscfgGetDot11PhyTxPowerEntry.
               MibObject.i4Dot11CurrentTxPowerLevel);

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WssCfgShowApWlanConfig 
* Description :
* Input       :  CliHandle 
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowApWlanConfig (tCliHandle CliHandle,
                        UINT4 *pu4FsRadioType,
                        UINT1 *pu1CapwapBaseWtpProfileName, UINT4 *pu4RadioId)
{
#ifdef WLC_WANTED
    UINT1               u1Count = 0;
    UINT1               au1ModelNumber[256];
    UINT1               u1WlanId = 0;
    UINT4               u4RadioId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4GetRadioType = 0;
    UINT4               u4NoOfRadio = 0;
    UINT1               u1BssIdCount = 0;
    UINT4               u4BssIfIndex = 0;
    INT4                i4RadioIfIndex = 0;
    tMacAddr            BssId;
    UINT1               au1SSID[32];
    UINT1               au1MacAddrBssid[32];
    UINT1              *pu1MacAddrBssId = NULL;
    UINT1              *pu1BssId = NULL;
    tSNMP_OCTET_STRING_TYPE MacAddressBssId;
    tSNMP_OCTET_STRING_TYPE ModelNumber;
#endif
    UINT1               u1WlanId = 0;
    UINT1               au1MacAddrBssid[32];
#ifdef WTP_WANTED
    UINT1               u1StartRadioId = 0, u1NoRadio = 0, u1RadioId = 0;
    tWssWlanDB         *pWssWlanDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

#endif
#ifdef WLC_WANTED
    MEMSET (BssId, 0, sizeof (tMacAddr));
    MEMSET (au1SSID, 0, sizeof (au1SSID));
    MEMSET (&au1ModelNumber, 0, sizeof (au1ModelNumber));
    MEMSET (&MacAddressBssId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ModelNumber, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1MacAddrBssid, 0, 32);

    pu1BssId = BssId;
    pu1MacAddrBssId = au1MacAddrBssid;

    if (pu1CapwapBaseWtpProfileName != NULL)
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1CapwapBaseWtpProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n No entry found\r\n");
            return OSIX_FAILURE;
        }

        ModelNumber.pu1_OctetList = au1ModelNumber;
        if (nmhGetCapwapBaseWtpProfileWtpModelNumber
            (u4WtpProfileId, &ModelNumber) == SNMP_SUCCESS)
        {
            if (nmhGetFsNoOfRadio (&ModelNumber, &u4NoOfRadio) != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\n Radio Type not configured\r\n");
                return CLI_FAILURE;
            }
            if (*pu4RadioId == 0)
            {
                for (u4RadioId = 1; u4RadioId <= u4NoOfRadio; u4RadioId++)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4WtpProfileId, u4RadioId,
                         &i4RadioIfIndex) != SNMP_SUCCESS)
                    {
                        continue;
                    }
                    if (nmhGetFsDot11RadioType
                        (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                    {
                        continue;
                    }

                    if (u4GetRadioType == *(INT4 *) pu4FsRadioType)
                    {
                        if (nmhGetFsDot11RadioNoOfBssIdSupported
                            (i4RadioIfIndex, &u1BssIdCount) != SNMP_SUCCESS)
                        {
                            continue;
                        }

                        WssWlanGetDot11BssIdCount ((UINT4)
                                                   i4RadioIfIndex,
                                                   &u1BssIdCount);

                        CliPrintf (CliHandle,
                                   "\r\nWlan Id    BSSID              SSID ");
                        CliPrintf (CliHandle, "\r\n");
                        CliPrintf (CliHandle,
                                   "-------    ------             ---- ");

                        for (u1WlanId = 1; u1WlanId <= u1BssIdCount; u1WlanId++)
                        {

                            WssWlanGetDot11BssIfIndex ((UINT4)
                                                       i4RadioIfIndex,
                                                       u1WlanId, &u4BssIfIndex);
                            WssWlanGetDot11BssId (u4BssIfIndex, pu1BssId);
                            PrintMacAddress (pu1BssId, pu1MacAddrBssId);
                            WssWlanGetDot11SSID (u4BssIfIndex, au1SSID);
                            CliPrintf (CliHandle, "\r\n");
                            CliPrintf (CliHandle, "%-10d", u1WlanId);
                            CliPrintf (CliHandle, "%-20s", au1MacAddrBssid);
                            CliPrintf (CliHandle, "%-32s", au1SSID);
                            CliPrintf (CliHandle, "\r\n");
                            u1Count++;
                        }
                        if (u1Count == 0)
                        {
                            CliPrintf (CliHandle, "\r\n No entry found\r\n");
                        }
                    }
                }
            }
            else
            {
                if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                    (u4WtpProfileId, *pu4RadioId,
                     &i4RadioIfIndex) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Invalid interface index %d \r\n",
                               *pu4RadioId);
                    return CLI_SUCCESS;
                }
                if (nmhGetFsDot11RadioType
                    (i4RadioIfIndex, &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_SUCCESS;
                }

                if (u4GetRadioType != *(INT4 *) pu4FsRadioType)
                {
                    CliPrintf (CliHandle,
                               "\r\n No binding entry found for the radio type\r\n");
                    return CLI_SUCCESS;
                }
                else
                {
                    if (nmhGetFsDot11RadioNoOfBssIdSupported
                        (i4RadioIfIndex, &u1BssIdCount) != SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "\r\n Access to DB failed \r\n");
                        return CLI_FAILURE;
                    }

                    WssWlanGetDot11BssIdCount ((UINT4) i4RadioIfIndex,
                                               &u1BssIdCount);

                    CliPrintf (CliHandle,
                               "\r\nWlan Id    BSSID              SSID ");
                    CliPrintf (CliHandle, "\r\n");
                    CliPrintf (CliHandle,
                               "-------    ------             ---- ");
                    CliPrintf (CliHandle, "\r\n");
                    for (u1WlanId = 1; u1WlanId <= u1BssIdCount; u1WlanId++)
                    {

                        WssWlanGetDot11BssIfIndex ((UINT4)
                                                   i4RadioIfIndex,
                                                   u1WlanId, &u4BssIfIndex);
                        WssWlanGetDot11BssId (u4BssIfIndex, pu1BssId);
                        PrintMacAddress (pu1BssId, au1MacAddrBssid);
                        MEMSET (au1SSID, 0, sizeof (au1SSID));
                        WssWlanGetDot11SSID (u4BssIfIndex, au1SSID);

                        CliPrintf (CliHandle, "\r\n");
                        CliPrintf (CliHandle, "%-10d", u1WlanId);
                        CliPrintf (CliHandle, "%-20s", au1MacAddrBssid);
                        CliPrintf (CliHandle, "%-32s", au1SSID);
                        CliPrintf (CliHandle, "\r\n");
                        u1Count++;
                    }
                    if (u1Count == 0)
                    {
                        CliPrintf (CliHandle, "\r\n No entry found\r\n");
                    }
                }
            }
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r\n AP Name missing\r\n");
        return CLI_FAILURE;
    }
#endif
#ifdef WTP_WANTED
    if (*pu4RadioId == 0)
    {
        u1StartRadioId = 1;
        u1NoRadio = SYS_DEF_MAX_RADIO_INTERFACES;
    }
    else
    {
        u1StartRadioId = *pu4RadioId;
        u1NoRadio = *pu4RadioId;
    }

    for (u1RadioId = u1StartRadioId; u1RadioId <= u1NoRadio; u1RadioId++)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            u1RadioId + SYS_DEF_MAX_ENET_INTERFACES;
        for (u1WlanId = WSSWLAN_START_WLANID_PER_RADIO;
             u1WlanId <= WSSWLAN_END_WLANID_PER_RADIO; u1WlanId++)
        {
            RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1WlanId;
            if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                          &RadioIfGetDB) != OSIX_SUCCESS)
            {
                continue;
            }
            pWssWlanDB->WssWlanAttributeDB.u1RadioId = u1RadioId;
            pWssWlanDB->WssWlanAttributeDB.u1WlanId = u1WlanId;
            pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                continue;
            }
            pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bBssId = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bDesiredSsid = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                continue;
            }

            PrintMacAddress (pWssWlanDB->WssWlanAttributeDB.BssId,
                             au1MacAddrBssid);
            CliPrintf (CliHandle, "\r\nWlan Id    BSSID              SSID ");
            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle, "-------    ------             ---- ");
            CliPrintf (CliHandle, "\r\n");

            CliPrintf (CliHandle, "%-10d", u1WlanId);
            CliPrintf (CliHandle, "%-20s", au1MacAddrBssid);
            CliPrintf (CliHandle, "%-32s",
                       pWssWlanDB->WssWlanAttributeDB.au1DesiredSsid);
            CliPrintf (CliHandle, "\r\n");

        }
    }
    UNUSED_PARAM (pu4FsRadioType);
    UNUSED_PARAM (pu1CapwapBaseWtpProfileName);
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
#endif
    return CLI_SUCCESS;
}

INT4
WssCfgShowClientSummary (tCliHandle CliHandle)
{

#ifdef WLC_WANTED
    UINT1               u1Index = 0;
    tWssWlanDB          wssWlanDB;
    tRadioIfGetDB       radioIfgetDB;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4RadioType = 0;
    UINT2               u2WlanProfileId = 0;
    UINT1               au1MacAddr[21];
    tWssStaStateDB      WssStaStateDB;

    CliPrintf (CliHandle,
               "\r\nMac Address            AP Name     WLAN   Auth    Protocol ");
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle,
               "------------           -------     ----   ----    -------- ");
    CliPrintf (CliHandle, "\r\n");

    WssStaShowClient ();
    for (u1Index = 0; u1Index < gu4ClientWalkIndex; u1Index++)
    {
        WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
            MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));
        MEMSET (&radioIfgetDB, 0, sizeof (tRadioIfGetDB));
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
        /*Coverity Change Begins */
        /*MEMSET (&au1MacAddr, 0, 17); */
        MEMSET (&au1MacAddr, 0, sizeof (au1MacAddr));
        MEMSET (&WssStaStateDB, 0, sizeof (tWssStaStateDB));

        MEMCPY (WssStaStateDB.stationMacAddress,
                gaWssStaWtpDBWalk[u1Index].staMacAddr, sizeof (tMacAddr));
        PrintMacAddress (&(gaWssStaWtpDBWalk[u1Index].staMacAddr), au1MacAddr);
        CliPrintf (CliHandle, "\r\n%-24s", au1MacAddr);

        if (WssStaGetStationDB (&WssStaStateDB) != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return CLI_FAILURE;
        }
        radioIfgetDB.RadioIfGetAllDB.pu1AntennaSelection =
            UtlShMemAllocAntennaSelectionBuf ();

        if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return CLI_FAILURE;
        }

        radioIfgetDB.RadioIfGetAllDB.u4RadioIfIndex =
            WssStaStateDB.u1RadioId + SYS_DEF_MAX_ENET_INTERFACES;
        radioIfgetDB.RadioIfIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        radioIfgetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &radioIfgetDB))
        {
            UtlShMemFreeAntennaSelectionBuf (radioIfgetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return CLI_FAILURE;
        }
        u4RadioType = radioIfgetDB.RadioIfGetAllDB.u4Dot11RadioType;
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId =
            radioIfgetDB.RadioIfGetAllDB.u2WtpInternalId;
        UtlShMemFreeAntennaSelectionBuf (radioIfgetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB) !=
            OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return CLI_FAILURE;
        }
        wssWlanDB.WssWlanAttributeDB.u1RadioId = WssStaStateDB.u1RadioId;
        wssWlanDB.WssWlanAttributeDB.u1WlanId = WssStaStateDB.u1WlanId;
        wssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY,
                                      &wssWlanDB) != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return CLI_FAILURE;
        }
        wssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
        wssWlanDB.WssWlanIsPresentDB.bAdminStatus = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                      &wssWlanDB) != OSIX_SUCCESS)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return CLI_FAILURE;
        }

        CliPrintf (CliHandle, "%-12s",
                   pWssIfCapwapDB->CapwapGetDB.au1ProfileName);
        CliPrintf (CliHandle, "%-7d", WssStaStateDB.u1WlanId);
        if (CLI_WLAN_ADMIN_ENABLE == wssWlanDB.WssWlanAttributeDB.u1AdminStatus)
            CliPrintf (CliHandle, "%s", "Enabled");
        else
            CliPrintf (CliHandle, "%s", "Disabled");

        if (u4RadioType == CLI_RADIO_TYPEA)
        {
            CliPrintf (CliHandle, "%-17s", "802.11a");
        }
        else if (u4RadioType == CLI_RADIO_TYPEB)
        {
            CliPrintf (CliHandle, "%-17s", "802.11b");
        }
        else if (u4RadioType == CLI_RADIO_TYPEG)
        {
            CliPrintf (CliHandle, "%-17s", "802.11g");
        }
        CliPrintf (CliHandle, "\r\n");
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    }
#else
    UNUSED_PARAM (CliHandle);
#endif
    return CLI_SUCCESS;
}

INT4
WssCfgShowWebAuthConfig (tCliHandle CliHandle)
{
    INT4                i4FsSecurityWebAuthType = 0;
    INT4                i4FsSecurityWebAuthDisplay = 0;
    INT4                i4FsSecurityWebAuthColor = 0;
    INT4                i4FsSecurityWebAddr = 0;
    tSNMP_OCTET_STRING_TYPE WebAuthUrl;
    tSNMP_OCTET_STRING_TYPE WebAuthWebTitle;
    tSNMP_OCTET_STRING_TYPE WebAuthWebMessage;
    tSNMP_OCTET_STRING_TYPE WebAuthWebSuccMessage;
    tSNMP_OCTET_STRING_TYPE WebAuthWebFailMessage;
    tSNMP_OCTET_STRING_TYPE WebAuthWebButtonText;
    tSNMP_OCTET_STRING_TYPE WebAuthLoadBalInfo;
    tSNMP_OCTET_STRING_TYPE WebAuthLogoFileName;

    UINT1               au1FsSecurityWebAuthUrl[256];
    UINT1               au1FsSecurityWebAuthWebTitle[256];
    UINT1               au1FsSecurityWebAuthWebMessage[256];
    UINT1               au1FsSecurityWebAuthWebSuccMessage[256];
    UINT1               au1FsSecurityWebAuthWebFailMessage[256];
    UINT1               au1FsSecurityWebAuthWebButtonText[256];
    UINT1               au1FsSecurityWebAuthLoadBalInfo[256];
    UINT1               au1FsSecurityWebAuthLogoFileName[256];
    UINT1               au1WebAuthType[256];
    UINT1               au1WebAuthColor[256];
    UINT1               au1WebAuthLang[256];

    MEMSET (&au1FsSecurityWebAuthUrl, 0, sizeof (au1FsSecurityWebAuthUrl));
    MEMSET (&au1FsSecurityWebAuthWebTitle, 0,
            sizeof (au1FsSecurityWebAuthWebTitle));
    MEMSET (&au1FsSecurityWebAuthWebMessage, 0,
            sizeof (au1FsSecurityWebAuthWebMessage));
    MEMSET (&au1FsSecurityWebAuthWebSuccMessage, 0,
            sizeof (au1FsSecurityWebAuthWebSuccMessage));
    MEMSET (&au1FsSecurityWebAuthWebFailMessage, 0,
            sizeof (au1FsSecurityWebAuthWebFailMessage));
    MEMSET (&au1FsSecurityWebAuthWebButtonText, 0,
            sizeof (au1FsSecurityWebAuthWebButtonText));
    MEMSET (&au1FsSecurityWebAuthLoadBalInfo, 0,
            sizeof (au1FsSecurityWebAuthLoadBalInfo));
    MEMSET (&au1FsSecurityWebAuthLogoFileName, 0,
            sizeof (au1FsSecurityWebAuthLogoFileName));

    MEMSET (&WebAuthUrl, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WebAuthWebTitle, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WebAuthWebMessage, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WebAuthWebSuccMessage, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WebAuthWebFailMessage, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WebAuthWebButtonText, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WebAuthLoadBalInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WebAuthLogoFileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (&au1WebAuthType, 0, sizeof (au1WebAuthType));
    MEMSET (&au1WebAuthColor, 0, sizeof (au1WebAuthColor));
    MEMSET (&au1WebAuthLang, 0, sizeof (au1WebAuthLang));

    if (nmhGetFsSecurityWebAuthType (&i4FsSecurityWebAuthType) != SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (i4FsSecurityWebAuthType == WEB_AUTH_TYPE_EXTERNAL)
    {
        WebAuthUrl.pu1_OctetList = au1FsSecurityWebAuthUrl;

        if (nmhGetFsSecurityWebAuthUrl (&WebAuthUrl) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        if (nmhGetFsSecurityWebAddr (&i4FsSecurityWebAddr) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        CliPrintf (CliHandle, "\r\nWebAuth Type: %d\r\n",
                   i4FsSecurityWebAuthType);
        CliPrintf (CliHandle, "\r\nWebAuth URl: %s\r\n",
                   WebAuthUrl.pu1_OctetList);
        CliPrintf (CliHandle, "\r\nWebAddress: %d\r\n", i4FsSecurityWebAddr);
    }
    else
    {
        if (nmhGetFsSecurityWebAuthDisplayLang
            (&i4FsSecurityWebAuthDisplay) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        if (nmhGetFsSecurityWebAuthColor (&i4FsSecurityWebAuthColor) !=
            SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        WebAuthWebTitle.pu1_OctetList = au1FsSecurityWebAuthWebTitle;

        if (nmhGetFsSecurityWebAuthWebTitle (&WebAuthWebTitle) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        WebAuthWebMessage.pu1_OctetList = au1FsSecurityWebAuthWebMessage;

        if (nmhGetFsSecurityWebAuthWebMessage (&WebAuthWebMessage) !=
            SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        WebAuthWebSuccMessage.pu1_OctetList =
            au1FsSecurityWebAuthWebSuccMessage;

        if (nmhGetFsSecurityWebAuthWebSuccMessage
            (&WebAuthWebSuccMessage) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        WebAuthWebFailMessage.pu1_OctetList =
            au1FsSecurityWebAuthWebFailMessage;

        if (nmhGetFsSecurityWebAuthWebFailMessage
            (&WebAuthWebFailMessage) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        WebAuthWebButtonText.pu1_OctetList = au1FsSecurityWebAuthWebButtonText;

        if (nmhGetFsSecurityWebAuthWebButtonText
            (&WebAuthWebButtonText) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        WebAuthLoadBalInfo.pu1_OctetList = au1FsSecurityWebAuthLoadBalInfo;

        if (nmhGetFsSecurityWebAuthWebLoadBalInfo (&WebAuthLoadBalInfo)
            != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        WebAuthLogoFileName.pu1_OctetList = au1FsSecurityWebAuthLogoFileName;

        if (nmhGetFsSecurityWebAuthWebLogoFileName
            (&WebAuthLogoFileName) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        if ((i4FsSecurityWebAuthType == 0) ||
            (i4FsSecurityWebAuthType == WEB_AUTH_TYPE_INTERNAL))
        {
            STRCPY (au1WebAuthType, "Internal");
        }
        if (i4FsSecurityWebAuthType == WEB_AUTH_TYPE_CUSTOMIZE)
        {
            STRCPY (au1WebAuthType, "Internal Customizable");
        }

        if (i4FsSecurityWebAuthDisplay == CLI_WEBAUTH_LANG_ENGLISH)
            STRCPY (au1WebAuthLang, "English");
        else if (i4FsSecurityWebAuthDisplay == CLI_WEBAUTH_LANG_CHINESE)
            STRCPY (au1WebAuthLang, "Chinese");
        else if (i4FsSecurityWebAuthDisplay == CLI_WEBAUTH_LANG_JAPANESE)
            STRCPY (au1WebAuthLang, "Japanese");
        else if (i4FsSecurityWebAuthDisplay == CLI_WEBAUTH_LANG_FRENCH)
            STRCPY (au1WebAuthLang, "French");
        else if (i4FsSecurityWebAuthDisplay == CLI_WEBAUTH_LANG_RUSSIAN)
            STRCPY (au1WebAuthLang, "Russian");

        if (i4FsSecurityWebAuthColor == CLI_WEBAUTH_COLOR_ORANGE)
            STRCPY (au1WebAuthColor, "Orange");
        else if (i4FsSecurityWebAuthColor == CLI_WEBAUTH_COLOR_GREY)
            STRCPY (au1WebAuthColor, "Grey");
        else if (i4FsSecurityWebAuthColor == CLI_WEBAUTH_COLOR_YELLOW)
            STRCPY (au1WebAuthColor, "Yellow");

        CliPrintf (CliHandle, "\r\nWebAuth Type: %s\r\n", au1WebAuthType);
        CliPrintf (CliHandle, "\r\nWebTitle: %s\r\n",
                   WebAuthWebTitle.pu1_OctetList);
        CliPrintf (CliHandle, "\r\nWebAuth WebMessage: %s\r\n",
                   WebAuthWebMessage.pu1_OctetList);
        CliPrintf (CliHandle, "\r\nWebAuth Logo filename: %s\r\n",
                   WebAuthLogoFileName.pu1_OctetList);
        CliPrintf (CliHandle, "\r\nWebAuth Success Message: %s\r\n",
                   WebAuthWebSuccMessage.pu1_OctetList);
        CliPrintf (CliHandle, "\r\nWebAuth Failure Message: %s\r\n",
                   WebAuthWebFailMessage.pu1_OctetList);
        CliPrintf (CliHandle, "\r\nWebAuth Button Text: %s\r\n",
                   WebAuthWebButtonText.pu1_OctetList);
        CliPrintf (CliHandle, "\r\nWebAuth Load Bal Info: %s\r\n",
                   WebAuthLoadBalInfo.pu1_OctetList);
        CliPrintf (CliHandle, "\r\nWebAuth Display Lang: %s\r\n",
                   au1WebAuthLang);
        CliPrintf (CliHandle, "\r\nWebAuth Color:%s\r\n", au1WebAuthColor);
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  WssCfgShowNetUserSummary 
* Description :
* Input       :  CliHandle 
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowNetUserSummary (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE NextWebAuthUName;
    tSNMP_OCTET_STRING_TYPE WebAuthUName;
    tSNMP_OCTET_STRING_TYPE WebAuthUserEmailId;

    UINT1               au1FsSecurityWebAuthUName[WSS_STA_WEBUSER_LEN_MAX];
    UINT1               au1NextFsSecurityWebAuthUName[WSS_STA_WEBUSER_LEN_MAX];
    UINT1               au1FsSecurityWebAuthUserEmailId[256];
    INT4                i4RetValFsSecurityWlanProfileId = 0;
    INT4                i4RetValFsSecurityWebAuthUserLifetime = 0;

    MEMSET (&au1FsSecurityWebAuthUName, 0, sizeof (au1FsSecurityWebAuthUName));
    MEMSET (&au1NextFsSecurityWebAuthUName, 0,
            sizeof (au1NextFsSecurityWebAuthUName));
    MEMSET (&au1FsSecurityWebAuthUserEmailId, 0,
            sizeof (au1FsSecurityWebAuthUserEmailId));

    MEMSET (&WebAuthUName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextWebAuthUName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WebAuthUserEmailId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    WebAuthUName.pu1_OctetList = au1FsSecurityWebAuthUName;
    NextWebAuthUName.pu1_OctetList = au1NextFsSecurityWebAuthUName;
    WebAuthUserEmailId.pu1_OctetList = au1FsSecurityWebAuthUserEmailId;

    if (nmhGetFirstIndexFsSecurityWebAuthGuestInfoTable
        (&NextWebAuthUName) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\n\r\n\rEntry does not exist \n\r\n\r");
        return OSIX_FAILURE;
    }
    CliPrintf (CliHandle, "\r\nName        WLANID        Lifetime\n\r");
    do
    {
        WebAuthUName = NextWebAuthUName;
        if (nmhGetFsSecurityWlanProfileId
            (&WebAuthUName, &i4RetValFsSecurityWlanProfileId) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\n\r\n\rUnable to get WlanProfile Id \n\r\n\r");
            return OSIX_FAILURE;
        }
        if (nmhGetFsSecurityWebAuthUserLifetime
            (&WebAuthUName,
             &i4RetValFsSecurityWebAuthUserLifetime) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\n\r\n\rUnable to get Lifetime\n\r\n\r");
            return OSIX_FAILURE;
        }
        CliPrintf (CliHandle, "\r\n%s\t\t%d\t\t%d\n\r",
                   WebAuthUName.pu1_OctetList,
                   i4RetValFsSecurityWlanProfileId,
                   i4RetValFsSecurityWebAuthUserLifetime);
    }
    while ((nmhGetNextIndexFsSecurityWebAuthGuestInfoTable
            (&WebAuthUName, &NextWebAuthUName)));

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WssCfgShowNetworkSummary  
* Description :
* Input       :  CliHandle 
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowNetworkSummary (tCliHandle CliHandle, UINT4 *pu4FsRadioType)
{
    tSNMP_OCTET_STRING_TYPE WtpProfileName;
    tSNMP_OCTET_STRING_TYPE WtpModelNumber;
    tSNMP_OCTET_STRING_TYPE MacAddress;

    tMacAddr            MacAddr;
    UINT1               au1MacAddress[21];
    UINT1               au1WtpName[256];
    tMacAddr            au1MacAddr;
    UINT4               u4RadioIfType = 0;
    INT4                i4RadioIfIndex = 0;
    INT4                i4AdminStatus = 0;
    INT4                i4OperStatus = 0;
    UINT4               currentProfileId = 0, nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;
    UINT1              *pu1MacAddr = NULL;
    INT4                i4Dot11TxPowerLevel1 = 0;
    INT4                i4Dot11CurrentChannel = 0;
    UINT1               au1WtpModel[256];

    MEMSET (&WtpProfileName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&WtpModelNumber, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&MacAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1MacAddr, 0, sizeof (tMacAddr));
    MEMSET (au1MacAddress, 0, sizeof (UINT1));
    MEMSET (MacAddr, 0, sizeof (tMacAddr));
    MEMSET (au1WtpModel, 0, 256);
    MEMSET (au1WtpName, 0, 256);

    pu1MacAddr = au1MacAddr;
    WtpProfileName.pu1_OctetList = au1WtpName;
    WtpModelNumber.pu1_OctetList = au1WtpModel;
    MacAddress.pu1_OctetList = au1MacAddress;

    if (pu4FsRadioType == NULL)
    {
        CliPrintf (CliHandle, "\r\n Radio Type missing \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return OSIX_FAILURE;
    }

    if (nmhGetFirstIndexFsCapwapWirelessBindingTable
        (&nextProfileId, &u4nextBindingId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\n\r\n\rEntry does not exist \n\r\n\r");
        return OSIX_FAILURE;
    }

    CliPrintf (CliHandle,
               "\r\nAP Name     Radio Id     MAC Address       Admin State   Operational State");
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle,
               "-------     --------     -----------       -----------   -----------------  ");
    CliPrintf (CliHandle, "\r\nChannel   TxPower");
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, "-------     --------");

    CliPrintf (CliHandle, "\r\n");

    do
    {
        currentProfileId = nextProfileId;
        u4currentBindingId = u4nextBindingId;

        if ((currentProfileId == 0) || (u4currentBindingId == 0))
        {
            return OSIX_FAILURE;
        }

        if (nmhGetCapwapBaseWtpProfileName
            (currentProfileId, &WtpProfileName) != SNMP_SUCCESS)
        {
            continue;
        }
        if (nmhGetCapwapBaseWtpProfileWtpMacAddress
            (currentProfileId, &MacAddress) != SNMP_SUCCESS)
        {
            continue;
        }

        if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
            (currentProfileId, u4nextBindingId,
             &i4RadioIfIndex) != SNMP_SUCCESS)
        {
            continue;
        }

        if (nmhGetFsDot11RadioType (i4RadioIfIndex, &u4RadioIfType) !=
            SNMP_FAILURE)
        {
            if (u4RadioIfType == *pu4FsRadioType)
            {
                if (*pu4FsRadioType == CLI_RADIO_TYPEA)
                {
                    if (nmhGetDot11CurrentFrequency
                        (i4RadioIfIndex,
                         (UINT4 *) &i4Dot11CurrentChannel) != SNMP_SUCCESS)
                    {
                        continue;
                    }
                }
                else if (*pu4FsRadioType == CLI_RADIO_TYPEB)
                {
                    if (nmhGetDot11CurrentChannel
                        (i4RadioIfIndex,
                         (UINT4 *) &i4Dot11CurrentChannel) != SNMP_SUCCESS)
                    {
                        continue;
                    }
                }
                if (nmhGetDot11CurrentTxPowerLevel
                    (i4RadioIfIndex,
                     (UINT4 *) &i4Dot11TxPowerLevel1) != SNMP_SUCCESS)
                {
                    continue;
                }
            }
            else
            {
                continue;
            }
        }
        else
        {
            continue;
        }
        if (nmhGetIfMainAdminStatus (i4RadioIfIndex, &i4AdminStatus) !=
            SNMP_SUCCESS)
        {
            continue;
        }

        if (nmhGetIfMainOperStatus (i4RadioIfIndex, &i4OperStatus) !=
            SNMP_SUCCESS)
        {
            continue;
        }

        if (u4RadioIfType == *pu4FsRadioType)
        {
            CliPrintf (CliHandle, "\r\n");

            CliPrintf (CliHandle, "%-15s", WtpProfileName.pu1_OctetList);
            CliPrintf (CliHandle, "%-10d", SYS_DEF_MAX_RADIO_INTERFACES);
            /* PrintMacAddress(MacAddress.pu1_OctetList, pu1MacAddr); */

            CliPrintf (CliHandle, "%-20s", MacAddress.pu1_OctetList);

            if (i4AdminStatus == 1)
            {
                CliPrintf (CliHandle, "%-17s", "Enabled");
            }
            else
            {
                CliPrintf (CliHandle, "%-17s", "Disabled");
            }

            if (i4OperStatus == 1)
            {
                CliPrintf (CliHandle, "%-16s", "Up");
            }
            else
            {
                CliPrintf (CliHandle, "%-16s", "Down");
            }

            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle, "%-15d", i4Dot11CurrentChannel);
            CliPrintf (CliHandle, "%-15d", i4Dot11TxPowerLevel1);
            CliPrintf (CliHandle, "\r\n");

        }
    }
    while (nmhGetNextIndexFsCapwapWirelessBindingTable
           (currentProfileId, &nextProfileId, u4currentBindingId,
            &u4nextBindingId) == SNMP_SUCCESS);

    UNUSED_PARAM (pu1MacAddr);
    return CLI_SUCCESS;
}

/****************************************************************************
  * Function    : WssCfgShowCountrySupported
  * Description :
  * Input       :  CliHandle
  *
  *
  * Output      :  None
  * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowCountrySupported (tCliHandle CliHandle)
{
    CliPrintf (CliHandle, "\r\nSupported Country Codes\n");
    CliPrintf (CliHandle,
               "\r\nCN  - China.....................................            802.11a / 802.11b / 802.11g\n");
    CliPrintf (CliHandle,
               "\r\nSG  - Singapore.................................            802.11a / 802.11b / 802.11g\n");
    CliPrintf (CliHandle,
               "\r\nGB  - United Kingdom............................            802.11a / 802.11b / 802.11g\n");
    CliPrintf (CliHandle,
               "\r\nIN  - India.....................................            802.11a / 802.11b / 802.11g\n");
    CliPrintf (CliHandle,
               "\r\nUS  - United States.............................            802.11a / 802.11b / 802.11g\n");
    CliPrintf (CliHandle,
               "\r\nJP  - Japan.....................................            802.11a / 802.11b / 802.11g\n");

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WssCfgShowWlanConfig  
* Description :
* Input       :  CliHandle 
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowWlanConfig (tCliHandle CliHandle, UINT4 u4WlanProfileId)
{
    tBitList            u1array;
    UINT1               u1RadioId = 0;
    tWssWlanDB          WssWlanDB;
    MEMSET (&u1array, 0, 16);
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));

    for (u1RadioId = RADIOIF_START_RADIOID;
         u1RadioId <= SYS_DEF_MAX_RADIO_INTERFACES; u1RadioId++)
    {
        WssWlanDB.WssWlanAttributeDB.u1RadioId = u1RadioId;
        WssWlanDB.WssWlanAttributeDB.u1WlanId = (UINT1) u4WlanProfileId;
        WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY,
                                      &WssWlanDB) != OSIX_SUCCESS)
        {
            continue;
        }
        WssWlanDB.WssWlanIsPresentDB.bMacType = OSIX_TRUE;
        WssWlanDB.WssWlanIsPresentDB.bTunnelMode = OSIX_TRUE;
        WssWlanDB.WssWlanIsPresentDB.bDesiredSsid = OSIX_TRUE;
        WssWlanDB.WssWlanIsPresentDB.bAdminStatus = OSIX_TRUE;
        WssWlanDB.WssWlanIsPresentDB.bSupressSsid = OSIX_TRUE;
        WssWlanDB.WssWlanIsPresentDB.bVlanId = OSIX_TRUE;
        WssWlanDB.WssWlanIsPresentDB.bSsidIsolation = OSIX_TRUE;
        WssWlanDB.WssWlanIsPresentDB.bQosRateLimit = OSIX_TRUE;
        WssWlanDB.WssWlanIsPresentDB.bQosUpStreamCIR = OSIX_TRUE;
        WssWlanDB.WssWlanIsPresentDB.bQosUpStreamCBS = OSIX_TRUE;
        WssWlanDB.WssWlanIsPresentDB.bQosUpStreamEIR = OSIX_TRUE;
        WssWlanDB.WssWlanIsPresentDB.bQosUpStreamEBS = OSIX_TRUE;
        WssWlanDB.WssWlanIsPresentDB.bQosDownStreamCIR = OSIX_TRUE;
        WssWlanDB.WssWlanIsPresentDB.bQosDownStreamCBS = OSIX_TRUE;
        WssWlanDB.WssWlanIsPresentDB.bQosDownStreamEIR = OSIX_TRUE;
        WssWlanDB.WssWlanIsPresentDB.bQosDownStreamEBS = OSIX_TRUE;
        WssWlanDB.WssWlanIsPresentDB.bAuthMethod = OSIX_TRUE;

        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                      &WssWlanDB) != OSIX_SUCCESS)
        {
            continue;
        }

        CliPrintf (CliHandle, "\r\nWLAN Identifier             : %d",
                   u4WlanProfileId);

        if (WssWlanDB.WssWlanAttributeDB.u1MacType == CLI_WTP_MAC_TYPE_SPLIT)
        {
            CliPrintf (CliHandle, "\r\nMac Type                    : Split");
        }
        else if (WssWlanDB.WssWlanAttributeDB.u1MacType ==
                 CLI_WTP_MAC_TYPE_LOCAL)
        {
            CliPrintf (CliHandle, "\r\nMac Type                    : Local");
        }

        if (WssWlanDB.WssWlanAttributeDB.u1TunnelMode ==
            CLI_WLAN_TUNNEL_MODE_BRIDGE)
        {
            CliPrintf (CliHandle, "\r\nTunnel Mode                 : 802.3");
        }
        else if (WssWlanDB.WssWlanAttributeDB.u1TunnelMode ==
                 CLI_WLAN_TUNNEL_MODE_DOT3)
        {
            CliPrintf (CliHandle, "\r\nTunnel Mode                 : 802.3");
        }
        else if (WssWlanDB.WssWlanAttributeDB.u1TunnelMode ==
                 CLI_WLAN_TUNNEL_MODE_NATIVE)
        {
            CliPrintf (CliHandle, "\r\nTunnel Mode                 : Native");
        }

        CliPrintf (CliHandle, "\r\nSSID Name                   : %s",
                   WssWlanDB.WssWlanAttributeDB.au1DesiredSsid);

        if (WssWlanDB.WssWlanAttributeDB.u1AdminStatus == CLI_WLAN_ADMIN_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nAdmin State                 : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nAdmin State                 : Disabled");
        }

        if (WssWlanDB.WssWlanAttributeDB.u1SupressSsid ==
            CLI_WLAN_HIDE_SSID_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nSupress SSID                : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nSupress SSID                : Disabled");
        }

        if (WssWlanDB.WssWlanAttributeDB.u1SsidIsolation == CLI_WLAN_ENABLE)
        {
            CliPrintf (CliHandle, "\r\nSsid Isolation              : Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r\nSsid Isolation              : Disabled");
        }

        if (WssWlanDB.WssWlanAttributeDB.u1AuthMethod == CLI_AUTH_ALGO_OPEN)
        {
            CliPrintf (CliHandle,
                       "\r\nAuthentication Algorithm    : Open System");
        }
        else if (WssWlanDB.WssWlanAttributeDB.u1AuthMethod ==
                 CLI_AUTH_ALGO_SHARED)
        {
            CliPrintf (CliHandle, "\r\nAuthentication Algorithm  :"
                       "Static WEP (Shared Key)");
            CliPrintf (CliHandle, "\r\nKey Index                 : %d",
                       WssWlanDB.WssWlanAttributeDB.u1KeyIndex);
            if (WssWlanDB.WssWlanAttributeDB.u1KeyType == CLI_WEP_KEY_TYPE_HEX)
                CliPrintf (CliHandle, "\r\nWeb Authentication         : Hex");
            else
                CliPrintf (CliHandle, "\r\nWeb Authentication         : Ascii");
            CliPrintf (CliHandle, "\r\nKey :                      : %s",
                       WssWlanDB.WssWlanAttributeDB.au1WepKey);
            CliPrintf (CliHandle, "\r\nKey Length                 : %d",
                       STRLEN (WssWlanDB.WssWlanAttributeDB.au1WepKey));
        }
        else
        {
            CliPrintf (CliHandle, "\r\nAuthentication Algorithm    : Invalid");
        }

        if ((WssWlanDB.WssWlanAttributeDB.u2Capability & 0x0001) == 0x0001)
            CliPrintf (CliHandle, "\r\nImmediate Block ACK          : Enabled");
        else
            CliPrintf (CliHandle,
                       "\r\nImmediate Block ACK          : Disabled");
        if ((WssWlanDB.WssWlanAttributeDB.u2Capability & 0x0002) == 0x0002)
            CliPrintf (CliHandle, "\r\nDelayed Block ACK            : Enabled");
        else
            CliPrintf (CliHandle,
                       "\r\nDelayed Block ACK            : Disabled");
        if ((WssWlanDB.WssWlanAttributeDB.u2Capability & 0x0004) == 0x0004)
            CliPrintf (CliHandle, "\r\nDSSS-OFDM                    : Enabled");
        else
            CliPrintf (CliHandle,
                       "\r\nDSSS-OFDM                    : Disabled");
        if ((WssWlanDB.WssWlanAttributeDB.u2Capability & 0x0010) == 0x0010)
            CliPrintf (CliHandle, "\r\nAPSD                         : Enabled");
        else
            CliPrintf (CliHandle,
                       "\r\nAPSD                         : Disabled");
        if ((WssWlanDB.WssWlanAttributeDB.u2Capability & 0x0020) == 0x0020)
            CliPrintf (CliHandle, "\r\nShort Slot Time              : Enabled");
        else
            CliPrintf (CliHandle,
                       "\r\nShort Slot Time              : Disabled");
        if ((WssWlanDB.WssWlanAttributeDB.u2Capability & 0x0040) == 0x0040)
            CliPrintf (CliHandle, "\r\nQoS                          : Enabled");
        else
            CliPrintf (CliHandle,
                       "\r\nQoS                          : Disabled");
        if ((WssWlanDB.WssWlanAttributeDB.u2Capability & 0x0080) == 0x0080)
            CliPrintf (CliHandle, "\r\nSpectrum Management          : Enabled");
        else
            CliPrintf (CliHandle,
                       "\r\nSpectrum Management          : Disabled");
        if ((WssWlanDB.WssWlanAttributeDB.u2Capability & 0x0100) == 0x0100)
            CliPrintf (CliHandle, "\r\nChannel Agility              : Enabled");
        else
            CliPrintf (CliHandle,
                       "\r\nChannel Agility              : Disabled");
        if ((WssWlanDB.WssWlanAttributeDB.u2Capability & 0x0200) == 0x0200)
            CliPrintf (CliHandle, "\r\nPBCC                         : Enabled");
        else
            CliPrintf (CliHandle,
                       "\r\nPBCC                         : Disabled");
        if ((WssWlanDB.WssWlanAttributeDB.u2Capability & 0x0400) == 0x0400)
            CliPrintf (CliHandle, "\r\nShort Preamble               : Enabled");
        else
            CliPrintf (CliHandle,
                       "\r\nShort Preamble               : Disabled");
        if ((WssWlanDB.WssWlanAttributeDB.u2Capability & 0x0800) == 0x0800)
            CliPrintf (CliHandle, "\r\nPrivacy                      : Enabled");
        else
            CliPrintf (CliHandle,
                       "\r\nPrivacy                      : Disabled");
        if ((WssWlanDB.WssWlanAttributeDB.u2Capability & 0x1000) == 0x1000)
            CliPrintf (CliHandle, "\r\nCF-Poll Request              : Enabled");
        else
            CliPrintf (CliHandle,
                       "\r\nCF-Poll Request              : Disabled");
        if ((WssWlanDB.WssWlanAttributeDB.u2Capability & 0x2000) == 0x2000)
            CliPrintf (CliHandle, "\r\nCF-Pollable                  : Enabled");
        else
            CliPrintf (CliHandle,
                       "\r\nCF-Pollable                  : Disabled");
        CliPrintf (CliHandle, "\r\n");
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    : WssCfgShowWlanSummary  
* Description :
* Input       :  CliHandle 
*            
*            
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowWlanSummary (tCliHandle CliHandle)
{
    UINT1               u1WlanId = 0, u1RadioId = 0;
    tWssWlanDB         *pWssWlanDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    for (u1RadioId = RADIOIF_START_RADIOID;
         u1RadioId <= SYS_DEF_MAX_RADIO_INTERFACES; u1RadioId++)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            u1RadioId + SYS_DEF_MAX_ENET_INTERFACES;
        for (u1WlanId = WSSWLAN_START_WLANID_PER_RADIO;
             u1WlanId <= WSSWLAN_END_WLANID_PER_RADIO; u1WlanId++)
        {
            RadioIfGetDB.RadioIfGetAllDB.u1WlanId = u1WlanId;
            if (WssIfProcessRadioIfDBMsg (WSS_GET_WLAN_BSS_IFINDEX_DB,
                                          &RadioIfGetDB) != OSIX_SUCCESS)
            {
                continue;
            }
            pWssWlanDB->WssWlanAttributeDB.u1RadioId = u1RadioId;
            pWssWlanDB->WssWlanAttributeDB.u1WlanId = u1WlanId;
            pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                continue;
            }
            pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bDesiredSsid = OSIX_TRUE;
            pWssWlanDB->WssWlanIsPresentDB.bAdminStatus = OSIX_TRUE;
            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                          pWssWlanDB) != OSIX_SUCCESS)
            {
                continue;
            }

            CliPrintf (CliHandle, "\r\nWlan Id    SSID         Status ");
            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle, "-------    ------       ------ ");
            CliPrintf (CliHandle, "\r\n");

            CliPrintf (CliHandle, "%-11d", u1WlanId);
            CliPrintf (CliHandle, "%-12s",
                       pWssWlanDB->WssWlanAttributeDB.au1DesiredSsid);
            if (CLI_WLAN_ADMIN_ENABLE ==
                pWssWlanDB->WssWlanAttributeDB.u1AdminStatus)
                CliPrintf (CliHandle, "%s", "Enabled");
            else
                CliPrintf (CliHandle, "%s", "Disabled");
            CliPrintf (CliHandle, "\r\n");

        }
    }
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    return CLI_SUCCESS;
}

/****************************************************************************
  * Function    : WssCfgShowWlanStatistics
  * Description :
  * Input       :  CliHandle
  *
  * Output      :  None
  * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WssCfgShowWlanStatistics (tCliHandle CliHandle,
                          UINT4 *pu4CurrentCapwapDot11WlanProfileId)
{
    INT4                i4BssIntfIndex = 0;
    UINT1               u1RadioId = 0;
    UINT4               u4GetFsWlanBeaconsSentCount = 0,
        u4GetFsWlanProbeReqRcvdCount = 0,
        u4GetFsWlanProbeRespSentCount = 0,
        u4GetFsWlanDataPktRcvdCount = 0, u4GetFsWlanDataPktSentCount = 0;
    tWssWlanDB          WssWlanDB;

    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));

    CliPrintf (CliHandle,
               "\r\nWlanId  BeaconSent ProbeReqRcvd ProbeRespSent DataRcvd DataSent");
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle,
               "------  ---------- ------------ ------------- -------- -------- ");
    CliPrintf (CliHandle, "\r\n");

    for (u1RadioId = RADIOIF_START_RADIOID;
         u1RadioId <= SYS_DEF_MAX_RADIO_INTERFACES; u1RadioId++)
    {
        WssWlanDB.WssWlanAttributeDB.u1RadioId = u1RadioId;
        WssWlanDB.WssWlanAttributeDB.u1WlanId =
            *pu4CurrentCapwapDot11WlanProfileId;
        WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
        if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_INTERFACE_ENTRY,
                                      &WssWlanDB) == OSIX_SUCCESS)
        {
            i4BssIntfIndex = WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex;
            if (nmhGetFsWlanBeaconsSentCount (i4BssIntfIndex,
                                              &u4GetFsWlanBeaconsSentCount) !=
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhGetFsWlanProbeReqRcvdCount (i4BssIntfIndex,
                                               &u4GetFsWlanProbeReqRcvdCount) !=
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhGetFsWlanProbeRespSentCount (i4BssIntfIndex,
                                                &u4GetFsWlanProbeRespSentCount)
                != SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhGetFsWlanDataPktRcvdCount (i4BssIntfIndex,
                                              &u4GetFsWlanDataPktRcvdCount) !=
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhGetFsWlanDataPktSentCount (i4BssIntfIndex,
                                              &u4GetFsWlanDataPktSentCount) !=
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle, "\r\n  %-8d",
                       *pu4CurrentCapwapDot11WlanProfileId);
            /* CliPrintf (CliHandle, "%-10d",i4GetFsWlanBeaconsSentCount); */
            CliPrintf (CliHandle, "Not Supported\t ");
            CliPrintf (CliHandle, "%-13u", u4GetFsWlanProbeReqRcvdCount);
            CliPrintf (CliHandle, "%-14u", u4GetFsWlanProbeRespSentCount);
            CliPrintf (CliHandle, "%-8u", u4GetFsWlanDataPktRcvdCount);
            CliPrintf (CliHandle, "%-10u", u4GetFsWlanDataPktSentCount);
            CliPrintf (CliHandle, "\r\n");
        }
    }
    return CLI_SUCCESS;
}

#endif
