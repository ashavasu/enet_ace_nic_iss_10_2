/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: fs1accli.c,v 1.2 2017/05/23 14:16:55 siva Exp $
 *
 * Description: This file contains the routines for fs11ac module 
 *         related cli commands. 
 *                            
 ********************************************************************/
#ifndef __FS11ACCLI_C__
#define __FS11ACCLI_C__

#include "wsscfginc.h"
#include "wssifpmdb.h"
#include "wsscfgcli.h"
#include "capwapcli.h"
#include "wsspm.h"
#include "wsscfgprot.h"
#include "fpam.h"
#include "fs11aclw.h"

/****************************************************************************
 * Function    :  cli_process_fs11ac_cmd
 * Description :  This function is exported to CLI module to handle the
                  fs11ac cli commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
cli_process_fs11ac_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[WSSCFG_CLI_MAX_ARGS];
    INT1                i1argno = 0;
    INT1                i1RetStatus = CLI_SUCCESS;
    INT4                i4Inst = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4ErrCode = 0;
    CliRegisterLock (CliHandle, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    va_start (ap, u4Command);

    /* Walk through the rest of the arguements and store in args array. 
     * Store RFMGMT_CLI_MAX_ARGS arguements at the max. 
     */

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }

    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == WSSCFG_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    switch (u4Command)
    {
        case CLI_FS11AC_SET_MAXMPDULENGTH:
            i1RetStatus =
                Fs11acCliSetMaxMPDULength (CliHandle, (UINT4) *args[0],
                                           *args[1], (UINT1 *) args[2],
                                           args[3]);
            break;
        case CLI_FS11AC_SET_MAXAMPDUFACTOR:
            i1RetStatus = Fs11acCliSetMaxAMPDUFactor (CliHandle,
                                                      (UINT4) *args[0],
                                                      *args[1],
                                                      (UINT1 *) args[2],
                                                      args[3]);

            break;
        case CLI_FS11AC_SET_CHANNELWIDTH:
            i1RetStatus = Fs11acCliSetChannelWidth (CliHandle, (UINT4) *args[0],
                                                    *args[1],
                                                    (UINT1 *) args[2], args[3]);

            break;
        case CLI_FS11AC_SET_CHANNELCENTERINDEX0:
            i1RetStatus = Fs11acCliSetChannelCenterIndex0 (CliHandle,
                                                           (UINT4) *args[0],
                                                           *args[1],
                                                           (UINT1 *) args[2],
                                                           args[3]);

            break;
        case CLI_FS11AC_SET_SHORTGI80:
            i1RetStatus = Fs11acCliSetShortGi80 (CliHandle, (UINT4) *args[0],
                                                 *args[1],
                                                 (UINT1 *) args[2], args[3]);

            break;
        case CLI_FS11AC_SET_LDPCOPTION:
            i1RetStatus = Fs11acCliSetLdpcOption (CliHandle, (UINT4) *args[0],
                                                  *args[1],
                                                  (UINT1 *) args[2], args[3]);

            break;
        case CLI_FS11AC_SET_RXMCSMAP:
            i1RetStatus = Fs11acCliSetRxMcsMap (CliHandle, (UINT4) *args[0],
                                                *args[1],
                                                (UINT1 *) args[2],
                                                args[3], *args[4]);

            break;
        case CLI_FS11AC_SET_TXMCSMAP:
            i1RetStatus = Fs11acCliSetTxMcsMap (CliHandle, (UINT4) *args[0],
                                                *args[1],
                                                (UINT1 *) args[2],
                                                args[3], *args[4]);

            break;
        case CLI_FS11AC_SHOW:
            i1RetStatus = Fs11acCliShow (CliHandle, (UINT4) *args[0],
                                         (UINT1 *) args[2], args[3]);
            break;

        case CLI_FS11AC_SET_RX_STBC:
            i1RetStatus = Fs11acCliSetRxStbc (CliHandle, (UINT4) *args[0],
                                              *args[1],
                                              (UINT1 *) args[2], args[3]);
            break;

        case CLI_FS11AC_SET_TX_STBC:
            i1RetStatus = Fs11acCliSetTxStbc (CliHandle, (UINT4) *args[0],
                                              *args[1],
                                              (UINT1 *) args[2], args[3]);
            break;

        case CLI_FS11AC_SET_STS:
            i1RetStatus = Fs11acCliSetSTS (CliHandle, (UINT4) *args[0],
                                           *args[1],
                                           (UINT1 *) args[2], args[3]);
            break;

        default:
            break;
    }
    if ((i1RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_WSSCFG_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", WsscfgCliErrString[u4ErrCode]);
            CLI_FATAL_ERROR (CliHandle);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i1RetStatus);

    CliUnRegisterLock (CliHandle);
    WSSCFG_UNLOCK;
    UNUSED_PARAM (u4IfIndex);
    return i1RetStatus;
}

/****************************************************************************
 * Function    :  Fs11acCliSetMaxMPDULength
 * Description :  This function is exported to CLI module to set the 
 *           MPDU length.

 * Input       :  CliHandle
            u4RadioType
            u4MaxMPDULength
            pu1ProfileName
            pu4RadioId

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/

INT1
Fs11acCliSetMaxMPDULength (tCliHandle CliHandle, UINT4 u4RadioType,
                           UINT4 u4MaxMPDULength, UINT1 *pu1ProfileName,
                           UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    /*If no Profile name is provided AP MPDU Length is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId, u4currentBindingId,
                 &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (nmhTestv2FsDot11ACMaxMPDULengthConfig (&u4ErrCode,
                                                           i4RadioIfIndex,
                                                           (INT4)
                                                           u4MaxMPDULength) ==
                    SNMP_FAILURE)
                {
                    continue;
                }
                nmhSetFsDot11ACMaxMPDULengthConfig (i4RadioIfIndex,
                                                    (INT4) u4MaxMPDULength);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11AC_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }

        /*If AP name is provided and not the radio id, all radio id's of the
         *          * AP is updated with the AP MPDU Length. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId, &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {

                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId, u4currentBindingId,
                         &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioType) != OSIX_SUCCESS)
                        {
                            if (u4RadioType == RADIO_TYPE_AC)
                            {
                                if (nmhTestv2FsDot11ACMaxMPDULengthConfig
                                    (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4MaxMPDULength) == SNMP_FAILURE)
                                {
                                    continue;
                                }

                                nmhSetFsDot11ACMaxMPDULengthConfig
                                    (i4RadioIfIndex, (INT4) u4MaxMPDULength);
                            }
                        }
                    }
                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

        }
        /*Only for the particular radio id of the AP, MPDU Length is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (nmhTestv2FsDot11ACMaxMPDULengthConfig (&u4ErrCode,
                                                           i4RadioIfIndex,
                                                           (INT4)
                                                           u4MaxMPDULength) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11ACMaxMPDULengthConfig (i4RadioIfIndex,
                                                    (INT4) u4MaxMPDULength);
            }
            else
            {
                CLI_SET_ERR (CLI_11AC_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Fs11acCliSetMaxAMPDUFactor
 * Description :  This function is exported to CLI module to set the 
 *           maximum A-MPDU factor

 * Input       :  CliHandle
            u4RadioType
            u4MaxAMPDUFactor
            pu1ProfileName
            pu4RadioId

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT1
Fs11acCliSetMaxAMPDUFactor (tCliHandle CliHandle, UINT4 u4RadioType,
                            UINT4 u4MaxAMPDUFactor, UINT1 *pu1ProfileName,
                            UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    /*If no Profile name is provided AP A-MPDU Factor is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId, u4currentBindingId,
                 &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (nmhTestv2FsDot11ACVHTMaxRxAMPDUFactorConfig (&u4ErrCode,
                                                                 i4RadioIfIndex,
                                                                 u4MaxAMPDUFactor)
                    == SNMP_FAILURE)
                {
                    continue;
                }
                nmhSetFsDot11ACVHTMaxRxAMPDUFactorConfig (i4RadioIfIndex,
                                                          u4MaxAMPDUFactor);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11AC_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }

        /*If AP name is provided and not the radio id, all radio id's of the
         *          * AP is updated with the AP A-MPDU Factor. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId, &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)

            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {

                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId, u4currentBindingId,
                         &i4RadioIfIndex) == SNMP_SUCCESS)

                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioType) != OSIX_SUCCESS)
                        {
                            if (u4RadioType == RADIO_TYPE_AC)
                            {

                                if (nmhTestv2FsDot11ACVHTMaxRxAMPDUFactorConfig
                                    (&u4ErrCode, i4RadioIfIndex,
                                     u4MaxAMPDUFactor) == SNMP_FAILURE)
                                {
                                    continue;
                                }
                                nmhSetFsDot11ACVHTMaxRxAMPDUFactorConfig
                                    (i4RadioIfIndex, u4MaxAMPDUFactor);
                            }
                        }
                    }
                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

        }
        /*Only for the particular radio id of the AP, A-MPDU Factor is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhTestv2FsDot11ACVHTMaxRxAMPDUFactorConfig (&u4ErrCode,
                                                                 i4RadioIfIndex,
                                                                 u4MaxAMPDUFactor)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11ACVHTMaxRxAMPDUFactorConfig (i4RadioIfIndex,
                                                          u4MaxAMPDUFactor);
            }
            else
            {
                CLI_SET_ERR (CLI_11AC_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Fs11acCliSetChannelWidth
 * Description :  This function is exported to CLI module to set the 
 *           Channel Width needed.

 * Input       :  CliHandle
            u4RadioType
            u4ChannelWidth
            pu1ProfileName
            pu4RadioId

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT1
Fs11acCliSetChannelWidth (tCliHandle CliHandle, UINT4 u4RadioType,
                          UINT4 u4ChannelWidth, UINT1 *pu1ProfileName,
                          UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    /*If no Profile name is provided AP Channel Width is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId, u4currentBindingId,
                 &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (nmhTestv2FsDot11ACCurrentChannelBandwidthConfig (&u4ErrCode,
                                                                     i4RadioIfIndex,
                                                                     (INT4)
                                                                     u4ChannelWidth)
                    == SNMP_FAILURE)
                {
                    continue;
                }
                nmhSetFsDot11ACCurrentChannelBandwidthConfig (i4RadioIfIndex,
                                                              (INT4)
                                                              u4ChannelWidth);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11AC_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }

        /*If AP name is provided and not the radio id, all radio id's of the
         *          * AP is updated with the AP Channel Width. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId, &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)

            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {

                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId, u4currentBindingId,
                         &i4RadioIfIndex) == SNMP_SUCCESS)

                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioType) != OSIX_SUCCESS)
                        {
                            if (u4RadioType == RADIO_TYPE_AC)
                            {

                                if (nmhTestv2FsDot11ACCurrentChannelBandwidthConfig (&u4ErrCode, i4RadioIfIndex, (INT4) u4ChannelWidth) == SNMP_FAILURE)
                                {
                                    continue;
                                }
                                nmhSetFsDot11ACCurrentChannelBandwidthConfig
                                    (i4RadioIfIndex, (INT4) u4ChannelWidth);
                            }
                        }
                    }
                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

        }
        /*Only for the particular radio id of the AP, Channel Width is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (nmhTestv2FsDot11ACCurrentChannelBandwidthConfig (&u4ErrCode,
                                                                     i4RadioIfIndex,
                                                                     (INT4)
                                                                     u4ChannelWidth)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11ACCurrentChannelBandwidthConfig (i4RadioIfIndex,
                                                              (INT4)
                                                              u4ChannelWidth);
            }
            else
            {
                CLI_SET_ERR (CLI_11AC_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Fs11acCliSetChannelCenterIndex0
 * Description :  This function is exported to CLI module to set the 
           Channel Center Frequency for the Index 0

 * Input       :  Variable arguments

 * Output      :  CliHandle
            u4RadioType
            u4ChannelIndex
            pu1ProfileName
            pu4RadioId

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT1
Fs11acCliSetChannelCenterIndex0 (tCliHandle CliHandle, UINT4 u4RadioType,
                                 UINT4 u4ChannelIndex, UINT1 *pu1ProfileName,
                                 UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    /*If no Profile name is provided AP Center Frequency index0 is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId, u4currentBindingId,
                 &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (nmhTestv2FsDot11ACCurrentChannelCenterFrequencyIndex0Config
                    (&u4ErrCode, i4RadioIfIndex,
                     u4ChannelIndex) == SNMP_FAILURE)
                {
                    continue;
                }
                nmhSetFsDot11ACCurrentChannelCenterFrequencyIndex0Config
                    (i4RadioIfIndex, u4ChannelIndex);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11AC_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }

        /*If AP name is provided and not the radio id, all radio id's of the
         *          * AP is updated with the AP Center Frequency index0. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId, &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {

                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId, u4currentBindingId,
                         &i4RadioIfIndex) == SNMP_SUCCESS)

                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioType) != OSIX_SUCCESS)
                        {
                            if (u4RadioType == RADIO_TYPE_AC)
                            {

                                if (nmhTestv2FsDot11ACCurrentChannelCenterFrequencyIndex0Config (&u4ErrCode, i4RadioIfIndex, u4ChannelIndex) == SNMP_FAILURE)
                                {
                                    continue;
                                }
                                nmhSetFsDot11ACCurrentChannelCenterFrequencyIndex0Config
                                    (i4RadioIfIndex, u4ChannelIndex);
                            }
                        }
                    }
                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

        }
        /*Only for the particular radio id of the AP, Center Frequency index0 is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (nmhTestv2FsDot11ACCurrentChannelCenterFrequencyIndex0Config
                    (&u4ErrCode, i4RadioIfIndex, u4ChannelIndex)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11ACCurrentChannelCenterFrequencyIndex0Config
                    (i4RadioIfIndex, u4ChannelIndex);
            }
            else
            {
                CLI_SET_ERR (CLI_11AC_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Fs11acCliSetShortGi80
 * Description :  This function is exported to CLI module to enable or disable
           the Short Guard interval.

 * Input       :  CliHandle
            u4RadioType
            u4ShortGI
            pu1ProfileName
            pu4RadioId

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT1
Fs11acCliSetShortGi80 (tCliHandle CliHandle, UINT4 u4RadioType,
                       UINT4 u4ShortGI, UINT1 *pu1ProfileName,
                       UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    /*If no Profile name is provided AP Short Gi status is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId, u4currentBindingId,
                 &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (nmhTestv2FsDot11ACVHTShortGIOptionIn80Activated (&u4ErrCode,
                                                                     i4RadioIfIndex,
                                                                     (INT4)
                                                                     u4ShortGI)
                    == SNMP_FAILURE)
                {
                    continue;
                }
                nmhSetFsDot11ACVHTShortGIOptionIn80Activated (i4RadioIfIndex,
                                                              (INT4) u4ShortGI);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11AC_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }

        /*If AP name is provided and not the radio id, all radio id's of the
         *          * AP is updated with the AP short Gi status. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId, &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)

            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId, u4currentBindingId,
                         &i4RadioIfIndex) == SNMP_SUCCESS)

                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioType) != OSIX_SUCCESS)
                        {
                            if (u4RadioType == RADIO_TYPE_AC)
                            {

                                if (nmhTestv2FsDot11ACVHTShortGIOptionIn80Activated (&u4ErrCode, i4RadioIfIndex, (INT4) u4ShortGI) == SNMP_FAILURE)
                                {
                                    continue;
                                }
                                nmhSetFsDot11ACVHTShortGIOptionIn80Activated
                                    (i4RadioIfIndex, (INT4) u4ShortGI);
                            }
                        }
                    }
                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

        }
        /*Only for the particular radio id of the AP, Short Gi status is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (nmhTestv2FsDot11ACVHTShortGIOptionIn80Activated (&u4ErrCode,
                                                                     i4RadioIfIndex,
                                                                     (INT4)
                                                                     u4ShortGI)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11ACVHTShortGIOptionIn80Activated (i4RadioIfIndex,
                                                              (INT4) u4ShortGI);
            }
            else
            {
                CLI_SET_ERR (CLI_11AC_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Fs11acCliSetLdpcOption
 * Description :  This function is exported to CLI module to enable or 
            disable the LpdcOption.

 * Input       :  CliHandle
            u4RadioType
            u4Lpdc
            pu1ProfileName
            pu4RadioId

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT1
Fs11acCliSetLdpcOption (tCliHandle CliHandle, UINT4 u4RadioType,
                        UINT4 u4Lpdc, UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    /*If no Profile name is provided AP Lpdc status is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId, u4currentBindingId,
                 &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (nmhTestv2FsDot11ACVHTLDPCCodingOptionActivated (&u4ErrCode,
                                                                    i4RadioIfIndex,
                                                                    (INT4)
                                                                    u4Lpdc) ==
                    SNMP_FAILURE)
                {
                    continue;
                }
                nmhSetFsDot11ACVHTLDPCCodingOptionActivated (i4RadioIfIndex,
                                                             (INT4) u4Lpdc);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11AC_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }

        /*If AP name is provided and not the radio id, all radio id's of the
         *          * AP is updated with the AP Lpdc status. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId, &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)

            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {

                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId, u4currentBindingId,
                         &i4RadioIfIndex) == SNMP_SUCCESS)

                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioType) != OSIX_SUCCESS)
                        {
                            if (u4RadioType == RADIO_TYPE_AC)
                            {

                                if (nmhTestv2FsDot11ACVHTLDPCCodingOptionActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4Lpdc) == SNMP_FAILURE)
                                {
                                    continue;
                                }
                                nmhSetFsDot11ACVHTLDPCCodingOptionActivated
                                    (i4RadioIfIndex, (INT4) u4Lpdc);
                            }
                        }
                    }
                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

        }
        /*Only for the particular radio id of the AP, Lpdc status is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (nmhTestv2FsDot11ACVHTLDPCCodingOptionActivated (&u4ErrCode,
                                                                    i4RadioIfIndex,
                                                                    (INT4)
                                                                    u4Lpdc) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11ACVHTLDPCCodingOptionActivated (i4RadioIfIndex,
                                                             (INT4) u4Lpdc);
            }
            else
            {
                CLI_SET_ERR (CLI_11AC_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}/****************************************************************************

 * Function    :  Fs11acCliSetRxMcsMap
 * Description :  This function is exported to CLI module to set the value
            for rx MCS Map

 * Input       :  CliHandle
            u4RadioType
            u4Rate
            pu1ProfileName
            pu4RadioId
            u4SpatialNum

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT1
Fs11acCliSetRxMcsMap (tCliHandle CliHandle, UINT4 u4RadioType,
                      UINT4 u4Rate, UINT1 *pu1ProfileName,
                      UINT4 *pu4RadioId, UINT4 u4SpatialNum)
{

    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT1               au1rxdb[FS11AC_SIZE8];
    UINT1               au1rx[FS11AC_SIZE8];
    UINT1               u1loop;
    tSNMP_OCTET_STRING_TYPE RxMapDb;
    tSNMP_OCTET_STRING_TYPE RxMap;

    MEMSET (au1rxdb, 0, FS11AC_SIZE8);
    MEMSET (au1rx, 0, FS11AC_SIZE8);
    MEMSET (&RxMapDb, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RxMap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    RxMapDb.pu1_OctetList = au1rxdb;
    /*If no Profile name is provided AP RxMCS Map is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId, u4currentBindingId,
                 &i4RadioIfIndex) == SNMP_SUCCESS)

            {

                nmhGetFsDot11ACVHTRxMCSMapConfig (i4RadioIfIndex, &RxMapDb);

                for (u1loop = 0; u1loop < FS11AC_MCS_SPATIALVALUE; u1loop++)
                {
                    if (u1loop == (u4SpatialNum - 1))
                    {
                        if ((u4Rate == 0)
                            && (au1rxdb[u1loop] == FS11AC_MCS_CHECK))
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            return CLI_FAILURE;
                        }
                        else
                        {
                            au1rx[u1loop] = (UINT1) u4Rate;
                        }
                    }
                    else
                    {
                        au1rx[u1loop] = au1rxdb[u1loop];
                    }
                }

                RxMap.pu1_OctetList = au1rx;

                if (nmhTestv2FsDot11ACVHTRxMCSMapConfig (&u4ErrCode,
                                                         i4RadioIfIndex, &RxMap)
                    == SNMP_FAILURE)
                {
                    continue;
                }
                nmhSetFsDot11ACVHTRxMCSMapConfig (i4RadioIfIndex, &RxMap);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11AC_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *  *          * AP is updated with the AP Rx MCS Map. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId, &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)

            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {

                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId, u4currentBindingId,
                         &i4RadioIfIndex) == SNMP_SUCCESS)

                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioType) != OSIX_SUCCESS)
                        {
                            if (u4RadioType == RADIO_TYPE_AC)
                            {

                                nmhGetFsDot11ACVHTRxMCSMapConfig
                                    (i4RadioIfIndex, &RxMapDb);
                                for (u1loop = 0;
                                     u1loop < FS11AC_MCS_SPATIALVALUE; u1loop++)
                                {
                                    if (u1loop == (u4SpatialNum - 1))
                                    {
                                        if ((u4Rate == 0)
                                            && (au1rxdb[u1loop] ==
                                                FS11AC_MCS_CHECK))
                                        {
                                            CLI_FATAL_ERROR (CliHandle);
                                            return CLI_FAILURE;
                                        }
                                        else
                                        {
                                            au1rx[u1loop] = (UINT1) u4Rate;
                                        }
                                    }

                                    else
                                    {
                                        au1rx[u1loop] = au1rxdb[u1loop];
                                    }
                                }
                                RxMap.pu1_OctetList = au1rx;

                                if (nmhTestv2FsDot11ACVHTRxMCSMapConfig
                                    (&u4ErrCode, i4RadioIfIndex,
                                     &RxMap) == SNMP_FAILURE)

                                {
                                    continue;
                                }
                                nmhSetFsDot11ACVHTRxMCSMapConfig
                                    (i4RadioIfIndex, &RxMap);
                            }
                        }
                    }
                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

        }
        /*Only for the particular radio id of the AP, Rx MCS Map is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                nmhGetFsDot11ACVHTRxMCSMapConfig (i4RadioIfIndex, &RxMapDb);
                for (u1loop = 0; u1loop < FS11AC_MCS_SPATIALVALUE; u1loop++)
                {
                    if (u1loop == (u4SpatialNum - 1))
                    {
                        if ((u4Rate == 0)
                            && (au1rxdb[u1loop] == FS11AC_MCS_CHECK))
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            return CLI_FAILURE;
                        }
                        else
                        {
                            au1rx[u1loop] = (UINT1) u4Rate;
                        }

                    }
                    else
                    {
                        au1rx[u1loop] = au1rxdb[u1loop];
                    }
                }
                RxMap.pu1_OctetList = au1rx;

                if (nmhTestv2FsDot11ACVHTRxMCSMapConfig (&u4ErrCode,
                                                         i4RadioIfIndex,
                                                         &RxMap) ==
                    SNMP_FAILURE)

                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11ACVHTRxMCSMapConfig (i4RadioIfIndex, &RxMap);

            }
            else
            {
                CLI_SET_ERR (CLI_11AC_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Fs11acCliSetTxMcsMap
 * Description :  This function is exported to CLI module to set the value
            for the tx MCS Map.

 * Input       :  CliHandle
            u4RadioType
            u4Rate
           pu1ProfileName
            pu4RadioId
            u4SpatialNum

 * Output      :  None

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/

INT1
Fs11acCliSetTxMcsMap (tCliHandle CliHandle, UINT4 u4RadioType,
                      UINT4 u4Rate, UINT1 *pu1ProfileName,
                      UINT4 *pu4RadioId, UINT4 u4SpatialNum)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT1               au1txdb[FS11AC_SIZE8];
    UINT1               au1tx[FS11AC_SIZE8];
    UINT1               u1loop;
    tSNMP_OCTET_STRING_TYPE TxMapDb;
    tSNMP_OCTET_STRING_TYPE TxMap;

    MEMSET (au1txdb, 0, FS11AC_SIZE8);
    MEMSET (au1tx, 0, FS11AC_SIZE8);
    MEMSET (&TxMapDb, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TxMap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    TxMapDb.pu1_OctetList = au1txdb;

    /*If no Profile name is provided AP Tx MCS Map is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId, u4currentBindingId,
                 &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                nmhGetFsDot11ACVHTTxMCSMapConfig (i4RadioIfIndex, &TxMapDb);
                for (u1loop = 0; u1loop < FS11AC_MCS_SPATIALVALUE; u1loop++)
                {
                    if (u1loop == (u4SpatialNum - 1))
                    {
                        if ((u4Rate == 0)
                            && (au1txdb[u1loop] == FS11AC_MCS_CHECK))
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            return CLI_FAILURE;
                        }
                        else
                        {
                            au1tx[u1loop] = (UINT1) u4Rate;
                        }

                    }
                    else
                    {
                        au1tx[u1loop] = au1txdb[u1loop];
                    }
                }
                TxMap.pu1_OctetList = au1tx;

                if (nmhTestv2FsDot11ACVHTTxMCSMapConfig (&u4ErrCode,
                                                         i4RadioIfIndex, &TxMap)
                    == SNMP_FAILURE)
                {
                    continue;
                }
                nmhSetFsDot11ACVHTTxMCSMapConfig (i4RadioIfIndex, &TxMap);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11AC_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }

        /*If AP name is provided and not the radio id, all radio id's of the
         *  *  *          * AP is updated with the AP Tx MCS Map. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId, &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)

            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {

                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId, u4currentBindingId,
                         &i4RadioIfIndex) == SNMP_SUCCESS)

                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioType) != OSIX_SUCCESS)
                        {
                            if (u4RadioType == RADIO_TYPE_AC)
                            {

                                nmhGetFsDot11ACVHTTxMCSMapConfig
                                    (i4RadioIfIndex, &TxMapDb);
                                for (u1loop = 0;
                                     u1loop < FS11AC_MCS_SPATIALVALUE; u1loop++)
                                {
                                    if (u1loop == (u4SpatialNum - 1))
                                    {
                                        if ((u4Rate == 0)
                                            && (au1txdb[u1loop] ==
                                                FS11AC_MCS_CHECK))
                                        {
                                            CLI_FATAL_ERROR (CliHandle);
                                            return CLI_FAILURE;
                                        }
                                        else
                                        {
                                            au1tx[u1loop] = (UINT1) u4Rate;
                                        }

                                    }
                                    else
                                    {
                                        au1tx[u1loop] = au1txdb[u1loop];
                                    }
                                }
                                TxMap.pu1_OctetList = au1tx;

                                if (nmhTestv2FsDot11ACVHTTxMCSMapConfig
                                    (&u4ErrCode, i4RadioIfIndex,
                                     &TxMap) == SNMP_FAILURE)

                                {
                                    continue;
                                }
                                nmhSetFsDot11ACVHTTxMCSMapConfig
                                    (i4RadioIfIndex, &TxMap);

                            }
                        }
                    }
                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

        }
        /*Only for the particular radio id of the AP, Tx MCS Map is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (nmhGetFsDot11ACVHTTxMCSMapConfig (i4RadioIfIndex, &TxMapDb)
                    == SNMP_SUCCESS)
                {
                    if (au1txdb[u4SpatialNum - 1] == u4Rate)
                    {
                        CliPrintf (CliHandle,
                                   "\r\n This value is already configured \r\n");
                        CLI_FATAL_ERROR (CliHandle);
                        return CLI_FAILURE;
                    }
                    else
                    {
                        for (u1loop = 0; u1loop < FS11AC_MCS_SPATIALVALUE;
                             u1loop++)
                        {
                            if (u1loop == (u4SpatialNum - 1))
                            {
                                if ((u4Rate == 0)
                                    && (au1txdb[u1loop] == FS11AC_MCS_CHECK))
                                {
                                    CLI_FATAL_ERROR (CliHandle);
                                    return CLI_FAILURE;
                                }
                                else
                                {
                                    au1tx[u1loop] = (UINT1) u4Rate;
                                }

                            }
                            else
                            {
                                au1tx[u1loop] = au1txdb[u1loop];
                            }
                        }
                    }
                    TxMap.pu1_OctetList = au1tx;

                }
                if (nmhTestv2FsDot11ACVHTTxMCSMapConfig (&u4ErrCode,
                                                         i4RadioIfIndex,
                                                         &TxMap) ==
                    SNMP_FAILURE)

                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11ACVHTTxMCSMapConfig (i4RadioIfIndex, &TxMap);

            }
            else
            {
                CLI_SET_ERR (CLI_11AC_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Fs11acCliShow
 * Description :  This function is exported to CLI module to display all the
            mib ojects values.

 * Input       :  CliHandle
            u4RadioType
            pu1ProfileName
            pu4RadioId            

 * Output      :  None

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT1
Fs11acCliShow (tCliHandle CliHandle, UINT4 u4RadioType,
               UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;

    /*If no Profile name is provided then all mib objects for */
    /* all Ap's are disaplyed */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId, u4currentBindingId,
                 &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (Fs11acCliShowCmd (CliHandle, i4RadioIfIndex) != CLI_SUCCESS)
                {
                    CLI_SET_ERR (CLI_11AC_SHOW_ERROR);
                    return CLI_FAILURE;
                }

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11AC_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }

        /*If AP name is provided and not the radio id, all radio id's of the
         *  *          * AP with its mib objects are displayed. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId, &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)

            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {

                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId, u4currentBindingId,
                         &i4RadioIfIndex) == SNMP_SUCCESS)

                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioType) != OSIX_SUCCESS)
                        {
                            if (u4RadioType == RADIO_TYPE_AC)
                            {

                                if (Fs11acCliShowCmd (CliHandle, i4RadioIfIndex)
                                    != CLI_SUCCESS)
                                {
                                    continue;
                                }

                            }
                        }
                    }
                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

        }
        /*Only for the particular radio id of the AP, mib objects are disaplyed. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex, &u4RadioType)
                    != OSIX_SUCCESS)
                {
                    if (u4RadioType == RADIO_TYPE_AC)
                    {
                        if (Fs11acCliShowCmd (CliHandle, i4RadioIfIndex) !=
                            CLI_SUCCESS)
                        {
                            CLI_SET_ERR (CLI_11AC_SHOW_ERROR);
                            return CLI_FAILURE;
                        }
                    }
                    else
                    {
                        CliPrintf (CliHandle, "No Entry found\r\n");
                    }
                }
            }
            else
            {
                CLI_SET_ERR (CLI_11AC_RADIOINDEX_ERROR);

                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Fs11acCliShowCmd
 * Description :  This function is use for displaying the values of
           various mib objects.

 * Input       :  CliHandle
 *           i4RadioIfIndex

 * Output      :  None

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/

INT1
Fs11acCliShowCmd (tCliHandle CliHandle, INT4 i4RadioIfIndex)
{
    INT4                i4MpduLength = 0;
    INT4                i4MpduLengthConfig = 0;
    UINT4               u4AmpduFactor = 0;
    UINT4               u4AmpduFactorConfig = 0;
    INT4                i4ControlField = 0;
    INT4                i4PowerOption = 0;
    UINT4               u4RxHighestData = 0;
    UINT4               u4TxHighestData = 0;
    INT4                i4ChannelWidth = 0;
    INT4                i4ChannelWidthConfig = 0;
    UINT4               u4CenterFreqIndex0 = 0;
    UINT4               u4CenterFreqIndex0Config = 0;
    UINT4               u4CenterFreqIndex1 = 0;
    UINT4               u4SpatialStreamsImp = 0;
    INT4                i4HTOption = 0;
    UINT4               u4RxDataRateConfig = 0;
    UINT4               u4TxDataRateConfig = 0;
    UINT4               u4SpatialStreamsAct = 0;
    UINT4               u4FragThres = 0;
    INT4                i4ShortGI80 = 0;
    INT4                i4ShortGI80Config = 0;
    INT4                i4ShortGI160 = 0;
    INT4                i4LdpcOption = 0;
    INT4                i4LdpcOptionConfig = 0;
    INT4                i4TxStbcOption = 0;
    INT4                i4RxStbcOption = 0;
    INT4                i4SuBeamFormer = 0;
    INT4                i4SuBeamFormee = 0;
    INT4                i4MuBeamFormer = 0;
    INT4                i4MuBeamFormee = 0;
    INT4                i4LinkAdaption = 0;
    INT4                i4RxAntenna = 0;
    INT4                i4TxAntenna = 0;
    INT4                i4VHTOption = 0;
    INT4                i4OpModeNotif = 0;
    INT4                i4TxStbcOptionAct = 0;
    INT4                i4RxStbcOptionAct = 0;
    UINT1               au1RxMcs[FS11AC_SIZE8];
    UINT1               au1TxMcs[FS11AC_SIZE8];
    UINT1               au1RxMcsConfig[FS11AC_SIZE8];
    UINT1               au1TxMcsConfig[FS11AC_SIZE8];
    tSNMP_OCTET_STRING_TYPE RxMap;
    tSNMP_OCTET_STRING_TYPE TxMap;
    tSNMP_OCTET_STRING_TYPE RxMapConfig;
    tSNMP_OCTET_STRING_TYPE TxMapConfig;
    UINT1               u1loop = 0;
    UINT1               au1BasicMap[FS11AC_SIZE2];
    tSNMP_OCTET_STRING_TYPE BasicMap;

    MEMSET (au1RxMcs, 0, FS11AC_SIZE8);
    MEMSET (au1TxMcs, 0, FS11AC_SIZE8);
    MEMSET (au1RxMcsConfig, 0, FS11AC_SIZE8);
    MEMSET (au1TxMcsConfig, 0, FS11AC_SIZE8);
    MEMSET (au1BasicMap, 0, FS11AC_SIZE2);

    MEMSET (&RxMap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TxMap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RxMapConfig, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&TxMapConfig, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&BasicMap, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    RxMap.pu1_OctetList = au1RxMcs;
    TxMap.pu1_OctetList = au1TxMcs;
    RxMapConfig.pu1_OctetList = au1RxMcsConfig;
    TxMapConfig.pu1_OctetList = au1TxMcsConfig;
    BasicMap.pu1_OctetList = au1BasicMap;

    CliPrintf (CliHandle, "\r\nSupported :\r\n");
    if (nmhGetFsDot11ACMaxMPDULength (i4RadioIfIndex, &i4MpduLength)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nMaximum MPDU Length Supported              : %d",
                   i4MpduLength);
    }
    if (nmhGetFsDot11ACVHTMaxRxAMPDUFactor (i4RadioIfIndex, &u4AmpduFactor)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nMaximum AMPDU Factor Supported             : %d",
                   u4AmpduFactor);
    }
    if (nmhGetFsDot11ACVHTControlFieldSupported
        (i4RadioIfIndex, &i4ControlField) == SNMP_SUCCESS)
    {
        if (i4ControlField == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nControl Field Support                      : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nControl Field Support                      : Enable");

        }
    }
    if (nmhGetFsDot11ACVHTTXOPPowerSaveOptionImplemented (i4RadioIfIndex,
                                                          &i4PowerOption) ==
        SNMP_SUCCESS)
    {
        if (i4PowerOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nTxOP Power Save Option                     : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nTxOP Power Save Option                     : Enable");

        }
    }
    if (nmhGetFsDot11ACVHTRxHighestDataRateSupported (i4RadioIfIndex,
                                                      &u4RxHighestData) ==
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nRx Highest Data Rate                       : %d",
                   u4RxHighestData);
    }
    if (nmhGetFsDot11ACVHTTxHighestDataRateSupported (i4RadioIfIndex,
                                                      &u4TxHighestData) ==
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nTx Highest Data Rate                       : %d",
                   u4TxHighestData);
    }
    if (nmhGetFsDot11ACCurrentChannelBandwidth (i4RadioIfIndex, &i4ChannelWidth)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nCurrent Channel Bandwidth Supported        : %d",
                   i4ChannelWidth);
    }
    if (nmhGetFsDot11ACCurrentChannelCenterFrequencyIndex0 (i4RadioIfIndex,
                                                            &u4CenterFreqIndex0)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nChannel Center Frequency Index0 Supported   : %d",
                   u4CenterFreqIndex0);
    }
    if (nmhGetFsDot11ACCurrentChannelCenterFrequencyIndex1 (i4RadioIfIndex,
                                                            &u4CenterFreqIndex1)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nChannel Center Frequency Index1 Supported   : %d",
                   u4CenterFreqIndex1);
    }
    if (nmhGetFsDot11ACVHTShortGIOptionIn80Implemented (i4RadioIfIndex,
                                                        &i4ShortGI80) ==
        SNMP_SUCCESS)
    {
        if (i4ShortGI80 == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nShort guard Interval for 80MHZ Supported   : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nShort guard Interval for 80MHZ Supported   : Enable");

        }
    }
    if (nmhGetFsDot11ACVHTShortGIOptionIn160and80p80Implemented (i4RadioIfIndex,
                                                                 &i4ShortGI160)
        == SNMP_SUCCESS)
    {
        if (i4ShortGI160 == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nShort guard Interval for 160MHZ Supported  : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nShort guard Interval for 160MHZ Supported  : Enable");

        }
    }
    if (nmhGetFsDot11ACVHTLDPCCodingOptionImplemented (i4RadioIfIndex,
                                                       &i4LdpcOption) ==
        SNMP_SUCCESS)
    {
        if (i4LdpcOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nLDPC Coding Option Supported               : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nLDPC Coding Option Supported               : Enable");

        }
    }
    if (nmhGetFsDot11ACVHTTxSTBCOptionImplemented
        (i4RadioIfIndex, &i4TxStbcOption) == SNMP_SUCCESS)
    {
        if (i4TxStbcOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nTxStbc Option                              : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nTxStbc Option                              : Enable");

        }
    }
    if (nmhGetFsDot11ACVHTRxSTBCOptionImplemented
        (i4RadioIfIndex, &i4RxStbcOption) == SNMP_SUCCESS)
    {
        if (i4RxStbcOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nRxStbc Option                              : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nRxStbc Option                              : Enable");

        }
    }
    if (nmhGetFsDot11ACSuBeamFormer (i4RadioIfIndex, &i4SuBeamFormer)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nSu Beam Former                             : %d",
                   i4SuBeamFormer);
    }
    if (nmhGetFsDot11ACSuBeamFormee (i4RadioIfIndex, &i4SuBeamFormee)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nSu Beam Formee                             : %d",
                   i4SuBeamFormee);
    }
    if (nmhGetFsDot11ACMuBeamFormer (i4RadioIfIndex, &i4MuBeamFormer)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nMu Beam Former                             : %d",
                   i4MuBeamFormer);
    }
    if (nmhGetFsDot11ACMuBeamFormee (i4RadioIfIndex, &i4MuBeamFormee)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nMu Beam Formee                             : %d",
                   i4MuBeamFormee);
    }
    if (nmhGetFsDot11ACVHTLinkAdaption (i4RadioIfIndex, &i4LinkAdaption)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nVHT Link Adaption                          : %d",
                   i4LinkAdaption);
    }
    if (nmhGetFsDot11VHTOptionImplemented (i4RadioIfIndex, &i4VHTOption)
        == SNMP_SUCCESS)
    {
        if (i4VHTOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nVHT Option                                 : Disabled");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nVHT Option                                 : Enabled");
        }
    }
    if (nmhGetFsDot11NumberOfSpatialStreamsImplemented
        (i4RadioIfIndex, &u4SpatialStreamsImp) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nNumber of Spatial Streams Implemented      : %d",
                   u4SpatialStreamsImp);
    }
    if (nmhGetDot11HighThroughputOptionImplemented (i4RadioIfIndex, &i4HTOption)
        == SNMP_SUCCESS)
    {
        if (i4HTOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nHigh Throughput Option                    : Disabled");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nHigh Throughput Option                    : Enabled");
        }

    }
    if (nmhGetFsDot11OperatingModeNotificationImplemented
        (i4RadioIfIndex, &i4OpModeNotif) == SNMP_SUCCESS)
    {
        if (i4OpModeNotif == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nOperating Mode Notification                : Disabled");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nOperating Mode Notification                : Enabled");
        }
    }

    if (nmhGetFsDot11ACRxAntennaPattern (i4RadioIfIndex, &i4RxAntenna)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nRx Antenna Pattern                         : %d",
                   i4RxAntenna);
    }
    if (nmhGetFsDot11ACTxAntennaPattern (i4RadioIfIndex, &i4TxAntenna)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nTx Antenna Pattern                         : %d",
                   i4TxAntenna);
    }
    if (nmhGetFsDot11ACVHTRxMCSMap (i4RadioIfIndex, &RxMap) == SNMP_SUCCESS)
    {
        for (u1loop = 0; u1loop < 8; u1loop++)
        {
            CliPrintf (CliHandle, "\r\nRx MCS Map Spatial Stream %d :",
                       u1loop + 1);
            if (DisplayMap (CliHandle, au1RxMcs[u1loop]) != CLI_SUCCESS)
            {
                CliPrintf (CliHandle, "Error in stream\r\n");
            }
        }
    }
    if (nmhGetFsDot11ACVHTTxMCSMap (i4RadioIfIndex, &TxMap) == SNMP_SUCCESS)
    {
        for (u1loop = 0; u1loop < 8; u1loop++)
        {
            CliPrintf (CliHandle, "\r\nTx MCS Map Spatial Stream %d :",
                       u1loop + 1);
            if (DisplayMap (CliHandle, au1TxMcs[u1loop]) != CLI_SUCCESS)
            {
                CliPrintf (CliHandle, "Error in stream\r\n");
            }
        }
    }
    if (nmhGetFsDot11ACBasicMCSMap (i4RadioIfIndex, &BasicMap) != CLI_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nBasic MCS Map                              : %d:",
                   au1BasicMap[0]);
        CliPrintf (CliHandle, "%d\r\n", au1BasicMap[1]);
    }

    CliPrintf (CliHandle, "\r\nConfigured :\r\n");
    if (nmhGetFsDot11ACMaxMPDULengthConfig (i4RadioIfIndex, &i4MpduLengthConfig)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nMaximum MPDU Length Configured             : %d",
                   i4MpduLengthConfig);
    }
    if (nmhGetFsDot11ACVHTMaxRxAMPDUFactorConfig (i4RadioIfIndex,
                                                  &u4AmpduFactorConfig) ==
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nMaximum AMPDU Factor Configured            : %d",
                   u4AmpduFactorConfig);
    }
    if (nmhGetFsDot11ACCurrentChannelBandwidthConfig (i4RadioIfIndex,
                                                      &i4ChannelWidthConfig) ==
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nChannel Bandwidth Configured                : %d",
                   i4ChannelWidthConfig);
    }
    if (nmhGetFsDot11ACCurrentChannelCenterFrequencyIndex0Config
        (i4RadioIfIndex, &u4CenterFreqIndex0Config) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nChannel Center Frequency Index0 Configured  : %d",
                   u4CenterFreqIndex0Config);
    }
    if (nmhGetFsDot11ACVHTShortGIOptionIn80Activated (i4RadioIfIndex,
                                                      &i4ShortGI80Config) ==
        SNMP_SUCCESS)
    {
        if (i4ShortGI80Config == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nShort guard Interval for 80MHZ Configured  : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nShort guard Interval for 80MHZ Configured  : Enable");

        }
    }
    if (nmhGetFsDot11ACVHTLDPCCodingOptionActivated (i4RadioIfIndex,
                                                     &i4LdpcOptionConfig) ==
        SNMP_SUCCESS)
    {
        if (i4LdpcOptionConfig == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nLDPC Coding Option Configured              : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nLDPC Coding Option Configured              : Enable");

        }
    }
    if (nmhGetFsDot11VHTRxHighestDataRateConfig
        (i4RadioIfIndex, &u4RxDataRateConfig) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nRx Highest Data Rate Configured            : %d",
                   u4RxDataRateConfig);
    }
    if (nmhGetFsDot11VHTTxHighestDataRateConfig
        (i4RadioIfIndex, &u4TxDataRateConfig) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nTx Highest Data Rate Configured            : %d",
                   u4TxDataRateConfig);
    }
    if (nmhGetFsDot11NumberOfSpatialStreamsActivated
        (i4RadioIfIndex, &u4SpatialStreamsAct) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nNumber of Spatial Streams Activated         : %d",
                   u4SpatialStreamsAct);
    }
    if (nmhGetFsDot11FragmentationThreshold (i4RadioIfIndex, &u4FragThres)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nFragmentation Threshold                     : %d",
                   u4FragThres);
    }
    if (nmhGetFsDot11ACVHTTxSTBCOptionActivated
        (i4RadioIfIndex, &i4TxStbcOptionAct) == SNMP_SUCCESS)
    {
        if (i4TxStbcOptionAct == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nTxStbc Option                              : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nTxStbc Option                              : Enable");

        }
    }
    if (nmhGetFsDot11ACVHTRxSTBCOptionActivated
        (i4RadioIfIndex, &i4RxStbcOptionAct) == SNMP_SUCCESS)
    {
        if (i4RxStbcOptionAct == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nRxStbc Option                              : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nRxStbc Option                              : Enable");

        }
    }

    if (nmhGetFsDot11ACVHTRxMCSMapConfig (i4RadioIfIndex, &RxMapConfig)
        == SNMP_SUCCESS)
    {
        for (u1loop = 0; u1loop < 8; u1loop++)
        {
            CliPrintf (CliHandle, "\r\nRx MCS Map Spatial Stream %d :",
                       u1loop + 1);
            if (DisplayMap (CliHandle, au1RxMcsConfig[u1loop]) != CLI_SUCCESS)
            {
                CliPrintf (CliHandle, "Error in stream\r\n");
            }
        }
    }
    if (nmhGetFsDot11ACVHTTxMCSMapConfig (i4RadioIfIndex, &TxMapConfig)
        == SNMP_SUCCESS)
    {
        for (u1loop = 0; u1loop < 8; u1loop++)
        {
            CliPrintf (CliHandle, "\r\nTx MCS Map Spatial Stream %d :",
                       u1loop + 1);
            if (DisplayMap (CliHandle, au1TxMcsConfig[u1loop]) != CLI_SUCCESS)
            {
                CliPrintf (CliHandle, "Error in stream\r\n");
            }
        }
    }
    return CLI_SUCCESS;
}

INT1
DisplayMap (tCliHandle CliHandle, UINT1 RxMcsConfig)
{
    if (RxMcsConfig == FS11AC_MCS_RATE7)
    {
        CliPrintf (CliHandle, "0-7");
        return CLI_SUCCESS;
    }
    if (RxMcsConfig == FS11AC_MCS_RATE8)
    {
        CliPrintf (CliHandle, "0-8");
        return CLI_SUCCESS;
    }
    if (RxMcsConfig == FS11AC_MCS_RATE9)
    {
        CliPrintf (CliHandle, "0-9");
        return CLI_SUCCESS;
    }
    if (RxMcsConfig == FS11AC_MCS_DISABLED)
    {
        CliPrintf (CliHandle, "Disabled");
        return CLI_SUCCESS;
    }
    return CLI_FAILURE;
}

/****************************************************************************
 * Function    :  Fs11acCliSetRxStbc 
 * Description :  This function is exported to CLI module to enable or disable
          the Rx STBC.

 * Input       :  CliHandle
          u4RadioType
          u4RxStbc
          pu1ProfileName
          pu4RadioId

 * Output      :  None

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT1
Fs11acCliSetRxStbc (tCliHandle CliHandle, UINT4 u4RadioType,
                    UINT4 u4Rxstbc, UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{

    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    /*If no Profile name is provided AP Short Gi status is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId, u4currentBindingId,
                 &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (nmhTestv2FsDot11ACVHTRxSTBCOptionActivated (&u4ErrCode,
                                                                i4RadioIfIndex,
                                                                (INT4) u4Rxstbc)
                    == SNMP_FAILURE)
                {
                    continue;
                }
                nmhSetFsDot11ACVHTRxSTBCOptionActivated (i4RadioIfIndex,
                                                         (INT4) u4Rxstbc);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11AC_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }

        /*If AP name is provided and not the radio id, all radio id's of the
         *          * AP is updated with the AP short Gi status. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId, &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)

            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId, u4currentBindingId,
                         &i4RadioIfIndex) == SNMP_SUCCESS)

                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioType) != OSIX_SUCCESS)
                        {
                            if (u4RadioType == RADIO_TYPE_AC)
                            {

                                if (nmhTestv2FsDot11ACVHTRxSTBCOptionActivated
                                    (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4Rxstbc) == SNMP_FAILURE)
                                {
                                    continue;
                                }
                                nmhSetFsDot11ACVHTRxSTBCOptionActivated
                                    (i4RadioIfIndex, (INT4) u4Rxstbc);
                            }
                        }
                    }
                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

        }
        /*Only for the particular radio id of the AP, Short Gi status is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (nmhTestv2FsDot11ACVHTRxSTBCOptionActivated (&u4ErrCode,
                                                                i4RadioIfIndex,
                                                                (INT4) u4Rxstbc)
                    == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                }
                nmhSetFsDot11ACVHTRxSTBCOptionActivated (i4RadioIfIndex,
                                                         (INT4) u4Rxstbc);
            }
            else
            {
                CLI_SET_ERR (CLI_11AC_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Fs11acCliSetTxStbc 
 * Description :  This function is exported to CLI module to enable or disable
          the Tx STBC.

 * Input       :  CliHandle
          u4RadioType
          u4TxStbc
          pu1ProfileName
          pu4RadioId

 * Output      :  None

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT1
Fs11acCliSetTxStbc (tCliHandle CliHandle, UINT4 u4RadioType,
                    UINT4 u4Txstbc, UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{

    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    /*If no Profile name is provided AP Short Gi status is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId, u4currentBindingId,
                 &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (nmhTestv2FsDot11ACVHTTxSTBCOptionActivated (&u4ErrCode,
                                                                i4RadioIfIndex,
                                                                (INT4) u4Txstbc)
                    == SNMP_FAILURE)
                {
                    continue;
                }
                nmhSetFsDot11ACVHTTxSTBCOptionActivated (i4RadioIfIndex,
                                                         (INT4) u4Txstbc);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11AC_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }

        /*If AP name is provided and not the radio id, all radio id's of the
         *          * AP is updated with the AP short Gi status. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId, &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)

            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId, u4currentBindingId,
                         &i4RadioIfIndex) == SNMP_SUCCESS)

                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioType) != OSIX_SUCCESS)
                        {
                            if (u4RadioType == RADIO_TYPE_AC)
                            {

                                if (nmhTestv2FsDot11ACVHTTxSTBCOptionActivated
                                    (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4Txstbc) == SNMP_FAILURE)
                                {
                                    continue;
                                }
                                nmhSetFsDot11ACVHTTxSTBCOptionActivated
                                    (i4RadioIfIndex, (INT4) u4Txstbc);
                            }
                        }
                    }
                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

        }
        /*Only for the particular radio id of the AP, Short Gi status is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (nmhTestv2FsDot11ACVHTTxSTBCOptionActivated (&u4ErrCode,
                                                                i4RadioIfIndex,
                                                                (INT4) u4Txstbc)
                    == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                }
                nmhSetFsDot11ACVHTTxSTBCOptionActivated (i4RadioIfIndex,
                                                         (INT4) u4Txstbc);
            }
            else
            {
                CLI_SET_ERR (CLI_11AC_RADIOINDEX_ERROR);
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Fs11acCliSetSTS 
 * Description :  This function is exported to CLI module to enable or disable
          the Rx STBC.

 * Input       :  CliHandle
          u4RadioType
          u4RxStbc
          pu1ProfileName
          pu4RadioId

 * Output      :  None

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT1
Fs11acCliSetSTS (tCliHandle CliHandle, UINT4 u4RadioType,
                 UINT4 u4STS, UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{

    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    /*If no Profile name is provided AP Short Gi status is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId, u4currentBindingId,
                 &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (nmhTestv2FsDot11NumberOfSpatialStreamsActivated (&u4ErrCode,
                                                                     i4RadioIfIndex,
                                                                     u4STS) ==
                    SNMP_FAILURE)
                {
                    continue;
                }
                nmhSetFsDot11NumberOfSpatialStreamsActivated (i4RadioIfIndex,
                                                              u4STS);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11AC_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }

        /*If AP name is provided and not the radio id, all radio id's of the
         *          * AP is updated with the AP short Gi status. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId, &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)

            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId, u4currentBindingId,
                         &i4RadioIfIndex) == SNMP_SUCCESS)

                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioType) != OSIX_SUCCESS)
                        {
                            if (u4RadioType == RADIO_TYPE_AC)
                            {

                                if (nmhTestv2FsDot11NumberOfSpatialStreamsActivated (&u4ErrCode, i4RadioIfIndex, u4STS) == SNMP_FAILURE)
                                {
                                    continue;
                                }
                                nmhSetFsDot11NumberOfSpatialStreamsActivated
                                    (i4RadioIfIndex, u4STS);
                            }
                        }
                    }
                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

        }
        /*Only for the particular radio id of the AP, Short Gi status is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (nmhTestv2FsDot11NumberOfSpatialStreamsActivated (&u4ErrCode,
                                                                     i4RadioIfIndex,
                                                                     u4STS) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);

                }
                nmhSetFsDot11NumberOfSpatialStreamsActivated (i4RadioIfIndex,
                                                              u4STS);
            }
            else
            {
                CLI_SET_ERR (CLI_11AC_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}
#endif
