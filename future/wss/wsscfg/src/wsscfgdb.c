/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
 *  $Id: wsscfgdb.c,v 1.6 2018/01/04 09:54:33 siva Exp $
*
* Description: This file contains the routines for the protocol Database Access for the module Wsscfg 
*********************************************************************/

#include "wsscfginc.h"
#include "wsscfgcli.h"
#include "capwapcli.h"
#include "radioif.h"
#include "radioifextn.h"
#include "fpam.h"
tCountryList        country[] = {
    {"US", '\0'}
    ,
    {"GB", '\0'}
    ,
    {"CN", '\0'}
    ,
    {"IN", '\0'}
    ,
    {"JP", '\0'}
    ,
    {"SG", '\0'}
};

#ifdef WLC_WANTED
extern INT1         gUserCnt;
#endif

/****************************************************************************
 Function    :  WsscfgTestAllCapwapDot11WlanTable
 Input       :  pu4ErrorCode
                pWsscfgSetCapwapDot11WlanEntry
                pWsscfgIsSetCapwapDot11WlanEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllCapwapDot11WlanTable (UINT4 *pu4ErrorCode,
                                   tWsscfgCapwapDot11WlanEntry *
                                   pWsscfgSetCapwapDot11WlanEntry,
                                   tWsscfgIsSetCapwapDot11WlanEntry *
                                   pWsscfgIsSetCapwapDot11WlanEntry,
                                   INT4 i4RowStatusLogic,
                                   INT4 i4RowCreateOption)
{
    INT4                i4MacType = 0;
    INT4                i4RowStatus = 0;
    INT4                i4RetValCapwapDot11WlanRowStatus = 0;
    UINT4               u4ManagmentProfileId = 0;
    UINT2               u2WlanCount = 0;
    UINT1               au1TunnelMode[256];
    INT4                i4WlanIfIndex = 0;
    INT4                i4FsSecurityWlanProfileId = 0;
    UINT1               au1Dot11DesiredSSID[CLI_MAX_SSID_LEN];
    UINT1               au1FsSecurityWebAuthUName[WSS_STA_WEBUSER_LEN_MAX];
    UINT1               au1NextFsSecurityWebAuthUName[WSS_STA_WEBUSER_LEN_MAX];
    tSNMP_OCTET_STRING_TYPE RetValCapwapDot11WlanTunnelMode;
    tSNMP_OCTET_STRING_TYPE WebAuthUName;
    tSNMP_OCTET_STRING_TYPE NextWebAuthUName;

    MEMSET (&au1TunnelMode, 0, 256);
    MEMSET (&RetValCapwapDot11WlanTunnelMode, 0,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1Dot11DesiredSSID, 0, CLI_MAX_SSID_LEN);
    MEMSET (&au1FsSecurityWebAuthUName, 0, sizeof (au1FsSecurityWebAuthUName));
    MEMSET (&au1NextFsSecurityWebAuthUName, 0,
            sizeof (au1NextFsSecurityWebAuthUName));
    MEMSET (&WebAuthUName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextWebAuthUName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    RetValCapwapDot11WlanTunnelMode.pu1_OctetList = au1TunnelMode;
    WebAuthUName.pu1_OctetList = au1FsSecurityWebAuthUName;
    NextWebAuthUName.pu1_OctetList = au1NextFsSecurityWebAuthUName;
    WssCfgGetWlanCount (&u2WlanCount);

    if ((pWsscfgSetCapwapDot11WlanEntry->MibObject.i4CapwapDot11WlanRowStatus
         == CREATE_AND_GO)
        || (pWsscfgSetCapwapDot11WlanEntry->MibObject.i4CapwapDot11WlanRowStatus
            == CREATE_AND_WAIT))
    {

        if (u2WlanCount == MAX_NUM_OF_SSID_PER_WLC)
        {
            *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanProfileId ==
        OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }

    if (pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanMacType != OSIX_FALSE)
    {
        if ((pWsscfgSetCapwapDot11WlanEntry->
             MibObject.i4CapwapDot11WlanMacType !=
             CLI_WTP_MAC_TYPE_SPLIT)
            && (pWsscfgSetCapwapDot11WlanEntry->
                MibObject.i4CapwapDot11WlanMacType != CLI_WTP_MAC_TYPE_LOCAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetCapwapDot11WlanEntry->
        bCapwapDot11WlanTunnelMode != OSIX_FALSE)
    {
        if ((pWsscfgSetCapwapDot11WlanEntry->
             MibObject.au1CapwapDot11WlanTunnelMode[0]) ==
            CLI_WLAN_TUNNEL_MODE_BRIDGE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_WSSCFG_TUNMODE_NOT_SUPP);
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetCapwapDot11WlanEntry->
        bCapwapDot11WlanTunnelMode != OSIX_FALSE)
    {
        if ((pWsscfgSetCapwapDot11WlanEntry->
             MibObject.au1CapwapDot11WlanTunnelMode[0] !=
             CLI_WLAN_TUNNEL_MODE_DOT3)
            && (pWsscfgSetCapwapDot11WlanEntry->
                MibObject.au1CapwapDot11WlanTunnelMode[0] !=
                CLI_WLAN_TUNNEL_MODE_NATIVE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

    }

    if (pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanRowStatus !=
        OSIX_FALSE)
    {
        if (pWsscfgSetCapwapDot11WlanEntry->MibObject.
            i4CapwapDot11WlanRowStatus == CREATE_AND_GO)
        {
            CLI_SET_ERR (CLI_WSSCFG_TABLE_UPDATE);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
        if (nmhGetCapwapDot11WlanRowStatus
            (pWsscfgSetCapwapDot11WlanEntry->MibObject.
             u4CapwapDot11WlanProfileId, &i4RowStatus) == SNMP_SUCCESS)
        {
            if ((pWsscfgSetCapwapDot11WlanEntry->MibObject.
                 i4CapwapDot11WlanRowStatus == CREATE_AND_GO)
                || (pWsscfgSetCapwapDot11WlanEntry->MibObject.
                    i4CapwapDot11WlanRowStatus == CREATE_AND_WAIT))
            {
                CLI_SET_ERR (CLI_WSSCFG_ROW_CREATED);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }
        }
        else
        {
            if ((pWsscfgSetCapwapDot11WlanEntry->MibObject.
                 i4CapwapDot11WlanRowStatus != CREATE_AND_GO)
                && (pWsscfgSetCapwapDot11WlanEntry->MibObject.
                    i4CapwapDot11WlanRowStatus != CREATE_AND_WAIT))
            {
                CLI_SET_ERR (CLI_WSSCFG_WLAN_NO_CREATE);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return OSIX_FAILURE;
            }
        }
        if (pWsscfgSetCapwapDot11WlanEntry->
            MibObject.i4CapwapDot11WlanRowStatus == ACTIVE)
        {
            nmhGetCapwapDot11WlanTunnelMode
                (pWsscfgSetCapwapDot11WlanEntry->MibObject.
                 u4CapwapDot11WlanProfileId, &RetValCapwapDot11WlanTunnelMode);

            if (nmhGetCapwapDot11WlanMacType
                (pWsscfgSetCapwapDot11WlanEntry->MibObject.
                 u4CapwapDot11WlanProfileId, &i4MacType) != SNMP_SUCCESS)
            {
            }
            if ((i4MacType > 1)
                &&
                ((pWsscfgSetCapwapDot11WlanEntry->MibObject.
                  i4CapwapDot11WlanMacType) > 1))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return OSIX_FAILURE;
            }

            if ((pWsscfgIsSetCapwapDot11WlanEntry->
                 bCapwapDot11WlanTunnelMode != OSIX_FALSE) &&
                (pWsscfgIsSetCapwapDot11WlanEntry->
                 bCapwapDot11WlanMacType != OSIX_FALSE))
            {
                if ((pWsscfgSetCapwapDot11WlanEntry->
                     MibObject.au1CapwapDot11WlanTunnelMode[0]
                     != CLI_WLAN_TUNNEL_MODE_NATIVE) &&
                    (pWsscfgSetCapwapDot11WlanEntry->
                     MibObject.i4CapwapDot11WlanMacType ==
                     CLI_WTP_MAC_TYPE_SPLIT))
                {
                    CLI_SET_ERR (CLI_WSSCFG_INV_TUN_MODE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
                if ((pWsscfgSetCapwapDot11WlanEntry->MibObject.
                     au1CapwapDot11WlanTunnelMode[0] !=
                     CLI_WLAN_TUNNEL_MODE_DOT3) &&
                    (pWsscfgSetCapwapDot11WlanEntry->MibObject.
                     i4CapwapDot11WlanMacType == CLI_WTP_MAC_TYPE_LOCAL))
                {
                    CLI_SET_ERR (CLI_WSSCFG_INV_TUN_MODE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
            }
            else if ((pWsscfgIsSetCapwapDot11WlanEntry->
                      bCapwapDot11WlanTunnelMode != OSIX_FALSE)
                     && (pWsscfgIsSetCapwapDot11WlanEntry->
                         bCapwapDot11WlanMacType == OSIX_FALSE))
            {
                if ((pWsscfgSetCapwapDot11WlanEntry->
                     MibObject.au1CapwapDot11WlanTunnelMode[0]
                     != CLI_WLAN_TUNNEL_MODE_NATIVE) &&
                    (i4MacType == CLI_WTP_MAC_TYPE_SPLIT))
                {
                    CLI_SET_ERR (CLI_WSSCFG_INV_TUN_MODE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
                if ((pWsscfgSetCapwapDot11WlanEntry->MibObject.
                     au1CapwapDot11WlanTunnelMode[0] !=
                     CLI_WLAN_TUNNEL_MODE_DOT3) &&
                    (i4MacType == CLI_WTP_MAC_TYPE_LOCAL))
                {
                    CLI_SET_ERR (CLI_WSSCFG_INV_TUN_MODE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }

            }
            else if ((pWsscfgIsSetCapwapDot11WlanEntry->
                      bCapwapDot11WlanTunnelMode == OSIX_FALSE)
                     && (pWsscfgIsSetCapwapDot11WlanEntry->
                         bCapwapDot11WlanMacType != OSIX_FALSE))
            {
                if ((RetValCapwapDot11WlanTunnelMode.pu1_OctetList[0]
                     != CLI_WLAN_TUNNEL_MODE_NATIVE) &&
                    (pWsscfgSetCapwapDot11WlanEntry->MibObject.
                     i4CapwapDot11WlanMacType == CLI_WTP_MAC_TYPE_SPLIT))
                {
                    CLI_SET_ERR (CLI_WSSCFG_INV_TUN_MODE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
                if ((RetValCapwapDot11WlanTunnelMode.pu1_OctetList[0] !=
                     CLI_WLAN_TUNNEL_MODE_DOT3) &&
                    (pWsscfgSetCapwapDot11WlanEntry->MibObject.
                     i4CapwapDot11WlanMacType == CLI_WTP_MAC_TYPE_LOCAL))
                {
                    CLI_SET_ERR (CLI_WSSCFG_INV_TUN_MODE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }

            }
            else
            {
                if ((RetValCapwapDot11WlanTunnelMode.pu1_OctetList[0]
                     != CLI_WLAN_TUNNEL_MODE_NATIVE) &&
                    (i4MacType == CLI_WTP_MAC_TYPE_SPLIT))
                {
                    CLI_SET_ERR (CLI_WSSCFG_INV_TUN_MODE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
                if ((RetValCapwapDot11WlanTunnelMode.pu1_OctetList[0] !=
                     CLI_WLAN_TUNNEL_MODE_DOT3) &&
                    (i4MacType == CLI_WTP_MAC_TYPE_LOCAL))
                {
                    CLI_SET_ERR (CLI_WSSCFG_INV_TUN_MODE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
            }
            nmhGetCapwapDot11WlanProfileIfIndex
                (pWsscfgSetCapwapDot11WlanEntry->MibObject.
                 u4CapwapDot11WlanProfileId, &i4WlanIfIndex);
            WssCfGetDot11DesiredSSID ((UINT4) i4WlanIfIndex,
                                      au1Dot11DesiredSSID);
            if (STRLEN (au1Dot11DesiredSSID) == 0)
            {
                CLI_SET_ERR (CLI_WSSCFG_NO_SSID);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return OSIX_FAILURE;
            }
        }
    }
    if ((pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanRowStatus ==
         OSIX_FALSE)
        &&
        ((pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanTunnelMode ==
          OSIX_TRUE)
         || (pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanMacType ==
             OSIX_TRUE)))
    {
        if ((nmhGetCapwapDot11WlanRowStatus (pWsscfgSetCapwapDot11WlanEntry->
                                             MibObject.
                                             u4CapwapDot11WlanProfileId,
                                             &i4RetValCapwapDot11WlanRowStatus))
            != SNMP_SUCCESS)

        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        if (i4RetValCapwapDot11WlanRowStatus == ACTIVE)
        {
            CLI_SET_ERR (CLI_WSSCFG_NO_ACCESS);
            return OSIX_FAILURE;
        }
    }

    if (pWsscfgSetCapwapDot11WlanEntry->
        MibObject.i4CapwapDot11WlanRowStatus == DESTROY)
    {
        WsscfgGetFsDot11ManagmentSSID (&u4ManagmentProfileId);
        if (u4ManagmentProfileId ==
            pWsscfgSetCapwapDot11WlanEntry->
            MibObject.u4CapwapDot11WlanProfileId)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return OSIX_FAILURE;
        }
        if (nmhGetFirstIndexFsSecurityWebAuthGuestInfoTable (&NextWebAuthUName)
            == SNMP_SUCCESS)
        {
            do
            {
                WebAuthUName = NextWebAuthUName;
                nmhGetFsSecurityWlanProfileId (&WebAuthUName,
                                               &i4FsSecurityWlanProfileId);
                if (pWsscfgSetCapwapDot11WlanEntry->
                    MibObject.u4CapwapDot11WlanProfileId
                    == (UINT4) i4FsSecurityWlanProfileId)
                {
                    CLI_SET_ERR (CLI_WSSCFG_WEBAUTH_ERR);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }

            }
            while (nmhGetNextIndexFsSecurityWebAuthGuestInfoTable
                   (&WebAuthUName, &NextWebAuthUName) == SNMP_SUCCESS);
        }
    }

    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllCapwapDot11WlanBindTable
 Input       :  pu4ErrorCode
                pWsscfgSetCapwapDot11WlanBindEntry
                pWsscfgIsSetCapwapDot11WlanBindEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllCapwapDot11WlanBindTable (UINT4 *pu4ErrorCode,
                                       tWsscfgCapwapDot11WlanBindEntry *
                                       pWsscfgSetCapwapDot11WlanBindEntry,
                                       tWsscfgIsSetCapwapDot11WlanBindEntry *
                                       pWsscfgIsSetCapwapDot11WlanBindEntry,
                                       INT4 i4RowStatusLogic,
                                       INT4 i4RowCreateOption)
{
    UINT4               u4WlanProfileId;
    UINT4               u4WlanRowStatus = 0;
    INT4                i4WlanIfIndex = 0;
    INT4                i4QosEnable = 0;
    INT4                i4AdminStatus = CFA_IF_DOWN;
    tRadioIfGetDB       RadioIfGetDB;

    if (pWsscfgIsSetCapwapDot11WlanBindEntry->bCapwapDot11WlanBindRowStatus !=
        OSIX_FALSE)
    {
        u4WlanProfileId =
            pWsscfgSetCapwapDot11WlanBindEntry->
            MibObject.u4CapwapDot11WlanProfileId;
        if (nmhGetCapwapDot11WlanRowStatus (u4WlanProfileId, (INT4 *)
                                            &u4WlanRowStatus) != SNMP_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        if (u4WlanRowStatus != ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
        /*
         *  Added validation for QoS enable in bgn/an/ac mode 
         */
        MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWsscfgSetCapwapDot11WlanBindEntry->MibObject.i4IfIndex;
        RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;

        if (WssCfgGetRadioParams (&RadioIfGetDB) == OSIX_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        if ((RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
             RADIO_TYPE_BGN) ||
            (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType ==
             RADIO_TYPE_AN) ||
            (RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType == RADIO_TYPE_AC))
        {
            if (nmhGetCapwapDot11WlanProfileIfIndex (u4WlanProfileId,
                                                     &i4WlanIfIndex) !=
                SNMP_SUCCESS)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

            if (nmhGetFsDot11WlanQosOptionImplemented (i4WlanIfIndex,
                                                       &i4QosEnable) !=
                SNMP_SUCCESS)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

            if (i4QosEnable == 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
        /* FIX for 3333 */
        nmhGetIfMainAdminStatus (pWsscfgSetCapwapDot11WlanBindEntry->
                                 MibObject.i4IfIndex, &i4AdminStatus);
        if (i4AdminStatus != CFA_IF_UP)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
    }
    UNUSED_PARAM (pWsscfgSetCapwapDot11WlanBindEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11StationConfigTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11StationConfigEntry
                pWsscfgIsSetDot11StationConfigEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11StationConfigTable (UINT4 *pu4ErrorCode,
                                      tWsscfgDot11StationConfigEntry *
                                      pWsscfgSetDot11StationConfigEntry,
                                      tWsscfgIsSetDot11StationConfigEntry *
                                      pWsscfgIsSetDot11StationConfigEntry)
{
    UINT4               u4Dot11RadioType = 0;
    INT4                i4IfType = 0;

    tCliHandle          CliHandle;
    MEMSET (&CliHandle, 0, sizeof (tCliHandle));

    /* Get the radio type for the input received */

    if ((pWsscfgIsSetDot11StationConfigEntry->
         bDot11OperationalRateSet != OSIX_FALSE) ||
        (pWsscfgIsSetDot11StationConfigEntry->bDot11BeaconPeriod != OSIX_FALSE))
    {
        if (nmhGetIfMainType (pWsscfgSetDot11StationConfigEntry->
                              MibObject.i4IfIndex, &i4IfType) == SUCCESS)
        {
            if (i4IfType == 154)
            {
                if (nmhGetFsDot11RadioType (pWsscfgSetDot11StationConfigEntry->
                                            MibObject.i4IfIndex,
                                            &u4Dot11RadioType) != SNMP_SUCCESS)
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return OSIX_FAILURE;
                }
            }
        }
    }

    if (pWsscfgIsSetDot11StationConfigEntry->bDot11StationID != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11StationConfigEntry->
        bDot11MediumOccupancyLimit != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11CFPPeriod != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11CFPMaxDuration != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11StationConfigEntry->
        bDot11AuthenticationResponseTimeOut != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11StationConfigEntry->
        bDot11PowerManagementMode != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11DesiredSSID != OSIX_FALSE)
    {
        if (STRLEN
            (pWsscfgSetDot11StationConfigEntry->
             MibObject.au1Dot11DesiredSSID) > CLI_MAX_SSID_LEN)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11DesiredBSSType != OSIX_FALSE)
    {
        /*Fill your check condition */
    }

    if (pWsscfgIsSetDot11StationConfigEntry->
        bDot11OperationalRateSet != OSIX_FALSE)
    {
        if (STRLEN
            (pWsscfgSetDot11StationConfigEntry->
             MibObject.au1Dot11OperationalRateSet) > CLI_MAX_OPE_RATE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;

        }

        if ((u4Dot11RadioType == CLI_RADIO_TYPEB) ||
            (u4Dot11RadioType == CLI_RADIO_TYPEBG) ||
            (u4Dot11RadioType == CLI_RADIO_TYPEBGN))
        {
            if (((pWsscfgSetDot11StationConfigEntry->
                  MibObject.au1Dot11OperationalRateSet[CLI_OPER_INDEX_1MB] !=
                  CLI_OPER_RATEB_1MB)
                 && (pWsscfgSetDot11StationConfigEntry->MibObject.
                     au1Dot11OperationalRateSet[CLI_OPER_INDEX_1MB] != 0))
                ||
                ((pWsscfgSetDot11StationConfigEntry->MibObject.
                  au1Dot11OperationalRateSet[CLI_OPER_INDEX_2MB] !=
                  CLI_OPER_RATEB_2MB)
                 && (pWsscfgSetDot11StationConfigEntry->MibObject.
                     au1Dot11OperationalRateSet[CLI_OPER_INDEX_2MB] != 0))
                ||
                ((pWsscfgSetDot11StationConfigEntry->MibObject.
                  au1Dot11OperationalRateSet[CLI_OPER_INDEX_5MB] !=
                  CLI_OPER_RATEB_5MB)
                 && (pWsscfgSetDot11StationConfigEntry->MibObject.
                     au1Dot11OperationalRateSet[CLI_OPER_INDEX_5MB] != 0))
                ||
                ((pWsscfgSetDot11StationConfigEntry->MibObject.
                  au1Dot11OperationalRateSet[CLI_OPER_INDEX_11MB] !=
                  CLI_OPER_RATEB_11MB)
                 && (pWsscfgSetDot11StationConfigEntry->MibObject.
                     au1Dot11OperationalRateSet[CLI_OPER_INDEX_11MB] != 0)))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }
        }

        if ((u4Dot11RadioType == CLI_RADIO_TYPEA) ||
            (u4Dot11RadioType == CLI_RADIO_TYPEA) ||
            (u4Dot11RadioType == CLI_RADIO_TYPEG))

        {
            if (((pWsscfgSetDot11StationConfigEntry->
                  MibObject.au1Dot11OperationalRateSet[CLI_OPER_INDEX_6MB] !=
                  CLI_OPER_RATEA_6MB)
                 && (pWsscfgSetDot11StationConfigEntry->MibObject.
                     au1Dot11OperationalRateSet[CLI_OPER_INDEX_6MB] != 0))
                ||
                ((pWsscfgSetDot11StationConfigEntry->MibObject.
                  au1Dot11OperationalRateSet[CLI_OPER_INDEX_9MB] !=
                  CLI_OPER_RATEA_9MB)
                 && (pWsscfgSetDot11StationConfigEntry->MibObject.
                     au1Dot11OperationalRateSet[CLI_OPER_INDEX_9MB] != 0))
                ||
                ((pWsscfgSetDot11StationConfigEntry->MibObject.
                  au1Dot11OperationalRateSet[CLI_OPER_INDEX_12MB] !=
                  CLI_OPER_RATEA_12MB)
                 && (pWsscfgSetDot11StationConfigEntry->MibObject.
                     au1Dot11OperationalRateSet[CLI_OPER_INDEX_12MB] != 0))
                ||
                ((pWsscfgSetDot11StationConfigEntry->MibObject.
                  au1Dot11OperationalRateSet[CLI_OPER_INDEX_18MB] !=
                  CLI_OPER_RATEA_18MB)
                 && (pWsscfgSetDot11StationConfigEntry->MibObject.
                     au1Dot11OperationalRateSet[CLI_OPER_INDEX_18MB] != 0))
                ||
                ((pWsscfgSetDot11StationConfigEntry->MibObject.
                  au1Dot11OperationalRateSet[CLI_OPER_INDEX_24MB] !=
                  CLI_OPER_RATEA_24MB)
                 && (pWsscfgSetDot11StationConfigEntry->MibObject.
                     au1Dot11OperationalRateSet[CLI_OPER_INDEX_24MB] != 0))
                ||
                ((pWsscfgSetDot11StationConfigEntry->MibObject.
                  au1Dot11OperationalRateSet[CLI_OPER_INDEX_36MB] !=
                  CLI_OPER_RATEA_36MB)
                 && (pWsscfgSetDot11StationConfigEntry->MibObject.
                     au1Dot11OperationalRateSet[CLI_OPER_INDEX_36MB] != 0))
                ||
                ((pWsscfgSetDot11StationConfigEntry->MibObject.
                  au1Dot11OperationalRateSet[CLI_OPER_INDEX_48MB] !=
                  CLI_OPER_RATEA_48MB)
                 && (pWsscfgSetDot11StationConfigEntry->MibObject.
                     au1Dot11OperationalRateSet[CLI_OPER_INDEX_48MB] != 0))
                ||
                ((pWsscfgSetDot11StationConfigEntry->MibObject.
                  au1Dot11OperationalRateSet[CLI_OPER_INDEX_54MB] !=
                  CLI_OPER_RATEA_54MB)
                 && (pWsscfgSetDot11StationConfigEntry->MibObject.
                     au1Dot11OperationalRateSet[CLI_OPER_INDEX_54MB] != 0)))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }
        }

    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11BeaconPeriod != OSIX_FALSE)
    {
        if ((pWsscfgSetDot11StationConfigEntry->
             MibObject.i4Dot11BeaconPeriod < CLI_MIN_BEACON_PERIOD)
            || (pWsscfgSetDot11StationConfigEntry->
                MibObject.i4Dot11BeaconPeriod > CLI_MAX_BEACON_PERIOD))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11DTIMPeriod != OSIX_FALSE)
    {
        if ((pWsscfgSetDot11StationConfigEntry->
             MibObject.i4Dot11DTIMPeriod < 1)
            || (pWsscfgSetDot11StationConfigEntry->
                MibObject.i4Dot11DTIMPeriod > 255))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11AssociationResponseTimeOut !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11StationConfigEntry->
        bDot11MultiDomainCapabilityImplemented != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11StationConfigEntry->
        bDot11MultiDomainCapabilityEnabled != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11SpectrumManagementRequired !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11StationConfigEntry->
        bDot11RegulatoryClassesImplemented != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11RegulatoryClassesRequired !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11AssociateinNQBSS !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11DLSAllowedInQBSS !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11DLSAllowed != OSIX_FALSE)
    {
        /*Fill your check condition */
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11AuthenticationAlgorithmsTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11AuthenticationAlgorithmsEntry
                pWsscfgIsSetDot11AuthenticationAlgorithmsEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11AuthenticationAlgorithmsTable (UINT4 *pu4ErrorCode,
                                                 tWsscfgDot11AuthenticationAlgorithmsEntry
                                                 *
                                                 pWsscfgSetDot11AuthenticationAlgorithmsEntry,
                                                 tWsscfgIsSetDot11AuthenticationAlgorithmsEntry
                                                 *
                                                 pWsscfgIsSetDot11AuthenticationAlgorithmsEntry)
{
    INT4                i4Dot11RSNAEnabled = RSNA_DISABLED;

    /*Fill your check condition */
    if (pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->
        bDot11AuthenticationAlgorithmsEnable != OSIX_FALSE)
    {
        if ((pWsscfgSetDot11AuthenticationAlgorithmsEntry->
             MibObject.i4Dot11AuthenticationAlgorithmsEnable !=
             TRUE)
            && (pWsscfgSetDot11AuthenticationAlgorithmsEntry->
                MibObject.i4Dot11AuthenticationAlgorithmsEnable != FALSE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    if (pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->
        bDot11AuthenticationAlgorithm != OSIX_FALSE)
    {
        if ((pWsscfgSetDot11AuthenticationAlgorithmsEntry->
             MibObject.i4Dot11AuthenticationAlgorithm !=
             CLI_AUTH_ALGO_SHARED)
            && (pWsscfgSetDot11AuthenticationAlgorithmsEntry->
                MibObject.i4Dot11AuthenticationAlgorithm != CLI_AUTH_ALGO_OPEN))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

#ifdef  RSNA_WANTED
    /* Get the RSNA Enable/Disable Status for the specified profile */
    RsnaGetDot11RSNAEnabled (pWsscfgSetDot11AuthenticationAlgorithmsEntry->
                             MibObject.i4IfIndex, &i4Dot11RSNAEnabled);

    /* Fix to Verify whether RSNA Disabled before Setting Auth Algorithm to Shared-key Authentication */
    if ((pWsscfgSetDot11AuthenticationAlgorithmsEntry->MibObject.
         i4Dot11AuthenticationAlgorithm == CLI_AUTH_ALGO_SHARED
         && i4Dot11RSNAEnabled == RSNA_ENABLED))
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgTestAllDot11AuthenticationAlgorithmsTable:"
                     "RSNA should be DISABLED to set Shared-key Auth Alogrithm"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }
#endif

    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetDot11AuthenticationAlgorithmsEntry);
    UNUSED_PARAM (i4Dot11RSNAEnabled);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11WEPDefaultKeysTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11WEPDefaultKeysEntry
                pWsscfgIsSetDot11WEPDefaultKeysEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11WEPDefaultKeysTable (UINT4 *pu4ErrorCode,
                                       tWsscfgDot11WEPDefaultKeysEntry *
                                       pWsscfgSetDot11WEPDefaultKeysEntry,
                                       tWsscfgIsSetDot11WEPDefaultKeysEntry *
                                       pWsscfgIsSetDot11WEPDefaultKeysEntry)
{
    if (pWsscfgIsSetDot11WEPDefaultKeysEntry->
        bDot11WEPDefaultKeyValue != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetDot11WEPDefaultKeysEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11WEPKeyMappingsTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11WEPKeyMappingsEntry
                pWsscfgIsSetDot11WEPKeyMappingsEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11WEPKeyMappingsTable (UINT4 *pu4ErrorCode,
                                       tWsscfgDot11WEPKeyMappingsEntry *
                                       pWsscfgSetDot11WEPKeyMappingsEntry,
                                       tWsscfgIsSetDot11WEPKeyMappingsEntry *
                                       pWsscfgIsSetDot11WEPKeyMappingsEntry,
                                       INT4 i4RowStatusLogic,
                                       INT4 i4RowCreateOption)
{
    if (pWsscfgIsSetDot11WEPKeyMappingsEntry->
        bDot11WEPKeyMappingAddress != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11WEPKeyMappingsEntry->
        bDot11WEPKeyMappingWEPOn != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11WEPKeyMappingsEntry->
        bDot11WEPKeyMappingValue != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11WEPKeyMappingsEntry->
        bDot11WEPKeyMappingStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetDot11WEPKeyMappingsEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11PrivacyTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11PrivacyEntry
                pWsscfgIsSetDot11PrivacyEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11PrivacyTable (UINT4 *pu4ErrorCode,
                                tWsscfgDot11PrivacyEntry *
                                pWsscfgSetDot11PrivacyEntry,
                                tWsscfgIsSetDot11PrivacyEntry *
                                pWsscfgIsSetDot11PrivacyEntry)
{
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11PrivacyInvoked != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11WEPDefaultKeyID != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11WEPKeyMappingLength != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11ExcludeUnencrypted != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11RSNAEnabled != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11RSNAPreauthenticationEnabled !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetDot11PrivacyEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11MultiDomainCapabilityTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11MultiDomainCapabilityEntry
                pWsscfgIsSetDot11MultiDomainCapabilityEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11MultiDomainCapabilityTable (UINT4 *pu4ErrorCode,
                                              tWsscfgDot11MultiDomainCapabilityEntry
                                              *
                                              pWsscfgSetDot11MultiDomainCapabilityEntry,
                                              tWsscfgIsSetDot11MultiDomainCapabilityEntry
                                              *
                                              pWsscfgIsSetDot11MultiDomainCapabilityEntry)
{
    if (pWsscfgIsSetDot11MultiDomainCapabilityEntry->bDot11FirstChannelNumber !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11MultiDomainCapabilityEntry->bDot11NumberofChannels !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11MultiDomainCapabilityEntry->
        bDot11MaximumTransmitPowerLevel != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetDot11MultiDomainCapabilityEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11SpectrumManagementTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11SpectrumManagementEntry
                pWsscfgIsSetDot11SpectrumManagementEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11SpectrumManagementTable (UINT4 *pu4ErrorCode,
                                           tWsscfgDot11SpectrumManagementEntry *
                                           pWsscfgSetDot11SpectrumManagementEntry,
                                           tWsscfgIsSetDot11SpectrumManagementEntry
                                           *
                                           pWsscfgIsSetDot11SpectrumManagementEntry)
{
    if (pWsscfgIsSetDot11SpectrumManagementEntry->bDot11MitigationRequirement !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetDot11SpectrumManagementEntry->
             MibObject.i4Dot11MitigationRequirement > 30)
            || (pWsscfgSetDot11SpectrumManagementEntry->
                MibObject.i4Dot11MitigationRequirement < 0))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;

        }

    }
    if (pWsscfgIsSetDot11SpectrumManagementEntry->bDot11ChannelSwitchTime !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11RegulatoryClassesTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11RegulatoryClassesEntry
                pWsscfgIsSetDot11RegulatoryClassesEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11RegulatoryClassesTable (UINT4 *pu4ErrorCode,
                                          tWsscfgDot11RegulatoryClassesEntry *
                                          pWsscfgSetDot11RegulatoryClassesEntry,
                                          tWsscfgIsSetDot11RegulatoryClassesEntry
                                          *
                                          pWsscfgIsSetDot11RegulatoryClassesEntry)
{
    if (pWsscfgIsSetDot11RegulatoryClassesEntry->
        bDot11RegulatoryClass != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11RegulatoryClassesEntry->
        bDot11CoverageClass != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetDot11RegulatoryClassesEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11OperationTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11OperationEntry
                pWsscfgIsSetDot11OperationEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11OperationTable (UINT4 *pu4ErrorCode,
                                  tWsscfgDot11OperationEntry *
                                  pWsscfgSetDot11OperationEntry,
                                  tWsscfgIsSetDot11OperationEntry *
                                  pWsscfgIsSetDot11OperationEntry)
{
    if (pWsscfgIsSetDot11OperationEntry->bDot11RTSThreshold != OSIX_FALSE)
    {
        if ((pWsscfgSetDot11OperationEntry->MibObject.
             i4Dot11RTSThreshold < 0)
            || (pWsscfgSetDot11OperationEntry->
                MibObject.i4Dot11RTSThreshold > CLI_RTS_MAX_VALUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11ShortRetryLimit != OSIX_FALSE)
    {
        if ((pWsscfgSetDot11OperationEntry->
             MibObject.i4Dot11ShortRetryLimit < 1)
            || (pWsscfgSetDot11OperationEntry->
                MibObject.i4Dot11ShortRetryLimit > CLI_RETRY_MAX_VALUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11LongRetryLimit != OSIX_FALSE)
    {
        if ((pWsscfgSetDot11OperationEntry->
             MibObject.i4Dot11LongRetryLimit < 1)
            || (pWsscfgSetDot11OperationEntry->
                MibObject.i4Dot11LongRetryLimit > CLI_RETRY_MAX_VALUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetDot11OperationEntry->
        bDot11FragmentationThreshold != OSIX_FALSE)
    {
        if ((pWsscfgSetDot11OperationEntry->
             MibObject.i4Dot11FragmentationThreshold <
             CLI_MIN_FRAGMENT_VALUE)
            || (pWsscfgSetDot11OperationEntry->
                MibObject.i4Dot11FragmentationThreshold >
                CLI_MAX_FRAGMENT_VALUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetDot11OperationEntry->
        bDot11MaxTransmitMSDULifetime != OSIX_FALSE)
    {
        if ((pWsscfgSetDot11OperationEntry->
             MibObject.u4Dot11MaxTransmitMSDULifetime <
             CLI_MIN_TX_MSDU_VALUE)
            || (pWsscfgSetDot11OperationEntry->
                MibObject.u4Dot11MaxTransmitMSDULifetime >
                CLI_MAX_TX_MSDU_VALUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11MaxReceiveLifetime != OSIX_FALSE)
    {
        if ((pWsscfgSetDot11OperationEntry->
             MibObject.u4Dot11MaxReceiveLifetime <
             CLI_MIN_RX_MSDU_VALUE)
            || (pWsscfgSetDot11OperationEntry->
                MibObject.u4Dot11MaxReceiveLifetime > CLI_MAX_RX_MSDU_VALUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11CAPLimit != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11HCCWmin != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11HCCWmax != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11HCCAIFSN != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11ADDBAResponseTimeout !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11ADDTSResponseTimeout !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11OperationEntry->
        bDot11ChannelUtilizationBeaconInterval != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11ScheduleTimeout != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11DLSResponseTimeout != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11OperationEntry->
        bDot11QAPMissingAckRetryLimit != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11EDCAAveragingPeriod !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetDot11OperationEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11GroupAddressesTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11GroupAddressesEntry
                pWsscfgIsSetDot11GroupAddressesEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11GroupAddressesTable (UINT4 *pu4ErrorCode,
                                       tWsscfgDot11GroupAddressesEntry *
                                       pWsscfgSetDot11GroupAddressesEntry,
                                       tWsscfgIsSetDot11GroupAddressesEntry *
                                       pWsscfgIsSetDot11GroupAddressesEntry,
                                       INT4 i4RowStatusLogic,
                                       INT4 i4RowCreateOption)
{
    if (pWsscfgIsSetDot11GroupAddressesEntry->bDot11Address != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11GroupAddressesEntry->
        bDot11GroupAddressesStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetDot11GroupAddressesEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11EDCATable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11EDCAEntry
                pWsscfgIsSetDot11EDCAEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11EDCATable (UINT4 *pu4ErrorCode,
                             tWsscfgDot11EDCAEntry * pWsscfgSetDot11EDCAEntry,
                             tWsscfgIsSetDot11EDCAEntry *
                             pWsscfgIsSetDot11EDCAEntry)
{

#ifdef UNUSED_CODE
/* If configuaration is allowed, following must be uncommented */
    if ((pWsscfgSetDot11EDCAEntry->MibObject.i4Dot11EDCATableIndex <
         0) || (pWsscfgSetDot11EDCAEntry->MibObject.i4Dot11EDCATableIndex > 4))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableCWmin != OSIX_FALSE)
    {
        if ((pWsscfgSetDot11EDCAEntry->MibObject.
             i4Dot11EDCATableCWmin > 255)
            || (pWsscfgSetDot11EDCAEntry->MibObject.i4Dot11EDCATableCWmin < 0))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableCWmax != OSIX_FALSE)
    {
        if ((pWsscfgSetDot11EDCAEntry->MibObject.
             i4Dot11EDCATableCWmax > 65535)
            || (pWsscfgSetDot11EDCAEntry->MibObject.i4Dot11EDCATableCWmax < 0))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableAIFSN != OSIX_FALSE)
    {
        if (((pWsscfgSetDot11EDCAEntry->MibObject.
              i4Dot11EDCATableAIFSN) > 15)
            &&
            ((pWsscfgSetDot11EDCAEntry->MibObject.i4Dot11EDCATableAIFSN) < 2))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableTXOPLimit != OSIX_FALSE)
    {
        if ((pWsscfgSetDot11EDCAEntry->
             MibObject.i4Dot11EDCATableTXOPLimit > 65535)
            || (pWsscfgSetDot11EDCAEntry->
                MibObject.i4Dot11EDCATableTXOPLimit < 0))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableMSDULifetime != OSIX_FALSE)
    {
        if ((pWsscfgSetDot11EDCAEntry->
             MibObject.i4Dot11EDCATableMSDULifetime) > 500)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableMandatory != OSIX_FALSE)
    {
        if ((pWsscfgSetDot11EDCAEntry->
             MibObject.i4Dot11EDCATableMandatory != 1)
            && (pWsscfgSetDot11EDCAEntry->
                MibObject.i4Dot11EDCATableMandatory != 2))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
#else
    UNUSED_PARAM (pWsscfgSetDot11EDCAEntry);
    UNUSED_PARAM (pWsscfgIsSetDot11EDCAEntry);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_WSSCFG_CONFIG_NOT_ALLOWED);
    return OSIX_FAILURE;

#endif
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11QAPEDCATable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11QAPEDCAEntry
                pWsscfgIsSetDot11QAPEDCAEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11QAPEDCATable (UINT4 *pu4ErrorCode,
                                tWsscfgDot11QAPEDCAEntry *
                                pWsscfgSetDot11QAPEDCAEntry,
                                tWsscfgIsSetDot11QAPEDCAEntry *
                                pWsscfgIsSetDot11QAPEDCAEntry)
{
    if (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableCWmin != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableCWmax != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableAIFSN != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableTXOPLimit !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->
        bDot11QAPEDCATableMSDULifetime != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableMandatory !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetDot11QAPEDCAEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11PhyOperationTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11PhyOperationEntry
                pWsscfgIsSetDot11PhyOperationEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11PhyOperationTable (UINT4 *pu4ErrorCode,
                                     tWsscfgDot11PhyOperationEntry *
                                     pWsscfgSetDot11PhyOperationEntry,
                                     tWsscfgIsSetDot11PhyOperationEntry *
                                     pWsscfgIsSetDot11PhyOperationEntry)
{
    if (pWsscfgIsSetDot11PhyOperationEntry->bDot11CurrentRegDomain !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetDot11PhyOperationEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11PhyAntennaTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11PhyAntennaEntry
                pWsscfgIsSetDot11PhyAntennaEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11PhyAntennaTable (UINT4 *pu4ErrorCode,
                                   tWsscfgDot11PhyAntennaEntry *
                                   pWsscfgSetDot11PhyAntennaEntry,
                                   tWsscfgIsSetDot11PhyAntennaEntry *
                                   pWsscfgIsSetDot11PhyAntennaEntry)
{
    if (pWsscfgIsSetDot11PhyAntennaEntry->bDot11CurrentTxAntenna != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PhyAntennaEntry->bDot11CurrentRxAntenna != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetDot11PhyAntennaEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11PhyTxPowerTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11PhyTxPowerEntry
                pWsscfgIsSetDot11PhyTxPowerEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11PhyTxPowerTable (UINT4 *pu4ErrorCode,
                                   tWsscfgDot11PhyTxPowerEntry *
                                   pWsscfgSetDot11PhyTxPowerEntry,
                                   tWsscfgIsSetDot11PhyTxPowerEntry *
                                   pWsscfgIsSetDot11PhyTxPowerEntry)
{
    if (pWsscfgIsSetDot11PhyTxPowerEntry->bDot11CurrentTxPowerLevel !=
        OSIX_FALSE)
    {
        if (pWsscfgSetDot11PhyTxPowerEntry->
            MibObject.i4Dot11CurrentTxPowerLevel <
            CLI_MIN_TXPOWER_LEVEL
            || pWsscfgSetDot11PhyTxPowerEntry->
            MibObject.i4Dot11CurrentTxPowerLevel > CLI_MAX_TXPOWER_LEVEL)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11PhyFHSSTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11PhyFHSSEntry
                pWsscfgIsSetDot11PhyFHSSEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11PhyFHSSTable (UINT4 *pu4ErrorCode,
                                tWsscfgDot11PhyFHSSEntry *
                                pWsscfgSetDot11PhyFHSSEntry,
                                tWsscfgIsSetDot11PhyFHSSEntry *
                                pWsscfgIsSetDot11PhyFHSSEntry)
{
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentChannelNumber != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentDwellTime != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentSet != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentPattern != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentIndex != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCPrimeRadix != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCNumberofChannelsFamilyIndex !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->
        bDot11EHCCCapabilityImplemented != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCCapabilityEnabled !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11HopAlgorithmAdopted != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11RandomTableFlag != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11HopOffset != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetDot11PhyFHSSEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11PhyDSSSTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11PhyDSSSEntry
                pWsscfgIsSetDot11PhyDSSSEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11PhyDSSSTable (UINT4 *pu4ErrorCode,
                                tWsscfgDot11PhyDSSSEntry *
                                pWsscfgSetDot11PhyDSSSEntry,
                                tWsscfgIsSetDot11PhyDSSSEntry *
                                pWsscfgIsSetDot11PhyDSSSEntry)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               au1AllowedChannelList[RADIOIF_MAX_CHANNEL_B];
    INT1                i1FrequencyCount = 0;
    INT1                i1FrequencyIndex = 0;
    UINT1               u1CountryCount = 0;
    UINT1               u1CountryIndex = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&au1AllowedChannelList, 0, RADIOIF_MAX_CHANNEL_B);

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        (UINT4) (pWsscfgSetDot11PhyDSSSEntry->MibObject.i4IfIndex);
    RadioIfGetDB.RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  (&RadioIfGetDB)) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (RadioIfGetDB.RadioIfGetAllDB.au1CountryString[0] == '\0')
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }

    if (pWsscfgIsSetDot11PhyDSSSEntry->bDot11CurrentChannel != OSIX_FALSE)
    {

        if (pWsscfgSetDot11PhyDSSSEntry->MibObject.
            i4Dot11CurrentChannel < RADIO_MIN_VALUE_CHANNELB ||
            pWsscfgSetDot11PhyDSSSEntry->MibObject.
            i4Dot11CurrentChannel > RADIO_MAX_VALUE_CHANNELB)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        i1FrequencyCount = sizeof (gau1Dot11bRegDomainChannelList);

        u1CountryCount = (UINT1) (RADIOIF_SUPPORTED_COUNTRY_LIST);

        for (i1FrequencyIndex = 0;
             i1FrequencyIndex < i1FrequencyCount; i1FrequencyIndex++)
        {
            if (pWsscfgSetDot11PhyDSSSEntry->MibObject.
                i4Dot11CurrentChannel ==
                gau1Dot11bRegDomainChannelList[i1FrequencyIndex])
            {
                break;
            }
        }
        for (u1CountryIndex = 0;
             u1CountryIndex < u1CountryCount; u1CountryIndex++)
        {
            if (MEMCMP (RadioIfGetDB.RadioIfGetAllDB.au1CountryString,
                        gWsscfgSupportedCountryList[u1CountryIndex].
                        au1CountryCode, RADIO_COUNTRY_LENGTH) == 0)
            {
                u1CountryIndex =
                    (UINT1) gRegDomainCountryMapping[u1CountryIndex].
                    u2RegDomain;
                break;
            }
        }
        /* If the above search does not find the frequency allowed for
         * the country then return failure */
        if ((u1CountryIndex == (UINT1) RADIOIF_SUPPORTED_COUNTRY_LIST) ||
            (i1FrequencyIndex == RADIOIF_MAX_CHANNEL_B))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /* If the configured channel is not allowed for the country 
         * then return failure */
        if (gau1Dot11bChannelRegDomainCheckList[u1CountryIndex]
            [i1FrequencyIndex] == 0)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        if (RadioIfValidateAllowedChannels
            (RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex,
             (UINT1) pWsscfgSetDot11PhyDSSSEntry->MibObject.
             i4Dot11CurrentChannel, au1AllowedChannelList) == OSIX_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetDot11PhyDSSSEntry->bDot11CurrentCCAMode != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PhyDSSSEntry->bDot11EDThreshold != OSIX_FALSE)
    {
        if (pWsscfgSetDot11PhyDSSSEntry->MibObject.
            i4Dot11EDThreshold > EDTHRESHOLD_MIN_VALUE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        if (pWsscfgSetDot11PhyDSSSEntry->MibObject.
            i4Dot11EDThreshold < EDTHRESHOLD_MAX_VALUE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    UNUSED_PARAM (pWsscfgSetDot11PhyDSSSEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11PhyIRTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11PhyIREntry
                pWsscfgIsSetDot11PhyIREntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11PhyIRTable (UINT4 *pu4ErrorCode,
                              tWsscfgDot11PhyIREntry *
                              pWsscfgSetDot11PhyIREntry,
                              tWsscfgIsSetDot11PhyIREntry *
                              pWsscfgIsSetDot11PhyIREntry)
{
    if (pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogTimerMax != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogCountMax != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogTimerMin != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogCountMin != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetDot11PhyIREntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11AntennasListTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11AntennasListEntry
                pWsscfgIsSetDot11AntennasListEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11AntennasListTable (UINT4 *pu4ErrorCode,
                                     tWsscfgDot11AntennasListEntry *
                                     pWsscfgSetDot11AntennasListEntry,
                                     tWsscfgIsSetDot11AntennasListEntry *
                                     pWsscfgIsSetDot11AntennasListEntry)
{
    if (pWsscfgIsSetDot11AntennasListEntry->
        bDot11SupportedTxAntenna != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11AntennasListEntry->
        bDot11SupportedRxAntenna != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11AntennasListEntry->
        bDot11DiversitySelectionRx != OSIX_FALSE)
    {
        if ((pWsscfgSetDot11AntennasListEntry->
             MibObject.i4Dot11DiversitySelectionRx !=
             CLI_ANTENNA_DIVERSITY_ENABLE)
            && (pWsscfgSetDot11AntennasListEntry->
                MibObject.i4Dot11DiversitySelectionRx !=
                CLI_ANTENNA_DIVERSITY_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11PhyOFDMTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11PhyOFDMEntry
                pWsscfgIsSetDot11PhyOFDMEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11PhyOFDMTable (UINT4 *pu4ErrorCode,
                                tWsscfgDot11PhyOFDMEntry *
                                pWsscfgSetDot11PhyOFDMEntry,
                                tWsscfgIsSetDot11PhyOFDMEntry *
                                pWsscfgIsSetDot11PhyOFDMEntry)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               au1AllowedChannelList[RADIOIF_MAX_CHANNEL_A];
    INT1                i1FrequencyCount = 0;
    INT1                i1FrequencyIndex = 0;
    UINT1               u1CountryCount = 0;
    UINT1               u1CountryIndex = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&au1AllowedChannelList, 0, RADIOIF_MAX_CHANNEL_A);

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        (UINT4) (pWsscfgSetDot11PhyOFDMEntry->MibObject.i4IfIndex);
    RadioIfGetDB.RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  (&RadioIfGetDB)) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (RadioIfGetDB.RadioIfGetAllDB.au1CountryString[0] == '\0')
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }

    if (pWsscfgIsSetDot11PhyOFDMEntry->bDot11CurrentFrequency != OSIX_FALSE)
    {
        if (pWsscfgSetDot11PhyOFDMEntry->
            MibObject.i4Dot11CurrentFrequency < RADIO_MIN_VALUE_CHANNELA ||
            pWsscfgSetDot11PhyOFDMEntry->
            MibObject.i4Dot11CurrentFrequency > RADIO_MAX_VALUE_CHANNELA)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

        i1FrequencyCount = sizeof (gau1Dot11aRegDomainChannelList);

        u1CountryCount = (UINT1) RADIOIF_SUPPORTED_COUNTRY_LIST;

        for (i1FrequencyIndex = 0;
             i1FrequencyIndex < i1FrequencyCount; i1FrequencyIndex++)
        {
            if (pWsscfgSetDot11PhyOFDMEntry->
                MibObject.i4Dot11CurrentFrequency ==
                gau1Dot11aRegDomainChannelList[i1FrequencyIndex])
            {
                break;
            }
        }
        for (u1CountryIndex = 0;
             u1CountryIndex < u1CountryCount; u1CountryIndex++)
        {
            if (MEMCMP (RadioIfGetDB.RadioIfGetAllDB.au1CountryString,
                        gWsscfgSupportedCountryList[u1CountryIndex].
                        au1CountryCode, RADIO_COUNTRY_LENGTH) == 0)
            {
                u1CountryIndex =
                    (UINT1) gRegDomainCountryMapping[u1CountryIndex].
                    u2RegDomain;
                break;
            }
        }
        /* If the above search does not find the frequency allowed for
         * the country then return failure */
        if ((u1CountryIndex == (UINT1) RADIOIF_SUPPORTED_COUNTRY_LIST) ||
            (i1FrequencyIndex == RADIOIF_MAX_CHANNEL_A))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /* If the configured channel is not allowed for the country 
         * then return failure */
        if (gau1Dot11aChannelCheckList[i1FrequencyIndex] == 0)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        if (RadioIfValidateAllowedChannels
            (RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex,
             (UINT1) pWsscfgSetDot11PhyOFDMEntry->MibObject.
             i4Dot11CurrentFrequency, au1AllowedChannelList) == OSIX_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetDot11PhyOFDMEntry->bDot11TIThreshold != OSIX_FALSE)
    {
        if (pWsscfgSetDot11PhyOFDMEntry->MibObject.
            i4Dot11TIThreshold > TITHRESHOLD_MIN_VALUE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        if (pWsscfgSetDot11PhyOFDMEntry->MibObject.
            i4Dot11TIThreshold < TITHRESHOLD_MAX_VALUE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetDot11PhyOFDMEntry->bDot11ChannelStartingFactor !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PhyOFDMEntry->bDot11PhyOFDMChannelWidth != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pWsscfgSetDot11PhyOFDMEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11HoppingPatternTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11HoppingPatternEntry
                pWsscfgIsSetDot11HoppingPatternEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11HoppingPatternTable (UINT4 *pu4ErrorCode,
                                       tWsscfgDot11HoppingPatternEntry *
                                       pWsscfgSetDot11HoppingPatternEntry,
                                       tWsscfgIsSetDot11HoppingPatternEntry *
                                       pWsscfgIsSetDot11HoppingPatternEntry)
{
    if (pWsscfgIsSetDot11HoppingPatternEntry->bDot11RandomTableFieldNumber !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetDot11HoppingPatternEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11PhyERPTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11PhyERPEntry
                pWsscfgIsSetDot11PhyERPEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11PhyERPTable (UINT4 *pu4ErrorCode,
                               tWsscfgDot11PhyERPEntry *
                               pWsscfgSetDot11PhyERPEntry,
                               tWsscfgIsSetDot11PhyERPEntry *
                               pWsscfgIsSetDot11PhyERPEntry)
{
    if (pWsscfgIsSetDot11PhyERPEntry->bDot11ERPBCCOptionEnabled != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PhyERPEntry->bDot11DSSSOFDMOptionEnabled != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PhyERPEntry->bDot11ShortSlotTimeOptionImplemented !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11PhyERPEntry->
        bDot11ShortSlotTimeOptionEnabled != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetDot11PhyERPEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11RSNAConfigTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11RSNAConfigEntry
                pWsscfgIsSetDot11RSNAConfigEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11RSNAConfigTable (UINT4 *pu4ErrorCode,
                                   tWsscfgDot11RSNAConfigEntry *
                                   pWsscfgSetDot11RSNAConfigEntry,
                                   tWsscfgIsSetDot11RSNAConfigEntry *
                                   pWsscfgIsSetDot11RSNAConfigEntry)
{
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigGroupCipher != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyMethod !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigGroupRekeyTime != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyPackets !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyStrict !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPSKValue !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigPSKPassPhrase != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupUpdateCount !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPairwiseUpdateCount !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigPMKLifetime != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPMKReauthThreshold !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigSATimeout !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNATKIPCounterMeasuresInvoked != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNA4WayHandshakeFailures !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigSTKRekeyTime != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigSMKUpdateCount != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigSMKLifetime != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNASMKHandshakeFailures != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetDot11RSNAConfigEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11RSNAConfigPairwiseCiphersTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry
                pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11RSNAConfigPairwiseCiphersTable (UINT4 *pu4ErrorCode,
                                                  tWsscfgDot11RSNAConfigPairwiseCiphersEntry
                                                  *
                                                  pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry,
                                                  tWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry
                                                  *
                                                  pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry)
{
    if (pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry->
        bDot11RSNAConfigPairwiseCipherEnabled != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllDot11RSNAConfigAuthenticationSuitesTable
 Input       :  pu4ErrorCode
                pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry
                pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllDot11RSNAConfigAuthenticationSuitesTable (UINT4 *pu4ErrorCode,
                                                       tWsscfgDot11RSNAConfigAuthenticationSuitesEntry
                                                       *
                                                       pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry,
                                                       tWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry
                                                       *
                                                       pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry)
{
    if (pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry->
        bDot11RSNAConfigAuthenticationSuiteEnabled != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllFsDot11StationConfigTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsDot11StationConfigEntry
                pWsscfgIsSetFsDot11StationConfigEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsDot11StationConfigTable (UINT4 *pu4ErrorCode,
                                        tWsscfgFsDot11StationConfigEntry *
                                        pWsscfgSetFsDot11StationConfigEntry,
                                        tWsscfgIsSetFsDot11StationConfigEntry *
                                        pWsscfgIsSetFsDot11StationConfigEntry)
{
    if (pWsscfgIsSetFsDot11StationConfigEntry->bFsDot11SupressSSID !=
        OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11StationConfigEntry->
             MibObject.i4FsDot11SupressSSID != CLI_WLAN_ENABLE)
            && (pWsscfgSetFsDot11StationConfigEntry->
                MibObject.i4FsDot11SupressSSID != CLI_WLAN_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11StationConfigEntry->bFsDot11VlanId != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11StationConfigEntry->
             MibObject.i4FsDot11VlanId < 0)
            || (pWsscfgSetFsDot11StationConfigEntry->
                MibObject.i4FsDot11VlanId > 4094))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    if (pWsscfgIsSetFsDot11StationConfigEntry->bFsDot11BandwidthThresh !=
        OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11StationConfigEntry->
             MibObject.i4FsDot11BandwidthThresh < CLI_WLAN_MIN_SSID_RATE) ||
            (pWsscfgSetFsDot11StationConfigEntry->
             MibObject.i4FsDot11BandwidthThresh > CLI_WLAN_MAX_SSID_RATE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllFsDot11ExternalWebAuthProfileTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsDot11ExternalWebAuthProfileEntry
                pWsscfgIsSetFsDot11ExternalWebAuthProfileEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsDot11ExternalWebAuthProfileTable (UINT4 *pu4ErrorCode,
                                                 tWsscfgFsDot11ExternalWebAuthProfileEntry
                                                 *
                                                 pWsscfgSetFsDot11ExternalWebAuthProfileEntry,
                                                 tWsscfgIsSetFsDot11ExternalWebAuthProfileEntry
                                                 *
                                                 pWsscfgIsSetFsDot11ExternalWebAuthProfileEntry)
{
    if (pWsscfgIsSetFsDot11ExternalWebAuthProfileEntry->
        bFsDot11ExternalWebAuthMethod != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11ExternalWebAuthProfileEntry->
             MibObject.i4FsDot11ExternalWebAuthMethod !=
             CLI_WLAN_WEBAUTH_INTERNAL)
            && (pWsscfgSetFsDot11ExternalWebAuthProfileEntry->MibObject.
                i4FsDot11ExternalWebAuthMethod != CLI_WLAN_WEBAUTH_EXTERNAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11ExternalWebAuthProfileEntry->
        bFsDot11ExternalWebAuthUrl != OSIX_FALSE)
    {
        if (pWsscfgSetFsDot11ExternalWebAuthProfileEntry->
            MibObject.au1FsDot11ExternalWebAuthUrl[0] == '\0')

        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

        if (STRLEN (pWsscfgSetFsDot11ExternalWebAuthProfileEntry->
                    MibObject.au1FsDot11ExternalWebAuthUrl) == 0)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllFsDot11CapabilityProfileTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsDot11CapabilityProfileEntry
                pWsscfgIsSetFsDot11CapabilityProfileEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsDot11CapabilityProfileTable (UINT4 *pu4ErrorCode,
                                            tWsscfgFsDot11CapabilityProfileEntry
                                            *
                                            pWsscfgSetFsDot11CapabilityProfileEntry,
                                            tWsscfgIsSetFsDot11CapabilityProfileEntry
                                            *
                                            pWsscfgIsSetFsDot11CapabilityProfileEntry,
                                            INT4 i4RowStatusLogic,
                                            INT4 i4RowCreateOption)
{
    tCliHandle          CliHandle;

    MEMSET (&CliHandle, 0, sizeof (tCliHandle));

    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11CapabilityProfileName != OSIX_FALSE)
    {
        if (pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11CapabilityProfileNameLen > CLI_MAX_PROFILE_NAME)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        if (WsscfgCheckForAlphaNumeric
            (&
             (pWsscfgSetFsDot11CapabilityProfileEntry->MibObject.
              au1FsDot11CapabilityProfileName[0])) == OSIX_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\nOnly characters and numbers are accepted!\r\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11CFPollable != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11CapabilityProfileEntry->
             MibObject.i4FsDot11CFPollable != CLI_WLAN_ENABLE)
            && (pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11CFPollable != CLI_WLAN_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11CFPollRequest != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11CapabilityProfileEntry->
             MibObject.i4FsDot11CFPollRequest != CLI_WLAN_ENABLE)
            && (pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11CFPollRequest != CLI_WLAN_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;

        }
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11PrivacyOptionImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11CapabilityProfileEntry->
             MibObject.i4FsDot11PrivacyOptionImplemented !=
             CLI_WLAN_ENABLE)
            && (pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11PrivacyOptionImplemented !=
                CLI_WLAN_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;

        }
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11ShortPreambleOptionImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11CapabilityProfileEntry->
             MibObject.i4FsDot11ShortPreambleOptionImplemented !=
             CLI_WLAN_ENABLE)
            && (pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11ShortPreambleOptionImplemented !=
                CLI_WLAN_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;

        }

    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11PBCCOptionImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11CapabilityProfileEntry->
             MibObject.i4FsDot11PBCCOptionImplemented !=
             CLI_WLAN_ENABLE)
            && (pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11PBCCOptionImplemented != CLI_WLAN_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;

        }
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11ChannelAgilityPresent != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11CapabilityProfileEntry->
             MibObject.i4FsDot11ChannelAgilityPresent !=
             CLI_WLAN_ENABLE)
            && (pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11ChannelAgilityPresent != CLI_WLAN_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;

        }
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11QosOptionImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11CapabilityProfileEntry->
             MibObject.i4FsDot11QosOptionImplemented !=
             CLI_WLAN_ENABLE)
            && (pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11QosOptionImplemented != CLI_WLAN_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;

        }
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11SpectrumManagementRequired != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11CapabilityProfileEntry->
             MibObject.i4FsDot11SpectrumManagementRequired !=
             CLI_WLAN_ENABLE)
            && (pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11SpectrumManagementRequired !=
                CLI_WLAN_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;

        }
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11ShortSlotTimeOptionImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11CapabilityProfileEntry->
             MibObject.i4FsDot11ShortSlotTimeOptionImplemented !=
             CLI_WLAN_ENABLE)
            && (pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11ShortSlotTimeOptionImplemented !=
                CLI_WLAN_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;

        }
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11APSDOptionImplemented != OSIX_FALSE)
    {

        if ((pWsscfgSetFsDot11CapabilityProfileEntry->
             MibObject.i4FsDot11APSDOptionImplemented !=
             CLI_WLAN_ENABLE)
            && (pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11APSDOptionImplemented != CLI_WLAN_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;

        }
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11DSSSOFDMOptionEnabled != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11CapabilityProfileEntry->
             MibObject.i4FsDot11DSSSOFDMOptionEnabled !=
             CLI_WLAN_ENABLE)
            && (pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11DSSSOFDMOptionEnabled != CLI_WLAN_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;

        }
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11DelayedBlockAckOptionImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11CapabilityProfileEntry->
             MibObject.i4FsDot11DelayedBlockAckOptionImplemented !=
             CLI_WLAN_ENABLE)
            && (pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11DelayedBlockAckOptionImplemented !=
                CLI_WLAN_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;

        }
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11ImmediateBlockAckOptionImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11CapabilityProfileEntry->
             MibObject.i4FsDot11ImmediateBlockAckOptionImplemented !=
             CLI_WLAN_ENABLE)
            && (pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11ImmediateBlockAckOptionImplemented
                != CLI_WLAN_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;

        }
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11QAckOptionImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11CapabilityProfileEntry->
             MibObject.i4FsDot11QAckOptionImplemented !=
             CLI_WLAN_ENABLE)
            && (pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11QAckOptionImplemented != CLI_WLAN_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;

        }
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11QueueRequestOptionImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11CapabilityProfileEntry->
             MibObject.i4FsDot11QueueRequestOptionImplemented !=
             CLI_WLAN_ENABLE)
            && (pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11QueueRequestOptionImplemented !=
                CLI_WLAN_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;

        }
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11TXOPRequestOptionImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11CapabilityProfileEntry->
             MibObject.i4FsDot11TXOPRequestOptionImplemented !=
             CLI_WLAN_ENABLE)
            && (pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11TXOPRequestOptionImplemented !=
                CLI_WLAN_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;

        }
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11RSNAOptionImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11CapabilityProfileEntry->
             MibObject.i4FsDot11RSNAOptionImplemented !=
             CLI_WLAN_ENABLE)
            && (pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11RSNAOptionImplemented != CLI_WLAN_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;

        }
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11RSNAPreauthenticationImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11CapabilityProfileEntry->
             MibObject.i4FsDot11RSNAPreauthenticationImplemented !=
             CLI_WLAN_ENABLE)
            && (pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11RSNAPreauthenticationImplemented !=
                CLI_WLAN_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;

        }
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11CapabilityRowStatus != OSIX_FALSE)
    {
    }
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllFsDot11AuthenticationProfileTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsDot11AuthenticationProfileEntry
                pWsscfgIsSetFsDot11AuthenticationProfileEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsDot11AuthenticationProfileTable (UINT4 *pu4ErrorCode,
                                                tWsscfgFsDot11AuthenticationProfileEntry
                                                *
                                                pWsscfgSetFsDot11AuthenticationProfileEntry,
                                                tWsscfgIsSetFsDot11AuthenticationProfileEntry
                                                *
                                                pWsscfgIsSetFsDot11AuthenticationProfileEntry,
                                                INT4 i4RowStatusLogic,
                                                INT4 i4RowCreateOption)
{
    INT4                i4NameLen = 0;
    tCliHandle          CliHandle;
    MEMSET (&CliHandle, 0, sizeof (tCliHandle));

    tWsscfgFsDot11AuthenticationProfileEntry
        wsscfgFsDot11AuthenticationProfileEntry;
    MEMSET (&wsscfgFsDot11AuthenticationProfileEntry, 0,
            sizeof (wsscfgFsDot11AuthenticationProfileEntry));

    i4NameLen =
        STRLEN (pWsscfgSetFsDot11AuthenticationProfileEntry->
                MibObject.au1FsDot11AuthenticationProfileName);
    MEMCPY (wsscfgFsDot11AuthenticationProfileEntry.
            MibObject.au1FsDot11AuthenticationProfileName,
            pWsscfgSetFsDot11AuthenticationProfileEntry->
            MibObject.au1FsDot11AuthenticationProfileName, i4NameLen);
    wsscfgFsDot11AuthenticationProfileEntry.
        MibObject.i4FsDot11AuthenticationProfileNameLen = i4NameLen;
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
        bFsDot11AuthenticationProfileName != OSIX_FALSE)
    {
        if (pWsscfgSetFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11AuthenticationProfileNameLen >
            CLI_MAX_PROFILE_NAME)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /* Validate profile name, only character and numbers check */

        if (WsscfgCheckForAlphaNumeric
            (&
             (pWsscfgSetFsDot11AuthenticationProfileEntry->MibObject.
              au1FsDot11AuthenticationProfileName[0])) == OSIX_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\nOnly characters and numbers are accepted!\r\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
        bFsDot11AuthenticationAlgorithm != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11AuthenticationProfileEntry->
             MibObject.i4FsDot11AuthenticationAlgorithm !=
             CLI_AUTH_ALGO_OPEN)
            && (pWsscfgSetFsDot11AuthenticationProfileEntry->
                MibObject.i4FsDot11AuthenticationAlgorithm !=
                CLI_AUTH_ALGO_SHARED))
        {
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (WsscfgGetAllFsDot11AuthenticationProfileTable
            (&wsscfgFsDot11AuthenticationProfileEntry) == OSIX_SUCCESS)
        {
            if (wsscfgFsDot11AuthenticationProfileEntry.
                MibObject.i4FsDot11AuthenticationAlgorithm !=
                CLI_AUTH_ALGO_SHARED)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKeyIndex !=
        OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11AuthenticationProfileEntry->
             MibObject.i4FsDot11WepKeyIndex < 1)
            || (pWsscfgSetFsDot11AuthenticationProfileEntry->
                MibObject.i4FsDot11WepKeyIndex > 4))
        {
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKeyType !=
        OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11AuthenticationProfileEntry->
             MibObject.i4FsDot11WepKeyType != CLI_WEP_KEY_TYPE_HEX)
            && (pWsscfgSetFsDot11AuthenticationProfileEntry->
                MibObject.i4FsDot11WepKeyType != CLI_WEP_KEY_TYPE_ASCII))
        {
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKeyLength !=
        OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11AuthenticationProfileEntry->
             MibObject.i4FsDot11WepKeyLength != 40)
            && (pWsscfgSetFsDot11AuthenticationProfileEntry->
                MibObject.i4FsDot11WepKeyLength != 104))
        {
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
        bFsDot11WepKey != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11AuthenticationProfileEntry->
             MibObject.i4FsDot11WepKeyType == CLI_WEP_KEY_TYPE_HEX))
        {
            if (pWsscfgSetFsDot11AuthenticationProfileEntry->
                MibObject.i4FsDot11WepKeyLen !=
                (pWsscfgSetFsDot11AuthenticationProfileEntry->
                 MibObject.i4FsDot11WepKeyLength / 4))
            {
                return OSIX_FAILURE;
            }
        }
        else if ((pWsscfgSetFsDot11AuthenticationProfileEntry->
                  MibObject.i4FsDot11WepKeyType == CLI_WEP_KEY_TYPE_ASCII))
        {
            if (pWsscfgSetFsDot11AuthenticationProfileEntry->
                MibObject.i4FsDot11WepKeyLen !=
                (pWsscfgSetFsDot11AuthenticationProfileEntry->
                 MibObject.i4FsDot11WepKeyLength / 8))
            {
                return OSIX_FAILURE;
            }
        }
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
        bFsDot11WebAuthentication != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11AuthenticationProfileEntry->
             MibObject.i4FsDot11WebAuthentication !=
             WSS_AUTH_STATUS_ENABLE)
            && (pWsscfgSetFsDot11AuthenticationProfileEntry->
                MibObject.i4FsDot11WebAuthentication !=
                WSS_AUTH_STATUS_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
        bFsDot11AuthenticationRowStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllFsSecurityWebAuthGuestInfoTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsSecurityWebAuthGuestInfoEntry
                pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsSecurityWebAuthGuestInfoTable (UINT4 *pu4ErrorCode,
                                              tWsscfgFsSecurityWebAuthGuestInfoEntry
                                              *
                                              pWsscfgSetFsSecurityWebAuthGuestInfoEntry,
                                              tWsscfgIsSetFsSecurityWebAuthGuestInfoEntry
                                              *
                                              pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry,
                                              INT4 i4RowStatusLogic,
                                              INT4 i4RowCreateOption)
{
    tCliHandle          CliHandle;
    MEMSET (&CliHandle, 0, sizeof (tCliHandle));

    INT4                u4WlanRowStatus = 0;
    INT4                i4RetValFsSecurityWebAuthGuestInfoRowStatus = 0;
    INT4                i4RetValFsSecurityWlanProfileId = 0;
    UINT1               au1FsSecurityWebAuthUName[WSS_STA_WEBUSER_LEN_MAX];
    tSNMP_OCTET_STRING_TYPE pFsSecurityWebAuthUName;

    MEMSET (au1FsSecurityWebAuthUName, 0, WSS_STA_WEBUSER_LEN_MAX);
    MEMSET (&pFsSecurityWebAuthUName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    if (pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry->bFsSecurityWlanProfileId !=
        OSIX_FALSE)
    {
        /*check if the Wlan profile Id is already available */
        if (nmhGetCapwapDot11WlanRowStatus
            (pWsscfgSetFsSecurityWebAuthGuestInfoEntry->
             MibObject.i4FsSecurityWlanProfileId,
             &u4WlanRowStatus) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry->
        bFsSecurityWebAuthUserLifetime != OSIX_FALSE)
    {
        /* Check the user lifetime should be between 1 minutes and 30 days */
        if ((pWsscfgSetFsSecurityWebAuthGuestInfoEntry->
             MibObject.i4FsSecurityWebAuthUserLifetime < 0)
            || (pWsscfgSetFsSecurityWebAuthGuestInfoEntry->
                MibObject.i4FsSecurityWebAuthUserLifetime > 43200))
        {
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry->bFsSecurityWebAuthUName !=
        OSIX_FALSE)
    {
        if (WsscfgCheckForAlphaNumeric
            (&
             (pWsscfgSetFsSecurityWebAuthGuestInfoEntry->
              MibObject.au1FsSecurityWebAuthUName[0])) == OSIX_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\nOnly characters and numbers are "
                       " accepted in username!\r\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

    }
    if (pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry->
        bFsSecurityWebAuthUPassword != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetFsSecurityWebAuthGuestInfoEntry->MibObject.
             i4FsSecurityWebAuthUPasswordLen < 8) ||
            (pWsscfgSetFsSecurityWebAuthGuestInfoEntry->MibObject.
             i4FsSecurityWebAuthUPasswordLen > 20))
        {
            return OSIX_FAILURE;
        }
    }

    if (pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry->
        bFsSecurityWebAuthGuestInfoRowStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
        if (pWsscfgSetFsSecurityWebAuthGuestInfoEntry->MibObject.
            i4FsSecurityWebAuthGuestInfoRowStatus == ACTIVE)
        {

            pFsSecurityWebAuthUName.pu1_OctetList = au1FsSecurityWebAuthUName;
            pFsSecurityWebAuthUName.i4_Length =
                STRLEN (pWsscfgSetFsSecurityWebAuthGuestInfoEntry->
                        MibObject.au1FsSecurityWebAuthUName);

            MEMCPY (pFsSecurityWebAuthUName.pu1_OctetList,
                    pWsscfgSetFsSecurityWebAuthGuestInfoEntry->
                    MibObject.au1FsSecurityWebAuthUName,
                    pFsSecurityWebAuthUName.i4_Length);
            if (pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry->
                bFsSecurityWlanProfileId != OSIX_TRUE)
            {

                if (nmhGetFsSecurityWlanProfileId (&pFsSecurityWebAuthUName,
                                                   &i4RetValFsSecurityWlanProfileId)
                    != SNMP_SUCCESS)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
                pWsscfgSetFsSecurityWebAuthGuestInfoEntry->MibObject.
                    i4FsSecurityWlanProfileId = i4RetValFsSecurityWlanProfileId;
            }

            if (pWsscfgSetFsSecurityWebAuthGuestInfoEntry->MibObject.
                i4FsSecurityWlanProfileId != 0)
            {
                if (nmhGetFsSecurityWebAuthGuestInfoRowStatus
                    (&pFsSecurityWebAuthUName,
                     &i4RetValFsSecurityWebAuthGuestInfoRowStatus) !=
                    SNMP_SUCCESS)

                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }

    }

    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllFsStationQosParamsTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsStationQosParamsEntry
                pWsscfgIsSetFsStationQosParamsEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsStationQosParamsTable (UINT4 *pu4ErrorCode,
                                      tWsscfgFsStationQosParamsEntry *
                                      pWsscfgSetFsStationQosParamsEntry,
                                      tWsscfgIsSetFsStationQosParamsEntry *
                                      pWsscfgIsSetFsStationQosParamsEntry)
{
    if (pWsscfgIsSetFsStationQosParamsEntry->bFsStaQoSPriority != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsStationQosParamsEntry->bFsStaQoSDscp != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetFsStationQosParamsEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllFsVlanIsolationTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsVlanIsolationEntry
                pWsscfgIsSetFsVlanIsolationEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsVlanIsolationTable (UINT4 *pu4ErrorCode,
                                   tWsscfgFsVlanIsolationEntry *
                                   pWsscfgSetFsVlanIsolationEntry,
                                   tWsscfgIsSetFsVlanIsolationEntry *
                                   pWsscfgIsSetFsVlanIsolationEntry)
{
    if (pWsscfgIsSetFsVlanIsolationEntry->bFsVlanIsolation != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetFsVlanIsolationEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllFsDot11RadioConfigTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsDot11RadioConfigEntry
                pWsscfgIsSetFsDot11RadioConfigEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsDot11RadioConfigTable (UINT4 *pu4ErrorCode,
                                      tWsscfgFsDot11RadioConfigEntry *
                                      pWsscfgSetFsDot11RadioConfigEntry,
                                      tWsscfgIsSetFsDot11RadioConfigEntry *
                                      pWsscfgIsSetFsDot11RadioConfigEntry,
                                      INT4 i4RowStatusLogic,
                                      INT4 i4RowCreateOption)
{
    if (pWsscfgIsSetFsDot11RadioConfigEntry->bFsDot11RadioType != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetFsDot11RadioConfigEntry->
             MibObject.u4FsDot11RadioType != CLI_RADIO_TYPEA)
            && (pWsscfgSetFsDot11RadioConfigEntry->
                MibObject.u4FsDot11RadioType != CLI_RADIO_TYPEB)
            && (pWsscfgSetFsDot11RadioConfigEntry->
                MibObject.u4FsDot11RadioType != CLI_RADIO_TYPEG)
            && (pWsscfgSetFsDot11RadioConfigEntry->
                MibObject.u4FsDot11RadioType != CLI_RADIO_TYPEAN)
            && (pWsscfgSetFsDot11RadioConfigEntry->
                MibObject.u4FsDot11RadioType != CLI_RADIO_TYPEBG)
            && (pWsscfgSetFsDot11RadioConfigEntry->
                MibObject.u4FsDot11RadioType != CLI_RADIO_TYPEBGN)
            && (pWsscfgSetFsDot11RadioConfigEntry->
                MibObject.u4FsDot11RadioType != CLI_RADIO_TYPEAC))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11RadioConfigEntry->bFsDot11RowStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetFsDot11RadioConfigEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllFsDot11QosProfileTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsDot11QosProfileEntry
                pWsscfgIsSetFsDot11QosProfileEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsDot11QosProfileTable (UINT4 *pu4ErrorCode,
                                     tWsscfgFsDot11QosProfileEntry *
                                     pWsscfgSetFsDot11QosProfileEntry,
                                     tWsscfgIsSetFsDot11QosProfileEntry *
                                     pWsscfgIsSetFsDot11QosProfileEntry,
                                     INT4 i4RowStatusLogic,
                                     INT4 i4RowCreateOption)
{
    tCliHandle          CliHandle;

    MEMSET (&CliHandle, 0, sizeof (tCliHandle));

    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosProfileName
        != OSIX_FALSE)
    {
        if (pWsscfgSetFsDot11QosProfileEntry->
            MibObject.i4FsDot11QosProfileNameLen > CLI_MAX_PROFILE_NAME)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /* Validate profile name, only character and numbers check */

        if (WsscfgCheckForAlphaNumeric
            (&
             (pWsscfgSetFsDot11QosProfileEntry->MibObject.
              au1FsDot11QosProfileName[0])) == OSIX_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\nOnly characters and numbers are accepted!\r\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosTraffic != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11QosProfileEntry->
             MibObject.i4FsDot11QosTraffic < CLI_WSSCFG_BEST_EFFORT)
            || (pWsscfgSetFsDot11QosProfileEntry->
                MibObject.i4FsDot11QosTraffic > CLI_WSSCFG_BACKGROUND))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosPassengerTrustMode !=
        OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11QosProfileEntry->
             MibObject.i4FsDot11QosPassengerTrustMode !=
             CLI_WSSCFG_QOSPROF_ENABLE)
            && (pWsscfgSetFsDot11QosProfileEntry->
                MibObject.i4FsDot11QosPassengerTrustMode !=
                CLI_WSSCFG_QOSPROF_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosRateLimit != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetFsDot11QosProfileEntry->
             MibObject.i4FsDot11QosRateLimit != CLI_WSSCFG_QOSPROF_ENABLE)
            && (pWsscfgSetFsDot11QosProfileEntry->
                MibObject.i4FsDot11QosRateLimit != CLI_WSSCFG_QOSPROF_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamCIR != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetFsDot11QosProfileEntry->
             MibObject.i4FsDot11UpStreamCIR < CLI_MIN_CIR_VAL)
            && (pWsscfgSetFsDot11QosProfileEntry->
                MibObject.i4FsDot11UpStreamCIR > CLI_MAX_CIR_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamCBS != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetFsDot11QosProfileEntry->
             MibObject.i4FsDot11UpStreamCBS < CLI_MIN_CBS_VAL)
            && (pWsscfgSetFsDot11QosProfileEntry->
                MibObject.i4FsDot11UpStreamCBS > CLI_MAX_CBS_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamEIR != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetFsDot11QosProfileEntry->
             MibObject.i4FsDot11UpStreamEIR < CLI_MIN_EIR_VAL)
            && (pWsscfgSetFsDot11QosProfileEntry->
                MibObject.i4FsDot11UpStreamEIR > CLI_MAX_EIR_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamEBS != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetFsDot11QosProfileEntry->
             MibObject.i4FsDot11UpStreamEBS < CLI_MIN_EBS_VAL)
            && (pWsscfgSetFsDot11QosProfileEntry->
                MibObject.i4FsDot11UpStreamEBS > CLI_MAX_EBS_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamCIR != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetFsDot11QosProfileEntry->
             MibObject.i4FsDot11DownStreamCIR < CLI_MIN_CIR_VAL)
            && (pWsscfgSetFsDot11QosProfileEntry->
                MibObject.i4FsDot11DownStreamCIR > CLI_MAX_CIR_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamCBS != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetFsDot11QosProfileEntry->
             MibObject.i4FsDot11DownStreamCBS < CLI_MIN_CBS_VAL)
            && (pWsscfgSetFsDot11QosProfileEntry->
                MibObject.i4FsDot11DownStreamCBS > CLI_MAX_CBS_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamEIR != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetFsDot11QosProfileEntry->
             MibObject.i4FsDot11DownStreamEIR < CLI_MIN_EIR_VAL)
            && (pWsscfgSetFsDot11QosProfileEntry->
                MibObject.i4FsDot11DownStreamEIR > CLI_MAX_EIR_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamEBS != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetFsDot11QosProfileEntry->
             MibObject.i4FsDot11DownStreamEBS < CLI_MIN_EBS_VAL)
            && (pWsscfgSetFsDot11QosProfileEntry->
                MibObject.i4FsDot11DownStreamEBS > CLI_MAX_EBS_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosRowStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }

    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllFsDot11WlanCapabilityProfileTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsDot11WlanCapabilityProfileEntry
                pWsscfgIsSetFsDot11WlanCapabilityProfileEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsDot11WlanCapabilityProfileTable (UINT4 *pu4ErrorCode,
                                                tWsscfgFsDot11WlanCapabilityProfileEntry
                                                *
                                                pWsscfgSetFsDot11WlanCapabilityProfileEntry,
                                                tWsscfgIsSetFsDot11WlanCapabilityProfileEntry
                                                *
                                                pWsscfgIsSetFsDot11WlanCapabilityProfileEntry,
                                                INT4 i4RowStatusLogic,
                                                INT4 i4RowCreateOption)
{
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanCFPollable !=
        OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
             MibObject.i4FsDot11WlanCFPollable != CLI_WSSCFG_ENABLE)
            && (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                MibObject.i4FsDot11WlanCFPollable != CLI_WSSCFG_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanCFPollRequest != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
             MibObject.i4FsDot11WlanCFPollRequest !=
             CLI_WSSCFG_ENABLE)
            && (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                MibObject.i4FsDot11WlanCFPollRequest != CLI_WSSCFG_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanPrivacyOptionImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
             MibObject.i4FsDot11WlanPrivacyOptionImplemented !=
             CLI_WSSCFG_ENABLE)
            && (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                MibObject.i4FsDot11WlanPrivacyOptionImplemented !=
                CLI_WSSCFG_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanShortPreambleOptionImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
             MibObject.i4FsDot11WlanShortPreambleOptionImplemented !=
             CLI_WSSCFG_ENABLE)
            && (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                MibObject.i4FsDot11WlanShortPreambleOptionImplemented
                != CLI_WSSCFG_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanPBCCOptionImplemented != OSIX_FALSE)
    {

        if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
             MibObject.i4FsDot11WlanPBCCOptionImplemented !=
             CLI_WSSCFG_ENABLE)
            && (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                MibObject.i4FsDot11WlanPBCCOptionImplemented !=
                CLI_WSSCFG_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanChannelAgilityPresent != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
             MibObject.i4FsDot11WlanChannelAgilityPresent !=
             CLI_WSSCFG_ENABLE)
            && (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                MibObject.i4FsDot11WlanChannelAgilityPresent !=
                CLI_WSSCFG_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanQosOptionImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
             MibObject.i4FsDot11WlanQosOptionImplemented !=
             CLI_WSSCFG_ENABLE)
            && (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                MibObject.i4FsDot11WlanQosOptionImplemented !=
                CLI_WSSCFG_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanSpectrumManagementRequired != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
             MibObject.i4FsDot11WlanSpectrumManagementRequired !=
             CLI_WSSCFG_ENABLE)
            && (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                MibObject.i4FsDot11WlanSpectrumManagementRequired !=
                CLI_WSSCFG_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanShortSlotTimeOptionImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
             MibObject.i4FsDot11WlanShortSlotTimeOptionImplemented !=
             CLI_WSSCFG_ENABLE)
            && (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                MibObject.i4FsDot11WlanShortSlotTimeOptionImplemented
                != CLI_WSSCFG_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanAPSDOptionImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
             MibObject.i4FsDot11WlanAPSDOptionImplemented !=
             CLI_WSSCFG_ENABLE)
            && (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                MibObject.i4FsDot11WlanAPSDOptionImplemented !=
                CLI_WSSCFG_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanDSSSOFDMOptionEnabled != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
             MibObject.i4FsDot11WlanDSSSOFDMOptionEnabled !=
             CLI_WSSCFG_ENABLE)
            && (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                MibObject.i4FsDot11WlanDSSSOFDMOptionEnabled !=
                CLI_WSSCFG_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanDelayedBlockAckOptionImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
             MibObject.i4FsDot11WlanDelayedBlockAckOptionImplemented
             != CLI_WSSCFG_ENABLE)
            && (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                MibObject.i4FsDot11WlanDelayedBlockAckOptionImplemented
                != CLI_WSSCFG_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanImmediateBlockAckOptionImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
             MibObject.i4FsDot11WlanImmediateBlockAckOptionImplemented
             != CLI_WSSCFG_ENABLE)
            && (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                MibObject.i4FsDot11WlanImmediateBlockAckOptionImplemented
                != CLI_WSSCFG_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanQAckOptionImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
             MibObject.i4FsDot11WlanQAckOptionImplemented !=
             CLI_WSSCFG_ENABLE)
            && (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                MibObject.i4FsDot11WlanQAckOptionImplemented !=
                CLI_WSSCFG_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanQueueRequestOptionImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
             MibObject.i4FsDot11WlanQueueRequestOptionImplemented !=
             CLI_WSSCFG_ENABLE)
            && (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                MibObject.i4FsDot11WlanQueueRequestOptionImplemented
                != CLI_WSSCFG_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanTXOPRequestOptionImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
             MibObject.i4FsDot11WlanTXOPRequestOptionImplemented !=
             CLI_WSSCFG_ENABLE)
            && (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                MibObject.i4FsDot11WlanTXOPRequestOptionImplemented !=
                CLI_WSSCFG_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanRSNAOptionImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
             MibObject.i4FsDot11WlanRSNAOptionImplemented !=
             CLI_WSSCFG_ENABLE)
            && (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                MibObject.i4FsDot11WlanRSNAOptionImplemented !=
                CLI_WSSCFG_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanRSNAPreauthenticationImplemented != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
             MibObject.i4FsDot11WlanRSNAPreauthenticationImplemented
             != CLI_WSSCFG_ENABLE)
            && (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                MibObject.i4FsDot11WlanRSNAPreauthenticationImplemented
                != CLI_WSSCFG_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanCapabilityRowStatus != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
             MibObject.i4FsDot11WlanCapabilityRowStatus !=
             CLI_WSSCFG_ENABLE)
            && (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                MibObject.i4FsDot11WlanCapabilityRowStatus !=
                CLI_WSSCFG_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllFsDot11WlanAuthenticationProfileTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsDot11WlanAuthenticationProfileEntry
                pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsDot11WlanAuthenticationProfileTable (UINT4 *pu4ErrorCode,
                                                    tWsscfgFsDot11WlanAuthenticationProfileEntry
                                                    *
                                                    pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
                                                    tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry
                                                    *
                                                    pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry,
                                                    INT4 i4RowStatusLogic,
                                                    INT4 i4RowCreateOption)
{
    INT4                i4WlanKeyLength = 0;
    INT4                i4WlanKeyType = 0;
#ifdef  RSNA_WANTED
    INT4                i4Dot11RSNAEnabled = 0;
#endif
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanAuthenticationAlgorithm != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
             MibObject.i4FsDot11WlanAuthenticationAlgorithm !=
             CLI_AUTH_ALGO_OPEN)
            && (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                MibObject.i4FsDot11WlanAuthenticationAlgorithm !=
                CLI_AUTH_ALGO_SHARED))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanWepKeyIndex != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
             MibObject.i4FsDot11WlanWepKeyIndex) > 4)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanWepKeyType != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
             MibObject.i4FsDot11WlanWepKeyType !=
             CLI_WEP_KEY_TYPE_HEX)
            && (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                MibObject.i4FsDot11WlanWepKeyType != CLI_WEP_KEY_TYPE_ASCII))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanWepKeyLength != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
             MibObject.i4FsDot11WlanWepKeyLength != 40)
            && (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                MibObject.i4FsDot11WlanWepKeyLength != 104))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bFsDot11WlanWepKey !=
        OSIX_FALSE)
    {
        nmhGetFsDot11WlanWepKeyLength
            (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->MibObject.
             i4IfIndex, &i4WlanKeyLength);

        if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
            bFsDot11WlanWepKeyType != OSIX_FALSE)
        {
            if (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->MibObject.
                i4FsDot11WlanWepKeyType == CLI_WEP_KEY_TYPE_ASCII)
            {
                if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
                    bFsDot11WlanWepKeyLength != OSIX_FALSE)
                {
                    if ((pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                         MibObject.i4FsDot11WlanWepKeyLen) !=
                        ((pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                          MibObject.i4FsDot11WlanWepKeyLength) / 8))
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return OSIX_FAILURE;
                    }
                }
                else
                {
                    if (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                        MibObject.i4FsDot11WlanWepKeyLen !=
                        ((i4WlanKeyLength) / 8))
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return OSIX_FAILURE;
                    }
                }
            }
            else if ((pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                      MibObject.i4FsDot11WlanWepKeyType) ==
                     CLI_WEP_KEY_TYPE_HEX)
            {
                if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
                    bFsDot11WlanWepKeyLength != OSIX_FALSE)
                {
                    if ((pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                         MibObject.i4FsDot11WlanWepKeyLen) !=
                        ((pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                          MibObject.i4FsDot11WlanWepKeyLength) / 4))
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return OSIX_FAILURE;
                    }
                }
                else
                {
                    if (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                        MibObject.i4FsDot11WlanWepKeyLen !=
                        (i4WlanKeyLength / 8))
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return OSIX_FAILURE;
                    }
                }
            }
        }
        else if (nmhGetFsDot11WlanWepKeyType
                 (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->MibObject.
                  i4IfIndex, &i4WlanKeyType) != OSIX_SUCCESS)
        {
            if (i4WlanKeyType == CLI_WEP_KEY_TYPE_ASCII)
            {
                if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
                    bFsDot11WlanWepKeyLength != OSIX_FALSE)
                {
                    if ((pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                         MibObject.i4FsDot11WlanWepKeyLen) !=
                        ((pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                          MibObject.i4FsDot11WlanWepKeyLength) / 8))
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return OSIX_FAILURE;
                    }
                }
                else
                {
                    if (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                        MibObject.i4FsDot11WlanWepKeyLen !=
                        (i4WlanKeyLength / 8))
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return OSIX_FAILURE;
                    }
                }
            }
            else if (i4WlanKeyType == CLI_WEP_KEY_TYPE_HEX)
            {
                if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
                    bFsDot11WlanWepKeyLength != OSIX_FALSE)
                {
                    if ((pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                         MibObject.i4FsDot11WlanWepKeyLen) !=
                        ((pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                          MibObject.i4FsDot11WlanWepKeyLength) / 4))
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return OSIX_FAILURE;
                    }
                }
                else
                {
                    if (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                        MibObject.i4FsDot11WlanWepKeyLen !=
                        (i4WlanKeyLength / 4))
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return OSIX_FAILURE;
                    }
                }
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }

    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanWebAuthentication != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
             MibObject.i4FsDot11WlanWebAuthentication !=
             WSS_AUTH_STATUS_ENABLE)
            && (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                MibObject.i4FsDot11WlanWebAuthentication !=
                WSS_AUTH_STATUS_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
#ifdef  RSNA_WANTED
        /* Get the RSNA Enable/Disable Status for the specified profile */
        RsnaGetDot11RSNAEnabled
            (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->MibObject.
             i4IfIndex, &i4Dot11RSNAEnabled);
        if ((pWsscfgSetFsDot11WlanAuthenticationProfileEntry->MibObject.
             i4FsDot11WlanWebAuthentication == WSS_AUTH_STATUS_ENABLE)
            && (i4Dot11RSNAEnabled == RSNA_ENABLED))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
#endif
    }

    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanAuthenticationRowStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllFsDot11WlanQosProfileTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsDot11WlanQosProfileEntry
                pWsscfgIsSetFsDot11WlanQosProfileEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsDot11WlanQosProfileTable (UINT4 *pu4ErrorCode,
                                         tWsscfgFsDot11WlanQosProfileEntry *
                                         pWsscfgSetFsDot11WlanQosProfileEntry,
                                         tWsscfgIsSetFsDot11WlanQosProfileEntry
                                         *
                                         pWsscfgIsSetFsDot11WlanQosProfileEntry,
                                         INT4 i4RowStatusLogic,
                                         INT4 i4RowCreateOption)
{
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanQosTraffic != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetFsDot11WlanQosProfileEntry->
             MibObject.i4FsDot11WlanQosTraffic !=
             CLI_WSSCFG_BEST_EFFORT)
            && (pWsscfgSetFsDot11WlanQosProfileEntry->
                MibObject.i4FsDot11WlanQosTraffic != CLI_WSSCFG_VIDEO)
            && (pWsscfgSetFsDot11WlanQosProfileEntry->
                MibObject.i4FsDot11WlanQosTraffic != CLI_WSSCFG_VOICE)
            && (pWsscfgSetFsDot11WlanQosProfileEntry->
                MibObject.i4FsDot11WlanQosTraffic != CLI_WSSCFG_BACKGROUND))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanQosPassengerTrustMode != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetFsDot11WlanQosProfileEntry->
             MibObject.i4FsDot11WlanQosPassengerTrustMode !=
             CLI_WSSCFG_QOSPROF_ENABLE)
            && (pWsscfgSetFsDot11WlanQosProfileEntry->
                MibObject.i4FsDot11WlanQosPassengerTrustMode !=
                CLI_WSSCFG_QOSPROF_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanQosRateLimit != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetFsDot11WlanQosProfileEntry->
             MibObject.i4FsDot11WlanQosRateLimit != CLI_WSSCFG_QOSPROF_ENABLE)
            && (pWsscfgSetFsDot11WlanQosProfileEntry->
                MibObject.i4FsDot11WlanQosRateLimit !=
                CLI_WSSCFG_QOSPROF_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanUpStreamCIR != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetFsDot11WlanQosProfileEntry->
             MibObject.i4FsDot11WlanUpStreamCIR < CLI_MIN_CIR_VAL)
            || (pWsscfgSetFsDot11WlanQosProfileEntry->
                MibObject.i4FsDot11WlanUpStreamCIR > CLI_MAX_CIR_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanUpStreamCBS != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetFsDot11WlanQosProfileEntry->
             MibObject.i4FsDot11WlanUpStreamCBS < CLI_MIN_CBS_VAL)
            || (pWsscfgSetFsDot11WlanQosProfileEntry->
                MibObject.i4FsDot11WlanUpStreamCBS > CLI_MAX_CBS_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanUpStreamEIR != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetFsDot11WlanQosProfileEntry->
             MibObject.i4FsDot11WlanUpStreamEIR < CLI_MIN_EIR_VAL)
            || (pWsscfgSetFsDot11WlanQosProfileEntry->
                MibObject.i4FsDot11WlanUpStreamEIR > CLI_MAX_EIR_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanUpStreamEBS != OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetFsDot11WlanQosProfileEntry->
             MibObject.i4FsDot11WlanUpStreamEBS < CLI_MIN_EBS_VAL)
            || (pWsscfgSetFsDot11WlanQosProfileEntry->
                MibObject.i4FsDot11WlanUpStreamEBS > CLI_MAX_EBS_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    if ((pWsscfgIsSetFsDot11WlanQosProfileEntry->
         bFsDot11WlanUpStreamCIR != OSIX_FALSE)
        &&
        (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanUpStreamEIR
         != OSIX_FALSE))
    {
        if ((pWsscfgSetFsDot11WlanQosProfileEntry->
             MibObject.i4FsDot11WlanUpStreamEIR)
            < (pWsscfgSetFsDot11WlanQosProfileEntry->
               MibObject.i4FsDot11WlanUpStreamCIR))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamCIR !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetFsDot11WlanQosProfileEntry->
             MibObject.i4FsDot11WlanDownStreamCIR < CLI_MIN_CIR_VAL)
            || (pWsscfgSetFsDot11WlanQosProfileEntry->
                MibObject.i4FsDot11WlanDownStreamCIR > CLI_MAX_CIR_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamCBS !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetFsDot11WlanQosProfileEntry->
             MibObject.i4FsDot11WlanDownStreamCBS < CLI_MIN_CBS_VAL)
            || (pWsscfgSetFsDot11WlanQosProfileEntry->
                MibObject.i4FsDot11WlanDownStreamCBS > CLI_MAX_CBS_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamEIR !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetFsDot11WlanQosProfileEntry->
             MibObject.i4FsDot11WlanDownStreamEIR < CLI_MIN_EIR_VAL)
            || (pWsscfgSetFsDot11WlanQosProfileEntry->
                MibObject.i4FsDot11WlanDownStreamEIR > CLI_MAX_EIR_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamEBS !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
        if ((pWsscfgSetFsDot11WlanQosProfileEntry->
             MibObject.i4FsDot11WlanDownStreamEBS < CLI_MIN_EBS_VAL)
            || (pWsscfgSetFsDot11WlanQosProfileEntry->
                MibObject.i4FsDot11WlanDownStreamEBS > CLI_MAX_EBS_VAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    if ((pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamCIR !=
         OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamEIR !=
            OSIX_FALSE))
    {
        if ((pWsscfgSetFsDot11WlanQosProfileEntry->
             MibObject.i4FsDot11WlanDownStreamCIR) >
            (pWsscfgSetFsDot11WlanQosProfileEntry->
             MibObject.i4FsDot11WlanDownStreamEIR))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;

        }
    }

    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanQosRowStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllFsDot11RadioQosTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsDot11RadioQosEntry
                pWsscfgIsSetFsDot11RadioQosEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsDot11RadioQosTable (UINT4 *pu4ErrorCode,
                                   tWsscfgFsDot11RadioQosEntry *
                                   pWsscfgSetFsDot11RadioQosEntry,
                                   tWsscfgIsSetFsDot11RadioQosEntry *
                                   pWsscfgIsSetFsDot11RadioQosEntry)
{
    if (pWsscfgIsSetFsDot11RadioQosEntry->bFsDot11TaggingPolicy != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11RadioQosEntry->
             MibObject.au1FsDot11TaggingPolicy[0] !=
             CLI_TAG_POLICY_NONE)
            && (pWsscfgSetFsDot11RadioQosEntry->
                MibObject.au1FsDot11TaggingPolicy[0] !=
                CLI_TAG_POLICY_PBIT)
            && (pWsscfgSetFsDot11RadioQosEntry->
                MibObject.au1FsDot11TaggingPolicy[0] !=
                CLI_TAG_POLICY_DSCP)
            && (pWsscfgSetFsDot11RadioQosEntry->
                MibObject.au1FsDot11TaggingPolicy[0] != CLI_TAG_POLICY_BOTH))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;

        }
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetFsDot11RadioQosEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllFsDot11QAPTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsDot11QAPEntry
                pWsscfgIsSetFsDot11QAPEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsDot11QAPTable (UINT4 *pu4ErrorCode,
                              tWsscfgFsDot11QAPEntry *
                              pWsscfgSetFsDot11QAPEntry,
                              tWsscfgIsSetFsDot11QAPEntry *
                              pWsscfgIsSetFsDot11QAPEntry)
{
    /*
       if (pWsscfgIsSetFsDot11QAPEntry->bFsDot11QueueDepth != OSIX_FALSE)
       {
       if ( (pWsscfgSetFsDot11QAPEntry->MibObject.i4FsDot11QueueDepth) > 255)
       {
       *pu4ErrorCode  =  SNMP_ERR_WRONG_VALUE;
       return OSIX_FAILURE;
       }                 
       //               Fill your check condition
       }
     */
    /* Configuring this object is not allowed as per requirement. */
#ifdef UNUSED_CODE

    /*when the configuration is enabled, the following should be un commented */

    if (pWsscfgIsSetFsDot11QAPEntry->bFsDot11PriorityValue != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11QAPEntry->MibObject.
             i4FsDot11PriorityValue > 7)
            || (pWsscfgSetFsDot11QAPEntry->
                MibObject.i4FsDot11PriorityValue < 0))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11QAPEntry->bFsDot11DscpValue != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11QAPEntry->MibObject.i4FsDot11DscpValue >
             64)
            || (pWsscfgSetFsDot11QAPEntry->MibObject.i4FsDot11DscpValue < 0))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    return OSIX_SUCCESS;
#else
    UNUSED_PARAM (pWsscfgSetFsDot11QAPEntry);
    UNUSED_PARAM (pWsscfgIsSetFsDot11QAPEntry);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_WSSCFG_CONFIG_NOT_ALLOWED);
    return OSIX_FAILURE;
#endif
}

/****************************************************************************
 Function    :  WsscfgTestAllFsQAPProfileTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsQAPProfileEntry
                pWsscfgIsSetFsQAPProfileEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsQAPProfileTable (UINT4 *pu4ErrorCode,
                                tWsscfgFsQAPProfileEntry *
                                pWsscfgSetFsQAPProfileEntry,
                                tWsscfgIsSetFsQAPProfileEntry *
                                pWsscfgIsSetFsQAPProfileEntry)
{
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileCWmin != OSIX_FALSE)
    {
        if ((pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileCWmin) > 255)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileCWmax != OSIX_FALSE)
    {
        if ((pWsscfgSetFsQAPProfileEntry->MibObject.
             i4FsQAPProfileCWmax) > 65535)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileAIFSN != OSIX_FALSE)
    {
        if (((pWsscfgSetFsQAPProfileEntry->MibObject.
              i4FsQAPProfileAIFSN) > 15)
            &&
            ((pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileAIFSN) < 2))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsQAPProfileEntry->
        bFsQAPProfileAdmissionControl != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfilePriorityValue != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileDscpValue != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileRowStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileName != OSIX_FALSE)
    {
        if (STRNCMP
            (pWsscfgSetFsQAPProfileEntry->MibObject.
             au1FsQAPProfileName, "default",
             pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileNameLen) == 0)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllFsDot11CapabilityMappingTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsDot11CapabilityMappingEntry
                pWsscfgIsSetFsDot11CapabilityMappingEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsDot11CapabilityMappingTable (UINT4 *pu4ErrorCode,
                                            tWsscfgFsDot11CapabilityMappingEntry
                                            *
                                            pWsscfgSetFsDot11CapabilityMappingEntry,
                                            tWsscfgIsSetFsDot11CapabilityMappingEntry
                                            *
                                            pWsscfgIsSetFsDot11CapabilityMappingEntry,
                                            INT4 i4RowStatusLogic,
                                            INT4 i4RowCreateOption)
{
    if (pWsscfgIsSetFsDot11CapabilityMappingEntry->
        bFsDot11CapabilityMappingRowStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetFsDot11CapabilityMappingEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllFsDot11AuthMappingTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsDot11AuthMappingEntry
                pWsscfgIsSetFsDot11AuthMappingEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsDot11AuthMappingTable (UINT4 *pu4ErrorCode,
                                      tWsscfgFsDot11AuthMappingEntry *
                                      pWsscfgSetFsDot11AuthMappingEntry,
                                      tWsscfgIsSetFsDot11AuthMappingEntry *
                                      pWsscfgIsSetFsDot11AuthMappingEntry,
                                      INT4 i4RowStatusLogic,
                                      INT4 i4RowCreateOption)
{
    if (pWsscfgIsSetFsDot11AuthMappingEntry->bFsDot11AuthMappingRowStatus !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetFsDot11AuthMappingEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllFsDot11QosMappingTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsDot11QosMappingEntry
                pWsscfgIsSetFsDot11QosMappingEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsDot11QosMappingTable (UINT4 *pu4ErrorCode,
                                     tWsscfgFsDot11QosMappingEntry *
                                     pWsscfgSetFsDot11QosMappingEntry,
                                     tWsscfgIsSetFsDot11QosMappingEntry *
                                     pWsscfgIsSetFsDot11QosMappingEntry,
                                     INT4 i4RowStatusLogic,
                                     INT4 i4RowCreateOption)
{
    if (pWsscfgIsSetFsDot11QosMappingEntry->
        bFsDot11QosMappingProfileName != OSIX_FALSE)
    {
        if (pWsscfgSetFsDot11QosMappingEntry->
            MibObject.i4FsDot11QosMappingProfileNameLen > CLI_MAX_PROFILE_NAME)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11QosMappingEntry->
        bFsDot11QosMappingRowStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllFsDot11AntennasListTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsDot11AntennasListEntry
                pWsscfgIsSetFsDot11AntennasListEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsDot11AntennasListTable (UINT4 *pu4ErrorCode,
                                       tWsscfgFsDot11AntennasListEntry *
                                       pWsscfgSetFsDot11AntennasListEntry,
                                       tWsscfgIsSetFsDot11AntennasListEntry *
                                       pWsscfgIsSetFsDot11AntennasListEntry)
{
    if (pWsscfgIsSetFsDot11AntennasListEntry->bFsAntennaMode != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11AntennasListEntry->
             MibObject.i4FsAntennaMode != CLI_ANTENNA_MODE_OMNI)
            && (pWsscfgSetFsDot11AntennasListEntry->
                MibObject.i4FsAntennaMode != CLI_ANTENNA_MODE_LEFTA)
            && (pWsscfgSetFsDot11AntennasListEntry->
                MibObject.i4FsAntennaMode != CLI_ANTENNA_MODE_RIGHTA)
            && (pWsscfgSetFsDot11AntennasListEntry->
                MibObject.i4FsAntennaMode != CLI_ANTENNA_MODE_MIMO))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11AntennasListEntry->bFsAntennaSelection != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11AntennasListEntry->
             MibObject.i4FsAntennaSelection != CLI_ANTENNA_INTERNAL)
            && (pWsscfgSetFsDot11AntennasListEntry->
                MibObject.i4FsAntennaSelection != CLI_ANTENNA_EXTERNAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
        /*Fill your check condition */
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllFsDot11WlanTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsDot11WlanEntry
                pWsscfgIsSetFsDot11WlanEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsDot11WlanTable (UINT4 *pu4ErrorCode,
                               tWsscfgFsDot11WlanEntry *
                               pWsscfgSetFsDot11WlanEntry,
                               tWsscfgIsSetFsDot11WlanEntry *
                               pWsscfgIsSetFsDot11WlanEntry,
                               INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    if (pWsscfgIsSetFsDot11WlanEntry->bFsDot11WlanProfileIfIndex != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanEntry->bFsDot11WlanRowStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetFsDot11WlanEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllFsDot11WlanBindTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsDot11WlanBindEntry
                pWsscfgIsSetFsDot11WlanBindEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsDot11WlanBindTable (UINT4 *pu4ErrorCode,
                                   tWsscfgFsDot11WlanBindEntry *
                                   pWsscfgSetFsDot11WlanBindEntry,
                                   tWsscfgIsSetFsDot11WlanBindEntry *
                                   pWsscfgIsSetFsDot11WlanBindEntry,
                                   INT4 i4RowStatusLogic,
                                   INT4 i4RowCreateOption)
{
    if (pWsscfgIsSetFsDot11WlanBindEntry->bFsDot11WlanBindWlanId != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanBindEntry->
        bFsDot11WlanBindBssIfIndex != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsDot11WlanBindEntry->bFsDot11WlanBindRowStatus !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgSetFsDot11WlanBindEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/*****************************************************************************
Function    :  WsscfgTestAllFsDot11nConfigTable
Input       :  pu4ErrorCode
               pWsscfgSetFsDot11nConfigEntry
               pWsscfgIsSetFsDot11nConfigEntry
Description :  This Routine Take the Indices &
               Test the Value accordingly.
Output      :  None
Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsDot11nConfigTable (UINT4 *pu4ErrorCode,
                                  tWsscfgFsDot11nConfigEntry *
                                  pWsscfgSetFsDot11nConfigEntry,
                                  tWsscfgIsSetFsDot11nConfigEntry *
                                  pWsscfgIsSetFsDot11nConfigEntry)
{
    UINT4               u4GetRadioType = 0;

    if (nmhGetFsDot11RadioType
        (pWsscfgSetFsDot11nConfigEntry->MibObject.i4IfIndex,
         &u4GetRadioType) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }

    if (pWsscfgIsSetFsDot11nConfigEntry->
        bFsDot11nConfigShortGIfor20MHz != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11nConfigEntry->
             MibObject.i4FsDot11nConfigShortGIfor20MHz !=
             CLI_FSDOT11CONFIG_GUARDINTERVAL_ENABLE)
            && (pWsscfgSetFsDot11nConfigEntry->
                MibObject.i4FsDot11nConfigShortGIfor20MHz !=
                CLI_FSDOT11CONFIG_GUARDINTERVAL_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11nConfigEntry->
        bFsDot11nConfigShortGIfor40MHz != OSIX_FALSE)
    {
        if ((pWsscfgSetFsDot11nConfigEntry->
             MibObject.i4FsDot11nConfigShortGIfor40MHz !=
             CLI_FSDOT11CONFIG_GUARDINTERVAL_ENABLE)
            && (pWsscfgSetFsDot11nConfigEntry->
                MibObject.i4FsDot11nConfigShortGIfor40MHz !=
                CLI_FSDOT11CONFIG_GUARDINTERVAL_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pWsscfgIsSetFsDot11nConfigEntry->
        bFsDot11nConfigChannelWidth != OSIX_FALSE)
    {
        if ((u4GetRadioType != RADIO_TYPE_AN) &&
            (u4GetRadioType != RADIO_TYPE_AC))
        {
            if (pWsscfgSetFsDot11nConfigEntry->
                MibObject.i4FsDot11nConfigChannelWidth ==
                CLI_FSDOT11CONFIG_CHANNELWIDTH_40)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }
        }
        if ((pWsscfgSetFsDot11nConfigEntry->
             MibObject.i4FsDot11nConfigChannelWidth !=
             CLI_FSDOT11CONFIG_CHANNELWIDTH_20)
            && (pWsscfgSetFsDot11nConfigEntry->
                MibObject.i4FsDot11nConfigChannelWidth !=
                CLI_FSDOT11CONFIG_CHANNELWIDTH_40))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
Function    :  WsscfgTestAllFsDot11nMCSDataRateTable
Input       :  pu4ErrorCode
               pWsscfgSetFsDot11nMCSDataRateEntry
               pWsscfgIsSetFsDot11nMCSDataRateEntry
Description :  This Routine Take the Indices & Test the Value
               accordingly.
Output      :  None
Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsDot11nMCSDataRateTable (UINT4 *pu4ErrorCode,
                                       tWsscfgFsDot11nMCSDataRateEntry *
                                       pWsscfgSetFsDot11nMCSDataRateEntry,
                                       tWsscfgIsSetFsDot11nMCSDataRateEntry *
                                       pWsscfgIsSetFsDot11nMCSDataRateEntry)
{
    tRadioIfGetDB       RadioIfGetDB;
    INT4                i4ArrayIndex = 0;
    INT4                i4Index = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if (pWsscfgIsSetFsDot11nMCSDataRateEntry->bFsDot11nMCSDataRate !=
        OSIX_FALSE)
    {

        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
            pWsscfgSetFsDot11nMCSDataRateEntry->MibObject.i4IfIndex;

        RadioIfGetDB.RadioIfIsGetAllDB.bSuppRxMcsBitmask = OSIX_TRUE;

        /* Get the Radio IF DB info to fill the structure  */
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N,
                                      &RadioIfGetDB) != OSIX_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return OSIX_FAILURE;

        }

        i4ArrayIndex =
            pWsscfgSetFsDot11nMCSDataRateEntry->MibObject.
            i4FsDot11nMCSDataRateIndex / FSDOT11_MCS_INDEX;
        i4Index =
            pWsscfgSetFsDot11nMCSDataRateEntry->MibObject.
            i4FsDot11nMCSDataRateIndex % FSDOT11_MCS_INDEX;

        if (((RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
              au1SuppRxMcsBitmask[i4ArrayIndex] >> i4Index) &
             FSDOT11_MASK_BIT1) == FSDOT11_MCS_DISABLE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        if ((pWsscfgSetFsDot11nMCSDataRateEntry->
             MibObject.i4FsDot11nMCSDataRate !=
             CLI_NSUPPORTMCSTXRATE_ENABLE)
            && (pWsscfgSetFsDot11nMCSDataRateEntry->
                MibObject.i4FsDot11nMCSDataRate !=
                CLI_NSUPPORTMCSTXRATE_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllFsWtpImageUpgradeTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsWtpImageUpgradeEntry
                pWsscfgIsSetFsWtpImageUpgradeEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsWtpImageUpgradeTable (UINT4 *pu4ErrorCode,
                                     tWsscfgFsWtpImageUpgradeEntry *
                                     pWsscfgSetFsWtpImageUpgradeEntry,
                                     tWsscfgIsSetFsWtpImageUpgradeEntry *
                                     pWsscfgIsSetFsWtpImageUpgradeEntry,
                                     INT4 i4RowStatusLogic,
                                     INT4 i4RowCreateOption)
{
    tWssIfCapDB        *pWssIfCapwapDB = NULL;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4currentProfileId = 0;
    UINT2               u2WtpInternalId = 0;
    UINT2               u2TmpWtpProfiledId = 0;
    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpImageVersion != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpUpgradeDev != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpName != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpImageName != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpAddressType != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpServerIP != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpRowStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }

    if (nmhGetFirstIndexCapwapBaseWtpProfileTable
        (&u4WtpProfileId) != SNMP_SUCCESS)
    {
    }

    do
    {
        u2TmpWtpProfiledId = (UINT2) u4WtpProfileId;
        if (CapwapGetInternalProfildId (&u2TmpWtpProfiledId,
                                        &u2WtpInternalId) != OSIX_SUCCESS)
        {
        }
        pWssIfCapwapDB->CapwapIsGetAllDB.bWtpInternalId = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bImageTransfer = OSIX_TRUE;
        pWssIfCapwapDB->CapwapIsGetAllDB.bProfileName = OSIX_TRUE;
        pWssIfCapwapDB->CapwapGetDB.u2WtpInternalId = u2WtpInternalId;

        if (WssIfProcessCapwapDBMsg (WSS_CAPWAP_GET_DB, pWssIfCapwapDB)
            != OSIX_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        if (pWssIfCapwapDB->CapwapGetDB.u1ImageTransferFlag == OSIX_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_WSS_IMAGE_UPGRADE_ALREADY_IN_PROCESS);
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return OSIX_FAILURE;
        }
        u4currentProfileId = u4WtpProfileId;
    }
    while (nmhGetNextIndexCapwapBaseWtpProfileTable
           (u4currentProfileId, &u4WtpProfileId));

    UNUSED_PARAM (pWsscfgSetFsWtpImageUpgradeEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestFsDot11aNetworkEnable
 Input       :  pu4ErrorCode
                i4FsDot11aNetworkEnable
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgTestFsDot11aNetworkEnable (UINT4 *pu4ErrorCode,
                                 INT4 i4FsDot11aNetworkEnable)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot11aNetworkEnable);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestFsDot11bNetworkEnable
 Input       :  pu4ErrorCode
                i4FsDot11bNetworkEnable
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgTestFsDot11bNetworkEnable (UINT4 *pu4ErrorCode,
                                 INT4 i4FsDot11bNetworkEnable)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot11bNetworkEnable);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestFsDot11gSupport
 Input       :  pu4ErrorCode
                i4FsDot11gSupport
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgTestFsDot11gSupport (UINT4 *pu4ErrorCode, INT4 i4FsDot11gSupport)
{
    if (i4FsDot11gSupport != CLI_NSUPPORT_ENABLE
        && i4FsDot11gSupport != CLI_NSUPPORT_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (pu4ErrorCode);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestFsDot11anSupport
 Input       :  pu4ErrorCode
                i4FsDot11anSupport
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgTestFsDot11anSupport (UINT4 *pu4ErrorCode, INT4 i4FsDot11anSupport)
{
    if (i4FsDot11anSupport != CLI_NSUPPORT_ENABLE
        && i4FsDot11anSupport != CLI_NSUPPORT_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (pu4ErrorCode);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestFsDot11bnSupport
 Input       :  pu4ErrorCode
                i4FsDot11bnSupport
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgTestFsDot11bnSupport (UINT4 *pu4ErrorCode, INT4 i4FsDot11bnSupport)
{
    if (i4FsDot11bnSupport != CLI_NSUPPORT_ENABLE
        && i4FsDot11bnSupport != CLI_NSUPPORT_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (pu4ErrorCode);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestFsDot11ManagmentSSID
 Input       :  pu4ErrorCode
                u4FsDot11ManagmentSSID
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgTestFsDot11ManagmentSSID (UINT4 *pu4ErrorCode,
                                UINT4 u4FsDot11ManagmentSSID)
{

    INT4                i4WlanIfIndex = 0;
    if (nmhGetCapwapDot11WlanProfileIfIndex
        (u4FsDot11ManagmentSSID, &i4WlanIfIndex) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return CLI_FAILURE;
    }
    if ((u4FsDot11ManagmentSSID < 1) || (u4FsDot11ManagmentSSID > 512))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestFsDot11CountryString
 Input       :  pu4ErrorCode
                pFsDot11CountryString
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgTestFsDot11CountryString (UINT4 *pu4ErrorCode,
                                UINT1 *pFsDot11CountryString)
{
    UINT4               u4Index = 0;
    for (u4Index = 0; u4Index < COUNTRY_MAX; u4Index++)
    {
        if (STRCMP
            (pFsDot11CountryString,
             gWsscfgSupportedCountryList[u4Index].au1CountryCode) == 0)
        {
            break;
        }
    }
    if (u4Index == COUNTRY_MAX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestFsSecurityWebAuthType
 Input       :  pu4ErrorCode
                i4FsSecurityWebAuthType
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgTestFsSecurityWebAuthType (UINT4 *pu4ErrorCode,
                                 INT4 i4FsSecurityWebAuthType)
{
    UNUSED_PARAM (pu4ErrorCode);
    if ((i4FsSecurityWebAuthType < WEB_AUTH_TYPE_INTERNAL) ||
        (i4FsSecurityWebAuthType > WEB_AUTH_TYPE_EXTERNAL))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestFsSecurityWebAuthUrl
 Input       :  pu4ErrorCode
                pFsSecurityWebAuthUrl
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgTestFsSecurityWebAuthUrl (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pFsSecurityWebAuthUrl)
{
    if ((pFsSecurityWebAuthUrl->i4_Length) > 128)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestFsSecurityWebAuthRedirectUrl
 Input       :  pu4ErrorCode
                pFsSecurityWebAuthRedirectUrl
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgTestFsSecurityWebAuthRedirectUrl (UINT4 *pu4ErrorCode,
                                        UINT1 *pFsSecurityWebAuthRedirectUrl)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFsSecurityWebAuthRedirectUrl);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestFsSecurityWebAddr
 Input       :  pu4ErrorCode
                i4FsSecurityWebAddr
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgTestFsSecurityWebAddr (UINT4 *pu4ErrorCode, INT4 i4FsSecurityWebAddr)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsSecurityWebAddr);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestFsSecurityWebAuthWebTitle
 Input       :  pu4ErrorCode
                pFsSecurityWebAuthWebTitle
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgTestFsSecurityWebAuthWebTitle (UINT4 *pu4ErrorCode,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsSecurityWebAuthWebTitle)
{
    if ((pFsSecurityWebAuthWebTitle->i4_Length) > 256)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestFsSecurityWebAuthWebMessage
 Input       :  pu4ErrorCode
                pFsSecurityWebAuthWebMessage
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgTestFsSecurityWebAuthWebMessage (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsSecurityWebAuthWebMessage)
{
    if ((pFsSecurityWebAuthWebMessage->i4_Length) > 256)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestFsSecurityWebAuthWebLogoFileName
 Input       :  pu4ErrorCode
                pFsSecurityWebAuthWebLogoFileName
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgTestFsSecurityWebAuthWebLogoFileName (UINT4 *pu4ErrorCode,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsSecurityWebAuthWebLogoFileName)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFsSecurityWebAuthWebLogoFileName);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestFsSecurityWebAuthWebSuccMessage
 Input       :  pu4ErrorCode
                pFsSecurityWebAuthWebSuccMessage
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgTestFsSecurityWebAuthWebSuccMessage (UINT4 *pu4ErrorCode,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsSecurityWebAuthWebSuccMessage)
{

    if ((pFsSecurityWebAuthWebSuccMessage->i4_Length) > 256)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestFsSecurityWebAuthWebFailMessage
 Input       :  pu4ErrorCode
                pFsSecurityWebAuthWebFailMessage
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgTestFsSecurityWebAuthWebFailMessage (UINT4 *pu4ErrorCode,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsSecurityWebAuthWebFailMessage)
{
    if ((pFsSecurityWebAuthWebFailMessage->i4_Length) > 256)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestFsSecurityWebAuthWebButtonText
 Input       :  pu4ErrorCode
                pFsSecurityWebAuthWebButtonText
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgTestFsSecurityWebAuthWebButtonText (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsSecurityWebAuthWebButtonText)
{
    if ((pFsSecurityWebAuthWebButtonText->i4_Length) > 128)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestFsSecurityWebAuthWebLoadBalInfo
 Input       :  pu4ErrorCode
                pFsSecurityWebAuthWebLoadBalInfo
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgTestFsSecurityWebAuthWebLoadBalInfo (UINT4 *pu4ErrorCode,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsSecurityWebAuthWebLoadBalInfo)
{
    if ((pFsSecurityWebAuthWebLoadBalInfo->i4_Length) > 512)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestFsSecurityWebAuthDisplayLang
 Input       :  pu4ErrorCode
                i4FsSecurityWebAuthDisplayLang
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgTestFsSecurityWebAuthDisplayLang (UINT4 *pu4ErrorCode,
                                        INT4 i4FsSecurityWebAuthDisplayLang)
{
    if ((i4FsSecurityWebAuthDisplayLang < WEB_AUTH_LANG_ENGLISH)
        || (i4FsSecurityWebAuthDisplayLang > WEB_AUTH_LANG_RUSSIAN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;

    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestFsSecurityWebAuthColor
 Input       :  pu4ErrorCode
                i4FsSecurityWebAuthColor
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgTestFsSecurityWebAuthColor (UINT4 *pu4ErrorCode,
                                  INT4 i4FsSecurityWebAuthColor)
{
    if ((i4FsSecurityWebAuthColor < WEB_AUTH_COLOR_RED)
        || (i4FsSecurityWebAuthColor > WEB_AUTH_COLOR_BLUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetDot11OperationTable
 Input       :  u4IfIndex
 Description :  This Routine create the operation table
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetDot11OperationTable (UINT4 u4IfIndex, UINT1 u1RowStatus)
{
    tWsscfgDot11OperationEntry *pWsscfgSetDot11OperationEntry = NULL;
    tWsscfgIsSetDot11OperationEntry WsscfgIsSetDot11OperationEntry;
    tWsscfgDot11OperationEntry WsscfgSetDot11OperationEntry;

    MEMSET (&WsscfgSetDot11OperationEntry, 0,
            sizeof (tWsscfgDot11OperationEntry));
    MEMSET (&WsscfgIsSetDot11OperationEntry, OSIX_TRUE,
            sizeof (tWsscfgIsSetDot11OperationEntry));

    WsscfgSetDot11OperationEntry.MibObject.i4IfIndex = (INT4) u4IfIndex;

    pWsscfgSetDot11OperationEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11OperationTable,
                   (tRBElem *) & WsscfgSetDot11OperationEntry);

    if ((pWsscfgSetDot11OperationEntry == NULL) && (u1RowStatus == DESTROY))
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetDot11OperationTable:No entry present. Delete failed\r\n"));
        return OSIX_FAILURE;
    }
    else if ((pWsscfgSetDot11OperationEntry != NULL)
             && (u1RowStatus == CREATE_AND_WAIT))
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetDot11OperationTable:Entry already present.  Creation failed\r\n"));
        return OSIX_FAILURE;
    }
    else if (pWsscfgSetDot11OperationEntry == NULL)
    {
        pWsscfgSetDot11OperationEntry = (tWsscfgDot11OperationEntry *)
            MemAllocMemBlk (WSSCFG_DOT11OPERATIONTABLE_POOLID);
        if (pWsscfgSetDot11OperationEntry == NULL)
        {
            if (WsscfgSetAllDot11OperationTableTrigger
                (&WsscfgSetDot11OperationEntry,
                 &WsscfgIsSetDot11OperationEntry, SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetDot11OperationTable:WsscfgSetAllDot11OperationTableTrigger function fails\r\n"));

            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetDot11OperationTable: Fail to Allocate Memory.\r\n"));
            return OSIX_FAILURE;
        }

        MEMSET (pWsscfgSetDot11OperationEntry, 0,
                sizeof (tWsscfgDot11OperationEntry));
        if ((WsscfgInitializeDot11OperationTable
             (pWsscfgSetDot11OperationEntry)) == OSIX_FAILURE)
        {
            if (WsscfgSetAllDot11OperationTableTrigger
                (&WsscfgSetDot11OperationEntry,
                 &WsscfgIsSetDot11OperationEntry, SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetDot11OperationTable:WsscfgSetAllDot11OperationTableTrigger function fails\r\n"));

            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetDot11OperationTable: Fail to Initialize the Objects.\r\n"));

            MemReleaseMemBlock (WSSCFG_DOT11OPERATIONTABLE_POOLID,
                                (UINT1 *) pWsscfgSetDot11OperationEntry);
            return OSIX_FAILURE;
        }
        /* Assign values for the new node */
        if (WsscfgIsSetDot11OperationEntry.bIfIndex != OSIX_FALSE)
        {
            pWsscfgSetDot11OperationEntry->MibObject.i4IfIndex =
                WsscfgSetDot11OperationEntry.MibObject.i4IfIndex;
        }

        /* Add the new node to the database */
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11OperationTable,
             (tRBElem *) pWsscfgSetDot11OperationEntry) != RB_SUCCESS)
        {
            if (WsscfgSetAllDot11OperationTableTrigger
                (pWsscfgSetDot11OperationEntry,
                 &WsscfgIsSetDot11OperationEntry, SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetDot11OperationTable: WsscfgSetAllDot11OperationTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetDot11OperationTable: RBTreeAdd is failed.\r\n"));

            MemReleaseMemBlock (WSSCFG_DOT11OPERATIONTABLE_POOLID,
                                (UINT1 *) pWsscfgSetDot11OperationEntry);
            return OSIX_FAILURE;
        }

        return OSIX_SUCCESS;

    }
    else if (u1RowStatus == DESTROY)
    {
        RBTreeRem (gWsscfgGlobals.WsscfgGlbMib.Dot11OperationTable,
                   pWsscfgSetDot11OperationEntry);

        if (WsscfgSetAllDot11OperationTableTrigger
            (pWsscfgSetDot11OperationEntry,
             &WsscfgIsSetDot11OperationEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetDot11OperationTable:  function returns failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11OPERATIONTABLE_POOLID,
                                (UINT1 *) pWsscfgSetDot11OperationEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (WSSCFG_DOT11OPERATIONTABLE_POOLID,
                            (UINT1 *) pWsscfgSetDot11OperationEntry);
        return OSIX_SUCCESS;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgTestAllFsRrmConfigTable
 Input       :  pu4ErrorCode
                pWsscfgSetFsRrmConfigEntry
                pWsscfgIsSetFsRrmConfigEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
WsscfgTestAllFsRrmConfigTable (UINT4 *pu4ErrorCode,
                               tWsscfgFsRrmConfigEntry *
                               pWsscfgSetFsRrmConfigEntry,
                               tWsscfgIsSetFsRrmConfigEntry *
                               pWsscfgIsSetFsRrmConfigEntry,
                               INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
#ifndef RFMGMT_WANTED
    INT4                i4TestFsRrmDcaMode = -1;
    INT4                i4TestFsRrmDcaChannelSelectionMode = -1;

    INT4                i4TestFsRrmTpcMode = -1;
    INT4                i4TestFsRrmTpcSelectionMode = -1;

    if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmDcaMode != OSIX_FALSE)
    {

        if (nmhGetFsRrmDcaMode
            (pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmRadioType,
             &i4TestFsRrmDcaMode) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

    }

    if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmDcaChannelSelectionMode !=
        OSIX_FALSE)
    {
        if (nmhGetFsRrmDcaChannelSelectionMode
            (pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmRadioType,
             &i4TestFsRrmDcaChannelSelectionMode) != SNMP_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        if (pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmDcaMode ==
            CLI_RRM_DCA_MODE_GLOBAL)
        {
            if (pWsscfgSetFsRrmConfigEntry->
                MibObject.i4FsRrmDcaChannelSelectionMode ==
                CLI_RRM_DCA_CHANNEL_ONCE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
            else if
                (pWsscfgSetFsRrmConfigEntry->
                 MibObject.i4FsRrmDcaChannelSelectionMode ==
                 CLI_RRM_DCA_CHANNEL_OFF)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_SUCCESS;
            }
            else if
                (pWsscfgSetFsRrmConfigEntry->
                 MibObject.i4FsRrmDcaChannelSelectionMode ==
                 CLI_RRM_DCA_CHANNEL_AUTO)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_SUCCESS;
            }
        }
        if (pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmDcaMode ==
            CLI_RRM_DCA_MODE_PER_AP)
        {
            if (i4TestFsRrmDcaMode == CLI_RRM_DCA_MODE_GLOBAL
                && i4TestFsRrmDcaChannelSelectionMode ==
                CLI_RRM_DCA_CHANNEL_AUTO)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }
            else
            {
                return OSIX_SUCCESS;
            }

        }
    }
    if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmTpcMode != OSIX_FALSE)
    {
        if (nmhGetFsRrmTpcMode
            (pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmRadioType,
             &i4TestFsRrmTpcMode) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

    }

    if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmTpcSelectionMode != OSIX_FALSE)
    {

        if (nmhGetFsRrmTpcSelectionMode
            (pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmRadioType,
             &i4TestFsRrmTpcSelectionMode) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        if (pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmTpcMode ==
            CLI_RRM_DCA_MODE_GLOBAL)
        {
            if (pWsscfgSetFsRrmConfigEntry->
                MibObject.i4FsRrmTpcSelectionMode == CLI_RRM_DCA_CHANNEL_ONCE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
            else if
                (pWsscfgSetFsRrmConfigEntry->
                 MibObject.i4FsRrmTpcSelectionMode == CLI_RRM_DCA_CHANNEL_OFF)
            {
                return OSIX_SUCCESS;
            }
            else if
                (pWsscfgSetFsRrmConfigEntry->
                 MibObject.i4FsRrmTpcSelectionMode == CLI_RRM_DCA_CHANNEL_AUTO)
            {
                return OSIX_SUCCESS;
            }
        }
        if (pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmTpcMode ==
            CLI_RRM_DCA_MODE_PER_AP)
        {
            if ((i4TestFsRrmTpcMode == CLI_RRM_DCA_MODE_GLOBAL)
                && (i4TestFsRrmTpcSelectionMode == CLI_RRM_DCA_CHANNEL_AUTO))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }
            else
            {
                return OSIX_SUCCESS;
            }

        }
    }
    if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmRowStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pWsscfgIsSetFsRrmConfigEntry);
    UNUSED_PARAM (pWsscfgSetFsRrmConfigEntry);
#endif
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    :  WsscfgCheckForAlphaNumeric
* Description :  Validate profile name, only character and numbers check
* Input       :  pu1WtpProfileName
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
WsscfgCheckForAlphaNumeric (UINT1 *pu1WtpProfileName)
{
    UINT2               u2Temp = 0, u2Len = 0;
    tCliHandle          CliHandle;

    MEMSET (&CliHandle, 0, sizeof (tCliHandle));

    u2Len = STRLEN (pu1WtpProfileName);

    for (u2Temp = 0; u2Temp < u2Len; u2Temp++)
    {
        if (((*(pu1WtpProfileName + u2Temp) >= CLI_CAPWAP_NUM_ASCII_START)
             && (*(pu1WtpProfileName + u2Temp) <= CLI_CAPWAP_NUM_ASCII_END)) ||
            ((*(pu1WtpProfileName + u2Temp) >= CLI_CAPWAP_CALPHA_ASCII_START)
             && (*(pu1WtpProfileName + u2Temp) <= CLI_CAPWAP_CALPHA_ASCII_END))
            || ((*(pu1WtpProfileName + u2Temp) >= CLI_CAPWAP_SALPHA_ASCII_START)
                && (*(pu1WtpProfileName + u2Temp) <=
                    CLI_CAPWAP_SALPHA_ASCII_END)))
        {
            continue;
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nOnly characters and numbers are accepted!\r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}
