# include  "lr.h" 
# include  "fssnmp.h" 
# include  "fswssllw.h"
# include  "fswsslwr.h"
# include  "fswssldb.h"


VOID RegisterFSWSSL ()
{
	SNMPRegisterMib (&fswsslOID, &fswsslEntry, SNMP_MSR_TGR_FALSE);
	SNMPAddSysorEntry (&fswsslOID, (const UINT1 *) "fswsslr");
}



VOID UnRegisterFSWSSL ()
{
	SNMPUnRegisterMib (&fswsslOID, &fswsslEntry);
	SNMPDelSysorEntry (&fswsslOID, (const UINT1 *) "fswsslr");
}

INT4 FsWlanDhcpServerStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsWlanDhcpServerStatus(&(pMultiData->i4_SLongValue)));
}
INT4 FsWlanDhcpRelayStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsWlanDhcpRelayStatus(&(pMultiData->i4_SLongValue)));
}
INT4 FsWlanDhcpNextSrvIpAddrGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsWlanDhcpNextSrvIpAddr(&(pMultiData->u4_ULongValue)));
}
INT4 FsWlanFirewallStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsWlanFirewallStatus(&(pMultiData->i4_SLongValue)));
}
INT4 FsWlanNATStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsWlanNATStatus(&(pMultiData->i4_SLongValue)));
}
INT4 FsWlanDNSServerIpAddrGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsWlanDNSServerIpAddr(&(pMultiData->u4_ULongValue)));
}
INT4 FsWlanDefaultRouterIpAddrGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsWlanDefaultRouterIpAddr(&(pMultiData->u4_ULongValue)));
}
INT4 FsWlanDhcpServerStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsWlanDhcpServerStatus(pMultiData->i4_SLongValue));
}


INT4 FsWlanDhcpRelayStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsWlanDhcpRelayStatus(pMultiData->i4_SLongValue));
}


INT4 FsWlanDhcpNextSrvIpAddrSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsWlanDhcpNextSrvIpAddr(pMultiData->u4_ULongValue));
}


INT4 FsWlanFirewallStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsWlanFirewallStatus(pMultiData->i4_SLongValue));
}


INT4 FsWlanNATStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsWlanNATStatus(pMultiData->i4_SLongValue));
}


INT4 FsWlanDNSServerIpAddrSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsWlanDNSServerIpAddr(pMultiData->u4_ULongValue));
}


INT4 FsWlanDefaultRouterIpAddrSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsWlanDefaultRouterIpAddr(pMultiData->u4_ULongValue));
}


INT4 FsWlanDhcpServerStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsWlanDhcpServerStatus(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsWlanDhcpRelayStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsWlanDhcpRelayStatus(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsWlanDhcpNextSrvIpAddrTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsWlanDhcpNextSrvIpAddr(pu4Error, pMultiData->u4_ULongValue));
}


INT4 FsWlanFirewallStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsWlanFirewallStatus(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsWlanNATStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsWlanNATStatus(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsWlanDNSServerIpAddrTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsWlanDNSServerIpAddr(pu4Error, pMultiData->u4_ULongValue));
}


INT4 FsWlanDefaultRouterIpAddrTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsWlanDefaultRouterIpAddr(pu4Error, pMultiData->u4_ULongValue));
}


INT4 FsWlanDhcpServerStatusDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWlanDhcpServerStatus(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsWlanDhcpRelayStatusDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWlanDhcpRelayStatus(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsWlanDhcpNextSrvIpAddrDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWlanDhcpNextSrvIpAddr(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsWlanFirewallStatusDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWlanFirewallStatus(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsWlanNATStatusDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWlanNATStatus(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsWlanDNSServerIpAddrDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWlanDNSServerIpAddr(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsWlanDefaultRouterIpAddrDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWlanDefaultRouterIpAddr(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsWlanRadioIfTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsWlanRadioIfTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsWlanRadioIfTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pFirstMultiIndex->pIndex[1].u4_ULongValue,
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsWlanRadioIfIpAddrGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWlanRadioIfTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWlanRadioIfIpAddr(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsWlanRadioIfIpSubnetMaskGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWlanRadioIfTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWlanRadioIfIpSubnetMask(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsWlanRadioIfAdminStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWlanRadioIfTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWlanRadioIfAdminStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWlanRadioIfRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWlanRadioIfTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWlanRadioIfRowStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWlanRadioIfIpAddrSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWlanRadioIfIpAddr(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsWlanRadioIfIpSubnetMaskSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWlanRadioIfIpSubnetMask(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsWlanRadioIfAdminStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWlanRadioIfAdminStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWlanRadioIfRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWlanRadioIfRowStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWlanRadioIfIpAddrTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWlanRadioIfIpAddr(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsWlanRadioIfIpSubnetMaskTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWlanRadioIfIpSubnetMask(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsWlanRadioIfAdminStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWlanRadioIfAdminStatus(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWlanRadioIfRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWlanRadioIfRowStatus(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWlanRadioIfTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWlanRadioIfTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsDot11WtpDhcpSrvSubnetSubnetGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11WtpDhcpSrvSubnetPoolConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11WtpDhcpSrvSubnetSubnet(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11WtpDhcpSrvSubnetMaskGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11WtpDhcpSrvSubnetPoolConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11WtpDhcpSrvSubnetMask(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11WtpDhcpSrvSubnetStartIpAddressGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11WtpDhcpSrvSubnetPoolConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11WtpDhcpSrvSubnetStartIpAddress(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11WtpDhcpSrvSubnetEndIpAddressGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11WtpDhcpSrvSubnetPoolConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11WtpDhcpSrvSubnetEndIpAddress(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11WtpDhcpSrvSubnetLeaseTimeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11WtpDhcpSrvSubnetPoolConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11WtpDhcpSrvSubnetLeaseTime(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11WtpDhcpSrvDefaultRouterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11WtpDhcpSrvSubnetPoolConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11WtpDhcpSrvDefaultRouter(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11WtpDhcpSrvDnsServerIpGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11WtpDhcpSrvSubnetPoolConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11WtpDhcpSrvDnsServerIp(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11WtpDhcpSrvSubnetPoolRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11WtpDhcpSrvSubnetPoolConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11WtpDhcpSrvSubnetPoolRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11WtpDhcpSrvSubnetSubnetSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11WtpDhcpSrvSubnetSubnet(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11WtpDhcpSrvSubnetMaskSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11WtpDhcpSrvSubnetMask(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11WtpDhcpSrvSubnetStartIpAddressSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11WtpDhcpSrvSubnetStartIpAddress(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11WtpDhcpSrvSubnetEndIpAddressSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11WtpDhcpSrvSubnetEndIpAddress(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11WtpDhcpSrvSubnetLeaseTimeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11WtpDhcpSrvSubnetLeaseTime(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11WtpDhcpSrvDefaultRouterSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11WtpDhcpSrvDefaultRouter(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11WtpDhcpSrvDnsServerIpSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11WtpDhcpSrvDnsServerIp(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11WtpDhcpSrvSubnetPoolRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11WtpDhcpSrvSubnetPoolRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11WtpDhcpSrvSubnetSubnetTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11WtpDhcpSrvSubnetSubnet(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11WtpDhcpSrvSubnetMaskTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11WtpDhcpSrvSubnetMask(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11WtpDhcpSrvSubnetStartIpAddressTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11WtpDhcpSrvSubnetStartIpAddress(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11WtpDhcpSrvSubnetEndIpAddressTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11WtpDhcpSrvSubnetEndIpAddress(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11WtpDhcpSrvSubnetLeaseTimeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11WtpDhcpSrvSubnetLeaseTime(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11WtpDhcpSrvDefaultRouterTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11WtpDhcpSrvDefaultRouter(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11WtpDhcpSrvDnsServerIpTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11WtpDhcpSrvDnsServerIp(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11WtpDhcpSrvSubnetPoolRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11WtpDhcpSrvSubnetPoolRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11WtpDhcpSrvSubnetPoolConfigTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsDot11WtpDhcpSrvSubnetPoolConfigTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsWtpNATTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsWtpNATTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsWtpNATTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsWtpIfMainWanTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpNATTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpIfMainWanType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpNATConfigRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpNATTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpNATConfigRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpIfMainWanTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpIfMainWanType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpNATConfigRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpNATConfigRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfMainWanTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpIfMainWanType(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpNATConfigRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpNATConfigRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpNATTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWtpNATTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsWtpDhcpConfigTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsWtpDhcpConfigTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsWtpDhcpConfigTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsWtpDhcpServerStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpDhcpConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpDhcpServerStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpDhcpRelayStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpDhcpConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpDhcpRelayStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpDhcpServerStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpDhcpServerStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpDhcpRelayStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpDhcpRelayStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpDhcpServerStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpDhcpServerStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpDhcpRelayStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpDhcpRelayStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpDhcpConfigTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWtpDhcpConfigTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsWtpFirewallConfigTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsWtpFirewallConfigTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsWtpFirewallConfigTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsWtpFirewallStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpFirewallConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpFirewallStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpFirewallRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpFirewallConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpFirewallRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpFirewallStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpFirewallStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFirewallRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpFirewallRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFirewallStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpFirewallStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFirewallRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpFirewallRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFirewallConfigTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWtpFirewallConfigTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsWtpIpRouteConfigTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsWtpIpRouteConfigTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].u4_ULongValue),
			&(pNextMultiIndex->pIndex[2].u4_ULongValue),
			&(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsWtpIpRouteConfigTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].u4_ULongValue,
			&(pNextMultiIndex->pIndex[1].u4_ULongValue),
			pFirstMultiIndex->pIndex[2].u4_ULongValue,
			&(pNextMultiIndex->pIndex[2].u4_ULongValue),
			pFirstMultiIndex->pIndex[3].u4_ULongValue,
			&(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsWtpIpRouteConfigRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpIpRouteConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpIpRouteConfigRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpIpRouteConfigRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpIpRouteConfigRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIpRouteConfigRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpIpRouteConfigRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIpRouteConfigTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWtpIpRouteConfigTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsWtpFwlFilterTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsWtpFwlFilterTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsWtpFwlFilterTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].pOctetStrValue,
			pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsWtpFwlFilterSrcAddressGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpFwlFilterTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpFwlFilterSrcAddress(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->pOctetStrValue));

}
INT4 FsWtpFwlFilterDestAddressGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpFwlFilterTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpFwlFilterDestAddress(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->pOctetStrValue));

}
INT4 FsWtpFwlFilterProtocolGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpFwlFilterTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpFwlFilterProtocol(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpFwlFilterSrcPortGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpFwlFilterTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpFwlFilterSrcPort(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->pOctetStrValue));

}
INT4 FsWtpFwlFilterDestPortGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpFwlFilterTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpFwlFilterDestPort(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->pOctetStrValue));

}
INT4 FsWtpFwlFilterTosGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpFwlFilterTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpFwlFilterTos(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpFwlFilterAccountingGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpFwlFilterTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpFwlFilterAccounting(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpFwlFilterHitClearGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpFwlFilterTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpFwlFilterHitClear(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpFwlFilterHitsCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpFwlFilterTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpFwlFilterHitsCount(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsWtpFwlFilterAddrTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpFwlFilterTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpFwlFilterAddrType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpFwlFilterFlowIdGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpFwlFilterTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpFwlFilterFlowId(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsWtpFwlFilterDscpGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpFwlFilterTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpFwlFilterDscp(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpFwlFilterRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpFwlFilterTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpFwlFilterRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpFwlFilterSrcAddressSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpFwlFilterSrcAddress(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->pOctetStrValue));

}

INT4 FsWtpFwlFilterDestAddressSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpFwlFilterDestAddress(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->pOctetStrValue));

}

INT4 FsWtpFwlFilterProtocolSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpFwlFilterProtocol(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlFilterSrcPortSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpFwlFilterSrcPort(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->pOctetStrValue));

}

INT4 FsWtpFwlFilterDestPortSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpFwlFilterDestPort(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->pOctetStrValue));

}

INT4 FsWtpFwlFilterTosSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpFwlFilterTos(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlFilterAccountingSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpFwlFilterAccounting(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlFilterHitClearSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpFwlFilterHitClear(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlFilterAddrTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpFwlFilterAddrType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlFilterFlowIdSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpFwlFilterFlowId(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->u4_ULongValue));

}

INT4 FsWtpFwlFilterDscpSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpFwlFilterDscp(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlFilterRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpFwlFilterRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlFilterSrcAddressTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpFwlFilterSrcAddress(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->pOctetStrValue));

}

INT4 FsWtpFwlFilterDestAddressTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpFwlFilterDestAddress(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->pOctetStrValue));

}

INT4 FsWtpFwlFilterProtocolTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpFwlFilterProtocol(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlFilterSrcPortTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpFwlFilterSrcPort(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->pOctetStrValue));

}

INT4 FsWtpFwlFilterDestPortTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpFwlFilterDestPort(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->pOctetStrValue));

}

INT4 FsWtpFwlFilterTosTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpFwlFilterTos(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlFilterAccountingTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpFwlFilterAccounting(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlFilterHitClearTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpFwlFilterHitClear(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlFilterAddrTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpFwlFilterAddrType(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlFilterFlowIdTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpFwlFilterFlowId(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->u4_ULongValue));

}

INT4 FsWtpFwlFilterDscpTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpFwlFilterDscp(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlFilterRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpFwlFilterRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlFilterTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWtpFwlFilterTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsWtpFwlRuleTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsWtpFwlRuleTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsWtpFwlRuleTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].pOctetStrValue,
			pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsWtpFwlRuleFilterSetGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpFwlRuleTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpFwlRuleFilterSet(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->pOctetStrValue));

}
INT4 FsWtpFwlRuleRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpFwlRuleTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpFwlRuleRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpFwlRuleFilterSetSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpFwlRuleFilterSet(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->pOctetStrValue));

}

INT4 FsWtpFwlRuleRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpFwlRuleRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlRuleFilterSetTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpFwlRuleFilterSet(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->pOctetStrValue));

}

INT4 FsWtpFwlRuleRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpFwlRuleRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlRuleTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWtpFwlRuleTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsWtpFwlAclTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsWtpFwlAclTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pNextMultiIndex->pIndex[2].pOctetStrValue,
			&(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsWtpFwlAclTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].pOctetStrValue,
			pNextMultiIndex->pIndex[2].pOctetStrValue,
			pFirstMultiIndex->pIndex[3].i4_SLongValue,
			&(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsWtpFwlAclActionGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpFwlAclTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpFwlAclAction(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpFwlAclSequenceNumberGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpFwlAclTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpFwlAclSequenceNumber(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpFwlAclLogTriggerGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpFwlAclTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpFwlAclLogTrigger(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpFwlAclFragActionGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpFwlAclTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpFwlAclFragAction(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpFwlAclRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpFwlAclTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpFwlAclRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpFwlAclActionSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpFwlAclAction(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlAclSequenceNumberSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpFwlAclSequenceNumber(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlAclLogTriggerSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpFwlAclLogTrigger(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlAclFragActionSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpFwlAclFragAction(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlAclRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpFwlAclRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlAclActionTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpFwlAclAction(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlAclSequenceNumberTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpFwlAclSequenceNumber(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlAclLogTriggerTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpFwlAclLogTrigger(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlAclFragActionTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpFwlAclFragAction(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlAclRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpFwlAclRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].pOctetStrValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpFwlAclTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWtpFwlAclTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 GetNextIndexFsWtpNatDynamicTransTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsWtpNatDynamicTransTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].u4_ULongValue),
			&(pNextMultiIndex->pIndex[3].i4_SLongValue),
			&(pNextMultiIndex->pIndex[4].u4_ULongValue),
			&(pNextMultiIndex->pIndex[5].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsWtpNatDynamicTransTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].u4_ULongValue,
			&(pNextMultiIndex->pIndex[2].u4_ULongValue),
			pFirstMultiIndex->pIndex[3].i4_SLongValue,
			&(pNextMultiIndex->pIndex[3].i4_SLongValue),
			pFirstMultiIndex->pIndex[4].u4_ULongValue,
			&(pNextMultiIndex->pIndex[4].u4_ULongValue),
			pFirstMultiIndex->pIndex[5].i4_SLongValue,
			&(pNextMultiIndex->pIndex[5].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsWtpNatDynamicTransTranslatedLocalIpGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpNatDynamicTransTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].u4_ULongValue,
		pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpNatDynamicTransTranslatedLocalIp(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].u4_ULongValue,
		pMultiIndex->pIndex[5].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsWtpNatDynamicTransTranslatedLocalPortGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpNatDynamicTransTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].u4_ULongValue,
		pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpNatDynamicTransTranslatedLocalPort(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].u4_ULongValue,
		pMultiIndex->pIndex[5].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpNatDynamicTransLastUseTimeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpNatDynamicTransTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].u4_ULongValue,
		pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpNatDynamicTransLastUseTime(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].u4_ULongValue,
		pMultiIndex->pIndex[5].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}

INT4 GetNextIndexFsWtpNatGlobalAddressTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsWtpNatGlobalAddressTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsWtpNatGlobalAddressTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].u4_ULongValue,
			&(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsWtpNatGlobalAddressMaskGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpNatGlobalAddressTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpNatGlobalAddressMask(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsWtpNatGlobalAddressEntryStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpNatGlobalAddressTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpNatGlobalAddressEntryStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpNatGlobalAddressMaskSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpNatGlobalAddressMask(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsWtpNatGlobalAddressEntryStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpNatGlobalAddressEntryStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpNatGlobalAddressMaskTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpNatGlobalAddressMask(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsWtpNatGlobalAddressEntryStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpNatGlobalAddressEntryStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpNatGlobalAddressTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWtpNatGlobalAddressTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsWtpNatLocalAddressTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsWtpNatLocalAddressTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsWtpNatLocalAddressTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].u4_ULongValue,
			&(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsWtpNatLocalAddressMaskGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpNatLocalAddressTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpNatLocalAddressMask(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsWtpNatLocalAddressEntryStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpNatLocalAddressTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpNatLocalAddressEntryStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpNatLocalAddressMaskSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpNatLocalAddressMask(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsWtpNatLocalAddressEntryStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpNatLocalAddressEntryStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpNatLocalAddressMaskTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpNatLocalAddressMask(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsWtpNatLocalAddressEntryStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpNatLocalAddressEntryStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpNatLocalAddressTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWtpNatLocalAddressTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsWtpNatStaticTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsWtpNatStaticTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsWtpNatStaticTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].u4_ULongValue,
			&(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsWtpNatStaticTranslatedLocalIpGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpNatStaticTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpNatStaticTranslatedLocalIp(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsWtpNatStaticEntryStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpNatStaticTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpNatStaticEntryStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpNatStaticTranslatedLocalIpSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpNatStaticTranslatedLocalIp(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsWtpNatStaticEntryStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpNatStaticEntryStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpNatStaticTranslatedLocalIpTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpNatStaticTranslatedLocalIp(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsWtpNatStaticEntryStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpNatStaticEntryStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpNatStaticTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWtpNatStaticTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsWtpNatStaticNaptTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsWtpNatStaticNaptTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].u4_ULongValue),
			&(pNextMultiIndex->pIndex[3].i4_SLongValue),
			&(pNextMultiIndex->pIndex[4].i4_SLongValue),
			&(pNextMultiIndex->pIndex[5].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsWtpNatStaticNaptTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].u4_ULongValue,
			&(pNextMultiIndex->pIndex[2].u4_ULongValue),
			pFirstMultiIndex->pIndex[3].i4_SLongValue,
			&(pNextMultiIndex->pIndex[3].i4_SLongValue),
			pFirstMultiIndex->pIndex[4].i4_SLongValue,
			&(pNextMultiIndex->pIndex[4].i4_SLongValue),
			pFirstMultiIndex->pIndex[5].i4_SLongValue,
			&(pNextMultiIndex->pIndex[5].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsWtpNatStaticNaptTranslatedLocalIpGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpNatStaticNaptTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpNatStaticNaptTranslatedLocalIp(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiIndex->pIndex[5].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsWtpNatStaticNaptTranslatedLocalPortGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpNatStaticNaptTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpNatStaticNaptTranslatedLocalPort(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiIndex->pIndex[5].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpNatStaticNaptDescriptionGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpNatStaticNaptTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpNatStaticNaptDescription(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiIndex->pIndex[5].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsWtpNatStaticNaptEntryStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpNatStaticNaptTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpNatStaticNaptEntryStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiIndex->pIndex[5].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpNatStaticNaptTranslatedLocalIpSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpNatStaticNaptTranslatedLocalIp(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiIndex->pIndex[5].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsWtpNatStaticNaptTranslatedLocalPortSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpNatStaticNaptTranslatedLocalPort(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiIndex->pIndex[5].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpNatStaticNaptDescriptionSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpNatStaticNaptDescription(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiIndex->pIndex[5].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 FsWtpNatStaticNaptEntryStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpNatStaticNaptEntryStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiIndex->pIndex[5].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpNatStaticNaptTranslatedLocalIpTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpNatStaticNaptTranslatedLocalIp(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiIndex->pIndex[5].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsWtpNatStaticNaptTranslatedLocalPortTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpNatStaticNaptTranslatedLocalPort(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiIndex->pIndex[5].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpNatStaticNaptDescriptionTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpNatStaticNaptDescription(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiIndex->pIndex[5].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 FsWtpNatStaticNaptEntryStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpNatStaticNaptEntryStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiIndex->pIndex[4].i4_SLongValue,
		pMultiIndex->pIndex[5].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpNatStaticNaptTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWtpNatStaticNaptTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsWtpNatIfTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsWtpNatIfTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsWtpNatIfTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsWtpNatIfNatGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpNatIfTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpNatIfNat(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpNatIfNaptGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpNatIfTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpNatIfNapt(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpNatIfTwoWayNatGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpNatIfTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpNatIfTwoWayNat(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpNatIfEntryStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpNatIfTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpNatIfEntryStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpNatIfNatSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpNatIfNat(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpNatIfNaptSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpNatIfNapt(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpNatIfTwoWayNatSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpNatIfTwoWayNat(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpNatIfEntryStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpNatIfEntryStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpNatIfNatTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpNatIfNat(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpNatIfNaptTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpNatIfNapt(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpNatIfTwoWayNatTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpNatIfTwoWayNat(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpNatIfEntryStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpNatIfEntryStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpNatIfTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWtpNatIfTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsWtpVlanStaticTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsWtpVlanStaticTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsWtpVlanStaticTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].u4_ULongValue,
			&(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsWtpVlanStaticNameGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpVlanStaticTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpVlanStaticName(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsWtpVlanStaticRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpVlanStaticTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpVlanStaticRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpVlanStaticNameSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpVlanStaticName(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiData->pOctetStrValue));

}

INT4 FsWtpVlanStaticRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpVlanStaticRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpVlanStaticNameTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpVlanStaticName(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiData->pOctetStrValue));

}

INT4 FsWtpVlanStaticRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpVlanStaticRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpVlanStaticTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWtpVlanStaticTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsWtpVlanStaticPortConfigTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsWtpVlanStaticPortConfigTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].u4_ULongValue),
			&(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsWtpVlanStaticPortConfigTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].u4_ULongValue,
			&(pNextMultiIndex->pIndex[2].u4_ULongValue),
			pFirstMultiIndex->pIndex[3].i4_SLongValue,
			&(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsWtpVlanStaticPortGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpVlanStaticPortConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpVlanStaticPort(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpVlanStaticPortSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpVlanStaticPort(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpVlanStaticPortTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpVlanStaticPort(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiIndex->pIndex[3].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpVlanStaticPortConfigTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWtpVlanStaticPortConfigTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsWtpIfMainTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsWtpIfMainTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsWtpIfMainTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsWtpIfMainTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpIfMainTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpIfMainType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpIfMainMtuGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpIfMainTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpIfMainMtu(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpIfMainAdminStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpIfMainTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpIfMainAdminStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpIfMainOperStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpIfMainTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpIfMainOperStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpIfMainEncapTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpIfMainTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpIfMainEncapType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpIfMainBrgPortTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpIfMainTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpIfMainBrgPortType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpIfMainRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpIfMainTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpIfMainRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpIfMainSubTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpIfMainTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpIfMainSubType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpIfMainNetworkTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpIfMainTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpIfMainNetworkType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpIfWanTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpIfMainTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpIfWanType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpIfMainEncapDot1qVlanIdGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpIfMainTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpIfMainEncapDot1qVlanId(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsWtpIfIpAddrGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpIfMainTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpIfIpAddr(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsWtpIfIpSubnetMaskGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpIfMainTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpIfIpSubnetMask(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsWtpIfIpBroadcastAddrGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpIfMainTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpIfIpBroadcastAddr(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsWtpIfMainPhyPortGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsWtpIfMainTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsWtpIfMainPhyPort(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}

INT4 FsWtpIfMainTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpIfMainType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfMainMtuSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpIfMainMtu(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfMainAdminStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpIfMainAdminStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfMainEncapTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpIfMainEncapType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfMainBrgPortTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpIfMainBrgPortType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfMainRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpIfMainRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfMainSubTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpIfMainSubType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfMainNetworkTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpIfMainNetworkType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfWanTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpIfWanType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfMainEncapDot1qVlanIdSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpIfMainEncapDot1qVlanId(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfIpAddrSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpIfIpAddr(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsWtpIfIpSubnetMaskSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpIfIpSubnetMask(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsWtpIfIpBroadcastAddrSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpIfIpBroadcastAddr(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsWtpIfMainPhyPortSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsWtpIfMainPhyPort(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfMainTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpIfMainType(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfMainMtuTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpIfMainMtu(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfMainAdminStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpIfMainAdminStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfMainEncapTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpIfMainEncapType(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfMainBrgPortTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpIfMainBrgPortType(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfMainRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpIfMainRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfMainSubTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpIfMainSubType(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfMainNetworkTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpIfMainNetworkType(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfWanTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpIfWanType(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfMainEncapDot1qVlanIdTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpIfMainEncapDot1qVlanId(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfIpAddrTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpIfIpAddr(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsWtpIfIpSubnetMaskTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpIfIpSubnetMask(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsWtpIfIpBroadcastAddrTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpIfIpBroadcastAddr(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsWtpIfMainPhyPortTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsWtpIfMainPhyPort(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsWtpIfMainTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsWtpIfMainTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

