/******************************************************************************

 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: std80211ncli.c,v 1.3 2017/12/12 13:28:32 siva Exp $
 * 
 * Description: This file contains the routines for fs11n module
 *          related cli commands.
 * 
 *******************************************************************************/
#ifndef __FS11NCLI_C__
#define __FS11NCLI_C__

#include "wsscfginc.h"
#include "wssifpmdb.h"
#include "wsscfgcli.h"
#include "capwapcli.h"
#include "wsspm.h"
#include "wsscfgprot.h"
#include "fpam.h"

/*******************************************************************************
 * Function    :  cli_process_std11n_cmd
 * Description :  This function is exported to CLI module to handle the
 *                  fs11n cli commands to take the corresponding action.
 *
 * Input       :  Variable arguments
 *
 * Output      :  None
 *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ******************************************************************************/
INT4
cli_process_std11n_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[WSSCFG_CLI_MAX_ARGS];
    INT1                i1argno = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    INT4                i4Inst = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4ErrCode = 0;

    CliRegisterLock (CliHandle, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    /* Walk through the rest of the arguements and store in args array.
     * Store RFMGMT_CLI_MAX_ARGS arguements at the max.
     */

    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }

    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == WSSCFG_CLI_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);

    switch (u4Command)
    {
        case CLI_FS11N_SET_LDPCOPTION:
            i4RetStatus = Fs11nCliSetLdpcOption (CliHandle,
                                                 (INT4) *args[0],
                                                 *args[1],
                                                 (UINT1 *) args[2], args[3]);
            break;

        case CLI_FS11N_SET_POWER_SAVE_MODE:
            i4RetStatus = Fs11nCliSetPowerSaverMode (CliHandle,
                                                     (INT4) *args[0],
                                                     *args[1],
                                                     (UINT1 *) args[2],
                                                     args[3]);
            break;

        case CLI_FS11N_SET_GREENFIELDOPTION:
            i4RetStatus = Fs11nCliSetGreenFieldOption (CliHandle,
                                                       (INT4) *args[0],
                                                       *args[1],
                                                       (UINT1 *) args[2],
                                                       args[3]);
            break;

        case CLI_FS11N_SET_SHORTGUARDINTERVAL:
            i4RetStatus = Fs11nCliSetShort20GaurdInterval (CliHandle,
                                                           (INT4) *args[0],
                                                           *args[1],
                                                           (UINT1 *) args[2],
                                                           args[3]);
            break;

        case CLI_FS11N_SET_GUARDINTERVAL:
            i4RetStatus = Fs11nCliSetShortGaurdInterval (CliHandle,
                                                         (INT4) *args[0],
                                                         *args[1],
                                                         (UINT1 *) args[2],
                                                         args[3]);
            break;

        case CLI_FS11N_SET_TXSTBCOPTION:
            i4RetStatus = Fs11nCliSetTxStbcOption (CliHandle,
                                                   (INT4) *args[0],
                                                   *args[1],
                                                   (UINT1 *) args[2], args[3]);
            break;

        case CLI_FS11N_SET_RXSTBCOPTION:
            i4RetStatus = Fs11nCliSetRxStbcOption (CliHandle,
                                                   (INT4) *args[0],
                                                   *args[1],
                                                   (UINT1 *) args[2], args[3]);
            break;

        case CLI_FS11N_SET_DELAYBLOCKACKOPTION:
            i4RetStatus = Fs11nCliSetDelayBlockAckOption (CliHandle,
                                                          (INT4) *args[0],
                                                          *args[1],
                                                          (UINT1 *) args[2],
                                                          args[3]);
            break;

        case CLI_FS11N_SET_MAXAMSDULEMGTH:
            i4RetStatus = Fs11nCliSetMaxAmsduLength (CliHandle,
                                                     (INT4) *args[0],
                                                     *args[1],
                                                     (UINT1 *) args[2],
                                                     args[3]);
            break;

        case CLI_FS11N_SET_DSSCCKMODEOPTION:
            i4RetStatus = Fs11nCliSetDsscckModeOption (CliHandle,
                                                       (INT4) *args[0],
                                                       *args[1],
                                                       (UINT1 *) args[2],
                                                       args[3]);
            break;

        case CLI_FS11N_SET_FORTYMHZINTOLERANTOPTION:
            i4RetStatus = Fs11nCliSetFortyMhzIntolarantOption (CliHandle,
                                                               (INT4) *args[0],
                                                               *args[1],
                                                               (UINT1 *)
                                                               args[2],
                                                               args[3]);
            break;

        case CLI_FS11N_SET_LSIGTXOPPROTECTIONOPTION:
            i4RetStatus = Fs11nCliSetLSigTxOpProtectionOption (CliHandle,
                                                               (INT4) *args[0],
                                                               *args[1],
                                                               (UINT1 *)
                                                               args[2],
                                                               args[3]);
            break;
        case CLI_FS11N_SET_MAXMPDU:
            i4RetStatus = Fs11nCliSetMaxMpduOption (CliHandle,
                                                    (INT4) *args[0],
                                                    *args[1],
                                                    (UINT1 *) args[2], args[3]);
            break;

        case CLI_FS11N_SET_AMPDUFACTOR:
            i4RetStatus = Fs11nCliSetAmpduFactorOption (CliHandle,
                                                        (INT4) *args[0],
                                                        *args[1],
                                                        (UINT1 *) args[2],
                                                        args[3]);
            break;

        case CLI_FS11N_SET_PCO_OPTION:
            i4RetStatus = Fs11nCliSetPcoOption (CliHandle,
                                                (INT4) *args[0],
                                                *args[1],
                                                (UINT1 *) args[2], args[3]);
            break;

        case CLI_FS11N_SET_PCOTRANSITIONTIME:
            i4RetStatus = Fs11nCliSetPcoTransitionTimeOption (CliHandle,
                                                              (INT4) *args[0],
                                                              *args[1],
                                                              (UINT1 *) args[2],
                                                              args[3]);
            break;

        case CLI_FS11N_SET_MCSFEEDBACKOPTION:
            i4RetStatus = Fs11nCliSetMcsFeedbackOption (CliHandle,
                                                        (INT4) *args[0],
                                                        *args[1],
                                                        (UINT1 *) args[2],
                                                        args[3]);
            break;

        case CLI_FS11N_SET_HTCFIELDOPTION:
            i4RetStatus = Fs11nCliSetHtControlFieldOption (CliHandle,
                                                           (INT4) *args[0],
                                                           *args[1],
                                                           (UINT1 *) args[2],
                                                           args[3]);
            break;

        case CLI_FS11N_SET_RDRESPONDEROPTION:
            i4RetStatus = Fs11nCliSetRevDResponderOption (CliHandle,
                                                          (INT4) *args[0],
                                                          *args[1],
                                                          (UINT1 *) args[2],
                                                          args[3]);
            break;

        case CLI_FS11N_SET_IMPTRANBEAMRECVOPTION:
            i4RetStatus = Fs11nCliSetLImpTranBeamRecvOption (CliHandle,
                                                             (INT4) *args[0],
                                                             *args[1],
                                                             (UINT1 *) args[2],
                                                             args[3]);
            break;

        case CLI_FS11N_SET_RECVSTAGSOUNDOPTION:
            i4RetStatus = Fs11nCliSetRecvStagSoundOption (CliHandle,
                                                          (INT4) *args[0],
                                                          *args[1],
                                                          (UINT1 *) args[2],
                                                          args[3]);
            break;

        case CLI_FS11N_SET_TRANSTAGSOUNDOPTION:
            i4RetStatus = Fs11nCliSetTranStagSoundOption (CliHandle,
                                                          (INT4) *args[0],
                                                          *args[1],
                                                          (UINT1 *) args[2],
                                                          args[3]);
            break;

        case CLI_FS11N_SET_RECVNULLDATAFRAMINGOPTION:
            i4RetStatus = Fs11nCliSetRecvNullDataFramingOption (CliHandle,
                                                                (INT4) *args[0],
                                                                *args[1],
                                                                (UINT1 *)
                                                                args[2],
                                                                args[3]);
            break;

        case CLI_FS11N_SET_TRANNULLDATAFRAMINGOPTION:
            i4RetStatus = Fs11nCliSetTranNullDataFramingOption (CliHandle,
                                                                (INT4) *args[0],
                                                                *args[1],
                                                                (UINT1 *)
                                                                args[2],
                                                                args[3]);
            break;

        case CLI_FS11N_SET_IMPTRANBEAMOPTION:
            i4RetStatus = Fs11nCliSetImpTranBeamOption (CliHandle,
                                                        (INT4) *args[0],
                                                        *args[1],
                                                        (UINT1 *) args[2],
                                                        args[3]);
            break;

        case CLI_FS11N_SET_CALIBRATIONOPTION:
            i4RetStatus = Fs11nCliSetCalibrationOption (CliHandle,
                                                        (INT4) *args[0],
                                                        *args[1],
                                                        (UINT1 *) args[2],
                                                        args[3]);
            break;

        case CLI_FS11N_SET_EXPCSITRANBEAMOPTION:
            i4RetStatus = Fs11nCliSetExplCsiTranBeamOption (CliHandle,
                                                            (INT4) *args[0],
                                                            *args[1],
                                                            (UINT1 *) args[2],
                                                            args[3]);
            break;

        case CLI_FS11N_SET_EXPNONCOMPSTEOPTION:
            i4RetStatus = Fs11nCliSetExplNonCompSterOption (CliHandle,
                                                            (INT4) *args[0],
                                                            *args[1],
                                                            (UINT1 *) args[2],
                                                            args[3]);
            break;

        case CLI_FS11N_SET_EXPCOMPSTEOPTION:
            i4RetStatus = Fs11nCliSetExplCompSterOption (CliHandle,
                                                         (INT4) *args[0],
                                                         *args[1],
                                                         (UINT1 *) args[2],
                                                         args[3]);
            break;

        case CLI_FS11N_SET_EXPTRANBEAMCSIOPTION:
            i4RetStatus = Fs11nCliSetExplTranBeamCsiOption (CliHandle,
                                                            (INT4) *args[0],
                                                            *args[1],
                                                            (UINT1 *) args[2],
                                                            args[3]);
            break;

        case CLI_FS11N_SET_EXPNCOMPBEAMOPTION:
            i4RetStatus = Fs11nCliSetExplNonCompBeamOption (CliHandle,
                                                            (INT4) *args[0],
                                                            *args[1],
                                                            (UINT1 *) args[2],
                                                            args[3]);
            break;

        case CLI_FS11N_SET_EXPCOMPBEAMOPTION:
            i4RetStatus = Fs11nCliSetExplCompBeamOption (CliHandle,
                                                         (INT4) *args[0],
                                                         *args[1],
                                                         (UINT1 *) args[2],
                                                         args[3]);
            break;

        case CLI_FS11N_SET_MINGROUPINGOPTION:
            i4RetStatus = Fs11nCliSetMinGroupingOption (CliHandle,
                                                        (INT4) *args[0],
                                                        *args[1],
                                                        (UINT1 *) args[2],
                                                        args[3]);
            break;
        case CLI_FS11N_SET_NUMCSIANTSUPOPTION:
            i4RetStatus = Fs11nCliSetNumCsiAnteSupOption (CliHandle,
                                                          (INT4) *args[0],
                                                          *args[1],
                                                          (UINT1 *) args[2],
                                                          args[3]);
            break;

        case CLI_FS11N_SET_NUMNONCOMPANTSUPOPTION:
            i4RetStatus = Fs11nCliSetNumNonCompAnteSupOption (CliHandle,
                                                              (INT4) *args[0],
                                                              *args[1],
                                                              (UINT1 *) args[2],
                                                              args[3]);
            break;

        case CLI_FS11N_SET_NUMCOMPANTSUPOPTION:
            i4RetStatus = Fs11nCliSetNumCompAnteSupOption (CliHandle,
                                                           (INT4) *args[0],
                                                           *args[1],
                                                           (UINT1 *) args[2],
                                                           args[3]);
            break;

        case CLI_FS11N_SET_MAXNUMCSIANTSUPOPTION:
            i4RetStatus = Fs11nCliSetMaxNumCsiAnteSupOption (CliHandle,
                                                             (INT4) *args[0],
                                                             *args[1],
                                                             (UINT1 *) args[2],
                                                             args[3]);
            break;

        case CLI_FS11N_SET_CHANNELESTVALUEOPTION:
            i4RetStatus = Fs11nCliSetChannelEstValOption (CliHandle,
                                                          (INT4) *args[0],
                                                          *args[1],
                                                          (UINT1 *) args[2],
                                                          args[3]);
            break;

        case CLI_FS11N_SET_PRIMARYCHANNELOPTION:
            i4RetStatus = Fs11nCliSetPriChannelOption (CliHandle,
                                                       (INT4) *args[0],
                                                       *args[1],
                                                       (UINT1 *) args[2],
                                                       args[3]);
            break;

        case CLI_FS11N_SET_SECONDARYCHANNELOPTION:
            i4RetStatus = Fs11nCliSetSecChannelOption (CliHandle,
                                                       (INT4) *args[0],
                                                       *args[1],
                                                       (UINT1 *) args[2],
                                                       args[3]);
            break;

        case CLI_FS11N_SET_HTOPCHANNELWIDTHOPTION:
            i4RetStatus = Fs11nCliSetHtOpChannelWidthOption (CliHandle,
                                                             (INT4) *args[0],
                                                             *args[1],
                                                             (UINT1 *) args[2],
                                                             args[3]);
            break;

        case CLI_FS11N_SET_RIFSMODEOPTION:
            i4RetStatus = Fs11nCliSetRifsModeOption (CliHandle,
                                                     (INT4) *args[0],
                                                     *args[1],
                                                     (UINT1 *) args[2],
                                                     args[3]);
            break;

        case CLI_FS11N_SET_HTPROTECTIONOPTION:
            i4RetStatus = Fs11nCliSetHtProtectionOption (CliHandle,
                                                         (INT4) *args[0],
                                                         *args[1],
                                                         (UINT1 *) args[2],
                                                         args[3]);
            break;

        case CLI_FS11N_SET_NONGFSTAOPTION:
            i4RetStatus = Fs11nCliSetNonGFStaOption (CliHandle,
                                                     (INT4) *args[0],
                                                     *args[1],
                                                     (UINT1 *) args[2],
                                                     args[3]);
            break;

        case CLI_FS11N_SET_OBSSNONHTSTAOPTION:
            i4RetStatus = Fs11nCliSetObssNonHtStaOption (CliHandle,
                                                         (INT4) *args[0],
                                                         *args[1],
                                                         (UINT1 *) args[2],
                                                         args[3]);
            break;

        case CLI_FS11N_SET_DUALBEACONOPTION:
            i4RetStatus = Fs11nCliSetObssNonHtStaOption (CliHandle,
                                                         (INT4) *args[0],
                                                         *args[1],
                                                         (UINT1 *) args[2],
                                                         args[3]);
            break;

        case CLI_FS11N_SET_DUALCTSPROTECTIONOPTION:
            i4RetStatus = Fs11nCliSetDualCtsProtectionOption (CliHandle,
                                                              (INT4) *args[0],
                                                              *args[1],
                                                              (UINT1 *) args[2],
                                                              args[3]);
            break;

        case CLI_FS11N_SET_STBCBEACONOPTION:
            i4RetStatus = Fs11nCliSetLStbcBeaconOption (CliHandle,
                                                        (INT4) *args[0],
                                                        *args[1],
                                                        (UINT1 *) args[2],
                                                        args[3]);
            break;

        case CLI_FS11N_SET_PCOOPTION:
            i4RetStatus = Fs11nCliSetPco (CliHandle,
                                          (INT4) *args[0],
                                          *args[1], (UINT1 *) args[2], args[3]);
            break;

        case CLI_FS11N_SET_PCOPHASEOPTION:
            i4RetStatus = Fs11nCliSetPcoPhaseOption (CliHandle,
                                                     (INT4) *args[0],
                                                     *args[1],
                                                     (UINT1 *) args[2],
                                                     args[3]);
            break;

        case CLI_FS11N_SET_MCSSETDEFINED:
            i4RetStatus = Fs11nCliSetMcsSetDefinedOption (CliHandle,
                                                          (INT4) *args[0],
                                                          *args[1],
                                                          (UINT1 *) args[2],
                                                          args[3]);
            break;

        case CLI_FS11N_SET_TXRXMCSSETNOTEQUAL:
            i4RetStatus = Fs11nCliSetTxRxMcsSetNotEqualOption (CliHandle,
                                                               (INT4) *args[0],
                                                               *args[1],
                                                               (UINT1 *)
                                                               args[2],
                                                               args[3]);
            break;

        case CLI_FS11N_SET_AMPDUSTATUS:
            i4RetStatus = Fs11nCliSetAmpduStatus (CliHandle,
                                                  (INT4) *args[0],
                                                  *args[1],
                                                  (UINT1 *) args[2], args[3]);
            break;
        case CLI_FS11N_SET_AMPDUSUBFRAME:
            i4RetStatus = Fs11nCliSetAmpduSubFrame (CliHandle,
                                                    (INT4) *args[0],
                                                    *args[1],
                                                    (UINT1 *) args[2], args[3]);
            break;
        case CLI_FS11N_SET_AMPDULIMIT:
            i4RetStatus = Fs11nCliSetAmpduLimit (CliHandle,
                                                 (INT4) *args[0],
                                                 *args[1],
                                                 (UINT1 *) args[2], args[3]);
            break;
        case CLI_FS11N_SET_AMSDUSTATUS:
            i4RetStatus = Fs11nCliSetAmsduStatus (CliHandle,
                                                  (INT4) *args[0],
                                                  *args[1],
                                                  (UINT1 *) args[2], args[3]);
            break;
        case CLI_FS11N_SET_AMSDULIMIT:
            i4RetStatus = Fs11nCliSetAmsduLimit (CliHandle,
                                                 (INT4) *args[0],
                                                 *args[1],
                                                 (UINT1 *) args[2], args[3]);
            break;
        case CLI_FS11N_CLI_SHOW:
            i4RetStatus = Fs11nCliShow (CliHandle,
                                        (UINT4) *args[0],
                                        (UINT1 *) args[1], args[2]);
            break;

        default:
            break;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_WSSCFG_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", WsscfgCliErrString[u4ErrCode]);
            CLI_FATAL_ERROR (CliHandle);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    CliUnRegisterLock (CliHandle);
    WSSCFG_UNLOCK;
    UNUSED_PARAM (u4IfIndex);
    return i4RetStatus;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetLdpcOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the LDPC option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4LdpcOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetLdpcOption (tCliHandle CliHandle, UINT4 u4RadioType,
                       UINT4 u4LdpcOption, UINT1 *pu1ProfileName,
                       UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4GetRadioType = 0;
    UINT4               u4ErrCode = 0;
    /*If no Profile name is provided AP ldpc option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2Dot11LDPCCodingOptionActivated (&u4ErrCode,
                                                             i4RadioIfIndex,
                                                             u4LdpcOption) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetDot11LDPCCodingOptionActivated (i4RadioIfIndex,
                                                      u4LdpcOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP ldpc option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2Dot11LDPCCodingOptionActivated
                                    (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4LdpcOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetDot11LDPCCodingOptionActivated
                                    (i4RadioIfIndex, (INT4) u4LdpcOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, LDPC Option is updated. */
        else
        {

            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2Dot11LDPCCodingOptionActivated (&u4ErrCode,
                                                             i4RadioIfIndex,
                                                             (INT4)
                                                             u4LdpcOption) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetDot11LDPCCodingOptionActivated (i4RadioIfIndex,
                                                      (INT4) u4LdpcOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetPowerSaverMode
 *  Description :  This function is exported to CLI module to set the
 *                  Power Save Mode.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4PowerSaveOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetPowerSaverMode (tCliHandle CliHandle, UINT4 u4RadioType,
                           UINT4 u4PowerSaveOption, UINT1 *pu1ProfileName,
                           UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4GetRadioType = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;

    /*If no Profile name is provided AP Power save mode is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigMIMOPowerSave (&u4ErrCode,
                                                          i4RadioIfIndex,
                                                          u4PowerSaveOption) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigMIMOPowerSave (i4RadioIfIndex,
                                                   u4PowerSaveOption);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Power save Mode. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigMIMOPowerSave
                                    (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4PowerSaveOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigMIMOPowerSave
                                    (i4RadioIfIndex, (INT4) u4PowerSaveOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Power Save Mode is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigMIMOPowerSave (&u4ErrCode,
                                                          i4RadioIfIndex,
                                                          (INT4)
                                                          u4PowerSaveOption) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigMIMOPowerSave (i4RadioIfIndex,
                                                   (INT4) u4PowerSaveOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetGreenFieldOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables the Green Field  Option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4GreenfieldOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetGreenFieldOption (tCliHandle CliHandle, UINT4 u4RadioType,
                             UINT4 u4GreenfieldOption, UINT1 *pu1ProfileName,
                             UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP GreenField Option is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2Dot11HTGreenfieldOptionActivated (&u4ErrCode,
                                                               i4RadioIfIndex,
                                                               u4GreenfieldOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetDot11HTGreenfieldOptionActivated (i4RadioIfIndex,
                                                        u4GreenfieldOption);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP GreenFIeld Option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2Dot11HTGreenfieldOptionActivated
                                    (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4GreenfieldOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetDot11HTGreenfieldOptionActivated
                                    (i4RadioIfIndex, (INT4) u4GreenfieldOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, GreenFIeld Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2Dot11HTGreenfieldOptionActivated (&u4ErrCode,
                                                               i4RadioIfIndex,
                                                               (INT4)
                                                               u4GreenfieldOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetDot11HTGreenfieldOptionActivated (i4RadioIfIndex,
                                                        (INT4)
                                                        u4GreenfieldOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetShort20GaurdInterval
 *  Description :  This function is exported to CLI module to set the
 *                  Guard Interval Option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4GuardintervalOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetShort20GaurdInterval (tCliHandle CliHandle, UINT4 u4RadioType,
                                 UINT4 u4GuardintervalOption,
                                 UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided Guard Interval Option is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2Dot11ShortGIOptionInTwentyActivated (&u4ErrCode,
                                                                  i4RadioIfIndex,
                                                                  u4GuardintervalOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetDot11ShortGIOptionInTwentyActivated (i4RadioIfIndex,
                                                           u4GuardintervalOption);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP  Guard Interval Option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2Dot11ShortGIOptionInTwentyActivated
                                    (&u4ErrCode,
                                     i4RadioIfIndex,
                                     (INT4) u4GuardintervalOption) ==
                                    SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetDot11ShortGIOptionInTwentyActivated
                                    (i4RadioIfIndex,
                                     (INT4) u4GuardintervalOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Guard Interval Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2Dot11ShortGIOptionInTwentyActivated (&u4ErrCode,
                                                                  i4RadioIfIndex,
                                                                  (INT4)
                                                                  u4GuardintervalOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetDot11ShortGIOptionInTwentyActivated (i4RadioIfIndex,
                                                           (INT4)
                                                           u4GuardintervalOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetShortGaurdInterval
 *  Description :  This function is exported to CLI module to set the
 *                  Guard Interval Option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4GuardintervalOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetShortGaurdInterval (tCliHandle CliHandle, UINT4 u4RadioType,
                               UINT4 u4GuardintervalOption,
                               UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided Guard Interval Option is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2Dot11ShortGIOptionInFortyActivated (&u4ErrCode,
                                                                 i4RadioIfIndex,
                                                                 u4GuardintervalOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetDot11ShortGIOptionInFortyActivated (i4RadioIfIndex,
                                                          u4GuardintervalOption);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP  Guard Interval Option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2Dot11ShortGIOptionInFortyActivated
                                    (&u4ErrCode,
                                     i4RadioIfIndex,
                                     (INT4) u4GuardintervalOption) ==
                                    SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetDot11ShortGIOptionInFortyActivated
                                    (i4RadioIfIndex,
                                     (INT4) u4GuardintervalOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Guard Interval Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2Dot11ShortGIOptionInFortyActivated (&u4ErrCode,
                                                                 i4RadioIfIndex,
                                                                 (INT4)
                                                                 u4GuardintervalOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetDot11ShortGIOptionInFortyActivated (i4RadioIfIndex,
                                                          (INT4)
                                                          u4GuardintervalOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetTxStbcOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the Tx STBC option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4TxStbcOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetTxStbcOption (tCliHandle CliHandle, UINT4 u4RadioType,
                         UINT4 u4TxStbcOption, UINT1 *pu1ProfileName,
                         UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Tx STBC option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2Dot11TxSTBCOptionActivated (&u4ErrCode,
                                                         i4RadioIfIndex,
                                                         u4TxStbcOption) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetDot11TxSTBCOptionActivated (i4RadioIfIndex,
                                                  u4TxStbcOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Tx STBC option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2Dot11TxSTBCOptionActivated
                                    (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4TxStbcOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetDot11TxSTBCOptionActivated
                                    (i4RadioIfIndex, (INT4) u4TxStbcOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Tx STBC Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2Dot11TxSTBCOptionActivated (&u4ErrCode,
                                                         i4RadioIfIndex,
                                                         (INT4) u4TxStbcOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetDot11TxSTBCOptionActivated (i4RadioIfIndex,
                                                  (INT4) u4TxStbcOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetRxStbcOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the Rx STBC option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4RxSTBCOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetRxStbcOption (tCliHandle CliHandle, UINT4 u4RadioType,
                         UINT4 u4RxSTBCOption, UINT1 *pu1ProfileName,
                         UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Rx STBC option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2Dot11RxSTBCOptionActivated (&u4ErrCode,
                                                         i4RadioIfIndex,
                                                         u4RxSTBCOption) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetDot11RxSTBCOptionActivated (i4RadioIfIndex,
                                                  u4RxSTBCOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Rx STBC option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2Dot11RxSTBCOptionActivated
                                    (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4RxSTBCOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetDot11RxSTBCOptionActivated
                                    (i4RadioIfIndex, (INT4) u4RxSTBCOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Rx STBC Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2Dot11RxSTBCOptionActivated (&u4ErrCode,
                                                         i4RadioIfIndex,
                                                         (INT4) u4RxSTBCOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetDot11RxSTBCOptionActivated (i4RadioIfIndex,
                                                  (INT4) u4RxSTBCOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetDelayBlockAckOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the HT Delayed Block Ack option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4DelBlockAckOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetDelayBlockAckOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                UINT4 u4DelBlockAckOption,
                                UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP HT Delayed Block Ack option is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigDelayedBlockAckOptionActivated
                    (&u4ErrCode, i4RadioIfIndex,
                     u4DelBlockAckOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigDelayedBlockAckOptionActivated
                    (i4RadioIfIndex, u4DelBlockAckOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP HT Delayed Block Ack option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigDelayedBlockAckOptionActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4DelBlockAckOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigDelayedBlockAckOptionActivated
                                    (i4RadioIfIndex,
                                     (INT4) u4DelBlockAckOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, HT Delayed Block Ack Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigDelayedBlockAckOptionActivated
                    (&u4ErrCode, i4RadioIfIndex,
                     (INT4) u4DelBlockAckOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigDelayedBlockAckOptionActivated
                    (i4RadioIfIndex, (INT4) u4DelBlockAckOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetMaxAmsduLength
 *  Description :  This function is exported to CLI module to set the
 *                  Max AMSDU Length.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4MaxAmsduLenOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetMaxAmsduLength (tCliHandle CliHandle, UINT4 u4RadioType,
                           UINT4 u4MaxAmsduLenOption, UINT1 *pu1ProfileName,
                           UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Max AMSDU Length is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigMaxAMSDULengthConfig (&u4ErrCode,
                                                                 i4RadioIfIndex,
                                                                 u4MaxAmsduLenOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigMaxAMSDULengthConfig (i4RadioIfIndex,
                                                          u4MaxAmsduLenOption);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Max AMSDU Length. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigMaxAMSDULengthConfig
                                    (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4MaxAmsduLenOption) ==
                                    SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigMaxAMSDULengthConfig
                                    (i4RadioIfIndex,
                                     (INT4) u4MaxAmsduLenOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Max AMSDU Length is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigMaxAMSDULengthConfig (&u4ErrCode,
                                                                 i4RadioIfIndex,
                                                                 (INT4)
                                                                 u4MaxAmsduLenOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigMaxAMSDULengthConfig (i4RadioIfIndex,
                                                          (INT4)
                                                          u4MaxAmsduLenOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetDsscckModeOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the LDPC option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4DsscckModeOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetDsscckModeOption (tCliHandle CliHandle, UINT4 u4RadioType,
                             UINT4 u4DsscckModeOption, UINT1 *pu1ProfileName,
                             UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP DSS/CCK Mode option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigtHTDSSCCKModein40MHzConfig
                    (&u4ErrCode, i4RadioIfIndex,
                     u4DsscckModeOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigtHTDSSCCKModein40MHzConfig (i4RadioIfIndex,
                                                                u4DsscckModeOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP DSS/CCL Mode option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigtHTDSSCCKModein40MHzConfig (&u4ErrCode, i4RadioIfIndex, (INT4) u4DsscckModeOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigtHTDSSCCKModein40MHzConfig
                                    (i4RadioIfIndex, (INT4) u4DsscckModeOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, DSS/CCk Mode Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigtHTDSSCCKModein40MHzConfig
                    (&u4ErrCode, i4RadioIfIndex,
                     (INT4) u4DsscckModeOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigtHTDSSCCKModein40MHzConfig (i4RadioIfIndex,
                                                                (INT4)
                                                                u4DsscckModeOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetFortyMhzIntolarantOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the Forty Mhz Intolarant option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4FortyMhzIntolarantOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetFortyMhzIntolarantOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                     UINT4 u4FortyMhzIntolarantOption,
                                     UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Forty Mhz Intolarant option is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2Dot11FortyMHzIntolerant (&u4ErrCode,
                                                      i4RadioIfIndex,
                                                      u4FortyMhzIntolarantOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetDot11FortyMHzIntolerant (i4RadioIfIndex,
                                               u4FortyMhzIntolarantOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Forty Mhz Intolarant option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2Dot11FortyMHzIntolerant
                                    (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4FortyMhzIntolarantOption) ==
                                    SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetDot11FortyMHzIntolerant (i4RadioIfIndex,
                                                               (INT4)
                                                               u4FortyMhzIntolarantOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Forty Mhz Intolarant Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2Dot11FortyMHzIntolerant (&u4ErrCode,
                                                      i4RadioIfIndex,
                                                      (INT4)
                                                      u4FortyMhzIntolarantOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetDot11FortyMHzIntolerant (i4RadioIfIndex,
                                               (INT4)
                                               u4FortyMhzIntolarantOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetLSigTxOpProtectionOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the L-SIG TXOP Protection Support option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4LSigTxOpProtectionOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetLSigTxOpProtectionOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                     UINT4 u4LSigTxOpProtectionOption,
                                     UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP L-SIG TXOP Protection Support  option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2Dot11LSIGTXOPFullProtectionActivated (&u4ErrCode,
                                                                   i4RadioIfIndex,
                                                                   u4LSigTxOpProtectionOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if (nmhSetDot11LSIGTXOPFullProtectionActivated (i4RadioIfIndex,
                                                                u4LSigTxOpProtectionOption)
                    != SNMP_SUCCESS)
                {
                }
            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP L-SIG TXOP Protectiob Support option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2Dot11LSIGTXOPFullProtectionActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4LSigTxOpProtectionOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                if (nmhSetDot11LSIGTXOPFullProtectionActivated
                                    (i4RadioIfIndex,
                                     (INT4) u4LSigTxOpProtectionOption) !=
                                    SNMP_SUCCESS)
                                {
                                }
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, L-SIG TXOP Protection Support Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2Dot11LSIGTXOPFullProtectionActivated (&u4ErrCode,
                                                                   i4RadioIfIndex,
                                                                   (INT4)
                                                                   u4LSigTxOpProtectionOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                if (nmhSetDot11LSIGTXOPFullProtectionActivated (i4RadioIfIndex,
                                                                (INT4)
                                                                u4LSigTxOpProtectionOption)
                    != SNMP_SUCCESS)
                {
                }
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetMaxMpduOption
 *  Description :  This function is exported to CLI module to set the
 *                  Max MPDU Length.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4MaxMpduOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetMaxMpduOption (tCliHandle CliHandle, UINT4 u4RadioType,
                          UINT4 u4MaxMpduOption, UINT1 *pu1ProfileName,
                          UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Max MPDU Length is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigMaxRxAMPDUFactorConfig (&u4ErrCode,
                                                                   i4RadioIfIndex,
                                                                   u4MaxMpduOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigMaxRxAMPDUFactorConfig (i4RadioIfIndex,
                                                            u4MaxMpduOption);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Max MPDU Length Mode. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigMaxRxAMPDUFactorConfig (&u4ErrCode, i4RadioIfIndex, (INT4) u4MaxMpduOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigMaxRxAMPDUFactorConfig
                                    (i4RadioIfIndex, (INT4) u4MaxMpduOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Max MPDU Length is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigMaxRxAMPDUFactorConfig (&u4ErrCode,
                                                                   i4RadioIfIndex,
                                                                   (INT4)
                                                                   u4MaxMpduOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigMaxRxAMPDUFactorConfig (i4RadioIfIndex,
                                                            (INT4)
                                                            u4MaxMpduOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetAmpduFactorOption
 *  Description :  This function is exported to CLI module to set the
 *                   AMPDU Factor.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4AmpduFactorOtion
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetAmpduFactorOption (tCliHandle CliHandle, UINT4 u4RadioType,
                              UINT4 u4AmpduFactorOption, UINT1 *pu1ProfileName,
                              UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP AMPDU Factor is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigMinimumMPDUStartSpacingConfig
                    (&u4ErrCode, i4RadioIfIndex,
                     u4AmpduFactorOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigMinimumMPDUStartSpacingConfig
                    (i4RadioIfIndex, u4AmpduFactorOption);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP AMPDU Factor. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigMinimumMPDUStartSpacingConfig (&u4ErrCode, i4RadioIfIndex, (INT4) u4AmpduFactorOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigMinimumMPDUStartSpacingConfig
                                    (i4RadioIfIndex,
                                     (INT4) u4AmpduFactorOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, AMPDU Factor is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigMinimumMPDUStartSpacingConfig
                    (&u4ErrCode, i4RadioIfIndex,
                     (INT4) u4AmpduFactorOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigMinimumMPDUStartSpacingConfig
                    (i4RadioIfIndex, (INT4) u4AmpduFactorOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetPcoOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the PCO option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4PcoOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetPcoOption (tCliHandle CliHandle, UINT4 u4RadioType,
                      UINT4 u4PcoOption, UINT1 *pu1ProfileName,
                      UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP PCO option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigPCOOptionActivated (&u4ErrCode,
                                                               i4RadioIfIndex,
                                                               u4PcoOption) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if (nmhSetFsDot11nConfigPCOOptionActivated (i4RadioIfIndex,
                                                            u4PcoOption) !=
                    SNMP_SUCCESS)
                {
                }
            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP PCO option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigPCOOptionActivated
                                    (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4PcoOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                if (nmhSetFsDot11nConfigPCOOptionActivated
                                    (i4RadioIfIndex,
                                     (INT4) u4PcoOption) != SNMP_SUCCESS)
                                {
                                }
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, PCO Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigPCOOptionActivated (&u4ErrCode,
                                                               i4RadioIfIndex,
                                                               (INT4)
                                                               u4PcoOption) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                if (nmhSetFsDot11nConfigPCOOptionActivated (i4RadioIfIndex,
                                                            (INT4) u4PcoOption)
                    != SNMP_SUCCESS)
                {
                }
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetPcoTransitionTimeOption
 *  Description :  This function is exported to CLI module to Stes  the PCO 
 *                  Transition Time option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4PcoTranTimeOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetPcoTransitionTimeOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                    UINT4 u4PcoTranTimeOption,
                                    UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP PCO Transition Time option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigTransitionTimeConfig (&u4ErrCode,
                                                                 i4RadioIfIndex,
                                                                 u4PcoTranTimeOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigTransitionTimeConfig (i4RadioIfIndex,
                                                          u4PcoTranTimeOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP PCO Transition Time option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigTransitionTimeConfig
                                    (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4PcoTranTimeOption) ==
                                    SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigTransitionTimeConfig
                                    (i4RadioIfIndex,
                                     (INT4) u4PcoTranTimeOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, PCO Transition Time Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigTransitionTimeConfig (&u4ErrCode,
                                                                 i4RadioIfIndex,
                                                                 (INT4)
                                                                 u4PcoTranTimeOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigTransitionTimeConfig (i4RadioIfIndex,
                                                          (INT4)
                                                          u4PcoTranTimeOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetMcsFeedbackOption
 *  Description :  This function is exported to CLI module to set the
 *                  MCS Feedback Option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4MCSFeedbackOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetMcsFeedbackOption (tCliHandle CliHandle, UINT4 u4RadioType,
                              UINT4 u4MCSFeedbackOption, UINT1 *pu1ProfileName,
                              UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP MCS Feeedabck Option is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigMCSFeedbackOptionActivated
                    (&u4ErrCode, i4RadioIfIndex,
                     u4MCSFeedbackOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigMCSFeedbackOptionActivated (i4RadioIfIndex,
                                                                u4MCSFeedbackOption);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP MCS Feedback Option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigMCSFeedbackOptionActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4MCSFeedbackOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigMCSFeedbackOptionActivated
                                    (i4RadioIfIndex,
                                     (INT4) u4MCSFeedbackOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, MCS Feedback OPtion is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigMCSFeedbackOptionActivated
                    (&u4ErrCode, i4RadioIfIndex,
                     (INT4) u4MCSFeedbackOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigMCSFeedbackOptionActivated (i4RadioIfIndex,
                                                                (INT4)
                                                                u4MCSFeedbackOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetHtControlFieldOption
 *  Description :  This function is exported to CLI module to set the
 *                  HTC Field Option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4HTCFieldOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetHtControlFieldOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                 UINT4 u4HTCFieldOption, UINT1 *pu1ProfileName,
                                 UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP HTC Field Option is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigHTControlFieldSupported (&u4ErrCode,
                                                                    i4RadioIfIndex,
                                                                    u4HTCFieldOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigHTControlFieldSupported (i4RadioIfIndex,
                                                             u4HTCFieldOption);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP HTC Field Option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigHTControlFieldSupported (&u4ErrCode, i4RadioIfIndex, (INT4) u4HTCFieldOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigHTControlFieldSupported
                                    (i4RadioIfIndex, (INT4) u4HTCFieldOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, HTC Field Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigHTControlFieldSupported (&u4ErrCode,
                                                                    i4RadioIfIndex,
                                                                    (INT4)
                                                                    u4HTCFieldOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigHTControlFieldSupported (i4RadioIfIndex,
                                                             (INT4)
                                                             u4HTCFieldOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetRevDResponderOption
 *  Description :  This function is exported to CLI module to set the
 *                  Reverse Direction Responder Option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4RDRespondOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetRevDResponderOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                UINT4 u4RDRespondOption, UINT1 *pu1ProfileName,
                                UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Reverse Direction Responder is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigRDResponderOptionActivated
                    (&u4ErrCode, i4RadioIfIndex,
                     u4RDRespondOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigRDResponderOptionActivated (i4RadioIfIndex,
                                                                u4RDRespondOption);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Reverse Direction Responder Option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigRDResponderOptionActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4RDRespondOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigRDResponderOptionActivated
                                    (i4RadioIfIndex, (INT4) u4RDRespondOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Reverse Direction Responder
         * Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigRDResponderOptionActivated
                    (&u4ErrCode, i4RadioIfIndex,
                     (INT4) u4RDRespondOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigRDResponderOptionActivated (i4RadioIfIndex,
                                                                (INT4)
                                                                u4RDRespondOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetLImpTranBeamRecvOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the Implicit Transmit Beamforming Receiving option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4ImpTranBeamRecvOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetLImpTranBeamRecvOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                   UINT4 u4ImpTranBeamRecvOption,
                                   UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Implicit Transmit Beamforming Receiving option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated (&u4ErrCode, i4RadioIfIndex, u4ImpTranBeamRecvOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated
                    (i4RadioIfIndex, u4ImpTranBeamRecvOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Implicit Transmit Beamforming Receiving option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4ImpTranBeamRecvOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated
                                    (i4RadioIfIndex,
                                     (INT4) u4ImpTranBeamRecvOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Implicit Transmit Beamforming
         * ReceivingOption is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4ImpTranBeamRecvOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated
                    (i4RadioIfIndex, (INT4) u4ImpTranBeamRecvOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetRecvStagSoundOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the Receiving Staggered Sounding option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4RecvStagSoundOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetRecvStagSoundOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                UINT4 u4RecvStagSoundOption,
                                UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Receiving Staggered Sounding option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigReceiveStaggerSoundingOptionActivated
                    (&u4ErrCode, i4RadioIfIndex,
                     u4RecvStagSoundOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigReceiveStaggerSoundingOptionActivated
                    (i4RadioIfIndex, u4RecvStagSoundOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Receiving Staggered Sounding option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigReceiveStaggerSoundingOptionActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4RecvStagSoundOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigReceiveStaggerSoundingOptionActivated
                                    (i4RadioIfIndex,
                                     (INT4) u4RecvStagSoundOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Receiving Staggered Sounding Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigReceiveStaggerSoundingOptionActivated
                    (&u4ErrCode, i4RadioIfIndex,
                     (INT4) u4RecvStagSoundOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigReceiveStaggerSoundingOptionActivated
                    (i4RadioIfIndex, (INT4) u4RecvStagSoundOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetTranStagSoundOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the Transmit Staggered Sounding option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4TranStagSoundOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetTranStagSoundOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                UINT4 u4TranStagSoundOption,
                                UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Transmit Staggered Sounding option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigTransmitStaggerSoundingOptionActivated (&u4ErrCode, i4RadioIfIndex, u4TranStagSoundOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigTransmitStaggerSoundingOptionActivated
                    (i4RadioIfIndex, u4TranStagSoundOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Transmit Staggered Sounding option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigTransmitStaggerSoundingOptionActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4TranStagSoundOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigTransmitStaggerSoundingOptionActivated
                                    (i4RadioIfIndex,
                                     (INT4) u4TranStagSoundOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Transmit Staggered Sounding Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigTransmitStaggerSoundingOptionActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4TranStagSoundOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigTransmitStaggerSoundingOptionActivated
                    (i4RadioIfIndex, (INT4) u4TranStagSoundOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetRecvNullDataFramingOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the Receive NDP option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4RecvNDPOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetRecvNullDataFramingOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                      UINT4 u4RecvNDPOption,
                                      UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Receive NDP option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigReceiveNDPOptionActivated
                    (&u4ErrCode, i4RadioIfIndex,
                     u4RecvNDPOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigReceiveNDPOptionActivated (i4RadioIfIndex,
                                                               u4RecvNDPOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Receive NDP option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigReceiveNDPOptionActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4RecvNDPOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigReceiveNDPOptionActivated
                                    (i4RadioIfIndex, (INT4) u4RecvNDPOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Receive NDP Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigReceiveNDPOptionActivated
                    (&u4ErrCode, i4RadioIfIndex,
                     (INT4) u4RecvNDPOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigReceiveNDPOptionActivated (i4RadioIfIndex,
                                                               (INT4)
                                                               u4RecvNDPOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetTranNullDataFramingOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the Transmit NDP option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4TranNDPOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetTranNullDataFramingOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                      UINT4 u4TranNDPOption,
                                      UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Transmit NDP option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigTransmitNDPOptionActivated
                    (&u4ErrCode, i4RadioIfIndex,
                     u4TranNDPOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigTransmitNDPOptionActivated (i4RadioIfIndex,
                                                                u4TranNDPOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Transmit NDP option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigTransmitNDPOptionActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4TranNDPOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigTransmitNDPOptionActivated
                                    (i4RadioIfIndex, (INT4) u4TranNDPOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Transmit NDP Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigTransmitNDPOptionActivated
                    (&u4ErrCode, i4RadioIfIndex,
                     (INT4) u4TranNDPOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigTransmitNDPOptionActivated (i4RadioIfIndex,
                                                                (INT4)
                                                                u4TranNDPOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetImpTranBeamOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the Implicit Transmit BeamingForming option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4ImpTranBeamOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetImpTranBeamOption (tCliHandle CliHandle, UINT4 u4RadioType,
                              UINT4 u4ImpTranBeamOption, UINT1 *pu1ProfileName,
                              UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Implicit Transmit BeamForming option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigImplicitTransmitBeamformingOptionActivated (&u4ErrCode, i4RadioIfIndex, u4ImpTranBeamOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigImplicitTransmitBeamformingOptionActivated
                    (i4RadioIfIndex, u4ImpTranBeamOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Implicit Transmit BeamForming option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigImplicitTransmitBeamformingOptionActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4ImpTranBeamOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigImplicitTransmitBeamformingOptionActivated
                                    (i4RadioIfIndex,
                                     (INT4) u4ImpTranBeamOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Implicit Transmit BeamForming Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigImplicitTransmitBeamformingOptionActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4ImpTranBeamOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigImplicitTransmitBeamformingOptionActivated
                    (i4RadioIfIndex, (INT4) u4ImpTranBeamOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetCalibrationOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the Calibration option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4CalibrationOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetCalibrationOption (tCliHandle CliHandle, UINT4 u4RadioType,
                              UINT4 u4CalibrationOption, UINT1 *pu1ProfileName,
                              UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Calibration option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigCalibrationOptionActivated
                    (&u4ErrCode, i4RadioIfIndex,
                     u4CalibrationOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigCalibrationOptionActivated (i4RadioIfIndex,
                                                                u4CalibrationOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Calibration option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigCalibrationOptionActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4CalibrationOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigCalibrationOptionActivated
                                    (i4RadioIfIndex,
                                     (INT4) u4CalibrationOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Calibration Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigCalibrationOptionActivated
                    (&u4ErrCode, i4RadioIfIndex,
                     (INT4) u4CalibrationOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigCalibrationOptionActivated (i4RadioIfIndex,
                                                                (INT4)
                                                                u4CalibrationOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetExplCsiTranBeamOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the Explicit CSI Transmit Beamforming  option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4ExplCsiTranBeamOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetExplCsiTranBeamOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                  UINT4 u4ExplCsiTranBeamOption,
                                  UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Explicit CSI Transmit Beamforming  option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigExplicitCSITransmitBeamformingOptionActivated (&u4ErrCode, i4RadioIfIndex, u4ExplCsiTranBeamOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated
                    (i4RadioIfIndex, u4ExplCsiTranBeamOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Explicit CSI Transmit Beamforming option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigExplicitCSITransmitBeamformingOptionActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4ExplCsiTranBeamOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated
                                    (i4RadioIfIndex,
                                     (INT4) u4ExplCsiTranBeamOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Explicit CSI Transmit
         * Beamforming  Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigExplicitCSITransmitBeamformingOptionActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4ExplCsiTranBeamOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated
                    (i4RadioIfIndex, (INT4) u4ExplCsiTranBeamOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetExplNonCompSterOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the Explicit Noncompressed Steering option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4ExplNonCompSterOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetExplNonCompSterOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                  UINT4 u4ExplNonCompSterOption,
                                  UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Explicit Noncompressed Steering option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated (&u4ErrCode, i4RadioIfIndex, u4ExplNonCompSterOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated
                    (i4RadioIfIndex, u4ExplNonCompSterOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Explicit Noncompressed Steering option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4ExplNonCompSterOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated
                                    (i4RadioIfIndex,
                                     (INT4) u4ExplNonCompSterOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Explicit Noncompressed
         * Steering Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4ExplNonCompSterOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated
                    (i4RadioIfIndex, (INT4) u4ExplNonCompSterOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetExplCompSterOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the Explicit Noncompressed Steering option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4ExplCompSterOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetExplCompSterOption (tCliHandle CliHandle, UINT4 u4RadioType,
                               UINT4 u4ExplCompSterOption,
                               UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Explicit compressed Steering option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated (&u4ErrCode, i4RadioIfIndex, u4ExplCompSterOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated
                    (i4RadioIfIndex, u4ExplCompSterOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Explicit compressed Steering option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4ExplCompSterOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated
                                    (i4RadioIfIndex,
                                     (INT4) u4ExplCompSterOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Explicit compressed
         * Steering Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4ExplCompSterOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated
                    (i4RadioIfIndex, (INT4) u4ExplCompSterOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetExplTranBeamCsiOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the Explicit Transmit BeamForming CSI option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4ExplTranBeamCsiOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetExplTranBeamCsiOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                  UINT4 u4ExplTranBeamCsiOption,
                                  UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Explicit Transmit BeamForming CSI option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated (&u4ErrCode, i4RadioIfIndex, u4ExplTranBeamCsiOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated
                    (i4RadioIfIndex, u4ExplTranBeamCsiOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Explicit Transmit BeamForming CSI option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4ExplTranBeamCsiOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated
                                    (i4RadioIfIndex,
                                     (INT4) u4ExplTranBeamCsiOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Explicit Transmit
         * BeamForming CSI Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4ExplTranBeamCsiOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated
                    (i4RadioIfIndex, (INT4) u4ExplTranBeamCsiOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetExplNonCompBeamOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the Explicit Noncompressed BeamForming option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4ExplNonCompBeamOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetExplNonCompBeamOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                  UINT4 u4ExplNonCompBeamOption,
                                  UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Explicit Noncompressed BeamForming option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated (&u4ErrCode, i4RadioIfIndex, u4ExplNonCompBeamOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated
                    (i4RadioIfIndex, u4ExplNonCompBeamOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Explicit Noncompressed BeamForming option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4ExplNonCompBeamOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated
                                    (i4RadioIfIndex,
                                     (INT4) u4ExplNonCompBeamOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Explicit Noncompressed
         * BeamForming Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4ExplNonCompBeamOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated
                    (i4RadioIfIndex, (INT4) u4ExplNonCompBeamOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetExplCompBeamOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the Explicit Noncompressed BeamForming option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4ExplCompBeamOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetExplCompBeamOption (tCliHandle CliHandle, UINT4 u4RadioType,
                               UINT4 u4ExplCompBeamOption,
                               UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Explicit compressed BeamForming option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated (&u4ErrCode, i4RadioIfIndex, u4ExplCompBeamOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated
                    (i4RadioIfIndex, u4ExplCompBeamOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Explicit compressed BeamForming option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4ExplCompBeamOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated
                                    (i4RadioIfIndex,
                                     (INT4) u4ExplCompBeamOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Explicit compressed
         * BeamForming Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4ExplCompBeamOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated
                    (i4RadioIfIndex, (INT4) u4ExplCompBeamOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetMinGroupingOption
 *  Description :  This function is exported to CLI module to set the
 *                  Minimal Grouping Option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4MinGroupingOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetMinGroupingOption (tCliHandle CliHandle, UINT4 u4RadioType,
                              UINT4 u4MinGroupingOption, UINT1 *pu1ProfileName,
                              UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Minimal Grouping Option is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigMinimalGroupingActivated (&u4ErrCode,
                                                                     i4RadioIfIndex,
                                                                     u4MinGroupingOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigMinimalGroupingActivated (i4RadioIfIndex,
                                                              u4MinGroupingOption);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Minimal Grouping Option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigMinimalGroupingActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4MinGroupingOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigMinimalGroupingActivated
                                    (i4RadioIfIndex,
                                     (INT4) u4MinGroupingOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Minimal Grouping Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigMinimalGroupingActivated (&u4ErrCode,
                                                                     i4RadioIfIndex,
                                                                     (INT4)
                                                                     u4MinGroupingOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigMinimalGroupingActivated (i4RadioIfIndex,
                                                              (INT4)
                                                              u4MinGroupingOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetNumCsiAnteSupOption
 *  Description :  This function is exported to CLI module to set the
 *                  CSI Number of Beamformer Antennas Supported.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4NumCsiAnteSupOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetNumCsiAnteSupOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                UINT4 u4NumCsiAnteSupOption,
                                UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP CSI Number of Beamformer Antennas
     * Supported is updated for all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigNumberBeamFormingCSISupportAntenna
                    (&u4ErrCode, i4RadioIfIndex,
                     u4NumCsiAnteSupOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigNumberBeamFormingCSISupportAntenna
                    (i4RadioIfIndex, u4NumCsiAnteSupOption);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP CSI Number of Beamformer Antennas
         *      Supported. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigNumberBeamFormingCSISupportAntenna (&u4ErrCode, i4RadioIfIndex, (INT4) u4NumCsiAnteSupOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigNumberBeamFormingCSISupportAntenna
                                    (i4RadioIfIndex,
                                     (INT4) u4NumCsiAnteSupOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, CSI Number of Beamformer
         * Antennas Supported is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigNumberBeamFormingCSISupportAntenna
                    (&u4ErrCode, i4RadioIfIndex,
                     (INT4) u4NumCsiAnteSupOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigNumberBeamFormingCSISupportAntenna
                    (i4RadioIfIndex, (INT4) u4NumCsiAnteSupOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetNumNonCompAnteSupOption
 *  Description :  This function is exported to CLI module to set the
 *                  Noncompressed Steering Number of Beamformer Antennas
 *                  Supported.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4NumNonCompAnteSupOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetNumNonCompAnteSupOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                    UINT4 u4NumNonCompAnteSupOption,
                                    UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Noncompressed Steering Number of
     * Beamformer Antennas Supported Option is updated for all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna (&u4ErrCode, i4RadioIfIndex, u4NumNonCompAnteSupOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna
                    (i4RadioIfIndex, u4NumNonCompAnteSupOption);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the Noncompressed Steering Number of
         *      Beamformer Antennas Supported Option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna (&u4ErrCode, i4RadioIfIndex, (INT4) u4NumNonCompAnteSupOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna
                                    (i4RadioIfIndex,
                                     (INT4) u4NumNonCompAnteSupOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Noncompressed Steering Number
         * of Beamformer Antennas Supported Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna (&u4ErrCode, i4RadioIfIndex, (INT4) u4NumNonCompAnteSupOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna
                    (i4RadioIfIndex, (INT4) u4NumNonCompAnteSupOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetNumCompAnteSupOption
 *  Description :  This function is exported to CLI module to set the
 *                  compressed Steering Number of Beamformer Antennas
 *                  Supported.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4NumCompAnteSupOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetNumCompAnteSupOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                 UINT4 u4NumCompAnteSupOption,
                                 UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP compressed Steering Number of
     * Beamformer Antennas Supported Option is updated for all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna (&u4ErrCode, i4RadioIfIndex, u4NumCompAnteSupOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna
                    (i4RadioIfIndex, u4NumCompAnteSupOption);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the compressed Steering Number of
         *      Beamformer Antennas Supported Option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna (&u4ErrCode, i4RadioIfIndex, (INT4) u4NumCompAnteSupOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna
                                    (i4RadioIfIndex,
                                     (INT4) u4NumCompAnteSupOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, compressed Steering Number
         * of Beamformer Antennas Supported Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna (&u4ErrCode, i4RadioIfIndex, (INT4) u4NumCompAnteSupOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna
                    (i4RadioIfIndex, (INT4) u4NumCompAnteSupOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetMaxNumCsiAnteSupOption
 *  Description :  This function is exported to CLI module to set the
 *                  CSI Maximum Number of Beamformer Antennas Supported.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4MaxNumCsiAnteSupOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetMaxNumCsiAnteSupOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                   UINT4 u4MaxNumCsiAnteSupOption,
                                   UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP CSI Maximum Number of Beamformer Antennas
     * Supported is updated for all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated (&u4ErrCode, i4RadioIfIndex, u4MaxNumCsiAnteSupOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated
                    (i4RadioIfIndex, u4MaxNumCsiAnteSupOption);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP CSI Maximum Number of Beamformer Antennas
         *      Supported. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4MaxNumCsiAnteSupOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated
                                    (i4RadioIfIndex,
                                     (INT4) u4MaxNumCsiAnteSupOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, CSI Maximum Number of Beamformer
         * Antennas Supported is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4MaxNumCsiAnteSupOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated
                    (i4RadioIfIndex, (INT4) u4MaxNumCsiAnteSupOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetChannelEstValOption
 *  Description :  This function is exported to CLI module to set the
 *                  Channel Estimation Value.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4ChannelEstValOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetChannelEstValOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                UINT4 u4ChannelEstValOption,
                                UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Channel Estimation Value is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigChannelEstimationCapabilityActivated
                    (&u4ErrCode, i4RadioIfIndex,
                     u4ChannelEstValOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigChannelEstimationCapabilityActivated
                    (i4RadioIfIndex, u4ChannelEstValOption);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Channel Estimation value. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigChannelEstimationCapabilityActivated (&u4ErrCode, i4RadioIfIndex, (INT4) u4ChannelEstValOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigChannelEstimationCapabilityActivated
                                    (i4RadioIfIndex,
                                     (INT4) u4ChannelEstValOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Channel Estimation Value is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigChannelEstimationCapabilityActivated
                    (&u4ErrCode, i4RadioIfIndex,
                     (INT4) u4ChannelEstValOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigChannelEstimationCapabilityActivated
                    (i4RadioIfIndex, (INT4) u4ChannelEstValOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetPriChannelOption
 *  Description :  This function is exported to CLI module to set the
 *                  Primary Channel Option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4PriChannelOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetPriChannelOption (tCliHandle CliHandle, UINT4 u4RadioType,
                             UINT4 u4PriChannelOption, UINT1 *pu1ProfileName,
                             UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Primary Channel Option updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigCurrentPrimaryChannel (&u4ErrCode,
                                                                  i4RadioIfIndex,
                                                                  u4PriChannelOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigCurrentPrimaryChannel (i4RadioIfIndex,
                                                           u4PriChannelOption);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Primary Channel Option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigCurrentPrimaryChannel
                                    (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4PriChannelOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigCurrentPrimaryChannel
                                    (i4RadioIfIndex, (INT4) u4PriChannelOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Primary Channel Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigCurrentPrimaryChannel (&u4ErrCode,
                                                                  i4RadioIfIndex,
                                                                  (INT4)
                                                                  u4PriChannelOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigCurrentPrimaryChannel (i4RadioIfIndex,
                                                           (INT4)
                                                           u4PriChannelOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetSecChannelOption
 *  Description :  This function is exported to CLI module to set the
 *                  Secondary Channel Option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4PriChannelOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetSecChannelOption (tCliHandle CliHandle, UINT4 u4RadioType,
                             UINT4 u4PriChannelOption, UINT1 *pu1ProfileName,
                             UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Secondary Channel Option updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigCurrentSecondaryChannel (&u4ErrCode,
                                                                    i4RadioIfIndex,
                                                                    u4PriChannelOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigCurrentSecondaryChannel (i4RadioIfIndex,
                                                             u4PriChannelOption);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Secondary Channel Option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigCurrentSecondaryChannel (&u4ErrCode, i4RadioIfIndex, (INT4) u4PriChannelOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigCurrentSecondaryChannel
                                    (i4RadioIfIndex, (INT4) u4PriChannelOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, SECONDARY Channel Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigCurrentSecondaryChannel (&u4ErrCode,
                                                                    i4RadioIfIndex,
                                                                    (INT4)
                                                                    u4PriChannelOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigCurrentSecondaryChannel (i4RadioIfIndex,
                                                             (INT4)
                                                             u4PriChannelOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetHtOpChannelWidthOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the Channel Width.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4HtOpChannelWidthOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetHtOpChannelWidthOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                   UINT4 u4HtOpChannelWidthOption,
                                   UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Channel Width option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigSTAChannelWidthConfig (&u4ErrCode,
                                                                  i4RadioIfIndex,
                                                                  u4HtOpChannelWidthOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if (nmhSetFsDot11nConfigSTAChannelWidthConfig (i4RadioIfIndex,
                                                               u4HtOpChannelWidthOption)
                    != SNMP_SUCCESS)
                {
                }
            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Channel Width option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigSTAChannelWidthConfig
                                    (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4HtOpChannelWidthOption) ==
                                    SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                if (nmhSetFsDot11nConfigSTAChannelWidthConfig
                                    (i4RadioIfIndex,
                                     (INT4) u4HtOpChannelWidthOption) !=
                                    SNMP_SUCCESS)
                                {
                                }
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Channel Width Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigSTAChannelWidthConfig (&u4ErrCode,
                                                                  i4RadioIfIndex,
                                                                  (INT4)
                                                                  u4HtOpChannelWidthOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                if (nmhSetFsDot11nConfigSTAChannelWidthConfig (i4RadioIfIndex,
                                                               (INT4)
                                                               u4HtOpChannelWidthOption)
                    != SNMP_SUCCESS)
                {
                }
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetRifsModeOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the RIFS Mode.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4RifsModeOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetRifsModeOption (tCliHandle CliHandle, UINT4 u4RadioType,
                           UINT4 u4RifsModeOption, UINT1 *pu1ProfileName,
                           UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP RIFS Mode  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2Dot11RIFSMode (&u4ErrCode,
                                            i4RadioIfIndex,
                                            u4RifsModeOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetDot11RIFSMode (i4RadioIfIndex, u4RifsModeOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP RIFS Mode. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2Dot11RIFSMode
                                    (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4RifsModeOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetDot11RIFSMode (i4RadioIfIndex,
                                                     (INT4) u4RifsModeOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, RIFS Mode is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2Dot11RIFSMode (&u4ErrCode,
                                            i4RadioIfIndex,
                                            (INT4) u4RifsModeOption) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetDot11RIFSMode (i4RadioIfIndex, (INT4) u4RifsModeOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetHtProtectionOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the HT Protection option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4HtProtectionOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetHtProtectionOption (tCliHandle CliHandle, UINT4 u4RadioType,
                               UINT4 u4HtProtectionOption,
                               UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Ht Protection option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2Dot11HTProtection (&u4ErrCode,
                                                i4RadioIfIndex,
                                                u4HtProtectionOption) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetDot11HTProtection (i4RadioIfIndex, u4HtProtectionOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Ht Protection option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2Dot11HTProtection
                                    (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4HtProtectionOption) ==
                                    SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetDot11HTProtection (i4RadioIfIndex,
                                                         (INT4)
                                                         u4HtProtectionOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Ht Protection Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2Dot11HTProtection (&u4ErrCode,
                                                i4RadioIfIndex,
                                                (INT4) u4HtProtectionOption) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetDot11HTProtection (i4RadioIfIndex,
                                         (INT4) u4HtProtectionOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetNonGFStaOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the HT Protection option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4HtProtectionOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetNonGFStaOption (tCliHandle CliHandle, UINT4 u4RadioType,
                           UINT4 u4HtProtectionOption, UINT1 *pu1ProfileName,
                           UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Ht Protection option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigNonGreenfieldHTSTAsPresentConfig
                    (&u4ErrCode, i4RadioIfIndex,
                     u4HtProtectionOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigNonGreenfieldHTSTAsPresentConfig
                    (i4RadioIfIndex, u4HtProtectionOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Ht Protection option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigNonGreenfieldHTSTAsPresentConfig (&u4ErrCode, i4RadioIfIndex, (INT4) u4HtProtectionOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigNonGreenfieldHTSTAsPresentConfig
                                    (i4RadioIfIndex,
                                     (INT4) u4HtProtectionOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Ht Protection Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigNonGreenfieldHTSTAsPresentConfig
                    (&u4ErrCode, i4RadioIfIndex,
                     (INT4) u4HtProtectionOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigNonGreenfieldHTSTAsPresentConfig
                    (i4RadioIfIndex, (INT4) u4HtProtectionOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetObssNonHtStaOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the OBSS Non-HT STAs present option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4ObssNonHtStaOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetObssNonHtStaOption (tCliHandle CliHandle, UINT4 u4RadioType,
                               UINT4 u4ObssNonHtStaOption,
                               UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP OBSS Non-HT STAs present option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigOBSSNonHTSTAsPresentConfig
                    (&u4ErrCode, i4RadioIfIndex,
                     u4ObssNonHtStaOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigOBSSNonHTSTAsPresentConfig (i4RadioIfIndex,
                                                                u4ObssNonHtStaOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP OBSS Non-HT STAs present option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigOBSSNonHTSTAsPresentConfig (&u4ErrCode, i4RadioIfIndex, (INT4) u4ObssNonHtStaOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigOBSSNonHTSTAsPresentConfig
                                    (i4RadioIfIndex,
                                     (INT4) u4ObssNonHtStaOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, OBSS Non-HT STAs present Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigOBSSNonHTSTAsPresentConfig
                    (&u4ErrCode, i4RadioIfIndex,
                     (INT4) u4ObssNonHtStaOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigOBSSNonHTSTAsPresentConfig (i4RadioIfIndex,
                                                                (INT4)
                                                                u4ObssNonHtStaOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetDualCtsProtectionOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the Dual CTS Protection option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4DualCtsProtectionOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetDualCtsProtectionOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                    UINT4 u4DualCtsProtectionOption,
                                    UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Dual CTS Protection option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2Dot11DualCTSProtection (&u4ErrCode,
                                                     i4RadioIfIndex,
                                                     u4DualCtsProtectionOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetDot11DualCTSProtection (i4RadioIfIndex,
                                              u4DualCtsProtectionOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Dual CTS Protection option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2Dot11DualCTSProtection
                                    (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4DualCtsProtectionOption) ==
                                    SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetDot11DualCTSProtection (i4RadioIfIndex,
                                                              (INT4)
                                                              u4DualCtsProtectionOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Dual CTS Protection Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2Dot11DualCTSProtection (&u4ErrCode,
                                                     i4RadioIfIndex,
                                                     (INT4)
                                                     u4DualCtsProtectionOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetDot11DualCTSProtection (i4RadioIfIndex,
                                              (INT4) u4DualCtsProtectionOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetLStbcBeaconOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the Stbc Beacon option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4StbcBeaconOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetLStbcBeaconOption (tCliHandle CliHandle, UINT4 u4RadioType,
                              UINT4 u4StbcBeaconOption, UINT1 *pu1ProfileName,
                              UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Stbc Beacon option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigSTBCBeaconConfig (&u4ErrCode,
                                                             i4RadioIfIndex,
                                                             u4StbcBeaconOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigSTBCBeaconConfig (i4RadioIfIndex,
                                                      u4StbcBeaconOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Stbc Beacon option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {

                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigSTBCBeaconConfig
                                    (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4StbcBeaconOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigSTBCBeaconConfig
                                    (i4RadioIfIndex, (INT4) u4StbcBeaconOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Stbc Beacon Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigSTBCBeaconConfig (&u4ErrCode,
                                                             i4RadioIfIndex,
                                                             (INT4)
                                                             u4StbcBeaconOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigSTBCBeaconConfig (i4RadioIfIndex,
                                                      (INT4)
                                                      u4StbcBeaconOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetPco
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the PCO option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4pcoOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetPco (tCliHandle CliHandle, UINT4 u4RadioType,
                UINT4 u4pcoOption, UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP pco option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2Dot11PCOActivated (&u4ErrCode,
                                                i4RadioIfIndex,
                                                u4pcoOption) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetDot11PCOActivated (i4RadioIfIndex, u4pcoOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP pco option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2Dot11PCOActivated
                                    (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4pcoOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetDot11PCOActivated (i4RadioIfIndex,
                                                         (INT4) u4pcoOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, pco Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2Dot11PCOActivated (&u4ErrCode,
                                                i4RadioIfIndex,
                                                (INT4) u4pcoOption) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetDot11PCOActivated (i4RadioIfIndex, (INT4) u4pcoOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetPcoPhaseOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the PCO option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4pcoPhaseOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetPcoPhaseOption (tCliHandle CliHandle, UINT4 u4RadioType,
                           UINT4 u4pcoPhaseOption, UINT1 *pu1ProfileName,
                           UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP pco Phase option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigPCOPhaseConfig (&u4ErrCode,
                                                           i4RadioIfIndex,
                                                           u4pcoPhaseOption) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigPCOPhaseConfig (i4RadioIfIndex,
                                                    u4pcoPhaseOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP pco Phase option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nConfigPCOPhaseConfig
                                    (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4pcoPhaseOption) == SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigPCOPhaseConfig
                                    (i4RadioIfIndex, (INT4) u4pcoPhaseOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, pco Phase Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigPCOPhaseConfig (&u4ErrCode,
                                                           i4RadioIfIndex,
                                                           (INT4)
                                                           u4pcoPhaseOption) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigPCOPhaseConfig (i4RadioIfIndex,
                                                    (INT4) u4pcoPhaseOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetMcsSetDefinedOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the MCS Set Definedoption.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4McsSetDefinedOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetMcsSetDefinedOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                UINT4 u4McsSetDefinedOption,
                                UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Mcs Set Defined Option option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2Dot11TxMCSSetDefined (&u4ErrCode,
                                                   i4RadioIfIndex,
                                                   u4McsSetDefinedOption) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetDot11TxMCSSetDefined (i4RadioIfIndex,
                                            u4McsSetDefinedOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Mcs Set Defined Option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                            {
                                continue;
                            }
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2Dot11TxMCSSetDefined
                                    (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4McsSetDefinedOption) ==
                                    SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetDot11TxMCSSetDefined (i4RadioIfIndex,
                                                            (INT4)
                                                            u4McsSetDefinedOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Mcs Set Defined Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2Dot11TxMCSSetDefined (&u4ErrCode,
                                                   i4RadioIfIndex,
                                                   (INT4) u4McsSetDefinedOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetDot11TxMCSSetDefined (i4RadioIfIndex,
                                            (INT4) u4McsSetDefinedOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetTxRxMcsSetNotEqualOption
 *  Description :  This function is exported to CLI module to Enables or 
 *                  Disables  the PCO option.
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4TxRxMcsSetNotEqualOption
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT1
Fs11nCliSetTxRxMcsSetNotEqualOption (tCliHandle CliHandle, UINT4 u4RadioType,
                                     UINT4 u4TxRxMcsSetNotEqualOption,
                                     UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Tx Rx Mcs Set Not Equal Option  is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nConfigTxRxMCSSetNotEqual (&u4ErrCode,
                                                               i4RadioIfIndex,
                                                               u4TxRxMcsSetNotEqualOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nConfigTxRxMCSSetNotEqual (i4RadioIfIndex,
                                                        u4TxRxMcsSetNotEqualOption);

            }
            else
            {
                continue;
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Tx Rx Mcs Set Not Equal Option. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                                {
                                    continue;
                                }
                                if (nmhTestv2FsDot11nConfigTxRxMCSSetNotEqual
                                    (&u4ErrCode, i4RadioIfIndex,
                                     (INT4) u4TxRxMcsSetNotEqualOption) ==
                                    SNMP_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                                nmhSetFsDot11nConfigTxRxMCSSetNotEqual
                                    (i4RadioIfIndex,
                                     (INT4) u4TxRxMcsSetNotEqualOption);
                            }
                        }
                    }

                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Tx Rx Mcs Set Not Equal Option is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }

                if (nmhTestv2FsDot11nConfigTxRxMCSSetNotEqual (&u4ErrCode,
                                                               i4RadioIfIndex,
                                                               (INT4)
                                                               u4TxRxMcsSetNotEqualOption)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nConfigTxRxMCSSetNotEqual (i4RadioIfIndex,
                                                        (INT4)
                                                        u4TxRxMcsSetNotEqualOption);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetAmpduStatus   
 *  Description :  This function is used to set the AMPDU status 
 *
 * Input       :  CliHandle
 *                  u4RadioType
 *                  u4AmpduStatus
 *                  pu1ProfileName
 *                  pu4RadioId
 * Output      :  None
 *  
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetAmpduStatus (tCliHandle CliHandle, UINT4 u4RadioType,
                        UINT4 u4AmpduStatus, UINT1 *pu1ProfileName,
                        UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Max MPDU Length is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    continue;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nAMPDUStatus (&u4ErrCode,
                                                  i4RadioIfIndex,
                                                  u4AmpduStatus) ==
                    SNMP_FAILURE)
                {
                    continue;
                }
                nmhSetFsDot11nAMPDUStatus (i4RadioIfIndex, u4AmpduStatus);
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Max MPDU Length Mode. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioType) != OSIX_SUCCESS)
                        {
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nAMPDUStatus (&u4ErrCode,
                                                                  i4RadioIfIndex,
                                                                  u4AmpduStatus)
                                    == SNMP_FAILURE)
                                {
                                    continue;
                                }
                                nmhSetFsDot11nAMPDUStatus (i4RadioIfIndex,
                                                           u4AmpduStatus);
                            }
                        }
                    }

                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Max MPDU Length is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }
                if (nmhTestv2FsDot11nAMPDUStatus (&u4ErrCode,
                                                  i4RadioIfIndex,
                                                  (INT4) u4AmpduStatus) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nAMPDUStatus (i4RadioIfIndex,
                                           (INT4) u4AmpduStatus);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetAmpduSubFrame 
 *  Description :  This function is used to set the AMPDU Subframe length  
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4AmpduSubFrame
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetAmpduSubFrame (tCliHandle CliHandle, UINT4 u4RadioType,
                          UINT4 u4AmpduSubFrame, UINT1 *pu1ProfileName,
                          UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Max MPDU Length is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    continue;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }
                if (nmhTestv2FsDot11nAMPDUSubFrame (&u4ErrCode,
                                                    i4RadioIfIndex,
                                                    u4AmpduSubFrame) ==
                    SNMP_FAILURE)
                {
                    continue;
                }
                nmhSetFsDot11nAMPDUSubFrame (i4RadioIfIndex, u4AmpduSubFrame);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Max MPDU Length Mode. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioType) != OSIX_SUCCESS)
                        {
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nAMPDUSubFrame (&u4ErrCode,
                                                                    i4RadioIfIndex,
                                                                    u4AmpduSubFrame)
                                    == SNMP_FAILURE)
                                {
                                    continue;
                                }
                                nmhSetFsDot11nAMPDUSubFrame (i4RadioIfIndex,
                                                             u4AmpduSubFrame);
                            }
                        }
                    }

                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Max MPDU Length is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }
                if (nmhTestv2FsDot11nAMPDUSubFrame (&u4ErrCode,
                                                    i4RadioIfIndex,
                                                    u4AmpduSubFrame) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nAMPDUSubFrame (i4RadioIfIndex, u4AmpduSubFrame);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetAmpduLimit   
 *  Description :  This function is used to set the AMPDU Limit 
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4AmpduLimit
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetAmpduLimit (tCliHandle CliHandle, UINT4 u4RadioType,
                       UINT4 u4AmpduLimit, UINT1 *pu1ProfileName,
                       UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Max MPDU Length is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    continue;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }

                if (nmhTestv2FsDot11nAMPDULimit (&u4ErrCode,
                                                 i4RadioIfIndex, u4AmpduLimit)
                    == SNMP_FAILURE)
                {
                    continue;
                }

                nmhSetFsDot11nAMPDULimit (i4RadioIfIndex, u4AmpduLimit);
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Max MPDU Length Mode. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioType) != OSIX_SUCCESS)
                        {
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nAMPDULimit (&u4ErrCode,
                                                                 i4RadioIfIndex,
                                                                 u4AmpduLimit)
                                    == SNMP_FAILURE)
                                {
                                    continue;
                                }

                                nmhSetFsDot11nAMPDULimit (i4RadioIfIndex,
                                                          u4AmpduLimit);
                            }
                        }
                    }

                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Max MPDU Length is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }
                if (nmhTestv2FsDot11nAMPDULimit (&u4ErrCode,
                                                 i4RadioIfIndex, u4AmpduLimit)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsDot11nAMPDULimit (i4RadioIfIndex, u4AmpduLimit);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetAmsduStatus   
 *  Description :  This function is used to set the AMPDU status 
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4AmsduStatus
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetAmsduStatus (tCliHandle CliHandle, UINT4 u4RadioType,
                        UINT4 u4AmsduStatus, UINT1 *pu1ProfileName,
                        UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Max MPDU Length is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    continue;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }
                if (nmhTestv2FsDot11nAMSDUStatus (&u4ErrCode,
                                                  i4RadioIfIndex,
                                                  u4AmsduStatus) ==
                    SNMP_FAILURE)
                {
                    continue;
                }
                nmhSetFsDot11nAMSDUStatus (i4RadioIfIndex, u4AmsduStatus);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Max MPDU Length Mode. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioType) != OSIX_SUCCESS)
                        {
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nAMSDUStatus (&u4ErrCode,
                                                                  i4RadioIfIndex,
                                                                  u4AmsduStatus)
                                    == SNMP_FAILURE)
                                {
                                    continue;
                                }
                                nmhSetFsDot11nAMSDUStatus (i4RadioIfIndex,
                                                           u4AmsduStatus);
                            }
                        }
                    }

                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Max MPDU Length is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }
                if (nmhTestv2FsDot11nAMSDUStatus (&u4ErrCode,
                                                  i4RadioIfIndex,
                                                  u4AmsduStatus) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nAMSDUStatus (i4RadioIfIndex, u4AmsduStatus);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *  Function    :  Fs11nCliSetAmsduLimit   
 *  Description :  This function is used to set the AMSDU Limit
 *
 *  Input       :  CliHandle
 *                  u4RadioType
 *                  u4AmsduStatus
 *                  pu1ProfileName
 *                  pu4RadioId
 *  Output      :  None
 *  
 *  Returns     :  CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
Fs11nCliSetAmsduLimit (tCliHandle CliHandle, UINT4 u4RadioType,
                       UINT4 u4AmsduLimit, UINT1 *pu1ProfileName,
                       UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided AP Max MPDU Length is updated for */
    /* all Ap's */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId,
                 u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    continue;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    continue;
                }
                if (nmhTestv2FsDot11nAMSDULimit (&u4ErrCode,
                                                 i4RadioIfIndex,
                                                 u4AmsduLimit) == SNMP_FAILURE)
                {
                    continue;
                }
                nmhSetFsDot11nAMSDULimit (i4RadioIfIndex, u4AmsduLimit);

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId,
                &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }
        /*If AP name is provided and not the radio id, all radio id's of the
         *      AP is updated with the AP Max MPDU Length Mode. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId,
                 &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }

            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId,
                         u4currentBindingId, &i4RadioIfIndex) == SNMP_SUCCESS)
                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4RadioType) != OSIX_SUCCESS)
                        {
                            if ((u4RadioType == RADIO_TYPE_A)
                                || (u4RadioType == RADIO_TYPE_B))
                            {
                                if (nmhTestv2FsDot11nAMSDULimit (&u4ErrCode,
                                                                 i4RadioIfIndex,
                                                                 u4AmsduLimit)
                                    == SNMP_FAILURE)
                                {
                                    continue;
                                }
                                nmhSetFsDot11nAMSDULimit (i4RadioIfIndex,
                                                          u4AmsduLimit);
                            }
                        }
                    }

                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId,
                    &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
        }
        /*Only for the particular radio id of the AP, Max MPDU Length is updated. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex,
                                            &u4GetRadioType) != SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n Getting Radio Type failed \r\n");
                    return CLI_FAILURE;
                }

                if (WSSCFG_RADIO_TYPE_COMP (u4RadioType))
                {
                    CliPrintf (CliHandle, "\r\n Radio type mismatch\r\n");
                    return CLI_FAILURE;
                }
                if (nmhTestv2FsDot11nAMSDULimit (&u4ErrCode,
                                                 i4RadioIfIndex,
                                                 u4AmsduLimit) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                nmhSetFsDot11nAMSDULimit (i4RadioIfIndex, u4AmsduLimit);
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  Fs11nCliShow
 * Description :  This function is exported to CLI module to display all the
            mib ojects values.

 * Input       :  CliHandle
            u4RadioType
            pu1ProfileName
            pu4RadioId            

 * Output      :  None

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT1
Fs11nCliShow (tCliHandle CliHandle, UINT4 u4RadioType,
              UINT1 *pu1ProfileName, UINT4 *pu4RadioId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    UINT4               u4GetRadioType = 0;

    /*If no Profile name is provided then all mib objects for */
    /* all Ap's are disaplyed */
    if (pu1ProfileName == NULL)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId, u4currentBindingId,
                 &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (ShowCommand_11N (CliHandle, i4RadioIfIndex) != CLI_SUCCESS)
                {
                    CLI_SET_ERR (CLI_11N_SHOW_ERROR);
                    return CLI_FAILURE;
                }

            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

    }
    else
    {
        if (CapwapGetWtpProfileIdFromProfileName
            (pu1ProfileName, &u4WtpProfileId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_11N_WTPPROFILE_ERROR);
            return CLI_FAILURE;
        }

        /*If AP name is provided and not the radio id, all radio id's of the
         *  *          * AP with its mib objects are displayed. */
        if (pu4RadioId == NULL)
        {
            u4currentBindingId = 0;
            if (nmhGetNextIndexFsCapwapWirelessBindingTable
                (u4WtpProfileId, &u4nextProfileId,
                 u4currentBindingId, &u4nextBindingId) != SNMP_SUCCESS)

            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            do
            {
                u4currentProfileId = u4nextProfileId;
                u4currentBindingId = u4nextBindingId;
                if (u4currentProfileId == u4WtpProfileId)
                {

                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u4currentProfileId, u4currentBindingId,
                         &i4RadioIfIndex) == SNMP_SUCCESS)

                    {
                        if (nmhGetFsDot11RadioType
                            (i4RadioIfIndex, &u4GetRadioType) != OSIX_SUCCESS)
                        {
                            if ((u4RadioType == RADIO_TYPE_A) ||
                                (u4RadioType == RADIO_TYPE_B))
                            {

                                if (ShowCommand_11N (CliHandle, i4RadioIfIndex)
                                    != CLI_SUCCESS)
                                {
                                    CLI_SET_ERR (CLI_11N_SHOW_ERROR);
                                    return CLI_FAILURE;

                                }

                            }
                        }
                    }
                }
                else
                {
                    break;
                }
            }
            while (nmhGetNextIndexFsCapwapWirelessBindingTable
                   (u4WtpProfileId, &u4nextProfileId,
                    u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);

        }
        /*Only for the particular radio id of the AP, mib objects are disaplyed. */
        else
        {
            u4RadioId = (UINT4) CLI_PTR_TO_U4 (*pu4RadioId);

            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4WtpProfileId, u4RadioId, &i4RadioIfIndex) == SNMP_SUCCESS)

            {
                if (ShowCommand_11N (CliHandle, i4RadioIfIndex) != CLI_SUCCESS)
                {
                    CLI_SET_ERR (CLI_11N_SHOW_ERROR);
                    return CLI_FAILURE;
                }
            }
            else
            {
                CLI_SET_ERR (CLI_11N_RADIOINDEX_ERROR);

                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  ShowCommand_11N
 * Description :  This function is use for displaying the values of
           various mib objects.

 * Input       :  CliHandle
 *           i4RadioIfIndex

 * Output      :  None

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/

INT1
ShowCommand_11N (tCliHandle CliHandle, INT4 i4RadioIfIndex)
{
    INT4                i4LdpcOption = 0;
    INT4                i4PowerSaveOption = 0;
    INT4                i4GreenfieldOption = 0;
    INT4                i4ShortGITwentyOption = 0;
    INT4                i4Short40GI = 0;
    INT4                i4TxStbcOption = 0;
    INT4                i4RxStbcOption = 0;
    INT4                i4delayBlockAck = 0;
    INT4                i4DssCckModeOption = 0;
    INT4                i4FortyMhzIntolerantOption = 0;
    INT4                i4LSigTxopProtectionOption = 0;
    UINT4               u4MaxMpduOption = 0;
    UINT4               u4ampduFactor = 0;
    INT4                i4ampduStatus = 0;
    INT4                i4amsduStatus = 0;
    UINT4               u4ampduSubFrame = 0;
    UINT4               u4ampduLimit = 0;
    UINT4               u4amsduLimit = 0;
    INT4                i4pcoOption = 0;
    UINT4               u4pcoTransTime = 0;
    INT4                i4mcsFeedbackOption = 0;
    INT4                i4htcFieldOption = 0;
    INT4                i4RDRespondOption = 0;
    INT4                i4ImpTranBeamRecOption = 0;
    INT4                i4RecStagSoundOption = 0;
    INT4                i4TranStagSoundOption = 0;
    INT4                i4RecvNDPOption = 0;
    INT4                i4TranNDPOption = 0;
    INT4                i4ImpTranBeamOption = 0;
    INT4                i4CalibrationOption = 0;
    INT4                i4ExplCsiTranBeamOption = 0;
    INT4                i4ExplNoncompSteOption = 0;
    INT4                i4ExplcompSteOption = 0;
    INT4                i4ExplTranBeamCsiOption = 0;
    INT4                i4ExplNCompBeamOption = 0;
    INT4                i4ExplCompBeamOption = 0;
    UINT4               u4numCsiAntSupOption = 0;
    UINT4               u4numNCompAntSupOption = 0;
    UINT4               u4ChannelEstOption = 0;
    UINT4               u4PrimaryChannelOption = 0;
    INT4                i4RifsModeOption = 0;
    INT4                i4htProtectionOption = 0;
    INT4                i4htNonObssStaOption = 0;
    INT4                i4DualCtsProtectionOption = 0;
    INT4                i4StbcBeaconOption = 0;
    INT4                i4PcoPhaseOption = 0;
    INT4                i4MaxAmsduLenghth = 0;
    INT4                i4PcoConfigOption = 0;
    UINT4               u4MinGroupingOption = 0;
    UINT4               u4numCompAntSupOption = 0;
    UINT4               u4MaxnumCsiAntSupOption = 0;
    INT4                i4htOpChannelWidthOption = 0;
    UINT4               u4mcsFeedbackOption = 0;
    INT4                i4TxRxMcsSetOption = 0;
    INT4                i4mcsSetOption = 0;

    if (nmhGetDot11LDPCCodingOptionImplemented (i4RadioIfIndex, &i4LdpcOption)
        == SNMP_SUCCESS)
    {
        if (i4LdpcOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nLDPC Option                              : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nLDPC Option                              : Enable");
        }
    }
    if (nmhGetDot11MIMOPowerSave (i4RadioIfIndex, &i4PowerSaveOption)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nPower Save Mode            : %d", i4PowerSaveOption);
    }
    if (nmhGetDot11HTGreenfieldOptionImplemented
        (i4RadioIfIndex, &i4GreenfieldOption) == SNMP_SUCCESS)
    {
        if (i4GreenfieldOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nGreen Field Option                     : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nGreen Field OPtion                     : Enable");
        }
    }
    if (nmhGetDot11ShortGIOptionInTwentyImplemented
        (i4RadioIfIndex, &i4ShortGITwentyOption) == SNMP_SUCCESS)
    {
        if (i4ShortGITwentyOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nShort Guard Interval 20 MHz            : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nShort Guard Interval 20 MHz            : Enable");
        }
    }
    if (nmhGetDot11ShortGIOptionInFortyImplemented
        (i4RadioIfIndex, &i4Short40GI) == SNMP_SUCCESS)
    {
        if (i4Short40GI == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nShort Guard Interval 40 MHz            : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nShort Guard Interval 40 MHz            : Enable");
        }
    }

    if (nmhGetDot11TxSTBCOptionImplemented (i4RadioIfIndex, &i4TxStbcOption)
        == SNMP_SUCCESS)
    {
        if (i4TxStbcOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nTx STBC Option                         : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nTx STBC Option                         : Enable");
        }
    }

    if (nmhGetDot11RxSTBCOptionImplemented (i4RadioIfIndex, &i4RxStbcOption)
        == SNMP_SUCCESS)
    {
        if (i4RxStbcOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nRx STBC Option                         : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nRx STBC Option                         : Enable");
        }
    }
    if (nmhGetDot11NDelayedBlockAckOptionImplemented
        (i4RadioIfIndex, &i4delayBlockAck) == SNMP_SUCCESS)
    {
        if (i4delayBlockAck == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nHT Delayed Block Ack Option            : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nHT Delayed Block Ack Option            : Enable");
        }
    }
    if (nmhGetDot11MaxAMSDULength (i4RadioIfIndex, &i4MaxAmsduLenghth)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nMaximum AMSDU Length                   : %d",
                   i4MaxAmsduLenghth);
    }
    if (nmhGetFsDot11nConfigtHTDSSCCKModein40MHz
        (i4RadioIfIndex, &i4DssCckModeOption) == SNMP_SUCCESS)
    {
        if (i4DssCckModeOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nDSS/CCk Mode in 40 MHz                 : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nDSS?CCK Mode in 40 MHz                 : Enable");
        }
    }
    if (nmhGetDot11FortyMHzIntolerant
        (i4RadioIfIndex, &i4FortyMhzIntolerantOption) == SNMP_SUCCESS)
    {
        if (i4FortyMhzIntolerantOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\n40 MHz Intolerant Option               : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\n40 MHz Intolerant Option               : Enable");
        }
    }
    if (nmhGetDot11LsigTxopProtectionOptionImplemented
        (i4RadioIfIndex, &i4LSigTxopProtectionOption) == SNMP_SUCCESS)
    {
        if (i4LSigTxopProtectionOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nL-SiL-SIG TXOP Protection Support      : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nL-SIG TXOP Protection Support          : Enable");
        }
    }
    if (nmhGetDot11MaxRxAMPDUFactor (i4RadioIfIndex, &u4MaxMpduOption)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nMaximum MPDU Factor                    : %d",
                   u4MaxMpduOption);
    }
    if (nmhGetDot11MinimumMPDUStartSpacing (i4RadioIfIndex, &u4ampduFactor)
        == SNMP_SUCCESS)
    {
        if (u4ampduFactor == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nMaximum MPDU Start Spacing             : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nMaximum MPDU Start Spacing             : Enable");
        }
    }
    if (nmhGetDot11PCOOptionImplemented (i4RadioIfIndex, &i4pcoOption)
        == SNMP_SUCCESS)
    {
        if (i4pcoOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nPCO Option                             : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nPCI Option                             : Enable");
        }
    }
    if (nmhGetDot11TransitionTime (i4RadioIfIndex, &u4pcoTransTime)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nPCO Transition time                    : %d",
                   u4pcoTransTime);
    }
    if (nmhGetDot11MCSFeedbackOptionImplemented
        (i4RadioIfIndex, &i4mcsFeedbackOption) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nMCS Feedback Value                    : %d",
                   i4mcsFeedbackOption);
    }
    if (nmhGetDot11HTControlFieldSupported (i4RadioIfIndex, &i4htcFieldOption)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nHTC Support                            : %d",
                   i4htcFieldOption);
    }
    if (nmhGetDot11RDResponderOptionImplemented
        (i4RadioIfIndex, &i4RDRespondOption) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nRD Responder                           : %d",
                   i4RDRespondOption);
    }
    if (nmhGetFsDot11nConfigImplicitTransmitBeamformingRecvOptionImplemented
        (i4RadioIfIndex, &i4ImpTranBeamRecOption) == SNMP_SUCCESS)
    {
        if (i4ImpTranBeamRecOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nImplicit Transmit Beamforming Receiving Capable    : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nImplicit Transmit Beamforming Receiving Capable    : Enable");
        }
    }
    if (nmhGetDot11ReceiveStaggerSoundingOptionImplemented
        (i4RadioIfIndex, &i4RecStagSoundOption) == SNMP_SUCCESS)
    {
        if (i4RecStagSoundOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nReceiving Staggered Sounding Capable           : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nReceiving Staggered Sounding Capable           : Enable");
        }
    }
    if (nmhGetDot11TransmitStaggerSoundingOptionImplemented
        (i4RadioIfIndex, &i4TranStagSoundOption) == SNMP_SUCCESS)
    {
        if (i4TranStagSoundOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nTransmit Staggered Sounding Capable        : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nTransmit Staggered Sounding Capable        : Enable");
        }
    }
    if (nmhGetDot11ReceiveNDPOptionImplemented
        (i4RadioIfIndex, &i4RecvNDPOption) == SNMP_SUCCESS)
    {
        if (i4RecvNDPOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nReceive NDP Capable                        : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nReceive NDP Capable                        : Enable");
        }
    }

    if (nmhGetDot11TransmitNDPOptionImplemented
        (i4RadioIfIndex, &i4TranNDPOption) == SNMP_SUCCESS)
    {
        if (i4TranNDPOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\n Transmit NDP Capable                      : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\n Transmit NDP Capable                      : Enable");
        }
    }

    if (nmhGetDot11ImplicitTransmitBeamformingOptionImplemented
        (i4RadioIfIndex, &i4ImpTranBeamOption) == SNMP_SUCCESS)
    {
        if (i4ImpTranBeamOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nImplicit Transmit Beamforming Capable      : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nImplicit Transmit Beamforming Capable      : Enable");
        }
    }
    if (nmhGetDot11CalibrationOptionImplemented
        (i4RadioIfIndex, &i4CalibrationOption) == SNMP_SUCCESS)
    {
        if (i4CalibrationOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nCalibration Option                          : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nCalibration Option                         : Enable");
        }
    }
    if (nmhGetDot11ExplicitCSITransmitBeamformingOptionImplemented
        (i4RadioIfIndex, &i4ExplCsiTranBeamOption) == SNMP_SUCCESS)
    {
        if (i4ExplCsiTranBeamOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit CSI Transmit Beamforming          : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit CSI Transmit Beamforming          : Enable");
        }
    }
    if (nmhGetDot11ExplicitNonCompressedBeamformingMatrixOptionImplemented
        (i4RadioIfIndex, &i4ExplNoncompSteOption) == SNMP_SUCCESS)
    {
        if (i4ExplNoncompSteOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit Noncompressed Steering            : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit Noncompressed Steering            : Enable");
        }
    }
    if (nmhGetFsDot11nConfigExplicitCompressedBeamformingMatrixOptImplemented
        (i4RadioIfIndex, &i4ExplcompSteOption) == SNMP_SUCCESS)
    {
        if (i4ExplcompSteOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit Compressed Steering               : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit Compressed Steering               : Enable");
        }
    }
    if (nmhGetDot11ExplicitTransmitBeamformingCSIFeedbackOptionImplemented
        (i4RadioIfIndex, &i4ExplTranBeamCsiOption) == SNMP_SUCCESS)
    {
        if (i4ExplTranBeamCsiOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit Transmit Beamforming CSI Feedback : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit Transmit Beamforming CSI Feedback : Enable");
        }
    }
    if (nmhGetDot11ExplicitNonCompressedBeamformingFeedbackOptionImplemented
        (i4RadioIfIndex, &i4ExplNCompBeamOption) == SNMP_SUCCESS)
    {
        if (i4ExplNCompBeamOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit Noncompressed Beamforming Feedback    : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit Noncompressed Beamforming Feedback    : Enable");
        }
    }
    if (nmhGetDot11ExplicitCompressedBeamformingFeedbackOptionImplemented
        (i4RadioIfIndex, &i4ExplCompBeamOption) == SNMP_SUCCESS)
    {
        if (i4ExplCompBeamOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit Compressed Beamforming Feedback       : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit Compressed Beamforming Feedback       : Enable");
        }
    }
    if (nmhGetFsDot11nConfigMinimalGroupingImplemented
        (i4RadioIfIndex, &u4MinGroupingOption) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nMinimal Grouping                               : %d",
                   u4MinGroupingOption);
    }
    if (nmhGetDot11NumberBeamFormingCSISupportAntenna
        (i4RadioIfIndex, &u4numCsiAntSupOption) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nCSI Number of Beamformer Antennas Supported:   %d",
                   u4numCsiAntSupOption);
    }
    if (nmhGetDot11NumberNonCompressedBeamformingMatrixSupportAntenna
        (i4RadioIfIndex, &u4numNCompAntSupOption) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nNoncompressed Steering Number of Beamformer Antennas Supported : %d",
                   u4numNCompAntSupOption);
    }
    if (nmhGetDot11NumberCompressedBeamformingMatrixSupportAntenna
        (i4RadioIfIndex, &u4numCompAntSupOption) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\ncompressed Steering Number of Beamformer Antennas Supported    : %d",
                   u4numCompAntSupOption);
    }
    if (nmhGetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaImplemented
        (i4RadioIfIndex, &u4MaxnumCsiAntSupOption) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nCSI Max Number of Rows Beamformer Supported                    : %d",
                   u4MaxnumCsiAntSupOption);
    }
    if (nmhGetFsDot11nConfigChannelEstimationCapabilityImplemented
        (i4RadioIfIndex, &u4ChannelEstOption) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nChannel Estimation                         : %d",
                   u4ChannelEstOption);
    }
    if (nmhGetDot11CurrentPrimaryChannel
        (i4RadioIfIndex, &u4PrimaryChannelOption) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nPrimary Channel                            : %d",
                   u4PrimaryChannelOption);
    }
    if (nmhGetFsDot11nConfigSTAChannelWidth
        (i4RadioIfIndex, &i4htOpChannelWidthOption) == SNMP_SUCCESS)
    {
        if (i4htOpChannelWidthOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nSTA Channel Width                          : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nSTA Channel Width                          : Enable");
        }
    }
    if (nmhGetDot11RIFSMode (i4RadioIfIndex, &i4RifsModeOption) == SNMP_SUCCESS)
    {
        if (i4RifsModeOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nRIFS Mode                                  : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nRIFS Mode                                  : Enable");
        }
    }
    if (nmhGetDot11HTProtection (i4RadioIfIndex, &i4htProtectionOption)
        == SNMP_SUCCESS)
    {
        if (i4htProtectionOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nHT Protection                              : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nHT Protection                              : Enable");
        }
    }
    if (nmhGetFsDot11nConfigOBSSNonHTSTAsPresent
        (i4RadioIfIndex, &i4htNonObssStaOption) == SNMP_SUCCESS)
    {
        if (i4htNonObssStaOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nOBSS Non-HT STAs present                   : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nOBSS Non-HT STAs present                   : Enable");
        }
    }
    if (nmhGetDot11DualCTSProtection
        (i4RadioIfIndex, &i4DualCtsProtectionOption) == SNMP_SUCCESS)
    {
        if (i4DualCtsProtectionOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nDual CTS Protection                        : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nDual CTS Protection                        : Enable");
        }
    }
    if (nmhGetFsDot11nConfigSTBCBeacon (i4RadioIfIndex, &i4StbcBeaconOption)
        == SNMP_SUCCESS)
    {
        if (i4StbcBeaconOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nSTBC Beacon                                : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nSTBC Beacon                                : Enable");
        }
    }
    if (nmhGetFsDot11nConfigPCOPhase (i4RadioIfIndex, &i4PcoPhaseOption)
        == SNMP_SUCCESS)
    {
        if (i4PcoPhaseOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nPCO Phase                                   : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nPCO Phase                                  : Enable");
        }
    }

    if (nmhGetDot11LDPCCodingOptionActivated (i4RadioIfIndex, &i4LdpcOption)
        == SNMP_SUCCESS)
    {
        if (i4LdpcOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nLDPC Option Configured                     : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nLDPC Option Configured                      : Enable");
        }
    }
    if (nmhGetFsDot11nConfigMIMOPowerSave (i4RadioIfIndex, &i4PowerSaveOption)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nPower Save Mode Configured                   : %d",
                   i4PowerSaveOption);
    }
    if (nmhGetDot11HTGreenfieldOptionActivated
        (i4RadioIfIndex, &i4GreenfieldOption) == SNMP_SUCCESS)
    {
        if (i4GreenfieldOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nGreen Field Option Configured                 : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nGreen Field OPtion Configured                  : Enable");
        }
    }
    if (nmhGetDot11ShortGIOptionInTwentyActivated
        (i4RadioIfIndex, &i4ShortGITwentyOption) == SNMP_SUCCESS)
    {
        if (i4ShortGITwentyOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nShort Guard Interval 20 MHz Configured             : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nShort Guard Interval 20 MHz Configured             : Enable");
        }
    }
    if (nmhGetDot11ShortGIOptionInFortyActivated (i4RadioIfIndex, &i4Short40GI)
        == SNMP_SUCCESS)
    {
        if (i4Short40GI == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nShort Guard Interval 40 MHz Configured             : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nShort Guard Interval 40 MHz Configured             : Enable");
        }
    }

    if (nmhGetDot11TxSTBCOptionActivated (i4RadioIfIndex, &i4TxStbcOption)
        == SNMP_SUCCESS)
    {
        if (i4TxStbcOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nTx STBC Option Configured                          : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nTx STBC Option COnfigured                          : Enable");
        }
    }

    if (nmhGetDot11RxSTBCOptionActivated (i4RadioIfIndex, &i4RxStbcOption)
        == SNMP_SUCCESS)
    {
        if (i4RxStbcOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nRx STBC Option Configured                          : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nRx STBC Option Configured                          : Enable");
        }
    }
    if (nmhGetFsDot11nConfigDelayedBlockAckOptionActivated
        (i4RadioIfIndex, &i4delayBlockAck) == SNMP_SUCCESS)
    {
        if (i4delayBlockAck == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nHT Delayed Block Ack Option Configured           : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nHT Delayed Block Ack Option Configured           : Enable");
        }
    }
    if (nmhGetFsDot11nConfigMaxAMSDULengthConfig
        (i4RadioIfIndex, &i4MaxAmsduLenghth) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nMaximum AMSDU Length Configured                  : %d",
                   i4MaxAmsduLenghth);
    }
    if (nmhGetFsDot11nConfigtHTDSSCCKModein40MHzConfig
        (i4RadioIfIndex, &i4DssCckModeOption) == SNMP_SUCCESS)
    {
        if (i4DssCckModeOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nDSS/CCk Mode in 40 MHz Configured                  : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nDSS?CCK Mode in 40 MHz Configured                  : Enable");
        }
    }
    if (nmhGetDot11LSIGTXOPFullProtectionActivated
        (i4RadioIfIndex, &i4LSigTxopProtectionOption) == SNMP_SUCCESS)
    {
        if (i4LSigTxopProtectionOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nL-SIG TXOP Protection Support Configured          : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nL-SIG TXOP Protection Support Configured           : Enable");
        }
    }
    if (nmhGetFsDot11nConfigMaxRxAMPDUFactorConfig
        (i4RadioIfIndex, &u4MaxMpduOption) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nMaximum MPDU Factor Confidured                       : %d",
                   u4MaxMpduOption);
    }
    if (nmhGetFsDot11nConfigMinimumMPDUStartSpacingConfig
        (i4RadioIfIndex, &u4ampduFactor) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nMaximum MPDU Start Spacing Configured              : %d",
                   u4ampduFactor);
    }
    if (nmhGetFsDot11nConfigPCOOptionActivated (i4RadioIfIndex, &i4pcoOption)
        == SNMP_SUCCESS)
    {
        if (i4pcoOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nPCO Option Configured                              : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nPCI Option Configured                              : Enable");
        }
    }
    if (nmhGetFsDot11nConfigTransitionTimeConfig
        (i4RadioIfIndex, &u4pcoTransTime) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nPCO Transition time Configured                     : %d",
                   u4pcoTransTime);
    }
    if (nmhGetFsDot11nConfigMCSFeedbackOptionActivated
        (i4RadioIfIndex, &i4mcsFeedbackOption) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nMCS Feedback Value Configured                     : %d",
                   u4mcsFeedbackOption);
    }
    if (nmhGetFsDot11nConfigHTControlFieldSupported
        (i4RadioIfIndex, &i4htcFieldOption) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nHTC Support Configured                            : %d",
                   i4htcFieldOption);
    }
    if (nmhGetFsDot11nConfigRDResponderOptionActivated
        (i4RadioIfIndex, &i4RDRespondOption) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nRD Responder Configured                          : %d",
                   i4RDRespondOption);
    }
    if (nmhGetFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated
        (i4RadioIfIndex, &i4ImpTranBeamRecOption) == SNMP_SUCCESS)
    {
        if (i4ImpTranBeamRecOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nImplicit Transmit Beamforming Receiving Configured    : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nImplicit Transmit Beamforming Receiving Configured    : Enable");
        }
    }
    if (nmhGetFsDot11nConfigReceiveStaggerSoundingOptionActivated
        (i4RadioIfIndex, &i4RecStagSoundOption) == SNMP_SUCCESS)
    {
        if (i4RecStagSoundOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nReceiving Staggered Sounding Configured               : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nReceiving Staggered Sounding Configured               : Enable");
        }
    }
    if (nmhGetFsDot11nConfigTransmitStaggerSoundingOptionActivated
        (i4RadioIfIndex, &i4TranStagSoundOption) == SNMP_SUCCESS)
    {
        if (i4TranStagSoundOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nTransmit Staggered Sounding Configured                 : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nTransmit Staggered Sounding Configured                  : Enable");
        }
    }
    if (nmhGetFsDot11nConfigReceiveNDPOptionActivated
        (i4RadioIfIndex, &i4RecvNDPOption) == SNMP_SUCCESS)
    {
        if (i4RecvNDPOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nReceive NDP Configured                                 : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nReceive NDP Configured                                 : Enable");
        }
    }

    if (nmhGetFsDot11nConfigTransmitNDPOptionActivated
        (i4RadioIfIndex, &i4TranNDPOption) == SNMP_SUCCESS)
    {
        if (i4TranNDPOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\n Transmit NDP Configured                                 : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\n Transmit NDP Configured                                 : Enable");
        }
    }

    if (nmhGetFsDot11nConfigImplicitTransmitBeamformingOptionActivated
        (i4RadioIfIndex, &i4ImpTranBeamOption) == SNMP_SUCCESS)
    {
        if (i4ImpTranBeamOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nImplicit Transmit Beamforming Configured                 : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nImplicit Transmit Beamforming Configured                 : Enable");
        }
    }
    if (nmhGetFsDot11nConfigCalibrationOptionActivated
        (i4RadioIfIndex, &i4CalibrationOption) == SNMP_SUCCESS)
    {
        if (i4CalibrationOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nCalibration Option Configured                             : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nCalibration Option Configured                             : Enable");
        }
    }
    if (nmhGetFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated
        (i4RadioIfIndex, &i4ExplCsiTranBeamOption) == SNMP_SUCCESS)
    {
        if (i4ExplCsiTranBeamOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit CSI Transmit Beamforming Configured                : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit CSI Transmit Beamforming Configured                : Enable");
        }
    }
    if (nmhGetFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated
        (i4RadioIfIndex, &i4ExplNoncompSteOption) == SNMP_SUCCESS)
    {
        if (i4ExplNoncompSteOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit Noncompressed Steering Configured                : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit Noncompressed Steering Configured                : Enable");
        }
    }
    if (nmhGetFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated
        (i4RadioIfIndex, &i4ExplcompSteOption) == SNMP_SUCCESS)
    {
        if (i4ExplcompSteOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit Compressed Steering Configured                  : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit Compressed Steering                             : Enable");
        }
    }
    if (nmhGetFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated
        (i4RadioIfIndex, &i4ExplTranBeamCsiOption) == SNMP_SUCCESS)
    {
        if (i4ExplTranBeamCsiOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit Transmit Beamforming CSI Configured            : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit Transmit Beamforming CSI Configured            : Enable");
        }
    }
    if (nmhGetFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated
        (i4RadioIfIndex, &i4ExplNCompBeamOption) == SNMP_SUCCESS)
    {
        if (i4ExplNCompBeamOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit Noncompressed Beamforming Feedback Configured     : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit Noncompressed Beamforming Feedback Configured      : Enable");
        }
    }
    if (nmhGetFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated
        (i4RadioIfIndex, &i4ExplCompBeamOption) == SNMP_SUCCESS)
    {
        if (i4ExplCompBeamOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit Compressed Beamforming Feedback Configured          : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nExplicit Compressed Beamforming Feedback Configured          : Enable");
        }
    }
    if (nmhGetFsDot11nConfigMinimalGroupingActivated
        (i4RadioIfIndex, &u4MinGroupingOption) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nMinimal Grouping Configured                                  : %d",
                   u4MinGroupingOption);
    }
    if (nmhGetFsDot11nConfigNumberBeamFormingCSISupportAntenna
        (i4RadioIfIndex, &u4numCsiAntSupOption) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nCSI Number of Beamformer Antennas Configured                   : %d",
                   u4numCsiAntSupOption);
    }
    if (nmhGetFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna
        (i4RadioIfIndex, &u4numNCompAntSupOption) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nNoncompressed Steering Number of Beamformer Antennas Configured     : %d",
                   u4numNCompAntSupOption);
    }
    if (nmhGetFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna
        (i4RadioIfIndex, &u4numCompAntSupOption) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\ncompressed Steering Number of Beamformer Antennas Configured    : %d",
                   u4numCompAntSupOption);
    }
    if (nmhGetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated
        (i4RadioIfIndex, &u4MaxnumCsiAntSupOption) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nCSI Max Number of Rows Beamformer Configured                    : %d",
                   u4MaxnumCsiAntSupOption);
    }
    if (nmhGetFsDot11nConfigChannelEstimationCapabilityActivated
        (i4RadioIfIndex, &u4ChannelEstOption) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nChannel Estimation Configured                        : %d",
                   u4ChannelEstOption);
    }
    if (nmhGetFsDot11nConfigCurrentPrimaryChannel
        (i4RadioIfIndex, &u4PrimaryChannelOption) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nPrimary Channel Configured                           : %d",
                   u4PrimaryChannelOption);
    }
    if (nmhGetFsDot11nConfigSTAChannelWidthConfig
        (i4RadioIfIndex, &i4htOpChannelWidthOption) == SNMP_SUCCESS)
    {
        if (i4htOpChannelWidthOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nSTA Channel Width Configured                         : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nSTA Channel Width Configured                         : Enable");
        }
    }
    if (nmhGetFsDot11nConfigOBSSNonHTSTAsPresentConfig
        (i4RadioIfIndex, &i4htNonObssStaOption) == SNMP_SUCCESS)
    {
        if (i4htNonObssStaOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nOBSS Non-HT STAs Configured                   : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nOBSS Non-HT STAs Configured                   : Enable");
        }
    }
    if (nmhGetFsDot11nConfigSTBCBeaconConfig
        (i4RadioIfIndex, &i4StbcBeaconOption) == SNMP_SUCCESS)
    {
        if (i4StbcBeaconOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\nSTBC Beacon                                : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nSTBC Beacon                                : Enable");
        }
    }
    if (nmhGetDot11PCOActivated (i4RadioIfIndex, &i4PcoConfigOption)
        == SNMP_SUCCESS)
    {
        if (i4PcoConfigOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\n PCO Configured                            : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\n PCO Configured                           : Enable");
        }
    }
    if (nmhGetFsDot11nConfigPCOPhaseConfig (i4RadioIfIndex, &i4PcoPhaseOption)
        == SNMP_SUCCESS)
    {
        if (i4PcoPhaseOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\n PCO Phase Configured                            : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nPCO Phase Configured                           : Enable");
        }
    }
    if (nmhGetDot11TxMCSSetDefined (i4RadioIfIndex, &i4mcsSetOption)
        == SNMP_SUCCESS)
    {
        if (i4mcsSetOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\n MCS SEt Defined Option                            : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\n MCS SEt Defined Option                           : Enable");
        }
    }
    if (nmhGetFsDot11nConfigTxRxMCSSetNotEqual
        (i4RadioIfIndex, &i4TxRxMcsSetOption) == SNMP_SUCCESS)
    {
        if (i4TxRxMcsSetOption == 0)
        {
            CliPrintf (CliHandle,
                       "\r\n Tx Rx MCs Set Option                            : Disable");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\n Tx Rx MCs Set Option                           : Enable");
        }
    }

    if (nmhGetFsDot11nAMPDUStatus (i4RadioIfIndex, &i4ampduStatus)
        == SNMP_SUCCESS)
    {
        if (i4ampduStatus == 0)
        {
            CliPrintf (CliHandle,
                       "\r\n AMPDU Status                            : Disable");
        }

        else
        {
            CliPrintf (CliHandle,
                       "\r\n AMPDU Status                            : Enable");
        }
    }
    if (nmhGetFsDot11nAMPDUSubFrame (i4RadioIfIndex, &u4ampduSubFrame)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n AMPDU SubFrame                          : %d",
                   u4ampduSubFrame);
    }
    if (nmhGetFsDot11nAMPDULimit (i4RadioIfIndex, &u4ampduLimit)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n AMPDU Limit                             : %d",
                   u4ampduLimit);

    }
    if (nmhGetFsDot11nAMSDUStatus (i4RadioIfIndex, &i4amsduStatus)
        == SNMP_SUCCESS)
    {
        if (i4amsduStatus == 0)
        {
            CliPrintf (CliHandle,
                       "\r\n AMSDU Status                            : Disable");
        }

        else
        {
            CliPrintf (CliHandle,
                       "\r\n AMSDU Status                            : Enable");
        }
    }
    if (nmhGetFsDot11nAMSDULimit (i4RadioIfIndex, &u4amsduLimit)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n AMSDU Limit                             : %d\n",
                   u4amsduLimit);

    }

    return CLI_SUCCESS;
}

#endif
