/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
 *  $Id: wsscfgutlg.c,v 1.2 2017/11/24 10:37:09 siva Exp $
*
* Description: This file contains utility functions used by protocol Wsscfg
*********************************************************************/

#include "wsscfginc.h"
#include "wsscfgcli.h"

/****************************************************************************
 Function    :  WsscfgUtlCreateRBTree
 Input       :  None
 Description :  This function creates all 
                the RBTree required.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
WsscfgUtlCreateRBTree ()
{
    if (WsscfgCapwapDot11WlanTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgCapwapDot11WlanBindTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11StationConfigTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11AuthenticationAlgorithmsTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11WEPDefaultKeysTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11WEPKeyMappingsTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11PrivacyTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11MultiDomainCapabilityTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11SpectrumManagementTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11RegulatoryClassesTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11OperationTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11CountersTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11GroupAddressesTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11EDCATableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11QAPEDCATableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11QosCountersTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11ResourceInfoTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11PhyOperationTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11PhyAntennaTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11PhyTxPowerTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11PhyFHSSTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11PhyDSSSTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11PhyIRTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11RegDomainsSupportedTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11AntennasListTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11SupportedDataRatesTxTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11SupportedDataRatesRxTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11PhyOFDMTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11PhyHRDSSSTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11HoppingPatternTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11PhyERPTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11RSNAConfigTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11RSNAConfigPairwiseCiphersTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11RSNAConfigAuthenticationSuitesTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgDot11RSNAStatsTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsDot11StationConfigTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsDot11CapabilityProfileTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsDot11AuthenticationProfileTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsStationQosParamsTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsVlanIsolationTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsDot11RadioConfigTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsDot11QosProfileTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsDot11WlanCapabilityProfileTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsDot11WlanAuthenticationProfileTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsDot11WlanQosProfileTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsDot11RadioQosTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsDot11QAPTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsQAPProfileTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsDot11CapabilityMappingTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsDot11AuthMappingTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsDot11QosMappingTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsDot11ClientSummaryTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsDot11AntennasListTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsDot11WlanTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsDot11WlanBindTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsDot11nConfigTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsDot11nMCSDataRateTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsWtpImageUpgradeTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (WsscfgFsRrmConfigTableCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }
    if (APGroupDBCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (APGroupWLANDBCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    if (APGroupWTPDBCreate () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgUtlAddDefaultRBTree 
 Input       :  None
 Description :  This function adds all default RBTree required.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
WsscfgAddDefaultFsRrmConfigTable (VOID)
{
    tWsscfgFsRrmConfigEntry WsscfgFsRrmConfigEntry;
    tWsscfgIsSetFsRrmConfigEntry WsscfgIsSetFsRrmConfigEntry;

    MEMSET (&WsscfgFsRrmConfigEntry, 0, sizeof (tWsscfgFsRrmConfigEntry));
    MEMSET (&WsscfgIsSetFsRrmConfigEntry, 0,
            sizeof (tWsscfgIsSetFsRrmConfigEntry));

    /*To Add the Default Values for Type A */
    WsscfgFsRrmConfigEntry.MibObject.i4FsRrmRadioType = CLI_RADIO_TYPEA;
    WsscfgFsRrmConfigEntry.MibObject.i4FsRrmDcaMode = CLI_RRM_DCA_MODE_GLOBAL;
    WsscfgFsRrmConfigEntry.MibObject.i4FsRrmDcaChannelSelectionMode =
        CLI_RRM_DCA_CHANNEL_OFF;
    WsscfgFsRrmConfigEntry.MibObject.i4FsRrmTpcMode = CLI_RRM_DCA_MODE_GLOBAL;
    WsscfgFsRrmConfigEntry.MibObject.i4FsRrmTpcSelectionMode =
        CLI_RRM_DCA_CHANNEL_OFF;
    WsscfgFsRrmConfigEntry.MibObject.i4FsRrmRowStatus = CREATE_AND_GO;

    WsscfgIsSetFsRrmConfigEntry.bFsRrmRadioType = OSIX_TRUE;
    WsscfgIsSetFsRrmConfigEntry.bFsRrmDcaMode = OSIX_TRUE;
    WsscfgIsSetFsRrmConfigEntry.bFsRrmDcaChannelSelectionMode = OSIX_TRUE;
    WsscfgIsSetFsRrmConfigEntry.bFsRrmTpcMode = OSIX_TRUE;
    WsscfgIsSetFsRrmConfigEntry.bFsRrmTpcSelectionMode = OSIX_TRUE;
    WsscfgIsSetFsRrmConfigEntry.bFsRrmRowStatus = OSIX_TRUE;

    if (WsscfgSetAllFsRrmConfigTable
        (&WsscfgFsRrmConfigEntry, &WsscfgIsSetFsRrmConfigEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /*To Add the Default Values for Type B */
    WsscfgFsRrmConfigEntry.MibObject.i4FsRrmRadioType = CLI_RADIO_TYPEB;
    WsscfgFsRrmConfigEntry.MibObject.i4FsRrmDcaMode = CLI_RRM_DCA_MODE_GLOBAL;
    WsscfgFsRrmConfigEntry.MibObject.i4FsRrmDcaChannelSelectionMode =
        CLI_RRM_DCA_CHANNEL_OFF;
    WsscfgFsRrmConfigEntry.MibObject.i4FsRrmTpcMode = CLI_RRM_DCA_MODE_GLOBAL;
    WsscfgFsRrmConfigEntry.MibObject.i4FsRrmTpcSelectionMode =
        CLI_RRM_DCA_CHANNEL_OFF;
    WsscfgFsRrmConfigEntry.MibObject.i4FsRrmRowStatus = CREATE_AND_GO;

    WsscfgIsSetFsRrmConfigEntry.bFsRrmRadioType = OSIX_TRUE;
    WsscfgIsSetFsRrmConfigEntry.bFsRrmDcaMode = OSIX_TRUE;
    WsscfgIsSetFsRrmConfigEntry.bFsRrmDcaChannelSelectionMode = OSIX_TRUE;
    WsscfgIsSetFsRrmConfigEntry.bFsRrmTpcMode = OSIX_TRUE;
    WsscfgIsSetFsRrmConfigEntry.bFsRrmTpcSelectionMode = OSIX_TRUE;
    WsscfgIsSetFsRrmConfigEntry.bFsRrmRowStatus = OSIX_TRUE;

    if (WsscfgSetAllFsRrmConfigTable
        (&WsscfgFsRrmConfigEntry, &WsscfgIsSetFsRrmConfigEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /*To Add the Default Values for Type G */
    WsscfgFsRrmConfigEntry.MibObject.i4FsRrmRadioType = CLI_RADIO_TYPEG;
    WsscfgFsRrmConfigEntry.MibObject.i4FsRrmDcaMode = CLI_RRM_DCA_MODE_GLOBAL;
    WsscfgFsRrmConfigEntry.MibObject.i4FsRrmDcaChannelSelectionMode =
        CLI_RRM_DCA_CHANNEL_OFF;
    WsscfgFsRrmConfigEntry.MibObject.i4FsRrmTpcMode = CLI_RRM_DCA_MODE_GLOBAL;
    WsscfgFsRrmConfigEntry.MibObject.i4FsRrmTpcSelectionMode =
        CLI_RRM_DCA_CHANNEL_OFF;
    WsscfgFsRrmConfigEntry.MibObject.i4FsRrmRowStatus = CREATE_AND_GO;

    WsscfgIsSetFsRrmConfigEntry.bFsRrmRadioType = OSIX_TRUE;
    WsscfgIsSetFsRrmConfigEntry.bFsRrmDcaMode = OSIX_TRUE;
    WsscfgIsSetFsRrmConfigEntry.bFsRrmDcaChannelSelectionMode = OSIX_TRUE;
    WsscfgIsSetFsRrmConfigEntry.bFsRrmTpcMode = OSIX_TRUE;
    WsscfgIsSetFsRrmConfigEntry.bFsRrmTpcSelectionMode = OSIX_TRUE;
    WsscfgIsSetFsRrmConfigEntry.bFsRrmRowStatus = OSIX_TRUE;

    if (WsscfgSetAllFsRrmConfigTable
        (&WsscfgFsRrmConfigEntry, &WsscfgIsSetFsRrmConfigEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgUtlAddDefaultRBTree 
 Input       :  None
 Description :  This function adds all default RBTree required.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
WsscfgUtlAddDefaultRBTree (VOID)
{
    if (WsscfgAddDefaultFsRrmConfigTable () != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11StationConfigTableCreate
 Input       :  None
 Description :  This function creates the Dot11StationConfigTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11StationConfigTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11StationConfigEntry,
                       MibObject.Dot11StationConfigTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11StationConfigTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11StationConfigTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11AuthenticationAlgorithmsTableCreate
 Input       :  None
 Description :  This function creates the Dot11AuthenticationAlgorithmsTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11AuthenticationAlgorithmsTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11AuthenticationAlgorithmsEntry,
                       MibObject.Dot11AuthenticationAlgorithmsTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.
         Dot11AuthenticationAlgorithmsTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11AuthenticationAlgorithmsTableRBCmp))
        == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11WEPDefaultKeysTableCreate
 Input       :  None
 Description :  This function creates the Dot11WEPDefaultKeysTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11WEPDefaultKeysTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11WEPDefaultKeysEntry,
                       MibObject.Dot11WEPDefaultKeysTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11WEPDefaultKeysTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11WEPDefaultKeysTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11WEPKeyMappingsTableCreate
 Input       :  None
 Description :  This function creates the Dot11WEPKeyMappingsTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11WEPKeyMappingsTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11WEPKeyMappingsEntry,
                       MibObject.Dot11WEPKeyMappingsTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11WEPKeyMappingsTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11WEPKeyMappingsTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11PrivacyTableCreate
 Input       :  None
 Description :  This function creates the Dot11PrivacyTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11PrivacyTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11PrivacyEntry,
                       MibObject.Dot11PrivacyTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11PrivacyTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, Dot11PrivacyTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11MultiDomainCapabilityTableCreate
 Input       :  None
 Description :  This function creates the Dot11MultiDomainCapabilityTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11MultiDomainCapabilityTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11MultiDomainCapabilityEntry,
                       MibObject.Dot11MultiDomainCapabilityTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11MultiDomainCapabilityTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11MultiDomainCapabilityTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11SpectrumManagementTableCreate
 Input       :  None
 Description :  This function creates the Dot11SpectrumManagementTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11SpectrumManagementTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11SpectrumManagementEntry,
                       MibObject.Dot11SpectrumManagementTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11SpectrumManagementTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11SpectrumManagementTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11RegulatoryClassesTableCreate
 Input       :  None
 Description :  This function creates the Dot11RegulatoryClassesTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11RegulatoryClassesTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11RegulatoryClassesEntry,
                       MibObject.Dot11RegulatoryClassesTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11RegulatoryClassesTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11RegulatoryClassesTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11OperationTableCreate
 Input       :  None
 Description :  This function creates the Dot11OperationTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11OperationTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11OperationEntry,
                       MibObject.Dot11OperationTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11OperationTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11OperationTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11CountersTableCreate
 Input       :  None
 Description :  This function creates the Dot11CountersTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11CountersTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11CountersEntry,
                       MibObject.Dot11CountersTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11CountersTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11CountersTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11GroupAddressesTableCreate
 Input       :  None
 Description :  This function creates the Dot11GroupAddressesTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11GroupAddressesTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11GroupAddressesEntry,
                       MibObject.Dot11GroupAddressesTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11GroupAddressesTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11GroupAddressesTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11EDCATableCreate
 Input       :  None
 Description :  This function creates the Dot11EDCATable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11EDCATableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11EDCAEntry, MibObject.Dot11EDCATableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11EDCATable =
         RBTreeCreateEmbedded (u4RBNodeOffset, Dot11EDCATableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11QAPEDCATableCreate
 Input       :  None
 Description :  This function creates the Dot11QAPEDCATable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11QAPEDCATableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11QAPEDCAEntry,
                       MibObject.Dot11QAPEDCATableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11QAPEDCATable =
         RBTreeCreateEmbedded (u4RBNodeOffset, Dot11QAPEDCATableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11QosCountersTableCreate
 Input       :  None
 Description :  This function creates the Dot11QosCountersTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11QosCountersTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11QosCountersEntry,
                       MibObject.Dot11QosCountersTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11QosCountersTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11QosCountersTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11ResourceInfoTableCreate
 Input       :  None
 Description :  This function creates the Dot11ResourceInfoTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11ResourceInfoTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11ResourceInfoEntry,
                       MibObject.Dot11ResourceInfoTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11ResourceInfoTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11ResourceInfoTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11PhyOperationTableCreate
 Input       :  None
 Description :  This function creates the Dot11PhyOperationTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11PhyOperationTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11PhyOperationEntry,
                       MibObject.Dot11PhyOperationTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11PhyOperationTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11PhyOperationTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11PhyAntennaTableCreate
 Input       :  None
 Description :  This function creates the Dot11PhyAntennaTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11PhyAntennaTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11PhyAntennaEntry,
                       MibObject.Dot11PhyAntennaTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11PhyAntennaTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11PhyAntennaTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11PhyTxPowerTableCreate
 Input       :  None
 Description :  This function creates the Dot11PhyTxPowerTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11PhyTxPowerTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11PhyTxPowerEntry,
                       MibObject.Dot11PhyTxPowerTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11PhyTxPowerTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11PhyTxPowerTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11PhyFHSSTableCreate
 Input       :  None
 Description :  This function creates the Dot11PhyFHSSTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11PhyFHSSTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11PhyFHSSEntry,
                       MibObject.Dot11PhyFHSSTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11PhyFHSSTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, Dot11PhyFHSSTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11PhyDSSSTableCreate
 Input       :  None
 Description :  This function creates the Dot11PhyDSSSTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11PhyDSSSTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11PhyDSSSEntry,
                       MibObject.Dot11PhyDSSSTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11PhyDSSSTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, Dot11PhyDSSSTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11PhyIRTableCreate
 Input       :  None
 Description :  This function creates the Dot11PhyIRTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11PhyIRTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11PhyIREntry, MibObject.Dot11PhyIRTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11PhyIRTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, Dot11PhyIRTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11RegDomainsSupportedTableCreate
 Input       :  None
 Description :  This function creates the Dot11RegDomainsSupportedTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11RegDomainsSupportedTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11RegDomainsSupportedEntry,
                       MibObject.Dot11RegDomainsSupportedTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11RegDomainsSupportedTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11RegDomainsSupportedTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11AntennasListTableCreate
 Input       :  None
 Description :  This function creates the Dot11AntennasListTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11AntennasListTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11AntennasListEntry,
                       MibObject.Dot11AntennasListTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11AntennasListTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11AntennasListTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11SupportedDataRatesTxTableCreate
 Input       :  None
 Description :  This function creates the Dot11SupportedDataRatesTxTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11SupportedDataRatesTxTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11SupportedDataRatesTxEntry,
                       MibObject.Dot11SupportedDataRatesTxTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11SupportedDataRatesTxTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11SupportedDataRatesTxTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11SupportedDataRatesRxTableCreate
 Input       :  None
 Description :  This function creates the Dot11SupportedDataRatesRxTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11SupportedDataRatesRxTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11SupportedDataRatesRxEntry,
                       MibObject.Dot11SupportedDataRatesRxTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11SupportedDataRatesRxTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11SupportedDataRatesRxTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11PhyOFDMTableCreate
 Input       :  None
 Description :  This function creates the Dot11PhyOFDMTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11PhyOFDMTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11PhyOFDMEntry,
                       MibObject.Dot11PhyOFDMTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11PhyOFDMTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, Dot11PhyOFDMTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11PhyHRDSSSTableCreate
 Input       :  None
 Description :  This function creates the Dot11PhyHRDSSSTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11PhyHRDSSSTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11PhyHRDSSSEntry,
                       MibObject.Dot11PhyHRDSSSTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11PhyHRDSSSTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11PhyHRDSSSTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11HoppingPatternTableCreate
 Input       :  None
 Description :  This function creates the Dot11HoppingPatternTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11HoppingPatternTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11HoppingPatternEntry,
                       MibObject.Dot11HoppingPatternTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11HoppingPatternTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11HoppingPatternTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11PhyERPTableCreate
 Input       :  None
 Description :  This function creates the Dot11PhyERPTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11PhyERPTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11PhyERPEntry, MibObject.Dot11PhyERPTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11PhyERPTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, Dot11PhyERPTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11RSNAConfigTableCreate
 Input       :  None
 Description :  This function creates the Dot11RSNAConfigTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11RSNAConfigTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11RSNAConfigEntry,
                       MibObject.Dot11RSNAConfigTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11RSNAConfigTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11RSNAConfigTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11RSNAConfigPairwiseCiphersTableCreate
 Input       :  None
 Description :  This function creates the Dot11RSNAConfigPairwiseCiphersTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11RSNAConfigPairwiseCiphersTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11RSNAConfigPairwiseCiphersEntry,
                       MibObject.Dot11RSNAConfigPairwiseCiphersTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.
         Dot11RSNAConfigPairwiseCiphersTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11RSNAConfigPairwiseCiphersTableRBCmp))
        == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11RSNAConfigAuthenticationSuitesTableCreate
 Input       :  None
 Description :  This function creates the Dot11RSNAConfigAuthenticationSuitesTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11RSNAConfigAuthenticationSuitesTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry,
                       MibObject.Dot11RSNAConfigAuthenticationSuitesTableNode);

    if ((gWsscfgGlobals.
         WsscfgGlbMib.Dot11RSNAConfigAuthenticationSuitesTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11RSNAConfigAuthenticationSuitesTableRBCmp))
        == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11RSNAStatsTableCreate
 Input       :  None
 Description :  This function creates the Dot11RSNAStatsTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgDot11RSNAStatsTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgDot11RSNAStatsEntry,
                       MibObject.Dot11RSNAStatsTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.Dot11RSNAStatsTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               Dot11RSNAStatsTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  Dot11StationConfigTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11StationConfigTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11StationConfigTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11StationConfigEntry *pDot11StationConfigEntry1 =
        (tWsscfgDot11StationConfigEntry *) pRBElem1;
    tWsscfgDot11StationConfigEntry *pDot11StationConfigEntry2 =
        (tWsscfgDot11StationConfigEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11StationConfigEntry1->MibObject.i4IfIndex >
        pDot11StationConfigEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11StationConfigEntry1->MibObject.i4IfIndex <
             pDot11StationConfigEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11AuthenticationAlgorithmsTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11AuthenticationAlgorithmsTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11AuthenticationAlgorithmsTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11AuthenticationAlgorithmsEntry
        * pDot11AuthenticationAlgorithmsEntry1 =
        (tWsscfgDot11AuthenticationAlgorithmsEntry *) pRBElem1;
    tWsscfgDot11AuthenticationAlgorithmsEntry
        * pDot11AuthenticationAlgorithmsEntry2 =
        (tWsscfgDot11AuthenticationAlgorithmsEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11AuthenticationAlgorithmsEntry1->
        MibObject.i4Dot11AuthenticationAlgorithmsIndex >
        pDot11AuthenticationAlgorithmsEntry2->
        MibObject.i4Dot11AuthenticationAlgorithmsIndex)
    {
        return 1;
    }
    else if (pDot11AuthenticationAlgorithmsEntry1->
             MibObject.i4Dot11AuthenticationAlgorithmsIndex <
             pDot11AuthenticationAlgorithmsEntry2->
             MibObject.i4Dot11AuthenticationAlgorithmsIndex)
    {
        return -1;
    }

    if (pDot11AuthenticationAlgorithmsEntry1->MibObject.i4IfIndex >
        pDot11AuthenticationAlgorithmsEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11AuthenticationAlgorithmsEntry1->MibObject.
             i4IfIndex <
             pDot11AuthenticationAlgorithmsEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11WEPDefaultKeysTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11WEPDefaultKeysTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11WEPDefaultKeysTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11WEPDefaultKeysEntry *pDot11WEPDefaultKeysEntry1 =
        (tWsscfgDot11WEPDefaultKeysEntry *) pRBElem1;
    tWsscfgDot11WEPDefaultKeysEntry *pDot11WEPDefaultKeysEntry2 =
        (tWsscfgDot11WEPDefaultKeysEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11WEPDefaultKeysEntry1->MibObject.
        i4Dot11WEPDefaultKeyIndex >
        pDot11WEPDefaultKeysEntry2->MibObject.i4Dot11WEPDefaultKeyIndex)
    {
        return 1;
    }
    else if (pDot11WEPDefaultKeysEntry1->
             MibObject.i4Dot11WEPDefaultKeyIndex <
             pDot11WEPDefaultKeysEntry2->MibObject.i4Dot11WEPDefaultKeyIndex)
    {
        return -1;
    }

    if (pDot11WEPDefaultKeysEntry1->MibObject.i4IfIndex >
        pDot11WEPDefaultKeysEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11WEPDefaultKeysEntry1->MibObject.i4IfIndex <
             pDot11WEPDefaultKeysEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11WEPKeyMappingsTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11WEPKeyMappingsTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11WEPKeyMappingsTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11WEPKeyMappingsEntry *pDot11WEPKeyMappingsEntry1 =
        (tWsscfgDot11WEPKeyMappingsEntry *) pRBElem1;
    tWsscfgDot11WEPKeyMappingsEntry *pDot11WEPKeyMappingsEntry2 =
        (tWsscfgDot11WEPKeyMappingsEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11WEPKeyMappingsEntry1->MibObject.
        i4Dot11WEPKeyMappingIndex >
        pDot11WEPKeyMappingsEntry2->MibObject.i4Dot11WEPKeyMappingIndex)
    {
        return 1;
    }
    else if (pDot11WEPKeyMappingsEntry1->
             MibObject.i4Dot11WEPKeyMappingIndex <
             pDot11WEPKeyMappingsEntry2->MibObject.i4Dot11WEPKeyMappingIndex)
    {
        return -1;
    }

    if (pDot11WEPKeyMappingsEntry1->MibObject.i4IfIndex >
        pDot11WEPKeyMappingsEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11WEPKeyMappingsEntry1->MibObject.i4IfIndex <
             pDot11WEPKeyMappingsEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11PrivacyTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11PrivacyTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11PrivacyTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11PrivacyEntry *pDot11PrivacyEntry1 =
        (tWsscfgDot11PrivacyEntry *) pRBElem1;
    tWsscfgDot11PrivacyEntry *pDot11PrivacyEntry2 =
        (tWsscfgDot11PrivacyEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11PrivacyEntry1->MibObject.i4IfIndex >
        pDot11PrivacyEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11PrivacyEntry1->MibObject.i4IfIndex <
             pDot11PrivacyEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11MultiDomainCapabilityTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11MultiDomainCapabilityTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11MultiDomainCapabilityTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11MultiDomainCapabilityEntry
        * pDot11MultiDomainCapabilityEntry1 =
        (tWsscfgDot11MultiDomainCapabilityEntry *) pRBElem1;
    tWsscfgDot11MultiDomainCapabilityEntry
        * pDot11MultiDomainCapabilityEntry2 =
        (tWsscfgDot11MultiDomainCapabilityEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11MultiDomainCapabilityEntry1->
        MibObject.i4Dot11MultiDomainCapabilityIndex >
        pDot11MultiDomainCapabilityEntry2->
        MibObject.i4Dot11MultiDomainCapabilityIndex)
    {
        return 1;
    }
    else if (pDot11MultiDomainCapabilityEntry1->
             MibObject.i4Dot11MultiDomainCapabilityIndex <
             pDot11MultiDomainCapabilityEntry2->
             MibObject.i4Dot11MultiDomainCapabilityIndex)
    {
        return -1;
    }

    if (pDot11MultiDomainCapabilityEntry1->MibObject.i4IfIndex >
        pDot11MultiDomainCapabilityEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11MultiDomainCapabilityEntry1->MibObject.
             i4IfIndex < pDot11MultiDomainCapabilityEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11SpectrumManagementTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11SpectrumManagementTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11SpectrumManagementTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11SpectrumManagementEntry
        * pDot11SpectrumManagementEntry1 =
        (tWsscfgDot11SpectrumManagementEntry *) pRBElem1;
    tWsscfgDot11SpectrumManagementEntry
        * pDot11SpectrumManagementEntry2 =
        (tWsscfgDot11SpectrumManagementEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11SpectrumManagementEntry1->
        MibObject.i4Dot11SpectrumManagementIndex >
        pDot11SpectrumManagementEntry2->
        MibObject.i4Dot11SpectrumManagementIndex)
    {
        return 1;
    }
    else if (pDot11SpectrumManagementEntry1->
             MibObject.i4Dot11SpectrumManagementIndex <
             pDot11SpectrumManagementEntry2->
             MibObject.i4Dot11SpectrumManagementIndex)
    {
        return -1;
    }

    if (pDot11SpectrumManagementEntry1->MibObject.i4IfIndex >
        pDot11SpectrumManagementEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11SpectrumManagementEntry1->MibObject.i4IfIndex <
             pDot11SpectrumManagementEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11RegulatoryClassesTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11RegulatoryClassesTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11RegulatoryClassesTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11RegulatoryClassesEntry *pDot11RegulatoryClassesEntry1
        = (tWsscfgDot11RegulatoryClassesEntry *) pRBElem1;
    tWsscfgDot11RegulatoryClassesEntry *pDot11RegulatoryClassesEntry2
        = (tWsscfgDot11RegulatoryClassesEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11RegulatoryClassesEntry1->
        MibObject.i4Dot11RegulatoryClassesIndex >
        pDot11RegulatoryClassesEntry2->MibObject.i4Dot11RegulatoryClassesIndex)
    {
        return 1;
    }
    else if (pDot11RegulatoryClassesEntry1->
             MibObject.i4Dot11RegulatoryClassesIndex <
             pDot11RegulatoryClassesEntry2->
             MibObject.i4Dot11RegulatoryClassesIndex)
    {
        return -1;
    }

    if (pDot11RegulatoryClassesEntry1->MibObject.i4IfIndex >
        pDot11RegulatoryClassesEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11RegulatoryClassesEntry1->MibObject.i4IfIndex <
             pDot11RegulatoryClassesEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11OperationTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11OperationTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11OperationTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11OperationEntry *pDot11OperationEntry1 =
        (tWsscfgDot11OperationEntry *) pRBElem1;
    tWsscfgDot11OperationEntry *pDot11OperationEntry2 =
        (tWsscfgDot11OperationEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11OperationEntry1->MibObject.i4IfIndex >
        pDot11OperationEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11OperationEntry1->MibObject.i4IfIndex <
             pDot11OperationEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11CountersTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11CountersTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11CountersTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11CountersEntry *pDot11CountersEntry1 =
        (tWsscfgDot11CountersEntry *) pRBElem1;
    tWsscfgDot11CountersEntry *pDot11CountersEntry2 =
        (tWsscfgDot11CountersEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11CountersEntry1->MibObject.i4IfIndex >
        pDot11CountersEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11CountersEntry1->MibObject.i4IfIndex <
             pDot11CountersEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11GroupAddressesTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11GroupAddressesTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11GroupAddressesTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11GroupAddressesEntry *pDot11GroupAddressesEntry1 =
        (tWsscfgDot11GroupAddressesEntry *) pRBElem1;
    tWsscfgDot11GroupAddressesEntry *pDot11GroupAddressesEntry2 =
        (tWsscfgDot11GroupAddressesEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11GroupAddressesEntry1->MibObject.
        i4Dot11GroupAddressesIndex >
        pDot11GroupAddressesEntry2->MibObject.i4Dot11GroupAddressesIndex)
    {
        return 1;
    }
    else if (pDot11GroupAddressesEntry1->
             MibObject.i4Dot11GroupAddressesIndex <
             pDot11GroupAddressesEntry2->MibObject.i4Dot11GroupAddressesIndex)
    {
        return -1;
    }

    if (pDot11GroupAddressesEntry1->MibObject.i4IfIndex >
        pDot11GroupAddressesEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11GroupAddressesEntry1->MibObject.i4IfIndex <
             pDot11GroupAddressesEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11EDCATableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11EDCATable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11EDCATableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11EDCAEntry *pDot11EDCAEntry1 =
        (tWsscfgDot11EDCAEntry *) pRBElem1;
    tWsscfgDot11EDCAEntry *pDot11EDCAEntry2 =
        (tWsscfgDot11EDCAEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11EDCAEntry1->MibObject.i4Dot11EDCATableIndex >
        pDot11EDCAEntry2->MibObject.i4Dot11EDCATableIndex)
    {
        return 1;
    }
    else if (pDot11EDCAEntry1->MibObject.i4Dot11EDCATableIndex <
             pDot11EDCAEntry2->MibObject.i4Dot11EDCATableIndex)
    {
        return -1;
    }

    if (pDot11EDCAEntry1->MibObject.i4IfIndex >
        pDot11EDCAEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11EDCAEntry1->MibObject.i4IfIndex <
             pDot11EDCAEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11QAPEDCATableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11QAPEDCATable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11QAPEDCATableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11QAPEDCAEntry *pDot11QAPEDCAEntry1 =
        (tWsscfgDot11QAPEDCAEntry *) pRBElem1;
    tWsscfgDot11QAPEDCAEntry *pDot11QAPEDCAEntry2 =
        (tWsscfgDot11QAPEDCAEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11QAPEDCAEntry1->MibObject.i4Dot11QAPEDCATableIndex >
        pDot11QAPEDCAEntry2->MibObject.i4Dot11QAPEDCATableIndex)
    {
        return 1;
    }
    else if (pDot11QAPEDCAEntry1->MibObject.
             i4Dot11QAPEDCATableIndex <
             pDot11QAPEDCAEntry2->MibObject.i4Dot11QAPEDCATableIndex)
    {
        return -1;
    }

    if (pDot11QAPEDCAEntry1->MibObject.i4IfIndex >
        pDot11QAPEDCAEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11QAPEDCAEntry1->MibObject.i4IfIndex <
             pDot11QAPEDCAEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11QosCountersTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11QosCountersTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11QosCountersTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11QosCountersEntry *pDot11QosCountersEntry1 =
        (tWsscfgDot11QosCountersEntry *) pRBElem1;
    tWsscfgDot11QosCountersEntry *pDot11QosCountersEntry2 =
        (tWsscfgDot11QosCountersEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11QosCountersEntry1->MibObject.i4Dot11QosCountersIndex >
        pDot11QosCountersEntry2->MibObject.i4Dot11QosCountersIndex)
    {
        return 1;
    }
    else if (pDot11QosCountersEntry1->MibObject.
             i4Dot11QosCountersIndex <
             pDot11QosCountersEntry2->MibObject.i4Dot11QosCountersIndex)
    {
        return -1;
    }

    if (pDot11QosCountersEntry1->MibObject.i4IfIndex >
        pDot11QosCountersEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11QosCountersEntry1->MibObject.i4IfIndex <
             pDot11QosCountersEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11ResourceInfoTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11ResourceInfoTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11ResourceInfoTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11ResourceInfoEntry *pDot11ResourceInfoEntry1 =
        (tWsscfgDot11ResourceInfoEntry *) pRBElem1;
    tWsscfgDot11ResourceInfoEntry *pDot11ResourceInfoEntry2 =
        (tWsscfgDot11ResourceInfoEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11ResourceInfoEntry1->MibObject.i4IfIndex >
        pDot11ResourceInfoEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11ResourceInfoEntry1->MibObject.i4IfIndex <
             pDot11ResourceInfoEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11PhyOperationTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11PhyOperationTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11PhyOperationTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11PhyOperationEntry *pDot11PhyOperationEntry1 =
        (tWsscfgDot11PhyOperationEntry *) pRBElem1;
    tWsscfgDot11PhyOperationEntry *pDot11PhyOperationEntry2 =
        (tWsscfgDot11PhyOperationEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11PhyOperationEntry1->MibObject.i4IfIndex >
        pDot11PhyOperationEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11PhyOperationEntry1->MibObject.i4IfIndex <
             pDot11PhyOperationEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11PhyAntennaTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11PhyAntennaTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11PhyAntennaTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11PhyAntennaEntry *pDot11PhyAntennaEntry1 =
        (tWsscfgDot11PhyAntennaEntry *) pRBElem1;
    tWsscfgDot11PhyAntennaEntry *pDot11PhyAntennaEntry2 =
        (tWsscfgDot11PhyAntennaEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11PhyAntennaEntry1->MibObject.i4IfIndex >
        pDot11PhyAntennaEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11PhyAntennaEntry1->MibObject.i4IfIndex <
             pDot11PhyAntennaEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11PhyTxPowerTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11PhyTxPowerTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11PhyTxPowerTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11PhyTxPowerEntry *pDot11PhyTxPowerEntry1 =
        (tWsscfgDot11PhyTxPowerEntry *) pRBElem1;
    tWsscfgDot11PhyTxPowerEntry *pDot11PhyTxPowerEntry2 =
        (tWsscfgDot11PhyTxPowerEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11PhyTxPowerEntry1->MibObject.i4IfIndex >
        pDot11PhyTxPowerEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11PhyTxPowerEntry1->MibObject.i4IfIndex <
             pDot11PhyTxPowerEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11PhyFHSSTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11PhyFHSSTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11PhyFHSSTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11PhyFHSSEntry *pDot11PhyFHSSEntry1 =
        (tWsscfgDot11PhyFHSSEntry *) pRBElem1;
    tWsscfgDot11PhyFHSSEntry *pDot11PhyFHSSEntry2 =
        (tWsscfgDot11PhyFHSSEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11PhyFHSSEntry1->MibObject.i4IfIndex >
        pDot11PhyFHSSEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11PhyFHSSEntry1->MibObject.i4IfIndex <
             pDot11PhyFHSSEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11PhyDSSSTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11PhyDSSSTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11PhyDSSSTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11PhyDSSSEntry *pDot11PhyDSSSEntry1 =
        (tWsscfgDot11PhyDSSSEntry *) pRBElem1;
    tWsscfgDot11PhyDSSSEntry *pDot11PhyDSSSEntry2 =
        (tWsscfgDot11PhyDSSSEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11PhyDSSSEntry1->MibObject.i4IfIndex >
        pDot11PhyDSSSEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11PhyDSSSEntry1->MibObject.i4IfIndex <
             pDot11PhyDSSSEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11PhyIRTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11PhyIRTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11PhyIRTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11PhyIREntry *pDot11PhyIREntry1 =
        (tWsscfgDot11PhyIREntry *) pRBElem1;
    tWsscfgDot11PhyIREntry *pDot11PhyIREntry2 =
        (tWsscfgDot11PhyIREntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11PhyIREntry1->MibObject.i4IfIndex >
        pDot11PhyIREntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11PhyIREntry1->MibObject.i4IfIndex <
             pDot11PhyIREntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11RegDomainsSupportedTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11RegDomainsSupportedTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11RegDomainsSupportedTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11RegDomainsSupportedEntry
        * pDot11RegDomainsSupportedEntry1 =
        (tWsscfgDot11RegDomainsSupportedEntry *) pRBElem1;
    tWsscfgDot11RegDomainsSupportedEntry
        * pDot11RegDomainsSupportedEntry2 =
        (tWsscfgDot11RegDomainsSupportedEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11RegDomainsSupportedEntry1->
        MibObject.i4Dot11RegDomainsSupportedIndex >
        pDot11RegDomainsSupportedEntry2->
        MibObject.i4Dot11RegDomainsSupportedIndex)
    {
        return 1;
    }
    else if (pDot11RegDomainsSupportedEntry1->
             MibObject.i4Dot11RegDomainsSupportedIndex <
             pDot11RegDomainsSupportedEntry2->
             MibObject.i4Dot11RegDomainsSupportedIndex)
    {
        return -1;
    }

    if (pDot11RegDomainsSupportedEntry1->MibObject.i4IfIndex >
        pDot11RegDomainsSupportedEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11RegDomainsSupportedEntry1->MibObject.i4IfIndex <
             pDot11RegDomainsSupportedEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11AntennasListTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11AntennasListTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11AntennasListTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11AntennasListEntry *pDot11AntennasListEntry1 =
        (tWsscfgDot11AntennasListEntry *) pRBElem1;
    tWsscfgDot11AntennasListEntry *pDot11AntennasListEntry2 =
        (tWsscfgDot11AntennasListEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11AntennasListEntry1->MibObject.i4Dot11AntennaListIndex >
        pDot11AntennasListEntry2->MibObject.i4Dot11AntennaListIndex)
    {
        return 1;
    }
    else if (pDot11AntennasListEntry1->
             MibObject.i4Dot11AntennaListIndex <
             pDot11AntennasListEntry2->MibObject.i4Dot11AntennaListIndex)
    {
        return -1;
    }

    if (pDot11AntennasListEntry1->MibObject.i4IfIndex >
        pDot11AntennasListEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11AntennasListEntry1->MibObject.i4IfIndex <
             pDot11AntennasListEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11SupportedDataRatesTxTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11SupportedDataRatesTxTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11SupportedDataRatesTxTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11SupportedDataRatesTxEntry
        * pDot11SupportedDataRatesTxEntry1 =
        (tWsscfgDot11SupportedDataRatesTxEntry *) pRBElem1;
    tWsscfgDot11SupportedDataRatesTxEntry
        * pDot11SupportedDataRatesTxEntry2 =
        (tWsscfgDot11SupportedDataRatesTxEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11SupportedDataRatesTxEntry1->
        MibObject.i4Dot11SupportedDataRatesTxIndex >
        pDot11SupportedDataRatesTxEntry2->
        MibObject.i4Dot11SupportedDataRatesTxIndex)
    {
        return 1;
    }
    else if (pDot11SupportedDataRatesTxEntry1->
             MibObject.i4Dot11SupportedDataRatesTxIndex <
             pDot11SupportedDataRatesTxEntry2->
             MibObject.i4Dot11SupportedDataRatesTxIndex)
    {
        return -1;
    }

    if (pDot11SupportedDataRatesTxEntry1->MibObject.i4IfIndex >
        pDot11SupportedDataRatesTxEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11SupportedDataRatesTxEntry1->MibObject.i4IfIndex <
             pDot11SupportedDataRatesTxEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11SupportedDataRatesRxTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11SupportedDataRatesRxTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11SupportedDataRatesRxTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11SupportedDataRatesRxEntry
        * pDot11SupportedDataRatesRxEntry1 =
        (tWsscfgDot11SupportedDataRatesRxEntry *) pRBElem1;
    tWsscfgDot11SupportedDataRatesRxEntry
        * pDot11SupportedDataRatesRxEntry2 =
        (tWsscfgDot11SupportedDataRatesRxEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11SupportedDataRatesRxEntry1->
        MibObject.i4Dot11SupportedDataRatesRxIndex >
        pDot11SupportedDataRatesRxEntry2->
        MibObject.i4Dot11SupportedDataRatesRxIndex)
    {
        return 1;
    }
    else if (pDot11SupportedDataRatesRxEntry1->
             MibObject.i4Dot11SupportedDataRatesRxIndex <
             pDot11SupportedDataRatesRxEntry2->
             MibObject.i4Dot11SupportedDataRatesRxIndex)
    {
        return -1;
    }

    if (pDot11SupportedDataRatesRxEntry1->MibObject.i4IfIndex >
        pDot11SupportedDataRatesRxEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11SupportedDataRatesRxEntry1->MibObject.i4IfIndex <
             pDot11SupportedDataRatesRxEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11PhyOFDMTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11PhyOFDMTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11PhyOFDMTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11PhyOFDMEntry *pDot11PhyOFDMEntry1 =
        (tWsscfgDot11PhyOFDMEntry *) pRBElem1;
    tWsscfgDot11PhyOFDMEntry *pDot11PhyOFDMEntry2 =
        (tWsscfgDot11PhyOFDMEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11PhyOFDMEntry1->MibObject.i4IfIndex >
        pDot11PhyOFDMEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11PhyOFDMEntry1->MibObject.i4IfIndex <
             pDot11PhyOFDMEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11PhyHRDSSSTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11PhyHRDSSSTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11PhyHRDSSSTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11PhyHRDSSSEntry *pDot11PhyHRDSSSEntry1 =
        (tWsscfgDot11PhyHRDSSSEntry *) pRBElem1;
    tWsscfgDot11PhyHRDSSSEntry *pDot11PhyHRDSSSEntry2 =
        (tWsscfgDot11PhyHRDSSSEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11PhyHRDSSSEntry1->MibObject.i4IfIndex >
        pDot11PhyHRDSSSEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11PhyHRDSSSEntry1->MibObject.i4IfIndex <
             pDot11PhyHRDSSSEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11HoppingPatternTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11HoppingPatternTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11HoppingPatternTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11HoppingPatternEntry *pDot11HoppingPatternEntry1 =
        (tWsscfgDot11HoppingPatternEntry *) pRBElem1;
    tWsscfgDot11HoppingPatternEntry *pDot11HoppingPatternEntry2 =
        (tWsscfgDot11HoppingPatternEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11HoppingPatternEntry1->MibObject.
        i4Dot11HoppingPatternIndex >
        pDot11HoppingPatternEntry2->MibObject.i4Dot11HoppingPatternIndex)
    {
        return 1;
    }
    else if (pDot11HoppingPatternEntry1->
             MibObject.i4Dot11HoppingPatternIndex <
             pDot11HoppingPatternEntry2->MibObject.i4Dot11HoppingPatternIndex)
    {
        return -1;
    }

    if (pDot11HoppingPatternEntry1->MibObject.i4IfIndex >
        pDot11HoppingPatternEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11HoppingPatternEntry1->MibObject.i4IfIndex <
             pDot11HoppingPatternEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11PhyERPTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11PhyERPTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11PhyERPTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11PhyERPEntry *pDot11PhyERPEntry1 =
        (tWsscfgDot11PhyERPEntry *) pRBElem1;
    tWsscfgDot11PhyERPEntry *pDot11PhyERPEntry2 =
        (tWsscfgDot11PhyERPEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11PhyERPEntry1->MibObject.i4IfIndex >
        pDot11PhyERPEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11PhyERPEntry1->MibObject.i4IfIndex <
             pDot11PhyERPEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11RSNAConfigTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11RSNAConfigTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11RSNAConfigTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11RSNAConfigEntry *pDot11RSNAConfigEntry1 =
        (tWsscfgDot11RSNAConfigEntry *) pRBElem1;
    tWsscfgDot11RSNAConfigEntry *pDot11RSNAConfigEntry2 =
        (tWsscfgDot11RSNAConfigEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11RSNAConfigEntry1->MibObject.i4IfIndex >
        pDot11RSNAConfigEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11RSNAConfigEntry1->MibObject.i4IfIndex <
             pDot11RSNAConfigEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11RSNAConfigPairwiseCiphersTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11RSNAConfigPairwiseCiphersTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11RSNAConfigPairwiseCiphersTableRBCmp (tRBElem * pRBElem1,
                                          tRBElem * pRBElem2)
{

    tWsscfgDot11RSNAConfigPairwiseCiphersEntry
        * pDot11RSNAConfigPairwiseCiphersEntry1 =
        (tWsscfgDot11RSNAConfigPairwiseCiphersEntry *) pRBElem1;
    tWsscfgDot11RSNAConfigPairwiseCiphersEntry
        * pDot11RSNAConfigPairwiseCiphersEntry2 =
        (tWsscfgDot11RSNAConfigPairwiseCiphersEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11RSNAConfigPairwiseCiphersEntry1->
        MibObject.u4Dot11RSNAConfigPairwiseCipherIndex >
        pDot11RSNAConfigPairwiseCiphersEntry2->
        MibObject.u4Dot11RSNAConfigPairwiseCipherIndex)
    {
        return 1;
    }
    else if (pDot11RSNAConfigPairwiseCiphersEntry1->
             MibObject.u4Dot11RSNAConfigPairwiseCipherIndex <
             pDot11RSNAConfigPairwiseCiphersEntry2->
             MibObject.u4Dot11RSNAConfigPairwiseCipherIndex)
    {
        return -1;
    }

    if (pDot11RSNAConfigPairwiseCiphersEntry1->MibObject.i4IfIndex >
        pDot11RSNAConfigPairwiseCiphersEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11RSNAConfigPairwiseCiphersEntry1->MibObject.
             i4IfIndex <
             pDot11RSNAConfigPairwiseCiphersEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11RSNAConfigAuthenticationSuitesTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11RSNAConfigAuthenticationSuitesTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11RSNAConfigAuthenticationSuitesTableRBCmp (tRBElem * pRBElem1,
                                               tRBElem * pRBElem2)
{

    tWsscfgDot11RSNAConfigAuthenticationSuitesEntry
        * pDot11RSNAConfigAuthenticationSuitesEntry1 =
        (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *) pRBElem1;
    tWsscfgDot11RSNAConfigAuthenticationSuitesEntry
        * pDot11RSNAConfigAuthenticationSuitesEntry2 =
        (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11RSNAConfigAuthenticationSuitesEntry1->
        MibObject.u4Dot11RSNAConfigAuthenticationSuiteIndex >
        pDot11RSNAConfigAuthenticationSuitesEntry2->
        MibObject.u4Dot11RSNAConfigAuthenticationSuiteIndex)
    {
        return 1;
    }
    else if (pDot11RSNAConfigAuthenticationSuitesEntry1->
             MibObject.u4Dot11RSNAConfigAuthenticationSuiteIndex <
             pDot11RSNAConfigAuthenticationSuitesEntry2->
             MibObject.u4Dot11RSNAConfigAuthenticationSuiteIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11RSNAStatsTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                Dot11RSNAStatsTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
Dot11RSNAStatsTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgDot11RSNAStatsEntry *pDot11RSNAStatsEntry1 =
        (tWsscfgDot11RSNAStatsEntry *) pRBElem1;
    tWsscfgDot11RSNAStatsEntry *pDot11RSNAStatsEntry2 =
        (tWsscfgDot11RSNAStatsEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pDot11RSNAStatsEntry1->MibObject.u4Dot11RSNAStatsIndex >
        pDot11RSNAStatsEntry2->MibObject.u4Dot11RSNAStatsIndex)
    {
        return 1;
    }
    else if (pDot11RSNAStatsEntry1->MibObject.
             u4Dot11RSNAStatsIndex <
             pDot11RSNAStatsEntry2->MibObject.u4Dot11RSNAStatsIndex)
    {
        return -1;
    }

    if (pDot11RSNAStatsEntry1->MibObject.i4IfIndex >
        pDot11RSNAStatsEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pDot11RSNAStatsEntry1->MibObject.i4IfIndex <
             pDot11RSNAStatsEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  Dot11StationConfigTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11StationConfigEntry
                pWsscfgSetDot11StationConfigEntry
                pWsscfgIsSetDot11StationConfigEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
Dot11StationConfigTableFilterInputs (tWsscfgDot11StationConfigEntry *
                                     pWsscfgDot11StationConfigEntry,
                                     tWsscfgDot11StationConfigEntry *
                                     pWsscfgSetDot11StationConfigEntry,
                                     tWsscfgIsSetDot11StationConfigEntry *
                                     pWsscfgIsSetDot11StationConfigEntry)
{
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11StationID == OSIX_TRUE)
    {
        if (MEMCMP
            (&
             (pWsscfgDot11StationConfigEntry->MibObject.
              Dot11StationID),
             &(pWsscfgSetDot11StationConfigEntry->
               MibObject.Dot11StationID), 6) == 0)
            pWsscfgIsSetDot11StationConfigEntry->bDot11StationID = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11StationConfigEntry->
        bDot11MediumOccupancyLimit == OSIX_TRUE)
    {
        if (pWsscfgDot11StationConfigEntry->
            MibObject.i4Dot11MediumOccupancyLimit ==
            pWsscfgSetDot11StationConfigEntry->
            MibObject.i4Dot11MediumOccupancyLimit)
            pWsscfgIsSetDot11StationConfigEntry->bDot11MediumOccupancyLimit
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11CFPPeriod == OSIX_TRUE)
    {
        if (pWsscfgDot11StationConfigEntry->MibObject.
            i4Dot11CFPPeriod ==
            pWsscfgSetDot11StationConfigEntry->MibObject.i4Dot11CFPPeriod)
            pWsscfgIsSetDot11StationConfigEntry->bDot11CFPPeriod = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11CFPMaxDuration == OSIX_TRUE)
    {
        if (pWsscfgDot11StationConfigEntry->
            MibObject.i4Dot11CFPMaxDuration ==
            pWsscfgSetDot11StationConfigEntry->MibObject.i4Dot11CFPMaxDuration)
            pWsscfgIsSetDot11StationConfigEntry->
                bDot11CFPMaxDuration = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11StationConfigEntry->
        bDot11AuthenticationResponseTimeOut == OSIX_TRUE)
    {
        if (pWsscfgDot11StationConfigEntry->
            MibObject.u4Dot11AuthenticationResponseTimeOut ==
            pWsscfgSetDot11StationConfigEntry->
            MibObject.u4Dot11AuthenticationResponseTimeOut)
            pWsscfgIsSetDot11StationConfigEntry->
                bDot11AuthenticationResponseTimeOut = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11StationConfigEntry->
        bDot11PowerManagementMode == OSIX_TRUE)
    {
        if (pWsscfgDot11StationConfigEntry->
            MibObject.i4Dot11PowerManagementMode ==
            pWsscfgSetDot11StationConfigEntry->
            MibObject.i4Dot11PowerManagementMode)
            pWsscfgIsSetDot11StationConfigEntry->bDot11PowerManagementMode
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11DesiredSSID == OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgDot11StationConfigEntry->
              MibObject.au1Dot11DesiredSSID,
              pWsscfgSetDot11StationConfigEntry->
              MibObject.au1Dot11DesiredSSID,
              pWsscfgSetDot11StationConfigEntry->
              MibObject.i4Dot11DesiredSSIDLen) == 0)
            && (pWsscfgDot11StationConfigEntry->
                MibObject.i4Dot11DesiredSSIDLen ==
                pWsscfgSetDot11StationConfigEntry->
                MibObject.i4Dot11DesiredSSIDLen))
            pWsscfgIsSetDot11StationConfigEntry->bDot11DesiredSSID = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11DesiredBSSType == OSIX_TRUE)
    {
        if (pWsscfgDot11StationConfigEntry->
            MibObject.i4Dot11DesiredBSSType ==
            pWsscfgSetDot11StationConfigEntry->MibObject.i4Dot11DesiredBSSType)
            pWsscfgIsSetDot11StationConfigEntry->
                bDot11DesiredBSSType = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11StationConfigEntry->
        bDot11OperationalRateSet == OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgDot11StationConfigEntry->
              MibObject.au1Dot11OperationalRateSet,
              pWsscfgSetDot11StationConfigEntry->
              MibObject.au1Dot11OperationalRateSet,
              pWsscfgSetDot11StationConfigEntry->
              MibObject.i4Dot11OperationalRateSetLen) == 0)
            && (pWsscfgDot11StationConfigEntry->
                MibObject.i4Dot11OperationalRateSetLen ==
                pWsscfgSetDot11StationConfigEntry->
                MibObject.i4Dot11OperationalRateSetLen))
            pWsscfgIsSetDot11StationConfigEntry->
                bDot11OperationalRateSet = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11BeaconPeriod == OSIX_TRUE)
    {
        if (pWsscfgDot11StationConfigEntry->
            MibObject.i4Dot11BeaconPeriod ==
            pWsscfgSetDot11StationConfigEntry->MibObject.i4Dot11BeaconPeriod)
            pWsscfgIsSetDot11StationConfigEntry->bDot11BeaconPeriod =
                OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11DTIMPeriod == OSIX_TRUE)
    {
        if (pWsscfgDot11StationConfigEntry->MibObject.
            i4Dot11DTIMPeriod ==
            pWsscfgSetDot11StationConfigEntry->MibObject.i4Dot11DTIMPeriod)
            pWsscfgIsSetDot11StationConfigEntry->bDot11DTIMPeriod = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11AssociationResponseTimeOut ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11StationConfigEntry->
            MibObject.u4Dot11AssociationResponseTimeOut ==
            pWsscfgSetDot11StationConfigEntry->
            MibObject.u4Dot11AssociationResponseTimeOut)
            pWsscfgIsSetDot11StationConfigEntry->
                bDot11AssociationResponseTimeOut = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11StationConfigEntry->
        bDot11MultiDomainCapabilityImplemented == OSIX_TRUE)
    {
        if (pWsscfgDot11StationConfigEntry->
            MibObject.i4Dot11MultiDomainCapabilityImplemented ==
            pWsscfgSetDot11StationConfigEntry->
            MibObject.i4Dot11MultiDomainCapabilityImplemented)
            pWsscfgIsSetDot11StationConfigEntry->
                bDot11MultiDomainCapabilityImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11StationConfigEntry->
        bDot11MultiDomainCapabilityEnabled == OSIX_TRUE)
    {
        if (pWsscfgDot11StationConfigEntry->
            MibObject.i4Dot11MultiDomainCapabilityEnabled ==
            pWsscfgSetDot11StationConfigEntry->
            MibObject.i4Dot11MultiDomainCapabilityEnabled)
            pWsscfgIsSetDot11StationConfigEntry->
                bDot11MultiDomainCapabilityEnabled = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11SpectrumManagementRequired ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11StationConfigEntry->
            MibObject.i4Dot11SpectrumManagementRequired ==
            pWsscfgSetDot11StationConfigEntry->
            MibObject.i4Dot11SpectrumManagementRequired)
            pWsscfgIsSetDot11StationConfigEntry->
                bDot11SpectrumManagementRequired = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11StationConfigEntry->
        bDot11RegulatoryClassesImplemented == OSIX_TRUE)
    {
        if (pWsscfgDot11StationConfigEntry->
            MibObject.i4Dot11RegulatoryClassesImplemented ==
            pWsscfgSetDot11StationConfigEntry->
            MibObject.i4Dot11RegulatoryClassesImplemented)
            pWsscfgIsSetDot11StationConfigEntry->
                bDot11RegulatoryClassesImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11RegulatoryClassesRequired ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11StationConfigEntry->
            MibObject.i4Dot11RegulatoryClassesRequired ==
            pWsscfgSetDot11StationConfigEntry->
            MibObject.i4Dot11RegulatoryClassesRequired)
            pWsscfgIsSetDot11StationConfigEntry->bDot11RegulatoryClassesRequired
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11AssociateinNQBSS ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11StationConfigEntry->
            MibObject.i4Dot11AssociateinNQBSS ==
            pWsscfgSetDot11StationConfigEntry->
            MibObject.i4Dot11AssociateinNQBSS)
            pWsscfgIsSetDot11StationConfigEntry->
                bDot11AssociateinNQBSS = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11DLSAllowedInQBSS ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11StationConfigEntry->
            MibObject.i4Dot11DLSAllowedInQBSS ==
            pWsscfgSetDot11StationConfigEntry->
            MibObject.i4Dot11DLSAllowedInQBSS)
            pWsscfgIsSetDot11StationConfigEntry->
                bDot11DLSAllowedInQBSS = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11DLSAllowed == OSIX_TRUE)
    {
        if (pWsscfgDot11StationConfigEntry->MibObject.
            i4Dot11DLSAllowed ==
            pWsscfgSetDot11StationConfigEntry->MibObject.i4Dot11DLSAllowed)
            pWsscfgIsSetDot11StationConfigEntry->bDot11DLSAllowed = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11StationConfigEntry->MibObject.i4IfIndex ==
            pWsscfgSetDot11StationConfigEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11StationConfigEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11StationConfigEntry->bDot11StationID ==
         OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11StationConfigEntry->bDot11MediumOccupancyLimit
         == OSIX_FALSE)
        && (pWsscfgIsSetDot11StationConfigEntry->bDot11CFPPeriod ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11StationConfigEntry->
            bDot11CFPMaxDuration == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11StationConfigEntry->
         bDot11AuthenticationResponseTimeOut == OSIX_FALSE)
        && (pWsscfgIsSetDot11StationConfigEntry->bDot11PowerManagementMode ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11StationConfigEntry->bDot11DesiredSSID ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11StationConfigEntry->bDot11DesiredBSSType ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11StationConfigEntry->bDot11OperationalRateSet ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11StationConfigEntry->bDot11BeaconPeriod ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11StationConfigEntry->bDot11DTIMPeriod == OSIX_FALSE)
        && (pWsscfgIsSetDot11StationConfigEntry->
            bDot11AssociationResponseTimeOut == OSIX_FALSE)
        && (pWsscfgIsSetDot11StationConfigEntry->
            bDot11MultiDomainCapabilityImplemented == OSIX_FALSE)
        && (pWsscfgIsSetDot11StationConfigEntry->
            bDot11MultiDomainCapabilityEnabled == OSIX_FALSE)
        && (pWsscfgIsSetDot11StationConfigEntry->
            bDot11SpectrumManagementRequired == OSIX_FALSE)
        && (pWsscfgIsSetDot11StationConfigEntry->
            bDot11RegulatoryClassesImplemented == OSIX_FALSE)
        && (pWsscfgIsSetDot11StationConfigEntry->
            bDot11RegulatoryClassesRequired == OSIX_FALSE)
        && (pWsscfgIsSetDot11StationConfigEntry->bDot11AssociateinNQBSS ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11StationConfigEntry->bDot11DLSAllowedInQBSS ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11StationConfigEntry->bDot11DLSAllowed == OSIX_FALSE)
        && (pWsscfgIsSetDot11StationConfigEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11AuthenticationAlgorithmsTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11AuthenticationAlgorithmsEntry
                pWsscfgSetDot11AuthenticationAlgorithmsEntry
                pWsscfgIsSetDot11AuthenticationAlgorithmsEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    Dot11AuthenticationAlgorithmsTableFilterInputs
    (tWsscfgDot11AuthenticationAlgorithmsEntry *
     pWsscfgDot11AuthenticationAlgorithmsEntry,
     tWsscfgDot11AuthenticationAlgorithmsEntry *
     pWsscfgSetDot11AuthenticationAlgorithmsEntry,
     tWsscfgIsSetDot11AuthenticationAlgorithmsEntry *
     pWsscfgIsSetDot11AuthenticationAlgorithmsEntry)
{
    if (pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->
        bDot11AuthenticationAlgorithmsIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11AuthenticationAlgorithmsEntry->
            MibObject.i4Dot11AuthenticationAlgorithmsIndex ==
            pWsscfgSetDot11AuthenticationAlgorithmsEntry->
            MibObject.i4Dot11AuthenticationAlgorithmsIndex)
            pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->
                bDot11AuthenticationAlgorithmsIndex = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->
        bDot11AuthenticationAlgorithmsEnable == OSIX_TRUE)
    {
        if (pWsscfgDot11AuthenticationAlgorithmsEntry->
            MibObject.i4Dot11AuthenticationAlgorithmsEnable ==
            pWsscfgSetDot11AuthenticationAlgorithmsEntry->
            MibObject.i4Dot11AuthenticationAlgorithmsEnable)
            pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->
                bDot11AuthenticationAlgorithmsEnable = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11AuthenticationAlgorithmsEntry->
            MibObject.i4IfIndex ==
            pWsscfgSetDot11AuthenticationAlgorithmsEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->bIfIndex =
                OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->
         bDot11AuthenticationAlgorithmsIndex == OSIX_FALSE)
        && (pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->
            bDot11AuthenticationAlgorithmsEnable == OSIX_FALSE)
        && (pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->bIfIndex ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11WEPDefaultKeysTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11WEPDefaultKeysEntry
                pWsscfgSetDot11WEPDefaultKeysEntry
                pWsscfgIsSetDot11WEPDefaultKeysEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
Dot11WEPDefaultKeysTableFilterInputs (tWsscfgDot11WEPDefaultKeysEntry *
                                      pWsscfgDot11WEPDefaultKeysEntry,
                                      tWsscfgDot11WEPDefaultKeysEntry *
                                      pWsscfgSetDot11WEPDefaultKeysEntry,
                                      tWsscfgIsSetDot11WEPDefaultKeysEntry *
                                      pWsscfgIsSetDot11WEPDefaultKeysEntry)
{
    if (pWsscfgIsSetDot11WEPDefaultKeysEntry->
        bDot11WEPDefaultKeyIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11WEPDefaultKeysEntry->
            MibObject.i4Dot11WEPDefaultKeyIndex ==
            pWsscfgSetDot11WEPDefaultKeysEntry->
            MibObject.i4Dot11WEPDefaultKeyIndex)
            pWsscfgIsSetDot11WEPDefaultKeysEntry->bDot11WEPDefaultKeyIndex
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11WEPDefaultKeysEntry->
        bDot11WEPDefaultKeyValue == OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgDot11WEPDefaultKeysEntry->
              MibObject.au1Dot11WEPDefaultKeyValue,
              pWsscfgSetDot11WEPDefaultKeysEntry->
              MibObject.au1Dot11WEPDefaultKeyValue,
              pWsscfgSetDot11WEPDefaultKeysEntry->
              MibObject.i4Dot11WEPDefaultKeyValueLen) == 0)
            && (pWsscfgDot11WEPDefaultKeysEntry->
                MibObject.i4Dot11WEPDefaultKeyValueLen ==
                pWsscfgSetDot11WEPDefaultKeysEntry->
                MibObject.i4Dot11WEPDefaultKeyValueLen))
            pWsscfgIsSetDot11WEPDefaultKeysEntry->bDot11WEPDefaultKeyValue
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11WEPDefaultKeysEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11WEPDefaultKeysEntry->MibObject.i4IfIndex ==
            pWsscfgSetDot11WEPDefaultKeysEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11WEPDefaultKeysEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11WEPDefaultKeysEntry->
         bDot11WEPDefaultKeyIndex == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11WEPDefaultKeysEntry->bDot11WEPDefaultKeyValue
         == OSIX_FALSE)
        && (pWsscfgIsSetDot11WEPDefaultKeysEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11WEPKeyMappingsTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11WEPKeyMappingsEntry
                pWsscfgSetDot11WEPKeyMappingsEntry
                pWsscfgIsSetDot11WEPKeyMappingsEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
Dot11WEPKeyMappingsTableFilterInputs (tWsscfgDot11WEPKeyMappingsEntry *
                                      pWsscfgDot11WEPKeyMappingsEntry,
                                      tWsscfgDot11WEPKeyMappingsEntry *
                                      pWsscfgSetDot11WEPKeyMappingsEntry,
                                      tWsscfgIsSetDot11WEPKeyMappingsEntry *
                                      pWsscfgIsSetDot11WEPKeyMappingsEntry)
{
    if (pWsscfgIsSetDot11WEPKeyMappingsEntry->
        bDot11WEPKeyMappingIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11WEPKeyMappingsEntry->
            MibObject.i4Dot11WEPKeyMappingIndex ==
            pWsscfgSetDot11WEPKeyMappingsEntry->
            MibObject.i4Dot11WEPKeyMappingIndex)
            pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingIndex
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11WEPKeyMappingsEntry->
        bDot11WEPKeyMappingAddress == OSIX_TRUE)
    {
        if (MEMCMP
            (&
             (pWsscfgDot11WEPKeyMappingsEntry->
              MibObject.Dot11WEPKeyMappingAddress),
             &(pWsscfgSetDot11WEPKeyMappingsEntry->
               MibObject.Dot11WEPKeyMappingAddress), 6) == 0)
            pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingAddress
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11WEPKeyMappingsEntry->
        bDot11WEPKeyMappingWEPOn == OSIX_TRUE)
    {
        if (pWsscfgDot11WEPKeyMappingsEntry->
            MibObject.i4Dot11WEPKeyMappingWEPOn ==
            pWsscfgSetDot11WEPKeyMappingsEntry->
            MibObject.i4Dot11WEPKeyMappingWEPOn)
            pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingWEPOn
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11WEPKeyMappingsEntry->
        bDot11WEPKeyMappingValue == OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgDot11WEPKeyMappingsEntry->
              MibObject.au1Dot11WEPKeyMappingValue,
              pWsscfgSetDot11WEPKeyMappingsEntry->
              MibObject.au1Dot11WEPKeyMappingValue,
              pWsscfgSetDot11WEPKeyMappingsEntry->
              MibObject.i4Dot11WEPKeyMappingValueLen) == 0)
            && (pWsscfgDot11WEPKeyMappingsEntry->
                MibObject.i4Dot11WEPKeyMappingValueLen ==
                pWsscfgSetDot11WEPKeyMappingsEntry->
                MibObject.i4Dot11WEPKeyMappingValueLen))
            pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingValue
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11WEPKeyMappingsEntry->
        bDot11WEPKeyMappingStatus == OSIX_TRUE)
    {
        if (pWsscfgDot11WEPKeyMappingsEntry->
            MibObject.i4Dot11WEPKeyMappingStatus ==
            pWsscfgSetDot11WEPKeyMappingsEntry->
            MibObject.i4Dot11WEPKeyMappingStatus)
            pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingStatus
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11WEPKeyMappingsEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11WEPKeyMappingsEntry->MibObject.i4IfIndex ==
            pWsscfgSetDot11WEPKeyMappingsEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11WEPKeyMappingsEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11WEPKeyMappingsEntry->
         bDot11WEPKeyMappingIndex == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingAddress
         == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingWEPOn
         == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingValue
         == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingStatus
         == OSIX_FALSE)
        && (pWsscfgIsSetDot11WEPKeyMappingsEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11PrivacyTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11PrivacyEntry
                pWsscfgSetDot11PrivacyEntry
                pWsscfgIsSetDot11PrivacyEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
Dot11PrivacyTableFilterInputs (tWsscfgDot11PrivacyEntry *
                               pWsscfgDot11PrivacyEntry,
                               tWsscfgDot11PrivacyEntry *
                               pWsscfgSetDot11PrivacyEntry,
                               tWsscfgIsSetDot11PrivacyEntry *
                               pWsscfgIsSetDot11PrivacyEntry)
{
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11PrivacyInvoked == OSIX_TRUE)
    {
        if (pWsscfgDot11PrivacyEntry->MibObject.
            i4Dot11PrivacyInvoked ==
            pWsscfgSetDot11PrivacyEntry->MibObject.i4Dot11PrivacyInvoked)
            pWsscfgIsSetDot11PrivacyEntry->bDot11PrivacyInvoked = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11WEPDefaultKeyID == OSIX_TRUE)
    {
        if (pWsscfgDot11PrivacyEntry->MibObject.
            i4Dot11WEPDefaultKeyID ==
            pWsscfgSetDot11PrivacyEntry->MibObject.i4Dot11WEPDefaultKeyID)
            pWsscfgIsSetDot11PrivacyEntry->bDot11WEPDefaultKeyID = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11WEPKeyMappingLength == OSIX_TRUE)
    {
        if (pWsscfgDot11PrivacyEntry->
            MibObject.u4Dot11WEPKeyMappingLength ==
            pWsscfgSetDot11PrivacyEntry->MibObject.u4Dot11WEPKeyMappingLength)
            pWsscfgIsSetDot11PrivacyEntry->bDot11WEPKeyMappingLength =
                OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11ExcludeUnencrypted == OSIX_TRUE)
    {
        if (pWsscfgDot11PrivacyEntry->
            MibObject.i4Dot11ExcludeUnencrypted ==
            pWsscfgSetDot11PrivacyEntry->MibObject.i4Dot11ExcludeUnencrypted)
            pWsscfgIsSetDot11PrivacyEntry->bDot11ExcludeUnencrypted =
                OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11RSNAEnabled == OSIX_TRUE)
    {
        if (pWsscfgDot11PrivacyEntry->MibObject.i4Dot11RSNAEnabled ==
            pWsscfgSetDot11PrivacyEntry->MibObject.i4Dot11RSNAEnabled)
            pWsscfgIsSetDot11PrivacyEntry->bDot11RSNAEnabled = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11RSNAPreauthenticationEnabled ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11PrivacyEntry->
            MibObject.i4Dot11RSNAPreauthenticationEnabled ==
            pWsscfgSetDot11PrivacyEntry->
            MibObject.i4Dot11RSNAPreauthenticationEnabled)
            pWsscfgIsSetDot11PrivacyEntry->bDot11RSNAPreauthenticationEnabled
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PrivacyEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11PrivacyEntry->MibObject.i4IfIndex ==
            pWsscfgSetDot11PrivacyEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11PrivacyEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11PrivacyEntry->bDot11PrivacyInvoked ==
         OSIX_FALSE)
        && (pWsscfgIsSetDot11PrivacyEntry->bDot11WEPDefaultKeyID ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11PrivacyEntry->
            bDot11WEPKeyMappingLength == OSIX_FALSE)
        && (pWsscfgIsSetDot11PrivacyEntry->bDot11ExcludeUnencrypted ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11PrivacyEntry->bDot11RSNAEnabled ==
            OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11PrivacyEntry->bDot11RSNAPreauthenticationEnabled
         == OSIX_FALSE)
        && (pWsscfgIsSetDot11PrivacyEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11MultiDomainCapabilityTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11MultiDomainCapabilityEntry
                pWsscfgSetDot11MultiDomainCapabilityEntry
                pWsscfgIsSetDot11MultiDomainCapabilityEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    Dot11MultiDomainCapabilityTableFilterInputs
    (tWsscfgDot11MultiDomainCapabilityEntry *
     pWsscfgDot11MultiDomainCapabilityEntry,
     tWsscfgDot11MultiDomainCapabilityEntry *
     pWsscfgSetDot11MultiDomainCapabilityEntry,
     tWsscfgIsSetDot11MultiDomainCapabilityEntry *
     pWsscfgIsSetDot11MultiDomainCapabilityEntry)
{
    if (pWsscfgIsSetDot11MultiDomainCapabilityEntry->
        bDot11MultiDomainCapabilityIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11MultiDomainCapabilityEntry->
            MibObject.i4Dot11MultiDomainCapabilityIndex ==
            pWsscfgSetDot11MultiDomainCapabilityEntry->
            MibObject.i4Dot11MultiDomainCapabilityIndex)
            pWsscfgIsSetDot11MultiDomainCapabilityEntry->
                bDot11MultiDomainCapabilityIndex = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11MultiDomainCapabilityEntry->bDot11FirstChannelNumber ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11MultiDomainCapabilityEntry->
            MibObject.i4Dot11FirstChannelNumber ==
            pWsscfgSetDot11MultiDomainCapabilityEntry->
            MibObject.i4Dot11FirstChannelNumber)
            pWsscfgIsSetDot11MultiDomainCapabilityEntry->
                bDot11FirstChannelNumber = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11MultiDomainCapabilityEntry->bDot11NumberofChannels ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11MultiDomainCapabilityEntry->
            MibObject.i4Dot11NumberofChannels ==
            pWsscfgSetDot11MultiDomainCapabilityEntry->
            MibObject.i4Dot11NumberofChannels)
            pWsscfgIsSetDot11MultiDomainCapabilityEntry->bDot11NumberofChannels
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11MultiDomainCapabilityEntry->
        bDot11MaximumTransmitPowerLevel == OSIX_TRUE)
    {
        if (pWsscfgDot11MultiDomainCapabilityEntry->
            MibObject.i4Dot11MaximumTransmitPowerLevel ==
            pWsscfgSetDot11MultiDomainCapabilityEntry->
            MibObject.i4Dot11MaximumTransmitPowerLevel)
            pWsscfgIsSetDot11MultiDomainCapabilityEntry->
                bDot11MaximumTransmitPowerLevel = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11MultiDomainCapabilityEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11MultiDomainCapabilityEntry->MibObject.
            i4IfIndex ==
            pWsscfgSetDot11MultiDomainCapabilityEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11MultiDomainCapabilityEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11MultiDomainCapabilityEntry->
         bDot11MultiDomainCapabilityIndex == OSIX_FALSE)
        && (pWsscfgIsSetDot11MultiDomainCapabilityEntry->
            bDot11FirstChannelNumber == OSIX_FALSE)
        && (pWsscfgIsSetDot11MultiDomainCapabilityEntry->
            bDot11NumberofChannels == OSIX_FALSE)
        && (pWsscfgIsSetDot11MultiDomainCapabilityEntry->
            bDot11MaximumTransmitPowerLevel == OSIX_FALSE)
        && (pWsscfgIsSetDot11MultiDomainCapabilityEntry->bIfIndex ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11SpectrumManagementTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11SpectrumManagementEntry
                pWsscfgSetDot11SpectrumManagementEntry
                pWsscfgIsSetDot11SpectrumManagementEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
Dot11SpectrumManagementTableFilterInputs (tWsscfgDot11SpectrumManagementEntry *
                                          pWsscfgDot11SpectrumManagementEntry,
                                          tWsscfgDot11SpectrumManagementEntry *
                                          pWsscfgSetDot11SpectrumManagementEntry,
                                          tWsscfgIsSetDot11SpectrumManagementEntry
                                          *
                                          pWsscfgIsSetDot11SpectrumManagementEntry)
{
    if (pWsscfgIsSetDot11SpectrumManagementEntry->
        bDot11SpectrumManagementIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11SpectrumManagementEntry->
            MibObject.i4Dot11SpectrumManagementIndex ==
            pWsscfgSetDot11SpectrumManagementEntry->
            MibObject.i4Dot11SpectrumManagementIndex)
            pWsscfgIsSetDot11SpectrumManagementEntry->
                bDot11SpectrumManagementIndex = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11SpectrumManagementEntry->bDot11MitigationRequirement ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11SpectrumManagementEntry->
            MibObject.i4Dot11MitigationRequirement ==
            pWsscfgSetDot11SpectrumManagementEntry->
            MibObject.i4Dot11MitigationRequirement)
            pWsscfgIsSetDot11SpectrumManagementEntry->
                bDot11MitigationRequirement = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11SpectrumManagementEntry->bDot11ChannelSwitchTime ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11SpectrumManagementEntry->
            MibObject.i4Dot11ChannelSwitchTime ==
            pWsscfgSetDot11SpectrumManagementEntry->
            MibObject.i4Dot11ChannelSwitchTime)
            pWsscfgIsSetDot11SpectrumManagementEntry->bDot11ChannelSwitchTime
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11SpectrumManagementEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11SpectrumManagementEntry->MibObject.
            i4IfIndex ==
            pWsscfgSetDot11SpectrumManagementEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11SpectrumManagementEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11SpectrumManagementEntry->
         bDot11SpectrumManagementIndex == OSIX_FALSE)
        && (pWsscfgIsSetDot11SpectrumManagementEntry->
            bDot11MitigationRequirement == OSIX_FALSE)
        && (pWsscfgIsSetDot11SpectrumManagementEntry->bDot11ChannelSwitchTime ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11SpectrumManagementEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11RegulatoryClassesTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11RegulatoryClassesEntry
                pWsscfgSetDot11RegulatoryClassesEntry
                pWsscfgIsSetDot11RegulatoryClassesEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
Dot11RegulatoryClassesTableFilterInputs (tWsscfgDot11RegulatoryClassesEntry *
                                         pWsscfgDot11RegulatoryClassesEntry,
                                         tWsscfgDot11RegulatoryClassesEntry *
                                         pWsscfgSetDot11RegulatoryClassesEntry,
                                         tWsscfgIsSetDot11RegulatoryClassesEntry
                                         *
                                         pWsscfgIsSetDot11RegulatoryClassesEntry)
{
    if (pWsscfgIsSetDot11RegulatoryClassesEntry->bDot11RegulatoryClassesIndex ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11RegulatoryClassesEntry->
            MibObject.i4Dot11RegulatoryClassesIndex ==
            pWsscfgSetDot11RegulatoryClassesEntry->
            MibObject.i4Dot11RegulatoryClassesIndex)
            pWsscfgIsSetDot11RegulatoryClassesEntry->
                bDot11RegulatoryClassesIndex = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RegulatoryClassesEntry->
        bDot11RegulatoryClass == OSIX_TRUE)
    {
        if (pWsscfgDot11RegulatoryClassesEntry->
            MibObject.i4Dot11RegulatoryClass ==
            pWsscfgSetDot11RegulatoryClassesEntry->
            MibObject.i4Dot11RegulatoryClass)
            pWsscfgIsSetDot11RegulatoryClassesEntry->bDot11RegulatoryClass
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RegulatoryClassesEntry->
        bDot11CoverageClass == OSIX_TRUE)
    {
        if (pWsscfgDot11RegulatoryClassesEntry->
            MibObject.i4Dot11CoverageClass ==
            pWsscfgSetDot11RegulatoryClassesEntry->
            MibObject.i4Dot11CoverageClass)
            pWsscfgIsSetDot11RegulatoryClassesEntry->
                bDot11CoverageClass = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RegulatoryClassesEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11RegulatoryClassesEntry->MibObject.i4IfIndex ==
            pWsscfgSetDot11RegulatoryClassesEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11RegulatoryClassesEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11RegulatoryClassesEntry->
         bDot11RegulatoryClassesIndex == OSIX_FALSE)
        && (pWsscfgIsSetDot11RegulatoryClassesEntry->bDot11RegulatoryClass ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11RegulatoryClassesEntry->bDot11CoverageClass ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11RegulatoryClassesEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11OperationTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11OperationEntry
                pWsscfgSetDot11OperationEntry
                pWsscfgIsSetDot11OperationEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
Dot11OperationTableFilterInputs (tWsscfgDot11OperationEntry *
                                 pWsscfgDot11OperationEntry,
                                 tWsscfgDot11OperationEntry *
                                 pWsscfgSetDot11OperationEntry,
                                 tWsscfgIsSetDot11OperationEntry *
                                 pWsscfgIsSetDot11OperationEntry)
{
    if (pWsscfgIsSetDot11OperationEntry->bDot11RTSThreshold == OSIX_TRUE)
    {
        if (pWsscfgDot11OperationEntry->MibObject.
            i4Dot11RTSThreshold ==
            pWsscfgSetDot11OperationEntry->MibObject.i4Dot11RTSThreshold)
            pWsscfgIsSetDot11OperationEntry->bDot11RTSThreshold = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11ShortRetryLimit == OSIX_TRUE)
    {
        if (pWsscfgDot11OperationEntry->MibObject.
            i4Dot11ShortRetryLimit ==
            pWsscfgSetDot11OperationEntry->MibObject.i4Dot11ShortRetryLimit)
            pWsscfgIsSetDot11OperationEntry->bDot11ShortRetryLimit = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11LongRetryLimit == OSIX_TRUE)
    {
        if (pWsscfgDot11OperationEntry->MibObject.
            i4Dot11LongRetryLimit ==
            pWsscfgSetDot11OperationEntry->MibObject.i4Dot11LongRetryLimit)
            pWsscfgIsSetDot11OperationEntry->bDot11LongRetryLimit = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11OperationEntry->
        bDot11FragmentationThreshold == OSIX_TRUE)
    {
        if (pWsscfgDot11OperationEntry->
            MibObject.i4Dot11FragmentationThreshold ==
            pWsscfgSetDot11OperationEntry->
            MibObject.i4Dot11FragmentationThreshold)
            pWsscfgIsSetDot11OperationEntry->
                bDot11FragmentationThreshold = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11OperationEntry->
        bDot11MaxTransmitMSDULifetime == OSIX_TRUE)
    {
        if (pWsscfgDot11OperationEntry->
            MibObject.u4Dot11MaxTransmitMSDULifetime ==
            pWsscfgSetDot11OperationEntry->
            MibObject.u4Dot11MaxTransmitMSDULifetime)
            pWsscfgIsSetDot11OperationEntry->bDot11MaxTransmitMSDULifetime
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11MaxReceiveLifetime == OSIX_TRUE)
    {
        if (pWsscfgDot11OperationEntry->
            MibObject.u4Dot11MaxReceiveLifetime ==
            pWsscfgSetDot11OperationEntry->MibObject.u4Dot11MaxReceiveLifetime)
            pWsscfgIsSetDot11OperationEntry->
                bDot11MaxReceiveLifetime = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11CAPLimit == OSIX_TRUE)
    {
        if (pWsscfgDot11OperationEntry->MibObject.i4Dot11CAPLimit ==
            pWsscfgSetDot11OperationEntry->MibObject.i4Dot11CAPLimit)
            pWsscfgIsSetDot11OperationEntry->bDot11CAPLimit = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11HCCWmin == OSIX_TRUE)
    {
        if (pWsscfgDot11OperationEntry->MibObject.i4Dot11HCCWmin ==
            pWsscfgSetDot11OperationEntry->MibObject.i4Dot11HCCWmin)
            pWsscfgIsSetDot11OperationEntry->bDot11HCCWmin = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11HCCWmax == OSIX_TRUE)
    {
        if (pWsscfgDot11OperationEntry->MibObject.i4Dot11HCCWmax ==
            pWsscfgSetDot11OperationEntry->MibObject.i4Dot11HCCWmax)
            pWsscfgIsSetDot11OperationEntry->bDot11HCCWmax = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11HCCAIFSN == OSIX_TRUE)
    {
        if (pWsscfgDot11OperationEntry->MibObject.i4Dot11HCCAIFSN ==
            pWsscfgSetDot11OperationEntry->MibObject.i4Dot11HCCAIFSN)
            pWsscfgIsSetDot11OperationEntry->bDot11HCCAIFSN = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11ADDBAResponseTimeout ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11OperationEntry->
            MibObject.i4Dot11ADDBAResponseTimeout ==
            pWsscfgSetDot11OperationEntry->
            MibObject.i4Dot11ADDBAResponseTimeout)
            pWsscfgIsSetDot11OperationEntry->
                bDot11ADDBAResponseTimeout = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11ADDTSResponseTimeout ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11OperationEntry->
            MibObject.i4Dot11ADDTSResponseTimeout ==
            pWsscfgSetDot11OperationEntry->
            MibObject.i4Dot11ADDTSResponseTimeout)
            pWsscfgIsSetDot11OperationEntry->
                bDot11ADDTSResponseTimeout = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11OperationEntry->
        bDot11ChannelUtilizationBeaconInterval == OSIX_TRUE)
    {
        if (pWsscfgDot11OperationEntry->
            MibObject.i4Dot11ChannelUtilizationBeaconInterval ==
            pWsscfgSetDot11OperationEntry->
            MibObject.i4Dot11ChannelUtilizationBeaconInterval)
            pWsscfgIsSetDot11OperationEntry->
                bDot11ChannelUtilizationBeaconInterval = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11ScheduleTimeout == OSIX_TRUE)
    {
        if (pWsscfgDot11OperationEntry->MibObject.
            i4Dot11ScheduleTimeout ==
            pWsscfgSetDot11OperationEntry->MibObject.i4Dot11ScheduleTimeout)
            pWsscfgIsSetDot11OperationEntry->bDot11ScheduleTimeout = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11DLSResponseTimeout == OSIX_TRUE)
    {
        if (pWsscfgDot11OperationEntry->
            MibObject.i4Dot11DLSResponseTimeout ==
            pWsscfgSetDot11OperationEntry->MibObject.i4Dot11DLSResponseTimeout)
            pWsscfgIsSetDot11OperationEntry->
                bDot11DLSResponseTimeout = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11OperationEntry->
        bDot11QAPMissingAckRetryLimit == OSIX_TRUE)
    {
        if (pWsscfgDot11OperationEntry->
            MibObject.i4Dot11QAPMissingAckRetryLimit ==
            pWsscfgSetDot11OperationEntry->
            MibObject.i4Dot11QAPMissingAckRetryLimit)
            pWsscfgIsSetDot11OperationEntry->bDot11QAPMissingAckRetryLimit
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11EDCAAveragingPeriod == OSIX_TRUE)
    {
        if (pWsscfgDot11OperationEntry->
            MibObject.i4Dot11EDCAAveragingPeriod ==
            pWsscfgSetDot11OperationEntry->MibObject.i4Dot11EDCAAveragingPeriod)
            pWsscfgIsSetDot11OperationEntry->
                bDot11EDCAAveragingPeriod = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11OperationEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11OperationEntry->MibObject.i4IfIndex ==
            pWsscfgSetDot11OperationEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11OperationEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11OperationEntry->bDot11RTSThreshold ==
         OSIX_FALSE)
        && (pWsscfgIsSetDot11OperationEntry->bDot11ShortRetryLimit ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11OperationEntry->bDot11LongRetryLimit ==
            OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11OperationEntry->bDot11FragmentationThreshold
         == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11OperationEntry->bDot11MaxTransmitMSDULifetime
         == OSIX_FALSE)
        && (pWsscfgIsSetDot11OperationEntry->
            bDot11MaxReceiveLifetime == OSIX_FALSE)
        && (pWsscfgIsSetDot11OperationEntry->bDot11CAPLimit ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11OperationEntry->bDot11HCCWmin ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11OperationEntry->bDot11HCCWmax ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11OperationEntry->bDot11HCCAIFSN ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11OperationEntry->
            bDot11ADDBAResponseTimeout == OSIX_FALSE)
        && (pWsscfgIsSetDot11OperationEntry->
            bDot11ADDTSResponseTimeout == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11OperationEntry->bDot11ChannelUtilizationBeaconInterval
         == OSIX_FALSE)
        && (pWsscfgIsSetDot11OperationEntry->bDot11ScheduleTimeout ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11OperationEntry->
            bDot11DLSResponseTimeout == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11OperationEntry->bDot11QAPMissingAckRetryLimit
         == OSIX_FALSE)
        && (pWsscfgIsSetDot11OperationEntry->
            bDot11EDCAAveragingPeriod == OSIX_FALSE)
        && (pWsscfgIsSetDot11OperationEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11GroupAddressesTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11GroupAddressesEntry
                pWsscfgSetDot11GroupAddressesEntry
                pWsscfgIsSetDot11GroupAddressesEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
Dot11GroupAddressesTableFilterInputs (tWsscfgDot11GroupAddressesEntry *
                                      pWsscfgDot11GroupAddressesEntry,
                                      tWsscfgDot11GroupAddressesEntry *
                                      pWsscfgSetDot11GroupAddressesEntry,
                                      tWsscfgIsSetDot11GroupAddressesEntry *
                                      pWsscfgIsSetDot11GroupAddressesEntry)
{
    if (pWsscfgIsSetDot11GroupAddressesEntry->
        bDot11GroupAddressesIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11GroupAddressesEntry->
            MibObject.i4Dot11GroupAddressesIndex ==
            pWsscfgSetDot11GroupAddressesEntry->
            MibObject.i4Dot11GroupAddressesIndex)
            pWsscfgIsSetDot11GroupAddressesEntry->bDot11GroupAddressesIndex
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11GroupAddressesEntry->bDot11Address == OSIX_TRUE)
    {
        if (MEMCMP
            (&
             (pWsscfgDot11GroupAddressesEntry->MibObject.
              Dot11Address),
             &(pWsscfgSetDot11GroupAddressesEntry->MibObject.
               Dot11Address), 6) == 0)
            pWsscfgIsSetDot11GroupAddressesEntry->bDot11Address = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11GroupAddressesEntry->
        bDot11GroupAddressesStatus == OSIX_TRUE)
    {
        if (pWsscfgDot11GroupAddressesEntry->
            MibObject.i4Dot11GroupAddressesStatus ==
            pWsscfgSetDot11GroupAddressesEntry->
            MibObject.i4Dot11GroupAddressesStatus)
            pWsscfgIsSetDot11GroupAddressesEntry->bDot11GroupAddressesStatus
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11GroupAddressesEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11GroupAddressesEntry->MibObject.i4IfIndex ==
            pWsscfgSetDot11GroupAddressesEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11GroupAddressesEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11GroupAddressesEntry->
         bDot11GroupAddressesIndex == OSIX_FALSE)
        && (pWsscfgIsSetDot11GroupAddressesEntry->bDot11Address ==
            OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11GroupAddressesEntry->bDot11GroupAddressesStatus
         == OSIX_FALSE)
        && (pWsscfgIsSetDot11GroupAddressesEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11EDCATableFilterInputs
 Input       :  The Indices
                pWsscfgDot11EDCAEntry
                pWsscfgSetDot11EDCAEntry
                pWsscfgIsSetDot11EDCAEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
Dot11EDCATableFilterInputs (tWsscfgDot11EDCAEntry * pWsscfgDot11EDCAEntry,
                            tWsscfgDot11EDCAEntry * pWsscfgSetDot11EDCAEntry,
                            tWsscfgIsSetDot11EDCAEntry *
                            pWsscfgIsSetDot11EDCAEntry)
{
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11EDCAEntry->MibObject.i4Dot11EDCATableIndex ==
            pWsscfgSetDot11EDCAEntry->MibObject.i4Dot11EDCATableIndex)
            pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableIndex = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableCWmin == OSIX_TRUE)
    {
        if (pWsscfgDot11EDCAEntry->MibObject.i4Dot11EDCATableCWmin ==
            pWsscfgSetDot11EDCAEntry->MibObject.i4Dot11EDCATableCWmin)
            pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableCWmin = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableCWmax == OSIX_TRUE)
    {
        if (pWsscfgDot11EDCAEntry->MibObject.i4Dot11EDCATableCWmax ==
            pWsscfgSetDot11EDCAEntry->MibObject.i4Dot11EDCATableCWmax)
            pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableCWmax = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableAIFSN == OSIX_TRUE)
    {
        if (pWsscfgDot11EDCAEntry->MibObject.i4Dot11EDCATableAIFSN ==
            pWsscfgSetDot11EDCAEntry->MibObject.i4Dot11EDCATableAIFSN)
            pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableAIFSN = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableTXOPLimit == OSIX_TRUE)
    {
        if (pWsscfgDot11EDCAEntry->MibObject.
            i4Dot11EDCATableTXOPLimit ==
            pWsscfgSetDot11EDCAEntry->MibObject.i4Dot11EDCATableTXOPLimit)
            pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableTXOPLimit = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableMSDULifetime == OSIX_TRUE)
    {
        if (pWsscfgDot11EDCAEntry->
            MibObject.i4Dot11EDCATableMSDULifetime ==
            pWsscfgSetDot11EDCAEntry->MibObject.i4Dot11EDCATableMSDULifetime)
            pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableMSDULifetime =
                OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableMandatory == OSIX_TRUE)
    {
        if (pWsscfgDot11EDCAEntry->MibObject.
            i4Dot11EDCATableMandatory ==
            pWsscfgSetDot11EDCAEntry->MibObject.i4Dot11EDCATableMandatory)
            pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableMandatory = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11EDCAEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11EDCAEntry->MibObject.i4IfIndex ==
            pWsscfgSetDot11EDCAEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11EDCAEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableIndex ==
         OSIX_FALSE)
        && (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableCWmin ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableCWmax ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableAIFSN ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableTXOPLimit ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableMSDULifetime ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableMandatory ==
            OSIX_FALSE) && (pWsscfgIsSetDot11EDCAEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11QAPEDCATableFilterInputs
 Input       :  The Indices
                pWsscfgDot11QAPEDCAEntry
                pWsscfgSetDot11QAPEDCAEntry
                pWsscfgIsSetDot11QAPEDCAEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
Dot11QAPEDCATableFilterInputs (tWsscfgDot11QAPEDCAEntry *
                               pWsscfgDot11QAPEDCAEntry,
                               tWsscfgDot11QAPEDCAEntry *
                               pWsscfgSetDot11QAPEDCAEntry,
                               tWsscfgIsSetDot11QAPEDCAEntry *
                               pWsscfgIsSetDot11QAPEDCAEntry)
{
    if (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11QAPEDCAEntry->MibObject.
            i4Dot11QAPEDCATableIndex ==
            pWsscfgSetDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableIndex)
            pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableIndex = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableCWmin == OSIX_TRUE)
    {
        if (pWsscfgDot11QAPEDCAEntry->MibObject.
            i4Dot11QAPEDCATableCWmin ==
            pWsscfgSetDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableCWmin)
            pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableCWmin = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableCWmax == OSIX_TRUE)
    {
        if (pWsscfgDot11QAPEDCAEntry->MibObject.
            i4Dot11QAPEDCATableCWmax ==
            pWsscfgSetDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableCWmax)
            pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableCWmax = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableAIFSN == OSIX_TRUE)
    {
        if (pWsscfgDot11QAPEDCAEntry->MibObject.
            i4Dot11QAPEDCATableAIFSN ==
            pWsscfgSetDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableAIFSN)
            pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableAIFSN = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableTXOPLimit == OSIX_TRUE)
    {
        if (pWsscfgDot11QAPEDCAEntry->
            MibObject.i4Dot11QAPEDCATableTXOPLimit ==
            pWsscfgSetDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableTXOPLimit)
            pWsscfgIsSetDot11QAPEDCAEntry->
                bDot11QAPEDCATableTXOPLimit = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->
        bDot11QAPEDCATableMSDULifetime == OSIX_TRUE)
    {
        if (pWsscfgDot11QAPEDCAEntry->
            MibObject.i4Dot11QAPEDCATableMSDULifetime ==
            pWsscfgSetDot11QAPEDCAEntry->
            MibObject.i4Dot11QAPEDCATableMSDULifetime)
            pWsscfgIsSetDot11QAPEDCAEntry->
                bDot11QAPEDCATableMSDULifetime = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableMandatory == OSIX_TRUE)
    {
        if (pWsscfgDot11QAPEDCAEntry->
            MibObject.i4Dot11QAPEDCATableMandatory ==
            pWsscfgSetDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableMandatory)
            pWsscfgIsSetDot11QAPEDCAEntry->
                bDot11QAPEDCATableMandatory = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11QAPEDCAEntry->MibObject.i4IfIndex ==
            pWsscfgSetDot11QAPEDCAEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11QAPEDCAEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableIndex ==
         OSIX_FALSE)
        && (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableCWmin ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableCWmax ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableAIFSN ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11QAPEDCAEntry->
            bDot11QAPEDCATableTXOPLimit == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableMSDULifetime
         == OSIX_FALSE)
        && (pWsscfgIsSetDot11QAPEDCAEntry->
            bDot11QAPEDCATableMandatory == OSIX_FALSE)
        && (pWsscfgIsSetDot11QAPEDCAEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11PhyOperationTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11PhyOperationEntry
                pWsscfgSetDot11PhyOperationEntry
                pWsscfgIsSetDot11PhyOperationEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
Dot11PhyOperationTableFilterInputs (tWsscfgDot11PhyOperationEntry *
                                    pWsscfgDot11PhyOperationEntry,
                                    tWsscfgDot11PhyOperationEntry *
                                    pWsscfgSetDot11PhyOperationEntry,
                                    tWsscfgIsSetDot11PhyOperationEntry *
                                    pWsscfgIsSetDot11PhyOperationEntry)
{
    if (pWsscfgIsSetDot11PhyOperationEntry->bDot11CurrentRegDomain == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyOperationEntry->
            MibObject.i4Dot11CurrentRegDomain ==
            pWsscfgSetDot11PhyOperationEntry->MibObject.i4Dot11CurrentRegDomain)
            pWsscfgIsSetDot11PhyOperationEntry->
                bDot11CurrentRegDomain = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyOperationEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyOperationEntry->MibObject.i4IfIndex ==
            pWsscfgSetDot11PhyOperationEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11PhyOperationEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11PhyOperationEntry->bDot11CurrentRegDomain ==
         OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyOperationEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11PhyAntennaTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11PhyAntennaEntry
                pWsscfgSetDot11PhyAntennaEntry
                pWsscfgIsSetDot11PhyAntennaEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
Dot11PhyAntennaTableFilterInputs (tWsscfgDot11PhyAntennaEntry *
                                  pWsscfgDot11PhyAntennaEntry,
                                  tWsscfgDot11PhyAntennaEntry *
                                  pWsscfgSetDot11PhyAntennaEntry,
                                  tWsscfgIsSetDot11PhyAntennaEntry *
                                  pWsscfgIsSetDot11PhyAntennaEntry)
{
    if (pWsscfgIsSetDot11PhyAntennaEntry->bDot11CurrentTxAntenna == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyAntennaEntry->
            MibObject.i4Dot11CurrentTxAntenna ==
            pWsscfgSetDot11PhyAntennaEntry->MibObject.i4Dot11CurrentTxAntenna)
            pWsscfgIsSetDot11PhyAntennaEntry->bDot11CurrentTxAntenna =
                OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyAntennaEntry->bDot11CurrentRxAntenna == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyAntennaEntry->
            MibObject.i4Dot11CurrentRxAntenna ==
            pWsscfgSetDot11PhyAntennaEntry->MibObject.i4Dot11CurrentRxAntenna)
            pWsscfgIsSetDot11PhyAntennaEntry->bDot11CurrentRxAntenna =
                OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyAntennaEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyAntennaEntry->MibObject.i4IfIndex ==
            pWsscfgSetDot11PhyAntennaEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11PhyAntennaEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11PhyAntennaEntry->bDot11CurrentTxAntenna ==
         OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyAntennaEntry->
            bDot11CurrentRxAntenna == OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyAntennaEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11PhyTxPowerTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11PhyTxPowerEntry
                pWsscfgSetDot11PhyTxPowerEntry
                pWsscfgIsSetDot11PhyTxPowerEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
Dot11PhyTxPowerTableFilterInputs (tWsscfgDot11PhyTxPowerEntry *
                                  pWsscfgDot11PhyTxPowerEntry,
                                  tWsscfgDot11PhyTxPowerEntry *
                                  pWsscfgSetDot11PhyTxPowerEntry,
                                  tWsscfgIsSetDot11PhyTxPowerEntry *
                                  pWsscfgIsSetDot11PhyTxPowerEntry)
{
    if (pWsscfgIsSetDot11PhyTxPowerEntry->bDot11CurrentTxPowerLevel ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11PhyTxPowerEntry->
            MibObject.i4Dot11CurrentTxPowerLevel ==
            pWsscfgSetDot11PhyTxPowerEntry->
            MibObject.i4Dot11CurrentTxPowerLevel)
            pWsscfgIsSetDot11PhyTxPowerEntry->
                bDot11CurrentTxPowerLevel = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyTxPowerEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyTxPowerEntry->MibObject.i4IfIndex ==
            pWsscfgSetDot11PhyTxPowerEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11PhyTxPowerEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11PhyTxPowerEntry->
         bDot11CurrentTxPowerLevel == OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyTxPowerEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11PhyFHSSTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11PhyFHSSEntry
                pWsscfgSetDot11PhyFHSSEntry
                pWsscfgIsSetDot11PhyFHSSEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
Dot11PhyFHSSTableFilterInputs (tWsscfgDot11PhyFHSSEntry *
                               pWsscfgDot11PhyFHSSEntry,
                               tWsscfgDot11PhyFHSSEntry *
                               pWsscfgSetDot11PhyFHSSEntry,
                               tWsscfgIsSetDot11PhyFHSSEntry *
                               pWsscfgIsSetDot11PhyFHSSEntry)
{
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentChannelNumber == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyFHSSEntry->
            MibObject.i4Dot11CurrentChannelNumber ==
            pWsscfgSetDot11PhyFHSSEntry->MibObject.i4Dot11CurrentChannelNumber)
            pWsscfgIsSetDot11PhyFHSSEntry->
                bDot11CurrentChannelNumber = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentDwellTime == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyFHSSEntry->MibObject.
            i4Dot11CurrentDwellTime ==
            pWsscfgSetDot11PhyFHSSEntry->MibObject.i4Dot11CurrentDwellTime)
            pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentDwellTime = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentSet == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11CurrentSet ==
            pWsscfgSetDot11PhyFHSSEntry->MibObject.i4Dot11CurrentSet)
            pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentSet = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentPattern == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyFHSSEntry->MibObject.
            i4Dot11CurrentPattern ==
            pWsscfgSetDot11PhyFHSSEntry->MibObject.i4Dot11CurrentPattern)
            pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentPattern = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11CurrentIndex ==
            pWsscfgSetDot11PhyFHSSEntry->MibObject.i4Dot11CurrentIndex)
            pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentIndex = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCPrimeRadix == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyFHSSEntry->MibObject.
            i4Dot11EHCCPrimeRadix ==
            pWsscfgSetDot11PhyFHSSEntry->MibObject.i4Dot11EHCCPrimeRadix)
            pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCPrimeRadix = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCNumberofChannelsFamilyIndex ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11PhyFHSSEntry->
            MibObject.i4Dot11EHCCNumberofChannelsFamilyIndex ==
            pWsscfgSetDot11PhyFHSSEntry->
            MibObject.i4Dot11EHCCNumberofChannelsFamilyIndex)
            pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCNumberofChannelsFamilyIndex
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->
        bDot11EHCCCapabilityImplemented == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyFHSSEntry->
            MibObject.i4Dot11EHCCCapabilityImplemented ==
            pWsscfgSetDot11PhyFHSSEntry->
            MibObject.i4Dot11EHCCCapabilityImplemented)
            pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCCapabilityImplemented
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCCapabilityEnabled == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyFHSSEntry->
            MibObject.i4Dot11EHCCCapabilityEnabled ==
            pWsscfgSetDot11PhyFHSSEntry->MibObject.i4Dot11EHCCCapabilityEnabled)
            pWsscfgIsSetDot11PhyFHSSEntry->
                bDot11EHCCCapabilityEnabled = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11HopAlgorithmAdopted == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyFHSSEntry->
            MibObject.i4Dot11HopAlgorithmAdopted ==
            pWsscfgSetDot11PhyFHSSEntry->MibObject.i4Dot11HopAlgorithmAdopted)
            pWsscfgIsSetDot11PhyFHSSEntry->bDot11HopAlgorithmAdopted =
                OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11RandomTableFlag == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyFHSSEntry->MibObject.
            i4Dot11RandomTableFlag ==
            pWsscfgSetDot11PhyFHSSEntry->MibObject.i4Dot11RandomTableFlag)
            pWsscfgIsSetDot11PhyFHSSEntry->bDot11RandomTableFlag = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11HopOffset == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11HopOffset ==
            pWsscfgSetDot11PhyFHSSEntry->MibObject.i4Dot11HopOffset)
            pWsscfgIsSetDot11PhyFHSSEntry->bDot11HopOffset = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyFHSSEntry->MibObject.i4IfIndex ==
            pWsscfgSetDot11PhyFHSSEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11PhyFHSSEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentChannelNumber ==
         OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentDwellTime ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentSet ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentPattern ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentIndex ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCPrimeRadix ==
            OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCNumberofChannelsFamilyIndex
         == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCCapabilityImplemented
         == OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyFHSSEntry->
            bDot11EHCCCapabilityEnabled == OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyFHSSEntry->
            bDot11HopAlgorithmAdopted == OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyFHSSEntry->bDot11RandomTableFlag ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyFHSSEntry->bDot11HopOffset ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyFHSSEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11PhyDSSSTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11PhyDSSSEntry
                pWsscfgSetDot11PhyDSSSEntry
                pWsscfgIsSetDot11PhyDSSSEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
Dot11PhyDSSSTableFilterInputs (tWsscfgDot11PhyDSSSEntry *
                               pWsscfgDot11PhyDSSSEntry,
                               tWsscfgDot11PhyDSSSEntry *
                               pWsscfgSetDot11PhyDSSSEntry,
                               tWsscfgIsSetDot11PhyDSSSEntry *
                               pWsscfgIsSetDot11PhyDSSSEntry)
{
    if (pWsscfgIsSetDot11PhyDSSSEntry->bDot11CurrentChannel == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyDSSSEntry->MibObject.
            i4Dot11CurrentChannel ==
            pWsscfgSetDot11PhyDSSSEntry->MibObject.i4Dot11CurrentChannel)
            pWsscfgIsSetDot11PhyDSSSEntry->bDot11CurrentChannel = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyDSSSEntry->bDot11CurrentCCAMode == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyDSSSEntry->MibObject.
            i4Dot11CurrentCCAMode ==
            pWsscfgSetDot11PhyDSSSEntry->MibObject.i4Dot11CurrentCCAMode)
            pWsscfgIsSetDot11PhyDSSSEntry->bDot11CurrentCCAMode = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyDSSSEntry->bDot11EDThreshold == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyDSSSEntry->MibObject.i4Dot11EDThreshold ==
            pWsscfgSetDot11PhyDSSSEntry->MibObject.i4Dot11EDThreshold)
            pWsscfgIsSetDot11PhyDSSSEntry->bDot11EDThreshold = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyDSSSEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyDSSSEntry->MibObject.i4IfIndex ==
            pWsscfgSetDot11PhyDSSSEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11PhyDSSSEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11PhyDSSSEntry->bDot11CurrentChannel ==
         OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyDSSSEntry->bDot11CurrentCCAMode ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyDSSSEntry->bDot11EDThreshold ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyDSSSEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11PhyIRTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11PhyIREntry
                pWsscfgSetDot11PhyIREntry
                pWsscfgIsSetDot11PhyIREntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
Dot11PhyIRTableFilterInputs (tWsscfgDot11PhyIREntry * pWsscfgDot11PhyIREntry,
                             tWsscfgDot11PhyIREntry * pWsscfgSetDot11PhyIREntry,
                             tWsscfgIsSetDot11PhyIREntry *
                             pWsscfgIsSetDot11PhyIREntry)
{
    if (pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogTimerMax == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyIREntry->MibObject.
            i4Dot11CCAWatchdogTimerMax ==
            pWsscfgSetDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogTimerMax)
            pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogTimerMax = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogCountMax == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyIREntry->MibObject.
            i4Dot11CCAWatchdogCountMax ==
            pWsscfgSetDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogCountMax)
            pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogCountMax = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogTimerMin == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyIREntry->MibObject.
            i4Dot11CCAWatchdogTimerMin ==
            pWsscfgSetDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogTimerMin)
            pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogTimerMin = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogCountMin == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyIREntry->MibObject.
            i4Dot11CCAWatchdogCountMin ==
            pWsscfgSetDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogCountMin)
            pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogCountMin = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyIREntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyIREntry->MibObject.i4IfIndex ==
            pWsscfgSetDot11PhyIREntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11PhyIREntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogTimerMax ==
         OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogCountMax ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogTimerMin ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogCountMin ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyIREntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11AntennasListTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11AntennasListEntry
                pWsscfgSetDot11AntennasListEntry
                pWsscfgIsSetDot11AntennasListEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
Dot11AntennasListTableFilterInputs (tWsscfgDot11AntennasListEntry *
                                    pWsscfgDot11AntennasListEntry,
                                    tWsscfgDot11AntennasListEntry *
                                    pWsscfgSetDot11AntennasListEntry,
                                    tWsscfgIsSetDot11AntennasListEntry *
                                    pWsscfgIsSetDot11AntennasListEntry)
{
    if (pWsscfgIsSetDot11AntennasListEntry->bDot11AntennaListIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11AntennasListEntry->
            MibObject.i4Dot11AntennaListIndex ==
            pWsscfgSetDot11AntennasListEntry->MibObject.i4Dot11AntennaListIndex)
            pWsscfgIsSetDot11AntennasListEntry->
                bDot11AntennaListIndex = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11AntennasListEntry->
        bDot11SupportedTxAntenna == OSIX_TRUE)
    {
        if (pWsscfgDot11AntennasListEntry->
            MibObject.i4Dot11SupportedTxAntenna ==
            pWsscfgSetDot11AntennasListEntry->
            MibObject.i4Dot11SupportedTxAntenna)
            pWsscfgIsSetDot11AntennasListEntry->
                bDot11SupportedTxAntenna = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11AntennasListEntry->
        bDot11SupportedRxAntenna == OSIX_TRUE)
    {
        if (pWsscfgDot11AntennasListEntry->
            MibObject.i4Dot11SupportedRxAntenna ==
            pWsscfgSetDot11AntennasListEntry->
            MibObject.i4Dot11SupportedRxAntenna)
            pWsscfgIsSetDot11AntennasListEntry->
                bDot11SupportedRxAntenna = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11AntennasListEntry->
        bDot11DiversitySelectionRx == OSIX_TRUE)
    {
        if (pWsscfgDot11AntennasListEntry->
            MibObject.i4Dot11DiversitySelectionRx ==
            pWsscfgSetDot11AntennasListEntry->
            MibObject.i4Dot11DiversitySelectionRx)
            pWsscfgIsSetDot11AntennasListEntry->bDot11DiversitySelectionRx
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11AntennasListEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11AntennasListEntry->MibObject.i4IfIndex ==
            pWsscfgSetDot11AntennasListEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11AntennasListEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11AntennasListEntry->bDot11AntennaListIndex ==
         OSIX_FALSE)
        && (pWsscfgIsSetDot11AntennasListEntry->
            bDot11SupportedTxAntenna == OSIX_FALSE)
        && (pWsscfgIsSetDot11AntennasListEntry->
            bDot11SupportedRxAntenna == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11AntennasListEntry->bDot11DiversitySelectionRx
         == OSIX_FALSE)
        && (pWsscfgIsSetDot11AntennasListEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11PhyOFDMTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11PhyOFDMEntry
                pWsscfgSetDot11PhyOFDMEntry
                pWsscfgIsSetDot11PhyOFDMEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
Dot11PhyOFDMTableFilterInputs (tWsscfgDot11PhyOFDMEntry *
                               pWsscfgDot11PhyOFDMEntry,
                               tWsscfgDot11PhyOFDMEntry *
                               pWsscfgSetDot11PhyOFDMEntry,
                               tWsscfgIsSetDot11PhyOFDMEntry *
                               pWsscfgIsSetDot11PhyOFDMEntry)
{
    if (pWsscfgIsSetDot11PhyOFDMEntry->bDot11CurrentFrequency == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyOFDMEntry->MibObject.
            i4Dot11CurrentFrequency ==
            pWsscfgSetDot11PhyOFDMEntry->MibObject.i4Dot11CurrentFrequency)
            pWsscfgIsSetDot11PhyOFDMEntry->bDot11CurrentFrequency = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyOFDMEntry->bDot11TIThreshold == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyOFDMEntry->MibObject.i4Dot11TIThreshold ==
            pWsscfgSetDot11PhyOFDMEntry->MibObject.i4Dot11TIThreshold)
            pWsscfgIsSetDot11PhyOFDMEntry->bDot11TIThreshold = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyOFDMEntry->bDot11ChannelStartingFactor == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyOFDMEntry->
            MibObject.i4Dot11ChannelStartingFactor ==
            pWsscfgSetDot11PhyOFDMEntry->MibObject.i4Dot11ChannelStartingFactor)
            pWsscfgIsSetDot11PhyOFDMEntry->
                bDot11ChannelStartingFactor = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyOFDMEntry->bDot11PhyOFDMChannelWidth == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyOFDMEntry->
            MibObject.i4Dot11PhyOFDMChannelWidth ==
            pWsscfgSetDot11PhyOFDMEntry->MibObject.i4Dot11PhyOFDMChannelWidth)
            pWsscfgIsSetDot11PhyOFDMEntry->bDot11PhyOFDMChannelWidth =
                OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyOFDMEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyOFDMEntry->MibObject.i4IfIndex ==
            pWsscfgSetDot11PhyOFDMEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11PhyOFDMEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11PhyOFDMEntry->bDot11CurrentFrequency ==
         OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyOFDMEntry->bDot11TIThreshold ==
            OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyOFDMEntry->
            bDot11ChannelStartingFactor == OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyOFDMEntry->
            bDot11PhyOFDMChannelWidth == OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyOFDMEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11HoppingPatternTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11HoppingPatternEntry
                pWsscfgSetDot11HoppingPatternEntry
                pWsscfgIsSetDot11HoppingPatternEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
Dot11HoppingPatternTableFilterInputs (tWsscfgDot11HoppingPatternEntry *
                                      pWsscfgDot11HoppingPatternEntry,
                                      tWsscfgDot11HoppingPatternEntry *
                                      pWsscfgSetDot11HoppingPatternEntry,
                                      tWsscfgIsSetDot11HoppingPatternEntry *
                                      pWsscfgIsSetDot11HoppingPatternEntry)
{
    if (pWsscfgIsSetDot11HoppingPatternEntry->
        bDot11HoppingPatternIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11HoppingPatternEntry->
            MibObject.i4Dot11HoppingPatternIndex ==
            pWsscfgSetDot11HoppingPatternEntry->
            MibObject.i4Dot11HoppingPatternIndex)
            pWsscfgIsSetDot11HoppingPatternEntry->bDot11HoppingPatternIndex
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11HoppingPatternEntry->bDot11RandomTableFieldNumber ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11HoppingPatternEntry->
            MibObject.i4Dot11RandomTableFieldNumber ==
            pWsscfgSetDot11HoppingPatternEntry->
            MibObject.i4Dot11RandomTableFieldNumber)
            pWsscfgIsSetDot11HoppingPatternEntry->bDot11RandomTableFieldNumber
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11HoppingPatternEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11HoppingPatternEntry->MibObject.i4IfIndex ==
            pWsscfgSetDot11HoppingPatternEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11HoppingPatternEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11HoppingPatternEntry->
         bDot11HoppingPatternIndex == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11HoppingPatternEntry->bDot11RandomTableFieldNumber
         == OSIX_FALSE)
        && (pWsscfgIsSetDot11HoppingPatternEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11PhyERPTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11PhyERPEntry
                pWsscfgSetDot11PhyERPEntry
                pWsscfgIsSetDot11PhyERPEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
Dot11PhyERPTableFilterInputs (tWsscfgDot11PhyERPEntry * pWsscfgDot11PhyERPEntry,
                              tWsscfgDot11PhyERPEntry *
                              pWsscfgSetDot11PhyERPEntry,
                              tWsscfgIsSetDot11PhyERPEntry *
                              pWsscfgIsSetDot11PhyERPEntry)
{
    if (pWsscfgIsSetDot11PhyERPEntry->bDot11ERPBCCOptionEnabled == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyERPEntry->
            MibObject.i4Dot11ERPBCCOptionEnabled ==
            pWsscfgSetDot11PhyERPEntry->MibObject.i4Dot11ERPBCCOptionEnabled)
            pWsscfgIsSetDot11PhyERPEntry->bDot11ERPBCCOptionEnabled =
                OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyERPEntry->bDot11DSSSOFDMOptionEnabled == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyERPEntry->
            MibObject.i4Dot11DSSSOFDMOptionEnabled ==
            pWsscfgSetDot11PhyERPEntry->MibObject.i4Dot11DSSSOFDMOptionEnabled)
            pWsscfgIsSetDot11PhyERPEntry->
                bDot11DSSSOFDMOptionEnabled = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyERPEntry->bDot11ShortSlotTimeOptionImplemented ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11PhyERPEntry->
            MibObject.i4Dot11ShortSlotTimeOptionImplemented ==
            pWsscfgSetDot11PhyERPEntry->
            MibObject.i4Dot11ShortSlotTimeOptionImplemented)
            pWsscfgIsSetDot11PhyERPEntry->bDot11ShortSlotTimeOptionImplemented
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyERPEntry->
        bDot11ShortSlotTimeOptionEnabled == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyERPEntry->
            MibObject.i4Dot11ShortSlotTimeOptionEnabled ==
            pWsscfgSetDot11PhyERPEntry->
            MibObject.i4Dot11ShortSlotTimeOptionEnabled)
            pWsscfgIsSetDot11PhyERPEntry->bDot11ShortSlotTimeOptionEnabled
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11PhyERPEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11PhyERPEntry->MibObject.i4IfIndex ==
            pWsscfgSetDot11PhyERPEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11PhyERPEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11PhyERPEntry->bDot11ERPBCCOptionEnabled ==
         OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyERPEntry->
            bDot11DSSSOFDMOptionEnabled == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11PhyERPEntry->bDot11ShortSlotTimeOptionImplemented
         == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11PhyERPEntry->bDot11ShortSlotTimeOptionEnabled
         == OSIX_FALSE)
        && (pWsscfgIsSetDot11PhyERPEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11RSNAConfigTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11RSNAConfigEntry
                pWsscfgSetDot11RSNAConfigEntry
                pWsscfgIsSetDot11RSNAConfigEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
Dot11RSNAConfigTableFilterInputs (tWsscfgDot11RSNAConfigEntry *
                                  pWsscfgDot11RSNAConfigEntry,
                                  tWsscfgDot11RSNAConfigEntry *
                                  pWsscfgSetDot11RSNAConfigEntry,
                                  tWsscfgIsSetDot11RSNAConfigEntry *
                                  pWsscfgIsSetDot11RSNAConfigEntry)
{
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigGroupCipher == OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgDot11RSNAConfigEntry->
              MibObject.au1Dot11RSNAConfigGroupCipher,
              pWsscfgSetDot11RSNAConfigEntry->
              MibObject.au1Dot11RSNAConfigGroupCipher,
              pWsscfgSetDot11RSNAConfigEntry->
              MibObject.i4Dot11RSNAConfigGroupCipherLen) == 0)
            && (pWsscfgDot11RSNAConfigEntry->
                MibObject.i4Dot11RSNAConfigGroupCipherLen ==
                pWsscfgSetDot11RSNAConfigEntry->
                MibObject.i4Dot11RSNAConfigGroupCipherLen))
            pWsscfgIsSetDot11RSNAConfigEntry->
                bDot11RSNAConfigGroupCipher = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyMethod ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAConfigGroupRekeyMethod ==
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAConfigGroupRekeyMethod)
            pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyMethod
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigGroupRekeyTime == OSIX_TRUE)
    {
        if (pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigGroupRekeyTime ==
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigGroupRekeyTime)
            pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyTime
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyPackets ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigGroupRekeyPackets ==
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigGroupRekeyPackets)
            pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyPackets
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyStrict ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAConfigGroupRekeyStrict ==
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAConfigGroupRekeyStrict)
            pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyStrict
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPSKValue == OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgDot11RSNAConfigEntry->
              MibObject.au1Dot11RSNAConfigPSKValue,
              pWsscfgSetDot11RSNAConfigEntry->
              MibObject.au1Dot11RSNAConfigPSKValue,
              pWsscfgSetDot11RSNAConfigEntry->
              MibObject.i4Dot11RSNAConfigPSKValueLen) == 0)
            && (pWsscfgDot11RSNAConfigEntry->
                MibObject.i4Dot11RSNAConfigPSKValueLen ==
                pWsscfgSetDot11RSNAConfigEntry->
                MibObject.i4Dot11RSNAConfigPSKValueLen))
            pWsscfgIsSetDot11RSNAConfigEntry->
                bDot11RSNAConfigPSKValue = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigPSKPassPhrase == OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgDot11RSNAConfigEntry->
              MibObject.au1Dot11RSNAConfigPSKPassPhrase,
              pWsscfgSetDot11RSNAConfigEntry->
              MibObject.au1Dot11RSNAConfigPSKPassPhrase,
              pWsscfgSetDot11RSNAConfigEntry->
              MibObject.i4Dot11RSNAConfigPSKPassPhraseLen) == 0)
            && (pWsscfgDot11RSNAConfigEntry->
                MibObject.i4Dot11RSNAConfigPSKPassPhraseLen ==
                pWsscfgSetDot11RSNAConfigEntry->
                MibObject.i4Dot11RSNAConfigPSKPassPhraseLen))
            pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPSKPassPhrase
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupUpdateCount ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigGroupUpdateCount ==
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigGroupUpdateCount)
            pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupUpdateCount
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPairwiseUpdateCount ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigPairwiseUpdateCount ==
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigPairwiseUpdateCount)
            pWsscfgIsSetDot11RSNAConfigEntry->
                bDot11RSNAConfigPairwiseUpdateCount = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigPMKLifetime == OSIX_TRUE)
    {
        if (pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigPMKLifetime ==
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigPMKLifetime)
            pWsscfgIsSetDot11RSNAConfigEntry->
                bDot11RSNAConfigPMKLifetime = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPMKReauthThreshold ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigPMKReauthThreshold ==
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigPMKReauthThreshold)
            pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPMKReauthThreshold
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigSATimeout ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigSATimeout ==
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigSATimeout)
            pWsscfgIsSetDot11RSNAConfigEntry->
                bDot11RSNAConfigSATimeout = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNATKIPCounterMeasuresInvoked == OSIX_TRUE)
    {
        if (pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNATKIPCounterMeasuresInvoked ==
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNATKIPCounterMeasuresInvoked)
            pWsscfgIsSetDot11RSNAConfigEntry->
                bDot11RSNATKIPCounterMeasuresInvoked = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNA4WayHandshakeFailures ==
        OSIX_TRUE)
    {
        if (pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNA4WayHandshakeFailures ==
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNA4WayHandshakeFailures)
            pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNA4WayHandshakeFailures
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigSTKRekeyTime == OSIX_TRUE)
    {
        if (pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigSTKRekeyTime ==
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigSTKRekeyTime)
            pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigSTKRekeyTime
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigSMKUpdateCount == OSIX_TRUE)
    {
        if (pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigSMKUpdateCount ==
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigSMKUpdateCount)
            pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigSMKUpdateCount
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigSMKLifetime == OSIX_TRUE)
    {
        if (pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigSMKLifetime ==
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigSMKLifetime)
            pWsscfgIsSetDot11RSNAConfigEntry->
                bDot11RSNAConfigSMKLifetime = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNASMKHandshakeFailures == OSIX_TRUE)
    {
        if (pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNASMKHandshakeFailures ==
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNASMKHandshakeFailures)
            pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNASMKHandshakeFailures
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11RSNAConfigEntry->MibObject.i4IfIndex ==
            pWsscfgSetDot11RSNAConfigEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11RSNAConfigEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11RSNAConfigEntry->
         bDot11RSNAConfigGroupCipher == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyMethod
         == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyTime
         == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyPackets
         == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyStrict
         == OSIX_FALSE)
        && (pWsscfgIsSetDot11RSNAConfigEntry->
            bDot11RSNAConfigPSKValue == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPSKPassPhrase
         == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupUpdateCount
         == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPairwiseUpdateCount
         == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPMKLifetime
         == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPMKReauthThreshold
         == OSIX_FALSE)
        && (pWsscfgIsSetDot11RSNAConfigEntry->
            bDot11RSNAConfigSATimeout == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNATKIPCounterMeasuresInvoked
         == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNA4WayHandshakeFailures
         == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigSTKRekeyTime
         == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigSMKUpdateCount
         == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigSMKLifetime
         == OSIX_FALSE)
        &&
        (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNASMKHandshakeFailures
         == OSIX_FALSE)
        && (pWsscfgIsSetDot11RSNAConfigEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11RSNAConfigPairwiseCiphersTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11RSNAConfigPairwiseCiphersEntry
                pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry
                pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    Dot11RSNAConfigPairwiseCiphersTableFilterInputs
    (tWsscfgDot11RSNAConfigPairwiseCiphersEntry *
     pWsscfgDot11RSNAConfigPairwiseCiphersEntry,
     tWsscfgDot11RSNAConfigPairwiseCiphersEntry *
     pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry,
     tWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry *
     pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry)
{
    if (pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry->
        bDot11RSNAConfigPairwiseCipherIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11RSNAConfigPairwiseCiphersEntry->
            MibObject.u4Dot11RSNAConfigPairwiseCipherIndex ==
            pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry->
            MibObject.u4Dot11RSNAConfigPairwiseCipherIndex)
            pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry->
                bDot11RSNAConfigPairwiseCipherIndex = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry->
        bDot11RSNAConfigPairwiseCipherEnabled == OSIX_TRUE)
    {
        if (pWsscfgDot11RSNAConfigPairwiseCiphersEntry->
            MibObject.i4Dot11RSNAConfigPairwiseCipherEnabled ==
            pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry->
            MibObject.i4Dot11RSNAConfigPairwiseCipherEnabled)
            pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry->
                bDot11RSNAConfigPairwiseCipherEnabled = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11RSNAConfigPairwiseCiphersEntry->
            MibObject.i4IfIndex ==
            pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry->MibObject.i4IfIndex)
            pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry->
                bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry->
         bDot11RSNAConfigPairwiseCipherIndex == OSIX_FALSE)
        && (pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry->
            bDot11RSNAConfigPairwiseCipherEnabled == OSIX_FALSE)
        && (pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry->bIfIndex ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  Dot11RSNAConfigAuthenticationSuitesTableFilterInputs
 Input       :  The Indices
                pWsscfgDot11RSNAConfigAuthenticationSuitesEntry
                pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry
                pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    Dot11RSNAConfigAuthenticationSuitesTableFilterInputs
    (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *
     pWsscfgDot11RSNAConfigAuthenticationSuitesEntry,
     tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *
     pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry,
     tWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry *
     pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry)
{
    if (pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry->
        bDot11RSNAConfigAuthenticationSuiteIndex == OSIX_TRUE)
    {
        if (pWsscfgDot11RSNAConfigAuthenticationSuitesEntry->
            MibObject.u4Dot11RSNAConfigAuthenticationSuiteIndex ==
            pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry->
            MibObject.u4Dot11RSNAConfigAuthenticationSuiteIndex)
            pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry->
                bDot11RSNAConfigAuthenticationSuiteIndex = OSIX_FALSE;
    }
    if (pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry->
        bDot11RSNAConfigAuthenticationSuiteEnabled == OSIX_TRUE)
    {
        if (pWsscfgDot11RSNAConfigAuthenticationSuitesEntry->
            MibObject.i4Dot11RSNAConfigAuthenticationSuiteEnabled ==
            pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry->
            MibObject.i4Dot11RSNAConfigAuthenticationSuiteEnabled)
            pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry->
                bDot11RSNAConfigAuthenticationSuiteEnabled = OSIX_FALSE;
    }
    if ((pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry->
         bDot11RSNAConfigAuthenticationSuiteIndex == OSIX_FALSE)
        && (pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry->
            bDot11RSNAConfigAuthenticationSuiteEnabled == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11StationConfigTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11StationConfigEntry
                pWsscfgIsSetDot11StationConfigEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllDot11StationConfigTableTrigger (tWsscfgDot11StationConfigEntry *
                                            pWsscfgSetDot11StationConfigEntry,
                                            tWsscfgIsSetDot11StationConfigEntry
                                            *
                                            pWsscfgIsSetDot11StationConfigEntry,
                                            INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE Dot11DesiredSSIDVal;
    UINT1               au1Dot11DesiredSSIDVal[256];
    tSNMP_OCTET_STRING_TYPE Dot11OperationalRateSetVal;
    UINT1               au1Dot11OperationalRateSetVal[256];

    MEMSET (au1Dot11DesiredSSIDVal, 0, sizeof (au1Dot11DesiredSSIDVal));
    Dot11DesiredSSIDVal.pu1_OctetList = au1Dot11DesiredSSIDVal;
    Dot11DesiredSSIDVal.i4_Length = 0;

    MEMSET (au1Dot11OperationalRateSetVal, 0,
            sizeof (au1Dot11OperationalRateSetVal));
    Dot11OperationalRateSetVal.pu1_OctetList = au1Dot11OperationalRateSetVal;
    Dot11OperationalRateSetVal.i4_Length = 0;

    if (pWsscfgIsSetDot11StationConfigEntry->bDot11StationID == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11StationID, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 1, 0, 1, i4SetOption,
                      "%i %m",
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.Dot11StationID);
    }
    if (pWsscfgIsSetDot11StationConfigEntry->
        bDot11MediumOccupancyLimit == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11MediumOccupancyLimit, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4Dot11MediumOccupancyLimit);
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11CFPPeriod == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11CFPPeriod, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4Dot11CFPPeriod);
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11CFPMaxDuration == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11CFPMaxDuration, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4Dot11CFPMaxDuration);
    }
    if (pWsscfgIsSetDot11StationConfigEntry->
        bDot11AuthenticationResponseTimeOut == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11AuthenticationResponseTimeOut, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %u",
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.u4Dot11AuthenticationResponseTimeOut);
    }
    if (pWsscfgIsSetDot11StationConfigEntry->
        bDot11PowerManagementMode == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11PowerManagementMode, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4Dot11PowerManagementMode);
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11DesiredSSID == OSIX_TRUE)
    {
        MEMCPY (Dot11DesiredSSIDVal.pu1_OctetList,
                pWsscfgSetDot11StationConfigEntry->
                MibObject.au1Dot11DesiredSSID,
                pWsscfgSetDot11StationConfigEntry->
                MibObject.i4Dot11DesiredSSIDLen);
        Dot11DesiredSSIDVal.i4_Length =
            pWsscfgSetDot11StationConfigEntry->MibObject.i4Dot11DesiredSSIDLen;

        nmhSetCmnNew (Dot11DesiredSSID, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %s",
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4IfIndex, &Dot11DesiredSSIDVal);
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11DesiredBSSType == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11DesiredBSSType, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4Dot11DesiredBSSType);
    }
    if (pWsscfgIsSetDot11StationConfigEntry->
        bDot11OperationalRateSet == OSIX_TRUE)
    {
        MEMCPY (Dot11OperationalRateSetVal.pu1_OctetList,
                pWsscfgSetDot11StationConfigEntry->
                MibObject.au1Dot11OperationalRateSet,
                pWsscfgSetDot11StationConfigEntry->
                MibObject.i4Dot11OperationalRateSetLen);
        Dot11OperationalRateSetVal.i4_Length =
            pWsscfgSetDot11StationConfigEntry->
            MibObject.i4Dot11OperationalRateSetLen;

        nmhSetCmnNew (Dot11OperationalRateSet, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %s",
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4IfIndex, &Dot11OperationalRateSetVal);
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11BeaconPeriod == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11BeaconPeriod, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4Dot11BeaconPeriod);
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11DTIMPeriod == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11DTIMPeriod, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4Dot11DTIMPeriod);
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11AssociationResponseTimeOut ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11AssociationResponseTimeOut, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %u",
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.u4Dot11AssociationResponseTimeOut);
    }
    if (pWsscfgIsSetDot11StationConfigEntry->
        bDot11MultiDomainCapabilityImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11MultiDomainCapabilityImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4Dot11MultiDomainCapabilityImplemented);
    }
    if (pWsscfgIsSetDot11StationConfigEntry->
        bDot11MultiDomainCapabilityEnabled == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11MultiDomainCapabilityActivated, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4Dot11MultiDomainCapabilityEnabled);
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11SpectrumManagementRequired ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11SpectrumManagementRequired, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4Dot11SpectrumManagementRequired);
    }
    if (pWsscfgIsSetDot11StationConfigEntry->
        bDot11RegulatoryClassesImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11OperatingClassesImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4Dot11RegulatoryClassesImplemented);
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11RegulatoryClassesRequired ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11OperatingClassesRequired, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4Dot11RegulatoryClassesRequired);
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11AssociateinNQBSS ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11AssociateInNQBSS, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4Dot11AssociateinNQBSS);
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11DLSAllowedInQBSS ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11DLSAllowedInQBSS, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4Dot11DLSAllowedInQBSS);
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bDot11DLSAllowed == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11DLSAllowed, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4Dot11DLSAllowed);
    }
    if (pWsscfgIsSetDot11StationConfigEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11StationConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11StationConfigEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11AuthenticationAlgorithmsTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11AuthenticationAlgorithmsEntry
                pWsscfgIsSetDot11AuthenticationAlgorithmsEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgSetAllDot11AuthenticationAlgorithmsTableTrigger
    (tWsscfgDot11AuthenticationAlgorithmsEntry *
     pWsscfgSetDot11AuthenticationAlgorithmsEntry,
     tWsscfgIsSetDot11AuthenticationAlgorithmsEntry *
     pWsscfgIsSetDot11AuthenticationAlgorithmsEntry, INT4 i4SetOption)
{

    if (pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->
        bDot11AuthenticationAlgorithmsIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11AuthenticationAlgorithmsIndex, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      2, i4SetOption, "%i %i %i",
                      pWsscfgSetDot11AuthenticationAlgorithmsEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11AuthenticationAlgorithmsEntry->MibObject.
                      i4Dot11AuthenticationAlgorithmsIndex,
                      pWsscfgSetDot11AuthenticationAlgorithmsEntry->MibObject.
                      i4Dot11AuthenticationAlgorithmsIndex);
    }
    if (pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->
        bDot11AuthenticationAlgorithmsEnable == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11AuthenticationAlgorithmsActivated, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      2, i4SetOption, "%i %i %i",
                      pWsscfgSetDot11AuthenticationAlgorithmsEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11AuthenticationAlgorithmsEntry->MibObject.
                      i4Dot11AuthenticationAlgorithmsIndex,
                      pWsscfgSetDot11AuthenticationAlgorithmsEntry->MibObject.
                      i4Dot11AuthenticationAlgorithmsEnable);
    }
    if (pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11AuthenticationAlgorithmsEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11AuthenticationAlgorithmsEntry->MibObject.
                      i4Dot11AuthenticationAlgorithmsIndex,
                      pWsscfgSetDot11AuthenticationAlgorithmsEntry->MibObject.
                      i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11WEPDefaultKeysTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11WEPDefaultKeysEntry
                pWsscfgIsSetDot11WEPDefaultKeysEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllDot11WEPDefaultKeysTableTrigger (tWsscfgDot11WEPDefaultKeysEntry *
                                             pWsscfgSetDot11WEPDefaultKeysEntry,
                                             tWsscfgIsSetDot11WEPDefaultKeysEntry
                                             *
                                             pWsscfgIsSetDot11WEPDefaultKeysEntry,
                                             INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE Dot11WEPDefaultKeyValueVal;
    UINT1               au1Dot11WEPDefaultKeyValueVal[256];

    MEMSET (au1Dot11WEPDefaultKeyValueVal, 0,
            sizeof (au1Dot11WEPDefaultKeyValueVal));
    Dot11WEPDefaultKeyValueVal.pu1_OctetList = au1Dot11WEPDefaultKeyValueVal;
    Dot11WEPDefaultKeyValueVal.i4_Length = 0;

    if (pWsscfgIsSetDot11WEPDefaultKeysEntry->
        bDot11WEPDefaultKeyIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11WEPDefaultKeyIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11WEPDefaultKeysEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11WEPDefaultKeysEntry->
                      MibObject.i4Dot11WEPDefaultKeyIndex,
                      pWsscfgSetDot11WEPDefaultKeysEntry->
                      MibObject.i4Dot11WEPDefaultKeyIndex);
    }
    if (pWsscfgIsSetDot11WEPDefaultKeysEntry->
        bDot11WEPDefaultKeyValue == OSIX_TRUE)
    {
        MEMCPY (Dot11WEPDefaultKeyValueVal.pu1_OctetList,
                pWsscfgSetDot11WEPDefaultKeysEntry->
                MibObject.au1Dot11WEPDefaultKeyValue,
                pWsscfgSetDot11WEPDefaultKeysEntry->
                MibObject.i4Dot11WEPDefaultKeyValueLen);
        Dot11WEPDefaultKeyValueVal.i4_Length =
            pWsscfgSetDot11WEPDefaultKeysEntry->
            MibObject.i4Dot11WEPDefaultKeyValueLen;

        nmhSetCmnNew (Dot11WEPDefaultKeyValue, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %s",
                      pWsscfgSetDot11WEPDefaultKeysEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11WEPDefaultKeysEntry->
                      MibObject.i4Dot11WEPDefaultKeyIndex,
                      &Dot11WEPDefaultKeyValueVal);
    }
    if (pWsscfgIsSetDot11WEPDefaultKeysEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11WEPDefaultKeysEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11WEPDefaultKeysEntry->
                      MibObject.i4Dot11WEPDefaultKeyIndex,
                      pWsscfgSetDot11WEPDefaultKeysEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11WEPKeyMappingsTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11WEPKeyMappingsEntry
                pWsscfgIsSetDot11WEPKeyMappingsEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllDot11WEPKeyMappingsTableTrigger (tWsscfgDot11WEPKeyMappingsEntry *
                                             pWsscfgSetDot11WEPKeyMappingsEntry,
                                             tWsscfgIsSetDot11WEPKeyMappingsEntry
                                             *
                                             pWsscfgIsSetDot11WEPKeyMappingsEntry,
                                             INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE Dot11WEPKeyMappingValueVal;
    UINT1               au1Dot11WEPKeyMappingValueVal[256];

    MEMSET (au1Dot11WEPKeyMappingValueVal, 0,
            sizeof (au1Dot11WEPKeyMappingValueVal));
    Dot11WEPKeyMappingValueVal.pu1_OctetList = au1Dot11WEPKeyMappingValueVal;
    Dot11WEPKeyMappingValueVal.i4_Length = 0;

    if (pWsscfgIsSetDot11WEPKeyMappingsEntry->
        bDot11WEPKeyMappingIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11WEPKeyMappingIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11WEPKeyMappingsEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11WEPKeyMappingsEntry->
                      MibObject.i4Dot11WEPKeyMappingIndex,
                      pWsscfgSetDot11WEPKeyMappingsEntry->
                      MibObject.i4Dot11WEPKeyMappingIndex);
    }
    if (pWsscfgIsSetDot11WEPKeyMappingsEntry->
        bDot11WEPKeyMappingAddress == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11WEPKeyMappingAddress, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %m",
                      pWsscfgSetDot11WEPKeyMappingsEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11WEPKeyMappingsEntry->
                      MibObject.i4Dot11WEPKeyMappingIndex,
                      pWsscfgSetDot11WEPKeyMappingsEntry->
                      MibObject.Dot11WEPKeyMappingAddress);
    }
    if (pWsscfgIsSetDot11WEPKeyMappingsEntry->
        bDot11WEPKeyMappingWEPOn == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11WEPKeyMappingWEPOn, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11WEPKeyMappingsEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11WEPKeyMappingsEntry->
                      MibObject.i4Dot11WEPKeyMappingIndex,
                      pWsscfgSetDot11WEPKeyMappingsEntry->
                      MibObject.i4Dot11WEPKeyMappingWEPOn);
    }
    if (pWsscfgIsSetDot11WEPKeyMappingsEntry->
        bDot11WEPKeyMappingValue == OSIX_TRUE)
    {
        MEMCPY (Dot11WEPKeyMappingValueVal.pu1_OctetList,
                pWsscfgSetDot11WEPKeyMappingsEntry->
                MibObject.au1Dot11WEPKeyMappingValue,
                pWsscfgSetDot11WEPKeyMappingsEntry->
                MibObject.i4Dot11WEPKeyMappingValueLen);
        Dot11WEPKeyMappingValueVal.i4_Length =
            pWsscfgSetDot11WEPKeyMappingsEntry->
            MibObject.i4Dot11WEPKeyMappingValueLen;

        nmhSetCmnNew (Dot11WEPKeyMappingValue, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %s",
                      pWsscfgSetDot11WEPKeyMappingsEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11WEPKeyMappingsEntry->
                      MibObject.i4Dot11WEPKeyMappingIndex,
                      &Dot11WEPKeyMappingValueVal);
    }
    if (pWsscfgIsSetDot11WEPKeyMappingsEntry->
        bDot11WEPKeyMappingStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11WEPKeyMappingStatus, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 1, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11WEPKeyMappingsEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11WEPKeyMappingsEntry->
                      MibObject.i4Dot11WEPKeyMappingIndex,
                      pWsscfgSetDot11WEPKeyMappingsEntry->
                      MibObject.i4Dot11WEPKeyMappingStatus);
    }
    if (pWsscfgIsSetDot11WEPKeyMappingsEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11WEPKeyMappingsEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11WEPKeyMappingsEntry->
                      MibObject.i4Dot11WEPKeyMappingIndex,
                      pWsscfgSetDot11WEPKeyMappingsEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11PrivacyTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11PrivacyEntry
                pWsscfgIsSetDot11PrivacyEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllDot11PrivacyTableTrigger (tWsscfgDot11PrivacyEntry *
                                      pWsscfgSetDot11PrivacyEntry,
                                      tWsscfgIsSetDot11PrivacyEntry *
                                      pWsscfgIsSetDot11PrivacyEntry,
                                      INT4 i4SetOption)
{

    if (pWsscfgIsSetDot11PrivacyEntry->bDot11PrivacyInvoked == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11PrivacyInvoked, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PrivacyEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PrivacyEntry->
                      MibObject.i4Dot11PrivacyInvoked);
    }
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11WEPDefaultKeyID == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11WEPDefaultKeyID, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PrivacyEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PrivacyEntry->
                      MibObject.i4Dot11WEPDefaultKeyID);
    }
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11WEPKeyMappingLength == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11WEPKeyMappingLengthImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0, 1,
                      i4SetOption, "%i %u",
                      pWsscfgSetDot11PrivacyEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PrivacyEntry->MibObject.
                      u4Dot11WEPKeyMappingLength);
    }
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11ExcludeUnencrypted == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11ExcludeUnencrypted, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PrivacyEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PrivacyEntry->
                      MibObject.i4Dot11ExcludeUnencrypted);
    }
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11RSNAEnabled == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RSNAActivated, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PrivacyEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PrivacyEntry->
                      MibObject.i4Dot11RSNAEnabled);
    }
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11RSNAPreauthenticationEnabled ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RSNAPreauthenticationActivated, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetDot11PrivacyEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PrivacyEntry->
                      MibObject.i4Dot11RSNAPreauthenticationEnabled);
    }
    if (pWsscfgIsSetDot11PrivacyEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PrivacyEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PrivacyEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11MultiDomainCapabilityTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11MultiDomainCapabilityEntry
                pWsscfgIsSetDot11MultiDomainCapabilityEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgSetAllDot11MultiDomainCapabilityTableTrigger
    (tWsscfgDot11MultiDomainCapabilityEntry *
     pWsscfgSetDot11MultiDomainCapabilityEntry,
     tWsscfgIsSetDot11MultiDomainCapabilityEntry *
     pWsscfgIsSetDot11MultiDomainCapabilityEntry, INT4 i4SetOption)
{

    if (pWsscfgIsSetDot11MultiDomainCapabilityEntry->
        bDot11MultiDomainCapabilityIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11MultiDomainCapabilityIndex, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      2, i4SetOption, "%i %i %i",
                      pWsscfgSetDot11MultiDomainCapabilityEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11MultiDomainCapabilityEntry->
                      MibObject.i4Dot11MultiDomainCapabilityIndex,
                      pWsscfgSetDot11MultiDomainCapabilityEntry->
                      MibObject.i4Dot11MultiDomainCapabilityIndex);
    }
    if (pWsscfgIsSetDot11MultiDomainCapabilityEntry->bDot11FirstChannelNumber ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11FirstChannelNumber, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11MultiDomainCapabilityEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11MultiDomainCapabilityEntry->
                      MibObject.i4Dot11MultiDomainCapabilityIndex,
                      pWsscfgSetDot11MultiDomainCapabilityEntry->
                      MibObject.i4Dot11FirstChannelNumber);
    }
    if (pWsscfgIsSetDot11MultiDomainCapabilityEntry->bDot11NumberofChannels ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11NumberofChannels, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11MultiDomainCapabilityEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11MultiDomainCapabilityEntry->
                      MibObject.i4Dot11MultiDomainCapabilityIndex,
                      pWsscfgSetDot11MultiDomainCapabilityEntry->
                      MibObject.i4Dot11NumberofChannels);
    }
    if (pWsscfgIsSetDot11MultiDomainCapabilityEntry->
        bDot11MaximumTransmitPowerLevel == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11MaximumTransmitPowerLevel, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      2, i4SetOption, "%i %i %i",
                      pWsscfgSetDot11MultiDomainCapabilityEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11MultiDomainCapabilityEntry->
                      MibObject.i4Dot11MultiDomainCapabilityIndex,
                      pWsscfgSetDot11MultiDomainCapabilityEntry->
                      MibObject.i4Dot11MaximumTransmitPowerLevel);
    }
    if (pWsscfgIsSetDot11MultiDomainCapabilityEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11MultiDomainCapabilityEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11MultiDomainCapabilityEntry->
                      MibObject.i4Dot11MultiDomainCapabilityIndex,
                      pWsscfgSetDot11MultiDomainCapabilityEntry->
                      MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11SpectrumManagementTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11SpectrumManagementEntry
                pWsscfgIsSetDot11SpectrumManagementEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgSetAllDot11SpectrumManagementTableTrigger
    (tWsscfgDot11SpectrumManagementEntry *
     pWsscfgSetDot11SpectrumManagementEntry,
     tWsscfgIsSetDot11SpectrumManagementEntry *
     pWsscfgIsSetDot11SpectrumManagementEntry, INT4 i4SetOption)
{

    if (pWsscfgIsSetDot11SpectrumManagementEntry->
        bDot11SpectrumManagementIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11SpectrumManagementIndex, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      2, i4SetOption, "%i %i %i",
                      pWsscfgSetDot11SpectrumManagementEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11SpectrumManagementEntry->
                      MibObject.i4Dot11SpectrumManagementIndex,
                      pWsscfgSetDot11SpectrumManagementEntry->
                      MibObject.i4Dot11SpectrumManagementIndex);
    }
    if (pWsscfgIsSetDot11SpectrumManagementEntry->bDot11MitigationRequirement ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11MitigationRequirement, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      2, i4SetOption, "%i %i %i",
                      pWsscfgSetDot11SpectrumManagementEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11SpectrumManagementEntry->
                      MibObject.i4Dot11SpectrumManagementIndex,
                      pWsscfgSetDot11SpectrumManagementEntry->
                      MibObject.i4Dot11MitigationRequirement);
    }
    if (pWsscfgIsSetDot11SpectrumManagementEntry->bDot11ChannelSwitchTime ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11ChannelSwitchTime, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11SpectrumManagementEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11SpectrumManagementEntry->
                      MibObject.i4Dot11SpectrumManagementIndex,
                      pWsscfgSetDot11SpectrumManagementEntry->
                      MibObject.i4Dot11ChannelSwitchTime);
    }
    if (pWsscfgIsSetDot11SpectrumManagementEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11SpectrumManagementEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11SpectrumManagementEntry->
                      MibObject.i4Dot11SpectrumManagementIndex,
                      pWsscfgSetDot11SpectrumManagementEntry->
                      MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11RegulatoryClassesTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11RegulatoryClassesEntry
                pWsscfgIsSetDot11RegulatoryClassesEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgSetAllDot11RegulatoryClassesTableTrigger
    (tWsscfgDot11RegulatoryClassesEntry * pWsscfgSetDot11RegulatoryClassesEntry,
     tWsscfgIsSetDot11RegulatoryClassesEntry *
     pWsscfgIsSetDot11RegulatoryClassesEntry, INT4 i4SetOption)
{

    if (pWsscfgIsSetDot11RegulatoryClassesEntry->bDot11RegulatoryClassesIndex ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11OperatingClassesIndex, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      2, i4SetOption, "%i %i %i",
                      pWsscfgSetDot11RegulatoryClassesEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11RegulatoryClassesEntry->
                      MibObject.i4Dot11RegulatoryClassesIndex,
                      pWsscfgSetDot11RegulatoryClassesEntry->
                      MibObject.i4Dot11RegulatoryClassesIndex);
    }
    if (pWsscfgIsSetDot11RegulatoryClassesEntry->
        bDot11RegulatoryClass == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11OperatingClass, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11RegulatoryClassesEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11RegulatoryClassesEntry->
                      MibObject.i4Dot11RegulatoryClassesIndex,
                      pWsscfgSetDot11RegulatoryClassesEntry->
                      MibObject.i4Dot11RegulatoryClass);
    }
    if (pWsscfgIsSetDot11RegulatoryClassesEntry->
        bDot11CoverageClass == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11CoverageClass, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11RegulatoryClassesEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11RegulatoryClassesEntry->
                      MibObject.i4Dot11RegulatoryClassesIndex,
                      pWsscfgSetDot11RegulatoryClassesEntry->
                      MibObject.i4Dot11CoverageClass);
    }
    if (pWsscfgIsSetDot11RegulatoryClassesEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11RegulatoryClassesEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11RegulatoryClassesEntry->
                      MibObject.i4Dot11RegulatoryClassesIndex,
                      pWsscfgSetDot11RegulatoryClassesEntry->
                      MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11OperationTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11OperationEntry
                pWsscfgIsSetDot11OperationEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllDot11OperationTableTrigger (tWsscfgDot11OperationEntry *
                                        pWsscfgSetDot11OperationEntry,
                                        tWsscfgIsSetDot11OperationEntry *
                                        pWsscfgIsSetDot11OperationEntry,
                                        INT4 i4SetOption)
{

    if (pWsscfgIsSetDot11OperationEntry->bDot11RTSThreshold == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RTSThreshold, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11OperationEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11OperationEntry->
                      MibObject.i4Dot11RTSThreshold);
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11ShortRetryLimit == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11ShortRetryLimit, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11OperationEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11OperationEntry->
                      MibObject.i4Dot11ShortRetryLimit);
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11LongRetryLimit == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11LongRetryLimit, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11OperationEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11OperationEntry->
                      MibObject.i4Dot11LongRetryLimit);
    }
    if (pWsscfgIsSetDot11OperationEntry->
        bDot11FragmentationThreshold == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11FragmentationThreshold, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetDot11OperationEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11OperationEntry->
                      MibObject.i4Dot11FragmentationThreshold);
    }
    if (pWsscfgIsSetDot11OperationEntry->
        bDot11MaxTransmitMSDULifetime == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11MaxTransmitMSDULifetime, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %u",
                      pWsscfgSetDot11OperationEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11OperationEntry->
                      MibObject.u4Dot11MaxTransmitMSDULifetime);
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11MaxReceiveLifetime == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11MaxReceiveLifetime, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %u",
                      pWsscfgSetDot11OperationEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11OperationEntry->
                      MibObject.u4Dot11MaxReceiveLifetime);
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11CAPLimit == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11CAPLimit, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11OperationEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11OperationEntry->MibObject.i4Dot11CAPLimit);
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11HCCWmin == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11HCCWmin, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11OperationEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11OperationEntry->MibObject.i4Dot11HCCWmin);
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11HCCWmax == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11HCCWmax, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11OperationEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11OperationEntry->MibObject.i4Dot11HCCWmax);
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11HCCAIFSN == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11HCCAIFSN, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11OperationEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11OperationEntry->MibObject.i4Dot11HCCAIFSN);
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11ADDBAResponseTimeout ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11ADDBAResponseTimeout, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11OperationEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11OperationEntry->
                      MibObject.i4Dot11ADDBAResponseTimeout);
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11ADDTSResponseTimeout ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11ADDTSResponseTimeout, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11OperationEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11OperationEntry->
                      MibObject.i4Dot11ADDTSResponseTimeout);
    }
    if (pWsscfgIsSetDot11OperationEntry->
        bDot11ChannelUtilizationBeaconInterval == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11ChannelUtilizationBeaconInterval, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetDot11OperationEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11OperationEntry->
                      MibObject.i4Dot11ChannelUtilizationBeaconInterval);
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11ScheduleTimeout == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11ScheduleTimeout, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11OperationEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11OperationEntry->
                      MibObject.i4Dot11ScheduleTimeout);
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11DLSResponseTimeout == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11DLSResponseTimeout, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11OperationEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11OperationEntry->
                      MibObject.i4Dot11DLSResponseTimeout);
    }
    if (pWsscfgIsSetDot11OperationEntry->
        bDot11QAPMissingAckRetryLimit == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11QAPMissingAckRetryLimit, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetDot11OperationEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11OperationEntry->
                      MibObject.i4Dot11QAPMissingAckRetryLimit);
    }
    if (pWsscfgIsSetDot11OperationEntry->bDot11EDCAAveragingPeriod == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11EDCAAveragingPeriod, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11OperationEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11OperationEntry->
                      MibObject.i4Dot11EDCAAveragingPeriod);
    }
    if (pWsscfgIsSetDot11OperationEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11OperationEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11OperationEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11GroupAddressesTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11GroupAddressesEntry
                pWsscfgIsSetDot11GroupAddressesEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllDot11GroupAddressesTableTrigger (tWsscfgDot11GroupAddressesEntry *
                                             pWsscfgSetDot11GroupAddressesEntry,
                                             tWsscfgIsSetDot11GroupAddressesEntry
                                             *
                                             pWsscfgIsSetDot11GroupAddressesEntry,
                                             INT4 i4SetOption)
{

    if (pWsscfgIsSetDot11GroupAddressesEntry->
        bDot11GroupAddressesIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11GroupAddressesIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11GroupAddressesEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11GroupAddressesEntry->
                      MibObject.i4Dot11GroupAddressesIndex,
                      pWsscfgSetDot11GroupAddressesEntry->
                      MibObject.i4Dot11GroupAddressesIndex);
    }
    if (pWsscfgIsSetDot11GroupAddressesEntry->bDot11Address == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11Address, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %m",
                      pWsscfgSetDot11GroupAddressesEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11GroupAddressesEntry->
                      MibObject.i4Dot11GroupAddressesIndex,
                      pWsscfgSetDot11GroupAddressesEntry->
                      MibObject.Dot11Address);
    }
    if (pWsscfgIsSetDot11GroupAddressesEntry->
        bDot11GroupAddressesStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11GroupAddressesStatus, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 1, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11GroupAddressesEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11GroupAddressesEntry->
                      MibObject.i4Dot11GroupAddressesIndex,
                      pWsscfgSetDot11GroupAddressesEntry->
                      MibObject.i4Dot11GroupAddressesStatus);
    }
    if (pWsscfgIsSetDot11GroupAddressesEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11GroupAddressesEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11GroupAddressesEntry->
                      MibObject.i4Dot11GroupAddressesIndex,
                      pWsscfgSetDot11GroupAddressesEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11EDCATableTrigger
 Input       :  The Indices
                pWsscfgSetDot11EDCAEntry
                pWsscfgIsSetDot11EDCAEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllDot11EDCATableTrigger (tWsscfgDot11EDCAEntry *
                                   pWsscfgSetDot11EDCAEntry,
                                   tWsscfgIsSetDot11EDCAEntry *
                                   pWsscfgIsSetDot11EDCAEntry, INT4 i4SetOption)
{

    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11EDCATableIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11EDCAEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11EDCAEntry->
                      MibObject.i4Dot11EDCATableIndex,
                      pWsscfgSetDot11EDCAEntry->
                      MibObject.i4Dot11EDCATableIndex);
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableCWmin == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11EDCATableCWmin, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11EDCAEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11EDCAEntry->
                      MibObject.i4Dot11EDCATableIndex,
                      pWsscfgSetDot11EDCAEntry->
                      MibObject.i4Dot11EDCATableCWmin);
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableCWmax == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11EDCATableCWmax, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11EDCAEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11EDCAEntry->
                      MibObject.i4Dot11EDCATableIndex,
                      pWsscfgSetDot11EDCAEntry->
                      MibObject.i4Dot11EDCATableCWmax);
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableAIFSN == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11EDCATableAIFSN, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11EDCAEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11EDCAEntry->
                      MibObject.i4Dot11EDCATableIndex,
                      pWsscfgSetDot11EDCAEntry->
                      MibObject.i4Dot11EDCATableAIFSN);
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableTXOPLimit == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11EDCATableTXOPLimit, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11EDCAEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11EDCAEntry->
                      MibObject.i4Dot11EDCATableIndex,
                      pWsscfgSetDot11EDCAEntry->
                      MibObject.i4Dot11EDCATableTXOPLimit);
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableMSDULifetime == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11EDCATableMSDULifetime, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      2, i4SetOption, "%i %i %i",
                      pWsscfgSetDot11EDCAEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11EDCAEntry->
                      MibObject.i4Dot11EDCATableIndex,
                      pWsscfgSetDot11EDCAEntry->
                      MibObject.i4Dot11EDCATableMSDULifetime);
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableMandatory == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11EDCATableMandatory, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11EDCAEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11EDCAEntry->
                      MibObject.i4Dot11EDCATableIndex,
                      pWsscfgSetDot11EDCAEntry->
                      MibObject.i4Dot11EDCATableMandatory);
    }
    if (pWsscfgIsSetDot11EDCAEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11EDCAEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11EDCAEntry->
                      MibObject.i4Dot11EDCATableIndex,
                      pWsscfgSetDot11EDCAEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11QAPEDCATableTrigger
 Input       :  The Indices
                pWsscfgSetDot11QAPEDCAEntry
                pWsscfgIsSetDot11QAPEDCAEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllDot11QAPEDCATableTrigger (tWsscfgDot11QAPEDCAEntry *
                                      pWsscfgSetDot11QAPEDCAEntry,
                                      tWsscfgIsSetDot11QAPEDCAEntry *
                                      pWsscfgIsSetDot11QAPEDCAEntry,
                                      INT4 i4SetOption)
{

    if (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11QAPEDCATableIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11QAPEDCAEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11QAPEDCAEntry->
                      MibObject.i4Dot11QAPEDCATableIndex,
                      pWsscfgSetDot11QAPEDCAEntry->
                      MibObject.i4Dot11QAPEDCATableIndex);
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableCWmin == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11QAPEDCATableCWmin, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11QAPEDCAEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11QAPEDCAEntry->
                      MibObject.i4Dot11QAPEDCATableIndex,
                      pWsscfgSetDot11QAPEDCAEntry->
                      MibObject.i4Dot11QAPEDCATableCWmin);
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableCWmax == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11QAPEDCATableCWmax, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11QAPEDCAEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11QAPEDCAEntry->
                      MibObject.i4Dot11QAPEDCATableIndex,
                      pWsscfgSetDot11QAPEDCAEntry->
                      MibObject.i4Dot11QAPEDCATableCWmax);
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableAIFSN == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11QAPEDCATableAIFSN, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11QAPEDCAEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11QAPEDCAEntry->
                      MibObject.i4Dot11QAPEDCATableIndex,
                      pWsscfgSetDot11QAPEDCAEntry->
                      MibObject.i4Dot11QAPEDCATableAIFSN);
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableTXOPLimit == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11QAPEDCATableTXOPLimit, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      2, i4SetOption, "%i %i %i",
                      pWsscfgSetDot11QAPEDCAEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11QAPEDCAEntry->
                      MibObject.i4Dot11QAPEDCATableIndex,
                      pWsscfgSetDot11QAPEDCAEntry->
                      MibObject.i4Dot11QAPEDCATableTXOPLimit);
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->
        bDot11QAPEDCATableMSDULifetime == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11QAPEDCATableMSDULifetime, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      2, i4SetOption, "%i %i %i",
                      pWsscfgSetDot11QAPEDCAEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11QAPEDCAEntry->
                      MibObject.i4Dot11QAPEDCATableIndex,
                      pWsscfgSetDot11QAPEDCAEntry->
                      MibObject.i4Dot11QAPEDCATableMSDULifetime);
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableMandatory == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11QAPEDCATableMandatory, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      2, i4SetOption, "%i %i %i",
                      pWsscfgSetDot11QAPEDCAEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11QAPEDCAEntry->
                      MibObject.i4Dot11QAPEDCATableIndex,
                      pWsscfgSetDot11QAPEDCAEntry->
                      MibObject.i4Dot11QAPEDCATableMandatory);
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11QAPEDCAEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11QAPEDCAEntry->
                      MibObject.i4Dot11QAPEDCATableIndex,
                      pWsscfgSetDot11QAPEDCAEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11PhyOperationTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11PhyOperationEntry
                pWsscfgIsSetDot11PhyOperationEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllDot11PhyOperationTableTrigger (tWsscfgDot11PhyOperationEntry *
                                           pWsscfgSetDot11PhyOperationEntry,
                                           tWsscfgIsSetDot11PhyOperationEntry *
                                           pWsscfgIsSetDot11PhyOperationEntry,
                                           INT4 i4SetOption)
{

    if (pWsscfgIsSetDot11PhyOperationEntry->bDot11CurrentRegDomain == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11CurrentRegDomain, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyOperationEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11PhyOperationEntry->
                      MibObject.i4Dot11CurrentRegDomain);
    }
    if (pWsscfgIsSetDot11PhyOperationEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyOperationEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11PhyOperationEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11PhyAntennaTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11PhyAntennaEntry
                pWsscfgIsSetDot11PhyAntennaEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllDot11PhyAntennaTableTrigger (tWsscfgDot11PhyAntennaEntry *
                                         pWsscfgSetDot11PhyAntennaEntry,
                                         tWsscfgIsSetDot11PhyAntennaEntry *
                                         pWsscfgIsSetDot11PhyAntennaEntry,
                                         INT4 i4SetOption)
{

    if (pWsscfgIsSetDot11PhyAntennaEntry->bDot11CurrentTxAntenna == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11CurrentTxAntenna, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyAntennaEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11PhyAntennaEntry->
                      MibObject.i4Dot11CurrentTxAntenna);
    }
    if (pWsscfgIsSetDot11PhyAntennaEntry->bDot11CurrentRxAntenna == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11CurrentRxAntenna, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyAntennaEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11PhyAntennaEntry->
                      MibObject.i4Dot11CurrentRxAntenna);
    }
    if (pWsscfgIsSetDot11PhyAntennaEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyAntennaEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11PhyAntennaEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11PhyTxPowerTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11PhyTxPowerEntry
                pWsscfgIsSetDot11PhyTxPowerEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllDot11PhyTxPowerTableTrigger (tWsscfgDot11PhyTxPowerEntry *
                                         pWsscfgSetDot11PhyTxPowerEntry,
                                         tWsscfgIsSetDot11PhyTxPowerEntry *
                                         pWsscfgIsSetDot11PhyTxPowerEntry,
                                         INT4 i4SetOption)
{

    if (pWsscfgIsSetDot11PhyTxPowerEntry->bDot11CurrentTxPowerLevel ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11CurrentTxPowerLevel, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyTxPowerEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11PhyTxPowerEntry->
                      MibObject.i4Dot11CurrentTxPowerLevel);
    }
    if (pWsscfgIsSetDot11PhyTxPowerEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyTxPowerEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11PhyTxPowerEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11PhyFHSSTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11PhyFHSSEntry
                pWsscfgIsSetDot11PhyFHSSEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllDot11PhyFHSSTableTrigger (tWsscfgDot11PhyFHSSEntry *
                                      pWsscfgSetDot11PhyFHSSEntry,
                                      tWsscfgIsSetDot11PhyFHSSEntry *
                                      pWsscfgIsSetDot11PhyFHSSEntry,
                                      INT4 i4SetOption)
{

    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentChannelNumber == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11CurrentChannelNumber, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyFHSSEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyFHSSEntry->
                      MibObject.i4Dot11CurrentChannelNumber);
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentDwellTime == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11CurrentDwellTime, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyFHSSEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyFHSSEntry->
                      MibObject.i4Dot11CurrentDwellTime);
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentSet == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11CurrentSet, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyFHSSEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyFHSSEntry->MibObject.i4Dot11CurrentSet);
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentPattern == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11CurrentPattern, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyFHSSEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyFHSSEntry->
                      MibObject.i4Dot11CurrentPattern);
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11CurrentIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyFHSSEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyFHSSEntry->
                      MibObject.i4Dot11CurrentIndex);
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCPrimeRadix == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11EHCCPrimeRadix, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyFHSSEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyFHSSEntry->
                      MibObject.i4Dot11EHCCPrimeRadix);
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCNumberofChannelsFamilyIndex ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11EHCCNumberofChannelsFamilyIndex, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetDot11PhyFHSSEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyFHSSEntry->
                      MibObject.i4Dot11EHCCNumberofChannelsFamilyIndex);
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->
        bDot11EHCCCapabilityImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11EHCCCapabilityImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetDot11PhyFHSSEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyFHSSEntry->
                      MibObject.i4Dot11EHCCCapabilityImplemented);
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCCapabilityEnabled == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11EHCCCapabilityActivated, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetDot11PhyFHSSEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyFHSSEntry->
                      MibObject.i4Dot11EHCCCapabilityEnabled);
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11HopAlgorithmAdopted == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11HopAlgorithmAdopted, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyFHSSEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyFHSSEntry->
                      MibObject.i4Dot11HopAlgorithmAdopted);
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11RandomTableFlag == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RandomTableFlag, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyFHSSEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyFHSSEntry->
                      MibObject.i4Dot11RandomTableFlag);
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11HopOffset == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11HopOffset, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyFHSSEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyFHSSEntry->MibObject.i4Dot11HopOffset);
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyFHSSEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyFHSSEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11PhyDSSSTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11PhyDSSSEntry
                pWsscfgIsSetDot11PhyDSSSEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllDot11PhyDSSSTableTrigger (tWsscfgDot11PhyDSSSEntry *
                                      pWsscfgSetDot11PhyDSSSEntry,
                                      tWsscfgIsSetDot11PhyDSSSEntry *
                                      pWsscfgIsSetDot11PhyDSSSEntry,
                                      INT4 i4SetOption)
{

    if (pWsscfgIsSetDot11PhyDSSSEntry->bDot11CurrentChannel == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11CurrentChannel, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyDSSSEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyDSSSEntry->
                      MibObject.i4Dot11CurrentChannel);
    }
    if (pWsscfgIsSetDot11PhyDSSSEntry->bDot11CurrentCCAMode == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11CurrentCCAMode, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyDSSSEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyDSSSEntry->
                      MibObject.i4Dot11CurrentCCAMode);
    }
    if (pWsscfgIsSetDot11PhyDSSSEntry->bDot11EDThreshold == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11EDThreshold, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyDSSSEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyDSSSEntry->
                      MibObject.i4Dot11EDThreshold);
    }
    if (pWsscfgIsSetDot11PhyDSSSEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyDSSSEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyDSSSEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11PhyIRTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11PhyIREntry
                pWsscfgIsSetDot11PhyIREntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllDot11PhyIRTableTrigger (tWsscfgDot11PhyIREntry *
                                    pWsscfgSetDot11PhyIREntry,
                                    tWsscfgIsSetDot11PhyIREntry *
                                    pWsscfgIsSetDot11PhyIREntry,
                                    INT4 i4SetOption)
{

    if (pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogTimerMax == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11CCAWatchdogTimerMax, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyIREntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyIREntry->
                      MibObject.i4Dot11CCAWatchdogTimerMax);
    }
    if (pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogCountMax == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11CCAWatchdogCountMax, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyIREntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyIREntry->
                      MibObject.i4Dot11CCAWatchdogCountMax);
    }
    if (pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogTimerMin == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11CCAWatchdogTimerMin, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyIREntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyIREntry->
                      MibObject.i4Dot11CCAWatchdogTimerMin);
    }
    if (pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogCountMin == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11CCAWatchdogCountMin, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyIREntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyIREntry->
                      MibObject.i4Dot11CCAWatchdogCountMin);
    }
    if (pWsscfgIsSetDot11PhyIREntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyIREntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyIREntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11AntennasListTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11AntennasListEntry
                pWsscfgIsSetDot11AntennasListEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllDot11AntennasListTableTrigger (tWsscfgDot11AntennasListEntry *
                                           pWsscfgSetDot11AntennasListEntry,
                                           tWsscfgIsSetDot11AntennasListEntry *
                                           pWsscfgIsSetDot11AntennasListEntry,
                                           INT4 i4SetOption)
{

    if (pWsscfgIsSetDot11AntennasListEntry->bDot11AntennaListIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11AntennaListIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11AntennasListEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11AntennasListEntry->
                      MibObject.i4Dot11AntennaListIndex,
                      pWsscfgSetDot11AntennasListEntry->
                      MibObject.i4Dot11AntennaListIndex);
    }
    if (pWsscfgIsSetDot11AntennasListEntry->
        bDot11SupportedTxAntenna == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11TxAntennaImplemented, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11AntennasListEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11AntennasListEntry->
                      MibObject.i4Dot11AntennaListIndex,
                      pWsscfgSetDot11AntennasListEntry->
                      MibObject.i4Dot11SupportedTxAntenna);
    }
    if (pWsscfgIsSetDot11AntennasListEntry->
        bDot11SupportedRxAntenna == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RxAntennaImplemented, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11AntennasListEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11AntennasListEntry->
                      MibObject.i4Dot11AntennaListIndex,
                      pWsscfgSetDot11AntennasListEntry->
                      MibObject.i4Dot11SupportedRxAntenna);
    }
    if (pWsscfgIsSetDot11AntennasListEntry->
        bDot11DiversitySelectionRx == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11DiversitySelectionRxImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0, 2,
                      i4SetOption, "%i %i %i",
                      pWsscfgSetDot11AntennasListEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11AntennasListEntry->MibObject.
                      i4Dot11AntennaListIndex,
                      pWsscfgSetDot11AntennasListEntry->MibObject.
                      i4Dot11DiversitySelectionRx);
    }
    if (pWsscfgIsSetDot11AntennasListEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11AntennasListEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11AntennasListEntry->
                      MibObject.i4Dot11AntennaListIndex,
                      pWsscfgSetDot11AntennasListEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11PhyOFDMTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11PhyOFDMEntry
                pWsscfgIsSetDot11PhyOFDMEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllDot11PhyOFDMTableTrigger (tWsscfgDot11PhyOFDMEntry *
                                      pWsscfgSetDot11PhyOFDMEntry,
                                      tWsscfgIsSetDot11PhyOFDMEntry *
                                      pWsscfgIsSetDot11PhyOFDMEntry,
                                      INT4 i4SetOption)
{

    if (pWsscfgIsSetDot11PhyOFDMEntry->bDot11CurrentFrequency == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11CurrentFrequency, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyOFDMEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyOFDMEntry->
                      MibObject.i4Dot11CurrentFrequency);
    }
    if (pWsscfgIsSetDot11PhyOFDMEntry->bDot11TIThreshold == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11TIThreshold, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 1, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyOFDMEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyOFDMEntry->
                      MibObject.i4Dot11TIThreshold);
    }
    if (pWsscfgIsSetDot11PhyOFDMEntry->bDot11ChannelStartingFactor == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11ChannelStartingFactor, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetDot11PhyOFDMEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyOFDMEntry->
                      MibObject.i4Dot11ChannelStartingFactor);
    }
    if (pWsscfgIsSetDot11PhyOFDMEntry->bDot11PhyOFDMChannelWidth == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11PhyOFDMChannelWidth, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyOFDMEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyOFDMEntry->
                      MibObject.i4Dot11PhyOFDMChannelWidth);
    }
    if (pWsscfgIsSetDot11PhyOFDMEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyOFDMEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyOFDMEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11HoppingPatternTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11HoppingPatternEntry
                pWsscfgIsSetDot11HoppingPatternEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllDot11HoppingPatternTableTrigger (tWsscfgDot11HoppingPatternEntry *
                                             pWsscfgSetDot11HoppingPatternEntry,
                                             tWsscfgIsSetDot11HoppingPatternEntry
                                             *
                                             pWsscfgIsSetDot11HoppingPatternEntry,
                                             INT4 i4SetOption)
{

    if (pWsscfgIsSetDot11HoppingPatternEntry->
        bDot11HoppingPatternIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11HoppingPatternIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11HoppingPatternEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11HoppingPatternEntry->
                      MibObject.i4Dot11HoppingPatternIndex,
                      pWsscfgSetDot11HoppingPatternEntry->
                      MibObject.i4Dot11HoppingPatternIndex);
    }
    if (pWsscfgIsSetDot11HoppingPatternEntry->bDot11RandomTableFieldNumber ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RandomTableFieldNumber, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      2, i4SetOption, "%i %i %i",
                      pWsscfgSetDot11HoppingPatternEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11HoppingPatternEntry->
                      MibObject.i4Dot11HoppingPatternIndex,
                      pWsscfgSetDot11HoppingPatternEntry->
                      MibObject.i4Dot11RandomTableFieldNumber);
    }
    if (pWsscfgIsSetDot11HoppingPatternEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetDot11HoppingPatternEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetDot11HoppingPatternEntry->
                      MibObject.i4Dot11HoppingPatternIndex,
                      pWsscfgSetDot11HoppingPatternEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11PhyERPTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11PhyERPEntry
                pWsscfgIsSetDot11PhyERPEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllDot11PhyERPTableTrigger (tWsscfgDot11PhyERPEntry *
                                     pWsscfgSetDot11PhyERPEntry,
                                     tWsscfgIsSetDot11PhyERPEntry *
                                     pWsscfgIsSetDot11PhyERPEntry,
                                     INT4 i4SetOption)
{

    if (pWsscfgIsSetDot11PhyERPEntry->bDot11ERPBCCOptionEnabled == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11ERPBCCOptionActivated, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyERPEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyERPEntry->
                      MibObject.i4Dot11ERPBCCOptionEnabled);
    }
    if (pWsscfgIsSetDot11PhyERPEntry->bDot11DSSSOFDMOptionEnabled == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11DSSSOFDMOptionActivated, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetDot11PhyERPEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyERPEntry->
                      MibObject.i4Dot11DSSSOFDMOptionEnabled);
    }
    if (pWsscfgIsSetDot11PhyERPEntry->bDot11ShortSlotTimeOptionImplemented ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11ShortSlotTimeOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetDot11PhyERPEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyERPEntry->
                      MibObject.i4Dot11ShortSlotTimeOptionImplemented);
    }
    if (pWsscfgIsSetDot11PhyERPEntry->
        bDot11ShortSlotTimeOptionEnabled == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11ShortSlotTimeOptionActivated, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetDot11PhyERPEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyERPEntry->
                      MibObject.i4Dot11ShortSlotTimeOptionEnabled);
    }
    if (pWsscfgIsSetDot11PhyERPEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11PhyERPEntry->MibObject.i4IfIndex,
                      pWsscfgSetDot11PhyERPEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11RSNAConfigTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11RSNAConfigEntry
                pWsscfgIsSetDot11RSNAConfigEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllDot11RSNAConfigTableTrigger (tWsscfgDot11RSNAConfigEntry *
                                         pWsscfgSetDot11RSNAConfigEntry,
                                         tWsscfgIsSetDot11RSNAConfigEntry *
                                         pWsscfgIsSetDot11RSNAConfigEntry,
                                         INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE Dot11RSNAConfigGroupCipherVal;
    UINT1               au1Dot11RSNAConfigGroupCipherVal[256];
    tSNMP_OCTET_STRING_TYPE Dot11RSNAConfigPSKValueVal;
    UINT1               au1Dot11RSNAConfigPSKValueVal[256];
    tSNMP_OCTET_STRING_TYPE Dot11RSNAConfigPSKPassPhraseVal;
    UINT1               au1Dot11RSNAConfigPSKPassPhraseVal[256];

    MEMSET (au1Dot11RSNAConfigGroupCipherVal, 0,
            sizeof (au1Dot11RSNAConfigGroupCipherVal));
    Dot11RSNAConfigGroupCipherVal.pu1_OctetList =
        au1Dot11RSNAConfigGroupCipherVal;
    Dot11RSNAConfigGroupCipherVal.i4_Length = 0;

    MEMSET (au1Dot11RSNAConfigPSKValueVal, 0,
            sizeof (au1Dot11RSNAConfigPSKValueVal));
    Dot11RSNAConfigPSKValueVal.pu1_OctetList = au1Dot11RSNAConfigPSKValueVal;
    Dot11RSNAConfigPSKValueVal.i4_Length = 0;

    MEMSET (au1Dot11RSNAConfigPSKPassPhraseVal, 0,
            sizeof (au1Dot11RSNAConfigPSKPassPhraseVal));
    Dot11RSNAConfigPSKPassPhraseVal.pu1_OctetList =
        au1Dot11RSNAConfigPSKPassPhraseVal;
    Dot11RSNAConfigPSKPassPhraseVal.i4_Length = 0;

    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigGroupCipher == OSIX_TRUE)
    {
        MEMCPY (Dot11RSNAConfigGroupCipherVal.pu1_OctetList,
                pWsscfgSetDot11RSNAConfigEntry->
                MibObject.au1Dot11RSNAConfigGroupCipher,
                pWsscfgSetDot11RSNAConfigEntry->
                MibObject.i4Dot11RSNAConfigGroupCipherLen);
        Dot11RSNAConfigGroupCipherVal.i4_Length =
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAConfigGroupCipherLen;

        nmhSetCmnNew (Dot11RSNAConfigGroupCipher, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %s",
                      pWsscfgSetDot11RSNAConfigEntry->MibObject.
                      i4IfIndex, &Dot11RSNAConfigGroupCipherVal);
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyMethod ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RSNAConfigGroupRekeyMethod, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetDot11RSNAConfigEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11RSNAConfigEntry->
                      MibObject.i4Dot11RSNAConfigGroupRekeyMethod);
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigGroupRekeyTime == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RSNAConfigGroupRekeyTime, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %u",
                      pWsscfgSetDot11RSNAConfigEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11RSNAConfigEntry->
                      MibObject.u4Dot11RSNAConfigGroupRekeyTime);
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyPackets ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RSNAConfigGroupRekeyPackets, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %u",
                      pWsscfgSetDot11RSNAConfigEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11RSNAConfigEntry->
                      MibObject.u4Dot11RSNAConfigGroupRekeyPackets);
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyStrict ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RSNAConfigGroupRekeyStrict, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetDot11RSNAConfigEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11RSNAConfigEntry->
                      MibObject.i4Dot11RSNAConfigGroupRekeyStrict);
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPSKValue == OSIX_TRUE)
    {
        MEMCPY (Dot11RSNAConfigPSKValueVal.pu1_OctetList,
                pWsscfgSetDot11RSNAConfigEntry->
                MibObject.au1Dot11RSNAConfigPSKValue,
                pWsscfgSetDot11RSNAConfigEntry->
                MibObject.i4Dot11RSNAConfigPSKValueLen);
        Dot11RSNAConfigPSKValueVal.i4_Length =
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAConfigPSKValueLen;

        nmhSetCmnNew (Dot11RSNAConfigPSKValue, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %s",
                      pWsscfgSetDot11RSNAConfigEntry->MibObject.
                      i4IfIndex, &Dot11RSNAConfigPSKValueVal);
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigPSKPassPhrase == OSIX_TRUE)
    {
        MEMCPY (Dot11RSNAConfigPSKPassPhraseVal.pu1_OctetList,
                pWsscfgSetDot11RSNAConfigEntry->
                MibObject.au1Dot11RSNAConfigPSKPassPhrase,
                pWsscfgSetDot11RSNAConfigEntry->
                MibObject.i4Dot11RSNAConfigPSKPassPhraseLen);
        Dot11RSNAConfigPSKPassPhraseVal.i4_Length =
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAConfigPSKPassPhraseLen;

        nmhSetCmnNew (Dot11RSNAConfigPSKPassPhrase, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %s",
                      pWsscfgSetDot11RSNAConfigEntry->MibObject.
                      i4IfIndex, &Dot11RSNAConfigPSKPassPhraseVal);
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupUpdateCount ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RSNAConfigGroupUpdateCount, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %u",
                      pWsscfgSetDot11RSNAConfigEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11RSNAConfigEntry->
                      MibObject.u4Dot11RSNAConfigGroupUpdateCount);
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPairwiseUpdateCount ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RSNAConfigPairwiseUpdateCount, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %u",
                      pWsscfgSetDot11RSNAConfigEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11RSNAConfigEntry->
                      MibObject.u4Dot11RSNAConfigPairwiseUpdateCount);
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigPMKLifetime == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RSNAConfigPMKLifetime, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %u",
                      pWsscfgSetDot11RSNAConfigEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11RSNAConfigEntry->
                      MibObject.u4Dot11RSNAConfigPMKLifetime);
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPMKReauthThreshold ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RSNAConfigPMKReauthThreshold, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %u",
                      pWsscfgSetDot11RSNAConfigEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11RSNAConfigEntry->
                      MibObject.u4Dot11RSNAConfigPMKReauthThreshold);
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigSATimeout ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RSNAConfigSATimeout, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %u",
                      pWsscfgSetDot11RSNAConfigEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11RSNAConfigEntry->
                      MibObject.u4Dot11RSNAConfigSATimeout);
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNATKIPCounterMeasuresInvoked == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RSNATKIPCounterMeasuresInvoked, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %u",
                      pWsscfgSetDot11RSNAConfigEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11RSNAConfigEntry->
                      MibObject.u4Dot11RSNATKIPCounterMeasuresInvoked);
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNA4WayHandshakeFailures ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RSNA4WayHandshakeFailures, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %u",
                      pWsscfgSetDot11RSNAConfigEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11RSNAConfigEntry->
                      MibObject.u4Dot11RSNA4WayHandshakeFailures);
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigSTKRekeyTime == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RSNAConfigSTKRekeyTime, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %u",
                      pWsscfgSetDot11RSNAConfigEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11RSNAConfigEntry->
                      MibObject.u4Dot11RSNAConfigSTKRekeyTime);
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigSMKUpdateCount == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RSNAConfigSMKUpdateCount, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %u",
                      pWsscfgSetDot11RSNAConfigEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11RSNAConfigEntry->
                      MibObject.u4Dot11RSNAConfigSMKUpdateCount);
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigSMKLifetime == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RSNAConfigSMKLifetime, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %u",
                      pWsscfgSetDot11RSNAConfigEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11RSNAConfigEntry->
                      MibObject.u4Dot11RSNAConfigSMKLifetime);
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNASMKHandshakeFailures == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RSNASMKHandshakeFailures, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %u",
                      pWsscfgSetDot11RSNAConfigEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11RSNAConfigEntry->
                      MibObject.u4Dot11RSNASMKHandshakeFailures);
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetDot11RSNAConfigEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11RSNAConfigEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11RSNAConfigPairwiseCiphersTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry
                pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgSetAllDot11RSNAConfigPairwiseCiphersTableTrigger
    (tWsscfgDot11RSNAConfigPairwiseCiphersEntry *
     pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry,
     tWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry *
     pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry, INT4 i4SetOption)
{

    if (pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry->
        bDot11RSNAConfigPairwiseCipherIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RSNAConfigPairwiseCipherIndex, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      2, i4SetOption, "%i %u %u",
                      pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry->MibObject.
                      u4Dot11RSNAConfigPairwiseCipherIndex,
                      pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry->MibObject.
                      u4Dot11RSNAConfigPairwiseCipherIndex);
    }
    if (pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry->
        bDot11RSNAConfigPairwiseCipherEnabled == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RSNAConfigPairwiseCipherActivated, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      2, i4SetOption, "%i %u %i",
                      pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry->MibObject.
                      u4Dot11RSNAConfigPairwiseCipherIndex,
                      pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry->MibObject.
                      i4Dot11RSNAConfigPairwiseCipherEnabled);
    }
    if (pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %u %i",
                      pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry->MibObject.
                      u4Dot11RSNAConfigPairwiseCipherIndex,
                      pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry->MibObject.
                      i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11RSNAConfigAuthenticationSuitesTableTrigger
 Input       :  The Indices
                pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry
                pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgSetAllDot11RSNAConfigAuthenticationSuitesTableTrigger
    (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *
     pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry,
     tWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry *
     pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry, INT4 i4SetOption)
{

    if (pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry->
        bDot11RSNAConfigAuthenticationSuiteIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RSNAConfigAuthenticationSuiteIndex, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%u %u",
                      pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry->
                      MibObject.u4Dot11RSNAConfigAuthenticationSuiteIndex,
                      pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry->
                      MibObject.u4Dot11RSNAConfigAuthenticationSuiteIndex);
    }
    if (pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry->
        bDot11RSNAConfigAuthenticationSuiteEnabled == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11RSNAConfigAuthenticationSuiteActivated, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%u %i",
                      pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry->
                      MibObject.u4Dot11RSNAConfigAuthenticationSuiteIndex,
                      pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry->
                      MibObject.i4Dot11RSNAConfigAuthenticationSuiteEnabled);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgDot11StationConfigTableCreateApi
 Input       :  pWsscfgDot11StationConfigEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11StationConfigEntry *
WsscfgDot11StationConfigTableCreateApi (tWsscfgDot11StationConfigEntry *
                                        pSetWsscfgDot11StationConfigEntry)
{
    tWsscfgDot11StationConfigEntry *pWsscfgDot11StationConfigEntry = NULL;

    if (pSetWsscfgDot11StationConfigEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11StationConfigTableCreatApi: pSetWsscfgDot11StationConfigEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11StationConfigEntry =
        (tWsscfgDot11StationConfigEntry *)
        MemAllocMemBlk (WSSCFG_DOT11STATIONCONFIGTABLE_POOLID);
    if (pWsscfgDot11StationConfigEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11StationConfigTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11StationConfigEntry,
                pSetWsscfgDot11StationConfigEntry,
                sizeof (tWsscfgDot11StationConfigEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11StationConfigTable,
             (tRBElem *) pWsscfgDot11StationConfigEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11StationConfigTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11STATIONCONFIGTABLE_POOLID,
                                (UINT1 *) pWsscfgDot11StationConfigEntry);
            return NULL;
        }
        return pWsscfgDot11StationConfigEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11AuthenticationAlgorithmsTableCreateApi
 Input       :  pWsscfgDot11AuthenticationAlgorithmsEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11AuthenticationAlgorithmsEntry
    *
    WsscfgDot11AuthenticationAlgorithmsTableCreateApi
    (tWsscfgDot11AuthenticationAlgorithmsEntry *
     pSetWsscfgDot11AuthenticationAlgorithmsEntry)
{
    tWsscfgDot11AuthenticationAlgorithmsEntry
        * pWsscfgDot11AuthenticationAlgorithmsEntry = NULL;

    if (pSetWsscfgDot11AuthenticationAlgorithmsEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11AuthenticationAlgorithmsTableCreatApi: pSetWsscfgDot11AuthenticationAlgorithmsEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11AuthenticationAlgorithmsEntry =
        (tWsscfgDot11AuthenticationAlgorithmsEntry *)
        MemAllocMemBlk (WSSCFG_DOT11AUTHENTICATIONALGORITHMSTABLE_POOLID);
    if (pWsscfgDot11AuthenticationAlgorithmsEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11AuthenticationAlgorithmsTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11AuthenticationAlgorithmsEntry,
                pSetWsscfgDot11AuthenticationAlgorithmsEntry,
                sizeof (tWsscfgDot11AuthenticationAlgorithmsEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.
             WsscfgGlbMib.Dot11AuthenticationAlgorithmsTable,
             (tRBElem *) pWsscfgDot11AuthenticationAlgorithmsEntry) !=
            RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11AuthenticationAlgorithmsTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_DOT11AUTHENTICATIONALGORITHMSTABLE_POOLID,
                 (UINT1 *) pWsscfgDot11AuthenticationAlgorithmsEntry);
            return NULL;
        }
        return pWsscfgDot11AuthenticationAlgorithmsEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11WEPDefaultKeysTableCreateApi
 Input       :  pWsscfgDot11WEPDefaultKeysEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11WEPDefaultKeysEntry
    * WsscfgDot11WEPDefaultKeysTableCreateApi (tWsscfgDot11WEPDefaultKeysEntry *
                                               pSetWsscfgDot11WEPDefaultKeysEntry)
{
    tWsscfgDot11WEPDefaultKeysEntry *pWsscfgDot11WEPDefaultKeysEntry = NULL;

    if (pSetWsscfgDot11WEPDefaultKeysEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11WEPDefaultKeysTableCreatApi: pSetWsscfgDot11WEPDefaultKeysEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11WEPDefaultKeysEntry =
        (tWsscfgDot11WEPDefaultKeysEntry *)
        MemAllocMemBlk (WSSCFG_DOT11WEPDEFAULTKEYSTABLE_POOLID);
    if (pWsscfgDot11WEPDefaultKeysEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11WEPDefaultKeysTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11WEPDefaultKeysEntry,
                pSetWsscfgDot11WEPDefaultKeysEntry,
                sizeof (tWsscfgDot11WEPDefaultKeysEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11WEPDefaultKeysTable,
             (tRBElem *) pWsscfgDot11WEPDefaultKeysEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11WEPDefaultKeysTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11WEPDEFAULTKEYSTABLE_POOLID,
                                (UINT1 *) pWsscfgDot11WEPDefaultKeysEntry);
            return NULL;
        }
        return pWsscfgDot11WEPDefaultKeysEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11WEPKeyMappingsTableCreateApi
 Input       :  pWsscfgDot11WEPKeyMappingsEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11WEPKeyMappingsEntry
    * WsscfgDot11WEPKeyMappingsTableCreateApi (tWsscfgDot11WEPKeyMappingsEntry *
                                               pSetWsscfgDot11WEPKeyMappingsEntry)
{
    tWsscfgDot11WEPKeyMappingsEntry *pWsscfgDot11WEPKeyMappingsEntry = NULL;

    if (pSetWsscfgDot11WEPKeyMappingsEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11WEPKeyMappingsTableCreatApi: pSetWsscfgDot11WEPKeyMappingsEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11WEPKeyMappingsEntry =
        (tWsscfgDot11WEPKeyMappingsEntry *)
        MemAllocMemBlk (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID);
    if (pWsscfgDot11WEPKeyMappingsEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11WEPKeyMappingsTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11WEPKeyMappingsEntry,
                pSetWsscfgDot11WEPKeyMappingsEntry,
                sizeof (tWsscfgDot11WEPKeyMappingsEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11WEPKeyMappingsTable,
             (tRBElem *) pWsscfgDot11WEPKeyMappingsEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11WEPKeyMappingsTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                                (UINT1 *) pWsscfgDot11WEPKeyMappingsEntry);
            return NULL;
        }
        return pWsscfgDot11WEPKeyMappingsEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11PrivacyTableCreateApi
 Input       :  pWsscfgDot11PrivacyEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11PrivacyEntry *
WsscfgDot11PrivacyTableCreateApi (tWsscfgDot11PrivacyEntry *
                                  pSetWsscfgDot11PrivacyEntry)
{
    tWsscfgDot11PrivacyEntry *pWsscfgDot11PrivacyEntry = NULL;

    if (pSetWsscfgDot11PrivacyEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11PrivacyTableCreatApi: pSetWsscfgDot11PrivacyEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11PrivacyEntry = (tWsscfgDot11PrivacyEntry *)
        MemAllocMemBlk (WSSCFG_DOT11PRIVACYTABLE_POOLID);
    if (pWsscfgDot11PrivacyEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11PrivacyTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11PrivacyEntry, pSetWsscfgDot11PrivacyEntry,
                sizeof (tWsscfgDot11PrivacyEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11PrivacyTable,
             (tRBElem *) pWsscfgDot11PrivacyEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11PrivacyTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11PRIVACYTABLE_POOLID,
                                (UINT1 *) pWsscfgDot11PrivacyEntry);
            return NULL;
        }
        return pWsscfgDot11PrivacyEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11MultiDomainCapabilityTableCreateApi
 Input       :  pWsscfgDot11MultiDomainCapabilityEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11MultiDomainCapabilityEntry
    *
    WsscfgDot11MultiDomainCapabilityTableCreateApi
    (tWsscfgDot11MultiDomainCapabilityEntry *
     pSetWsscfgDot11MultiDomainCapabilityEntry)
{
    tWsscfgDot11MultiDomainCapabilityEntry
        * pWsscfgDot11MultiDomainCapabilityEntry = NULL;

    if (pSetWsscfgDot11MultiDomainCapabilityEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11MultiDomainCapabilityTableCreatApi: pSetWsscfgDot11MultiDomainCapabilityEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11MultiDomainCapabilityEntry =
        (tWsscfgDot11MultiDomainCapabilityEntry *)
        MemAllocMemBlk (WSSCFG_DOT11MULTIDOMAINCAPABILITYTABLE_POOLID);
    if (pWsscfgDot11MultiDomainCapabilityEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11MultiDomainCapabilityTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11MultiDomainCapabilityEntry,
                pSetWsscfgDot11MultiDomainCapabilityEntry,
                sizeof (tWsscfgDot11MultiDomainCapabilityEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.
             Dot11MultiDomainCapabilityTable,
             (tRBElem *) pWsscfgDot11MultiDomainCapabilityEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11MultiDomainCapabilityTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_DOT11MULTIDOMAINCAPABILITYTABLE_POOLID,
                 (UINT1 *) pWsscfgDot11MultiDomainCapabilityEntry);
            return NULL;
        }
        return pWsscfgDot11MultiDomainCapabilityEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11SpectrumManagementTableCreateApi
 Input       :  pWsscfgDot11SpectrumManagementEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11SpectrumManagementEntry
    *
    WsscfgDot11SpectrumManagementTableCreateApi
    (tWsscfgDot11SpectrumManagementEntry *
     pSetWsscfgDot11SpectrumManagementEntry)
{
    tWsscfgDot11SpectrumManagementEntry
        * pWsscfgDot11SpectrumManagementEntry = NULL;

    if (pSetWsscfgDot11SpectrumManagementEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11SpectrumManagementTableCreatApi: pSetWsscfgDot11SpectrumManagementEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11SpectrumManagementEntry =
        (tWsscfgDot11SpectrumManagementEntry *)
        MemAllocMemBlk (WSSCFG_DOT11SPECTRUMMANAGEMENTTABLE_POOLID);
    if (pWsscfgDot11SpectrumManagementEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11SpectrumManagementTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11SpectrumManagementEntry,
                pSetWsscfgDot11SpectrumManagementEntry,
                sizeof (tWsscfgDot11SpectrumManagementEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11SpectrumManagementTable,
             (tRBElem *) pWsscfgDot11SpectrumManagementEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11SpectrumManagementTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_DOT11SPECTRUMMANAGEMENTTABLE_POOLID, (UINT1 *)
                 pWsscfgDot11SpectrumManagementEntry);
            return NULL;
        }
        return pWsscfgDot11SpectrumManagementEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11RegulatoryClassesTableCreateApi
 Input       :  pWsscfgDot11RegulatoryClassesEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11RegulatoryClassesEntry
    *
    WsscfgDot11RegulatoryClassesTableCreateApi
    (tWsscfgDot11RegulatoryClassesEntry * pSetWsscfgDot11RegulatoryClassesEntry)
{
    tWsscfgDot11RegulatoryClassesEntry
        * pWsscfgDot11RegulatoryClassesEntry = NULL;

    if (pSetWsscfgDot11RegulatoryClassesEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11RegulatoryClassesTableCreatApi: pSetWsscfgDot11RegulatoryClassesEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11RegulatoryClassesEntry =
        (tWsscfgDot11RegulatoryClassesEntry *)
        MemAllocMemBlk (WSSCFG_DOT11REGULATORYCLASSESTABLE_POOLID);
    if (pWsscfgDot11RegulatoryClassesEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11RegulatoryClassesTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11RegulatoryClassesEntry,
                pSetWsscfgDot11RegulatoryClassesEntry,
                sizeof (tWsscfgDot11RegulatoryClassesEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11RegulatoryClassesTable,
             (tRBElem *) pWsscfgDot11RegulatoryClassesEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11RegulatoryClassesTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_DOT11REGULATORYCLASSESTABLE_POOLID, (UINT1 *)
                 pWsscfgDot11RegulatoryClassesEntry);
            return NULL;
        }
        return pWsscfgDot11RegulatoryClassesEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11OperationTableCreateApi
 Input       :  pWsscfgDot11OperationEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11OperationEntry *
WsscfgDot11OperationTableCreateApi (tWsscfgDot11OperationEntry *
                                    pSetWsscfgDot11OperationEntry)
{
    tWsscfgDot11OperationEntry *pWsscfgDot11OperationEntry = NULL;

    if (pSetWsscfgDot11OperationEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11OperationTableCreatApi: pSetWsscfgDot11OperationEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11OperationEntry = (tWsscfgDot11OperationEntry *)
        MemAllocMemBlk (WSSCFG_DOT11OPERATIONTABLE_POOLID);
    if (pWsscfgDot11OperationEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11OperationTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11OperationEntry,
                pSetWsscfgDot11OperationEntry,
                sizeof (tWsscfgDot11OperationEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11OperationTable,
             (tRBElem *) pWsscfgDot11OperationEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11OperationTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11OPERATIONTABLE_POOLID,
                                (UINT1 *) pWsscfgDot11OperationEntry);
            return NULL;
        }
        return pWsscfgDot11OperationEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11CountersTableCreateApi
 Input       :  pWsscfgDot11CountersEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11CountersEntry *
WsscfgDot11CountersTableCreateApi (tWsscfgDot11CountersEntry *
                                   pSetWsscfgDot11CountersEntry)
{
    tWsscfgDot11CountersEntry *pWsscfgDot11CountersEntry = NULL;

    if (pSetWsscfgDot11CountersEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11CountersTableCreatApi: pSetWsscfgDot11CountersEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11CountersEntry = (tWsscfgDot11CountersEntry *)
        MemAllocMemBlk (WSSCFG_DOT11COUNTERSTABLE_POOLID);
    if (pWsscfgDot11CountersEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11CountersTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11CountersEntry,
                pSetWsscfgDot11CountersEntry,
                sizeof (tWsscfgDot11CountersEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11CountersTable,
             (tRBElem *) pWsscfgDot11CountersEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11CountersTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11COUNTERSTABLE_POOLID,
                                (UINT1 *) pWsscfgDot11CountersEntry);
            return NULL;
        }
        return pWsscfgDot11CountersEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11GroupAddressesTableCreateApi
 Input       :  pWsscfgDot11GroupAddressesEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11GroupAddressesEntry
    * WsscfgDot11GroupAddressesTableCreateApi (tWsscfgDot11GroupAddressesEntry *
                                               pSetWsscfgDot11GroupAddressesEntry)
{
    tWsscfgDot11GroupAddressesEntry *pWsscfgDot11GroupAddressesEntry = NULL;

    if (pSetWsscfgDot11GroupAddressesEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11GroupAddressesTableCreatApi: pSetWsscfgDot11GroupAddressesEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11GroupAddressesEntry =
        (tWsscfgDot11GroupAddressesEntry *)
        MemAllocMemBlk (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID);
    if (pWsscfgDot11GroupAddressesEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11GroupAddressesTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11GroupAddressesEntry,
                pSetWsscfgDot11GroupAddressesEntry,
                sizeof (tWsscfgDot11GroupAddressesEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11GroupAddressesTable,
             (tRBElem *) pWsscfgDot11GroupAddressesEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11GroupAddressesTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                                (UINT1 *) pWsscfgDot11GroupAddressesEntry);
            return NULL;
        }
        return pWsscfgDot11GroupAddressesEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11EDCATableCreateApi
 Input       :  pWsscfgDot11EDCAEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11EDCAEntry *
WsscfgDot11EDCATableCreateApi (tWsscfgDot11EDCAEntry * pSetWsscfgDot11EDCAEntry)
{
    tWsscfgDot11EDCAEntry *pWsscfgDot11EDCAEntry = NULL;

    if (pSetWsscfgDot11EDCAEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11EDCATableCreatApi: pSetWsscfgDot11EDCAEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11EDCAEntry = (tWsscfgDot11EDCAEntry *)
        MemAllocMemBlk (WSSCFG_DOT11EDCATABLE_POOLID);
    if (pWsscfgDot11EDCAEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11EDCATableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11EDCAEntry, pSetWsscfgDot11EDCAEntry,
                sizeof (tWsscfgDot11EDCAEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11EDCATable,
             (tRBElem *) pWsscfgDot11EDCAEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11EDCATableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11EDCATABLE_POOLID,
                                (UINT1 *) pWsscfgDot11EDCAEntry);
            return NULL;
        }
        return pWsscfgDot11EDCAEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11QAPEDCATableCreateApi
 Input       :  pWsscfgDot11QAPEDCAEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11QAPEDCAEntry *
WsscfgDot11QAPEDCATableCreateApi (tWsscfgDot11QAPEDCAEntry *
                                  pSetWsscfgDot11QAPEDCAEntry)
{
    tWsscfgDot11QAPEDCAEntry *pWsscfgDot11QAPEDCAEntry = NULL;

    if (pSetWsscfgDot11QAPEDCAEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11QAPEDCATableCreatApi: pSetWsscfgDot11QAPEDCAEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11QAPEDCAEntry = (tWsscfgDot11QAPEDCAEntry *)
        MemAllocMemBlk (WSSCFG_DOT11QAPEDCATABLE_POOLID);
    if (pWsscfgDot11QAPEDCAEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11QAPEDCATableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11QAPEDCAEntry, pSetWsscfgDot11QAPEDCAEntry,
                sizeof (tWsscfgDot11QAPEDCAEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11QAPEDCATable,
             (tRBElem *) pWsscfgDot11QAPEDCAEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11QAPEDCATableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11QAPEDCATABLE_POOLID,
                                (UINT1 *) pWsscfgDot11QAPEDCAEntry);
            return NULL;
        }
        return pWsscfgDot11QAPEDCAEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11QosCountersTableCreateApi
 Input       :  pWsscfgDot11QosCountersEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11QosCountersEntry *
WsscfgDot11QosCountersTableCreateApi (tWsscfgDot11QosCountersEntry *
                                      pSetWsscfgDot11QosCountersEntry)
{
    tWsscfgDot11QosCountersEntry *pWsscfgDot11QosCountersEntry = NULL;

    if (pSetWsscfgDot11QosCountersEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11QosCountersTableCreatApi: pSetWsscfgDot11QosCountersEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11QosCountersEntry = (tWsscfgDot11QosCountersEntry *)
        MemAllocMemBlk (WSSCFG_DOT11QOSCOUNTERSTABLE_POOLID);
    if (pWsscfgDot11QosCountersEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11QosCountersTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11QosCountersEntry,
                pSetWsscfgDot11QosCountersEntry,
                sizeof (tWsscfgDot11QosCountersEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11QosCountersTable,
             (tRBElem *) pWsscfgDot11QosCountersEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11QosCountersTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11QOSCOUNTERSTABLE_POOLID,
                                (UINT1 *) pWsscfgDot11QosCountersEntry);
            return NULL;
        }
        return pWsscfgDot11QosCountersEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11ResourceInfoTableCreateApi
 Input       :  pWsscfgDot11ResourceInfoEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11ResourceInfoEntry *
WsscfgDot11ResourceInfoTableCreateApi (tWsscfgDot11ResourceInfoEntry *
                                       pSetWsscfgDot11ResourceInfoEntry)
{
    tWsscfgDot11ResourceInfoEntry *pWsscfgDot11ResourceInfoEntry = NULL;

    if (pSetWsscfgDot11ResourceInfoEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11ResourceInfoTableCreatApi: pSetWsscfgDot11ResourceInfoEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11ResourceInfoEntry = (tWsscfgDot11ResourceInfoEntry *)
        MemAllocMemBlk (WSSCFG_DOT11RESOURCEINFOTABLE_POOLID);
    if (pWsscfgDot11ResourceInfoEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11ResourceInfoTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11ResourceInfoEntry,
                pSetWsscfgDot11ResourceInfoEntry,
                sizeof (tWsscfgDot11ResourceInfoEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11ResourceInfoTable,
             (tRBElem *) pWsscfgDot11ResourceInfoEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11ResourceInfoTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11RESOURCEINFOTABLE_POOLID,
                                (UINT1 *) pWsscfgDot11ResourceInfoEntry);
            return NULL;
        }
        return pWsscfgDot11ResourceInfoEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11PhyOperationTableCreateApi
 Input       :  pWsscfgDot11PhyOperationEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11PhyOperationEntry *
WsscfgDot11PhyOperationTableCreateApi (tWsscfgDot11PhyOperationEntry *
                                       pSetWsscfgDot11PhyOperationEntry)
{
    tWsscfgDot11PhyOperationEntry *pWsscfgDot11PhyOperationEntry = NULL;

    if (pSetWsscfgDot11PhyOperationEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11PhyOperationTableCreatApi: pSetWsscfgDot11PhyOperationEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11PhyOperationEntry = (tWsscfgDot11PhyOperationEntry *)
        MemAllocMemBlk (WSSCFG_DOT11PHYOPERATIONTABLE_POOLID);
    if (pWsscfgDot11PhyOperationEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11PhyOperationTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11PhyOperationEntry,
                pSetWsscfgDot11PhyOperationEntry,
                sizeof (tWsscfgDot11PhyOperationEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyOperationTable,
             (tRBElem *) pWsscfgDot11PhyOperationEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11PhyOperationTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11PHYOPERATIONTABLE_POOLID,
                                (UINT1 *) pWsscfgDot11PhyOperationEntry);
            return NULL;
        }
        return pWsscfgDot11PhyOperationEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11PhyAntennaTableCreateApi
 Input       :  pWsscfgDot11PhyAntennaEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11PhyAntennaEntry *
WsscfgDot11PhyAntennaTableCreateApi (tWsscfgDot11PhyAntennaEntry *
                                     pSetWsscfgDot11PhyAntennaEntry)
{
    tWsscfgDot11PhyAntennaEntry *pWsscfgDot11PhyAntennaEntry = NULL;

    if (pSetWsscfgDot11PhyAntennaEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11PhyAntennaTableCreatApi: pSetWsscfgDot11PhyAntennaEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11PhyAntennaEntry = (tWsscfgDot11PhyAntennaEntry *)
        MemAllocMemBlk (WSSCFG_DOT11PHYANTENNATABLE_POOLID);
    if (pWsscfgDot11PhyAntennaEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11PhyAntennaTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11PhyAntennaEntry,
                pSetWsscfgDot11PhyAntennaEntry,
                sizeof (tWsscfgDot11PhyAntennaEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyAntennaTable,
             (tRBElem *) pWsscfgDot11PhyAntennaEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11PhyAntennaTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11PHYANTENNATABLE_POOLID,
                                (UINT1 *) pWsscfgDot11PhyAntennaEntry);
            return NULL;
        }
        return pWsscfgDot11PhyAntennaEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11PhyTxPowerTableCreateApi
 Input       :  pWsscfgDot11PhyTxPowerEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11PhyTxPowerEntry *
WsscfgDot11PhyTxPowerTableCreateApi (tWsscfgDot11PhyTxPowerEntry *
                                     pSetWsscfgDot11PhyTxPowerEntry)
{
    tWsscfgDot11PhyTxPowerEntry *pWsscfgDot11PhyTxPowerEntry = NULL;

    if (pSetWsscfgDot11PhyTxPowerEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11PhyTxPowerTableCreatApi: pSetWsscfgDot11PhyTxPowerEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11PhyTxPowerEntry = (tWsscfgDot11PhyTxPowerEntry *)
        MemAllocMemBlk (WSSCFG_DOT11PHYTXPOWERTABLE_POOLID);
    if (pWsscfgDot11PhyTxPowerEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11PhyTxPowerTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11PhyTxPowerEntry,
                pSetWsscfgDot11PhyTxPowerEntry,
                sizeof (tWsscfgDot11PhyTxPowerEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyTxPowerTable,
             (tRBElem *) pWsscfgDot11PhyTxPowerEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11PhyTxPowerTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11PHYTXPOWERTABLE_POOLID,
                                (UINT1 *) pWsscfgDot11PhyTxPowerEntry);
            return NULL;
        }
        return pWsscfgDot11PhyTxPowerEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11PhyFHSSTableCreateApi
 Input       :  pWsscfgDot11PhyFHSSEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11PhyFHSSEntry *
WsscfgDot11PhyFHSSTableCreateApi (tWsscfgDot11PhyFHSSEntry *
                                  pSetWsscfgDot11PhyFHSSEntry)
{
    tWsscfgDot11PhyFHSSEntry *pWsscfgDot11PhyFHSSEntry = NULL;

    if (pSetWsscfgDot11PhyFHSSEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11PhyFHSSTableCreatApi: pSetWsscfgDot11PhyFHSSEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11PhyFHSSEntry = (tWsscfgDot11PhyFHSSEntry *)
        MemAllocMemBlk (WSSCFG_DOT11PHYFHSSTABLE_POOLID);
    if (pWsscfgDot11PhyFHSSEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11PhyFHSSTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11PhyFHSSEntry, pSetWsscfgDot11PhyFHSSEntry,
                sizeof (tWsscfgDot11PhyFHSSEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyFHSSTable,
             (tRBElem *) pWsscfgDot11PhyFHSSEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11PhyFHSSTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11PHYFHSSTABLE_POOLID,
                                (UINT1 *) pWsscfgDot11PhyFHSSEntry);
            return NULL;
        }
        return pWsscfgDot11PhyFHSSEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11PhyDSSSTableCreateApi
 Input       :  pWsscfgDot11PhyDSSSEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11PhyDSSSEntry *
WsscfgDot11PhyDSSSTableCreateApi (tWsscfgDot11PhyDSSSEntry *
                                  pSetWsscfgDot11PhyDSSSEntry)
{
    tWsscfgDot11PhyDSSSEntry *pWsscfgDot11PhyDSSSEntry = NULL;

    if (pSetWsscfgDot11PhyDSSSEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11PhyDSSSTableCreatApi: pSetWsscfgDot11PhyDSSSEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11PhyDSSSEntry = (tWsscfgDot11PhyDSSSEntry *)
        MemAllocMemBlk (WSSCFG_DOT11PHYDSSSTABLE_POOLID);
    if (pWsscfgDot11PhyDSSSEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11PhyDSSSTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11PhyDSSSEntry, pSetWsscfgDot11PhyDSSSEntry,
                sizeof (tWsscfgDot11PhyDSSSEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyDSSSTable,
             (tRBElem *) pWsscfgDot11PhyDSSSEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11PhyDSSSTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11PHYDSSSTABLE_POOLID,
                                (UINT1 *) pWsscfgDot11PhyDSSSEntry);
            return NULL;
        }
        return pWsscfgDot11PhyDSSSEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11PhyIRTableCreateApi
 Input       :  pWsscfgDot11PhyIREntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11PhyIREntry *
WsscfgDot11PhyIRTableCreateApi (tWsscfgDot11PhyIREntry *
                                pSetWsscfgDot11PhyIREntry)
{
    tWsscfgDot11PhyIREntry *pWsscfgDot11PhyIREntry = NULL;

    if (pSetWsscfgDot11PhyIREntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11PhyIRTableCreatApi: pSetWsscfgDot11PhyIREntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11PhyIREntry = (tWsscfgDot11PhyIREntry *)
        MemAllocMemBlk (WSSCFG_DOT11PHYIRTABLE_POOLID);
    if (pWsscfgDot11PhyIREntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11PhyIRTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11PhyIREntry, pSetWsscfgDot11PhyIREntry,
                sizeof (tWsscfgDot11PhyIREntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyIRTable,
             (tRBElem *) pWsscfgDot11PhyIREntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11PhyIRTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11PHYIRTABLE_POOLID,
                                (UINT1 *) pWsscfgDot11PhyIREntry);
            return NULL;
        }
        return pWsscfgDot11PhyIREntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11RegDomainsSupportedTableCreateApi
 Input       :  pWsscfgDot11RegDomainsSupportedEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11RegDomainsSupportedEntry
    *
    WsscfgDot11RegDomainsSupportedTableCreateApi
    (tWsscfgDot11RegDomainsSupportedEntry *
     pSetWsscfgDot11RegDomainsSupportedEntry)
{
    tWsscfgDot11RegDomainsSupportedEntry
        * pWsscfgDot11RegDomainsSupportedEntry = NULL;

    if (pSetWsscfgDot11RegDomainsSupportedEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11RegDomainsSupportedTableCreatApi: pSetWsscfgDot11RegDomainsSupportedEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11RegDomainsSupportedEntry =
        (tWsscfgDot11RegDomainsSupportedEntry *)
        MemAllocMemBlk (WSSCFG_DOT11REGDOMAINSSUPPORTEDTABLE_POOLID);
    if (pWsscfgDot11RegDomainsSupportedEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11RegDomainsSupportedTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11RegDomainsSupportedEntry,
                pSetWsscfgDot11RegDomainsSupportedEntry,
                sizeof (tWsscfgDot11RegDomainsSupportedEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.
             Dot11RegDomainsSupportedTable,
             (tRBElem *) pWsscfgDot11RegDomainsSupportedEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11RegDomainsSupportedTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_DOT11REGDOMAINSSUPPORTEDTABLE_POOLID,
                 (UINT1 *) pWsscfgDot11RegDomainsSupportedEntry);
            return NULL;
        }
        return pWsscfgDot11RegDomainsSupportedEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11AntennasListTableCreateApi
 Input       :  pWsscfgDot11AntennasListEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11AntennasListEntry *
WsscfgDot11AntennasListTableCreateApi (tWsscfgDot11AntennasListEntry *
                                       pSetWsscfgDot11AntennasListEntry)
{
    tWsscfgDot11AntennasListEntry *pWsscfgDot11AntennasListEntry = NULL;

    if (pSetWsscfgDot11AntennasListEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11AntennasListTableCreatApi: pSetWsscfgDot11AntennasListEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11AntennasListEntry = (tWsscfgDot11AntennasListEntry *)
        MemAllocMemBlk (WSSCFG_DOT11ANTENNASLISTTABLE_POOLID);
    if (pWsscfgDot11AntennasListEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11AntennasListTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11AntennasListEntry,
                pSetWsscfgDot11AntennasListEntry,
                sizeof (tWsscfgDot11AntennasListEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11AntennasListTable,
             (tRBElem *) pWsscfgDot11AntennasListEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11AntennasListTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11ANTENNASLISTTABLE_POOLID,
                                (UINT1 *) pWsscfgDot11AntennasListEntry);
            return NULL;
        }
        return pWsscfgDot11AntennasListEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11SupportedDataRatesTxTableCreateApi
 Input       :  pWsscfgDot11SupportedDataRatesTxEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11SupportedDataRatesTxEntry
    *
    WsscfgDot11SupportedDataRatesTxTableCreateApi
    (tWsscfgDot11SupportedDataRatesTxEntry *
     pSetWsscfgDot11SupportedDataRatesTxEntry)
{
    tWsscfgDot11SupportedDataRatesTxEntry
        * pWsscfgDot11SupportedDataRatesTxEntry = NULL;

    if (pSetWsscfgDot11SupportedDataRatesTxEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11SupportedDataRatesTxTableCreatApi: pSetWsscfgDot11SupportedDataRatesTxEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11SupportedDataRatesTxEntry =
        (tWsscfgDot11SupportedDataRatesTxEntry *)
        MemAllocMemBlk (WSSCFG_DOT11SUPPORTEDDATARATESTXTABLE_POOLID);
    if (pWsscfgDot11SupportedDataRatesTxEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11SupportedDataRatesTxTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11SupportedDataRatesTxEntry,
                pSetWsscfgDot11SupportedDataRatesTxEntry,
                sizeof (tWsscfgDot11SupportedDataRatesTxEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.
             Dot11SupportedDataRatesTxTable,
             (tRBElem *) pWsscfgDot11SupportedDataRatesTxEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11SupportedDataRatesTxTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_DOT11SUPPORTEDDATARATESTXTABLE_POOLID,
                 (UINT1 *) pWsscfgDot11SupportedDataRatesTxEntry);
            return NULL;
        }
        return pWsscfgDot11SupportedDataRatesTxEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11SupportedDataRatesRxTableCreateApi
 Input       :  pWsscfgDot11SupportedDataRatesRxEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11SupportedDataRatesRxEntry
    *
    WsscfgDot11SupportedDataRatesRxTableCreateApi
    (tWsscfgDot11SupportedDataRatesRxEntry *
     pSetWsscfgDot11SupportedDataRatesRxEntry)
{
    tWsscfgDot11SupportedDataRatesRxEntry
        * pWsscfgDot11SupportedDataRatesRxEntry = NULL;

    if (pSetWsscfgDot11SupportedDataRatesRxEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11SupportedDataRatesRxTableCreatApi: pSetWsscfgDot11SupportedDataRatesRxEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11SupportedDataRatesRxEntry =
        (tWsscfgDot11SupportedDataRatesRxEntry *)
        MemAllocMemBlk (WSSCFG_DOT11SUPPORTEDDATARATESRXTABLE_POOLID);
    if (pWsscfgDot11SupportedDataRatesRxEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11SupportedDataRatesRxTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11SupportedDataRatesRxEntry,
                pSetWsscfgDot11SupportedDataRatesRxEntry,
                sizeof (tWsscfgDot11SupportedDataRatesRxEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.
             Dot11SupportedDataRatesRxTable,
             (tRBElem *) pWsscfgDot11SupportedDataRatesRxEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11SupportedDataRatesRxTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_DOT11SUPPORTEDDATARATESRXTABLE_POOLID,
                 (UINT1 *) pWsscfgDot11SupportedDataRatesRxEntry);
            return NULL;
        }
        return pWsscfgDot11SupportedDataRatesRxEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11PhyOFDMTableCreateApi
 Input       :  pWsscfgDot11PhyOFDMEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11PhyOFDMEntry *
WsscfgDot11PhyOFDMTableCreateApi (tWsscfgDot11PhyOFDMEntry *
                                  pSetWsscfgDot11PhyOFDMEntry)
{
    tWsscfgDot11PhyOFDMEntry *pWsscfgDot11PhyOFDMEntry = NULL;

    if (pSetWsscfgDot11PhyOFDMEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11PhyOFDMTableCreatApi: pSetWsscfgDot11PhyOFDMEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11PhyOFDMEntry = (tWsscfgDot11PhyOFDMEntry *)
        MemAllocMemBlk (WSSCFG_DOT11PHYOFDMTABLE_POOLID);
    if (pWsscfgDot11PhyOFDMEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11PhyOFDMTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11PhyOFDMEntry, pSetWsscfgDot11PhyOFDMEntry,
                sizeof (tWsscfgDot11PhyOFDMEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyOFDMTable,
             (tRBElem *) pWsscfgDot11PhyOFDMEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11PhyOFDMTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11PHYOFDMTABLE_POOLID,
                                (UINT1 *) pWsscfgDot11PhyOFDMEntry);
            return NULL;
        }
        return pWsscfgDot11PhyOFDMEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11PhyHRDSSSTableCreateApi
 Input       :  pWsscfgDot11PhyHRDSSSEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11PhyHRDSSSEntry *
WsscfgDot11PhyHRDSSSTableCreateApi (tWsscfgDot11PhyHRDSSSEntry *
                                    pSetWsscfgDot11PhyHRDSSSEntry)
{
    tWsscfgDot11PhyHRDSSSEntry *pWsscfgDot11PhyHRDSSSEntry = NULL;

    if (pSetWsscfgDot11PhyHRDSSSEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11PhyHRDSSSTableCreatApi: pSetWsscfgDot11PhyHRDSSSEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11PhyHRDSSSEntry = (tWsscfgDot11PhyHRDSSSEntry *)
        MemAllocMemBlk (WSSCFG_DOT11PHYHRDSSSTABLE_POOLID);
    if (pWsscfgDot11PhyHRDSSSEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11PhyHRDSSSTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11PhyHRDSSSEntry,
                pSetWsscfgDot11PhyHRDSSSEntry,
                sizeof (tWsscfgDot11PhyHRDSSSEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyHRDSSSTable,
             (tRBElem *) pWsscfgDot11PhyHRDSSSEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11PhyHRDSSSTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11PHYHRDSSSTABLE_POOLID,
                                (UINT1 *) pWsscfgDot11PhyHRDSSSEntry);
            return NULL;
        }
        return pWsscfgDot11PhyHRDSSSEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11HoppingPatternTableCreateApi
 Input       :  pWsscfgDot11HoppingPatternEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11HoppingPatternEntry
    * WsscfgDot11HoppingPatternTableCreateApi (tWsscfgDot11HoppingPatternEntry *
                                               pSetWsscfgDot11HoppingPatternEntry)
{
    tWsscfgDot11HoppingPatternEntry *pWsscfgDot11HoppingPatternEntry = NULL;

    if (pSetWsscfgDot11HoppingPatternEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11HoppingPatternTableCreatApi: pSetWsscfgDot11HoppingPatternEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11HoppingPatternEntry =
        (tWsscfgDot11HoppingPatternEntry *)
        MemAllocMemBlk (WSSCFG_DOT11HOPPINGPATTERNTABLE_POOLID);
    if (pWsscfgDot11HoppingPatternEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11HoppingPatternTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11HoppingPatternEntry,
                pSetWsscfgDot11HoppingPatternEntry,
                sizeof (tWsscfgDot11HoppingPatternEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11HoppingPatternTable,
             (tRBElem *) pWsscfgDot11HoppingPatternEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11HoppingPatternTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11HOPPINGPATTERNTABLE_POOLID,
                                (UINT1 *) pWsscfgDot11HoppingPatternEntry);
            return NULL;
        }
        return pWsscfgDot11HoppingPatternEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11PhyERPTableCreateApi
 Input       :  pWsscfgDot11PhyERPEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11PhyERPEntry *
WsscfgDot11PhyERPTableCreateApi (tWsscfgDot11PhyERPEntry *
                                 pSetWsscfgDot11PhyERPEntry)
{
    tWsscfgDot11PhyERPEntry *pWsscfgDot11PhyERPEntry = NULL;

    if (pSetWsscfgDot11PhyERPEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11PhyERPTableCreatApi: pSetWsscfgDot11PhyERPEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11PhyERPEntry = (tWsscfgDot11PhyERPEntry *)
        MemAllocMemBlk (WSSCFG_DOT11PHYERPTABLE_POOLID);
    if (pWsscfgDot11PhyERPEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11PhyERPTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11PhyERPEntry, pSetWsscfgDot11PhyERPEntry,
                sizeof (tWsscfgDot11PhyERPEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyERPTable,
             (tRBElem *) pWsscfgDot11PhyERPEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11PhyERPTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11PHYERPTABLE_POOLID,
                                (UINT1 *) pWsscfgDot11PhyERPEntry);
            return NULL;
        }
        return pWsscfgDot11PhyERPEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11RSNAConfigTableCreateApi
 Input       :  pWsscfgDot11RSNAConfigEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11RSNAConfigEntry *
WsscfgDot11RSNAConfigTableCreateApi (tWsscfgDot11RSNAConfigEntry *
                                     pSetWsscfgDot11RSNAConfigEntry)
{
    tWsscfgDot11RSNAConfigEntry *pWsscfgDot11RSNAConfigEntry = NULL;

    if (pSetWsscfgDot11RSNAConfigEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11RSNAConfigTableCreatApi: pSetWsscfgDot11RSNAConfigEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11RSNAConfigEntry = (tWsscfgDot11RSNAConfigEntry *)
        MemAllocMemBlk (WSSCFG_DOT11RSNACONFIGTABLE_POOLID);
    if (pWsscfgDot11RSNAConfigEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11RSNAConfigTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11RSNAConfigEntry,
                pSetWsscfgDot11RSNAConfigEntry,
                sizeof (tWsscfgDot11RSNAConfigEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11RSNAConfigTable,
             (tRBElem *) pWsscfgDot11RSNAConfigEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11RSNAConfigTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11RSNACONFIGTABLE_POOLID,
                                (UINT1 *) pWsscfgDot11RSNAConfigEntry);
            return NULL;
        }
        return pWsscfgDot11RSNAConfigEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11RSNAConfigPairwiseCiphersTableCreateApi
 Input       :  pWsscfgDot11RSNAConfigPairwiseCiphersEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11RSNAConfigPairwiseCiphersEntry
    *
    WsscfgDot11RSNAConfigPairwiseCiphersTableCreateApi
    (tWsscfgDot11RSNAConfigPairwiseCiphersEntry *
     pSetWsscfgDot11RSNAConfigPairwiseCiphersEntry)
{
    tWsscfgDot11RSNAConfigPairwiseCiphersEntry
        * pWsscfgDot11RSNAConfigPairwiseCiphersEntry = NULL;

    if (pSetWsscfgDot11RSNAConfigPairwiseCiphersEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11RSNAConfigPairwiseCiphersTableCreatApi: pSetWsscfgDot11RSNAConfigPairwiseCiphersEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11RSNAConfigPairwiseCiphersEntry =
        (tWsscfgDot11RSNAConfigPairwiseCiphersEntry *)
        MemAllocMemBlk (WSSCFG_DOT11RSNACONFIGPAIRWISECIPHERSTABLE_POOLID);
    if (pWsscfgDot11RSNAConfigPairwiseCiphersEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11RSNAConfigPairwiseCiphersTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11RSNAConfigPairwiseCiphersEntry,
                pSetWsscfgDot11RSNAConfigPairwiseCiphersEntry,
                sizeof (tWsscfgDot11RSNAConfigPairwiseCiphersEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.
             WsscfgGlbMib.Dot11RSNAConfigPairwiseCiphersTable,
             (tRBElem *) pWsscfgDot11RSNAConfigPairwiseCiphersEntry)
            != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11RSNAConfigPairwiseCiphersTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_DOT11RSNACONFIGPAIRWISECIPHERSTABLE_POOLID,
                 (UINT1 *) pWsscfgDot11RSNAConfigPairwiseCiphersEntry);
            return NULL;
        }
        return pWsscfgDot11RSNAConfigPairwiseCiphersEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11RSNAConfigAuthenticationSuitesTableCreateApi
 Input       :  pWsscfgDot11RSNAConfigAuthenticationSuitesEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11RSNAConfigAuthenticationSuitesEntry
    *
    WsscfgDot11RSNAConfigAuthenticationSuitesTableCreateApi
    (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *
     pSetWsscfgDot11RSNAConfigAuthenticationSuitesEntry)
{
    tWsscfgDot11RSNAConfigAuthenticationSuitesEntry
        * pWsscfgDot11RSNAConfigAuthenticationSuitesEntry = NULL;

    if (pSetWsscfgDot11RSNAConfigAuthenticationSuitesEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11RSNAConfigAuthenticationSuitesTableCreatApi: pSetWsscfgDot11RSNAConfigAuthenticationSuitesEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11RSNAConfigAuthenticationSuitesEntry =
        (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *)
        MemAllocMemBlk (WSSCFG_DOT11RSNACONFIGAUTHENTICATIONSUITESTABLE_POOLID);
    if (pWsscfgDot11RSNAConfigAuthenticationSuitesEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11RSNAConfigAuthenticationSuitesTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11RSNAConfigAuthenticationSuitesEntry,
                pSetWsscfgDot11RSNAConfigAuthenticationSuitesEntry,
                sizeof (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.
             WsscfgGlbMib.Dot11RSNAConfigAuthenticationSuitesTable,
             (tRBElem *)
             pWsscfgDot11RSNAConfigAuthenticationSuitesEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11RSNAConfigAuthenticationSuitesTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_DOT11RSNACONFIGAUTHENTICATIONSUITESTABLE_POOLID,
                 (UINT1 *) pWsscfgDot11RSNAConfigAuthenticationSuitesEntry);
            return NULL;
        }
        return pWsscfgDot11RSNAConfigAuthenticationSuitesEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgDot11RSNAStatsTableCreateApi
 Input       :  pWsscfgDot11RSNAStatsEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgDot11RSNAStatsEntry *
WsscfgDot11RSNAStatsTableCreateApi (tWsscfgDot11RSNAStatsEntry *
                                    pSetWsscfgDot11RSNAStatsEntry)
{
    tWsscfgDot11RSNAStatsEntry *pWsscfgDot11RSNAStatsEntry = NULL;

    if (pSetWsscfgDot11RSNAStatsEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11RSNAStatsTableCreatApi: pSetWsscfgDot11RSNAStatsEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgDot11RSNAStatsEntry = (tWsscfgDot11RSNAStatsEntry *)
        MemAllocMemBlk (WSSCFG_DOT11RSNASTATSTABLE_POOLID);
    if (pWsscfgDot11RSNAStatsEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgDot11RSNAStatsTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgDot11RSNAStatsEntry,
                pSetWsscfgDot11RSNAStatsEntry,
                sizeof (tWsscfgDot11RSNAStatsEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.Dot11RSNAStatsTable,
             (tRBElem *) pWsscfgDot11RSNAStatsEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgDot11RSNAStatsTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11RSNASTATSTABLE_POOLID,
                                (UINT1 *) pWsscfgDot11RSNAStatsEntry);
            return NULL;
        }
        return pWsscfgDot11RSNAStatsEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgCapwapDot11WlanTableCreate
 Input       :  None
 Description :  This function creates the CapwapDot11WlanTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgCapwapDot11WlanTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgCapwapDot11WlanEntry,
                       MibObject.CapwapDot11WlanTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.CapwapDot11WlanTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               CapwapDot11WlanTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgCapwapDot11WlanBindTableCreate
 Input       :  None
 Description :  This function creates the CapwapDot11WlanBindTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgCapwapDot11WlanBindTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgCapwapDot11WlanBindEntry,
                       MibObject.CapwapDot11WlanBindTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.CapwapDot11WlanBindTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               CapwapDot11WlanBindTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsRrmConfigTableCreate
 Input       :  None
 Description :  This function creates the FsRrmConfigTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsRrmConfigTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsRrmConfigEntry, MibObject.FsRrmConfigTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.FsRrmConfigTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsRrmConfigTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  CapwapDot11WlanTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                CapwapDot11WlanTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
CapwapDot11WlanTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgCapwapDot11WlanEntry *pCapwapDot11WlanEntry1 =
        (tWsscfgCapwapDot11WlanEntry *) pRBElem1;
    tWsscfgCapwapDot11WlanEntry *pCapwapDot11WlanEntry2 =
        (tWsscfgCapwapDot11WlanEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pCapwapDot11WlanEntry1->MibObject.u4CapwapDot11WlanProfileId >
        pCapwapDot11WlanEntry2->MibObject.u4CapwapDot11WlanProfileId)
    {
        return 1;
    }
    else if (pCapwapDot11WlanEntry1->
             MibObject.u4CapwapDot11WlanProfileId <
             pCapwapDot11WlanEntry2->MibObject.u4CapwapDot11WlanProfileId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  CapwapDot11WlanBindTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                CapwapDot11WlanBindTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
CapwapDot11WlanBindTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgCapwapDot11WlanBindEntry *pCapwapDot11WlanBindEntry1 =
        (tWsscfgCapwapDot11WlanBindEntry *) pRBElem1;
    tWsscfgCapwapDot11WlanBindEntry *pCapwapDot11WlanBindEntry2 =
        (tWsscfgCapwapDot11WlanBindEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pCapwapDot11WlanBindEntry1->MibObject.i4IfIndex >
        pCapwapDot11WlanBindEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pCapwapDot11WlanBindEntry1->MibObject.i4IfIndex <
             pCapwapDot11WlanBindEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    if (pCapwapDot11WlanBindEntry1->MibObject.
        u4CapwapDot11WlanProfileId >
        pCapwapDot11WlanBindEntry2->MibObject.u4CapwapDot11WlanProfileId)
    {
        return 1;
    }
    else if (pCapwapDot11WlanBindEntry1->
             MibObject.u4CapwapDot11WlanProfileId <
             pCapwapDot11WlanBindEntry2->MibObject.u4CapwapDot11WlanProfileId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsRrmConfigTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsRrmConfigTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsRrmConfigTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsRrmConfigEntry *pFsRrmConfigEntry1 =
        (tWsscfgFsRrmConfigEntry *) pRBElem1;
    tWsscfgFsRrmConfigEntry *pFsRrmConfigEntry2 =
        (tWsscfgFsRrmConfigEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsRrmConfigEntry1->MibObject.i4FsRrmRadioType >
        pFsRrmConfigEntry2->MibObject.i4FsRrmRadioType)
    {
        return 1;
    }
    else if (pFsRrmConfigEntry1->MibObject.i4FsRrmRadioType <
             pFsRrmConfigEntry2->MibObject.i4FsRrmRadioType)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  CapwapDot11WlanTableFilterInputs
 Input       :  The Indices
                pWsscfgCapwapDot11WlanEntry
                pWsscfgSetCapwapDot11WlanEntry
                pWsscfgIsSetCapwapDot11WlanEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
CapwapDot11WlanTableFilterInputs (tWsscfgCapwapDot11WlanEntry *
                                  pWsscfgCapwapDot11WlanEntry,
                                  tWsscfgCapwapDot11WlanEntry *
                                  pWsscfgSetCapwapDot11WlanEntry,
                                  tWsscfgIsSetCapwapDot11WlanEntry *
                                  pWsscfgIsSetCapwapDot11WlanEntry)
{
    if (pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanProfileId ==
        OSIX_TRUE)
    {
        if (pWsscfgCapwapDot11WlanEntry->
            MibObject.u4CapwapDot11WlanProfileId ==
            pWsscfgSetCapwapDot11WlanEntry->MibObject.
            u4CapwapDot11WlanProfileId)
            pWsscfgIsSetCapwapDot11WlanEntry->
                bCapwapDot11WlanProfileId = OSIX_FALSE;
    }
    if (pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanMacType == OSIX_TRUE)
    {
        if (pWsscfgCapwapDot11WlanEntry->
            MibObject.i4CapwapDot11WlanMacType ==
            pWsscfgSetCapwapDot11WlanEntry->MibObject.i4CapwapDot11WlanMacType)
            pWsscfgIsSetCapwapDot11WlanEntry->
                bCapwapDot11WlanMacType = OSIX_FALSE;
    }
    if (pWsscfgIsSetCapwapDot11WlanEntry->
        bCapwapDot11WlanTunnelMode == OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgCapwapDot11WlanEntry->MibObject.
              au1CapwapDot11WlanTunnelMode,
              pWsscfgSetCapwapDot11WlanEntry->MibObject.
              au1CapwapDot11WlanTunnelMode,
              pWsscfgSetCapwapDot11WlanEntry->MibObject.
              i4CapwapDot11WlanTunnelModeLen) == 0)
            && (pWsscfgCapwapDot11WlanEntry->MibObject.
                i4CapwapDot11WlanTunnelModeLen ==
                pWsscfgSetCapwapDot11WlanEntry->MibObject.
                i4CapwapDot11WlanTunnelModeLen))
            pWsscfgIsSetCapwapDot11WlanEntry->
                bCapwapDot11WlanTunnelMode = OSIX_FALSE;
    }
    if (pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanRowStatus ==
        OSIX_TRUE)
    {
        if (pWsscfgCapwapDot11WlanEntry->
            MibObject.i4CapwapDot11WlanRowStatus ==
            pWsscfgSetCapwapDot11WlanEntry->MibObject.
            i4CapwapDot11WlanRowStatus)
            pWsscfgIsSetCapwapDot11WlanEntry->
                bCapwapDot11WlanRowStatus = OSIX_FALSE;
    }
    if ((pWsscfgIsSetCapwapDot11WlanEntry->
         bCapwapDot11WlanProfileId == OSIX_FALSE)
        && (pWsscfgIsSetCapwapDot11WlanEntry->
            bCapwapDot11WlanMacType == OSIX_FALSE)
        && (pWsscfgIsSetCapwapDot11WlanEntry->
            bCapwapDot11WlanTunnelMode == OSIX_FALSE)
        && (pWsscfgIsSetCapwapDot11WlanEntry->
            bCapwapDot11WlanRowStatus == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  CapwapDot11WlanBindTableFilterInputs
 Input       :  The Indices
                pWsscfgCapwapDot11WlanBindEntry
                pWsscfgSetCapwapDot11WlanBindEntry
                pWsscfgIsSetCapwapDot11WlanBindEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
CapwapDot11WlanBindTableFilterInputs (tWsscfgCapwapDot11WlanBindEntry *
                                      pWsscfgCapwapDot11WlanBindEntry,
                                      tWsscfgCapwapDot11WlanBindEntry *
                                      pWsscfgSetCapwapDot11WlanBindEntry,
                                      tWsscfgIsSetCapwapDot11WlanBindEntry *
                                      pWsscfgIsSetCapwapDot11WlanBindEntry)
{
    if (pWsscfgIsSetCapwapDot11WlanBindEntry->bCapwapDot11WlanBindRowStatus ==
        OSIX_TRUE)
    {
        if (pWsscfgCapwapDot11WlanBindEntry->MibObject.
            i4CapwapDot11WlanBindRowStatus ==
            pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
            i4CapwapDot11WlanBindRowStatus)
            pWsscfgIsSetCapwapDot11WlanBindEntry->
                bCapwapDot11WlanBindRowStatus = OSIX_FALSE;
    }
    if (pWsscfgIsSetCapwapDot11WlanBindEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgCapwapDot11WlanBindEntry->MibObject.i4IfIndex ==
            pWsscfgSetCapwapDot11WlanBindEntry->MibObject.i4IfIndex)
            pWsscfgIsSetCapwapDot11WlanBindEntry->bIfIndex = OSIX_FALSE;
    }
    if (pWsscfgIsSetCapwapDot11WlanBindEntry->
        bCapwapDot11WlanProfileId == OSIX_TRUE)
    {
        if (pWsscfgCapwapDot11WlanBindEntry->MibObject.
            u4CapwapDot11WlanProfileId ==
            pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
            u4CapwapDot11WlanProfileId)
            pWsscfgIsSetCapwapDot11WlanBindEntry->bCapwapDot11WlanProfileId
                = OSIX_FALSE;
    }
    if ((pWsscfgIsSetCapwapDot11WlanBindEntry->bCapwapDot11WlanBindRowStatus ==
         OSIX_FALSE)
        && (pWsscfgIsSetCapwapDot11WlanBindEntry->bIfIndex == OSIX_FALSE)
        && (pWsscfgIsSetCapwapDot11WlanBindEntry->bCapwapDot11WlanProfileId ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  WsscfgSetAllCapwapDot11WlanTableTrigger
 Input       :  The Indices
                pWsscfgSetCapwapDot11WlanEntry
                pWsscfgIsSetCapwapDot11WlanEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllCapwapDot11WlanTableTrigger (tWsscfgCapwapDot11WlanEntry *
                                         pWsscfgSetCapwapDot11WlanEntry,
                                         tWsscfgIsSetCapwapDot11WlanEntry *
                                         pWsscfgIsSetCapwapDot11WlanEntry,
                                         INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE CapwapDot11WlanTunnelModeVal;
    UINT1               au1CapwapDot11WlanTunnelModeVal[256];

    MEMSET (au1CapwapDot11WlanTunnelModeVal, 0,
            sizeof (au1CapwapDot11WlanTunnelModeVal));
    CapwapDot11WlanTunnelModeVal.pu1_OctetList =
        au1CapwapDot11WlanTunnelModeVal;
    CapwapDot11WlanTunnelModeVal.i4_Length = 0;

    if (pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanProfileId ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapDot11WlanProfileId, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%u %u",
                      pWsscfgSetCapwapDot11WlanEntry->MibObject.
                      u4CapwapDot11WlanProfileId,
                      pWsscfgSetCapwapDot11WlanEntry->MibObject.
                      u4CapwapDot11WlanProfileId);
    }
    if (pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanMacType == OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapDot11WlanMacType, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%u %i",
                      pWsscfgSetCapwapDot11WlanEntry->MibObject.
                      u4CapwapDot11WlanProfileId,
                      pWsscfgSetCapwapDot11WlanEntry->MibObject.
                      i4CapwapDot11WlanMacType);
    }
    if (pWsscfgIsSetCapwapDot11WlanEntry->
        bCapwapDot11WlanTunnelMode == OSIX_TRUE)
    {
        MEMCPY (CapwapDot11WlanTunnelModeVal.pu1_OctetList,
                pWsscfgSetCapwapDot11WlanEntry->MibObject.
                au1CapwapDot11WlanTunnelMode,
                pWsscfgSetCapwapDot11WlanEntry->MibObject.
                i4CapwapDot11WlanTunnelModeLen);
        CapwapDot11WlanTunnelModeVal.i4_Length =
            pWsscfgSetCapwapDot11WlanEntry->MibObject.
            i4CapwapDot11WlanTunnelModeLen;

        nmhSetCmnNew (CapwapDot11WlanTunnelMode, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%u %s",
                      pWsscfgSetCapwapDot11WlanEntry->MibObject.
                      u4CapwapDot11WlanProfileId,
                      &CapwapDot11WlanTunnelModeVal);
    }
    if (pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapDot11WlanRowStatus, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 1, 1, i4SetOption,
                      "%u %i",
                      pWsscfgSetCapwapDot11WlanEntry->MibObject.
                      u4CapwapDot11WlanProfileId,
                      pWsscfgSetCapwapDot11WlanEntry->MibObject.
                      i4CapwapDot11WlanRowStatus);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllCapwapDot11WlanBindTableTrigger
 Input       :  The Indices
                pWsscfgSetCapwapDot11WlanBindEntry
                pWsscfgIsSetCapwapDot11WlanBindEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllCapwapDot11WlanBindTableTrigger (tWsscfgCapwapDot11WlanBindEntry *
                                             pWsscfgSetCapwapDot11WlanBindEntry,
                                             tWsscfgIsSetCapwapDot11WlanBindEntry
                                             *
                                             pWsscfgIsSetCapwapDot11WlanBindEntry,
                                             INT4 i4SetOption)
{

    if (pWsscfgIsSetCapwapDot11WlanBindEntry->bCapwapDot11WlanBindRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapDot11WlanBindRowStatus, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 1,
                      2, i4SetOption, "%i %u %i",
                      pWsscfgSetCapwapDot11WlanBindEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
                      u4CapwapDot11WlanProfileId,
                      pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
                      i4CapwapDot11WlanBindRowStatus);
    }
    if (pWsscfgIsSetCapwapDot11WlanBindEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %u %i",
                      pWsscfgSetCapwapDot11WlanBindEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
                      u4CapwapDot11WlanProfileId,
                      pWsscfgSetCapwapDot11WlanBindEntry->MibObject.i4IfIndex);
    }
    if (pWsscfgIsSetCapwapDot11WlanBindEntry->
        bCapwapDot11WlanProfileId == OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapDot11WlanProfileId, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %u %u",
                      pWsscfgSetCapwapDot11WlanBindEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
                      u4CapwapDot11WlanProfileId,
                      pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
                      u4CapwapDot11WlanProfileId);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgCapwapDot11WlanTableCreateApi
 Input       :  pWsscfgCapwapDot11WlanEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgCapwapDot11WlanEntry *
WsscfgCapwapDot11WlanTableCreateApi (tWsscfgCapwapDot11WlanEntry *
                                     pSetWsscfgCapwapDot11WlanEntry)
{
    tWsscfgCapwapDot11WlanEntry *pWsscfgCapwapDot11WlanEntry = NULL;

    if (pSetWsscfgCapwapDot11WlanEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgCapwapDot11WlanTableCreatApi: pSetWsscfgCapwapDot11WlanEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgCapwapDot11WlanEntry = (tWsscfgCapwapDot11WlanEntry *)
        MemAllocMemBlk (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID);
    if (pWsscfgCapwapDot11WlanEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgCapwapDot11WlanTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgCapwapDot11WlanEntry,
                pSetWsscfgCapwapDot11WlanEntry,
                sizeof (tWsscfgCapwapDot11WlanEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.CapwapDot11WlanTable,
             (tRBElem *) pWsscfgCapwapDot11WlanEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgCapwapDot11WlanTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                                (UINT1 *) pWsscfgCapwapDot11WlanEntry);
            return NULL;
        }
        return pWsscfgCapwapDot11WlanEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgCapwapDot11WlanBindTableCreateApi
 Input       :  pWsscfgCapwapDot11WlanBindEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgCapwapDot11WlanBindEntry
    * WsscfgCapwapDot11WlanBindTableCreateApi (tWsscfgCapwapDot11WlanBindEntry *
                                               pSetWsscfgCapwapDot11WlanBindEntry)
{
    tWsscfgCapwapDot11WlanBindEntry *pWsscfgCapwapDot11WlanBindEntry = NULL;

    if (pSetWsscfgCapwapDot11WlanBindEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgCapwapDot11WlanBindTableCreatApi: pSetWsscfgCapwapDot11WlanBindEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgCapwapDot11WlanBindEntry =
        (tWsscfgCapwapDot11WlanBindEntry *)
        MemAllocMemBlk (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID);
    if (pWsscfgCapwapDot11WlanBindEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgCapwapDot11WlanBindTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgCapwapDot11WlanBindEntry,
                pSetWsscfgCapwapDot11WlanBindEntry,
                sizeof (tWsscfgCapwapDot11WlanBindEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.CapwapDot11WlanBindTable,
             (tRBElem *) pWsscfgCapwapDot11WlanBindEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgCapwapDot11WlanBindTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                                (UINT1 *) pWsscfgCapwapDot11WlanBindEntry);
            return NULL;
        }
        return pWsscfgCapwapDot11WlanBindEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsDot11StationConfigTableCreate
 Input       :  None
 Description :  This function creates the FsDot11StationConfigTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsDot11StationConfigTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsDot11StationConfigEntry,
                       MibObject.FsDot11StationConfigTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.FsDot11StationConfigTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsDot11StationConfigTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsDot11CapabilityProfileTableCreate
 Input       :  None
 Description :  This function creates the FsDot11CapabilityProfileTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsDot11CapabilityProfileTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsDot11CapabilityProfileEntry,
                       MibObject.FsDot11CapabilityProfileTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.FsDot11CapabilityProfileTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsDot11CapabilityProfileTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsDot11AuthenticationProfileTableCreate
 Input       :  None
 Description :  This function creates the FsDot11AuthenticationProfileTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsDot11AuthenticationProfileTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsDot11AuthenticationProfileEntry,
                       MibObject.FsDot11AuthenticationProfileTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.
         FsDot11AuthenticationProfileTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsDot11AuthenticationProfileTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsStationQosParamsTableCreate
 Input       :  None
 Description :  This function creates the FsStationQosParamsTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsStationQosParamsTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsStationQosParamsEntry,
                       MibObject.FsStationQosParamsTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.FsStationQosParamsTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsStationQosParamsTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsVlanIsolationTableCreate
 Input       :  None
 Description :  This function creates the FsVlanIsolationTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsVlanIsolationTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsVlanIsolationEntry,
                       MibObject.FsVlanIsolationTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.FsVlanIsolationTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsVlanIsolationTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsDot11RadioConfigTableCreate
 Input       :  None
 Description :  This function creates the FsDot11RadioConfigTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsDot11RadioConfigTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsDot11RadioConfigEntry,
                       MibObject.FsDot11RadioConfigTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.FsDot11RadioConfigTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsDot11RadioConfigTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsDot11QosProfileTableCreate
 Input       :  None
 Description :  This function creates the FsDot11QosProfileTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsDot11QosProfileTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsDot11QosProfileEntry,
                       MibObject.FsDot11QosProfileTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.FsDot11QosProfileTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsDot11QosProfileTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsDot11WlanCapabilityProfileTableCreate
 Input       :  None
 Description :  This function creates the FsDot11WlanCapabilityProfileTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsDot11WlanCapabilityProfileTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsDot11WlanCapabilityProfileEntry,
                       MibObject.FsDot11WlanCapabilityProfileTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.
         FsDot11WlanCapabilityProfileTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsDot11WlanCapabilityProfileTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsDot11WlanAuthenticationProfileTableCreate
 Input       :  None
 Description :  This function creates the FsDot11WlanAuthenticationProfileTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsDot11WlanAuthenticationProfileTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsDot11WlanAuthenticationProfileEntry,
                       MibObject.FsDot11WlanAuthenticationProfileTableNode);

    if ((gWsscfgGlobals.
         WsscfgGlbMib.FsDot11WlanAuthenticationProfileTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsDot11WlanAuthenticationProfileTableRBCmp))
        == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsDot11WlanQosProfileTableCreate
 Input       :  None
 Description :  This function creates the FsDot11WlanQosProfileTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsDot11WlanQosProfileTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsDot11WlanQosProfileEntry,
                       MibObject.FsDot11WlanQosProfileTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.FsDot11WlanQosProfileTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsDot11WlanQosProfileTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsDot11RadioQosTableCreate
 Input       :  None
 Description :  This function creates the FsDot11RadioQosTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsDot11RadioQosTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsDot11RadioQosEntry,
                       MibObject.FsDot11RadioQosTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.FsDot11RadioQosTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsDot11RadioQosTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsDot11QAPTableCreate
 Input       :  None
 Description :  This function creates the FsDot11QAPTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsDot11QAPTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsDot11QAPEntry, MibObject.FsDot11QAPTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.FsDot11QAPTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsDot11QAPTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsQAPProfileTableCreate
 Input       :  None
 Description :  This function creates the FsQAPProfileTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsQAPProfileTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsQAPProfileEntry,
                       MibObject.FsQAPProfileTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.FsQAPProfileTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsQAPProfileTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsDot11CapabilityMappingTableCreate
 Input       :  None
 Description :  This function creates the FsDot11CapabilityMappingTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsDot11CapabilityMappingTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsDot11CapabilityMappingEntry,
                       MibObject.FsDot11CapabilityMappingTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.FsDot11CapabilityMappingTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsDot11CapabilityMappingTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsDot11AuthMappingTableCreate
 Input       :  None
 Description :  This function creates the FsDot11AuthMappingTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsDot11AuthMappingTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsDot11AuthMappingEntry,
                       MibObject.FsDot11AuthMappingTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.FsDot11AuthMappingTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsDot11AuthMappingTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsDot11QosMappingTableCreate
 Input       :  None
 Description :  This function creates the FsDot11QosMappingTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsDot11QosMappingTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsDot11QosMappingEntry,
                       MibObject.FsDot11QosMappingTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.FsDot11QosMappingTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsDot11QosMappingTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsDot11ClientSummaryTableCreate
 Input       :  None
 Description :  This function creates the FsDot11ClientSummaryTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsDot11ClientSummaryTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsDot11ClientSummaryEntry,
                       MibObject.FsDot11ClientSummaryTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.FsDot11ClientSummaryTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsDot11ClientSummaryTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsDot11AntennasListTableCreate
 Input       :  None
 Description :  This function creates the FsDot11AntennasListTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsDot11AntennasListTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsDot11AntennasListEntry,
                       MibObject.FsDot11AntennasListTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.FsDot11AntennasListTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsDot11AntennasListTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsDot11WlanTableCreate
 Input       :  None
 Description :  This function creates the FsDot11WlanTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsDot11WlanTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsDot11WlanEntry, MibObject.FsDot11WlanTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.FsDot11WlanTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsDot11WlanTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsDot11WlanBindTableCreate
 Input       :  None
 Description :  This function creates the FsDot11WlanBindTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsDot11WlanBindTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsDot11WlanBindEntry,
                       MibObject.FsDot11WlanBindTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.FsDot11WlanBindTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsDot11WlanBindTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsDot11nConfigTableCreate
 Input       :  None
 Description :  This function creates the FsDot11nConfigTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsDot11nConfigTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsDot11nConfigEntry,
                       MibObject.FsDot11nConfigTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.FsDot11nConfigTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsDot11nConfigTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsDot11nMCSDataRateTableCreate
 Input       :  None
 Description :  This function creates the FsDot11nMCSDataRateTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsDot11nMCSDataRateTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsDot11nMCSDataRateEntry,
                       MibObject.FsDot11nMCSDataRateTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.FsDot11nMCSDataRateTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsDot11nMCSDataRateTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsWtpImageUpgradeTableCreate
 Input       :  None
 Description :  This function creates the FsWtpImageUpgradeTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
WsscfgFsWtpImageUpgradeTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tWsscfgFsWtpImageUpgradeEntry,
                       MibObject.FsWtpImageUpgradeTableNode);

    if ((gWsscfgGlobals.WsscfgGlbMib.FsWtpImageUpgradeTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsWtpImageUpgradeTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FsDot11StationConfigTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsDot11StationConfigTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsDot11StationConfigTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsDot11StationConfigEntry *pFsDot11StationConfigEntry1 =
        (tWsscfgFsDot11StationConfigEntry *) pRBElem1;
    tWsscfgFsDot11StationConfigEntry *pFsDot11StationConfigEntry2 =
        (tWsscfgFsDot11StationConfigEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsDot11StationConfigEntry1->MibObject.i4IfIndex >
        pFsDot11StationConfigEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pFsDot11StationConfigEntry1->MibObject.i4IfIndex <
             pFsDot11StationConfigEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsDot11CapabilityProfileTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsDot11CapabilityProfileTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsDot11CapabilityProfileTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsDot11CapabilityProfileEntry
        * pFsDot11CapabilityProfileEntry1 =
        (tWsscfgFsDot11CapabilityProfileEntry *) pRBElem1;
    tWsscfgFsDot11CapabilityProfileEntry
        * pFsDot11CapabilityProfileEntry2 =
        (tWsscfgFsDot11CapabilityProfileEntry *) pRBElem2;
    INT4                i4MaxLength = 0;
    i4MaxLength = 0;
    if (pFsDot11CapabilityProfileEntry1->
        MibObject.i4FsDot11CapabilityProfileNameLen >
        pFsDot11CapabilityProfileEntry2->
        MibObject.i4FsDot11CapabilityProfileNameLen)
    {
        i4MaxLength =
            pFsDot11CapabilityProfileEntry1->
            MibObject.i4FsDot11CapabilityProfileNameLen;
    }
    else
    {
        i4MaxLength =
            pFsDot11CapabilityProfileEntry2->
            MibObject.i4FsDot11CapabilityProfileNameLen;
    }

    if (MEMCMP
        (pFsDot11CapabilityProfileEntry1->
         MibObject.au1FsDot11CapabilityProfileName,
         pFsDot11CapabilityProfileEntry2->
         MibObject.au1FsDot11CapabilityProfileName, i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsDot11CapabilityProfileEntry1->
              MibObject.au1FsDot11CapabilityProfileName,
              pFsDot11CapabilityProfileEntry2->
              MibObject.au1FsDot11CapabilityProfileName, i4MaxLength) < 0)
    {
        return -1;
    }

    if (pFsDot11CapabilityProfileEntry1->
        MibObject.i4FsDot11CapabilityProfileNameLen >
        pFsDot11CapabilityProfileEntry2->
        MibObject.i4FsDot11CapabilityProfileNameLen)
    {
        return 1;
    }
    else if (pFsDot11CapabilityProfileEntry1->
             MibObject.i4FsDot11CapabilityProfileNameLen <
             pFsDot11CapabilityProfileEntry2->
             MibObject.i4FsDot11CapabilityProfileNameLen)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  FsDot11AuthenticationProfileTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsDot11AuthenticationProfileTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsDot11AuthenticationProfileTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsDot11AuthenticationProfileEntry
        * pFsDot11AuthenticationProfileEntry1 =
        (tWsscfgFsDot11AuthenticationProfileEntry *) pRBElem1;
    tWsscfgFsDot11AuthenticationProfileEntry
        * pFsDot11AuthenticationProfileEntry2 =
        (tWsscfgFsDot11AuthenticationProfileEntry *) pRBElem2;
    INT4                i4MaxLength = 0;
    i4MaxLength = 0;
    if (pFsDot11AuthenticationProfileEntry1->
        MibObject.i4FsDot11AuthenticationProfileNameLen >
        pFsDot11AuthenticationProfileEntry2->
        MibObject.i4FsDot11AuthenticationProfileNameLen)
    {
        i4MaxLength =
            pFsDot11AuthenticationProfileEntry1->
            MibObject.i4FsDot11AuthenticationProfileNameLen;
    }
    else
    {
        i4MaxLength =
            pFsDot11AuthenticationProfileEntry2->
            MibObject.i4FsDot11AuthenticationProfileNameLen;
    }

    if (MEMCMP
        (pFsDot11AuthenticationProfileEntry1->
         MibObject.au1FsDot11AuthenticationProfileName,
         pFsDot11AuthenticationProfileEntry2->
         MibObject.au1FsDot11AuthenticationProfileName, i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsDot11AuthenticationProfileEntry1->
              MibObject.au1FsDot11AuthenticationProfileName,
              pFsDot11AuthenticationProfileEntry2->
              MibObject.au1FsDot11AuthenticationProfileName, i4MaxLength) < 0)
    {
        return -1;
    }

    if (pFsDot11AuthenticationProfileEntry1->
        MibObject.i4FsDot11AuthenticationProfileNameLen >
        pFsDot11AuthenticationProfileEntry2->
        MibObject.i4FsDot11AuthenticationProfileNameLen)
    {
        return 1;
    }
    else if (pFsDot11AuthenticationProfileEntry1->
             MibObject.i4FsDot11AuthenticationProfileNameLen <
             pFsDot11AuthenticationProfileEntry2->
             MibObject.i4FsDot11AuthenticationProfileNameLen)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  FsStationQosParamsTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsStationQosParamsTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsStationQosParamsTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsStationQosParamsEntry *pFsStationQosParamsEntry1 =
        (tWsscfgFsStationQosParamsEntry *) pRBElem1;
    tWsscfgFsStationQosParamsEntry *pFsStationQosParamsEntry2 =
        (tWsscfgFsStationQosParamsEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (MEMCMP
        (pFsStationQosParamsEntry1->MibObject.FsStaMacAddress,
         pFsStationQosParamsEntry2->MibObject.FsStaMacAddress, 6) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsStationQosParamsEntry1->MibObject.FsStaMacAddress,
              pFsStationQosParamsEntry2->MibObject.FsStaMacAddress, 6) < 0)
    {
        return -1;
    }

    if (pFsStationQosParamsEntry1->MibObject.i4IfIndex >
        pFsStationQosParamsEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pFsStationQosParamsEntry1->MibObject.i4IfIndex <
             pFsStationQosParamsEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsVlanIsolationTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsVlanIsolationTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsVlanIsolationTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsVlanIsolationEntry *pFsVlanIsolationEntry1 =
        (tWsscfgFsVlanIsolationEntry *) pRBElem1;
    tWsscfgFsVlanIsolationEntry *pFsVlanIsolationEntry2 =
        (tWsscfgFsVlanIsolationEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsVlanIsolationEntry1->MibObject.i4IfIndex >
        pFsVlanIsolationEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pFsVlanIsolationEntry1->MibObject.i4IfIndex <
             pFsVlanIsolationEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsDot11RadioConfigTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsDot11RadioConfigTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsDot11RadioConfigTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsDot11RadioConfigEntry *pFsDot11RadioConfigEntry1 =
        (tWsscfgFsDot11RadioConfigEntry *) pRBElem1;
    tWsscfgFsDot11RadioConfigEntry *pFsDot11RadioConfigEntry2 =
        (tWsscfgFsDot11RadioConfigEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsDot11RadioConfigEntry1->MibObject.i4IfIndex >
        pFsDot11RadioConfigEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pFsDot11RadioConfigEntry1->MibObject.i4IfIndex <
             pFsDot11RadioConfigEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsDot11QosProfileTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsDot11QosProfileTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsDot11QosProfileTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsDot11QosProfileEntry *pFsDot11QosProfileEntry1 =
        (tWsscfgFsDot11QosProfileEntry *) pRBElem1;
    tWsscfgFsDot11QosProfileEntry *pFsDot11QosProfileEntry2 =
        (tWsscfgFsDot11QosProfileEntry *) pRBElem2;
    INT4                i4MaxLength = 0;
    i4MaxLength = 0;
    if (pFsDot11QosProfileEntry1->MibObject.
        i4FsDot11QosProfileNameLen >
        pFsDot11QosProfileEntry2->MibObject.i4FsDot11QosProfileNameLen)
    {
        i4MaxLength =
            pFsDot11QosProfileEntry1->MibObject.i4FsDot11QosProfileNameLen;
    }
    else
    {
        i4MaxLength =
            pFsDot11QosProfileEntry2->MibObject.i4FsDot11QosProfileNameLen;
    }

    if (MEMCMP
        (pFsDot11QosProfileEntry1->MibObject.au1FsDot11QosProfileName,
         pFsDot11QosProfileEntry2->MibObject.au1FsDot11QosProfileName,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsDot11QosProfileEntry1->MibObject.
              au1FsDot11QosProfileName,
              pFsDot11QosProfileEntry2->MibObject.
              au1FsDot11QosProfileName, i4MaxLength) < 0)
    {
        return -1;
    }

    if (pFsDot11QosProfileEntry1->MibObject.
        i4FsDot11QosProfileNameLen >
        pFsDot11QosProfileEntry2->MibObject.i4FsDot11QosProfileNameLen)
    {
        return 1;
    }
    else if (pFsDot11QosProfileEntry1->
             MibObject.i4FsDot11QosProfileNameLen <
             pFsDot11QosProfileEntry2->MibObject.i4FsDot11QosProfileNameLen)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  FsDot11WlanCapabilityProfileTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsDot11WlanCapabilityProfileTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsDot11WlanCapabilityProfileTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsDot11WlanCapabilityProfileEntry
        * pFsDot11WlanCapabilityProfileEntry1 =
        (tWsscfgFsDot11WlanCapabilityProfileEntry *) pRBElem1;
    tWsscfgFsDot11WlanCapabilityProfileEntry
        * pFsDot11WlanCapabilityProfileEntry2 =
        (tWsscfgFsDot11WlanCapabilityProfileEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsDot11WlanCapabilityProfileEntry1->MibObject.i4IfIndex >
        pFsDot11WlanCapabilityProfileEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pFsDot11WlanCapabilityProfileEntry1->MibObject.
             i4IfIndex <
             pFsDot11WlanCapabilityProfileEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsDot11WlanAuthenticationProfileTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsDot11WlanAuthenticationProfileTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsDot11WlanAuthenticationProfileTableRBCmp (tRBElem * pRBElem1,
                                            tRBElem * pRBElem2)
{

    tWsscfgFsDot11WlanAuthenticationProfileEntry
        * pFsDot11WlanAuthenticationProfileEntry1 =
        (tWsscfgFsDot11WlanAuthenticationProfileEntry *) pRBElem1;
    tWsscfgFsDot11WlanAuthenticationProfileEntry
        * pFsDot11WlanAuthenticationProfileEntry2 =
        (tWsscfgFsDot11WlanAuthenticationProfileEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsDot11WlanAuthenticationProfileEntry1->MibObject.i4IfIndex >
        pFsDot11WlanAuthenticationProfileEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pFsDot11WlanAuthenticationProfileEntry1->
             MibObject.i4IfIndex <
             pFsDot11WlanAuthenticationProfileEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsDot11WlanQosProfileTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsDot11WlanQosProfileTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsDot11WlanQosProfileTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsDot11WlanQosProfileEntry *pFsDot11WlanQosProfileEntry1 =
        (tWsscfgFsDot11WlanQosProfileEntry *) pRBElem1;
    tWsscfgFsDot11WlanQosProfileEntry *pFsDot11WlanQosProfileEntry2 =
        (tWsscfgFsDot11WlanQosProfileEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsDot11WlanQosProfileEntry1->MibObject.i4IfIndex >
        pFsDot11WlanQosProfileEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pFsDot11WlanQosProfileEntry1->MibObject.i4IfIndex <
             pFsDot11WlanQosProfileEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsDot11RadioQosTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsDot11RadioQosTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsDot11RadioQosTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsDot11RadioQosEntry *pFsDot11RadioQosEntry1 =
        (tWsscfgFsDot11RadioQosEntry *) pRBElem1;
    tWsscfgFsDot11RadioQosEntry *pFsDot11RadioQosEntry2 =
        (tWsscfgFsDot11RadioQosEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsDot11RadioQosEntry1->MibObject.i4IfIndex >
        pFsDot11RadioQosEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pFsDot11RadioQosEntry1->MibObject.i4IfIndex <
             pFsDot11RadioQosEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsDot11QAPTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsDot11QAPTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsDot11QAPTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsDot11QAPEntry *pFsDot11QAPEntry1 =
        (tWsscfgFsDot11QAPEntry *) pRBElem1;
    tWsscfgFsDot11QAPEntry *pFsDot11QAPEntry2 =
        (tWsscfgFsDot11QAPEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsDot11QAPEntry1->MibObject.i4IfIndex >
        pFsDot11QAPEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pFsDot11QAPEntry1->MibObject.i4IfIndex <
             pFsDot11QAPEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    if (pFsDot11QAPEntry1->MibObject.i4Dot11EDCATableIndex >
        pFsDot11QAPEntry2->MibObject.i4Dot11EDCATableIndex)
    {
        return 1;
    }
    else if (pFsDot11QAPEntry1->MibObject.i4Dot11EDCATableIndex <
             pFsDot11QAPEntry2->MibObject.i4Dot11EDCATableIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsQAPProfileTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsQAPProfileTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsQAPProfileTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsQAPProfileEntry *pFsQAPProfileEntry1 =
        (tWsscfgFsQAPProfileEntry *) pRBElem1;
    tWsscfgFsQAPProfileEntry *pFsQAPProfileEntry2 =
        (tWsscfgFsQAPProfileEntry *) pRBElem2;
    INT4                i4MaxLength = 0;
    i4MaxLength = 0;
    if (pFsQAPProfileEntry1->MibObject.i4FsQAPProfileNameLen >
        pFsQAPProfileEntry2->MibObject.i4FsQAPProfileNameLen)
    {
        i4MaxLength = pFsQAPProfileEntry1->MibObject.i4FsQAPProfileNameLen;
    }
    else
    {
        i4MaxLength = pFsQAPProfileEntry2->MibObject.i4FsQAPProfileNameLen;
    }

    if (MEMCMP
        (pFsQAPProfileEntry1->MibObject.au1FsQAPProfileName,
         pFsQAPProfileEntry2->MibObject.au1FsQAPProfileName, i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsQAPProfileEntry1->MibObject.au1FsQAPProfileName,
              pFsQAPProfileEntry2->MibObject.au1FsQAPProfileName,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pFsQAPProfileEntry1->MibObject.i4FsQAPProfileNameLen >
        pFsQAPProfileEntry2->MibObject.i4FsQAPProfileNameLen)
    {
        return 1;
    }
    else if (pFsQAPProfileEntry1->MibObject.i4FsQAPProfileNameLen <
             pFsQAPProfileEntry2->MibObject.i4FsQAPProfileNameLen)
    {
        return -1;
    }

    if (pFsQAPProfileEntry1->MibObject.i4FsQAPProfileIndex >
        pFsQAPProfileEntry2->MibObject.i4FsQAPProfileIndex)
    {
        return 1;
    }
    else if (pFsQAPProfileEntry1->MibObject.i4FsQAPProfileIndex <
             pFsQAPProfileEntry2->MibObject.i4FsQAPProfileIndex)
    {
        return -1;
    }

    return 0;
}

/****************************************************************************
 Function    :  FsDot11CapabilityMappingTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsDot11CapabilityMappingTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsDot11CapabilityMappingTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsDot11CapabilityMappingEntry
        * pFsDot11CapabilityMappingEntry1 =
        (tWsscfgFsDot11CapabilityMappingEntry *) pRBElem1;
    tWsscfgFsDot11CapabilityMappingEntry
        * pFsDot11CapabilityMappingEntry2 =
        (tWsscfgFsDot11CapabilityMappingEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsDot11CapabilityMappingEntry1->MibObject.i4IfIndex >
        pFsDot11CapabilityMappingEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pFsDot11CapabilityMappingEntry1->MibObject.i4IfIndex <
             pFsDot11CapabilityMappingEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);
    return 0;
}

/****************************************************************************
 Function    :  FsDot11AuthMappingTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsDot11AuthMappingTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsDot11AuthMappingTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsDot11AuthMappingEntry *pFsDot11AuthMappingEntry1 =
        (tWsscfgFsDot11AuthMappingEntry *) pRBElem1;
    tWsscfgFsDot11AuthMappingEntry *pFsDot11AuthMappingEntry2 =
        (tWsscfgFsDot11AuthMappingEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsDot11AuthMappingEntry1->MibObject.i4IfIndex >
        pFsDot11AuthMappingEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pFsDot11AuthMappingEntry1->MibObject.i4IfIndex <
             pFsDot11AuthMappingEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);
    return 0;
}

/****************************************************************************
 Function    :  FsDot11QosMappingTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsDot11QosMappingTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsDot11QosMappingTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsDot11QosMappingEntry *pFsDot11QosMappingEntry1 =
        (tWsscfgFsDot11QosMappingEntry *) pRBElem1;
    tWsscfgFsDot11QosMappingEntry *pFsDot11QosMappingEntry2 =
        (tWsscfgFsDot11QosMappingEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsDot11QosMappingEntry1->MibObject.i4IfIndex >
        pFsDot11QosMappingEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pFsDot11QosMappingEntry1->MibObject.i4IfIndex <
             pFsDot11QosMappingEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);
    return 0;
}

/****************************************************************************
 Function    :  FsDot11ClientSummaryTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsDot11ClientSummaryTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsDot11ClientSummaryTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsDot11ClientSummaryEntry *pFsDot11ClientSummaryEntry1 =
        (tWsscfgFsDot11ClientSummaryEntry *) pRBElem1;
    tWsscfgFsDot11ClientSummaryEntry *pFsDot11ClientSummaryEntry2 =
        (tWsscfgFsDot11ClientSummaryEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (MEMCMP
        (pFsDot11ClientSummaryEntry1->MibObject.FsDot11ClientMacAddress,
         pFsDot11ClientSummaryEntry2->MibObject.FsDot11ClientMacAddress, 6) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsDot11ClientSummaryEntry1->MibObject.FsDot11ClientMacAddress,
              pFsDot11ClientSummaryEntry2->MibObject.FsDot11ClientMacAddress,
              6) < 0)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsDot11AntennasListTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsDot11AntennasListTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsDot11AntennasListTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsDot11AntennasListEntry *pFsDot11AntennasListEntry1 =
        (tWsscfgFsDot11AntennasListEntry *) pRBElem1;
    tWsscfgFsDot11AntennasListEntry *pFsDot11AntennasListEntry2 =
        (tWsscfgFsDot11AntennasListEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsDot11AntennasListEntry1->MibObject.i4IfIndex >
        pFsDot11AntennasListEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pFsDot11AntennasListEntry1->MibObject.i4IfIndex <
             pFsDot11AntennasListEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    if (pFsDot11AntennasListEntry1->MibObject.
        i4Dot11AntennaListIndex >
        pFsDot11AntennasListEntry2->MibObject.i4Dot11AntennaListIndex)
    {
        return 1;
    }
    else if (pFsDot11AntennasListEntry1->
             MibObject.i4Dot11AntennaListIndex <
             pFsDot11AntennasListEntry2->MibObject.i4Dot11AntennaListIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsDot11WlanTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsDot11WlanTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsDot11WlanTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsDot11WlanEntry *pFsDot11WlanEntry1 =
        (tWsscfgFsDot11WlanEntry *) pRBElem1;
    tWsscfgFsDot11WlanEntry *pFsDot11WlanEntry2 =
        (tWsscfgFsDot11WlanEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsDot11WlanEntry1->MibObject.u4CapwapDot11WlanProfileId >
        pFsDot11WlanEntry2->MibObject.u4CapwapDot11WlanProfileId)
    {
        return 1;
    }
    else if (pFsDot11WlanEntry1->MibObject.
             u4CapwapDot11WlanProfileId <
             pFsDot11WlanEntry2->MibObject.u4CapwapDot11WlanProfileId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsDot11WlanBindTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsDot11WlanBindTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsDot11WlanBindTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsDot11WlanBindEntry *pFsDot11WlanBindEntry1 =
        (tWsscfgFsDot11WlanBindEntry *) pRBElem1;
    tWsscfgFsDot11WlanBindEntry *pFsDot11WlanBindEntry2 =
        (tWsscfgFsDot11WlanBindEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsDot11WlanBindEntry1->MibObject.i4IfIndex >
        pFsDot11WlanBindEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pFsDot11WlanBindEntry1->MibObject.i4IfIndex <
             pFsDot11WlanBindEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    if (pFsDot11WlanBindEntry1->MibObject.u4CapwapDot11WlanProfileId >
        pFsDot11WlanBindEntry2->MibObject.u4CapwapDot11WlanProfileId)
    {
        return 1;
    }
    else if (pFsDot11WlanBindEntry1->
             MibObject.u4CapwapDot11WlanProfileId <
             pFsDot11WlanBindEntry2->MibObject.u4CapwapDot11WlanProfileId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsDot11nConfigTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsDot11nConfigTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsDot11nConfigTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsDot11nConfigEntry *pFsDot11nConfigEntry1 =
        (tWsscfgFsDot11nConfigEntry *) pRBElem1;
    tWsscfgFsDot11nConfigEntry *pFsDot11nConfigEntry2 =
        (tWsscfgFsDot11nConfigEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsDot11nConfigEntry1->MibObject.i4IfIndex >
        pFsDot11nConfigEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pFsDot11nConfigEntry1->MibObject.i4IfIndex <
             pFsDot11nConfigEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsDot11nMCSDataRateTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsDot11nMCSDataRateTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsDot11nMCSDataRateTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsDot11nMCSDataRateEntry *pFsDot11nMCSDataRateEntry1 =
        (tWsscfgFsDot11nMCSDataRateEntry *) pRBElem1;
    tWsscfgFsDot11nMCSDataRateEntry *pFsDot11nMCSDataRateEntry2 =
        (tWsscfgFsDot11nMCSDataRateEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsDot11nMCSDataRateEntry1->MibObject.
        i4FsDot11nMCSDataRateIndex >
        pFsDot11nMCSDataRateEntry2->MibObject.i4FsDot11nMCSDataRateIndex)
    {
        return 1;
    }
    else if (pFsDot11nMCSDataRateEntry1->
             MibObject.i4FsDot11nMCSDataRateIndex <
             pFsDot11nMCSDataRateEntry2->MibObject.i4FsDot11nMCSDataRateIndex)
    {
        return -1;
    }

    if (pFsDot11nMCSDataRateEntry1->MibObject.i4IfIndex >
        pFsDot11nMCSDataRateEntry2->MibObject.i4IfIndex)
    {
        return 1;
    }
    else if (pFsDot11nMCSDataRateEntry1->MibObject.i4IfIndex <
             pFsDot11nMCSDataRateEntry2->MibObject.i4IfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsWtpImageUpgradeTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsWtpImageUpgradeTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsWtpImageUpgradeTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tWsscfgFsWtpImageUpgradeEntry *pFsWtpImageUpgradeEntry1 =
        (tWsscfgFsWtpImageUpgradeEntry *) pRBElem1;
    tWsscfgFsWtpImageUpgradeEntry *pFsWtpImageUpgradeEntry2 =
        (tWsscfgFsWtpImageUpgradeEntry *) pRBElem2;
    INT4                i4MaxLength = 0;
    i4MaxLength = 0;
    if (pFsWtpImageUpgradeEntry1->
        MibObject.i4CapwapBaseWtpProfileWtpModelNumberLen >
        pFsWtpImageUpgradeEntry2->
        MibObject.i4CapwapBaseWtpProfileWtpModelNumberLen)
    {
        i4MaxLength =
            pFsWtpImageUpgradeEntry1->
            MibObject.i4CapwapBaseWtpProfileWtpModelNumberLen;
    }
    else
    {
        i4MaxLength =
            pFsWtpImageUpgradeEntry2->
            MibObject.i4CapwapBaseWtpProfileWtpModelNumberLen;
    }

    if (MEMCMP
        (pFsWtpImageUpgradeEntry1->
         MibObject.au1CapwapBaseWtpProfileWtpModelNumber,
         pFsWtpImageUpgradeEntry2->
         MibObject.au1CapwapBaseWtpProfileWtpModelNumber, i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsWtpImageUpgradeEntry1->
              MibObject.au1CapwapBaseWtpProfileWtpModelNumber,
              pFsWtpImageUpgradeEntry2->
              MibObject.au1CapwapBaseWtpProfileWtpModelNumber, i4MaxLength) < 0)
    {
        return -1;
    }

    if (pFsWtpImageUpgradeEntry1->
        MibObject.i4CapwapBaseWtpProfileWtpModelNumberLen >
        pFsWtpImageUpgradeEntry2->
        MibObject.i4CapwapBaseWtpProfileWtpModelNumberLen)
    {
        return 1;
    }
    else if (pFsWtpImageUpgradeEntry1->
             MibObject.i4CapwapBaseWtpProfileWtpModelNumberLen <
             pFsWtpImageUpgradeEntry2->
             MibObject.i4CapwapBaseWtpProfileWtpModelNumberLen)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  FsDot11StationConfigTableFilterInputs
 Input       :  The Indices
                pWsscfgFsDot11StationConfigEntry
                pWsscfgSetFsDot11StationConfigEntry
                pWsscfgIsSetFsDot11StationConfigEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsDot11StationConfigTableFilterInputs (tWsscfgFsDot11StationConfigEntry *
                                       pWsscfgFsDot11StationConfigEntry,
                                       tWsscfgFsDot11StationConfigEntry *
                                       pWsscfgSetFsDot11StationConfigEntry,
                                       tWsscfgIsSetFsDot11StationConfigEntry *
                                       pWsscfgIsSetFsDot11StationConfigEntry)
{
    if (pWsscfgIsSetFsDot11StationConfigEntry->bFsDot11SupressSSID == OSIX_TRUE)
    {
        if (pWsscfgFsDot11StationConfigEntry->
            MibObject.i4FsDot11SupressSSID ==
            pWsscfgSetFsDot11StationConfigEntry->MibObject.i4FsDot11SupressSSID)
            pWsscfgIsSetFsDot11StationConfigEntry->
                bFsDot11SupressSSID = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11StationConfigEntry->bFsDot11VlanId == OSIX_TRUE)
    {
        if (pWsscfgFsDot11StationConfigEntry->MibObject.
            i4FsDot11VlanId ==
            pWsscfgSetFsDot11StationConfigEntry->MibObject.i4FsDot11VlanId)
            pWsscfgIsSetFsDot11StationConfigEntry->bFsDot11VlanId = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11StationConfigEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgFsDot11StationConfigEntry->MibObject.i4IfIndex ==
            pWsscfgSetFsDot11StationConfigEntry->MibObject.i4IfIndex)
            pWsscfgIsSetFsDot11StationConfigEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsDot11StationConfigEntry->bFsDot11SupressSSID ==
         OSIX_FALSE)
        && (pWsscfgIsSetFsDot11StationConfigEntry->bFsDot11VlanId ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11StationConfigEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsDot11CapabilityProfileTableFilterInputs
 Input       :  The Indices
                pWsscfgFsDot11CapabilityProfileEntry
                pWsscfgSetFsDot11CapabilityProfileEntry
                pWsscfgIsSetFsDot11CapabilityProfileEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    FsDot11CapabilityProfileTableFilterInputs
    (tWsscfgFsDot11CapabilityProfileEntry *
     pWsscfgFsDot11CapabilityProfileEntry,
     tWsscfgFsDot11CapabilityProfileEntry *
     pWsscfgSetFsDot11CapabilityProfileEntry,
     tWsscfgIsSetFsDot11CapabilityProfileEntry *
     pWsscfgIsSetFsDot11CapabilityProfileEntry)
{
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11CapabilityProfileName == OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgFsDot11CapabilityProfileEntry->
              MibObject.au1FsDot11CapabilityProfileName,
              pWsscfgSetFsDot11CapabilityProfileEntry->
              MibObject.au1FsDot11CapabilityProfileName,
              pWsscfgSetFsDot11CapabilityProfileEntry->
              MibObject.i4FsDot11CapabilityProfileNameLen) == 0)
            && (pWsscfgFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11CapabilityProfileNameLen ==
                pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11CapabilityProfileNameLen))
            pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11CapabilityProfileName = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11CFPollable == OSIX_TRUE)
    {
        if (pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11CFPollable ==
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11CFPollable)
            pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11CFPollable = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11CFPollRequest == OSIX_TRUE)
    {
        if (pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11CFPollRequest ==
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11CFPollRequest)
            pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11CFPollRequest
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11PrivacyOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11PrivacyOptionImplemented ==
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11PrivacyOptionImplemented)
            pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11PrivacyOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11ShortPreambleOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11ShortPreambleOptionImplemented ==
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11ShortPreambleOptionImplemented)
            pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11ShortPreambleOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11PBCCOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11PBCCOptionImplemented ==
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11PBCCOptionImplemented)
            pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11PBCCOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11ChannelAgilityPresent == OSIX_TRUE)
    {
        if (pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11ChannelAgilityPresent ==
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11ChannelAgilityPresent)
            pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11ChannelAgilityPresent = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11QosOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11QosOptionImplemented ==
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11QosOptionImplemented)
            pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11QosOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11SpectrumManagementRequired == OSIX_TRUE)
    {
        if (pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11SpectrumManagementRequired ==
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11SpectrumManagementRequired)
            pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11SpectrumManagementRequired = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11ShortSlotTimeOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11ShortSlotTimeOptionImplemented ==
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11ShortSlotTimeOptionImplemented)
            pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11ShortSlotTimeOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11APSDOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11APSDOptionImplemented ==
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11APSDOptionImplemented)
            pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11APSDOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11DSSSOFDMOptionEnabled == OSIX_TRUE)
    {
        if (pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11DSSSOFDMOptionEnabled ==
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11DSSSOFDMOptionEnabled)
            pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11DSSSOFDMOptionEnabled = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11DelayedBlockAckOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11DelayedBlockAckOptionImplemented ==
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11DelayedBlockAckOptionImplemented)
            pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11DelayedBlockAckOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11ImmediateBlockAckOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11ImmediateBlockAckOptionImplemented ==
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11ImmediateBlockAckOptionImplemented)
            pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11ImmediateBlockAckOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11QAckOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11QAckOptionImplemented ==
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11QAckOptionImplemented)
            pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11QAckOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11QueueRequestOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11QueueRequestOptionImplemented ==
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11QueueRequestOptionImplemented)
            pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11QueueRequestOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11TXOPRequestOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11TXOPRequestOptionImplemented ==
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11TXOPRequestOptionImplemented)
            pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11TXOPRequestOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11RSNAOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11RSNAOptionImplemented ==
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11RSNAOptionImplemented)
            pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11RSNAOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11RSNAPreauthenticationImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11RSNAPreauthenticationImplemented ==
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11RSNAPreauthenticationImplemented)
            pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11RSNAPreauthenticationImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11CapabilityRowStatus == OSIX_TRUE)
    {
        if (pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11CapabilityRowStatus ==
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11CapabilityRowStatus)
            pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11CapabilityRowStatus = OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsDot11CapabilityProfileEntry->
         bFsDot11CapabilityProfileName == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11CFPollable ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11CFPollRequest ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11CapabilityProfileEntry->
            bFsDot11PrivacyOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11CapabilityProfileEntry->
            bFsDot11ShortPreambleOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11CapabilityProfileEntry->
            bFsDot11PBCCOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11CapabilityProfileEntry->
            bFsDot11ChannelAgilityPresent == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11CapabilityProfileEntry->
            bFsDot11QosOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11CapabilityProfileEntry->
            bFsDot11SpectrumManagementRequired == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11CapabilityProfileEntry->
            bFsDot11ShortSlotTimeOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11CapabilityProfileEntry->
            bFsDot11APSDOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11CapabilityProfileEntry->
            bFsDot11DSSSOFDMOptionEnabled == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11CapabilityProfileEntry->
            bFsDot11DelayedBlockAckOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11CapabilityProfileEntry->
            bFsDot11ImmediateBlockAckOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11CapabilityProfileEntry->
            bFsDot11QAckOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11CapabilityProfileEntry->
            bFsDot11QueueRequestOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11CapabilityProfileEntry->
            bFsDot11TXOPRequestOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11CapabilityProfileEntry->
            bFsDot11RSNAOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11CapabilityProfileEntry->
            bFsDot11RSNAPreauthenticationImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11CapabilityProfileEntry->
            bFsDot11CapabilityRowStatus == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsDot11AuthenticationProfileTableFilterInputs
 Input       :  The Indices
                pWsscfgFsDot11AuthenticationProfileEntry
                pWsscfgSetFsDot11AuthenticationProfileEntry
                pWsscfgIsSetFsDot11AuthenticationProfileEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    FsDot11AuthenticationProfileTableFilterInputs
    (tWsscfgFsDot11AuthenticationProfileEntry *
     pWsscfgFsDot11AuthenticationProfileEntry,
     tWsscfgFsDot11AuthenticationProfileEntry *
     pWsscfgSetFsDot11AuthenticationProfileEntry,
     tWsscfgIsSetFsDot11AuthenticationProfileEntry *
     pWsscfgIsSetFsDot11AuthenticationProfileEntry)
{
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
        bFsDot11AuthenticationProfileName == OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgFsDot11AuthenticationProfileEntry->
              MibObject.au1FsDot11AuthenticationProfileName,
              pWsscfgSetFsDot11AuthenticationProfileEntry->
              MibObject.au1FsDot11AuthenticationProfileName,
              pWsscfgSetFsDot11AuthenticationProfileEntry->
              MibObject.i4FsDot11AuthenticationProfileNameLen) == 0)
            && (pWsscfgFsDot11AuthenticationProfileEntry->
                MibObject.i4FsDot11AuthenticationProfileNameLen ==
                pWsscfgSetFsDot11AuthenticationProfileEntry->
                MibObject.i4FsDot11AuthenticationProfileNameLen))
            pWsscfgIsSetFsDot11AuthenticationProfileEntry->
                bFsDot11AuthenticationProfileName = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
        bFsDot11AuthenticationAlgorithm == OSIX_TRUE)
    {
        if (pWsscfgFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11AuthenticationAlgorithm ==
            pWsscfgSetFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11AuthenticationAlgorithm)
            pWsscfgIsSetFsDot11AuthenticationProfileEntry->
                bFsDot11AuthenticationAlgorithm = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKeyIndex ==
        OSIX_TRUE)
    {
        if (pWsscfgFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11WepKeyIndex ==
            pWsscfgSetFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11WepKeyIndex)
            pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKeyIndex
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKeyType ==
        OSIX_TRUE)
    {
        if (pWsscfgFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11WepKeyType ==
            pWsscfgSetFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11WepKeyType)
            pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKeyType
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKeyLength ==
        OSIX_TRUE)
    {
        if (pWsscfgFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11WepKeyLength ==
            pWsscfgSetFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11WepKeyLength)
            pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKeyLength
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
        bFsDot11WepKey == OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgFsDot11AuthenticationProfileEntry->
              MibObject.au1FsDot11WepKey,
              pWsscfgSetFsDot11AuthenticationProfileEntry->
              MibObject.au1FsDot11WepKey,
              pWsscfgSetFsDot11AuthenticationProfileEntry->
              MibObject.i4FsDot11WepKeyLen) == 0)
            && (pWsscfgFsDot11AuthenticationProfileEntry->
                MibObject.i4FsDot11WepKeyLen ==
                pWsscfgSetFsDot11AuthenticationProfileEntry->
                MibObject.i4FsDot11WepKeyLen))
            pWsscfgIsSetFsDot11AuthenticationProfileEntry->
                bFsDot11WepKey = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
        bFsDot11WebAuthentication == OSIX_TRUE)
    {
        if (pWsscfgFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11WebAuthentication ==
            pWsscfgSetFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11WebAuthentication)
            pWsscfgIsSetFsDot11AuthenticationProfileEntry->
                bFsDot11WebAuthentication = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
        bFsDot11AuthenticationRowStatus == OSIX_TRUE)
    {
        if (pWsscfgFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11AuthenticationRowStatus ==
            pWsscfgSetFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11AuthenticationRowStatus)
            pWsscfgIsSetFsDot11AuthenticationProfileEntry->
                bFsDot11AuthenticationRowStatus = OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsDot11AuthenticationProfileEntry->
         bFsDot11AuthenticationProfileName == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
            bFsDot11AuthenticationAlgorithm == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
            bFsDot11WepKeyIndex == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKeyType ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
            bFsDot11WepKeyLength == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKey ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
            bFsDot11WebAuthentication == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
            bFsDot11AuthenticationRowStatus == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsStationQosParamsTableFilterInputs
 Input       :  The Indices
                pWsscfgFsStationQosParamsEntry
                pWsscfgSetFsStationQosParamsEntry
                pWsscfgIsSetFsStationQosParamsEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsStationQosParamsTableFilterInputs (tWsscfgFsStationQosParamsEntry *
                                     pWsscfgFsStationQosParamsEntry,
                                     tWsscfgFsStationQosParamsEntry *
                                     pWsscfgSetFsStationQosParamsEntry,
                                     tWsscfgIsSetFsStationQosParamsEntry *
                                     pWsscfgIsSetFsStationQosParamsEntry)
{
    if (pWsscfgIsSetFsStationQosParamsEntry->bFsStaMacAddress == OSIX_TRUE)
    {
        if (MEMCMP
            (&
             (pWsscfgFsStationQosParamsEntry->MibObject.
              FsStaMacAddress),
             &(pWsscfgSetFsStationQosParamsEntry->
               MibObject.FsStaMacAddress), 6) == 0)
            pWsscfgIsSetFsStationQosParamsEntry->bFsStaMacAddress = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsStationQosParamsEntry->bFsStaQoSPriority == OSIX_TRUE)
    {
        if (pWsscfgFsStationQosParamsEntry->MibObject.
            i4FsStaQoSPriority ==
            pWsscfgSetFsStationQosParamsEntry->MibObject.i4FsStaQoSPriority)
            pWsscfgIsSetFsStationQosParamsEntry->bFsStaQoSPriority = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsStationQosParamsEntry->bFsStaQoSDscp == OSIX_TRUE)
    {
        if (pWsscfgFsStationQosParamsEntry->MibObject.
            i4FsStaQoSDscp ==
            pWsscfgSetFsStationQosParamsEntry->MibObject.i4FsStaQoSDscp)
            pWsscfgIsSetFsStationQosParamsEntry->bFsStaQoSDscp = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsStationQosParamsEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgFsStationQosParamsEntry->MibObject.i4IfIndex ==
            pWsscfgSetFsStationQosParamsEntry->MibObject.i4IfIndex)
            pWsscfgIsSetFsStationQosParamsEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsStationQosParamsEntry->bFsStaMacAddress ==
         OSIX_FALSE)
        && (pWsscfgIsSetFsStationQosParamsEntry->bFsStaQoSPriority ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsStationQosParamsEntry->bFsStaQoSDscp ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsStationQosParamsEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsVlanIsolationTableFilterInputs
 Input       :  The Indices
                pWsscfgFsVlanIsolationEntry
                pWsscfgSetFsVlanIsolationEntry
                pWsscfgIsSetFsVlanIsolationEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsVlanIsolationTableFilterInputs (tWsscfgFsVlanIsolationEntry *
                                  pWsscfgFsVlanIsolationEntry,
                                  tWsscfgFsVlanIsolationEntry *
                                  pWsscfgSetFsVlanIsolationEntry,
                                  tWsscfgIsSetFsVlanIsolationEntry *
                                  pWsscfgIsSetFsVlanIsolationEntry)
{
    if (pWsscfgIsSetFsVlanIsolationEntry->bFsVlanIsolation == OSIX_TRUE)
    {
        if (pWsscfgFsVlanIsolationEntry->MibObject.
            i4FsVlanIsolation ==
            pWsscfgSetFsVlanIsolationEntry->MibObject.i4FsVlanIsolation)
            pWsscfgIsSetFsVlanIsolationEntry->bFsVlanIsolation = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsVlanIsolationEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgFsVlanIsolationEntry->MibObject.i4IfIndex ==
            pWsscfgSetFsVlanIsolationEntry->MibObject.i4IfIndex)
            pWsscfgIsSetFsVlanIsolationEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsVlanIsolationEntry->bFsVlanIsolation ==
         OSIX_FALSE)
        && (pWsscfgIsSetFsVlanIsolationEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsDot11RadioConfigTableFilterInputs
 Input       :  The Indices
                pWsscfgFsDot11RadioConfigEntry
                pWsscfgSetFsDot11RadioConfigEntry
                pWsscfgIsSetFsDot11RadioConfigEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsDot11RadioConfigTableFilterInputs (tWsscfgFsDot11RadioConfigEntry *
                                     pWsscfgFsDot11RadioConfigEntry,
                                     tWsscfgFsDot11RadioConfigEntry *
                                     pWsscfgSetFsDot11RadioConfigEntry,
                                     tWsscfgIsSetFsDot11RadioConfigEntry *
                                     pWsscfgIsSetFsDot11RadioConfigEntry)
{
    if (pWsscfgIsSetFsDot11RadioConfigEntry->bFsDot11RadioType == OSIX_TRUE)
    {
        if (pWsscfgFsDot11RadioConfigEntry->MibObject.
            u4FsDot11RadioType ==
            pWsscfgSetFsDot11RadioConfigEntry->MibObject.u4FsDot11RadioType)
            pWsscfgIsSetFsDot11RadioConfigEntry->bFsDot11RadioType = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11RadioConfigEntry->bFsDot11RowStatus == OSIX_TRUE)
    {
        if (pWsscfgFsDot11RadioConfigEntry->MibObject.
            i4FsDot11RowStatus ==
            pWsscfgSetFsDot11RadioConfigEntry->MibObject.i4FsDot11RowStatus)
            pWsscfgIsSetFsDot11RadioConfigEntry->bFsDot11RowStatus = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11RadioConfigEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgFsDot11RadioConfigEntry->MibObject.i4IfIndex ==
            pWsscfgSetFsDot11RadioConfigEntry->MibObject.i4IfIndex)
            pWsscfgIsSetFsDot11RadioConfigEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsDot11RadioConfigEntry->bFsDot11RadioType ==
         OSIX_FALSE)
        && (pWsscfgIsSetFsDot11RadioConfigEntry->bFsDot11RowStatus ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11RadioConfigEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsDot11QosProfileTableFilterInputs
 Input       :  The Indices
                pWsscfgFsDot11QosProfileEntry
                pWsscfgSetFsDot11QosProfileEntry
                pWsscfgIsSetFsDot11QosProfileEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsDot11QosProfileTableFilterInputs (tWsscfgFsDot11QosProfileEntry *
                                    pWsscfgFsDot11QosProfileEntry,
                                    tWsscfgFsDot11QosProfileEntry *
                                    pWsscfgSetFsDot11QosProfileEntry,
                                    tWsscfgIsSetFsDot11QosProfileEntry *
                                    pWsscfgIsSetFsDot11QosProfileEntry)
{
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosProfileName == OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgFsDot11QosProfileEntry->
              MibObject.au1FsDot11QosProfileName,
              pWsscfgSetFsDot11QosProfileEntry->
              MibObject.au1FsDot11QosProfileName,
              pWsscfgSetFsDot11QosProfileEntry->
              MibObject.i4FsDot11QosProfileNameLen) == 0)
            && (pWsscfgFsDot11QosProfileEntry->
                MibObject.i4FsDot11QosProfileNameLen ==
                pWsscfgSetFsDot11QosProfileEntry->
                MibObject.i4FsDot11QosProfileNameLen))
            pWsscfgIsSetFsDot11QosProfileEntry->
                bFsDot11QosProfileName = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosTraffic == OSIX_TRUE)
    {
        if (pWsscfgFsDot11QosProfileEntry->MibObject.
            i4FsDot11QosTraffic ==
            pWsscfgSetFsDot11QosProfileEntry->MibObject.i4FsDot11QosTraffic)
            pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosTraffic = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosPassengerTrustMode ==
        OSIX_TRUE)
    {
        if (pWsscfgFsDot11QosProfileEntry->
            MibObject.i4FsDot11QosPassengerTrustMode ==
            pWsscfgSetFsDot11QosProfileEntry->
            MibObject.i4FsDot11QosPassengerTrustMode)
            pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosPassengerTrustMode
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosRateLimit == OSIX_TRUE)
    {
        if (pWsscfgFsDot11QosProfileEntry->
            MibObject.i4FsDot11QosRateLimit ==
            pWsscfgSetFsDot11QosProfileEntry->MibObject.i4FsDot11QosRateLimit)
            pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosRateLimit =
                OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamCIR == OSIX_TRUE)
    {
        if (pWsscfgFsDot11QosProfileEntry->
            MibObject.i4FsDot11UpStreamCIR ==
            pWsscfgSetFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamCIR)
            pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamCIR =
                OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamCBS == OSIX_TRUE)
    {
        if (pWsscfgFsDot11QosProfileEntry->
            MibObject.i4FsDot11UpStreamCBS ==
            pWsscfgSetFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamCBS)
            pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamCBS =
                OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamEIR == OSIX_TRUE)
    {
        if (pWsscfgFsDot11QosProfileEntry->
            MibObject.i4FsDot11UpStreamEIR ==
            pWsscfgSetFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamEIR)
            pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamEIR =
                OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamEBS == OSIX_TRUE)
    {
        if (pWsscfgFsDot11QosProfileEntry->
            MibObject.i4FsDot11UpStreamEBS ==
            pWsscfgSetFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamEBS)
            pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamEBS =
                OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamCIR == OSIX_TRUE)
    {
        if (pWsscfgFsDot11QosProfileEntry->
            MibObject.i4FsDot11DownStreamCIR ==
            pWsscfgSetFsDot11QosProfileEntry->MibObject.i4FsDot11DownStreamCIR)
            pWsscfgIsSetFsDot11QosProfileEntry->
                bFsDot11DownStreamCIR = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamCBS == OSIX_TRUE)
    {
        if (pWsscfgFsDot11QosProfileEntry->
            MibObject.i4FsDot11DownStreamCBS ==
            pWsscfgSetFsDot11QosProfileEntry->MibObject.i4FsDot11DownStreamCBS)
            pWsscfgIsSetFsDot11QosProfileEntry->
                bFsDot11DownStreamCBS = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamEIR == OSIX_TRUE)
    {
        if (pWsscfgFsDot11QosProfileEntry->
            MibObject.i4FsDot11DownStreamEIR ==
            pWsscfgSetFsDot11QosProfileEntry->MibObject.i4FsDot11DownStreamEIR)
            pWsscfgIsSetFsDot11QosProfileEntry->
                bFsDot11DownStreamEIR = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamEBS == OSIX_TRUE)
    {
        if (pWsscfgFsDot11QosProfileEntry->
            MibObject.i4FsDot11DownStreamEBS ==
            pWsscfgSetFsDot11QosProfileEntry->MibObject.i4FsDot11DownStreamEBS)
            pWsscfgIsSetFsDot11QosProfileEntry->
                bFsDot11DownStreamEBS = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosRowStatus == OSIX_TRUE)
    {
        if (pWsscfgFsDot11QosProfileEntry->
            MibObject.i4FsDot11QosRowStatus ==
            pWsscfgSetFsDot11QosProfileEntry->MibObject.i4FsDot11QosRowStatus)
            pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosRowStatus =
                OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosProfileName ==
         OSIX_FALSE)
        && (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosTraffic ==
            OSIX_FALSE)
        &&
        (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosPassengerTrustMode
         == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11QosProfileEntry->
            bFsDot11QosRateLimit == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamCIR ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamCBS ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamEIR ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamEBS ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11QosProfileEntry->
            bFsDot11DownStreamCIR == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11QosProfileEntry->
            bFsDot11DownStreamCBS == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11QosProfileEntry->
            bFsDot11DownStreamEIR == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11QosProfileEntry->
            bFsDot11DownStreamEBS == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11QosProfileEntry->
            bFsDot11QosRowStatus == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsDot11WlanCapabilityProfileTableFilterInputs
 Input       :  The Indices
                pWsscfgFsDot11WlanCapabilityProfileEntry
                pWsscfgSetFsDot11WlanCapabilityProfileEntry
                pWsscfgIsSetFsDot11WlanCapabilityProfileEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    FsDot11WlanCapabilityProfileTableFilterInputs
    (tWsscfgFsDot11WlanCapabilityProfileEntry *
     pWsscfgFsDot11WlanCapabilityProfileEntry,
     tWsscfgFsDot11WlanCapabilityProfileEntry *
     pWsscfgSetFsDot11WlanCapabilityProfileEntry,
     tWsscfgIsSetFsDot11WlanCapabilityProfileEntry *
     pWsscfgIsSetFsDot11WlanCapabilityProfileEntry)
{
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanCFPollable ==
        OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanCFPollable ==
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanCFPollable)
            pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanCFPollable = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanCFPollRequest == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanCFPollRequest ==
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanCFPollRequest)
            pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanCFPollRequest = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanPrivacyOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanPrivacyOptionImplemented ==
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanPrivacyOptionImplemented)
            pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanPrivacyOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanShortPreambleOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanShortPreambleOptionImplemented ==
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanShortPreambleOptionImplemented)
            pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanShortPreambleOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanPBCCOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanPBCCOptionImplemented ==
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanPBCCOptionImplemented)
            pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanPBCCOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanChannelAgilityPresent == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanChannelAgilityPresent ==
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanChannelAgilityPresent)
            pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanChannelAgilityPresent = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanQosOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanQosOptionImplemented ==
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanQosOptionImplemented)
            pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanQosOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanSpectrumManagementRequired == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanSpectrumManagementRequired ==
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanSpectrumManagementRequired)
            pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanSpectrumManagementRequired = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanShortSlotTimeOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanShortSlotTimeOptionImplemented ==
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanShortSlotTimeOptionImplemented)
            pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanShortSlotTimeOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanAPSDOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanAPSDOptionImplemented ==
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanAPSDOptionImplemented)
            pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanAPSDOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanDSSSOFDMOptionEnabled == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanDSSSOFDMOptionEnabled ==
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanDSSSOFDMOptionEnabled)
            pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanDSSSOFDMOptionEnabled = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanDelayedBlockAckOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanDelayedBlockAckOptionImplemented ==
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanDelayedBlockAckOptionImplemented)
            pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanDelayedBlockAckOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanImmediateBlockAckOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanImmediateBlockAckOptionImplemented
            ==
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanImmediateBlockAckOptionImplemented)
            pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanImmediateBlockAckOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanQAckOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanQAckOptionImplemented ==
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanQAckOptionImplemented)
            pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanQAckOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanQueueRequestOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanQueueRequestOptionImplemented ==
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanQueueRequestOptionImplemented)
            pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanQueueRequestOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanTXOPRequestOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanTXOPRequestOptionImplemented ==
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanTXOPRequestOptionImplemented)
            pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanTXOPRequestOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanRSNAOptionImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanRSNAOptionImplemented ==
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanRSNAOptionImplemented)
            pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanRSNAOptionImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanRSNAPreauthenticationImplemented == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanRSNAPreauthenticationImplemented ==
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanRSNAPreauthenticationImplemented)
            pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanRSNAPreauthenticationImplemented = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanCapabilityRowStatus == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanCapabilityRowStatus ==
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanCapabilityRowStatus)
            pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanCapabilityRowStatus = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4IfIndex ==
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.i4IfIndex)
            pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bIfIndex =
                OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
         bFsDot11WlanCFPollable == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanCFPollRequest == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanPrivacyOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanShortPreambleOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanPBCCOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanChannelAgilityPresent == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanQosOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanSpectrumManagementRequired == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanShortSlotTimeOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanAPSDOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanDSSSOFDMOptionEnabled == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanDelayedBlockAckOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanImmediateBlockAckOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanQAckOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanQueueRequestOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanTXOPRequestOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanRSNAOptionImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanRSNAPreauthenticationImplemented == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanCapabilityRowStatus == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bIfIndex ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsDot11WlanAuthenticationProfileTableFilterInputs
 Input       :  The Indices
                pWsscfgFsDot11WlanAuthenticationProfileEntry
                pWsscfgSetFsDot11WlanAuthenticationProfileEntry
                pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    FsDot11WlanAuthenticationProfileTableFilterInputs
    (tWsscfgFsDot11WlanAuthenticationProfileEntry *
     pWsscfgFsDot11WlanAuthenticationProfileEntry,
     tWsscfgFsDot11WlanAuthenticationProfileEntry *
     pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
     tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry *
     pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry)
{
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanAuthenticationAlgorithm == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanAuthenticationAlgorithm ==
            pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanAuthenticationAlgorithm)
            pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
                bFsDot11WlanAuthenticationAlgorithm = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanWepKeyIndex == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanWepKeyIndex ==
            pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanWepKeyIndex)
            pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
                bFsDot11WlanWepKeyIndex = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanWepKeyType == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanWepKeyType ==
            pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanWepKeyType)
            pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
                bFsDot11WlanWepKeyType = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanWepKeyLength == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanWepKeyLength ==
            pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanWepKeyLength)
            pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
                bFsDot11WlanWepKeyLength = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bFsDot11WlanWepKey ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgFsDot11WlanAuthenticationProfileEntry->
              MibObject.au1FsDot11WlanWepKey,
              pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
              MibObject.au1FsDot11WlanWepKey,
              pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
              MibObject.i4FsDot11WlanWepKeyLen) == 0)
            && (pWsscfgFsDot11WlanAuthenticationProfileEntry->
                MibObject.i4FsDot11WlanWepKeyLen ==
                pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                MibObject.i4FsDot11WlanWepKeyLen))
            pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
                bFsDot11WlanWepKey = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanWebAuthentication == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanWebAuthentication ==
            pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanWebAuthentication)
            pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
                bFsDot11WlanWebAuthentication = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanAuthenticationRowStatus == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanAuthenticationRowStatus ==
            pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanAuthenticationRowStatus)
            pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
                bFsDot11WlanAuthenticationRowStatus = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bIfIndex ==
        OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4IfIndex ==
            pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4IfIndex)
            pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
                bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
         bFsDot11WlanAuthenticationAlgorithm == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
            bFsDot11WlanWepKeyIndex == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
            bFsDot11WlanWepKeyType == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
            bFsDot11WlanWepKeyLength == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
            bFsDot11WlanWepKey == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
            bFsDot11WlanWebAuthentication == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
            bFsDot11WlanAuthenticationRowStatus == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bIfIndex ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsDot11WlanQosProfileTableFilterInputs
 Input       :  The Indices
                pWsscfgFsDot11WlanQosProfileEntry
                pWsscfgSetFsDot11WlanQosProfileEntry
                pWsscfgIsSetFsDot11WlanQosProfileEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsDot11WlanQosProfileTableFilterInputs (tWsscfgFsDot11WlanQosProfileEntry *
                                        pWsscfgFsDot11WlanQosProfileEntry,
                                        tWsscfgFsDot11WlanQosProfileEntry *
                                        pWsscfgSetFsDot11WlanQosProfileEntry,
                                        tWsscfgIsSetFsDot11WlanQosProfileEntry *
                                        pWsscfgIsSetFsDot11WlanQosProfileEntry)
{
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanQosTraffic == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanQosTraffic ==
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanQosTraffic)
            pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanQosTraffic
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanQosPassengerTrustMode == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanQosPassengerTrustMode ==
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanQosPassengerTrustMode)
            pWsscfgIsSetFsDot11WlanQosProfileEntry->
                bFsDot11WlanQosPassengerTrustMode = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanQosRateLimit == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanQosRateLimit ==
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanQosRateLimit)
            pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanQosRateLimit
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanUpStreamCIR == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanUpStreamCIR ==
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanUpStreamCIR)
            pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanUpStreamCIR
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanUpStreamCBS == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanUpStreamCBS ==
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanUpStreamCBS)
            pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanUpStreamCBS
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanUpStreamEIR == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanUpStreamEIR ==
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanUpStreamEIR)
            pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanUpStreamEIR
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanUpStreamEBS == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanUpStreamEBS ==
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanUpStreamEBS)
            pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanUpStreamEBS
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamCIR ==
        OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanDownStreamCIR ==
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanDownStreamCIR)
            pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamCIR
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamCBS ==
        OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanDownStreamCBS ==
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanDownStreamCBS)
            pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamCBS
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamEIR ==
        OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanDownStreamEIR ==
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanDownStreamEIR)
            pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamEIR
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamEBS ==
        OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanDownStreamEBS ==
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanDownStreamEBS)
            pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamEBS
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanQosRowStatus == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanQosRowStatus ==
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanQosRowStatus)
            pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanQosRowStatus
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4IfIndex ==
            pWsscfgSetFsDot11WlanQosProfileEntry->MibObject.i4IfIndex)
            pWsscfgIsSetFsDot11WlanQosProfileEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsDot11WlanQosProfileEntry->
         bFsDot11WlanQosTraffic == OSIX_FALSE)
        &&
        (pWsscfgIsSetFsDot11WlanQosProfileEntry->
         bFsDot11WlanQosPassengerTrustMode == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanQosRateLimit ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanUpStreamCIR ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanUpStreamCBS ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanUpStreamEIR ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanUpStreamEBS ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamCIR ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamCBS ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamEIR ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamEBS ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanQosRowStatus ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanQosProfileEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsDot11RadioQosTableFilterInputs
 Input       :  The Indices
                pWsscfgFsDot11RadioQosEntry
                pWsscfgSetFsDot11RadioQosEntry
                pWsscfgIsSetFsDot11RadioQosEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsDot11RadioQosTableFilterInputs (tWsscfgFsDot11RadioQosEntry *
                                  pWsscfgFsDot11RadioQosEntry,
                                  tWsscfgFsDot11RadioQosEntry *
                                  pWsscfgSetFsDot11RadioQosEntry,
                                  tWsscfgIsSetFsDot11RadioQosEntry *
                                  pWsscfgIsSetFsDot11RadioQosEntry)
{
    if (pWsscfgIsSetFsDot11RadioQosEntry->bFsDot11TaggingPolicy == OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgFsDot11RadioQosEntry->
              MibObject.au1FsDot11TaggingPolicy,
              pWsscfgSetFsDot11RadioQosEntry->
              MibObject.au1FsDot11TaggingPolicy,
              pWsscfgSetFsDot11RadioQosEntry->
              MibObject.i4FsDot11TaggingPolicyLen) == 0)
            && (pWsscfgFsDot11RadioQosEntry->
                MibObject.i4FsDot11TaggingPolicyLen ==
                pWsscfgSetFsDot11RadioQosEntry->
                MibObject.i4FsDot11TaggingPolicyLen))
            pWsscfgIsSetFsDot11RadioQosEntry->bFsDot11TaggingPolicy =
                OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11RadioQosEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgFsDot11RadioQosEntry->MibObject.i4IfIndex ==
            pWsscfgSetFsDot11RadioQosEntry->MibObject.i4IfIndex)
            pWsscfgIsSetFsDot11RadioQosEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsDot11RadioQosEntry->bFsDot11TaggingPolicy ==
         OSIX_FALSE)
        && (pWsscfgIsSetFsDot11RadioQosEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsDot11QAPTableFilterInputs
 Input       :  The Indices
                pWsscfgFsDot11QAPEntry
                pWsscfgSetFsDot11QAPEntry
                pWsscfgIsSetFsDot11QAPEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsDot11QAPTableFilterInputs (tWsscfgFsDot11QAPEntry * pWsscfgFsDot11QAPEntry,
                             tWsscfgFsDot11QAPEntry * pWsscfgSetFsDot11QAPEntry,
                             tWsscfgIsSetFsDot11QAPEntry *
                             pWsscfgIsSetFsDot11QAPEntry)
{
    if (pWsscfgIsSetFsDot11QAPEntry->bFsDot11QueueDepth == OSIX_TRUE)
    {
        if (pWsscfgFsDot11QAPEntry->MibObject.i4FsDot11QueueDepth ==
            pWsscfgSetFsDot11QAPEntry->MibObject.i4FsDot11QueueDepth)
            pWsscfgIsSetFsDot11QAPEntry->bFsDot11QueueDepth = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11QAPEntry->bFsDot11PriorityValue == OSIX_TRUE)
    {
        if (pWsscfgFsDot11QAPEntry->MibObject.
            i4FsDot11PriorityValue ==
            pWsscfgSetFsDot11QAPEntry->MibObject.i4FsDot11PriorityValue)
            pWsscfgIsSetFsDot11QAPEntry->bFsDot11PriorityValue = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11QAPEntry->bFsDot11DscpValue == OSIX_TRUE)
    {
        if (pWsscfgFsDot11QAPEntry->MibObject.i4FsDot11DscpValue ==
            pWsscfgSetFsDot11QAPEntry->MibObject.i4FsDot11DscpValue)
            pWsscfgIsSetFsDot11QAPEntry->bFsDot11DscpValue = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11QAPEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgFsDot11QAPEntry->MibObject.i4IfIndex ==
            pWsscfgSetFsDot11QAPEntry->MibObject.i4IfIndex)
            pWsscfgIsSetFsDot11QAPEntry->bIfIndex = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11QAPEntry->bDot11EDCATableIndex == OSIX_TRUE)
    {
        if (pWsscfgFsDot11QAPEntry->MibObject.i4Dot11EDCATableIndex ==
            pWsscfgSetFsDot11QAPEntry->MibObject.i4Dot11EDCATableIndex)
            pWsscfgIsSetFsDot11QAPEntry->bDot11EDCATableIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsDot11QAPEntry->bFsDot11QueueDepth ==
         OSIX_FALSE)
        && (pWsscfgIsSetFsDot11QAPEntry->bFsDot11PriorityValue ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11QAPEntry->bFsDot11DscpValue ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11QAPEntry->bIfIndex == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11QAPEntry->bDot11EDCATableIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsQAPProfileTableFilterInputs
 Input       :  The Indices
                pWsscfgFsQAPProfileEntry
                pWsscfgSetFsQAPProfileEntry
                pWsscfgIsSetFsQAPProfileEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsQAPProfileTableFilterInputs (tWsscfgFsQAPProfileEntry *
                               pWsscfgFsQAPProfileEntry,
                               tWsscfgFsQAPProfileEntry *
                               pWsscfgSetFsQAPProfileEntry,
                               tWsscfgIsSetFsQAPProfileEntry *
                               pWsscfgIsSetFsQAPProfileEntry)
{
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileName == OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgFsQAPProfileEntry->MibObject.au1FsQAPProfileName,
              pWsscfgSetFsQAPProfileEntry->MibObject.
              au1FsQAPProfileName,
              pWsscfgSetFsQAPProfileEntry->
              MibObject.i4FsQAPProfileNameLen) == 0)
            && (pWsscfgFsQAPProfileEntry->
                MibObject.i4FsQAPProfileNameLen ==
                pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileNameLen))
            pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileName = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileIndex == OSIX_TRUE)
    {
        if (pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileIndex ==
            pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileIndex)
            pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileIndex = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileCWmin == OSIX_TRUE)
    {
        if (pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileCWmin ==
            pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileCWmin)
            pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileCWmin = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileCWmax == OSIX_TRUE)
    {
        if (pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileCWmax ==
            pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileCWmax)
            pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileCWmax = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileAIFSN == OSIX_TRUE)
    {
        if (pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileAIFSN ==
            pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileAIFSN)
            pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileAIFSN = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileTXOPLimit == OSIX_TRUE)
    {
        if (pWsscfgFsQAPProfileEntry->MibObject.
            i4FsQAPProfileTXOPLimit ==
            pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileTXOPLimit)
            pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileTXOPLimit = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsQAPProfileEntry->
        bFsQAPProfileAdmissionControl == OSIX_TRUE)
    {
        if (pWsscfgFsQAPProfileEntry->
            MibObject.i4FsQAPProfileAdmissionControl ==
            pWsscfgSetFsQAPProfileEntry->
            MibObject.i4FsQAPProfileAdmissionControl)
            pWsscfgIsSetFsQAPProfileEntry->
                bFsQAPProfileAdmissionControl = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfilePriorityValue == OSIX_TRUE)
    {
        if (pWsscfgFsQAPProfileEntry->
            MibObject.i4FsQAPProfilePriorityValue ==
            pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfilePriorityValue)
            pWsscfgIsSetFsQAPProfileEntry->
                bFsQAPProfilePriorityValue = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileDscpValue == OSIX_TRUE)
    {
        if (pWsscfgFsQAPProfileEntry->MibObject.
            i4FsQAPProfileDscpValue ==
            pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileDscpValue)
            pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileDscpValue = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileRowStatus == OSIX_TRUE)
    {
        if (pWsscfgFsQAPProfileEntry->MibObject.
            i4FsQAPProfileRowStatus ==
            pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileRowStatus)
            pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileRowStatus = OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileName ==
         OSIX_FALSE)
        && (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileIndex ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileCWmin ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileCWmax ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileAIFSN ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileTXOPLimit ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsQAPProfileEntry->
            bFsQAPProfileAdmissionControl == OSIX_FALSE)
        && (pWsscfgIsSetFsQAPProfileEntry->
            bFsQAPProfilePriorityValue == OSIX_FALSE)
        && (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileDscpValue ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileRowStatus ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsDot11CapabilityMappingTableFilterInputs
 Input       :  The Indices
                pWsscfgFsDot11CapabilityMappingEntry
                pWsscfgSetFsDot11CapabilityMappingEntry
                pWsscfgIsSetFsDot11CapabilityMappingEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsDot11CapabilityMappingTableFilterInputs (tWsscfgFsDot11CapabilityMappingEntry
                                           *
                                           pWsscfgFsDot11CapabilityMappingEntry,
                                           tWsscfgFsDot11CapabilityMappingEntry
                                           *
                                           pWsscfgSetFsDot11CapabilityMappingEntry,
                                           tWsscfgIsSetFsDot11CapabilityMappingEntry
                                           *
                                           pWsscfgIsSetFsDot11CapabilityMappingEntry)
{
    if (pWsscfgIsSetFsDot11CapabilityMappingEntry->
        bFsDot11CapabilityMappingProfileName == OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgFsDot11CapabilityMappingEntry->
              MibObject.au1FsDot11CapabilityMappingProfileName,
              pWsscfgSetFsDot11CapabilityMappingEntry->
              MibObject.au1FsDot11CapabilityMappingProfileName,
              pWsscfgSetFsDot11CapabilityMappingEntry->
              MibObject.i4FsDot11CapabilityMappingProfileNameLen) ==
             0)
            && (pWsscfgFsDot11CapabilityMappingEntry->
                MibObject.i4FsDot11CapabilityMappingProfileNameLen ==
                pWsscfgSetFsDot11CapabilityMappingEntry->
                MibObject.i4FsDot11CapabilityMappingProfileNameLen))
            pWsscfgIsSetFsDot11CapabilityMappingEntry->
                bFsDot11CapabilityMappingProfileName = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11CapabilityMappingEntry->
        bFsDot11CapabilityMappingRowStatus == OSIX_TRUE)
    {
        if (pWsscfgFsDot11CapabilityMappingEntry->
            MibObject.i4FsDot11CapabilityMappingRowStatus ==
            pWsscfgSetFsDot11CapabilityMappingEntry->
            MibObject.i4FsDot11CapabilityMappingRowStatus)
            pWsscfgIsSetFsDot11CapabilityMappingEntry->
                bFsDot11CapabilityMappingRowStatus = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11CapabilityMappingEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgFsDot11CapabilityMappingEntry->MibObject.
            i4IfIndex ==
            pWsscfgSetFsDot11CapabilityMappingEntry->MibObject.i4IfIndex)
            pWsscfgIsSetFsDot11CapabilityMappingEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsDot11CapabilityMappingEntry->
         bFsDot11CapabilityMappingProfileName == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11CapabilityMappingEntry->
            bFsDot11CapabilityMappingRowStatus == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11CapabilityMappingEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsDot11AuthMappingTableFilterInputs
 Input       :  The Indices
                pWsscfgFsDot11AuthMappingEntry
                pWsscfgSetFsDot11AuthMappingEntry
                pWsscfgIsSetFsDot11AuthMappingEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsDot11AuthMappingTableFilterInputs (tWsscfgFsDot11AuthMappingEntry *
                                     pWsscfgFsDot11AuthMappingEntry,
                                     tWsscfgFsDot11AuthMappingEntry *
                                     pWsscfgSetFsDot11AuthMappingEntry,
                                     tWsscfgIsSetFsDot11AuthMappingEntry *
                                     pWsscfgIsSetFsDot11AuthMappingEntry)
{
    if (pWsscfgIsSetFsDot11AuthMappingEntry->bFsDot11AuthMappingProfileName ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgFsDot11AuthMappingEntry->
              MibObject.au1FsDot11AuthMappingProfileName,
              pWsscfgSetFsDot11AuthMappingEntry->
              MibObject.au1FsDot11AuthMappingProfileName,
              pWsscfgSetFsDot11AuthMappingEntry->
              MibObject.i4FsDot11AuthMappingProfileNameLen) == 0)
            && (pWsscfgFsDot11AuthMappingEntry->
                MibObject.i4FsDot11AuthMappingProfileNameLen ==
                pWsscfgSetFsDot11AuthMappingEntry->
                MibObject.i4FsDot11AuthMappingProfileNameLen))
            pWsscfgIsSetFsDot11AuthMappingEntry->bFsDot11AuthMappingProfileName
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11AuthMappingEntry->bFsDot11AuthMappingRowStatus ==
        OSIX_TRUE)
    {
        if (pWsscfgFsDot11AuthMappingEntry->
            MibObject.i4FsDot11AuthMappingRowStatus ==
            pWsscfgSetFsDot11AuthMappingEntry->
            MibObject.i4FsDot11AuthMappingRowStatus)
            pWsscfgIsSetFsDot11AuthMappingEntry->bFsDot11AuthMappingRowStatus
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11AuthMappingEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgFsDot11AuthMappingEntry->MibObject.i4IfIndex ==
            pWsscfgSetFsDot11AuthMappingEntry->MibObject.i4IfIndex)
            pWsscfgIsSetFsDot11AuthMappingEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsDot11AuthMappingEntry->bFsDot11AuthMappingProfileName ==
         OSIX_FALSE)
        && (pWsscfgIsSetFsDot11AuthMappingEntry->bFsDot11AuthMappingRowStatus ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11AuthMappingEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsDot11QosMappingTableFilterInputs
 Input       :  The Indices
                pWsscfgFsDot11QosMappingEntry
                pWsscfgSetFsDot11QosMappingEntry
                pWsscfgIsSetFsDot11QosMappingEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsDot11QosMappingTableFilterInputs (tWsscfgFsDot11QosMappingEntry *
                                    pWsscfgFsDot11QosMappingEntry,
                                    tWsscfgFsDot11QosMappingEntry *
                                    pWsscfgSetFsDot11QosMappingEntry,
                                    tWsscfgIsSetFsDot11QosMappingEntry *
                                    pWsscfgIsSetFsDot11QosMappingEntry)
{
    if (pWsscfgIsSetFsDot11QosMappingEntry->bFsDot11QosMappingProfileName ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgFsDot11QosMappingEntry->
              MibObject.au1FsDot11QosMappingProfileName,
              pWsscfgSetFsDot11QosMappingEntry->
              MibObject.au1FsDot11QosMappingProfileName,
              pWsscfgSetFsDot11QosMappingEntry->
              MibObject.i4FsDot11QosMappingProfileNameLen) == 0)
            && (pWsscfgFsDot11QosMappingEntry->
                MibObject.i4FsDot11QosMappingProfileNameLen ==
                pWsscfgSetFsDot11QosMappingEntry->
                MibObject.i4FsDot11QosMappingProfileNameLen))
            pWsscfgIsSetFsDot11QosMappingEntry->bFsDot11QosMappingProfileName
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11QosMappingEntry->
        bFsDot11QosMappingRowStatus == OSIX_TRUE)
    {
        if (pWsscfgFsDot11QosMappingEntry->
            MibObject.i4FsDot11QosMappingRowStatus ==
            pWsscfgSetFsDot11QosMappingEntry->
            MibObject.i4FsDot11QosMappingRowStatus)
            pWsscfgIsSetFsDot11QosMappingEntry->bFsDot11QosMappingRowStatus
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11QosMappingEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgFsDot11QosMappingEntry->MibObject.i4IfIndex ==
            pWsscfgSetFsDot11QosMappingEntry->MibObject.i4IfIndex)
            pWsscfgIsSetFsDot11QosMappingEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsDot11QosMappingEntry->bFsDot11QosMappingProfileName ==
         OSIX_FALSE)
        && (pWsscfgIsSetFsDot11QosMappingEntry->bFsDot11QosMappingRowStatus ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11QosMappingEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsDot11AntennasListTableFilterInputs
 Input       :  The Indices
                pWsscfgFsDot11AntennasListEntry
                pWsscfgSetFsDot11AntennasListEntry
                pWsscfgIsSetFsDot11AntennasListEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsDot11AntennasListTableFilterInputs (tWsscfgFsDot11AntennasListEntry *
                                      pWsscfgFsDot11AntennasListEntry,
                                      tWsscfgFsDot11AntennasListEntry *
                                      pWsscfgSetFsDot11AntennasListEntry,
                                      tWsscfgIsSetFsDot11AntennasListEntry *
                                      pWsscfgIsSetFsDot11AntennasListEntry)
{
    if (pWsscfgIsSetFsDot11AntennasListEntry->bFsAntennaMode == OSIX_TRUE)
    {
        if (pWsscfgFsDot11AntennasListEntry->MibObject.
            i4FsAntennaMode ==
            pWsscfgSetFsDot11AntennasListEntry->MibObject.i4FsAntennaMode)
            pWsscfgIsSetFsDot11AntennasListEntry->bFsAntennaMode = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11AntennasListEntry->bFsAntennaSelection == OSIX_TRUE)
    {
        if (pWsscfgFsDot11AntennasListEntry->
            MibObject.i4FsAntennaSelection ==
            pWsscfgSetFsDot11AntennasListEntry->MibObject.i4FsAntennaSelection)
            pWsscfgIsSetFsDot11AntennasListEntry->
                bFsAntennaSelection = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11AntennasListEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgFsDot11AntennasListEntry->MibObject.i4IfIndex ==
            pWsscfgSetFsDot11AntennasListEntry->MibObject.i4IfIndex)
            pWsscfgIsSetFsDot11AntennasListEntry->bIfIndex = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11AntennasListEntry->
        bDot11AntennaListIndex == OSIX_TRUE)
    {
        if (pWsscfgFsDot11AntennasListEntry->
            MibObject.i4Dot11AntennaListIndex ==
            pWsscfgSetFsDot11AntennasListEntry->
            MibObject.i4Dot11AntennaListIndex)
            pWsscfgIsSetFsDot11AntennasListEntry->
                bDot11AntennaListIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsDot11AntennasListEntry->bFsAntennaMode ==
         OSIX_FALSE)
        && (pWsscfgIsSetFsDot11AntennasListEntry->
            bFsAntennaSelection == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11AntennasListEntry->bIfIndex ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11AntennasListEntry->
            bDot11AntennaListIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsDot11WlanTableFilterInputs
 Input       :  The Indices
                pWsscfgFsDot11WlanEntry
                pWsscfgSetFsDot11WlanEntry
                pWsscfgIsSetFsDot11WlanEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsDot11WlanTableFilterInputs (tWsscfgFsDot11WlanEntry * pWsscfgFsDot11WlanEntry,
                              tWsscfgFsDot11WlanEntry *
                              pWsscfgSetFsDot11WlanEntry,
                              tWsscfgIsSetFsDot11WlanEntry *
                              pWsscfgIsSetFsDot11WlanEntry)
{
    if (pWsscfgIsSetFsDot11WlanEntry->bFsDot11WlanProfileIfIndex == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanEntry->
            MibObject.i4FsDot11WlanProfileIfIndex ==
            pWsscfgSetFsDot11WlanEntry->MibObject.i4FsDot11WlanProfileIfIndex)
            pWsscfgIsSetFsDot11WlanEntry->bFsDot11WlanProfileIfIndex =
                OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanEntry->bFsDot11WlanRowStatus == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanEntry->MibObject.
            i4FsDot11WlanRowStatus ==
            pWsscfgSetFsDot11WlanEntry->MibObject.i4FsDot11WlanRowStatus)
            pWsscfgIsSetFsDot11WlanEntry->bFsDot11WlanRowStatus = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanEntry->bCapwapDot11WlanProfileId == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanEntry->
            MibObject.u4CapwapDot11WlanProfileId ==
            pWsscfgSetFsDot11WlanEntry->MibObject.u4CapwapDot11WlanProfileId)
            pWsscfgIsSetFsDot11WlanEntry->bCapwapDot11WlanProfileId =
                OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsDot11WlanEntry->bFsDot11WlanProfileIfIndex ==
         OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanEntry->bFsDot11WlanRowStatus ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanEntry->bCapwapDot11WlanProfileId ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsDot11WlanBindTableFilterInputs
 Input       :  The Indices
                pWsscfgFsDot11WlanBindEntry
                pWsscfgSetFsDot11WlanBindEntry
                pWsscfgIsSetFsDot11WlanBindEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsDot11WlanBindTableFilterInputs (tWsscfgFsDot11WlanBindEntry *
                                  pWsscfgFsDot11WlanBindEntry,
                                  tWsscfgFsDot11WlanBindEntry *
                                  pWsscfgSetFsDot11WlanBindEntry,
                                  tWsscfgIsSetFsDot11WlanBindEntry *
                                  pWsscfgIsSetFsDot11WlanBindEntry)
{
    if (pWsscfgIsSetFsDot11WlanBindEntry->bFsDot11WlanBindWlanId == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanBindEntry->
            MibObject.u4FsDot11WlanBindWlanId ==
            pWsscfgSetFsDot11WlanBindEntry->MibObject.u4FsDot11WlanBindWlanId)
            pWsscfgIsSetFsDot11WlanBindEntry->bFsDot11WlanBindWlanId =
                OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanBindEntry->
        bFsDot11WlanBindBssIfIndex == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanBindEntry->
            MibObject.i4FsDot11WlanBindBssIfIndex ==
            pWsscfgSetFsDot11WlanBindEntry->
            MibObject.i4FsDot11WlanBindBssIfIndex)
            pWsscfgIsSetFsDot11WlanBindEntry->
                bFsDot11WlanBindBssIfIndex = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanBindEntry->bFsDot11WlanBindRowStatus ==
        OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanBindEntry->
            MibObject.i4FsDot11WlanBindRowStatus ==
            pWsscfgSetFsDot11WlanBindEntry->
            MibObject.i4FsDot11WlanBindRowStatus)
            pWsscfgIsSetFsDot11WlanBindEntry->
                bFsDot11WlanBindRowStatus = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanBindEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanBindEntry->MibObject.i4IfIndex ==
            pWsscfgSetFsDot11WlanBindEntry->MibObject.i4IfIndex)
            pWsscfgIsSetFsDot11WlanBindEntry->bIfIndex = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11WlanBindEntry->bCapwapDot11WlanProfileId ==
        OSIX_TRUE)
    {
        if (pWsscfgFsDot11WlanBindEntry->
            MibObject.u4CapwapDot11WlanProfileId ==
            pWsscfgSetFsDot11WlanBindEntry->
            MibObject.u4CapwapDot11WlanProfileId)
            pWsscfgIsSetFsDot11WlanBindEntry->
                bCapwapDot11WlanProfileId = OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsDot11WlanBindEntry->bFsDot11WlanBindWlanId ==
         OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanBindEntry->
            bFsDot11WlanBindBssIfIndex == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanBindEntry->
            bFsDot11WlanBindRowStatus == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanBindEntry->bIfIndex == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11WlanBindEntry->
            bCapwapDot11WlanProfileId == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsDot11nConfigTableFilterInputs
 Input       :  The Indices
                pWsscfgFsDot11nConfigEntry
                pWsscfgSetFsDot11nConfigEntry
                pWsscfgIsSetFsDot11nConfigEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsDot11nConfigTableFilterInputs (tWsscfgFsDot11nConfigEntry *
                                 pWsscfgFsDot11nConfigEntry,
                                 tWsscfgFsDot11nConfigEntry *
                                 pWsscfgSetFsDot11nConfigEntry,
                                 tWsscfgIsSetFsDot11nConfigEntry *
                                 pWsscfgIsSetFsDot11nConfigEntry)
{
    if (pWsscfgIsSetFsDot11nConfigEntry->
        bFsDot11nConfigShortGIfor20MHz == OSIX_TRUE)
    {
        if (pWsscfgFsDot11nConfigEntry->
            MibObject.i4FsDot11nConfigShortGIfor20MHz ==
            pWsscfgSetFsDot11nConfigEntry->
            MibObject.i4FsDot11nConfigShortGIfor20MHz)
            pWsscfgIsSetFsDot11nConfigEntry->bFsDot11nConfigShortGIfor20MHz
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11nConfigEntry->
        bFsDot11nConfigShortGIfor40MHz == OSIX_TRUE)
    {
        if (pWsscfgFsDot11nConfigEntry->
            MibObject.i4FsDot11nConfigShortGIfor40MHz ==
            pWsscfgSetFsDot11nConfigEntry->
            MibObject.i4FsDot11nConfigShortGIfor40MHz)
            pWsscfgIsSetFsDot11nConfigEntry->bFsDot11nConfigShortGIfor40MHz
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11nConfigEntry->
        bFsDot11nConfigChannelWidth == OSIX_TRUE)
    {
        if (pWsscfgFsDot11nConfigEntry->
            MibObject.i4FsDot11nConfigChannelWidth ==
            pWsscfgSetFsDot11nConfigEntry->
            MibObject.i4FsDot11nConfigChannelWidth)
            pWsscfgIsSetFsDot11nConfigEntry->
                bFsDot11nConfigChannelWidth = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11nConfigEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgFsDot11nConfigEntry->MibObject.i4IfIndex ==
            pWsscfgSetFsDot11nConfigEntry->MibObject.i4IfIndex)
            pWsscfgIsSetFsDot11nConfigEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsDot11nConfigEntry->
         bFsDot11nConfigShortGIfor20MHz == OSIX_FALSE)
        &&
        (pWsscfgIsSetFsDot11nConfigEntry->bFsDot11nConfigShortGIfor40MHz
         == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11nConfigEntry->
            bFsDot11nConfigChannelWidth == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11nConfigEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsDot11nMCSDataRateTableFilterInputs
 Input       :  The Indices
                pWsscfgFsDot11nMCSDataRateEntry
                pWsscfgSetFsDot11nMCSDataRateEntry
                pWsscfgIsSetFsDot11nMCSDataRateEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsDot11nMCSDataRateTableFilterInputs (tWsscfgFsDot11nMCSDataRateEntry *
                                      pWsscfgFsDot11nMCSDataRateEntry,
                                      tWsscfgFsDot11nMCSDataRateEntry *
                                      pWsscfgSetFsDot11nMCSDataRateEntry,
                                      tWsscfgIsSetFsDot11nMCSDataRateEntry *
                                      pWsscfgIsSetFsDot11nMCSDataRateEntry)
{
    if (pWsscfgIsSetFsDot11nMCSDataRateEntry->
        bFsDot11nMCSDataRateIndex == OSIX_TRUE)
    {
        if (pWsscfgFsDot11nMCSDataRateEntry->
            MibObject.i4FsDot11nMCSDataRateIndex ==
            pWsscfgSetFsDot11nMCSDataRateEntry->
            MibObject.i4FsDot11nMCSDataRateIndex)
            pWsscfgIsSetFsDot11nMCSDataRateEntry->bFsDot11nMCSDataRateIndex
                = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11nMCSDataRateEntry->bFsDot11nMCSDataRate == OSIX_TRUE)
    {
        if (pWsscfgFsDot11nMCSDataRateEntry->
            MibObject.i4FsDot11nMCSDataRate ==
            pWsscfgSetFsDot11nMCSDataRateEntry->MibObject.i4FsDot11nMCSDataRate)
            pWsscfgIsSetFsDot11nMCSDataRateEntry->
                bFsDot11nMCSDataRate = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsDot11nMCSDataRateEntry->bIfIndex == OSIX_TRUE)
    {
        if (pWsscfgFsDot11nMCSDataRateEntry->MibObject.i4IfIndex ==
            pWsscfgSetFsDot11nMCSDataRateEntry->MibObject.i4IfIndex)
            pWsscfgIsSetFsDot11nMCSDataRateEntry->bIfIndex = OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsDot11nMCSDataRateEntry->
         bFsDot11nMCSDataRateIndex == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11nMCSDataRateEntry->
            bFsDot11nMCSDataRate == OSIX_FALSE)
        && (pWsscfgIsSetFsDot11nMCSDataRateEntry->bIfIndex == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsWtpImageUpgradeTableFilterInputs
 Input       :  The Indices
                pWsscfgFsWtpImageUpgradeEntry
                pWsscfgSetFsWtpImageUpgradeEntry
                pWsscfgIsSetFsWtpImageUpgradeEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsWtpImageUpgradeTableFilterInputs (tWsscfgFsWtpImageUpgradeEntry *
                                    pWsscfgFsWtpImageUpgradeEntry,
                                    tWsscfgFsWtpImageUpgradeEntry *
                                    pWsscfgSetFsWtpImageUpgradeEntry,
                                    tWsscfgIsSetFsWtpImageUpgradeEntry *
                                    pWsscfgIsSetFsWtpImageUpgradeEntry)
{
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpImageVersion == OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgFsWtpImageUpgradeEntry->
              MibObject.au1FsWtpImageVersion,
              pWsscfgSetFsWtpImageUpgradeEntry->
              MibObject.au1FsWtpImageVersion,
              pWsscfgSetFsWtpImageUpgradeEntry->
              MibObject.i4FsWtpImageVersionLen) == 0)
            && (pWsscfgFsWtpImageUpgradeEntry->
                MibObject.i4FsWtpImageVersionLen ==
                pWsscfgSetFsWtpImageUpgradeEntry->
                MibObject.i4FsWtpImageVersionLen))
            pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpImageVersion = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpUpgradeDev == OSIX_TRUE)
    {
        if (pWsscfgFsWtpImageUpgradeEntry->MibObject.
            i4FsWtpUpgradeDev ==
            pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpUpgradeDev)
            pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpUpgradeDev = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpName == OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgFsWtpImageUpgradeEntry->MibObject.au1FsWtpName,
              pWsscfgSetFsWtpImageUpgradeEntry->
              MibObject.au1FsWtpName,
              pWsscfgSetFsWtpImageUpgradeEntry->
              MibObject.i4FsWtpNameLen) == 0)
            && (pWsscfgFsWtpImageUpgradeEntry->MibObject.
                i4FsWtpNameLen ==
                pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpNameLen))
            pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpName = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpImageName == OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgFsWtpImageUpgradeEntry->MibObject.
              au1FsWtpImageName,
              pWsscfgSetFsWtpImageUpgradeEntry->
              MibObject.au1FsWtpImageName,
              pWsscfgSetFsWtpImageUpgradeEntry->
              MibObject.i4FsWtpImageNameLen) == 0)
            && (pWsscfgFsWtpImageUpgradeEntry->
                MibObject.i4FsWtpImageNameLen ==
                pWsscfgSetFsWtpImageUpgradeEntry->
                MibObject.i4FsWtpImageNameLen))
        {
        }
/*
            pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpImageName =
                OSIX_FALSE;
*/
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpAddressType == OSIX_TRUE)
    {
        if (pWsscfgFsWtpImageUpgradeEntry->MibObject.
            i4FsWtpAddressType ==
            pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpAddressType)
            pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpAddressType = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpServerIP == OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgFsWtpImageUpgradeEntry->MibObject.
              au1FsWtpServerIP,
              pWsscfgSetFsWtpImageUpgradeEntry->MibObject.
              au1FsWtpServerIP,
              pWsscfgSetFsWtpImageUpgradeEntry->
              MibObject.i4FsWtpServerIPLen) == 0)
            && (pWsscfgFsWtpImageUpgradeEntry->
                MibObject.i4FsWtpServerIPLen ==
                pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpServerIPLen))
            pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpServerIP = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpRowStatus == OSIX_TRUE)
    {
        if (pWsscfgFsWtpImageUpgradeEntry->MibObject.
            i4FsWtpRowStatus ==
            pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpRowStatus)
            pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpRowStatus = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->
        bCapwapBaseWtpProfileWtpModelNumber == OSIX_TRUE)
    {
        if ((MEMCMP
             (pWsscfgFsWtpImageUpgradeEntry->
              MibObject.au1CapwapBaseWtpProfileWtpModelNumber,
              pWsscfgSetFsWtpImageUpgradeEntry->
              MibObject.au1CapwapBaseWtpProfileWtpModelNumber,
              pWsscfgSetFsWtpImageUpgradeEntry->
              MibObject.i4CapwapBaseWtpProfileWtpModelNumberLen) == 0)
            && (pWsscfgFsWtpImageUpgradeEntry->
                MibObject.i4CapwapBaseWtpProfileWtpModelNumberLen ==
                pWsscfgSetFsWtpImageUpgradeEntry->
                MibObject.i4CapwapBaseWtpProfileWtpModelNumberLen))
            pWsscfgIsSetFsWtpImageUpgradeEntry->
                bCapwapBaseWtpProfileWtpModelNumber = OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpImageVersion ==
         OSIX_FALSE)
        && (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpUpgradeDev ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpName ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpImageName ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpAddressType ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpServerIP ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpRowStatus ==
            OSIX_FALSE)
        &&
        (pWsscfgIsSetFsWtpImageUpgradeEntry->bCapwapBaseWtpProfileWtpModelNumber
         == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11StationConfigTableTrigger
 Input       :  The Indices
                pWsscfgSetFsDot11StationConfigEntry
                pWsscfgIsSetFsDot11StationConfigEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllFsDot11StationConfigTableTrigger (tWsscfgFsDot11StationConfigEntry *
                                              pWsscfgSetFsDot11StationConfigEntry,
                                              tWsscfgIsSetFsDot11StationConfigEntry
                                              *
                                              pWsscfgIsSetFsDot11StationConfigEntry,
                                              INT4 i4SetOption)
{

    if (pWsscfgIsSetFsDot11StationConfigEntry->bFsDot11SupressSSID == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11SupressSSID, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11StationConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11StationConfigEntry->
                      MibObject.i4FsDot11SupressSSID);
    }
    if (pWsscfgIsSetFsDot11StationConfigEntry->bFsDot11VlanId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11VlanId, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11StationConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11StationConfigEntry->
                      MibObject.i4FsDot11VlanId);
    }
    if (pWsscfgIsSetFsDot11StationConfigEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11StationConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11StationConfigEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11CapabilityProfileTableTrigger
 Input       :  The Indices
                pWsscfgSetFsDot11CapabilityProfileEntry
                pWsscfgIsSetFsDot11CapabilityProfileEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgSetAllFsDot11CapabilityProfileTableTrigger
    (tWsscfgFsDot11CapabilityProfileEntry *
     pWsscfgSetFsDot11CapabilityProfileEntry,
     tWsscfgIsSetFsDot11CapabilityProfileEntry *
     pWsscfgIsSetFsDot11CapabilityProfileEntry, INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsDot11CapabilityProfileNameVal;
    UINT1               au1FsDot11CapabilityProfileNameVal[256];

    MEMSET (au1FsDot11CapabilityProfileNameVal, 0,
            sizeof (au1FsDot11CapabilityProfileNameVal));
    FsDot11CapabilityProfileNameVal.pu1_OctetList =
        au1FsDot11CapabilityProfileNameVal;
    FsDot11CapabilityProfileNameVal.i4_Length = 0;

    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11CapabilityProfileName == OSIX_TRUE)
    {
        MEMCPY (FsDot11CapabilityProfileNameVal.pu1_OctetList,
                pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.au1FsDot11CapabilityProfileName,
                pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11CapabilityProfileNameLen);
        FsDot11CapabilityProfileNameVal.i4_Length =
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11CapabilityProfileNameLen;

        nmhSetCmnNew (FsDot11CapabilityProfileName, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%s %s",
                      &FsDot11CapabilityProfileNameVal,
                      &FsDot11CapabilityProfileNameVal);
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11CFPollable == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11CFPollable, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %i", &FsDot11CapabilityProfileNameVal,
                      pWsscfgSetFsDot11CapabilityProfileEntry->
                      MibObject.i4FsDot11CFPollable);
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11CFPollRequest == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11CFPollRequest, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %i", &FsDot11CapabilityProfileNameVal,
                      pWsscfgSetFsDot11CapabilityProfileEntry->
                      MibObject.i4FsDot11CFPollRequest);
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11PrivacyOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11PrivacyOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%s %i",
                      &FsDot11CapabilityProfileNameVal,
                      pWsscfgSetFsDot11CapabilityProfileEntry->
                      MibObject.i4FsDot11PrivacyOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11ShortPreambleOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11ShortPreambleOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%s %i",
                      &FsDot11CapabilityProfileNameVal,
                      pWsscfgSetFsDot11CapabilityProfileEntry->
                      MibObject.i4FsDot11ShortPreambleOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11PBCCOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11PBCCOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%s %i",
                      &FsDot11CapabilityProfileNameVal,
                      pWsscfgSetFsDot11CapabilityProfileEntry->
                      MibObject.i4FsDot11PBCCOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11ChannelAgilityPresent == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11ChannelAgilityPresent, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%s %i",
                      &FsDot11CapabilityProfileNameVal,
                      pWsscfgSetFsDot11CapabilityProfileEntry->
                      MibObject.i4FsDot11ChannelAgilityPresent);
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11QosOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11QosOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%s %i",
                      &FsDot11CapabilityProfileNameVal,
                      pWsscfgSetFsDot11CapabilityProfileEntry->
                      MibObject.i4FsDot11QosOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11SpectrumManagementRequired == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11SpectrumManagementRequired, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%s %i",
                      &FsDot11CapabilityProfileNameVal,
                      pWsscfgSetFsDot11CapabilityProfileEntry->
                      MibObject.i4FsDot11SpectrumManagementRequired);
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11ShortSlotTimeOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11ShortSlotTimeOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%s %i",
                      &FsDot11CapabilityProfileNameVal,
                      pWsscfgSetFsDot11CapabilityProfileEntry->
                      MibObject.i4FsDot11ShortSlotTimeOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11APSDOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11APSDOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%s %i",
                      &FsDot11CapabilityProfileNameVal,
                      pWsscfgSetFsDot11CapabilityProfileEntry->
                      MibObject.i4FsDot11APSDOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11DSSSOFDMOptionEnabled == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11DSSSOFDMOptionEnabled, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%s %i",
                      &FsDot11CapabilityProfileNameVal,
                      pWsscfgSetFsDot11CapabilityProfileEntry->
                      MibObject.i4FsDot11DSSSOFDMOptionEnabled);
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11DelayedBlockAckOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11DelayedBlockAckOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%s %i",
                      &FsDot11CapabilityProfileNameVal,
                      pWsscfgSetFsDot11CapabilityProfileEntry->
                      MibObject.i4FsDot11DelayedBlockAckOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11ImmediateBlockAckOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11ImmediateBlockAckOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%s %i",
                      &FsDot11CapabilityProfileNameVal,
                      pWsscfgSetFsDot11CapabilityProfileEntry->
                      MibObject.i4FsDot11ImmediateBlockAckOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11QAckOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11QAckOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%s %i",
                      &FsDot11CapabilityProfileNameVal,
                      pWsscfgSetFsDot11CapabilityProfileEntry->
                      MibObject.i4FsDot11QAckOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11QueueRequestOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11QueueRequestOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%s %i",
                      &FsDot11CapabilityProfileNameVal,
                      pWsscfgSetFsDot11CapabilityProfileEntry->
                      MibObject.i4FsDot11QueueRequestOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11TXOPRequestOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11TXOPRequestOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%s %i",
                      &FsDot11CapabilityProfileNameVal,
                      pWsscfgSetFsDot11CapabilityProfileEntry->
                      MibObject.i4FsDot11TXOPRequestOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11RSNAOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11RSNAOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%s %i",
                      &FsDot11CapabilityProfileNameVal,
                      pWsscfgSetFsDot11CapabilityProfileEntry->
                      MibObject.i4FsDot11RSNAOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11RSNAPreauthenticationImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11RSNAPreauthenticationImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%s %i",
                      &FsDot11CapabilityProfileNameVal,
                      pWsscfgSetFsDot11CapabilityProfileEntry->
                      MibObject.i4FsDot11RSNAPreauthenticationImplemented);
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11CapabilityRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11CapabilityRowStatus, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 1,
                      1, i4SetOption, "%s %i",
                      &FsDot11CapabilityProfileNameVal,
                      pWsscfgSetFsDot11CapabilityProfileEntry->
                      MibObject.i4FsDot11CapabilityRowStatus);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11AuthenticationProfileTableTrigger
 Input       :  The Indices
                pWsscfgSetFsDot11AuthenticationProfileEntry
                pWsscfgIsSetFsDot11AuthenticationProfileEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgSetAllFsDot11AuthenticationProfileTableTrigger
    (tWsscfgFsDot11AuthenticationProfileEntry *
     pWsscfgSetFsDot11AuthenticationProfileEntry,
     tWsscfgIsSetFsDot11AuthenticationProfileEntry *
     pWsscfgIsSetFsDot11AuthenticationProfileEntry, INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsDot11AuthenticationProfileNameVal;
    UINT1               au1FsDot11AuthenticationProfileNameVal[256];
    tSNMP_OCTET_STRING_TYPE FsDot11WepKeyVal;
    UINT1               au1FsDot11WepKeyVal[256];

    MEMSET (au1FsDot11AuthenticationProfileNameVal, 0,
            sizeof (au1FsDot11AuthenticationProfileNameVal));
    FsDot11AuthenticationProfileNameVal.pu1_OctetList =
        au1FsDot11AuthenticationProfileNameVal;
    FsDot11AuthenticationProfileNameVal.i4_Length = 0;

    MEMSET (au1FsDot11WepKeyVal, 0, sizeof (au1FsDot11WepKeyVal));
    FsDot11WepKeyVal.pu1_OctetList = au1FsDot11WepKeyVal;
    FsDot11WepKeyVal.i4_Length = 0;

    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
        bFsDot11AuthenticationProfileName == OSIX_TRUE)
    {
        MEMCPY (FsDot11AuthenticationProfileNameVal.pu1_OctetList,
                pWsscfgSetFsDot11AuthenticationProfileEntry->
                MibObject.au1FsDot11AuthenticationProfileName,
                pWsscfgSetFsDot11AuthenticationProfileEntry->
                MibObject.i4FsDot11AuthenticationProfileNameLen);
        FsDot11AuthenticationProfileNameVal.i4_Length =
            pWsscfgSetFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11AuthenticationProfileNameLen;

        nmhSetCmnNew (FsDot11AuthenticationProfileName, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%s %s",
                      &FsDot11AuthenticationProfileNameVal,
                      &FsDot11AuthenticationProfileNameVal);
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
        bFsDot11AuthenticationAlgorithm == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11AuthenticationAlgorithm, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%s %i",
                      &FsDot11AuthenticationProfileNameVal,
                      pWsscfgSetFsDot11AuthenticationProfileEntry->MibObject.
                      i4FsDot11AuthenticationAlgorithm);
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKeyIndex ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WepKeyIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %i", &FsDot11AuthenticationProfileNameVal,
                      pWsscfgSetFsDot11AuthenticationProfileEntry->MibObject.
                      i4FsDot11WepKeyIndex);
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKeyType ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WepKeyType, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %i", &FsDot11AuthenticationProfileNameVal,
                      pWsscfgSetFsDot11AuthenticationProfileEntry->MibObject.
                      i4FsDot11WepKeyType);
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKeyLength ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WepKeyLength, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %i", &FsDot11AuthenticationProfileNameVal,
                      pWsscfgSetFsDot11AuthenticationProfileEntry->MibObject.
                      i4FsDot11WepKeyLength);
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
        bFsDot11WepKey == OSIX_TRUE)
    {
        MEMCPY (FsDot11WepKeyVal.pu1_OctetList,
                pWsscfgSetFsDot11AuthenticationProfileEntry->
                MibObject.au1FsDot11WepKey,
                pWsscfgSetFsDot11AuthenticationProfileEntry->
                MibObject.i4FsDot11WepKeyLen);
        FsDot11WepKeyVal.i4_Length =
            pWsscfgSetFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11WepKeyLen;

        nmhSetCmnNew (FsDot11WepKey, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %s", &FsDot11AuthenticationProfileNameVal,
                      &FsDot11WepKeyVal);
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
        bFsDot11WebAuthentication == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WebAuthentication, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %i", &FsDot11AuthenticationProfileNameVal,
                      pWsscfgSetFsDot11AuthenticationProfileEntry->MibObject.
                      i4FsDot11WebAuthentication);
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
        bFsDot11AuthenticationRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11AuthenticationRowStatus, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 1,
                      1, i4SetOption, "%s %i",
                      &FsDot11AuthenticationProfileNameVal,
                      pWsscfgSetFsDot11AuthenticationProfileEntry->MibObject.
                      i4FsDot11AuthenticationRowStatus);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsStationQosParamsTableTrigger
 Input       :  The Indices
                pWsscfgSetFsStationQosParamsEntry
                pWsscfgIsSetFsStationQosParamsEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllFsStationQosParamsTableTrigger (tWsscfgFsStationQosParamsEntry *
                                            pWsscfgSetFsStationQosParamsEntry,
                                            tWsscfgIsSetFsStationQosParamsEntry
                                            *
                                            pWsscfgIsSetFsStationQosParamsEntry,
                                            INT4 i4SetOption)
{

    if (pWsscfgIsSetFsStationQosParamsEntry->bFsStaMacAddress == OSIX_TRUE)
    {
        nmhSetCmnNew (FsStaMacAddress, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %m %m",
                      pWsscfgSetFsStationQosParamsEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsStationQosParamsEntry->
                      MibObject.FsStaMacAddress,
                      pWsscfgSetFsStationQosParamsEntry->
                      MibObject.FsStaMacAddress);
    }
    if (pWsscfgIsSetFsStationQosParamsEntry->bFsStaQoSPriority == OSIX_TRUE)
    {
        nmhSetCmnNew (FsStaQoSPriority, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %m %i",
                      pWsscfgSetFsStationQosParamsEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsStationQosParamsEntry->
                      MibObject.FsStaMacAddress,
                      pWsscfgSetFsStationQosParamsEntry->
                      MibObject.i4FsStaQoSPriority);
    }
    if (pWsscfgIsSetFsStationQosParamsEntry->bFsStaQoSDscp == OSIX_TRUE)
    {
        nmhSetCmnNew (FsStaQoSDscp, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %m %i",
                      pWsscfgSetFsStationQosParamsEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsStationQosParamsEntry->
                      MibObject.FsStaMacAddress,
                      pWsscfgSetFsStationQosParamsEntry->
                      MibObject.i4FsStaQoSDscp);
    }
    if (pWsscfgIsSetFsStationQosParamsEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %m %i",
                      pWsscfgSetFsStationQosParamsEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsStationQosParamsEntry->
                      MibObject.FsStaMacAddress,
                      pWsscfgSetFsStationQosParamsEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsVlanIsolationTableTrigger
 Input       :  The Indices
                pWsscfgSetFsVlanIsolationEntry
                pWsscfgIsSetFsVlanIsolationEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllFsVlanIsolationTableTrigger (tWsscfgFsVlanIsolationEntry *
                                         pWsscfgSetFsVlanIsolationEntry,
                                         tWsscfgIsSetFsVlanIsolationEntry *
                                         pWsscfgIsSetFsVlanIsolationEntry,
                                         INT4 i4SetOption)
{

    if (pWsscfgIsSetFsVlanIsolationEntry->bFsVlanIsolation == OSIX_TRUE)
    {
        nmhSetCmnNew (FsVlanIsolation, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsVlanIsolationEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsVlanIsolationEntry->
                      MibObject.i4FsVlanIsolation);
    }
    if (pWsscfgIsSetFsVlanIsolationEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsVlanIsolationEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsVlanIsolationEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11RadioConfigTableTrigger
 Input       :  The Indices
                pWsscfgSetFsDot11RadioConfigEntry
                pWsscfgIsSetFsDot11RadioConfigEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllFsDot11RadioConfigTableTrigger (tWsscfgFsDot11RadioConfigEntry *
                                            pWsscfgSetFsDot11RadioConfigEntry,
                                            tWsscfgIsSetFsDot11RadioConfigEntry
                                            *
                                            pWsscfgIsSetFsDot11RadioConfigEntry,
                                            INT4 i4SetOption)
{

    if (pWsscfgIsSetFsDot11RadioConfigEntry->bFsDot11RadioType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11RadioType, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11RadioConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11RadioConfigEntry->
                      MibObject.u4FsDot11RadioType);
    }
    if (pWsscfgIsSetFsDot11RadioConfigEntry->bFsDot11RowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11RowStatus, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 1, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11RadioConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11RadioConfigEntry->
                      MibObject.i4FsDot11RowStatus);
    }
    if (pWsscfgIsSetFsDot11RadioConfigEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11RadioConfigEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11RadioConfigEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11QosProfileTableTrigger
 Input       :  The Indices
                pWsscfgSetFsDot11QosProfileEntry
                pWsscfgIsSetFsDot11QosProfileEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllFsDot11QosProfileTableTrigger (tWsscfgFsDot11QosProfileEntry *
                                           pWsscfgSetFsDot11QosProfileEntry,
                                           tWsscfgIsSetFsDot11QosProfileEntry *
                                           pWsscfgIsSetFsDot11QosProfileEntry,
                                           INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsDot11QosProfileNameVal;
    UINT1               au1FsDot11QosProfileNameVal[256];

    MEMSET (au1FsDot11QosProfileNameVal, 0,
            sizeof (au1FsDot11QosProfileNameVal));
    FsDot11QosProfileNameVal.pu1_OctetList = au1FsDot11QosProfileNameVal;
    FsDot11QosProfileNameVal.i4_Length = 0;

    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosProfileName == OSIX_TRUE)
    {
        MEMCPY (FsDot11QosProfileNameVal.pu1_OctetList,
                pWsscfgSetFsDot11QosProfileEntry->
                MibObject.au1FsDot11QosProfileName,
                pWsscfgSetFsDot11QosProfileEntry->
                MibObject.i4FsDot11QosProfileNameLen);
        FsDot11QosProfileNameVal.i4_Length =
            pWsscfgSetFsDot11QosProfileEntry->
            MibObject.i4FsDot11QosProfileNameLen;

        nmhSetCmnNew (FsDot11QosProfileName, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %s", &FsDot11QosProfileNameVal,
                      &FsDot11QosProfileNameVal);
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosTraffic == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11QosTraffic, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %i", &FsDot11QosProfileNameVal,
                      pWsscfgSetFsDot11QosProfileEntry->
                      MibObject.i4FsDot11QosTraffic);
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosPassengerTrustMode ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11QosPassengerTrustMode, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%s %i",
                      &FsDot11QosProfileNameVal,
                      pWsscfgSetFsDot11QosProfileEntry->
                      MibObject.i4FsDot11QosPassengerTrustMode);
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosRateLimit == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11QosRateLimit, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %i", &FsDot11QosProfileNameVal,
                      pWsscfgSetFsDot11QosProfileEntry->
                      MibObject.i4FsDot11QosRateLimit);
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamCIR == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11UpStreamCIR, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %i", &FsDot11QosProfileNameVal,
                      pWsscfgSetFsDot11QosProfileEntry->
                      MibObject.i4FsDot11UpStreamCIR);
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamCBS == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11UpStreamCBS, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %i", &FsDot11QosProfileNameVal,
                      pWsscfgSetFsDot11QosProfileEntry->
                      MibObject.i4FsDot11UpStreamCBS);
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamEIR == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11UpStreamEIR, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %i", &FsDot11QosProfileNameVal,
                      pWsscfgSetFsDot11QosProfileEntry->
                      MibObject.i4FsDot11UpStreamEIR);
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamEBS == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11UpStreamEBS, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %i", &FsDot11QosProfileNameVal,
                      pWsscfgSetFsDot11QosProfileEntry->
                      MibObject.i4FsDot11UpStreamEBS);
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamCIR == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11DownStreamCIR, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %i", &FsDot11QosProfileNameVal,
                      pWsscfgSetFsDot11QosProfileEntry->
                      MibObject.i4FsDot11DownStreamCIR);
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamCBS == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11DownStreamCBS, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %i", &FsDot11QosProfileNameVal,
                      pWsscfgSetFsDot11QosProfileEntry->
                      MibObject.i4FsDot11DownStreamCBS);
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamEIR == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11DownStreamEIR, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %i", &FsDot11QosProfileNameVal,
                      pWsscfgSetFsDot11QosProfileEntry->
                      MibObject.i4FsDot11DownStreamEIR);
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamEBS == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11DownStreamEBS, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %i", &FsDot11QosProfileNameVal,
                      pWsscfgSetFsDot11QosProfileEntry->
                      MibObject.i4FsDot11DownStreamEBS);
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11QosRowStatus, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 1, 1, i4SetOption,
                      "%s %i", &FsDot11QosProfileNameVal,
                      pWsscfgSetFsDot11QosProfileEntry->
                      MibObject.i4FsDot11QosRowStatus);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger
 Input       :  The Indices
                pWsscfgSetFsDot11WlanCapabilityProfileEntry
                pWsscfgIsSetFsDot11WlanCapabilityProfileEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger
    (tWsscfgFsDot11WlanCapabilityProfileEntry *
     pWsscfgSetFsDot11WlanCapabilityProfileEntry,
     tWsscfgIsSetFsDot11WlanCapabilityProfileEntry *
     pWsscfgIsSetFsDot11WlanCapabilityProfileEntry, INT4 i4SetOption)
{

    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanCFPollable ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanCFPollable, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4FsDot11WlanCFPollable);
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanCFPollRequest == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanCFPollRequest, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4FsDot11WlanCFPollRequest);
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanPrivacyOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanPrivacyOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4FsDot11WlanPrivacyOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanShortPreambleOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanShortPreambleOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4FsDot11WlanShortPreambleOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanPBCCOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanPBCCOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4FsDot11WlanPBCCOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanChannelAgilityPresent == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanChannelAgilityPresent, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4FsDot11WlanChannelAgilityPresent);
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanQosOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanQosOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4FsDot11WlanQosOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanSpectrumManagementRequired == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanSpectrumManagementRequired, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4FsDot11WlanSpectrumManagementRequired);
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanShortSlotTimeOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanShortSlotTimeOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4FsDot11WlanShortSlotTimeOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanAPSDOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanAPSDOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4FsDot11WlanAPSDOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanDSSSOFDMOptionEnabled == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanDSSSOFDMOptionEnabled, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4FsDot11WlanDSSSOFDMOptionEnabled);
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanDelayedBlockAckOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanDelayedBlockAckOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4FsDot11WlanDelayedBlockAckOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanImmediateBlockAckOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanImmediateBlockAckOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4FsDot11WlanImmediateBlockAckOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanQAckOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanQAckOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4FsDot11WlanQAckOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanQueueRequestOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanQueueRequestOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4FsDot11WlanQueueRequestOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanTXOPRequestOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanTXOPRequestOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4FsDot11WlanTXOPRequestOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanRSNAOptionImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanRSNAOptionImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4FsDot11WlanRSNAOptionImplemented);
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanRSNAPreauthenticationImplemented == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanRSNAPreauthenticationImplemented, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4FsDot11WlanRSNAPreauthenticationImplemented);
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanCapabilityRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanCapabilityRowStatus, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 1,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4FsDot11WlanCapabilityRowStatus);
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                      i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger
 Input       :  The Indices
                pWsscfgSetFsDot11WlanAuthenticationProfileEntry
                pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger
    (tWsscfgFsDot11WlanAuthenticationProfileEntry *
     pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
     tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry *
     pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry, INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsDot11WlanWepKeyVal;
    UINT1               au1FsDot11WlanWepKeyVal[256];

    MEMSET (au1FsDot11WlanWepKeyVal, 0, sizeof (au1FsDot11WlanWepKeyVal));
    FsDot11WlanWepKeyVal.pu1_OctetList = au1FsDot11WlanWepKeyVal;
    FsDot11WlanWepKeyVal.i4_Length = 0;

    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanAuthenticationAlgorithm == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanAuthenticationAlgorithm, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                      MibObject.i4FsDot11WlanAuthenticationAlgorithm);
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanWepKeyIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanWepKeyIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                      MibObject.i4FsDot11WlanWepKeyIndex);
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanWepKeyType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanWepKeyType, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                      MibObject.i4FsDot11WlanWepKeyType);
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanWepKeyLength == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanWepKeyLength, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                      MibObject.i4FsDot11WlanWepKeyLength);
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bFsDot11WlanWepKey ==
        OSIX_TRUE)
    {
        MEMCPY (FsDot11WlanWepKeyVal.pu1_OctetList,
                pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                MibObject.au1FsDot11WlanWepKey,
                pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                MibObject.i4FsDot11WlanWepKeyLen);
        FsDot11WlanWepKeyVal.i4_Length =
            pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanWepKeyLen;

        nmhSetCmnNew (FsDot11WlanWepKey, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %s",
                      pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                      MibObject.i4IfIndex, &FsDot11WlanWepKeyVal);
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanWebAuthentication == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanWebAuthentication, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                      MibObject.i4FsDot11WlanWebAuthentication);
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanAuthenticationRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanAuthenticationRowStatus, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 1,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                      MibObject.i4FsDot11WlanAuthenticationRowStatus);
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bIfIndex ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                      MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11WlanQosProfileTableTrigger
 Input       :  The Indices
                pWsscfgSetFsDot11WlanQosProfileEntry
                pWsscfgIsSetFsDot11WlanQosProfileEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllFsDot11WlanQosProfileTableTrigger (tWsscfgFsDot11WlanQosProfileEntry
                                               *
                                               pWsscfgSetFsDot11WlanQosProfileEntry,
                                               tWsscfgIsSetFsDot11WlanQosProfileEntry
                                               *
                                               pWsscfgIsSetFsDot11WlanQosProfileEntry,
                                               INT4 i4SetOption)
{

    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanQosTraffic == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanQosTraffic, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4FsDot11WlanQosTraffic);
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanQosPassengerTrustMode == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanQosPassengerTrustMode, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4FsDot11WlanQosPassengerTrustMode);
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanQosRateLimit == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanQosRateLimit, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4FsDot11WlanQosRateLimit);
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanUpStreamCIR == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanUpStreamCIR, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4FsDot11WlanUpStreamCIR);
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanUpStreamCBS == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanUpStreamCBS, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4FsDot11WlanUpStreamCBS);
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanUpStreamEIR == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanUpStreamEIR, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4FsDot11WlanUpStreamEIR);
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanUpStreamEBS == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanUpStreamEBS, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4FsDot11WlanUpStreamEBS);
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamCIR ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanDownStreamCIR, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4FsDot11WlanDownStreamCIR);
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamCBS ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanDownStreamCBS, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4FsDot11WlanDownStreamCBS);
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamEIR ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanDownStreamEIR, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4FsDot11WlanDownStreamEIR);
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamEBS ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanDownStreamEBS, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4FsDot11WlanDownStreamEBS);
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanQosRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanQosRowStatus, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 1, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4FsDot11WlanQosRowStatus);
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11WlanQosProfileEntry->
                      MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11RadioQosTableTrigger
 Input       :  The Indices
                pWsscfgSetFsDot11RadioQosEntry
                pWsscfgIsSetFsDot11RadioQosEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllFsDot11RadioQosTableTrigger (tWsscfgFsDot11RadioQosEntry *
                                         pWsscfgSetFsDot11RadioQosEntry,
                                         tWsscfgIsSetFsDot11RadioQosEntry *
                                         pWsscfgIsSetFsDot11RadioQosEntry,
                                         INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsDot11TaggingPolicyVal;
    UINT1               au1FsDot11TaggingPolicyVal[256];

    MEMSET (au1FsDot11TaggingPolicyVal, 0, sizeof (au1FsDot11TaggingPolicyVal));
    FsDot11TaggingPolicyVal.pu1_OctetList = au1FsDot11TaggingPolicyVal;
    FsDot11TaggingPolicyVal.i4_Length = 0;

    if (pWsscfgIsSetFsDot11RadioQosEntry->bFsDot11TaggingPolicy == OSIX_TRUE)
    {
        MEMCPY (FsDot11TaggingPolicyVal.pu1_OctetList,
                pWsscfgSetFsDot11RadioQosEntry->
                MibObject.au1FsDot11TaggingPolicy,
                pWsscfgSetFsDot11RadioQosEntry->
                MibObject.i4FsDot11TaggingPolicyLen);
        FsDot11TaggingPolicyVal.i4_Length =
            pWsscfgSetFsDot11RadioQosEntry->MibObject.i4FsDot11TaggingPolicyLen;

        nmhSetCmnNew (FsDot11TaggingPolicy, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %s",
                      pWsscfgSetFsDot11RadioQosEntry->MibObject.
                      i4IfIndex, &FsDot11TaggingPolicyVal);
    }
    if (pWsscfgIsSetFsDot11RadioQosEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11RadioQosEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11RadioQosEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11QAPTableTrigger
 Input       :  The Indices
                pWsscfgSetFsDot11QAPEntry
                pWsscfgIsSetFsDot11QAPEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllFsDot11QAPTableTrigger (tWsscfgFsDot11QAPEntry *
                                    pWsscfgSetFsDot11QAPEntry,
                                    tWsscfgIsSetFsDot11QAPEntry *
                                    pWsscfgIsSetFsDot11QAPEntry,
                                    INT4 i4SetOption)
{

    if (pWsscfgIsSetFsDot11QAPEntry->bFsDot11QueueDepth == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11QueueDepth, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetFsDot11QAPEntry->MibObject.i4IfIndex,
                      pWsscfgSetFsDot11QAPEntry->
                      MibObject.i4Dot11EDCATableIndex,
                      pWsscfgSetFsDot11QAPEntry->MibObject.i4FsDot11QueueDepth);
    }
    if (pWsscfgIsSetFsDot11QAPEntry->bFsDot11PriorityValue == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11PriorityValue, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetFsDot11QAPEntry->MibObject.i4IfIndex,
                      pWsscfgSetFsDot11QAPEntry->
                      MibObject.i4Dot11EDCATableIndex,
                      pWsscfgSetFsDot11QAPEntry->
                      MibObject.i4FsDot11PriorityValue);
    }
    if (pWsscfgIsSetFsDot11QAPEntry->bFsDot11DscpValue == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11DscpValue, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetFsDot11QAPEntry->MibObject.i4IfIndex,
                      pWsscfgSetFsDot11QAPEntry->
                      MibObject.i4Dot11EDCATableIndex,
                      pWsscfgSetFsDot11QAPEntry->MibObject.i4FsDot11DscpValue);
    }
    if (pWsscfgIsSetFsDot11QAPEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetFsDot11QAPEntry->MibObject.i4IfIndex,
                      pWsscfgSetFsDot11QAPEntry->
                      MibObject.i4Dot11EDCATableIndex,
                      pWsscfgSetFsDot11QAPEntry->MibObject.i4IfIndex);
    }
    if (pWsscfgIsSetFsDot11QAPEntry->bDot11EDCATableIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11EDCATableIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetFsDot11QAPEntry->MibObject.i4IfIndex,
                      pWsscfgSetFsDot11QAPEntry->
                      MibObject.i4Dot11EDCATableIndex,
                      pWsscfgSetFsDot11QAPEntry->
                      MibObject.i4Dot11EDCATableIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsQAPProfileTableTrigger
 Input       :  The Indices
                pWsscfgSetFsQAPProfileEntry
                pWsscfgIsSetFsQAPProfileEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllFsQAPProfileTableTrigger (tWsscfgFsQAPProfileEntry *
                                      pWsscfgSetFsQAPProfileEntry,
                                      tWsscfgIsSetFsQAPProfileEntry *
                                      pWsscfgIsSetFsQAPProfileEntry,
                                      INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsQAPProfileNameVal;
    UINT1               au1FsQAPProfileNameVal[256];

    MEMSET (au1FsQAPProfileNameVal, 0, sizeof (au1FsQAPProfileNameVal));
    FsQAPProfileNameVal.pu1_OctetList = au1FsQAPProfileNameVal;
    FsQAPProfileNameVal.i4_Length = 0;

    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileName == OSIX_TRUE)
    {
        MEMCPY (FsQAPProfileNameVal.pu1_OctetList,
                pWsscfgSetFsQAPProfileEntry->MibObject.
                au1FsQAPProfileName,
                pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileNameLen);
        FsQAPProfileNameVal.i4_Length =
            pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileNameLen;

        nmhSetCmnNew (FsQAPProfileName, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%s %i %s", &FsQAPProfileNameVal,
                      pWsscfgSetFsQAPProfileEntry->
                      MibObject.i4FsQAPProfileIndex, &FsQAPProfileNameVal);
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (FsQAPProfileIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%s %i %i", &FsQAPProfileNameVal,
                      pWsscfgSetFsQAPProfileEntry->
                      MibObject.i4FsQAPProfileIndex,
                      pWsscfgSetFsQAPProfileEntry->
                      MibObject.i4FsQAPProfileIndex);
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileCWmin == OSIX_TRUE)
    {
        nmhSetCmnNew (FsQAPProfileCWmin, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%s %i %i", &FsQAPProfileNameVal,
                      pWsscfgSetFsQAPProfileEntry->
                      MibObject.i4FsQAPProfileIndex,
                      pWsscfgSetFsQAPProfileEntry->
                      MibObject.i4FsQAPProfileCWmin);
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileCWmax == OSIX_TRUE)
    {
        nmhSetCmnNew (FsQAPProfileCWmax, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%s %i %i", &FsQAPProfileNameVal,
                      pWsscfgSetFsQAPProfileEntry->
                      MibObject.i4FsQAPProfileIndex,
                      pWsscfgSetFsQAPProfileEntry->
                      MibObject.i4FsQAPProfileCWmax);
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileAIFSN == OSIX_TRUE)
    {
        nmhSetCmnNew (FsQAPProfileAIFSN, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%s %i %i", &FsQAPProfileNameVal,
                      pWsscfgSetFsQAPProfileEntry->
                      MibObject.i4FsQAPProfileIndex,
                      pWsscfgSetFsQAPProfileEntry->
                      MibObject.i4FsQAPProfileAIFSN);
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileTXOPLimit == OSIX_TRUE)
    {
        nmhSetCmnNew (FsQAPProfileTXOPLimit, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%s %i %i", &FsQAPProfileNameVal,
                      pWsscfgSetFsQAPProfileEntry->
                      MibObject.i4FsQAPProfileIndex,
                      pWsscfgSetFsQAPProfileEntry->
                      MibObject.i4FsQAPProfileTXOPLimit);
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfilePriorityValue == OSIX_TRUE)
    {
        nmhSetCmnNew (FsQAPProfilePriorityValue, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%s %i %i", &FsQAPProfileNameVal,
                      pWsscfgSetFsQAPProfileEntry->
                      MibObject.i4FsQAPProfileIndex,
                      pWsscfgSetFsQAPProfileEntry->
                      MibObject.i4FsQAPProfilePriorityValue);
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileDscpValue == OSIX_TRUE)
    {
        nmhSetCmnNew (FsQAPProfileDscpValue, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%s %i %i", &FsQAPProfileNameVal,
                      pWsscfgSetFsQAPProfileEntry->
                      MibObject.i4FsQAPProfileIndex,
                      pWsscfgSetFsQAPProfileEntry->
                      MibObject.i4FsQAPProfileDscpValue);
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsQAPProfileRowStatus, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%s %i %i", &FsQAPProfileNameVal,
                      pWsscfgSetFsQAPProfileEntry->
                      MibObject.i4FsQAPProfileIndex,
                      pWsscfgSetFsQAPProfileEntry->
                      MibObject.i4FsQAPProfileRowStatus);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11CapabilityMappingTableTrigger
 Input       :  The Indices
                pWsscfgSetFsDot11CapabilityMappingEntry
                pWsscfgIsSetFsDot11CapabilityMappingEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgSetAllFsDot11CapabilityMappingTableTrigger
    (tWsscfgFsDot11CapabilityMappingEntry *
     pWsscfgSetFsDot11CapabilityMappingEntry,
     tWsscfgIsSetFsDot11CapabilityMappingEntry *
     pWsscfgIsSetFsDot11CapabilityMappingEntry, INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsDot11CapabilityMappingProfileNameVal;
    UINT1               au1FsDot11CapabilityMappingProfileNameVal[256];

    MEMSET (au1FsDot11CapabilityMappingProfileNameVal, 0,
            sizeof (au1FsDot11CapabilityMappingProfileNameVal));
    FsDot11CapabilityMappingProfileNameVal.pu1_OctetList =
        au1FsDot11CapabilityMappingProfileNameVal;
    FsDot11CapabilityMappingProfileNameVal.i4_Length = 0;

    if (pWsscfgIsSetFsDot11CapabilityMappingEntry->
        bFsDot11CapabilityMappingProfileName == OSIX_TRUE)
    {
        MEMCPY (FsDot11CapabilityMappingProfileNameVal.pu1_OctetList,
                pWsscfgSetFsDot11CapabilityMappingEntry->
                MibObject.au1FsDot11CapabilityMappingProfileName,
                pWsscfgSetFsDot11CapabilityMappingEntry->
                MibObject.i4FsDot11CapabilityMappingProfileNameLen);
        FsDot11CapabilityMappingProfileNameVal.i4_Length =
            pWsscfgSetFsDot11CapabilityMappingEntry->
            MibObject.i4FsDot11CapabilityMappingProfileNameLen;

        nmhSetCmnNew (FsDot11CapabilityMappingProfileName, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %s",
                      pWsscfgSetFsDot11CapabilityMappingEntry->
                      MibObject.i4IfIndex,
                      &FsDot11CapabilityMappingProfileNameVal);
    }
    if (pWsscfgIsSetFsDot11CapabilityMappingEntry->
        bFsDot11CapabilityMappingRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11CapabilityMappingRowStatus, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 1,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11CapabilityMappingEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11CapabilityMappingEntry->
                      MibObject.i4FsDot11CapabilityMappingRowStatus);
    }
    if (pWsscfgIsSetFsDot11CapabilityMappingEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11CapabilityMappingEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11CapabilityMappingEntry->
                      MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11AuthMappingTableTrigger
 Input       :  The Indices
                pWsscfgSetFsDot11AuthMappingEntry
                pWsscfgIsSetFsDot11AuthMappingEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllFsDot11AuthMappingTableTrigger (tWsscfgFsDot11AuthMappingEntry *
                                            pWsscfgSetFsDot11AuthMappingEntry,
                                            tWsscfgIsSetFsDot11AuthMappingEntry
                                            *
                                            pWsscfgIsSetFsDot11AuthMappingEntry,
                                            INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsDot11AuthMappingProfileNameVal;
    UINT1               au1FsDot11AuthMappingProfileNameVal[256];

    MEMSET (au1FsDot11AuthMappingProfileNameVal, 0,
            sizeof (au1FsDot11AuthMappingProfileNameVal));
    FsDot11AuthMappingProfileNameVal.pu1_OctetList =
        au1FsDot11AuthMappingProfileNameVal;
    FsDot11AuthMappingProfileNameVal.i4_Length = 0;

    if (pWsscfgIsSetFsDot11AuthMappingEntry->bFsDot11AuthMappingProfileName ==
        OSIX_TRUE)
    {
        MEMCPY (FsDot11AuthMappingProfileNameVal.pu1_OctetList,
                pWsscfgSetFsDot11AuthMappingEntry->
                MibObject.au1FsDot11AuthMappingProfileName,
                pWsscfgSetFsDot11AuthMappingEntry->
                MibObject.i4FsDot11AuthMappingProfileNameLen);
        FsDot11AuthMappingProfileNameVal.i4_Length =
            pWsscfgSetFsDot11AuthMappingEntry->
            MibObject.i4FsDot11AuthMappingProfileNameLen;

        nmhSetCmnNew (FsDot11AuthMappingProfileName, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %s",
                      pWsscfgSetFsDot11AuthMappingEntry->
                      MibObject.i4IfIndex, &FsDot11AuthMappingProfileNameVal);
    }
    if (pWsscfgIsSetFsDot11AuthMappingEntry->bFsDot11AuthMappingRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11AuthMappingRowStatus, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 1,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11AuthMappingEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11AuthMappingEntry->
                      MibObject.i4FsDot11AuthMappingRowStatus);
    }
    if (pWsscfgIsSetFsDot11AuthMappingEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11AuthMappingEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11AuthMappingEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11QosMappingTableTrigger
 Input       :  The Indices
                pWsscfgSetFsDot11QosMappingEntry
                pWsscfgIsSetFsDot11QosMappingEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllFsDot11QosMappingTableTrigger (tWsscfgFsDot11QosMappingEntry *
                                           pWsscfgSetFsDot11QosMappingEntry,
                                           tWsscfgIsSetFsDot11QosMappingEntry *
                                           pWsscfgIsSetFsDot11QosMappingEntry,
                                           INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsDot11QosMappingProfileNameVal;
    UINT1               au1FsDot11QosMappingProfileNameVal[256];

    MEMSET (au1FsDot11QosMappingProfileNameVal, 0,
            sizeof (au1FsDot11QosMappingProfileNameVal));
    FsDot11QosMappingProfileNameVal.pu1_OctetList =
        au1FsDot11QosMappingProfileNameVal;
    FsDot11QosMappingProfileNameVal.i4_Length = 0;

    if (pWsscfgIsSetFsDot11QosMappingEntry->bFsDot11QosMappingProfileName ==
        OSIX_TRUE)
    {
        MEMCPY (FsDot11QosMappingProfileNameVal.pu1_OctetList,
                pWsscfgSetFsDot11QosMappingEntry->
                MibObject.au1FsDot11QosMappingProfileName,
                pWsscfgSetFsDot11QosMappingEntry->
                MibObject.i4FsDot11QosMappingProfileNameLen);
        FsDot11QosMappingProfileNameVal.i4_Length =
            pWsscfgSetFsDot11QosMappingEntry->
            MibObject.i4FsDot11QosMappingProfileNameLen;

        nmhSetCmnNew (FsDot11QosMappingProfileName, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %s",
                      pWsscfgSetFsDot11QosMappingEntry->MibObject.
                      i4IfIndex, &FsDot11QosMappingProfileNameVal);
    }
    if (pWsscfgIsSetFsDot11QosMappingEntry->
        bFsDot11QosMappingRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11QosMappingRowStatus, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 1,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11QosMappingEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11QosMappingEntry->
                      MibObject.i4FsDot11QosMappingRowStatus);
    }
    if (pWsscfgIsSetFsDot11QosMappingEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11QosMappingEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11QosMappingEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11AntennasListTableTrigger
 Input       :  The Indices
                pWsscfgSetFsDot11AntennasListEntry
                pWsscfgIsSetFsDot11AntennasListEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllFsDot11AntennasListTableTrigger (tWsscfgFsDot11AntennasListEntry *
                                             pWsscfgSetFsDot11AntennasListEntry,
                                             tWsscfgIsSetFsDot11AntennasListEntry
                                             *
                                             pWsscfgIsSetFsDot11AntennasListEntry,
                                             INT4 i4SetOption)
{

    if (pWsscfgIsSetFsDot11AntennasListEntry->bFsAntennaMode == OSIX_TRUE)
    {
        nmhSetCmnNew (FsAntennaMode, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetFsDot11AntennasListEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11AntennasListEntry->
                      MibObject.i4Dot11AntennaListIndex,
                      pWsscfgSetFsDot11AntennasListEntry->
                      MibObject.i4FsAntennaMode);
    }
    if (pWsscfgIsSetFsDot11AntennasListEntry->bFsAntennaSelection == OSIX_TRUE)
    {
        nmhSetCmnNew (FsAntennaSelection, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetFsDot11AntennasListEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11AntennasListEntry->
                      MibObject.i4Dot11AntennaListIndex,
                      pWsscfgSetFsDot11AntennasListEntry->
                      MibObject.i4FsAntennaSelection);
    }
    if (pWsscfgIsSetFsDot11AntennasListEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetFsDot11AntennasListEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11AntennasListEntry->
                      MibObject.i4Dot11AntennaListIndex,
                      pWsscfgSetFsDot11AntennasListEntry->MibObject.i4IfIndex);
    }
    if (pWsscfgIsSetFsDot11AntennasListEntry->
        bDot11AntennaListIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (Dot11AntennaListIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetFsDot11AntennasListEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11AntennasListEntry->
                      MibObject.i4Dot11AntennaListIndex,
                      pWsscfgSetFsDot11AntennasListEntry->
                      MibObject.i4Dot11AntennaListIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11WlanTableTrigger
 Input       :  The Indices
                pWsscfgSetFsDot11WlanEntry
                pWsscfgIsSetFsDot11WlanEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllFsDot11WlanTableTrigger (tWsscfgFsDot11WlanEntry *
                                     pWsscfgSetFsDot11WlanEntry,
                                     tWsscfgIsSetFsDot11WlanEntry *
                                     pWsscfgIsSetFsDot11WlanEntry,
                                     INT4 i4SetOption)
{

    if (pWsscfgIsSetFsDot11WlanEntry->bFsDot11WlanProfileIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanProfileIfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%u %i",
                      pWsscfgSetFsDot11WlanEntry->
                      MibObject.u4CapwapDot11WlanProfileId,
                      pWsscfgSetFsDot11WlanEntry->
                      MibObject.i4FsDot11WlanProfileIfIndex);
    }
    if (pWsscfgIsSetFsDot11WlanEntry->bFsDot11WlanRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanRowStatus, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 1, 1, i4SetOption,
                      "%u %i",
                      pWsscfgSetFsDot11WlanEntry->
                      MibObject.u4CapwapDot11WlanProfileId,
                      pWsscfgSetFsDot11WlanEntry->
                      MibObject.i4FsDot11WlanRowStatus);
    }
    if (pWsscfgIsSetFsDot11WlanEntry->bCapwapDot11WlanProfileId == OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapDot11WlanProfileId, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%u %u",
                      pWsscfgSetFsDot11WlanEntry->
                      MibObject.u4CapwapDot11WlanProfileId,
                      pWsscfgSetFsDot11WlanEntry->
                      MibObject.u4CapwapDot11WlanProfileId);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11WlanBindTableTrigger
 Input       :  The Indices
                pWsscfgSetFsDot11WlanBindEntry
                pWsscfgIsSetFsDot11WlanBindEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllFsDot11WlanBindTableTrigger (tWsscfgFsDot11WlanBindEntry *
                                         pWsscfgSetFsDot11WlanBindEntry,
                                         tWsscfgIsSetFsDot11WlanBindEntry *
                                         pWsscfgIsSetFsDot11WlanBindEntry,
                                         INT4 i4SetOption)
{

    if (pWsscfgIsSetFsDot11WlanBindEntry->bFsDot11WlanBindWlanId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanBindWlanId, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %u %u",
                      pWsscfgSetFsDot11WlanBindEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanBindEntry->
                      MibObject.u4CapwapDot11WlanProfileId,
                      pWsscfgSetFsDot11WlanBindEntry->
                      MibObject.u4FsDot11WlanBindWlanId);
    }
    if (pWsscfgIsSetFsDot11WlanBindEntry->
        bFsDot11WlanBindBssIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanBindBssIfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %u %i",
                      pWsscfgSetFsDot11WlanBindEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanBindEntry->
                      MibObject.u4CapwapDot11WlanProfileId,
                      pWsscfgSetFsDot11WlanBindEntry->
                      MibObject.i4FsDot11WlanBindBssIfIndex);
    }
    if (pWsscfgIsSetFsDot11WlanBindEntry->bFsDot11WlanBindRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11WlanBindRowStatus, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 1, 2, i4SetOption,
                      "%i %u %i",
                      pWsscfgSetFsDot11WlanBindEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanBindEntry->
                      MibObject.u4CapwapDot11WlanProfileId,
                      pWsscfgSetFsDot11WlanBindEntry->
                      MibObject.i4FsDot11WlanBindRowStatus);
    }
    if (pWsscfgIsSetFsDot11WlanBindEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %u %i",
                      pWsscfgSetFsDot11WlanBindEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanBindEntry->
                      MibObject.u4CapwapDot11WlanProfileId,
                      pWsscfgSetFsDot11WlanBindEntry->MibObject.i4IfIndex);
    }
    if (pWsscfgIsSetFsDot11WlanBindEntry->bCapwapDot11WlanProfileId ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (CapwapDot11WlanProfileId, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %u %u",
                      pWsscfgSetFsDot11WlanBindEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11WlanBindEntry->
                      MibObject.u4CapwapDot11WlanProfileId,
                      pWsscfgSetFsDot11WlanBindEntry->
                      MibObject.u4CapwapDot11WlanProfileId);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11nConfigTableTrigger
 Input       :  The Indices
                pWsscfgSetFsDot11nConfigEntry
                pWsscfgIsSetFsDot11nConfigEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllFsDot11nConfigTableTrigger (tWsscfgFsDot11nConfigEntry *
                                        pWsscfgSetFsDot11nConfigEntry,
                                        tWsscfgIsSetFsDot11nConfigEntry *
                                        pWsscfgIsSetFsDot11nConfigEntry,
                                        INT4 i4SetOption)
{

    if (pWsscfgIsSetFsDot11nConfigEntry->
        bFsDot11nConfigShortGIfor20MHz == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11nConfigShortGIfor20MHz, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11nConfigEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11nConfigEntry->
                      MibObject.i4FsDot11nConfigShortGIfor20MHz);
    }
    if (pWsscfgIsSetFsDot11nConfigEntry->
        bFsDot11nConfigShortGIfor40MHz == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11nConfigShortGIfor40MHz, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11nConfigEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11nConfigEntry->
                      MibObject.i4FsDot11nConfigShortGIfor40MHz);
    }
    if (pWsscfgIsSetFsDot11nConfigEntry->
        bFsDot11nConfigChannelWidth == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11nConfigChannelWidth, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsDot11nConfigEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11nConfigEntry->
                      MibObject.i4FsDot11nConfigChannelWidth);
    }
    if (pWsscfgIsSetFsDot11nConfigEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsDot11nConfigEntry->MibObject.
                      i4IfIndex,
                      pWsscfgSetFsDot11nConfigEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11nMCSDataRateTableTrigger
 Input       :  The Indices
                pWsscfgSetFsDot11nMCSDataRateEntry
                pWsscfgIsSetFsDot11nMCSDataRateEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllFsDot11nMCSDataRateTableTrigger (tWsscfgFsDot11nMCSDataRateEntry *
                                             pWsscfgSetFsDot11nMCSDataRateEntry,
                                             tWsscfgIsSetFsDot11nMCSDataRateEntry
                                             *
                                             pWsscfgIsSetFsDot11nMCSDataRateEntry,
                                             INT4 i4SetOption)
{

    if (pWsscfgIsSetFsDot11nMCSDataRateEntry->
        bFsDot11nMCSDataRateIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11nMCSDataRateIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetFsDot11nMCSDataRateEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11nMCSDataRateEntry->
                      MibObject.i4FsDot11nMCSDataRateIndex,
                      pWsscfgSetFsDot11nMCSDataRateEntry->
                      MibObject.i4FsDot11nMCSDataRateIndex);
    }
    if (pWsscfgIsSetFsDot11nMCSDataRateEntry->bFsDot11nMCSDataRate == OSIX_TRUE)
    {
        nmhSetCmnNew (FsDot11nMCSDataRate, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetFsDot11nMCSDataRateEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11nMCSDataRateEntry->
                      MibObject.i4FsDot11nMCSDataRateIndex,
                      pWsscfgSetFsDot11nMCSDataRateEntry->
                      MibObject.i4FsDot11nMCSDataRate);
    }
    if (pWsscfgIsSetFsDot11nMCSDataRateEntry->bIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (IfIndex, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 2, i4SetOption,
                      "%i %i %i",
                      pWsscfgSetFsDot11nMCSDataRateEntry->
                      MibObject.i4IfIndex,
                      pWsscfgSetFsDot11nMCSDataRateEntry->
                      MibObject.i4FsDot11nMCSDataRateIndex,
                      pWsscfgSetFsDot11nMCSDataRateEntry->MibObject.i4IfIndex);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsWtpImageUpgradeTableTrigger
 Input       :  The Indices
                pWsscfgSetFsWtpImageUpgradeEntry
                pWsscfgIsSetFsWtpImageUpgradeEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllFsWtpImageUpgradeTableTrigger (tWsscfgFsWtpImageUpgradeEntry *
                                           pWsscfgSetFsWtpImageUpgradeEntry,
                                           tWsscfgIsSetFsWtpImageUpgradeEntry *
                                           pWsscfgIsSetFsWtpImageUpgradeEntry,
                                           INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsWtpImageVersionVal;
    UINT1               au1FsWtpImageVersionVal[256];
    tSNMP_OCTET_STRING_TYPE FsWtpNameVal;
    UINT1               au1FsWtpNameVal[256];
    tSNMP_OCTET_STRING_TYPE FsWtpImageNameVal;
    UINT1               au1FsWtpImageNameVal[256];
    tSNMP_OCTET_STRING_TYPE FsWtpServerIPVal;
    UINT1               au1FsWtpServerIPVal[256];
    tSNMP_OCTET_STRING_TYPE CapwapBaseWtpProfileWtpModelNumberVal;
    UINT1               au1CapwapBaseWtpProfileWtpModelNumberVal[256];

    MEMSET (au1FsWtpImageVersionVal, 0, sizeof (au1FsWtpImageVersionVal));
    FsWtpImageVersionVal.pu1_OctetList = au1FsWtpImageVersionVal;
    FsWtpImageVersionVal.i4_Length = 0;

    MEMSET (au1FsWtpNameVal, 0, sizeof (au1FsWtpNameVal));
    FsWtpNameVal.pu1_OctetList = au1FsWtpNameVal;
    FsWtpNameVal.i4_Length = 0;

    MEMSET (au1FsWtpImageNameVal, 0, sizeof (au1FsWtpImageNameVal));
    FsWtpImageNameVal.pu1_OctetList = au1FsWtpImageNameVal;
    FsWtpImageNameVal.i4_Length = 0;

    MEMSET (au1FsWtpServerIPVal, 0, sizeof (au1FsWtpServerIPVal));
    FsWtpServerIPVal.pu1_OctetList = au1FsWtpServerIPVal;
    FsWtpServerIPVal.i4_Length = 0;

    MEMSET (au1CapwapBaseWtpProfileWtpModelNumberVal, 0,
            sizeof (au1CapwapBaseWtpProfileWtpModelNumberVal));
    CapwapBaseWtpProfileWtpModelNumberVal.pu1_OctetList =
        au1CapwapBaseWtpProfileWtpModelNumberVal;
    CapwapBaseWtpProfileWtpModelNumberVal.i4_Length = 0;

    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpImageVersion == OSIX_TRUE)
    {
        MEMCPY (FsWtpImageVersionVal.pu1_OctetList,
                pWsscfgSetFsWtpImageUpgradeEntry->
                MibObject.au1FsWtpImageVersion,
                pWsscfgSetFsWtpImageUpgradeEntry->
                MibObject.i4FsWtpImageVersionLen);
        FsWtpImageVersionVal.i4_Length =
            pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpImageVersionLen;

        nmhSetCmnNew (FsWtpImageVersion, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %s", &CapwapBaseWtpProfileWtpModelNumberVal,
                      &FsWtpImageVersionVal);
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpUpgradeDev == OSIX_TRUE)
    {
        nmhSetCmnNew (FsWtpUpgradeDev, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %i", &CapwapBaseWtpProfileWtpModelNumberVal,
                      pWsscfgSetFsWtpImageUpgradeEntry->
                      MibObject.i4FsWtpUpgradeDev);
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpName == OSIX_TRUE)
    {
        MEMCPY (FsWtpNameVal.pu1_OctetList,
                pWsscfgSetFsWtpImageUpgradeEntry->MibObject.
                au1FsWtpName,
                pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpNameLen);
        FsWtpNameVal.i4_Length =
            pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpNameLen;

        nmhSetCmnNew (FsWtpName, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %s", &CapwapBaseWtpProfileWtpModelNumberVal,
                      &FsWtpNameVal);
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpImageName == OSIX_TRUE)
    {
        MEMCPY (FsWtpImageNameVal.pu1_OctetList,
                pWsscfgSetFsWtpImageUpgradeEntry->
                MibObject.au1FsWtpImageName,
                pWsscfgSetFsWtpImageUpgradeEntry->
                MibObject.i4FsWtpImageNameLen);
        FsWtpImageNameVal.i4_Length =
            pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpImageNameLen;

        nmhSetCmnNew (FsWtpImageName, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %s", &CapwapBaseWtpProfileWtpModelNumberVal,
                      &FsWtpImageNameVal);
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpAddressType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsWtpAddressType, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %i", &CapwapBaseWtpProfileWtpModelNumberVal,
                      pWsscfgSetFsWtpImageUpgradeEntry->
                      MibObject.i4FsWtpAddressType);
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpServerIP == OSIX_TRUE)
    {
        MEMCPY (FsWtpServerIPVal.pu1_OctetList,
                pWsscfgSetFsWtpImageUpgradeEntry->
                MibObject.au1FsWtpServerIP,
                pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpServerIPLen);
        FsWtpServerIPVal.i4_Length =
            pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpServerIPLen;

        nmhSetCmnNew (FsWtpServerIP, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%s %s", &CapwapBaseWtpProfileWtpModelNumberVal,
                      &FsWtpServerIPVal);
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsWtpRowStatus, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 1, 1, i4SetOption,
                      "%s %i", &CapwapBaseWtpProfileWtpModelNumberVal,
                      pWsscfgSetFsWtpImageUpgradeEntry->
                      MibObject.i4FsWtpRowStatus);
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->
        bCapwapBaseWtpProfileWtpModelNumber == OSIX_TRUE)
    {
        MEMCPY (CapwapBaseWtpProfileWtpModelNumberVal.pu1_OctetList,
                pWsscfgSetFsWtpImageUpgradeEntry->
                MibObject.au1CapwapBaseWtpProfileWtpModelNumber,
                pWsscfgSetFsWtpImageUpgradeEntry->
                MibObject.i4CapwapBaseWtpProfileWtpModelNumberLen);
        CapwapBaseWtpProfileWtpModelNumberVal.i4_Length =
            pWsscfgSetFsWtpImageUpgradeEntry->
            MibObject.i4CapwapBaseWtpProfileWtpModelNumberLen;

        nmhSetCmnNew (CapwapBaseWtpProfileWtpModelNumber, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%s %s",
                      &CapwapBaseWtpProfileWtpModelNumberVal,
                      &CapwapBaseWtpProfileWtpModelNumberVal);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsDot11StationConfigTableCreateApi
 Input       :  pWsscfgFsDot11StationConfigEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsDot11StationConfigEntry
    *
WsscfgFsDot11StationConfigTableCreateApi (tWsscfgFsDot11StationConfigEntry *
                                          pSetWsscfgFsDot11StationConfigEntry)
{
    tWsscfgFsDot11StationConfigEntry *pWsscfgFsDot11StationConfigEntry = NULL;

    if (pSetWsscfgFsDot11StationConfigEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11StationConfigTableCreatApi: pSetWsscfgFsDot11StationConfigEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsDot11StationConfigEntry =
        (tWsscfgFsDot11StationConfigEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11STATIONCONFIGTABLE_POOLID);
    if (pWsscfgFsDot11StationConfigEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11StationConfigTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsDot11StationConfigEntry,
                pSetWsscfgFsDot11StationConfigEntry,
                sizeof (tWsscfgFsDot11StationConfigEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.FsDot11StationConfigTable,
             (tRBElem *) pWsscfgFsDot11StationConfigEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsDot11StationConfigTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_FSDOT11STATIONCONFIGTABLE_POOLID,
                 (UINT1 *) pWsscfgFsDot11StationConfigEntry);
            return NULL;
        }
        return pWsscfgFsDot11StationConfigEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsDot11CapabilityProfileTableCreateApi
 Input       :  pWsscfgFsDot11CapabilityProfileEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsDot11CapabilityProfileEntry
    *
    WsscfgFsDot11CapabilityProfileTableCreateApi
    (tWsscfgFsDot11CapabilityProfileEntry *
     pSetWsscfgFsDot11CapabilityProfileEntry)
{
    tWsscfgFsDot11CapabilityProfileEntry
        * pWsscfgFsDot11CapabilityProfileEntry = NULL;

    if (pSetWsscfgFsDot11CapabilityProfileEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11CapabilityProfileTableCreatApi: pSetWsscfgFsDot11CapabilityProfileEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsDot11CapabilityProfileEntry =
        (tWsscfgFsDot11CapabilityProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID);
    if (pWsscfgFsDot11CapabilityProfileEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11CapabilityProfileTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsDot11CapabilityProfileEntry,
                pSetWsscfgFsDot11CapabilityProfileEntry,
                sizeof (tWsscfgFsDot11CapabilityProfileEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.
             FsDot11CapabilityProfileTable,
             (tRBElem *) pWsscfgFsDot11CapabilityProfileEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsDot11CapabilityProfileTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgFsDot11CapabilityProfileEntry);
            return NULL;
        }
        return pWsscfgFsDot11CapabilityProfileEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsDot11AuthenticationProfileTableCreateApi
 Input       :  pWsscfgFsDot11AuthenticationProfileEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsDot11AuthenticationProfileEntry
    *
    WsscfgFsDot11AuthenticationProfileTableCreateApi
    (tWsscfgFsDot11AuthenticationProfileEntry *
     pSetWsscfgFsDot11AuthenticationProfileEntry)
{
    tWsscfgFsDot11AuthenticationProfileEntry
        * pWsscfgFsDot11AuthenticationProfileEntry = NULL;

    if (pSetWsscfgFsDot11AuthenticationProfileEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11AuthenticationProfileTableCreatApi: pSetWsscfgFsDot11AuthenticationProfileEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsDot11AuthenticationProfileEntry =
        (tWsscfgFsDot11AuthenticationProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID);
    if (pWsscfgFsDot11AuthenticationProfileEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11AuthenticationProfileTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsDot11AuthenticationProfileEntry,
                pSetWsscfgFsDot11AuthenticationProfileEntry,
                sizeof (tWsscfgFsDot11AuthenticationProfileEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.
             FsDot11AuthenticationProfileTable,
             (tRBElem *) pWsscfgFsDot11AuthenticationProfileEntry) !=
            RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsDot11AuthenticationProfileTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgFsDot11AuthenticationProfileEntry);
            return NULL;
        }
        return pWsscfgFsDot11AuthenticationProfileEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsStationQosParamsTableCreateApi
 Input       :  pWsscfgFsStationQosParamsEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsStationQosParamsEntry *
WsscfgFsStationQosParamsTableCreateApi (tWsscfgFsStationQosParamsEntry *
                                        pSetWsscfgFsStationQosParamsEntry)
{
    tWsscfgFsStationQosParamsEntry *pWsscfgFsStationQosParamsEntry = NULL;

    if (pSetWsscfgFsStationQosParamsEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsStationQosParamsTableCreatApi: pSetWsscfgFsStationQosParamsEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsStationQosParamsEntry =
        (tWsscfgFsStationQosParamsEntry *)
        MemAllocMemBlk (WSSCFG_FSSTATIONQOSPARAMSTABLE_POOLID);
    if (pWsscfgFsStationQosParamsEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsStationQosParamsTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsStationQosParamsEntry,
                pSetWsscfgFsStationQosParamsEntry,
                sizeof (tWsscfgFsStationQosParamsEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.FsStationQosParamsTable,
             (tRBElem *) pWsscfgFsStationQosParamsEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsStationQosParamsTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSSTATIONQOSPARAMSTABLE_POOLID,
                                (UINT1 *) pWsscfgFsStationQosParamsEntry);
            return NULL;
        }
        return pWsscfgFsStationQosParamsEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsVlanIsolationTableCreateApi
 Input       :  pWsscfgFsVlanIsolationEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsVlanIsolationEntry *
WsscfgFsVlanIsolationTableCreateApi (tWsscfgFsVlanIsolationEntry *
                                     pSetWsscfgFsVlanIsolationEntry)
{
    tWsscfgFsVlanIsolationEntry *pWsscfgFsVlanIsolationEntry = NULL;

    if (pSetWsscfgFsVlanIsolationEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsVlanIsolationTableCreatApi: pSetWsscfgFsVlanIsolationEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsVlanIsolationEntry = (tWsscfgFsVlanIsolationEntry *)
        MemAllocMemBlk (WSSCFG_FSVLANISOLATIONTABLE_POOLID);
    if (pWsscfgFsVlanIsolationEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsVlanIsolationTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsVlanIsolationEntry,
                pSetWsscfgFsVlanIsolationEntry,
                sizeof (tWsscfgFsVlanIsolationEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.FsVlanIsolationTable,
             (tRBElem *) pWsscfgFsVlanIsolationEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsVlanIsolationTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSVLANISOLATIONTABLE_POOLID,
                                (UINT1 *) pWsscfgFsVlanIsolationEntry);
            return NULL;
        }
        return pWsscfgFsVlanIsolationEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsDot11RadioConfigTableCreateApi
 Input       :  pWsscfgFsDot11RadioConfigEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsDot11RadioConfigEntry *
WsscfgFsDot11RadioConfigTableCreateApi (tWsscfgFsDot11RadioConfigEntry *
                                        pSetWsscfgFsDot11RadioConfigEntry)
{
    tWsscfgFsDot11RadioConfigEntry *pWsscfgFsDot11RadioConfigEntry = NULL;

    if (pSetWsscfgFsDot11RadioConfigEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11RadioConfigTableCreatApi: pSetWsscfgFsDot11RadioConfigEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsDot11RadioConfigEntry =
        (tWsscfgFsDot11RadioConfigEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID);
    if (pWsscfgFsDot11RadioConfigEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11RadioConfigTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsDot11RadioConfigEntry,
                pSetWsscfgFsDot11RadioConfigEntry,
                sizeof (tWsscfgFsDot11RadioConfigEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.FsDot11RadioConfigTable,
             (tRBElem *) pWsscfgFsDot11RadioConfigEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsDot11RadioConfigTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                                (UINT1 *) pWsscfgFsDot11RadioConfigEntry);
            return NULL;
        }
        return pWsscfgFsDot11RadioConfigEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsDot11QosProfileTableCreateApi
 Input       :  pWsscfgFsDot11QosProfileEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsDot11QosProfileEntry *
WsscfgFsDot11QosProfileTableCreateApi (tWsscfgFsDot11QosProfileEntry *
                                       pSetWsscfgFsDot11QosProfileEntry)
{
    tWsscfgFsDot11QosProfileEntry *pWsscfgFsDot11QosProfileEntry = NULL;

    if (pSetWsscfgFsDot11QosProfileEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11QosProfileTableCreatApi: pSetWsscfgFsDot11QosProfileEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsDot11QosProfileEntry = (tWsscfgFsDot11QosProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID);
    if (pWsscfgFsDot11QosProfileEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11QosProfileTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsDot11QosProfileEntry,
                pSetWsscfgFsDot11QosProfileEntry,
                sizeof (tWsscfgFsDot11QosProfileEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.FsDot11QosProfileTable,
             (tRBElem *) pWsscfgFsDot11QosProfileEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsDot11QosProfileTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                                (UINT1 *) pWsscfgFsDot11QosProfileEntry);
            return NULL;
        }
        return pWsscfgFsDot11QosProfileEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsDot11WlanCapabilityProfileTableCreateApi
 Input       :  pWsscfgFsDot11WlanCapabilityProfileEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsDot11WlanCapabilityProfileEntry
    *
    WsscfgFsDot11WlanCapabilityProfileTableCreateApi
    (tWsscfgFsDot11WlanCapabilityProfileEntry *
     pSetWsscfgFsDot11WlanCapabilityProfileEntry)
{
    tWsscfgFsDot11WlanCapabilityProfileEntry
        * pWsscfgFsDot11WlanCapabilityProfileEntry = NULL;

    if (pSetWsscfgFsDot11WlanCapabilityProfileEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11WlanCapabilityProfileTableCreatApi: pSetWsscfgFsDot11WlanCapabilityProfileEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsDot11WlanCapabilityProfileEntry =
        (tWsscfgFsDot11WlanCapabilityProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID);
    if (pWsscfgFsDot11WlanCapabilityProfileEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11WlanCapabilityProfileTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsDot11WlanCapabilityProfileEntry,
                pSetWsscfgFsDot11WlanCapabilityProfileEntry,
                sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.
             WsscfgGlbMib.FsDot11WlanCapabilityProfileTable,
             (tRBElem *) pWsscfgFsDot11WlanCapabilityProfileEntry) !=
            RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsDot11WlanCapabilityProfileTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgFsDot11WlanCapabilityProfileEntry);
            return NULL;
        }
        return pWsscfgFsDot11WlanCapabilityProfileEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsDot11WlanAuthenticationProfileTableCreateApi
 Input       :  pWsscfgFsDot11WlanAuthenticationProfileEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsDot11WlanAuthenticationProfileEntry
    *
    WsscfgFsDot11WlanAuthenticationProfileTableCreateApi
    (tWsscfgFsDot11WlanAuthenticationProfileEntry *
     pSetWsscfgFsDot11WlanAuthenticationProfileEntry)
{
    tWsscfgFsDot11WlanAuthenticationProfileEntry
        * pWsscfgFsDot11WlanAuthenticationProfileEntry = NULL;

    if (pSetWsscfgFsDot11WlanAuthenticationProfileEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11WlanAuthenticationProfileTableCreatApi: pSetWsscfgFsDot11WlanAuthenticationProfileEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsDot11WlanAuthenticationProfileEntry =
        (tWsscfgFsDot11WlanAuthenticationProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID);
    if (pWsscfgFsDot11WlanAuthenticationProfileEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11WlanAuthenticationProfileTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsDot11WlanAuthenticationProfileEntry,
                pSetWsscfgFsDot11WlanAuthenticationProfileEntry,
                sizeof (tWsscfgFsDot11WlanAuthenticationProfileEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.
             WsscfgGlbMib.FsDot11WlanAuthenticationProfileTable,
             (tRBElem *) pWsscfgFsDot11WlanAuthenticationProfileEntry)
            != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsDot11WlanAuthenticationProfileTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgFsDot11WlanAuthenticationProfileEntry);
            return NULL;
        }
        return pWsscfgFsDot11WlanAuthenticationProfileEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsDot11WlanQosProfileTableCreateApi
 Input       :  pWsscfgFsDot11WlanQosProfileEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsDot11WlanQosProfileEntry
    *
    WsscfgFsDot11WlanQosProfileTableCreateApi
    (tWsscfgFsDot11WlanQosProfileEntry * pSetWsscfgFsDot11WlanQosProfileEntry)
{
    tWsscfgFsDot11WlanQosProfileEntry
        * pWsscfgFsDot11WlanQosProfileEntry = NULL;

    if (pSetWsscfgFsDot11WlanQosProfileEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11WlanQosProfileTableCreatApi: pSetWsscfgFsDot11WlanQosProfileEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsDot11WlanQosProfileEntry =
        (tWsscfgFsDot11WlanQosProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID);
    if (pWsscfgFsDot11WlanQosProfileEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11WlanQosProfileTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsDot11WlanQosProfileEntry,
                pSetWsscfgFsDot11WlanQosProfileEntry,
                sizeof (tWsscfgFsDot11WlanQosProfileEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.FsDot11WlanQosProfileTable,
             (tRBElem *) pWsscfgFsDot11WlanQosProfileEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsDot11WlanQosProfileTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID, (UINT1 *)
                 pWsscfgFsDot11WlanQosProfileEntry);
            return NULL;
        }
        return pWsscfgFsDot11WlanQosProfileEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsDot11RadioQosTableCreateApi
 Input       :  pWsscfgFsDot11RadioQosEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsDot11RadioQosEntry *
WsscfgFsDot11RadioQosTableCreateApi (tWsscfgFsDot11RadioQosEntry *
                                     pSetWsscfgFsDot11RadioQosEntry)
{
    tWsscfgFsDot11RadioQosEntry *pWsscfgFsDot11RadioQosEntry = NULL;

    if (pSetWsscfgFsDot11RadioQosEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11RadioQosTableCreatApi: pSetWsscfgFsDot11RadioQosEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsDot11RadioQosEntry = (tWsscfgFsDot11RadioQosEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11RADIOQOSTABLE_POOLID);
    if (pWsscfgFsDot11RadioQosEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11RadioQosTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsDot11RadioQosEntry,
                pSetWsscfgFsDot11RadioQosEntry,
                sizeof (tWsscfgFsDot11RadioQosEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.FsDot11RadioQosTable,
             (tRBElem *) pWsscfgFsDot11RadioQosEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsDot11RadioQosTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11RADIOQOSTABLE_POOLID,
                                (UINT1 *) pWsscfgFsDot11RadioQosEntry);
            return NULL;
        }
        return pWsscfgFsDot11RadioQosEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsDot11QAPTableCreateApi
 Input       :  pWsscfgFsDot11QAPEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsDot11QAPEntry *
WsscfgFsDot11QAPTableCreateApi (tWsscfgFsDot11QAPEntry *
                                pSetWsscfgFsDot11QAPEntry)
{
    tWsscfgFsDot11QAPEntry *pWsscfgFsDot11QAPEntry = NULL;

    if (pSetWsscfgFsDot11QAPEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11QAPTableCreatApi: pSetWsscfgFsDot11QAPEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsDot11QAPEntry = (tWsscfgFsDot11QAPEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11QAPTABLE_POOLID);
    if (pWsscfgFsDot11QAPEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11QAPTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsDot11QAPEntry, pSetWsscfgFsDot11QAPEntry,
                sizeof (tWsscfgFsDot11QAPEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.FsDot11QAPTable,
             (tRBElem *) pWsscfgFsDot11QAPEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsDot11QAPTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11QAPTABLE_POOLID,
                                (UINT1 *) pWsscfgFsDot11QAPEntry);
            return NULL;
        }
        return pWsscfgFsDot11QAPEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsQAPProfileTableCreateApi
 Input       :  pWsscfgFsQAPProfileEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsQAPProfileEntry *
WsscfgFsQAPProfileTableCreateApi (tWsscfgFsQAPProfileEntry *
                                  pSetWsscfgFsQAPProfileEntry)
{
    tWsscfgFsQAPProfileEntry *pWsscfgFsQAPProfileEntry = NULL;

    if (pSetWsscfgFsQAPProfileEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsQAPProfileTableCreatApi: pSetWsscfgFsQAPProfileEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsQAPProfileEntry = (tWsscfgFsQAPProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSQAPPROFILETABLE_POOLID);
    if (pWsscfgFsQAPProfileEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsQAPProfileTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsQAPProfileEntry, pSetWsscfgFsQAPProfileEntry,
                sizeof (tWsscfgFsQAPProfileEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.FsQAPProfileTable,
             (tRBElem *) pWsscfgFsQAPProfileEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsQAPProfileTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                                (UINT1 *) pWsscfgFsQAPProfileEntry);
            return NULL;
        }
        return pWsscfgFsQAPProfileEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsDot11CapabilityMappingTableCreateApi
 Input       :  pWsscfgFsDot11CapabilityMappingEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsDot11CapabilityMappingEntry
    *
    WsscfgFsDot11CapabilityMappingTableCreateApi
    (tWsscfgFsDot11CapabilityMappingEntry *
     pSetWsscfgFsDot11CapabilityMappingEntry)
{
    tWsscfgFsDot11CapabilityMappingEntry
        * pWsscfgFsDot11CapabilityMappingEntry = NULL;

    if (pSetWsscfgFsDot11CapabilityMappingEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11CapabilityMappingTableCreatApi: pSetWsscfgFsDot11CapabilityMappingEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsDot11CapabilityMappingEntry =
        (tWsscfgFsDot11CapabilityMappingEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID);
    if (pWsscfgFsDot11CapabilityMappingEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11CapabilityMappingTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsDot11CapabilityMappingEntry,
                pSetWsscfgFsDot11CapabilityMappingEntry,
                sizeof (tWsscfgFsDot11CapabilityMappingEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.
             FsDot11CapabilityMappingTable,
             (tRBElem *) pWsscfgFsDot11CapabilityMappingEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsDot11CapabilityMappingTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                 (UINT1 *) pWsscfgFsDot11CapabilityMappingEntry);
            return NULL;
        }
        return pWsscfgFsDot11CapabilityMappingEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsDot11AuthMappingTableCreateApi
 Input       :  pWsscfgFsDot11AuthMappingEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsDot11AuthMappingEntry *
WsscfgFsDot11AuthMappingTableCreateApi (tWsscfgFsDot11AuthMappingEntry *
                                        pSetWsscfgFsDot11AuthMappingEntry)
{
    tWsscfgFsDot11AuthMappingEntry *pWsscfgFsDot11AuthMappingEntry = NULL;

    if (pSetWsscfgFsDot11AuthMappingEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11AuthMappingTableCreatApi: pSetWsscfgFsDot11AuthMappingEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsDot11AuthMappingEntry =
        (tWsscfgFsDot11AuthMappingEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID);
    if (pWsscfgFsDot11AuthMappingEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11AuthMappingTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsDot11AuthMappingEntry,
                pSetWsscfgFsDot11AuthMappingEntry,
                sizeof (tWsscfgFsDot11AuthMappingEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.FsDot11AuthMappingTable,
             (tRBElem *) pWsscfgFsDot11AuthMappingEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsDot11AuthMappingTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                                (UINT1 *) pWsscfgFsDot11AuthMappingEntry);
            return NULL;
        }
        return pWsscfgFsDot11AuthMappingEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsDot11QosMappingTableCreateApi
 Input       :  pWsscfgFsDot11QosMappingEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsDot11QosMappingEntry *
WsscfgFsDot11QosMappingTableCreateApi (tWsscfgFsDot11QosMappingEntry *
                                       pSetWsscfgFsDot11QosMappingEntry)
{
    tWsscfgFsDot11QosMappingEntry *pWsscfgFsDot11QosMappingEntry = NULL;

    if (pSetWsscfgFsDot11QosMappingEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11QosMappingTableCreatApi: pSetWsscfgFsDot11QosMappingEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsDot11QosMappingEntry = (tWsscfgFsDot11QosMappingEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID);
    if (pWsscfgFsDot11QosMappingEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11QosMappingTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsDot11QosMappingEntry,
                pSetWsscfgFsDot11QosMappingEntry,
                sizeof (tWsscfgFsDot11QosMappingEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.FsDot11QosMappingTable,
             (tRBElem *) pWsscfgFsDot11QosMappingEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsDot11QosMappingTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                                (UINT1 *) pWsscfgFsDot11QosMappingEntry);
            return NULL;
        }
        return pWsscfgFsDot11QosMappingEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsDot11ClientSummaryTableCreateApi
 Input       :  pWsscfgFsDot11ClientSummaryEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsDot11ClientSummaryEntry *
WsscfgFsDot11ClientSummaryTableCreateApi (tWsscfgFsDot11ClientSummaryEntry *
                                          pSetWsscfgFsDot11ClientSummaryEntry)
{
    tWsscfgFsDot11ClientSummaryEntry *pWsscfgFsDot11ClientSummaryEntry = NULL;

    if (pSetWsscfgFsDot11ClientSummaryEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11ClientSummaryTableCreatApi: pSetWsscfgFsDot11ClientSummaryEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsDot11ClientSummaryEntry =
        (tWsscfgFsDot11ClientSummaryEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11CLIENTSUMMARYTABLE_POOLID);
    if (pWsscfgFsDot11ClientSummaryEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11ClientSummaryTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsDot11ClientSummaryEntry,
                pSetWsscfgFsDot11ClientSummaryEntry,
                sizeof (tWsscfgFsDot11ClientSummaryEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.FsDot11ClientSummaryTable,
             (tRBElem *) pWsscfgFsDot11ClientSummaryEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsDot11ClientSummaryTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11CLIENTSUMMARYTABLE_POOLID,
                                (UINT1 *) pWsscfgFsDot11ClientSummaryEntry);
            return NULL;
        }
        return pWsscfgFsDot11ClientSummaryEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsDot11AntennasListTableCreateApi
 Input       :  pWsscfgFsDot11AntennasListEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsDot11AntennasListEntry
    * WsscfgFsDot11AntennasListTableCreateApi (tWsscfgFsDot11AntennasListEntry *
                                               pSetWsscfgFsDot11AntennasListEntry)
{
    tWsscfgFsDot11AntennasListEntry *pWsscfgFsDot11AntennasListEntry = NULL;

    if (pSetWsscfgFsDot11AntennasListEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11AntennasListTableCreatApi: pSetWsscfgFsDot11AntennasListEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsDot11AntennasListEntry =
        (tWsscfgFsDot11AntennasListEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11ANTENNASLISTTABLE_POOLID);
    if (pWsscfgFsDot11AntennasListEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11AntennasListTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsDot11AntennasListEntry,
                pSetWsscfgFsDot11AntennasListEntry,
                sizeof (tWsscfgFsDot11AntennasListEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.FsDot11AntennasListTable,
             (tRBElem *) pWsscfgFsDot11AntennasListEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsDot11AntennasListTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11ANTENNASLISTTABLE_POOLID,
                                (UINT1 *) pWsscfgFsDot11AntennasListEntry);
            return NULL;
        }
        return pWsscfgFsDot11AntennasListEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsDot11WlanTableCreateApi
 Input       :  pWsscfgFsDot11WlanEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsDot11WlanEntry *
WsscfgFsDot11WlanTableCreateApi (tWsscfgFsDot11WlanEntry *
                                 pSetWsscfgFsDot11WlanEntry)
{
    tWsscfgFsDot11WlanEntry *pWsscfgFsDot11WlanEntry = NULL;

    if (pSetWsscfgFsDot11WlanEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11WlanTableCreatApi: pSetWsscfgFsDot11WlanEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsDot11WlanEntry = (tWsscfgFsDot11WlanEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11WLANTABLE_POOLID);
    if (pWsscfgFsDot11WlanEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11WlanTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsDot11WlanEntry, pSetWsscfgFsDot11WlanEntry,
                sizeof (tWsscfgFsDot11WlanEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.FsDot11WlanTable,
             (tRBElem *) pWsscfgFsDot11WlanEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsDot11WlanTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                (UINT1 *) pWsscfgFsDot11WlanEntry);
            return NULL;
        }
        return pWsscfgFsDot11WlanEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsDot11WlanBindTableCreateApi
 Input       :  pWsscfgFsDot11WlanBindEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsDot11WlanBindEntry *
WsscfgFsDot11WlanBindTableCreateApi (tWsscfgFsDot11WlanBindEntry *
                                     pSetWsscfgFsDot11WlanBindEntry)
{
    tWsscfgFsDot11WlanBindEntry *pWsscfgFsDot11WlanBindEntry = NULL;

    if (pSetWsscfgFsDot11WlanBindEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11WlanBindTableCreatApi: pSetWsscfgFsDot11WlanBindEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsDot11WlanBindEntry = (tWsscfgFsDot11WlanBindEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11WLANBINDTABLE_POOLID);
    if (pWsscfgFsDot11WlanBindEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11WlanBindTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsDot11WlanBindEntry,
                pSetWsscfgFsDot11WlanBindEntry,
                sizeof (tWsscfgFsDot11WlanBindEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.FsDot11WlanBindTable,
             (tRBElem *) pWsscfgFsDot11WlanBindEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsDot11WlanBindTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                                (UINT1 *) pWsscfgFsDot11WlanBindEntry);
            return NULL;
        }
        return pWsscfgFsDot11WlanBindEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsDot11nConfigTableCreateApi
 Input       :  pWsscfgFsDot11nConfigEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsDot11nConfigEntry *
WsscfgFsDot11nConfigTableCreateApi (tWsscfgFsDot11nConfigEntry *
                                    pSetWsscfgFsDot11nConfigEntry)
{
    tWsscfgFsDot11nConfigEntry *pWsscfgFsDot11nConfigEntry = NULL;

    if (pSetWsscfgFsDot11nConfigEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11nConfigTableCreatApi: pSetWsscfgFsDot11nConfigEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsDot11nConfigEntry = (tWsscfgFsDot11nConfigEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11NCONFIGTABLE_POOLID);
    if (pWsscfgFsDot11nConfigEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11nConfigTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsDot11nConfigEntry,
                pSetWsscfgFsDot11nConfigEntry,
                sizeof (tWsscfgFsDot11nConfigEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.FsDot11nConfigTable,
             (tRBElem *) pWsscfgFsDot11nConfigEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsDot11nConfigTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11NCONFIGTABLE_POOLID,
                                (UINT1 *) pWsscfgFsDot11nConfigEntry);
            return NULL;
        }
        return pWsscfgFsDot11nConfigEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsDot11nMCSDataRateTableCreateApi
 Input       :  pWsscfgFsDot11nMCSDataRateEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsDot11nMCSDataRateEntry
    * WsscfgFsDot11nMCSDataRateTableCreateApi (tWsscfgFsDot11nMCSDataRateEntry *
                                               pSetWsscfgFsDot11nMCSDataRateEntry)
{
    tWsscfgFsDot11nMCSDataRateEntry *pWsscfgFsDot11nMCSDataRateEntry = NULL;

    if (pSetWsscfgFsDot11nMCSDataRateEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11nMCSDataRateTableCreatApi: pSetWsscfgFsDot11nMCSDataRateEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsDot11nMCSDataRateEntry =
        (tWsscfgFsDot11nMCSDataRateEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11NMCSDATARATETABLE_POOLID);
    if (pWsscfgFsDot11nMCSDataRateEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsDot11nMCSDataRateTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsDot11nMCSDataRateEntry,
                pSetWsscfgFsDot11nMCSDataRateEntry,
                sizeof (tWsscfgFsDot11nMCSDataRateEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.FsDot11nMCSDataRateTable,
             (tRBElem *) pWsscfgFsDot11nMCSDataRateEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsDot11nMCSDataRateTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11NMCSDATARATETABLE_POOLID,
                                (UINT1 *) pWsscfgFsDot11nMCSDataRateEntry);
            return NULL;
        }
        return pWsscfgFsDot11nMCSDataRateEntry;
    }
}

/****************************************************************************
 Function    :  WsscfgFsWtpImageUpgradeTableCreateApi
 Input       :  pWsscfgFsWtpImageUpgradeEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsWtpImageUpgradeEntry *
WsscfgFsWtpImageUpgradeTableCreateApi (tWsscfgFsWtpImageUpgradeEntry *
                                       pSetWsscfgFsWtpImageUpgradeEntry)
{
    tWsscfgFsWtpImageUpgradeEntry *pWsscfgFsWtpImageUpgradeEntry = NULL;

    if (pSetWsscfgFsWtpImageUpgradeEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsWtpImageUpgradeTableCreatApi: pSetWsscfgFsWtpImageUpgradeEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsWtpImageUpgradeEntry = (tWsscfgFsWtpImageUpgradeEntry *)
        MemAllocMemBlk (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID);
    if (pWsscfgFsWtpImageUpgradeEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsWtpImageUpgradeTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsWtpImageUpgradeEntry,
                pSetWsscfgFsWtpImageUpgradeEntry,
                sizeof (tWsscfgFsWtpImageUpgradeEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.FsWtpImageUpgradeTable,
             (tRBElem *) pWsscfgFsWtpImageUpgradeEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsWtpImageUpgradeTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                                (UINT1 *) pWsscfgFsWtpImageUpgradeEntry);
            return NULL;
        }
        return pWsscfgFsWtpImageUpgradeEntry;
    }
}

/****************************************************************************
 Function    :  FsRrmConfigTableFilterInputs
 Input       :  The Indices
                pWsscfgFsRrmConfigEntry
                pWsscfgSetFsRrmConfigEntry
                pWsscfgIsSetFsRrmConfigEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsRrmConfigTableFilterInputs (tWsscfgFsRrmConfigEntry * pWsscfgFsRrmConfigEntry,
                              tWsscfgFsRrmConfigEntry *
                              pWsscfgSetFsRrmConfigEntry,
                              tWsscfgIsSetFsRrmConfigEntry *
                              pWsscfgIsSetFsRrmConfigEntry)
{
    if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmRadioType == OSIX_TRUE)
    {
        if (pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmRadioType ==
            pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmRadioType)
            pWsscfgIsSetFsRrmConfigEntry->bFsRrmRadioType = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmDcaMode == OSIX_TRUE)
    {
        if (pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmDcaMode ==
            pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmDcaMode)
            pWsscfgIsSetFsRrmConfigEntry->bFsRrmDcaMode = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmDcaChannelSelectionMode ==
        OSIX_TRUE)
    {
        if (pWsscfgFsRrmConfigEntry->
            MibObject.i4FsRrmDcaChannelSelectionMode ==
            pWsscfgSetFsRrmConfigEntry->MibObject.
            i4FsRrmDcaChannelSelectionMode)
            pWsscfgIsSetFsRrmConfigEntry->
                bFsRrmDcaChannelSelectionMode = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmTpcMode == OSIX_TRUE)
    {
        if (pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmTpcMode ==
            pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmTpcMode)
            pWsscfgIsSetFsRrmConfigEntry->bFsRrmTpcMode = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmTpcSelectionMode == OSIX_TRUE)
    {
        if (pWsscfgFsRrmConfigEntry->MibObject.
            i4FsRrmTpcSelectionMode ==
            pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmTpcSelectionMode)
            pWsscfgIsSetFsRrmConfigEntry->bFsRrmTpcSelectionMode = OSIX_FALSE;
    }
    if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmRowStatus == OSIX_TRUE)
    {
        if (pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmRowStatus ==
            pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmRowStatus)
            pWsscfgIsSetFsRrmConfigEntry->bFsRrmRowStatus = OSIX_FALSE;
    }
    if ((pWsscfgIsSetFsRrmConfigEntry->bFsRrmRadioType == OSIX_FALSE)
        && (pWsscfgIsSetFsRrmConfigEntry->bFsRrmDcaMode == OSIX_FALSE)
        && (pWsscfgIsSetFsRrmConfigEntry->
            bFsRrmDcaChannelSelectionMode == OSIX_FALSE)
        && (pWsscfgIsSetFsRrmConfigEntry->bFsRrmTpcMode == OSIX_FALSE)
        && (pWsscfgIsSetFsRrmConfigEntry->bFsRrmTpcSelectionMode ==
            OSIX_FALSE)
        && (pWsscfgIsSetFsRrmConfigEntry->bFsRrmRowStatus == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  WsscfgSetAllFsRrmConfigTableTrigger
 Input       :  The Indices
                pWsscfgSetFsRrmConfigEntry
                pWsscfgIsSetFsRrmConfigEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WsscfgSetAllFsRrmConfigTableTrigger (tWsscfgFsRrmConfigEntry *
                                     pWsscfgSetFsRrmConfigEntry,
                                     tWsscfgIsSetFsRrmConfigEntry *
                                     pWsscfgIsSetFsRrmConfigEntry,
                                     INT4 i4SetOption)
{
#ifndef RFMGMT_WANTED
    if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmRadioType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsRrmRadioType, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsRrmConfigEntry->
                      MibObject.i4FsRrmRadioType,
                      pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmRadioType);
    }
    if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmDcaMode == OSIX_TRUE)
    {
        nmhSetCmnNew (FsRrmDcaMode, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsRrmConfigEntry->
                      MibObject.i4FsRrmRadioType,
                      pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmDcaMode);
    }
    if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmDcaChannelSelectionMode ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsRrmDcaChannelSelectionMode, 0,
                      WsscfgMainTaskLock, WsscfgMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%i %i",
                      pWsscfgSetFsRrmConfigEntry->
                      MibObject.i4FsRrmRadioType,
                      pWsscfgSetFsRrmConfigEntry->MibObject.
                      i4FsRrmDcaChannelSelectionMode);
    }
    if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmTpcMode == OSIX_TRUE)
    {
        nmhSetCmnNew (FsRrmTpcMode, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsRrmConfigEntry->
                      MibObject.i4FsRrmRadioType,
                      pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmTpcMode);
    }
    if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmTpcSelectionMode == OSIX_TRUE)
    {
        nmhSetCmnNew (FsRrmTpcSelectionMode, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 0, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsRrmConfigEntry->
                      MibObject.i4FsRrmRadioType,
                      pWsscfgSetFsRrmConfigEntry->MibObject.
                      i4FsRrmTpcSelectionMode);
    }
    if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsRrmRowStatus, 0, WsscfgMainTaskLock,
                      WsscfgMainTaskUnLock, 0, 1, 1, i4SetOption,
                      "%i %i",
                      pWsscfgSetFsRrmConfigEntry->
                      MibObject.i4FsRrmRadioType,
                      pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmRowStatus);
    }
#else
    UNUSED_PARAM (pWsscfgSetFsRrmConfigEntry);
    UNUSED_PARAM (pWsscfgIsSetFsRrmConfigEntry);
    UNUSED_PARAM (i4SetOption);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgFsRrmConfigTableCreateApi
 Input       :  pWsscfgFsRrmConfigEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tWsscfgFsRrmConfigEntry *
WsscfgFsRrmConfigTableCreateApi (tWsscfgFsRrmConfigEntry *
                                 pSetWsscfgFsRrmConfigEntry)
{
    tWsscfgFsRrmConfigEntry *pWsscfgFsRrmConfigEntry = NULL;

    if (pSetWsscfgFsRrmConfigEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsRrmConfigTableCreatApi: pSetWsscfgFsRrmConfigEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pWsscfgFsRrmConfigEntry = (tWsscfgFsRrmConfigEntry *)
        MemAllocMemBlk (WSSCFG_FSRRMCONFIGTABLE_POOLID);
    if (pWsscfgFsRrmConfigEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgFsRrmConfigTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pWsscfgFsRrmConfigEntry, pSetWsscfgFsRrmConfigEntry,
                sizeof (tWsscfgFsRrmConfigEntry));
        if (RBTreeAdd
            (gWsscfgGlobals.WsscfgGlbMib.FsRrmConfigTable,
             (tRBElem *) pWsscfgFsRrmConfigEntry) != RB_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgFsRrmConfigTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                (UINT1 *) pWsscfgFsRrmConfigEntry);
            return NULL;
        }
        return pWsscfgFsRrmConfigEntry;
    }
}

/****************************************************************************
 Function    :  WssCfgUtlSetConfigAllowChannelWidth
 Input       :  i4IfIndex ,i4SetValFsDot11nConfigAllowChannelWidth
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WssCfgUtlSetConfigAllowChannelWidth (INT4 i4IfIndex,
                                     INT4
                                     i4SetValFsDot11nConfigAllowChannelWidth)
{
    tRadioIfGetDB       RadioDB;
    MEMSET (&RadioDB, 0, sizeof (tRadioIfGetDB));
    RadioDB.RadioIfIsGetAllDB.bAllowChannelWidth = OSIX_TRUE;
    RadioDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioDB) !=
        OSIX_SUCCESS)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WssCfgUtlGetConfigAllowChannelWidth  Failed in radiodb.\r\n"));
        return OSIX_FAILURE;
    }
    if (RadioDB.RadioIfGetAllDB.u1AllowChannelWidth ==
        (UINT1) i4SetValFsDot11nConfigAllowChannelWidth)
    {
        return OSIX_SUCCESS;
    }
    RadioDB.RadioIfGetAllDB.u1AllowChannelWidth =
        (UINT1) i4SetValFsDot11nConfigAllowChannelWidth;
    if (WssIfProcessRadioIfDBMsg (WSS_SET_RADIO_IF_DB, &RadioDB) !=
        OSIX_SUCCESS)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WssCfgUtlSetConfigAllowChannelWidth  Failed in radiodb.\r\n"));
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WssCfgUtlGetConfigAllowChannelWidth
 Input       :  i4IfIndex 
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
WssCfgUtlGetConfigAllowChannelWidth (INT4 i4IfIndex,
                                     INT4
                                     *pi4GetValFsDot11nConfigAllowChannelWidth)
{
    tRadioIfGetDB       RadioDB;
    MEMSET (&RadioDB, 0, sizeof (tRadioIfGetDB));
    RadioDB.RadioIfIsGetAllDB.bAllowChannelWidth = OSIX_TRUE;
    RadioDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioDB) !=
        OSIX_SUCCESS)
    {
        *pi4GetValFsDot11nConfigAllowChannelWidth = 0;
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WssCfgUtlGetConfigAllowChannelWidth  Failed in radiodb.\r\n"));
        return OSIX_FAILURE;
    }
    *pi4GetValFsDot11nConfigAllowChannelWidth =
        (INT4) RadioDB.RadioIfGetAllDB.u1AllowChannelWidth;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgUtlTestDot11nAllowConfigTable
 Input       :  i4IfIndex 
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

INT4
WsscfgUtlTestDot11nAllowConfigTable (UINT4 *pu4ErrorCode,
                                     INT4 i4IfIndex,
                                     INT4
                                     i4TestValFsDot11nConfigAllowChannelWidth)
{
    tRadioIfGetDB       RadioDB;
    MEMSET (&RadioDB, 0, sizeof (tRadioIfGetDB));
    RadioDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
    RadioDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioDB) !=
        OSIX_SUCCESS)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgUtlTestDot11nAllowConfigTable Failed in radiodb.\r\n"));
        return OSIX_FAILURE;
    }
    if (WSSCFG_RADIO_TYPEN_AC_COMP (RadioDB.RadioIfGetAllDB.u4Dot11RadioType))
    {
        if ((i4TestValFsDot11nConfigAllowChannelWidth ==
             CLI_ALLOW_CHAN_WIDTH_ANY)
            || (i4TestValFsDot11nConfigAllowChannelWidth ==
                CLI_ALLOW_CHAN_WIDTH_OPERATIONAL))
        {
            return OSIX_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_WSSCFG_ALLOWED_CHAN_WIDTH_ERROR);
            return OSIX_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_WSSCFG_N_AC_RADIOTYPE_ERROR);
        return OSIX_FAILURE;
    }
}
