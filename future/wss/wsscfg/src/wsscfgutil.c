/******************************************************************************
 * Copyright (C) Future Software Limited,2005
 * $Id: wsscfgutil.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 * Description : This file contains the utility 
 *               functions of the WSSCFG module
 *****************************************************************************/

#include "wsscfginc.h"

/******************************************************************************
 * Function   : WsscfgUtilFindWsscfgStruct
 * Description: 
 * Input      :
 * Output     : 
 * Returns    : Pointer to the tWsscfgStruct ,if exists
 *              NULL otherwise
 *****************************************************************************/
/*
PUBLIC tWsscfgStruct    *
WsscfgUtilFindWsscfgStruct ( strcutIndex )
{
}
*/

/*-----------------------------------------------------------------------*/
/*                       End of the file  wsscfgutil.c                     */
/*-----------------------------------------------------------------------*/
