/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
*  $Id: fs1acwrp.c,v 1.2 2017/11/24 10:37:08 siva Exp $
*
* Description: This file contains the wrapper functions called from 
*              nmh routines 
*********************************************************************/

#include  "fs1acwrp.h"
#include  "lr.h"
#include  "fssnmp.h"
#include  "fs11aclw.h"
#include  "wssifinc.h"
#include  "wsscfglwg.h"
#include  "wsscfgcli.h"
#include  "radioifproto.h"

/****************************************************************************
Function    :  wsscfgGetFsDot11VHTOptionImplemented
Input       :  The Indices
IfIndex

The Object
retValFsDot11VHTOptionImplemented
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
wsscfgGetFsDot11VHTOptionImplemented (INT4 i4IfIndex,
                                      INT4
                                      *pi4RetValFsDot11VHTOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bVhtOption = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11VHTOptionImplemented
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1VhtOption;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  wsscfgGetFsDot11NumberOfSpatialStreamsImplemented
Input       :  The Indices
IfIndex

The Object
retValFsDot11NumberOfSpatialStreamsImplemented
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
wsscfgGetFsDot11NumberOfSpatialStreamsImplemented (INT4 i4IfIndex,
                                                   UINT4
                                                   *pu4RetValFsDot11NumberOfSpatialStreamsImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppVhtSTS = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsDot11NumberOfSpatialStreamsImplemented =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppVhtSTS;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  wsscfgGetFsDot11NumberOfSpatialStreamsActivated
Input       :  The Indices
IfIndex

The Object
retValFsDot11NumberOfSpatialStreamsActivated
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
wsscfgGetFsDot11NumberOfSpatialStreamsActivated (INT4 i4IfIndex,
                                                 UINT4
                                                 *pu4RetValFsDot11NumberOfSpatialStreamsActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bVhtSTS = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsDot11NumberOfSpatialStreamsActivated =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1VhtSTS;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  wsscfgGetFsDot11ACVHTRxHighestDataRateSupported 
Input       :  The Indices
IfIndex

The Object
retValFsDot11VHTRxHighestDataRateSupported
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
wsscfgGetFsDot11ACVHTRxHighestDataRateSupported (INT4 i4IfIndex,
                                                 UINT4
                                                 *pu4RetValFsDot11VHTRxHighestDataRateSupported)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppRxDataRate = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsDot11VHTRxHighestDataRateSupported =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u2SuppRxDataRate;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  wsscfgGetFsDot11VHTRxHighestDataRateConfig
Input       :  The Indices
IfIndex

The Object
retValFsDot11VHTRxHighestDataRateSupported
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
wsscfgGetFsDot11VHTRxHighestDataRateConfig (INT4 i4IfIndex,
                                            UINT4
                                            *pu4RetValFsDot11VHTRxHighestDataRateSupported)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRxDataRate = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsDot11VHTRxHighestDataRateSupported =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u2RxDataRate;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  wsscfgGetFsDot11ACVHTTxHighestDataRateSupported
Input       :  The Indices
IfIndex

The Object
retValFsDot11VHTTxHighestDataRateSupported
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
wsscfgGetFsDot11ACVHTTxHighestDataRateSupported (INT4 i4IfIndex,
                                                 UINT4
                                                 *pu4RetValFsDot11VHTTxHighestDataRateSupported)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppTxDataRate = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsDot11VHTTxHighestDataRateSupported =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u2SuppTxDataRate;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  wsscfgGetFsDot11VHTTxHighestDataRateConfig 
Input       :  The Indices
IfIndex

The Object
retValFsDot11VHTTxHighestDataRateSupported
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
wsscfgGetFsDot11VHTTxHighestDataRateConfig (INT4 i4IfIndex,
                                            UINT4
                                            *pu4RetValFsDot11VHTTxHighestDataRateSupported)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bTxDataRate = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsDot11VHTTxHighestDataRateSupported =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u2TxDataRate;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  wsscfgGetFsDot11HighThroughputOptionImplemented
Input       :  The Indices
IfIndex

The Object
retValFsDot11VHTOptionImplemented
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
wsscfgGetDot11HighThroughputOptionImplemented (INT4 i4IfIndex,
                                               INT4
                                               *pi4RetValFsDot11HighThroughputOptionImplemented)
{
    UINT1               u1IfType = 0;
    UINT4               u4RadioType = 0;
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
    CfaGetIfType (i4IfIndex, &u1IfType);

    if (CFA_CAPWAP_VIRT_RADIO == u1IfType)

    {
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB) ==
            OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }
        u4RadioType = RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType;

        if ((u4RadioType == RADIO_TYPE_AC) ||
            ((u4RadioType == RADIO_TYPE_BGN) ||
             (u4RadioType == RADIO_TYPE_AN) || (u4RadioType == RADIO_TYPE_NG)))
        {
            *pi4RetValFsDot11HighThroughputOptionImplemented = OSIX_TRUE;

            return SNMP_SUCCESS;
        }
    }
    *pi4RetValFsDot11HighThroughputOptionImplemented = OSIX_FALSE;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  wsscfgGetFsDot11FragmentationThreshold
Input       :  The Indices
IfIndex

The Object
retValFsDot11VHTTxHighestDataRateSupported
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
wsscfgGetFsDot11FragmentationThreshold (INT4 i4IfIndex,
                                        UINT4
                                        *pu4RetValFsDot11FragmentationThreshold)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bFragmentationThreshold = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsDot11FragmentationThreshold =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.u2FragmentationThreshold;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  wsscfgGetFsDot11OperatingModeNotificationImplemented
Input       :  The Indices
IfIndex

The Object
retValFsDot11VHTOptionImplemented
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
wsscfgGetFsDot11OperatingModeNotificationImplemented (INT4 i4IfIndex,
                                                      INT4
                                                      *pi4RetValFsDot11OperatingModeNotificationImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bVhtOperModeNotify = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11OperatingModeNotificationImplemented
        = (INT4)
        RadioIfGetDB.RadioIfGetAllDB.Dot11AcOperParams.u1VhtOperModeNotify;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  wsscfgGetFsDot11ACVHTTxSTBCOptionImplemented
Input       :  The Indices
IfIndex

The Object
retValFsDot11ACVHTTxSTBCOptionImplemented
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
wsscfgGetFsDot11ACVHTTxSTBCOptionImplemented (INT4 i4IfIndex,
                                              INT4
                                              *pi4RetValFsDot11ACVHTTxSTBCOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppVhtTxStbc = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11ACVHTTxSTBCOptionImplemented
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppTxStbc;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  wsscfgGetFsDot11ACVHTTxSTBCOptionActivated
Input       :  The Indices
IfIndex

The Object
retValFsDot11ACVHTTxSTBCOptionActivated
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
wsscfgGetFsDot11ACVHTTxSTBCOptionActivated (INT4 i4IfIndex,
                                            INT4
                                            *pi4RetValFsDot11ACVHTTxSTBCOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bTxStbc = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11ACVHTTxSTBCOptionActivated
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1TxStbc;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  wsscfgGetFsDot11ACVHTRxSTBCOptionImplemented
Input       :  The Indices
IfIndex

The Object
retValFsDot11ACVHTRxSTBCOptionImplemented
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
wsscfgGetFsDot11ACVHTRxSTBCOptionImplemented (INT4 i4IfIndex,
                                              INT4
                                              *pi4RetValFsDot11ACVHTRxSTBCOptionImplemented)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppRxStbcStatus = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11ACVHTRxSTBCOptionImplemented
        = (INT4)
        RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppRxStbcStatus;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  wsscfgGetFsDot11ACVHTRxSTBCOptionActivated
Input       :  The Indices
IfIndex

The Object
retValFsDot11ACVHTTxSTBCOptionActivated
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
wsscfgGetFsDot11ACVHTRxSTBCOptionActivated (INT4 i4IfIndex,
                                            INT4
                                            *pi4RetValFsDot11ACVHTRxSTBCOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRxStbcStatus = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC, &RadioIfGetDB) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsDot11ACVHTRxSTBCOptionActivated
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1RxStbcStatus;
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  wsscfgSetFsDot11NumberOfSpatialStreamsActivated
Input       :  The Indices
IfIndex

The Object
setValFsDot11ACVHTMaxRxAMPDUFactorConfig
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
wsscfgSetFsDot11NumberOfSpatialStreamsActivated (INT4 i4IfIndex,
                                                 UINT4
                                                 u4SetValFsDot11NumberOfSpatialStreamsActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT2               u2Array = 0;
    UINT1               u1loop = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bVhtSTS = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1VhtSTS =
        (UINT1) u4SetValFsDot11NumberOfSpatialStreamsActivated;
    u2Array = (UINT2) (~(u2Array));
    for (u1loop = (UINT1) u4SetValFsDot11NumberOfSpatialStreamsActivated;
         u1loop > 0; u1loop--)
    {
        u2Array = (UINT2) (u2Array << RADIO_11AC_VHT_MCS_SHIFT_BITS);
        u2Array = (UINT2) (u2Array | RADIO_11AC_VHT_MCS_SHIFT_BITS);
    }
    RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;
    MEMCPY (&RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
            au1VhtCapaMcs[RADIO_VALUE_4], &u2Array, FS11AC_SIZE2);
    MEMCPY (&RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
            au1VhtCapaMcs[RADIO_VALUE_0], &u2Array, FS11AC_SIZE2);

#ifdef WLC_WANTED
    if (RadioIfSetDot11AcCapaParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  wsscfgSetFsDot11FragmentationThreshold
 Input       :  The Indices
                IfIndex

                The Object
                setValFsDot11FragmentationThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
wsscfgSetFsDot11FragmentationThreshold (INT4 i4IfIndex,
                                        UINT4
                                        u4SetValFsDot11FragmentationThreshold)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    RadioIfGetDB.RadioIfIsGetAllDB.bFragmentationThreshold = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u2FragmentationThreshold =
        (UINT2) u4SetValFsDot11FragmentationThreshold;
    if (RadioIfSetOperationTable (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  wsscfgSetFsDot11ACVHTTxSTBCOptionActivated
Input       :  The Indices
IfIndex

The Object
setValFsDot11ACVHTTxSTBCOptionActivated
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
wsscfgSetFsDot11ACVHTTxSTBCOptionActivated (INT4 i4IfIndex,
                                            INT4
                                            i4SetValFsDot11ACVHTTxSTBCOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               u1Mcs = 0;
    UINT2               u2TxMcs = 0;
    UINT1               u1Count = 0;
    UINT1               u1Loop = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    if (i4SetValFsDot11ACVHTTxSTBCOptionActivated == RADIO_VALUE_1)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC,
                                      &RadioIfGetDB) == OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }
        RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_FALSE;
        MEMCPY (&u2TxMcs,
                &(RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                  au1VhtCapaMcs[RADIO_VALUE_4]), sizeof (u2TxMcs));

        for (u1Loop = RADIO_VALUE_0;
             u1Loop < RADIO_11AC_VHT_CAPA_MCS_LEN; u1Loop++)
        {
            u1Mcs = (UINT2) (u2TxMcs & RADIO_11AC_VHT_MCS_SHIFT);
            if (u1Mcs == RADIO_VHT_MCS_DISABLE)
            {
                u1Count++;
            }
            u2TxMcs = (u2TxMcs >> RADIO_11AC_VHT_MCS_SHIFT_BITS);
        }
        if (u1Count == RADIO_VALUE_8)
        {
            return SNMP_FAILURE;
        }
    }

    RadioIfGetDB.RadioIfIsGetAllDB.bTxStbc = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1TxStbc =
        (UINT1) i4SetValFsDot11ACVHTTxSTBCOptionActivated;

#ifdef WLC_WANTED
    if (RadioIfSetDot11AcCapaParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  wsscfgSetFsDot11ACVHTRxSTBCOptionActivated
Input       :  The Indices
IfIndex

The Object
setValFsDot11ACVHTRxSTBCOptionActivated
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
wsscfgSetFsDot11ACVHTRxSTBCOptionActivated (INT4 i4IfIndex,
                                            INT4
                                            i4SetValFsDot11ACVHTRxSTBCOptionActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT1               u1Mcs = 0;
    UINT2               u2RxMcs = 0;
    UINT1               u1Count = 0;
    UINT1               u1RxStbc = 0;
    UINT1               u1Loop = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;

    if (i4SetValFsDot11ACVHTRxSTBCOptionActivated == RADIO_VALUE_1)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_TRUE;
        if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC,
                                      &RadioIfGetDB) == OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }
        RadioIfGetDB.RadioIfIsGetAllDB.bVhtCapaMcs = OSIX_FALSE;
        MEMCPY (&u2RxMcs,
                &(RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                  au1VhtCapaMcs[0]), sizeof (u2RxMcs));

        for (u1Loop = 0; u1Loop < RADIO_11AC_VHT_CAPA_MCS_LEN; u1Loop++)
        {
            u1Mcs = (UINT2) (u2RxMcs & RADIO_11AC_VHT_MCS_SHIFT);
            if (u1Mcs == RADIO_VHT_MCS_DISABLE)
            {
                u1Count++;
            }
            u2RxMcs = (u2RxMcs >> RADIO_11AC_VHT_MCS_SHIFT_BITS);
        }
        if (u1Count == RADIO_VALUE_8)
        {
            return SNMP_FAILURE;
        }
        MEMCPY (&u2RxMcs,
                &(RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
                  au1VhtCapaMcs[RADIO_VALUE_0]), sizeof (u2RxMcs));
        for (u1Loop = RADIO_VALUE_0;
             u1Loop < RADIO_11AC_VHT_CAPA_MCS_LEN; u1Loop++)
        {
            u1Mcs = (UINT2) (u2RxMcs & RADIO_11AC_VHT_MCS_SHIFT);
            if (u1Mcs == RADIO_VHT_MCS_DISABLE)
            {
                u1RxStbc = u1Loop;
                break;
            }
            u2RxMcs = (u2RxMcs >> RADIO_11AC_VHT_MCS_SHIFT_BITS);
        }
        RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.
            u1RxStbc = (u1RxStbc / RADIO_VALUE_2);
    }
    else
    {
        RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1RxStbc = 0;
    }
    RadioIfGetDB.RadioIfIsGetAllDB.bRxStbc = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bRxStbcStatus = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1RxStbcStatus =
        (UINT1) i4SetValFsDot11ACVHTRxSTBCOptionActivated;
#ifdef WLC_WANTED
    if (RadioIfSetDot11AcCapaParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  wsscfgTestv2FsDot11NumberOfSpatialStreamsActivated
Input       :  The Indices
IfIndex

The Object
testValFsDot11NumberOfSpatialStreamsActivated
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
wsscfgTestv2FsDot11NumberOfSpatialStreamsActivated (UINT4 *pu4ErrorCode,
                                                    INT4 i4IfIndex,
                                                    UINT4
                                                    u4TestValFsDot11NumberOfSpatialStreamsActivated)
{
    tRadioIfGetDB       RadioIfGetDB;
    UINT4               u4RadioType = 0;
    UINT4               u4SuppSpatialStreams = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    if ((u4TestValFsDot11NumberOfSpatialStreamsActivated < RADIO_VALUE_1) ||
        (u4TestValFsDot11NumberOfSpatialStreamsActivated > RADIO_VALUE_8))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return SNMP_FAILURE;
    }
    if (nmhGetFsDot11RadioType (i4IfIndex, &u4RadioType) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }
    if (u4RadioType != RADIO_TYPE_AC)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = (UINT4) i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bSuppVhtSTS = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11AC,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return OSIX_FAILURE;

    }
    u4SuppSpatialStreams =
        (UINT4) RadioIfGetDB.RadioIfGetAllDB.Dot11AcCapaParams.u1SuppVhtSTS;
    if (u4TestValFsDot11NumberOfSpatialStreamsActivated > u4SuppSpatialStreams)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  wsscfgTestv2FsDot11FragmentationThreshold
 Input       :  The Indices
                IfIndex

                The Object
                testValFsDot11FragmentationThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
wsscfgTestv2FsDot11FragmentationThreshold (UINT4 *pu4ErrorCode,
                                           INT4 i4IfIndex,
                                           UINT4
                                           u4TestValFsDot11FragmentationThreshold)
{
    UINT4               u4RadioType = 0;
    if (nmhGetFsDot11RadioType (i4IfIndex, &u4RadioType) == SNMP_SUCCESS)
    {
        if (u4RadioType == RADIO_TYPE_AC)
        {
            if ((u4TestValFsDot11FragmentationThreshold <
                 RADIO_11N_FRAG_MINRANGE)
                || (u4TestValFsDot11FragmentationThreshold >
                    RADIO_11AC_FRAG_MAXRANGE))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
        else if ((u4RadioType == RADIO_TYPE_BGN) ||
                 (u4RadioType == RADIO_TYPE_AN) ||
                 (u4RadioType == RADIO_TYPE_NG))
        {
            if ((u4TestValFsDot11FragmentationThreshold <
                 RADIO_11N_FRAG_MINRANGE)
                || (u4TestValFsDot11FragmentationThreshold >
                    RADIO_11N_MAXRANGE))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
        else
        {
            if ((u4TestValFsDot11FragmentationThreshold < RADIO_FRAG_MINRANGE)
                || (u4TestValFsDot11FragmentationThreshold >
                    RADIO_FRAG_MAXRANGE))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  wsscfgTestv2FsDot11ACVHTTxSTBCOptionActivated
Input       :  The Indices
IfIndex

The Object
testValFsDot11ACVHTTxSTBCOptionActivated
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
wsscfgTestv2FsDot11ACVHTTxSTBCOptionActivated (UINT4 *pu4ErrorCode,
                                               INT4 i4IfIndex,
                                               INT4
                                               i4TestValFsDot11ACVHTTxSTBCOptionActivated)
{
    UINT4               u4RadioType = 0;
    INT4                i4TestValFsDot11ACVHTTxSTBCOptionImplemented = 0;
    if ((i4TestValFsDot11ACVHTTxSTBCOptionActivated != RADIO_VALUE_0) &&
        (i4TestValFsDot11ACVHTTxSTBCOptionActivated != RADIO_VALUE_1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (nmhGetFsDot11RadioType (i4IfIndex, &u4RadioType) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
        return SNMP_FAILURE;

    }
    if (u4RadioType != RADIO_TYPE_AC)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (wsscfgGetFsDot11ACVHTTxSTBCOptionImplemented (i4IfIndex,
                                                      &i4TestValFsDot11ACVHTTxSTBCOptionImplemented)
        == SNMP_SUCCESS)
    {
        if ((i4TestValFsDot11ACVHTTxSTBCOptionImplemented == RADIO_VALUE_0) &&
            (i4TestValFsDot11ACVHTTxSTBCOptionActivated == RADIO_VALUE_1))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  wsscfgTestv2FsDot11ACVHTRxSTBCOptionActivated
Input       :  The Indices
IfIndex

The Object
testValFsDot11ACVHTRxSTBCOptionActivated
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
wsscfgTestv2FsDot11ACVHTRxSTBCOptionActivated (UINT4 *pu4ErrorCode,
                                               INT4 i4IfIndex,
                                               INT4
                                               i4TestValFsDot11ACVHTRxSTBCOptionActivated)
{
    UINT4               u4RadioType = 0;
    INT4                i4TestValFsDot11ACVHTRxSTBCOptionImplemented = 0;
    if ((i4TestValFsDot11ACVHTRxSTBCOptionActivated != RADIO_VALUE_0) &&
        (i4TestValFsDot11ACVHTRxSTBCOptionActivated != RADIO_VALUE_1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (nmhGetFsDot11RadioType (i4IfIndex, &u4RadioType) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (CLI_11AC_RADIOTYPE_ERROR);
        return SNMP_FAILURE;

    }
    if (u4RadioType != RADIO_TYPE_AC)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (wsscfgGetFsDot11ACVHTRxSTBCOptionImplemented (i4IfIndex,
                                                      &i4TestValFsDot11ACVHTRxSTBCOptionImplemented)
        == SNMP_SUCCESS)
    {
        if ((i4TestValFsDot11ACVHTRxSTBCOptionImplemented == RADIO_VALUE_0) &&
            (i4TestValFsDot11ACVHTRxSTBCOptionActivated == RADIO_VALUE_1))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}
