/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
 *  $Id: wsscfgdbg.c,v 1.2 2017/05/23 14:16:56 siva Exp $
*
* Description: This file contains the routines for the protocol Database 
* Access for the module Wsscfg 
*********************************************************************/

#include "wsscfginc.h"
#include "wsscfgcli.h"
#define MAX_WLAN_QOS_PRF 9
#define MAX_WLAN_CPBLITY_PRF 9
INT1                gu1WlanQoSPrfCount;
INT1                gu1WlanCpbltyPrfCount;

/****************************************************************************
 Function    :  WsscfgGetAllDot11StationConfigTable
 Input       :  pWsscfgGetDot11StationConfigEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11StationConfigTable (tWsscfgDot11StationConfigEntry *
                                     pWsscfgGetDot11StationConfigEntry)
{
    tWsscfgDot11StationConfigEntry WsscfgDot11StationConfigEntry;

    MEMSET (&WsscfgDot11StationConfigEntry, 0,
            sizeof (tWsscfgDot11StationConfigEntry));

    if (WsscfgGetAllUtlDot11StationConfigTable
        (pWsscfgGetDot11StationConfigEntry,
         &WsscfgDot11StationConfigEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11StationConfigTable:"
                     "WsscfgGetAllUtlDot11StationConfigTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11AuthenticationAlgorithmsTable
 Input       :  pWsscfgGetDot11AuthenticationAlgorithmsEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllDot11AuthenticationAlgorithmsTable
    (tWsscfgDot11AuthenticationAlgorithmsEntry *
     pWsscfgGetDot11AuthenticationAlgorithmsEntry)
{
    tWsscfgDot11AuthenticationAlgorithmsEntry
        * pWsscfgDot11AuthenticationAlgorithmsEntry = NULL;

    tWsscfgFsDot11WlanAuthenticationProfileEntry
        WsscfgGetFsDot11WlanAuthenticationProfileEntry;

    MEMSET (&WsscfgGetFsDot11WlanAuthenticationProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanAuthenticationProfileEntry));

    WsscfgGetFsDot11WlanAuthenticationProfileEntry.MibObject.
        i4IfIndex =
        pWsscfgGetDot11AuthenticationAlgorithmsEntry->MibObject.i4IfIndex;

    if (WsscfgGetAllFsDot11WlanAuthenticationProfileTable
        (&WsscfgGetFsDot11WlanAuthenticationProfileEntry) != OSIX_SUCCESS)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11AuthenticationAlgorithmsTable : "
                     "WsscfgGetAllFsDot11WlanAuthenticationProfileTable failed\r\n"));
        return OSIX_FAILURE;

    }

    /* Assign values from the existing node of structure
     * tWsscfgFsDot11WlanAuthenticationProfileEntry */
    pWsscfgGetDot11AuthenticationAlgorithmsEntry->
        MibObject.i4Dot11AuthenticationAlgorithm =
        WsscfgGetFsDot11WlanAuthenticationProfileEntry.
        MibObject.i4FsDot11WlanAuthenticationAlgorithm;

    /* Setting WlanAuthentication always enabled */
    pWsscfgGetDot11AuthenticationAlgorithmsEntry->
        MibObject.i4Dot11AuthenticationAlgorithmsEnable = WSSCFG_ENABLED;

    if (WsscfgGetAllUtlDot11AuthenticationAlgorithmsTable
        (pWsscfgGetDot11AuthenticationAlgorithmsEntry,
         pWsscfgDot11AuthenticationAlgorithmsEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11AuthenticationAlgorithmsTable:"
                     "WsscfgGetAllUtlDot11AuthenticationAlgorithmsTable"
                     "Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11WEPDefaultKeysTable
 Input       :  pWsscfgGetDot11WEPDefaultKeysEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11WEPDefaultKeysTable (tWsscfgDot11WEPDefaultKeysEntry *
                                      pWsscfgGetDot11WEPDefaultKeysEntry)
{
    tWsscfgDot11WEPDefaultKeysEntry *pWsscfgDot11WEPDefaultKeysEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11WEPDefaultKeysEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.
                   Dot11WEPDefaultKeysTable,
                   (tRBElem *) pWsscfgGetDot11WEPDefaultKeysEntry);

    if (pWsscfgDot11WEPDefaultKeysEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11WEPDefaultKeysTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pWsscfgGetDot11WEPDefaultKeysEntry->
            MibObject.au1Dot11WEPDefaultKeyValue,
            pWsscfgDot11WEPDefaultKeysEntry->
            MibObject.au1Dot11WEPDefaultKeyValue,
            pWsscfgDot11WEPDefaultKeysEntry->
            MibObject.i4Dot11WEPDefaultKeyValueLen);

    pWsscfgGetDot11WEPDefaultKeysEntry->
        MibObject.i4Dot11WEPDefaultKeyValueLen =
        pWsscfgDot11WEPDefaultKeysEntry->MibObject.i4Dot11WEPDefaultKeyValueLen;

    if (WsscfgGetAllUtlDot11WEPDefaultKeysTable
        (pWsscfgGetDot11WEPDefaultKeysEntry,
         pWsscfgDot11WEPDefaultKeysEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11WEPDefaultKeysTable:"
                     "WsscfgGetAllUtlDot11WEPDefaultKeysTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11WEPKeyMappingsTable
 Input       :  pWsscfgGetDot11WEPKeyMappingsEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11WEPKeyMappingsTable (tWsscfgDot11WEPKeyMappingsEntry *
                                      pWsscfgGetDot11WEPKeyMappingsEntry)
{
    tWsscfgDot11WEPKeyMappingsEntry *pWsscfgDot11WEPKeyMappingsEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11WEPKeyMappingsEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.
                   Dot11WEPKeyMappingsTable,
                   (tRBElem *) pWsscfgGetDot11WEPKeyMappingsEntry);

    if (pWsscfgDot11WEPKeyMappingsEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11WEPKeyMappingsTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (&
            (pWsscfgGetDot11WEPKeyMappingsEntry->
             MibObject.Dot11WEPKeyMappingAddress),
            &(pWsscfgDot11WEPKeyMappingsEntry->
              MibObject.Dot11WEPKeyMappingAddress), 6);

    pWsscfgGetDot11WEPKeyMappingsEntry->
        MibObject.i4Dot11WEPKeyMappingWEPOn =
        pWsscfgDot11WEPKeyMappingsEntry->MibObject.i4Dot11WEPKeyMappingWEPOn;

    MEMCPY (pWsscfgGetDot11WEPKeyMappingsEntry->
            MibObject.au1Dot11WEPKeyMappingValue,
            pWsscfgDot11WEPKeyMappingsEntry->
            MibObject.au1Dot11WEPKeyMappingValue,
            pWsscfgDot11WEPKeyMappingsEntry->
            MibObject.i4Dot11WEPKeyMappingValueLen);

    pWsscfgGetDot11WEPKeyMappingsEntry->
        MibObject.i4Dot11WEPKeyMappingValueLen =
        pWsscfgDot11WEPKeyMappingsEntry->MibObject.i4Dot11WEPKeyMappingValueLen;

    pWsscfgGetDot11WEPKeyMappingsEntry->
        MibObject.i4Dot11WEPKeyMappingStatus =
        pWsscfgDot11WEPKeyMappingsEntry->MibObject.i4Dot11WEPKeyMappingStatus;

    if (WsscfgGetAllUtlDot11WEPKeyMappingsTable
        (pWsscfgGetDot11WEPKeyMappingsEntry,
         pWsscfgDot11WEPKeyMappingsEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11WEPKeyMappingsTable:"
                     "WsscfgGetAllUtlDot11WEPKeyMappingsTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11PrivacyTable
 Input       :  pWsscfgGetDot11PrivacyEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11PrivacyTable (tWsscfgDot11PrivacyEntry *
                               pWsscfgGetDot11PrivacyEntry)
{
    tWsscfgDot11PrivacyEntry *pWsscfgDot11PrivacyEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11PrivacyEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11PrivacyTable,
                   (tRBElem *) pWsscfgGetDot11PrivacyEntry);

    if (pWsscfgDot11PrivacyEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11PrivacyTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetDot11PrivacyEntry->MibObject.i4Dot11PrivacyInvoked =
        pWsscfgDot11PrivacyEntry->MibObject.i4Dot11PrivacyInvoked;

    pWsscfgGetDot11PrivacyEntry->MibObject.i4Dot11WEPDefaultKeyID =
        pWsscfgDot11PrivacyEntry->MibObject.i4Dot11WEPDefaultKeyID;

    pWsscfgGetDot11PrivacyEntry->MibObject.
        u4Dot11WEPKeyMappingLength =
        pWsscfgDot11PrivacyEntry->MibObject.u4Dot11WEPKeyMappingLength;

    pWsscfgGetDot11PrivacyEntry->MibObject.i4Dot11ExcludeUnencrypted =
        pWsscfgDot11PrivacyEntry->MibObject.i4Dot11ExcludeUnencrypted;

    pWsscfgGetDot11PrivacyEntry->MibObject.u4Dot11WEPICVErrorCount =
        pWsscfgDot11PrivacyEntry->MibObject.u4Dot11WEPICVErrorCount;

    pWsscfgGetDot11PrivacyEntry->MibObject.u4Dot11WEPExcludedCount =
        pWsscfgDot11PrivacyEntry->MibObject.u4Dot11WEPExcludedCount;

    pWsscfgGetDot11PrivacyEntry->MibObject.i4Dot11RSNAEnabled =
        pWsscfgDot11PrivacyEntry->MibObject.i4Dot11RSNAEnabled;

    pWsscfgGetDot11PrivacyEntry->
        MibObject.i4Dot11RSNAPreauthenticationEnabled =
        pWsscfgDot11PrivacyEntry->MibObject.i4Dot11RSNAPreauthenticationEnabled;

    if (WsscfgGetAllUtlDot11PrivacyTable
        (pWsscfgGetDot11PrivacyEntry, pWsscfgDot11PrivacyEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllDot11PrivacyTable:"
                     "WsscfgGetAllUtlDot11PrivacyTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllFsSecurityWebAuthGuestInfoTable
 Input       :  pWsscfgGetFsSecurityWebAuthGuestInfoEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllFsSecurityWebAuthGuestInfoTable
    (tWsscfgFsSecurityWebAuthGuestInfoEntry *
     pWsscfgGetFsSecurityWebAuthGuestInfoEntry)
{
    if (WsscfgGetAllUtlFsSecurityWebAuthGuestInfoTable
        (pWsscfgGetFsSecurityWebAuthGuestInfoEntry, NULL) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsSecurityWebAuthGuestInfoTable:"
                     "WsscfgGetAllUtlFsSecurityWebAuthGuestInfoTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11MultiDomainCapabilityTable
 Input       :  pWsscfgGetDot11MultiDomainCapabilityEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllDot11MultiDomainCapabilityTable
    (tWsscfgDot11MultiDomainCapabilityEntry *
     pWsscfgGetDot11MultiDomainCapabilityEntry)
{
    tWsscfgDot11MultiDomainCapabilityEntry
        * pWsscfgDot11MultiDomainCapabilityEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11MultiDomainCapabilityEntry =
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.Dot11MultiDomainCapabilityTable,
                   (tRBElem *) pWsscfgGetDot11MultiDomainCapabilityEntry);

    if (pWsscfgDot11MultiDomainCapabilityEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11MultiDomainCapabilityTable:"
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetDot11MultiDomainCapabilityEntry->
        MibObject.i4Dot11FirstChannelNumber =
        pWsscfgDot11MultiDomainCapabilityEntry->
        MibObject.i4Dot11FirstChannelNumber;

    pWsscfgGetDot11MultiDomainCapabilityEntry->
        MibObject.i4Dot11NumberofChannels =
        pWsscfgDot11MultiDomainCapabilityEntry->
        MibObject.i4Dot11NumberofChannels;

    pWsscfgGetDot11MultiDomainCapabilityEntry->
        MibObject.i4Dot11MaximumTransmitPowerLevel =
        pWsscfgDot11MultiDomainCapabilityEntry->
        MibObject.i4Dot11MaximumTransmitPowerLevel;

    if (WsscfgGetAllUtlDot11MultiDomainCapabilityTable
        (pWsscfgGetDot11MultiDomainCapabilityEntry,
         pWsscfgDot11MultiDomainCapabilityEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11MultiDomainCapabilityTable:"
                     "WsscfgGetAllUtlDot11MultiDomainCapabilityTable "
                     " Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11SpectrumManagementTable
 Input       :  pWsscfgGetDot11SpectrumManagementEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11SpectrumManagementTable (tWsscfgDot11SpectrumManagementEntry *
                                          pWsscfgGetDot11SpectrumManagementEntry)
{
    tWsscfgDot11SpectrumManagementEntry
        * pWsscfgDot11SpectrumManagementEntry = NULL;
    if (WsscfgGetAllUtlDot11SpectrumManagementTable
        (pWsscfgGetDot11SpectrumManagementEntry,
         pWsscfgDot11SpectrumManagementEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11SpectrumManagementTable:"
                     "WsscfgGetAllUtlDot11SpectrumManagementTable"
                     "Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11RegulatoryClassesTable
 Input       :  pWsscfgGetDot11RegulatoryClassesEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11RegulatoryClassesTable (tWsscfgDot11RegulatoryClassesEntry *
                                         pWsscfgGetDot11RegulatoryClassesEntry)
{
    tWsscfgDot11RegulatoryClassesEntry
        * pWsscfgDot11RegulatoryClassesEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11RegulatoryClassesEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.
                   Dot11RegulatoryClassesTable,
                   (tRBElem *) pWsscfgGetDot11RegulatoryClassesEntry);

    if (pWsscfgDot11RegulatoryClassesEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11RegulatoryClassesTable: "
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetDot11RegulatoryClassesEntry->
        MibObject.i4Dot11RegulatoryClass =
        pWsscfgDot11RegulatoryClassesEntry->MibObject.i4Dot11RegulatoryClass;

    pWsscfgGetDot11RegulatoryClassesEntry->MibObject.
        i4Dot11CoverageClass =
        pWsscfgDot11RegulatoryClassesEntry->MibObject.i4Dot11CoverageClass;

    if (WsscfgGetAllUtlDot11RegulatoryClassesTable
        (pWsscfgGetDot11RegulatoryClassesEntry,
         pWsscfgDot11RegulatoryClassesEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11RegulatoryClassesTable:"
                     "WsscfgGetAllUtlDot11RegulatoryClassesTable "
                     "Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11OperationTable
 Input       :  pWsscfgGetDot11OperationEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11OperationTable (tWsscfgDot11OperationEntry *
                                 pWsscfgGetDot11OperationEntry)
{
    tWsscfgDot11OperationEntry *pWsscfgDot11OperationEntry = NULL;
    if (WsscfgGetAllUtlDot11OperationTable
        (pWsscfgGetDot11OperationEntry,
         pWsscfgDot11OperationEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11OperationTable:"
                     "WsscfgGetAllUtlDot11OperationTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11CountersTable
 Input       :  pWsscfgGetDot11CountersEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11CountersTable (tWsscfgDot11CountersEntry *
                                pWsscfgGetDot11CountersEntry)
{
    tWsscfgDot11CountersEntry *pWsscfgDot11CountersEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11CountersEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11CountersTable,
                   (tRBElem *) pWsscfgGetDot11CountersEntry);

    if (pWsscfgDot11CountersEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11CountersTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetDot11CountersEntry->
        MibObject.u4Dot11TransmittedFragmentCount =
        pWsscfgDot11CountersEntry->MibObject.u4Dot11TransmittedFragmentCount;

    pWsscfgGetDot11CountersEntry->
        MibObject.u4Dot11MulticastTransmittedFrameCount =
        pWsscfgDot11CountersEntry->
        MibObject.u4Dot11MulticastTransmittedFrameCount;

    pWsscfgGetDot11CountersEntry->MibObject.u4Dot11FailedCount =
        pWsscfgDot11CountersEntry->MibObject.u4Dot11FailedCount;

    pWsscfgGetDot11CountersEntry->MibObject.u4Dot11RetryCount =
        pWsscfgDot11CountersEntry->MibObject.u4Dot11RetryCount;

    pWsscfgGetDot11CountersEntry->MibObject.
        u4Dot11MultipleRetryCount =
        pWsscfgDot11CountersEntry->MibObject.u4Dot11MultipleRetryCount;

    pWsscfgGetDot11CountersEntry->MibObject.
        u4Dot11FrameDuplicateCount =
        pWsscfgDot11CountersEntry->MibObject.u4Dot11FrameDuplicateCount;

    pWsscfgGetDot11CountersEntry->MibObject.u4Dot11RTSSuccessCount =
        pWsscfgDot11CountersEntry->MibObject.u4Dot11RTSSuccessCount;

    pWsscfgGetDot11CountersEntry->MibObject.u4Dot11RTSFailureCount =
        pWsscfgDot11CountersEntry->MibObject.u4Dot11RTSFailureCount;

    pWsscfgGetDot11CountersEntry->MibObject.u4Dot11ACKFailureCount =
        pWsscfgDot11CountersEntry->MibObject.u4Dot11ACKFailureCount;

    pWsscfgGetDot11CountersEntry->MibObject.
        u4Dot11ReceivedFragmentCount =
        pWsscfgDot11CountersEntry->MibObject.u4Dot11ReceivedFragmentCount;

    pWsscfgGetDot11CountersEntry->
        MibObject.u4Dot11MulticastReceivedFrameCount =
        pWsscfgDot11CountersEntry->MibObject.u4Dot11MulticastReceivedFrameCount;

    pWsscfgGetDot11CountersEntry->MibObject.u4Dot11FCSErrorCount =
        pWsscfgDot11CountersEntry->MibObject.u4Dot11FCSErrorCount;

    pWsscfgGetDot11CountersEntry->MibObject.
        u4Dot11TransmittedFrameCount =
        pWsscfgDot11CountersEntry->MibObject.u4Dot11TransmittedFrameCount;

    pWsscfgGetDot11CountersEntry->MibObject.
        u4Dot11WEPUndecryptableCount =
        pWsscfgDot11CountersEntry->MibObject.u4Dot11WEPUndecryptableCount;

    pWsscfgGetDot11CountersEntry->
        MibObject.u4Dot11QosDiscardedFragmentCount =
        pWsscfgDot11CountersEntry->MibObject.u4Dot11QosDiscardedFragmentCount;

    pWsscfgGetDot11CountersEntry->MibObject.
        u4Dot11AssociatedStationCount =
        pWsscfgDot11CountersEntry->MibObject.u4Dot11AssociatedStationCount;

    pWsscfgGetDot11CountersEntry->
        MibObject.u4Dot11QosCFPollsReceivedCount =
        pWsscfgDot11CountersEntry->MibObject.u4Dot11QosCFPollsReceivedCount;

    pWsscfgGetDot11CountersEntry->MibObject.
        u4Dot11QosCFPollsUnusedCount =
        pWsscfgDot11CountersEntry->MibObject.u4Dot11QosCFPollsUnusedCount;

    pWsscfgGetDot11CountersEntry->
        MibObject.u4Dot11QosCFPollsUnusableCount =
        pWsscfgDot11CountersEntry->MibObject.u4Dot11QosCFPollsUnusableCount;

    if (WsscfgGetAllUtlDot11CountersTable
        (pWsscfgGetDot11CountersEntry,
         pWsscfgDot11CountersEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllDot11CountersTable:"
                     "WsscfgGetAllUtlDot11CountersTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11GroupAddressesTable
 Input       :  pWsscfgGetDot11GroupAddressesEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11GroupAddressesTable (tWsscfgDot11GroupAddressesEntry *
                                      pWsscfgGetDot11GroupAddressesEntry)
{
    tWsscfgDot11GroupAddressesEntry *pWsscfgDot11GroupAddressesEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11GroupAddressesEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.
                   Dot11GroupAddressesTable,
                   (tRBElem *) pWsscfgGetDot11GroupAddressesEntry);

    if (pWsscfgDot11GroupAddressesEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11GroupAddressesTable:"
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (&
            (pWsscfgGetDot11GroupAddressesEntry->MibObject.
             Dot11Address),
            &(pWsscfgDot11GroupAddressesEntry->MibObject.Dot11Address), 6);

    pWsscfgGetDot11GroupAddressesEntry->
        MibObject.i4Dot11GroupAddressesStatus =
        pWsscfgDot11GroupAddressesEntry->MibObject.i4Dot11GroupAddressesStatus;

    if (WsscfgGetAllUtlDot11GroupAddressesTable
        (pWsscfgGetDot11GroupAddressesEntry,
         pWsscfgDot11GroupAddressesEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11GroupAddressesTable:"
                     "WsscfgGetAllUtlDot11GroupAddressesTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11EDCATable
 Input       :  pWsscfgGetDot11EDCAEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11EDCATable (tWsscfgDot11EDCAEntry * pWsscfgGetDot11EDCAEntry)
{
    tWsscfgDot11EDCAEntry *pWsscfgDot11EDCAEntry = NULL;
    if (WsscfgGetAllUtlDot11EDCATable
        (pWsscfgGetDot11EDCAEntry, pWsscfgDot11EDCAEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11EDCATable:"
                     "WsscfgGetAllUtlDot11EDCATable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11QAPEDCATable
 Input       :  pWsscfgGetDot11QAPEDCAEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11QAPEDCATable (tWsscfgDot11QAPEDCAEntry *
                               pWsscfgGetDot11QAPEDCAEntry)
{
    tWsscfgDot11QAPEDCAEntry *pWsscfgDot11QAPEDCAEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11QAPEDCAEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11QAPEDCATable,
                   (tRBElem *) pWsscfgGetDot11QAPEDCAEntry);

    if (pWsscfgDot11QAPEDCAEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11QAPEDCATable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableCWmin =
        pWsscfgDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableCWmin;

    pWsscfgGetDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableCWmax =
        pWsscfgDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableCWmax;

    pWsscfgGetDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableAIFSN =
        pWsscfgDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableAIFSN;

    pWsscfgGetDot11QAPEDCAEntry->MibObject.
        i4Dot11QAPEDCATableTXOPLimit =
        pWsscfgDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableTXOPLimit;

    pWsscfgGetDot11QAPEDCAEntry->
        MibObject.i4Dot11QAPEDCATableMSDULifetime =
        pWsscfgDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableMSDULifetime;

    pWsscfgGetDot11QAPEDCAEntry->MibObject.
        i4Dot11QAPEDCATableMandatory =
        pWsscfgDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableMandatory;

    if (WsscfgGetAllUtlDot11QAPEDCATable
        (pWsscfgGetDot11QAPEDCAEntry, pWsscfgDot11QAPEDCAEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllDot11QAPEDCATable:"
                     "WsscfgGetAllUtlDot11QAPEDCATable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11QosCountersTable
 Input       :  pWsscfgGetDot11QosCountersEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11QosCountersTable (tWsscfgDot11QosCountersEntry *
                                   pWsscfgGetDot11QosCountersEntry)
{
    tWsscfgDot11QosCountersEntry *pWsscfgDot11QosCountersEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11QosCountersEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11QosCountersTable,
                   (tRBElem *) pWsscfgGetDot11QosCountersEntry);

    if (pWsscfgDot11QosCountersEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11QosCountersTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetDot11QosCountersEntry->
        MibObject.u4Dot11QosTransmittedFragmentCount =
        pWsscfgDot11QosCountersEntry->
        MibObject.u4Dot11QosTransmittedFragmentCount;

    pWsscfgGetDot11QosCountersEntry->MibObject.u4Dot11QosFailedCount =
        pWsscfgDot11QosCountersEntry->MibObject.u4Dot11QosFailedCount;

    pWsscfgGetDot11QosCountersEntry->MibObject.u4Dot11QosRetryCount =
        pWsscfgDot11QosCountersEntry->MibObject.u4Dot11QosRetryCount;

    pWsscfgGetDot11QosCountersEntry->
        MibObject.u4Dot11QosMultipleRetryCount =
        pWsscfgDot11QosCountersEntry->MibObject.u4Dot11QosMultipleRetryCount;

    pWsscfgGetDot11QosCountersEntry->
        MibObject.u4Dot11QosFrameDuplicateCount =
        pWsscfgDot11QosCountersEntry->MibObject.u4Dot11QosFrameDuplicateCount;

    pWsscfgGetDot11QosCountersEntry->MibObject.
        u4Dot11QosRTSSuccessCount =
        pWsscfgDot11QosCountersEntry->MibObject.u4Dot11QosRTSSuccessCount;

    pWsscfgGetDot11QosCountersEntry->MibObject.
        u4Dot11QosRTSFailureCount =
        pWsscfgDot11QosCountersEntry->MibObject.u4Dot11QosRTSFailureCount;

    pWsscfgGetDot11QosCountersEntry->MibObject.
        u4Dot11QosACKFailureCount =
        pWsscfgDot11QosCountersEntry->MibObject.u4Dot11QosACKFailureCount;

    pWsscfgGetDot11QosCountersEntry->
        MibObject.u4Dot11QosReceivedFragmentCount =
        pWsscfgDot11QosCountersEntry->MibObject.u4Dot11QosReceivedFragmentCount;

    pWsscfgGetDot11QosCountersEntry->
        MibObject.u4Dot11QosTransmittedFrameCount =
        pWsscfgDot11QosCountersEntry->MibObject.u4Dot11QosTransmittedFrameCount;

    pWsscfgGetDot11QosCountersEntry->
        MibObject.u4Dot11QosDiscardedFrameCount =
        pWsscfgDot11QosCountersEntry->MibObject.u4Dot11QosDiscardedFrameCount;

    pWsscfgGetDot11QosCountersEntry->
        MibObject.u4Dot11QosMPDUsReceivedCount =
        pWsscfgDot11QosCountersEntry->MibObject.u4Dot11QosMPDUsReceivedCount;

    pWsscfgGetDot11QosCountersEntry->
        MibObject.u4Dot11QosRetriesReceivedCount =
        pWsscfgDot11QosCountersEntry->MibObject.u4Dot11QosRetriesReceivedCount;

    if (WsscfgGetAllUtlDot11QosCountersTable
        (pWsscfgGetDot11QosCountersEntry,
         pWsscfgDot11QosCountersEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11QosCountersTable:"
                     "WsscfgGetAllUtlDot11QosCountersTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11ResourceInfoTable
 Input       :  pWsscfgGetDot11ResourceInfoEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11ResourceInfoTable (tWsscfgDot11ResourceInfoEntry *
                                    pWsscfgGetDot11ResourceInfoEntry)
{
    tWsscfgDot11ResourceInfoEntry *pWsscfgDot11ResourceInfoEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11ResourceInfoEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11ResourceInfoTable,
                   (tRBElem *) pWsscfgGetDot11ResourceInfoEntry);

    if (pWsscfgDot11ResourceInfoEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11ResourceInfoTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pWsscfgGetDot11ResourceInfoEntry->
            MibObject.au1Dot11manufacturerOUI,
            pWsscfgDot11ResourceInfoEntry->
            MibObject.au1Dot11manufacturerOUI,
            pWsscfgDot11ResourceInfoEntry->MibObject.i4Dot11manufacturerOUILen);

    pWsscfgGetDot11ResourceInfoEntry->MibObject.
        i4Dot11manufacturerOUILen =
        pWsscfgDot11ResourceInfoEntry->MibObject.i4Dot11manufacturerOUILen;

    MEMCPY (pWsscfgGetDot11ResourceInfoEntry->
            MibObject.au1Dot11manufacturerName,
            pWsscfgDot11ResourceInfoEntry->
            MibObject.au1Dot11manufacturerName,
            pWsscfgDot11ResourceInfoEntry->
            MibObject.i4Dot11manufacturerNameLen);

    pWsscfgGetDot11ResourceInfoEntry->
        MibObject.i4Dot11manufacturerNameLen =
        pWsscfgDot11ResourceInfoEntry->MibObject.i4Dot11manufacturerNameLen;

    MEMCPY (pWsscfgGetDot11ResourceInfoEntry->
            MibObject.au1Dot11manufacturerProductName,
            pWsscfgDot11ResourceInfoEntry->
            MibObject.au1Dot11manufacturerProductName,
            pWsscfgDot11ResourceInfoEntry->
            MibObject.i4Dot11manufacturerProductNameLen);

    pWsscfgGetDot11ResourceInfoEntry->
        MibObject.i4Dot11manufacturerProductNameLen =
        pWsscfgDot11ResourceInfoEntry->
        MibObject.i4Dot11manufacturerProductNameLen;

    MEMCPY (pWsscfgGetDot11ResourceInfoEntry->
            MibObject.au1Dot11manufacturerProductVersion,
            pWsscfgDot11ResourceInfoEntry->
            MibObject.au1Dot11manufacturerProductVersion,
            pWsscfgDot11ResourceInfoEntry->
            MibObject.i4Dot11manufacturerProductVersionLen);

    pWsscfgGetDot11ResourceInfoEntry->
        MibObject.i4Dot11manufacturerProductVersionLen =
        pWsscfgDot11ResourceInfoEntry->
        MibObject.i4Dot11manufacturerProductVersionLen;

    if (WsscfgGetAllUtlDot11ResourceInfoTable
        (pWsscfgGetDot11ResourceInfoEntry,
         pWsscfgDot11ResourceInfoEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11ResourceInfoTable:"
                     "WsscfgGetAllUtlDot11ResourceInfoTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11PhyOperationTable
 Input       :  pWsscfgGetDot11PhyOperationEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11PhyOperationTable (tWsscfgDot11PhyOperationEntry *
                                    pWsscfgGetDot11PhyOperationEntry)
{
    tWsscfgDot11PhyOperationEntry *pWsscfgDot11PhyOperationEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11PhyOperationEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyOperationTable,
                   (tRBElem *) pWsscfgGetDot11PhyOperationEntry);

    if (pWsscfgDot11PhyOperationEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11PhyOperationTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetDot11PhyOperationEntry->MibObject.i4Dot11PHYType =
        pWsscfgDot11PhyOperationEntry->MibObject.i4Dot11PHYType;

    pWsscfgGetDot11PhyOperationEntry->MibObject.
        i4Dot11CurrentRegDomain =
        pWsscfgDot11PhyOperationEntry->MibObject.i4Dot11CurrentRegDomain;

    pWsscfgGetDot11PhyOperationEntry->MibObject.i4Dot11TempType =
        pWsscfgDot11PhyOperationEntry->MibObject.i4Dot11TempType;

    if (WsscfgGetAllUtlDot11PhyOperationTable
        (pWsscfgGetDot11PhyOperationEntry,
         pWsscfgDot11PhyOperationEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11PhyOperationTable:"
                     "WsscfgGetAllUtlDot11PhyOperationTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11PhyAntennaTable
 Input       :  pWsscfgGetDot11PhyAntennaEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11PhyAntennaTable (tWsscfgDot11PhyAntennaEntry *
                                  pWsscfgGetDot11PhyAntennaEntry)
{
    tWsscfgDot11PhyAntennaEntry *pWsscfgDot11PhyAntennaEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11PhyAntennaEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyAntennaTable,
                   (tRBElem *) pWsscfgGetDot11PhyAntennaEntry);

    if (pWsscfgDot11PhyAntennaEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11PhyAntennaTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetDot11PhyAntennaEntry->MibObject.
        i4Dot11CurrentTxAntenna =
        pWsscfgDot11PhyAntennaEntry->MibObject.i4Dot11CurrentTxAntenna;

    pWsscfgGetDot11PhyAntennaEntry->MibObject.
        i4Dot11DiversitySupport =
        pWsscfgDot11PhyAntennaEntry->MibObject.i4Dot11DiversitySupport;

    pWsscfgGetDot11PhyAntennaEntry->MibObject.
        i4Dot11CurrentRxAntenna =
        pWsscfgDot11PhyAntennaEntry->MibObject.i4Dot11CurrentRxAntenna;

    if (WsscfgGetAllUtlDot11PhyAntennaTable
        (pWsscfgGetDot11PhyAntennaEntry,
         pWsscfgDot11PhyAntennaEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11PhyAntennaTable:"
                     "WsscfgGetAllUtlDot11PhyAntennaTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11PhyTxPowerTable
 Input       :  pWsscfgGetDot11PhyTxPowerEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11PhyTxPowerTable (tWsscfgDot11PhyTxPowerEntry *
                                  pWsscfgGetDot11PhyTxPowerEntry)
{
    tWsscfgDot11PhyTxPowerEntry *pWsscfgDot11PhyTxPowerEntry = NULL;

    if (WsscfgGetAllUtlDot11PhyTxPowerTable
        (pWsscfgGetDot11PhyTxPowerEntry,
         pWsscfgDot11PhyTxPowerEntry) != OSIX_FAILURE)
    {
        return OSIX_SUCCESS;
    }
    else
    {

        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11PhyTxPowerTable:"
                     "WsscfgGetAllUtlDot11PhyTxPowerTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }

}

/****************************************************************************
 Function    :  WsscfgGetAllDot11PhyFHSSTable
 Input       :  pWsscfgGetDot11PhyFHSSEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11PhyFHSSTable (tWsscfgDot11PhyFHSSEntry *
                               pWsscfgGetDot11PhyFHSSEntry)
{
    tWsscfgDot11PhyFHSSEntry *pWsscfgDot11PhyFHSSEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11PhyFHSSEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyFHSSTable,
                   (tRBElem *) pWsscfgGetDot11PhyFHSSEntry);

    if (pWsscfgDot11PhyFHSSEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11PhyFHSSTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetDot11PhyFHSSEntry->MibObject.i4Dot11HopTime =
        pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11HopTime;

    pWsscfgGetDot11PhyFHSSEntry->MibObject.
        i4Dot11CurrentChannelNumber =
        pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11CurrentChannelNumber;

    pWsscfgGetDot11PhyFHSSEntry->MibObject.i4Dot11MaxDwellTime =
        pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11MaxDwellTime;

    pWsscfgGetDot11PhyFHSSEntry->MibObject.i4Dot11CurrentDwellTime =
        pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11CurrentDwellTime;

    pWsscfgGetDot11PhyFHSSEntry->MibObject.i4Dot11CurrentSet =
        pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11CurrentSet;

    pWsscfgGetDot11PhyFHSSEntry->MibObject.i4Dot11CurrentPattern =
        pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11CurrentPattern;

    pWsscfgGetDot11PhyFHSSEntry->MibObject.i4Dot11CurrentIndex =
        pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11CurrentIndex;

    pWsscfgGetDot11PhyFHSSEntry->MibObject.i4Dot11EHCCPrimeRadix =
        pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11EHCCPrimeRadix;

    pWsscfgGetDot11PhyFHSSEntry->
        MibObject.i4Dot11EHCCNumberofChannelsFamilyIndex =
        pWsscfgDot11PhyFHSSEntry->
        MibObject.i4Dot11EHCCNumberofChannelsFamilyIndex;

    pWsscfgGetDot11PhyFHSSEntry->
        MibObject.i4Dot11EHCCCapabilityImplemented =
        pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11EHCCCapabilityImplemented;

    pWsscfgGetDot11PhyFHSSEntry->MibObject.
        i4Dot11EHCCCapabilityEnabled =
        pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11EHCCCapabilityEnabled;

    pWsscfgGetDot11PhyFHSSEntry->MibObject.
        i4Dot11HopAlgorithmAdopted =
        pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11HopAlgorithmAdopted;

    pWsscfgGetDot11PhyFHSSEntry->MibObject.i4Dot11RandomTableFlag =
        pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11RandomTableFlag;

    pWsscfgGetDot11PhyFHSSEntry->MibObject.
        i4Dot11NumberofHoppingSets =
        pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11NumberofHoppingSets;

    pWsscfgGetDot11PhyFHSSEntry->MibObject.i4Dot11HopModulus =
        pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11HopModulus;

    pWsscfgGetDot11PhyFHSSEntry->MibObject.i4Dot11HopOffset =
        pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11HopOffset;

    if (WsscfgGetAllUtlDot11PhyFHSSTable
        (pWsscfgGetDot11PhyFHSSEntry, pWsscfgDot11PhyFHSSEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllDot11PhyFHSSTable:"
                     "WsscfgGetAllUtlDot11PhyFHSSTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11PhyDSSSTable
 Input       :  pWsscfgGetDot11PhyDSSSEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11PhyDSSSTable (tWsscfgDot11PhyDSSSEntry *
                               pWsscfgGetDot11PhyDSSSEntry)
{
    tWsscfgDot11PhyDSSSEntry *pWsscfgDot11PhyDSSSEntry = NULL;

    if (WsscfgGetAllUtlDot11PhyDSSSTable
        (pWsscfgGetDot11PhyDSSSEntry, pWsscfgDot11PhyDSSSEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11PhyDSSSTable:"
                     "WsscfgGetAllUtlDot11PhyDSSSTable Returns Failure\r\n"));
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11PhyIRTable
 Input       :  pWsscfgGetDot11PhyIREntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11PhyIRTable (tWsscfgDot11PhyIREntry * pWsscfgGetDot11PhyIREntry)
{
    tWsscfgDot11PhyIREntry *pWsscfgDot11PhyIREntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11PhyIREntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyIRTable,
                   (tRBElem *) pWsscfgGetDot11PhyIREntry);

    if (pWsscfgDot11PhyIREntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11PhyIRTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogTimerMax =
        pWsscfgDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogTimerMax;

    pWsscfgGetDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogCountMax =
        pWsscfgDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogCountMax;

    pWsscfgGetDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogTimerMin =
        pWsscfgDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogTimerMin;

    pWsscfgGetDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogCountMin =
        pWsscfgDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogCountMin;

    if (WsscfgGetAllUtlDot11PhyIRTable
        (pWsscfgGetDot11PhyIREntry, pWsscfgDot11PhyIREntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllDot11PhyIRTable:"
                     "WsscfgGetAllUtlDot11PhyIRTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11RegDomainsSupportedTable
 Input       :  pWsscfgGetDot11RegDomainsSupportedEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11RegDomainsSupportedTable (tWsscfgDot11RegDomainsSupportedEntry
                                           *
                                           pWsscfgGetDot11RegDomainsSupportedEntry)
{
    tWsscfgDot11RegDomainsSupportedEntry
        * pWsscfgDot11RegDomainsSupportedEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11RegDomainsSupportedEntry =
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.Dot11RegDomainsSupportedTable,
                   (tRBElem *) pWsscfgGetDot11RegDomainsSupportedEntry);

    if (pWsscfgDot11RegDomainsSupportedEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11RegDomainsSupportedTable:"
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetDot11RegDomainsSupportedEntry->
        MibObject.i4Dot11RegDomainsSupportedValue =
        pWsscfgDot11RegDomainsSupportedEntry->
        MibObject.i4Dot11RegDomainsSupportedValue;

    if (WsscfgGetAllUtlDot11RegDomainsSupportedTable
        (pWsscfgGetDot11RegDomainsSupportedEntry,
         pWsscfgDot11RegDomainsSupportedEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11RegDomainsSupportedTable:"
                     "WsscfgGetAllUtlDot11RegDomainsSupportedTable"
                     "Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11AntennasListTable
 Input       :  pWsscfgGetDot11AntennasListEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11AntennasListTable (tWsscfgDot11AntennasListEntry *
                                    pWsscfgGetDot11AntennasListEntry)
{
    tWsscfgDot11AntennasListEntry *pWsscfgDot11AntennasListEntry = NULL;

    if (WsscfgGetAllUtlDot11AntennasListTable
        (pWsscfgGetDot11AntennasListEntry,
         pWsscfgDot11AntennasListEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11AntennasListTable:"
                     "WsscfgGetAllUtlDot11AntennasListTable Returns Failure\r\n"));
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11SupportedDataRatesTxTable
 Input       :  pWsscfgGetDot11SupportedDataRatesTxEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllDot11SupportedDataRatesTxTable
    (tWsscfgDot11SupportedDataRatesTxEntry *
     pWsscfgGetDot11SupportedDataRatesTxEntry)
{
    tWsscfgDot11SupportedDataRatesTxEntry
        * pWsscfgDot11SupportedDataRatesTxEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11SupportedDataRatesTxEntry =
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.Dot11SupportedDataRatesTxTable,
                   (tRBElem *) pWsscfgGetDot11SupportedDataRatesTxEntry);

    if (pWsscfgDot11SupportedDataRatesTxEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11SupportedDataRatesTxTable: Entry doesn't"
                     "exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetDot11SupportedDataRatesTxEntry->
        MibObject.i4Dot11SupportedDataRatesTxValue =
        pWsscfgDot11SupportedDataRatesTxEntry->
        MibObject.i4Dot11SupportedDataRatesTxValue;

    if (WsscfgGetAllUtlDot11SupportedDataRatesTxTable
        (pWsscfgGetDot11SupportedDataRatesTxEntry,
         pWsscfgDot11SupportedDataRatesTxEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11SupportedDataRatesTxTable:"
                     "WsscfgGetAllUtlDot11SupportedDataRatesTxTable Returns"
                     "Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11SupportedDataRatesRxTable
 Input       :  pWsscfgGetDot11SupportedDataRatesRxEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllDot11SupportedDataRatesRxTable
    (tWsscfgDot11SupportedDataRatesRxEntry *
     pWsscfgGetDot11SupportedDataRatesRxEntry)
{
    tWsscfgDot11SupportedDataRatesRxEntry
        * pWsscfgDot11SupportedDataRatesRxEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11SupportedDataRatesRxEntry =
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.Dot11SupportedDataRatesRxTable,
                   (tRBElem *) pWsscfgGetDot11SupportedDataRatesRxEntry);

    if (pWsscfgDot11SupportedDataRatesRxEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11SupportedDataRatesRxTable: "
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetDot11SupportedDataRatesRxEntry->
        MibObject.i4Dot11SupportedDataRatesRxValue =
        pWsscfgDot11SupportedDataRatesRxEntry->
        MibObject.i4Dot11SupportedDataRatesRxValue;

    if (WsscfgGetAllUtlDot11SupportedDataRatesRxTable
        (pWsscfgGetDot11SupportedDataRatesRxEntry,
         pWsscfgDot11SupportedDataRatesRxEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11SupportedDataRatesRxTable:"
                     "WsscfgGetAllUtlDot11SupportedDataRatesRxTable"
                     "Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11PhyOFDMTable
 Input       :  pWsscfgGetDot11PhyOFDMEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11PhyOFDMTable (tWsscfgDot11PhyOFDMEntry *
                               pWsscfgGetDot11PhyOFDMEntry)
{
    tWsscfgDot11PhyOFDMEntry *pWsscfgDot11PhyOFDMEntry = NULL;

    if (WsscfgGetAllUtlDot11PhyOFDMTable
        (pWsscfgGetDot11PhyOFDMEntry, pWsscfgDot11PhyOFDMEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllDot11PhyOFDMTable:"
                     "WsscfgGetAllUtlDot11PhyOFDMTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11PhyHRDSSSTable
 Input       :  pWsscfgGetDot11PhyHRDSSSEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11PhyHRDSSSTable (tWsscfgDot11PhyHRDSSSEntry *
                                 pWsscfgGetDot11PhyHRDSSSEntry)
{
    tWsscfgDot11PhyHRDSSSEntry *pWsscfgDot11PhyHRDSSSEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11PhyHRDSSSEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyHRDSSSTable,
                   (tRBElem *) pWsscfgGetDot11PhyHRDSSSEntry);

    if (pWsscfgDot11PhyHRDSSSEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11PhyHRDSSSTable:"
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetDot11PhyHRDSSSEntry->
        MibObject.i4Dot11ShortPreambleOptionImplemented =
        pWsscfgDot11PhyHRDSSSEntry->
        MibObject.i4Dot11ShortPreambleOptionImplemented;

    pWsscfgGetDot11PhyHRDSSSEntry->MibObject.
        i4Dot11PBCCOptionImplemented =
        pWsscfgDot11PhyHRDSSSEntry->MibObject.i4Dot11PBCCOptionImplemented;

    pWsscfgGetDot11PhyHRDSSSEntry->MibObject.
        i4Dot11ChannelAgilityPresent =
        pWsscfgDot11PhyHRDSSSEntry->MibObject.i4Dot11ChannelAgilityPresent;

    pWsscfgGetDot11PhyHRDSSSEntry->MibObject.
        i4Dot11ChannelAgilityEnabled =
        pWsscfgDot11PhyHRDSSSEntry->MibObject.i4Dot11ChannelAgilityEnabled;

    pWsscfgGetDot11PhyHRDSSSEntry->MibObject.
        i4Dot11HRCCAModeSupported =
        pWsscfgDot11PhyHRDSSSEntry->MibObject.i4Dot11HRCCAModeSupported;

    if (WsscfgGetAllUtlDot11PhyHRDSSSTable
        (pWsscfgGetDot11PhyHRDSSSEntry,
         pWsscfgDot11PhyHRDSSSEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11PhyHRDSSSTable:"
                     "WsscfgGetAllUtlDot11PhyHRDSSSTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11HoppingPatternTable
 Input       :  pWsscfgGetDot11HoppingPatternEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11HoppingPatternTable (tWsscfgDot11HoppingPatternEntry *
                                      pWsscfgGetDot11HoppingPatternEntry)
{
    tWsscfgDot11HoppingPatternEntry *pWsscfgDot11HoppingPatternEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11HoppingPatternEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.
                   Dot11HoppingPatternTable,
                   (tRBElem *) pWsscfgGetDot11HoppingPatternEntry);

    if (pWsscfgDot11HoppingPatternEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11HoppingPatternTable:"
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetDot11HoppingPatternEntry->
        MibObject.i4Dot11RandomTableFieldNumber =
        pWsscfgDot11HoppingPatternEntry->
        MibObject.i4Dot11RandomTableFieldNumber;

    if (WsscfgGetAllUtlDot11HoppingPatternTable
        (pWsscfgGetDot11HoppingPatternEntry,
         pWsscfgDot11HoppingPatternEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11HoppingPatternTable:"
                     "WsscfgGetAllUtlDot11HoppingPatternTable"
                     "Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11PhyERPTable
 Input       :  pWsscfgGetDot11PhyERPEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11PhyERPTable (tWsscfgDot11PhyERPEntry *
                              pWsscfgGetDot11PhyERPEntry)
{
    tWsscfgDot11PhyERPEntry *pWsscfgDot11PhyERPEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11PhyERPEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyERPTable,
                   (tRBElem *) pWsscfgGetDot11PhyERPEntry);

    if (pWsscfgDot11PhyERPEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11PhyERPTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetDot11PhyERPEntry->MibObject.
        i4Dot11ERPPBCCOptionImplemented =
        pWsscfgDot11PhyERPEntry->MibObject.i4Dot11ERPPBCCOptionImplemented;

    pWsscfgGetDot11PhyERPEntry->MibObject.i4Dot11ERPBCCOptionEnabled =
        pWsscfgDot11PhyERPEntry->MibObject.i4Dot11ERPBCCOptionEnabled;

    pWsscfgGetDot11PhyERPEntry->
        MibObject.i4Dot11DSSSOFDMOptionImplemented =
        pWsscfgDot11PhyERPEntry->MibObject.i4Dot11DSSSOFDMOptionImplemented;

    pWsscfgGetDot11PhyERPEntry->MibObject.
        i4Dot11DSSSOFDMOptionEnabled =
        pWsscfgDot11PhyERPEntry->MibObject.i4Dot11DSSSOFDMOptionEnabled;

    pWsscfgGetDot11PhyERPEntry->
        MibObject.i4Dot11ShortSlotTimeOptionImplemented =
        pWsscfgDot11PhyERPEntry->
        MibObject.i4Dot11ShortSlotTimeOptionImplemented;

    pWsscfgGetDot11PhyERPEntry->
        MibObject.i4Dot11ShortSlotTimeOptionEnabled =
        pWsscfgDot11PhyERPEntry->MibObject.i4Dot11ShortSlotTimeOptionEnabled;

    if (WsscfgGetAllUtlDot11PhyERPTable
        (pWsscfgGetDot11PhyERPEntry, pWsscfgDot11PhyERPEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllDot11PhyERPTable:"
                     "WsscfgGetAllUtlDot11PhyERPTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11RSNAConfigTable
 Input       :  pWsscfgGetDot11RSNAConfigEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11RSNAConfigTable (tWsscfgDot11RSNAConfigEntry *
                                  pWsscfgGetDot11RSNAConfigEntry)
{
    tWsscfgDot11RSNAConfigEntry *pWsscfgDot11RSNAConfigEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11RSNAConfigEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11RSNAConfigTable,
                   (tRBElem *) pWsscfgGetDot11RSNAConfigEntry);

    if (pWsscfgDot11RSNAConfigEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11RSNAConfigTable: "
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetDot11RSNAConfigEntry->MibObject.
        i4Dot11RSNAConfigVersion =
        pWsscfgDot11RSNAConfigEntry->MibObject.i4Dot11RSNAConfigVersion;

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNAConfigPairwiseKeysSupported =
        pWsscfgDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNAConfigPairwiseKeysSupported;

    MEMCPY (pWsscfgGetDot11RSNAConfigEntry->
            MibObject.au1Dot11RSNAConfigGroupCipher,
            pWsscfgDot11RSNAConfigEntry->
            MibObject.au1Dot11RSNAConfigGroupCipher,
            pWsscfgDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAConfigGroupCipherLen);

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAConfigGroupCipherLen =
        pWsscfgDot11RSNAConfigEntry->MibObject.i4Dot11RSNAConfigGroupCipherLen;

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAConfigGroupRekeyMethod =
        pWsscfgDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAConfigGroupRekeyMethod;

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNAConfigGroupRekeyTime =
        pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNAConfigGroupRekeyTime;

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNAConfigGroupRekeyPackets =
        pWsscfgDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNAConfigGroupRekeyPackets;

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAConfigGroupRekeyStrict =
        pWsscfgDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAConfigGroupRekeyStrict;

    MEMCPY (pWsscfgGetDot11RSNAConfigEntry->
            MibObject.au1Dot11RSNAConfigPSKValue,
            pWsscfgDot11RSNAConfigEntry->
            MibObject.au1Dot11RSNAConfigPSKValue,
            pWsscfgDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAConfigPSKValueLen);

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAConfigPSKValueLen =
        pWsscfgDot11RSNAConfigEntry->MibObject.i4Dot11RSNAConfigPSKValueLen;

    MEMCPY (pWsscfgGetDot11RSNAConfigEntry->
            MibObject.au1Dot11RSNAConfigPSKPassPhrase,
            pWsscfgDot11RSNAConfigEntry->
            MibObject.au1Dot11RSNAConfigPSKPassPhrase,
            pWsscfgDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAConfigPSKPassPhraseLen);

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAConfigPSKPassPhraseLen =
        pWsscfgDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAConfigPSKPassPhraseLen;

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNAConfigGroupUpdateCount =
        pWsscfgDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNAConfigGroupUpdateCount;

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNAConfigPairwiseUpdateCount =
        pWsscfgDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNAConfigPairwiseUpdateCount;

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNAConfigGroupCipherSize =
        pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNAConfigGroupCipherSize;

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNAConfigPMKLifetime =
        pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNAConfigPMKLifetime;

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNAConfigPMKReauthThreshold =
        pWsscfgDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNAConfigPMKReauthThreshold;

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAConfigNumberOfPTKSAReplayCounters =
        pWsscfgDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAConfigNumberOfPTKSAReplayCounters;

    pWsscfgGetDot11RSNAConfigEntry->MibObject.
        u4Dot11RSNAConfigSATimeout =
        pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNAConfigSATimeout;

    MEMCPY (pWsscfgGetDot11RSNAConfigEntry->
            MibObject.au1Dot11RSNAAuthenticationSuiteSelected,
            pWsscfgDot11RSNAConfigEntry->
            MibObject.au1Dot11RSNAAuthenticationSuiteSelected,
            pWsscfgDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAAuthenticationSuiteSelectedLen);

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAAuthenticationSuiteSelectedLen =
        pWsscfgDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAAuthenticationSuiteSelectedLen;

    MEMCPY (pWsscfgGetDot11RSNAConfigEntry->
            MibObject.au1Dot11RSNAPairwiseCipherSelected,
            pWsscfgDot11RSNAConfigEntry->
            MibObject.au1Dot11RSNAPairwiseCipherSelected,
            pWsscfgDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAPairwiseCipherSelectedLen);

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAPairwiseCipherSelectedLen =
        pWsscfgDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAPairwiseCipherSelectedLen;

    MEMCPY (pWsscfgGetDot11RSNAConfigEntry->
            MibObject.au1Dot11RSNAGroupCipherSelected,
            pWsscfgDot11RSNAConfigEntry->
            MibObject.au1Dot11RSNAGroupCipherSelected,
            pWsscfgDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAGroupCipherSelectedLen);

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAGroupCipherSelectedLen =
        pWsscfgDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAGroupCipherSelectedLen;

    MEMCPY (pWsscfgGetDot11RSNAConfigEntry->MibObject.
            au1Dot11RSNAPMKIDUsed,
            pWsscfgDot11RSNAConfigEntry->MibObject.
            au1Dot11RSNAPMKIDUsed,
            pWsscfgDot11RSNAConfigEntry->MibObject.i4Dot11RSNAPMKIDUsedLen);

    pWsscfgGetDot11RSNAConfigEntry->MibObject.
        i4Dot11RSNAPMKIDUsedLen =
        pWsscfgDot11RSNAConfigEntry->MibObject.i4Dot11RSNAPMKIDUsedLen;

    MEMCPY (pWsscfgGetDot11RSNAConfigEntry->
            MibObject.au1Dot11RSNAAuthenticationSuiteRequested,
            pWsscfgDot11RSNAConfigEntry->
            MibObject.au1Dot11RSNAAuthenticationSuiteRequested,
            pWsscfgDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAAuthenticationSuiteRequestedLen);

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAAuthenticationSuiteRequestedLen =
        pWsscfgDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAAuthenticationSuiteRequestedLen;

    MEMCPY (pWsscfgGetDot11RSNAConfigEntry->
            MibObject.au1Dot11RSNAPairwiseCipherRequested,
            pWsscfgDot11RSNAConfigEntry->
            MibObject.au1Dot11RSNAPairwiseCipherRequested,
            pWsscfgDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAPairwiseCipherRequestedLen);

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAPairwiseCipherRequestedLen =
        pWsscfgDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAPairwiseCipherRequestedLen;

    MEMCPY (pWsscfgGetDot11RSNAConfigEntry->
            MibObject.au1Dot11RSNAGroupCipherRequested,
            pWsscfgDot11RSNAConfigEntry->
            MibObject.au1Dot11RSNAGroupCipherRequested,
            pWsscfgDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAGroupCipherRequestedLen);

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAGroupCipherRequestedLen =
        pWsscfgDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAGroupCipherRequestedLen;

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNATKIPCounterMeasuresInvoked =
        pWsscfgDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNATKIPCounterMeasuresInvoked;

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNA4WayHandshakeFailures =
        pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNA4WayHandshakeFailures;

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAConfigNumberOfGTKSAReplayCounters =
        pWsscfgDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAConfigNumberOfGTKSAReplayCounters;

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNAConfigSTKKeysSupported =
        pWsscfgDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNAConfigSTKKeysSupported;

    MEMCPY (pWsscfgGetDot11RSNAConfigEntry->
            MibObject.au1Dot11RSNAConfigSTKCipher,
            pWsscfgDot11RSNAConfigEntry->
            MibObject.au1Dot11RSNAConfigSTKCipher,
            pWsscfgDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAConfigSTKCipherLen);

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAConfigSTKCipherLen =
        pWsscfgDot11RSNAConfigEntry->MibObject.i4Dot11RSNAConfigSTKCipherLen;

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNAConfigSTKRekeyTime =
        pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNAConfigSTKRekeyTime;

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNAConfigSMKUpdateCount =
        pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNAConfigSMKUpdateCount;

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNAConfigSTKCipherSize =
        pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNAConfigSTKCipherSize;

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNAConfigSMKLifetime =
        pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNAConfigSMKLifetime;

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNAConfigSMKReauthThreshold =
        pWsscfgDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNAConfigSMKReauthThreshold;

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAConfigNumberOfSTKSAReplayCounters =
        pWsscfgDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAConfigNumberOfSTKSAReplayCounters;

    MEMCPY (pWsscfgGetDot11RSNAConfigEntry->
            MibObject.au1Dot11RSNAPairwiseSTKSelected,
            pWsscfgDot11RSNAConfigEntry->
            MibObject.au1Dot11RSNAPairwiseSTKSelected,
            pWsscfgDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAPairwiseSTKSelectedLen);

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAPairwiseSTKSelectedLen =
        pWsscfgDot11RSNAConfigEntry->
        MibObject.i4Dot11RSNAPairwiseSTKSelectedLen;

    pWsscfgGetDot11RSNAConfigEntry->
        MibObject.u4Dot11RSNASMKHandshakeFailures =
        pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNASMKHandshakeFailures;

    if (WsscfgGetAllUtlDot11RSNAConfigTable
        (pWsscfgGetDot11RSNAConfigEntry,
         pWsscfgDot11RSNAConfigEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11RSNAConfigTable:"
                     "WsscfgGetAllUtlDot11RSNAConfigTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11RSNAConfigPairwiseCiphersTable
 Input       :  pWsscfgGetDot11RSNAConfigPairwiseCiphersEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllDot11RSNAConfigPairwiseCiphersTable
    (tWsscfgDot11RSNAConfigPairwiseCiphersEntry *
     pWsscfgGetDot11RSNAConfigPairwiseCiphersEntry)
{
    tWsscfgDot11RSNAConfigPairwiseCiphersEntry
        * pWsscfgDot11RSNAConfigPairwiseCiphersEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11RSNAConfigPairwiseCiphersEntry =
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.Dot11RSNAConfigPairwiseCiphersTable,
                   (tRBElem *) pWsscfgGetDot11RSNAConfigPairwiseCiphersEntry);

    if (pWsscfgDot11RSNAConfigPairwiseCiphersEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11RSNAConfigPairwiseCiphersTable:"
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pWsscfgGetDot11RSNAConfigPairwiseCiphersEntry->
            MibObject.au1Dot11RSNAConfigPairwiseCipher,
            pWsscfgDot11RSNAConfigPairwiseCiphersEntry->
            MibObject.au1Dot11RSNAConfigPairwiseCipher,
            pWsscfgDot11RSNAConfigPairwiseCiphersEntry->
            MibObject.i4Dot11RSNAConfigPairwiseCipherLen);

    pWsscfgGetDot11RSNAConfigPairwiseCiphersEntry->
        MibObject.i4Dot11RSNAConfigPairwiseCipherLen =
        pWsscfgDot11RSNAConfigPairwiseCiphersEntry->
        MibObject.i4Dot11RSNAConfigPairwiseCipherLen;

    pWsscfgGetDot11RSNAConfigPairwiseCiphersEntry->
        MibObject.i4Dot11RSNAConfigPairwiseCipherEnabled =
        pWsscfgDot11RSNAConfigPairwiseCiphersEntry->
        MibObject.i4Dot11RSNAConfigPairwiseCipherEnabled;

    pWsscfgGetDot11RSNAConfigPairwiseCiphersEntry->
        MibObject.u4Dot11RSNAConfigPairwiseCipherSize =
        pWsscfgDot11RSNAConfigPairwiseCiphersEntry->
        MibObject.u4Dot11RSNAConfigPairwiseCipherSize;

    if (WsscfgGetAllUtlDot11RSNAConfigPairwiseCiphersTable
        (pWsscfgGetDot11RSNAConfigPairwiseCiphersEntry,
         pWsscfgDot11RSNAConfigPairwiseCiphersEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11RSNAConfigPairwiseCiphersTable:"
                     "WsscfgGetAllUtlDot11RSNAConfigPairwiseCiphersTable"
                     "Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11RSNAConfigAuthenticationSuitesTable
 Input       :  pWsscfgGetDot11RSNAConfigAuthenticationSuitesEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllDot11RSNAConfigAuthenticationSuitesTable
    (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *
     pWsscfgGetDot11RSNAConfigAuthenticationSuitesEntry)
{
    tWsscfgDot11RSNAConfigAuthenticationSuitesEntry
        * pWsscfgDot11RSNAConfigAuthenticationSuitesEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11RSNAConfigAuthenticationSuitesEntry =
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.Dot11RSNAConfigAuthenticationSuitesTable,
                   (tRBElem *)
                   pWsscfgGetDot11RSNAConfigAuthenticationSuitesEntry);

    if (pWsscfgDot11RSNAConfigAuthenticationSuitesEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11RSNAConfigAuthenticationSuitesTable: Entry"
                     "doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pWsscfgGetDot11RSNAConfigAuthenticationSuitesEntry->
            MibObject.au1Dot11RSNAConfigAuthenticationSuite,
            pWsscfgDot11RSNAConfigAuthenticationSuitesEntry->
            MibObject.au1Dot11RSNAConfigAuthenticationSuite,
            pWsscfgDot11RSNAConfigAuthenticationSuitesEntry->
            MibObject.i4Dot11RSNAConfigAuthenticationSuiteLen);

    pWsscfgGetDot11RSNAConfigAuthenticationSuitesEntry->
        MibObject.i4Dot11RSNAConfigAuthenticationSuiteLen =
        pWsscfgDot11RSNAConfigAuthenticationSuitesEntry->
        MibObject.i4Dot11RSNAConfigAuthenticationSuiteLen;

    pWsscfgGetDot11RSNAConfigAuthenticationSuitesEntry->
        MibObject.i4Dot11RSNAConfigAuthenticationSuiteEnabled =
        pWsscfgDot11RSNAConfigAuthenticationSuitesEntry->
        MibObject.i4Dot11RSNAConfigAuthenticationSuiteEnabled;

    if (WsscfgGetAllUtlDot11RSNAConfigAuthenticationSuitesTable
        (pWsscfgGetDot11RSNAConfigAuthenticationSuitesEntry,
         pWsscfgDot11RSNAConfigAuthenticationSuitesEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11RSNAConfigAuthenticationSuitesTable:"
                     "WsscfgGetAllUtlDot11RSNAConfigAuthenticationSuitesTable"
                     "Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllDot11RSNAStatsTable
 Input       :  pWsscfgGetDot11RSNAStatsEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllDot11RSNAStatsTable (tWsscfgDot11RSNAStatsEntry *
                                 pWsscfgGetDot11RSNAStatsEntry)
{
    tWsscfgDot11RSNAStatsEntry *pWsscfgDot11RSNAStatsEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgDot11RSNAStatsEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11RSNAStatsTable,
                   (tRBElem *) pWsscfgGetDot11RSNAStatsEntry);

    if (pWsscfgDot11RSNAStatsEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11RSNAStatsTable:"
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (&
            (pWsscfgGetDot11RSNAStatsEntry->
             MibObject.Dot11RSNAStatsSTAAddress),
            &(pWsscfgDot11RSNAStatsEntry->
              MibObject.Dot11RSNAStatsSTAAddress), 6);

    pWsscfgGetDot11RSNAStatsEntry->MibObject.u4Dot11RSNAStatsVersion =
        pWsscfgDot11RSNAStatsEntry->MibObject.u4Dot11RSNAStatsVersion;

    MEMCPY (pWsscfgGetDot11RSNAStatsEntry->
            MibObject.au1Dot11RSNAStatsSelectedPairwiseCipher,
            pWsscfgDot11RSNAStatsEntry->
            MibObject.au1Dot11RSNAStatsSelectedPairwiseCipher,
            pWsscfgDot11RSNAStatsEntry->
            MibObject.i4Dot11RSNAStatsSelectedPairwiseCipherLen);

    pWsscfgGetDot11RSNAStatsEntry->
        MibObject.i4Dot11RSNAStatsSelectedPairwiseCipherLen =
        pWsscfgDot11RSNAStatsEntry->
        MibObject.i4Dot11RSNAStatsSelectedPairwiseCipherLen;

    pWsscfgGetDot11RSNAStatsEntry->
        MibObject.u4Dot11RSNAStatsTKIPICVErrors =
        pWsscfgDot11RSNAStatsEntry->MibObject.u4Dot11RSNAStatsTKIPICVErrors;

    pWsscfgGetDot11RSNAStatsEntry->
        MibObject.u4Dot11RSNAStatsTKIPLocalMICFailures =
        pWsscfgDot11RSNAStatsEntry->
        MibObject.u4Dot11RSNAStatsTKIPLocalMICFailures;

    pWsscfgGetDot11RSNAStatsEntry->
        MibObject.u4Dot11RSNAStatsTKIPRemoteMICFailures =
        pWsscfgDot11RSNAStatsEntry->
        MibObject.u4Dot11RSNAStatsTKIPRemoteMICFailures;

    pWsscfgGetDot11RSNAStatsEntry->MibObject.
        u4Dot11RSNAStatsCCMPReplays =
        pWsscfgDot11RSNAStatsEntry->MibObject.u4Dot11RSNAStatsCCMPReplays;

    pWsscfgGetDot11RSNAStatsEntry->
        MibObject.u4Dot11RSNAStatsCCMPDecryptErrors =
        pWsscfgDot11RSNAStatsEntry->MibObject.u4Dot11RSNAStatsCCMPDecryptErrors;

    pWsscfgGetDot11RSNAStatsEntry->MibObject.
        u4Dot11RSNAStatsTKIPReplays =
        pWsscfgDot11RSNAStatsEntry->MibObject.u4Dot11RSNAStatsTKIPReplays;

    if (WsscfgGetAllUtlDot11RSNAStatsTable
        (pWsscfgGetDot11RSNAStatsEntry,
         pWsscfgDot11RSNAStatsEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllDot11RSNAStatsTable:"
                     "WsscfgGetAllUtlDot11RSNAStatsTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11StationConfigTable
 Input       :  pWsscfgSetDot11StationConfigEntry
                pWsscfgIsSetDot11StationConfigEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllDot11StationConfigTable (tWsscfgDot11StationConfigEntry *
                                     pWsscfgSetDot11StationConfigEntry,
                                     tWsscfgIsSetDot11StationConfigEntry *
                                     pWsscfgIsSetDot11StationConfigEntry)
{
    tWsscfgDot11StationConfigEntry WsscfgOldDot11StationConfigEntry;

    MEMSET (&WsscfgOldDot11StationConfigEntry, 0,
            sizeof (tWsscfgDot11StationConfigEntry));
    if (WsscfgUtilUpdateDot11StationConfigTable
        (&WsscfgOldDot11StationConfigEntry,
         pWsscfgSetDot11StationConfigEntry,
         pWsscfgIsSetDot11StationConfigEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11AuthenticationAlgorithmsTable
 Input       :  pWsscfgSetDot11AuthenticationAlgorithmsEntry
                pWsscfgIsSetDot11AuthenticationAlgorithmsEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgSetAllDot11AuthenticationAlgorithmsTable
    (tWsscfgDot11AuthenticationAlgorithmsEntry *
     pWsscfgSetDot11AuthenticationAlgorithmsEntry,
     tWsscfgIsSetDot11AuthenticationAlgorithmsEntry *
     pWsscfgIsSetDot11AuthenticationAlgorithmsEntry)
{

    tWsscfgFsDot11WlanAuthenticationProfileEntry
        WsscfgSetFsDot11WlanAuthenticationProfileEntry;

    tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry
        WsscfgIsSetFsDot11WlanAuthenticationProfileEntry;

    MEMSET (&WsscfgSetFsDot11WlanAuthenticationProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanAuthenticationProfileEntry));

    MEMSET (&WsscfgIsSetFsDot11WlanAuthenticationProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry));

    WsscfgSetFsDot11WlanAuthenticationProfileEntry.
        MibObject.i4FsDot11WlanAuthenticationAlgorithm =
        pWsscfgSetDot11AuthenticationAlgorithmsEntry->
        MibObject.i4Dot11AuthenticationAlgorithm;

    WsscfgIsSetFsDot11WlanAuthenticationProfileEntry.
        bFsDot11WlanAuthenticationAlgorithm = OSIX_TRUE;

    WsscfgSetFsDot11WlanAuthenticationProfileEntry.MibObject.
        i4IfIndex =
        pWsscfgSetDot11AuthenticationAlgorithmsEntry->MibObject.i4IfIndex;

    WsscfgIsSetFsDot11WlanAuthenticationProfileEntry.bIfIndex = OSIX_TRUE;

    /* To clean up key configurations if authentication is open */
    if (WsscfgSetFsDot11WlanAuthenticationProfileEntry.
        MibObject.i4FsDot11WlanAuthenticationAlgorithm == CLI_AUTH_ALGO_OPEN)
    {
        WsscfgSetFsDot11WlanAuthenticationProfileEntry.
            MibObject.i4FsDot11WlanWepKeyIndex = 0;
        WsscfgIsSetFsDot11WlanAuthenticationProfileEntry.bFsDot11WlanWepKeyIndex
            = OSIX_TRUE;

        WsscfgSetFsDot11WlanAuthenticationProfileEntry.
            MibObject.i4FsDot11WlanWepKeyType = 0;
        WsscfgIsSetFsDot11WlanAuthenticationProfileEntry.bFsDot11WlanWepKeyType
            = OSIX_TRUE;

        WsscfgSetFsDot11WlanAuthenticationProfileEntry.
            MibObject.i4FsDot11WlanWepKeyLength = 0;
        WsscfgIsSetFsDot11WlanAuthenticationProfileEntry.
            bFsDot11WlanWepKeyLength = OSIX_TRUE;

        WsscfgSetFsDot11WlanAuthenticationProfileEntry.
            MibObject.i4FsDot11WlanWepKeyLen =
            sizeof
            (WsscfgSetFsDot11WlanAuthenticationProfileEntry.MibObject.
             au1FsDot11WlanWepKey);

        if (WsscfgSetFsDot11WlanAuthenticationProfileEntry.
            MibObject.i4FsDot11WlanWepKeyLen != 0)
        {
            MEMSET
                (WsscfgSetFsDot11WlanAuthenticationProfileEntry.MibObject.
                 au1FsDot11WlanWepKey, 0,
                 WsscfgSetFsDot11WlanAuthenticationProfileEntry.MibObject.
                 i4FsDot11WlanWepKeyLen);

            WsscfgIsSetFsDot11WlanAuthenticationProfileEntry.bFsDot11WlanWepKey
                = OSIX_TRUE;
        }

    }
    if (WsscfgSetAllFsDot11WlanAuthenticationProfileTable
        (&WsscfgSetFsDot11WlanAuthenticationProfileEntry,
         &WsscfgIsSetFsDot11WlanAuthenticationProfileEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (pWsscfgIsSetDot11AuthenticationAlgorithmsEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11WEPDefaultKeysTable
 Input       :  pWsscfgSetDot11WEPDefaultKeysEntry
                pWsscfgIsSetDot11WEPDefaultKeysEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllDot11WEPDefaultKeysTable (tWsscfgDot11WEPDefaultKeysEntry *
                                      pWsscfgSetDot11WEPDefaultKeysEntry,
                                      tWsscfgIsSetDot11WEPDefaultKeysEntry *
                                      pWsscfgIsSetDot11WEPDefaultKeysEntry)
{
    tWsscfgDot11WEPDefaultKeysEntry *pWsscfgDot11WEPDefaultKeysEntry = NULL;
    tWsscfgDot11WEPDefaultKeysEntry WsscfgOldDot11WEPDefaultKeysEntry;

    MEMSET (&WsscfgOldDot11WEPDefaultKeysEntry, 0,
            sizeof (tWsscfgDot11WEPDefaultKeysEntry));

    /* Check whether the node is already present */
    pWsscfgDot11WEPDefaultKeysEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.
                   Dot11WEPDefaultKeysTable,
                   (tRBElem *) pWsscfgSetDot11WEPDefaultKeysEntry);

    if (pWsscfgDot11WEPDefaultKeysEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllDot11WEPDefaultKeysTable:"
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /*Function to check whether the given input is same as there in database */
    if (Dot11WEPDefaultKeysTableFilterInputs
        (pWsscfgDot11WEPDefaultKeysEntry,
         pWsscfgSetDot11WEPDefaultKeysEntry,
         pWsscfgIsSetDot11WEPDefaultKeysEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&WsscfgOldDot11WEPDefaultKeysEntry,
            pWsscfgDot11WEPDefaultKeysEntry,
            sizeof (tWsscfgDot11WEPDefaultKeysEntry));

    /* Assign values for the existing node */
    if (pWsscfgIsSetDot11WEPDefaultKeysEntry->
        bDot11WEPDefaultKeyValue != OSIX_FALSE)
    {
        MEMCPY (pWsscfgDot11WEPDefaultKeysEntry->
                MibObject.au1Dot11WEPDefaultKeyValue,
                pWsscfgSetDot11WEPDefaultKeysEntry->
                MibObject.au1Dot11WEPDefaultKeyValue,
                pWsscfgSetDot11WEPDefaultKeysEntry->
                MibObject.i4Dot11WEPDefaultKeyValueLen);

        pWsscfgDot11WEPDefaultKeysEntry->
            MibObject.i4Dot11WEPDefaultKeyValueLen =
            pWsscfgSetDot11WEPDefaultKeysEntry->
            MibObject.i4Dot11WEPDefaultKeyValueLen;
    }

    if (WsscfgUtilUpdateDot11WEPDefaultKeysTable
        (&WsscfgOldDot11WEPDefaultKeysEntry,
         pWsscfgDot11WEPDefaultKeysEntry,
         pWsscfgIsSetDot11WEPDefaultKeysEntry) != OSIX_SUCCESS)
    {
        if (WsscfgSetAllDot11WEPDefaultKeysTableTrigger
            (pWsscfgSetDot11WEPDefaultKeysEntry,
             pWsscfgIsSetDot11WEPDefaultKeysEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pWsscfgDot11WEPDefaultKeysEntry,
                &WsscfgOldDot11WEPDefaultKeysEntry,
                sizeof (tWsscfgDot11WEPDefaultKeysEntry));
        return OSIX_FAILURE;
    }

    if (WsscfgSetAllDot11WEPDefaultKeysTableTrigger
        (pWsscfgSetDot11WEPDefaultKeysEntry,
         pWsscfgIsSetDot11WEPDefaultKeysEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11WEPKeyMappingsTable
 Input       :  pWsscfgSetDot11WEPKeyMappingsEntry
                pWsscfgIsSetDot11WEPKeyMappingsEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllDot11WEPKeyMappingsTable (tWsscfgDot11WEPKeyMappingsEntry *
                                      pWsscfgSetDot11WEPKeyMappingsEntry,
                                      tWsscfgIsSetDot11WEPKeyMappingsEntry *
                                      pWsscfgIsSetDot11WEPKeyMappingsEntry,
                                      INT4 i4RowStatusLogic,
                                      INT4 i4RowCreateOption)
{
    tWsscfgDot11WEPKeyMappingsEntry *pWsscfgDot11WEPKeyMappingsEntry = NULL;
    tWsscfgDot11WEPKeyMappingsEntry *pWsscfgOldDot11WEPKeyMappingsEntry = NULL;
    tWsscfgDot11WEPKeyMappingsEntry *pWsscfgTrgDot11WEPKeyMappingsEntry = NULL;
    tWsscfgIsSetDot11WEPKeyMappingsEntry
        * pWsscfgTrgIsSetDot11WEPKeyMappingsEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pWsscfgOldDot11WEPKeyMappingsEntry =
        (tWsscfgDot11WEPKeyMappingsEntry *)
        MemAllocMemBlk (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID);
    if (pWsscfgOldDot11WEPKeyMappingsEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWsscfgTrgDot11WEPKeyMappingsEntry =
        (tWsscfgDot11WEPKeyMappingsEntry *)
        MemAllocMemBlk (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID);
    if (pWsscfgTrgDot11WEPKeyMappingsEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                            (UINT1 *) pWsscfgOldDot11WEPKeyMappingsEntry);
        return OSIX_FAILURE;
    }
    pWsscfgTrgIsSetDot11WEPKeyMappingsEntry =
        (tWsscfgIsSetDot11WEPKeyMappingsEntry *)
        MemAllocMemBlk (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_ISSET_POOLID);
    if (pWsscfgTrgIsSetDot11WEPKeyMappingsEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                            (UINT1 *) pWsscfgOldDot11WEPKeyMappingsEntry);
        MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgDot11WEPKeyMappingsEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pWsscfgOldDot11WEPKeyMappingsEntry, 0,
            sizeof (tWsscfgDot11WEPKeyMappingsEntry));
    MEMSET (pWsscfgTrgDot11WEPKeyMappingsEntry, 0,
            sizeof (tWsscfgDot11WEPKeyMappingsEntry));
    MEMSET (pWsscfgTrgIsSetDot11WEPKeyMappingsEntry, 0,
            sizeof (tWsscfgIsSetDot11WEPKeyMappingsEntry));

    /* Check whether the node is already present */
    pWsscfgDot11WEPKeyMappingsEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.
                   Dot11WEPKeyMappingsTable,
                   (tRBElem *) pWsscfgSetDot11WEPKeyMappingsEntry);

    if (pWsscfgDot11WEPKeyMappingsEntry == NULL)
    {
        /* Create the node if the RowStatus given is 
         * CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pWsscfgSetDot11WEPKeyMappingsEntry->
             MibObject.i4Dot11WEPKeyMappingStatus == CREATE_AND_WAIT)
            || (pWsscfgSetDot11WEPKeyMappingsEntry->
                MibObject.i4Dot11WEPKeyMappingStatus == CREATE_AND_GO)
            ||
            ((pWsscfgSetDot11WEPKeyMappingsEntry->
              MibObject.i4Dot11WEPKeyMappingStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pWsscfgDot11WEPKeyMappingsEntry =
                (tWsscfgDot11WEPKeyMappingsEntry *)
                MemAllocMemBlk (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID);
            if (pWsscfgDot11WEPKeyMappingsEntry == NULL)
            {
                if (WsscfgSetAllDot11WEPKeyMappingsTableTrigger
                    (pWsscfgSetDot11WEPKeyMappingsEntry,
                     pWsscfgIsSetDot11WEPKeyMappingsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllDot11WEPKeyMappingsTable:"
                                 "WsscfgSetAllDot11WEPKeyMappingsTableTrigger"
                                 "function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllDot11WEPKeyMappingsTable: Fail to"
                             "Allocate Memory.\r\n"));
                MemReleaseMemBlock
                    (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID, (UINT1 *)
                     pWsscfgOldDot11WEPKeyMappingsEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgDot11WEPKeyMappingsEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetDot11WEPKeyMappingsEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pWsscfgDot11WEPKeyMappingsEntry, 0,
                    sizeof (tWsscfgDot11WEPKeyMappingsEntry));
            if ((WsscfgInitializeDot11WEPKeyMappingsTable
                 (pWsscfgDot11WEPKeyMappingsEntry)) == OSIX_FAILURE)
            {
                if (WsscfgSetAllDot11WEPKeyMappingsTableTrigger
                    (pWsscfgSetDot11WEPKeyMappingsEntry,
                     pWsscfgIsSetDot11WEPKeyMappingsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllDot11WEPKeyMappingsTable:"
                                 "WsscfgSetAllDot11WEPKeyMappingsTableTrigger"
                                 "function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllDot11WEPKeyMappingsTable: Fail to"
                             "Initialize the Objects.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID, (UINT1 *)
                     pWsscfgDot11WEPKeyMappingsEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID, (UINT1 *)
                     pWsscfgOldDot11WEPKeyMappingsEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgDot11WEPKeyMappingsEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetDot11WEPKeyMappingsEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingIndex
                != OSIX_FALSE)
            {
                pWsscfgDot11WEPKeyMappingsEntry->
                    MibObject.i4Dot11WEPKeyMappingIndex =
                    pWsscfgSetDot11WEPKeyMappingsEntry->
                    MibObject.i4Dot11WEPKeyMappingIndex;
            }

            if (pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingAddress
                != OSIX_FALSE)
            {
                MEMCPY (&
                        (pWsscfgDot11WEPKeyMappingsEntry->
                         MibObject.Dot11WEPKeyMappingAddress),
                        &(pWsscfgSetDot11WEPKeyMappingsEntry->
                          MibObject.Dot11WEPKeyMappingAddress), 6);
            }

            if (pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingWEPOn
                != OSIX_FALSE)
            {
                pWsscfgDot11WEPKeyMappingsEntry->
                    MibObject.i4Dot11WEPKeyMappingWEPOn =
                    pWsscfgSetDot11WEPKeyMappingsEntry->
                    MibObject.i4Dot11WEPKeyMappingWEPOn;
            }

            if (pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingValue
                != OSIX_FALSE)
            {
                MEMCPY (pWsscfgDot11WEPKeyMappingsEntry->
                        MibObject.au1Dot11WEPKeyMappingValue,
                        pWsscfgSetDot11WEPKeyMappingsEntry->
                        MibObject.au1Dot11WEPKeyMappingValue,
                        pWsscfgSetDot11WEPKeyMappingsEntry->
                        MibObject.i4Dot11WEPKeyMappingValueLen);

                pWsscfgDot11WEPKeyMappingsEntry->
                    MibObject.i4Dot11WEPKeyMappingValueLen =
                    pWsscfgSetDot11WEPKeyMappingsEntry->
                    MibObject.i4Dot11WEPKeyMappingValueLen;
            }

            if (pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingStatus
                != OSIX_FALSE)
            {
                pWsscfgDot11WEPKeyMappingsEntry->
                    MibObject.i4Dot11WEPKeyMappingStatus =
                    pWsscfgSetDot11WEPKeyMappingsEntry->
                    MibObject.i4Dot11WEPKeyMappingStatus;
            }

            if (pWsscfgIsSetDot11WEPKeyMappingsEntry->bIfIndex != OSIX_FALSE)
            {
                pWsscfgDot11WEPKeyMappingsEntry->MibObject.i4IfIndex =
                    pWsscfgSetDot11WEPKeyMappingsEntry->MibObject.i4IfIndex;
            }

            if ((pWsscfgSetDot11WEPKeyMappingsEntry->
                 MibObject.i4Dot11WEPKeyMappingStatus ==
                 CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetDot11WEPKeyMappingsEntry->
                        MibObject.i4Dot11WEPKeyMappingStatus == ACTIVE)))
            {
                pWsscfgDot11WEPKeyMappingsEntry->
                    MibObject.i4Dot11WEPKeyMappingStatus = ACTIVE;
            }
            else if (pWsscfgSetDot11WEPKeyMappingsEntry->
                     MibObject.i4Dot11WEPKeyMappingStatus == CREATE_AND_WAIT)
            {
                pWsscfgDot11WEPKeyMappingsEntry->
                    MibObject.i4Dot11WEPKeyMappingStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gWsscfgGlobals.WsscfgGlbMib.Dot11WEPKeyMappingsTable,
                 (tRBElem *) pWsscfgDot11WEPKeyMappingsEntry) != RB_SUCCESS)
            {
                if (WsscfgSetAllDot11WEPKeyMappingsTableTrigger
                    (pWsscfgSetDot11WEPKeyMappingsEntry,
                     pWsscfgIsSetDot11WEPKeyMappingsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllDot11WEPKeyMappingsTable:"
                                 "WsscfgSetAllDot11WEPKeyMappingsTableTrigger"
                                 "function returns failure.\r\n"));
                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllDot11WEPKeyMappingsTable: "
                             "BTreeAdd is failed.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID, (UINT1 *)
                     pWsscfgDot11WEPKeyMappingsEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID, (UINT1 *)
                     pWsscfgOldDot11WEPKeyMappingsEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgDot11WEPKeyMappingsEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetDot11WEPKeyMappingsEntry);
                return OSIX_FAILURE;
            }
            if (WsscfgUtilUpdateDot11WEPKeyMappingsTable
                (NULL, pWsscfgDot11WEPKeyMappingsEntry,
                 pWsscfgIsSetDot11WEPKeyMappingsEntry) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllDot11WEPKeyMappingsTable:"
                             "WsscfgUtilUpdateDot11WEPKeyMappingsTable "
                             "function returns failure.\r\n"));

                if (WsscfgSetAllDot11WEPKeyMappingsTableTrigger
                    (pWsscfgSetDot11WEPKeyMappingsEntry,
                     pWsscfgIsSetDot11WEPKeyMappingsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllDot11WEPKeyMappingsTable: "
                                 "WsscfgSetAllDot11WEPKeyMappingsTableTrigger "
                                 "function returns failure.\r\n"));

                }
                RBTreeRem (gWsscfgGlobals.
                           WsscfgGlbMib.Dot11WEPKeyMappingsTable,
                           pWsscfgDot11WEPKeyMappingsEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID, (UINT1 *)
                     pWsscfgDot11WEPKeyMappingsEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID, (UINT1 *)
                     pWsscfgOldDot11WEPKeyMappingsEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgDot11WEPKeyMappingsEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetDot11WEPKeyMappingsEntry);
                return OSIX_FAILURE;
            }

            if ((pWsscfgSetDot11WEPKeyMappingsEntry->
                 MibObject.i4Dot11WEPKeyMappingStatus ==
                 CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetDot11WEPKeyMappingsEntry->
                        MibObject.i4Dot11WEPKeyMappingStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgDot11WEPKeyMappingsEntry->
                    MibObject.i4Dot11WEPKeyMappingIndex =
                    pWsscfgSetDot11WEPKeyMappingsEntry->
                    MibObject.i4Dot11WEPKeyMappingIndex;
                pWsscfgTrgDot11WEPKeyMappingsEntry->MibObject.
                    i4IfIndex =
                    pWsscfgSetDot11WEPKeyMappingsEntry->MibObject.i4IfIndex;
                pWsscfgTrgDot11WEPKeyMappingsEntry->
                    MibObject.i4Dot11WEPKeyMappingStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetDot11WEPKeyMappingsEntry->
                    bDot11WEPKeyMappingStatus = OSIX_TRUE;

                if (WsscfgSetAllDot11WEPKeyMappingsTableTrigger
                    (pWsscfgTrgDot11WEPKeyMappingsEntry,
                     pWsscfgTrgIsSetDot11WEPKeyMappingsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllDot11WEPKeyMappingsTable:"
                                 "WsscfgSetAllDot11WEPKeyMappingsTableTrigger"
                                 "function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                         (UINT1 *) pWsscfgDot11WEPKeyMappingsEntry);
                    MemReleaseMemBlock
                        (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                         (UINT1 *) pWsscfgOldDot11WEPKeyMappingsEntry);
                    MemReleaseMemBlock
                        (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                         (UINT1 *) pWsscfgTrgDot11WEPKeyMappingsEntry);
                    MemReleaseMemBlock
                        (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetDot11WEPKeyMappingsEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pWsscfgSetDot11WEPKeyMappingsEntry->
                     MibObject.i4Dot11WEPKeyMappingStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgDot11WEPKeyMappingsEntry->
                    MibObject.i4Dot11WEPKeyMappingStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetDot11WEPKeyMappingsEntry->
                    bDot11WEPKeyMappingStatus = OSIX_TRUE;

                if (WsscfgSetAllDot11WEPKeyMappingsTableTrigger
                    (pWsscfgTrgDot11WEPKeyMappingsEntry,
                     pWsscfgTrgIsSetDot11WEPKeyMappingsEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllDot11WEPKeyMappingsTable:"
                                 "WsscfgSetAllDot11WEPKeyMappingsTableTrigger "
                                 "function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                         (UINT1 *) pWsscfgDot11WEPKeyMappingsEntry);
                    MemReleaseMemBlock
                        (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                         (UINT1 *) pWsscfgOldDot11WEPKeyMappingsEntry);
                    MemReleaseMemBlock
                        (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                         (UINT1 *) pWsscfgTrgDot11WEPKeyMappingsEntry);
                    MemReleaseMemBlock
                        (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetDot11WEPKeyMappingsEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pWsscfgSetDot11WEPKeyMappingsEntry->
                MibObject.i4Dot11WEPKeyMappingStatus == CREATE_AND_GO)
            {
                pWsscfgSetDot11WEPKeyMappingsEntry->
                    MibObject.i4Dot11WEPKeyMappingStatus = ACTIVE;
            }

            if (WsscfgSetAllDot11WEPKeyMappingsTableTrigger
                (pWsscfgSetDot11WEPKeyMappingsEntry,
                 pWsscfgIsSetDot11WEPKeyMappingsEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllDot11WEPKeyMappingsTable:"
                             "WsscfgSetAllDot11WEPKeyMappingsTableTrigger"
                             "function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                                (UINT1 *) pWsscfgOldDot11WEPKeyMappingsEntry);
            MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgDot11WEPKeyMappingsEntry);
            MemReleaseMemBlock
                (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetDot11WEPKeyMappingsEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (WsscfgSetAllDot11WEPKeyMappingsTableTrigger
                (pWsscfgSetDot11WEPKeyMappingsEntry,
                 pWsscfgIsSetDot11WEPKeyMappingsEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllDot11WEPKeyMappingsTable:"
                             "WsscfgSetAllDot11WEPKeyMappingsTableTrigger"
                             "function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllDot11WEPKeyMappingsTable: Failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                                (UINT1 *) pWsscfgOldDot11WEPKeyMappingsEntry);
            MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgDot11WEPKeyMappingsEntry);
            MemReleaseMemBlock
                (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetDot11WEPKeyMappingsEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pWsscfgSetDot11WEPKeyMappingsEntry->
              MibObject.i4Dot11WEPKeyMappingStatus == CREATE_AND_WAIT)
             || (pWsscfgSetDot11WEPKeyMappingsEntry->
                 MibObject.i4Dot11WEPKeyMappingStatus == CREATE_AND_GO))
    {
        if (WsscfgSetAllDot11WEPKeyMappingsTableTrigger
            (pWsscfgSetDot11WEPKeyMappingsEntry,
             pWsscfgIsSetDot11WEPKeyMappingsEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllDot11WEPKeyMappingsTable:"
                         "WsscfgSetAllDot11WEPKeyMappingsTableTrigger function "
                         "returns failure.\r\n"));
        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllDot11WEPKeyMappingsTable: The row is already"
                     "present.\r\n"));
        MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                            (UINT1 *) pWsscfgOldDot11WEPKeyMappingsEntry);
        MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgDot11WEPKeyMappingsEntry);
        MemReleaseMemBlock
            (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetDot11WEPKeyMappingsEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pWsscfgOldDot11WEPKeyMappingsEntry,
            pWsscfgDot11WEPKeyMappingsEntry,
            sizeof (tWsscfgDot11WEPKeyMappingsEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pWsscfgSetDot11WEPKeyMappingsEntry->
        MibObject.i4Dot11WEPKeyMappingStatus == DESTROY)
    {
        pWsscfgDot11WEPKeyMappingsEntry->
            MibObject.i4Dot11WEPKeyMappingStatus = DESTROY;

        if (WsscfgUtilUpdateDot11WEPKeyMappingsTable
            (pWsscfgOldDot11WEPKeyMappingsEntry,
             pWsscfgDot11WEPKeyMappingsEntry,
             pWsscfgIsSetDot11WEPKeyMappingsEntry) != OSIX_SUCCESS)
        {

            if (WsscfgSetAllDot11WEPKeyMappingsTableTrigger
                (pWsscfgSetDot11WEPKeyMappingsEntry,
                 pWsscfgIsSetDot11WEPKeyMappingsEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllDot11WEPKeyMappingsTable:"
                             "WsscfgSetAllDot11WEPKeyMappingsTableTrigger"
                             "function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllDot11WEPKeyMappingsTable:"
                         "WsscfgUtilUpdateDot11WEPKeyMappingsTable"
                         "function returns failure.\r\n"));
        }
        RBTreeRem (gWsscfgGlobals.WsscfgGlbMib.
                   Dot11WEPKeyMappingsTable, pWsscfgDot11WEPKeyMappingsEntry);
        if (WsscfgSetAllDot11WEPKeyMappingsTableTrigger
            (pWsscfgSetDot11WEPKeyMappingsEntry,
             pWsscfgIsSetDot11WEPKeyMappingsEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllDot11WEPKeyMappingsTable:"
                         "WsscfgSetAllDot11WEPKeyMappingsTableTrigger"
                         "function returns failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                                (UINT1 *) pWsscfgOldDot11WEPKeyMappingsEntry);
            MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgDot11WEPKeyMappingsEntry);
            MemReleaseMemBlock
                (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetDot11WEPKeyMappingsEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                            (UINT1 *) pWsscfgDot11WEPKeyMappingsEntry);
        MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                            (UINT1 *) pWsscfgOldDot11WEPKeyMappingsEntry);
        MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgDot11WEPKeyMappingsEntry);
        MemReleaseMemBlock
            (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetDot11WEPKeyMappingsEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (Dot11WEPKeyMappingsTableFilterInputs
        (pWsscfgDot11WEPKeyMappingsEntry,
         pWsscfgSetDot11WEPKeyMappingsEntry,
         pWsscfgIsSetDot11WEPKeyMappingsEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                            (UINT1 *) pWsscfgOldDot11WEPKeyMappingsEntry);
        MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgDot11WEPKeyMappingsEntry);
        MemReleaseMemBlock
            (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetDot11WEPKeyMappingsEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before 
     * modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pWsscfgDot11WEPKeyMappingsEntry->
         MibObject.i4Dot11WEPKeyMappingStatus == ACTIVE)
        && (pWsscfgSetDot11WEPKeyMappingsEntry->
            MibObject.i4Dot11WEPKeyMappingStatus != NOT_IN_SERVICE))
    {
        pWsscfgDot11WEPKeyMappingsEntry->
            MibObject.i4Dot11WEPKeyMappingStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pWsscfgTrgDot11WEPKeyMappingsEntry->
            MibObject.i4Dot11WEPKeyMappingStatus = NOT_IN_SERVICE;
        pWsscfgTrgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingStatus
            = OSIX_TRUE;

        if (WsscfgUtilUpdateDot11WEPKeyMappingsTable
            (pWsscfgOldDot11WEPKeyMappingsEntry,
             pWsscfgDot11WEPKeyMappingsEntry,
             pWsscfgIsSetDot11WEPKeyMappingsEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pWsscfgDot11WEPKeyMappingsEntry,
                    pWsscfgOldDot11WEPKeyMappingsEntry,
                    sizeof (tWsscfgDot11WEPKeyMappingsEntry));
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllDot11WEPKeyMappingsTable:"
                         "WsscfgUtilUpdateDot11WEPKeyMappingsTable"
                         "Function returns failure.\r\n"));

            if (WsscfgSetAllDot11WEPKeyMappingsTableTrigger
                (pWsscfgSetDot11WEPKeyMappingsEntry,
                 pWsscfgIsSetDot11WEPKeyMappingsEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllDot11WEPKeyMappingsTable:"
                             "WsscfgSetAllDot11WEPKeyMappingsTableTrigger"
                             "function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                                (UINT1 *) pWsscfgOldDot11WEPKeyMappingsEntry);
            MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgDot11WEPKeyMappingsEntry);
            MemReleaseMemBlock
                (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetDot11WEPKeyMappingsEntry);
            return OSIX_FAILURE;
        }

        if (WsscfgSetAllDot11WEPKeyMappingsTableTrigger
            (pWsscfgTrgDot11WEPKeyMappingsEntry,
             pWsscfgTrgIsSetDot11WEPKeyMappingsEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllDot11WEPKeyMappingsTable:"
                         "WsscfgSetAllDot11WEPKeyMappingsTableTrigger"
                         "function returns failure.\r\n"));
        }
    }

    if (pWsscfgSetDot11WEPKeyMappingsEntry->
        MibObject.i4Dot11WEPKeyMappingStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pWsscfgIsSetDot11WEPKeyMappingsEntry->
        bDot11WEPKeyMappingAddress != OSIX_FALSE)
    {
        MEMCPY (&
                (pWsscfgDot11WEPKeyMappingsEntry->
                 MibObject.Dot11WEPKeyMappingAddress),
                &(pWsscfgSetDot11WEPKeyMappingsEntry->
                  MibObject.Dot11WEPKeyMappingAddress), 6);
    }
    if (pWsscfgIsSetDot11WEPKeyMappingsEntry->
        bDot11WEPKeyMappingWEPOn != OSIX_FALSE)
    {
        pWsscfgDot11WEPKeyMappingsEntry->
            MibObject.i4Dot11WEPKeyMappingWEPOn =
            pWsscfgSetDot11WEPKeyMappingsEntry->
            MibObject.i4Dot11WEPKeyMappingWEPOn;
    }
    if (pWsscfgIsSetDot11WEPKeyMappingsEntry->
        bDot11WEPKeyMappingValue != OSIX_FALSE)
    {
        MEMCPY (pWsscfgDot11WEPKeyMappingsEntry->
                MibObject.au1Dot11WEPKeyMappingValue,
                pWsscfgSetDot11WEPKeyMappingsEntry->
                MibObject.au1Dot11WEPKeyMappingValue,
                pWsscfgSetDot11WEPKeyMappingsEntry->
                MibObject.i4Dot11WEPKeyMappingValueLen);

        pWsscfgDot11WEPKeyMappingsEntry->
            MibObject.i4Dot11WEPKeyMappingValueLen =
            pWsscfgSetDot11WEPKeyMappingsEntry->
            MibObject.i4Dot11WEPKeyMappingValueLen;
    }
    if (pWsscfgIsSetDot11WEPKeyMappingsEntry->
        bDot11WEPKeyMappingStatus != OSIX_FALSE)
    {
        pWsscfgDot11WEPKeyMappingsEntry->
            MibObject.i4Dot11WEPKeyMappingStatus =
            pWsscfgSetDot11WEPKeyMappingsEntry->
            MibObject.i4Dot11WEPKeyMappingStatus;
    }

    /*This condtion is to make the row back to ACTIVE after 
     * modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pWsscfgDot11WEPKeyMappingsEntry->
            MibObject.i4Dot11WEPKeyMappingStatus = ACTIVE;
    }

    if (WsscfgUtilUpdateDot11WEPKeyMappingsTable
        (pWsscfgOldDot11WEPKeyMappingsEntry,
         pWsscfgDot11WEPKeyMappingsEntry,
         pWsscfgIsSetDot11WEPKeyMappingsEntry) != OSIX_SUCCESS)
    {

        if (WsscfgSetAllDot11WEPKeyMappingsTableTrigger
            (pWsscfgSetDot11WEPKeyMappingsEntry,
             pWsscfgIsSetDot11WEPKeyMappingsEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllDot11WEPKeyMappingsTable:"
                         "WsscfgSetAllDot11WEPKeyMappingsTableTrigger"
                         "function returns failure.\r\n"));

        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllDot11WEPKeyMappingsTable:"
                     "WsscfgUtilUpdateDot11WEPKeyMappingsTable"
                     "function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pWsscfgDot11WEPKeyMappingsEntry,
                pWsscfgOldDot11WEPKeyMappingsEntry,
                sizeof (tWsscfgDot11WEPKeyMappingsEntry));
        MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                            (UINT1 *) pWsscfgOldDot11WEPKeyMappingsEntry);
        MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgDot11WEPKeyMappingsEntry);
        MemReleaseMemBlock
            (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetDot11WEPKeyMappingsEntry);
        return OSIX_FAILURE;

    }
    if (WsscfgSetAllDot11WEPKeyMappingsTableTrigger
        (pWsscfgSetDot11WEPKeyMappingsEntry,
         pWsscfgIsSetDot11WEPKeyMappingsEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllDot11WEPKeyMappingsTable:"
                     "WsscfgSetAllDot11WEPKeyMappingsTableTrigger"
                     "function returns failure.\r\n"));
    }

    MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                        (UINT1 *) pWsscfgOldDot11WEPKeyMappingsEntry);
    MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID,
                        (UINT1 *) pWsscfgTrgDot11WEPKeyMappingsEntry);
    MemReleaseMemBlock (WSSCFG_DOT11WEPKEYMAPPINGSTABLE_ISSET_POOLID,
                        (UINT1 *) pWsscfgTrgIsSetDot11WEPKeyMappingsEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgSetAllDot11PrivacyTable
 Input       :  pWsscfgSetDot11PrivacyEntry
                pWsscfgIsSetDot11PrivacyEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllDot11PrivacyTable (tWsscfgDot11PrivacyEntry *
                               pWsscfgSetDot11PrivacyEntry,
                               tWsscfgIsSetDot11PrivacyEntry *
                               pWsscfgIsSetDot11PrivacyEntry)
{
    tWsscfgDot11PrivacyEntry *pWsscfgDot11PrivacyEntry = NULL;
    tWsscfgDot11PrivacyEntry WsscfgOldDot11PrivacyEntry;

    MEMSET (&WsscfgOldDot11PrivacyEntry, 0, sizeof (tWsscfgDot11PrivacyEntry));

    /* Check whether the node is already present */
    pWsscfgDot11PrivacyEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11PrivacyTable,
                   (tRBElem *) pWsscfgSetDot11PrivacyEntry);

    if (pWsscfgDot11PrivacyEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllDot11PrivacyTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /*Function to check whether the given input is same as there in database */
    if (Dot11PrivacyTableFilterInputs
        (pWsscfgDot11PrivacyEntry, pWsscfgSetDot11PrivacyEntry,
         pWsscfgIsSetDot11PrivacyEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&WsscfgOldDot11PrivacyEntry, pWsscfgDot11PrivacyEntry,
            sizeof (tWsscfgDot11PrivacyEntry));

    /* Assign values for the existing node */
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11PrivacyInvoked != OSIX_FALSE)
    {
        pWsscfgDot11PrivacyEntry->MibObject.i4Dot11PrivacyInvoked =
            pWsscfgSetDot11PrivacyEntry->MibObject.i4Dot11PrivacyInvoked;
    }
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11WEPDefaultKeyID != OSIX_FALSE)
    {
        pWsscfgDot11PrivacyEntry->MibObject.i4Dot11WEPDefaultKeyID =
            pWsscfgSetDot11PrivacyEntry->MibObject.i4Dot11WEPDefaultKeyID;
    }
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11WEPKeyMappingLength != OSIX_FALSE)
    {
        pWsscfgDot11PrivacyEntry->MibObject.
            u4Dot11WEPKeyMappingLength =
            pWsscfgSetDot11PrivacyEntry->MibObject.u4Dot11WEPKeyMappingLength;
    }
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11ExcludeUnencrypted != OSIX_FALSE)
    {
        pWsscfgDot11PrivacyEntry->MibObject.
            i4Dot11ExcludeUnencrypted =
            pWsscfgSetDot11PrivacyEntry->MibObject.i4Dot11ExcludeUnencrypted;
    }
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11RSNAEnabled != OSIX_FALSE)
    {
        pWsscfgDot11PrivacyEntry->MibObject.i4Dot11RSNAEnabled =
            pWsscfgSetDot11PrivacyEntry->MibObject.i4Dot11RSNAEnabled;
    }
    if (pWsscfgIsSetDot11PrivacyEntry->bDot11RSNAPreauthenticationEnabled
        != OSIX_FALSE)
    {
        pWsscfgDot11PrivacyEntry->
            MibObject.i4Dot11RSNAPreauthenticationEnabled =
            pWsscfgSetDot11PrivacyEntry->
            MibObject.i4Dot11RSNAPreauthenticationEnabled;
    }

    if (WsscfgUtilUpdateDot11PrivacyTable
        (&WsscfgOldDot11PrivacyEntry, pWsscfgDot11PrivacyEntry,
         pWsscfgIsSetDot11PrivacyEntry) != OSIX_SUCCESS)
    {
        if (WsscfgSetAllDot11PrivacyTableTrigger
            (pWsscfgSetDot11PrivacyEntry,
             pWsscfgIsSetDot11PrivacyEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pWsscfgDot11PrivacyEntry, &WsscfgOldDot11PrivacyEntry,
                sizeof (tWsscfgDot11PrivacyEntry));
        return OSIX_FAILURE;
    }

    if (WsscfgSetAllDot11PrivacyTableTrigger
        (pWsscfgSetDot11PrivacyEntry, pWsscfgIsSetDot11PrivacyEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11MultiDomainCapabilityTable
 Input       :  pWsscfgSetDot11MultiDomainCapabilityEntry
                pWsscfgIsSetDot11MultiDomainCapabilityEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgSetAllDot11MultiDomainCapabilityTable
    (tWsscfgDot11MultiDomainCapabilityEntry *
     pWsscfgSetDot11MultiDomainCapabilityEntry,
     tWsscfgIsSetDot11MultiDomainCapabilityEntry *
     pWsscfgIsSetDot11MultiDomainCapabilityEntry)
{
    tWsscfgDot11MultiDomainCapabilityEntry
        * pWsscfgDot11MultiDomainCapabilityEntry = NULL;
    tWsscfgDot11MultiDomainCapabilityEntry
        WsscfgOldDot11MultiDomainCapabilityEntry;

    MEMSET (&WsscfgOldDot11MultiDomainCapabilityEntry, 0,
            sizeof (tWsscfgDot11MultiDomainCapabilityEntry));

    /* Check whether the node is already present */
    pWsscfgDot11MultiDomainCapabilityEntry =
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.Dot11MultiDomainCapabilityTable,
                   (tRBElem *) pWsscfgSetDot11MultiDomainCapabilityEntry);

    if (pWsscfgDot11MultiDomainCapabilityEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllDot11MultiDomainCapabilityTable:"
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /*Function to check whether the given input is same as there in database */
    if (Dot11MultiDomainCapabilityTableFilterInputs
        (pWsscfgDot11MultiDomainCapabilityEntry,
         pWsscfgSetDot11MultiDomainCapabilityEntry,
         pWsscfgIsSetDot11MultiDomainCapabilityEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&WsscfgOldDot11MultiDomainCapabilityEntry,
            pWsscfgDot11MultiDomainCapabilityEntry,
            sizeof (tWsscfgDot11MultiDomainCapabilityEntry));

    /* Assign values for the existing node */
    if (pWsscfgIsSetDot11MultiDomainCapabilityEntry->bDot11FirstChannelNumber
        != OSIX_FALSE)
    {
        pWsscfgDot11MultiDomainCapabilityEntry->
            MibObject.i4Dot11FirstChannelNumber =
            pWsscfgSetDot11MultiDomainCapabilityEntry->
            MibObject.i4Dot11FirstChannelNumber;
    }
    if (pWsscfgIsSetDot11MultiDomainCapabilityEntry->bDot11NumberofChannels
        != OSIX_FALSE)
    {
        pWsscfgDot11MultiDomainCapabilityEntry->
            MibObject.i4Dot11NumberofChannels =
            pWsscfgSetDot11MultiDomainCapabilityEntry->
            MibObject.i4Dot11NumberofChannels;
    }
    if (pWsscfgIsSetDot11MultiDomainCapabilityEntry->
        bDot11MaximumTransmitPowerLevel != OSIX_FALSE)
    {
        pWsscfgDot11MultiDomainCapabilityEntry->
            MibObject.i4Dot11MaximumTransmitPowerLevel =
            pWsscfgSetDot11MultiDomainCapabilityEntry->
            MibObject.i4Dot11MaximumTransmitPowerLevel;
    }

    if (WsscfgUtilUpdateDot11MultiDomainCapabilityTable
        (&WsscfgOldDot11MultiDomainCapabilityEntry,
         pWsscfgDot11MultiDomainCapabilityEntry,
         pWsscfgIsSetDot11MultiDomainCapabilityEntry) != OSIX_SUCCESS)
    {
        if (WsscfgSetAllDot11MultiDomainCapabilityTableTrigger
            (pWsscfgSetDot11MultiDomainCapabilityEntry,
             pWsscfgIsSetDot11MultiDomainCapabilityEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pWsscfgDot11MultiDomainCapabilityEntry,
                &WsscfgOldDot11MultiDomainCapabilityEntry,
                sizeof (tWsscfgDot11MultiDomainCapabilityEntry));
        return OSIX_FAILURE;
    }

    if (WsscfgSetAllDot11MultiDomainCapabilityTableTrigger
        (pWsscfgSetDot11MultiDomainCapabilityEntry,
         pWsscfgIsSetDot11MultiDomainCapabilityEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11SpectrumManagementTable
 Input       :  pWsscfgSetDot11SpectrumManagementEntry
                pWsscfgIsSetDot11SpectrumManagementEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllDot11SpectrumManagementTable (tWsscfgDot11SpectrumManagementEntry *
                                          pWsscfgSetDot11SpectrumManagementEntry,
                                          tWsscfgIsSetDot11SpectrumManagementEntry
                                          *
                                          pWsscfgIsSetDot11SpectrumManagementEntry)
{
    tWsscfgDot11SpectrumManagementEntry WsscfgOldDot11SpectrumManagementEntry;

    MEMSET (&WsscfgOldDot11SpectrumManagementEntry, 0,
            sizeof (tWsscfgDot11SpectrumManagementEntry));
    if (WsscfgUtilUpdateDot11SpectrumManagementTable
        (NULL, pWsscfgSetDot11SpectrumManagementEntry,
         pWsscfgIsSetDot11SpectrumManagementEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11RegulatoryClassesTable
 Input       :  pWsscfgSetDot11RegulatoryClassesEntry
                pWsscfgIsSetDot11RegulatoryClassesEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllDot11RegulatoryClassesTable (tWsscfgDot11RegulatoryClassesEntry *
                                         pWsscfgSetDot11RegulatoryClassesEntry,
                                         tWsscfgIsSetDot11RegulatoryClassesEntry
                                         *
                                         pWsscfgIsSetDot11RegulatoryClassesEntry)
{
    tWsscfgDot11RegulatoryClassesEntry
        * pWsscfgDot11RegulatoryClassesEntry = NULL;
    tWsscfgDot11RegulatoryClassesEntry WsscfgOldDot11RegulatoryClassesEntry;

    MEMSET (&WsscfgOldDot11RegulatoryClassesEntry, 0,
            sizeof (tWsscfgDot11RegulatoryClassesEntry));

    /* Check whether the node is already present */
    pWsscfgDot11RegulatoryClassesEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.
                   Dot11RegulatoryClassesTable,
                   (tRBElem *) pWsscfgSetDot11RegulatoryClassesEntry);

    if (pWsscfgDot11RegulatoryClassesEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllDot11RegulatoryClassesTable: "
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /*Function to check whether the given input is same as there in database */
    if (Dot11RegulatoryClassesTableFilterInputs
        (pWsscfgDot11RegulatoryClassesEntry,
         pWsscfgSetDot11RegulatoryClassesEntry,
         pWsscfgIsSetDot11RegulatoryClassesEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&WsscfgOldDot11RegulatoryClassesEntry,
            pWsscfgDot11RegulatoryClassesEntry,
            sizeof (tWsscfgDot11RegulatoryClassesEntry));

    /* Assign values for the existing node */
    if (pWsscfgIsSetDot11RegulatoryClassesEntry->
        bDot11RegulatoryClass != OSIX_FALSE)
    {
        pWsscfgDot11RegulatoryClassesEntry->
            MibObject.i4Dot11RegulatoryClass =
            pWsscfgSetDot11RegulatoryClassesEntry->
            MibObject.i4Dot11RegulatoryClass;
    }
    if (pWsscfgIsSetDot11RegulatoryClassesEntry->
        bDot11CoverageClass != OSIX_FALSE)
    {
        pWsscfgDot11RegulatoryClassesEntry->
            MibObject.i4Dot11CoverageClass =
            pWsscfgSetDot11RegulatoryClassesEntry->
            MibObject.i4Dot11CoverageClass;
    }

    if (WsscfgUtilUpdateDot11RegulatoryClassesTable
        (&WsscfgOldDot11RegulatoryClassesEntry,
         pWsscfgDot11RegulatoryClassesEntry,
         pWsscfgIsSetDot11RegulatoryClassesEntry) != OSIX_SUCCESS)
    {
        if (WsscfgSetAllDot11RegulatoryClassesTableTrigger
            (pWsscfgSetDot11RegulatoryClassesEntry,
             pWsscfgIsSetDot11RegulatoryClassesEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pWsscfgDot11RegulatoryClassesEntry,
                &WsscfgOldDot11RegulatoryClassesEntry,
                sizeof (tWsscfgDot11RegulatoryClassesEntry));
        return OSIX_FAILURE;
    }

    if (WsscfgSetAllDot11RegulatoryClassesTableTrigger
        (pWsscfgSetDot11RegulatoryClassesEntry,
         pWsscfgIsSetDot11RegulatoryClassesEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11OperationTable
 Input       :  pWsscfgSetDot11OperationEntry
                pWsscfgIsSetDot11OperationEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllDot11OperationTable (tWsscfgDot11OperationEntry *
                                 pWsscfgSetDot11OperationEntry,
                                 tWsscfgIsSetDot11OperationEntry *
                                 pWsscfgIsSetDot11OperationEntry)
{
    tWsscfgDot11OperationEntry WsscfgOldDot11OperationEntry;
    if (WsscfgUtilUpdateDot11OperationTable
        (&WsscfgOldDot11OperationEntry, pWsscfgSetDot11OperationEntry,
         pWsscfgIsSetDot11OperationEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11GroupAddressesTable
 Input       :  pWsscfgSetDot11GroupAddressesEntry
                pWsscfgIsSetDot11GroupAddressesEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllDot11GroupAddressesTable (tWsscfgDot11GroupAddressesEntry *
                                      pWsscfgSetDot11GroupAddressesEntry,
                                      tWsscfgIsSetDot11GroupAddressesEntry *
                                      pWsscfgIsSetDot11GroupAddressesEntry,
                                      INT4 i4RowStatusLogic,
                                      INT4 i4RowCreateOption)
{
    tWsscfgDot11GroupAddressesEntry *pWsscfgDot11GroupAddressesEntry = NULL;
    tWsscfgDot11GroupAddressesEntry *pWsscfgOldDot11GroupAddressesEntry = NULL;
    tWsscfgDot11GroupAddressesEntry *pWsscfgTrgDot11GroupAddressesEntry = NULL;
    tWsscfgIsSetDot11GroupAddressesEntry
        * pWsscfgTrgIsSetDot11GroupAddressesEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pWsscfgOldDot11GroupAddressesEntry =
        (tWsscfgDot11GroupAddressesEntry *)
        MemAllocMemBlk (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID);
    if (pWsscfgOldDot11GroupAddressesEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWsscfgTrgDot11GroupAddressesEntry =
        (tWsscfgDot11GroupAddressesEntry *)
        MemAllocMemBlk (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID);
    if (pWsscfgTrgDot11GroupAddressesEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                            (UINT1 *) pWsscfgOldDot11GroupAddressesEntry);
        return OSIX_FAILURE;
    }
    pWsscfgTrgIsSetDot11GroupAddressesEntry =
        (tWsscfgIsSetDot11GroupAddressesEntry *)
        MemAllocMemBlk (WSSCFG_DOT11GROUPADDRESSESTABLE_ISSET_POOLID);
    if (pWsscfgTrgIsSetDot11GroupAddressesEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                            (UINT1 *) pWsscfgOldDot11GroupAddressesEntry);
        MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgDot11GroupAddressesEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pWsscfgOldDot11GroupAddressesEntry, 0,
            sizeof (tWsscfgDot11GroupAddressesEntry));
    MEMSET (pWsscfgTrgDot11GroupAddressesEntry, 0,
            sizeof (tWsscfgDot11GroupAddressesEntry));
    MEMSET (pWsscfgTrgIsSetDot11GroupAddressesEntry, 0,
            sizeof (tWsscfgIsSetDot11GroupAddressesEntry));

    /* Check whether the node is already present */
    pWsscfgDot11GroupAddressesEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.
                   Dot11GroupAddressesTable,
                   (tRBElem *) pWsscfgSetDot11GroupAddressesEntry);

    if (pWsscfgDot11GroupAddressesEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or 
         * CREATE_AND_GO */
        if ((pWsscfgSetDot11GroupAddressesEntry->
             MibObject.i4Dot11GroupAddressesStatus == CREATE_AND_WAIT)
            || (pWsscfgSetDot11GroupAddressesEntry->
                MibObject.i4Dot11GroupAddressesStatus ==
                CREATE_AND_GO)
            ||
            ((pWsscfgSetDot11GroupAddressesEntry->
              MibObject.i4Dot11GroupAddressesStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pWsscfgDot11GroupAddressesEntry =
                (tWsscfgDot11GroupAddressesEntry *)
                MemAllocMemBlk (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID);
            if (pWsscfgDot11GroupAddressesEntry == NULL)
            {
                if (WsscfgSetAllDot11GroupAddressesTableTrigger
                    (pWsscfgSetDot11GroupAddressesEntry,
                     pWsscfgIsSetDot11GroupAddressesEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllDot11GroupAddressesTable:"
                                 "WsscfgSetAllDot11GroupAddressesTableTrigger"
                                 "function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllDot11GroupAddressesTable:"
                             "Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock
                    (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID, (UINT1 *)
                     pWsscfgOldDot11GroupAddressesEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgDot11GroupAddressesEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11GROUPADDRESSESTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetDot11GroupAddressesEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pWsscfgDot11GroupAddressesEntry, 0,
                    sizeof (tWsscfgDot11GroupAddressesEntry));
            if ((WsscfgInitializeDot11GroupAddressesTable
                 (pWsscfgDot11GroupAddressesEntry)) == OSIX_FAILURE)
            {
                if (WsscfgSetAllDot11GroupAddressesTableTrigger
                    (pWsscfgSetDot11GroupAddressesEntry,
                     pWsscfgIsSetDot11GroupAddressesEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllDot11GroupAddressesTable:"
                                 "WsscfgSetAllDot11GroupAddressesTableTrigger"
                                 "function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllDot11GroupAddressesTable: "
                             "Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID, (UINT1 *)
                     pWsscfgDot11GroupAddressesEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID, (UINT1 *)
                     pWsscfgOldDot11GroupAddressesEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgDot11GroupAddressesEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11GROUPADDRESSESTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetDot11GroupAddressesEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pWsscfgIsSetDot11GroupAddressesEntry->bDot11GroupAddressesIndex
                != OSIX_FALSE)
            {
                pWsscfgDot11GroupAddressesEntry->
                    MibObject.i4Dot11GroupAddressesIndex =
                    pWsscfgSetDot11GroupAddressesEntry->
                    MibObject.i4Dot11GroupAddressesIndex;
            }

            if (pWsscfgIsSetDot11GroupAddressesEntry->bDot11Address !=
                OSIX_FALSE)
            {
                MEMCPY (&
                        (pWsscfgDot11GroupAddressesEntry->
                         MibObject.Dot11Address),
                        &(pWsscfgSetDot11GroupAddressesEntry->
                          MibObject.Dot11Address), 6);
            }

            if (pWsscfgIsSetDot11GroupAddressesEntry->bDot11GroupAddressesStatus
                != OSIX_FALSE)
            {
                pWsscfgDot11GroupAddressesEntry->
                    MibObject.i4Dot11GroupAddressesStatus =
                    pWsscfgSetDot11GroupAddressesEntry->
                    MibObject.i4Dot11GroupAddressesStatus;
            }

            if (pWsscfgIsSetDot11GroupAddressesEntry->bIfIndex != OSIX_FALSE)
            {
                pWsscfgDot11GroupAddressesEntry->MibObject.i4IfIndex =
                    pWsscfgSetDot11GroupAddressesEntry->MibObject.i4IfIndex;
            }

            if ((pWsscfgSetDot11GroupAddressesEntry->
                 MibObject.i4Dot11GroupAddressesStatus ==
                 CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetDot11GroupAddressesEntry->
                        MibObject.i4Dot11GroupAddressesStatus == ACTIVE)))
            {
                pWsscfgDot11GroupAddressesEntry->
                    MibObject.i4Dot11GroupAddressesStatus = ACTIVE;
            }
            else if (pWsscfgSetDot11GroupAddressesEntry->
                     MibObject.i4Dot11GroupAddressesStatus == CREATE_AND_WAIT)
            {
                pWsscfgDot11GroupAddressesEntry->
                    MibObject.i4Dot11GroupAddressesStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gWsscfgGlobals.WsscfgGlbMib.Dot11GroupAddressesTable,
                 (tRBElem *) pWsscfgDot11GroupAddressesEntry) != RB_SUCCESS)
            {
                if (WsscfgSetAllDot11GroupAddressesTableTrigger
                    (pWsscfgSetDot11GroupAddressesEntry,
                     pWsscfgIsSetDot11GroupAddressesEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllDot11GroupAddressesTable:"
                                 "WsscfgSetAllDot11GroupAddressesTableTrigger "
                                 "function returns failure.\r\n"));
                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllDot11GroupAddressesTable: "
                             "RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID, (UINT1 *)
                     pWsscfgDot11GroupAddressesEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID, (UINT1 *)
                     pWsscfgOldDot11GroupAddressesEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgDot11GroupAddressesEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11GROUPADDRESSESTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetDot11GroupAddressesEntry);
                return OSIX_FAILURE;
            }
            if (WsscfgUtilUpdateDot11GroupAddressesTable
                (NULL, pWsscfgDot11GroupAddressesEntry,
                 pWsscfgIsSetDot11GroupAddressesEntry) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllDot11GroupAddressesTable:"
                             "WsscfgUtilUpdateDot11GroupAddressesTable"
                             "function returns failure.\r\n"));

                if (WsscfgSetAllDot11GroupAddressesTableTrigger
                    (pWsscfgSetDot11GroupAddressesEntry,
                     pWsscfgIsSetDot11GroupAddressesEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllDot11GroupAddressesTable:"
                                 "WsscfgSetAllDot11GroupAddressesTableTrigger"
                                 "function returns failure.\r\n"));

                }
                RBTreeRem (gWsscfgGlobals.
                           WsscfgGlbMib.Dot11GroupAddressesTable,
                           pWsscfgDot11GroupAddressesEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID, (UINT1 *)
                     pWsscfgDot11GroupAddressesEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID, (UINT1 *)
                     pWsscfgOldDot11GroupAddressesEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgDot11GroupAddressesEntry);
                MemReleaseMemBlock
                    (WSSCFG_DOT11GROUPADDRESSESTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetDot11GroupAddressesEntry);
                return OSIX_FAILURE;
            }

            if ((pWsscfgSetDot11GroupAddressesEntry->
                 MibObject.i4Dot11GroupAddressesStatus ==
                 CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetDot11GroupAddressesEntry->
                        MibObject.i4Dot11GroupAddressesStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgDot11GroupAddressesEntry->
                    MibObject.i4Dot11GroupAddressesIndex =
                    pWsscfgSetDot11GroupAddressesEntry->
                    MibObject.i4Dot11GroupAddressesIndex;
                pWsscfgTrgDot11GroupAddressesEntry->MibObject.
                    i4IfIndex =
                    pWsscfgSetDot11GroupAddressesEntry->MibObject.i4IfIndex;
                pWsscfgTrgDot11GroupAddressesEntry->
                    MibObject.i4Dot11GroupAddressesStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetDot11GroupAddressesEntry->
                    bDot11GroupAddressesStatus = OSIX_TRUE;

                if (WsscfgSetAllDot11GroupAddressesTableTrigger
                    (pWsscfgTrgDot11GroupAddressesEntry,
                     pWsscfgTrgIsSetDot11GroupAddressesEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllDot11GroupAddressesTable:"
                                 "WsscfgSetAllDot11GroupAddressesTableTrigger"
                                 "function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                         (UINT1 *) pWsscfgDot11GroupAddressesEntry);
                    MemReleaseMemBlock
                        (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                         (UINT1 *) pWsscfgOldDot11GroupAddressesEntry);
                    MemReleaseMemBlock
                        (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                         (UINT1 *) pWsscfgTrgDot11GroupAddressesEntry);
                    MemReleaseMemBlock
                        (WSSCFG_DOT11GROUPADDRESSESTABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetDot11GroupAddressesEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pWsscfgSetDot11GroupAddressesEntry->
                     MibObject.i4Dot11GroupAddressesStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgDot11GroupAddressesEntry->
                    MibObject.i4Dot11GroupAddressesStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetDot11GroupAddressesEntry->
                    bDot11GroupAddressesStatus = OSIX_TRUE;

                if (WsscfgSetAllDot11GroupAddressesTableTrigger
                    (pWsscfgTrgDot11GroupAddressesEntry,
                     pWsscfgTrgIsSetDot11GroupAddressesEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllDot11GroupAddressesTable:"
                                 "WsscfgSetAllDot11GroupAddressesTableTrigger"
                                 "function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                         (UINT1 *) pWsscfgDot11GroupAddressesEntry);
                    MemReleaseMemBlock
                        (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                         (UINT1 *) pWsscfgOldDot11GroupAddressesEntry);
                    MemReleaseMemBlock
                        (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                         (UINT1 *) pWsscfgTrgDot11GroupAddressesEntry);
                    MemReleaseMemBlock
                        (WSSCFG_DOT11GROUPADDRESSESTABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetDot11GroupAddressesEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pWsscfgSetDot11GroupAddressesEntry->
                MibObject.i4Dot11GroupAddressesStatus == CREATE_AND_GO)
            {
                pWsscfgSetDot11GroupAddressesEntry->
                    MibObject.i4Dot11GroupAddressesStatus = ACTIVE;
            }

            if (WsscfgSetAllDot11GroupAddressesTableTrigger
                (pWsscfgSetDot11GroupAddressesEntry,
                 pWsscfgIsSetDot11GroupAddressesEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllDot11GroupAddressesTable:"
                             "WsscfgSetAllDot11GroupAddressesTableTrigger "
                             "function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                                (UINT1 *) pWsscfgOldDot11GroupAddressesEntry);
            MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgDot11GroupAddressesEntry);
            MemReleaseMemBlock
                (WSSCFG_DOT11GROUPADDRESSESTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetDot11GroupAddressesEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (WsscfgSetAllDot11GroupAddressesTableTrigger
                (pWsscfgSetDot11GroupAddressesEntry,
                 pWsscfgIsSetDot11GroupAddressesEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllDot11GroupAddressesTable: "
                             "WsscfgSetAllDot11GroupAddressesTableTrigger "
                             "function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllDot11GroupAddressesTable: Failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                                (UINT1 *) pWsscfgOldDot11GroupAddressesEntry);
            MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgDot11GroupAddressesEntry);
            MemReleaseMemBlock
                (WSSCFG_DOT11GROUPADDRESSESTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetDot11GroupAddressesEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pWsscfgSetDot11GroupAddressesEntry->
              MibObject.i4Dot11GroupAddressesStatus == CREATE_AND_WAIT)
             || (pWsscfgSetDot11GroupAddressesEntry->
                 MibObject.i4Dot11GroupAddressesStatus == CREATE_AND_GO))
    {
        if (WsscfgSetAllDot11GroupAddressesTableTrigger
            (pWsscfgSetDot11GroupAddressesEntry,
             pWsscfgIsSetDot11GroupAddressesEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllDot11GroupAddressesTable:"
                         "WsscfgSetAllDot11GroupAddressesTableTrigger"
                         "function returns failure.\r\n"));
        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllDot11GroupAddressesTable:"
                     "The row is already present.\r\n"));
        MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                            (UINT1 *) pWsscfgOldDot11GroupAddressesEntry);
        MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgDot11GroupAddressesEntry);
        MemReleaseMemBlock
            (WSSCFG_DOT11GROUPADDRESSESTABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetDot11GroupAddressesEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pWsscfgOldDot11GroupAddressesEntry,
            pWsscfgDot11GroupAddressesEntry,
            sizeof (tWsscfgDot11GroupAddressesEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pWsscfgSetDot11GroupAddressesEntry->
        MibObject.i4Dot11GroupAddressesStatus == DESTROY)
    {
        pWsscfgDot11GroupAddressesEntry->
            MibObject.i4Dot11GroupAddressesStatus = DESTROY;

        if (WsscfgUtilUpdateDot11GroupAddressesTable
            (pWsscfgOldDot11GroupAddressesEntry,
             pWsscfgDot11GroupAddressesEntry,
             pWsscfgIsSetDot11GroupAddressesEntry) != OSIX_SUCCESS)
        {

            if (WsscfgSetAllDot11GroupAddressesTableTrigger
                (pWsscfgSetDot11GroupAddressesEntry,
                 pWsscfgIsSetDot11GroupAddressesEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllDot11GroupAddressesTable:"
                             "WsscfgSetAllDot11GroupAddressesTableTrigger"
                             "function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllDot11GroupAddressesTable:"
                         "WsscfgUtilUpdateDot11GroupAddressesTable"
                         "function returns failure.\r\n"));
        }
        RBTreeRem (gWsscfgGlobals.WsscfgGlbMib.
                   Dot11GroupAddressesTable, pWsscfgDot11GroupAddressesEntry);
        if (WsscfgSetAllDot11GroupAddressesTableTrigger
            (pWsscfgSetDot11GroupAddressesEntry,
             pWsscfgIsSetDot11GroupAddressesEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllDot11GroupAddressesTable:"
                         "WsscfgSetAllDot11GroupAddressesTableTrigger"
                         "function returns failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                                (UINT1 *) pWsscfgOldDot11GroupAddressesEntry);
            MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgDot11GroupAddressesEntry);
            MemReleaseMemBlock
                (WSSCFG_DOT11GROUPADDRESSESTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetDot11GroupAddressesEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                            (UINT1 *) pWsscfgDot11GroupAddressesEntry);
        MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                            (UINT1 *) pWsscfgOldDot11GroupAddressesEntry);
        MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgDot11GroupAddressesEntry);
        MemReleaseMemBlock
            (WSSCFG_DOT11GROUPADDRESSESTABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetDot11GroupAddressesEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (Dot11GroupAddressesTableFilterInputs
        (pWsscfgDot11GroupAddressesEntry,
         pWsscfgSetDot11GroupAddressesEntry,
         pWsscfgIsSetDot11GroupAddressesEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                            (UINT1 *) pWsscfgOldDot11GroupAddressesEntry);
        MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgDot11GroupAddressesEntry);
        MemReleaseMemBlock
            (WSSCFG_DOT11GROUPADDRESSESTABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetDot11GroupAddressesEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before 
     * modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pWsscfgDot11GroupAddressesEntry->
         MibObject.i4Dot11GroupAddressesStatus == ACTIVE)
        && (pWsscfgSetDot11GroupAddressesEntry->
            MibObject.i4Dot11GroupAddressesStatus != NOT_IN_SERVICE))
    {
        pWsscfgDot11GroupAddressesEntry->
            MibObject.i4Dot11GroupAddressesStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pWsscfgTrgDot11GroupAddressesEntry->
            MibObject.i4Dot11GroupAddressesStatus = NOT_IN_SERVICE;
        pWsscfgTrgIsSetDot11GroupAddressesEntry->bDot11GroupAddressesStatus
            = OSIX_TRUE;

        if (WsscfgUtilUpdateDot11GroupAddressesTable
            (pWsscfgOldDot11GroupAddressesEntry,
             pWsscfgDot11GroupAddressesEntry,
             pWsscfgIsSetDot11GroupAddressesEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pWsscfgDot11GroupAddressesEntry,
                    pWsscfgOldDot11GroupAddressesEntry,
                    sizeof (tWsscfgDot11GroupAddressesEntry));
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllDot11GroupAddressesTable:"
                         "WsscfgUtilUpdateDot11GroupAddressesTable Function"
                         "returns failure.\r\n"));

            if (WsscfgSetAllDot11GroupAddressesTableTrigger
                (pWsscfgSetDot11GroupAddressesEntry,
                 pWsscfgIsSetDot11GroupAddressesEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllDot11GroupAddressesTable:"
                             "WsscfgSetAllDot11GroupAddressesTableTrigger"
                             "function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                                (UINT1 *) pWsscfgOldDot11GroupAddressesEntry);
            MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgDot11GroupAddressesEntry);
            MemReleaseMemBlock
                (WSSCFG_DOT11GROUPADDRESSESTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetDot11GroupAddressesEntry);
            return OSIX_FAILURE;
        }

        if (WsscfgSetAllDot11GroupAddressesTableTrigger
            (pWsscfgTrgDot11GroupAddressesEntry,
             pWsscfgTrgIsSetDot11GroupAddressesEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllDot11GroupAddressesTable:"
                         "WsscfgSetAllDot11GroupAddressesTableTrigger function"
                         "returns failure.\r\n"));
        }
    }

    if (pWsscfgSetDot11GroupAddressesEntry->
        MibObject.i4Dot11GroupAddressesStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pWsscfgIsSetDot11GroupAddressesEntry->bDot11Address != OSIX_FALSE)
    {
        MEMCPY (&
                (pWsscfgDot11GroupAddressesEntry->MibObject.
                 Dot11Address),
                &(pWsscfgSetDot11GroupAddressesEntry->
                  MibObject.Dot11Address), 6);
    }
    if (pWsscfgIsSetDot11GroupAddressesEntry->
        bDot11GroupAddressesStatus != OSIX_FALSE)
    {
        pWsscfgDot11GroupAddressesEntry->
            MibObject.i4Dot11GroupAddressesStatus =
            pWsscfgSetDot11GroupAddressesEntry->
            MibObject.i4Dot11GroupAddressesStatus;
    }

    /*This condtion is to make the row back to ACTIVE 
     * after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pWsscfgDot11GroupAddressesEntry->
            MibObject.i4Dot11GroupAddressesStatus = ACTIVE;
    }

    if (WsscfgUtilUpdateDot11GroupAddressesTable
        (pWsscfgOldDot11GroupAddressesEntry,
         pWsscfgDot11GroupAddressesEntry,
         pWsscfgIsSetDot11GroupAddressesEntry) != OSIX_SUCCESS)
    {

        if (WsscfgSetAllDot11GroupAddressesTableTrigger
            (pWsscfgSetDot11GroupAddressesEntry,
             pWsscfgIsSetDot11GroupAddressesEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllDot11GroupAddressesTable:"
                         "WsscfgSetAllDot11GroupAddressesTableTrigger function"
                         "returns failure.\r\n"));

        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllDot11GroupAddressesTable:"
                     "WsscfgUtilUpdateDot11GroupAddressesTable function"
                     "returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pWsscfgDot11GroupAddressesEntry,
                pWsscfgOldDot11GroupAddressesEntry,
                sizeof (tWsscfgDot11GroupAddressesEntry));
        MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                            (UINT1 *) pWsscfgOldDot11GroupAddressesEntry);
        MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgDot11GroupAddressesEntry);
        MemReleaseMemBlock
            (WSSCFG_DOT11GROUPADDRESSESTABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetDot11GroupAddressesEntry);
        return OSIX_FAILURE;

    }
    if (WsscfgSetAllDot11GroupAddressesTableTrigger
        (pWsscfgSetDot11GroupAddressesEntry,
         pWsscfgIsSetDot11GroupAddressesEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllDot11GroupAddressesTable:"
                     "WsscfgSetAllDot11GroupAddressesTableTrigger function"
                     "returns failure.\r\n"));
    }

    MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                        (UINT1 *) pWsscfgOldDot11GroupAddressesEntry);
    MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID,
                        (UINT1 *) pWsscfgTrgDot11GroupAddressesEntry);
    MemReleaseMemBlock (WSSCFG_DOT11GROUPADDRESSESTABLE_ISSET_POOLID,
                        (UINT1 *) pWsscfgTrgIsSetDot11GroupAddressesEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgSetAllDot11EDCATable
 Input       :  pWsscfgSetDot11EDCAEntry
                pWsscfgIsSetDot11EDCAEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllDot11EDCATable (tWsscfgDot11EDCAEntry * pWsscfgSetDot11EDCAEntry,
                            tWsscfgIsSetDot11EDCAEntry *
                            pWsscfgIsSetDot11EDCAEntry)
{
    tWsscfgDot11EDCAEntry WsscfgOldDot11EDCAEntry;

    MEMSET (&WsscfgOldDot11EDCAEntry, 0, sizeof (tWsscfgDot11EDCAEntry));
    if (WsscfgUtilUpdateDot11EDCATable
        (&WsscfgOldDot11EDCAEntry, pWsscfgSetDot11EDCAEntry,
         pWsscfgIsSetDot11EDCAEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11QAPEDCATable
 Input       :  pWsscfgSetDot11QAPEDCAEntry
                pWsscfgIsSetDot11QAPEDCAEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllDot11QAPEDCATable (tWsscfgDot11QAPEDCAEntry *
                               pWsscfgSetDot11QAPEDCAEntry,
                               tWsscfgIsSetDot11QAPEDCAEntry *
                               pWsscfgIsSetDot11QAPEDCAEntry)
{
    tWsscfgDot11QAPEDCAEntry *pWsscfgDot11QAPEDCAEntry = NULL;
    tWsscfgDot11QAPEDCAEntry WsscfgOldDot11QAPEDCAEntry;

    MEMSET (&WsscfgOldDot11QAPEDCAEntry, 0, sizeof (tWsscfgDot11QAPEDCAEntry));

    /* Check whether the node is already present */
    pWsscfgDot11QAPEDCAEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11QAPEDCATable,
                   (tRBElem *) pWsscfgSetDot11QAPEDCAEntry);

    if (pWsscfgDot11QAPEDCAEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllDot11QAPEDCATable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /*Function to check whether the given input is same as there in database */
    if (Dot11QAPEDCATableFilterInputs
        (pWsscfgDot11QAPEDCAEntry, pWsscfgSetDot11QAPEDCAEntry,
         pWsscfgIsSetDot11QAPEDCAEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&WsscfgOldDot11QAPEDCAEntry, pWsscfgDot11QAPEDCAEntry,
            sizeof (tWsscfgDot11QAPEDCAEntry));

    /* Assign values for the existing node */
    if (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableCWmin != OSIX_FALSE)
    {
        pWsscfgDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableCWmin =
            pWsscfgSetDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableCWmin;
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableCWmax != OSIX_FALSE)
    {
        pWsscfgDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableCWmax =
            pWsscfgSetDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableCWmax;
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableAIFSN != OSIX_FALSE)
    {
        pWsscfgDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableAIFSN =
            pWsscfgSetDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableAIFSN;
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableTXOPLimit !=
        OSIX_FALSE)
    {
        pWsscfgDot11QAPEDCAEntry->MibObject.
            i4Dot11QAPEDCATableTXOPLimit =
            pWsscfgSetDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableTXOPLimit;
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->
        bDot11QAPEDCATableMSDULifetime != OSIX_FALSE)
    {
        pWsscfgDot11QAPEDCAEntry->
            MibObject.i4Dot11QAPEDCATableMSDULifetime =
            pWsscfgSetDot11QAPEDCAEntry->
            MibObject.i4Dot11QAPEDCATableMSDULifetime;
    }
    if (pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableMandatory !=
        OSIX_FALSE)
    {
        pWsscfgDot11QAPEDCAEntry->MibObject.
            i4Dot11QAPEDCATableMandatory =
            pWsscfgSetDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableMandatory;
    }

    if (WsscfgUtilUpdateDot11QAPEDCATable
        (&WsscfgOldDot11QAPEDCAEntry, pWsscfgDot11QAPEDCAEntry,
         pWsscfgIsSetDot11QAPEDCAEntry) != OSIX_SUCCESS)
    {
        if (WsscfgSetAllDot11QAPEDCATableTrigger
            (pWsscfgSetDot11QAPEDCAEntry,
             pWsscfgIsSetDot11QAPEDCAEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pWsscfgDot11QAPEDCAEntry, &WsscfgOldDot11QAPEDCAEntry,
                sizeof (tWsscfgDot11QAPEDCAEntry));
        return OSIX_FAILURE;
    }

    if (WsscfgSetAllDot11QAPEDCATableTrigger
        (pWsscfgSetDot11QAPEDCAEntry, pWsscfgIsSetDot11QAPEDCAEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsSecurityWebAuthGuestInfoTable
 Input       :  pWsscfgSetFsSecurityWebAuthGuestInfoEntry
                pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgSetAllFsSecurityWebAuthGuestInfoTable
    (tWsscfgFsSecurityWebAuthGuestInfoEntry *
     pWsscfgSetFsSecurityWebAuthGuestInfoEntry,
     tWsscfgIsSetFsSecurityWebAuthGuestInfoEntry *
     pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry, INT4 i4RowStatusLogic,
     INT4 i4RowCreateOption)
{
    UNUSED_PARAM (i4RowCreateOption);
    UNUSED_PARAM (i4RowStatusLogic);
    if (WsscfgUtilUpdateFsSecurityWebAuthGuestInfoTable
        (NULL, pWsscfgSetFsSecurityWebAuthGuestInfoEntry,
         pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry) != OSIX_SUCCESS)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsSecurityWebAuthGuestInfoTable:"
                     "WsscfgUtilUpdateFsSecurityWebAuthGuestInfoTable"
                     "function returns failure.\r\n"));
        return OSIX_FAILURE;

    }

    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgSetAllDot11PhyOperationTable
 Input       :  pWsscfgSetDot11PhyOperationEntry
                pWsscfgIsSetDot11PhyOperationEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllDot11PhyOperationTable (tWsscfgDot11PhyOperationEntry *
                                    pWsscfgSetDot11PhyOperationEntry,
                                    tWsscfgIsSetDot11PhyOperationEntry *
                                    pWsscfgIsSetDot11PhyOperationEntry)
{
    tWsscfgDot11PhyOperationEntry *pWsscfgDot11PhyOperationEntry = NULL;
    tWsscfgDot11PhyOperationEntry WsscfgOldDot11PhyOperationEntry;

    MEMSET (&WsscfgOldDot11PhyOperationEntry, 0,
            sizeof (tWsscfgDot11PhyOperationEntry));

    /* Check whether the node is already present */
    pWsscfgDot11PhyOperationEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyOperationTable,
                   (tRBElem *) pWsscfgSetDot11PhyOperationEntry);

    if (pWsscfgDot11PhyOperationEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllDot11PhyOperationTable:"
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /*Function to check whether the given input is same as there in database */
    if (Dot11PhyOperationTableFilterInputs
        (pWsscfgDot11PhyOperationEntry,
         pWsscfgSetDot11PhyOperationEntry,
         pWsscfgIsSetDot11PhyOperationEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&WsscfgOldDot11PhyOperationEntry,
            pWsscfgDot11PhyOperationEntry,
            sizeof (tWsscfgDot11PhyOperationEntry));

    /* Assign values for the existing node */
    if (pWsscfgIsSetDot11PhyOperationEntry->bDot11CurrentRegDomain !=
        OSIX_FALSE)
    {
        pWsscfgDot11PhyOperationEntry->MibObject.
            i4Dot11CurrentRegDomain =
            pWsscfgSetDot11PhyOperationEntry->MibObject.i4Dot11CurrentRegDomain;
    }

    if (WsscfgUtilUpdateDot11PhyOperationTable
        (&WsscfgOldDot11PhyOperationEntry,
         pWsscfgDot11PhyOperationEntry,
         pWsscfgIsSetDot11PhyOperationEntry) != OSIX_SUCCESS)
    {
        if (WsscfgSetAllDot11PhyOperationTableTrigger
            (pWsscfgSetDot11PhyOperationEntry,
             pWsscfgIsSetDot11PhyOperationEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pWsscfgDot11PhyOperationEntry,
                &WsscfgOldDot11PhyOperationEntry,
                sizeof (tWsscfgDot11PhyOperationEntry));
        return OSIX_FAILURE;
    }

    if (WsscfgSetAllDot11PhyOperationTableTrigger
        (pWsscfgSetDot11PhyOperationEntry,
         pWsscfgIsSetDot11PhyOperationEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11PhyAntennaTable
 Input       :  pWsscfgSetDot11PhyAntennaEntry
                pWsscfgIsSetDot11PhyAntennaEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllDot11PhyAntennaTable (tWsscfgDot11PhyAntennaEntry *
                                  pWsscfgSetDot11PhyAntennaEntry,
                                  tWsscfgIsSetDot11PhyAntennaEntry *
                                  pWsscfgIsSetDot11PhyAntennaEntry)
{
    tWsscfgDot11PhyAntennaEntry *pWsscfgDot11PhyAntennaEntry = NULL;
    tWsscfgDot11PhyAntennaEntry WsscfgOldDot11PhyAntennaEntry;

    MEMSET (&WsscfgOldDot11PhyAntennaEntry, 0,
            sizeof (tWsscfgDot11PhyAntennaEntry));

    /* Check whether the node is already present */
    pWsscfgDot11PhyAntennaEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyAntennaTable,
                   (tRBElem *) pWsscfgSetDot11PhyAntennaEntry);

    if (pWsscfgDot11PhyAntennaEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllDot11PhyAntennaTable:"
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /*Function to check whether the given input is same as there in database */
    if (Dot11PhyAntennaTableFilterInputs
        (pWsscfgDot11PhyAntennaEntry, pWsscfgSetDot11PhyAntennaEntry,
         pWsscfgIsSetDot11PhyAntennaEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&WsscfgOldDot11PhyAntennaEntry,
            pWsscfgDot11PhyAntennaEntry, sizeof (tWsscfgDot11PhyAntennaEntry));

    /* Assign values for the existing node */
    if (pWsscfgIsSetDot11PhyAntennaEntry->bDot11CurrentTxAntenna != OSIX_FALSE)
    {
        pWsscfgDot11PhyAntennaEntry->MibObject.
            i4Dot11CurrentTxAntenna =
            pWsscfgSetDot11PhyAntennaEntry->MibObject.i4Dot11CurrentTxAntenna;
    }
    if (pWsscfgIsSetDot11PhyAntennaEntry->bDot11CurrentRxAntenna != OSIX_FALSE)
    {
        pWsscfgDot11PhyAntennaEntry->MibObject.
            i4Dot11CurrentRxAntenna =
            pWsscfgSetDot11PhyAntennaEntry->MibObject.i4Dot11CurrentRxAntenna;
    }

    if (WsscfgUtilUpdateDot11PhyAntennaTable
        (&WsscfgOldDot11PhyAntennaEntry, pWsscfgDot11PhyAntennaEntry,
         pWsscfgIsSetDot11PhyAntennaEntry) != OSIX_SUCCESS)
    {
        if (WsscfgSetAllDot11PhyAntennaTableTrigger
            (pWsscfgSetDot11PhyAntennaEntry,
             pWsscfgIsSetDot11PhyAntennaEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pWsscfgDot11PhyAntennaEntry,
                &WsscfgOldDot11PhyAntennaEntry,
                sizeof (tWsscfgDot11PhyAntennaEntry));
        return OSIX_FAILURE;
    }

    if (WsscfgSetAllDot11PhyAntennaTableTrigger
        (pWsscfgSetDot11PhyAntennaEntry,
         pWsscfgIsSetDot11PhyAntennaEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11PhyTxPowerTable
 Input       :  pWsscfgSetDot11PhyTxPowerEntry
                pWsscfgIsSetDot11PhyTxPowerEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllDot11PhyTxPowerTable (tWsscfgDot11PhyTxPowerEntry *
                                  pWsscfgSetDot11PhyTxPowerEntry,
                                  tWsscfgIsSetDot11PhyTxPowerEntry *
                                  pWsscfgIsSetDot11PhyTxPowerEntry)
{
    tWsscfgDot11PhyTxPowerEntry WsscfgOldDot11PhyTxPowerEntry;

    MEMSET (&WsscfgOldDot11PhyTxPowerEntry, 0,
            sizeof (tWsscfgDot11PhyTxPowerEntry));

    if (WsscfgUtilUpdateDot11PhyTxPowerTable
        (&WsscfgOldDot11PhyTxPowerEntry,
         pWsscfgSetDot11PhyTxPowerEntry,
         pWsscfgIsSetDot11PhyTxPowerEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11PhyFHSSTable
 Input       :  pWsscfgSetDot11PhyFHSSEntry
                pWsscfgIsSetDot11PhyFHSSEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllDot11PhyFHSSTable (tWsscfgDot11PhyFHSSEntry *
                               pWsscfgSetDot11PhyFHSSEntry,
                               tWsscfgIsSetDot11PhyFHSSEntry *
                               pWsscfgIsSetDot11PhyFHSSEntry)
{
    tWsscfgDot11PhyFHSSEntry *pWsscfgDot11PhyFHSSEntry = NULL;
    tWsscfgDot11PhyFHSSEntry WsscfgOldDot11PhyFHSSEntry;

    MEMSET (&WsscfgOldDot11PhyFHSSEntry, 0, sizeof (tWsscfgDot11PhyFHSSEntry));

    /* Check whether the node is already present */
    pWsscfgDot11PhyFHSSEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyFHSSTable,
                   (tRBElem *) pWsscfgSetDot11PhyFHSSEntry);

    if (pWsscfgDot11PhyFHSSEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllDot11PhyFHSSTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /*Function to check whether the given input is same as there in database */
    if (Dot11PhyFHSSTableFilterInputs
        (pWsscfgDot11PhyFHSSEntry, pWsscfgSetDot11PhyFHSSEntry,
         pWsscfgIsSetDot11PhyFHSSEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&WsscfgOldDot11PhyFHSSEntry, pWsscfgDot11PhyFHSSEntry,
            sizeof (tWsscfgDot11PhyFHSSEntry));

    /* Assign values for the existing node */
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentChannelNumber != OSIX_FALSE)
    {
        pWsscfgDot11PhyFHSSEntry->MibObject.
            i4Dot11CurrentChannelNumber =
            pWsscfgSetDot11PhyFHSSEntry->MibObject.i4Dot11CurrentChannelNumber;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentDwellTime != OSIX_FALSE)
    {
        pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11CurrentDwellTime =
            pWsscfgSetDot11PhyFHSSEntry->MibObject.i4Dot11CurrentDwellTime;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentSet != OSIX_FALSE)
    {
        pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11CurrentSet =
            pWsscfgSetDot11PhyFHSSEntry->MibObject.i4Dot11CurrentSet;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentPattern != OSIX_FALSE)
    {
        pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11CurrentPattern =
            pWsscfgSetDot11PhyFHSSEntry->MibObject.i4Dot11CurrentPattern;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentIndex != OSIX_FALSE)
    {
        pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11CurrentIndex =
            pWsscfgSetDot11PhyFHSSEntry->MibObject.i4Dot11CurrentIndex;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCPrimeRadix != OSIX_FALSE)
    {
        pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11EHCCPrimeRadix =
            pWsscfgSetDot11PhyFHSSEntry->MibObject.i4Dot11EHCCPrimeRadix;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCNumberofChannelsFamilyIndex
        != OSIX_FALSE)
    {
        pWsscfgDot11PhyFHSSEntry->
            MibObject.i4Dot11EHCCNumberofChannelsFamilyIndex =
            pWsscfgSetDot11PhyFHSSEntry->
            MibObject.i4Dot11EHCCNumberofChannelsFamilyIndex;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->
        bDot11EHCCCapabilityImplemented != OSIX_FALSE)
    {
        pWsscfgDot11PhyFHSSEntry->
            MibObject.i4Dot11EHCCCapabilityImplemented =
            pWsscfgSetDot11PhyFHSSEntry->
            MibObject.i4Dot11EHCCCapabilityImplemented;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCCapabilityEnabled !=
        OSIX_FALSE)
    {
        pWsscfgDot11PhyFHSSEntry->MibObject.
            i4Dot11EHCCCapabilityEnabled =
            pWsscfgSetDot11PhyFHSSEntry->MibObject.i4Dot11EHCCCapabilityEnabled;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11HopAlgorithmAdopted != OSIX_FALSE)
    {
        pWsscfgDot11PhyFHSSEntry->MibObject.
            i4Dot11HopAlgorithmAdopted =
            pWsscfgSetDot11PhyFHSSEntry->MibObject.i4Dot11HopAlgorithmAdopted;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11RandomTableFlag != OSIX_FALSE)
    {
        pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11RandomTableFlag =
            pWsscfgSetDot11PhyFHSSEntry->MibObject.i4Dot11RandomTableFlag;
    }
    if (pWsscfgIsSetDot11PhyFHSSEntry->bDot11HopOffset != OSIX_FALSE)
    {
        pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11HopOffset =
            pWsscfgSetDot11PhyFHSSEntry->MibObject.i4Dot11HopOffset;
    }

    if (WsscfgUtilUpdateDot11PhyFHSSTable
        (&WsscfgOldDot11PhyFHSSEntry, pWsscfgDot11PhyFHSSEntry,
         pWsscfgIsSetDot11PhyFHSSEntry) != OSIX_SUCCESS)
    {
        if (WsscfgSetAllDot11PhyFHSSTableTrigger
            (pWsscfgSetDot11PhyFHSSEntry,
             pWsscfgIsSetDot11PhyFHSSEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pWsscfgDot11PhyFHSSEntry, &WsscfgOldDot11PhyFHSSEntry,
                sizeof (tWsscfgDot11PhyFHSSEntry));
        return OSIX_FAILURE;
    }

    if (WsscfgSetAllDot11PhyFHSSTableTrigger
        (pWsscfgSetDot11PhyFHSSEntry, pWsscfgIsSetDot11PhyFHSSEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11PhyDSSSTable
 Input       :  pWsscfgSetDot11PhyDSSSEntry
                pWsscfgIsSetDot11PhyDSSSEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllDot11PhyDSSSTable (tWsscfgDot11PhyDSSSEntry *
                               pWsscfgSetDot11PhyDSSSEntry,
                               tWsscfgIsSetDot11PhyDSSSEntry *
                               pWsscfgIsSetDot11PhyDSSSEntry)
{
    tWsscfgDot11PhyDSSSEntry WsscfgOldDot11PhyDSSSEntry;

    MEMSET (&WsscfgOldDot11PhyDSSSEntry, 0, sizeof (tWsscfgDot11PhyDSSSEntry));

    if (WsscfgUtilUpdateDot11PhyDSSSTable
        (&WsscfgOldDot11PhyDSSSEntry, pWsscfgSetDot11PhyDSSSEntry,
         pWsscfgIsSetDot11PhyDSSSEntry) != OSIX_SUCCESS)
    {

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11PhyIRTable
 Input       :  pWsscfgSetDot11PhyIREntry
                pWsscfgIsSetDot11PhyIREntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllDot11PhyIRTable (tWsscfgDot11PhyIREntry * pWsscfgSetDot11PhyIREntry,
                             tWsscfgIsSetDot11PhyIREntry *
                             pWsscfgIsSetDot11PhyIREntry)
{
    tWsscfgDot11PhyIREntry *pWsscfgDot11PhyIREntry = NULL;
    tWsscfgDot11PhyIREntry WsscfgOldDot11PhyIREntry;

    MEMSET (&WsscfgOldDot11PhyIREntry, 0, sizeof (tWsscfgDot11PhyIREntry));

    /* Check whether the node is already present */
    pWsscfgDot11PhyIREntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyIRTable,
                   (tRBElem *) pWsscfgSetDot11PhyIREntry);

    if (pWsscfgDot11PhyIREntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllDot11PhyIRTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /*Function to check whether the given input is same as there in database */
    if (Dot11PhyIRTableFilterInputs
        (pWsscfgDot11PhyIREntry, pWsscfgSetDot11PhyIREntry,
         pWsscfgIsSetDot11PhyIREntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&WsscfgOldDot11PhyIREntry, pWsscfgDot11PhyIREntry,
            sizeof (tWsscfgDot11PhyIREntry));

    /* Assign values for the existing node */
    if (pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogTimerMax != OSIX_FALSE)
    {
        pWsscfgDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogTimerMax =
            pWsscfgSetDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogTimerMax;
    }
    if (pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogCountMax != OSIX_FALSE)
    {
        pWsscfgDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogCountMax =
            pWsscfgSetDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogCountMax;
    }
    if (pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogTimerMin != OSIX_FALSE)
    {
        pWsscfgDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogTimerMin =
            pWsscfgSetDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogTimerMin;
    }
    if (pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogCountMin != OSIX_FALSE)
    {
        pWsscfgDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogCountMin =
            pWsscfgSetDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogCountMin;
    }

    if (WsscfgUtilUpdateDot11PhyIRTable
        (&WsscfgOldDot11PhyIREntry, pWsscfgDot11PhyIREntry,
         pWsscfgIsSetDot11PhyIREntry) != OSIX_SUCCESS)
    {
        if (WsscfgSetAllDot11PhyIRTableTrigger
            (pWsscfgSetDot11PhyIREntry, pWsscfgIsSetDot11PhyIREntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pWsscfgDot11PhyIREntry, &WsscfgOldDot11PhyIREntry,
                sizeof (tWsscfgDot11PhyIREntry));
        return OSIX_FAILURE;
    }

    if (WsscfgSetAllDot11PhyIRTableTrigger (pWsscfgSetDot11PhyIREntry,
                                            pWsscfgIsSetDot11PhyIREntry,
                                            SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11AntennasListTable
 Input       :  pWsscfgSetDot11AntennasListEntry
                pWsscfgIsSetDot11AntennasListEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllDot11AntennasListTable (tWsscfgDot11AntennasListEntry *
                                    pWsscfgSetDot11AntennasListEntry,
                                    tWsscfgIsSetDot11AntennasListEntry *
                                    pWsscfgIsSetDot11AntennasListEntry)
{
    tWsscfgDot11AntennasListEntry WsscfgOldDot11AntennasListEntry;

    MEMSET (&WsscfgOldDot11AntennasListEntry, 0,
            sizeof (tWsscfgDot11AntennasListEntry));
    if (WsscfgUtilUpdateDot11AntennasListTable
        (&WsscfgOldDot11AntennasListEntry,
         pWsscfgSetDot11AntennasListEntry,
         pWsscfgIsSetDot11AntennasListEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11PhyOFDMTable
 Input       :  pWsscfgSetDot11PhyOFDMEntry
                pWsscfgIsSetDot11PhyOFDMEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllDot11PhyOFDMTable (tWsscfgDot11PhyOFDMEntry *
                               pWsscfgSetDot11PhyOFDMEntry,
                               tWsscfgIsSetDot11PhyOFDMEntry *
                               pWsscfgIsSetDot11PhyOFDMEntry)
{
    tWsscfgDot11PhyOFDMEntry WsscfgOldDot11PhyOFDMEntry;

    MEMSET (&WsscfgOldDot11PhyOFDMEntry, 0, sizeof (tWsscfgDot11PhyOFDMEntry));

    if (WsscfgUtilUpdateDot11PhyOFDMTable
        (&WsscfgOldDot11PhyOFDMEntry, pWsscfgSetDot11PhyOFDMEntry,
         pWsscfgIsSetDot11PhyOFDMEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11HoppingPatternTable
 Input       :  pWsscfgSetDot11HoppingPatternEntry
                pWsscfgIsSetDot11HoppingPatternEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllDot11HoppingPatternTable (tWsscfgDot11HoppingPatternEntry *
                                      pWsscfgSetDot11HoppingPatternEntry,
                                      tWsscfgIsSetDot11HoppingPatternEntry *
                                      pWsscfgIsSetDot11HoppingPatternEntry)
{
    tWsscfgDot11HoppingPatternEntry *pWsscfgDot11HoppingPatternEntry = NULL;
    tWsscfgDot11HoppingPatternEntry WsscfgOldDot11HoppingPatternEntry;

    MEMSET (&WsscfgOldDot11HoppingPatternEntry, 0,
            sizeof (tWsscfgDot11HoppingPatternEntry));

    /* Check whether the node is already present */
    pWsscfgDot11HoppingPatternEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.
                   Dot11HoppingPatternTable,
                   (tRBElem *) pWsscfgSetDot11HoppingPatternEntry);

    if (pWsscfgDot11HoppingPatternEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllDot11HoppingPatternTable:"
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /*Function to check whether the given input is same as there in database */
    if (Dot11HoppingPatternTableFilterInputs
        (pWsscfgDot11HoppingPatternEntry,
         pWsscfgSetDot11HoppingPatternEntry,
         pWsscfgIsSetDot11HoppingPatternEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&WsscfgOldDot11HoppingPatternEntry,
            pWsscfgDot11HoppingPatternEntry,
            sizeof (tWsscfgDot11HoppingPatternEntry));

    /* Assign values for the existing node */
    if (pWsscfgIsSetDot11HoppingPatternEntry->bDot11RandomTableFieldNumber
        != OSIX_FALSE)
    {
        pWsscfgDot11HoppingPatternEntry->
            MibObject.i4Dot11RandomTableFieldNumber =
            pWsscfgSetDot11HoppingPatternEntry->
            MibObject.i4Dot11RandomTableFieldNumber;
    }

    if (WsscfgUtilUpdateDot11HoppingPatternTable
        (&WsscfgOldDot11HoppingPatternEntry,
         pWsscfgDot11HoppingPatternEntry,
         pWsscfgIsSetDot11HoppingPatternEntry) != OSIX_SUCCESS)
    {
        if (WsscfgSetAllDot11HoppingPatternTableTrigger
            (pWsscfgSetDot11HoppingPatternEntry,
             pWsscfgIsSetDot11HoppingPatternEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pWsscfgDot11HoppingPatternEntry,
                &WsscfgOldDot11HoppingPatternEntry,
                sizeof (tWsscfgDot11HoppingPatternEntry));
        return OSIX_FAILURE;
    }

    if (WsscfgSetAllDot11HoppingPatternTableTrigger
        (pWsscfgSetDot11HoppingPatternEntry,
         pWsscfgIsSetDot11HoppingPatternEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11PhyERPTable
 Input       :  pWsscfgSetDot11PhyERPEntry
                pWsscfgIsSetDot11PhyERPEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllDot11PhyERPTable (tWsscfgDot11PhyERPEntry *
                              pWsscfgSetDot11PhyERPEntry,
                              tWsscfgIsSetDot11PhyERPEntry *
                              pWsscfgIsSetDot11PhyERPEntry)
{
    tWsscfgDot11PhyERPEntry *pWsscfgDot11PhyERPEntry = NULL;
    tWsscfgDot11PhyERPEntry WsscfgOldDot11PhyERPEntry;

    MEMSET (&WsscfgOldDot11PhyERPEntry, 0, sizeof (tWsscfgDot11PhyERPEntry));

    /* Check whether the node is already present */
    pWsscfgDot11PhyERPEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11PhyERPTable,
                   (tRBElem *) pWsscfgSetDot11PhyERPEntry);

    if (pWsscfgDot11PhyERPEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllDot11PhyERPTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /*Function to check whether the given input is same as there in database */
    if (Dot11PhyERPTableFilterInputs
        (pWsscfgDot11PhyERPEntry, pWsscfgSetDot11PhyERPEntry,
         pWsscfgIsSetDot11PhyERPEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&WsscfgOldDot11PhyERPEntry, pWsscfgDot11PhyERPEntry,
            sizeof (tWsscfgDot11PhyERPEntry));

    /* Assign values for the existing node */
    if (pWsscfgIsSetDot11PhyERPEntry->bDot11ERPBCCOptionEnabled != OSIX_FALSE)
    {
        pWsscfgDot11PhyERPEntry->MibObject.
            i4Dot11ERPBCCOptionEnabled =
            pWsscfgSetDot11PhyERPEntry->MibObject.i4Dot11ERPBCCOptionEnabled;
    }
    if (pWsscfgIsSetDot11PhyERPEntry->bDot11DSSSOFDMOptionEnabled != OSIX_FALSE)
    {
        pWsscfgDot11PhyERPEntry->MibObject.
            i4Dot11DSSSOFDMOptionEnabled =
            pWsscfgSetDot11PhyERPEntry->MibObject.i4Dot11DSSSOFDMOptionEnabled;
    }
    if (pWsscfgIsSetDot11PhyERPEntry->bDot11ShortSlotTimeOptionImplemented
        != OSIX_FALSE)
    {
        pWsscfgDot11PhyERPEntry->
            MibObject.i4Dot11ShortSlotTimeOptionImplemented =
            pWsscfgSetDot11PhyERPEntry->
            MibObject.i4Dot11ShortSlotTimeOptionImplemented;
    }
    if (pWsscfgIsSetDot11PhyERPEntry->
        bDot11ShortSlotTimeOptionEnabled != OSIX_FALSE)
    {
        pWsscfgDot11PhyERPEntry->
            MibObject.i4Dot11ShortSlotTimeOptionEnabled =
            pWsscfgSetDot11PhyERPEntry->
            MibObject.i4Dot11ShortSlotTimeOptionEnabled;
    }

    if (WsscfgUtilUpdateDot11PhyERPTable
        (&WsscfgOldDot11PhyERPEntry, pWsscfgDot11PhyERPEntry,
         pWsscfgIsSetDot11PhyERPEntry) != OSIX_SUCCESS)
    {
        if (WsscfgSetAllDot11PhyERPTableTrigger
            (pWsscfgSetDot11PhyERPEntry, pWsscfgIsSetDot11PhyERPEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pWsscfgDot11PhyERPEntry, &WsscfgOldDot11PhyERPEntry,
                sizeof (tWsscfgDot11PhyERPEntry));
        return OSIX_FAILURE;
    }

    if (WsscfgSetAllDot11PhyERPTableTrigger
        (pWsscfgSetDot11PhyERPEntry, pWsscfgIsSetDot11PhyERPEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11RSNAConfigTable
 Input       :  pWsscfgSetDot11RSNAConfigEntry
                pWsscfgIsSetDot11RSNAConfigEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllDot11RSNAConfigTable (tWsscfgDot11RSNAConfigEntry *
                                  pWsscfgSetDot11RSNAConfigEntry,
                                  tWsscfgIsSetDot11RSNAConfigEntry *
                                  pWsscfgIsSetDot11RSNAConfigEntry)
{
    tWsscfgDot11RSNAConfigEntry *pWsscfgDot11RSNAConfigEntry = NULL;
    tWsscfgDot11RSNAConfigEntry WsscfgOldDot11RSNAConfigEntry;

    MEMSET (&WsscfgOldDot11RSNAConfigEntry, 0,
            sizeof (tWsscfgDot11RSNAConfigEntry));

    /* Check whether the node is already present */
    pWsscfgDot11RSNAConfigEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.Dot11RSNAConfigTable,
                   (tRBElem *) pWsscfgSetDot11RSNAConfigEntry);

    if (pWsscfgDot11RSNAConfigEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllDot11RSNAConfigTable:"
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /*Function to check whether the given input is same as there in database */
    if (Dot11RSNAConfigTableFilterInputs
        (pWsscfgDot11RSNAConfigEntry, pWsscfgSetDot11RSNAConfigEntry,
         pWsscfgIsSetDot11RSNAConfigEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&WsscfgOldDot11RSNAConfigEntry,
            pWsscfgDot11RSNAConfigEntry, sizeof (tWsscfgDot11RSNAConfigEntry));

    /* Assign values for the existing node */
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigGroupCipher != OSIX_FALSE)
    {
        MEMCPY (pWsscfgDot11RSNAConfigEntry->
                MibObject.au1Dot11RSNAConfigGroupCipher,
                pWsscfgSetDot11RSNAConfigEntry->
                MibObject.au1Dot11RSNAConfigGroupCipher,
                pWsscfgSetDot11RSNAConfigEntry->
                MibObject.i4Dot11RSNAConfigGroupCipherLen);

        pWsscfgDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAConfigGroupCipherLen =
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAConfigGroupCipherLen;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyMethod
        != OSIX_FALSE)
    {
        pWsscfgDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAConfigGroupRekeyMethod =
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAConfigGroupRekeyMethod;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigGroupRekeyTime != OSIX_FALSE)
    {
        pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigGroupRekeyTime =
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigGroupRekeyTime;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyPackets
        != OSIX_FALSE)
    {
        pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigGroupRekeyPackets =
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigGroupRekeyPackets;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyStrict
        != OSIX_FALSE)
    {
        pWsscfgDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAConfigGroupRekeyStrict =
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAConfigGroupRekeyStrict;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPSKValue !=
        OSIX_FALSE)
    {
        MEMCPY (pWsscfgDot11RSNAConfigEntry->
                MibObject.au1Dot11RSNAConfigPSKValue,
                pWsscfgSetDot11RSNAConfigEntry->
                MibObject.au1Dot11RSNAConfigPSKValue,
                pWsscfgSetDot11RSNAConfigEntry->
                MibObject.i4Dot11RSNAConfigPSKValueLen);

        pWsscfgDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAConfigPSKValueLen =
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAConfigPSKValueLen;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigPSKPassPhrase != OSIX_FALSE)
    {
        MEMCPY (pWsscfgDot11RSNAConfigEntry->
                MibObject.au1Dot11RSNAConfigPSKPassPhrase,
                pWsscfgSetDot11RSNAConfigEntry->
                MibObject.au1Dot11RSNAConfigPSKPassPhrase,
                pWsscfgSetDot11RSNAConfigEntry->
                MibObject.i4Dot11RSNAConfigPSKPassPhraseLen);

        pWsscfgDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAConfigPSKPassPhraseLen =
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.i4Dot11RSNAConfigPSKPassPhraseLen;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupUpdateCount
        != OSIX_FALSE)
    {
        pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigGroupUpdateCount =
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigGroupUpdateCount;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPairwiseUpdateCount
        != OSIX_FALSE)
    {
        pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigPairwiseUpdateCount =
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigPairwiseUpdateCount;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigPMKLifetime != OSIX_FALSE)
    {
        pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigPMKLifetime =
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigPMKLifetime;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPMKReauthThreshold
        != OSIX_FALSE)
    {
        pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigPMKReauthThreshold =
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigPMKReauthThreshold;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigSATimeout !=
        OSIX_FALSE)
    {
        pWsscfgDot11RSNAConfigEntry->MibObject.
            u4Dot11RSNAConfigSATimeout =
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigSATimeout;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNATKIPCounterMeasuresInvoked
        != OSIX_FALSE)
    {
        pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNATKIPCounterMeasuresInvoked =
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNATKIPCounterMeasuresInvoked;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNA4WayHandshakeFailures
        != OSIX_FALSE)
    {
        pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNA4WayHandshakeFailures =
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNA4WayHandshakeFailures;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigSTKRekeyTime != OSIX_FALSE)
    {
        pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigSTKRekeyTime =
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigSTKRekeyTime;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigSMKUpdateCount != OSIX_FALSE)
    {
        pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigSMKUpdateCount =
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigSMKUpdateCount;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNAConfigSMKLifetime != OSIX_FALSE)
    {
        pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigSMKLifetime =
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNAConfigSMKLifetime;
    }
    if (pWsscfgIsSetDot11RSNAConfigEntry->
        bDot11RSNASMKHandshakeFailures != OSIX_FALSE)
    {
        pWsscfgDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNASMKHandshakeFailures =
            pWsscfgSetDot11RSNAConfigEntry->
            MibObject.u4Dot11RSNASMKHandshakeFailures;
    }

    if (WsscfgUtilUpdateDot11RSNAConfigTable
        (&WsscfgOldDot11RSNAConfigEntry, pWsscfgDot11RSNAConfigEntry,
         pWsscfgIsSetDot11RSNAConfigEntry) != OSIX_SUCCESS)
    {
        if (WsscfgSetAllDot11RSNAConfigTableTrigger
            (pWsscfgSetDot11RSNAConfigEntry,
             pWsscfgIsSetDot11RSNAConfigEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pWsscfgDot11RSNAConfigEntry,
                &WsscfgOldDot11RSNAConfigEntry,
                sizeof (tWsscfgDot11RSNAConfigEntry));
        return OSIX_FAILURE;
    }

    if (WsscfgSetAllDot11RSNAConfigTableTrigger
        (pWsscfgSetDot11RSNAConfigEntry,
         pWsscfgIsSetDot11RSNAConfigEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11RSNAConfigPairwiseCiphersTable
 Input       :  pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry
                pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgSetAllDot11RSNAConfigPairwiseCiphersTable
    (tWsscfgDot11RSNAConfigPairwiseCiphersEntry *
     pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry,
     tWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry *
     pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry)
{
    tWsscfgDot11RSNAConfigPairwiseCiphersEntry
        * pWsscfgDot11RSNAConfigPairwiseCiphersEntry = NULL;
    tWsscfgDot11RSNAConfigPairwiseCiphersEntry
        WsscfgOldDot11RSNAConfigPairwiseCiphersEntry;

    MEMSET (&WsscfgOldDot11RSNAConfigPairwiseCiphersEntry, 0,
            sizeof (tWsscfgDot11RSNAConfigPairwiseCiphersEntry));

    /* Check whether the node is already present */
    pWsscfgDot11RSNAConfigPairwiseCiphersEntry =
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.Dot11RSNAConfigPairwiseCiphersTable,
                   (tRBElem *) pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry);

    if (pWsscfgDot11RSNAConfigPairwiseCiphersEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllDot11RSNAConfigPairwiseCiphersTable: Entry"
                     "doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /*Function to check whether the given input is same as there in database */
    if (Dot11RSNAConfigPairwiseCiphersTableFilterInputs
        (pWsscfgDot11RSNAConfigPairwiseCiphersEntry,
         pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry,
         pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&WsscfgOldDot11RSNAConfigPairwiseCiphersEntry,
            pWsscfgDot11RSNAConfigPairwiseCiphersEntry,
            sizeof (tWsscfgDot11RSNAConfigPairwiseCiphersEntry));

    /* Assign values for the existing node */
    if (pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry->
        bDot11RSNAConfigPairwiseCipherEnabled != OSIX_FALSE)
    {
        pWsscfgDot11RSNAConfigPairwiseCiphersEntry->
            MibObject.i4Dot11RSNAConfigPairwiseCipherEnabled =
            pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry->
            MibObject.i4Dot11RSNAConfigPairwiseCipherEnabled;
    }

    if (WsscfgUtilUpdateDot11RSNAConfigPairwiseCiphersTable
        (&WsscfgOldDot11RSNAConfigPairwiseCiphersEntry,
         pWsscfgDot11RSNAConfigPairwiseCiphersEntry,
         pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry) != OSIX_SUCCESS)
    {
        if (WsscfgSetAllDot11RSNAConfigPairwiseCiphersTableTrigger
            (pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry,
             pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pWsscfgDot11RSNAConfigPairwiseCiphersEntry,
                &WsscfgOldDot11RSNAConfigPairwiseCiphersEntry,
                sizeof (tWsscfgDot11RSNAConfigPairwiseCiphersEntry));
        return OSIX_FAILURE;
    }

    if (WsscfgSetAllDot11RSNAConfigPairwiseCiphersTableTrigger
        (pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry,
         pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllDot11RSNAConfigAuthenticationSuitesTable
 Input       :  pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry
                pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgSetAllDot11RSNAConfigAuthenticationSuitesTable
    (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *
     pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry,
     tWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry *
     pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry)
{
    tWsscfgDot11RSNAConfigAuthenticationSuitesEntry
        * pWsscfgDot11RSNAConfigAuthenticationSuitesEntry = NULL;
    tWsscfgDot11RSNAConfigAuthenticationSuitesEntry
        WsscfgOldDot11RSNAConfigAuthenticationSuitesEntry;

    MEMSET (&WsscfgOldDot11RSNAConfigAuthenticationSuitesEntry, 0,
            sizeof (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry));

    /* Check whether the node is already present */
    pWsscfgDot11RSNAConfigAuthenticationSuitesEntry =
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.Dot11RSNAConfigAuthenticationSuitesTable,
                   (tRBElem *)
                   pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry);

    if (pWsscfgDot11RSNAConfigAuthenticationSuitesEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllDot11RSNAConfigAuthenticationSuitesTable:"
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /*Function to check whether the given input is same as there in database */
    if (Dot11RSNAConfigAuthenticationSuitesTableFilterInputs
        (pWsscfgDot11RSNAConfigAuthenticationSuitesEntry,
         pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry,
         pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&WsscfgOldDot11RSNAConfigAuthenticationSuitesEntry,
            pWsscfgDot11RSNAConfigAuthenticationSuitesEntry,
            sizeof (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry));

    /* Assign values for the existing node */
    if (pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry->
        bDot11RSNAConfigAuthenticationSuiteEnabled != OSIX_FALSE)
    {
        pWsscfgDot11RSNAConfigAuthenticationSuitesEntry->
            MibObject.i4Dot11RSNAConfigAuthenticationSuiteEnabled =
            pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry->
            MibObject.i4Dot11RSNAConfigAuthenticationSuiteEnabled;
    }

    if (WsscfgUtilUpdateDot11RSNAConfigAuthenticationSuitesTable
        (&WsscfgOldDot11RSNAConfigAuthenticationSuitesEntry,
         pWsscfgDot11RSNAConfigAuthenticationSuitesEntry,
         pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry) != OSIX_SUCCESS)
    {
        if (WsscfgSetAllDot11RSNAConfigAuthenticationSuitesTableTrigger
            (pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry,
             pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pWsscfgDot11RSNAConfigAuthenticationSuitesEntry,
                &WsscfgOldDot11RSNAConfigAuthenticationSuitesEntry,
                sizeof (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry));
        return OSIX_FAILURE;
    }

    if (WsscfgSetAllDot11RSNAConfigAuthenticationSuitesTableTrigger
        (pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry,
         pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllCapwapDot11WlanTable
 Input       :  pWsscfgGetCapwapDot11WlanEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllCapwapDot11WlanTable (tWsscfgCapwapDot11WlanEntry *
                                  pWsscfgGetCapwapDot11WlanEntry)
{
    tWsscfgCapwapDot11WlanEntry *pWsscfgCapwapDot11WlanEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgCapwapDot11WlanEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.CapwapDot11WlanTable,
                   (tRBElem *) pWsscfgGetCapwapDot11WlanEntry);

    if (pWsscfgCapwapDot11WlanEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllCapwapDot11WlanTable: Entry"
                     "doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetCapwapDot11WlanEntry->
        MibObject.i4CapwapDot11WlanProfileIfIndex =
        pWsscfgCapwapDot11WlanEntry->MibObject.i4CapwapDot11WlanProfileIfIndex;

    pWsscfgGetCapwapDot11WlanEntry->MibObject.
        i4CapwapDot11WlanMacType =
        pWsscfgCapwapDot11WlanEntry->MibObject.i4CapwapDot11WlanMacType;

    MEMCPY (pWsscfgGetCapwapDot11WlanEntry->MibObject.
            au1CapwapDot11WlanTunnelMode,
            pWsscfgCapwapDot11WlanEntry->
            MibObject.au1CapwapDot11WlanTunnelMode,
            pWsscfgCapwapDot11WlanEntry->MibObject.
            i4CapwapDot11WlanTunnelModeLen);

    pWsscfgGetCapwapDot11WlanEntry->
        MibObject.i4CapwapDot11WlanTunnelModeLen =
        pWsscfgCapwapDot11WlanEntry->MibObject.i4CapwapDot11WlanTunnelModeLen;

    pWsscfgGetCapwapDot11WlanEntry->MibObject.
        i4CapwapDot11WlanRowStatus =
        pWsscfgCapwapDot11WlanEntry->MibObject.i4CapwapDot11WlanRowStatus;

    if (WsscfgGetAllUtlCapwapDot11WlanTable
        (pWsscfgGetCapwapDot11WlanEntry,
         pWsscfgCapwapDot11WlanEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllCapwapDot11WlanTable:"
                     "WsscfgGetAllUtlCapwapDot11WlanTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllCapwapDot11WlanBindTable
 Input       :  pWsscfgGetCapwapDot11WlanBindEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllCapwapDot11WlanBindTable (tWsscfgCapwapDot11WlanBindEntry *
                                      pWsscfgGetCapwapDot11WlanBindEntry)
{
    tWsscfgCapwapDot11WlanBindEntry *pWsscfgCapwapDot11WlanBindEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgCapwapDot11WlanBindEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.
                   CapwapDot11WlanBindTable,
                   (tRBElem *) pWsscfgGetCapwapDot11WlanBindEntry);

    if (pWsscfgCapwapDot11WlanBindEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllCapwapDot11WlanBindTable:"
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetCapwapDot11WlanBindEntry->
        MibObject.u4CapwapDot11WlanBindWlanId =
        pWsscfgCapwapDot11WlanBindEntry->MibObject.u4CapwapDot11WlanBindWlanId;

    pWsscfgGetCapwapDot11WlanBindEntry->MibObject.
        i4CapwapDot11WlanBindBssIfIndex =
        pWsscfgCapwapDot11WlanBindEntry->MibObject.
        i4CapwapDot11WlanBindBssIfIndex;

    pWsscfgGetCapwapDot11WlanBindEntry->MibObject.
        i4CapwapDot11WlanBindRowStatus =
        pWsscfgCapwapDot11WlanBindEntry->MibObject.
        i4CapwapDot11WlanBindRowStatus;

    if (WsscfgGetAllUtlCapwapDot11WlanBindTable
        (pWsscfgGetCapwapDot11WlanBindEntry,
         pWsscfgCapwapDot11WlanBindEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllCapwapDot11WlanBindTable:"
                     "WsscfgGetAllUtlCapwapDot11WlanBindTable Returns"
                     "Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllCapwapDot11WlanTable
 Input       :  pWsscfgSetCapwapDot11WlanEntry
                pWsscfgIsSetCapwapDot11WlanEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllCapwapDot11WlanTable (tWsscfgCapwapDot11WlanEntry *
                                  pWsscfgSetCapwapDot11WlanEntry,
                                  tWsscfgIsSetCapwapDot11WlanEntry *
                                  pWsscfgIsSetCapwapDot11WlanEntry,
                                  INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tWsscfgCapwapDot11WlanEntry *pWsscfgCapwapDot11WlanEntry = NULL;
    tWsscfgCapwapDot11WlanEntry *pWsscfgOldCapwapDot11WlanEntry = NULL;
    tWsscfgCapwapDot11WlanEntry *pWsscfgTrgCapwapDot11WlanEntry = NULL;
    tWsscfgIsSetCapwapDot11WlanEntry
        * pWsscfgTrgIsSetCapwapDot11WlanEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pWsscfgOldCapwapDot11WlanEntry = (tWsscfgCapwapDot11WlanEntry *)
        MemAllocMemBlk (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID);
    if (pWsscfgOldCapwapDot11WlanEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWsscfgTrgCapwapDot11WlanEntry = (tWsscfgCapwapDot11WlanEntry *)
        MemAllocMemBlk (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID);
    if (pWsscfgTrgCapwapDot11WlanEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgOldCapwapDot11WlanEntry);
        return OSIX_FAILURE;
    }
    pWsscfgTrgIsSetCapwapDot11WlanEntry =
        (tWsscfgIsSetCapwapDot11WlanEntry *)
        MemAllocMemBlk (WSSCFG_CAPWAPDOT11WLANTABLE_ISSET_POOLID);
    if (pWsscfgTrgIsSetCapwapDot11WlanEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgOldCapwapDot11WlanEntry);
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgCapwapDot11WlanEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pWsscfgOldCapwapDot11WlanEntry, 0,
            sizeof (tWsscfgCapwapDot11WlanEntry));
    MEMSET (pWsscfgTrgCapwapDot11WlanEntry, 0,
            sizeof (tWsscfgCapwapDot11WlanEntry));
    MEMSET (pWsscfgTrgIsSetCapwapDot11WlanEntry, 0,
            sizeof (tWsscfgIsSetCapwapDot11WlanEntry));

    /* Check whether the node is already present */
    pWsscfgCapwapDot11WlanEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.CapwapDot11WlanTable,
                   (tRBElem *) pWsscfgSetCapwapDot11WlanEntry);

    if (pWsscfgCapwapDot11WlanEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or 
         * CREATE_AND_GO */
        if ((pWsscfgSetCapwapDot11WlanEntry->MibObject.
             i4CapwapDot11WlanRowStatus == CREATE_AND_WAIT)
            || (pWsscfgSetCapwapDot11WlanEntry->MibObject.
                i4CapwapDot11WlanRowStatus == CREATE_AND_GO)
            ||
            ((pWsscfgSetCapwapDot11WlanEntry->MibObject.
              i4CapwapDot11WlanRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pWsscfgCapwapDot11WlanEntry =
                (tWsscfgCapwapDot11WlanEntry *)
                MemAllocMemBlk (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID);
            if (pWsscfgCapwapDot11WlanEntry == NULL)
            {
                if (WsscfgSetAllCapwapDot11WlanTableTrigger
                    (pWsscfgSetCapwapDot11WlanEntry,
                     pWsscfgIsSetCapwapDot11WlanEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllCapwapDot11WlanTable:"
                                 "WsscfgSetAllCapwapDot11WlanTableTrigger function"
                                 "fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllCapwapDot11WlanTable: Fail to Allocate"
                             "Memory.\r\n"));
                MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                                    (UINT1 *) pWsscfgOldCapwapDot11WlanEntry);
                MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                                    (UINT1 *) pWsscfgTrgCapwapDot11WlanEntry);
                MemReleaseMemBlock
                    (WSSCFG_CAPWAPDOT11WLANTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetCapwapDot11WlanEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pWsscfgCapwapDot11WlanEntry, 0,
                    sizeof (tWsscfgCapwapDot11WlanEntry));
            if ((WsscfgInitializeCapwapDot11WlanTable
                 (pWsscfgCapwapDot11WlanEntry)) == OSIX_FAILURE)
            {
                if (WsscfgSetAllCapwapDot11WlanTableTrigger
                    (pWsscfgSetCapwapDot11WlanEntry,
                     pWsscfgIsSetCapwapDot11WlanEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllCapwapDot11WlanTable:WsscfgSetAll"
                                 "CapwapDot11WlanTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllCapwapDot11WlanTable: Fail to"
                             "Initialize the Objects.\r\n"));

                MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                                    (UINT1 *) pWsscfgCapwapDot11WlanEntry);
                MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                                    (UINT1 *) pWsscfgOldCapwapDot11WlanEntry);
                MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                                    (UINT1 *) pWsscfgTrgCapwapDot11WlanEntry);
                MemReleaseMemBlock
                    (WSSCFG_CAPWAPDOT11WLANTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetCapwapDot11WlanEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanProfileId
                != OSIX_FALSE)
            {
                pWsscfgCapwapDot11WlanEntry->MibObject.
                    u4CapwapDot11WlanProfileId =
                    pWsscfgSetCapwapDot11WlanEntry->MibObject.
                    u4CapwapDot11WlanProfileId;
            }

            if (pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanMacType
                != OSIX_FALSE)
            {
                pWsscfgCapwapDot11WlanEntry->MibObject.
                    i4CapwapDot11WlanMacType =
                    pWsscfgSetCapwapDot11WlanEntry->MibObject.
                    i4CapwapDot11WlanMacType;
            }

            if (pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanTunnelMode
                != OSIX_FALSE)
            {
                MEMCPY (pWsscfgCapwapDot11WlanEntry->MibObject.
                        au1CapwapDot11WlanTunnelMode,
                        pWsscfgSetCapwapDot11WlanEntry->MibObject.
                        au1CapwapDot11WlanTunnelMode,
                        pWsscfgSetCapwapDot11WlanEntry->MibObject.
                        i4CapwapDot11WlanTunnelModeLen);

                pWsscfgCapwapDot11WlanEntry->MibObject.
                    i4CapwapDot11WlanTunnelModeLen =
                    pWsscfgSetCapwapDot11WlanEntry->MibObject.
                    i4CapwapDot11WlanTunnelModeLen;
            }

            if (pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanRowStatus
                != OSIX_FALSE)
            {
                pWsscfgCapwapDot11WlanEntry->MibObject.
                    i4CapwapDot11WlanRowStatus =
                    pWsscfgSetCapwapDot11WlanEntry->MibObject.
                    i4CapwapDot11WlanRowStatus;
            }

            if ((pWsscfgSetCapwapDot11WlanEntry->MibObject.
                 i4CapwapDot11WlanRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetCapwapDot11WlanEntry->MibObject.
                        i4CapwapDot11WlanRowStatus == ACTIVE)))
            {
                pWsscfgCapwapDot11WlanEntry->MibObject.
                    i4CapwapDot11WlanRowStatus = ACTIVE;
            }
            else if (pWsscfgSetCapwapDot11WlanEntry->MibObject.
                     i4CapwapDot11WlanRowStatus == CREATE_AND_WAIT)
            {
                pWsscfgCapwapDot11WlanEntry->MibObject.
                    i4CapwapDot11WlanRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gWsscfgGlobals.WsscfgGlbMib.CapwapDot11WlanTable,
                 (tRBElem *) pWsscfgCapwapDot11WlanEntry) != RB_SUCCESS)
            {
                if (WsscfgSetAllCapwapDot11WlanTableTrigger
                    (pWsscfgSetCapwapDot11WlanEntry,
                     pWsscfgIsSetCapwapDot11WlanEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllCapwapDot11WlanTable:"
                                 "WsscfgSetAllCapwapDot11WlanTableTrigger"
                                 "function returns failure.\r\n"));
                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllCapwapDot11WlanTable: RBTreeAdd is"
                             "failed.\r\n"));

                MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                                    (UINT1 *) pWsscfgCapwapDot11WlanEntry);
                MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                                    (UINT1 *) pWsscfgOldCapwapDot11WlanEntry);
                MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                                    (UINT1 *) pWsscfgTrgCapwapDot11WlanEntry);
                MemReleaseMemBlock
                    (WSSCFG_CAPWAPDOT11WLANTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetCapwapDot11WlanEntry);
                return OSIX_FAILURE;
            }
            if (WsscfgUtilUpdateCapwapDot11WlanTable
                (NULL, pWsscfgCapwapDot11WlanEntry,
                 pWsscfgIsSetCapwapDot11WlanEntry) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllCapwapDot11WlanTable:"
                             "WsscfgUtilUpdateCapwapDot11WlanTable function"
                             "returns failure.\r\n"));

                if (WsscfgSetAllCapwapDot11WlanTableTrigger
                    (pWsscfgSetCapwapDot11WlanEntry,
                     pWsscfgIsSetCapwapDot11WlanEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllCapwapDot11WlanTable:"
                                 "WsscfgSetAllCapwapDot11WlanTableTrigger function"
                                 "returns failure.\r\n"));

                }
                RBTreeRem (gWsscfgGlobals.WsscfgGlbMib.
                           CapwapDot11WlanTable, pWsscfgCapwapDot11WlanEntry);
                MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                                    (UINT1 *) pWsscfgCapwapDot11WlanEntry);
                MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                                    (UINT1 *) pWsscfgOldCapwapDot11WlanEntry);
                MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                                    (UINT1 *) pWsscfgTrgCapwapDot11WlanEntry);
                MemReleaseMemBlock
                    (WSSCFG_CAPWAPDOT11WLANTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetCapwapDot11WlanEntry);
                return OSIX_FAILURE;
            }

            if ((pWsscfgSetCapwapDot11WlanEntry->MibObject.
                 i4CapwapDot11WlanRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetCapwapDot11WlanEntry->MibObject.
                        i4CapwapDot11WlanRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgCapwapDot11WlanEntry->MibObject.
                    u4CapwapDot11WlanProfileId =
                    pWsscfgSetCapwapDot11WlanEntry->MibObject.
                    u4CapwapDot11WlanProfileId;
                pWsscfgTrgCapwapDot11WlanEntry->MibObject.
                    i4CapwapDot11WlanRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanRowStatus
                    = OSIX_TRUE;

                if (WsscfgSetAllCapwapDot11WlanTableTrigger
                    (pWsscfgTrgCapwapDot11WlanEntry,
                     pWsscfgTrgIsSetCapwapDot11WlanEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllCapwapDot11WlanTable:"
                                 "WsscfgSetAllCapwapDot11WlanTableTrigger function"
                                 "returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID, (UINT1 *)
                         pWsscfgCapwapDot11WlanEntry);
                    MemReleaseMemBlock
                        (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID, (UINT1 *)
                         pWsscfgOldCapwapDot11WlanEntry);
                    MemReleaseMemBlock
                        (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID, (UINT1 *)
                         pWsscfgTrgCapwapDot11WlanEntry);
                    MemReleaseMemBlock
                        (WSSCFG_CAPWAPDOT11WLANTABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetCapwapDot11WlanEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pWsscfgSetCapwapDot11WlanEntry->MibObject.
                     i4CapwapDot11WlanRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgCapwapDot11WlanEntry->MibObject.
                    i4CapwapDot11WlanRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanRowStatus
                    = OSIX_TRUE;

                if (WsscfgSetAllCapwapDot11WlanTableTrigger
                    (pWsscfgTrgCapwapDot11WlanEntry,
                     pWsscfgTrgIsSetCapwapDot11WlanEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllCapwapDot11WlanTable:"
                                 "WsscfgSetAllCapwapDot11WlanTableTrigger function"
                                 "returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID, (UINT1 *)
                         pWsscfgCapwapDot11WlanEntry);
                    MemReleaseMemBlock
                        (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID, (UINT1 *)
                         pWsscfgOldCapwapDot11WlanEntry);
                    MemReleaseMemBlock
                        (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID, (UINT1 *)
                         pWsscfgTrgCapwapDot11WlanEntry);
                    MemReleaseMemBlock
                        (WSSCFG_CAPWAPDOT11WLANTABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetCapwapDot11WlanEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pWsscfgSetCapwapDot11WlanEntry->MibObject.
                i4CapwapDot11WlanRowStatus == CREATE_AND_GO)
            {
                pWsscfgSetCapwapDot11WlanEntry->MibObject.
                    i4CapwapDot11WlanRowStatus = ACTIVE;
            }

            if (WsscfgSetAllCapwapDot11WlanTableTrigger
                (pWsscfgSetCapwapDot11WlanEntry,
                 pWsscfgIsSetCapwapDot11WlanEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllCapwapDot11WlanTable:"
                             "WsscfgSetAllCapwapDot11WlanTableTrigger function"
                             " returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                                (UINT1 *) pWsscfgOldCapwapDot11WlanEntry);
            MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgCapwapDot11WlanEntry);
            MemReleaseMemBlock
                (WSSCFG_CAPWAPDOT11WLANTABLE_ISSET_POOLID, (UINT1 *)
                 pWsscfgTrgIsSetCapwapDot11WlanEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (WsscfgSetAllCapwapDot11WlanTableTrigger
                (pWsscfgSetCapwapDot11WlanEntry,
                 pWsscfgIsSetCapwapDot11WlanEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllCapwapDot11WlanTable:"
                             "WsscfgSetAllCapwapDot11WlanTableTrigger function "
                             "returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllCapwapDot11WlanTable: Failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                                (UINT1 *) pWsscfgOldCapwapDot11WlanEntry);
            MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgCapwapDot11WlanEntry);
            MemReleaseMemBlock
                (WSSCFG_CAPWAPDOT11WLANTABLE_ISSET_POOLID, (UINT1 *)
                 pWsscfgTrgIsSetCapwapDot11WlanEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pWsscfgSetCapwapDot11WlanEntry->MibObject.
              i4CapwapDot11WlanRowStatus == CREATE_AND_WAIT)
             || (pWsscfgSetCapwapDot11WlanEntry->MibObject.
                 i4CapwapDot11WlanRowStatus == CREATE_AND_GO))
    {
        if (WsscfgSetAllCapwapDot11WlanTableTrigger
            (pWsscfgSetCapwapDot11WlanEntry,
             pWsscfgIsSetCapwapDot11WlanEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllCapwapDot11WlanTable: "
                         "WsscfgSetAllCapwapDot11WlanTableTrigger function"
                         "returns failure.\r\n"));
        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllCapwapDot11WlanTable: The row is already present.\r\n"));
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgOldCapwapDot11WlanEntry);
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgCapwapDot11WlanEntry);
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetCapwapDot11WlanEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pWsscfgOldCapwapDot11WlanEntry,
            pWsscfgCapwapDot11WlanEntry, sizeof (tWsscfgCapwapDot11WlanEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pWsscfgSetCapwapDot11WlanEntry->
        MibObject.i4CapwapDot11WlanRowStatus == DESTROY)
    {
        pWsscfgCapwapDot11WlanEntry->MibObject.
            i4CapwapDot11WlanRowStatus = DESTROY;

        if (WsscfgUtilUpdateCapwapDot11WlanTable
            (pWsscfgOldCapwapDot11WlanEntry,
             pWsscfgCapwapDot11WlanEntry,
             pWsscfgIsSetCapwapDot11WlanEntry) != OSIX_SUCCESS)
        {
            pWsscfgCapwapDot11WlanEntry->
                MibObject.i4CapwapDot11WlanRowStatus =
                pWsscfgOldCapwapDot11WlanEntry->
                MibObject.i4CapwapDot11WlanRowStatus;

            if (WsscfgSetAllCapwapDot11WlanTableTrigger
                (pWsscfgSetCapwapDot11WlanEntry,
                 pWsscfgIsSetCapwapDot11WlanEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllCapwapDot11WlanTable: "
                             "WsscfgSetAllCapwapDot11WlanTableTrigger function "
                             "returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllCapwapDot11WlanTable:"
                         "WsscfgUtilUpdateCapwapDot11WlanTable function "
                         "returns failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                                (UINT1 *) pWsscfgOldCapwapDot11WlanEntry);
            MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgCapwapDot11WlanEntry);
            MemReleaseMemBlock
                (WSSCFG_CAPWAPDOT11WLANTABLE_ISSET_POOLID, (UINT1 *)
                 pWsscfgTrgIsSetCapwapDot11WlanEntry);
            return OSIX_FAILURE;
        }
        RBTreeRem (gWsscfgGlobals.WsscfgGlbMib.CapwapDot11WlanTable,
                   pWsscfgCapwapDot11WlanEntry);
        if (WsscfgSetAllCapwapDot11WlanTableTrigger
            (pWsscfgSetCapwapDot11WlanEntry,
             pWsscfgIsSetCapwapDot11WlanEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllCapwapDot11WlanTable: "
                         "WsscfgSetAllCapwapDot11WlanTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                                (UINT1 *) pWsscfgOldCapwapDot11WlanEntry);
            MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgCapwapDot11WlanEntry);
            MemReleaseMemBlock
                (WSSCFG_CAPWAPDOT11WLANTABLE_ISSET_POOLID, (UINT1 *)
                 pWsscfgTrgIsSetCapwapDot11WlanEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgCapwapDot11WlanEntry);
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgOldCapwapDot11WlanEntry);
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgCapwapDot11WlanEntry);
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetCapwapDot11WlanEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (CapwapDot11WlanTableFilterInputs
        (pWsscfgCapwapDot11WlanEntry, pWsscfgSetCapwapDot11WlanEntry,
         pWsscfgIsSetCapwapDot11WlanEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgOldCapwapDot11WlanEntry);
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgCapwapDot11WlanEntry);
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetCapwapDot11WlanEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before
     * modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pWsscfgCapwapDot11WlanEntry->
         MibObject.i4CapwapDot11WlanRowStatus == ACTIVE)
        && (pWsscfgSetCapwapDot11WlanEntry->MibObject.
            i4CapwapDot11WlanRowStatus != NOT_IN_SERVICE))
    {
        pWsscfgCapwapDot11WlanEntry->MibObject.
            i4CapwapDot11WlanRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pWsscfgTrgCapwapDot11WlanEntry->
            MibObject.i4CapwapDot11WlanRowStatus = NOT_IN_SERVICE;
        pWsscfgTrgIsSetCapwapDot11WlanEntry->
            bCapwapDot11WlanRowStatus = OSIX_TRUE;

        if (WsscfgUtilUpdateCapwapDot11WlanTable
            (pWsscfgOldCapwapDot11WlanEntry,
             pWsscfgCapwapDot11WlanEntry,
             pWsscfgIsSetCapwapDot11WlanEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pWsscfgCapwapDot11WlanEntry,
                    pWsscfgOldCapwapDot11WlanEntry,
                    sizeof (tWsscfgCapwapDot11WlanEntry));
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllCapwapDot11WlanTable:"
                         "WsscfgUtilUpdateCapwapDot11WlanTable Function "
                         "returns failure.\r\n"));

            if (WsscfgSetAllCapwapDot11WlanTableTrigger
                (pWsscfgSetCapwapDot11WlanEntry,
                 pWsscfgIsSetCapwapDot11WlanEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllCapwapDot11WlanTable:"
                             "WsscfgSetAllCapwapDot11WlanTableTrigger "
                             "function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                                (UINT1 *) pWsscfgOldCapwapDot11WlanEntry);
            MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgCapwapDot11WlanEntry);
            MemReleaseMemBlock
                (WSSCFG_CAPWAPDOT11WLANTABLE_ISSET_POOLID, (UINT1 *)
                 pWsscfgTrgIsSetCapwapDot11WlanEntry);
            return OSIX_FAILURE;
        }

        if (WsscfgSetAllCapwapDot11WlanTableTrigger
            (pWsscfgTrgCapwapDot11WlanEntry,
             pWsscfgTrgIsSetCapwapDot11WlanEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllCapwapDot11WlanTable:"
                         "WsscfgSetAllCapwapDot11WlanTableTrigger function "
                         "returns failure.\r\n"));
        }
    }

    if (pWsscfgSetCapwapDot11WlanEntry->
        MibObject.i4CapwapDot11WlanRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanMacType != OSIX_FALSE)
    {
        pWsscfgCapwapDot11WlanEntry->MibObject.
            i4CapwapDot11WlanMacType =
            pWsscfgSetCapwapDot11WlanEntry->MibObject.i4CapwapDot11WlanMacType;
    }
    if (pWsscfgIsSetCapwapDot11WlanEntry->
        bCapwapDot11WlanTunnelMode != OSIX_FALSE)
    {
        MEMCPY (pWsscfgCapwapDot11WlanEntry->MibObject.
                au1CapwapDot11WlanTunnelMode,
                pWsscfgSetCapwapDot11WlanEntry->MibObject.
                au1CapwapDot11WlanTunnelMode,
                pWsscfgSetCapwapDot11WlanEntry->MibObject.
                i4CapwapDot11WlanTunnelModeLen);

        pWsscfgCapwapDot11WlanEntry->
            MibObject.i4CapwapDot11WlanTunnelModeLen =
            pWsscfgSetCapwapDot11WlanEntry->MibObject.
            i4CapwapDot11WlanTunnelModeLen;
    }
    if (pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanRowStatus !=
        OSIX_FALSE)
    {
        pWsscfgCapwapDot11WlanEntry->MibObject.
            i4CapwapDot11WlanRowStatus =
            pWsscfgSetCapwapDot11WlanEntry->MibObject.
            i4CapwapDot11WlanRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after
     * modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pWsscfgCapwapDot11WlanEntry->MibObject.
            i4CapwapDot11WlanRowStatus = ACTIVE;
    }

    if (WsscfgUtilUpdateCapwapDot11WlanTable
        (pWsscfgOldCapwapDot11WlanEntry, pWsscfgCapwapDot11WlanEntry,
         pWsscfgIsSetCapwapDot11WlanEntry) != OSIX_SUCCESS)
    {

        if (WsscfgSetAllCapwapDot11WlanTableTrigger
            (pWsscfgSetCapwapDot11WlanEntry,
             pWsscfgIsSetCapwapDot11WlanEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllCapwapDot11WlanTable:"
                         "WsscfgSetAllCapwapDot11WlanTableTrigger function returns failure.\r\n"));

        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllCapwapDot11WlanTable:"
                     "WsscfgUtilUpdateCapwapDot11WlanTable function "
                     "returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pWsscfgCapwapDot11WlanEntry,
                pWsscfgOldCapwapDot11WlanEntry,
                sizeof (tWsscfgCapwapDot11WlanEntry));
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgOldCapwapDot11WlanEntry);
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgCapwapDot11WlanEntry);
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetCapwapDot11WlanEntry);
        return OSIX_FAILURE;

    }
    if (WsscfgSetAllCapwapDot11WlanTableTrigger
        (pWsscfgSetCapwapDot11WlanEntry,
         pWsscfgIsSetCapwapDot11WlanEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllCapwapDot11WlanTable:"
                     "WsscfgSetAllCapwapDot11WlanTableTrigger function"
                     "returns failure.\r\n"));
    }

    MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                        (UINT1 *) pWsscfgOldCapwapDot11WlanEntry);
    MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_POOLID,
                        (UINT1 *) pWsscfgTrgCapwapDot11WlanEntry);
    MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANTABLE_ISSET_POOLID,
                        (UINT1 *) pWsscfgTrgIsSetCapwapDot11WlanEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgSetAllCapwapDot11WlanBindTable
 Input       :  pWsscfgSetCapwapDot11WlanBindEntry
                pWsscfgIsSetCapwapDot11WlanBindEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllCapwapDot11WlanBindTable (tWsscfgCapwapDot11WlanBindEntry *
                                      pWsscfgSetCapwapDot11WlanBindEntry,
                                      tWsscfgIsSetCapwapDot11WlanBindEntry *
                                      pWsscfgIsSetCapwapDot11WlanBindEntry,
                                      INT4 i4RowStatusLogic,
                                      INT4 i4RowCreateOption)
{
    tWsscfgCapwapDot11WlanBindEntry *pWsscfgCapwapDot11WlanBindEntry = NULL;
    tWsscfgCapwapDot11WlanBindEntry *pWsscfgOldCapwapDot11WlanBindEntry = NULL;
    tWsscfgCapwapDot11WlanBindEntry *pWsscfgTrgCapwapDot11WlanBindEntry = NULL;
    tWsscfgIsSetCapwapDot11WlanBindEntry
        * pWsscfgTrgIsSetCapwapDot11WlanBindEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pWsscfgOldCapwapDot11WlanBindEntry =
        (tWsscfgCapwapDot11WlanBindEntry *)
        MemAllocMemBlk (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID);
    if (pWsscfgOldCapwapDot11WlanBindEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWsscfgTrgCapwapDot11WlanBindEntry =
        (tWsscfgCapwapDot11WlanBindEntry *)
        MemAllocMemBlk (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID);
    if (pWsscfgTrgCapwapDot11WlanBindEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgOldCapwapDot11WlanBindEntry);
        return OSIX_FAILURE;
    }
    pWsscfgTrgIsSetCapwapDot11WlanBindEntry =
        (tWsscfgIsSetCapwapDot11WlanBindEntry *)
        MemAllocMemBlk (WSSCFG_CAPWAPDOT11WLANBINDTABLE_ISSET_POOLID);
    if (pWsscfgTrgIsSetCapwapDot11WlanBindEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgOldCapwapDot11WlanBindEntry);
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgCapwapDot11WlanBindEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pWsscfgOldCapwapDot11WlanBindEntry, 0,
            sizeof (tWsscfgCapwapDot11WlanBindEntry));
    MEMSET (pWsscfgTrgCapwapDot11WlanBindEntry, 0,
            sizeof (tWsscfgCapwapDot11WlanBindEntry));
    MEMSET (pWsscfgTrgIsSetCapwapDot11WlanBindEntry, 0,
            sizeof (tWsscfgIsSetCapwapDot11WlanBindEntry));

    /* Check whether the node is already present */
    pWsscfgCapwapDot11WlanBindEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.
                   CapwapDot11WlanBindTable,
                   (tRBElem *) pWsscfgSetCapwapDot11WlanBindEntry);

    if (pWsscfgCapwapDot11WlanBindEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or 
         * CREATE_AND_GO */
        if ((pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
             i4CapwapDot11WlanBindRowStatus == CREATE_AND_WAIT)
            || (pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
                i4CapwapDot11WlanBindRowStatus == CREATE_AND_GO)
            ||
            ((pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
              i4CapwapDot11WlanBindRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pWsscfgCapwapDot11WlanBindEntry =
                (tWsscfgCapwapDot11WlanBindEntry *)
                MemAllocMemBlk (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID);
            if (pWsscfgCapwapDot11WlanBindEntry == NULL)
            {
                if (WsscfgSetAllCapwapDot11WlanBindTableTrigger
                    (pWsscfgSetCapwapDot11WlanBindEntry,
                     pWsscfgIsSetCapwapDot11WlanBindEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllCapwapDot11WlanBindTable:"
                                 "WsscfgSetAllCapwapDot11WlanBindTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllCapwapDot11WlanBindTable: Fail to"
                             "Allocate Memory.\r\n"));
                MemReleaseMemBlock
                    (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID, (UINT1 *)
                     pWsscfgOldCapwapDot11WlanBindEntry);
                MemReleaseMemBlock
                    (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgCapwapDot11WlanBindEntry);
                MemReleaseMemBlock
                    (WSSCFG_CAPWAPDOT11WLANBINDTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetCapwapDot11WlanBindEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pWsscfgCapwapDot11WlanBindEntry, 0,
                    sizeof (tWsscfgCapwapDot11WlanBindEntry));
            if ((WsscfgInitializeCapwapDot11WlanBindTable
                 (pWsscfgCapwapDot11WlanBindEntry)) == OSIX_FAILURE)
            {
                if (WsscfgSetAllCapwapDot11WlanBindTableTrigger
                    (pWsscfgSetCapwapDot11WlanBindEntry,
                     pWsscfgIsSetCapwapDot11WlanBindEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllCapwapDot11WlanBindTable:WsscfgSetAllCapwapDot"
                                 "11WlanBindTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllCapwapDot11WlanBindTable: Fail to "
                             "Initialize the Objects.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID, (UINT1 *)
                     pWsscfgCapwapDot11WlanBindEntry);
                MemReleaseMemBlock
                    (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID, (UINT1 *)
                     pWsscfgOldCapwapDot11WlanBindEntry);
                MemReleaseMemBlock
                    (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgCapwapDot11WlanBindEntry);
                MemReleaseMemBlock
                    (WSSCFG_CAPWAPDOT11WLANBINDTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetCapwapDot11WlanBindEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pWsscfgIsSetCapwapDot11WlanBindEntry->
                bCapwapDot11WlanBindRowStatus != OSIX_FALSE)
            {
                pWsscfgCapwapDot11WlanBindEntry->MibObject.
                    i4CapwapDot11WlanBindRowStatus =
                    pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
                    i4CapwapDot11WlanBindRowStatus;
            }

            if (pWsscfgIsSetCapwapDot11WlanBindEntry->bIfIndex != OSIX_FALSE)
            {
                pWsscfgCapwapDot11WlanBindEntry->MibObject.i4IfIndex =
                    pWsscfgSetCapwapDot11WlanBindEntry->MibObject.i4IfIndex;
            }

            if (pWsscfgIsSetCapwapDot11WlanBindEntry->
                bCapwapDot11WlanProfileId != OSIX_FALSE)
            {
                pWsscfgCapwapDot11WlanBindEntry->MibObject.
                    u4CapwapDot11WlanProfileId =
                    pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
                    u4CapwapDot11WlanProfileId;
            }

            if ((pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
                 i4CapwapDot11WlanBindRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
                        i4CapwapDot11WlanBindRowStatus == ACTIVE)))
            {
                pWsscfgCapwapDot11WlanBindEntry->MibObject.
                    i4CapwapDot11WlanBindRowStatus = ACTIVE;
            }
            else if (pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
                     i4CapwapDot11WlanBindRowStatus == CREATE_AND_WAIT)
            {
                pWsscfgCapwapDot11WlanBindEntry->MibObject.
                    i4CapwapDot11WlanBindRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gWsscfgGlobals.WsscfgGlbMib.CapwapDot11WlanBindTable,
                 (tRBElem *) pWsscfgCapwapDot11WlanBindEntry) != RB_SUCCESS)
            {
                if (WsscfgSetAllCapwapDot11WlanBindTableTrigger
                    (pWsscfgSetCapwapDot11WlanBindEntry,
                     pWsscfgIsSetCapwapDot11WlanBindEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllCapwapDot11WlanBindTable: "
                                 "WsscfgSetAllCapwapDot11WlanBindTableTrigger "
                                 "function returns failure.\r\n"));
                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgSetAllCapwapDot11WlanBind"
                             "Table: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID, (UINT1 *)
                     pWsscfgCapwapDot11WlanBindEntry);
                MemReleaseMemBlock
                    (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID, (UINT1 *)
                     pWsscfgOldCapwapDot11WlanBindEntry);
                MemReleaseMemBlock
                    (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgCapwapDot11WlanBindEntry);
                MemReleaseMemBlock
                    (WSSCFG_CAPWAPDOT11WLANBINDTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetCapwapDot11WlanBindEntry);
                return OSIX_FAILURE;
            }
            if (WsscfgUtilUpdateCapwapDot11WlanBindTable
                (NULL, pWsscfgCapwapDot11WlanBindEntry,
                 pWsscfgIsSetCapwapDot11WlanBindEntry) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllCapwapDot11WlanBindTable:"
                             "WsscfgUtilUpdateCapwapDot11WlanBindTable function "
                             "returns failure.\r\n"));

                if (WsscfgSetAllCapwapDot11WlanBindTableTrigger
                    (pWsscfgSetCapwapDot11WlanBindEntry,
                     pWsscfgIsSetCapwapDot11WlanBindEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllCapwapDot11WlanBindTable: "
                                 "WsscfgSetAllCapwapDot11WlanBindTableTrigger"
                                 "function returns failure.\r\n"));

                }
                RBTreeRem (gWsscfgGlobals.
                           WsscfgGlbMib.CapwapDot11WlanBindTable,
                           pWsscfgCapwapDot11WlanBindEntry);
                MemReleaseMemBlock
                    (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID, (UINT1 *)
                     pWsscfgCapwapDot11WlanBindEntry);
                MemReleaseMemBlock
                    (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID, (UINT1 *)
                     pWsscfgOldCapwapDot11WlanBindEntry);
                MemReleaseMemBlock
                    (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgCapwapDot11WlanBindEntry);
                MemReleaseMemBlock
                    (WSSCFG_CAPWAPDOT11WLANBINDTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetCapwapDot11WlanBindEntry);
                return OSIX_FAILURE;
            }

            if ((pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
                 i4CapwapDot11WlanBindRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
                        i4CapwapDot11WlanBindRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgCapwapDot11WlanBindEntry->MibObject.
                    i4IfIndex =
                    pWsscfgSetCapwapDot11WlanBindEntry->MibObject.i4IfIndex;
                pWsscfgTrgCapwapDot11WlanBindEntry->MibObject.
                    u4CapwapDot11WlanProfileId =
                    pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
                    u4CapwapDot11WlanProfileId;
                pWsscfgTrgCapwapDot11WlanBindEntry->MibObject.
                    i4CapwapDot11WlanBindRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetCapwapDot11WlanBindEntry->
                    bCapwapDot11WlanBindRowStatus = OSIX_TRUE;

                if (WsscfgSetAllCapwapDot11WlanBindTableTrigger
                    (pWsscfgTrgCapwapDot11WlanBindEntry,
                     pWsscfgTrgIsSetCapwapDot11WlanBindEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllCapwapDot11WlanBindTable:"
                                 "WsscfgSetAllCapwapDot11WlanBindTableTrigger"
                                 "function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                         (UINT1 *) pWsscfgCapwapDot11WlanBindEntry);
                    MemReleaseMemBlock
                        (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                         (UINT1 *) pWsscfgOldCapwapDot11WlanBindEntry);
                    MemReleaseMemBlock
                        (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                         (UINT1 *) pWsscfgTrgCapwapDot11WlanBindEntry);
                    MemReleaseMemBlock
                        (WSSCFG_CAPWAPDOT11WLANBINDTABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetCapwapDot11WlanBindEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
                     i4CapwapDot11WlanBindRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgCapwapDot11WlanBindEntry->MibObject.
                    i4CapwapDot11WlanBindRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetCapwapDot11WlanBindEntry->
                    bCapwapDot11WlanBindRowStatus = OSIX_TRUE;

                if (WsscfgSetAllCapwapDot11WlanBindTableTrigger
                    (pWsscfgTrgCapwapDot11WlanBindEntry,
                     pWsscfgTrgIsSetCapwapDot11WlanBindEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllCapwapDot11WlanBindTable: "
                                 "WsscfgSetAllCapwapDot11WlanBindTableTrigger "
                                 "function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                         (UINT1 *) pWsscfgCapwapDot11WlanBindEntry);
                    MemReleaseMemBlock
                        (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                         (UINT1 *) pWsscfgOldCapwapDot11WlanBindEntry);
                    MemReleaseMemBlock
                        (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                         (UINT1 *) pWsscfgTrgCapwapDot11WlanBindEntry);
                    MemReleaseMemBlock
                        (WSSCFG_CAPWAPDOT11WLANBINDTABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetCapwapDot11WlanBindEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
                i4CapwapDot11WlanBindRowStatus == CREATE_AND_GO)
            {
                pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
                    i4CapwapDot11WlanBindRowStatus = ACTIVE;
            }

            if (WsscfgSetAllCapwapDot11WlanBindTableTrigger
                (pWsscfgSetCapwapDot11WlanBindEntry,
                 pWsscfgIsSetCapwapDot11WlanBindEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllCapwapDot11WlanBindTable: "
                             "WsscfgSetAllCapwapDot11WlanBindTableTrigger "
                             "function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                                (UINT1 *) pWsscfgOldCapwapDot11WlanBindEntry);
            MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgCapwapDot11WlanBindEntry);
            MemReleaseMemBlock
                (WSSCFG_CAPWAPDOT11WLANBINDTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetCapwapDot11WlanBindEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (WsscfgSetAllCapwapDot11WlanBindTableTrigger
                (pWsscfgSetCapwapDot11WlanBindEntry,
                 pWsscfgIsSetCapwapDot11WlanBindEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllCapwapDot11WlanBindTable:"
                             "WsscfgSetAllCapwapDot11WlanBindTableTrigger "
                             "function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllCapwapDot11WlanBindTable: Failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                                (UINT1 *) pWsscfgOldCapwapDot11WlanBindEntry);
            MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgCapwapDot11WlanBindEntry);
            MemReleaseMemBlock
                (WSSCFG_CAPWAPDOT11WLANBINDTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetCapwapDot11WlanBindEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
              i4CapwapDot11WlanBindRowStatus == CREATE_AND_WAIT)
             || (pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
                 i4CapwapDot11WlanBindRowStatus == CREATE_AND_GO))
    {
        if (WsscfgSetAllCapwapDot11WlanBindTableTrigger
            (pWsscfgSetCapwapDot11WlanBindEntry,
             pWsscfgIsSetCapwapDot11WlanBindEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllCapwapDot11WlanBindTable: "
                         "WsscfgSetAllCapwapDot11WlanBindTableTrigger function "
                         "returns failure.\r\n"));
        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllCapwapDot11WlanBindTable: The row is already"
                     "present.\r\n"));
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgOldCapwapDot11WlanBindEntry);
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgCapwapDot11WlanBindEntry);
        MemReleaseMemBlock
            (WSSCFG_CAPWAPDOT11WLANBINDTABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetCapwapDot11WlanBindEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pWsscfgOldCapwapDot11WlanBindEntry,
            pWsscfgCapwapDot11WlanBindEntry,
            sizeof (tWsscfgCapwapDot11WlanBindEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
        i4CapwapDot11WlanBindRowStatus == DESTROY)
    {
        pWsscfgCapwapDot11WlanBindEntry->MibObject.
            i4CapwapDot11WlanBindRowStatus = DESTROY;

        if (WsscfgUtilUpdateCapwapDot11WlanBindTable
            (pWsscfgOldCapwapDot11WlanBindEntry,
             pWsscfgCapwapDot11WlanBindEntry,
             pWsscfgIsSetCapwapDot11WlanBindEntry) != OSIX_SUCCESS)
        {

            if (WsscfgSetAllCapwapDot11WlanBindTableTrigger
                (pWsscfgSetCapwapDot11WlanBindEntry,
                 pWsscfgIsSetCapwapDot11WlanBindEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllCapwapDot11WlanBindTable: "
                             "WsscfgSetAllCapwapDot11WlanBindTableTrigger"
                             "function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllCapwapDot11WlanBindTable:"
                         "WsscfgUtilUpdateCapwapDot11WlanBindTable function"
                         "returns failure.\r\n"));
        }
        RBTreeRem (gWsscfgGlobals.WsscfgGlbMib.
                   CapwapDot11WlanBindTable, pWsscfgCapwapDot11WlanBindEntry);
        if (WsscfgSetAllCapwapDot11WlanBindTableTrigger
            (pWsscfgSetCapwapDot11WlanBindEntry,
             pWsscfgIsSetCapwapDot11WlanBindEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllCapwapDot11WlanBindTable:"
                         "WsscfgSetAllCapwapDot11WlanBindTableTrigger function"
                         "returns failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                                (UINT1 *) pWsscfgOldCapwapDot11WlanBindEntry);
            MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgCapwapDot11WlanBindEntry);
            MemReleaseMemBlock
                (WSSCFG_CAPWAPDOT11WLANBINDTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetCapwapDot11WlanBindEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgCapwapDot11WlanBindEntry);
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgOldCapwapDot11WlanBindEntry);
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgCapwapDot11WlanBindEntry);
        MemReleaseMemBlock
            (WSSCFG_CAPWAPDOT11WLANBINDTABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetCapwapDot11WlanBindEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (CapwapDot11WlanBindTableFilterInputs
        (pWsscfgCapwapDot11WlanBindEntry,
         pWsscfgSetCapwapDot11WlanBindEntry,
         pWsscfgIsSetCapwapDot11WlanBindEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgOldCapwapDot11WlanBindEntry);
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgCapwapDot11WlanBindEntry);
        MemReleaseMemBlock
            (WSSCFG_CAPWAPDOT11WLANBINDTABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetCapwapDot11WlanBindEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying
     * the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pWsscfgCapwapDot11WlanBindEntry->MibObject.
         i4CapwapDot11WlanBindRowStatus == ACTIVE)
        && (pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
            i4CapwapDot11WlanBindRowStatus != NOT_IN_SERVICE))
    {
        pWsscfgCapwapDot11WlanBindEntry->MibObject.
            i4CapwapDot11WlanBindRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pWsscfgTrgCapwapDot11WlanBindEntry->MibObject.
            i4CapwapDot11WlanBindRowStatus = NOT_IN_SERVICE;
        pWsscfgTrgIsSetCapwapDot11WlanBindEntry->bCapwapDot11WlanBindRowStatus
            = OSIX_TRUE;

        if (WsscfgUtilUpdateCapwapDot11WlanBindTable
            (pWsscfgOldCapwapDot11WlanBindEntry,
             pWsscfgCapwapDot11WlanBindEntry,
             pWsscfgIsSetCapwapDot11WlanBindEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pWsscfgCapwapDot11WlanBindEntry,
                    pWsscfgOldCapwapDot11WlanBindEntry,
                    sizeof (tWsscfgCapwapDot11WlanBindEntry));
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllCapwapDot11WlanBindTable:"
                         "WsscfgUtilUpdateCapwapDot11WlanBindTable Function"
                         "returns failure.\r\n"));

            if (WsscfgSetAllCapwapDot11WlanBindTableTrigger
                (pWsscfgSetCapwapDot11WlanBindEntry,
                 pWsscfgIsSetCapwapDot11WlanBindEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllCapwapDot11WlanBindTable: WsscfgSetAll"
                             "CapwapDot11WlanBindTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                                (UINT1 *) pWsscfgOldCapwapDot11WlanBindEntry);
            MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgCapwapDot11WlanBindEntry);
            MemReleaseMemBlock
                (WSSCFG_CAPWAPDOT11WLANBINDTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetCapwapDot11WlanBindEntry);
            return OSIX_FAILURE;
        }

        if (WsscfgSetAllCapwapDot11WlanBindTableTrigger
            (pWsscfgTrgCapwapDot11WlanBindEntry,
             pWsscfgTrgIsSetCapwapDot11WlanBindEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllCapwapDot11WlanBindTable: "
                         "WsscfgSetAllCapwapDot11WlanBindTableTrigger function "
                         "returns failure.\r\n"));
        }
    }

    if (pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
        i4CapwapDot11WlanBindRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pWsscfgIsSetCapwapDot11WlanBindEntry->bCapwapDot11WlanBindRowStatus
        != OSIX_FALSE)
    {
        pWsscfgCapwapDot11WlanBindEntry->MibObject.
            i4CapwapDot11WlanBindRowStatus =
            pWsscfgSetCapwapDot11WlanBindEntry->MibObject.
            i4CapwapDot11WlanBindRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after 
     * modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pWsscfgCapwapDot11WlanBindEntry->MibObject.
            i4CapwapDot11WlanBindRowStatus = ACTIVE;
    }
    if (WsscfgUtilUpdateCapwapDot11WlanBindTable
        (pWsscfgOldCapwapDot11WlanBindEntry,
         pWsscfgCapwapDot11WlanBindEntry,
         pWsscfgIsSetCapwapDot11WlanBindEntry) != OSIX_SUCCESS)
    {

        if (WsscfgSetAllCapwapDot11WlanBindTableTrigger
            (pWsscfgSetCapwapDot11WlanBindEntry,
             pWsscfgIsSetCapwapDot11WlanBindEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllCapwapDot11WlanBindTable:"
                         "WsscfgSetAllCapwapDot11WlanBindTableTrigger function"
                         "returns failure.\r\n"));

        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllCapwapDot11WlanBindTable:"
                     "WsscfgUtilUpdateCapwapDot11WlanBindTable function"
                     "returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pWsscfgCapwapDot11WlanBindEntry,
                pWsscfgOldCapwapDot11WlanBindEntry,
                sizeof (tWsscfgCapwapDot11WlanBindEntry));
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgOldCapwapDot11WlanBindEntry);
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgCapwapDot11WlanBindEntry);
        MemReleaseMemBlock
            (WSSCFG_CAPWAPDOT11WLANBINDTABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetCapwapDot11WlanBindEntry);
        return OSIX_FAILURE;

    }
    if (WsscfgSetAllCapwapDot11WlanBindTableTrigger
        (pWsscfgSetCapwapDot11WlanBindEntry,
         pWsscfgIsSetCapwapDot11WlanBindEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllCapwapDot11WlanBindTable:"
                     "WsscfgSetAllCapwapDot11WlanBindTableTrigger function"
                     "returns failure.\r\n"));
    }

    MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                        (UINT1 *) pWsscfgOldCapwapDot11WlanBindEntry);
    MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                        (UINT1 *) pWsscfgTrgCapwapDot11WlanBindEntry);
    MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_ISSET_POOLID,
                        (UINT1 *) pWsscfgTrgIsSetCapwapDot11WlanBindEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgGetAllFsDot11StationConfigTable
 Input       :  pWsscfgGetFsDot11StationConfigEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllFsDot11StationConfigTable (tWsscfgFsDot11StationConfigEntry *
                                       pWsscfgGetFsDot11StationConfigEntry)
{
    tWsscfgFsDot11StationConfigEntry *pWsscfgFsDot11StationConfigEntry = NULL;
    if (WsscfgGetAllUtlFsDot11StationConfigTable
        (pWsscfgGetFsDot11StationConfigEntry,
         pWsscfgFsDot11StationConfigEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11StationConfigTable:"
                     "WsscfgGetAllUtlFsDot11StationConfigTable Returns"
                     "Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllFsDot11ExternalWebAuthProfileTable
 Input       :  pWsscfgGetFsDot11ExternalWebAuthEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllFsDot11ExternalWebAuthProfileTable
    (tWsscfgFsDot11ExternalWebAuthProfileEntry *
     pWsscfgGetFsDot11ExternalWebAuthProfileEntry)
{
    if (WsscfgGetAllUtlFsDot11ExternalWebAuthProfileTable
        (pWsscfgGetFsDot11ExternalWebAuthProfileEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11ExternalWebAuthProfileTable:"
                     "WsscfgGetAllUtlFsDot11ExternalWebAuthProfileTable Returns"
                     "Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllFsDot11CapabilityProfileTable
 Input       :  pWsscfgGetFsDot11CapabilityProfileEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllFsDot11CapabilityProfileTable
    (tWsscfgFsDot11CapabilityProfileEntry *
     pWsscfgGetFsDot11CapabilityProfileEntry)
{
    tWsscfgFsDot11CapabilityProfileEntry
        * pWsscfgFsDot11CapabilityProfileEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgFsDot11CapabilityProfileEntry =
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.FsDot11CapabilityProfileTable,
                   (tRBElem *) pWsscfgGetFsDot11CapabilityProfileEntry);

    if (pWsscfgFsDot11CapabilityProfileEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11CapabilityProfileTable:"
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11CFPollable =
        pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11CFPollable;

    pWsscfgGetFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11CFPollRequest =
        pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11CFPollRequest;

    pWsscfgGetFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11PrivacyOptionImplemented =
        pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11PrivacyOptionImplemented;

    pWsscfgGetFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11ShortPreambleOptionImplemented =
        pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11ShortPreambleOptionImplemented;

    pWsscfgGetFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11PBCCOptionImplemented =
        pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11PBCCOptionImplemented;

    pWsscfgGetFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11ChannelAgilityPresent =
        pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11ChannelAgilityPresent;

    pWsscfgGetFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11QosOptionImplemented =
        pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11QosOptionImplemented;

    pWsscfgGetFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11SpectrumManagementRequired =
        pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11SpectrumManagementRequired;

    pWsscfgGetFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11ShortSlotTimeOptionImplemented =
        pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11ShortSlotTimeOptionImplemented;

    pWsscfgGetFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11APSDOptionImplemented =
        pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11APSDOptionImplemented;

    pWsscfgGetFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11DSSSOFDMOptionEnabled =
        pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11DSSSOFDMOptionEnabled;

    pWsscfgGetFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11DelayedBlockAckOptionImplemented =
        pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11DelayedBlockAckOptionImplemented;

    pWsscfgGetFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11ImmediateBlockAckOptionImplemented =
        pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11ImmediateBlockAckOptionImplemented;

    pWsscfgGetFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11QAckOptionImplemented =
        pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11QAckOptionImplemented;

    pWsscfgGetFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11QueueRequestOptionImplemented =
        pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11QueueRequestOptionImplemented;

    pWsscfgGetFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11TXOPRequestOptionImplemented =
        pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11TXOPRequestOptionImplemented;

    pWsscfgGetFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11RSNAOptionImplemented =
        pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11RSNAOptionImplemented;

    pWsscfgGetFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11RSNAPreauthenticationImplemented =
        pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11RSNAPreauthenticationImplemented;

    pWsscfgGetFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11CapabilityRowStatus =
        pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11CapabilityRowStatus;

    if (WsscfgGetAllUtlFsDot11CapabilityProfileTable
        (pWsscfgGetFsDot11CapabilityProfileEntry,
         pWsscfgFsDot11CapabilityProfileEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11CapabilityProfileTable:"
                     "WsscfgGetAllUtlFsDot11CapabilityProfileTable"
                     "Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllFsDot11AuthenticationProfileTable
 Input       :  pWsscfgGetFsDot11AuthenticationProfileEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllFsDot11AuthenticationProfileTable
    (tWsscfgFsDot11AuthenticationProfileEntry *
     pWsscfgGetFsDot11AuthenticationProfileEntry)
{
    tWsscfgFsDot11AuthenticationProfileEntry
        * pWsscfgFsDot11AuthenticationProfileEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgFsDot11AuthenticationProfileEntry =
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.FsDot11AuthenticationProfileTable,
                   (tRBElem *) pWsscfgGetFsDot11AuthenticationProfileEntry);

    if (pWsscfgFsDot11AuthenticationProfileEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11AuthenticationProfileTable: "
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetFsDot11AuthenticationProfileEntry->
        MibObject.i4FsDot11AuthenticationAlgorithm =
        pWsscfgFsDot11AuthenticationProfileEntry->
        MibObject.i4FsDot11AuthenticationAlgorithm;

    pWsscfgGetFsDot11AuthenticationProfileEntry->
        MibObject.i4FsDot11WepKeyIndex =
        pWsscfgFsDot11AuthenticationProfileEntry->
        MibObject.i4FsDot11WepKeyIndex;

    pWsscfgGetFsDot11AuthenticationProfileEntry->
        MibObject.i4FsDot11WepKeyType =
        pWsscfgFsDot11AuthenticationProfileEntry->MibObject.i4FsDot11WepKeyType;

    pWsscfgGetFsDot11AuthenticationProfileEntry->
        MibObject.i4FsDot11WepKeyLength =
        pWsscfgFsDot11AuthenticationProfileEntry->
        MibObject.i4FsDot11WepKeyLength;

    MEMCPY (pWsscfgGetFsDot11AuthenticationProfileEntry->
            MibObject.au1FsDot11WepKey,
            pWsscfgFsDot11AuthenticationProfileEntry->
            MibObject.au1FsDot11WepKey,
            pWsscfgFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11WepKeyLen);

    pWsscfgGetFsDot11AuthenticationProfileEntry->
        MibObject.i4FsDot11WepKeyLen =
        pWsscfgFsDot11AuthenticationProfileEntry->MibObject.i4FsDot11WepKeyLen;

    pWsscfgGetFsDot11AuthenticationProfileEntry->
        MibObject.i4FsDot11WebAuthentication =
        pWsscfgFsDot11AuthenticationProfileEntry->
        MibObject.i4FsDot11WebAuthentication;

    pWsscfgGetFsDot11AuthenticationProfileEntry->
        MibObject.i4FsDot11AuthenticationRowStatus =
        pWsscfgFsDot11AuthenticationProfileEntry->
        MibObject.i4FsDot11AuthenticationRowStatus;

    if (WsscfgGetAllUtlFsDot11AuthenticationProfileTable
        (pWsscfgGetFsDot11AuthenticationProfileEntry,
         pWsscfgFsDot11AuthenticationProfileEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11AuthenticationProfileTable:"
                     "WsscfgGetAllUtlFsDot11AuthenticationProfileTable"
                     "Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllFsStationQosParamsTable
 Input       :  pWsscfgGetFsStationQosParamsEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllFsStationQosParamsTable (tWsscfgFsStationQosParamsEntry *
                                     pWsscfgGetFsStationQosParamsEntry)
{
    tWsscfgFsStationQosParamsEntry *pWsscfgFsStationQosParamsEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgFsStationQosParamsEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsStationQosParamsTable,
                   (tRBElem *) pWsscfgGetFsStationQosParamsEntry);

    if (pWsscfgFsStationQosParamsEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsStationQosParamsTable:"
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetFsStationQosParamsEntry->MibObject.i4FsStaQoSPriority =
        pWsscfgFsStationQosParamsEntry->MibObject.i4FsStaQoSPriority;

    pWsscfgGetFsStationQosParamsEntry->MibObject.i4FsStaQoSDscp =
        pWsscfgFsStationQosParamsEntry->MibObject.i4FsStaQoSDscp;

    if (WsscfgGetAllUtlFsStationQosParamsTable
        (pWsscfgGetFsStationQosParamsEntry,
         pWsscfgFsStationQosParamsEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsStationQosParamsTable:"
                     "WsscfgGetAllUtlFsStationQosParamsTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllFsVlanIsolationTable
 Input       :  pWsscfgGetFsVlanIsolationEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllFsVlanIsolationTable (tWsscfgFsVlanIsolationEntry *
                                  pWsscfgGetFsVlanIsolationEntry)
{
    tWsscfgFsVlanIsolationEntry *pWsscfgFsVlanIsolationEntry = NULL;
    if (WsscfgGetAllUtlFsVlanIsolationTable
        (pWsscfgGetFsVlanIsolationEntry,
         pWsscfgFsVlanIsolationEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsVlanIsolationTable:"
                     "WsscfgGetAllUtlFsVlanIsolationTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllFsDot11RadioConfigTable
 Input       :  pWsscfgGetFsDot11RadioConfigEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllFsDot11RadioConfigTable (tWsscfgFsDot11RadioConfigEntry *
                                     pWsscfgGetFsDot11RadioConfigEntry)
{
    if (WsscfgGetAllUtlFsDot11RadioConfigTable
        (pWsscfgGetFsDot11RadioConfigEntry,
         pWsscfgGetFsDot11RadioConfigEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11RadioConfigTable:"
                     "WsscfgGetAllUtlFsDot11RadioConfigTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllFsDot11QosProfileTable
 Input       :  pWsscfgGetFsDot11QosProfileEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllFsDot11QosProfileTable (tWsscfgFsDot11QosProfileEntry *
                                    pWsscfgGetFsDot11QosProfileEntry)
{
    tWsscfgFsDot11QosProfileEntry *pWsscfgFsDot11QosProfileEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgFsDot11QosProfileEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsDot11QosProfileTable,
                   (tRBElem *) pWsscfgGetFsDot11QosProfileEntry);

    if (pWsscfgFsDot11QosProfileEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11QosProfileTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetFsDot11QosProfileEntry->MibObject.i4FsDot11QosTraffic =
        pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11QosTraffic;

    pWsscfgGetFsDot11QosProfileEntry->
        MibObject.i4FsDot11QosPassengerTrustMode =
        pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11QosPassengerTrustMode;

    pWsscfgGetFsDot11QosProfileEntry->MibObject.
        i4FsDot11QosRateLimit =
        pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11QosRateLimit;

    pWsscfgGetFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamCIR =
        pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamCIR;

    pWsscfgGetFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamCBS =
        pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamCBS;

    pWsscfgGetFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamEIR =
        pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamEIR;

    pWsscfgGetFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamEBS =
        pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamEBS;

    pWsscfgGetFsDot11QosProfileEntry->MibObject.
        i4FsDot11DownStreamCIR =
        pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11DownStreamCIR;

    pWsscfgGetFsDot11QosProfileEntry->MibObject.
        i4FsDot11DownStreamCBS =
        pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11DownStreamCBS;

    pWsscfgGetFsDot11QosProfileEntry->MibObject.
        i4FsDot11DownStreamEIR =
        pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11DownStreamEIR;

    pWsscfgGetFsDot11QosProfileEntry->MibObject.
        i4FsDot11DownStreamEBS =
        pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11DownStreamEBS;

    pWsscfgGetFsDot11QosProfileEntry->MibObject.
        i4FsDot11QosRowStatus =
        pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11QosRowStatus;

    MEMCPY (pWsscfgGetFsDot11QosProfileEntry->
            MibObject.au1FsDot11QosProfileName,
            pWsscfgFsDot11QosProfileEntry->
            MibObject.au1FsDot11QosProfileName,
            pWsscfgFsDot11QosProfileEntry->
            MibObject.i4FsDot11QosProfileNameLen);

    pWsscfgGetFsDot11QosProfileEntry->
        MibObject.i4FsDot11QosProfileNameLen =
        pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11QosProfileNameLen;

    if (WsscfgGetAllUtlFsDot11QosProfileTable
        (pWsscfgGetFsDot11QosProfileEntry,
         pWsscfgFsDot11QosProfileEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11QosProfileTable:"
                     "WsscfgGetAllUtlFsDot11QosProfileTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllFsDot11WlanCapabilityProfileTable
 Input       :  pWsscfgGetFsDot11WlanCapabilityProfileEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllFsDot11WlanCapabilityProfileTable
    (tWsscfgFsDot11WlanCapabilityProfileEntry *
     pWsscfgGetFsDot11WlanCapabilityProfileEntry)
{
    tWsscfgFsDot11WlanCapabilityProfileEntry
        * pWsscfgFsDot11WlanCapabilityProfileEntry = NULL;
#if 1
    /* Check whether the node is already present */
    pWsscfgFsDot11WlanCapabilityProfileEntry =
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.FsDot11WlanCapabilityProfileTable,
                   (tRBElem *) pWsscfgGetFsDot11WlanCapabilityProfileEntry);

    if (pWsscfgFsDot11WlanCapabilityProfileEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11WlanCapabilityProfileTable:"
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanCFPollable =
        pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanCFPollable;

    pWsscfgGetFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanCFPollRequest =
        pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanCFPollRequest;

    pWsscfgGetFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanPrivacyOptionImplemented =
        pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanPrivacyOptionImplemented;

    pWsscfgGetFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanShortPreambleOptionImplemented =
        pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanShortPreambleOptionImplemented;

    pWsscfgGetFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanPBCCOptionImplemented =
        pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanPBCCOptionImplemented;

    pWsscfgGetFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanChannelAgilityPresent =
        pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanChannelAgilityPresent;

    pWsscfgGetFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanQosOptionImplemented =
        pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanQosOptionImplemented;

    pWsscfgGetFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanSpectrumManagementRequired =
        pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanSpectrumManagementRequired;

    pWsscfgGetFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanShortSlotTimeOptionImplemented =
        pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanShortSlotTimeOptionImplemented;

    pWsscfgGetFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanAPSDOptionImplemented =
        pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanAPSDOptionImplemented;

    pWsscfgGetFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanDSSSOFDMOptionEnabled =
        pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanDSSSOFDMOptionEnabled;

    pWsscfgGetFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanDelayedBlockAckOptionImplemented =
        pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanDelayedBlockAckOptionImplemented;

    pWsscfgGetFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanImmediateBlockAckOptionImplemented =
        pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanImmediateBlockAckOptionImplemented;

    pWsscfgGetFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanQAckOptionImplemented =
        pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanQAckOptionImplemented;

    pWsscfgGetFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanQueueRequestOptionImplemented =
        pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanQueueRequestOptionImplemented;

    pWsscfgGetFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanTXOPRequestOptionImplemented =
        pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanTXOPRequestOptionImplemented;

    pWsscfgGetFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanRSNAOptionImplemented =
        pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanRSNAOptionImplemented;

    pWsscfgGetFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanRSNAPreauthenticationImplemented =
        pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanRSNAPreauthenticationImplemented;

    pWsscfgGetFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanCapabilityRowStatus =
        pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanCapabilityRowStatus;
#endif
    if (WsscfgGetAllUtlFsDot11WlanCapabilityProfileTable
        (pWsscfgGetFsDot11WlanCapabilityProfileEntry,
         pWsscfgFsDot11WlanCapabilityProfileEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11WlanCapabilityProfileTable:"
                     "WsscfgGetAllUtlFsDot11WlanCapabilityProfileTable"
                     "Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllFsDot11WlanAuthenticationProfileTable
 Input       :  pWsscfgGetFsDot11WlanAuthenticationProfileEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgGetAllFsDot11WlanAuthenticationProfileTable
    (tWsscfgFsDot11WlanAuthenticationProfileEntry *
     pWsscfgGetFsDot11WlanAuthenticationProfileEntry)
{
    tWsscfgFsDot11WlanAuthenticationProfileEntry
        * pWsscfgFsDot11WlanAuthenticationProfileEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgFsDot11WlanAuthenticationProfileEntry =
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.FsDot11WlanAuthenticationProfileTable,
                   (tRBElem *) pWsscfgGetFsDot11WlanAuthenticationProfileEntry);

    if (pWsscfgFsDot11WlanAuthenticationProfileEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11WlanAuthenticationProfileTable: "
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetFsDot11WlanAuthenticationProfileEntry->
        MibObject.i4FsDot11WlanAuthenticationAlgorithm =
        pWsscfgFsDot11WlanAuthenticationProfileEntry->
        MibObject.i4FsDot11WlanAuthenticationAlgorithm;

    pWsscfgGetFsDot11WlanAuthenticationProfileEntry->
        MibObject.i4FsDot11WlanWepKeyIndex =
        pWsscfgFsDot11WlanAuthenticationProfileEntry->
        MibObject.i4FsDot11WlanWepKeyIndex;

    pWsscfgGetFsDot11WlanAuthenticationProfileEntry->
        MibObject.i4FsDot11WlanWepKeyType =
        pWsscfgFsDot11WlanAuthenticationProfileEntry->
        MibObject.i4FsDot11WlanWepKeyType;

    pWsscfgGetFsDot11WlanAuthenticationProfileEntry->
        MibObject.i4FsDot11WlanWepKeyLength =
        pWsscfgFsDot11WlanAuthenticationProfileEntry->
        MibObject.i4FsDot11WlanWepKeyLength;

    MEMCPY (pWsscfgGetFsDot11WlanAuthenticationProfileEntry->
            MibObject.au1FsDot11WlanWepKey,
            pWsscfgFsDot11WlanAuthenticationProfileEntry->
            MibObject.au1FsDot11WlanWepKey,
            pWsscfgFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanWepKeyLen);

    pWsscfgGetFsDot11WlanAuthenticationProfileEntry->
        MibObject.i4FsDot11WlanWepKeyLen =
        pWsscfgFsDot11WlanAuthenticationProfileEntry->
        MibObject.i4FsDot11WlanWepKeyLen;

    pWsscfgGetFsDot11WlanAuthenticationProfileEntry->
        MibObject.i4FsDot11WlanWebAuthentication =
        pWsscfgFsDot11WlanAuthenticationProfileEntry->
        MibObject.i4FsDot11WlanWebAuthentication;

    pWsscfgGetFsDot11WlanAuthenticationProfileEntry->
        MibObject.i4FsDot11WlanAuthenticationRowStatus =
        pWsscfgFsDot11WlanAuthenticationProfileEntry->
        MibObject.i4FsDot11WlanAuthenticationRowStatus;

    if (WsscfgGetAllUtlFsDot11WlanAuthenticationProfileTable
        (pWsscfgGetFsDot11WlanAuthenticationProfileEntry,
         pWsscfgFsDot11WlanAuthenticationProfileEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11WlanAuthenticationProfileTable:"
                     "WsscfgGetAllUtlFsDot11WlanAuthenticationProfileTable "
                     "Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllFsDot11WlanQosProfileTable
 Input       :  pWsscfgGetFsDot11WlanQosProfileEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllFsDot11WlanQosProfileTable (tWsscfgFsDot11WlanQosProfileEntry *
                                        pWsscfgGetFsDot11WlanQosProfileEntry)
{
    tWsscfgFsDot11WlanQosProfileEntry
        * pWsscfgFsDot11WlanQosProfileEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgFsDot11WlanQosProfileEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.
                   FsDot11WlanQosProfileTable,
                   (tRBElem *) pWsscfgGetFsDot11WlanQosProfileEntry);

    if (pWsscfgFsDot11WlanQosProfileEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11WlanQosProfileTable:"
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetFsDot11WlanQosProfileEntry->
        MibObject.i4FsDot11WlanQosTraffic =
        pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanQosTraffic;

    pWsscfgGetFsDot11WlanQosProfileEntry->
        MibObject.i4FsDot11WlanQosPassengerTrustMode =
        pWsscfgFsDot11WlanQosProfileEntry->
        MibObject.i4FsDot11WlanQosPassengerTrustMode;

    pWsscfgGetFsDot11WlanQosProfileEntry->
        MibObject.i4FsDot11WlanQosRateLimit =
        pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanQosRateLimit;

    pWsscfgGetFsDot11WlanQosProfileEntry->
        MibObject.i4FsDot11WlanUpStreamCIR =
        pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanUpStreamCIR;

    pWsscfgGetFsDot11WlanQosProfileEntry->
        MibObject.i4FsDot11WlanUpStreamCBS =
        pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanUpStreamCBS;

    pWsscfgGetFsDot11WlanQosProfileEntry->
        MibObject.i4FsDot11WlanUpStreamEIR =
        pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanUpStreamEIR;

    pWsscfgGetFsDot11WlanQosProfileEntry->
        MibObject.i4FsDot11WlanUpStreamEBS =
        pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanUpStreamEBS;

    pWsscfgGetFsDot11WlanQosProfileEntry->
        MibObject.i4FsDot11WlanDownStreamCIR =
        pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanDownStreamCIR;

    pWsscfgGetFsDot11WlanQosProfileEntry->
        MibObject.i4FsDot11WlanDownStreamCBS =
        pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanDownStreamCBS;

    pWsscfgGetFsDot11WlanQosProfileEntry->
        MibObject.i4FsDot11WlanDownStreamEIR =
        pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanDownStreamEIR;

    pWsscfgGetFsDot11WlanQosProfileEntry->
        MibObject.i4FsDot11WlanDownStreamEBS =
        pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanDownStreamEBS;

    pWsscfgGetFsDot11WlanQosProfileEntry->
        MibObject.i4FsDot11WlanQosRowStatus =
        pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanQosRowStatus;

    if (WsscfgGetAllUtlFsDot11WlanQosProfileTable
        (pWsscfgGetFsDot11WlanQosProfileEntry,
         pWsscfgFsDot11WlanQosProfileEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11WlanQosProfileTable:"
                     "WsscfgGetAllUtlFsDot11WlanQosProfileTable"
                     "Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllFsDot11RadioQosTable
 Input       :  pWsscfgGetFsDot11RadioQosEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllFsDot11RadioQosTable (tWsscfgFsDot11RadioQosEntry *
                                  pWsscfgGetFsDot11RadioQosEntry)
{
    tWsscfgFsDot11RadioQosEntry *pWsscfgFsDot11RadioQosEntry = NULL;

    if (WsscfgGetAllUtlFsDot11RadioQosTable
        (pWsscfgGetFsDot11RadioQosEntry,
         pWsscfgFsDot11RadioQosEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11RadioQosTable:"
                     "WsscfgGetAllUtlFsDot11RadioQosTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllFsDot11QAPTable
 Input       :  pWsscfgGetFsDot11QAPEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllFsDot11QAPTable (tWsscfgFsDot11QAPEntry * pWsscfgGetFsDot11QAPEntry)
{
    tWsscfgFsDot11QAPEntry *pWsscfgFsDot11QAPEntry = NULL;

    if (WsscfgGetAllUtlFsDot11QAPTable
        (pWsscfgGetFsDot11QAPEntry, pWsscfgFsDot11QAPEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllFsDot11QAPTable:"
                     "WsscfgGetAllUtlFsDot11QAPTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllFsQAPProfileTable
 Input       :  pWsscfgGetFsQAPProfileEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllFsQAPProfileTable (tWsscfgFsQAPProfileEntry *
                               pWsscfgGetFsQAPProfileEntry)
{
    tWsscfgFsQAPProfileEntry *pWsscfgFsQAPProfileEntry = NULL;
    /* Check whether the node is already present */
    pWsscfgFsQAPProfileEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsQAPProfileTable,
                   (tRBElem *) pWsscfgGetFsQAPProfileEntry);

    if (pWsscfgFsQAPProfileEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsQAPProfileTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }
    /* Assign values from the existing node */
    pWsscfgGetFsQAPProfileEntry->MibObject.i4FsQAPProfileCWmin =
        pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileCWmin;

    pWsscfgGetFsQAPProfileEntry->MibObject.i4FsQAPProfileCWmax =
        pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileCWmax;

    pWsscfgGetFsQAPProfileEntry->MibObject.i4FsQAPProfileAIFSN =
        pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileAIFSN;

    pWsscfgGetFsQAPProfileEntry->MibObject.i4FsQAPProfileTXOPLimit =
        pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileTXOPLimit;

    pWsscfgGetFsQAPProfileEntry->MibObject.
        i4FsQAPProfileAdmissionControl =
        pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileAdmissionControl;

    pWsscfgGetFsQAPProfileEntry->MibObject.
        i4FsQAPProfilePriorityValue =
        pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfilePriorityValue;

    pWsscfgGetFsQAPProfileEntry->MibObject.i4FsQAPProfileDscpValue =
        pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileDscpValue;

    pWsscfgGetFsQAPProfileEntry->MibObject.i4FsQAPProfileRowStatus =
        pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileRowStatus;
    if (WsscfgGetAllUtlFsQAPProfileTable
        (pWsscfgGetFsQAPProfileEntry, pWsscfgFsQAPProfileEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllFsQAPProfileTable:"
                     "WsscfgGetAllUtlFsQAPProfileTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllFsDot11CapabilityMappingTable
 Input       :  pWsscfgGetFsDot11CapabilityMappingEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllFsDot11CapabilityMappingTable (tWsscfgFsDot11CapabilityMappingEntry
                                           *
                                           pWsscfgGetFsDot11CapabilityMappingEntry)
{
    tWsscfgFsDot11CapabilityMappingEntry
        * pWsscfgFsDot11CapabilityMappingEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgFsDot11CapabilityMappingEntry =
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.FsDot11CapabilityMappingTable,
                   (tRBElem *) pWsscfgGetFsDot11CapabilityMappingEntry);

    if (pWsscfgFsDot11CapabilityMappingEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11CapabilityMappingTable: "
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pWsscfgGetFsDot11CapabilityMappingEntry->
            MibObject.au1FsDot11CapabilityMappingProfileName,
            pWsscfgFsDot11CapabilityMappingEntry->
            MibObject.au1FsDot11CapabilityMappingProfileName,
            pWsscfgFsDot11CapabilityMappingEntry->
            MibObject.i4FsDot11CapabilityMappingProfileNameLen);

    pWsscfgGetFsDot11CapabilityMappingEntry->
        MibObject.i4FsDot11CapabilityMappingProfileNameLen =
        pWsscfgFsDot11CapabilityMappingEntry->
        MibObject.i4FsDot11CapabilityMappingProfileNameLen;

    pWsscfgGetFsDot11CapabilityMappingEntry->
        MibObject.i4FsDot11CapabilityMappingRowStatus =
        pWsscfgFsDot11CapabilityMappingEntry->
        MibObject.i4FsDot11CapabilityMappingRowStatus;

    if (WsscfgGetAllUtlFsDot11CapabilityMappingTable
        (pWsscfgGetFsDot11CapabilityMappingEntry,
         pWsscfgFsDot11CapabilityMappingEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11CapabilityMappingTable:"
                     "WsscfgGetAllUtlFsDot11CapabilityMappingTable"
                     "Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllFsDot11AuthMappingTable
 Input       :  pWsscfgGetFsDot11AuthMappingEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllFsDot11AuthMappingTable (tWsscfgFsDot11AuthMappingEntry *
                                     pWsscfgGetFsDot11AuthMappingEntry)
{
    tWsscfgFsDot11AuthMappingEntry *pWsscfgFsDot11AuthMappingEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgFsDot11AuthMappingEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsDot11AuthMappingTable,
                   (tRBElem *) pWsscfgGetFsDot11AuthMappingEntry);

    if (pWsscfgFsDot11AuthMappingEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11AuthMappingTable: "
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pWsscfgGetFsDot11AuthMappingEntry->
            MibObject.au1FsDot11AuthMappingProfileName,
            pWsscfgFsDot11AuthMappingEntry->
            MibObject.au1FsDot11AuthMappingProfileName,
            pWsscfgFsDot11AuthMappingEntry->
            MibObject.i4FsDot11AuthMappingProfileNameLen);

    pWsscfgGetFsDot11AuthMappingEntry->
        MibObject.i4FsDot11AuthMappingProfileNameLen =
        pWsscfgFsDot11AuthMappingEntry->
        MibObject.i4FsDot11AuthMappingProfileNameLen;

    pWsscfgGetFsDot11AuthMappingEntry->
        MibObject.i4FsDot11AuthMappingRowStatus =
        pWsscfgFsDot11AuthMappingEntry->MibObject.i4FsDot11AuthMappingRowStatus;

    if (WsscfgGetAllUtlFsDot11AuthMappingTable
        (pWsscfgGetFsDot11AuthMappingEntry,
         pWsscfgFsDot11AuthMappingEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11AuthMappingTable:"
                     "WsscfgGetAllUtlFsDot11AuthMappingTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllFsDot11QosMappingTable
 Input       :  pWsscfgGetFsDot11QosMappingEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllFsDot11QosMappingTable (tWsscfgFsDot11QosMappingEntry *
                                    pWsscfgGetFsDot11QosMappingEntry)
{
    tWsscfgFsDot11QosMappingEntry *pWsscfgFsDot11QosMappingEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgFsDot11QosMappingEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsDot11QosMappingTable,
                   (tRBElem *) pWsscfgGetFsDot11QosMappingEntry);

    if (pWsscfgFsDot11QosMappingEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11QosMappingTable: "
                     "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pWsscfgGetFsDot11QosMappingEntry->
            MibObject.au1FsDot11QosMappingProfileName,
            pWsscfgFsDot11QosMappingEntry->
            MibObject.au1FsDot11QosMappingProfileName,
            pWsscfgFsDot11QosMappingEntry->
            MibObject.i4FsDot11QosMappingProfileNameLen);

    pWsscfgGetFsDot11QosMappingEntry->
        MibObject.i4FsDot11QosMappingProfileNameLen =
        pWsscfgFsDot11QosMappingEntry->
        MibObject.i4FsDot11QosMappingProfileNameLen;

    pWsscfgGetFsDot11QosMappingEntry->
        MibObject.i4FsDot11QosMappingRowStatus =
        pWsscfgFsDot11QosMappingEntry->MibObject.i4FsDot11QosMappingRowStatus;

    if (WsscfgGetAllUtlFsDot11QosMappingTable
        (pWsscfgGetFsDot11QosMappingEntry,
         pWsscfgFsDot11QosMappingEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11QosMappingTable:"
                     "WsscfgGetAllUtlFsDot11QosMappingTable Returns"
                     "Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllFsDot11ClientSummaryTable
 Input       :  pWsscfgGetFsDot11ClientSummaryEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllFsDot11ClientSummaryTable (tWsscfgFsDot11ClientSummaryEntry *
                                       pWsscfgGetFsDot11ClientSummaryEntry)
{
    tWsscfgFsDot11ClientSummaryEntry *pWsscfgFsDot11ClientSummaryEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgFsDot11ClientSummaryEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsDot11ClientSummaryTable,
                   (tRBElem *) pWsscfgGetFsDot11ClientSummaryEntry);

    if (pWsscfgFsDot11ClientSummaryEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11ClientSummaryTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetFsDot11ClientSummaryEntry->MibObject.u4FsDot11WlanProfileId =
        pWsscfgFsDot11ClientSummaryEntry->MibObject.u4FsDot11WlanProfileId;

    MEMCPY (pWsscfgGetFsDot11ClientSummaryEntry->MibObject.
            au1FsDot11WtpProfileName,
            pWsscfgFsDot11ClientSummaryEntry->MibObject.
            au1FsDot11WtpProfileName,
            pWsscfgFsDot11ClientSummaryEntry->MibObject.
            i4FsDot11WtpProfileNameLen);

    pWsscfgGetFsDot11ClientSummaryEntry->MibObject.i4FsDot11WtpProfileNameLen =
        pWsscfgFsDot11ClientSummaryEntry->MibObject.i4FsDot11WtpProfileNameLen;

    pWsscfgGetFsDot11ClientSummaryEntry->MibObject.u4FsDot11WtpRadioId =
        pWsscfgFsDot11ClientSummaryEntry->MibObject.u4FsDot11WtpRadioId;

    pWsscfgGetFsDot11ClientSummaryEntry->MibObject.i4FsDot11AuthStatus =
        pWsscfgFsDot11ClientSummaryEntry->MibObject.i4FsDot11AuthStatus;

    pWsscfgGetFsDot11ClientSummaryEntry->MibObject.i4FsDot11AssocStatus =
        pWsscfgFsDot11ClientSummaryEntry->MibObject.i4FsDot11AssocStatus;

    if (WsscfgGetAllUtlFsDot11ClientSummaryTable
        (pWsscfgGetFsDot11ClientSummaryEntry,
         pWsscfgFsDot11ClientSummaryEntry) == OSIX_FAILURE)

    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllFsDot11ClientSummaryTable:"
                     "WsscfgGetAllUtlFsDot11ClientSummaryTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllFsDot11AntennasListTable
 Input       :  pWsscfgGetFsDot11AntennasListEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllFsDot11AntennasListTable (tWsscfgFsDot11AntennasListEntry *
                                      pWsscfgGetFsDot11AntennasListEntry)
{
    tWsscfgFsDot11AntennasListEntry *pWsscfgFsDot11AntennasListEntry = NULL;
    if (WsscfgGetAllUtlFsDot11AntennasListTable
        (pWsscfgGetFsDot11AntennasListEntry,
         pWsscfgFsDot11AntennasListEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11AntennasListTable:"
                     "WsscfgGetAllUtlFsDot11AntennasListTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllFsDot11WlanTable
 Input       :  pWsscfgGetFsDot11WlanEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllFsDot11WlanTable (tWsscfgFsDot11WlanEntry *
                              pWsscfgGetFsDot11WlanEntry)
{
    tWsscfgFsDot11WlanEntry *pWsscfgFsDot11WlanEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgFsDot11WlanEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsDot11WlanTable,
                   (tRBElem *) pWsscfgGetFsDot11WlanEntry);

    if (pWsscfgFsDot11WlanEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11WlanTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetFsDot11WlanEntry->MibObject.
        i4FsDot11WlanProfileIfIndex =
        pWsscfgFsDot11WlanEntry->MibObject.i4FsDot11WlanProfileIfIndex;

    pWsscfgGetFsDot11WlanEntry->MibObject.i4FsDot11WlanRowStatus =
        pWsscfgFsDot11WlanEntry->MibObject.i4FsDot11WlanRowStatus;

    if (WsscfgGetAllUtlFsDot11WlanTable
        (pWsscfgGetFsDot11WlanEntry, pWsscfgFsDot11WlanEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllFsDot11WlanTable:"
                     "WsscfgGetAllUtlFsDot11WlanTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllFsDot11WlanBindTable
 Input       :  pWsscfgGetFsDot11WlanBindEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllFsDot11WlanBindTable (tWsscfgFsDot11WlanBindEntry *
                                  pWsscfgGetFsDot11WlanBindEntry)
{
    tWsscfgFsDot11WlanBindEntry *pWsscfgFsDot11WlanBindEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgFsDot11WlanBindEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsDot11WlanBindTable,
                   (tRBElem *) pWsscfgGetFsDot11WlanBindEntry);

    if (pWsscfgFsDot11WlanBindEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11WlanBindTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetFsDot11WlanBindEntry->MibObject.
        u4FsDot11WlanBindWlanId =
        pWsscfgFsDot11WlanBindEntry->MibObject.u4FsDot11WlanBindWlanId;

    pWsscfgGetFsDot11WlanBindEntry->MibObject.
        i4FsDot11WlanBindBssIfIndex =
        pWsscfgFsDot11WlanBindEntry->MibObject.i4FsDot11WlanBindBssIfIndex;

    pWsscfgGetFsDot11WlanBindEntry->MibObject.
        i4FsDot11WlanBindRowStatus =
        pWsscfgFsDot11WlanBindEntry->MibObject.i4FsDot11WlanBindRowStatus;

    if (WsscfgGetAllUtlFsDot11WlanBindTable
        (pWsscfgGetFsDot11WlanBindEntry,
         pWsscfgFsDot11WlanBindEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11WlanBindTable:"
                     "WsscfgGetAllUtlFsDot11WlanBindTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
    Function    :  WsscfgGetAllFsDot11nConfigTable
    Input       :  pWsscfgGetFsDot11nConfigEntry
    Description :  This Routine Take the Indices &
                   Gets the Value accordingly.
    Output      :  None
    Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllFsDot11nConfigTable (tWsscfgFsDot11nConfigEntry *
                                 pWsscfgGetFsDot11nConfigEntry)
{
    tWsscfgFsDot11nConfigEntry *pWsscfgFsDot11nConfigEntry = NULL;
    if (WsscfgGetAllUtlFsDot11nConfigTable
        (pWsscfgGetFsDot11nConfigEntry,
         pWsscfgFsDot11nConfigEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11nConfigTable:"
                     "WsscfgGetAllUtlFsDot11nConfigTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
    Function    :  WsscfgGetAllFsDot11nMCSDataRateTable
    Input       :  pWsscfgGetFsDot11nMCSDataRateEntry
    Description :  This Routine Take the Indices &
                   Gets the Value accordingly.
    Output      :  None
    Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllFsDot11nMCSDataRateTable (tWsscfgFsDot11nMCSDataRateEntry *
                                      pWsscfgGetFsDot11nMCSDataRateEntry)
{
    tWsscfgFsDot11nMCSDataRateEntry *pWsscfgFsDot11nMCSDataRateEntry = NULL;
    if (WsscfgGetAllUtlFsDot11nMCSDataRateTable
        (pWsscfgGetFsDot11nMCSDataRateEntry,
         pWsscfgFsDot11nMCSDataRateEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsDot11nMCSDataRateTable:"
                     "WsscfgGetAllUtlFsDot11nMCSDataRateTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgGetAllFsWtpImageUpgradeTable
 Input       :  pWsscfgGetFsWtpImageUpgradeEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllFsWtpImageUpgradeTable (tWsscfgFsWtpImageUpgradeEntry *
                                    pWsscfgGetFsWtpImageUpgradeEntry)
{
    tWsscfgFsWtpImageUpgradeEntry *pWsscfgFsWtpImageUpgradeEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgFsWtpImageUpgradeEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsWtpImageUpgradeTable,
                   (tRBElem *) pWsscfgGetFsWtpImageUpgradeEntry);

    if (pWsscfgFsWtpImageUpgradeEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsWtpImageUpgradeTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pWsscfgGetFsWtpImageUpgradeEntry->
            MibObject.au1FsWtpImageVersion,
            pWsscfgFsWtpImageUpgradeEntry->MibObject.
            au1FsWtpImageVersion,
            pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpImageVersionLen);

    pWsscfgGetFsWtpImageUpgradeEntry->MibObject.
        i4FsWtpImageVersionLen =
        pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpImageVersionLen;

    pWsscfgGetFsWtpImageUpgradeEntry->MibObject.i4FsWtpUpgradeDev =
        pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpUpgradeDev;

    MEMCPY (pWsscfgGetFsWtpImageUpgradeEntry->MibObject.au1FsWtpName,
            pWsscfgFsWtpImageUpgradeEntry->MibObject.au1FsWtpName,
            pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpNameLen);

    pWsscfgGetFsWtpImageUpgradeEntry->MibObject.i4FsWtpNameLen =
        pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpNameLen;

    MEMCPY (pWsscfgGetFsWtpImageUpgradeEntry->MibObject.
            au1FsWtpImageName,
            pWsscfgFsWtpImageUpgradeEntry->MibObject.au1FsWtpImageName,
            pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpImageNameLen);

    pWsscfgGetFsWtpImageUpgradeEntry->MibObject.i4FsWtpImageNameLen =
        pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpImageNameLen;

    pWsscfgGetFsWtpImageUpgradeEntry->MibObject.i4FsWtpAddressType =
        pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpAddressType;

    MEMCPY (pWsscfgGetFsWtpImageUpgradeEntry->MibObject.
            au1FsWtpServerIP,
            pWsscfgFsWtpImageUpgradeEntry->MibObject.au1FsWtpServerIP,
            pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpServerIPLen);

    pWsscfgGetFsWtpImageUpgradeEntry->MibObject.i4FsWtpServerIPLen =
        pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpServerIPLen;

    pWsscfgGetFsWtpImageUpgradeEntry->MibObject.i4FsWtpRowStatus =
        pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpRowStatus;

    if (WsscfgGetAllUtlFsWtpImageUpgradeTable
        (pWsscfgGetFsWtpImageUpgradeEntry,
         pWsscfgFsWtpImageUpgradeEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsWtpImageUpgradeTable:"
                     "WsscfgGetAllUtlFsWtpImageUpgradeTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11StationConfigTable
 Input       :  pWsscfgSetFsDot11StationConfigEntry
                pWsscfgIsSetFsDot11StationConfigEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllFsDot11StationConfigTable (tWsscfgFsDot11StationConfigEntry *
                                       pWsscfgSetFsDot11StationConfigEntry,
                                       tWsscfgIsSetFsDot11StationConfigEntry *
                                       pWsscfgIsSetFsDot11StationConfigEntry)
{
    tWsscfgFsDot11StationConfigEntry WsscfgOldFsDot11StationConfigEntry;

    MEMSET (&WsscfgOldFsDot11StationConfigEntry, 0,
            sizeof (tWsscfgFsDot11StationConfigEntry));
    if (WsscfgUtilUpdateFsDot11StationConfigTable
        (&WsscfgOldFsDot11StationConfigEntry,
         pWsscfgSetFsDot11StationConfigEntry,
         pWsscfgIsSetFsDot11StationConfigEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11CapabilityProfileTable
 Input       :  pWsscfgSetFsDot11CapabilityProfileEntry
                pWsscfgIsSetFsDot11CapabilityProfileEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgSetAllFsDot11CapabilityProfileTable
    (tWsscfgFsDot11CapabilityProfileEntry *
     pWsscfgSetFsDot11CapabilityProfileEntry,
     tWsscfgIsSetFsDot11CapabilityProfileEntry *
     pWsscfgIsSetFsDot11CapabilityProfileEntry, INT4 i4RowStatusLogic,
     INT4 i4RowCreateOption)
{
    tWsscfgFsDot11CapabilityProfileEntry
        * pWsscfgFsDot11CapabilityProfileEntry = NULL;
    tWsscfgFsDot11CapabilityProfileEntry
        * pWsscfgOldFsDot11CapabilityProfileEntry = NULL;
    tWsscfgFsDot11CapabilityProfileEntry
        * pWsscfgTrgFsDot11CapabilityProfileEntry = NULL;
    tWsscfgIsSetFsDot11CapabilityProfileEntry
        * pWsscfgTrgIsSetFsDot11CapabilityProfileEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pWsscfgOldFsDot11CapabilityProfileEntry =
        (tWsscfgFsDot11CapabilityProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID);
    if (pWsscfgOldFsDot11CapabilityProfileEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWsscfgTrgFsDot11CapabilityProfileEntry =
        (tWsscfgFsDot11CapabilityProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID);
    if (pWsscfgTrgFsDot11CapabilityProfileEntry == NULL)
    {
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID, (UINT1 *)
             pWsscfgOldFsDot11CapabilityProfileEntry);
        return OSIX_FAILURE;
    }
    pWsscfgTrgIsSetFsDot11CapabilityProfileEntry =
        (tWsscfgIsSetFsDot11CapabilityProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_ISSET_POOLID);
    if (pWsscfgTrgIsSetFsDot11CapabilityProfileEntry == NULL)
    {
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID, (UINT1 *)
             pWsscfgOldFsDot11CapabilityProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID, (UINT1 *)
             pWsscfgTrgFsDot11CapabilityProfileEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pWsscfgOldFsDot11CapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11CapabilityProfileEntry));
    MEMSET (pWsscfgTrgFsDot11CapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11CapabilityProfileEntry));
    MEMSET (pWsscfgTrgIsSetFsDot11CapabilityProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11CapabilityProfileEntry));

    /* Check whether the node is already present */
    pWsscfgFsDot11CapabilityProfileEntry =
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.FsDot11CapabilityProfileTable,
                   (tRBElem *) pWsscfgSetFsDot11CapabilityProfileEntry);

    if (gu1WlanCpbltyPrfCount > MAX_WLAN_CPBLITY_PRF)
        /* Kloc Fix Start */
    {
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID, (UINT1 *)
             pWsscfgOldFsDot11CapabilityProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID, (UINT1 *)
             pWsscfgTrgFsDot11CapabilityProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_ISSET_POOLID,
             (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityProfileEntry);
        /* Kloc Fix Ends */
        return OSIX_FAILURE;
        /* Kloc Fix Start */
    }
    /* Kloc Fix Ends */

    if (pWsscfgFsDot11CapabilityProfileEntry == NULL)
    {
        /* Create the node if the RowStatus given is 
         * CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pWsscfgSetFsDot11CapabilityProfileEntry->
             MibObject.i4FsDot11CapabilityRowStatus ==
             CREATE_AND_WAIT)
            || (pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11CapabilityRowStatus ==
                CREATE_AND_GO)
            ||
            ((pWsscfgSetFsDot11CapabilityProfileEntry->
              MibObject.i4FsDot11CapabilityRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pWsscfgFsDot11CapabilityProfileEntry =
                (tWsscfgFsDot11CapabilityProfileEntry *)
                MemAllocMemBlk (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID);
            if (pWsscfgFsDot11CapabilityProfileEntry == NULL)
            {
                if (WsscfgSetAllFsDot11CapabilityProfileTableTrigger
                    (pWsscfgSetFsDot11CapabilityProfileEntry,
                     pWsscfgIsSetFsDot11CapabilityProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11CapabilityProfileTable:"
                                 "WsscfgSetAllFsDot11CapabilityProfileTableTrigger"
                                 "function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11CapabilityProfileTable: Fail"
                             "to Allocate Memory.\r\n"));
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11CapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11CapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityProfileEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pWsscfgFsDot11CapabilityProfileEntry, 0,
                    sizeof (tWsscfgFsDot11CapabilityProfileEntry));
            if ((WsscfgInitializeFsDot11CapabilityProfileTable
                 (pWsscfgFsDot11CapabilityProfileEntry)) == OSIX_FAILURE)
            {
                if (WsscfgSetAllFsDot11CapabilityProfileTableTrigger
                    (pWsscfgSetFsDot11CapabilityProfileEntry,
                     pWsscfgIsSetFsDot11CapabilityProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11CapabilityProfileTable:"
                                 "WsscfgSetAllFsDot11CapabilityProfileTableTrigger"
                                 "function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11CapabilityProfileTable: Fail"
                             "to Initialize the Objects.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgFsDot11CapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11CapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11CapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityProfileEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11CapabilityProfileName != OSIX_FALSE)
            {
                MEMCPY (pWsscfgFsDot11CapabilityProfileEntry->
                        MibObject.au1FsDot11CapabilityProfileName,
                        pWsscfgSetFsDot11CapabilityProfileEntry->
                        MibObject.au1FsDot11CapabilityProfileName,
                        pWsscfgSetFsDot11CapabilityProfileEntry->
                        MibObject.i4FsDot11CapabilityProfileNameLen);

                pWsscfgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11CapabilityProfileNameLen =
                    pWsscfgSetFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11CapabilityProfileNameLen;
            }

            if (pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11CFPollable
                != OSIX_FALSE)
            {
                pWsscfgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11CFPollable =
                    pWsscfgSetFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11CFPollable;
            }

            if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11CFPollRequest != OSIX_FALSE)
            {
                pWsscfgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11CFPollRequest =
                    pWsscfgSetFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11CFPollRequest;
            }

            if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11PrivacyOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11PrivacyOptionImplemented =
                    pWsscfgSetFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11PrivacyOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11ShortPreambleOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11ShortPreambleOptionImplemented
                    =
                    pWsscfgSetFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11ShortPreambleOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11PBCCOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11PBCCOptionImplemented =
                    pWsscfgSetFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11PBCCOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11ChannelAgilityPresent != OSIX_FALSE)
            {
                pWsscfgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11ChannelAgilityPresent =
                    pWsscfgSetFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11ChannelAgilityPresent;
            }

            if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11QosOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11QosOptionImplemented =
                    pWsscfgSetFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11QosOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11SpectrumManagementRequired != OSIX_FALSE)
            {
                pWsscfgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11SpectrumManagementRequired =
                    pWsscfgSetFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11SpectrumManagementRequired;
            }

            if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11ShortSlotTimeOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11ShortSlotTimeOptionImplemented
                    =
                    pWsscfgSetFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11ShortSlotTimeOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11APSDOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11APSDOptionImplemented =
                    pWsscfgSetFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11APSDOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11DSSSOFDMOptionEnabled != OSIX_FALSE)
            {
                pWsscfgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11DSSSOFDMOptionEnabled =
                    pWsscfgSetFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11DSSSOFDMOptionEnabled;
            }

            if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11DelayedBlockAckOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11DelayedBlockAckOptionImplemented
                    =
                    pWsscfgSetFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11DelayedBlockAckOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11ImmediateBlockAckOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11ImmediateBlockAckOptionImplemented
                    =
                    pWsscfgSetFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11ImmediateBlockAckOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11QAckOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11QAckOptionImplemented =
                    pWsscfgSetFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11QAckOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11QueueRequestOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11QueueRequestOptionImplemented =
                    pWsscfgSetFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11QueueRequestOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11TXOPRequestOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11TXOPRequestOptionImplemented =
                    pWsscfgSetFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11TXOPRequestOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11RSNAOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11RSNAOptionImplemented =
                    pWsscfgSetFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11RSNAOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11RSNAPreauthenticationImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11RSNAPreauthenticationImplemented
                    =
                    pWsscfgSetFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11RSNAPreauthenticationImplemented;
            }

            if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
                bFsDot11CapabilityRowStatus != OSIX_FALSE)
            {
                pWsscfgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11CapabilityRowStatus =
                    pWsscfgSetFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11CapabilityRowStatus;
            }

            if ((pWsscfgSetFsDot11CapabilityProfileEntry->
                 MibObject.i4FsDot11CapabilityRowStatus ==
                 CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsDot11CapabilityProfileEntry->
                        MibObject.i4FsDot11CapabilityRowStatus == ACTIVE)))
            {
                pWsscfgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11CapabilityRowStatus = ACTIVE;
            }
            else if (pWsscfgSetFsDot11CapabilityProfileEntry->
                     MibObject.i4FsDot11CapabilityRowStatus == CREATE_AND_WAIT)
            {
                pWsscfgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11CapabilityRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gWsscfgGlobals.
                 WsscfgGlbMib.FsDot11CapabilityProfileTable,
                 (tRBElem *) pWsscfgFsDot11CapabilityProfileEntry) !=
                RB_SUCCESS)
            {
                if (WsscfgSetAllFsDot11CapabilityProfileTableTrigger
                    (pWsscfgSetFsDot11CapabilityProfileEntry,
                     pWsscfgIsSetFsDot11CapabilityProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11CapabilityProfileTable: "
                                 "WsscfgSetAllFsDot11CapabilityProfileTableTrigger"
                                 "function returns failure.\r\n"));
                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11CapabilityProfileTable:"
                             "RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgFsDot11CapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11CapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11CapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityProfileEntry);
                return OSIX_FAILURE;
            }
            if (WsscfgUtilUpdateFsDot11CapabilityProfileTable
                (NULL, pWsscfgFsDot11CapabilityProfileEntry,
                 pWsscfgIsSetFsDot11CapabilityProfileEntry) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11CapabilityProfileTable:"
                             "WsscfgUtilUpdateFsDot11CapabilityProfileTable"
                             "function returns failure.\r\n"));

                if (WsscfgSetAllFsDot11CapabilityProfileTableTrigger
                    (pWsscfgSetFsDot11CapabilityProfileEntry,
                     pWsscfgIsSetFsDot11CapabilityProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11CapabilityProfileTable:"
                                 "WsscfgSetAllFsDot11CapabilityProfileTableTrigger"
                                 "function returns failure.\r\n"));

                }
                RBTreeRem (gWsscfgGlobals.
                           WsscfgGlbMib.FsDot11CapabilityProfileTable,
                           pWsscfgFsDot11CapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgFsDot11CapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11CapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11CapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityProfileEntry);
                return OSIX_FAILURE;
            }

            if ((pWsscfgSetFsDot11CapabilityProfileEntry->
                 MibObject.i4FsDot11CapabilityRowStatus ==
                 CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsDot11CapabilityProfileEntry->
                        MibObject.i4FsDot11CapabilityRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                MEMCPY (pWsscfgTrgFsDot11CapabilityProfileEntry->
                        MibObject.au1FsDot11CapabilityProfileName,
                        pWsscfgSetFsDot11CapabilityProfileEntry->
                        MibObject.au1FsDot11CapabilityProfileName,
                        pWsscfgSetFsDot11CapabilityProfileEntry->
                        MibObject.i4FsDot11CapabilityProfileNameLen);

                pWsscfgTrgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11CapabilityProfileNameLen =
                    pWsscfgSetFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11CapabilityProfileNameLen;
                pWsscfgTrgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11CapabilityRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11CapabilityProfileEntry->
                    bFsDot11CapabilityRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsDot11CapabilityProfileTableTrigger
                    (pWsscfgTrgFsDot11CapabilityProfileEntry,
                     pWsscfgTrgIsSetFsDot11CapabilityProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11CapabilityProfileTable:"
                                 "WsscfgSetAllFsDot11CapabilityProfileTableTrigger"
                                 "function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgFsDot11CapabilityProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgOldFsDot11CapabilityProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgTrgFsDot11CapabilityProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_ISSET_POOLID,
                         (UINT1 *)
                         pWsscfgTrgIsSetFsDot11CapabilityProfileEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pWsscfgSetFsDot11CapabilityProfileEntry->
                     MibObject.i4FsDot11CapabilityRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11CapabilityRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11CapabilityProfileEntry->
                    bFsDot11CapabilityRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsDot11CapabilityProfileTableTrigger
                    (pWsscfgTrgFsDot11CapabilityProfileEntry,
                     pWsscfgTrgIsSetFsDot11CapabilityProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11CapabilityProfileTable:"
                                 "WsscfgSetAllFsDot11CapabilityProfileTableTrigger"
                                 "function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgFsDot11CapabilityProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgOldFsDot11CapabilityProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgTrgFsDot11CapabilityProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_ISSET_POOLID,
                         (UINT1 *)
                         pWsscfgTrgIsSetFsDot11CapabilityProfileEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pWsscfgSetFsDot11CapabilityProfileEntry->
                MibObject.i4FsDot11CapabilityRowStatus == CREATE_AND_GO)
            {
                pWsscfgSetFsDot11CapabilityProfileEntry->
                    MibObject.i4FsDot11CapabilityRowStatus = ACTIVE;
            }

            if (WsscfgSetAllFsDot11CapabilityProfileTableTrigger
                (pWsscfgSetFsDot11CapabilityProfileEntry,
                 pWsscfgIsSetFsDot11CapabilityProfileEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11CapabilityProfileTable:"
                             "WsscfgSetAllFsDot11CapabilityProfileTableTrigger"
                             "function returns failure.\r\n"));
            }
            gu1WlanCpbltyPrfCount++;
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgOldFsDot11CapabilityProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgTrgFsDot11CapabilityProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityProfileEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (WsscfgSetAllFsDot11CapabilityProfileTableTrigger
                (pWsscfgSetFsDot11CapabilityProfileEntry,
                 pWsscfgIsSetFsDot11CapabilityProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11CapabilityProfileTable:"
                             "WsscfgSetAllFsDot11CapabilityProfileTableTrigger"
                             "function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11CapabilityProfileTable: Failure.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgOldFsDot11CapabilityProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgTrgFsDot11CapabilityProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityProfileEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pWsscfgSetFsDot11CapabilityProfileEntry->
              MibObject.i4FsDot11CapabilityRowStatus ==
              CREATE_AND_WAIT)
             || (pWsscfgSetFsDot11CapabilityProfileEntry->
                 MibObject.i4FsDot11CapabilityRowStatus == CREATE_AND_GO))
    {
        if (WsscfgSetAllFsDot11CapabilityProfileTableTrigger
            (pWsscfgSetFsDot11CapabilityProfileEntry,
             pWsscfgIsSetFsDot11CapabilityProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11CapabilityProfileTable:"
                         "WsscfgSetAllFsDot11CapabilityProfileTableTrigger"
                         "function returns failure.\r\n"));
        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11CapabilityProfileTable: The row"
                     "is already present.\r\n"));
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID, (UINT1 *)
             pWsscfgOldFsDot11CapabilityProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID, (UINT1 *)
             pWsscfgTrgFsDot11CapabilityProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_ISSET_POOLID,
             (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityProfileEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pWsscfgOldFsDot11CapabilityProfileEntry,
            pWsscfgFsDot11CapabilityProfileEntry,
            sizeof (tWsscfgFsDot11CapabilityProfileEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pWsscfgSetFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11CapabilityRowStatus == DESTROY)
    {
        pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11CapabilityRowStatus = DESTROY;

        if (WsscfgUtilUpdateFsDot11CapabilityProfileTable
            (pWsscfgOldFsDot11CapabilityProfileEntry,
             pWsscfgFsDot11CapabilityProfileEntry,
             pWsscfgIsSetFsDot11CapabilityProfileEntry) != OSIX_SUCCESS)
        {

            if (WsscfgSetAllFsDot11CapabilityProfileTableTrigger
                (pWsscfgSetFsDot11CapabilityProfileEntry,
                 pWsscfgIsSetFsDot11CapabilityProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11CapabilityProfileTable:"
                             "WsscfgSetAllFsDot11CapabilityProfileTableTrigger"
                             "function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11CapabilityProfileTable:"
                         "WsscfgUtilUpdateFsDot11CapabilityProfileTable"
                         "function returns failure.\r\n"));
        }
        RBTreeRem (gWsscfgGlobals.
                   WsscfgGlbMib.FsDot11CapabilityProfileTable,
                   pWsscfgFsDot11CapabilityProfileEntry);
        if (WsscfgSetAllFsDot11CapabilityProfileTableTrigger
            (pWsscfgSetFsDot11CapabilityProfileEntry,
             pWsscfgIsSetFsDot11CapabilityProfileEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11CapabilityProfileTable: "
                         "WsscfgSetAllFsDot11CapabilityProfileTableTrigger"
                         "function returns failure.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgOldFsDot11CapabilityProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgTrgFsDot11CapabilityProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityProfileEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgFsDot11CapabilityProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID, (UINT1 *)
             pWsscfgOldFsDot11CapabilityProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID, (UINT1 *)
             pWsscfgTrgFsDot11CapabilityProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_ISSET_POOLID,
             (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityProfileEntry);
        gu1WlanCpbltyPrfCount--;
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsDot11CapabilityProfileTableFilterInputs
        (pWsscfgFsDot11CapabilityProfileEntry,
         pWsscfgSetFsDot11CapabilityProfileEntry,
         pWsscfgIsSetFsDot11CapabilityProfileEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID, (UINT1 *)
             pWsscfgOldFsDot11CapabilityProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID, (UINT1 *)
             pWsscfgTrgFsDot11CapabilityProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_ISSET_POOLID,
             (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityProfileEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying 
     * the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pWsscfgFsDot11CapabilityProfileEntry->
         MibObject.i4FsDot11CapabilityRowStatus == ACTIVE)
        && (pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11CapabilityRowStatus != NOT_IN_SERVICE))
    {
        pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11CapabilityRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pWsscfgTrgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11CapabilityRowStatus = NOT_IN_SERVICE;
        pWsscfgTrgIsSetFsDot11CapabilityProfileEntry->
            bFsDot11CapabilityRowStatus = OSIX_TRUE;

        if (WsscfgUtilUpdateFsDot11CapabilityProfileTable
            (pWsscfgOldFsDot11CapabilityProfileEntry,
             pWsscfgFsDot11CapabilityProfileEntry,
             pWsscfgIsSetFsDot11CapabilityProfileEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pWsscfgFsDot11CapabilityProfileEntry,
                    pWsscfgOldFsDot11CapabilityProfileEntry,
                    sizeof (tWsscfgFsDot11CapabilityProfileEntry));
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11CapabilityProfileTable:"
                         "WsscfgUtilUpdateFsDot11CapabilityProfileTable"
                         "Function returns failure.\r\n"));

            if (WsscfgSetAllFsDot11CapabilityProfileTableTrigger
                (pWsscfgSetFsDot11CapabilityProfileEntry,
                 pWsscfgIsSetFsDot11CapabilityProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11CapabilityProfileTable:"
                             "WsscfgSetAllFsDot11CapabilityProfileTableTrigger"
                             "function returns failure.\r\n"));
            }
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgOldFsDot11CapabilityProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgTrgFsDot11CapabilityProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityProfileEntry);
            return OSIX_FAILURE;
        }

        if (WsscfgSetAllFsDot11CapabilityProfileTableTrigger
            (pWsscfgTrgFsDot11CapabilityProfileEntry,
             pWsscfgTrgIsSetFsDot11CapabilityProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11CapabilityProfileTable:"
                         "WsscfgSetAllFsDot11CapabilityProfileTableTrigger"
                         "function returns failure.\r\n"));
        }
    }

    if (pWsscfgSetFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11CapabilityRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11CFPollable != OSIX_FALSE)
    {
        pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11CFPollable =
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11CFPollable;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11CFPollRequest != OSIX_FALSE)
    {
        pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11CFPollRequest =
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11CFPollRequest;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11PrivacyOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11PrivacyOptionImplemented =
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11PrivacyOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11ShortPreambleOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11ShortPreambleOptionImplemented =
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11ShortPreambleOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11PBCCOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11PBCCOptionImplemented =
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11PBCCOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11ChannelAgilityPresent != OSIX_FALSE)
    {
        pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11ChannelAgilityPresent =
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11ChannelAgilityPresent;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11QosOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11QosOptionImplemented =
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11QosOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11SpectrumManagementRequired != OSIX_FALSE)
    {
        pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11SpectrumManagementRequired =
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11SpectrumManagementRequired;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11ShortSlotTimeOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11ShortSlotTimeOptionImplemented =
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11ShortSlotTimeOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11APSDOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11APSDOptionImplemented =
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11APSDOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11DSSSOFDMOptionEnabled != OSIX_FALSE)
    {
        pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11DSSSOFDMOptionEnabled =
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11DSSSOFDMOptionEnabled;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11DelayedBlockAckOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11DelayedBlockAckOptionImplemented =
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11DelayedBlockAckOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11ImmediateBlockAckOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11ImmediateBlockAckOptionImplemented =
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11ImmediateBlockAckOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11QAckOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11QAckOptionImplemented =
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11QAckOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11QueueRequestOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11QueueRequestOptionImplemented =
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11QueueRequestOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11TXOPRequestOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11TXOPRequestOptionImplemented =
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11TXOPRequestOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11RSNAOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11RSNAOptionImplemented =
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11RSNAOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11RSNAPreauthenticationImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11RSNAPreauthenticationImplemented =
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11RSNAPreauthenticationImplemented;
    }
    if (pWsscfgIsSetFsDot11CapabilityProfileEntry->
        bFsDot11CapabilityRowStatus != OSIX_FALSE)
    {
        pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11CapabilityRowStatus =
            pWsscfgSetFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11CapabilityRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE 
     * after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pWsscfgFsDot11CapabilityProfileEntry->
            MibObject.i4FsDot11CapabilityRowStatus = ACTIVE;
    }

    if (WsscfgUtilUpdateFsDot11CapabilityProfileTable
        (pWsscfgOldFsDot11CapabilityProfileEntry,
         pWsscfgFsDot11CapabilityProfileEntry,
         pWsscfgIsSetFsDot11CapabilityProfileEntry) != OSIX_SUCCESS)
    {

        if (WsscfgSetAllFsDot11CapabilityProfileTableTrigger
            (pWsscfgSetFsDot11CapabilityProfileEntry,
             pWsscfgIsSetFsDot11CapabilityProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11CapabilityProfileTable:"
                         "WsscfgSetAllFsDot11CapabilityProfileTableTrigger"
                         "function returns failure.\r\n"));

        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11CapabilityProfileTable:"
                     "WsscfgUtilUpdateFsDot11CapabilityProfileTable "
                     "function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pWsscfgFsDot11CapabilityProfileEntry,
                pWsscfgOldFsDot11CapabilityProfileEntry,
                sizeof (tWsscfgFsDot11CapabilityProfileEntry));
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID, (UINT1 *)
             pWsscfgOldFsDot11CapabilityProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID, (UINT1 *)
             pWsscfgTrgFsDot11CapabilityProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_ISSET_POOLID,
             (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityProfileEntry);
        return OSIX_FAILURE;

    }
    if (WsscfgSetAllFsDot11CapabilityProfileTableTrigger
        (pWsscfgSetFsDot11CapabilityProfileEntry,
         pWsscfgIsSetFsDot11CapabilityProfileEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11CapabilityProfileTable:"
                     "WsscfgSetAllFsDot11CapabilityProfileTableTrigger "
                     "function returns failure.\r\n"));
    }

    MemReleaseMemBlock (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                        (UINT1 *) pWsscfgOldFsDot11CapabilityProfileEntry);
    MemReleaseMemBlock (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID,
                        (UINT1 *) pWsscfgTrgFsDot11CapabilityProfileEntry);
    MemReleaseMemBlock
        (WSSCFG_FSDOT11CAPABILITYPROFILETABLE_ISSET_POOLID,
         (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityProfileEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11AuthenticationProfileTable
 Input       :  pWsscfgSetFsDot11AuthenticationProfileEntry
                pWsscfgIsSetFsDot11AuthenticationProfileEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgSetAllFsDot11AuthenticationProfileTable
    (tWsscfgFsDot11AuthenticationProfileEntry *
     pWsscfgSetFsDot11AuthenticationProfileEntry,
     tWsscfgIsSetFsDot11AuthenticationProfileEntry *
     pWsscfgIsSetFsDot11AuthenticationProfileEntry, INT4 i4RowStatusLogic,
     INT4 i4RowCreateOption)
{
    tWsscfgFsDot11AuthenticationProfileEntry
        * pWsscfgFsDot11AuthenticationProfileEntry = NULL;
    tWsscfgFsDot11AuthenticationProfileEntry
        * pWsscfgOldFsDot11AuthenticationProfileEntry = NULL;
    tWsscfgFsDot11AuthenticationProfileEntry
        * pWsscfgTrgFsDot11AuthenticationProfileEntry = NULL;
    tWsscfgIsSetFsDot11AuthenticationProfileEntry
        * pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pWsscfgOldFsDot11AuthenticationProfileEntry =
        (tWsscfgFsDot11AuthenticationProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID);
    if (pWsscfgOldFsDot11AuthenticationProfileEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWsscfgTrgFsDot11AuthenticationProfileEntry =
        (tWsscfgFsDot11AuthenticationProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID);
    if (pWsscfgTrgFsDot11AuthenticationProfileEntry == NULL)
    {
        MemReleaseMemBlock
            (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgOldFsDot11AuthenticationProfileEntry);
        return OSIX_FAILURE;
    }
    pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry =
        (tWsscfgIsSetFsDot11AuthenticationProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_ISSET_POOLID);
    if (pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry == NULL)
    {
        MemReleaseMemBlock
            (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgOldFsDot11AuthenticationProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgTrgFsDot11AuthenticationProfileEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pWsscfgOldFsDot11AuthenticationProfileEntry, 0,
            sizeof (tWsscfgFsDot11AuthenticationProfileEntry));
    MEMSET (pWsscfgTrgFsDot11AuthenticationProfileEntry, 0,
            sizeof (tWsscfgFsDot11AuthenticationProfileEntry));
    MEMSET (pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11AuthenticationProfileEntry));

    /* Check whether the node is already present */
    pWsscfgFsDot11AuthenticationProfileEntry =
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.FsDot11AuthenticationProfileTable,
                   (tRBElem *) pWsscfgSetFsDot11AuthenticationProfileEntry);

    if (pWsscfgFsDot11AuthenticationProfileEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or
         * CREATE_AND_GO */
        if ((pWsscfgSetFsDot11AuthenticationProfileEntry->
             MibObject.i4FsDot11AuthenticationRowStatus ==
             CREATE_AND_WAIT)
            || (pWsscfgSetFsDot11AuthenticationProfileEntry->
                MibObject.i4FsDot11AuthenticationRowStatus ==
                CREATE_AND_GO)
            ||
            ((pWsscfgSetFsDot11AuthenticationProfileEntry->
              MibObject.i4FsDot11AuthenticationRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pWsscfgFsDot11AuthenticationProfileEntry =
                (tWsscfgFsDot11AuthenticationProfileEntry *)
                MemAllocMemBlk
                (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID);
            if (pWsscfgFsDot11AuthenticationProfileEntry == NULL)
            {
                if (WsscfgSetAllFsDot11AuthenticationProfileTableTrigger
                    (pWsscfgSetFsDot11AuthenticationProfileEntry,
                     pWsscfgIsSetFsDot11AuthenticationProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11AuthenticationProfileTable:"
                                 "WsscfgSetAllFsDot11AuthenticationProfileTableTrigger"
                                 "function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11AuthenticationProfileTable:"
                             "Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11AuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11AuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_ISSET_POOLID,
                     (UINT1 *)
                     pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pWsscfgFsDot11AuthenticationProfileEntry, 0,
                    sizeof (tWsscfgFsDot11AuthenticationProfileEntry));
            if ((WsscfgInitializeFsDot11AuthenticationProfileTable
                 (pWsscfgFsDot11AuthenticationProfileEntry)) == OSIX_FAILURE)
            {
                if (WsscfgSetAllFsDot11AuthenticationProfileTableTrigger
                    (pWsscfgSetFsDot11AuthenticationProfileEntry,
                     pWsscfgIsSetFsDot11AuthenticationProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11AuthenticationProfileTable"
                                 ":WsscfgSetAllFsDot11AuthenticationProfileTableTrigger"
                                 "function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11AuthenticationProfileTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgFsDot11AuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11AuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11AuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_ISSET_POOLID,
                     (UINT1 *)
                     pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
                bFsDot11AuthenticationProfileName != OSIX_FALSE)
            {
                MEMCPY (pWsscfgFsDot11AuthenticationProfileEntry->
                        MibObject.au1FsDot11AuthenticationProfileName,
                        pWsscfgSetFsDot11AuthenticationProfileEntry->MibObject.
                        au1FsDot11AuthenticationProfileName,
                        pWsscfgSetFsDot11AuthenticationProfileEntry->MibObject.
                        i4FsDot11AuthenticationProfileNameLen);

                pWsscfgFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11AuthenticationProfileNameLen =
                    pWsscfgSetFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11AuthenticationProfileNameLen;
            }

            if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
                bFsDot11AuthenticationAlgorithm != OSIX_FALSE)
            {
                pWsscfgFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11AuthenticationAlgorithm =
                    pWsscfgSetFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11AuthenticationAlgorithm;
            }

            if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
                bFsDot11WepKeyIndex != OSIX_FALSE)
            {
                pWsscfgFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11WepKeyIndex =
                    pWsscfgSetFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11WepKeyIndex;
            }

            if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
                bFsDot11WepKeyType != OSIX_FALSE)
            {
                pWsscfgFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11WepKeyType =
                    pWsscfgSetFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11WepKeyType;
            }

            if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
                bFsDot11WepKeyLength != OSIX_FALSE)
            {
                pWsscfgFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11WepKeyLength =
                    pWsscfgSetFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11WepKeyLength;
            }

            if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKey !=
                OSIX_FALSE)
            {
                MEMCPY (pWsscfgFsDot11AuthenticationProfileEntry->
                        MibObject.au1FsDot11WepKey,
                        pWsscfgSetFsDot11AuthenticationProfileEntry->MibObject.
                        au1FsDot11WepKey,
                        pWsscfgSetFsDot11AuthenticationProfileEntry->MibObject.
                        i4FsDot11WepKeyLen);

                pWsscfgFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11WepKeyLen =
                    pWsscfgSetFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11WepKeyLen;
            }

            if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
                bFsDot11WebAuthentication != OSIX_FALSE)
            {
                pWsscfgFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11WebAuthentication =
                    pWsscfgSetFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11WebAuthentication;
            }

            if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
                bFsDot11AuthenticationRowStatus != OSIX_FALSE)
            {
                pWsscfgFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11AuthenticationRowStatus =
                    pWsscfgSetFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11AuthenticationRowStatus;
            }

            if ((pWsscfgSetFsDot11AuthenticationProfileEntry->
                 MibObject.i4FsDot11AuthenticationRowStatus ==
                 CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    &&
                    (pWsscfgSetFsDot11AuthenticationProfileEntry->MibObject.
                     i4FsDot11AuthenticationRowStatus == ACTIVE)))
            {
                pWsscfgFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11AuthenticationRowStatus = ACTIVE;
            }
            else if
                (pWsscfgSetFsDot11AuthenticationProfileEntry->MibObject.
                 i4FsDot11AuthenticationRowStatus == CREATE_AND_WAIT)
            {
                pWsscfgFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11AuthenticationRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gWsscfgGlobals.
                 WsscfgGlbMib.FsDot11AuthenticationProfileTable,
                 (tRBElem *) pWsscfgFsDot11AuthenticationProfileEntry)
                != RB_SUCCESS)
            {
                if (WsscfgSetAllFsDot11AuthenticationProfileTableTrigger
                    (pWsscfgSetFsDot11AuthenticationProfileEntry,
                     pWsscfgIsSetFsDot11AuthenticationProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11AuthenticationProfileTable: WsscfgSetAllFsDot11AuthenticationProfileTableTrigger function returns failure.\r\n"));
                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11AuthenticationProfileTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgFsDot11AuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11AuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11AuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_ISSET_POOLID,
                     (UINT1 *)
                     pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry);
                return OSIX_FAILURE;
            }
            if (WsscfgUtilUpdateFsDot11AuthenticationProfileTable
                (NULL, pWsscfgFsDot11AuthenticationProfileEntry,
                 pWsscfgIsSetFsDot11AuthenticationProfileEntry) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11AuthenticationProfileTable: WsscfgUtilUpdateFsDot11AuthenticationProfileTable function returns failure.\r\n"));

                if (WsscfgSetAllFsDot11AuthenticationProfileTableTrigger
                    (pWsscfgSetFsDot11AuthenticationProfileEntry,
                     pWsscfgIsSetFsDot11AuthenticationProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11AuthenticationProfileTable: WsscfgSetAllFsDot11AuthenticationProfileTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gWsscfgGlobals.
                           WsscfgGlbMib.FsDot11AuthenticationProfileTable,
                           pWsscfgFsDot11AuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgFsDot11AuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11AuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11AuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_ISSET_POOLID,
                     (UINT1 *)
                     pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry);
                return OSIX_FAILURE;
            }

            if ((pWsscfgSetFsDot11AuthenticationProfileEntry->
                 MibObject.i4FsDot11AuthenticationRowStatus ==
                 CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    &&
                    (pWsscfgSetFsDot11AuthenticationProfileEntry->MibObject.
                     i4FsDot11AuthenticationRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                MEMCPY
                    (pWsscfgTrgFsDot11AuthenticationProfileEntry->MibObject.
                     au1FsDot11AuthenticationProfileName,
                     pWsscfgSetFsDot11AuthenticationProfileEntry->MibObject.
                     au1FsDot11AuthenticationProfileName,
                     pWsscfgSetFsDot11AuthenticationProfileEntry->MibObject.
                     i4FsDot11AuthenticationProfileNameLen);

                pWsscfgTrgFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11AuthenticationProfileNameLen =
                    pWsscfgSetFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11AuthenticationProfileNameLen;
                pWsscfgTrgFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11AuthenticationRowStatus =
                    CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry->
                    bFsDot11AuthenticationRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsDot11AuthenticationProfileTableTrigger
                    (pWsscfgTrgFsDot11AuthenticationProfileEntry,
                     pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11AuthenticationProfileTable: WsscfgSetAllFsDot11AuthenticationProfileTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgFsDot11AuthenticationProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgOldFsDot11AuthenticationProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgTrgFsDot11AuthenticationProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_ISSET_POOLID,
                         (UINT1 *)
                         pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry);
                    return OSIX_FAILURE;
                }
            }
            else if
                (pWsscfgSetFsDot11AuthenticationProfileEntry->MibObject.
                 i4FsDot11AuthenticationRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11AuthenticationRowStatus =
                    CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry->
                    bFsDot11AuthenticationRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsDot11AuthenticationProfileTableTrigger
                    (pWsscfgTrgFsDot11AuthenticationProfileEntry,
                     pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11AuthenticationProfileTable: WsscfgSetAllFsDot11AuthenticationProfileTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgFsDot11AuthenticationProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgOldFsDot11AuthenticationProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgTrgFsDot11AuthenticationProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_ISSET_POOLID,
                         (UINT1 *)
                         pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pWsscfgSetFsDot11AuthenticationProfileEntry->
                MibObject.i4FsDot11AuthenticationRowStatus == CREATE_AND_GO)
            {
                pWsscfgSetFsDot11AuthenticationProfileEntry->
                    MibObject.i4FsDot11AuthenticationRowStatus = ACTIVE;
            }

            if (WsscfgSetAllFsDot11AuthenticationProfileTableTrigger
                (pWsscfgSetFsDot11AuthenticationProfileEntry,
                 pWsscfgIsSetFsDot11AuthenticationProfileEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11AuthenticationProfileTable:  WsscfgSetAllFsDot11AuthenticationProfileTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock
                (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgOldFsDot11AuthenticationProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgTrgFsDot11AuthenticationProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (WsscfgSetAllFsDot11AuthenticationProfileTableTrigger
                (pWsscfgSetFsDot11AuthenticationProfileEntry,
                 pWsscfgIsSetFsDot11AuthenticationProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11AuthenticationProfileTable: WsscfgSetAllFsDot11AuthenticationProfileTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11AuthenticationProfileTable: Failure.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgOldFsDot11AuthenticationProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgTrgFsDot11AuthenticationProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pWsscfgSetFsDot11AuthenticationProfileEntry->
              MibObject.i4FsDot11AuthenticationRowStatus ==
              CREATE_AND_WAIT)
             || (pWsscfgSetFsDot11AuthenticationProfileEntry->
                 MibObject.i4FsDot11AuthenticationRowStatus == CREATE_AND_GO))
    {
        if (WsscfgSetAllFsDot11AuthenticationProfileTableTrigger
            (pWsscfgSetFsDot11AuthenticationProfileEntry,
             pWsscfgIsSetFsDot11AuthenticationProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11AuthenticationProfileTable: WsscfgSetAllFsDot11AuthenticationProfileTableTrigger function returns failure.\r\n"));
        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11AuthenticationProfileTable: The row is already present.\r\n"));
        MemReleaseMemBlock
            (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgOldFsDot11AuthenticationProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgTrgFsDot11AuthenticationProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_ISSET_POOLID,
             (UINT1 *) pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pWsscfgOldFsDot11AuthenticationProfileEntry,
            pWsscfgFsDot11AuthenticationProfileEntry,
            sizeof (tWsscfgFsDot11AuthenticationProfileEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pWsscfgSetFsDot11AuthenticationProfileEntry->
        MibObject.i4FsDot11AuthenticationRowStatus == DESTROY)
    {
        pWsscfgFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11AuthenticationRowStatus = DESTROY;

        if (WsscfgUtilUpdateFsDot11AuthenticationProfileTable
            (pWsscfgOldFsDot11AuthenticationProfileEntry,
             pWsscfgFsDot11AuthenticationProfileEntry,
             pWsscfgIsSetFsDot11AuthenticationProfileEntry) != OSIX_SUCCESS)
        {

            if (WsscfgSetAllFsDot11AuthenticationProfileTableTrigger
                (pWsscfgSetFsDot11AuthenticationProfileEntry,
                 pWsscfgIsSetFsDot11AuthenticationProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11AuthenticationProfileTable: WsscfgSetAllFsDot11AuthenticationProfileTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11AuthenticationProfileTable: WsscfgUtilUpdateFsDot11AuthenticationProfileTable function returns failure.\r\n"));
        }
        RBTreeRem (gWsscfgGlobals.
                   WsscfgGlbMib.FsDot11AuthenticationProfileTable,
                   pWsscfgFsDot11AuthenticationProfileEntry);
        if (WsscfgSetAllFsDot11AuthenticationProfileTableTrigger
            (pWsscfgSetFsDot11AuthenticationProfileEntry,
             pWsscfgIsSetFsDot11AuthenticationProfileEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11AuthenticationProfileTable: WsscfgSetAllFsDot11AuthenticationProfileTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgOldFsDot11AuthenticationProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgTrgFsDot11AuthenticationProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock
            (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgFsDot11AuthenticationProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgOldFsDot11AuthenticationProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgTrgFsDot11AuthenticationProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_ISSET_POOLID,
             (UINT1 *) pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsDot11AuthenticationProfileTableFilterInputs
        (pWsscfgFsDot11AuthenticationProfileEntry,
         pWsscfgSetFsDot11AuthenticationProfileEntry,
         pWsscfgIsSetFsDot11AuthenticationProfileEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock
            (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgOldFsDot11AuthenticationProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgTrgFsDot11AuthenticationProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_ISSET_POOLID,
             (UINT1 *) pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pWsscfgFsDot11AuthenticationProfileEntry->
         MibObject.i4FsDot11AuthenticationRowStatus == ACTIVE)
        && (pWsscfgSetFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11AuthenticationRowStatus != NOT_IN_SERVICE))
    {
        pWsscfgFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11AuthenticationRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pWsscfgTrgFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11AuthenticationRowStatus = NOT_IN_SERVICE;
        pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry->
            bFsDot11AuthenticationRowStatus = OSIX_TRUE;

        if (WsscfgUtilUpdateFsDot11AuthenticationProfileTable
            (pWsscfgOldFsDot11AuthenticationProfileEntry,
             pWsscfgFsDot11AuthenticationProfileEntry,
             pWsscfgIsSetFsDot11AuthenticationProfileEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pWsscfgFsDot11AuthenticationProfileEntry,
                    pWsscfgOldFsDot11AuthenticationProfileEntry,
                    sizeof (tWsscfgFsDot11AuthenticationProfileEntry));
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11AuthenticationProfileTable:                 WsscfgUtilUpdateFsDot11AuthenticationProfileTable Function returns failure.\r\n"));

            if (WsscfgSetAllFsDot11AuthenticationProfileTableTrigger
                (pWsscfgSetFsDot11AuthenticationProfileEntry,
                 pWsscfgIsSetFsDot11AuthenticationProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11AuthenticationProfileTable: WsscfgSetAllFsDot11AuthenticationProfileTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock
                (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgOldFsDot11AuthenticationProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgTrgFsDot11AuthenticationProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry);
            return OSIX_FAILURE;
        }

        if (WsscfgSetAllFsDot11AuthenticationProfileTableTrigger
            (pWsscfgTrgFsDot11AuthenticationProfileEntry,
             pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11AuthenticationProfileTable: WsscfgSetAllFsDot11AuthenticationProfileTableTrigger function returns failure.\r\n"));
        }
    }

    if (pWsscfgSetFsDot11AuthenticationProfileEntry->
        MibObject.i4FsDot11AuthenticationRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
        bFsDot11AuthenticationAlgorithm != OSIX_FALSE)
    {
        pWsscfgFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11AuthenticationAlgorithm =
            pWsscfgSetFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11AuthenticationAlgorithm;
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKeyIndex !=
        OSIX_FALSE)
    {
        pWsscfgFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11WepKeyIndex =
            pWsscfgSetFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11WepKeyIndex;
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKeyType !=
        OSIX_FALSE)
    {
        pWsscfgFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11WepKeyType =
            pWsscfgSetFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11WepKeyType;
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKeyLength !=
        OSIX_FALSE)
    {
        pWsscfgFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11WepKeyLength =
            pWsscfgSetFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11WepKeyLength;
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
        bFsDot11WepKey != OSIX_FALSE)
    {
        MEMCPY (pWsscfgFsDot11AuthenticationProfileEntry->
                MibObject.au1FsDot11WepKey,
                pWsscfgSetFsDot11AuthenticationProfileEntry->
                MibObject.au1FsDot11WepKey,
                pWsscfgSetFsDot11AuthenticationProfileEntry->
                MibObject.i4FsDot11WepKeyLen);

        pWsscfgFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11WepKeyLen =
            pWsscfgSetFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11WepKeyLen;
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
        bFsDot11WebAuthentication != OSIX_FALSE)
    {
        pWsscfgFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11WebAuthentication =
            pWsscfgSetFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11WebAuthentication;
    }
    if (pWsscfgIsSetFsDot11AuthenticationProfileEntry->
        bFsDot11AuthenticationRowStatus != OSIX_FALSE)
    {
        pWsscfgFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11AuthenticationRowStatus =
            pWsscfgSetFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11AuthenticationRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pWsscfgFsDot11AuthenticationProfileEntry->
            MibObject.i4FsDot11AuthenticationRowStatus = ACTIVE;
    }

    if (WsscfgUtilUpdateFsDot11AuthenticationProfileTable
        (pWsscfgOldFsDot11AuthenticationProfileEntry,
         pWsscfgFsDot11AuthenticationProfileEntry,
         pWsscfgIsSetFsDot11AuthenticationProfileEntry) != OSIX_SUCCESS)
    {

        if (WsscfgSetAllFsDot11AuthenticationProfileTableTrigger
            (pWsscfgSetFsDot11AuthenticationProfileEntry,
             pWsscfgIsSetFsDot11AuthenticationProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11AuthenticationProfileTable: WsscfgSetAllFsDot11AuthenticationProfileTableTrigger function returns failure.\r\n"));

        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11AuthenticationProfileTable: WsscfgUtilUpdateFsDot11AuthenticationProfileTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pWsscfgFsDot11AuthenticationProfileEntry,
                pWsscfgOldFsDot11AuthenticationProfileEntry,
                sizeof (tWsscfgFsDot11AuthenticationProfileEntry));
        MemReleaseMemBlock
            (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgOldFsDot11AuthenticationProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgTrgFsDot11AuthenticationProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_ISSET_POOLID,
             (UINT1 *) pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry);
        return OSIX_FAILURE;

    }
    if (WsscfgSetAllFsDot11AuthenticationProfileTableTrigger
        (pWsscfgSetFsDot11AuthenticationProfileEntry,
         pWsscfgIsSetFsDot11AuthenticationProfileEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11AuthenticationProfileTable: WsscfgSetAllFsDot11AuthenticationProfileTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock
        (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID, (UINT1 *)
         pWsscfgOldFsDot11AuthenticationProfileEntry);
    MemReleaseMemBlock
        (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID, (UINT1 *)
         pWsscfgTrgFsDot11AuthenticationProfileEntry);
    MemReleaseMemBlock
        (WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_ISSET_POOLID,
         (UINT1 *) pWsscfgTrgIsSetFsDot11AuthenticationProfileEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgSetAllFsStationQosParamsTable
 Input       :  pWsscfgSetFsStationQosParamsEntry
                pWsscfgIsSetFsStationQosParamsEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllFsStationQosParamsTable (tWsscfgFsStationQosParamsEntry *
                                     pWsscfgSetFsStationQosParamsEntry,
                                     tWsscfgIsSetFsStationQosParamsEntry *
                                     pWsscfgIsSetFsStationQosParamsEntry)
{
    tWsscfgFsStationQosParamsEntry *pWsscfgFsStationQosParamsEntry = NULL;
    tWsscfgFsStationQosParamsEntry WsscfgOldFsStationQosParamsEntry;

    MEMSET (&WsscfgOldFsStationQosParamsEntry, 0,
            sizeof (tWsscfgFsStationQosParamsEntry));

    /* Check whether the node is already present */
    pWsscfgFsStationQosParamsEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsStationQosParamsTable,
                   (tRBElem *) pWsscfgSetFsStationQosParamsEntry);

    if (pWsscfgFsStationQosParamsEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsStationQosParamsTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsStationQosParamsTableFilterInputs
        (pWsscfgFsStationQosParamsEntry,
         pWsscfgSetFsStationQosParamsEntry,
         pWsscfgIsSetFsStationQosParamsEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&WsscfgOldFsStationQosParamsEntry,
            pWsscfgFsStationQosParamsEntry,
            sizeof (tWsscfgFsStationQosParamsEntry));

    /* Assign values for the existing node */
    if (pWsscfgIsSetFsStationQosParamsEntry->bFsStaQoSPriority != OSIX_FALSE)
    {
        pWsscfgFsStationQosParamsEntry->MibObject.i4FsStaQoSPriority =
            pWsscfgSetFsStationQosParamsEntry->MibObject.i4FsStaQoSPriority;
    }
    if (pWsscfgIsSetFsStationQosParamsEntry->bFsStaQoSDscp != OSIX_FALSE)
    {
        pWsscfgFsStationQosParamsEntry->MibObject.i4FsStaQoSDscp =
            pWsscfgSetFsStationQosParamsEntry->MibObject.i4FsStaQoSDscp;
    }

    if (WsscfgUtilUpdateFsStationQosParamsTable
        (&WsscfgOldFsStationQosParamsEntry,
         pWsscfgFsStationQosParamsEntry,
         pWsscfgIsSetFsStationQosParamsEntry) != OSIX_SUCCESS)
    {
        if (WsscfgSetAllFsStationQosParamsTableTrigger
            (pWsscfgSetFsStationQosParamsEntry,
             pWsscfgIsSetFsStationQosParamsEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pWsscfgFsStationQosParamsEntry,
                &WsscfgOldFsStationQosParamsEntry,
                sizeof (tWsscfgFsStationQosParamsEntry));
        return OSIX_FAILURE;
    }

    if (WsscfgSetAllFsStationQosParamsTableTrigger
        (pWsscfgSetFsStationQosParamsEntry,
         pWsscfgIsSetFsStationQosParamsEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsVlanIsolationTable
 Input       :  pWsscfgSetFsVlanIsolationEntry
                pWsscfgIsSetFsVlanIsolationEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllFsVlanIsolationTable (tWsscfgFsVlanIsolationEntry *
                                  pWsscfgSetFsVlanIsolationEntry,
                                  tWsscfgIsSetFsVlanIsolationEntry *
                                  pWsscfgIsSetFsVlanIsolationEntry)
{
    tWsscfgFsVlanIsolationEntry WsscfgOldFsVlanIsolationEntry;

    MEMSET (&WsscfgOldFsVlanIsolationEntry, 0,
            sizeof (tWsscfgFsVlanIsolationEntry));
    if (WsscfgUtilUpdateFsVlanIsolationTable
        (&WsscfgOldFsVlanIsolationEntry,
         pWsscfgSetFsVlanIsolationEntry,
         pWsscfgIsSetFsVlanIsolationEntry) != OSIX_SUCCESS)
    {
        if (WsscfgSetAllFsVlanIsolationTableTrigger
            (pWsscfgSetFsVlanIsolationEntry,
             pWsscfgIsSetFsVlanIsolationEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
        }
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11RadioConfigTable
 Input       :  pWsscfgSetFsDot11RadioConfigEntry
                pWsscfgIsSetFsDot11RadioConfigEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllFsDot11RadioConfigTable (tWsscfgFsDot11RadioConfigEntry *
                                     pWsscfgSetFsDot11RadioConfigEntry,
                                     tWsscfgIsSetFsDot11RadioConfigEntry *
                                     pWsscfgIsSetFsDot11RadioConfigEntry,
                                     INT4 i4RowStatusLogic,
                                     INT4 i4RowCreateOption)
{
    tWsscfgFsDot11RadioConfigEntry *pWsscfgFsDot11RadioConfigEntry = NULL;
    tWsscfgFsDot11RadioConfigEntry *pWsscfgOldFsDot11RadioConfigEntry = NULL;
    tWsscfgFsDot11RadioConfigEntry *pWsscfgTrgFsDot11RadioConfigEntry = NULL;
    tWsscfgIsSetFsDot11RadioConfigEntry *pWsscfgTrgIsSetFsDot11RadioConfigEntry
        = NULL;
    INT4                i4RowMakeActive = FALSE;

    pWsscfgOldFsDot11RadioConfigEntry =
        (tWsscfgFsDot11RadioConfigEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID);
    if (pWsscfgOldFsDot11RadioConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWsscfgTrgFsDot11RadioConfigEntry =
        (tWsscfgFsDot11RadioConfigEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID);
    if (pWsscfgTrgFsDot11RadioConfigEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11RadioConfigEntry);
        return OSIX_FAILURE;
    }
    pWsscfgTrgIsSetFsDot11RadioConfigEntry =
        (tWsscfgIsSetFsDot11RadioConfigEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11RADIOCONFIGTABLE_ISSET_POOLID);
    if (pWsscfgTrgIsSetFsDot11RadioConfigEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11RadioConfigEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11RadioConfigEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pWsscfgOldFsDot11RadioConfigEntry, 0,
            sizeof (tWsscfgFsDot11RadioConfigEntry));
    MEMSET (pWsscfgTrgFsDot11RadioConfigEntry, 0,
            sizeof (tWsscfgFsDot11RadioConfigEntry));
    MEMSET (pWsscfgTrgIsSetFsDot11RadioConfigEntry, 0,
            sizeof (tWsscfgIsSetFsDot11RadioConfigEntry));

    /* Check whether the node is already present */
    pWsscfgFsDot11RadioConfigEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsDot11RadioConfigTable,
                   (tRBElem *) pWsscfgSetFsDot11RadioConfigEntry);

    if (pWsscfgFsDot11RadioConfigEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pWsscfgSetFsDot11RadioConfigEntry->
             MibObject.i4FsDot11RowStatus == CREATE_AND_WAIT)
            || (pWsscfgSetFsDot11RadioConfigEntry->
                MibObject.i4FsDot11RowStatus == CREATE_AND_GO)
            ||
            ((pWsscfgSetFsDot11RadioConfigEntry->
              MibObject.i4FsDot11RowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pWsscfgFsDot11RadioConfigEntry =
                (tWsscfgFsDot11RadioConfigEntry *)
                MemAllocMemBlk (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID);
            if (pWsscfgFsDot11RadioConfigEntry == NULL)
            {
                if (WsscfgSetAllFsDot11RadioConfigTableTrigger
                    (pWsscfgSetFsDot11RadioConfigEntry,
                     pWsscfgIsSetFsDot11RadioConfigEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11RadioConfigTable:WsscfgSetAllFsDot11RadioConfigTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11RadioConfigTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID, (UINT1 *)
                     pWsscfgOldFsDot11RadioConfigEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgFsDot11RadioConfigEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11RADIOCONFIGTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11RadioConfigEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pWsscfgFsDot11RadioConfigEntry, 0,
                    sizeof (tWsscfgFsDot11RadioConfigEntry));
            if ((WsscfgInitializeFsDot11RadioConfigTable
                 (pWsscfgFsDot11RadioConfigEntry)) == OSIX_FAILURE)
            {
                if (WsscfgSetAllFsDot11RadioConfigTableTrigger
                    (pWsscfgSetFsDot11RadioConfigEntry,
                     pWsscfgIsSetFsDot11RadioConfigEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11RadioConfigTable:WsscfgSetAllFsDot11RadioConfigTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11RadioConfigTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID, (UINT1 *)
                     pWsscfgFsDot11RadioConfigEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID, (UINT1 *)
                     pWsscfgOldFsDot11RadioConfigEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgFsDot11RadioConfigEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11RADIOCONFIGTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11RadioConfigEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pWsscfgIsSetFsDot11RadioConfigEntry->
                bFsDot11RadioType != OSIX_FALSE)
            {
                pWsscfgFsDot11RadioConfigEntry->
                    MibObject.u4FsDot11RadioType =
                    pWsscfgSetFsDot11RadioConfigEntry->
                    MibObject.u4FsDot11RadioType;
            }

            if (pWsscfgIsSetFsDot11RadioConfigEntry->
                bFsDot11RowStatus != OSIX_FALSE)
            {
                pWsscfgFsDot11RadioConfigEntry->
                    MibObject.i4FsDot11RowStatus =
                    pWsscfgSetFsDot11RadioConfigEntry->
                    MibObject.i4FsDot11RowStatus;
            }

            if (pWsscfgIsSetFsDot11RadioConfigEntry->bIfIndex != OSIX_FALSE)
            {
                pWsscfgFsDot11RadioConfigEntry->MibObject.i4IfIndex =
                    pWsscfgSetFsDot11RadioConfigEntry->MibObject.i4IfIndex;
            }

            if ((pWsscfgSetFsDot11RadioConfigEntry->
                 MibObject.i4FsDot11RowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsDot11RadioConfigEntry->
                        MibObject.i4FsDot11RowStatus == ACTIVE)))
            {
                pWsscfgFsDot11RadioConfigEntry->
                    MibObject.i4FsDot11RowStatus = ACTIVE;
            }
            else if (pWsscfgSetFsDot11RadioConfigEntry->
                     MibObject.i4FsDot11RowStatus == CREATE_AND_WAIT)
            {
                pWsscfgFsDot11RadioConfigEntry->
                    MibObject.i4FsDot11RowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gWsscfgGlobals.WsscfgGlbMib.FsDot11RadioConfigTable,
                 (tRBElem *) pWsscfgFsDot11RadioConfigEntry) != RB_SUCCESS)
            {
                if (WsscfgSetAllFsDot11RadioConfigTableTrigger
                    (pWsscfgSetFsDot11RadioConfigEntry,
                     pWsscfgIsSetFsDot11RadioConfigEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11RadioConfigTable: WsscfgSetAllFsDot11RadioConfigTableTrigger function returns failure.\r\n"));
                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11RadioConfigTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID, (UINT1 *)
                     pWsscfgFsDot11RadioConfigEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID, (UINT1 *)
                     pWsscfgOldFsDot11RadioConfigEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgFsDot11RadioConfigEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11RADIOCONFIGTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11RadioConfigEntry);
                return OSIX_FAILURE;
            }
            if (WsscfgUtilUpdateFsDot11RadioConfigTable
                (NULL, pWsscfgFsDot11RadioConfigEntry,
                 pWsscfgIsSetFsDot11RadioConfigEntry) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11RadioConfigTable: WsscfgUtilUpdateFsDot11RadioConfigTable function returns failure.\r\n"));

                if (WsscfgSetAllFsDot11RadioConfigTableTrigger
                    (pWsscfgSetFsDot11RadioConfigEntry,
                     pWsscfgIsSetFsDot11RadioConfigEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11RadioConfigTable: WsscfgSetAllFsDot11RadioConfigTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gWsscfgGlobals.
                           WsscfgGlbMib.FsDot11RadioConfigTable,
                           pWsscfgFsDot11RadioConfigEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID, (UINT1 *)
                     pWsscfgFsDot11RadioConfigEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID, (UINT1 *)
                     pWsscfgOldFsDot11RadioConfigEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgFsDot11RadioConfigEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11RADIOCONFIGTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11RadioConfigEntry);
                return OSIX_FAILURE;
            }

            if ((pWsscfgSetFsDot11RadioConfigEntry->
                 MibObject.i4FsDot11RowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsDot11RadioConfigEntry->
                        MibObject.i4FsDot11RowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsDot11RadioConfigEntry->MibObject.
                    i4IfIndex =
                    pWsscfgSetFsDot11RadioConfigEntry->MibObject.i4IfIndex;
                pWsscfgTrgFsDot11RadioConfigEntry->
                    MibObject.i4FsDot11RowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11RadioConfigEntry->
                    bFsDot11RowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsDot11RadioConfigTableTrigger
                    (pWsscfgTrgFsDot11RadioConfigEntry,
                     pWsscfgTrgIsSetFsDot11RadioConfigEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11RadioConfigTable: WsscfgSetAllFsDot11RadioConfigTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                         (UINT1 *) pWsscfgFsDot11RadioConfigEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                         (UINT1 *) pWsscfgOldFsDot11RadioConfigEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                         (UINT1 *) pWsscfgTrgFsDot11RadioConfigEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11RADIOCONFIGTABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetFsDot11RadioConfigEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pWsscfgSetFsDot11RadioConfigEntry->
                     MibObject.i4FsDot11RowStatus == CREATE_AND_WAIT)
            {
                /* For MSR and RM Trigger */
                pWsscfgTrgFsDot11RadioConfigEntry->
                    MibObject.i4FsDot11RowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11RadioConfigEntry->
                    bFsDot11RowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsDot11RadioConfigTableTrigger
                    (pWsscfgTrgFsDot11RadioConfigEntry,
                     pWsscfgTrgIsSetFsDot11RadioConfigEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11RadioConfigTable: WsscfgSetAllFsDot11RadioConfigTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                         (UINT1 *) pWsscfgFsDot11RadioConfigEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                         (UINT1 *) pWsscfgOldFsDot11RadioConfigEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                         (UINT1 *) pWsscfgTrgFsDot11RadioConfigEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11RADIOCONFIGTABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetFsDot11RadioConfigEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pWsscfgSetFsDot11RadioConfigEntry->
                MibObject.i4FsDot11RowStatus == CREATE_AND_GO)
            {
                pWsscfgSetFsDot11RadioConfigEntry->
                    MibObject.i4FsDot11RowStatus = ACTIVE;
            }

            if (WsscfgSetAllFsDot11RadioConfigTableTrigger
                (pWsscfgSetFsDot11RadioConfigEntry,
                 pWsscfgIsSetFsDot11RadioConfigEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11RadioConfigTable:  WsscfgSetAllFsDot11RadioConfigTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11RadioConfigEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11RadioConfigEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11RADIOCONFIGTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11RadioConfigEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (WsscfgSetAllFsDot11RadioConfigTableTrigger
                (pWsscfgSetFsDot11RadioConfigEntry,
                 pWsscfgIsSetFsDot11RadioConfigEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11RadioConfigTable: WsscfgSetAllFsDot11RadioConfigTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11RadioConfigTable: Failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11RadioConfigEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11RadioConfigEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11RADIOCONFIGTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11RadioConfigEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pWsscfgSetFsDot11RadioConfigEntry->
              MibObject.i4FsDot11RowStatus == CREATE_AND_WAIT)
             || (pWsscfgSetFsDot11RadioConfigEntry->
                 MibObject.i4FsDot11RowStatus == CREATE_AND_GO))
    {
        if (WsscfgSetAllFsDot11RadioConfigTableTrigger
            (pWsscfgSetFsDot11RadioConfigEntry,
             pWsscfgIsSetFsDot11RadioConfigEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11RadioConfigTable: WsscfgSetAllFsDot11RadioConfigTableTrigger function returns failure.\r\n"));
        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11RadioConfigTable: The row is already present.\r\n"));
        MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11RadioConfigEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11RadioConfigEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11RADIOCONFIGTABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetFsDot11RadioConfigEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pWsscfgOldFsDot11RadioConfigEntry,
            pWsscfgFsDot11RadioConfigEntry,
            sizeof (tWsscfgFsDot11RadioConfigEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pWsscfgSetFsDot11RadioConfigEntry->MibObject.
        i4FsDot11RowStatus == DESTROY)
    {
        pWsscfgFsDot11RadioConfigEntry->MibObject.i4FsDot11RowStatus = DESTROY;

        if (WsscfgUtilUpdateFsDot11RadioConfigTable
            (pWsscfgOldFsDot11RadioConfigEntry,
             pWsscfgFsDot11RadioConfigEntry,
             pWsscfgIsSetFsDot11RadioConfigEntry) != OSIX_SUCCESS)
        {

            if (WsscfgSetAllFsDot11RadioConfigTableTrigger
                (pWsscfgSetFsDot11RadioConfigEntry,
                 pWsscfgIsSetFsDot11RadioConfigEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11RadioConfigTable: WsscfgSetAllFsDot11RadioConfigTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11RadioConfigTable: WsscfgUtilUpdateFsDot11RadioConfigTable function returns failure.\r\n"));
        }
        RBTreeRem (gWsscfgGlobals.WsscfgGlbMib.FsDot11RadioConfigTable,
                   pWsscfgFsDot11RadioConfigEntry);
        if (WsscfgSetAllFsDot11RadioConfigTableTrigger
            (pWsscfgSetFsDot11RadioConfigEntry,
             pWsscfgIsSetFsDot11RadioConfigEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11RadioConfigTable: WsscfgSetAllFsDot11RadioConfigTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11RadioConfigEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11RadioConfigEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11RADIOCONFIGTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11RadioConfigEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgFsDot11RadioConfigEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11RadioConfigEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11RadioConfigEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11RADIOCONFIGTABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetFsDot11RadioConfigEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsDot11RadioConfigTableFilterInputs
        (pWsscfgFsDot11RadioConfigEntry,
         pWsscfgSetFsDot11RadioConfigEntry,
         pWsscfgIsSetFsDot11RadioConfigEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11RadioConfigEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11RadioConfigEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11RADIOCONFIGTABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetFsDot11RadioConfigEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pWsscfgFsDot11RadioConfigEntry->MibObject.
         i4FsDot11RowStatus == ACTIVE)
        && (pWsscfgSetFsDot11RadioConfigEntry->
            MibObject.i4FsDot11RowStatus != NOT_IN_SERVICE))
    {
        pWsscfgFsDot11RadioConfigEntry->MibObject.i4FsDot11RowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pWsscfgTrgFsDot11RadioConfigEntry->MibObject.
            i4FsDot11RowStatus = NOT_IN_SERVICE;
        pWsscfgTrgIsSetFsDot11RadioConfigEntry->bFsDot11RowStatus = OSIX_TRUE;

        if (WsscfgUtilUpdateFsDot11RadioConfigTable
            (pWsscfgOldFsDot11RadioConfigEntry,
             pWsscfgFsDot11RadioConfigEntry,
             pWsscfgIsSetFsDot11RadioConfigEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pWsscfgFsDot11RadioConfigEntry,
                    pWsscfgOldFsDot11RadioConfigEntry,
                    sizeof (tWsscfgFsDot11RadioConfigEntry));
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11RadioConfigTable:                 WsscfgUtilUpdateFsDot11RadioConfigTable Function returns failure.\r\n"));

            if (WsscfgSetAllFsDot11RadioConfigTableTrigger
                (pWsscfgSetFsDot11RadioConfigEntry,
                 pWsscfgIsSetFsDot11RadioConfigEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11RadioConfigTable: WsscfgSetAllFsDot11RadioConfigTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11RadioConfigEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11RadioConfigEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11RADIOCONFIGTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11RadioConfigEntry);
            return OSIX_FAILURE;
        }

        if (WsscfgSetAllFsDot11RadioConfigTableTrigger
            (pWsscfgTrgFsDot11RadioConfigEntry,
             pWsscfgTrgIsSetFsDot11RadioConfigEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11RadioConfigTable: WsscfgSetAllFsDot11RadioConfigTableTrigger function returns failure.\r\n"));
        }
    }

    if (pWsscfgSetFsDot11RadioConfigEntry->MibObject.
        i4FsDot11RowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pWsscfgIsSetFsDot11RadioConfigEntry->bFsDot11RadioType != OSIX_FALSE)
    {
        pWsscfgFsDot11RadioConfigEntry->MibObject.u4FsDot11RadioType =
            pWsscfgSetFsDot11RadioConfigEntry->MibObject.u4FsDot11RadioType;
    }
    if (pWsscfgIsSetFsDot11RadioConfigEntry->bFsDot11RowStatus != OSIX_FALSE)
    {
        pWsscfgFsDot11RadioConfigEntry->MibObject.i4FsDot11RowStatus =
            pWsscfgSetFsDot11RadioConfigEntry->MibObject.i4FsDot11RowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pWsscfgFsDot11RadioConfigEntry->MibObject.i4FsDot11RowStatus = ACTIVE;
    }

    if (WsscfgUtilUpdateFsDot11RadioConfigTable
        (pWsscfgOldFsDot11RadioConfigEntry,
         pWsscfgFsDot11RadioConfigEntry,
         pWsscfgIsSetFsDot11RadioConfigEntry) != OSIX_SUCCESS)
    {

        if (WsscfgSetAllFsDot11RadioConfigTableTrigger
            (pWsscfgSetFsDot11RadioConfigEntry,
             pWsscfgIsSetFsDot11RadioConfigEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11RadioConfigTable: WsscfgSetAllFsDot11RadioConfigTableTrigger function returns failure.\r\n"));

        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11RadioConfigTable: WsscfgUtilUpdateFsDot11RadioConfigTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pWsscfgFsDot11RadioConfigEntry,
                pWsscfgOldFsDot11RadioConfigEntry,
                sizeof (tWsscfgFsDot11RadioConfigEntry));
        MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11RadioConfigEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11RadioConfigEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11RADIOCONFIGTABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetFsDot11RadioConfigEntry);
        return OSIX_FAILURE;

    }
    if (WsscfgSetAllFsDot11RadioConfigTableTrigger
        (pWsscfgSetFsDot11RadioConfigEntry,
         pWsscfgIsSetFsDot11RadioConfigEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11RadioConfigTable: WsscfgSetAllFsDot11RadioConfigTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                        (UINT1 *) pWsscfgOldFsDot11RadioConfigEntry);
    MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID,
                        (UINT1 *) pWsscfgTrgFsDot11RadioConfigEntry);
    MemReleaseMemBlock (WSSCFG_FSDOT11RADIOCONFIGTABLE_ISSET_POOLID,
                        (UINT1 *) pWsscfgTrgIsSetFsDot11RadioConfigEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11QosProfileTable
 Input       :  pWsscfgSetFsDot11QosProfileEntry
                pWsscfgIsSetFsDot11QosProfileEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllFsDot11QosProfileTable (tWsscfgFsDot11QosProfileEntry *
                                    pWsscfgSetFsDot11QosProfileEntry,
                                    tWsscfgIsSetFsDot11QosProfileEntry *
                                    pWsscfgIsSetFsDot11QosProfileEntry,
                                    INT4 i4RowStatusLogic,
                                    INT4 i4RowCreateOption)
{
    tWsscfgFsDot11QosProfileEntry *pWsscfgFsDot11QosProfileEntry = NULL;
    tWsscfgFsDot11QosProfileEntry *pWsscfgOldFsDot11QosProfileEntry = NULL;
    tWsscfgFsDot11QosProfileEntry *pWsscfgTrgFsDot11QosProfileEntry = NULL;
    tWsscfgIsSetFsDot11QosProfileEntry *pWsscfgTrgIsSetFsDot11QosProfileEntry =
        NULL;
    INT4                i4RowMakeActive = FALSE;

    pWsscfgOldFsDot11QosProfileEntry =
        (tWsscfgFsDot11QosProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID);
    if (pWsscfgOldFsDot11QosProfileEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWsscfgTrgFsDot11QosProfileEntry =
        (tWsscfgFsDot11QosProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID);
    if (pWsscfgTrgFsDot11QosProfileEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11QosProfileEntry);
        return OSIX_FAILURE;
    }
    pWsscfgTrgIsSetFsDot11QosProfileEntry =
        (tWsscfgIsSetFsDot11QosProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11QOSPROFILETABLE_ISSET_POOLID);
    if (pWsscfgTrgIsSetFsDot11QosProfileEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11QosProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11QosProfileEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pWsscfgOldFsDot11QosProfileEntry, 0,
            sizeof (tWsscfgFsDot11QosProfileEntry));
    MEMSET (pWsscfgTrgFsDot11QosProfileEntry, 0,
            sizeof (tWsscfgFsDot11QosProfileEntry));
    MEMSET (pWsscfgTrgIsSetFsDot11QosProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11QosProfileEntry));

    /* Check whether the node is already present */
    pWsscfgFsDot11QosProfileEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsDot11QosProfileTable,
                   (tRBElem *) pWsscfgSetFsDot11QosProfileEntry);

    if (pWsscfgFsDot11QosProfileEntry == NULL)
    {
        if (gu1WlanQoSPrfCount > MAX_WLAN_QOS_PRF)
            /* Kloc Fix Start */
        {
            MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11QosProfileEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11QosProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11QOSPROFILETABLE_ISSET_POOLID, (UINT1 *)
                 pWsscfgTrgIsSetFsDot11QosProfileEntry);
            /* Kloc Fix Ends */
            return OSIX_FAILURE;
            /* Kloc Fix Start */
        }
        /* Kloc Fix Ends */
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pWsscfgSetFsDot11QosProfileEntry->
             MibObject.i4FsDot11QosRowStatus == CREATE_AND_WAIT)
            || (pWsscfgSetFsDot11QosProfileEntry->
                MibObject.i4FsDot11QosRowStatus == CREATE_AND_GO)
            ||
            ((pWsscfgSetFsDot11QosProfileEntry->
              MibObject.i4FsDot11QosRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pWsscfgFsDot11QosProfileEntry =
                (tWsscfgFsDot11QosProfileEntry *)
                MemAllocMemBlk (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID);
            if (pWsscfgFsDot11QosProfileEntry == NULL)
            {
                if (WsscfgSetAllFsDot11QosProfileTableTrigger
                    (pWsscfgSetFsDot11QosProfileEntry,
                     pWsscfgIsSetFsDot11QosProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11QosProfileTable:WsscfgSetAllFsDot11QosProfileTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11QosProfileTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID, (UINT1 *)
                     pWsscfgOldFsDot11QosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID, (UINT1 *)
                     pWsscfgTrgFsDot11QosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSPROFILETABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11QosProfileEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pWsscfgFsDot11QosProfileEntry, 0,
                    sizeof (tWsscfgFsDot11QosProfileEntry));
            if ((WsscfgInitializeFsDot11QosProfileTable
                 (pWsscfgFsDot11QosProfileEntry)) == OSIX_FAILURE)
            {
                if (WsscfgSetAllFsDot11QosProfileTableTrigger
                    (pWsscfgSetFsDot11QosProfileEntry,
                     pWsscfgIsSetFsDot11QosProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11QosProfileTable:WsscfgSetAllFsDot11QosProfileTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11QosProfileTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID, (UINT1 *)
                     pWsscfgFsDot11QosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID, (UINT1 *)
                     pWsscfgOldFsDot11QosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID, (UINT1 *)
                     pWsscfgTrgFsDot11QosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSPROFILETABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11QosProfileEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosProfileName !=
                OSIX_FALSE)
            {
                MEMCPY (pWsscfgFsDot11QosProfileEntry->
                        MibObject.au1FsDot11QosProfileName,
                        pWsscfgSetFsDot11QosProfileEntry->
                        MibObject.au1FsDot11QosProfileName,
                        pWsscfgSetFsDot11QosProfileEntry->
                        MibObject.i4FsDot11QosProfileNameLen);

                pWsscfgFsDot11QosProfileEntry->
                    MibObject.i4FsDot11QosProfileNameLen =
                    pWsscfgSetFsDot11QosProfileEntry->
                    MibObject.i4FsDot11QosProfileNameLen;
            }

            if (pWsscfgIsSetFsDot11QosProfileEntry->
                bFsDot11QosTraffic != OSIX_FALSE)
            {
                pWsscfgFsDot11QosProfileEntry->
                    MibObject.i4FsDot11QosTraffic =
                    pWsscfgSetFsDot11QosProfileEntry->
                    MibObject.i4FsDot11QosTraffic;
            }

            if (pWsscfgIsSetFsDot11QosProfileEntry->
                bFsDot11QosPassengerTrustMode != OSIX_FALSE)
            {
                pWsscfgFsDot11QosProfileEntry->
                    MibObject.i4FsDot11QosPassengerTrustMode =
                    pWsscfgSetFsDot11QosProfileEntry->
                    MibObject.i4FsDot11QosPassengerTrustMode;
            }

            if (pWsscfgIsSetFsDot11QosProfileEntry->
                bFsDot11QosRateLimit != OSIX_FALSE)
            {
                pWsscfgFsDot11QosProfileEntry->
                    MibObject.i4FsDot11QosRateLimit =
                    pWsscfgSetFsDot11QosProfileEntry->
                    MibObject.i4FsDot11QosRateLimit;
            }
            if (pWsscfgIsSetFsDot11QosProfileEntry->
                bFsDot11UpStreamCIR != OSIX_FALSE)
            {
                pWsscfgFsDot11QosProfileEntry->
                    MibObject.i4FsDot11UpStreamCIR =
                    pWsscfgSetFsDot11QosProfileEntry->
                    MibObject.i4FsDot11UpStreamCIR;
            }
            if (pWsscfgIsSetFsDot11QosProfileEntry->
                bFsDot11UpStreamCBS != OSIX_FALSE)
            {
                pWsscfgFsDot11QosProfileEntry->
                    MibObject.i4FsDot11UpStreamCBS =
                    pWsscfgSetFsDot11QosProfileEntry->
                    MibObject.i4FsDot11UpStreamCBS;

            }
            if (pWsscfgIsSetFsDot11QosProfileEntry->
                bFsDot11UpStreamEIR != OSIX_FALSE)
            {
                pWsscfgFsDot11QosProfileEntry->
                    MibObject.i4FsDot11UpStreamEIR =
                    pWsscfgSetFsDot11QosProfileEntry->
                    MibObject.i4FsDot11UpStreamEIR;
            }
            if (pWsscfgIsSetFsDot11QosProfileEntry->
                bFsDot11UpStreamEBS != OSIX_FALSE)
            {
                pWsscfgFsDot11QosProfileEntry->
                    MibObject.i4FsDot11UpStreamEBS =
                    pWsscfgSetFsDot11QosProfileEntry->
                    MibObject.i4FsDot11UpStreamEBS;
            }
            if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamCIR !=
                OSIX_FALSE)
            {
                pWsscfgFsDot11QosProfileEntry->
                    MibObject.i4FsDot11DownStreamCIR =
                    pWsscfgSetFsDot11QosProfileEntry->
                    MibObject.i4FsDot11DownStreamCIR;
            }
            if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamCBS !=
                OSIX_FALSE)
            {
                pWsscfgFsDot11QosProfileEntry->
                    MibObject.i4FsDot11DownStreamCBS =
                    pWsscfgSetFsDot11QosProfileEntry->
                    MibObject.i4FsDot11DownStreamCBS;
            }
            if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamEIR !=
                OSIX_FALSE)
            {
                pWsscfgFsDot11QosProfileEntry->
                    MibObject.i4FsDot11DownStreamEIR =
                    pWsscfgSetFsDot11QosProfileEntry->
                    MibObject.i4FsDot11DownStreamEIR;
            }
            if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamEBS !=
                OSIX_FALSE)
            {
                pWsscfgFsDot11QosProfileEntry->
                    MibObject.i4FsDot11DownStreamEBS =
                    pWsscfgSetFsDot11QosProfileEntry->
                    MibObject.i4FsDot11DownStreamEBS;
            }
            if (pWsscfgIsSetFsDot11QosProfileEntry->
                bFsDot11QosRowStatus != OSIX_FALSE)
            {
                pWsscfgFsDot11QosProfileEntry->
                    MibObject.i4FsDot11QosRowStatus =
                    pWsscfgSetFsDot11QosProfileEntry->
                    MibObject.i4FsDot11QosRowStatus;
            }

            if ((pWsscfgSetFsDot11QosProfileEntry->
                 MibObject.i4FsDot11QosRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsDot11QosProfileEntry->
                        MibObject.i4FsDot11QosRowStatus == ACTIVE)))
            {
                pWsscfgFsDot11QosProfileEntry->
                    MibObject.i4FsDot11QosRowStatus = ACTIVE;
            }
            else if (pWsscfgSetFsDot11QosProfileEntry->
                     MibObject.i4FsDot11QosRowStatus == CREATE_AND_WAIT)
            {
                pWsscfgFsDot11QosProfileEntry->
                    MibObject.i4FsDot11QosRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gWsscfgGlobals.WsscfgGlbMib.FsDot11QosProfileTable,
                 (tRBElem *) pWsscfgFsDot11QosProfileEntry) != RB_SUCCESS)
            {
                if (WsscfgSetAllFsDot11QosProfileTableTrigger
                    (pWsscfgSetFsDot11QosProfileEntry,
                     pWsscfgIsSetFsDot11QosProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11QosProfileTable: WsscfgSetAllFsDot11QosProfileTableTrigger function returns failure.\r\n"));
                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11QosProfileTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID, (UINT1 *)
                     pWsscfgFsDot11QosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID, (UINT1 *)
                     pWsscfgOldFsDot11QosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID, (UINT1 *)
                     pWsscfgTrgFsDot11QosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSPROFILETABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11QosProfileEntry);
                return OSIX_FAILURE;
            }
            if (WsscfgUtilUpdateFsDot11QosProfileTable
                (NULL, pWsscfgFsDot11QosProfileEntry,
                 pWsscfgIsSetFsDot11QosProfileEntry) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11QosProfileTable: WsscfgUtilUpdateFsDot11QosProfileTable function returns failure.\r\n"));

                if (WsscfgSetAllFsDot11QosProfileTableTrigger
                    (pWsscfgSetFsDot11QosProfileEntry,
                     pWsscfgIsSetFsDot11QosProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11QosProfileTable: WsscfgSetAllFsDot11QosProfileTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gWsscfgGlobals.
                           WsscfgGlbMib.FsDot11QosProfileTable,
                           pWsscfgFsDot11QosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID, (UINT1 *)
                     pWsscfgFsDot11QosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID, (UINT1 *)
                     pWsscfgOldFsDot11QosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID, (UINT1 *)
                     pWsscfgTrgFsDot11QosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSPROFILETABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11QosProfileEntry);
                return OSIX_FAILURE;
            }

            if ((pWsscfgSetFsDot11QosProfileEntry->
                 MibObject.i4FsDot11QosRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsDot11QosProfileEntry->
                        MibObject.i4FsDot11QosRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                MEMCPY (pWsscfgTrgFsDot11QosProfileEntry->
                        MibObject.au1FsDot11QosProfileName,
                        pWsscfgSetFsDot11QosProfileEntry->
                        MibObject.au1FsDot11QosProfileName,
                        pWsscfgSetFsDot11QosProfileEntry->
                        MibObject.i4FsDot11QosProfileNameLen);

                pWsscfgTrgFsDot11QosProfileEntry->
                    MibObject.i4FsDot11QosProfileNameLen =
                    pWsscfgSetFsDot11QosProfileEntry->
                    MibObject.i4FsDot11QosProfileNameLen;
                pWsscfgTrgFsDot11QosProfileEntry->
                    MibObject.i4FsDot11QosRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11QosProfileEntry->bFsDot11QosRowStatus
                    = OSIX_TRUE;

                if (WsscfgSetAllFsDot11QosProfileTableTrigger
                    (pWsscfgTrgFsDot11QosProfileEntry,
                     pWsscfgTrgIsSetFsDot11QosProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11QosProfileTable: WsscfgSetAllFsDot11QosProfileTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgFsDot11QosProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgOldFsDot11QosProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgTrgFsDot11QosProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11QOSPROFILETABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetFsDot11QosProfileEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pWsscfgSetFsDot11QosProfileEntry->
                     MibObject.i4FsDot11QosRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsDot11QosProfileEntry->
                    MibObject.i4FsDot11QosRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11QosProfileEntry->bFsDot11QosRowStatus
                    = OSIX_TRUE;

                if (WsscfgSetAllFsDot11QosProfileTableTrigger
                    (pWsscfgTrgFsDot11QosProfileEntry,
                     pWsscfgTrgIsSetFsDot11QosProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11QosProfileTable: WsscfgSetAllFsDot11QosProfileTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgFsDot11QosProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgOldFsDot11QosProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgTrgFsDot11QosProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11QOSPROFILETABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetFsDot11QosProfileEntry);
                    return OSIX_FAILURE;
                }
            }
            gu1WlanQoSPrfCount++;

            if (pWsscfgSetFsDot11QosProfileEntry->
                MibObject.i4FsDot11QosRowStatus == CREATE_AND_GO)
            {
                pWsscfgSetFsDot11QosProfileEntry->
                    MibObject.i4FsDot11QosRowStatus = ACTIVE;
            }

            if (WsscfgSetAllFsDot11QosProfileTableTrigger
                (pWsscfgSetFsDot11QosProfileEntry,
                 pWsscfgIsSetFsDot11QosProfileEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11QosProfileTable:  WsscfgSetAllFsDot11QosProfileTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11QosProfileEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11QosProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11QOSPROFILETABLE_ISSET_POOLID, (UINT1 *)
                 pWsscfgTrgIsSetFsDot11QosProfileEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (WsscfgSetAllFsDot11QosProfileTableTrigger
                (pWsscfgSetFsDot11QosProfileEntry,
                 pWsscfgIsSetFsDot11QosProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11QosProfileTable: WsscfgSetAllFsDot11QosProfileTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11QosProfileTable: Failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11QosProfileEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11QosProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11QOSPROFILETABLE_ISSET_POOLID, (UINT1 *)
                 pWsscfgTrgIsSetFsDot11QosProfileEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pWsscfgSetFsDot11QosProfileEntry->
              MibObject.i4FsDot11QosRowStatus == CREATE_AND_WAIT)
             || (pWsscfgSetFsDot11QosProfileEntry->
                 MibObject.i4FsDot11QosRowStatus == CREATE_AND_GO))
    {
        if (WsscfgSetAllFsDot11QosProfileTableTrigger
            (pWsscfgSetFsDot11QosProfileEntry,
             pWsscfgIsSetFsDot11QosProfileEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11QosProfileTable: WsscfgSetAllFsDot11QosProfileTableTrigger function returns failure.\r\n"));
        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11QosProfileTable: The row is already present.\r\n"));
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11QosProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11QosProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsDot11QosProfileEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pWsscfgOldFsDot11QosProfileEntry,
            pWsscfgFsDot11QosProfileEntry,
            sizeof (tWsscfgFsDot11QosProfileEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pWsscfgSetFsDot11QosProfileEntry->
        MibObject.i4FsDot11QosRowStatus == DESTROY)
    {
        pWsscfgFsDot11QosProfileEntry->MibObject.
            i4FsDot11QosRowStatus = DESTROY;

        if (WsscfgUtilUpdateFsDot11QosProfileTable
            (pWsscfgOldFsDot11QosProfileEntry,
             pWsscfgFsDot11QosProfileEntry,
             pWsscfgIsSetFsDot11QosProfileEntry) != OSIX_SUCCESS)
        {

            if (WsscfgSetAllFsDot11QosProfileTableTrigger
                (pWsscfgSetFsDot11QosProfileEntry,
                 pWsscfgIsSetFsDot11QosProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11QosProfileTable: WsscfgSetAllFsDot11QosProfileTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11QosProfileTable: WsscfgUtilUpdateFsDot11QosProfileTable function returns failure.\r\n"));
        }
        RBTreeRem (gWsscfgGlobals.WsscfgGlbMib.FsDot11QosProfileTable,
                   pWsscfgFsDot11QosProfileEntry);
        if (WsscfgSetAllFsDot11QosProfileTableTrigger
            (pWsscfgSetFsDot11QosProfileEntry,
             pWsscfgIsSetFsDot11QosProfileEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11QosProfileTable: WsscfgSetAllFsDot11QosProfileTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11QosProfileEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11QosProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11QOSPROFILETABLE_ISSET_POOLID, (UINT1 *)
                 pWsscfgTrgIsSetFsDot11QosProfileEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgFsDot11QosProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11QosProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11QosProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsDot11QosProfileEntry);
        gu1WlanQoSPrfCount--;
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsDot11QosProfileTableFilterInputs
        (pWsscfgFsDot11QosProfileEntry,
         pWsscfgSetFsDot11QosProfileEntry,
         pWsscfgIsSetFsDot11QosProfileEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11QosProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11QosProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsDot11QosProfileEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pWsscfgFsDot11QosProfileEntry->MibObject.
         i4FsDot11QosRowStatus == ACTIVE)
        && (pWsscfgSetFsDot11QosProfileEntry->
            MibObject.i4FsDot11QosRowStatus != NOT_IN_SERVICE))
    {
        pWsscfgFsDot11QosProfileEntry->MibObject.
            i4FsDot11QosRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pWsscfgTrgFsDot11QosProfileEntry->MibObject.
            i4FsDot11QosRowStatus = NOT_IN_SERVICE;
        pWsscfgTrgIsSetFsDot11QosProfileEntry->bFsDot11QosRowStatus = OSIX_TRUE;

        if (WsscfgUtilUpdateFsDot11QosProfileTable
            (pWsscfgOldFsDot11QosProfileEntry,
             pWsscfgFsDot11QosProfileEntry,
             pWsscfgIsSetFsDot11QosProfileEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pWsscfgFsDot11QosProfileEntry,
                    pWsscfgOldFsDot11QosProfileEntry,
                    sizeof (tWsscfgFsDot11QosProfileEntry));
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11QosProfileTable:                 WsscfgUtilUpdateFsDot11QosProfileTable Function returns failure.\r\n"));

            if (WsscfgSetAllFsDot11QosProfileTableTrigger
                (pWsscfgSetFsDot11QosProfileEntry,
                 pWsscfgIsSetFsDot11QosProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11QosProfileTable: WsscfgSetAllFsDot11QosProfileTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11QosProfileEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11QosProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11QOSPROFILETABLE_ISSET_POOLID, (UINT1 *)
                 pWsscfgTrgIsSetFsDot11QosProfileEntry);
            return OSIX_FAILURE;
        }

        if (WsscfgSetAllFsDot11QosProfileTableTrigger
            (pWsscfgTrgFsDot11QosProfileEntry,
             pWsscfgTrgIsSetFsDot11QosProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11QosProfileTable: WsscfgSetAllFsDot11QosProfileTableTrigger function returns failure.\r\n"));
        }
    }

    if (pWsscfgSetFsDot11QosProfileEntry->
        MibObject.i4FsDot11QosRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosTraffic != OSIX_FALSE)
    {
        pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11QosTraffic =
            pWsscfgSetFsDot11QosProfileEntry->MibObject.i4FsDot11QosTraffic;
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosPassengerTrustMode !=
        OSIX_FALSE)
    {
        pWsscfgFsDot11QosProfileEntry->
            MibObject.i4FsDot11QosPassengerTrustMode =
            pWsscfgSetFsDot11QosProfileEntry->
            MibObject.i4FsDot11QosPassengerTrustMode;
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosRateLimit != OSIX_FALSE)
    {
        pWsscfgFsDot11QosProfileEntry->MibObject.
            i4FsDot11QosRateLimit =
            pWsscfgSetFsDot11QosProfileEntry->MibObject.i4FsDot11QosRateLimit;
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamCIR != OSIX_FALSE)
    {
        pWsscfgFsDot11QosProfileEntry->MibObject.
            i4FsDot11UpStreamCIR =
            pWsscfgSetFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamCIR;
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamCBS != OSIX_FALSE)
    {
        pWsscfgFsDot11QosProfileEntry->MibObject.
            i4FsDot11UpStreamCBS =
            pWsscfgSetFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamCBS;

    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamEIR != OSIX_FALSE)
    {
        pWsscfgFsDot11QosProfileEntry->MibObject.
            i4FsDot11UpStreamEIR =
            pWsscfgSetFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamEIR;
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamEBS != OSIX_FALSE)
    {
        pWsscfgFsDot11QosProfileEntry->MibObject.
            i4FsDot11UpStreamEBS =
            pWsscfgSetFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamEBS;
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamCIR != OSIX_FALSE)
    {
        pWsscfgFsDot11QosProfileEntry->MibObject.
            i4FsDot11DownStreamCIR =
            pWsscfgSetFsDot11QosProfileEntry->MibObject.i4FsDot11DownStreamCIR;
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamCBS != OSIX_FALSE)
    {
        pWsscfgFsDot11QosProfileEntry->MibObject.
            i4FsDot11DownStreamCBS =
            pWsscfgSetFsDot11QosProfileEntry->MibObject.i4FsDot11DownStreamCBS;
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamEIR != OSIX_FALSE)
    {
        pWsscfgFsDot11QosProfileEntry->MibObject.
            i4FsDot11DownStreamEIR =
            pWsscfgSetFsDot11QosProfileEntry->MibObject.i4FsDot11DownStreamEIR;
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamEBS != OSIX_FALSE)
    {
        pWsscfgFsDot11QosProfileEntry->MibObject.
            i4FsDot11DownStreamEBS =
            pWsscfgSetFsDot11QosProfileEntry->MibObject.i4FsDot11DownStreamEBS;
    }
    if (pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosRowStatus != OSIX_FALSE)
    {
        pWsscfgFsDot11QosProfileEntry->MibObject.
            i4FsDot11QosRowStatus =
            pWsscfgSetFsDot11QosProfileEntry->MibObject.i4FsDot11QosRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11QosRowStatus = ACTIVE;
    }

    if (WsscfgUtilUpdateFsDot11QosProfileTable
        (pWsscfgOldFsDot11QosProfileEntry,
         pWsscfgFsDot11QosProfileEntry,
         pWsscfgIsSetFsDot11QosProfileEntry) != OSIX_SUCCESS)
    {

        if (WsscfgSetAllFsDot11QosProfileTableTrigger
            (pWsscfgSetFsDot11QosProfileEntry,
             pWsscfgIsSetFsDot11QosProfileEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11QosProfileTable: WsscfgSetAllFsDot11QosProfileTableTrigger function returns failure.\r\n"));

        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11QosProfileTable: WsscfgUtilUpdateFsDot11QosProfileTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pWsscfgFsDot11QosProfileEntry,
                pWsscfgOldFsDot11QosProfileEntry,
                sizeof (tWsscfgFsDot11QosProfileEntry));
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11QosProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11QosProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsDot11QosProfileEntry);
        return OSIX_FAILURE;

    }
    if (WsscfgSetAllFsDot11QosProfileTableTrigger
        (pWsscfgSetFsDot11QosProfileEntry,
         pWsscfgIsSetFsDot11QosProfileEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11QosProfileTable: WsscfgSetAllFsDot11QosProfileTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                        (UINT1 *) pWsscfgOldFsDot11QosProfileEntry);
    MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_POOLID,
                        (UINT1 *) pWsscfgTrgFsDot11QosProfileEntry);
    MemReleaseMemBlock (WSSCFG_FSDOT11QOSPROFILETABLE_ISSET_POOLID,
                        (UINT1 *) pWsscfgTrgIsSetFsDot11QosProfileEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11WlanCapabilityProfileTable
 Input       :  pWsscfgSetFsDot11WlanCapabilityProfileEntry
                pWsscfgIsSetFsDot11WlanCapabilityProfileEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgSetAllFsDot11WlanCapabilityProfileTable
    (tWsscfgFsDot11WlanCapabilityProfileEntry *
     pWsscfgSetFsDot11WlanCapabilityProfileEntry,
     tWsscfgIsSetFsDot11WlanCapabilityProfileEntry *
     pWsscfgIsSetFsDot11WlanCapabilityProfileEntry, INT4 i4RowStatusLogic,
     INT4 i4RowCreateOption)
{
    tWsscfgFsDot11WlanCapabilityProfileEntry
        * pWsscfgFsDot11WlanCapabilityProfileEntry = NULL;
    tWsscfgFsDot11WlanCapabilityProfileEntry
        * pWsscfgOldFsDot11WlanCapabilityProfileEntry = NULL;
    tWsscfgFsDot11WlanCapabilityProfileEntry
        * pWsscfgTrgFsDot11WlanCapabilityProfileEntry = NULL;
    tWsscfgIsSetFsDot11WlanCapabilityProfileEntry
        * pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pWsscfgOldFsDot11WlanCapabilityProfileEntry =
        (tWsscfgFsDot11WlanCapabilityProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID);
    if (pWsscfgOldFsDot11WlanCapabilityProfileEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWsscfgTrgFsDot11WlanCapabilityProfileEntry =
        (tWsscfgFsDot11WlanCapabilityProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID);
    if (pWsscfgTrgFsDot11WlanCapabilityProfileEntry == NULL)
    {
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgOldFsDot11WlanCapabilityProfileEntry);
        return OSIX_FAILURE;
    }
    pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry =
        (tWsscfgIsSetFsDot11WlanCapabilityProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_ISSET_POOLID);
    if (pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry == NULL)
    {
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgOldFsDot11WlanCapabilityProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgTrgFsDot11WlanCapabilityProfileEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pWsscfgOldFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));
    MEMSET (pWsscfgTrgFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));
    MEMSET (pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11WlanCapabilityProfileEntry));

    /* Check whether the node is already present */
    pWsscfgFsDot11WlanCapabilityProfileEntry =
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.FsDot11WlanCapabilityProfileTable,
                   (tRBElem *) pWsscfgSetFsDot11WlanCapabilityProfileEntry);

    if (pWsscfgFsDot11WlanCapabilityProfileEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
             MibObject.i4FsDot11WlanCapabilityRowStatus ==
             CREATE_AND_WAIT)
            || (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                MibObject.i4FsDot11WlanCapabilityRowStatus ==
                CREATE_AND_GO)
            ||
            ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
              MibObject.i4FsDot11WlanCapabilityRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pWsscfgFsDot11WlanCapabilityProfileEntry =
                (tWsscfgFsDot11WlanCapabilityProfileEntry *)
                MemAllocMemBlk
                (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID);
            if (pWsscfgFsDot11WlanCapabilityProfileEntry == NULL)
            {
                if (WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger
                    (pWsscfgSetFsDot11WlanCapabilityProfileEntry,
                     pWsscfgIsSetFsDot11WlanCapabilityProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanCapabilityProfileTable:WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanCapabilityProfileTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11WlanCapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11WlanCapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_ISSET_POOLID,
                     (UINT1 *)
                     pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pWsscfgFsDot11WlanCapabilityProfileEntry, 0,
                    sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));
            if ((WsscfgInitializeFsDot11WlanCapabilityProfileTable
                 (pWsscfgFsDot11WlanCapabilityProfileEntry)) == OSIX_FAILURE)
            {
                if (WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger
                    (pWsscfgSetFsDot11WlanCapabilityProfileEntry,
                     pWsscfgIsSetFsDot11WlanCapabilityProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanCapabilityProfileTable:WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanCapabilityProfileTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgFsDot11WlanCapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11WlanCapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11WlanCapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_ISSET_POOLID,
                     (UINT1 *)
                     pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanCFPollable != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanCFPollable =
                    pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanCFPollable;
            }

            if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanCFPollRequest != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanCFPollRequest =
                    pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanCFPollRequest;
            }

            if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanPrivacyOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanPrivacyOptionImplemented =
                    pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanPrivacyOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanShortPreambleOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanShortPreambleOptionImplemented
                    =
                    pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanShortPreambleOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanPBCCOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanPBCCOptionImplemented =
                    pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanPBCCOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanChannelAgilityPresent != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanChannelAgilityPresent =
                    pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanChannelAgilityPresent;
            }

            if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanQosOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanQosOptionImplemented =
                    pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanQosOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanSpectrumManagementRequired != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanSpectrumManagementRequired
                    =
                    pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanSpectrumManagementRequired;
            }

            if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanShortSlotTimeOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanShortSlotTimeOptionImplemented
                    =
                    pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanShortSlotTimeOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanAPSDOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanAPSDOptionImplemented =
                    pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanAPSDOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanDSSSOFDMOptionEnabled != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanDSSSOFDMOptionEnabled =
                    pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanDSSSOFDMOptionEnabled;
            }

            if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanDelayedBlockAckOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanDelayedBlockAckOptionImplemented
                    =
                    pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanDelayedBlockAckOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanImmediateBlockAckOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanImmediateBlockAckOptionImplemented
                    =
                    pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanImmediateBlockAckOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanQAckOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanQAckOptionImplemented =
                    pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanQAckOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanQueueRequestOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanQueueRequestOptionImplemented
                    =
                    pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanQueueRequestOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanTXOPRequestOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanTXOPRequestOptionImplemented
                    =
                    pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanTXOPRequestOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanRSNAOptionImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanRSNAOptionImplemented =
                    pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanRSNAOptionImplemented;
            }

            if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanRSNAPreauthenticationImplemented != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanRSNAPreauthenticationImplemented
                    =
                    pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanRSNAPreauthenticationImplemented;
            }

            if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bFsDot11WlanCapabilityRowStatus != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanCapabilityRowStatus =
                    pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanCapabilityRowStatus;
            }

            if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
                bIfIndex != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4IfIndex =
                    pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4IfIndex;
            }

            if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                 MibObject.i4FsDot11WlanCapabilityRowStatus ==
                 CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    &&
                    (pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                     i4FsDot11WlanCapabilityRowStatus == ACTIVE)))
            {
                pWsscfgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanCapabilityRowStatus = ACTIVE;
            }
            else if
                (pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                 i4FsDot11WlanCapabilityRowStatus == CREATE_AND_WAIT)
            {
                pWsscfgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanCapabilityRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gWsscfgGlobals.
                 WsscfgGlbMib.FsDot11WlanCapabilityProfileTable,
                 (tRBElem *) pWsscfgFsDot11WlanCapabilityProfileEntry)
                != RB_SUCCESS)
            {
                if (WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger
                    (pWsscfgSetFsDot11WlanCapabilityProfileEntry,
                     pWsscfgIsSetFsDot11WlanCapabilityProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanCapabilityProfileTable: WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger function returns failure.\r\n"));
                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanCapabilityProfileTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgFsDot11WlanCapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11WlanCapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11WlanCapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_ISSET_POOLID,
                     (UINT1 *)
                     pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry);
                return OSIX_FAILURE;
            }
            if (WsscfgUtilUpdateFsDot11WlanCapabilityProfileTable
                (NULL, pWsscfgFsDot11WlanCapabilityProfileEntry,
                 pWsscfgIsSetFsDot11WlanCapabilityProfileEntry) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanCapabilityProfileTable: WsscfgUtilUpdateFsDot11WlanCapabilityProfileTable function returns failure.\r\n"));

                if (WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger
                    (pWsscfgSetFsDot11WlanCapabilityProfileEntry,
                     pWsscfgIsSetFsDot11WlanCapabilityProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanCapabilityProfileTable: WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gWsscfgGlobals.
                           WsscfgGlbMib.FsDot11WlanCapabilityProfileTable,
                           pWsscfgFsDot11WlanCapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgFsDot11WlanCapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11WlanCapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11WlanCapabilityProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_ISSET_POOLID,
                     (UINT1 *)
                     pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry);
                return OSIX_FAILURE;
            }

            if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                 MibObject.i4FsDot11WlanCapabilityRowStatus ==
                 CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    &&
                    (pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                     i4FsDot11WlanCapabilityRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4IfIndex =
                    pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4IfIndex;
                pWsscfgTrgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanCapabilityRowStatus =
                    CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry->
                    bFsDot11WlanCapabilityRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger
                    (pWsscfgTrgFsDot11WlanCapabilityProfileEntry,
                     pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanCapabilityProfileTable: WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgFsDot11WlanCapabilityProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgOldFsDot11WlanCapabilityProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgTrgFsDot11WlanCapabilityProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_ISSET_POOLID,
                         (UINT1 *)
                         pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry);
                    return OSIX_FAILURE;
                }
            }
            else if
                (pWsscfgSetFsDot11WlanCapabilityProfileEntry->MibObject.
                 i4FsDot11WlanCapabilityRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanCapabilityRowStatus =
                    CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry->
                    bFsDot11WlanCapabilityRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger
                    (pWsscfgTrgFsDot11WlanCapabilityProfileEntry,
                     pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanCapabilityProfileTable: WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgFsDot11WlanCapabilityProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgOldFsDot11WlanCapabilityProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgTrgFsDot11WlanCapabilityProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_ISSET_POOLID,
                         (UINT1 *)
                         pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                MibObject.i4FsDot11WlanCapabilityRowStatus == CREATE_AND_GO)
            {
                pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                    MibObject.i4FsDot11WlanCapabilityRowStatus = ACTIVE;
            }

            if (WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger
                (pWsscfgSetFsDot11WlanCapabilityProfileEntry,
                 pWsscfgIsSetFsDot11WlanCapabilityProfileEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanCapabilityProfileTable:  WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgOldFsDot11WlanCapabilityProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgTrgFsDot11WlanCapabilityProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger
                (pWsscfgSetFsDot11WlanCapabilityProfileEntry,
                 pWsscfgIsSetFsDot11WlanCapabilityProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanCapabilityProfileTable: WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanCapabilityProfileTable: Failure.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgOldFsDot11WlanCapabilityProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgTrgFsDot11WlanCapabilityProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pWsscfgSetFsDot11WlanCapabilityProfileEntry->
              MibObject.i4FsDot11WlanCapabilityRowStatus ==
              CREATE_AND_WAIT)
             || (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
                 MibObject.i4FsDot11WlanCapabilityRowStatus == CREATE_AND_GO))
    {
        if (WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger
            (pWsscfgSetFsDot11WlanCapabilityProfileEntry,
             pWsscfgIsSetFsDot11WlanCapabilityProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanCapabilityProfileTable: WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger function returns failure.\r\n"));
        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11WlanCapabilityProfileTable: The row is already present.\r\n"));
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgOldFsDot11WlanCapabilityProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgTrgFsDot11WlanCapabilityProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_ISSET_POOLID,
             (UINT1 *) pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pWsscfgOldFsDot11WlanCapabilityProfileEntry,
            pWsscfgFsDot11WlanCapabilityProfileEntry,
            sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanCapabilityRowStatus == DESTROY)
    {
        pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanCapabilityRowStatus = DESTROY;

        if (WsscfgUtilUpdateFsDot11WlanCapabilityProfileTable
            (pWsscfgOldFsDot11WlanCapabilityProfileEntry,
             pWsscfgFsDot11WlanCapabilityProfileEntry,
             pWsscfgIsSetFsDot11WlanCapabilityProfileEntry) != OSIX_SUCCESS)
        {

            if (WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger
                (pWsscfgSetFsDot11WlanCapabilityProfileEntry,
                 pWsscfgIsSetFsDot11WlanCapabilityProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanCapabilityProfileTable: WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanCapabilityProfileTable: WsscfgUtilUpdateFsDot11WlanCapabilityProfileTable function returns failure.\r\n"));
        }
        RBTreeRem (gWsscfgGlobals.
                   WsscfgGlbMib.FsDot11WlanCapabilityProfileTable,
                   pWsscfgFsDot11WlanCapabilityProfileEntry);
        if (WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger
            (pWsscfgSetFsDot11WlanCapabilityProfileEntry,
             pWsscfgIsSetFsDot11WlanCapabilityProfileEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanCapabilityProfileTable: WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgOldFsDot11WlanCapabilityProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgTrgFsDot11WlanCapabilityProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgFsDot11WlanCapabilityProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgOldFsDot11WlanCapabilityProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgTrgFsDot11WlanCapabilityProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_ISSET_POOLID,
             (UINT1 *) pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsDot11WlanCapabilityProfileTableFilterInputs
        (pWsscfgFsDot11WlanCapabilityProfileEntry,
         pWsscfgSetFsDot11WlanCapabilityProfileEntry,
         pWsscfgIsSetFsDot11WlanCapabilityProfileEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgOldFsDot11WlanCapabilityProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgTrgFsDot11WlanCapabilityProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_ISSET_POOLID,
             (UINT1 *) pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pWsscfgFsDot11WlanCapabilityProfileEntry->
         MibObject.i4FsDot11WlanCapabilityRowStatus == ACTIVE)
        && (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanCapabilityRowStatus != NOT_IN_SERVICE))
    {
        pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanCapabilityRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pWsscfgTrgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanCapabilityRowStatus = NOT_IN_SERVICE;
        pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanCapabilityRowStatus = OSIX_TRUE;

        if (WsscfgUtilUpdateFsDot11WlanCapabilityProfileTable
            (pWsscfgOldFsDot11WlanCapabilityProfileEntry,
             pWsscfgFsDot11WlanCapabilityProfileEntry,
             pWsscfgIsSetFsDot11WlanCapabilityProfileEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pWsscfgFsDot11WlanCapabilityProfileEntry,
                    pWsscfgOldFsDot11WlanCapabilityProfileEntry,
                    sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanCapabilityProfileTable:                 WsscfgUtilUpdateFsDot11WlanCapabilityProfileTable Function returns failure.\r\n"));

            if (WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger
                (pWsscfgSetFsDot11WlanCapabilityProfileEntry,
                 pWsscfgIsSetFsDot11WlanCapabilityProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanCapabilityProfileTable: WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgOldFsDot11WlanCapabilityProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgTrgFsDot11WlanCapabilityProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry);
            return OSIX_FAILURE;
        }

        if (WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger
            (pWsscfgTrgFsDot11WlanCapabilityProfileEntry,
             pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanCapabilityProfileTable: WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger function returns failure.\r\n"));
        }
    }

    if (pWsscfgSetFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanCapabilityRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanCFPollable !=
        OSIX_FALSE)
    {
        pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanCFPollable =
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanCFPollable;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanCFPollRequest != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanCFPollRequest =
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanCFPollRequest;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanPrivacyOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanPrivacyOptionImplemented =
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanPrivacyOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanShortPreambleOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanShortPreambleOptionImplemented =
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanShortPreambleOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanPBCCOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanPBCCOptionImplemented =
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanPBCCOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanChannelAgilityPresent != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanChannelAgilityPresent =
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanChannelAgilityPresent;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanQosOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanQosOptionImplemented =
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanQosOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanSpectrumManagementRequired != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanSpectrumManagementRequired =
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanSpectrumManagementRequired;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanShortSlotTimeOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanShortSlotTimeOptionImplemented =
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanShortSlotTimeOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanAPSDOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanAPSDOptionImplemented =
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanAPSDOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanDSSSOFDMOptionEnabled != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanDSSSOFDMOptionEnabled =
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanDSSSOFDMOptionEnabled;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanDelayedBlockAckOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanDelayedBlockAckOptionImplemented =
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanDelayedBlockAckOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanImmediateBlockAckOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanImmediateBlockAckOptionImplemented
            =
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanImmediateBlockAckOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanQAckOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanQAckOptionImplemented =
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanQAckOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanQueueRequestOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanQueueRequestOptionImplemented =
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanQueueRequestOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanTXOPRequestOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanTXOPRequestOptionImplemented =
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanTXOPRequestOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanRSNAOptionImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanRSNAOptionImplemented =
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanRSNAOptionImplemented;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanRSNAPreauthenticationImplemented != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanRSNAPreauthenticationImplemented =
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanRSNAPreauthenticationImplemented;
    }
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanCapabilityRowStatus != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanCapabilityRowStatus =
            pWsscfgSetFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanCapabilityRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanCapabilityRowStatus = ACTIVE;
    }

    if (WsscfgUtilUpdateFsDot11WlanCapabilityProfileTable
        (pWsscfgOldFsDot11WlanCapabilityProfileEntry,
         pWsscfgFsDot11WlanCapabilityProfileEntry,
         pWsscfgIsSetFsDot11WlanCapabilityProfileEntry) != OSIX_SUCCESS)
    {

        if (WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger
            (pWsscfgSetFsDot11WlanCapabilityProfileEntry,
             pWsscfgIsSetFsDot11WlanCapabilityProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanCapabilityProfileTable: WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger function returns failure.\r\n"));

        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11WlanCapabilityProfileTable: WsscfgUtilUpdateFsDot11WlanCapabilityProfileTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pWsscfgFsDot11WlanCapabilityProfileEntry,
                pWsscfgOldFsDot11WlanCapabilityProfileEntry,
                sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgOldFsDot11WlanCapabilityProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgTrgFsDot11WlanCapabilityProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_ISSET_POOLID,
             (UINT1 *) pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry);
        return OSIX_FAILURE;

    }
    if (WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger
        (pWsscfgSetFsDot11WlanCapabilityProfileEntry,
         pWsscfgIsSetFsDot11WlanCapabilityProfileEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11WlanCapabilityProfileTable: WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock
        (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID, (UINT1 *)
         pWsscfgOldFsDot11WlanCapabilityProfileEntry);
    MemReleaseMemBlock
        (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID, (UINT1 *)
         pWsscfgTrgFsDot11WlanCapabilityProfileEntry);
    MemReleaseMemBlock
        (WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_ISSET_POOLID,
         (UINT1 *) pWsscfgTrgIsSetFsDot11WlanCapabilityProfileEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11WlanAuthenticationProfileTable
 Input       :  pWsscfgSetFsDot11WlanAuthenticationProfileEntry
                pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgSetAllFsDot11WlanAuthenticationProfileTable
    (tWsscfgFsDot11WlanAuthenticationProfileEntry *
     pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
     tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry *
     pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry, INT4 i4RowStatusLogic,
     INT4 i4RowCreateOption)
{
    tWsscfgFsDot11WlanAuthenticationProfileEntry
        * pWsscfgFsDot11WlanAuthenticationProfileEntry = NULL;
    tWsscfgFsDot11WlanAuthenticationProfileEntry
        * pWsscfgOldFsDot11WlanAuthenticationProfileEntry = NULL;
    tWsscfgFsDot11WlanAuthenticationProfileEntry
        * pWsscfgTrgFsDot11WlanAuthenticationProfileEntry = NULL;
    tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry
        * pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pWsscfgOldFsDot11WlanAuthenticationProfileEntry =
        (tWsscfgFsDot11WlanAuthenticationProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID);
    if (pWsscfgOldFsDot11WlanAuthenticationProfileEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWsscfgTrgFsDot11WlanAuthenticationProfileEntry =
        (tWsscfgFsDot11WlanAuthenticationProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID);
    if (pWsscfgTrgFsDot11WlanAuthenticationProfileEntry == NULL)
    {
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgOldFsDot11WlanAuthenticationProfileEntry);
        return OSIX_FAILURE;
    }
    pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry =
        (tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry *)
        MemAllocMemBlk
        (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_ISSET_POOLID);
    if (pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry == NULL)
    {
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgOldFsDot11WlanAuthenticationProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgTrgFsDot11WlanAuthenticationProfileEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pWsscfgOldFsDot11WlanAuthenticationProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanAuthenticationProfileEntry));
    MEMSET (pWsscfgTrgFsDot11WlanAuthenticationProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanAuthenticationProfileEntry));
    MEMSET (pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry));

    /* Check whether the node is already present */
    pWsscfgFsDot11WlanAuthenticationProfileEntry =
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.FsDot11WlanAuthenticationProfileTable,
                   (tRBElem *) pWsscfgSetFsDot11WlanAuthenticationProfileEntry);

    if (pWsscfgFsDot11WlanAuthenticationProfileEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
             MibObject.i4FsDot11WlanAuthenticationRowStatus ==
             CREATE_AND_WAIT)
            || (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                MibObject.i4FsDot11WlanAuthenticationRowStatus ==
                CREATE_AND_GO)
            ||
            ((pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
              MibObject.i4FsDot11WlanAuthenticationRowStatus ==
              ACTIVE) && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pWsscfgFsDot11WlanAuthenticationProfileEntry =
                (tWsscfgFsDot11WlanAuthenticationProfileEntry *)
                MemAllocMemBlk
                (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID);
            if (pWsscfgFsDot11WlanAuthenticationProfileEntry == NULL)
            {
                if (WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger
                    (pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
                     pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanAuthenticationProfileTable:WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanAuthenticationProfileTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11WlanAuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11WlanAuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_ISSET_POOLID,
                     (UINT1 *)
                     pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pWsscfgFsDot11WlanAuthenticationProfileEntry, 0,
                    sizeof (tWsscfgFsDot11WlanAuthenticationProfileEntry));
            if ((WsscfgInitializeFsDot11WlanAuthenticationProfileTable
                 (pWsscfgFsDot11WlanAuthenticationProfileEntry)) ==
                OSIX_FAILURE)
            {
                if (WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger
                    (pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
                     pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanAuthenticationProfileTable:WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanAuthenticationProfileTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgFsDot11WlanAuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11WlanAuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11WlanAuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_ISSET_POOLID,
                     (UINT1 *)
                     pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
                bFsDot11WlanAuthenticationAlgorithm != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanAuthenticationProfileEntry->
                    MibObject.i4FsDot11WlanAuthenticationAlgorithm =
                    pWsscfgSetFsDot11WlanAuthenticationProfileEntry->MibObject.
                    i4FsDot11WlanAuthenticationAlgorithm;
            }

            if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
                bFsDot11WlanWepKeyIndex != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanAuthenticationProfileEntry->
                    MibObject.i4FsDot11WlanWepKeyIndex =
                    pWsscfgSetFsDot11WlanAuthenticationProfileEntry->MibObject.
                    i4FsDot11WlanWepKeyIndex;
            }

            if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
                bFsDot11WlanWepKeyType != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanAuthenticationProfileEntry->
                    MibObject.i4FsDot11WlanWepKeyType =
                    pWsscfgSetFsDot11WlanAuthenticationProfileEntry->MibObject.
                    i4FsDot11WlanWepKeyType;
            }

            if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
                bFsDot11WlanWepKeyLength != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanAuthenticationProfileEntry->
                    MibObject.i4FsDot11WlanWepKeyLength =
                    pWsscfgSetFsDot11WlanAuthenticationProfileEntry->MibObject.
                    i4FsDot11WlanWepKeyLength;
            }

            if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
                bFsDot11WlanWepKey != OSIX_FALSE)
            {
                if (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->MibObject.
                    i4FsDot11WlanWepKeyLen != 0)
                {
                    MEMCPY
                        (pWsscfgFsDot11WlanAuthenticationProfileEntry->
                         MibObject.au1FsDot11WlanWepKey,
                         pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                         MibObject.au1FsDot11WlanWepKey,
                         pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                         MibObject.i4FsDot11WlanWepKeyLen);
                }
                pWsscfgFsDot11WlanAuthenticationProfileEntry->
                    MibObject.i4FsDot11WlanWepKeyLen =
                    pWsscfgSetFsDot11WlanAuthenticationProfileEntry->MibObject.
                    i4FsDot11WlanWepKeyLen;
            }

            if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
                bFsDot11WlanWebAuthentication != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanAuthenticationProfileEntry->
                    MibObject.i4FsDot11WlanWebAuthentication =
                    pWsscfgSetFsDot11WlanAuthenticationProfileEntry->MibObject.
                    i4FsDot11WlanWebAuthentication;
            }

            if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
                bFsDot11WlanAuthenticationRowStatus != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanAuthenticationProfileEntry->
                    MibObject.i4FsDot11WlanAuthenticationRowStatus =
                    pWsscfgSetFsDot11WlanAuthenticationProfileEntry->MibObject.
                    i4FsDot11WlanAuthenticationRowStatus;
            }

            if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bIfIndex !=
                OSIX_FALSE)
            {
                pWsscfgFsDot11WlanAuthenticationProfileEntry->
                    MibObject.i4IfIndex =
                    pWsscfgSetFsDot11WlanAuthenticationProfileEntry->MibObject.
                    i4IfIndex;
            }

            if ((pWsscfgSetFsDot11WlanAuthenticationProfileEntry->MibObject.
                 i4FsDot11WlanAuthenticationRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                        MibObject.i4FsDot11WlanAuthenticationRowStatus ==
                        ACTIVE)))
            {
                pWsscfgFsDot11WlanAuthenticationProfileEntry->
                    MibObject.i4FsDot11WlanAuthenticationRowStatus = ACTIVE;
            }
            else if
                (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->MibObject.
                 i4FsDot11WlanAuthenticationRowStatus == CREATE_AND_WAIT)
            {
                pWsscfgFsDot11WlanAuthenticationProfileEntry->
                    MibObject.i4FsDot11WlanAuthenticationRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gWsscfgGlobals.
                 WsscfgGlbMib.FsDot11WlanAuthenticationProfileTable,
                 (tRBElem *)
                 pWsscfgFsDot11WlanAuthenticationProfileEntry) != RB_SUCCESS)
            {
                if (WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger
                    (pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
                     pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanAuthenticationProfileTable: WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger function returns failure.\r\n"));
                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanAuthenticationProfileTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgFsDot11WlanAuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11WlanAuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11WlanAuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_ISSET_POOLID,
                     (UINT1 *)
                     pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry);
                return OSIX_FAILURE;
            }
            if (WsscfgUtilUpdateFsDot11WlanAuthenticationProfileTable
                (NULL, pWsscfgFsDot11WlanAuthenticationProfileEntry,
                 pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry) !=
                OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanAuthenticationProfileTable: WsscfgUtilUpdateFsDot11WlanAuthenticationProfileTable function returns failure.\r\n"));

                if (WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger
                    (pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
                     pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanAuthenticationProfileTable: WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gWsscfgGlobals.
                           WsscfgGlbMib.FsDot11WlanAuthenticationProfileTable,
                           pWsscfgFsDot11WlanAuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgFsDot11WlanAuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11WlanAuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11WlanAuthenticationProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_ISSET_POOLID,
                     (UINT1 *)
                     pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry);
                return OSIX_FAILURE;
            }

            if ((pWsscfgSetFsDot11WlanAuthenticationProfileEntry->MibObject.
                 i4FsDot11WlanAuthenticationRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                        MibObject.i4FsDot11WlanAuthenticationRowStatus ==
                        ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsDot11WlanAuthenticationProfileEntry->
                    MibObject.i4IfIndex =
                    pWsscfgSetFsDot11WlanAuthenticationProfileEntry->MibObject.
                    i4IfIndex;
                pWsscfgTrgFsDot11WlanAuthenticationProfileEntry->
                    MibObject.i4FsDot11WlanAuthenticationRowStatus =
                    CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry->
                    bFsDot11WlanAuthenticationRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger
                    (pWsscfgTrgFsDot11WlanAuthenticationProfileEntry,
                     pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanAuthenticationProfileTable: WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                         (UINT1 *)
                         pWsscfgFsDot11WlanAuthenticationProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                         (UINT1 *)
                         pWsscfgOldFsDot11WlanAuthenticationProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                         (UINT1 *)
                         pWsscfgTrgFsDot11WlanAuthenticationProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_ISSET_POOLID,
                         (UINT1 *)
                         pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry);
                    return OSIX_FAILURE;
                }
            }
            else if
                (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->MibObject.
                 i4FsDot11WlanAuthenticationRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsDot11WlanAuthenticationProfileEntry->
                    MibObject.i4FsDot11WlanAuthenticationRowStatus =
                    CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry->
                    bFsDot11WlanAuthenticationRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger
                    (pWsscfgTrgFsDot11WlanAuthenticationProfileEntry,
                     pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanAuthenticationProfileTable: WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                         (UINT1 *)
                         pWsscfgFsDot11WlanAuthenticationProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                         (UINT1 *)
                         pWsscfgOldFsDot11WlanAuthenticationProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                         (UINT1 *)
                         pWsscfgTrgFsDot11WlanAuthenticationProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_ISSET_POOLID,
                         (UINT1 *)
                         pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                MibObject.i4FsDot11WlanAuthenticationRowStatus == CREATE_AND_GO)
            {
                pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                    MibObject.i4FsDot11WlanAuthenticationRowStatus = ACTIVE;
            }

            if (WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger
                (pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
                 pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanAuthenticationProfileTable:  WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgOldFsDot11WlanAuthenticationProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgTrgFsDot11WlanAuthenticationProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_ISSET_POOLID,
                 (UINT1 *)
                 pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger
                (pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
                 pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanAuthenticationProfileTable: WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanAuthenticationProfileTable: Failure.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgOldFsDot11WlanAuthenticationProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgTrgFsDot11WlanAuthenticationProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_ISSET_POOLID,
                 (UINT1 *)
                 pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
              MibObject.i4FsDot11WlanAuthenticationRowStatus ==
              CREATE_AND_WAIT)
             || (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                 MibObject.i4FsDot11WlanAuthenticationRowStatus ==
                 CREATE_AND_GO))
    {
        if (WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger
            (pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
             pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanAuthenticationProfileTable: WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger function returns failure.\r\n"));
        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11WlanAuthenticationProfileTable: The row is already present.\r\n"));
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgOldFsDot11WlanAuthenticationProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgTrgFsDot11WlanAuthenticationProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_ISSET_POOLID,
             (UINT1 *) pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pWsscfgOldFsDot11WlanAuthenticationProfileEntry,
            pWsscfgFsDot11WlanAuthenticationProfileEntry,
            sizeof (tWsscfgFsDot11WlanAuthenticationProfileEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
        MibObject.i4FsDot11WlanAuthenticationRowStatus == DESTROY)
    {
        pWsscfgFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanAuthenticationRowStatus = DESTROY;

        if (WsscfgUtilUpdateFsDot11WlanAuthenticationProfileTable
            (pWsscfgOldFsDot11WlanAuthenticationProfileEntry,
             pWsscfgFsDot11WlanAuthenticationProfileEntry,
             pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry) != OSIX_SUCCESS)
        {

            if (WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger
                (pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
                 pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanAuthenticationProfileTable: WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanAuthenticationProfileTable: WsscfgUtilUpdateFsDot11WlanAuthenticationProfileTable function returns failure.\r\n"));
        }
        RBTreeRem (gWsscfgGlobals.
                   WsscfgGlbMib.FsDot11WlanAuthenticationProfileTable,
                   pWsscfgFsDot11WlanAuthenticationProfileEntry);
        if (WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger
            (pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
             pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanAuthenticationProfileTable: WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgOldFsDot11WlanAuthenticationProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgTrgFsDot11WlanAuthenticationProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_ISSET_POOLID,
                 (UINT1 *)
                 pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgFsDot11WlanAuthenticationProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgOldFsDot11WlanAuthenticationProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgTrgFsDot11WlanAuthenticationProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_ISSET_POOLID,
             (UINT1 *) pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsDot11WlanAuthenticationProfileTableFilterInputs
        (pWsscfgFsDot11WlanAuthenticationProfileEntry,
         pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
         pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgOldFsDot11WlanAuthenticationProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgTrgFsDot11WlanAuthenticationProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_ISSET_POOLID,
             (UINT1 *) pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pWsscfgFsDot11WlanAuthenticationProfileEntry->
         MibObject.i4FsDot11WlanAuthenticationRowStatus == ACTIVE)
        && (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanAuthenticationRowStatus != NOT_IN_SERVICE))
    {
        pWsscfgFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanAuthenticationRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pWsscfgTrgFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanAuthenticationRowStatus = NOT_IN_SERVICE;
        pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry->
            bFsDot11WlanAuthenticationRowStatus = OSIX_TRUE;

        if (WsscfgUtilUpdateFsDot11WlanAuthenticationProfileTable
            (pWsscfgOldFsDot11WlanAuthenticationProfileEntry,
             pWsscfgFsDot11WlanAuthenticationProfileEntry,
             pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pWsscfgFsDot11WlanAuthenticationProfileEntry,
                    pWsscfgOldFsDot11WlanAuthenticationProfileEntry,
                    sizeof (tWsscfgFsDot11WlanAuthenticationProfileEntry));
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanAuthenticationProfileTable:                 WsscfgUtilUpdateFsDot11WlanAuthenticationProfileTable Function returns failure.\r\n"));

            if (WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger
                (pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
                 pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanAuthenticationProfileTable: WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgOldFsDot11WlanAuthenticationProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
                 (UINT1 *) pWsscfgTrgFsDot11WlanAuthenticationProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_ISSET_POOLID,
                 (UINT1 *)
                 pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry);
            return OSIX_FAILURE;
        }

        if (WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger
            (pWsscfgTrgFsDot11WlanAuthenticationProfileEntry,
             pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanAuthenticationProfileTable: WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger function returns failure.\r\n"));
        }
    }

    if (pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
        MibObject.i4FsDot11WlanAuthenticationRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanAuthenticationAlgorithm != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanAuthenticationAlgorithm =
            pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanAuthenticationAlgorithm;
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanWepKeyIndex != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanWepKeyIndex =
            pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanWepKeyIndex;
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanWepKeyType != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanWepKeyType =
            pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanWepKeyType;
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanWepKeyLength != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanWepKeyLength =
            pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanWepKeyLength;
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bFsDot11WlanWepKey !=
        OSIX_FALSE)
    {
        MEMCPY (pWsscfgFsDot11WlanAuthenticationProfileEntry->
                MibObject.au1FsDot11WlanWepKey,
                pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                MibObject.au1FsDot11WlanWepKey,
                pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
                MibObject.i4FsDot11WlanWepKeyLen);

        pWsscfgFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanWepKeyLen =
            pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanWepKeyLen;
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanWebAuthentication != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanWebAuthentication =
            pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanWebAuthentication;
    }
    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanAuthenticationRowStatus != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanAuthenticationRowStatus =
            pWsscfgSetFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanAuthenticationRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pWsscfgFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanAuthenticationRowStatus = ACTIVE;
    }

    if (WsscfgUtilUpdateFsDot11WlanAuthenticationProfileTable
        (pWsscfgOldFsDot11WlanAuthenticationProfileEntry,
         pWsscfgFsDot11WlanAuthenticationProfileEntry,
         pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry) != OSIX_SUCCESS)
    {

        if (WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger
            (pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
             pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanAuthenticationProfileTable: WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger function returns failure.\r\n"));

        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11WlanAuthenticationProfileTable: WsscfgUtilUpdateFsDot11WlanAuthenticationProfileTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pWsscfgFsDot11WlanAuthenticationProfileEntry,
                pWsscfgOldFsDot11WlanAuthenticationProfileEntry,
                sizeof (tWsscfgFsDot11WlanAuthenticationProfileEntry));
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgOldFsDot11WlanAuthenticationProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
             (UINT1 *) pWsscfgTrgFsDot11WlanAuthenticationProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_ISSET_POOLID,
             (UINT1 *) pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry);
        return OSIX_FAILURE;

    }
    if (WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger
        (pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
         pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11WlanAuthenticationProfileTable: WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock
        (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
         (UINT1 *) pWsscfgOldFsDot11WlanAuthenticationProfileEntry);
    MemReleaseMemBlock
        (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID,
         (UINT1 *) pWsscfgTrgFsDot11WlanAuthenticationProfileEntry);
    MemReleaseMemBlock
        (WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_ISSET_POOLID,
         (UINT1 *) pWsscfgTrgIsSetFsDot11WlanAuthenticationProfileEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11WlanQosProfileTable
 Input       :  pWsscfgSetFsDot11WlanQosProfileEntry
                pWsscfgIsSetFsDot11WlanQosProfileEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllFsDot11WlanQosProfileTable (tWsscfgFsDot11WlanQosProfileEntry *
                                        pWsscfgSetFsDot11WlanQosProfileEntry,
                                        tWsscfgIsSetFsDot11WlanQosProfileEntry *
                                        pWsscfgIsSetFsDot11WlanQosProfileEntry,
                                        INT4 i4RowStatusLogic,
                                        INT4 i4RowCreateOption)
{
    tWsscfgFsDot11WlanQosProfileEntry
        * pWsscfgFsDot11WlanQosProfileEntry = NULL;
    tWsscfgFsDot11WlanQosProfileEntry
        * pWsscfgOldFsDot11WlanQosProfileEntry = NULL;
    tWsscfgFsDot11WlanQosProfileEntry
        * pWsscfgTrgFsDot11WlanQosProfileEntry = NULL;
    tWsscfgIsSetFsDot11WlanQosProfileEntry
        * pWsscfgTrgIsSetFsDot11WlanQosProfileEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pWsscfgOldFsDot11WlanQosProfileEntry =
        (tWsscfgFsDot11WlanQosProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID);
    if (pWsscfgOldFsDot11WlanQosProfileEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWsscfgTrgFsDot11WlanQosProfileEntry =
        (tWsscfgFsDot11WlanQosProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID);
    if (pWsscfgTrgFsDot11WlanQosProfileEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11WlanQosProfileEntry);
        return OSIX_FAILURE;
    }
    pWsscfgTrgIsSetFsDot11WlanQosProfileEntry =
        (tWsscfgIsSetFsDot11WlanQosProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11WLANQOSPROFILETABLE_ISSET_POOLID);
    if (pWsscfgTrgIsSetFsDot11WlanQosProfileEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11WlanQosProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11WlanQosProfileEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pWsscfgOldFsDot11WlanQosProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanQosProfileEntry));
    MEMSET (pWsscfgTrgFsDot11WlanQosProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanQosProfileEntry));
    MEMSET (pWsscfgTrgIsSetFsDot11WlanQosProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11WlanQosProfileEntry));

    /* Check whether the node is already present */
    pWsscfgFsDot11WlanQosProfileEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.
                   FsDot11WlanQosProfileTable,
                   (tRBElem *) pWsscfgSetFsDot11WlanQosProfileEntry);

    if (pWsscfgFsDot11WlanQosProfileEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pWsscfgSetFsDot11WlanQosProfileEntry->
             MibObject.i4FsDot11WlanQosRowStatus == CREATE_AND_WAIT)
            || (pWsscfgSetFsDot11WlanQosProfileEntry->
                MibObject.i4FsDot11WlanQosRowStatus == CREATE_AND_GO)
            ||
            ((pWsscfgSetFsDot11WlanQosProfileEntry->
              MibObject.i4FsDot11WlanQosRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pWsscfgFsDot11WlanQosProfileEntry =
                (tWsscfgFsDot11WlanQosProfileEntry *)
                MemAllocMemBlk (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID);
            if (pWsscfgFsDot11WlanQosProfileEntry == NULL)
            {
                if (WsscfgSetAllFsDot11WlanQosProfileTableTrigger
                    (pWsscfgSetFsDot11WlanQosProfileEntry,
                     pWsscfgIsSetFsDot11WlanQosProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanQosProfileTable:WsscfgSetAllFsDot11WlanQosProfileTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanQosProfileTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11WlanQosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11WlanQosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANQOSPROFILETABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11WlanQosProfileEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pWsscfgFsDot11WlanQosProfileEntry, 0,
                    sizeof (tWsscfgFsDot11WlanQosProfileEntry));
            if ((WsscfgInitializeFsDot11WlanQosProfileTable
                 (pWsscfgFsDot11WlanQosProfileEntry)) == OSIX_FAILURE)
            {
                if (WsscfgSetAllFsDot11WlanQosProfileTableTrigger
                    (pWsscfgSetFsDot11WlanQosProfileEntry,
                     pWsscfgIsSetFsDot11WlanQosProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanQosProfileTable:WsscfgSetAllFsDot11WlanQosProfileTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanQosProfileTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgFsDot11WlanQosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11WlanQosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11WlanQosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANQOSPROFILETABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11WlanQosProfileEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
                bFsDot11WlanQosTraffic != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanQosTraffic =
                    pWsscfgSetFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanQosTraffic;
            }

            if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
                bFsDot11WlanQosPassengerTrustMode != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanQosPassengerTrustMode =
                    pWsscfgSetFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanQosPassengerTrustMode;
            }

            if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
                bFsDot11WlanQosRateLimit != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanQosRateLimit =
                    pWsscfgSetFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanQosRateLimit;
            }
            if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
                bFsDot11WlanUpStreamCIR != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanUpStreamCIR =
                    pWsscfgSetFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanUpStreamCIR;
            }
            if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
                bFsDot11WlanUpStreamCBS != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanUpStreamCBS =
                    pWsscfgSetFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanUpStreamCBS;

            }
            if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
                bFsDot11WlanUpStreamEIR != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanUpStreamEIR =
                    pWsscfgSetFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanUpStreamEIR;
            }
            if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
                bFsDot11WlanUpStreamEBS != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanUpStreamEBS =
                    pWsscfgSetFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanUpStreamEBS;
            }
            if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
                bFsDot11WlanDownStreamCIR != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanDownStreamCIR =
                    pWsscfgSetFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanDownStreamCIR;
            }
            if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
                bFsDot11WlanDownStreamCBS != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanDownStreamCBS =
                    pWsscfgSetFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanDownStreamCBS;
            }
            if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
                bFsDot11WlanDownStreamEIR != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanDownStreamEIR =
                    pWsscfgSetFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanDownStreamEIR;
            }
            if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
                bFsDot11WlanDownStreamEBS != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanDownStreamEBS =
                    pWsscfgSetFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanDownStreamEBS;
            }

            if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
                bFsDot11WlanQosRowStatus != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanQosRowStatus =
                    pWsscfgSetFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanQosRowStatus;
            }

            if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bIfIndex != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanQosProfileEntry->MibObject.
                    i4IfIndex =
                    pWsscfgSetFsDot11WlanQosProfileEntry->MibObject.i4IfIndex;
            }

            if ((pWsscfgSetFsDot11WlanQosProfileEntry->
                 MibObject.i4FsDot11WlanQosRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsDot11WlanQosProfileEntry->
                        MibObject.i4FsDot11WlanQosRowStatus == ACTIVE)))
            {
                pWsscfgFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanQosRowStatus = ACTIVE;
            }
            else if (pWsscfgSetFsDot11WlanQosProfileEntry->
                     MibObject.i4FsDot11WlanQosRowStatus == CREATE_AND_WAIT)
            {
                pWsscfgFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanQosRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gWsscfgGlobals.WsscfgGlbMib.
                 FsDot11WlanQosProfileTable,
                 (tRBElem *) pWsscfgFsDot11WlanQosProfileEntry) != RB_SUCCESS)
            {
                if (WsscfgSetAllFsDot11WlanQosProfileTableTrigger
                    (pWsscfgSetFsDot11WlanQosProfileEntry,
                     pWsscfgIsSetFsDot11WlanQosProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanQosProfileTable: WsscfgSetAllFsDot11WlanQosProfileTableTrigger function returns failure.\r\n"));
                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanQosProfileTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgFsDot11WlanQosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11WlanQosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11WlanQosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANQOSPROFILETABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11WlanQosProfileEntry);
                return OSIX_FAILURE;
            }
            if (WsscfgUtilUpdateFsDot11WlanQosProfileTable
                (NULL, pWsscfgFsDot11WlanQosProfileEntry,
                 pWsscfgIsSetFsDot11WlanQosProfileEntry) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanQosProfileTable: WsscfgUtilUpdateFsDot11WlanQosProfileTable function returns failure.\r\n"));

                if (WsscfgSetAllFsDot11WlanQosProfileTableTrigger
                    (pWsscfgSetFsDot11WlanQosProfileEntry,
                     pWsscfgIsSetFsDot11WlanQosProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanQosProfileTable: WsscfgSetAllFsDot11WlanQosProfileTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gWsscfgGlobals.
                           WsscfgGlbMib.FsDot11WlanQosProfileTable,
                           pWsscfgFsDot11WlanQosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgFsDot11WlanQosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11WlanQosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11WlanQosProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANQOSPROFILETABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11WlanQosProfileEntry);
                return OSIX_FAILURE;
            }

            if ((pWsscfgSetFsDot11WlanQosProfileEntry->
                 MibObject.i4FsDot11WlanQosRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsDot11WlanQosProfileEntry->
                        MibObject.i4FsDot11WlanQosRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsDot11WlanQosProfileEntry->MibObject.
                    i4IfIndex =
                    pWsscfgSetFsDot11WlanQosProfileEntry->MibObject.i4IfIndex;

                pWsscfgTrgFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanQosRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11WlanQosProfileEntry->
                    bFsDot11WlanQosRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsDot11WlanQosProfileTableTrigger
                    (pWsscfgTrgFsDot11WlanQosProfileEntry,
                     pWsscfgTrgIsSetFsDot11WlanQosProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanQosProfileTable: WsscfgSetAllFsDot11WlanQosProfileTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgFsDot11WlanQosProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgOldFsDot11WlanQosProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgTrgFsDot11WlanQosProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANQOSPROFILETABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetFsDot11WlanQosProfileEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pWsscfgSetFsDot11WlanQosProfileEntry->
                     MibObject.i4FsDot11WlanQosRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanQosRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11WlanQosProfileEntry->
                    bFsDot11WlanQosRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsDot11WlanQosProfileTableTrigger
                    (pWsscfgTrgFsDot11WlanQosProfileEntry,
                     pWsscfgTrgIsSetFsDot11WlanQosProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanQosProfileTable: WsscfgSetAllFsDot11WlanQosProfileTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgFsDot11WlanQosProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgOldFsDot11WlanQosProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgTrgFsDot11WlanQosProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANQOSPROFILETABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetFsDot11WlanQosProfileEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pWsscfgSetFsDot11WlanQosProfileEntry->
                MibObject.i4FsDot11WlanQosRowStatus == CREATE_AND_GO)
            {
                pWsscfgSetFsDot11WlanQosProfileEntry->
                    MibObject.i4FsDot11WlanQosRowStatus = ACTIVE;
            }

            if (WsscfgSetAllFsDot11WlanQosProfileTableTrigger
                (pWsscfgSetFsDot11WlanQosProfileEntry,
                 pWsscfgIsSetFsDot11WlanQosProfileEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanQosProfileTable:  WsscfgSetAllFsDot11WlanQosProfileTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID, (UINT1 *)
                 pWsscfgOldFsDot11WlanQosProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID, (UINT1 *)
                 pWsscfgTrgFsDot11WlanQosProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANQOSPROFILETABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11WlanQosProfileEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (WsscfgSetAllFsDot11WlanQosProfileTableTrigger
                (pWsscfgSetFsDot11WlanQosProfileEntry,
                 pWsscfgIsSetFsDot11WlanQosProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanQosProfileTable: WsscfgSetAllFsDot11WlanQosProfileTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanQosProfileTable: Failure.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID, (UINT1 *)
                 pWsscfgOldFsDot11WlanQosProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID, (UINT1 *)
                 pWsscfgTrgFsDot11WlanQosProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANQOSPROFILETABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11WlanQosProfileEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pWsscfgSetFsDot11WlanQosProfileEntry->
              MibObject.i4FsDot11WlanQosRowStatus == CREATE_AND_WAIT)
             || (pWsscfgSetFsDot11WlanQosProfileEntry->
                 MibObject.i4FsDot11WlanQosRowStatus == CREATE_AND_GO))
    {
        if (WsscfgSetAllFsDot11WlanQosProfileTableTrigger
            (pWsscfgSetFsDot11WlanQosProfileEntry,
             pWsscfgIsSetFsDot11WlanQosProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanQosProfileTable: WsscfgSetAllFsDot11WlanQosProfileTableTrigger function returns failure.\r\n"));
        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11WlanQosProfileTable: The row is already present.\r\n"));
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11WlanQosProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11WlanQosProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANQOSPROFILETABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetFsDot11WlanQosProfileEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pWsscfgOldFsDot11WlanQosProfileEntry,
            pWsscfgFsDot11WlanQosProfileEntry,
            sizeof (tWsscfgFsDot11WlanQosProfileEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pWsscfgSetFsDot11WlanQosProfileEntry->
        MibObject.i4FsDot11WlanQosRowStatus == DESTROY)
    {
        pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanQosRowStatus = DESTROY;

        if (WsscfgUtilUpdateFsDot11WlanQosProfileTable
            (pWsscfgOldFsDot11WlanQosProfileEntry,
             pWsscfgFsDot11WlanQosProfileEntry,
             pWsscfgIsSetFsDot11WlanQosProfileEntry) != OSIX_SUCCESS)
        {

            if (WsscfgSetAllFsDot11WlanQosProfileTableTrigger
                (pWsscfgSetFsDot11WlanQosProfileEntry,
                 pWsscfgIsSetFsDot11WlanQosProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanQosProfileTable: WsscfgSetAllFsDot11WlanQosProfileTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanQosProfileTable: WsscfgUtilUpdateFsDot11WlanQosProfileTable function returns failure.\r\n"));
        }
        RBTreeRem (gWsscfgGlobals.WsscfgGlbMib.
                   FsDot11WlanQosProfileTable,
                   pWsscfgFsDot11WlanQosProfileEntry);
        if (WsscfgSetAllFsDot11WlanQosProfileTableTrigger
            (pWsscfgSetFsDot11WlanQosProfileEntry,
             pWsscfgIsSetFsDot11WlanQosProfileEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanQosProfileTable: WsscfgSetAllFsDot11WlanQosProfileTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID, (UINT1 *)
                 pWsscfgOldFsDot11WlanQosProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID, (UINT1 *)
                 pWsscfgTrgFsDot11WlanQosProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANQOSPROFILETABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11WlanQosProfileEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgFsDot11WlanQosProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11WlanQosProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11WlanQosProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANQOSPROFILETABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetFsDot11WlanQosProfileEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsDot11WlanQosProfileTableFilterInputs
        (pWsscfgFsDot11WlanQosProfileEntry,
         pWsscfgSetFsDot11WlanQosProfileEntry,
         pWsscfgIsSetFsDot11WlanQosProfileEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11WlanQosProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11WlanQosProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANQOSPROFILETABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetFsDot11WlanQosProfileEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pWsscfgFsDot11WlanQosProfileEntry->
         MibObject.i4FsDot11WlanQosRowStatus == ACTIVE)
        && (pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanQosRowStatus != NOT_IN_SERVICE))
    {
        pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanQosRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pWsscfgTrgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanQosRowStatus = NOT_IN_SERVICE;
        pWsscfgTrgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanQosRowStatus
            = OSIX_TRUE;

        if (WsscfgUtilUpdateFsDot11WlanQosProfileTable
            (pWsscfgOldFsDot11WlanQosProfileEntry,
             pWsscfgFsDot11WlanQosProfileEntry,
             pWsscfgIsSetFsDot11WlanQosProfileEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pWsscfgFsDot11WlanQosProfileEntry,
                    pWsscfgOldFsDot11WlanQosProfileEntry,
                    sizeof (tWsscfgFsDot11WlanQosProfileEntry));
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanQosProfileTable:                 WsscfgUtilUpdateFsDot11WlanQosProfileTable Function returns failure.\r\n"));

            if (WsscfgSetAllFsDot11WlanQosProfileTableTrigger
                (pWsscfgSetFsDot11WlanQosProfileEntry,
                 pWsscfgIsSetFsDot11WlanQosProfileEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanQosProfileTable: WsscfgSetAllFsDot11WlanQosProfileTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID, (UINT1 *)
                 pWsscfgOldFsDot11WlanQosProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID, (UINT1 *)
                 pWsscfgTrgFsDot11WlanQosProfileEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANQOSPROFILETABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11WlanQosProfileEntry);
            return OSIX_FAILURE;
        }

        if (WsscfgSetAllFsDot11WlanQosProfileTableTrigger
            (pWsscfgTrgFsDot11WlanQosProfileEntry,
             pWsscfgTrgIsSetFsDot11WlanQosProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanQosProfileTable: WsscfgSetAllFsDot11WlanQosProfileTableTrigger function returns failure.\r\n"));
        }
    }

    if (pWsscfgSetFsDot11WlanQosProfileEntry->
        MibObject.i4FsDot11WlanQosRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanQosTraffic != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanQosTraffic =
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanQosTraffic;
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanQosPassengerTrustMode != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanQosPassengerTrustMode =
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanQosPassengerTrustMode;
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanQosRateLimit != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanQosRateLimit =
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanQosRateLimit;
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanUpStreamCIR != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanUpStreamCIR =
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanUpStreamCIR;
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanUpStreamCBS != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanUpStreamCBS =
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanUpStreamCBS;

    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanUpStreamEIR != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanUpStreamEIR =
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanUpStreamEIR;
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanUpStreamEBS != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanUpStreamEBS =
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanUpStreamEBS;
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamCIR !=
        OSIX_FALSE)
    {
        pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanDownStreamCIR =
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanDownStreamCIR;
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamCBS !=
        OSIX_FALSE)
    {
        pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanDownStreamCBS =
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanDownStreamCBS;
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamEIR !=
        OSIX_FALSE)
    {
        pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanDownStreamEIR =
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanDownStreamEIR;
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamEBS !=
        OSIX_FALSE)
    {
        pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanDownStreamEBS =
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanDownStreamEBS;
    }
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanQosRowStatus != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanQosRowStatus =
            pWsscfgSetFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanQosRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanQosRowStatus = ACTIVE;
    }

    if (WsscfgUtilUpdateFsDot11WlanQosProfileTable
        (pWsscfgOldFsDot11WlanQosProfileEntry,
         pWsscfgFsDot11WlanQosProfileEntry,
         pWsscfgIsSetFsDot11WlanQosProfileEntry) != OSIX_SUCCESS)
    {

        if (WsscfgSetAllFsDot11WlanQosProfileTableTrigger
            (pWsscfgSetFsDot11WlanQosProfileEntry,
             pWsscfgIsSetFsDot11WlanQosProfileEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanQosProfileTable: WsscfgSetAllFsDot11WlanQosProfileTableTrigger function returns failure.\r\n"));

        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11WlanQosProfileTable: WsscfgUtilUpdateFsDot11WlanQosProfileTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pWsscfgFsDot11WlanQosProfileEntry,
                pWsscfgOldFsDot11WlanQosProfileEntry,
                sizeof (tWsscfgFsDot11WlanQosProfileEntry));
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11WlanQosProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11WlanQosProfileEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11WLANQOSPROFILETABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetFsDot11WlanQosProfileEntry);
        return OSIX_FAILURE;

    }
    if (WsscfgSetAllFsDot11WlanQosProfileTableTrigger
        (pWsscfgSetFsDot11WlanQosProfileEntry,
         pWsscfgIsSetFsDot11WlanQosProfileEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11WlanQosProfileTable: WsscfgSetAllFsDot11WlanQosProfileTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                        (UINT1 *) pWsscfgOldFsDot11WlanQosProfileEntry);
    MemReleaseMemBlock (WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID,
                        (UINT1 *) pWsscfgTrgFsDot11WlanQosProfileEntry);
    MemReleaseMemBlock (WSSCFG_FSDOT11WLANQOSPROFILETABLE_ISSET_POOLID,
                        (UINT1 *) pWsscfgTrgIsSetFsDot11WlanQosProfileEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11RadioQosTable
 Input       :  pWsscfgSetFsDot11RadioQosEntry
                pWsscfgIsSetFsDot11RadioQosEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllFsDot11RadioQosTable (tWsscfgFsDot11RadioQosEntry *
                                  pWsscfgSetFsDot11RadioQosEntry,
                                  tWsscfgIsSetFsDot11RadioQosEntry *
                                  pWsscfgIsSetFsDot11RadioQosEntry)
{
    tWsscfgFsDot11RadioQosEntry WsscfgOldFsDot11RadioQosEntry;

    MEMSET (&WsscfgOldFsDot11RadioQosEntry, 0,
            sizeof (tWsscfgFsDot11RadioQosEntry));

    if (WsscfgUtilUpdateFsDot11RadioQosTable
        (&WsscfgOldFsDot11RadioQosEntry,
         pWsscfgSetFsDot11RadioQosEntry,
         pWsscfgIsSetFsDot11RadioQosEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11QAPTable
 Input       :  pWsscfgSetFsDot11QAPEntry
                pWsscfgIsSetFsDot11QAPEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllFsDot11QAPTable (tWsscfgFsDot11QAPEntry * pWsscfgSetFsDot11QAPEntry,
                             tWsscfgIsSetFsDot11QAPEntry *
                             pWsscfgIsSetFsDot11QAPEntry)
{
    tWsscfgFsDot11QAPEntry WsscfgOldFsDot11QAPEntry;

    MEMSET (&WsscfgOldFsDot11QAPEntry, 0, sizeof (tWsscfgFsDot11QAPEntry));
    if (WsscfgUtilUpdateFsDot11QAPTable
        (&WsscfgOldFsDot11QAPEntry, pWsscfgSetFsDot11QAPEntry,
         pWsscfgIsSetFsDot11QAPEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsQAPProfileTable
 Input       :  pWsscfgSetFsQAPProfileEntry
                pWsscfgIsSetFsQAPProfileEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllFsQAPProfileTable (tWsscfgFsQAPProfileEntry *
                               pWsscfgSetFsQAPProfileEntry,
                               tWsscfgIsSetFsQAPProfileEntry *
                               pWsscfgIsSetFsQAPProfileEntry)
{
    tWsscfgFsQAPProfileEntry *pWsscfgFsQAPProfileEntry = NULL;
    tWsscfgFsQAPProfileEntry *pWsscfgOldFsQAPProfileEntry = NULL;
    tWsscfgFsQAPProfileEntry *pWsscfgTrgFsQAPProfileEntry = NULL;
    tWsscfgIsSetFsQAPProfileEntry *pWsscfgTrgIsSetFsQAPProfileEntry = NULL;
    INT4                i4RowMakeActive = FALSE, i4RowStatusLogic = FALSE;

    pWsscfgOldFsQAPProfileEntry = (tWsscfgFsQAPProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSQAPPROFILETABLE_POOLID);
    if (pWsscfgOldFsQAPProfileEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWsscfgTrgFsQAPProfileEntry = (tWsscfgFsQAPProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSQAPPROFILETABLE_POOLID);
    if (pWsscfgTrgFsQAPProfileEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsQAPProfileEntry);
        return OSIX_FAILURE;
    }
    pWsscfgTrgIsSetFsQAPProfileEntry =
        (tWsscfgIsSetFsQAPProfileEntry *)
        MemAllocMemBlk (WSSCFG_FSQAPPROFILETABLE_ISSET_POOLID);
    if (pWsscfgTrgIsSetFsQAPProfileEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsQAPProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsQAPProfileEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pWsscfgOldFsQAPProfileEntry, 0, sizeof (tWsscfgFsQAPProfileEntry));
    MEMSET (pWsscfgTrgFsQAPProfileEntry, 0, sizeof (tWsscfgFsQAPProfileEntry));
    MEMSET (pWsscfgTrgIsSetFsQAPProfileEntry, 0,
            sizeof (tWsscfgIsSetFsQAPProfileEntry));
    /* Check whether the node is already present */
    pWsscfgFsQAPProfileEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsQAPProfileTable,
                   (tRBElem *) pWsscfgSetFsQAPProfileEntry);

    if (pWsscfgFsQAPProfileEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pWsscfgSetFsQAPProfileEntry->
             MibObject.i4FsQAPProfileRowStatus == CREATE_AND_WAIT)
            || (pWsscfgSetFsQAPProfileEntry->
                MibObject.i4FsQAPProfileRowStatus == CREATE_AND_GO)
            || (pWsscfgSetFsQAPProfileEntry->
                MibObject.i4FsQAPProfileRowStatus == ACTIVE))
        {
            /* Allocate memory for the new node */
            pWsscfgFsQAPProfileEntry = (tWsscfgFsQAPProfileEntry *)
                MemAllocMemBlk (WSSCFG_FSQAPPROFILETABLE_POOLID);
            if (pWsscfgFsQAPProfileEntry == NULL)
            {
                if (WsscfgSetAllFsQAPProfileTableTrigger
                    (pWsscfgSetFsQAPProfileEntry,
                     pWsscfgIsSetFsQAPProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsQAPProfileTable:WsscfgSetAllFsQAPProfileTableTrigger function fails\r\n"));
                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsQAPProfileTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                                    (UINT1 *) pWsscfgOldFsQAPProfileEntry);
                MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                                    (UINT1 *) pWsscfgTrgFsQAPProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSQAPPROFILETABLE_ISSET_POOLID, (UINT1 *)
                     pWsscfgTrgIsSetFsQAPProfileEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pWsscfgFsQAPProfileEntry, 0,
                    sizeof (tWsscfgFsQAPProfileEntry));
            if ((WsscfgInitializeFsQAPProfileTable
                 (pWsscfgFsQAPProfileEntry)) == OSIX_FAILURE)
            {
                if (WsscfgSetAllFsQAPProfileTableTrigger
                    (pWsscfgSetFsQAPProfileEntry,
                     pWsscfgIsSetFsQAPProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsQAPProfileTable:WsscfgSetAllFsQAPProfileTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsQAPProfileTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                                    (UINT1 *) pWsscfgFsQAPProfileEntry);
                MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                                    (UINT1 *) pWsscfgOldFsQAPProfileEntry);
                MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                                    (UINT1 *) pWsscfgTrgFsQAPProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSQAPPROFILETABLE_ISSET_POOLID, (UINT1 *)
                     pWsscfgTrgIsSetFsQAPProfileEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileName != OSIX_FALSE)
            {
                MEMCPY (pWsscfgFsQAPProfileEntry->
                        MibObject.au1FsQAPProfileName,
                        pWsscfgSetFsQAPProfileEntry->
                        MibObject.au1FsQAPProfileName,
                        pWsscfgSetFsQAPProfileEntry->
                        MibObject.i4FsQAPProfileNameLen);

                pWsscfgFsQAPProfileEntry->MibObject.
                    i4FsQAPProfileNameLen =
                    pWsscfgSetFsQAPProfileEntry->
                    MibObject.i4FsQAPProfileNameLen;
            }

            if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileIndex != OSIX_FALSE)
            {
                pWsscfgFsQAPProfileEntry->MibObject.
                    i4FsQAPProfileIndex =
                    pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileIndex;
            }

            if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileCWmin != OSIX_FALSE)
            {
                pWsscfgFsQAPProfileEntry->MibObject.
                    i4FsQAPProfileCWmin =
                    pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileCWmin;
            }

            if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileCWmax != OSIX_FALSE)
            {
                pWsscfgFsQAPProfileEntry->MibObject.
                    i4FsQAPProfileCWmax =
                    pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileCWmax;
            }

            if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileAIFSN != OSIX_FALSE)
            {
                pWsscfgFsQAPProfileEntry->MibObject.
                    i4FsQAPProfileAIFSN =
                    pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileAIFSN;
            }
            if (pWsscfgIsSetFsQAPProfileEntry->
                bFsQAPProfileTXOPLimit != OSIX_FALSE)
            {
                pWsscfgFsQAPProfileEntry->
                    MibObject.i4FsQAPProfileTXOPLimit =
                    pWsscfgSetFsQAPProfileEntry->
                    MibObject.i4FsQAPProfileTXOPLimit;
            }
            if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileAdmissionControl !=
                OSIX_FALSE)
            {
                pWsscfgFsQAPProfileEntry->
                    MibObject.i4FsQAPProfileAdmissionControl =
                    pWsscfgSetFsQAPProfileEntry->
                    MibObject.i4FsQAPProfileAdmissionControl;
            }

            if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfilePriorityValue !=
                OSIX_FALSE)
            {
                pWsscfgFsQAPProfileEntry->
                    MibObject.i4FsQAPProfilePriorityValue =
                    pWsscfgSetFsQAPProfileEntry->
                    MibObject.i4FsQAPProfilePriorityValue;
            }

            if (pWsscfgIsSetFsQAPProfileEntry->
                bFsQAPProfileDscpValue != OSIX_FALSE)
            {
                pWsscfgFsQAPProfileEntry->
                    MibObject.i4FsQAPProfileDscpValue =
                    pWsscfgSetFsQAPProfileEntry->
                    MibObject.i4FsQAPProfileDscpValue;
            }

            if (pWsscfgIsSetFsQAPProfileEntry->
                bFsQAPProfileRowStatus != OSIX_FALSE)
            {
                pWsscfgFsQAPProfileEntry->
                    MibObject.i4FsQAPProfileRowStatus =
                    pWsscfgSetFsQAPProfileEntry->
                    MibObject.i4FsQAPProfileRowStatus;
            }

            if ((pWsscfgSetFsQAPProfileEntry->
                 MibObject.i4FsQAPProfileRowStatus == CREATE_AND_GO))
            {
                pWsscfgFsQAPProfileEntry->
                    MibObject.i4FsQAPProfileRowStatus = ACTIVE;
            }
            else if (pWsscfgSetFsQAPProfileEntry->
                     MibObject.i4FsQAPProfileRowStatus == CREATE_AND_WAIT)
            {
                pWsscfgFsQAPProfileEntry->
                    MibObject.i4FsQAPProfileRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gWsscfgGlobals.WsscfgGlbMib.FsQAPProfileTable,
                 (tRBElem *) pWsscfgFsQAPProfileEntry) != RB_SUCCESS)
            {
                if (WsscfgSetAllFsQAPProfileTableTrigger
                    (pWsscfgSetFsQAPProfileEntry,
                     pWsscfgIsSetFsQAPProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsQAPProfileTable: WsscfgSetAllFsQAPProfileTableTrigger function returns failure.\r\n"));
                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsQAPProfileTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                                    (UINT1 *) pWsscfgFsQAPProfileEntry);
                MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                                    (UINT1 *) pWsscfgOldFsQAPProfileEntry);
                MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                                    (UINT1 *) pWsscfgTrgFsQAPProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSQAPPROFILETABLE_ISSET_POOLID, (UINT1 *)
                     pWsscfgTrgIsSetFsQAPProfileEntry);
                return OSIX_FAILURE;
            }
            if (WsscfgUtilUpdateFsQAPProfileTable
                (NULL, pWsscfgFsQAPProfileEntry,
                 pWsscfgIsSetFsQAPProfileEntry) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsQAPProfileTable: WsscfgUtilUpdateFsQAPProfileTable function returns failure.\r\n"));

                if (WsscfgSetAllFsQAPProfileTableTrigger
                    (pWsscfgSetFsQAPProfileEntry,
                     pWsscfgIsSetFsQAPProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsQAPProfileTable: WsscfgSetAllFsQAPProfileTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gWsscfgGlobals.WsscfgGlbMib.
                           FsQAPProfileTable, pWsscfgFsQAPProfileEntry);
                MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                                    (UINT1 *) pWsscfgFsQAPProfileEntry);
                MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                                    (UINT1 *) pWsscfgOldFsQAPProfileEntry);
                MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                                    (UINT1 *) pWsscfgTrgFsQAPProfileEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSQAPPROFILETABLE_ISSET_POOLID, (UINT1 *)
                     pWsscfgTrgIsSetFsQAPProfileEntry);
                return OSIX_FAILURE;
            }

            if ((pWsscfgSetFsQAPProfileEntry->
                 MibObject.i4FsQAPProfileRowStatus == CREATE_AND_GO))
            {
                /* For MSR and RM Trigger */
                MEMCPY (pWsscfgTrgFsQAPProfileEntry->
                        MibObject.au1FsQAPProfileName,
                        pWsscfgSetFsQAPProfileEntry->
                        MibObject.au1FsQAPProfileName,
                        pWsscfgSetFsQAPProfileEntry->
                        MibObject.i4FsQAPProfileNameLen);

                pWsscfgTrgFsQAPProfileEntry->
                    MibObject.i4FsQAPProfileNameLen =
                    pWsscfgSetFsQAPProfileEntry->
                    MibObject.i4FsQAPProfileNameLen;
                pWsscfgTrgFsQAPProfileEntry->
                    MibObject.i4FsQAPProfileIndex =
                    pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileIndex;
                pWsscfgTrgFsQAPProfileEntry->
                    MibObject.i4FsQAPProfileRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsQAPProfileEntry->
                    bFsQAPProfileRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsQAPProfileTableTrigger
                    (pWsscfgTrgFsQAPProfileEntry,
                     pWsscfgTrgIsSetFsQAPProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsQAPProfileTable: WsscfgSetAllFsQAPProfileTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSQAPPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgFsQAPProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSQAPPROFILETABLE_POOLID, (UINT1 *)
                         pWsscfgOldFsQAPProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSQAPPROFILETABLE_POOLID, (UINT1 *)
                         pWsscfgTrgFsQAPProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSQAPPROFILETABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetFsQAPProfileEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pWsscfgSetFsQAPProfileEntry->
                     MibObject.i4FsQAPProfileRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsQAPProfileEntry->
                    MibObject.i4FsQAPProfileRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsQAPProfileEntry->
                    bFsQAPProfileRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsQAPProfileTableTrigger
                    (pWsscfgTrgFsQAPProfileEntry,
                     pWsscfgTrgIsSetFsQAPProfileEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsQAPProfileTable: WsscfgSetAllFsQAPProfileTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSQAPPROFILETABLE_POOLID,
                         (UINT1 *) pWsscfgFsQAPProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSQAPPROFILETABLE_POOLID, (UINT1 *)
                         pWsscfgOldFsQAPProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSQAPPROFILETABLE_POOLID, (UINT1 *)
                         pWsscfgTrgFsQAPProfileEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSQAPPROFILETABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetFsQAPProfileEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pWsscfgSetFsQAPProfileEntry->
                MibObject.i4FsQAPProfileRowStatus == CREATE_AND_GO)
            {
                pWsscfgSetFsQAPProfileEntry->
                    MibObject.i4FsQAPProfileRowStatus = ACTIVE;
            }

            if (WsscfgSetAllFsQAPProfileTableTrigger
                (pWsscfgSetFsQAPProfileEntry,
                 pWsscfgIsSetFsQAPProfileEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsQAPProfileTable:  WsscfgSetAllFsQAPProfileTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsQAPProfileEntry);
            MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsQAPProfileEntry);
            MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_ISSET_POOLID,
                                (UINT1 *) pWsscfgTrgIsSetFsQAPProfileEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (WsscfgSetAllFsQAPProfileTableTrigger
                (pWsscfgSetFsQAPProfileEntry,
                 pWsscfgIsSetFsQAPProfileEntry, SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsQAPProfileTable: WsscfgSetAllFsQAPProfileTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsQAPProfileTable: Failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsQAPProfileEntry);
            MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsQAPProfileEntry);
            MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_ISSET_POOLID,
                                (UINT1 *) pWsscfgTrgIsSetFsQAPProfileEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pWsscfgSetFsQAPProfileEntry->
              MibObject.i4FsQAPProfileRowStatus == CREATE_AND_WAIT)
             || (pWsscfgSetFsQAPProfileEntry->
                 MibObject.i4FsQAPProfileRowStatus == CREATE_AND_GO))
    {
        if (WsscfgSetAllFsQAPProfileTableTrigger
            (pWsscfgSetFsQAPProfileEntry,
             pWsscfgIsSetFsQAPProfileEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsQAPProfileTable: WsscfgSetAllFsQAPProfileTableTrigger function returns failure.\r\n"));
        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsQAPProfileTable: The row is already present.\r\n"));
        MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsQAPProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsQAPProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsQAPProfileEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pWsscfgOldFsQAPProfileEntry, pWsscfgFsQAPProfileEntry,
            sizeof (tWsscfgFsQAPProfileEntry));
    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pWsscfgSetFsQAPProfileEntry->MibObject.
        i4FsQAPProfileRowStatus == DESTROY)
    {
        pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileRowStatus = DESTROY;

        if (WsscfgUtilUpdateFsQAPProfileTable
            (pWsscfgOldFsQAPProfileEntry, pWsscfgFsQAPProfileEntry,
             pWsscfgIsSetFsQAPProfileEntry) != OSIX_SUCCESS)
        {

            if (WsscfgSetAllFsQAPProfileTableTrigger
                (pWsscfgSetFsQAPProfileEntry,
                 pWsscfgIsSetFsQAPProfileEntry, SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsQAPProfileTable: WsscfgSetAllFsQAPProfileTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsQAPProfileTable: WsscfgUtilUpdateFsQAPProfileTable function returns failure.\r\n"));
        }
        RBTreeRem (gWsscfgGlobals.WsscfgGlbMib.FsQAPProfileTable,
                   pWsscfgFsQAPProfileEntry);
        if (WsscfgSetAllFsQAPProfileTableTrigger
            (pWsscfgSetFsQAPProfileEntry,
             pWsscfgIsSetFsQAPProfileEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsQAPProfileTable: WsscfgSetAllFsQAPProfileTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsQAPProfileEntry);
            MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsQAPProfileEntry);
            MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_ISSET_POOLID,
                                (UINT1 *) pWsscfgTrgIsSetFsQAPProfileEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgFsQAPProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsQAPProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsQAPProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsQAPProfileEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsQAPProfileTableFilterInputs
        (pWsscfgFsQAPProfileEntry, pWsscfgSetFsQAPProfileEntry,
         pWsscfgIsSetFsQAPProfileEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsQAPProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsQAPProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsQAPProfileEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((pWsscfgFsQAPProfileEntry->MibObject.
         i4FsQAPProfileRowStatus == ACTIVE)
        && (pWsscfgSetFsQAPProfileEntry->
            MibObject.i4FsQAPProfileRowStatus != NOT_IN_SERVICE))
    {
        pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pWsscfgTrgFsQAPProfileEntry->MibObject.
            i4FsQAPProfileRowStatus = NOT_IN_SERVICE;
        pWsscfgTrgIsSetFsQAPProfileEntry->bFsQAPProfileRowStatus = OSIX_TRUE;

        if (WsscfgUtilUpdateFsQAPProfileTable
            (pWsscfgOldFsQAPProfileEntry, pWsscfgFsQAPProfileEntry,
             pWsscfgIsSetFsQAPProfileEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pWsscfgFsQAPProfileEntry,
                    pWsscfgOldFsQAPProfileEntry,
                    sizeof (tWsscfgFsQAPProfileEntry));
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsQAPProfileTable:                            WsscfgUtilUpdateFsQAPProfileTable Function returns failure.\r\n"));

            if (WsscfgSetAllFsQAPProfileTableTrigger
                (pWsscfgSetFsQAPProfileEntry,
                 pWsscfgIsSetFsQAPProfileEntry, SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsQAPProfileTable: WsscfgSetAllFsQAPProfileTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsQAPProfileEntry);
            MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsQAPProfileEntry);
            MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_ISSET_POOLID,
                                (UINT1 *) pWsscfgTrgIsSetFsQAPProfileEntry);
            return OSIX_FAILURE;
        }

        if (WsscfgSetAllFsQAPProfileTableTrigger
            (pWsscfgTrgFsQAPProfileEntry,
             pWsscfgTrgIsSetFsQAPProfileEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsQAPProfileTable: WsscfgSetAllFsQAPProfileTableTrigger function returns failure.\r\n"));
        }
    }

    if (pWsscfgSetFsQAPProfileEntry->MibObject.
        i4FsQAPProfileRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }
    /* Assign values for the existing node */
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileCWmin != OSIX_FALSE)
    {
        pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileCWmin =
            pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileCWmin;
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileCWmax != OSIX_FALSE)
    {
        pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileCWmax =
            pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileCWmax;
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileAIFSN != OSIX_FALSE)
    {
        pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileAIFSN =
            pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileAIFSN;
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileTXOPLimit != OSIX_FALSE)
    {
        pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileTXOPLimit =
            pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileTXOPLimit;
    }
    if (pWsscfgIsSetFsQAPProfileEntry->
        bFsQAPProfileAdmissionControl != OSIX_FALSE)
    {
        pWsscfgFsQAPProfileEntry->
            MibObject.i4FsQAPProfileAdmissionControl =
            pWsscfgSetFsQAPProfileEntry->
            MibObject.i4FsQAPProfileAdmissionControl;
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfilePriorityValue != OSIX_FALSE)
    {
        pWsscfgFsQAPProfileEntry->MibObject.
            i4FsQAPProfilePriorityValue =
            pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfilePriorityValue;
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileDscpValue != OSIX_FALSE)
    {
        pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileDscpValue =
            pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileDscpValue;
    }
    if (pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileRowStatus != OSIX_FALSE)
    {
        pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileRowStatus =
            pWsscfgSetFsQAPProfileEntry->MibObject.i4FsQAPProfileRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileRowStatus = ACTIVE;
    }

    if (WsscfgUtilUpdateFsQAPProfileTable (pWsscfgOldFsQAPProfileEntry,
                                           pWsscfgFsQAPProfileEntry,
                                           pWsscfgIsSetFsQAPProfileEntry)
        != OSIX_SUCCESS)
    {

        if (WsscfgSetAllFsQAPProfileTableTrigger
            (pWsscfgSetFsQAPProfileEntry,
             pWsscfgIsSetFsQAPProfileEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsQAPProfileTable: WsscfgSetAllFsQAPProfileTableTrigger function returns failure.\r\n"));

        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsQAPProfileTable: WsscfgUtilUpdateFsQAPProfileTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pWsscfgFsQAPProfileEntry, pWsscfgOldFsQAPProfileEntry,
                sizeof (tWsscfgFsQAPProfileEntry));
        MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsQAPProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsQAPProfileEntry);
        MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsQAPProfileEntry);
        return OSIX_FAILURE;

    }
    if (WsscfgSetAllFsQAPProfileTableTrigger
        (pWsscfgSetFsQAPProfileEntry, pWsscfgIsSetFsQAPProfileEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsQAPProfileTable: WsscfgSetAllFsQAPProfileTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                        (UINT1 *) pWsscfgOldFsQAPProfileEntry);
    MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_POOLID,
                        (UINT1 *) pWsscfgTrgFsQAPProfileEntry);
    MemReleaseMemBlock (WSSCFG_FSQAPPROFILETABLE_ISSET_POOLID,
                        (UINT1 *) pWsscfgTrgIsSetFsQAPProfileEntry);
    UNUSED_PARAM (i4RowStatusLogic);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11CapabilityMappingTable
 Input       :  pWsscfgSetFsDot11CapabilityMappingEntry
                pWsscfgIsSetFsDot11CapabilityMappingEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllFsDot11CapabilityMappingTable (tWsscfgFsDot11CapabilityMappingEntry
                                           *
                                           pWsscfgSetFsDot11CapabilityMappingEntry,
                                           tWsscfgIsSetFsDot11CapabilityMappingEntry
                                           *
                                           pWsscfgIsSetFsDot11CapabilityMappingEntry,
                                           INT4 i4RowStatusLogic,
                                           INT4 i4RowCreateOption)
{
    tWsscfgFsDot11CapabilityMappingEntry
        * pWsscfgFsDot11CapabilityMappingEntry = NULL;
    tWsscfgFsDot11CapabilityMappingEntry
        * pWsscfgOldFsDot11CapabilityMappingEntry = NULL;
    tWsscfgFsDot11CapabilityMappingEntry
        * pWsscfgTrgFsDot11CapabilityMappingEntry = NULL;
    tWsscfgIsSetFsDot11CapabilityMappingEntry
        * pWsscfgTrgIsSetFsDot11CapabilityMappingEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pWsscfgOldFsDot11CapabilityMappingEntry =
        (tWsscfgFsDot11CapabilityMappingEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID);
    if (pWsscfgOldFsDot11CapabilityMappingEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWsscfgTrgFsDot11CapabilityMappingEntry =
        (tWsscfgFsDot11CapabilityMappingEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID);
    if (pWsscfgTrgFsDot11CapabilityMappingEntry == NULL)
    {
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID, (UINT1 *)
             pWsscfgOldFsDot11CapabilityMappingEntry);
        return OSIX_FAILURE;
    }
    pWsscfgTrgIsSetFsDot11CapabilityMappingEntry =
        (tWsscfgIsSetFsDot11CapabilityMappingEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_ISSET_POOLID);
    if (pWsscfgTrgIsSetFsDot11CapabilityMappingEntry == NULL)
    {
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID, (UINT1 *)
             pWsscfgOldFsDot11CapabilityMappingEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID, (UINT1 *)
             pWsscfgTrgFsDot11CapabilityMappingEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pWsscfgOldFsDot11CapabilityMappingEntry, 0,
            sizeof (tWsscfgFsDot11CapabilityMappingEntry));
    MEMSET (pWsscfgTrgFsDot11CapabilityMappingEntry, 0,
            sizeof (tWsscfgFsDot11CapabilityMappingEntry));
    MEMSET (pWsscfgTrgIsSetFsDot11CapabilityMappingEntry, 0,
            sizeof (tWsscfgIsSetFsDot11CapabilityMappingEntry));

    /* Check whether the node is already present */
    pWsscfgFsDot11CapabilityMappingEntry =
        RBTreeGet (gWsscfgGlobals.
                   WsscfgGlbMib.FsDot11CapabilityMappingTable,
                   (tRBElem *) pWsscfgSetFsDot11CapabilityMappingEntry);

    if (pWsscfgFsDot11CapabilityMappingEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pWsscfgSetFsDot11CapabilityMappingEntry->
             MibObject.i4FsDot11CapabilityMappingRowStatus ==
             CREATE_AND_WAIT)
            || (pWsscfgSetFsDot11CapabilityMappingEntry->
                MibObject.i4FsDot11CapabilityMappingRowStatus ==
                CREATE_AND_GO)
            ||
            ((pWsscfgSetFsDot11CapabilityMappingEntry->
              MibObject.i4FsDot11CapabilityMappingRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pWsscfgFsDot11CapabilityMappingEntry =
                (tWsscfgFsDot11CapabilityMappingEntry *)
                MemAllocMemBlk (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID);
            if (pWsscfgFsDot11CapabilityMappingEntry == NULL)
            {
                if (WsscfgSetAllFsDot11CapabilityMappingTableTrigger
                    (pWsscfgSetFsDot11CapabilityMappingEntry,
                     pWsscfgIsSetFsDot11CapabilityMappingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11CapabilityMappingTable:WsscfgSetAllFsDot11CapabilityMappingTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11CapabilityMappingTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11CapabilityMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11CapabilityMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityMappingEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pWsscfgFsDot11CapabilityMappingEntry, 0,
                    sizeof (tWsscfgFsDot11CapabilityMappingEntry));
            if ((WsscfgInitializeFsDot11CapabilityMappingTable
                 (pWsscfgFsDot11CapabilityMappingEntry)) == OSIX_FAILURE)
            {
                if (WsscfgSetAllFsDot11CapabilityMappingTableTrigger
                    (pWsscfgSetFsDot11CapabilityMappingEntry,
                     pWsscfgIsSetFsDot11CapabilityMappingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11CapabilityMappingTable:WsscfgSetAllFsDot11CapabilityMappingTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11CapabilityMappingTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                     (UINT1 *) pWsscfgFsDot11CapabilityMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11CapabilityMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11CapabilityMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityMappingEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pWsscfgIsSetFsDot11CapabilityMappingEntry->
                bFsDot11CapabilityMappingProfileName != OSIX_FALSE)
            {
                MEMCPY (pWsscfgFsDot11CapabilityMappingEntry->
                        MibObject.au1FsDot11CapabilityMappingProfileName,
                        pWsscfgSetFsDot11CapabilityMappingEntry->
                        MibObject.au1FsDot11CapabilityMappingProfileName,
                        pWsscfgSetFsDot11CapabilityMappingEntry->
                        MibObject.i4FsDot11CapabilityMappingProfileNameLen);

                pWsscfgFsDot11CapabilityMappingEntry->
                    MibObject.i4FsDot11CapabilityMappingProfileNameLen
                    =
                    pWsscfgSetFsDot11CapabilityMappingEntry->
                    MibObject.i4FsDot11CapabilityMappingProfileNameLen;
            }

            if (pWsscfgIsSetFsDot11CapabilityMappingEntry->
                bFsDot11CapabilityMappingRowStatus != OSIX_FALSE)
            {
                pWsscfgFsDot11CapabilityMappingEntry->
                    MibObject.i4FsDot11CapabilityMappingRowStatus =
                    pWsscfgSetFsDot11CapabilityMappingEntry->
                    MibObject.i4FsDot11CapabilityMappingRowStatus;
            }

            if (pWsscfgIsSetFsDot11CapabilityMappingEntry->bIfIndex !=
                OSIX_FALSE)
            {
                pWsscfgFsDot11CapabilityMappingEntry->MibObject.
                    i4IfIndex =
                    pWsscfgSetFsDot11CapabilityMappingEntry->
                    MibObject.i4IfIndex;
            }

            if ((pWsscfgSetFsDot11CapabilityMappingEntry->
                 MibObject.i4FsDot11CapabilityMappingRowStatus ==
                 CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsDot11CapabilityMappingEntry->
                        MibObject.i4FsDot11CapabilityMappingRowStatus
                        == ACTIVE)))
            {
                pWsscfgFsDot11CapabilityMappingEntry->
                    MibObject.i4FsDot11CapabilityMappingRowStatus = ACTIVE;
            }
            else if (pWsscfgSetFsDot11CapabilityMappingEntry->
                     MibObject.i4FsDot11CapabilityMappingRowStatus
                     == CREATE_AND_WAIT)
            {
                pWsscfgFsDot11CapabilityMappingEntry->
                    MibObject.i4FsDot11CapabilityMappingRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gWsscfgGlobals.WsscfgGlbMib.
                 FsDot11CapabilityMappingTable,
                 (tRBElem *) pWsscfgFsDot11CapabilityMappingEntry) !=
                RB_SUCCESS)
            {
                if (WsscfgSetAllFsDot11CapabilityMappingTableTrigger
                    (pWsscfgSetFsDot11CapabilityMappingEntry,
                     pWsscfgIsSetFsDot11CapabilityMappingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11CapabilityMappingTable: WsscfgSetAllFsDot11CapabilityMappingTableTrigger function returns failure.\r\n"));
                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11CapabilityMappingTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                     (UINT1 *) pWsscfgFsDot11CapabilityMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11CapabilityMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11CapabilityMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityMappingEntry);
                return OSIX_FAILURE;
            }
            if (WsscfgUtilUpdateFsDot11CapabilityMappingTable
                (NULL, pWsscfgFsDot11CapabilityMappingEntry,
                 pWsscfgIsSetFsDot11CapabilityMappingEntry) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11CapabilityMappingTable: WsscfgUtilUpdateFsDot11CapabilityMappingTable function returns failure.\r\n"));

                if (WsscfgSetAllFsDot11CapabilityMappingTableTrigger
                    (pWsscfgSetFsDot11CapabilityMappingEntry,
                     pWsscfgIsSetFsDot11CapabilityMappingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11CapabilityMappingTable: WsscfgSetAllFsDot11CapabilityMappingTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gWsscfgGlobals.
                           WsscfgGlbMib.FsDot11CapabilityMappingTable,
                           pWsscfgFsDot11CapabilityMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                     (UINT1 *) pWsscfgFsDot11CapabilityMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                     (UINT1 *) pWsscfgOldFsDot11CapabilityMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                     (UINT1 *) pWsscfgTrgFsDot11CapabilityMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityMappingEntry);
                return OSIX_FAILURE;
            }

            if ((pWsscfgSetFsDot11CapabilityMappingEntry->
                 MibObject.i4FsDot11CapabilityMappingRowStatus ==
                 CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsDot11CapabilityMappingEntry->
                        MibObject.i4FsDot11CapabilityMappingRowStatus
                        == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsDot11CapabilityMappingEntry->
                    MibObject.i4IfIndex =
                    pWsscfgSetFsDot11CapabilityMappingEntry->
                    MibObject.i4IfIndex;
                pWsscfgTrgFsDot11CapabilityMappingEntry->
                    MibObject.i4FsDot11CapabilityMappingRowStatus =
                    CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11CapabilityMappingEntry->
                    bFsDot11CapabilityMappingRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsDot11CapabilityMappingTableTrigger
                    (pWsscfgTrgFsDot11CapabilityMappingEntry,
                     pWsscfgTrgIsSetFsDot11CapabilityMappingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11CapabilityMappingTable: WsscfgSetAllFsDot11CapabilityMappingTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                         (UINT1 *) pWsscfgFsDot11CapabilityMappingEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                         (UINT1 *) pWsscfgOldFsDot11CapabilityMappingEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                         (UINT1 *) pWsscfgTrgFsDot11CapabilityMappingEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_ISSET_POOLID,
                         (UINT1 *)
                         pWsscfgTrgIsSetFsDot11CapabilityMappingEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pWsscfgSetFsDot11CapabilityMappingEntry->
                     MibObject.i4FsDot11CapabilityMappingRowStatus
                     == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsDot11CapabilityMappingEntry->
                    MibObject.i4FsDot11CapabilityMappingRowStatus =
                    CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11CapabilityMappingEntry->
                    bFsDot11CapabilityMappingRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsDot11CapabilityMappingTableTrigger
                    (pWsscfgTrgFsDot11CapabilityMappingEntry,
                     pWsscfgTrgIsSetFsDot11CapabilityMappingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11CapabilityMappingTable: WsscfgSetAllFsDot11CapabilityMappingTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                         (UINT1 *) pWsscfgFsDot11CapabilityMappingEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                         (UINT1 *) pWsscfgOldFsDot11CapabilityMappingEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                         (UINT1 *) pWsscfgTrgFsDot11CapabilityMappingEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_ISSET_POOLID,
                         (UINT1 *)
                         pWsscfgTrgIsSetFsDot11CapabilityMappingEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pWsscfgSetFsDot11CapabilityMappingEntry->
                MibObject.i4FsDot11CapabilityMappingRowStatus == CREATE_AND_GO)
            {
                pWsscfgSetFsDot11CapabilityMappingEntry->
                    MibObject.i4FsDot11CapabilityMappingRowStatus = ACTIVE;
            }

            if (WsscfgSetAllFsDot11CapabilityMappingTableTrigger
                (pWsscfgSetFsDot11CapabilityMappingEntry,
                 pWsscfgIsSetFsDot11CapabilityMappingEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11CapabilityMappingTable:  WsscfgSetAllFsDot11CapabilityMappingTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                 (UINT1 *) pWsscfgOldFsDot11CapabilityMappingEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                 (UINT1 *) pWsscfgTrgFsDot11CapabilityMappingEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityMappingEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (WsscfgSetAllFsDot11CapabilityMappingTableTrigger
                (pWsscfgSetFsDot11CapabilityMappingEntry,
                 pWsscfgIsSetFsDot11CapabilityMappingEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11CapabilityMappingTable: WsscfgSetAllFsDot11CapabilityMappingTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11CapabilityMappingTable: Failure.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                 (UINT1 *) pWsscfgOldFsDot11CapabilityMappingEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                 (UINT1 *) pWsscfgTrgFsDot11CapabilityMappingEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityMappingEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pWsscfgSetFsDot11CapabilityMappingEntry->
              MibObject.i4FsDot11CapabilityMappingRowStatus ==
              CREATE_AND_WAIT)
             || (pWsscfgSetFsDot11CapabilityMappingEntry->
                 MibObject.i4FsDot11CapabilityMappingRowStatus ==
                 CREATE_AND_GO))
    {
        if (WsscfgSetAllFsDot11CapabilityMappingTableTrigger
            (pWsscfgSetFsDot11CapabilityMappingEntry,
             pWsscfgIsSetFsDot11CapabilityMappingEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11CapabilityMappingTable: WsscfgSetAllFsDot11CapabilityMappingTableTrigger function returns failure.\r\n"));
        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11CapabilityMappingTable: The row is already present.\r\n"));
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID, (UINT1 *)
             pWsscfgOldFsDot11CapabilityMappingEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID, (UINT1 *)
             pWsscfgTrgFsDot11CapabilityMappingEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_ISSET_POOLID,
             (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityMappingEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pWsscfgOldFsDot11CapabilityMappingEntry,
            pWsscfgFsDot11CapabilityMappingEntry,
            sizeof (tWsscfgFsDot11CapabilityMappingEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pWsscfgSetFsDot11CapabilityMappingEntry->
        MibObject.i4FsDot11CapabilityMappingRowStatus == DESTROY)
    {
        pWsscfgFsDot11CapabilityMappingEntry->
            MibObject.i4FsDot11CapabilityMappingRowStatus = DESTROY;

        if (WsscfgUtilUpdateFsDot11CapabilityMappingTable
            (pWsscfgOldFsDot11CapabilityMappingEntry,
             pWsscfgFsDot11CapabilityMappingEntry,
             pWsscfgIsSetFsDot11CapabilityMappingEntry) != OSIX_SUCCESS)
        {

            if (WsscfgSetAllFsDot11CapabilityMappingTableTrigger
                (pWsscfgSetFsDot11CapabilityMappingEntry,
                 pWsscfgIsSetFsDot11CapabilityMappingEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11CapabilityMappingTable: "
                             "WsscfgSetAllFsDot11CapabilityMappingTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11CapabilityMappingTable:"
                         " WsscfgUtilUpdateFsDot11CapabilityMappingTable function returns failure.\r\n"));
        }
        RBTreeRem (gWsscfgGlobals.
                   WsscfgGlbMib.FsDot11CapabilityMappingTable,
                   pWsscfgFsDot11CapabilityMappingEntry);
        if (WsscfgSetAllFsDot11CapabilityMappingTableTrigger
            (pWsscfgSetFsDot11CapabilityMappingEntry,
             pWsscfgIsSetFsDot11CapabilityMappingEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11CapabilityMappingTable: WsscfgSetAllFsDot11CapabilityMappingTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                 (UINT1 *) pWsscfgOldFsDot11CapabilityMappingEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                 (UINT1 *) pWsscfgTrgFsDot11CapabilityMappingEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityMappingEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
             (UINT1 *) pWsscfgFsDot11CapabilityMappingEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID, (UINT1 *)
             pWsscfgOldFsDot11CapabilityMappingEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID, (UINT1 *)
             pWsscfgTrgFsDot11CapabilityMappingEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_ISSET_POOLID,
             (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityMappingEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsDot11CapabilityMappingTableFilterInputs
        (pWsscfgFsDot11CapabilityMappingEntry,
         pWsscfgSetFsDot11CapabilityMappingEntry,
         pWsscfgIsSetFsDot11CapabilityMappingEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID, (UINT1 *)
             pWsscfgOldFsDot11CapabilityMappingEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID, (UINT1 *)
             pWsscfgTrgFsDot11CapabilityMappingEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_ISSET_POOLID,
             (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityMappingEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pWsscfgFsDot11CapabilityMappingEntry->
         MibObject.i4FsDot11CapabilityMappingRowStatus == ACTIVE)
        && (pWsscfgSetFsDot11CapabilityMappingEntry->
            MibObject.i4FsDot11CapabilityMappingRowStatus != NOT_IN_SERVICE))
    {
        pWsscfgFsDot11CapabilityMappingEntry->
            MibObject.i4FsDot11CapabilityMappingRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pWsscfgTrgFsDot11CapabilityMappingEntry->
            MibObject.i4FsDot11CapabilityMappingRowStatus = NOT_IN_SERVICE;
        pWsscfgTrgIsSetFsDot11CapabilityMappingEntry->
            bFsDot11CapabilityMappingRowStatus = OSIX_TRUE;

        if (WsscfgUtilUpdateFsDot11CapabilityMappingTable
            (pWsscfgOldFsDot11CapabilityMappingEntry,
             pWsscfgFsDot11CapabilityMappingEntry,
             pWsscfgIsSetFsDot11CapabilityMappingEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pWsscfgFsDot11CapabilityMappingEntry,
                    pWsscfgOldFsDot11CapabilityMappingEntry,
                    sizeof (tWsscfgFsDot11CapabilityMappingEntry));
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11CapabilityMappingTable:                 WsscfgUtilUpdateFsDot11CapabilityMappingTable Function returns failure.\r\n"));

            if (WsscfgSetAllFsDot11CapabilityMappingTableTrigger
                (pWsscfgSetFsDot11CapabilityMappingEntry,
                 pWsscfgIsSetFsDot11CapabilityMappingEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11CapabilityMappingTable: WsscfgSetAllFsDot11CapabilityMappingTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                 (UINT1 *) pWsscfgOldFsDot11CapabilityMappingEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                 (UINT1 *) pWsscfgTrgFsDot11CapabilityMappingEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityMappingEntry);
            return OSIX_FAILURE;
        }

        if (WsscfgSetAllFsDot11CapabilityMappingTableTrigger
            (pWsscfgTrgFsDot11CapabilityMappingEntry,
             pWsscfgTrgIsSetFsDot11CapabilityMappingEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11CapabilityMappingTable: WsscfgSetAllFsDot11CapabilityMappingTableTrigger function returns failure.\r\n"));
        }
    }

    if (pWsscfgSetFsDot11CapabilityMappingEntry->
        MibObject.i4FsDot11CapabilityMappingRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pWsscfgIsSetFsDot11CapabilityMappingEntry->
        bFsDot11CapabilityMappingProfileName != OSIX_FALSE)
    {
        MEMCPY (pWsscfgFsDot11CapabilityMappingEntry->
                MibObject.au1FsDot11CapabilityMappingProfileName,
                pWsscfgSetFsDot11CapabilityMappingEntry->
                MibObject.au1FsDot11CapabilityMappingProfileName,
                pWsscfgSetFsDot11CapabilityMappingEntry->
                MibObject.i4FsDot11CapabilityMappingProfileNameLen);

        pWsscfgFsDot11CapabilityMappingEntry->
            MibObject.i4FsDot11CapabilityMappingProfileNameLen =
            pWsscfgSetFsDot11CapabilityMappingEntry->
            MibObject.i4FsDot11CapabilityMappingProfileNameLen;
    }
    if (pWsscfgIsSetFsDot11CapabilityMappingEntry->
        bFsDot11CapabilityMappingRowStatus != OSIX_FALSE)
    {
        pWsscfgFsDot11CapabilityMappingEntry->
            MibObject.i4FsDot11CapabilityMappingRowStatus =
            pWsscfgSetFsDot11CapabilityMappingEntry->
            MibObject.i4FsDot11CapabilityMappingRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pWsscfgFsDot11CapabilityMappingEntry->
            MibObject.i4FsDot11CapabilityMappingRowStatus = ACTIVE;
    }

    if (WsscfgUtilUpdateFsDot11CapabilityMappingTable
        (pWsscfgOldFsDot11CapabilityMappingEntry,
         pWsscfgFsDot11CapabilityMappingEntry,
         pWsscfgIsSetFsDot11CapabilityMappingEntry) != OSIX_SUCCESS)
    {

        if (WsscfgSetAllFsDot11CapabilityMappingTableTrigger
            (pWsscfgSetFsDot11CapabilityMappingEntry,
             pWsscfgIsSetFsDot11CapabilityMappingEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11CapabilityMappingTable: WsscfgSetAllFsDot11CapabilityMappingTableTrigger function returns failure.\r\n"));

        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11CapabilityMappingTable: WsscfgUtilUpdateFsDot11CapabilityMappingTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pWsscfgFsDot11CapabilityMappingEntry,
                pWsscfgOldFsDot11CapabilityMappingEntry,
                sizeof (tWsscfgFsDot11CapabilityMappingEntry));
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID, (UINT1 *)
             pWsscfgOldFsDot11CapabilityMappingEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID, (UINT1 *)
             pWsscfgTrgFsDot11CapabilityMappingEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_ISSET_POOLID,
             (UINT1 *) pWsscfgTrgIsSetFsDot11CapabilityMappingEntry);
        return OSIX_FAILURE;

    }
    if (WsscfgSetAllFsDot11CapabilityMappingTableTrigger
        (pWsscfgSetFsDot11CapabilityMappingEntry,
         pWsscfgIsSetFsDot11CapabilityMappingEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11CapabilityMappingTable: WsscfgSetAllFsDot11CapabilityMappingTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                        (UINT1 *) pWsscfgOldFsDot11CapabilityMappingEntry);
    MemReleaseMemBlock (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID,
                        (UINT1 *) pWsscfgTrgFsDot11CapabilityMappingEntry);
    MemReleaseMemBlock
        (WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_ISSET_POOLID, (UINT1 *)
         pWsscfgTrgIsSetFsDot11CapabilityMappingEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11AuthMappingTable
 Input       :  pWsscfgSetFsDot11AuthMappingEntry
                pWsscfgIsSetFsDot11AuthMappingEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllFsDot11AuthMappingTable (tWsscfgFsDot11AuthMappingEntry *
                                     pWsscfgSetFsDot11AuthMappingEntry,
                                     tWsscfgIsSetFsDot11AuthMappingEntry *
                                     pWsscfgIsSetFsDot11AuthMappingEntry,
                                     INT4 i4RowStatusLogic,
                                     INT4 i4RowCreateOption)
{
    tWsscfgFsDot11AuthMappingEntry *pWsscfgFsDot11AuthMappingEntry = NULL;
    tWsscfgFsDot11AuthMappingEntry *pWsscfgOldFsDot11AuthMappingEntry = NULL;
    tWsscfgFsDot11AuthMappingEntry *pWsscfgTrgFsDot11AuthMappingEntry = NULL;
    tWsscfgIsSetFsDot11AuthMappingEntry *pWsscfgTrgIsSetFsDot11AuthMappingEntry
        = NULL;
    INT4                i4RowMakeActive = FALSE;

    pWsscfgOldFsDot11AuthMappingEntry =
        (tWsscfgFsDot11AuthMappingEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID);
    if (pWsscfgOldFsDot11AuthMappingEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWsscfgTrgFsDot11AuthMappingEntry =
        (tWsscfgFsDot11AuthMappingEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID);
    if (pWsscfgTrgFsDot11AuthMappingEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11AuthMappingEntry);
        return OSIX_FAILURE;
    }
    pWsscfgTrgIsSetFsDot11AuthMappingEntry =
        (tWsscfgIsSetFsDot11AuthMappingEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11AUTHMAPPINGTABLE_ISSET_POOLID);
    if (pWsscfgTrgIsSetFsDot11AuthMappingEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11AuthMappingEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11AuthMappingEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pWsscfgOldFsDot11AuthMappingEntry, 0,
            sizeof (tWsscfgFsDot11AuthMappingEntry));
    MEMSET (pWsscfgTrgFsDot11AuthMappingEntry, 0,
            sizeof (tWsscfgFsDot11AuthMappingEntry));
    MEMSET (pWsscfgTrgIsSetFsDot11AuthMappingEntry, 0,
            sizeof (tWsscfgIsSetFsDot11AuthMappingEntry));

    /* Check whether the node is already present */
    pWsscfgFsDot11AuthMappingEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsDot11AuthMappingTable,
                   (tRBElem *) pWsscfgSetFsDot11AuthMappingEntry);

    if (pWsscfgFsDot11AuthMappingEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pWsscfgSetFsDot11AuthMappingEntry->
             MibObject.i4FsDot11AuthMappingRowStatus ==
             CREATE_AND_WAIT)
            || (pWsscfgSetFsDot11AuthMappingEntry->
                MibObject.i4FsDot11AuthMappingRowStatus ==
                CREATE_AND_GO)
            ||
            ((pWsscfgSetFsDot11AuthMappingEntry->
              MibObject.i4FsDot11AuthMappingRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pWsscfgFsDot11AuthMappingEntry =
                (tWsscfgFsDot11AuthMappingEntry *)
                MemAllocMemBlk (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID);
            if (pWsscfgFsDot11AuthMappingEntry == NULL)
            {
                if (WsscfgSetAllFsDot11AuthMappingTableTrigger
                    (pWsscfgSetFsDot11AuthMappingEntry,
                     pWsscfgIsSetFsDot11AuthMappingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11AuthMappingTable:WsscfgSetAllFsDot11AuthMappingTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11AuthMappingTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID, (UINT1 *)
                     pWsscfgOldFsDot11AuthMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgFsDot11AuthMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHMAPPINGTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11AuthMappingEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pWsscfgFsDot11AuthMappingEntry, 0,
                    sizeof (tWsscfgFsDot11AuthMappingEntry));
            if ((WsscfgInitializeFsDot11AuthMappingTable
                 (pWsscfgFsDot11AuthMappingEntry)) == OSIX_FAILURE)
            {
                if (WsscfgSetAllFsDot11AuthMappingTableTrigger
                    (pWsscfgSetFsDot11AuthMappingEntry,
                     pWsscfgIsSetFsDot11AuthMappingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11AuthMappingTable:WsscfgSetAllFsDot11AuthMappingTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11AuthMappingTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID, (UINT1 *)
                     pWsscfgFsDot11AuthMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID, (UINT1 *)
                     pWsscfgOldFsDot11AuthMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgFsDot11AuthMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHMAPPINGTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11AuthMappingEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pWsscfgIsSetFsDot11AuthMappingEntry->
                bFsDot11AuthMappingProfileName != OSIX_FALSE)
            {
                MEMCPY (pWsscfgFsDot11AuthMappingEntry->
                        MibObject.au1FsDot11AuthMappingProfileName,
                        pWsscfgSetFsDot11AuthMappingEntry->
                        MibObject.au1FsDot11AuthMappingProfileName,
                        pWsscfgSetFsDot11AuthMappingEntry->
                        MibObject.i4FsDot11AuthMappingProfileNameLen);

                pWsscfgFsDot11AuthMappingEntry->
                    MibObject.i4FsDot11AuthMappingProfileNameLen =
                    pWsscfgSetFsDot11AuthMappingEntry->
                    MibObject.i4FsDot11AuthMappingProfileNameLen;
            }

            if (pWsscfgIsSetFsDot11AuthMappingEntry->
                bFsDot11AuthMappingRowStatus != OSIX_FALSE)
            {
                pWsscfgFsDot11AuthMappingEntry->
                    MibObject.i4FsDot11AuthMappingRowStatus =
                    pWsscfgSetFsDot11AuthMappingEntry->
                    MibObject.i4FsDot11AuthMappingRowStatus;
            }

            if (pWsscfgIsSetFsDot11AuthMappingEntry->bIfIndex != OSIX_FALSE)
            {
                pWsscfgFsDot11AuthMappingEntry->MibObject.i4IfIndex =
                    pWsscfgSetFsDot11AuthMappingEntry->MibObject.i4IfIndex;
            }

            if ((pWsscfgSetFsDot11AuthMappingEntry->
                 MibObject.i4FsDot11AuthMappingRowStatus ==
                 CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsDot11AuthMappingEntry->
                        MibObject.i4FsDot11AuthMappingRowStatus == ACTIVE)))
            {
                pWsscfgFsDot11AuthMappingEntry->
                    MibObject.i4FsDot11AuthMappingRowStatus = ACTIVE;
            }
            else if (pWsscfgSetFsDot11AuthMappingEntry->
                     MibObject.i4FsDot11AuthMappingRowStatus == CREATE_AND_WAIT)
            {
                pWsscfgFsDot11AuthMappingEntry->
                    MibObject.i4FsDot11AuthMappingRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gWsscfgGlobals.WsscfgGlbMib.FsDot11AuthMappingTable,
                 (tRBElem *) pWsscfgFsDot11AuthMappingEntry) != RB_SUCCESS)
            {
                if (WsscfgSetAllFsDot11AuthMappingTableTrigger
                    (pWsscfgSetFsDot11AuthMappingEntry,
                     pWsscfgIsSetFsDot11AuthMappingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11AuthMappingTable: WsscfgSetAllFsDot11AuthMappingTableTrigger function returns failure.\r\n"));
                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11AuthMappingTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID, (UINT1 *)
                     pWsscfgFsDot11AuthMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID, (UINT1 *)
                     pWsscfgOldFsDot11AuthMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgFsDot11AuthMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHMAPPINGTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11AuthMappingEntry);
                return OSIX_FAILURE;
            }
            if (WsscfgUtilUpdateFsDot11AuthMappingTable
                (NULL, pWsscfgFsDot11AuthMappingEntry,
                 pWsscfgIsSetFsDot11AuthMappingEntry) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11AuthMappingTable: WsscfgUtilUpdateFsDot11AuthMappingTable function returns failure.\r\n"));

                if (WsscfgSetAllFsDot11AuthMappingTableTrigger
                    (pWsscfgSetFsDot11AuthMappingEntry,
                     pWsscfgIsSetFsDot11AuthMappingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11AuthMappingTable: WsscfgSetAllFsDot11AuthMappingTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gWsscfgGlobals.
                           WsscfgGlbMib.FsDot11AuthMappingTable,
                           pWsscfgFsDot11AuthMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID, (UINT1 *)
                     pWsscfgFsDot11AuthMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID, (UINT1 *)
                     pWsscfgOldFsDot11AuthMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgFsDot11AuthMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11AUTHMAPPINGTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11AuthMappingEntry);
                return OSIX_FAILURE;
            }

            if ((pWsscfgSetFsDot11AuthMappingEntry->
                 MibObject.i4FsDot11AuthMappingRowStatus ==
                 CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsDot11AuthMappingEntry->
                        MibObject.i4FsDot11AuthMappingRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsDot11AuthMappingEntry->MibObject.
                    i4IfIndex =
                    pWsscfgSetFsDot11AuthMappingEntry->MibObject.i4IfIndex;
                pWsscfgTrgFsDot11AuthMappingEntry->
                    MibObject.i4FsDot11AuthMappingRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11AuthMappingEntry->
                    bFsDot11AuthMappingRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsDot11AuthMappingTableTrigger
                    (pWsscfgTrgFsDot11AuthMappingEntry,
                     pWsscfgTrgIsSetFsDot11AuthMappingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11AuthMappingTable: WsscfgSetAllFsDot11AuthMappingTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                         (UINT1 *) pWsscfgFsDot11AuthMappingEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                         (UINT1 *) pWsscfgOldFsDot11AuthMappingEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                         (UINT1 *) pWsscfgTrgFsDot11AuthMappingEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11AUTHMAPPINGTABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetFsDot11AuthMappingEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pWsscfgSetFsDot11AuthMappingEntry->
                     MibObject.i4FsDot11AuthMappingRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsDot11AuthMappingEntry->
                    MibObject.i4FsDot11AuthMappingRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11AuthMappingEntry->
                    bFsDot11AuthMappingRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsDot11AuthMappingTableTrigger
                    (pWsscfgTrgFsDot11AuthMappingEntry,
                     pWsscfgTrgIsSetFsDot11AuthMappingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11AuthMappingTable: WsscfgSetAllFsDot11AuthMappingTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                         (UINT1 *) pWsscfgFsDot11AuthMappingEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                         (UINT1 *) pWsscfgOldFsDot11AuthMappingEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                         (UINT1 *) pWsscfgTrgFsDot11AuthMappingEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11AUTHMAPPINGTABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetFsDot11AuthMappingEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pWsscfgSetFsDot11AuthMappingEntry->
                MibObject.i4FsDot11AuthMappingRowStatus == CREATE_AND_GO)
            {
                pWsscfgSetFsDot11AuthMappingEntry->
                    MibObject.i4FsDot11AuthMappingRowStatus = ACTIVE;
            }

            if (WsscfgSetAllFsDot11AuthMappingTableTrigger
                (pWsscfgSetFsDot11AuthMappingEntry,
                 pWsscfgIsSetFsDot11AuthMappingEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11AuthMappingTable:  WsscfgSetAllFsDot11AuthMappingTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11AuthMappingEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11AuthMappingEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11AUTHMAPPINGTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11AuthMappingEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (WsscfgSetAllFsDot11AuthMappingTableTrigger
                (pWsscfgSetFsDot11AuthMappingEntry,
                 pWsscfgIsSetFsDot11AuthMappingEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11AuthMappingTable: WsscfgSetAllFsDot11AuthMappingTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11AuthMappingTable: Failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11AuthMappingEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11AuthMappingEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11AUTHMAPPINGTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11AuthMappingEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pWsscfgSetFsDot11AuthMappingEntry->
              MibObject.i4FsDot11AuthMappingRowStatus ==
              CREATE_AND_WAIT)
             || (pWsscfgSetFsDot11AuthMappingEntry->
                 MibObject.i4FsDot11AuthMappingRowStatus == CREATE_AND_GO))
    {
        if (WsscfgSetAllFsDot11AuthMappingTableTrigger
            (pWsscfgSetFsDot11AuthMappingEntry,
             pWsscfgIsSetFsDot11AuthMappingEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11AuthMappingTable: WsscfgSetAllFsDot11AuthMappingTableTrigger function returns failure.\r\n"));
        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11AuthMappingTable: The row is already present.\r\n"));
        MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11AuthMappingEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11AuthMappingEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11AUTHMAPPINGTABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetFsDot11AuthMappingEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pWsscfgOldFsDot11AuthMappingEntry,
            pWsscfgFsDot11AuthMappingEntry,
            sizeof (tWsscfgFsDot11AuthMappingEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pWsscfgSetFsDot11AuthMappingEntry->
        MibObject.i4FsDot11AuthMappingRowStatus == DESTROY)
    {
        pWsscfgFsDot11AuthMappingEntry->
            MibObject.i4FsDot11AuthMappingRowStatus = DESTROY;

        if (WsscfgUtilUpdateFsDot11AuthMappingTable
            (pWsscfgOldFsDot11AuthMappingEntry,
             pWsscfgFsDot11AuthMappingEntry,
             pWsscfgIsSetFsDot11AuthMappingEntry) != OSIX_SUCCESS)
        {

            if (WsscfgSetAllFsDot11AuthMappingTableTrigger
                (pWsscfgSetFsDot11AuthMappingEntry,
                 pWsscfgIsSetFsDot11AuthMappingEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11AuthMappingTable: WsscfgSetAllFsDot11AuthMappingTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11AuthMappingTable: WsscfgUtilUpdateFsDot11AuthMappingTable function returns failure.\r\n"));
        }
        RBTreeRem (gWsscfgGlobals.WsscfgGlbMib.FsDot11AuthMappingTable,
                   pWsscfgFsDot11AuthMappingEntry);
        if (WsscfgSetAllFsDot11AuthMappingTableTrigger
            (pWsscfgSetFsDot11AuthMappingEntry,
             pWsscfgIsSetFsDot11AuthMappingEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11AuthMappingTable: WsscfgSetAllFsDot11AuthMappingTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11AuthMappingEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11AuthMappingEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11AUTHMAPPINGTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11AuthMappingEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgFsDot11AuthMappingEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11AuthMappingEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11AuthMappingEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11AUTHMAPPINGTABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetFsDot11AuthMappingEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsDot11AuthMappingTableFilterInputs
        (pWsscfgFsDot11AuthMappingEntry,
         pWsscfgSetFsDot11AuthMappingEntry,
         pWsscfgIsSetFsDot11AuthMappingEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11AuthMappingEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11AuthMappingEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11AUTHMAPPINGTABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetFsDot11AuthMappingEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pWsscfgFsDot11AuthMappingEntry->
         MibObject.i4FsDot11AuthMappingRowStatus == ACTIVE)
        && (pWsscfgSetFsDot11AuthMappingEntry->
            MibObject.i4FsDot11AuthMappingRowStatus != NOT_IN_SERVICE))
    {
        pWsscfgFsDot11AuthMappingEntry->
            MibObject.i4FsDot11AuthMappingRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pWsscfgTrgFsDot11AuthMappingEntry->
            MibObject.i4FsDot11AuthMappingRowStatus = NOT_IN_SERVICE;
        pWsscfgTrgIsSetFsDot11AuthMappingEntry->bFsDot11AuthMappingRowStatus
            = OSIX_TRUE;

        if (WsscfgUtilUpdateFsDot11AuthMappingTable
            (pWsscfgOldFsDot11AuthMappingEntry,
             pWsscfgFsDot11AuthMappingEntry,
             pWsscfgIsSetFsDot11AuthMappingEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pWsscfgFsDot11AuthMappingEntry,
                    pWsscfgOldFsDot11AuthMappingEntry,
                    sizeof (tWsscfgFsDot11AuthMappingEntry));
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11AuthMappingTable:                 WsscfgUtilUpdateFsDot11AuthMappingTable Function returns failure.\r\n"));

            if (WsscfgSetAllFsDot11AuthMappingTableTrigger
                (pWsscfgSetFsDot11AuthMappingEntry,
                 pWsscfgIsSetFsDot11AuthMappingEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11AuthMappingTable: WsscfgSetAllFsDot11AuthMappingTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11AuthMappingEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11AuthMappingEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11AUTHMAPPINGTABLE_ISSET_POOLID,
                 (UINT1 *) pWsscfgTrgIsSetFsDot11AuthMappingEntry);
            return OSIX_FAILURE;
        }

        if (WsscfgSetAllFsDot11AuthMappingTableTrigger
            (pWsscfgTrgFsDot11AuthMappingEntry,
             pWsscfgTrgIsSetFsDot11AuthMappingEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11AuthMappingTable: WsscfgSetAllFsDot11AuthMappingTableTrigger function returns failure.\r\n"));
        }
    }

    if (pWsscfgSetFsDot11AuthMappingEntry->
        MibObject.i4FsDot11AuthMappingRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pWsscfgIsSetFsDot11AuthMappingEntry->bFsDot11AuthMappingProfileName !=
        OSIX_FALSE)
    {
        MEMCPY (pWsscfgFsDot11AuthMappingEntry->
                MibObject.au1FsDot11AuthMappingProfileName,
                pWsscfgSetFsDot11AuthMappingEntry->
                MibObject.au1FsDot11AuthMappingProfileName,
                pWsscfgSetFsDot11AuthMappingEntry->
                MibObject.i4FsDot11AuthMappingProfileNameLen);

        pWsscfgFsDot11AuthMappingEntry->
            MibObject.i4FsDot11AuthMappingProfileNameLen =
            pWsscfgSetFsDot11AuthMappingEntry->
            MibObject.i4FsDot11AuthMappingProfileNameLen;
    }
    if (pWsscfgIsSetFsDot11AuthMappingEntry->bFsDot11AuthMappingRowStatus !=
        OSIX_FALSE)
    {
        pWsscfgFsDot11AuthMappingEntry->
            MibObject.i4FsDot11AuthMappingRowStatus =
            pWsscfgSetFsDot11AuthMappingEntry->
            MibObject.i4FsDot11AuthMappingRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pWsscfgFsDot11AuthMappingEntry->
            MibObject.i4FsDot11AuthMappingRowStatus = ACTIVE;
    }

    if (WsscfgUtilUpdateFsDot11AuthMappingTable
        (pWsscfgOldFsDot11AuthMappingEntry,
         pWsscfgFsDot11AuthMappingEntry,
         pWsscfgIsSetFsDot11AuthMappingEntry) != OSIX_SUCCESS)
    {

        if (WsscfgSetAllFsDot11AuthMappingTableTrigger
            (pWsscfgSetFsDot11AuthMappingEntry,
             pWsscfgIsSetFsDot11AuthMappingEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11AuthMappingTable: WsscfgSetAllFsDot11AuthMappingTableTrigger function returns failure.\r\n"));

        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11AuthMappingTable: WsscfgUtilUpdateFsDot11AuthMappingTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pWsscfgFsDot11AuthMappingEntry,
                pWsscfgOldFsDot11AuthMappingEntry,
                sizeof (tWsscfgFsDot11AuthMappingEntry));
        MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11AuthMappingEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11AuthMappingEntry);
        MemReleaseMemBlock
            (WSSCFG_FSDOT11AUTHMAPPINGTABLE_ISSET_POOLID, (UINT1 *)
             pWsscfgTrgIsSetFsDot11AuthMappingEntry);
        return OSIX_FAILURE;

    }
    if (WsscfgSetAllFsDot11AuthMappingTableTrigger
        (pWsscfgSetFsDot11AuthMappingEntry,
         pWsscfgIsSetFsDot11AuthMappingEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11AuthMappingTable: WsscfgSetAllFsDot11AuthMappingTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                        (UINT1 *) pWsscfgOldFsDot11AuthMappingEntry);
    MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID,
                        (UINT1 *) pWsscfgTrgFsDot11AuthMappingEntry);
    MemReleaseMemBlock (WSSCFG_FSDOT11AUTHMAPPINGTABLE_ISSET_POOLID,
                        (UINT1 *) pWsscfgTrgIsSetFsDot11AuthMappingEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11QosMappingTable
 Input       :  pWsscfgSetFsDot11QosMappingEntry
                pWsscfgIsSetFsDot11QosMappingEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllFsDot11QosMappingTable (tWsscfgFsDot11QosMappingEntry *
                                    pWsscfgSetFsDot11QosMappingEntry,
                                    tWsscfgIsSetFsDot11QosMappingEntry *
                                    pWsscfgIsSetFsDot11QosMappingEntry,
                                    INT4 i4RowStatusLogic,
                                    INT4 i4RowCreateOption)
{
    tWsscfgFsDot11QosMappingEntry *pWsscfgFsDot11QosMappingEntry = NULL;
    tWsscfgFsDot11QosMappingEntry *pWsscfgOldFsDot11QosMappingEntry = NULL;
    tWsscfgFsDot11QosMappingEntry *pWsscfgTrgFsDot11QosMappingEntry = NULL;
    tWsscfgIsSetFsDot11QosMappingEntry *pWsscfgTrgIsSetFsDot11QosMappingEntry =
        NULL;
    INT4                i4RowMakeActive = FALSE;

    pWsscfgOldFsDot11QosMappingEntry =
        (tWsscfgFsDot11QosMappingEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID);
    if (pWsscfgOldFsDot11QosMappingEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWsscfgTrgFsDot11QosMappingEntry =
        (tWsscfgFsDot11QosMappingEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID);
    if (pWsscfgTrgFsDot11QosMappingEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11QosMappingEntry);
        return OSIX_FAILURE;
    }
    pWsscfgTrgIsSetFsDot11QosMappingEntry =
        (tWsscfgIsSetFsDot11QosMappingEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11QOSMAPPINGTABLE_ISSET_POOLID);
    if (pWsscfgTrgIsSetFsDot11QosMappingEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11QosMappingEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11QosMappingEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pWsscfgOldFsDot11QosMappingEntry, 0,
            sizeof (tWsscfgFsDot11QosMappingEntry));
    MEMSET (pWsscfgTrgFsDot11QosMappingEntry, 0,
            sizeof (tWsscfgFsDot11QosMappingEntry));
    MEMSET (pWsscfgTrgIsSetFsDot11QosMappingEntry, 0,
            sizeof (tWsscfgIsSetFsDot11QosMappingEntry));

    /* Check whether the node is already present */
    pWsscfgFsDot11QosMappingEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsDot11QosMappingTable,
                   (tRBElem *) pWsscfgSetFsDot11QosMappingEntry);

    if (pWsscfgFsDot11QosMappingEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pWsscfgSetFsDot11QosMappingEntry->
             MibObject.i4FsDot11QosMappingRowStatus ==
             CREATE_AND_WAIT)
            || (pWsscfgSetFsDot11QosMappingEntry->
                MibObject.i4FsDot11QosMappingRowStatus ==
                CREATE_AND_GO)
            ||
            ((pWsscfgSetFsDot11QosMappingEntry->
              MibObject.i4FsDot11QosMappingRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pWsscfgFsDot11QosMappingEntry =
                (tWsscfgFsDot11QosMappingEntry *)
                MemAllocMemBlk (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID);
            if (pWsscfgFsDot11QosMappingEntry == NULL)
            {
                if (WsscfgSetAllFsDot11QosMappingTableTrigger
                    (pWsscfgSetFsDot11QosMappingEntry,
                     pWsscfgIsSetFsDot11QosMappingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11QosMappingTable:WsscfgSetAllFsDot11QosMappingTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11QosMappingTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID, (UINT1 *)
                     pWsscfgOldFsDot11QosMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgFsDot11QosMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSMAPPINGTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11QosMappingEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pWsscfgFsDot11QosMappingEntry, 0,
                    sizeof (tWsscfgFsDot11QosMappingEntry));
            if ((WsscfgInitializeFsDot11QosMappingTable
                 (pWsscfgFsDot11QosMappingEntry)) == OSIX_FAILURE)
            {
                if (WsscfgSetAllFsDot11QosMappingTableTrigger
                    (pWsscfgSetFsDot11QosMappingEntry,
                     pWsscfgIsSetFsDot11QosMappingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11QosMappingTable:WsscfgSetAllFsDot11QosMappingTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11QosMappingTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID, (UINT1 *)
                     pWsscfgFsDot11QosMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID, (UINT1 *)
                     pWsscfgOldFsDot11QosMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgFsDot11QosMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSMAPPINGTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11QosMappingEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pWsscfgIsSetFsDot11QosMappingEntry->
                bFsDot11QosMappingProfileName != OSIX_FALSE)
            {
                MEMCPY (pWsscfgFsDot11QosMappingEntry->
                        MibObject.au1FsDot11QosMappingProfileName,
                        pWsscfgSetFsDot11QosMappingEntry->
                        MibObject.au1FsDot11QosMappingProfileName,
                        pWsscfgSetFsDot11QosMappingEntry->
                        MibObject.i4FsDot11QosMappingProfileNameLen);

                pWsscfgFsDot11QosMappingEntry->
                    MibObject.i4FsDot11QosMappingProfileNameLen =
                    pWsscfgSetFsDot11QosMappingEntry->
                    MibObject.i4FsDot11QosMappingProfileNameLen;
            }

            if (pWsscfgIsSetFsDot11QosMappingEntry->
                bFsDot11QosMappingRowStatus != OSIX_FALSE)
            {
                pWsscfgFsDot11QosMappingEntry->
                    MibObject.i4FsDot11QosMappingRowStatus =
                    pWsscfgSetFsDot11QosMappingEntry->
                    MibObject.i4FsDot11QosMappingRowStatus;
            }

            if (pWsscfgIsSetFsDot11QosMappingEntry->bIfIndex != OSIX_FALSE)
            {
                pWsscfgFsDot11QosMappingEntry->MibObject.i4IfIndex =
                    pWsscfgSetFsDot11QosMappingEntry->MibObject.i4IfIndex;
            }

            if ((pWsscfgSetFsDot11QosMappingEntry->
                 MibObject.i4FsDot11QosMappingRowStatus ==
                 CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsDot11QosMappingEntry->
                        MibObject.i4FsDot11QosMappingRowStatus == ACTIVE)))
            {
                pWsscfgFsDot11QosMappingEntry->
                    MibObject.i4FsDot11QosMappingRowStatus = ACTIVE;
            }
            else if (pWsscfgSetFsDot11QosMappingEntry->
                     MibObject.i4FsDot11QosMappingRowStatus == CREATE_AND_WAIT)
            {
                pWsscfgFsDot11QosMappingEntry->
                    MibObject.i4FsDot11QosMappingRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gWsscfgGlobals.WsscfgGlbMib.FsDot11QosMappingTable,
                 (tRBElem *) pWsscfgFsDot11QosMappingEntry) != RB_SUCCESS)
            {
                if (WsscfgSetAllFsDot11QosMappingTableTrigger
                    (pWsscfgSetFsDot11QosMappingEntry,
                     pWsscfgIsSetFsDot11QosMappingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11QosMappingTable: WsscfgSetAllFsDot11QosMappingTableTrigger function returns failure.\r\n"));
                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11QosMappingTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID, (UINT1 *)
                     pWsscfgFsDot11QosMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID, (UINT1 *)
                     pWsscfgOldFsDot11QosMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgFsDot11QosMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSMAPPINGTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11QosMappingEntry);
                return OSIX_FAILURE;
            }
            if (WsscfgUtilUpdateFsDot11QosMappingTable
                (NULL, pWsscfgFsDot11QosMappingEntry,
                 pWsscfgIsSetFsDot11QosMappingEntry) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11QosMappingTable: WsscfgUtilUpdateFsDot11QosMappingTable function returns failure.\r\n"));

                if (WsscfgSetAllFsDot11QosMappingTableTrigger
                    (pWsscfgSetFsDot11QosMappingEntry,
                     pWsscfgIsSetFsDot11QosMappingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11QosMappingTable: WsscfgSetAllFsDot11QosMappingTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gWsscfgGlobals.
                           WsscfgGlbMib.FsDot11QosMappingTable,
                           pWsscfgFsDot11QosMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID, (UINT1 *)
                     pWsscfgFsDot11QosMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID, (UINT1 *)
                     pWsscfgOldFsDot11QosMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID, (UINT1 *)
                     pWsscfgTrgFsDot11QosMappingEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11QOSMAPPINGTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11QosMappingEntry);
                return OSIX_FAILURE;
            }

            if ((pWsscfgSetFsDot11QosMappingEntry->
                 MibObject.i4FsDot11QosMappingRowStatus ==
                 CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsDot11QosMappingEntry->
                        MibObject.i4FsDot11QosMappingRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsDot11QosMappingEntry->MibObject.
                    i4IfIndex =
                    pWsscfgSetFsDot11QosMappingEntry->MibObject.i4IfIndex;
                pWsscfgTrgFsDot11QosMappingEntry->
                    MibObject.i4FsDot11QosMappingRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11QosMappingEntry->
                    bFsDot11QosMappingRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsDot11QosMappingTableTrigger
                    (pWsscfgTrgFsDot11QosMappingEntry,
                     pWsscfgTrgIsSetFsDot11QosMappingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11QosMappingTable: WsscfgSetAllFsDot11QosMappingTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                         (UINT1 *) pWsscfgFsDot11QosMappingEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                         (UINT1 *) pWsscfgOldFsDot11QosMappingEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                         (UINT1 *) pWsscfgTrgFsDot11QosMappingEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11QOSMAPPINGTABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetFsDot11QosMappingEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pWsscfgSetFsDot11QosMappingEntry->
                     MibObject.i4FsDot11QosMappingRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsDot11QosMappingEntry->
                    MibObject.i4FsDot11QosMappingRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11QosMappingEntry->
                    bFsDot11QosMappingRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsDot11QosMappingTableTrigger
                    (pWsscfgTrgFsDot11QosMappingEntry,
                     pWsscfgTrgIsSetFsDot11QosMappingEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11QosMappingTable: WsscfgSetAllFsDot11QosMappingTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                         (UINT1 *) pWsscfgFsDot11QosMappingEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                         (UINT1 *) pWsscfgOldFsDot11QosMappingEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                         (UINT1 *) pWsscfgTrgFsDot11QosMappingEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11QOSMAPPINGTABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetFsDot11QosMappingEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pWsscfgSetFsDot11QosMappingEntry->
                MibObject.i4FsDot11QosMappingRowStatus == CREATE_AND_GO)
            {
                pWsscfgSetFsDot11QosMappingEntry->
                    MibObject.i4FsDot11QosMappingRowStatus = ACTIVE;
            }

            if (WsscfgSetAllFsDot11QosMappingTableTrigger
                (pWsscfgSetFsDot11QosMappingEntry,
                 pWsscfgIsSetFsDot11QosMappingEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11QosMappingTable:  WsscfgSetAllFsDot11QosMappingTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11QosMappingEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11QosMappingEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11QOSMAPPINGTABLE_ISSET_POOLID, (UINT1 *)
                 pWsscfgTrgIsSetFsDot11QosMappingEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (WsscfgSetAllFsDot11QosMappingTableTrigger
                (pWsscfgSetFsDot11QosMappingEntry,
                 pWsscfgIsSetFsDot11QosMappingEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11QosMappingTable: WsscfgSetAllFsDot11QosMappingTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11QosMappingTable: Failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11QosMappingEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11QosMappingEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11QOSMAPPINGTABLE_ISSET_POOLID, (UINT1 *)
                 pWsscfgTrgIsSetFsDot11QosMappingEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pWsscfgSetFsDot11QosMappingEntry->
              MibObject.i4FsDot11QosMappingRowStatus ==
              CREATE_AND_WAIT)
             || (pWsscfgSetFsDot11QosMappingEntry->
                 MibObject.i4FsDot11QosMappingRowStatus == CREATE_AND_GO))
    {
        if (WsscfgSetAllFsDot11QosMappingTableTrigger
            (pWsscfgSetFsDot11QosMappingEntry,
             pWsscfgIsSetFsDot11QosMappingEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11QosMappingTable: WsscfgSetAllFsDot11QosMappingTableTrigger function returns failure.\r\n"));
        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11QosMappingTable: The row is already present.\r\n"));
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11QosMappingEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11QosMappingEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsDot11QosMappingEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pWsscfgOldFsDot11QosMappingEntry,
            pWsscfgFsDot11QosMappingEntry,
            sizeof (tWsscfgFsDot11QosMappingEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pWsscfgSetFsDot11QosMappingEntry->
        MibObject.i4FsDot11QosMappingRowStatus == DESTROY)
    {
        pWsscfgFsDot11QosMappingEntry->
            MibObject.i4FsDot11QosMappingRowStatus = DESTROY;

        if (WsscfgUtilUpdateFsDot11QosMappingTable
            (pWsscfgOldFsDot11QosMappingEntry,
             pWsscfgFsDot11QosMappingEntry,
             pWsscfgIsSetFsDot11QosMappingEntry) != OSIX_SUCCESS)
        {

            if (WsscfgSetAllFsDot11QosMappingTableTrigger
                (pWsscfgSetFsDot11QosMappingEntry,
                 pWsscfgIsSetFsDot11QosMappingEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11QosMappingTable: WsscfgSetAllFsDot11QosMappingTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11QosMappingTable: WsscfgUtilUpdateFsDot11QosMappingTable function returns failure.\r\n"));
        }
        RBTreeRem (gWsscfgGlobals.WsscfgGlbMib.FsDot11QosMappingTable,
                   pWsscfgFsDot11QosMappingEntry);
        if (WsscfgSetAllFsDot11QosMappingTableTrigger
            (pWsscfgSetFsDot11QosMappingEntry,
             pWsscfgIsSetFsDot11QosMappingEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11QosMappingTable: WsscfgSetAllFsDot11QosMappingTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11QosMappingEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11QosMappingEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11QOSMAPPINGTABLE_ISSET_POOLID, (UINT1 *)
                 pWsscfgTrgIsSetFsDot11QosMappingEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgFsDot11QosMappingEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11QosMappingEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11QosMappingEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsDot11QosMappingEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsDot11QosMappingTableFilterInputs
        (pWsscfgFsDot11QosMappingEntry,
         pWsscfgSetFsDot11QosMappingEntry,
         pWsscfgIsSetFsDot11QosMappingEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11QosMappingEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11QosMappingEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsDot11QosMappingEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pWsscfgFsDot11QosMappingEntry->
         MibObject.i4FsDot11QosMappingRowStatus == ACTIVE)
        && (pWsscfgSetFsDot11QosMappingEntry->
            MibObject.i4FsDot11QosMappingRowStatus != NOT_IN_SERVICE))
    {
        pWsscfgFsDot11QosMappingEntry->
            MibObject.i4FsDot11QosMappingRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pWsscfgTrgFsDot11QosMappingEntry->
            MibObject.i4FsDot11QosMappingRowStatus = NOT_IN_SERVICE;
        pWsscfgTrgIsSetFsDot11QosMappingEntry->bFsDot11QosMappingRowStatus
            = OSIX_TRUE;

        if (WsscfgUtilUpdateFsDot11QosMappingTable
            (pWsscfgOldFsDot11QosMappingEntry,
             pWsscfgFsDot11QosMappingEntry,
             pWsscfgIsSetFsDot11QosMappingEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pWsscfgFsDot11QosMappingEntry,
                    pWsscfgOldFsDot11QosMappingEntry,
                    sizeof (tWsscfgFsDot11QosMappingEntry));
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11QosMappingTable:                 WsscfgUtilUpdateFsDot11QosMappingTable Function returns failure.\r\n"));

            if (WsscfgSetAllFsDot11QosMappingTableTrigger
                (pWsscfgSetFsDot11QosMappingEntry,
                 pWsscfgIsSetFsDot11QosMappingEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11QosMappingTable: WsscfgSetAllFsDot11QosMappingTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11QosMappingEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11QosMappingEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11QOSMAPPINGTABLE_ISSET_POOLID, (UINT1 *)
                 pWsscfgTrgIsSetFsDot11QosMappingEntry);
            return OSIX_FAILURE;
        }

        if (WsscfgSetAllFsDot11QosMappingTableTrigger
            (pWsscfgTrgFsDot11QosMappingEntry,
             pWsscfgTrgIsSetFsDot11QosMappingEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11QosMappingTable: WsscfgSetAllFsDot11QosMappingTableTrigger function returns failure.\r\n"));
        }
    }

    if (pWsscfgSetFsDot11QosMappingEntry->
        MibObject.i4FsDot11QosMappingRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pWsscfgIsSetFsDot11QosMappingEntry->bFsDot11QosMappingProfileName !=
        OSIX_FALSE)
    {
        MEMCPY (pWsscfgFsDot11QosMappingEntry->
                MibObject.au1FsDot11QosMappingProfileName,
                pWsscfgSetFsDot11QosMappingEntry->
                MibObject.au1FsDot11QosMappingProfileName,
                pWsscfgSetFsDot11QosMappingEntry->
                MibObject.i4FsDot11QosMappingProfileNameLen);

        pWsscfgFsDot11QosMappingEntry->
            MibObject.i4FsDot11QosMappingProfileNameLen =
            pWsscfgSetFsDot11QosMappingEntry->
            MibObject.i4FsDot11QosMappingProfileNameLen;
    }
    if (pWsscfgIsSetFsDot11QosMappingEntry->
        bFsDot11QosMappingRowStatus != OSIX_FALSE)
    {
        pWsscfgFsDot11QosMappingEntry->
            MibObject.i4FsDot11QosMappingRowStatus =
            pWsscfgSetFsDot11QosMappingEntry->
            MibObject.i4FsDot11QosMappingRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pWsscfgFsDot11QosMappingEntry->
            MibObject.i4FsDot11QosMappingRowStatus = ACTIVE;
    }

    if (WsscfgUtilUpdateFsDot11QosMappingTable
        (pWsscfgOldFsDot11QosMappingEntry,
         pWsscfgFsDot11QosMappingEntry,
         pWsscfgIsSetFsDot11QosMappingEntry) != OSIX_SUCCESS)
    {

        if (WsscfgSetAllFsDot11QosMappingTableTrigger
            (pWsscfgSetFsDot11QosMappingEntry,
             pWsscfgIsSetFsDot11QosMappingEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11QosMappingTable: WsscfgSetAllFsDot11QosMappingTableTrigger function returns failure.\r\n"));

        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11QosMappingTable: WsscfgUtilUpdateFsDot11QosMappingTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pWsscfgFsDot11QosMappingEntry,
                pWsscfgOldFsDot11QosMappingEntry,
                sizeof (tWsscfgFsDot11QosMappingEntry));
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11QosMappingEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11QosMappingEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsDot11QosMappingEntry);
        return OSIX_FAILURE;

    }
    if (WsscfgSetAllFsDot11QosMappingTableTrigger
        (pWsscfgSetFsDot11QosMappingEntry,
         pWsscfgIsSetFsDot11QosMappingEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11QosMappingTable: WsscfgSetAllFsDot11QosMappingTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                        (UINT1 *) pWsscfgOldFsDot11QosMappingEntry);
    MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID,
                        (UINT1 *) pWsscfgTrgFsDot11QosMappingEntry);
    MemReleaseMemBlock (WSSCFG_FSDOT11QOSMAPPINGTABLE_ISSET_POOLID,
                        (UINT1 *) pWsscfgTrgIsSetFsDot11QosMappingEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11AntennasListTable
 Input       :  pWsscfgSetFsDot11AntennasListEntry
                pWsscfgIsSetFsDot11AntennasListEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllFsDot11AntennasListTable (tWsscfgFsDot11AntennasListEntry *
                                      pWsscfgSetFsDot11AntennasListEntry,
                                      tWsscfgIsSetFsDot11AntennasListEntry *
                                      pWsscfgIsSetFsDot11AntennasListEntry)
{
    tWsscfgFsDot11AntennasListEntry WsscfgOldFsDot11AntennasListEntry;

    MEMSET (&WsscfgOldFsDot11AntennasListEntry, 0,
            sizeof (tWsscfgFsDot11AntennasListEntry));
    if (WsscfgUtilUpdateFsDot11AntennasListTable
        (&WsscfgOldFsDot11AntennasListEntry,
         pWsscfgSetFsDot11AntennasListEntry,
         pWsscfgIsSetFsDot11AntennasListEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    pWsscfgIsSetFsDot11AntennasListEntry->bFsAntennaMode = OSIX_TRUE;
    pWsscfgIsSetFsDot11AntennasListEntry->bFsAntennaSelection = OSIX_TRUE;
    pWsscfgIsSetFsDot11AntennasListEntry->bIfIndex = OSIX_TRUE;
    pWsscfgIsSetFsDot11AntennasListEntry->bDot11AntennaListIndex = OSIX_TRUE;
    WsscfgSetAllFsDot11AntennasListTableTrigger
        (pWsscfgSetFsDot11AntennasListEntry,
         pWsscfgIsSetFsDot11AntennasListEntry, SNMP_FAILURE);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11WlanTable
 Input       :  pWsscfgSetFsDot11WlanEntry
                pWsscfgIsSetFsDot11WlanEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllFsDot11WlanTable (tWsscfgFsDot11WlanEntry *
                              pWsscfgSetFsDot11WlanEntry,
                              tWsscfgIsSetFsDot11WlanEntry *
                              pWsscfgIsSetFsDot11WlanEntry,
                              INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tWsscfgFsDot11WlanEntry *pWsscfgFsDot11WlanEntry = NULL;
    tWsscfgFsDot11WlanEntry *pWsscfgOldFsDot11WlanEntry = NULL;
    tWsscfgFsDot11WlanEntry *pWsscfgTrgFsDot11WlanEntry = NULL;
    tWsscfgIsSetFsDot11WlanEntry *pWsscfgTrgIsSetFsDot11WlanEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pWsscfgOldFsDot11WlanEntry = (tWsscfgFsDot11WlanEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11WLANTABLE_POOLID);
    if (pWsscfgOldFsDot11WlanEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWsscfgTrgFsDot11WlanEntry = (tWsscfgFsDot11WlanEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11WLANTABLE_POOLID);
    if (pWsscfgTrgFsDot11WlanEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11WlanEntry);
        return OSIX_FAILURE;
    }
    pWsscfgTrgIsSetFsDot11WlanEntry = (tWsscfgIsSetFsDot11WlanEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11WLANTABLE_ISSET_POOLID);
    if (pWsscfgTrgIsSetFsDot11WlanEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11WlanEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11WlanEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pWsscfgOldFsDot11WlanEntry, 0, sizeof (tWsscfgFsDot11WlanEntry));
    MEMSET (pWsscfgTrgFsDot11WlanEntry, 0, sizeof (tWsscfgFsDot11WlanEntry));
    MEMSET (pWsscfgTrgIsSetFsDot11WlanEntry, 0,
            sizeof (tWsscfgIsSetFsDot11WlanEntry));

    /* Check whether the node is already present */
    pWsscfgFsDot11WlanEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsDot11WlanTable,
                   (tRBElem *) pWsscfgSetFsDot11WlanEntry);

    if (pWsscfgFsDot11WlanEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pWsscfgSetFsDot11WlanEntry->
             MibObject.i4FsDot11WlanRowStatus == CREATE_AND_WAIT)
            || (pWsscfgSetFsDot11WlanEntry->
                MibObject.i4FsDot11WlanRowStatus == CREATE_AND_GO)
            ||
            ((pWsscfgSetFsDot11WlanEntry->
              MibObject.i4FsDot11WlanRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pWsscfgFsDot11WlanEntry = (tWsscfgFsDot11WlanEntry *)
                MemAllocMemBlk (WSSCFG_FSDOT11WLANTABLE_POOLID);
            if (pWsscfgFsDot11WlanEntry == NULL)
            {
                if (WsscfgSetAllFsDot11WlanTableTrigger
                    (pWsscfgSetFsDot11WlanEntry,
                     pWsscfgIsSetFsDot11WlanEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanTable:WsscfgSetAllFsDot11WlanTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                    (UINT1 *) pWsscfgOldFsDot11WlanEntry);
                MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                    (UINT1 *) pWsscfgTrgFsDot11WlanEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANTABLE_ISSET_POOLID, (UINT1 *)
                     pWsscfgTrgIsSetFsDot11WlanEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pWsscfgFsDot11WlanEntry, 0,
                    sizeof (tWsscfgFsDot11WlanEntry));
            if ((WsscfgInitializeFsDot11WlanTable
                 (pWsscfgFsDot11WlanEntry)) == OSIX_FAILURE)
            {
                if (WsscfgSetAllFsDot11WlanTableTrigger
                    (pWsscfgSetFsDot11WlanEntry,
                     pWsscfgIsSetFsDot11WlanEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanTable:WsscfgSetAllFsDot11WlanTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                    (UINT1 *) pWsscfgFsDot11WlanEntry);
                MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                    (UINT1 *) pWsscfgOldFsDot11WlanEntry);
                MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                    (UINT1 *) pWsscfgTrgFsDot11WlanEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANTABLE_ISSET_POOLID, (UINT1 *)
                     pWsscfgTrgIsSetFsDot11WlanEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pWsscfgIsSetFsDot11WlanEntry->
                bFsDot11WlanProfileIfIndex != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanEntry->
                    MibObject.i4FsDot11WlanProfileIfIndex =
                    pWsscfgSetFsDot11WlanEntry->
                    MibObject.i4FsDot11WlanProfileIfIndex;
            }

            if (pWsscfgIsSetFsDot11WlanEntry->bFsDot11WlanRowStatus !=
                OSIX_FALSE)
            {
                pWsscfgFsDot11WlanEntry->MibObject.
                    i4FsDot11WlanRowStatus =
                    pWsscfgSetFsDot11WlanEntry->
                    MibObject.i4FsDot11WlanRowStatus;
            }

            if (pWsscfgIsSetFsDot11WlanEntry->
                bCapwapDot11WlanProfileId != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanEntry->
                    MibObject.u4CapwapDot11WlanProfileId =
                    pWsscfgSetFsDot11WlanEntry->
                    MibObject.u4CapwapDot11WlanProfileId;
            }

            if ((pWsscfgSetFsDot11WlanEntry->
                 MibObject.i4FsDot11WlanRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsDot11WlanEntry->
                        MibObject.i4FsDot11WlanRowStatus == ACTIVE)))
            {
                pWsscfgFsDot11WlanEntry->MibObject.
                    i4FsDot11WlanRowStatus = ACTIVE;
            }
            else if (pWsscfgSetFsDot11WlanEntry->
                     MibObject.i4FsDot11WlanRowStatus == CREATE_AND_WAIT)
            {
                pWsscfgFsDot11WlanEntry->MibObject.
                    i4FsDot11WlanRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gWsscfgGlobals.WsscfgGlbMib.FsDot11WlanTable,
                 (tRBElem *) pWsscfgFsDot11WlanEntry) != RB_SUCCESS)
            {
                if (WsscfgSetAllFsDot11WlanTableTrigger
                    (pWsscfgSetFsDot11WlanEntry,
                     pWsscfgIsSetFsDot11WlanEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanTable: WsscfgSetAllFsDot11WlanTableTrigger function returns failure.\r\n"));
                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                    (UINT1 *) pWsscfgFsDot11WlanEntry);
                MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                    (UINT1 *) pWsscfgOldFsDot11WlanEntry);
                MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                    (UINT1 *) pWsscfgTrgFsDot11WlanEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANTABLE_ISSET_POOLID, (UINT1 *)
                     pWsscfgTrgIsSetFsDot11WlanEntry);
                return OSIX_FAILURE;
            }
            if (WsscfgUtilUpdateFsDot11WlanTable
                (NULL, pWsscfgFsDot11WlanEntry,
                 pWsscfgIsSetFsDot11WlanEntry) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanTable: WsscfgUtilUpdateFsDot11WlanTable function returns failure.\r\n"));

                if (WsscfgSetAllFsDot11WlanTableTrigger
                    (pWsscfgSetFsDot11WlanEntry,
                     pWsscfgIsSetFsDot11WlanEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanTable: WsscfgSetAllFsDot11WlanTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gWsscfgGlobals.WsscfgGlbMib.
                           FsDot11WlanTable, pWsscfgFsDot11WlanEntry);
                MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                    (UINT1 *) pWsscfgFsDot11WlanEntry);
                MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                    (UINT1 *) pWsscfgOldFsDot11WlanEntry);
                MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                    (UINT1 *) pWsscfgTrgFsDot11WlanEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANTABLE_ISSET_POOLID, (UINT1 *)
                     pWsscfgTrgIsSetFsDot11WlanEntry);
                return OSIX_FAILURE;
            }

            if ((pWsscfgSetFsDot11WlanEntry->
                 MibObject.i4FsDot11WlanRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsDot11WlanEntry->
                        MibObject.i4FsDot11WlanRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsDot11WlanEntry->
                    MibObject.u4CapwapDot11WlanProfileId =
                    pWsscfgSetFsDot11WlanEntry->
                    MibObject.u4CapwapDot11WlanProfileId;
                pWsscfgTrgFsDot11WlanEntry->
                    MibObject.i4FsDot11WlanRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11WlanEntry->
                    bFsDot11WlanRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsDot11WlanTableTrigger
                    (pWsscfgTrgFsDot11WlanEntry,
                     pWsscfgTrgIsSetFsDot11WlanEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanTable: WsscfgSetAllFsDot11WlanTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                        (UINT1 *) pWsscfgFsDot11WlanEntry);
                    MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                        (UINT1 *) pWsscfgOldFsDot11WlanEntry);
                    MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                        (UINT1 *) pWsscfgTrgFsDot11WlanEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANTABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetFsDot11WlanEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pWsscfgSetFsDot11WlanEntry->
                     MibObject.i4FsDot11WlanRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsDot11WlanEntry->
                    MibObject.i4FsDot11WlanRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11WlanEntry->
                    bFsDot11WlanRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsDot11WlanTableTrigger
                    (pWsscfgTrgFsDot11WlanEntry,
                     pWsscfgTrgIsSetFsDot11WlanEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanTable: WsscfgSetAllFsDot11WlanTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                        (UINT1 *) pWsscfgFsDot11WlanEntry);
                    MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                        (UINT1 *) pWsscfgOldFsDot11WlanEntry);
                    MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                        (UINT1 *) pWsscfgTrgFsDot11WlanEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANTABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetFsDot11WlanEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pWsscfgSetFsDot11WlanEntry->
                MibObject.i4FsDot11WlanRowStatus == CREATE_AND_GO)
            {
                pWsscfgSetFsDot11WlanEntry->
                    MibObject.i4FsDot11WlanRowStatus = ACTIVE;
            }

            if (WsscfgSetAllFsDot11WlanTableTrigger
                (pWsscfgSetFsDot11WlanEntry,
                 pWsscfgIsSetFsDot11WlanEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanTable:  WsscfgSetAllFsDot11WlanTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11WlanEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11WlanEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_ISSET_POOLID,
                                (UINT1 *) pWsscfgTrgIsSetFsDot11WlanEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (WsscfgSetAllFsDot11WlanTableTrigger
                (pWsscfgSetFsDot11WlanEntry,
                 pWsscfgIsSetFsDot11WlanEntry, SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanTable: WsscfgSetAllFsDot11WlanTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanTable: Failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11WlanEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11WlanEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_ISSET_POOLID,
                                (UINT1 *) pWsscfgTrgIsSetFsDot11WlanEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pWsscfgSetFsDot11WlanEntry->
              MibObject.i4FsDot11WlanRowStatus == CREATE_AND_WAIT)
             || (pWsscfgSetFsDot11WlanEntry->
                 MibObject.i4FsDot11WlanRowStatus == CREATE_AND_GO))
    {
        if (WsscfgSetAllFsDot11WlanTableTrigger
            (pWsscfgSetFsDot11WlanEntry, pWsscfgIsSetFsDot11WlanEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanTable: WsscfgSetAllFsDot11WlanTableTrigger function returns failure.\r\n"));
        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11WlanTable: The row is already present.\r\n"));
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11WlanEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11WlanEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsDot11WlanEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pWsscfgOldFsDot11WlanEntry, pWsscfgFsDot11WlanEntry,
            sizeof (tWsscfgFsDot11WlanEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pWsscfgSetFsDot11WlanEntry->MibObject.i4FsDot11WlanRowStatus == DESTROY)
    {
        pWsscfgFsDot11WlanEntry->MibObject.i4FsDot11WlanRowStatus = DESTROY;

        if (WsscfgUtilUpdateFsDot11WlanTable
            (pWsscfgOldFsDot11WlanEntry, pWsscfgFsDot11WlanEntry,
             pWsscfgIsSetFsDot11WlanEntry) != OSIX_SUCCESS)
        {

            if (WsscfgSetAllFsDot11WlanTableTrigger
                (pWsscfgSetFsDot11WlanEntry,
                 pWsscfgIsSetFsDot11WlanEntry, SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanTable: WsscfgSetAllFsDot11WlanTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanTable: WsscfgUtilUpdateFsDot11WlanTable function returns failure.\r\n"));
        }
        RBTreeRem (gWsscfgGlobals.WsscfgGlbMib.FsDot11WlanTable,
                   pWsscfgFsDot11WlanEntry);
        if (WsscfgSetAllFsDot11WlanTableTrigger
            (pWsscfgSetFsDot11WlanEntry, pWsscfgIsSetFsDot11WlanEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanTable: WsscfgSetAllFsDot11WlanTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11WlanEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11WlanEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_ISSET_POOLID,
                                (UINT1 *) pWsscfgTrgIsSetFsDot11WlanEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgFsDot11WlanEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11WlanEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11WlanEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsDot11WlanEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsDot11WlanTableFilterInputs
        (pWsscfgFsDot11WlanEntry, pWsscfgSetFsDot11WlanEntry,
         pWsscfgIsSetFsDot11WlanEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11WlanEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11WlanEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsDot11WlanEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pWsscfgFsDot11WlanEntry->MibObject.i4FsDot11WlanRowStatus ==
         ACTIVE)
        && (pWsscfgSetFsDot11WlanEntry->MibObject.
            i4FsDot11WlanRowStatus != NOT_IN_SERVICE))
    {
        pWsscfgFsDot11WlanEntry->MibObject.i4FsDot11WlanRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pWsscfgTrgFsDot11WlanEntry->MibObject.i4FsDot11WlanRowStatus =
            NOT_IN_SERVICE;
        pWsscfgTrgIsSetFsDot11WlanEntry->bFsDot11WlanRowStatus = OSIX_TRUE;

        if (WsscfgUtilUpdateFsDot11WlanTable
            (pWsscfgOldFsDot11WlanEntry, pWsscfgFsDot11WlanEntry,
             pWsscfgIsSetFsDot11WlanEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pWsscfgFsDot11WlanEntry,
                    pWsscfgOldFsDot11WlanEntry,
                    sizeof (tWsscfgFsDot11WlanEntry));
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanTable:                 WsscfgUtilUpdateFsDot11WlanTable Function returns failure.\r\n"));

            if (WsscfgSetAllFsDot11WlanTableTrigger
                (pWsscfgSetFsDot11WlanEntry,
                 pWsscfgIsSetFsDot11WlanEntry, SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanTable: WsscfgSetAllFsDot11WlanTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11WlanEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11WlanEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_ISSET_POOLID,
                                (UINT1 *) pWsscfgTrgIsSetFsDot11WlanEntry);
            return OSIX_FAILURE;
        }

        if (WsscfgSetAllFsDot11WlanTableTrigger
            (pWsscfgTrgFsDot11WlanEntry,
             pWsscfgTrgIsSetFsDot11WlanEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanTable: WsscfgSetAllFsDot11WlanTableTrigger function returns failure.\r\n"));
        }
    }

    if (pWsscfgSetFsDot11WlanEntry->MibObject.i4FsDot11WlanRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pWsscfgIsSetFsDot11WlanEntry->bFsDot11WlanProfileIfIndex != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanEntry->MibObject.
            i4FsDot11WlanProfileIfIndex =
            pWsscfgSetFsDot11WlanEntry->MibObject.i4FsDot11WlanProfileIfIndex;
    }
    if (pWsscfgIsSetFsDot11WlanEntry->bFsDot11WlanRowStatus != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanEntry->MibObject.i4FsDot11WlanRowStatus =
            pWsscfgSetFsDot11WlanEntry->MibObject.i4FsDot11WlanRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pWsscfgFsDot11WlanEntry->MibObject.i4FsDot11WlanRowStatus = ACTIVE;
    }

    if (WsscfgUtilUpdateFsDot11WlanTable (pWsscfgOldFsDot11WlanEntry,
                                          pWsscfgFsDot11WlanEntry,
                                          pWsscfgIsSetFsDot11WlanEntry)
        != OSIX_SUCCESS)
    {

        if (WsscfgSetAllFsDot11WlanTableTrigger
            (pWsscfgSetFsDot11WlanEntry, pWsscfgIsSetFsDot11WlanEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanTable: WsscfgSetAllFsDot11WlanTableTrigger function returns failure.\r\n"));

        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11WlanTable: WsscfgUtilUpdateFsDot11WlanTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pWsscfgFsDot11WlanEntry, pWsscfgOldFsDot11WlanEntry,
                sizeof (tWsscfgFsDot11WlanEntry));
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11WlanEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11WlanEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsDot11WlanEntry);
        return OSIX_FAILURE;

    }
    if (WsscfgSetAllFsDot11WlanTableTrigger
        (pWsscfgSetFsDot11WlanEntry, pWsscfgIsSetFsDot11WlanEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11WlanTable: WsscfgSetAllFsDot11WlanTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                        (UINT1 *) pWsscfgOldFsDot11WlanEntry);
    MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_POOLID,
                        (UINT1 *) pWsscfgTrgFsDot11WlanEntry);
    MemReleaseMemBlock (WSSCFG_FSDOT11WLANTABLE_ISSET_POOLID,
                        (UINT1 *) pWsscfgTrgIsSetFsDot11WlanEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11WlanBindTable
 Input       :  pWsscfgSetFsDot11WlanBindEntry
                pWsscfgIsSetFsDot11WlanBindEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllFsDot11WlanBindTable (tWsscfgFsDot11WlanBindEntry *
                                  pWsscfgSetFsDot11WlanBindEntry,
                                  tWsscfgIsSetFsDot11WlanBindEntry *
                                  pWsscfgIsSetFsDot11WlanBindEntry,
                                  INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tWsscfgFsDot11WlanBindEntry *pWsscfgFsDot11WlanBindEntry = NULL;
    tWsscfgFsDot11WlanBindEntry *pWsscfgOldFsDot11WlanBindEntry = NULL;
    tWsscfgFsDot11WlanBindEntry *pWsscfgTrgFsDot11WlanBindEntry = NULL;
    tWsscfgIsSetFsDot11WlanBindEntry
        * pWsscfgTrgIsSetFsDot11WlanBindEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pWsscfgOldFsDot11WlanBindEntry = (tWsscfgFsDot11WlanBindEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11WLANBINDTABLE_POOLID);
    if (pWsscfgOldFsDot11WlanBindEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWsscfgTrgFsDot11WlanBindEntry = (tWsscfgFsDot11WlanBindEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11WLANBINDTABLE_POOLID);
    if (pWsscfgTrgFsDot11WlanBindEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11WlanBindEntry);
        return OSIX_FAILURE;
    }
    pWsscfgTrgIsSetFsDot11WlanBindEntry =
        (tWsscfgIsSetFsDot11WlanBindEntry *)
        MemAllocMemBlk (WSSCFG_FSDOT11WLANBINDTABLE_ISSET_POOLID);
    if (pWsscfgTrgIsSetFsDot11WlanBindEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11WlanBindEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11WlanBindEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pWsscfgOldFsDot11WlanBindEntry, 0,
            sizeof (tWsscfgFsDot11WlanBindEntry));
    MEMSET (pWsscfgTrgFsDot11WlanBindEntry, 0,
            sizeof (tWsscfgFsDot11WlanBindEntry));
    MEMSET (pWsscfgTrgIsSetFsDot11WlanBindEntry, 0,
            sizeof (tWsscfgIsSetFsDot11WlanBindEntry));

    /* Check whether the node is already present */
    pWsscfgFsDot11WlanBindEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsDot11WlanBindTable,
                   (tRBElem *) pWsscfgSetFsDot11WlanBindEntry);

    if (pWsscfgFsDot11WlanBindEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pWsscfgSetFsDot11WlanBindEntry->
             MibObject.i4FsDot11WlanBindRowStatus == CREATE_AND_WAIT)
            || (pWsscfgSetFsDot11WlanBindEntry->
                MibObject.i4FsDot11WlanBindRowStatus == CREATE_AND_GO)
            ||
            ((pWsscfgSetFsDot11WlanBindEntry->
              MibObject.i4FsDot11WlanBindRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pWsscfgFsDot11WlanBindEntry =
                (tWsscfgFsDot11WlanBindEntry *)
                MemAllocMemBlk (WSSCFG_FSDOT11WLANBINDTABLE_POOLID);
            if (pWsscfgFsDot11WlanBindEntry == NULL)
            {
                if (WsscfgSetAllFsDot11WlanBindTableTrigger
                    (pWsscfgSetFsDot11WlanBindEntry,
                     pWsscfgIsSetFsDot11WlanBindEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanBindTable:WsscfgSetAllFsDot11WlanBindTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanBindTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                                    (UINT1 *) pWsscfgOldFsDot11WlanBindEntry);
                MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                                    (UINT1 *) pWsscfgTrgFsDot11WlanBindEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANBINDTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11WlanBindEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pWsscfgFsDot11WlanBindEntry, 0,
                    sizeof (tWsscfgFsDot11WlanBindEntry));
            if ((WsscfgInitializeFsDot11WlanBindTable
                 (pWsscfgFsDot11WlanBindEntry)) == OSIX_FAILURE)
            {
                if (WsscfgSetAllFsDot11WlanBindTableTrigger
                    (pWsscfgSetFsDot11WlanBindEntry,
                     pWsscfgIsSetFsDot11WlanBindEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanBindTable:WsscfgSetAllFsDot11WlanBindTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanBindTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                                    (UINT1 *) pWsscfgFsDot11WlanBindEntry);
                MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                                    (UINT1 *) pWsscfgOldFsDot11WlanBindEntry);
                MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                                    (UINT1 *) pWsscfgTrgFsDot11WlanBindEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANBINDTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11WlanBindEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pWsscfgIsSetFsDot11WlanBindEntry->
                bFsDot11WlanBindWlanId != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanBindEntry->
                    MibObject.u4FsDot11WlanBindWlanId =
                    pWsscfgSetFsDot11WlanBindEntry->
                    MibObject.u4FsDot11WlanBindWlanId;
            }

            if (pWsscfgIsSetFsDot11WlanBindEntry->bFsDot11WlanBindBssIfIndex !=
                OSIX_FALSE)
            {
                pWsscfgFsDot11WlanBindEntry->
                    MibObject.i4FsDot11WlanBindBssIfIndex =
                    pWsscfgSetFsDot11WlanBindEntry->
                    MibObject.i4FsDot11WlanBindBssIfIndex;
            }

            if (pWsscfgIsSetFsDot11WlanBindEntry->bFsDot11WlanBindRowStatus !=
                OSIX_FALSE)
            {
                pWsscfgFsDot11WlanBindEntry->
                    MibObject.i4FsDot11WlanBindRowStatus =
                    pWsscfgSetFsDot11WlanBindEntry->
                    MibObject.i4FsDot11WlanBindRowStatus;
            }

            if (pWsscfgIsSetFsDot11WlanBindEntry->bIfIndex != OSIX_FALSE)
            {
                pWsscfgFsDot11WlanBindEntry->MibObject.i4IfIndex =
                    pWsscfgSetFsDot11WlanBindEntry->MibObject.i4IfIndex;
            }

            if (pWsscfgIsSetFsDot11WlanBindEntry->bCapwapDot11WlanProfileId !=
                OSIX_FALSE)
            {
                pWsscfgFsDot11WlanBindEntry->
                    MibObject.u4CapwapDot11WlanProfileId =
                    pWsscfgSetFsDot11WlanBindEntry->
                    MibObject.u4CapwapDot11WlanProfileId;
            }

            if ((pWsscfgSetFsDot11WlanBindEntry->
                 MibObject.i4FsDot11WlanBindRowStatus ==
                 CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsDot11WlanBindEntry->
                        MibObject.i4FsDot11WlanBindRowStatus == ACTIVE)))
            {
                pWsscfgFsDot11WlanBindEntry->
                    MibObject.i4FsDot11WlanBindRowStatus = ACTIVE;
            }
            else if (pWsscfgSetFsDot11WlanBindEntry->
                     MibObject.i4FsDot11WlanBindRowStatus == CREATE_AND_WAIT)
            {
                pWsscfgFsDot11WlanBindEntry->
                    MibObject.i4FsDot11WlanBindRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gWsscfgGlobals.WsscfgGlbMib.FsDot11WlanBindTable,
                 (tRBElem *) pWsscfgFsDot11WlanBindEntry) != RB_SUCCESS)
            {
                if (WsscfgSetAllFsDot11WlanBindTableTrigger
                    (pWsscfgSetFsDot11WlanBindEntry,
                     pWsscfgIsSetFsDot11WlanBindEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanBindTable: WsscfgSetAllFsDot11WlanBindTableTrigger function returns failure.\r\n"));
                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanBindTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                                    (UINT1 *) pWsscfgFsDot11WlanBindEntry);
                MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                                    (UINT1 *) pWsscfgOldFsDot11WlanBindEntry);
                MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                                    (UINT1 *) pWsscfgTrgFsDot11WlanBindEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANBINDTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11WlanBindEntry);
                return OSIX_FAILURE;
            }
            if (WsscfgUtilUpdateFsDot11WlanBindTable
                (NULL, pWsscfgFsDot11WlanBindEntry,
                 pWsscfgIsSetFsDot11WlanBindEntry) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanBindTable: WsscfgUtilUpdateFsDot11WlanBindTable function returns failure.\r\n"));

                if (WsscfgSetAllFsDot11WlanBindTableTrigger
                    (pWsscfgSetFsDot11WlanBindEntry,
                     pWsscfgIsSetFsDot11WlanBindEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanBindTable: WsscfgSetAllFsDot11WlanBindTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gWsscfgGlobals.WsscfgGlbMib.
                           FsDot11WlanBindTable, pWsscfgFsDot11WlanBindEntry);
                MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                                    (UINT1 *) pWsscfgFsDot11WlanBindEntry);
                MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                                    (UINT1 *) pWsscfgOldFsDot11WlanBindEntry);
                MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                                    (UINT1 *) pWsscfgTrgFsDot11WlanBindEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSDOT11WLANBINDTABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsDot11WlanBindEntry);
                return OSIX_FAILURE;
            }

            if ((pWsscfgSetFsDot11WlanBindEntry->
                 MibObject.i4FsDot11WlanBindRowStatus ==
                 CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsDot11WlanBindEntry->
                        MibObject.i4FsDot11WlanBindRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsDot11WlanBindEntry->MibObject.i4IfIndex =
                    pWsscfgSetFsDot11WlanBindEntry->MibObject.i4IfIndex;
                pWsscfgTrgFsDot11WlanBindEntry->
                    MibObject.u4CapwapDot11WlanProfileId =
                    pWsscfgSetFsDot11WlanBindEntry->
                    MibObject.u4CapwapDot11WlanProfileId;
                pWsscfgTrgFsDot11WlanBindEntry->
                    MibObject.i4FsDot11WlanBindRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11WlanBindEntry->bFsDot11WlanBindRowStatus
                    = OSIX_TRUE;

                if (WsscfgSetAllFsDot11WlanBindTableTrigger
                    (pWsscfgTrgFsDot11WlanBindEntry,
                     pWsscfgTrgIsSetFsDot11WlanBindEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanBindTable: WsscfgSetAllFsDot11WlanBindTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANBINDTABLE_POOLID, (UINT1 *)
                         pWsscfgFsDot11WlanBindEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANBINDTABLE_POOLID, (UINT1 *)
                         pWsscfgOldFsDot11WlanBindEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANBINDTABLE_POOLID, (UINT1 *)
                         pWsscfgTrgFsDot11WlanBindEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANBINDTABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetFsDot11WlanBindEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pWsscfgSetFsDot11WlanBindEntry->
                     MibObject.i4FsDot11WlanBindRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsDot11WlanBindEntry->
                    MibObject.i4FsDot11WlanBindRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsDot11WlanBindEntry->bFsDot11WlanBindRowStatus
                    = OSIX_TRUE;

                if (WsscfgSetAllFsDot11WlanBindTableTrigger
                    (pWsscfgTrgFsDot11WlanBindEntry,
                     pWsscfgTrgIsSetFsDot11WlanBindEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsDot11WlanBindTable: WsscfgSetAllFsDot11WlanBindTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANBINDTABLE_POOLID, (UINT1 *)
                         pWsscfgFsDot11WlanBindEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANBINDTABLE_POOLID, (UINT1 *)
                         pWsscfgOldFsDot11WlanBindEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANBINDTABLE_POOLID, (UINT1 *)
                         pWsscfgTrgFsDot11WlanBindEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSDOT11WLANBINDTABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetFsDot11WlanBindEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pWsscfgSetFsDot11WlanBindEntry->
                MibObject.i4FsDot11WlanBindRowStatus == CREATE_AND_GO)
            {
                pWsscfgSetFsDot11WlanBindEntry->
                    MibObject.i4FsDot11WlanBindRowStatus = ACTIVE;
            }

            if (WsscfgSetAllFsDot11WlanBindTableTrigger
                (pWsscfgSetFsDot11WlanBindEntry,
                 pWsscfgIsSetFsDot11WlanBindEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanBindTable:  WsscfgSetAllFsDot11WlanBindTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11WlanBindEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11WlanBindEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANBINDTABLE_ISSET_POOLID, (UINT1 *)
                 pWsscfgTrgIsSetFsDot11WlanBindEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (WsscfgSetAllFsDot11WlanBindTableTrigger
                (pWsscfgSetFsDot11WlanBindEntry,
                 pWsscfgIsSetFsDot11WlanBindEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanBindTable: WsscfgSetAllFsDot11WlanBindTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanBindTable: Failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11WlanBindEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11WlanBindEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANBINDTABLE_ISSET_POOLID, (UINT1 *)
                 pWsscfgTrgIsSetFsDot11WlanBindEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pWsscfgSetFsDot11WlanBindEntry->
              MibObject.i4FsDot11WlanBindRowStatus == CREATE_AND_WAIT)
             || (pWsscfgSetFsDot11WlanBindEntry->
                 MibObject.i4FsDot11WlanBindRowStatus == CREATE_AND_GO))
    {
        if (WsscfgSetAllFsDot11WlanBindTableTrigger
            (pWsscfgSetFsDot11WlanBindEntry,
             pWsscfgIsSetFsDot11WlanBindEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanBindTable: WsscfgSetAllFsDot11WlanBindTableTrigger function returns failure.\r\n"));
        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11WlanBindTable: The row is already present.\r\n"));
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11WlanBindEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11WlanBindEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsDot11WlanBindEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pWsscfgOldFsDot11WlanBindEntry,
            pWsscfgFsDot11WlanBindEntry, sizeof (tWsscfgFsDot11WlanBindEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pWsscfgSetFsDot11WlanBindEntry->
        MibObject.i4FsDot11WlanBindRowStatus == DESTROY)
    {
        pWsscfgFsDot11WlanBindEntry->MibObject.
            i4FsDot11WlanBindRowStatus = DESTROY;

        if (WsscfgUtilUpdateFsDot11WlanBindTable
            (pWsscfgOldFsDot11WlanBindEntry,
             pWsscfgFsDot11WlanBindEntry,
             pWsscfgIsSetFsDot11WlanBindEntry) != OSIX_SUCCESS)
        {

            if (WsscfgSetAllFsDot11WlanBindTableTrigger
                (pWsscfgSetFsDot11WlanBindEntry,
                 pWsscfgIsSetFsDot11WlanBindEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanBindTable: WsscfgSetAllFsDot11WlanBindTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanBindTable: WsscfgUtilUpdateFsDot11WlanBindTable function returns failure.\r\n"));
        }
        RBTreeRem (gWsscfgGlobals.WsscfgGlbMib.FsDot11WlanBindTable,
                   pWsscfgFsDot11WlanBindEntry);
        if (WsscfgSetAllFsDot11WlanBindTableTrigger
            (pWsscfgSetFsDot11WlanBindEntry,
             pWsscfgIsSetFsDot11WlanBindEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanBindTable: WsscfgSetAllFsDot11WlanBindTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11WlanBindEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11WlanBindEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANBINDTABLE_ISSET_POOLID, (UINT1 *)
                 pWsscfgTrgIsSetFsDot11WlanBindEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgFsDot11WlanBindEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11WlanBindEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11WlanBindEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsDot11WlanBindEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsDot11WlanBindTableFilterInputs
        (pWsscfgFsDot11WlanBindEntry, pWsscfgSetFsDot11WlanBindEntry,
         pWsscfgIsSetFsDot11WlanBindEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11WlanBindEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11WlanBindEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsDot11WlanBindEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pWsscfgFsDot11WlanBindEntry->
         MibObject.i4FsDot11WlanBindRowStatus == ACTIVE)
        && (pWsscfgSetFsDot11WlanBindEntry->
            MibObject.i4FsDot11WlanBindRowStatus != NOT_IN_SERVICE))
    {
        pWsscfgFsDot11WlanBindEntry->MibObject.
            i4FsDot11WlanBindRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pWsscfgTrgFsDot11WlanBindEntry->
            MibObject.i4FsDot11WlanBindRowStatus = NOT_IN_SERVICE;
        pWsscfgTrgIsSetFsDot11WlanBindEntry->
            bFsDot11WlanBindRowStatus = OSIX_TRUE;

        if (WsscfgUtilUpdateFsDot11WlanBindTable
            (pWsscfgOldFsDot11WlanBindEntry,
             pWsscfgFsDot11WlanBindEntry,
             pWsscfgIsSetFsDot11WlanBindEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pWsscfgFsDot11WlanBindEntry,
                    pWsscfgOldFsDot11WlanBindEntry,
                    sizeof (tWsscfgFsDot11WlanBindEntry));
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanBindTable:                 WsscfgUtilUpdateFsDot11WlanBindTable Function returns failure.\r\n"));

            if (WsscfgSetAllFsDot11WlanBindTableTrigger
                (pWsscfgSetFsDot11WlanBindEntry,
                 pWsscfgIsSetFsDot11WlanBindEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsDot11WlanBindTable: WsscfgSetAllFsDot11WlanBindTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsDot11WlanBindEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsDot11WlanBindEntry);
            MemReleaseMemBlock
                (WSSCFG_FSDOT11WLANBINDTABLE_ISSET_POOLID, (UINT1 *)
                 pWsscfgTrgIsSetFsDot11WlanBindEntry);
            return OSIX_FAILURE;
        }

        if (WsscfgSetAllFsDot11WlanBindTableTrigger
            (pWsscfgTrgFsDot11WlanBindEntry,
             pWsscfgTrgIsSetFsDot11WlanBindEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanBindTable: WsscfgSetAllFsDot11WlanBindTableTrigger function returns failure.\r\n"));
        }
    }

    if (pWsscfgSetFsDot11WlanBindEntry->
        MibObject.i4FsDot11WlanBindRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pWsscfgIsSetFsDot11WlanBindEntry->bFsDot11WlanBindWlanId != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanBindEntry->MibObject.
            u4FsDot11WlanBindWlanId =
            pWsscfgSetFsDot11WlanBindEntry->MibObject.u4FsDot11WlanBindWlanId;
    }
    if (pWsscfgIsSetFsDot11WlanBindEntry->
        bFsDot11WlanBindBssIfIndex != OSIX_FALSE)
    {
        pWsscfgFsDot11WlanBindEntry->
            MibObject.i4FsDot11WlanBindBssIfIndex =
            pWsscfgSetFsDot11WlanBindEntry->
            MibObject.i4FsDot11WlanBindBssIfIndex;
    }
    if (pWsscfgIsSetFsDot11WlanBindEntry->bFsDot11WlanBindRowStatus !=
        OSIX_FALSE)
    {
        pWsscfgFsDot11WlanBindEntry->MibObject.
            i4FsDot11WlanBindRowStatus =
            pWsscfgSetFsDot11WlanBindEntry->
            MibObject.i4FsDot11WlanBindRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pWsscfgFsDot11WlanBindEntry->MibObject.
            i4FsDot11WlanBindRowStatus = ACTIVE;
    }

    if (WsscfgUtilUpdateFsDot11WlanBindTable
        (pWsscfgOldFsDot11WlanBindEntry, pWsscfgFsDot11WlanBindEntry,
         pWsscfgIsSetFsDot11WlanBindEntry) != OSIX_SUCCESS)
    {

        if (WsscfgSetAllFsDot11WlanBindTableTrigger
            (pWsscfgSetFsDot11WlanBindEntry,
             pWsscfgIsSetFsDot11WlanBindEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsDot11WlanBindTable: WsscfgSetAllFsDot11WlanBindTableTrigger function returns failure.\r\n"));

        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11WlanBindTable: WsscfgUtilUpdateFsDot11WlanBindTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pWsscfgFsDot11WlanBindEntry,
                pWsscfgOldFsDot11WlanBindEntry,
                sizeof (tWsscfgFsDot11WlanBindEntry));
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsDot11WlanBindEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsDot11WlanBindEntry);
        MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsDot11WlanBindEntry);
        return OSIX_FAILURE;

    }
    if (WsscfgSetAllFsDot11WlanBindTableTrigger
        (pWsscfgSetFsDot11WlanBindEntry,
         pWsscfgIsSetFsDot11WlanBindEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsDot11WlanBindTable: WsscfgSetAllFsDot11WlanBindTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                        (UINT1 *) pWsscfgOldFsDot11WlanBindEntry);
    MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_POOLID,
                        (UINT1 *) pWsscfgTrgFsDot11WlanBindEntry);
    MemReleaseMemBlock (WSSCFG_FSDOT11WLANBINDTABLE_ISSET_POOLID,
                        (UINT1 *) pWsscfgTrgIsSetFsDot11WlanBindEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11nConfigTable
 Input       :  pWsscfgSetFsDot11nConfigEntry
                pWsscfgIsSetFsDot11nConfigEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllFsDot11nConfigTable (tWsscfgFsDot11nConfigEntry *
                                 pWsscfgSetFsDot11nConfigEntry,
                                 tWsscfgIsSetFsDot11nConfigEntry *
                                 pWsscfgIsSetFsDot11nConfigEntry)
{
    tWsscfgFsDot11nConfigEntry WsscfgOldFsDot11nConfigEntry;
    if (WsscfgUtilUpdateFsDot11nConfigTable
        (&WsscfgOldFsDot11nConfigEntry, pWsscfgSetFsDot11nConfigEntry,
         pWsscfgIsSetFsDot11nConfigEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11nMCSDataRateTable
 Input       :  pWsscfgSetFsDot11nMCSDataRateEntry
                pWsscfgIsSetFsDot11nMCSDataRateEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllFsDot11nMCSDataRateTable (tWsscfgFsDot11nMCSDataRateEntry *
                                      pWsscfgSetFsDot11nMCSDataRateEntry,
                                      tWsscfgIsSetFsDot11nMCSDataRateEntry *
                                      pWsscfgIsSetFsDot11nMCSDataRateEntry)
{
    tWsscfgFsDot11nMCSDataRateEntry WsscfgOldFsDot11nMCSDataRateEntry;
    if (WsscfgUtilUpdateFsDot11nMCSDataRateTable
        (&WsscfgOldFsDot11nMCSDataRateEntry,
         pWsscfgSetFsDot11nMCSDataRateEntry,
         pWsscfgIsSetFsDot11nMCSDataRateEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsWtpImageUpgradeTable
 Input       :  pWsscfgSetFsWtpImageUpgradeEntry
                pWsscfgIsSetFsWtpImageUpgradeEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllFsWtpImageUpgradeTable (tWsscfgFsWtpImageUpgradeEntry *
                                    pWsscfgSetFsWtpImageUpgradeEntry,
                                    tWsscfgIsSetFsWtpImageUpgradeEntry *
                                    pWsscfgIsSetFsWtpImageUpgradeEntry,
                                    INT4 i4RowStatusLogic,
                                    INT4 i4RowCreateOption)
{
    tWsscfgFsWtpImageUpgradeEntry *pWsscfgFsWtpImageUpgradeEntry = NULL;
    tWsscfgFsWtpImageUpgradeEntry *pWsscfgOldFsWtpImageUpgradeEntry = NULL;
    tWsscfgFsWtpImageUpgradeEntry *pWsscfgTrgFsWtpImageUpgradeEntry = NULL;
    tWsscfgIsSetFsWtpImageUpgradeEntry *pWsscfgTrgIsSetFsWtpImageUpgradeEntry =
        NULL;
    INT4                i4RowMakeActive = FALSE;

    pWsscfgOldFsWtpImageUpgradeEntry =
        (tWsscfgFsWtpImageUpgradeEntry *)
        MemAllocMemBlk (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID);
    if (pWsscfgOldFsWtpImageUpgradeEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWsscfgTrgFsWtpImageUpgradeEntry =
        (tWsscfgFsWtpImageUpgradeEntry *)
        MemAllocMemBlk (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID);
    if (pWsscfgTrgFsWtpImageUpgradeEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsWtpImageUpgradeEntry);
        return OSIX_FAILURE;
    }
    pWsscfgTrgIsSetFsWtpImageUpgradeEntry =
        (tWsscfgIsSetFsWtpImageUpgradeEntry *)
        MemAllocMemBlk (WSSCFG_FSWTPIMAGEUPGRADETABLE_ISSET_POOLID);

    if (pWsscfgTrgIsSetFsWtpImageUpgradeEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsWtpImageUpgradeEntry);
        MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsWtpImageUpgradeEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pWsscfgOldFsWtpImageUpgradeEntry, 0,
            sizeof (tWsscfgFsWtpImageUpgradeEntry));
    MEMSET (pWsscfgTrgFsWtpImageUpgradeEntry, 0,
            sizeof (tWsscfgFsWtpImageUpgradeEntry));
    MEMSET (pWsscfgTrgIsSetFsWtpImageUpgradeEntry, 0,
            sizeof (tWsscfgIsSetFsWtpImageUpgradeEntry));

    /* Check whether the node is already present */
    pWsscfgFsWtpImageUpgradeEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsWtpImageUpgradeTable,
                   (tRBElem *) pWsscfgSetFsWtpImageUpgradeEntry);

    if (pWsscfgFsWtpImageUpgradeEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pWsscfgSetFsWtpImageUpgradeEntry->
             MibObject.i4FsWtpRowStatus == CREATE_AND_WAIT)
            || (pWsscfgSetFsWtpImageUpgradeEntry->
                MibObject.i4FsWtpRowStatus == CREATE_AND_GO)
            ||
            ((pWsscfgSetFsWtpImageUpgradeEntry->
              MibObject.i4FsWtpRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pWsscfgFsWtpImageUpgradeEntry =
                (tWsscfgFsWtpImageUpgradeEntry *)
                MemAllocMemBlk (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID);
            if (pWsscfgFsWtpImageUpgradeEntry == NULL)
            {
                if (WsscfgSetAllFsWtpImageUpgradeTableTrigger
                    (pWsscfgSetFsWtpImageUpgradeEntry,
                     pWsscfgIsSetFsWtpImageUpgradeEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsWtpImageUpgradeTable:"
                                 "WsscfgSetAllFsWtpImageUpgradeTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsWtpImageUpgradeTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock
                    (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID, (UINT1 *)
                     pWsscfgOldFsWtpImageUpgradeEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID, (UINT1 *)
                     pWsscfgTrgFsWtpImageUpgradeEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSWTPIMAGEUPGRADETABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsWtpImageUpgradeEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pWsscfgFsWtpImageUpgradeEntry, 0,
                    sizeof (tWsscfgFsWtpImageUpgradeEntry));
            if ((WsscfgInitializeFsWtpImageUpgradeTable
                 (pWsscfgFsWtpImageUpgradeEntry)) == OSIX_FAILURE)
            {
                if (WsscfgSetAllFsWtpImageUpgradeTableTrigger
                    (pWsscfgSetFsWtpImageUpgradeEntry,
                     pWsscfgIsSetFsWtpImageUpgradeEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsWtpImageUpgradeTable:"
                                 "WsscfgSetAllFsWtpImageUpgradeTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsWtpImageUpgradeTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID, (UINT1 *)
                     pWsscfgFsWtpImageUpgradeEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID, (UINT1 *)
                     pWsscfgOldFsWtpImageUpgradeEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID, (UINT1 *)
                     pWsscfgTrgFsWtpImageUpgradeEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSWTPIMAGEUPGRADETABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsWtpImageUpgradeEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pWsscfgIsSetFsWtpImageUpgradeEntry->
                bFsWtpImageVersion != OSIX_FALSE)
            {
                MEMCPY (pWsscfgFsWtpImageUpgradeEntry->
                        MibObject.au1FsWtpImageVersion,
                        pWsscfgSetFsWtpImageUpgradeEntry->
                        MibObject.au1FsWtpImageVersion,
                        pWsscfgSetFsWtpImageUpgradeEntry->
                        MibObject.i4FsWtpImageVersionLen);

                pWsscfgFsWtpImageUpgradeEntry->
                    MibObject.i4FsWtpImageVersionLen =
                    pWsscfgSetFsWtpImageUpgradeEntry->
                    MibObject.i4FsWtpImageVersionLen;
            }

            if (pWsscfgIsSetFsWtpImageUpgradeEntry->
                bFsWtpUpgradeDev != OSIX_FALSE)
            {
                pWsscfgFsWtpImageUpgradeEntry->
                    MibObject.i4FsWtpUpgradeDev =
                    pWsscfgSetFsWtpImageUpgradeEntry->
                    MibObject.i4FsWtpUpgradeDev;
            }

            if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpName != OSIX_FALSE)
            {
                MEMCPY (pWsscfgFsWtpImageUpgradeEntry->
                        MibObject.au1FsWtpName,
                        pWsscfgSetFsWtpImageUpgradeEntry->
                        MibObject.au1FsWtpName,
                        pWsscfgSetFsWtpImageUpgradeEntry->
                        MibObject.i4FsWtpNameLen);

                pWsscfgFsWtpImageUpgradeEntry->MibObject.
                    i4FsWtpNameLen =
                    pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpNameLen;
            }

            if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpImageName !=
                OSIX_FALSE)
            {
                MEMCPY (pWsscfgFsWtpImageUpgradeEntry->
                        MibObject.au1FsWtpImageName,
                        pWsscfgSetFsWtpImageUpgradeEntry->
                        MibObject.au1FsWtpImageName,
                        pWsscfgSetFsWtpImageUpgradeEntry->
                        MibObject.i4FsWtpImageNameLen);

                pWsscfgFsWtpImageUpgradeEntry->
                    MibObject.i4FsWtpImageNameLen =
                    pWsscfgSetFsWtpImageUpgradeEntry->
                    MibObject.i4FsWtpImageNameLen;
            }

            if (pWsscfgIsSetFsWtpImageUpgradeEntry->
                bFsWtpAddressType != OSIX_FALSE)
            {
                pWsscfgFsWtpImageUpgradeEntry->
                    MibObject.i4FsWtpAddressType =
                    pWsscfgSetFsWtpImageUpgradeEntry->
                    MibObject.i4FsWtpAddressType;
            }

            if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpServerIP !=
                OSIX_FALSE)
            {
                MEMCPY (pWsscfgFsWtpImageUpgradeEntry->
                        MibObject.au1FsWtpServerIP,
                        pWsscfgSetFsWtpImageUpgradeEntry->
                        MibObject.au1FsWtpServerIP,
                        pWsscfgSetFsWtpImageUpgradeEntry->
                        MibObject.i4FsWtpServerIPLen);

                pWsscfgFsWtpImageUpgradeEntry->
                    MibObject.i4FsWtpServerIPLen =
                    pWsscfgSetFsWtpImageUpgradeEntry->
                    MibObject.i4FsWtpServerIPLen;
            }

            if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpRowStatus !=
                OSIX_FALSE)
            {
                pWsscfgFsWtpImageUpgradeEntry->MibObject.
                    i4FsWtpRowStatus =
                    pWsscfgSetFsWtpImageUpgradeEntry->
                    MibObject.i4FsWtpRowStatus;
            }

            if (pWsscfgIsSetFsWtpImageUpgradeEntry->
                bCapwapBaseWtpProfileWtpModelNumber != OSIX_FALSE)
            {
                MEMCPY (pWsscfgFsWtpImageUpgradeEntry->
                        MibObject.au1CapwapBaseWtpProfileWtpModelNumber,
                        pWsscfgSetFsWtpImageUpgradeEntry->
                        MibObject.au1CapwapBaseWtpProfileWtpModelNumber,
                        pWsscfgSetFsWtpImageUpgradeEntry->
                        MibObject.i4CapwapBaseWtpProfileWtpModelNumberLen);

                pWsscfgFsWtpImageUpgradeEntry->
                    MibObject.i4CapwapBaseWtpProfileWtpModelNumberLen
                    =
                    pWsscfgSetFsWtpImageUpgradeEntry->
                    MibObject.i4CapwapBaseWtpProfileWtpModelNumberLen;
            }

            if ((pWsscfgSetFsWtpImageUpgradeEntry->
                 MibObject.i4FsWtpRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsWtpImageUpgradeEntry->
                        MibObject.i4FsWtpRowStatus == ACTIVE)))
            {
                pWsscfgFsWtpImageUpgradeEntry->MibObject.
                    i4FsWtpRowStatus = ACTIVE;
            }
            else if (pWsscfgSetFsWtpImageUpgradeEntry->
                     MibObject.i4FsWtpRowStatus == CREATE_AND_WAIT)
            {
                pWsscfgFsWtpImageUpgradeEntry->MibObject.
                    i4FsWtpRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gWsscfgGlobals.WsscfgGlbMib.FsWtpImageUpgradeTable,
                 (tRBElem *) pWsscfgFsWtpImageUpgradeEntry) != RB_SUCCESS)
            {
                if (WsscfgSetAllFsWtpImageUpgradeTableTrigger
                    (pWsscfgSetFsWtpImageUpgradeEntry,
                     pWsscfgIsSetFsWtpImageUpgradeEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsWtpImageUpgradeTable:"
                                 " WsscfgSetAllFsWtpImageUpgradeTableTrigger function returns failure.\r\n"));
                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsWtpImageUpgradeTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock
                    (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID, (UINT1 *)
                     pWsscfgFsWtpImageUpgradeEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID, (UINT1 *)
                     pWsscfgOldFsWtpImageUpgradeEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID, (UINT1 *)
                     pWsscfgTrgFsWtpImageUpgradeEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSWTPIMAGEUPGRADETABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsWtpImageUpgradeEntry);
                return OSIX_FAILURE;
            }
            if (WsscfgUtilUpdateFsWtpImageUpgradeTable
                (NULL, pWsscfgFsWtpImageUpgradeEntry,
                 pWsscfgIsSetFsWtpImageUpgradeEntry) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsWtpImageUpgradeTable:"
                             " WsscfgUtilUpdateFsWtpImageUpgradeTable function returns failure.\r\n"));

                if (WsscfgSetAllFsWtpImageUpgradeTableTrigger
                    (pWsscfgSetFsWtpImageUpgradeEntry,
                     pWsscfgIsSetFsWtpImageUpgradeEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsWtpImageUpgradeTable: "
                                 "WsscfgSetAllFsWtpImageUpgradeTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gWsscfgGlobals.
                           WsscfgGlbMib.FsWtpImageUpgradeTable,
                           pWsscfgFsWtpImageUpgradeEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID, (UINT1 *)
                     pWsscfgFsWtpImageUpgradeEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID, (UINT1 *)
                     pWsscfgOldFsWtpImageUpgradeEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID, (UINT1 *)
                     pWsscfgTrgFsWtpImageUpgradeEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSWTPIMAGEUPGRADETABLE_ISSET_POOLID,
                     (UINT1 *) pWsscfgTrgIsSetFsWtpImageUpgradeEntry);
                return OSIX_FAILURE;
            }

            if ((pWsscfgSetFsWtpImageUpgradeEntry->
                 MibObject.i4FsWtpRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsWtpImageUpgradeEntry->
                        MibObject.i4FsWtpRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                MEMCPY (pWsscfgTrgFsWtpImageUpgradeEntry->
                        MibObject.au1CapwapBaseWtpProfileWtpModelNumber,
                        pWsscfgSetFsWtpImageUpgradeEntry->
                        MibObject.au1CapwapBaseWtpProfileWtpModelNumber,
                        pWsscfgSetFsWtpImageUpgradeEntry->
                        MibObject.i4CapwapBaseWtpProfileWtpModelNumberLen);

                pWsscfgTrgFsWtpImageUpgradeEntry->
                    MibObject.i4CapwapBaseWtpProfileWtpModelNumberLen
                    =
                    pWsscfgSetFsWtpImageUpgradeEntry->
                    MibObject.i4CapwapBaseWtpProfileWtpModelNumberLen;
                pWsscfgTrgFsWtpImageUpgradeEntry->
                    MibObject.i4FsWtpRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsWtpImageUpgradeEntry->
                    bFsWtpRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsWtpImageUpgradeTableTrigger
                    (pWsscfgTrgFsWtpImageUpgradeEntry,
                     pWsscfgTrgIsSetFsWtpImageUpgradeEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsWtpImageUpgradeTable:"
                                 " WsscfgSetAllFsWtpImageUpgradeTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                         (UINT1 *) pWsscfgFsWtpImageUpgradeEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                         (UINT1 *) pWsscfgOldFsWtpImageUpgradeEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                         (UINT1 *) pWsscfgTrgFsWtpImageUpgradeEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSWTPIMAGEUPGRADETABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetFsWtpImageUpgradeEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pWsscfgSetFsWtpImageUpgradeEntry->
                     MibObject.i4FsWtpRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsWtpImageUpgradeEntry->
                    MibObject.i4FsWtpRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsWtpImageUpgradeEntry->
                    bFsWtpRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsWtpImageUpgradeTableTrigger
                    (pWsscfgTrgFsWtpImageUpgradeEntry,
                     pWsscfgTrgIsSetFsWtpImageUpgradeEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsWtpImageUpgradeTable:"
                                 " WsscfgSetAllFsWtpImageUpgradeTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock
                        (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                         (UINT1 *) pWsscfgFsWtpImageUpgradeEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                         (UINT1 *) pWsscfgOldFsWtpImageUpgradeEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                         (UINT1 *) pWsscfgTrgFsWtpImageUpgradeEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSWTPIMAGEUPGRADETABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetFsWtpImageUpgradeEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pWsscfgSetFsWtpImageUpgradeEntry->
                MibObject.i4FsWtpRowStatus == CREATE_AND_GO)
            {
                pWsscfgSetFsWtpImageUpgradeEntry->
                    MibObject.i4FsWtpRowStatus = ACTIVE;
            }

            if (WsscfgSetAllFsWtpImageUpgradeTableTrigger
                (pWsscfgSetFsWtpImageUpgradeEntry,
                 pWsscfgIsSetFsWtpImageUpgradeEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsWtpImageUpgradeTable: "
                             " WsscfgSetAllFsWtpImageUpgradeTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsWtpImageUpgradeEntry);
            MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsWtpImageUpgradeEntry);
            MemReleaseMemBlock
                (WSSCFG_FSWTPIMAGEUPGRADETABLE_ISSET_POOLID, (UINT1 *)
                 pWsscfgTrgIsSetFsWtpImageUpgradeEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (WsscfgSetAllFsWtpImageUpgradeTableTrigger
                (pWsscfgSetFsWtpImageUpgradeEntry,
                 pWsscfgIsSetFsWtpImageUpgradeEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsWtpImageUpgradeTable: WsscfgSetAllFsWtpImageUpgradeTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsWtpImageUpgradeTable: Failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsWtpImageUpgradeEntry);
            MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsWtpImageUpgradeEntry);
            MemReleaseMemBlock
                (WSSCFG_FSWTPIMAGEUPGRADETABLE_ISSET_POOLID, (UINT1 *)
                 pWsscfgTrgIsSetFsWtpImageUpgradeEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pWsscfgSetFsWtpImageUpgradeEntry->
              MibObject.i4FsWtpRowStatus == CREATE_AND_WAIT)
             || (pWsscfgSetFsWtpImageUpgradeEntry->
                 MibObject.i4FsWtpRowStatus == CREATE_AND_GO))
    {
        if (WsscfgSetAllFsWtpImageUpgradeTableTrigger
            (pWsscfgSetFsWtpImageUpgradeEntry,
             pWsscfgIsSetFsWtpImageUpgradeEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsWtpImageUpgradeTable: WsscfgSetAllFsWtpImageUpgradeTableTrigger function returns failure.\r\n"));
        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsWtpImageUpgradeTable: The row is already present.\r\n"));
        MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsWtpImageUpgradeEntry);
        MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsWtpImageUpgradeEntry);
        MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsWtpImageUpgradeEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pWsscfgOldFsWtpImageUpgradeEntry,
            pWsscfgFsWtpImageUpgradeEntry,
            sizeof (tWsscfgFsWtpImageUpgradeEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpRowStatus == DESTROY)
    {
        pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpRowStatus = DESTROY;

        if (WsscfgUtilUpdateFsWtpImageUpgradeTable
            (pWsscfgOldFsWtpImageUpgradeEntry,
             pWsscfgFsWtpImageUpgradeEntry,
             pWsscfgIsSetFsWtpImageUpgradeEntry) != OSIX_SUCCESS)
        {

            if (WsscfgSetAllFsWtpImageUpgradeTableTrigger
                (pWsscfgSetFsWtpImageUpgradeEntry,
                 pWsscfgIsSetFsWtpImageUpgradeEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsWtpImageUpgradeTable: WsscfgSetAllFsWtpImageUpgradeTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsWtpImageUpgradeTable: WsscfgUtilUpdateFsWtpImageUpgradeTable function returns failure.\r\n"));
        }
        RBTreeRem (gWsscfgGlobals.WsscfgGlbMib.FsWtpImageUpgradeTable,
                   pWsscfgFsWtpImageUpgradeEntry);
        if (WsscfgSetAllFsWtpImageUpgradeTableTrigger
            (pWsscfgSetFsWtpImageUpgradeEntry,
             pWsscfgIsSetFsWtpImageUpgradeEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsWtpImageUpgradeTable: WsscfgSetAllFsWtpImageUpgradeTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsWtpImageUpgradeEntry);
            MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsWtpImageUpgradeEntry);
            MemReleaseMemBlock
                (WSSCFG_FSWTPIMAGEUPGRADETABLE_ISSET_POOLID, (UINT1 *)
                 pWsscfgTrgIsSetFsWtpImageUpgradeEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                            (UINT1 *) pWsscfgFsWtpImageUpgradeEntry);
        MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsWtpImageUpgradeEntry);
        MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsWtpImageUpgradeEntry);
        MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsWtpImageUpgradeEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsWtpImageUpgradeTableFilterInputs
        (pWsscfgFsWtpImageUpgradeEntry,
         pWsscfgSetFsWtpImageUpgradeEntry,
         pWsscfgIsSetFsWtpImageUpgradeEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsWtpImageUpgradeEntry);
        MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsWtpImageUpgradeEntry);
        MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsWtpImageUpgradeEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpRowStatus ==
         ACTIVE)
        && (pWsscfgSetFsWtpImageUpgradeEntry->MibObject.
            i4FsWtpRowStatus != NOT_IN_SERVICE))
    {
        pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pWsscfgTrgFsWtpImageUpgradeEntry->MibObject.i4FsWtpRowStatus =
            NOT_IN_SERVICE;
        pWsscfgTrgIsSetFsWtpImageUpgradeEntry->bFsWtpRowStatus = OSIX_TRUE;

        if (WsscfgUtilUpdateFsWtpImageUpgradeTable
            (pWsscfgOldFsWtpImageUpgradeEntry,
             pWsscfgFsWtpImageUpgradeEntry,
             pWsscfgIsSetFsWtpImageUpgradeEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pWsscfgFsWtpImageUpgradeEntry,
                    pWsscfgOldFsWtpImageUpgradeEntry,
                    sizeof (tWsscfgFsWtpImageUpgradeEntry));
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsWtpImageUpgradeTable:                 WsscfgUtilUpdateFsWtpImageUpgradeTable Function returns failure.\r\n"));

            if (WsscfgSetAllFsWtpImageUpgradeTableTrigger
                (pWsscfgSetFsWtpImageUpgradeEntry,
                 pWsscfgIsSetFsWtpImageUpgradeEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsWtpImageUpgradeTable: WsscfgSetAllFsWtpImageUpgradeTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsWtpImageUpgradeEntry);
            MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsWtpImageUpgradeEntry);
            MemReleaseMemBlock
                (WSSCFG_FSWTPIMAGEUPGRADETABLE_ISSET_POOLID, (UINT1 *)
                 pWsscfgTrgIsSetFsWtpImageUpgradeEntry);
            return OSIX_FAILURE;
        }

        if (WsscfgSetAllFsWtpImageUpgradeTableTrigger
            (pWsscfgTrgFsWtpImageUpgradeEntry,
             pWsscfgTrgIsSetFsWtpImageUpgradeEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsWtpImageUpgradeTable: WsscfgSetAllFsWtpImageUpgradeTableTrigger function returns failure.\r\n"));
        }
    }

    if (pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpImageVersion != OSIX_FALSE)
    {
        MEMCPY (pWsscfgFsWtpImageUpgradeEntry->
                MibObject.au1FsWtpImageVersion,
                pWsscfgSetFsWtpImageUpgradeEntry->
                MibObject.au1FsWtpImageVersion,
                pWsscfgSetFsWtpImageUpgradeEntry->
                MibObject.i4FsWtpImageVersionLen);

        pWsscfgFsWtpImageUpgradeEntry->MibObject.
            i4FsWtpImageVersionLen =
            pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpImageVersionLen;
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpUpgradeDev != OSIX_FALSE)
    {
        pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpUpgradeDev =
            pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpUpgradeDev;
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpName != OSIX_FALSE)
    {
        MEMSET (&pWsscfgFsWtpImageUpgradeEntry->MibObject.au1FsWtpName, 0, 256);
        pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpNameLen = 0;
        MEMCPY (pWsscfgFsWtpImageUpgradeEntry->MibObject.au1FsWtpName,
                pWsscfgSetFsWtpImageUpgradeEntry->MibObject.
                au1FsWtpName,
                pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpNameLen);

        pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpNameLen =
            pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpNameLen;
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpImageName != OSIX_FALSE)
    {
        MEMSET (&pWsscfgFsWtpImageUpgradeEntry->MibObject.au1FsWtpImageName, 0,
                256);
        MEMCPY (pWsscfgFsWtpImageUpgradeEntry->MibObject.au1FsWtpImageName,
                pWsscfgSetFsWtpImageUpgradeEntry->MibObject.au1FsWtpImageName,
                pWsscfgSetFsWtpImageUpgradeEntry->MibObject.
                i4FsWtpImageNameLen);

        pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpImageNameLen =
            pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpImageNameLen;
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpAddressType != OSIX_FALSE)
    {
        pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpAddressType =
            pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpAddressType;
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpServerIP != OSIX_FALSE)
    {
        MEMCPY (pWsscfgFsWtpImageUpgradeEntry->MibObject.
                au1FsWtpServerIP,
                pWsscfgSetFsWtpImageUpgradeEntry->
                MibObject.au1FsWtpServerIP,
                pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpServerIPLen);

        pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpServerIPLen =
            pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpServerIPLen;
    }
    if (pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpRowStatus != OSIX_FALSE)
    {
        pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpRowStatus =
            pWsscfgSetFsWtpImageUpgradeEntry->MibObject.i4FsWtpRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpRowStatus = ACTIVE;
    }

    if (WsscfgUtilUpdateFsWtpImageUpgradeTable
        (pWsscfgOldFsWtpImageUpgradeEntry,
         pWsscfgFsWtpImageUpgradeEntry,
         pWsscfgIsSetFsWtpImageUpgradeEntry) != OSIX_SUCCESS)
    {

        if (WsscfgSetAllFsWtpImageUpgradeTableTrigger
            (pWsscfgSetFsWtpImageUpgradeEntry,
             pWsscfgIsSetFsWtpImageUpgradeEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsWtpImageUpgradeTable: WsscfgSetAllFsWtpImageUpgradeTableTrigger function returns failure.\r\n"));

        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsWtpImageUpgradeTable: WsscfgUtilUpdateFsWtpImageUpgradeTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pWsscfgFsWtpImageUpgradeEntry,
                pWsscfgOldFsWtpImageUpgradeEntry,
                sizeof (tWsscfgFsWtpImageUpgradeEntry));
        MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsWtpImageUpgradeEntry);
        MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsWtpImageUpgradeEntry);
        MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsWtpImageUpgradeEntry);
        return OSIX_FAILURE;

    }
    if (WsscfgSetAllFsWtpImageUpgradeTableTrigger
        (pWsscfgSetFsWtpImageUpgradeEntry,
         pWsscfgIsSetFsWtpImageUpgradeEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsWtpImageUpgradeTable: WsscfgSetAllFsWtpImageUpgradeTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                        (UINT1 *) pWsscfgOldFsWtpImageUpgradeEntry);
    MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID,
                        (UINT1 *) pWsscfgTrgFsWtpImageUpgradeEntry);
    MemReleaseMemBlock (WSSCFG_FSWTPIMAGEUPGRADETABLE_ISSET_POOLID,
                        (UINT1 *) pWsscfgTrgIsSetFsWtpImageUpgradeEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgGetAllFsRrmConfigTable
 Input       :  pWsscfgGetFsRrmConfigEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgGetAllFsRrmConfigTable (tWsscfgFsRrmConfigEntry *
                              pWsscfgGetFsRrmConfigEntry)
{
    tWsscfgFsRrmConfigEntry *pWsscfgFsRrmConfigEntry = NULL;

    /* Check whether the node is already present */
    pWsscfgFsRrmConfigEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsRrmConfigTable,
                   (tRBElem *) pWsscfgGetFsRrmConfigEntry);

    if (pWsscfgFsRrmConfigEntry == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgGetAllFsRrmConfigTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pWsscfgGetFsRrmConfigEntry->MibObject.i4FsRrmDcaMode =
        pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmDcaMode;

    pWsscfgGetFsRrmConfigEntry->MibObject.
        i4FsRrmDcaChannelSelectionMode =
        pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmDcaChannelSelectionMode;

    pWsscfgGetFsRrmConfigEntry->MibObject.i4FsRrmTpcMode =
        pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmTpcMode;

    pWsscfgGetFsRrmConfigEntry->MibObject.i4FsRrmTpcSelectionMode =
        pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmTpcSelectionMode;

    pWsscfgGetFsRrmConfigEntry->MibObject.i4FsRrmRowStatus =
        pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmRowStatus;

    if (WsscfgGetAllUtlFsRrmConfigTable
        (pWsscfgGetFsRrmConfigEntry, pWsscfgFsRrmConfigEntry) == OSIX_FAILURE)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC, "WsscfgGetAllFsRrmConfigTable:"
                     "WsscfgGetAllUtlFsRrmConfigTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WsscfgSetAllFsRrmConfigTable
 Input       :  pWsscfgSetFsRrmConfigEntry
                pWsscfgIsSetFsRrmConfigEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllFsRrmConfigTable (tWsscfgFsRrmConfigEntry *
                              pWsscfgSetFsRrmConfigEntry,
                              tWsscfgIsSetFsRrmConfigEntry *
                              pWsscfgIsSetFsRrmConfigEntry,
                              INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tWsscfgFsRrmConfigEntry *pWsscfgFsRrmConfigEntry = NULL;
    tWsscfgFsRrmConfigEntry *pWsscfgOldFsRrmConfigEntry = NULL;
    tWsscfgFsRrmConfigEntry *pWsscfgTrgFsRrmConfigEntry = NULL;
    tWsscfgIsSetFsRrmConfigEntry *pWsscfgTrgIsSetFsRrmConfigEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pWsscfgOldFsRrmConfigEntry = (tWsscfgFsRrmConfigEntry *)
        MemAllocMemBlk (WSSCFG_FSRRMCONFIGTABLE_POOLID);
    if (pWsscfgOldFsRrmConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pWsscfgTrgFsRrmConfigEntry = (tWsscfgFsRrmConfigEntry *)
        MemAllocMemBlk (WSSCFG_FSRRMCONFIGTABLE_POOLID);
    if (pWsscfgTrgFsRrmConfigEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsRrmConfigEntry);
        return OSIX_FAILURE;
    }
    pWsscfgTrgIsSetFsRrmConfigEntry = (tWsscfgIsSetFsRrmConfigEntry *)
        MemAllocMemBlk (WSSCFG_FSRRMCONFIGTABLE_ISSET_POOLID);
    if (pWsscfgTrgIsSetFsRrmConfigEntry == NULL)
    {
        MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsRrmConfigEntry);
        MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsRrmConfigEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pWsscfgOldFsRrmConfigEntry, 0, sizeof (tWsscfgFsRrmConfigEntry));
    MEMSET (pWsscfgTrgFsRrmConfigEntry, 0, sizeof (tWsscfgFsRrmConfigEntry));
    MEMSET (pWsscfgTrgIsSetFsRrmConfigEntry, 0,
            sizeof (tWsscfgIsSetFsRrmConfigEntry));

    /* Check whether the node is already present */
    pWsscfgFsRrmConfigEntry =
        RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.FsRrmConfigTable,
                   (tRBElem *) pWsscfgSetFsRrmConfigEntry);

    if (pWsscfgFsRrmConfigEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmRowStatus ==
             CREATE_AND_WAIT)
            || (pWsscfgSetFsRrmConfigEntry->MibObject.
                i4FsRrmRowStatus == CREATE_AND_GO)
            ||
            ((pWsscfgSetFsRrmConfigEntry->MibObject.
              i4FsRrmRowStatus == ACTIVE) && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pWsscfgFsRrmConfigEntry = (tWsscfgFsRrmConfigEntry *)
                MemAllocMemBlk (WSSCFG_FSRRMCONFIGTABLE_POOLID);
            if (pWsscfgFsRrmConfigEntry == NULL)
            {
                if (WsscfgSetAllFsRrmConfigTableTrigger
                    (pWsscfgSetFsRrmConfigEntry,
                     pWsscfgIsSetFsRrmConfigEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsRrmConfigTable:WsscfgSetAllFsRrmConfigTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsRrmConfigTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                    (UINT1 *) pWsscfgOldFsRrmConfigEntry);
                MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                    (UINT1 *) pWsscfgTrgFsRrmConfigEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSRRMCONFIGTABLE_ISSET_POOLID, (UINT1 *)
                     pWsscfgTrgIsSetFsRrmConfigEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pWsscfgFsRrmConfigEntry, 0,
                    sizeof (tWsscfgFsRrmConfigEntry));
            if ((WsscfgInitializeFsRrmConfigTable
                 (pWsscfgFsRrmConfigEntry)) == OSIX_FAILURE)
            {
                if (WsscfgSetAllFsRrmConfigTableTrigger
                    (pWsscfgSetFsRrmConfigEntry,
                     pWsscfgIsSetFsRrmConfigEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsRrmConfigTable:WsscfgSetAllFsRrmConfigTableTrigger function fails\r\n"));

                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsRrmConfigTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                    (UINT1 *) pWsscfgFsRrmConfigEntry);
                MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                    (UINT1 *) pWsscfgOldFsRrmConfigEntry);
                MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                    (UINT1 *) pWsscfgTrgFsRrmConfigEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSRRMCONFIGTABLE_ISSET_POOLID, (UINT1 *)
                     pWsscfgTrgIsSetFsRrmConfigEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmRadioType != OSIX_FALSE)
            {
                pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmRadioType =
                    pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmRadioType;
            }

            if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmDcaMode != OSIX_FALSE)
            {
                pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmDcaMode =
                    pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmDcaMode;
            }

            if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmDcaChannelSelectionMode !=
                OSIX_FALSE)
            {
                pWsscfgFsRrmConfigEntry->MibObject.
                    i4FsRrmDcaChannelSelectionMode =
                    pWsscfgSetFsRrmConfigEntry->MibObject.
                    i4FsRrmDcaChannelSelectionMode;
            }

            if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmTpcMode != OSIX_FALSE)
            {
                pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmTpcMode =
                    pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmTpcMode;
            }

            if (pWsscfgIsSetFsRrmConfigEntry->
                bFsRrmTpcSelectionMode != OSIX_FALSE)
            {
                pWsscfgFsRrmConfigEntry->
                    MibObject.i4FsRrmTpcSelectionMode =
                    pWsscfgSetFsRrmConfigEntry->MibObject.
                    i4FsRrmTpcSelectionMode;
            }

            if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmRowStatus != OSIX_FALSE)
            {
                pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmRowStatus =
                    pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmRowStatus;
            }

            if ((pWsscfgSetFsRrmConfigEntry->MibObject.
                 i4FsRrmRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsRrmConfigEntry->MibObject.
                        i4FsRrmRowStatus == ACTIVE)))
            {
                pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmRowStatus = ACTIVE;
            }
            else if (pWsscfgSetFsRrmConfigEntry->
                     MibObject.i4FsRrmRowStatus == CREATE_AND_WAIT)
            {
                pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gWsscfgGlobals.WsscfgGlbMib.FsRrmConfigTable,
                 (tRBElem *) pWsscfgFsRrmConfigEntry) != RB_SUCCESS)
            {
                if (WsscfgSetAllFsRrmConfigTableTrigger
                    (pWsscfgSetFsRrmConfigEntry,
                     pWsscfgIsSetFsRrmConfigEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsRrmConfigTable: WsscfgSetAllFsRrmConfigTableTrigger function returns failure.\r\n"));
                }
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsRrmConfigTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                    (UINT1 *) pWsscfgFsRrmConfigEntry);
                MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                    (UINT1 *) pWsscfgOldFsRrmConfigEntry);
                MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                    (UINT1 *) pWsscfgTrgFsRrmConfigEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSRRMCONFIGTABLE_ISSET_POOLID, (UINT1 *)
                     pWsscfgTrgIsSetFsRrmConfigEntry);
                return OSIX_FAILURE;
            }
            if (WsscfgUtilUpdateFsRrmConfigTable
                (NULL, pWsscfgFsRrmConfigEntry,
                 pWsscfgIsSetFsRrmConfigEntry) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsRrmConfigTable: WsscfgUtilUpdateFsRrmConfigTable function returns failure.\r\n"));

                if (WsscfgSetAllFsRrmConfigTableTrigger
                    (pWsscfgSetFsRrmConfigEntry,
                     pWsscfgIsSetFsRrmConfigEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsRrmConfigTable: WsscfgSetAllFsRrmConfigTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gWsscfgGlobals.WsscfgGlbMib.
                           FsRrmConfigTable, pWsscfgFsRrmConfigEntry);
                MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                    (UINT1 *) pWsscfgFsRrmConfigEntry);
                MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                    (UINT1 *) pWsscfgOldFsRrmConfigEntry);
                MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                    (UINT1 *) pWsscfgTrgFsRrmConfigEntry);
                MemReleaseMemBlock
                    (WSSCFG_FSRRMCONFIGTABLE_ISSET_POOLID, (UINT1 *)
                     pWsscfgTrgIsSetFsRrmConfigEntry);
                return OSIX_FAILURE;
            }

            if ((pWsscfgSetFsRrmConfigEntry->MibObject.
                 i4FsRrmRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pWsscfgSetFsRrmConfigEntry->MibObject.
                        i4FsRrmRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsRrmConfigEntry->MibObject.
                    i4FsRrmRadioType =
                    pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmRadioType;
                pWsscfgTrgFsRrmConfigEntry->MibObject.
                    i4FsRrmRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsRrmConfigEntry->bFsRrmRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsRrmConfigTableTrigger
                    (pWsscfgTrgFsRrmConfigEntry,
                     pWsscfgTrgIsSetFsRrmConfigEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsRrmConfigTable: WsscfgSetAllFsRrmConfigTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                        (UINT1 *) pWsscfgFsRrmConfigEntry);
                    MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                        (UINT1 *) pWsscfgOldFsRrmConfigEntry);
                    MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                        (UINT1 *) pWsscfgTrgFsRrmConfigEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSRRMCONFIGTABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetFsRrmConfigEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pWsscfgSetFsRrmConfigEntry->
                     MibObject.i4FsRrmRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pWsscfgTrgFsRrmConfigEntry->MibObject.
                    i4FsRrmRowStatus = CREATE_AND_WAIT;
                pWsscfgTrgIsSetFsRrmConfigEntry->bFsRrmRowStatus = OSIX_TRUE;

                if (WsscfgSetAllFsRrmConfigTableTrigger
                    (pWsscfgTrgFsRrmConfigEntry,
                     pWsscfgTrgIsSetFsRrmConfigEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                                 "WsscfgSetAllFsRrmConfigTable: WsscfgSetAllFsRrmConfigTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                        (UINT1 *) pWsscfgFsRrmConfigEntry);
                    MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                        (UINT1 *) pWsscfgOldFsRrmConfigEntry);
                    MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                        (UINT1 *) pWsscfgTrgFsRrmConfigEntry);
                    MemReleaseMemBlock
                        (WSSCFG_FSRRMCONFIGTABLE_ISSET_POOLID,
                         (UINT1 *) pWsscfgTrgIsSetFsRrmConfigEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pWsscfgSetFsRrmConfigEntry->MibObject.
                i4FsRrmRowStatus == CREATE_AND_GO)
            {
                pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmRowStatus = ACTIVE;
            }

            if (WsscfgSetAllFsRrmConfigTableTrigger
                (pWsscfgSetFsRrmConfigEntry,
                 pWsscfgIsSetFsRrmConfigEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsRrmConfigTable:  WsscfgSetAllFsRrmConfigTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsRrmConfigEntry);
            MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsRrmConfigEntry);
            MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_ISSET_POOLID,
                                (UINT1 *) pWsscfgTrgIsSetFsRrmConfigEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (WsscfgSetAllFsRrmConfigTableTrigger
                (pWsscfgSetFsRrmConfigEntry,
                 pWsscfgIsSetFsRrmConfigEntry, SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsRrmConfigTable: WsscfgSetAllFsRrmConfigTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsRrmConfigTable: Failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsRrmConfigEntry);
            MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsRrmConfigEntry);
            MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_ISSET_POOLID,
                                (UINT1 *) pWsscfgTrgIsSetFsRrmConfigEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmRowStatus ==
              CREATE_AND_WAIT)
             || (pWsscfgSetFsRrmConfigEntry->
                 MibObject.i4FsRrmRowStatus == CREATE_AND_GO))
    {
        if (WsscfgSetAllFsRrmConfigTableTrigger
            (pWsscfgSetFsRrmConfigEntry, pWsscfgIsSetFsRrmConfigEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsRrmConfigTable: WsscfgSetAllFsRrmConfigTableTrigger function returns failure.\r\n"));
        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsRrmConfigTable: The row is already present.\r\n"));
        MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsRrmConfigEntry);
        MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsRrmConfigEntry);
        MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsRrmConfigEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pWsscfgOldFsRrmConfigEntry, pWsscfgFsRrmConfigEntry,
            sizeof (tWsscfgFsRrmConfigEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmRowStatus == DESTROY)
    {
        pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmRowStatus = DESTROY;

        if (WsscfgUtilUpdateFsRrmConfigTable
            (pWsscfgOldFsRrmConfigEntry, pWsscfgFsRrmConfigEntry,
             pWsscfgIsSetFsRrmConfigEntry) != OSIX_SUCCESS)
        {

            if (WsscfgSetAllFsRrmConfigTableTrigger
                (pWsscfgSetFsRrmConfigEntry,
                 pWsscfgIsSetFsRrmConfigEntry, SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsRrmConfigTable: WsscfgSetAllFsRrmConfigTableTrigger function returns failure.\r\n"));
            }
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsRrmConfigTable: WsscfgUtilUpdateFsRrmConfigTable function returns failure.\r\n"));
        }
        RBTreeRem (gWsscfgGlobals.WsscfgGlbMib.FsRrmConfigTable,
                   pWsscfgFsRrmConfigEntry);
        if (WsscfgSetAllFsRrmConfigTableTrigger
            (pWsscfgSetFsRrmConfigEntry, pWsscfgIsSetFsRrmConfigEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsRrmConfigTable: WsscfgSetAllFsRrmConfigTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsRrmConfigEntry);
            MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsRrmConfigEntry);
            MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_ISSET_POOLID,
                                (UINT1 *) pWsscfgTrgIsSetFsRrmConfigEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgFsRrmConfigEntry);
        MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsRrmConfigEntry);
        MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsRrmConfigEntry);
        MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsRrmConfigEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsRrmConfigTableFilterInputs
        (pWsscfgFsRrmConfigEntry, pWsscfgSetFsRrmConfigEntry,
         pWsscfgIsSetFsRrmConfigEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsRrmConfigEntry);
        MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsRrmConfigEntry);
        MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsRrmConfigEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmRowStatus ==
         ACTIVE)
        && (pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmRowStatus !=
            NOT_IN_SERVICE))
    {
        pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pWsscfgTrgFsRrmConfigEntry->MibObject.i4FsRrmRowStatus = NOT_IN_SERVICE;
        pWsscfgTrgIsSetFsRrmConfigEntry->bFsRrmRowStatus = OSIX_TRUE;

        if (WsscfgUtilUpdateFsRrmConfigTable
            (pWsscfgOldFsRrmConfigEntry, pWsscfgFsRrmConfigEntry,
             pWsscfgIsSetFsRrmConfigEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pWsscfgFsRrmConfigEntry,
                    pWsscfgOldFsRrmConfigEntry,
                    sizeof (tWsscfgFsRrmConfigEntry));
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsRrmConfigTable:                 WsscfgUtilUpdateFsRrmConfigTable Function returns failure.\r\n"));

            if (WsscfgSetAllFsRrmConfigTableTrigger
                (pWsscfgSetFsRrmConfigEntry,
                 pWsscfgIsSetFsRrmConfigEntry, SNMP_FAILURE) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "WsscfgSetAllFsRrmConfigTable: WsscfgSetAllFsRrmConfigTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                (UINT1 *) pWsscfgOldFsRrmConfigEntry);
            MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                                (UINT1 *) pWsscfgTrgFsRrmConfigEntry);
            MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_ISSET_POOLID,
                                (UINT1 *) pWsscfgTrgIsSetFsRrmConfigEntry);
            return OSIX_FAILURE;
        }

        if (WsscfgSetAllFsRrmConfigTableTrigger
            (pWsscfgTrgFsRrmConfigEntry,
             pWsscfgTrgIsSetFsRrmConfigEntry, SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsRrmConfigTable: WsscfgSetAllFsRrmConfigTableTrigger function returns failure.\r\n"));
        }
    }

    if (pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmDcaMode != OSIX_FALSE)
    {
        pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmDcaMode =
            pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmDcaMode;
    }
    if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmDcaChannelSelectionMode !=
        OSIX_FALSE)
    {
        pWsscfgFsRrmConfigEntry->MibObject.
            i4FsRrmDcaChannelSelectionMode =
            pWsscfgSetFsRrmConfigEntry->MibObject.
            i4FsRrmDcaChannelSelectionMode;
    }
    if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmTpcMode != OSIX_FALSE)
    {
        pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmTpcMode =
            pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmTpcMode;
    }
    if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmTpcSelectionMode != OSIX_FALSE)
    {
        pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmTpcSelectionMode =
            pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmTpcSelectionMode;
    }
    if (pWsscfgIsSetFsRrmConfigEntry->bFsRrmRowStatus != OSIX_FALSE)
    {
        pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmRowStatus =
            pWsscfgSetFsRrmConfigEntry->MibObject.i4FsRrmRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmRowStatus = ACTIVE;
    }

    if (WsscfgUtilUpdateFsRrmConfigTable (pWsscfgOldFsRrmConfigEntry,
                                          pWsscfgFsRrmConfigEntry,
                                          pWsscfgIsSetFsRrmConfigEntry)
        != OSIX_SUCCESS)
    {

        if (WsscfgSetAllFsRrmConfigTableTrigger
            (pWsscfgSetFsRrmConfigEntry, pWsscfgIsSetFsRrmConfigEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WsscfgSetAllFsRrmConfigTable: WsscfgSetAllFsRrmConfigTableTrigger function returns failure.\r\n"));

        }
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsRrmConfigTable: WsscfgUtilUpdateFsRrmConfigTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pWsscfgFsRrmConfigEntry, pWsscfgOldFsRrmConfigEntry,
                sizeof (tWsscfgFsRrmConfigEntry));
        MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgOldFsRrmConfigEntry);
        MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                            (UINT1 *) pWsscfgTrgFsRrmConfigEntry);
        MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_ISSET_POOLID,
                            (UINT1 *) pWsscfgTrgIsSetFsRrmConfigEntry);
        return OSIX_FAILURE;

    }
    if (WsscfgSetAllFsRrmConfigTableTrigger
        (pWsscfgSetFsRrmConfigEntry, pWsscfgIsSetFsRrmConfigEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WsscfgSetAllFsRrmConfigTable: WsscfgSetAllFsRrmConfigTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                        (UINT1 *) pWsscfgOldFsRrmConfigEntry);
    MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_POOLID,
                        (UINT1 *) pWsscfgTrgFsRrmConfigEntry);
    MemReleaseMemBlock (WSSCFG_FSRRMCONFIGTABLE_ISSET_POOLID,
                        (UINT1 *) pWsscfgTrgIsSetFsRrmConfigEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  WsscfgSetAllFsDot11ExternalWebAuthProfileTable
 Input       :  pWsscfgSetFsDot11ExternalWebAuthProfileEntry
                pWsscfgIsSetFsDot11ExternalWebAuthProfileEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetAllFsDot11ExternalWebAuthProfileTable
    (tWsscfgFsDot11ExternalWebAuthProfileEntry *
     pWsscfgSetFsDot11ExternalWebAuthProfileEntry,
     tWsscfgIsSetFsDot11ExternalWebAuthProfileEntry *
     pWsscfgIsSetFsDot11ExternalWebAuthProfileEntry)
{
    if (WsscfgUtilUpdateFsDot11ExternalWebAuthProfileTable
        (pWsscfgSetFsDot11ExternalWebAuthProfileEntry,
         pWsscfgIsSetFsDot11ExternalWebAuthProfileEntry) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
