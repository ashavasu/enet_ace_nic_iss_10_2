/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fs11acwr.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
 *
 * Description: Protocol Low Level Routines
 *********************************************************************/

# include  "lr.h" 
# include  "fssnmp.h" 
# include  "fs11aclw.h"
# include  "fs11acwr.h"
# include  "fs11acdb.h"

INT4 GetNextIndexFsDot11ACConfigTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsDot11ACConfigTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsDot11ACConfigTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}


VOID RegisterFS11AC ()
{
	SNMPRegisterMib (&fs11acOID, &fs11acEntry, SNMP_MSR_TGR_FALSE);
	SNMPAddSysorEntry (&fs11acOID, (const UINT1 *) "fs11ac");
}



VOID UnRegisterFS11AC ()
{
	SNMPUnRegisterMib (&fs11acOID, &fs11acEntry);
	SNMPDelSysorEntry (&fs11acOID, (const UINT1 *) "fs11ac");
}

INT4 FsDot11ACMaxMPDULengthGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACMaxMPDULength(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACMaxMPDULengthConfigGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACMaxMPDULengthConfig(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACVHTMaxRxAMPDUFactorGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTMaxRxAMPDUFactor(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11ACVHTMaxRxAMPDUFactorConfigGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTMaxRxAMPDUFactorConfig(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11ACVHTControlFieldSupportedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTControlFieldSupported(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACVHTTXOPPowerSaveOptionImplementedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTTXOPPowerSaveOptionImplemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACVHTRxMCSMapGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTRxMCSMap(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsDot11ACVHTRxHighestDataRateSupportedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTRxHighestDataRateSupported(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11ACVHTTxMCSMapGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTTxMCSMap(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsDot11ACVHTTxHighestDataRateSupportedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTTxHighestDataRateSupported(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11ACVHTOBSSScanCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTOBSSScanCount(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11ACCurrentChannelBandwidthGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACCurrentChannelBandwidth(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACCurrentChannelBandwidthConfigGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACCurrentChannelBandwidthConfig(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACCurrentChannelCenterFrequencyIndex0Get(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACCurrentChannelCenterFrequencyIndex0(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11ACCurrentChannelCenterFrequencyIndex0ConfigGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACCurrentChannelCenterFrequencyIndex0Config(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11ACCurrentChannelCenterFrequencyIndex1Get(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACCurrentChannelCenterFrequencyIndex1(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11ACVHTShortGIOptionIn80ImplementedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTShortGIOptionIn80Implemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACVHTShortGIOptionIn80ActivatedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTShortGIOptionIn80Activated(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACVHTShortGIOptionIn160and80p80ImplementedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTShortGIOptionIn160and80p80Implemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACVHTShortGIOptionIn160and80p80ActivatedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTShortGIOptionIn160and80p80Activated(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACVHTLDPCCodingOptionImplementedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTLDPCCodingOptionImplemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACVHTLDPCCodingOptionActivatedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTLDPCCodingOptionActivated(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACVHTTxSTBCOptionImplementedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTTxSTBCOptionImplemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACVHTTxSTBCOptionActivatedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTTxSTBCOptionActivated(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACVHTRxSTBCOptionImplementedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTRxSTBCOptionImplemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACVHTRxSTBCOptionActivatedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTRxSTBCOptionActivated(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACVHTMUMaxUsersImplementedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTMUMaxUsersImplemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11ACVHTMUMaxNSTSPerUserImplementedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTMUMaxNSTSPerUserImplemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11ACVHTMUMaxNSTSTotalImplementedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTMUMaxNSTSTotalImplemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11ACSuBeamFormerGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACSuBeamFormer(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACSuBeamFormeeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACSuBeamFormee(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACMuBeamFormerGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACMuBeamFormer(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACMuBeamFormeeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACMuBeamFormee(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACVHTLinkAdaptionGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTLinkAdaption(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACRxAntennaPatternGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACRxAntennaPattern(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACTxAntennaPatternGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACTxAntennaPattern(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ACBasicMCSMapGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACBasicMCSMap(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsDot11ACVHTRxMCSMapConfigGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTRxMCSMapConfig(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsDot11ACVHTTxMCSMapConfigGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACVHTTxMCSMapConfig(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 FsDot11ACCurrentChannelCenterFrequencyIndex1ConfigGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ACCurrentChannelCenterFrequencyIndex1Config(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11VHTOptionImplementedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11VHTOptionImplemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11OperatingModeNotificationImplementedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11OperatingModeNotificationImplemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11ExtendedChannelSwitchActivatedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11ExtendedChannelSwitchActivated(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDot11FragmentationThresholdGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11ACConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11FragmentationThreshold(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11ACMaxMPDULengthSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACMaxMPDULength(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACMaxMPDULengthConfigSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACMaxMPDULengthConfig(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTMaxRxAMPDUFactorSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTMaxRxAMPDUFactor(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACVHTMaxRxAMPDUFactorConfigSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTMaxRxAMPDUFactorConfig(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACVHTControlFieldSupportedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTControlFieldSupported(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTTXOPPowerSaveOptionImplementedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTTXOPPowerSaveOptionImplemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTRxMCSMapSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTRxMCSMap(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 FsDot11ACVHTRxHighestDataRateSupportedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTRxHighestDataRateSupported(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACVHTTxMCSMapSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTTxMCSMap(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 FsDot11ACVHTTxHighestDataRateSupportedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTTxHighestDataRateSupported(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACVHTOBSSScanCountSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTOBSSScanCount(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACCurrentChannelBandwidthSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACCurrentChannelBandwidth(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACCurrentChannelBandwidthConfigSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACCurrentChannelBandwidthConfig(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACCurrentChannelCenterFrequencyIndex0Set(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACCurrentChannelCenterFrequencyIndex0(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACCurrentChannelCenterFrequencyIndex0ConfigSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACCurrentChannelCenterFrequencyIndex0Config(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACCurrentChannelCenterFrequencyIndex1Set(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACCurrentChannelCenterFrequencyIndex1(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACVHTShortGIOptionIn80ImplementedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTShortGIOptionIn80Implemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTShortGIOptionIn80ActivatedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTShortGIOptionIn80Activated(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTShortGIOptionIn160and80p80ImplementedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTShortGIOptionIn160and80p80Implemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTShortGIOptionIn160and80p80ActivatedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTShortGIOptionIn160and80p80Activated(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTLDPCCodingOptionImplementedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTLDPCCodingOptionImplemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTLDPCCodingOptionActivatedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTLDPCCodingOptionActivated(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTTxSTBCOptionImplementedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTTxSTBCOptionImplemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTTxSTBCOptionActivatedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTTxSTBCOptionActivated(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTRxSTBCOptionImplementedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTRxSTBCOptionImplemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTRxSTBCOptionActivatedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTRxSTBCOptionActivated(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTMUMaxUsersImplementedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTMUMaxUsersImplemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACVHTMUMaxNSTSPerUserImplementedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTMUMaxNSTSPerUserImplemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACVHTMUMaxNSTSTotalImplementedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTMUMaxNSTSTotalImplemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACSuBeamFormerSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACSuBeamFormer(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACSuBeamFormeeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACSuBeamFormee(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACMuBeamFormerSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACMuBeamFormer(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACMuBeamFormeeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACMuBeamFormee(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTLinkAdaptionSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTLinkAdaption(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACRxAntennaPatternSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACRxAntennaPattern(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACTxAntennaPatternSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACTxAntennaPattern(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACBasicMCSMapSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACBasicMCSMap(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 FsDot11ACVHTRxMCSMapConfigSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTRxMCSMapConfig(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 FsDot11ACVHTTxMCSMapConfigSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACVHTTxMCSMapConfig(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 FsDot11ACCurrentChannelCenterFrequencyIndex1ConfigSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ACCurrentChannelCenterFrequencyIndex1Config(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11VHTOptionImplementedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11VHTOptionImplemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11OperatingModeNotificationImplementedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11OperatingModeNotificationImplemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ExtendedChannelSwitchActivatedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11ExtendedChannelSwitchActivated(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11FragmentationThresholdSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11FragmentationThreshold(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACMaxMPDULengthTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACMaxMPDULength(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACMaxMPDULengthConfigTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACMaxMPDULengthConfig(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTMaxRxAMPDUFactorTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTMaxRxAMPDUFactor(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACVHTMaxRxAMPDUFactorConfigTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTMaxRxAMPDUFactorConfig(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACVHTControlFieldSupportedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTControlFieldSupported(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTTXOPPowerSaveOptionImplementedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTTXOPPowerSaveOptionImplemented(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTRxMCSMapTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTRxMCSMap(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 FsDot11ACVHTRxHighestDataRateSupportedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTRxHighestDataRateSupported(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACVHTTxMCSMapTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTTxMCSMap(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 FsDot11ACVHTTxHighestDataRateSupportedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTTxHighestDataRateSupported(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACVHTOBSSScanCountTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTOBSSScanCount(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACCurrentChannelBandwidthTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACCurrentChannelBandwidth(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACCurrentChannelBandwidthConfigTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACCurrentChannelBandwidthConfig(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACCurrentChannelCenterFrequencyIndex0Test(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACCurrentChannelCenterFrequencyIndex0(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACCurrentChannelCenterFrequencyIndex0ConfigTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACCurrentChannelCenterFrequencyIndex0Config(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACCurrentChannelCenterFrequencyIndex1Test(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACCurrentChannelCenterFrequencyIndex1(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACVHTShortGIOptionIn80ImplementedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTShortGIOptionIn80Implemented(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTShortGIOptionIn80ActivatedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTShortGIOptionIn80Activated(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTShortGIOptionIn160and80p80ImplementedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTShortGIOptionIn160and80p80Implemented(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTShortGIOptionIn160and80p80ActivatedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTShortGIOptionIn160and80p80Activated(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTLDPCCodingOptionImplementedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTLDPCCodingOptionImplemented(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTLDPCCodingOptionActivatedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTLDPCCodingOptionActivated(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTTxSTBCOptionImplementedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTTxSTBCOptionImplemented(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTTxSTBCOptionActivatedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTTxSTBCOptionActivated(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTRxSTBCOptionImplementedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTRxSTBCOptionImplemented(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTRxSTBCOptionActivatedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTRxSTBCOptionActivated(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTMUMaxUsersImplementedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTMUMaxUsersImplemented(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACVHTMUMaxNSTSPerUserImplementedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTMUMaxNSTSPerUserImplemented(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACVHTMUMaxNSTSTotalImplementedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTMUMaxNSTSTotalImplemented(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACSuBeamFormerTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACSuBeamFormer(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACSuBeamFormeeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACSuBeamFormee(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACMuBeamFormerTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACMuBeamFormer(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACMuBeamFormeeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACMuBeamFormee(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACVHTLinkAdaptionTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTLinkAdaption(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACRxAntennaPatternTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACRxAntennaPattern(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACTxAntennaPatternTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACTxAntennaPattern(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ACBasicMCSMapTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACBasicMCSMap(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 FsDot11ACVHTRxMCSMapConfigTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTRxMCSMapConfig(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 FsDot11ACVHTTxMCSMapConfigTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACVHTTxMCSMapConfig(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 FsDot11ACCurrentChannelCenterFrequencyIndex1ConfigTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ACCurrentChannelCenterFrequencyIndex1Config(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11VHTOptionImplementedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11VHTOptionImplemented(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11OperatingModeNotificationImplementedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11OperatingModeNotificationImplemented(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11ExtendedChannelSwitchActivatedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11ExtendedChannelSwitchActivated(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsDot11FragmentationThresholdTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11FragmentationThreshold(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11ACConfigTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsDot11ACConfigTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsDot11VHTStationConfigTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsDot11VHTStationConfigTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsDot11VHTStationConfigTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsDot11VHTRxHighestDataRateConfigGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11VHTStationConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11VHTRxHighestDataRateConfig(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11VHTTxHighestDataRateConfigGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11VHTStationConfigTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11VHTTxHighestDataRateConfig(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11VHTRxHighestDataRateConfigSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11VHTRxHighestDataRateConfig(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11VHTTxHighestDataRateConfigSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11VHTTxHighestDataRateConfig(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11VHTRxHighestDataRateConfigTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11VHTRxHighestDataRateConfig(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11VHTTxHighestDataRateConfigTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11VHTTxHighestDataRateConfig(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11VHTStationConfigTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsDot11VHTStationConfigTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsDot11PhyVHTTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsDot11PhyVHTTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsDot11PhyVHTTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsDot11NumberOfSpatialStreamsImplementedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11PhyVHTTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11NumberOfSpatialStreamsImplemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11NumberOfSpatialStreamsActivatedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDot11PhyVHTTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDot11NumberOfSpatialStreamsActivated(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDot11NumberOfSpatialStreamsImplementedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11NumberOfSpatialStreamsImplemented(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11NumberOfSpatialStreamsActivatedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDot11NumberOfSpatialStreamsActivated(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11NumberOfSpatialStreamsImplementedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11NumberOfSpatialStreamsImplemented(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11NumberOfSpatialStreamsActivatedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDot11NumberOfSpatialStreamsActivated(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsDot11PhyVHTTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsDot11PhyVHTTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

