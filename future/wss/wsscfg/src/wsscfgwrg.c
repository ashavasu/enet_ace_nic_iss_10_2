/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
 *  $Id: wsscfgwrg.c,v 1.3.2.1 2018/03/19 14:21:10 siva Exp $
*
* Description: This file contains the wrapperfunctions for Wsscfg 
*********************************************************************/

#include "lr.h"
#include "fssnmp.h"
#ifdef RSNA_WANTED
#include "pnac.h"
#endif
#include "wsscfgtdfsg.h"
#include "wsscfgtdfs.h"
#include "wsscfglwg.h"
#include "wsscfgwrg.h"
#include "wsscfgprotg.h"
#include "wsscfgtmr.h"
#include "wsscfgprot.h"
#include "stdwladb.h"
#include "fswlandb.h"
#include "fsrrmdb.h"

VOID
RegisterSTDWLA ()
{
    SNMPRegisterMibWithLock (&stdwlaOID, &stdwlaEntry,
                             WsscfgMainTaskLock, WsscfgMainTaskUnLock,
                             SNMP_MSR_TGR_TRUE);

    SNMPAddSysorEntry (&stdwlaOID, (const UINT1 *) "stdwla");
}

VOID
UnRegisterSTDWLA ()
{
    SNMPUnRegisterMib (&stdwlaOID, &stdwlaEntry);
    SNMPDelSysorEntry (&stdwlaOID, (const UINT1 *) "stdwla");
}

INT4
GetNextIndexCapwapDot11WlanTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexCapwapDot11WlanTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexCapwapDot11WlanTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexCapwapDot11WlanBindTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexCapwapDot11WlanBindTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexCapwapDot11WlanBindTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
CapwapDot11WlanProfileIfIndexGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapDot11WlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapDot11WlanProfileIfIndex
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
CapwapDot11WlanMacTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapDot11WlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapDot11WlanMacType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
CapwapDot11WlanTunnelModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapDot11WlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapDot11WlanTunnelMode
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
CapwapDot11WlanRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapDot11WlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapDot11WlanRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
CapwapDot11WlanBindWlanIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapDot11WlanBindTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapDot11WlanBindWlanId
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
CapwapDot11WlanBindBssIfIndexGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapDot11WlanBindTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapDot11WlanBindBssIfIndex
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
CapwapDot11WlanBindRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceCapwapDot11WlanBindTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetCapwapDot11WlanBindRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
CapwapDot11WlanMacTypeTest (UINT4 *pu4Error,
                            tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2CapwapDot11WlanMacType (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
CapwapDot11WlanTunnelModeTest (UINT4 *pu4Error,
                               tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2CapwapDot11WlanTunnelMode (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->pOctetStrValue));

}

INT4
CapwapDot11WlanRowStatusTest (UINT4 *pu4Error,
                              tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2CapwapDot11WlanRowStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
CapwapDot11WlanBindRowStatusTest (UINT4 *pu4Error,
                                  tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2CapwapDot11WlanBindRowStatus (pu4Error,
                                                   pMultiIndex->pIndex
                                                   [0].i4_SLongValue,
                                                   pMultiIndex->pIndex
                                                   [1].u4_ULongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
CapwapDot11WlanMacTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetCapwapDot11WlanMacType
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
CapwapDot11WlanTunnelModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetCapwapDot11WlanTunnelMode
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
CapwapDot11WlanRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetCapwapDot11WlanRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
CapwapDot11WlanBindRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetCapwapDot11WlanBindRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
CapwapDot11WlanTableDep (UINT4 *pu4Error,
                         tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapDot11WlanTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
CapwapDot11WlanBindTableDep (UINT4 *pu4Error,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2CapwapDot11WlanBindTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

VOID
RegisterFSWLAN ()
{
    SNMPRegisterMibWithLock (&fswlanOID, &fswlanEntry,
                             WsscfgMainTaskLock, WsscfgMainTaskUnLock,
                             SNMP_MSR_TGR_TRUE);

    SNMPAddSysorEntry (&fswlanOID, (const UINT1 *) "fswlan");
}

VOID
UnRegisterFSWLAN ()
{
    SNMPUnRegisterMib (&fswlanOID, &fswlanEntry);
    SNMPDelSysorEntry (&fswlanOID, (const UINT1 *) "fswlan");
}

INT4
GetNextIndexFsDot11StationConfigTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11StationConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11StationConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsWlanMulticastTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsWlanMulticastTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsWlanMulticastTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsDot11CapabilityProfileTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11CapabilityProfileTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11CapabilityProfileTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsDot11AuthenticationProfileTable (tSnmpIndex * pFirstMultiIndex,
                                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11AuthenticationProfileTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11AuthenticationProfileTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsSecurityWebAuthGuestInfoTable (tSnmpIndex * pFirstMultiIndex,
                                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsSecurityWebAuthGuestInfoTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsSecurityWebAuthGuestInfoTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsStationQosParamsTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsStationQosParamsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[1].
             pOctetStrValue->pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsStationQosParamsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].
             pOctetStrValue->pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[1].
             pOctetStrValue->pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsVlanIsolationTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsVlanIsolationTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsVlanIsolationTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsDot11RadioConfigTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11RadioConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11RadioConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsDot11QosProfileTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11QosProfileTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11QosProfileTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsDot11WlanCapabilityProfileTable (tSnmpIndex * pFirstMultiIndex,
                                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11WlanCapabilityProfileTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11WlanCapabilityProfileTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsDot11WlanAuthenticationProfileTable (tSnmpIndex *
                                                   pFirstMultiIndex,
                                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11WlanAuthenticationProfileTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11WlanAuthenticationProfileTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsDot11WlanQosProfileTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11WlanQosProfileTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11WlanQosProfileTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsDot11ClientSummaryTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11ClientSummaryTable ((tMacAddr *)
                                                       pNextMultiIndex->
                                                       pIndex[0].
                                                       pOctetStrValue->
                                                       pu1_OctetList) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11ClientSummaryTable
            (*(tMacAddr *) pFirstMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsDot11RadioQosTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11RadioQosTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11RadioQosTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsDot11QAPTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11QAPTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11QAPTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsQAPProfileTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsQAPProfileTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsQAPProfileTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsDot11CapabilityMappingTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11CapabilityMappingTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11CapabilityMappingTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsDot11AuthMappingTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11AuthMappingTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11AuthMappingTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsDot11QosMappingTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11QosMappingTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11QosMappingTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsDot11AntennasListTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11AntennasListTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11AntennasListTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsDot11WlanTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11WlanTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11WlanTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsDot11WlanBindTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11WlanBindTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11WlanBindTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsDot11nConfigTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11nConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11nConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsDot11nMCSDataRateTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11nMCSDataRateTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11nMCSDataRateTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsWtpImageUpgradeTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsWtpImageUpgradeTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsWtpImageUpgradeTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsDot11nExtConfigTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11nExtConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11nExtConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsDot11nAMPDUStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nExtConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nAMPDUStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nAMPDUSubFrameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nExtConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nAMPDUSubFrame (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11nAMPDULimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nExtConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nAMPDULimit (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11nAMSDUStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nExtConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nAMSDUStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nAMSDULimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nExtConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nAMSDULimit (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11nAMPDUStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11nAMPDUStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsDot11nAMPDUSubFrameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11nAMPDUSubFrame (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
FsDot11nAMPDULimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11nAMPDULimit (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->u4_ULongValue));

}

INT4
FsDot11nAMSDUStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11nAMSDUStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsDot11nAMSDULimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11nAMSDULimit (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->u4_ULongValue));

}

INT4
FsDot11nAMPDUStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nAMPDUStatus (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsDot11nAMPDUSubFrameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nAMPDUSubFrame (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->u4_ULongValue));

}

INT4
FsDot11nAMPDULimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nAMPDULimit (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
FsDot11nAMSDUStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nAMSDUStatus (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsDot11nAMSDULimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nAMSDULimit (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
FsDot11nExtConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11nExtConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsWlanStatisticsTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsWlanStatisticsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsWlanStatisticsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsDot11aNetworkEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsDot11aNetworkEnable (&(pMultiData->i4_SLongValue)));
}

INT4
FsDot11bNetworkEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsDot11bNetworkEnable (&(pMultiData->i4_SLongValue)));
}

INT4
FsDot11gSupportGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsDot11gSupport (&(pMultiData->i4_SLongValue)));
}

INT4
FsDot11anSupportGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsDot11anSupport (&(pMultiData->i4_SLongValue)));
}

INT4
FsDot11bnSupportGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsDot11bnSupport (&(pMultiData->i4_SLongValue)));
}

INT4
FsDot11ManagmentSSIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsDot11ManagmentSSID (&(pMultiData->u4_ULongValue)));
}

INT4
FsDot11CountryStringGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsDot11CountryString (pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSecurityWebAuthType (&(pMultiData->i4_SLongValue)));
}

INT4
FsSecurityWebAuthUrlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSecurityWebAuthUrl (pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthRedirectUrlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSecurityWebAuthRedirectUrl (pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSecurityWebAddr (&(pMultiData->i4_SLongValue)));
}

INT4
FsSecurityWebAuthWebTitleGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSecurityWebAuthWebTitle (pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthWebMessageGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSecurityWebAuthWebMessage (pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthWebLogoFileNameGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSecurityWebAuthWebLogoFileName
            (pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthWebSuccMessageGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSecurityWebAuthWebSuccMessage (pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthWebFailMessageGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSecurityWebAuthWebFailMessage (pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthWebButtonTextGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSecurityWebAuthWebButtonText (pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthWebLoadBalInfoGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSecurityWebAuthWebLoadBalInfo (pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthDisplayLangGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSecurityWebAuthDisplayLang (&(pMultiData->i4_SLongValue)));
}

INT4
FsSecurityWebAuthColorGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsSecurityWebAuthColor (&(pMultiData->i4_SLongValue)));
}

INT4
FsDot11StationCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsDot11StationCount (&(pMultiData->i4_SLongValue)));
}

INT4
FsDot11SupressSSIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11StationConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11SupressSSID
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11VlanIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11StationConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11VlanId (pMultiIndex->pIndex[0].i4_SLongValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanWebAuthRedirectFileNameGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11StationConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanWebAuthRedirectFileName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsDot11BandwidthThreshGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11StationConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11BandwidthThresh (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanLoginAuthenticationGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11StationConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanLoginAuthentication
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11CFPollableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11CapabilityProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11CFPollable
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11CFPollRequestGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11CapabilityProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11CFPollRequest
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11PrivacyOptionImplementedGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11CapabilityProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11PrivacyOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11ShortPreambleOptionImplementedGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11CapabilityProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11ShortPreambleOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11PBCCOptionImplementedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11CapabilityProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11PBCCOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11ChannelAgilityPresentGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11CapabilityProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11ChannelAgilityPresent
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11QosOptionImplementedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11CapabilityProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11QosOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11SpectrumManagementRequiredGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11CapabilityProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11SpectrumManagementRequired
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11ShortSlotTimeOptionImplementedGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11CapabilityProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11ShortSlotTimeOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11APSDOptionImplementedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11CapabilityProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11APSDOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11DSSSOFDMOptionEnabledGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11CapabilityProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11DSSSOFDMOptionEnabled
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11DelayedBlockAckOptionImplementedGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11CapabilityProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11DelayedBlockAckOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11ImmediateBlockAckOptionImplementedGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11CapabilityProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11ImmediateBlockAckOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11QAckOptionImplementedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11CapabilityProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11QAckOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11QueueRequestOptionImplementedGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11CapabilityProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11QueueRequestOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11TXOPRequestOptionImplementedGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11CapabilityProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11TXOPRequestOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11RSNAOptionImplementedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11CapabilityProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11RSNAOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11RSNAPreauthenticationImplementedGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11CapabilityProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11RSNAPreauthenticationImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11CapabilityRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11CapabilityProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11CapabilityRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11AuthenticationAlgorithmGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11AuthenticationProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11AuthenticationAlgorithm
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WepKeyTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11AuthenticationProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WepKeyType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WepKeyLengthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11AuthenticationProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WepKeyLength
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WepKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    /* Key Should not be displayed. return success */

    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return 1;
}

INT4
FsDot11WebAuthenticationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11AuthenticationProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WebAuthentication
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11AuthenticationRowStatusGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11AuthenticationProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11AuthenticationRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsSecurityWlanProfileIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSecurityWebAuthGuestInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSecurityWlanProfileId
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsSecurityWebAuthUserLifetimeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSecurityWebAuthGuestInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSecurityWebAuthUserLifetime
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsSecurityWebAuthUserEmailIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSecurityWebAuthGuestInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSecurityWebAuthUserEmailId
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsSecurityWebAuthGuestInfoRowStatusGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSecurityWebAuthGuestInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSecurityWebAuthGuestInfoRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsSecurityWebAuthUPasswordGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsSecurityWebAuthGuestInfoTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsSecurityWebAuthUPassword
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsStaQoSPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsStationQosParamsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].
          pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsStaQoSPriority
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
              pu1_OctetList), &(pMultiData->i4_SLongValue)));

}

INT4
FsStaQoSDscpGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsStationQosParamsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].
          pOctetStrValue->pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsStaQoSDscp (pMultiIndex->pIndex[0].i4_SLongValue,
                                (*(tMacAddr *) pMultiIndex->
                                 pIndex[1].pOctetStrValue->
                                 pu1_OctetList), &(pMultiData->i4_SLongValue)));

}

INT4
FsVlanIsolationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsVlanIsolationTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsVlanIsolation
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11RadioTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11RadioConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11RadioType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11RadioNoOfBssIdSupportedGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11RadioConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11RadioNoOfBssIdSupported
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11RadioAntennaTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11RadioConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11RadioAntennaType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11RadioFailureStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11RadioConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11RadioFailureStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11RowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11RadioConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11RowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11QosTrafficGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11QosProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11QosTraffic
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11QosPassengerTrustModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11QosProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11QosPassengerTrustMode
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11QosRateLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11QosProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11QosRateLimit
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11UpStreamCIRGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11QosProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11UpStreamCIR
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11UpStreamCBSGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11QosProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11UpStreamCBS
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11UpStreamEIRGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11QosProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11UpStreamEIR
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11UpStreamEBSGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11QosProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11UpStreamEBS
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11DownStreamCIRGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11QosProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11DownStreamCIR
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11DownStreamCBSGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11QosProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11DownStreamCBS
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11DownStreamEIRGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11QosProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11DownStreamEIR
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11DownStreamEBSGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11QosProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11DownStreamEBS
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11QosRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11QosProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11QosRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanCFPollableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanCapabilityProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanCFPollable
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanCFPollRequestGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanCapabilityProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanCFPollRequest
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanPrivacyOptionImplementedGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanCapabilityProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanPrivacyOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanShortPreambleOptionImplementedGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanCapabilityProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanShortPreambleOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanPBCCOptionImplementedGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanCapabilityProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanPBCCOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanChannelAgilityPresentGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanCapabilityProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanChannelAgilityPresent
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanQosOptionImplementedGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanCapabilityProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanQosOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanSpectrumManagementRequiredGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanCapabilityProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanSpectrumManagementRequired
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanShortSlotTimeOptionImplementedGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanCapabilityProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanShortSlotTimeOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanAPSDOptionImplementedGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanCapabilityProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanAPSDOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanDSSSOFDMOptionEnabledGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanCapabilityProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanDSSSOFDMOptionEnabled
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanDelayedBlockAckOptionImplementedGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanCapabilityProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanDelayedBlockAckOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanImmediateBlockAckOptionImplementedGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanCapabilityProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanImmediateBlockAckOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanQAckOptionImplementedGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanCapabilityProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanQAckOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanQueueRequestOptionImplementedGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanCapabilityProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanQueueRequestOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanTXOPRequestOptionImplementedGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanCapabilityProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanTXOPRequestOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanRSNAOptionImplementedGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanCapabilityProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanRSNAOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanRSNAPreauthenticationImplementedGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanCapabilityProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanRSNAPreauthenticationImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanCapabilityRowStatusGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanCapabilityProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanCapabilityRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanAuthenticationAlgorithmGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanAuthenticationProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanAuthenticationAlgorithm
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanWepKeyIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanAuthenticationProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanWepKeyIndex
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanWepKeyTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanAuthenticationProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanWepKeyType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanWepKeyLengthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanAuthenticationProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanWepKeyLength
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanWepKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    /* Key Should not be displayed. return success */
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return 1;
}

INT4
FsDot11WlanWebAuthenticationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanAuthenticationProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanWebAuthentication
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanAuthenticationRowStatusGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanAuthenticationProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanAuthenticationRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanQosTrafficGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanQosProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanQosTraffic
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanQosPassengerTrustModeGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanQosProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanQosPassengerTrustMode
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanQosRateLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanQosProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanQosRateLimit
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanUpStreamCIRGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanQosProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanUpStreamCIR
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanUpStreamCBSGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanQosProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanUpStreamCBS
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanUpStreamEIRGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanQosProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanUpStreamEIR
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanUpStreamEBSGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanQosProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanUpStreamEBS
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanDownStreamCIRGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanQosProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanDownStreamCIR
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanDownStreamCBSGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanQosProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanDownStreamCBS
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanDownStreamEIRGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanQosProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanDownStreamEIR
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanDownStreamEBSGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanQosProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanDownStreamEBS
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanQosRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanQosProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanQosRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11TaggingPolicyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11RadioQosTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11TaggingPolicy
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsDot11QueueDepthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11QAPTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11QueueDepth
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11PriorityValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11QAPTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11PriorityValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11DscpValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11QAPTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11DscpValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsQAPProfileCWminGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQAPProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQAPProfileCWmin
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsQAPProfileCWmaxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQAPProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQAPProfileCWmax
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsQAPProfileAIFSNGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQAPProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQAPProfileAIFSN
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsQAPProfileTXOPLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQAPProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQAPProfileTXOPLimit
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsQAPProfileQueueDepthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQAPProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQAPProfileQueueDepth
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsQAPProfilePriorityValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQAPProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQAPProfilePriorityValue
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsQAPProfileDscpValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQAPProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQAPProfileDscpValue
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsQAPProfileRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQAPProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQAPProfileRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11CapabilityMappingProfileNameGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11CapabilityMappingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11CapabilityMappingProfileName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsDot11CapabilityMappingRowStatusGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11CapabilityMappingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11CapabilityMappingRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11AuthMappingProfileNameGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11AuthMappingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11AuthMappingProfileName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsDot11AuthMappingRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11AuthMappingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11AuthMappingRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11QosMappingProfileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11QosMappingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11QosMappingProfileName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsDot11QosMappingRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11QosMappingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11QosMappingRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsAntennaModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11AntennasListTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsAntennaMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiIndex->pIndex[1].i4_SLongValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WepKeyIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11AuthenticationProfileTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WepKeyIndex (pMultiIndex->pIndex[0].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsAntennaSelectionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11AntennasListTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsAntennaSelection
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanProfileIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanProfileIfIndex
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanBindWlanIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanBindTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanBindWlanId
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11WlanBindBssIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanBindTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanBindBssIfIndex
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11WlanBindRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11WlanBindTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanBindRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigShortGIfor20MHzGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigShortGIfor20MHz
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigShortGIfor40MHzGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigShortGIfor40MHz
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigChannelWidthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigChannelWidth
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigAllowChannelWidthGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigAllowChannelWidth
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nMCSDataRateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nMCSDataRateTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nMCSDataRate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsWtpImageVersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpImageUpgradeTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWtpImageVersion
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsWtpUpgradeDevGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpImageUpgradeTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWtpUpgradeDev
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsWtpNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpImageUpgradeTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWtpName (pMultiIndex->pIndex[0].pOctetStrValue,
                             pMultiData->pOctetStrValue));

}

INT4
FsWtpImageNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpImageUpgradeTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWtpImageName
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsWtpAddressTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpImageUpgradeTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWtpAddressType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsWtpServerIPGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpImageUpgradeTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWtpServerIP (pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiData->pOctetStrValue));

}

INT4
FsWtpRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWtpImageUpgradeTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWtpRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsWlanBeaconsSentCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanBeaconsSentCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsWlanMulticastModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanMulticastTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanMulticastMode (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsWlanMulticastSnoopTableLengthGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanMulticastTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanMulticastSnoopTableLength
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsWlanMulticastSnoopTimerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanMulticastTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanMulticastSnoopTimer
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsWlanMulticastSnoopTimeoutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanMulticastTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanMulticastSnoopTimeout
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsWlanProbeReqRcvdCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanProbeReqRcvdCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsWlanProbeRespSentCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanProbeRespSentCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsWlanDataPktRcvdCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanDataPktRcvdCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsWlanDataPktSentCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanDataPktSentCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11aNetworkEnableTest (UINT4 *pu4Error,
                           tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsDot11aNetworkEnable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsDot11bNetworkEnableTest (UINT4 *pu4Error,
                           tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsDot11bNetworkEnable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsDot11gSupportTest (UINT4 *pu4Error,
                     tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsDot11gSupport (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsDot11anSupportTest (UINT4 *pu4Error,
                      tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsDot11anSupport (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsDot11bnSupportTest (UINT4 *pu4Error,
                      tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsDot11bnSupport (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsDot11ManagmentSSIDTest (UINT4 *pu4Error,
                          tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsDot11ManagmentSSID
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsDot11CountryStringTest (UINT4 *pu4Error,
                          tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsDot11CountryString
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthTypeTest (UINT4 *pu4Error,
                           tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSecurityWebAuthType
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSecurityWebAuthUrlTest (UINT4 *pu4Error,
                          tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSecurityWebAuthUrl
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthRedirectUrlTest (UINT4 *pu4Error,
                                  tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSecurityWebAuthRedirectUrl
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAddrTest (UINT4 *pu4Error,
                       tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSecurityWebAddr (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSecurityWebAuthWebTitleTest (UINT4 *pu4Error,
                               tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSecurityWebAuthWebTitle
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthWebMessageTest (UINT4 *pu4Error,
                                 tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSecurityWebAuthWebMessage
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthWebLogoFileNameTest (UINT4 *pu4Error,
                                      tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSecurityWebAuthWebLogoFileName
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthWebSuccMessageTest (UINT4 *pu4Error,
                                     tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSecurityWebAuthWebSuccMessage
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthWebFailMessageTest (UINT4 *pu4Error,
                                     tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSecurityWebAuthWebFailMessage
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthWebButtonTextTest (UINT4 *pu4Error,
                                    tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSecurityWebAuthWebButtonText
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthWebLoadBalInfoTest (UINT4 *pu4Error,
                                     tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSecurityWebAuthWebLoadBalInfo
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthDisplayLangTest (UINT4 *pu4Error,
                                  tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSecurityWebAuthDisplayLang
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsSecurityWebAuthColorTest (UINT4 *pu4Error,
                            tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsSecurityWebAuthColor
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsDot11SupressSSIDTest (UINT4 *pu4Error,
                        tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11SupressSSID (pu4Error,
                                         pMultiIndex->
                                         pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsDot11VlanIdTest (UINT4 *pu4Error,
                   tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11VlanId (pu4Error,
                                    pMultiIndex->pIndex[0].
                                    i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11BandwidthThreshTest (UINT4 *pu4Error,
                            tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11BandwidthThresh (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanWebAuthRedirectFileNameTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanWebAuthRedirectFileName (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         pOctetStrValue));

}

INT4
FsDot11WlanLoginAuthenticationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanLoginAuthentication (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FsDot11CFPollableTest (UINT4 *pu4Error,
                       tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11CFPollable (pu4Error,
                                        pMultiIndex->
                                        pIndex[0].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsDot11CFPollRequestTest (UINT4 *pu4Error,
                          tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11CFPollRequest (pu4Error,
                                           pMultiIndex->
                                           pIndex[0].pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsDot11PrivacyOptionImplementedTest (UINT4 *pu4Error,
                                     tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11PrivacyOptionImplemented (pu4Error,
                                                      pMultiIndex->pIndex
                                                      [0].pOctetStrValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
FsDot11ShortPreambleOptionImplementedTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11ShortPreambleOptionImplemented (pu4Error,
                                                            pMultiIndex->pIndex
                                                            [0].pOctetStrValue,
                                                            pMultiData->
                                                            i4_SLongValue));

}

INT4
FsDot11PBCCOptionImplementedTest (UINT4 *pu4Error,
                                  tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11PBCCOptionImplemented (pu4Error,
                                                   pMultiIndex->
                                                   pIndex
                                                   [0].pOctetStrValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsDot11ChannelAgilityPresentTest (UINT4 *pu4Error,
                                  tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11ChannelAgilityPresent (pu4Error,
                                                   pMultiIndex->
                                                   pIndex
                                                   [0].pOctetStrValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsDot11QosOptionImplementedTest (UINT4 *pu4Error,
                                 tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11QosOptionImplemented (pu4Error,
                                                  pMultiIndex->
                                                  pIndex
                                                  [0].pOctetStrValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsDot11SpectrumManagementRequiredTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11SpectrumManagementRequired (pu4Error,
                                                        pMultiIndex->pIndex
                                                        [0].pOctetStrValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
FsDot11ShortSlotTimeOptionImplementedTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11ShortSlotTimeOptionImplemented (pu4Error,
                                                            pMultiIndex->pIndex
                                                            [0].pOctetStrValue,
                                                            pMultiData->
                                                            i4_SLongValue));

}

INT4
FsDot11APSDOptionImplementedTest (UINT4 *pu4Error,
                                  tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11APSDOptionImplemented (pu4Error,
                                                   pMultiIndex->
                                                   pIndex
                                                   [0].pOctetStrValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsDot11DSSSOFDMOptionEnabledTest (UINT4 *pu4Error,
                                  tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11DSSSOFDMOptionEnabled (pu4Error,
                                                   pMultiIndex->
                                                   pIndex
                                                   [0].pOctetStrValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsDot11DelayedBlockAckOptionImplementedTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11DelayedBlockAckOptionImplemented (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              pOctetStrValue,
                                                              pMultiData->
                                                              i4_SLongValue));

}

INT4
FsDot11ImmediateBlockAckOptionImplementedTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11ImmediateBlockAckOptionImplemented
            (pu4Error, pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->i4_SLongValue));

}

INT4
FsDot11QAckOptionImplementedTest (UINT4 *pu4Error,
                                  tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11QAckOptionImplemented (pu4Error,
                                                   pMultiIndex->
                                                   pIndex
                                                   [0].pOctetStrValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsDot11QueueRequestOptionImplementedTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11QueueRequestOptionImplemented (pu4Error,
                                                           pMultiIndex->pIndex
                                                           [0].pOctetStrValue,
                                                           pMultiData->
                                                           i4_SLongValue));

}

INT4
FsDot11TXOPRequestOptionImplementedTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11TXOPRequestOptionImplemented (pu4Error,
                                                          pMultiIndex->pIndex
                                                          [0].pOctetStrValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
FsDot11RSNAOptionImplementedTest (UINT4 *pu4Error,
                                  tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11RSNAOptionImplemented (pu4Error,
                                                   pMultiIndex->
                                                   pIndex
                                                   [0].pOctetStrValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsDot11RSNAPreauthenticationImplementedTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11RSNAPreauthenticationImplemented (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              pOctetStrValue,
                                                              pMultiData->
                                                              i4_SLongValue));

}

INT4
FsDot11CapabilityRowStatusTest (UINT4 *pu4Error,
                                tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11CapabilityRowStatus (pu4Error,
                                                 pMultiIndex->
                                                 pIndex
                                                 [0].pOctetStrValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsDot11AuthenticationAlgorithmTest (UINT4 *pu4Error,
                                    tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11AuthenticationAlgorithm (pu4Error,
                                                     pMultiIndex->
                                                     pIndex
                                                     [0].pOctetStrValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FsDot11WepKeyIndexTest (UINT4 *pu4Error,
                        tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;
}

INT4
FsDot11WepKeyTypeTest (UINT4 *pu4Error,
                       tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WepKeyType (pu4Error,
                                        pMultiIndex->
                                        pIndex[0].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsDot11WepKeyLengthTest (UINT4 *pu4Error,
                         tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WepKeyLength (pu4Error,
                                          pMultiIndex->
                                          pIndex[0].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsDot11WepKeyTest (UINT4 *pu4Error,
                   tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WepKey (pu4Error,
                                    pMultiIndex->pIndex[0].
                                    pOctetStrValue,
                                    pMultiData->pOctetStrValue));

}

INT4
FsDot11WebAuthenticationTest (UINT4 *pu4Error,
                              tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WebAuthentication (pu4Error,
                                               pMultiIndex->
                                               pIndex
                                               [0].pOctetStrValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsDot11AuthenticationRowStatusTest (UINT4 *pu4Error,
                                    tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11AuthenticationRowStatus (pu4Error,
                                                     pMultiIndex->
                                                     pIndex
                                                     [0].pOctetStrValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FsSecurityWlanProfileIdTest (UINT4 *pu4Error,
                             tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsSecurityWlanProfileId (pu4Error,
                                              pMultiIndex->
                                              pIndex[0].pOctetStrValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsSecurityWebAuthUserLifetimeTest (UINT4 *pu4Error,
                                   tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsSecurityWebAuthUserLifetime (pu4Error,
                                                    pMultiIndex->
                                                    pIndex
                                                    [0].pOctetStrValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
FsSecurityWebAuthGuestInfoRowStatusTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsSecurityWebAuthGuestInfoRowStatus (pu4Error,
                                                          pMultiIndex->pIndex
                                                          [0].pOctetStrValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
FsSecurityWebAuthUPasswordTest (UINT4 *pu4Error,
                                tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsSecurityWebAuthUPassword (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 pOctetStrValue,
                                                 pMultiData->pOctetStrValue));

}

INT4
FsStaQoSPriorityTest (UINT4 *pu4Error,
                      tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsStaQoSPriority (pu4Error,
                                       pMultiIndex->pIndex[0].
                                       i4_SLongValue,
                                       (*(tMacAddr *)
                                        pMultiIndex->pIndex[1].
                                        pOctetStrValue->pu1_OctetList),
                                       pMultiData->i4_SLongValue));

}

INT4
FsStaQoSDscpTest (UINT4 *pu4Error,
                  tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsStaQoSDscp (pu4Error,
                                   pMultiIndex->pIndex[0].
                                   i4_SLongValue,
                                   (*(tMacAddr *) pMultiIndex->
                                    pIndex[1].pOctetStrValue->
                                    pu1_OctetList), pMultiData->i4_SLongValue));

}

INT4
FsVlanIsolationTest (UINT4 *pu4Error,
                     tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsVlanIsolation (pu4Error,
                                      pMultiIndex->pIndex[0].
                                      i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsDot11RadioTypeTest (UINT4 *pu4Error,
                      tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11RadioType (pu4Error,
                                       pMultiIndex->pIndex[0].
                                       i4_SLongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
FsDot11RowStatusTest (UINT4 *pu4Error,
                      tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11RowStatus (pu4Error,
                                       pMultiIndex->pIndex[0].
                                       i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsDot11QosTrafficTest (UINT4 *pu4Error,
                       tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11QosTraffic (pu4Error,
                                        pMultiIndex->
                                        pIndex[0].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsDot11QosPassengerTrustModeTest (UINT4 *pu4Error,
                                  tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11QosPassengerTrustMode (pu4Error,
                                                   pMultiIndex->
                                                   pIndex
                                                   [0].pOctetStrValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsDot11QosRateLimitTest (UINT4 *pu4Error,
                         tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11QosRateLimit (pu4Error,
                                          pMultiIndex->
                                          pIndex[0].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsDot11UpStreamCIRTest (UINT4 *pu4Error,
                        tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11UpStreamCIR (pu4Error,
                                         pMultiIndex->
                                         pIndex[0].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsDot11UpStreamCBSTest (UINT4 *pu4Error,
                        tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11UpStreamCBS (pu4Error,
                                         pMultiIndex->
                                         pIndex[0].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsDot11UpStreamEIRTest (UINT4 *pu4Error,
                        tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11UpStreamEIR (pu4Error,
                                         pMultiIndex->
                                         pIndex[0].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsDot11UpStreamEBSTest (UINT4 *pu4Error,
                        tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11UpStreamEBS (pu4Error,
                                         pMultiIndex->
                                         pIndex[0].pOctetStrValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsDot11DownStreamCIRTest (UINT4 *pu4Error,
                          tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11DownStreamCIR (pu4Error,
                                           pMultiIndex->
                                           pIndex[0].pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsDot11DownStreamCBSTest (UINT4 *pu4Error,
                          tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11DownStreamCBS (pu4Error,
                                           pMultiIndex->
                                           pIndex[0].pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsDot11DownStreamEIRTest (UINT4 *pu4Error,
                          tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11DownStreamEIR (pu4Error,
                                           pMultiIndex->
                                           pIndex[0].pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsDot11DownStreamEBSTest (UINT4 *pu4Error,
                          tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11DownStreamEBS (pu4Error,
                                           pMultiIndex->
                                           pIndex[0].pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsDot11QosRowStatusTest (UINT4 *pu4Error,
                         tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11QosRowStatus (pu4Error,
                                          pMultiIndex->
                                          pIndex[0].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanCFPollableTest (UINT4 *pu4Error,
                           tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanCFPollable (pu4Error,
                                            pMultiIndex->
                                            pIndex[0].i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanCFPollRequestTest (UINT4 *pu4Error,
                              tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanCFPollRequest (pu4Error,
                                               pMultiIndex->
                                               pIndex[0].i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanPrivacyOptionImplementedTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanPrivacyOptionImplemented (pu4Error,
                                                          pMultiIndex->pIndex
                                                          [0].i4_SLongValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
FsDot11WlanShortPreambleOptionImplementedTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanShortPreambleOptionImplemented
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanPBCCOptionImplementedTest (UINT4 *pu4Error,
                                      tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanPBCCOptionImplemented (pu4Error,
                                                       pMultiIndex->pIndex
                                                       [0].i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
FsDot11WlanChannelAgilityPresentTest (UINT4 *pu4Error,
                                      tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanChannelAgilityPresent (pu4Error,
                                                       pMultiIndex->pIndex
                                                       [0].i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
FsDot11WlanQosOptionImplementedTest (UINT4 *pu4Error,
                                     tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanQosOptionImplemented (pu4Error,
                                                      pMultiIndex->pIndex
                                                      [0].i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
FsDot11WlanSpectrumManagementRequiredTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanSpectrumManagementRequired (pu4Error,
                                                            pMultiIndex->pIndex
                                                            [0].i4_SLongValue,
                                                            pMultiData->
                                                            i4_SLongValue));

}

INT4
FsDot11WlanShortSlotTimeOptionImplementedTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanShortSlotTimeOptionImplemented
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanAPSDOptionImplementedTest (UINT4 *pu4Error,
                                      tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanAPSDOptionImplemented (pu4Error,
                                                       pMultiIndex->pIndex
                                                       [0].i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
FsDot11WlanDSSSOFDMOptionEnabledTest (UINT4 *pu4Error,
                                      tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanDSSSOFDMOptionEnabled (pu4Error,
                                                       pMultiIndex->pIndex
                                                       [0].i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
FsDot11WlanDelayedBlockAckOptionImplementedTest (UINT4 *pu4Error,
                                                 tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanDelayedBlockAckOptionImplemented
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanImmediateBlockAckOptionImplementedTest (UINT4 *pu4Error,
                                                   tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanImmediateBlockAckOptionImplemented
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanQAckOptionImplementedTest (UINT4 *pu4Error,
                                      tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanQAckOptionImplemented (pu4Error,
                                                       pMultiIndex->pIndex
                                                       [0].i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
FsDot11WlanQueueRequestOptionImplementedTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanQueueRequestOptionImplemented
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanTXOPRequestOptionImplementedTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanTXOPRequestOptionImplemented (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              i4_SLongValue,
                                                              pMultiData->
                                                              i4_SLongValue));

}

INT4
FsDot11WlanRSNAOptionImplementedTest (UINT4 *pu4Error,
                                      tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanRSNAOptionImplemented (pu4Error,
                                                       pMultiIndex->pIndex
                                                       [0].i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
FsDot11WlanRSNAPreauthenticationImplementedTest (UINT4 *pu4Error,
                                                 tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanRSNAPreauthenticationImplemented
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanCapabilityRowStatusTest (UINT4 *pu4Error,
                                    tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanCapabilityRowStatus (pu4Error,
                                                     pMultiIndex->
                                                     pIndex
                                                     [0].i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FsDot11WlanAuthenticationAlgorithmTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanAuthenticationAlgorithm (pu4Error,
                                                         pMultiIndex->pIndex
                                                         [0].i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
FsDot11WlanWepKeyIndexTest (UINT4 *pu4Error,
                            tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanWepKeyIndex (pu4Error,
                                             pMultiIndex->
                                             pIndex[0].i4_SLongValue,
                                             pMultiData->i4_SLongValue));
}

INT4
FsDot11WlanWepKeyTypeTest (UINT4 *pu4Error,
                           tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanWepKeyType (pu4Error,
                                            pMultiIndex->
                                            pIndex[0].i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanWepKeyLengthTest (UINT4 *pu4Error,
                             tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanWepKeyLength (pu4Error,
                                              pMultiIndex->
                                              pIndex[0].i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanWepKeyTest (UINT4 *pu4Error,
                       tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanWepKey (pu4Error,
                                        pMultiIndex->
                                        pIndex[0].i4_SLongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsDot11WlanWebAuthenticationTest (UINT4 *pu4Error,
                                  tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanWebAuthentication (pu4Error,
                                                   pMultiIndex->
                                                   pIndex
                                                   [0].i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanAuthenticationRowStatusTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanAuthenticationRowStatus (pu4Error,
                                                         pMultiIndex->pIndex
                                                         [0].i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
FsDot11WlanQosTrafficTest (UINT4 *pu4Error,
                           tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanQosTraffic (pu4Error,
                                            pMultiIndex->
                                            pIndex[0].i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanQosPassengerTrustModeTest (UINT4 *pu4Error,
                                      tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanQosPassengerTrustMode (pu4Error,
                                                       pMultiIndex->pIndex
                                                       [0].i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
FsDot11WlanQosRateLimitTest (UINT4 *pu4Error,
                             tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanQosRateLimit (pu4Error,
                                              pMultiIndex->
                                              pIndex[0].i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanUpStreamCIRTest (UINT4 *pu4Error,
                            tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanUpStreamCIR (pu4Error,
                                             pMultiIndex->
                                             pIndex[0].i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanUpStreamCBSTest (UINT4 *pu4Error,
                            tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanUpStreamCBS (pu4Error,
                                             pMultiIndex->
                                             pIndex[0].i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanUpStreamEIRTest (UINT4 *pu4Error,
                            tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanUpStreamEIR (pu4Error,
                                             pMultiIndex->
                                             pIndex[0].i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanUpStreamEBSTest (UINT4 *pu4Error,
                            tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanUpStreamEBS (pu4Error,
                                             pMultiIndex->
                                             pIndex[0].i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanDownStreamCIRTest (UINT4 *pu4Error,
                              tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanDownStreamCIR (pu4Error,
                                               pMultiIndex->
                                               pIndex[0].i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanDownStreamCBSTest (UINT4 *pu4Error,
                              tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanDownStreamCBS (pu4Error,
                                               pMultiIndex->
                                               pIndex[0].i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanDownStreamEIRTest (UINT4 *pu4Error,
                              tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanDownStreamEIR (pu4Error,
                                               pMultiIndex->
                                               pIndex[0].i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanDownStreamEBSTest (UINT4 *pu4Error,
                              tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanDownStreamEBS (pu4Error,
                                               pMultiIndex->
                                               pIndex[0].i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanQosRowStatusTest (UINT4 *pu4Error,
                             tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanQosRowStatus (pu4Error,
                                              pMultiIndex->
                                              pIndex[0].i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsDot11TaggingPolicyTest (UINT4 *pu4Error,
                          tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11TaggingPolicy (pu4Error,
                                           pMultiIndex->
                                           pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FsDot11QueueDepthTest (UINT4 *pu4Error,
                       tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11QueueDepth (pu4Error,
                                        pMultiIndex->
                                        pIndex[0].i4_SLongValue,
                                        pMultiIndex->
                                        pIndex[1].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsDot11PriorityValueTest (UINT4 *pu4Error,
                          tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11PriorityValue (pu4Error,
                                           pMultiIndex->
                                           pIndex[0].i4_SLongValue,
                                           pMultiIndex->
                                           pIndex[1].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsDot11DscpValueTest (UINT4 *pu4Error,
                      tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11DscpValue (pu4Error,
                                       pMultiIndex->pIndex[0].
                                       i4_SLongValue,
                                       pMultiIndex->pIndex[1].
                                       i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsQAPProfileCWminTest (UINT4 *pu4Error,
                       tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsQAPProfileCWmin (pu4Error,
                                        pMultiIndex->
                                        pIndex[0].pOctetStrValue,
                                        pMultiIndex->
                                        pIndex[1].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsQAPProfileCWmaxTest (UINT4 *pu4Error,
                       tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsQAPProfileCWmax (pu4Error,
                                        pMultiIndex->
                                        pIndex[0].pOctetStrValue,
                                        pMultiIndex->
                                        pIndex[1].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsQAPProfileAIFSNTest (UINT4 *pu4Error,
                       tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsQAPProfileAIFSN (pu4Error,
                                        pMultiIndex->
                                        pIndex[0].pOctetStrValue,
                                        pMultiIndex->
                                        pIndex[1].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsQAPProfileTXOPLimitTest (UINT4 *pu4Error,
                           tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsQAPProfileTXOPLimit (pu4Error,
                                            pMultiIndex->
                                            pIndex[0].pOctetStrValue,
                                            pMultiIndex->
                                            pIndex[1].i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsQAPProfileQueueDepthTest (UINT4 *pu4Error,
                            tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsQAPProfileQueueDepth (pu4Error,
                                             pMultiIndex->
                                             pIndex[0].pOctetStrValue,
                                             pMultiIndex->
                                             pIndex[1].i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsQAPProfilePriorityValueTest (UINT4 *pu4Error,
                               tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsQAPProfilePriorityValue (pu4Error,
                                                pMultiIndex->
                                                pIndex
                                                [0].pOctetStrValue,
                                                pMultiIndex->
                                                pIndex
                                                [1].i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsQAPProfileDscpValueTest (UINT4 *pu4Error,
                           tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsQAPProfileDscpValue (pu4Error,
                                            pMultiIndex->
                                            pIndex[0].pOctetStrValue,
                                            pMultiIndex->
                                            pIndex[1].i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsQAPProfileRowStatusTest (UINT4 *pu4Error,
                           tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsQAPProfileRowStatus (pu4Error,
                                            pMultiIndex->
                                            pIndex[0].pOctetStrValue,
                                            pMultiIndex->
                                            pIndex[1].i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsDot11CapabilityMappingProfileNameTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11CapabilityMappingProfileName (pu4Error,
                                                          pMultiIndex->pIndex
                                                          [0].i4_SLongValue,
                                                          pMultiData->
                                                          pOctetStrValue));

}

INT4
FsDot11CapabilityMappingRowStatusTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11CapabilityMappingRowStatus (pu4Error,
                                                        pMultiIndex->pIndex
                                                        [0].i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
FsDot11AuthMappingProfileNameTest (UINT4 *pu4Error,
                                   tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11AuthMappingProfileName (pu4Error,
                                                    pMultiIndex->
                                                    pIndex
                                                    [0].i4_SLongValue,
                                                    pMultiData->
                                                    pOctetStrValue));

}

INT4
FsDot11AuthMappingRowStatusTest (UINT4 *pu4Error,
                                 tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11AuthMappingRowStatus (pu4Error,
                                                  pMultiIndex->
                                                  pIndex
                                                  [0].i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsDot11QosMappingProfileNameTest (UINT4 *pu4Error,
                                  tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11QosMappingProfileName (pu4Error,
                                                   pMultiIndex->
                                                   pIndex
                                                   [0].i4_SLongValue,
                                                   pMultiData->pOctetStrValue));

}

INT4
FsDot11QosMappingRowStatusTest (UINT4 *pu4Error,
                                tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11QosMappingRowStatus (pu4Error,
                                                 pMultiIndex->
                                                 pIndex
                                                 [0].i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanProfileIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11ClientSummaryTable ((*(tMacAddr *)
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            pOctetStrValue->
                                                            pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WlanProfileId ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                         pOctetStrValue->pu1_OctetList),
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11WtpProfileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11ClientSummaryTable ((*(tMacAddr *)
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            pOctetStrValue->
                                                            pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WtpProfileName ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                          pOctetStrValue->pu1_OctetList),
                                         pMultiData->pOctetStrValue));

}

INT4
FsDot11WtpRadioIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11ClientSummaryTable ((*(tMacAddr *)
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            pOctetStrValue->
                                                            pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11WtpRadioId ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                      pOctetStrValue->pu1_OctetList),
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11AuthStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11ClientSummaryTable ((*(tMacAddr *)
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            pOctetStrValue->
                                                            pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11AuthStatus ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                      pOctetStrValue->pu1_OctetList),
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11AssocStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11ClientSummaryTable ((*(tMacAddr *)
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            pOctetStrValue->
                                                            pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11AssocStatus ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                       pOctetStrValue->pu1_OctetList),
                                      &(pMultiData->i4_SLongValue)));

}

/*Routines for */
INT4
GetNextIndexFsDot11ExternalWebAuthProfileTable (tSnmpIndex * pFirstMultiIndex,
                                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11ExternalWebAuthProfileTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11ExternalWebAuthProfileTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsDot11ExternalWebAuthMethodGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11ExternalWebAuthProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11ExternalWebAuthMethod
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11ExternalWebAuthUrlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11ExternalWebAuthProfileTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11ExternalWebAuthUrl
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsDot11ExternalWebAuthMethodSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11ExternalWebAuthMethod
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11ExternalWebAuthUrlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11ExternalWebAuthUrl
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsDot11ExternalWebAuthMethodTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11ExternalWebAuthMethod (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsDot11ExternalWebAuthUrlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11ExternalWebAuthUrl (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->pOctetStrValue));

}

INT4
FsDot11ExternalWebAuthProfileTableDep (UINT4 *pu4Error,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11ExternalWebAuthProfileTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsAntennaModeTest (UINT4 *pu4Error,
                   tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsAntennaMode (pu4Error,
                                    pMultiIndex->pIndex[0].
                                    i4_SLongValue,
                                    pMultiIndex->pIndex[1].
                                    i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsAntennaSelectionTest (UINT4 *pu4Error,
                        tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsAntennaSelection (pu4Error,
                                         pMultiIndex->
                                         pIndex[0].i4_SLongValue,
                                         pMultiIndex->
                                         pIndex[1].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanProfileIfIndexTest (UINT4 *pu4Error,
                               tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanProfileIfIndex (pu4Error,
                                                pMultiIndex->
                                                pIndex
                                                [0].u4_ULongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanRowStatusTest (UINT4 *pu4Error,
                          tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanRowStatus (pu4Error,
                                           pMultiIndex->
                                           pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanBindWlanIdTest (UINT4 *pu4Error,
                           tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanBindWlanId (pu4Error,
                                            pMultiIndex->
                                            pIndex[0].i4_SLongValue,
                                            pMultiIndex->
                                            pIndex[1].u4_ULongValue,
                                            pMultiData->u4_ULongValue));

}

INT4
FsDot11WlanBindBssIfIndexTest (UINT4 *pu4Error,
                               tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanBindBssIfIndex (pu4Error,
                                                pMultiIndex->
                                                pIndex
                                                [0].i4_SLongValue,
                                                pMultiIndex->
                                                pIndex
                                                [1].u4_ULongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanBindRowStatusTest (UINT4 *pu4Error,
                              tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11WlanBindRowStatus (pu4Error,
                                               pMultiIndex->
                                               pIndex[0].i4_SLongValue,
                                               pMultiIndex->
                                               pIndex[1].u4_ULongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigShortGIfor20MHzTest (UINT4 *pu4Error,
                                   tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigShortGIfor20MHz (pu4Error,
                                                    pMultiIndex->
                                                    pIndex
                                                    [0].i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigShortGIfor40MHzTest (UINT4 *pu4Error,
                                   tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigShortGIfor40MHz (pu4Error,
                                                    pMultiIndex->
                                                    pIndex
                                                    [0].i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigChannelWidthTest (UINT4 *pu4Error,
                                tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigChannelWidth (pu4Error,
                                                 pMultiIndex->
                                                 pIndex
                                                 [0].i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigAllowChannelWidthTest (UINT4 *pu4Error,
                                     tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigAllowChannelWidth (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
FsDot11nMCSDataRateTest (UINT4 *pu4Error,
                         tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nMCSDataRate (pu4Error,
                                          pMultiIndex->
                                          pIndex[0].i4_SLongValue,
                                          pMultiIndex->
                                          pIndex[1].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsWtpImageVersionTest (UINT4 *pu4Error,
                       tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsWtpImageVersion (pu4Error,
                                        pMultiIndex->
                                        pIndex[0].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsWtpUpgradeDevTest (UINT4 *pu4Error,
                     tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsWtpUpgradeDev (pu4Error,
                                      pMultiIndex->pIndex[0].
                                      pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsWtpNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsWtpName (pu4Error,
                                pMultiIndex->pIndex[0].pOctetStrValue,
                                pMultiData->pOctetStrValue));

}

INT4
FsWtpImageNameTest (UINT4 *pu4Error,
                    tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsWtpImageName (pu4Error,
                                     pMultiIndex->pIndex[0].
                                     pOctetStrValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsWtpAddressTypeTest (UINT4 *pu4Error,
                      tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsWtpAddressType (pu4Error,
                                       pMultiIndex->
                                       pIndex[0].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsWtpServerIPTest (UINT4 *pu4Error,
                   tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsWtpServerIP (pu4Error,
                                    pMultiIndex->pIndex[0].
                                    pOctetStrValue,
                                    pMultiData->pOctetStrValue));

}

INT4
FsWtpRowStatusTest (UINT4 *pu4Error,
                    tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsWtpRowStatus (pu4Error,
                                     pMultiIndex->pIndex[0].
                                     pOctetStrValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsWlanBeaconsSentCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsWlanBeaconsSentCount (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->u4_ULongValue));

}

INT4
FsWlanMulticastModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsWlanMulticastMode (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsWlanMulticastSnoopTableLengthTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsWlanMulticastSnoopTableLength (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      u4_ULongValue));

}

INT4
FsWlanMulticastSnoopTimerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsWlanMulticastSnoopTimer (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->u4_ULongValue));

}

INT4
FsWlanMulticastSnoopTimeoutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsWlanMulticastSnoopTimeout (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue));

}

INT4
FsWlanProbeReqRcvdCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsWlanProbeReqRcvdCount (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
FsWlanProbeRespSentCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsWlanProbeRespSentCount (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->u4_ULongValue));

}

INT4
FsWlanDataPktRcvdCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsWlanDataPktRcvdCount (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->u4_ULongValue));

}

INT4
FsWlanDataPktSentCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsWlanDataPktSentCount (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->u4_ULongValue));

}

INT4
FsDot11aNetworkEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsDot11aNetworkEnable (pMultiData->i4_SLongValue));
}

INT4
FsDot11bNetworkEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsDot11bNetworkEnable (pMultiData->i4_SLongValue));
}

INT4
FsDot11gSupportSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsDot11gSupport (pMultiData->i4_SLongValue));
}

INT4
FsDot11anSupportSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsDot11anSupport (pMultiData->i4_SLongValue));
}

INT4
FsDot11bnSupportSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsDot11bnSupport (pMultiData->i4_SLongValue));
}

INT4
FsDot11ManagmentSSIDSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsDot11ManagmentSSID (pMultiData->u4_ULongValue));
}

INT4
FsDot11CountryStringSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsDot11CountryString (pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSecurityWebAuthType (pMultiData->i4_SLongValue));
}

INT4
FsSecurityWebAuthUrlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSecurityWebAuthUrl (pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthRedirectUrlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSecurityWebAuthRedirectUrl (pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSecurityWebAddr (pMultiData->i4_SLongValue));
}

INT4
FsSecurityWebAuthWebTitleSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSecurityWebAuthWebTitle (pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthWebMessageSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSecurityWebAuthWebMessage (pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthWebLogoFileNameSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSecurityWebAuthWebLogoFileName
            (pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthWebSuccMessageSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSecurityWebAuthWebSuccMessage (pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthWebFailMessageSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSecurityWebAuthWebFailMessage (pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthWebButtonTextSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSecurityWebAuthWebButtonText (pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthWebLoadBalInfoSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSecurityWebAuthWebLoadBalInfo (pMultiData->pOctetStrValue));
}

INT4
FsSecurityWebAuthDisplayLangSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSecurityWebAuthDisplayLang (pMultiData->i4_SLongValue));
}

INT4
FsSecurityWebAuthColorSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsSecurityWebAuthColor (pMultiData->i4_SLongValue));
}

INT4
FsDot11SupressSSIDSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11SupressSSID
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11VlanIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11VlanId (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiData->i4_SLongValue));

}

INT4
FsDot11BandwidthThreshSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11BandwidthThresh (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanWebAuthRedirectFileNameSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanWebAuthRedirectFileName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsDot11WlanLoginAuthenticationSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanLoginAuthentication
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11CFPollableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11CFPollable
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11CFPollRequestSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11CFPollRequest
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11PrivacyOptionImplementedSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsDot11PrivacyOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11ShortPreambleOptionImplementedSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsDot11ShortPreambleOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11PBCCOptionImplementedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11PBCCOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11ChannelAgilityPresentSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11ChannelAgilityPresent
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11QosOptionImplementedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11QosOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11SpectrumManagementRequiredSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsDot11SpectrumManagementRequired
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11ShortSlotTimeOptionImplementedSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsDot11ShortSlotTimeOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11APSDOptionImplementedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11APSDOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11DSSSOFDMOptionEnabledSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11DSSSOFDMOptionEnabled
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11DelayedBlockAckOptionImplementedSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetFsDot11DelayedBlockAckOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11ImmediateBlockAckOptionImplementedSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhSetFsDot11ImmediateBlockAckOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11QAckOptionImplementedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11QAckOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11QueueRequestOptionImplementedSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetFsDot11QueueRequestOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11TXOPRequestOptionImplementedSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsDot11TXOPRequestOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11RSNAOptionImplementedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11RSNAOptionImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11RSNAPreauthenticationImplementedSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetFsDot11RSNAPreauthenticationImplemented
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11CapabilityRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11CapabilityRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11AuthenticationAlgorithmSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsDot11AuthenticationAlgorithm
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WepKeyIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WepKeyIndex
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));
}

INT4
FsDot11WepKeyTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WepKeyType
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WepKeyLengthSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WepKeyLength
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WepKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WepKey (pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiData->pOctetStrValue));

}

INT4
FsDot11WebAuthenticationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WebAuthentication
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11AuthenticationRowStatusSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsDot11AuthenticationRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsSecurityWlanProfileIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsSecurityWlanProfileId
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsSecurityWebAuthUserLifetimeSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsSecurityWebAuthUserLifetime
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsSecurityWebAuthGuestInfoRowStatusSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsSecurityWebAuthGuestInfoRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsSecurityWebAuthUPasswordSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsSecurityWebAuthUPassword
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsStaQoSPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsStaQoSPriority
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->
              pu1_OctetList), pMultiData->i4_SLongValue));

}

INT4
FsStaQoSDscpSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsStaQoSDscp (pMultiIndex->pIndex[0].i4_SLongValue,
                                (*(tMacAddr *) pMultiIndex->
                                 pIndex[1].pOctetStrValue->
                                 pu1_OctetList), pMultiData->i4_SLongValue));

}

INT4
FsVlanIsolationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsVlanIsolation
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11RadioTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11RadioType
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsDot11RowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11RowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11QosTrafficSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11QosTraffic
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11QosPassengerTrustModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11QosPassengerTrustMode
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11QosRateLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11QosRateLimit
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11UpStreamCIRSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11UpStreamCIR
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11UpStreamCBSSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11UpStreamCBS
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11UpStreamEIRSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11UpStreamEIR
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11UpStreamEBSSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11UpStreamEBS
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11DownStreamCIRSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11DownStreamCIR
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11DownStreamCBSSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11DownStreamCBS
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11DownStreamEIRSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11DownStreamEIR
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11DownStreamEBSSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11DownStreamEBS
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11QosRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11QosRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanCFPollableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanCFPollable
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanCFPollRequestSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanCFPollRequest
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanPrivacyOptionImplementedSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanPrivacyOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanShortPreambleOptionImplementedSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanShortPreambleOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanPBCCOptionImplementedSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanPBCCOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanChannelAgilityPresentSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanChannelAgilityPresent
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanQosOptionImplementedSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanQosOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanSpectrumManagementRequiredSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanSpectrumManagementRequired
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanShortSlotTimeOptionImplementedSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanShortSlotTimeOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanAPSDOptionImplementedSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanAPSDOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanDSSSOFDMOptionEnabledSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanDSSSOFDMOptionEnabled
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanDelayedBlockAckOptionImplementedSet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanDelayedBlockAckOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanImmediateBlockAckOptionImplementedSet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanImmediateBlockAckOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanQAckOptionImplementedSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanQAckOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanQueueRequestOptionImplementedSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanQueueRequestOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanTXOPRequestOptionImplementedSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanTXOPRequestOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanRSNAOptionImplementedSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanRSNAOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanRSNAPreauthenticationImplementedSet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanRSNAPreauthenticationImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanCapabilityRowStatusSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanCapabilityRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanAuthenticationAlgorithmSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanAuthenticationAlgorithm
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanWepKeyIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanWepKeyIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));
}

INT4
FsDot11WlanWepKeyTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanWepKeyType
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanWepKeyLengthSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanWepKeyLength
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanWepKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanWepKey
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsDot11WlanWebAuthenticationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanWebAuthentication
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanAuthenticationRowStatusSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanAuthenticationRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanQosTrafficSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanQosTraffic
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanQosPassengerTrustModeSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanQosPassengerTrustMode
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanQosRateLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanQosRateLimit
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanUpStreamCIRSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanUpStreamCIR
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanUpStreamCBSSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanUpStreamCBS
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanUpStreamEIRSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanUpStreamEIR
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanUpStreamEBSSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanUpStreamEBS
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanDownStreamCIRSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanDownStreamCIR
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanDownStreamCBSSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanDownStreamCBS
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanDownStreamEIRSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanDownStreamEIR
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanDownStreamEBSSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanDownStreamEBS
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanQosRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanQosRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11TaggingPolicySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11TaggingPolicy
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsDot11QueueDepthSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11QueueDepth
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11PriorityValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11PriorityValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11DscpValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11DscpValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsQAPProfileCWminSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQAPProfileCWmin
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsQAPProfileCWmaxSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQAPProfileCWmax
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsQAPProfileAIFSNSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQAPProfileAIFSN
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsQAPProfileTXOPLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQAPProfileTXOPLimit
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsQAPProfileQueueDepthSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQAPProfileQueueDepth
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsQAPProfilePriorityValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQAPProfilePriorityValue
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsQAPProfileDscpValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQAPProfileDscpValue
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsQAPProfileRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQAPProfileRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11CapabilityMappingProfileNameSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsDot11CapabilityMappingProfileName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsDot11CapabilityMappingRowStatusSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsDot11CapabilityMappingRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11AuthMappingProfileNameSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsDot11AuthMappingProfileName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsDot11AuthMappingRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11AuthMappingRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11QosMappingProfileNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11QosMappingProfileName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsDot11QosMappingRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11QosMappingRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsAntennaModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsAntennaMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiIndex->pIndex[1].i4_SLongValue,
                                 pMultiData->i4_SLongValue));

}

INT4
FsAntennaSelectionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsAntennaSelection
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanProfileIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanProfileIfIndex
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanBindWlanIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanBindWlanId
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsDot11WlanBindBssIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanBindBssIfIndex
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11WlanBindRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11WlanBindRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigShortGIfor20MHzSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigShortGIfor20MHz
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigShortGIfor40MHzSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigShortGIfor40MHz
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigChannelWidthSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigChannelWidth
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigAllowChannelWidthSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigAllowChannelWidth
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nMCSDataRateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11nMCSDataRate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsWtpImageVersionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWtpImageVersion
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsWtpUpgradeDevSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWtpUpgradeDev
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsWtpNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWtpName (pMultiIndex->pIndex[0].pOctetStrValue,
                             pMultiData->pOctetStrValue));

}

INT4
FsWtpImageNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWtpImageName
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsWtpAddressTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWtpAddressType
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsWtpServerIPSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWtpServerIP (pMultiIndex->pIndex[0].pOctetStrValue,
                                 pMultiData->pOctetStrValue));

}

INT4
FsWtpRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWtpRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsWlanBeaconsSentCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWlanBeaconsSentCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsWlanMulticastModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWlanMulticastMode (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsWlanMulticastSnoopTableLengthSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsWlanMulticastSnoopTableLength
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsWlanMulticastSnoopTimerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWlanMulticastSnoopTimer
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsWlanMulticastSnoopTimeoutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWlanMulticastSnoopTimeout
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsWlanProbeReqRcvdCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWlanProbeReqRcvdCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
FsWlanProbeRespSentCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWlanProbeRespSentCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsWlanDataPktRcvdCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWlanDataPktRcvdCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsWlanDataPktSentCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWlanDataPktSentCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsDot11aNetworkEnableDep (UINT4 *pu4Error,
                          tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11aNetworkEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11bNetworkEnableDep (UINT4 *pu4Error,
                          tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11bNetworkEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11gSupportDep (UINT4 *pu4Error,
                    tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11gSupport (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11anSupportDep (UINT4 *pu4Error,
                     tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11anSupport (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11bnSupportDep (UINT4 *pu4Error,
                     tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11bnSupport (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11ManagmentSSIDDep (UINT4 *pu4Error,
                         tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11ManagmentSSID
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11CountryStringDep (UINT4 *pu4Error,
                         tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11CountryString
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsDot11SupportedCountryTable (tSnmpIndex * pFirstMultiIndex,
                                          tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11SupportedCountryTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11SupportedCountryTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsDot11CountryCodeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11SupportedCountryTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11CountryCode (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsDot11CountryNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11SupportedCountryTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11CountryName (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsDot11NumericIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11SupportedCountryTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11NumericIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsSecurityWebAuthTypeDep (UINT4 *pu4Error,
                          tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSecurityWebAuthType
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSecurityWebAuthUrlDep (UINT4 *pu4Error,
                         tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSecurityWebAuthUrl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSecurityWebAuthRedirectUrlDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSecurityWebAuthRedirectUrl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSecurityWebAddrDep (UINT4 *pu4Error,
                      tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSecurityWebAddr
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSecurityWebAuthWebTitleDep (UINT4 *pu4Error,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSecurityWebAuthWebTitle
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSecurityWebAuthWebMessageDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSecurityWebAuthWebMessage
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSecurityWebAuthWebLogoFileNameDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSecurityWebAuthWebLogoFileName
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSecurityWebAuthWebSuccMessageDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSecurityWebAuthWebSuccMessage
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSecurityWebAuthWebFailMessageDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSecurityWebAuthWebFailMessage
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSecurityWebAuthWebButtonTextDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSecurityWebAuthWebButtonText
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSecurityWebAuthWebLoadBalInfoDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSecurityWebAuthWebLoadBalInfo
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSecurityWebAuthDisplayLangDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSecurityWebAuthDisplayLang
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSecurityWebAuthColorDep (UINT4 *pu4Error,
                           tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSecurityWebAuthColor
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11StationConfigTableDep (UINT4 *pu4Error,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11StationConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11CapabilityProfileTableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11CapabilityProfileTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11AuthenticationProfileTableDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11AuthenticationProfileTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsSecurityWebAuthGuestInfoTableDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsSecurityWebAuthGuestInfoTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsStationQosParamsTableDep (UINT4 *pu4Error,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsStationQosParamsTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsVlanIsolationTableDep (UINT4 *pu4Error,
                         tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsVlanIsolationTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11RadioConfigTableDep (UINT4 *pu4Error,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11RadioConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11QosProfileTableDep (UINT4 *pu4Error,
                           tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11QosProfileTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11WlanCapabilityProfileTableDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11WlanCapabilityProfileTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11WlanAuthenticationProfileTableDep (UINT4 *pu4Error,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11WlanAuthenticationProfileTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11WlanQosProfileTableDep (UINT4 *pu4Error,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11WlanQosProfileTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11RadioQosTableDep (UINT4 *pu4Error,
                         tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11RadioQosTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11QAPTableDep (UINT4 *pu4Error,
                    tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11QAPTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsQAPProfileTableDep (UINT4 *pu4Error,
                      tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsQAPProfileTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11CapabilityMappingTableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11CapabilityMappingTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11AuthMappingTableDep (UINT4 *pu4Error,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11AuthMappingTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11QosMappingTableDep (UINT4 *pu4Error,
                           tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11QosMappingTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11AntennasListTableDep (UINT4 *pu4Error,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11AntennasListTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11WlanTableDep (UINT4 *pu4Error,
                     tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11WlanTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11WlanBindTableDep (UINT4 *pu4Error,
                         tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11WlanBindTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11nConfigTableDep (UINT4 *pu4Error,
                        tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11nConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11nMCSDataRateTableDep (UINT4 *pu4Error,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11nMCSDataRateTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsWtpImageUpgradeTableDep (UINT4 *pu4Error,
                           tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWtpImageUpgradeTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsWlanStatisticsTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWlanStatisticsTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsWlanSSIDStatsTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexCapwapDot11WlanTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexCapwapDot11WlanTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

INT4
FsWlanSSIDStatsAssocClientCountGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanSSIDStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanSSIDStatsAssocClientCount
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsWlanSSIDStatsMaxClientCountGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanSSIDStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanSSIDStatsMaxClientCount
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsWlanSSIDStatsMaxClientCountSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsWlanSSIDStatsMaxClientCount
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsWlanSSIDStatsMaxClientCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsWlanSSIDStatsMaxClientCount (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->u4_ULongValue));

}

INT4
FsWlanSSIDStatsTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWlanSSIDStatsTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsWlanMulticastTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWlanMulticastTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsWlanClientStatsTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsWlanClientStatsTable ((tMacAddr *)
                                                    pNextMultiIndex->pIndex[0].
                                                    pOctetStrValue->
                                                    pu1_OctetList) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsWlanClientStatsTable
            (*(tMacAddr *) pFirstMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
FsWlanClientStatsMACAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanClientStatsTable ((*(tMacAddr *)
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[0].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
FsWlanClientStatsIdleTimeoutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanClientStatsTable ((*(tMacAddr *)
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanClientStatsIdleTimeout ((*(tMacAddr *) pMultiIndex->
                                                 pIndex[0].pOctetStrValue->
                                                 pu1_OctetList),
                                                &(pMultiData->u4_ULongValue)));

}

INT4
FsWlanClientStatsBytesReceivedCountGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanClientStatsTable ((*(tMacAddr *)
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanClientStatsBytesReceivedCount ((*(tMacAddr *)
                                                        pMultiIndex->pIndex[0].
                                                        pOctetStrValue->
                                                        pu1_OctetList),
                                                       &(pMultiData->
                                                         u4_ULongValue)));

}

INT4
FsWlanClientStatsBytesSentCountGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanClientStatsTable ((*(tMacAddr *)
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanClientStatsBytesSentCount ((*(tMacAddr *) pMultiIndex->
                                                    pIndex[0].pOctetStrValue->
                                                    pu1_OctetList),
                                                   &(pMultiData->
                                                     u4_ULongValue)));

}

INT4
FsWlanClientStatsAuthStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanClientStatsTable ((*(tMacAddr *)
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanClientStatsAuthState ((*(tMacAddr *) pMultiIndex->
                                               pIndex[0].pOctetStrValue->
                                               pu1_OctetList),
                                              &(pMultiData->i4_SLongValue)));

}

INT4
FsWlanClientStatsRadiusUserNameGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanClientStatsTable ((*(tMacAddr *)
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanClientStatsRadiusUserName ((*(tMacAddr *) pMultiIndex->
                                                    pIndex[0].pOctetStrValue->
                                                    pu1_OctetList),
                                                   pMultiData->pOctetStrValue));

}

INT4
FsWlanClientStatsIpAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanClientStatsTable ((*(tMacAddr *)
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanClientStatsIpAddress ((*(tMacAddr *) pMultiIndex->
                                               pIndex[0].pOctetStrValue->
                                               pu1_OctetList),
                                              pMultiData->pOctetStrValue));

}

INT4
FsWlanClientStatsIpAddressTypeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanClientStatsTable ((*(tMacAddr *)
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanClientStatsIpAddressType ((*(tMacAddr *) pMultiIndex->
                                                   pIndex[0].pOctetStrValue->
                                                   pu1_OctetList),
                                                  &(pMultiData->
                                                    i4_SLongValue)));

}

INT4
FsWlanClientStatsAssocTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanClientStatsTable ((*(tMacAddr *)
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanClientStatsAssocTime ((*(tMacAddr *) pMultiIndex->
                                               pIndex[0].pOctetStrValue->
                                               pu1_OctetList),
                                              &(pMultiData->u4_ULongValue)));
}

INT4
FsWlanClientStatsSessionTimeLeftGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanClientStatsTable ((*(tMacAddr *)
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanClientStatsSessionTimeLeft ((*(tMacAddr *) pMultiIndex->
                                                     pIndex[0].pOctetStrValue->
                                                     pu1_OctetList),
                                                    &(pMultiData->
                                                      u4_ULongValue)));

}

INT4
FsWlanClientStatsConnTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanClientStatsTable ((*(tMacAddr *)
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanClientStatsConnTime ((*(tMacAddr *) pMultiIndex->
                                              pIndex[0].pOctetStrValue->
                                              pu1_OctetList),
                                             pMultiData->pOctetStrValue));
}

INT4
FsWlanClientStatsSSIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanClientStatsTable ((*(tMacAddr *)
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanClientStatsSSID ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                          pOctetStrValue->pu1_OctetList),
                                         pMultiData->pOctetStrValue));

}

INT4
FsWlanClientStatsToACStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanClientStatsTable ((*(tMacAddr *)
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanClientStatsToACStatus ((*(tMacAddr *) pMultiIndex->
                                                pIndex[0].pOctetStrValue->
                                                pu1_OctetList),
                                               pMultiData->pOctetStrValue));

}

INT4
FsWlanClientStatsClearGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanClientStatsTable ((*(tMacAddr *)
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanClientStatsClear ((*(tMacAddr *) pMultiIndex->
                                           pIndex[0].pOctetStrValue->
                                           pu1_OctetList),
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsWlanClientStatsToACStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWlanClientStatsToACStatus ((*(tMacAddr *) pMultiIndex->
                                                pIndex[0].pOctetStrValue->
                                                pu1_OctetList),
                                               pMultiData->pOctetStrValue));

}

INT4
FsWlanClientStatsClearSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWlanClientStatsClear ((*(tMacAddr *) pMultiIndex->
                                           pIndex[0].pOctetStrValue->
                                           pu1_OctetList),
                                          pMultiData->i4_SLongValue));

}

INT4
FsWlanClientStatsToACStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsWlanClientStatsToACStatus (pu4Error,
                                                  (*(tMacAddr *) pMultiIndex->
                                                   pIndex[0].pOctetStrValue->
                                                   pu1_OctetList),
                                                  pMultiData->pOctetStrValue));

}

INT4
FsWlanClientStatsClearTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsWlanClientStatsClear (pu4Error,
                                             (*(tMacAddr *) pMultiIndex->
                                              pIndex[0].pOctetStrValue->
                                              pu1_OctetList),
                                             pMultiData->i4_SLongValue));
}

INT4
FsWlanClientStatsTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWlanClientStatsTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsWlanRadioTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsWlanRadioTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsWlanRadioTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsWlanRadioRadiobaseMACAddressGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanRadioTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetFsWlanRadioRadiobaseMACAddress
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList));

}

INT4
FsWlanRadioIfInOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanRadioTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanRadioIfInOctets (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsWlanRadioIfOutOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanRadioTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanRadioIfOutOctets (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsWlanRadioRadioBandGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanRadioTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanRadioRadioBand (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsWlanRadioMaxClientCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanRadioTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanRadioMaxClientCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsWlanRadioClearStatsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanRadioTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanRadioClearStats (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsWlanRadioMaxClientCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWlanRadioMaxClientCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsWlanRadioClearStatsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsWlanRadioClearStats (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsWlanRadioMaxClientCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsWlanRadioMaxClientCount (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsWlanRadioClearStatsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsWlanRadioClearStats (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsWlanRadioTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWlanRadioTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsApWlanStatisticsTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsApWlanStatisticsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsApWlanStatisticsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsApWlanBeaconsSentCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsApWlanStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsApWlanBeaconsSentCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsApWlanProbeReqRcvdCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsApWlanStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsApWlanProbeReqRcvdCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsApWlanProbeRespSentCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsApWlanStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsApWlanProbeRespSentCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsApWlanDataPktRcvdCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsApWlanStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsApWlanDataPktRcvdCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsApWlanDataPktSentCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsApWlanStatisticsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsApWlanDataPktSentCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsApWlanBeaconsSentCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsApWlanBeaconsSentCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsApWlanProbeReqRcvdCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsApWlanProbeReqRcvdCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsApWlanProbeRespSentCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsApWlanProbeRespSentCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsApWlanDataPktRcvdCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsApWlanDataPktRcvdCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsApWlanDataPktSentCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsApWlanDataPktSentCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsApWlanBeaconsSentCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsApWlanBeaconsSentCount (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiData->u4_ULongValue));

}

INT4
FsApWlanProbeReqRcvdCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsApWlanProbeReqRcvdCount (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiData->u4_ULongValue));

}

INT4
FsApWlanProbeRespSentCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsApWlanProbeRespSentCount (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
FsApWlanDataPktRcvdCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsApWlanDataPktRcvdCount (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiData->u4_ULongValue));

}

INT4
FsApWlanDataPktSentCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsApWlanDataPktSentCount (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiData->u4_ULongValue));

}

INT4
FsApWlanStatisticsTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsApWlanStatisticsTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

#ifndef RFMGMT_WANTED
VOID
RegisterFSRRM ()
{
    SNMPRegisterMibWithLock (&fsrrmOID, &fsrrmEntry,
                             WsscfgMainTaskLock, WsscfgMainTaskUnLock,
                             SNMP_MSR_TGR_TRUE);

    SNMPAddSysorEntry (&fsrrmOID, (const UINT1 *) "fsrrm");
}

VOID
UnRegisterFSRRM ()
{
    SNMPUnRegisterMib (&fsrrmOID, &fsrrmEntry);
    SNMPDelSysorEntry (&fsrrmOID, (const UINT1 *) "fsrrm");
}

INT4
GetNextIndexFsRrmConfigTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRrmConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRrmConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRrmDcaModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmDcaMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmDcaChannelSelectionModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmDcaChannelSelectionMode
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmTpcModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmTpcMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmTpcSelectionModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmTpcSelectionMode
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrmConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrmRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FsRrmDcaModeTest (UINT4 *pu4Error,
                  tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmDcaMode (pu4Error,
                                   pMultiIndex->pIndex[0].
                                   i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsRrmDcaChannelSelectionModeTest (UINT4 *pu4Error,
                                  tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmDcaChannelSelectionMode (pu4Error,
                                                   pMultiIndex->pIndex
                                                   [0].i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsRrmTpcModeTest (UINT4 *pu4Error,
                  tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmTpcMode (pu4Error,
                                   pMultiIndex->pIndex[0].
                                   i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsRrmTpcSelectionModeTest (UINT4 *pu4Error,
                           tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmTpcSelectionMode (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsRrmRowStatusTest (UINT4 *pu4Error,
                    tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsRrmRowStatus (pu4Error,
                                     pMultiIndex->pIndex[0].
                                     i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsRrmDcaModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmDcaMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                pMultiData->i4_SLongValue));

}

INT4
FsRrmDcaChannelSelectionModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmDcaChannelSelectionMode
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsRrmTpcModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmTpcMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                pMultiData->i4_SLongValue));

}

INT4
FsRrmTpcSelectionModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmTpcSelectionMode
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsRrmRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrmRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FsRrmConfigTableDep (UINT4 *pu4Error,
                     tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrmConfigTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
#endif

INT4
FsWlanStationTrapStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsWlanStationTrapStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsWlanStationTrapStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsWlanStationTrapStatus (pMultiData->i4_SLongValue));
}

INT4
FsWlanStationTrapStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsWlanStationTrapStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsWlanStationTrapStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWlanStationTrapStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsBandSelectLegacyRateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsBandSelectLegacyRate (&(pMultiData->i4_SLongValue)));
}

INT4
FsBandSelectLegacyRateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsBandSelectLegacyRate (pMultiData->i4_SLongValue));
}

INT4
FsBandSelectLegacyRateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsBandSelectLegacyRate
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsBandSelectLegacyRateDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsBandSelectLegacyRate
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

#ifdef BAND_SELECT_WANTED
INT4
FsBandSelectStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsBandSelectStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsBandSelectStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsBandSelectStatus (pMultiData->i4_SLongValue));
}

INT4
FsBandSelectStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsBandSelectStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsBandSelectStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsBandSelectStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsWlanBandSelectTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsWlanBandSelectTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsWlanBandSelectTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsBandSelectPerWlanStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanBandSelectTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBandSelectPerWlanStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsBandSelectAssocCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanBandSelectTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBandSelectAssocCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsBandSelectAgeOutSuppTimerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanBandSelectTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBandSelectAgeOutSuppTimer
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsBandSelectAssocCountFlushTimerGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanBandSelectTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsBandSelectAssocCountFlushTimer
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsBandSelectPerWlanStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsBandSelectPerWlanStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsBandSelectAssocCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsBandSelectAssocCount (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsBandSelectAgeOutSuppTimerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsBandSelectAgeOutSuppTimer
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsBandSelectAssocCountFlushTimerSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsBandSelectAssocCountFlushTimer
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsBandSelectPerWlanStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsBandSelectPerWlanStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsBandSelectAssocCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsBandSelectAssocCount (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->u4_ULongValue));

}

INT4
FsBandSelectAgeOutSuppTimerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsBandSelectAgeOutSuppTimer (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue));

}

INT4
FsBandSelectAssocCountFlushTimerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsBandSelectAssocCountFlushTimer (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       u4_ULongValue));

}

INT4
FsWlanBandSelectTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWlanBandSelectTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsWlanBandSelectClientTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsWlanBandSelectClientTable ((tMacAddr *)
                                                         pNextMultiIndex->
                                                         pIndex[0].
                                                         pOctetStrValue->
                                                         pu1_OctetList,
                                                         (tMacAddr *)
                                                         pNextMultiIndex->
                                                         pIndex[1].
                                                         pOctetStrValue->
                                                         pu1_OctetList) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsWlanBandSelectClientTable
            (*(tMacAddr *) pFirstMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList,
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].pOctetStrValue->i4_Length = 6;
    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
FsWlanBandSelectStationActiveStatusGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsWlanBandSelectClientTable ((*(tMacAddr *)
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              pOctetStrValue->
                                                              pu1_OctetList),
                                                             (*(tMacAddr *)
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              pOctetStrValue->
                                                              pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsWlanBandSelectStationActiveStatus ((*(tMacAddr *)
                                                        pMultiIndex->pIndex[0].
                                                        pOctetStrValue->
                                                        pu1_OctetList),
                                                       (*(tMacAddr *)
                                                        pMultiIndex->pIndex[1].
                                                        pOctetStrValue->
                                                        pu1_OctetList),
                                                       &(pMultiData->
                                                         i4_SLongValue)));

}
#endif
INT4
FsStationDebugOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsStationDebugOption (&(pMultiData->i4_SLongValue)));
}

INT4
FsWlanDebugOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsWlanDebugOption (&(pMultiData->i4_SLongValue)));
}

INT4
FsRadioDebugOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRadioDebugOption (&(pMultiData->i4_SLongValue)));
}

INT4
FsPmDebugOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPmDebugOption (&(pMultiData->i4_SLongValue)));
}

INT4
FsWssIfDebugOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsWssIfDebugOption (&(pMultiData->i4_SLongValue)));
}

INT4
FsStationDebugOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsStationDebugOption (pMultiData->i4_SLongValue));
}

INT4
FsWlanDebugOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsWlanDebugOption (pMultiData->i4_SLongValue));
}

INT4
FsRadioDebugOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRadioDebugOption (pMultiData->i4_SLongValue));
}

INT4
FsPmDebugOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPmDebugOption (pMultiData->i4_SLongValue));
}

INT4
FsWssIfDebugOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsWssIfDebugOption (pMultiData->i4_SLongValue));
}

INT4
FsStationDebugOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsStationDebugOption
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsWlanDebugOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsWlanDebugOption (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRadioDebugOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRadioDebugOption (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsPmDebugOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPmDebugOption (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsWssIfDebugOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsWssIfDebugOption (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsStationDebugOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsStationDebugOption
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsWlanDebugOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWlanDebugOption
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRadioDebugOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRadioDebugOption
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPmDebugOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPmDebugOption (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsWssIfDebugOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsWssIfDebugOption
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDot11nConfigMIMOPowerSaveGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigMIMOPowerSave
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigDelayedBlockAckOptionActivatedGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigDelayedBlockAckOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigMaxAMSDULengthConfigGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigMaxAMSDULengthConfig
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigMaxRxAMPDUFactorConfigGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigMaxRxAMPDUFactorConfig
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11nConfigMinimumMPDUStartSpacingConfigGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigMinimumMPDUStartSpacingConfig
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11nConfigPCOOptionActivatedGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigPCOOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigTransitionTimeConfigGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigTransitionTimeConfig
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11nConfigMCSFeedbackOptionActivatedGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigMCSFeedbackOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigHTControlFieldSupportedGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigHTControlFieldSupported
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigRDResponderOptionActivatedGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigRDResponderOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigtHTDSSCCKModein40MHzGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigtHTDSSCCKModein40MHz
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigtHTDSSCCKModein40MHzConfigGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigtHTDSSCCKModein40MHzConfig
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigTxRxMCSSetNotEqualGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigTxRxMCSSetNotEqual
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigTxMaximumNumberSpatialStreamsSupportedGet (tSnmpIndex *
                                                         pMultiIndex,
                                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigTxMaximumNumberSpatialStreamsSupported
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11nConfigTxUnequalModulationSupportedGet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigTxUnequalModulationSupported
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigImplicitTransmitBeamformingRecvOptionImplementedGet (tSnmpIndex *
                                                                   pMultiIndex,
                                                                   tRetVal *
                                                                   pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigImplicitTransmitBeamformingRecvOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigImplicitTransmitBeamformingRecvOptionActivatedGet (tSnmpIndex *
                                                                 pMultiIndex,
                                                                 tRetVal *
                                                                 pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigReceiveStaggerSoundingOptionActivatedGet (tSnmpIndex *
                                                        pMultiIndex,
                                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigReceiveStaggerSoundingOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigTransmitStaggerSoundingOptionActivatedGet (tSnmpIndex *
                                                         pMultiIndex,
                                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigTransmitStaggerSoundingOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigReceiveNDPOptionActivatedGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigReceiveNDPOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigTransmitNDPOptionActivatedGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigTransmitNDPOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigImplicitTransmitBeamformingOptionActivatedGet (tSnmpIndex *
                                                             pMultiIndex,
                                                             tRetVal *
                                                             pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigImplicitTransmitBeamformingOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigCalibrationOptionActivatedGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigCalibrationOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigExplicitCSITransmitBeamformingOptionActivatedGet (tSnmpIndex *
                                                                pMultiIndex,
                                                                tRetVal *
                                                                pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivatedGet (tSnmpIndex
                                                                     *
                                                                     pMultiIndex,
                                                                     tRetVal *
                                                                     pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return
        (nmhGetFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigExplicitCompressedBeamformingMatrixOptImplementedGet (tSnmpIndex *
                                                                    pMultiIndex,
                                                                    tRetVal *
                                                                    pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return
        (nmhGetFsDot11nConfigExplicitCompressedBeamformingMatrixOptImplemented
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivatedGet (tSnmpIndex
                                                                     *
                                                                     pMultiIndex,
                                                                     tRetVal *
                                                                     pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return
        (nmhGetFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivatedGet (tSnmpIndex
                                                                     *
                                                                     pMultiIndex,
                                                                     tRetVal *
                                                                     pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return
        (nmhGetFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivatedGet (tSnmpIndex *
                                                                 pMultiIndex,
                                                                 tRetVal *
                                                                 pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivatedGet (tSnmpIndex *
                                                                    pMultiIndex,
                                                                    tRetVal *
                                                                    pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return
        (nmhGetFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigMinimalGroupingImplementedGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigMinimalGroupingImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11nConfigMinimalGroupingActivatedGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigMinimalGroupingActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11nConfigNumberBeamFormingCSISupportAntennaGet (tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigNumberBeamFormingCSISupportAntenna
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntennaGet (tSnmpIndex
                                                                     *
                                                                     pMultiIndex,
                                                                     tRetVal *
                                                                     pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return
        (nmhGetFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna
         (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11nConfigNumberCompressedBeamformingMatrixSupportAntennaGet (tSnmpIndex *
                                                                  pMultiIndex,
                                                                  tRetVal *
                                                                  pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaImplementedGet (tSnmpIndex *
                                                                   pMultiIndex,
                                                                   tRetVal *
                                                                   pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivatedGet (tSnmpIndex *
                                                                 pMultiIndex,
                                                                 tRetVal *
                                                                 pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11nConfigChannelEstimationCapabilityImplementedGet (tSnmpIndex *
                                                         pMultiIndex,
                                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigChannelEstimationCapabilityImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11nConfigChannelEstimationCapabilityActivatedGet (tSnmpIndex * pMultiIndex,
                                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigChannelEstimationCapabilityActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11nConfigCurrentPrimaryChannelGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigCurrentPrimaryChannel
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11nConfigCurrentSecondaryChannelGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigCurrentSecondaryChannel
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11nConfigSTAChannelWidthGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigSTAChannelWidth
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigSTAChannelWidthConfigGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigSTAChannelWidthConfig
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigNonGreenfieldHTSTAsPresentGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigNonGreenfieldHTSTAsPresent
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigNonGreenfieldHTSTAsPresentConfigGet (tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigNonGreenfieldHTSTAsPresentConfig
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigOBSSNonHTSTAsPresentGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigOBSSNonHTSTAsPresent
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigOBSSNonHTSTAsPresentConfigGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigOBSSNonHTSTAsPresentConfig
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigDualBeaconGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigDualBeacon
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigDualBeaconConfigGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigDualBeaconConfig
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigSTBCBeaconGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigSTBCBeacon
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigSTBCBeaconConfigGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigSTBCBeaconConfig
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigPCOPhaseGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigPCOPhase (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigPCOPhaseConfigGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigPCOPhaseConfig
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigMIMOPowerSaveSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigMIMOPowerSave
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigRxSTBCOptionImplementedGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigRxSTBCOptionImplemented
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigRxSTBCOptionActivatedSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigRxSTBCOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigRxSTBCOptionActivatedTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigRxSTBCOptionActivated (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          i4_SLongValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
FsDot11nConfigRxSTBCOptionActivatedGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11nConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11nConfigRxSTBCOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11nConfigDelayedBlockAckOptionActivatedSet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigDelayedBlockAckOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigMaxAMSDULengthConfigSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigMaxAMSDULengthConfig
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigMaxRxAMPDUFactorConfigSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigMaxRxAMPDUFactorConfig
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsDot11nConfigMinimumMPDUStartSpacingConfigSet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigMinimumMPDUStartSpacingConfig
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsDot11nConfigPCOOptionActivatedSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigPCOOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigTransitionTimeConfigSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigTransitionTimeConfig
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsDot11nConfigMCSFeedbackOptionActivatedSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigMCSFeedbackOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigHTControlFieldSupportedSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigHTControlFieldSupported
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigRDResponderOptionActivatedSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigRDResponderOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigtHTDSSCCKModein40MHzConfigSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigtHTDSSCCKModein40MHzConfig
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigTxRxMCSSetNotEqualSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigTxRxMCSSetNotEqual
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigTxMaximumNumberSpatialStreamsSupportedSet (tSnmpIndex *
                                                         pMultiIndex,
                                                         tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigTxMaximumNumberSpatialStreamsSupported
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsDot11nConfigTxUnequalModulationSupportedSet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigTxUnequalModulationSupported
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigImplicitTransmitBeamformingRecvOptionActivatedSet (tSnmpIndex *
                                                                 pMultiIndex,
                                                                 tRetVal *
                                                                 pMultiData)
{
    return (nmhSetFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigReceiveStaggerSoundingOptionActivatedSet (tSnmpIndex *
                                                        pMultiIndex,
                                                        tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigReceiveStaggerSoundingOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigTransmitStaggerSoundingOptionActivatedSet (tSnmpIndex *
                                                         pMultiIndex,
                                                         tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigTransmitStaggerSoundingOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigReceiveNDPOptionActivatedSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigReceiveNDPOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigTransmitNDPOptionActivatedSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigTransmitNDPOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigImplicitTransmitBeamformingOptionActivatedSet (tSnmpIndex *
                                                             pMultiIndex,
                                                             tRetVal *
                                                             pMultiData)
{
    return (nmhSetFsDot11nConfigImplicitTransmitBeamformingOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigCalibrationOptionActivatedSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigCalibrationOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigExplicitCSITransmitBeamformingOptionActivatedSet (tSnmpIndex *
                                                                pMultiIndex,
                                                                tRetVal *
                                                                pMultiData)
{
    return (nmhSetFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivatedSet (tSnmpIndex
                                                                     *
                                                                     pMultiIndex,
                                                                     tRetVal *
                                                                     pMultiData)
{
    return
        (nmhSetFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivatedSet (tSnmpIndex
                                                                     *
                                                                     pMultiIndex,
                                                                     tRetVal *
                                                                     pMultiData)
{
    return
        (nmhSetFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivatedSet (tSnmpIndex
                                                                     *
                                                                     pMultiIndex,
                                                                     tRetVal *
                                                                     pMultiData)
{
    return
        (nmhSetFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivatedSet (tSnmpIndex *
                                                                 pMultiIndex,
                                                                 tRetVal *
                                                                 pMultiData)
{
    return (nmhSetFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivatedSet (tSnmpIndex *
                                                                    pMultiIndex,
                                                                    tRetVal *
                                                                    pMultiData)
{
    return
        (nmhSetFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigMinimalGroupingActivatedSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigMinimalGroupingActivated
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsDot11nConfigNumberBeamFormingCSISupportAntennaSet (tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigNumberBeamFormingCSISupportAntenna
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntennaSet (tSnmpIndex
                                                                     *
                                                                     pMultiIndex,
                                                                     tRetVal *
                                                                     pMultiData)
{
    return
        (nmhSetFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsDot11nConfigNumberCompressedBeamformingMatrixSupportAntennaSet (tSnmpIndex *
                                                                  pMultiIndex,
                                                                  tRetVal *
                                                                  pMultiData)
{
    return (nmhSetFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivatedSet (tSnmpIndex *
                                                                 pMultiIndex,
                                                                 tRetVal *
                                                                 pMultiData)
{
    return (nmhSetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsDot11nConfigChannelEstimationCapabilityActivatedSet (tSnmpIndex * pMultiIndex,
                                                       tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigChannelEstimationCapabilityActivated
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsDot11nConfigCurrentPrimaryChannelSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigCurrentPrimaryChannel
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsDot11nConfigCurrentSecondaryChannelSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigCurrentSecondaryChannel
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsDot11nConfigSTAChannelWidthConfigSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigSTAChannelWidthConfig
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigNonGreenfieldHTSTAsPresentConfigSet (tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigNonGreenfieldHTSTAsPresentConfig
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigOBSSNonHTSTAsPresentConfigSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigOBSSNonHTSTAsPresentConfig
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigDualBeaconConfigSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigDualBeaconConfig
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigSTBCBeaconConfigSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigSTBCBeaconConfig
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigPCOPhaseConfigSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11nConfigPCOPhaseConfig
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigMIMOPowerSaveTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigMIMOPowerSave (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigDelayedBlockAckOptionActivatedTest (UINT4 *pu4Error,
                                                  tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigDelayedBlockAckOptionActivated (pu4Error,
                                                                   pMultiIndex->
                                                                   pIndex[0].
                                                                   i4_SLongValue,
                                                                   pMultiData->
                                                                   i4_SLongValue));

}

INT4
FsDot11nConfigMaxAMSDULengthConfigTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigMaxAMSDULengthConfig (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
FsDot11nConfigMaxRxAMPDUFactorConfigTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigMaxRxAMPDUFactorConfig (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           i4_SLongValue,
                                                           pMultiData->
                                                           u4_ULongValue));

}

INT4
FsDot11nConfigMinimumMPDUStartSpacingConfigTest (UINT4 *pu4Error,
                                                 tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigMinimumMPDUStartSpacingConfig (pu4Error,
                                                                  pMultiIndex->
                                                                  pIndex[0].
                                                                  i4_SLongValue,
                                                                  pMultiData->
                                                                  u4_ULongValue));

}

INT4
FsDot11nConfigPCOOptionActivatedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigPCOOptionActivated (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
FsDot11nConfigTransitionTimeConfigTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigTransitionTimeConfig (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         u4_ULongValue));

}

INT4
FsDot11nConfigMCSFeedbackOptionActivatedTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigMCSFeedbackOptionActivated (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               i4_SLongValue,
                                                               pMultiData->
                                                               i4_SLongValue));

}

INT4
FsDot11nConfigHTControlFieldSupportedTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigHTControlFieldSupported (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            i4_SLongValue,
                                                            pMultiData->
                                                            i4_SLongValue));

}

INT4
FsDot11nConfigRDResponderOptionActivatedTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigRDResponderOptionActivated (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               i4_SLongValue,
                                                               pMultiData->
                                                               i4_SLongValue));

}

INT4
FsDot11nConfigtHTDSSCCKModein40MHzConfigTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigtHTDSSCCKModein40MHzConfig (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               i4_SLongValue,
                                                               pMultiData->
                                                               i4_SLongValue));

}

INT4
FsDot11nConfigTxRxMCSSetNotEqualTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigTxRxMCSSetNotEqual (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
FsDot11nConfigTxMaximumNumberSpatialStreamsSupportedTest (UINT4 *pu4Error,
                                                          tSnmpIndex *
                                                          pMultiIndex,
                                                          tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigTxMaximumNumberSpatialStreamsSupported
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiData->u4_ULongValue));

}

INT4
FsDot11nConfigTxUnequalModulationSupportedTest (UINT4 *pu4Error,
                                                tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigTxUnequalModulationSupported (pu4Error,
                                                                 pMultiIndex->
                                                                 pIndex[0].
                                                                 i4_SLongValue,
                                                                 pMultiData->
                                                                 i4_SLongValue));

}

INT4
FsDot11nConfigImplicitTransmitBeamformingRecvOptionActivatedTest (UINT4
                                                                  *pu4Error,
                                                                  tSnmpIndex *
                                                                  pMultiIndex,
                                                                  tRetVal *
                                                                  pMultiData)
{
    return
        (nmhTestv2FsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated
         (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigReceiveStaggerSoundingOptionActivatedTest (UINT4 *pu4Error,
                                                         tSnmpIndex *
                                                         pMultiIndex,
                                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigReceiveStaggerSoundingOptionActivated
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigTransmitStaggerSoundingOptionActivatedTest (UINT4 *pu4Error,
                                                          tSnmpIndex *
                                                          pMultiIndex,
                                                          tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigTransmitStaggerSoundingOptionActivated
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigReceiveNDPOptionActivatedTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigReceiveNDPOptionActivated (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              i4_SLongValue,
                                                              pMultiData->
                                                              i4_SLongValue));

}

INT4
FsDot11nConfigTransmitNDPOptionActivatedTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigTransmitNDPOptionActivated (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               i4_SLongValue,
                                                               pMultiData->
                                                               i4_SLongValue));

}

INT4
FsDot11nConfigImplicitTransmitBeamformingOptionActivatedTest (UINT4 *pu4Error,
                                                              tSnmpIndex *
                                                              pMultiIndex,
                                                              tRetVal *
                                                              pMultiData)
{
    return (nmhTestv2FsDot11nConfigImplicitTransmitBeamformingOptionActivated
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigCalibrationOptionActivatedTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigCalibrationOptionActivated (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               i4_SLongValue,
                                                               pMultiData->
                                                               i4_SLongValue));

}

INT4
FsDot11nConfigExplicitCSITransmitBeamformingOptionActivatedTest (UINT4
                                                                 *pu4Error,
                                                                 tSnmpIndex *
                                                                 pMultiIndex,
                                                                 tRetVal *
                                                                 pMultiData)
{
    return (nmhTestv2FsDot11nConfigExplicitCSITransmitBeamformingOptionActivated
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivatedTest (UINT4
                                                                      *pu4Error,
                                                                      tSnmpIndex
                                                                      *
                                                                      pMultiIndex,
                                                                      tRetVal *
                                                                      pMultiData)
{
    return
        (nmhTestv2FsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated
         (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivatedTest (UINT4
                                                                      *pu4Error,
                                                                      tSnmpIndex
                                                                      *
                                                                      pMultiIndex,
                                                                      tRetVal *
                                                                      pMultiData)
{
    return
        (nmhTestv2FsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated
         (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivatedTest (UINT4
                                                                      *pu4Error,
                                                                      tSnmpIndex
                                                                      *
                                                                      pMultiIndex,
                                                                      tRetVal *
                                                                      pMultiData)
{
    return
        (nmhTestv2FsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated
         (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivatedTest (UINT4
                                                                  *pu4Error,
                                                                  tSnmpIndex *
                                                                  pMultiIndex,
                                                                  tRetVal *
                                                                  pMultiData)
{
    return
        (nmhTestv2FsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated
         (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivatedTest (UINT4
                                                                     *pu4Error,
                                                                     tSnmpIndex
                                                                     *
                                                                     pMultiIndex,
                                                                     tRetVal *
                                                                     pMultiData)
{
    return
        (nmhTestv2FsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated
         (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiData->i4_SLongValue));

}

INT4
FsDot11nConfigMinimalGroupingActivatedTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigMinimalGroupingActivated (pu4Error,
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             i4_SLongValue,
                                                             pMultiData->
                                                             u4_ULongValue));

}

INT4
FsDot11nConfigNumberBeamFormingCSISupportAntennaTest (UINT4 *pu4Error,
                                                      tSnmpIndex * pMultiIndex,
                                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigNumberBeamFormingCSISupportAntenna (pu4Error,
                                                                       pMultiIndex->
                                                                       pIndex
                                                                       [0].
                                                                       i4_SLongValue,
                                                                       pMultiData->
                                                                       u4_ULongValue));

}

INT4
FsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntennaTest (UINT4
                                                                      *pu4Error,
                                                                      tSnmpIndex
                                                                      *
                                                                      pMultiIndex,
                                                                      tRetVal *
                                                                      pMultiData)
{
    return
        (nmhTestv2FsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna
         (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiData->u4_ULongValue));

}

INT4
FsDot11nConfigNumberCompressedBeamformingMatrixSupportAntennaTest (UINT4
                                                                   *pu4Error,
                                                                   tSnmpIndex *
                                                                   pMultiIndex,
                                                                   tRetVal *
                                                                   pMultiData)
{
    return
        (nmhTestv2FsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna
         (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiData->u4_ULongValue));

}

INT4
FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivatedTest (UINT4
                                                                  *pu4Error,
                                                                  tSnmpIndex *
                                                                  pMultiIndex,
                                                                  tRetVal *
                                                                  pMultiData)
{
    return
        (nmhTestv2FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated
         (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
          pMultiData->u4_ULongValue));

}

INT4
FsDot11nConfigChannelEstimationCapabilityActivatedTest (UINT4 *pu4Error,
                                                        tSnmpIndex *
                                                        pMultiIndex,
                                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigChannelEstimationCapabilityActivated
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiData->u4_ULongValue));

}

INT4
FsDot11nConfigCurrentPrimaryChannelTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigCurrentPrimaryChannel (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          i4_SLongValue,
                                                          pMultiData->
                                                          u4_ULongValue));

}

INT4
FsDot11nConfigCurrentSecondaryChannelTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigCurrentSecondaryChannel (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            i4_SLongValue,
                                                            pMultiData->
                                                            u4_ULongValue));

}

INT4
FsDot11nConfigSTAChannelWidthConfigTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigSTAChannelWidthConfig (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          i4_SLongValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
FsDot11nConfigNonGreenfieldHTSTAsPresentConfigTest (UINT4 *pu4Error,
                                                    tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigNonGreenfieldHTSTAsPresentConfig (pu4Error,
                                                                     pMultiIndex->
                                                                     pIndex[0].
                                                                     i4_SLongValue,
                                                                     pMultiData->
                                                                     i4_SLongValue));

}

INT4
FsDot11nConfigOBSSNonHTSTAsPresentConfigTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigOBSSNonHTSTAsPresentConfig (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               i4_SLongValue,
                                                               pMultiData->
                                                               i4_SLongValue));

}

INT4
FsDot11nConfigDualBeaconConfigTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigDualBeaconConfig (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FsDot11nConfigSTBCBeaconConfigTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigSTBCBeaconConfig (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FsDot11nConfigPCOPhaseConfigTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11nConfigPCOPhaseConfig (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
GetNextIndexFsDot11DscpConfigTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDot11DscpConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDot11DscpConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsDot11DscpInPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11DscpConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11DscpInPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11OutDscpGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11DscpConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11OutDscp (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)));

}

INT4
FsDot11DscpRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDot11DscpConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDot11DscpRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsDot11DscpInPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11DscpInPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
FsDot11OutDscpSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11OutDscp (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiData->u4_ULongValue));

}

INT4
FsDot11DscpRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDot11DscpRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsDot11DscpInPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11DscpInPriority (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiData->u4_ULongValue));

}

INT4
FsDot11OutDscpTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11OutDscp (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiData->u4_ULongValue));

}

INT4
FsDot11DscpRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsDot11DscpRowStatus (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsDot11DscpConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDot11DscpConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsApGroupEnabledStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsApGroupEnabledStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsApGroupEnabledStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsApGroupEnabledStatus (pMultiData->i4_SLongValue));
}

INT4
FsApGroupEnabledStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsApGroupEnabledStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsApGroupEnabledStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsApGroupEnabledStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsApGroupTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsApGroupTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsApGroupTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

INT4
FsApGroupNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsApGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsApGroupName (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->pOctetStrValue));
}

INT4
FsApGroupNameDescriptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsApGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsApGroupNameDescription
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));
}

INT4
FsApGroupRadioPolicyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsApGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsApGroupRadioPolicy (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));
}

INT4
FsApGroupInterfaceVlanGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsApGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsApGroupInterfaceVlan (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)));
}

INT4
FsApGroupRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsApGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsApGroupRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));
}

INT4
FsApGroupNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsApGroupName (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->pOctetStrValue));
}

INT4
FsApGroupNameDescriptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsApGroupNameDescription
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));
}

INT4
FsApGroupRadioPolicySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsApGroupRadioPolicy (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->u4_ULongValue));
}

INT4
FsApGroupInterfaceVlanSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsApGroupInterfaceVlan (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->u4_ULongValue));
}

INT4
FsApGroupRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsApGroupRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue));
}

INT4
FsApGroupNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2FsApGroupName (pu4Error,
                                    pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->pOctetStrValue));
}

INT4
FsApGroupNameDescriptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsApGroupNameDescription (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiData->pOctetStrValue));
}

INT4
FsApGroupRadioPolicyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsApGroupRadioPolicy (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->u4_ULongValue));
}

INT4
FsApGroupInterfaceVlanTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsApGroupInterfaceVlan (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->u4_ULongValue));
}

INT4
FsApGroupRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsApGroupRowStatus (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue));
}

INT4
FsApGroupTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsApGroupTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsApGroupWTPTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsApGroupWTPTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsApGroupWTPTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

INT4
FsApGroupWTPRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsApGroupWTPTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsApGroupWTPRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));
}

INT4
FsApGroupWTPRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsApGroupWTPRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->i4_SLongValue));
}

INT4
FsApGroupWTPRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsApGroupWTPRowStatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue));
}

INT4
FsApGroupWTPTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsApGroupWTPTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsApGroupWLANTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsApGroupWLANTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsApGroupWLANTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

INT4
FsApGroupWLANRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsApGroupWLANTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsApGroupWLANRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsApGroupWLANRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsApGroupWLANRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsApGroupWLANRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsApGroupWLANRowStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsApGroupWLANTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsApGroupWLANTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
