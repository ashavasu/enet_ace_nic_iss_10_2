#ifndef _WSSCFGWLANRADIOC_
#define _WSSCFGWLANRADIOC_
/***************************************************************************** 
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved 
 * 
 * $Id: wsscfgwlanradio.c,v 1.6 2018/01/04 09:54:33 siva Exp $ 
 * 
 * Description: This file contains the configuration routines for WLAN Radio
 *              module. 
 * 
 *****************************************************************************/

#ifdef WLC_WANTED
#include "wsswlanwlcproto.h"
#endif
#include "wsscfginc.h"
#include "capwapcli.h"
#include "wsscfgcli.h"
#include "wsspmdb.h"
#include "capwap.h"
#include "capwapclilwg.h"
#include "wssstawlcprot.h"
extern INT4         pmWTPInfoNotify (UINT1, tWssPMInfo *);
/****************************************************************************
 * Function    :   WssCfgGetTxPowerTable 
 * Input       :  pWsscfgGetDot11PhyTxPowerEntry 
 * Descritpion :  This Routine invoke the Radio  module to send Radio Config 
 *                request 
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgGetTxPowerTable (tWsscfgDot11PhyTxPowerEntry *
                       pWsscfgGetDot11PhyTxPowerEntry)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }

    CONFIG_LOCK ();

    if (pWsscfgGetDot11PhyTxPowerEntry == NULL)
    {
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }

    if (pWsscfgGetDot11PhyTxPowerEntry->MibObject.i4IfIndex == 0)
    {
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        (UINT4) pWsscfgGetDot11PhyTxPowerEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPowerLevel = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bNoOfSupportedPowerLevels = OSIX_TRUE;

    if (RadioIfGetTxPowerTable (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }

    pWsscfgGetDot11PhyTxPowerEntry->MibObject.i4Dot11CurrentTxPowerLevel =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.u2CurrentTxPowerLevel;
    pWsscfgGetDot11PhyTxPowerEntry->MibObject.i4Dot11NumberSupportedPowerLevels
        = (INT4) RadioIfGetDB.RadioIfGetAllDB.u1NoOfSupportedPowerLevels;
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    CONFIG_UNLOCK ();
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : WssCfgGetRadioParams                              */
/*  Description     : The function retrieves radio configurations from  */
/*                    the data base                                     */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/

INT4
WssCfgGetRadioParams (tRadioIfGetDB * pRadioIfGetDB)
{
    if (RadioIfGetAllParams (pRadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : WssCfgGetRadioParams                              */
/*  Description     : The function retrieves EDCA  configurations from  */
/*                    the data base                                     */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
INT4
WssCfgGetEDCAParams (tRadioIfGetDB * pRadioIfGetDB)
{
#ifdef WLC_WANTED
    if (RadioIfGetEDCAParams (pRadioIfGetDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pRadioIfGetDB);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :   WssCfgGetOFDMTable 
 * Input       :  pWsscfgGetDot11PhyOFDMEntry 
 * Descritpion :  This Routine invoke the Radio  module to send Radio Config 
 *                request 
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgGetOFDMTable (tWsscfgDot11PhyOFDMEntry * pWsscfgGetDot11PhyOFDMEntry)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    CONFIG_LOCK ();

    if (pWsscfgGetDot11PhyOFDMEntry == NULL)
    {
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }

    if (pWsscfgGetDot11PhyOFDMEntry->MibObject.i4IfIndex == 0)
    {
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        (UINT4) pWsscfgGetDot11PhyOFDMEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bT1Threshold = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bSupportedBand = OSIX_TRUE;

    if (RadioIfGetOFDMTable (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }

    pWsscfgGetDot11PhyOFDMEntry->MibObject.i4Dot11CurrentFrequency =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;
    pWsscfgGetDot11PhyOFDMEntry->MibObject.i4Dot11TIThreshold =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.u4T1Threshold;
    pWsscfgGetDot11PhyOFDMEntry->MibObject.
        i4Dot11FrequencyBandsSupported =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.u1SupportedBand;
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    CONFIG_UNLOCK ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :   WssCfgGetDSSSTable 
 * Input       :  pWsscfgGetDot11PhyDSSSEntry 
 * Descritpion :  This Routine invoke the Radio  module to send Radio Config 
 *                request 
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgGetDSSSTable (tWsscfgDot11PhyDSSSEntry * pWsscfgGetDot11PhyDSSSEntry)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    CONFIG_LOCK ();

    if (pWsscfgGetDot11PhyDSSSEntry == NULL)
    {
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }

    if (pWsscfgGetDot11PhyDSSSEntry->MibObject.i4IfIndex == 0)
    {
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex
        = (UINT4) pWsscfgGetDot11PhyDSSSEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bEDThreshold = OSIX_TRUE;

    if (RadioIfGetDSSSTable (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }

    pWsscfgGetDot11PhyDSSSEntry->MibObject.i4Dot11CurrentChannel =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel;

    pWsscfgGetDot11PhyDSSSEntry->MibObject.i4Dot11EDThreshold =
        (INT4) RadioIfGetDB.RadioIfGetAllDB.i4EDThreshold;
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    CONFIG_UNLOCK ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetStationConfigTable
 * Input       :   pWsscfgDot11StationConfigEntry
                   pWsscfgIsSetDot11StationConfigEntry
 * Descritpion :  This Routine invoke the Radio If or WLAN module to process 
 *                the request 
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetStationConfigTable (tWsscfgDot11StationConfigEntry *
                             pWsscfgDot11StationConfigEntry,
                             tWsscfgIsSetDot11StationConfigEntry *
                             pWsscfgIsSetDot11StationConfigEntry)
{
    UINT4               u4ErrorCode = 0;
    UINT4              *pu4ErrorCode = &u4ErrorCode;
    tWssWlanDB         *pWssWlanMsgDB = NULL;
    tRadioIfGetDB       RadioIfGetDB;
    tWsscfgCapwapDot11WlanEntry WsscfgCapwapDot11WlanEntry;
    tWsscfgIsSetCapwapDot11WlanEntry WsscfgIsSetCapwapDot11WlanEntry;
    INT4                i4RetValIfMainType = 0;

    pWssWlanMsgDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanMsgDB == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&WsscfgCapwapDot11WlanEntry, 0,
            sizeof (WsscfgCapwapDot11WlanEntry));
    MEMSET (&WsscfgIsSetCapwapDot11WlanEntry, 0,
            sizeof (WsscfgIsSetCapwapDot11WlanEntry));
    MEMSET (pWssWlanMsgDB, 0, sizeof (tWssWlanDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (RadioIfGetDB));
    CONFIG_LOCK ();

    if ((pWsscfgDot11StationConfigEntry == NULL) ||
        (pWsscfgIsSetDot11StationConfigEntry == NULL))
    {
        CONFIG_UNLOCK ();
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
        return OSIX_FAILURE;
    }
    if (nmhGetIfMainType (pWsscfgDot11StationConfigEntry->MibObject.i4IfIndex,
                          &i4RetValIfMainType) == SNMP_FAILURE)
    {
        CONFIG_UNLOCK ();
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
        return OSIX_FAILURE;
    }

    if (i4RetValIfMainType == CFA_CAPWAP_DOT11_PROFILE)
    {
        if (pWsscfgIsSetDot11StationConfigEntry->bDot11DesiredSSID !=
            OSIX_FALSE)
        {
            pWssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex =
                (UINT4) (pWsscfgDot11StationConfigEntry->MibObject.i4IfIndex);

            pWssWlanMsgDB->WssWlanIsPresentDB.bDesiredSsid = OSIX_TRUE;
            MEMCPY (pWssWlanMsgDB->WssWlanAttributeDB.au1DesiredSsid,
                    pWsscfgDot11StationConfigEntry->
                    MibObject.au1Dot11DesiredSSID,
                    STRLEN (pWsscfgDot11StationConfigEntry->
                            MibObject.au1Dot11DesiredSSID));

            if (WssWlanSetStationConfig (pWssWlanMsgDB) != OSIX_SUCCESS)
            {
                pWssWlanMsgDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;
                pWssWlanMsgDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY,
                                              pWssWlanMsgDB) != OSIX_SUCCESS)
                {
                    CONFIG_UNLOCK ();
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
                    return OSIX_FAILURE;
                }

                WsscfgCapwapDot11WlanEntry.MibObject.u4CapwapDot11WlanProfileId
                    = (UINT4) pWssWlanMsgDB->WssWlanAttributeDB.u2WlanProfileId;
                WsscfgIsSetCapwapDot11WlanEntry.bCapwapDot11WlanProfileId
                    = OSIX_TRUE;
                WsscfgCapwapDot11WlanEntry.MibObject.i4CapwapDot11WlanRowStatus
                    = DESTROY;
                WsscfgIsSetCapwapDot11WlanEntry.bCapwapDot11WlanRowStatus
                    = OSIX_TRUE;
                CONFIG_UNLOCK ();
                if (WsscfgTestAllCapwapDot11WlanTable (pu4ErrorCode,
                                                       &WsscfgCapwapDot11WlanEntry,
                                                       &WsscfgIsSetCapwapDot11WlanEntry,
                                                       OSIX_FALSE,
                                                       OSIX_FALSE) !=
                    OSIX_SUCCESS)
                {
                    CONFIG_UNLOCK ();
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
                    return OSIX_FAILURE;
                }
                if (WsscfgSetAllCapwapDot11WlanTable
                    (&WsscfgCapwapDot11WlanEntry,
                     &WsscfgIsSetCapwapDot11WlanEntry, OSIX_TRUE,
                     OSIX_TRUE) != OSIX_SUCCESS)
                {
                    CONFIG_UNLOCK ();
                    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
                    return OSIX_FAILURE;
                }
                CONFIG_UNLOCK ();
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
                return OSIX_FAILURE;
            }
        }

        if (pWsscfgIsSetDot11StationConfigEntry->bDot11DTIMPeriod != OSIX_FALSE)
        {
            pWssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex =
                (UINT4) (pWsscfgDot11StationConfigEntry->MibObject.i4IfIndex);
            pWssWlanMsgDB->WssWlanIsPresentDB.bDtimPeriod = OSIX_TRUE;
            pWssWlanMsgDB->WssWlanAttributeDB.i2DtimPeriod =
                (INT2) (pWsscfgDot11StationConfigEntry->
                        MibObject.i4Dot11DTIMPeriod);
            if (WssWlanSetStationConfig (pWssWlanMsgDB) != OSIX_SUCCESS)
            {
                CONFIG_UNLOCK ();
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
                return OSIX_FAILURE;
            }

        }
    }
    else if ((i4RetValIfMainType == CFA_RADIO) ||
             (i4RetValIfMainType == CFA_CAPWAP_VIRT_RADIO))
    {                            /* TODO: CFA_WLAN_RADIO?? */
        if ((pWsscfgIsSetDot11StationConfigEntry->
             bDot11BeaconPeriod != OSIX_FALSE) ||
            (pWsscfgIsSetDot11StationConfigEntry->bDot11OperationalRateSet
             != OSIX_FALSE))
        {
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                (UINT4) pWsscfgDot11StationConfigEntry->MibObject.i4IfIndex;

            if (pWsscfgIsSetDot11StationConfigEntry->bDot11BeaconPeriod !=
                OSIX_FALSE)
            {
                RadioIfGetDB.RadioIfGetAllDB.u2BeaconPeriod =
                    (UINT2) pWsscfgDot11StationConfigEntry->
                    MibObject.i4Dot11BeaconPeriod;
                RadioIfGetDB.RadioIfIsGetAllDB.bBeaconPeriod = OSIX_TRUE;
            }

            if (pWsscfgIsSetDot11StationConfigEntry->bDot11OperationalRateSet !=
                OSIX_FALSE)
            {
                MEMCPY (RadioIfGetDB.RadioIfGetAllDB.au1OperationalRate,
                        pWsscfgDot11StationConfigEntry->
                        MibObject.au1Dot11OperationalRateSet,
                        sizeof (RadioIfGetDB.
                                RadioIfGetAllDB.au1OperationalRate));

                RadioIfGetDB.RadioIfIsGetAllDB.bOperationalRate = OSIX_TRUE;
            }

        }

        if (pWsscfgDot11StationConfigEntry->
            MibObject.i4Dot11CountryStringLen != 0)
        {
            RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
                (UINT4) pWsscfgDot11StationConfigEntry->MibObject.i4IfIndex;
            MEMCPY (RadioIfGetDB.RadioIfGetAllDB.au1CountryString,
                    pWsscfgDot11StationConfigEntry->
                    MibObject.au1Dot11CountryString,
                    sizeof (RadioIfGetDB.RadioIfGetAllDB.au1CountryString));

            RadioIfGetDB.RadioIfIsGetAllDB.bCountryString = OSIX_TRUE;

        }

        RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
            UtlShMemAllocAntennaSelectionBuf ();

        if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "\r%% Memory allocation for Antenna"
                         " selection failed.\r\n");
            CONFIG_UNLOCK ();
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
            return OSIX_FAILURE;
        }
        if (RadioIfSetStationConfig (&RadioIfGetDB) != OSIX_SUCCESS)
        {
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            CONFIG_UNLOCK ();
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
            return OSIX_FAILURE;
        }
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
    }
    else
    {
        CONFIG_UNLOCK ();
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
        return OSIX_FAILURE;
    }
    CONFIG_UNLOCK ();
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetLegacyRateStatus
 * Input       :  i4IfIndex - Radioif index  
                  pu1OperationalRate - Operat
 * Descritpion :  This routine disable the legacy rates when band steering
 *                feature is enabled
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
#ifdef WLC_WANTED
INT4
WssCfgSetLegacyRateStatus (UINT4 *pu4ProfileId)
{
    UINT4               u4nextProfileId = 0;
    UINT4               u4nextBindingId = 0;
    UINT4               u4RadioType = 0;
    INT4                i4RadioIfIndex = 0;
    UINT4               u4currentBindingId = 0;
    UINT4               u4currentProfileId = 0;
    tSNMP_OCTET_STRING_TYPE OperRate;
    UINT1               au1OperRate[RADIOIF_OPER_RATE];
    tWsscfgDot11StationConfigEntry *pWsscfgDot11StationConfigEntry = NULL;
    tWsscfgIsSetDot11StationConfigEntry WsscfgIsSetDot11StationConfigEntry;

    MEMSET (&au1OperRate, 0, RADIOIF_OPER_RATE);
    OperRate.pu1_OctetList = au1OperRate;

    if (pu4ProfileId)
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        pWsscfgDot11StationConfigEntry =
            (tWsscfgDot11StationConfigEntry *)
            MemAllocMemBlk (WSSCFG_DOT11STATIONCONFIGTABLE_POOLID);
        if (pWsscfgDot11StationConfigEntry == NULL)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WssCfgDisableLegacyRateSet:"
                         " Fail to Allocate Memory.\r\n"));
            return OSIX_FAILURE;
        }
        MEMSET (pWsscfgDot11StationConfigEntry, 0,
                sizeof (tWsscfgDot11StationConfigEntry));
        MEMSET (&WsscfgIsSetDot11StationConfigEntry, 0,
                sizeof (tWsscfgIsSetDot11StationConfigEntry));
        do
        {
            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (*pu4ProfileId != u4currentProfileId)
            {
                continue;
            }
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId, u4currentBindingId,
                 &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex, &u4RadioType) !=
                    SNMP_FAILURE)
                {
                    if ((u4RadioType == RADIO_TYPE_AC)
                        || (u4RadioType == RADIO_TYPE_AN)
                        || (u4RadioType == RADIO_TYPE_A))
                    {
                        continue;
                    }
                }
/*Get Operational rate set for the current if index*/
                nmhGetDot11OperationalRateSet (i4RadioIfIndex, &OperRate);

/*Disabling legacy rates and invoke setStationConfigTable*/
                if (gu1LegacyStatus != WSSCFG_DISABLE)
                {
                    OperRate.pu1_OctetList[CLI_OPER_INDEX_1MB] =
                        CLI_WSSCFG_DISABLE;
                    OperRate.pu1_OctetList[CLI_OPER_INDEX_2MB] =
                        CLI_WSSCFG_DISABLE;
                    OperRate.pu1_OctetList[CLI_OPER_INDEX_5MB] =
                        CLI_WSSCFG_DISABLE;
                }
                else
                {
                    OperRate.pu1_OctetList[CLI_OPER_INDEX_1MB] =
                        CLI_OPER_RATEB_1MB;
                    OperRate.pu1_OctetList[CLI_OPER_INDEX_2MB] =
                        CLI_OPER_RATEB_2MB;
                    OperRate.pu1_OctetList[CLI_OPER_INDEX_5MB] =
                        CLI_OPER_RATEB_5MB;
                }

                MEMCPY (au1OperRate, OperRate.pu1_OctetList,
                        sizeof (au1OperRate));
                MEMCPY (pWsscfgDot11StationConfigEntry->MibObject.
                        au1Dot11OperationalRateSet, au1OperRate,
                        sizeof (au1OperRate));
                WsscfgIsSetDot11StationConfigEntry.bDot11OperationalRateSet
                    = OSIX_TRUE;
                pWsscfgDot11StationConfigEntry->MibObject.i4IfIndex =
                    i4RadioIfIndex;

                if (WssCfgSetStationConfigTable (pWsscfgDot11StationConfigEntry,
                                                 &WsscfgIsSetDot11StationConfigEntry)
                    != OSIX_SUCCESS)
                {
                    continue;
                    /*Proceed with further radios */
                }
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    else
    {
        if (nmhGetFirstIndexFsCapwapWirelessBindingTable
            (&u4nextProfileId, &u4nextBindingId) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        pWsscfgDot11StationConfigEntry =
            (tWsscfgDot11StationConfigEntry *)
            MemAllocMemBlk (WSSCFG_DOT11STATIONCONFIGTABLE_POOLID);
        if (pWsscfgDot11StationConfigEntry == NULL)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WssCfgDisableLegacyRateSet:"
                         " Fail to Allocate Memory.\r\n"));
            return OSIX_FAILURE;
        }
        MEMSET (pWsscfgDot11StationConfigEntry, 0,
                sizeof (tWsscfgDot11StationConfigEntry));
        MEMSET (&WsscfgIsSetDot11StationConfigEntry, 0,
                sizeof (tWsscfgIsSetDot11StationConfigEntry));
        do
        {

            u4currentProfileId = u4nextProfileId;
            u4currentBindingId = u4nextBindingId;
            if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                (u4currentProfileId, u4currentBindingId,
                 &i4RadioIfIndex) == SNMP_SUCCESS)
            {
                if (nmhGetFsDot11RadioType (i4RadioIfIndex, &u4RadioType) !=
                    SNMP_FAILURE)
                {
                    if ((u4RadioType == RADIO_TYPE_AC)
                        || (u4RadioType == RADIO_TYPE_AN)
                        || (u4RadioType == RADIO_TYPE_A))
                    {
                        continue;
                    }
                }
                /*Get Operational rate set for the current if index */
                nmhGetDot11OperationalRateSet (i4RadioIfIndex, &OperRate);

                /*Disabling legacy rates and invoke setStationConfigTable */
                if (gu1LegacyStatus != WSSCFG_DISABLE)
                {
                    OperRate.pu1_OctetList[CLI_OPER_INDEX_1MB] =
                        CLI_WSSCFG_DISABLE;
                    OperRate.pu1_OctetList[CLI_OPER_INDEX_2MB] =
                        CLI_WSSCFG_DISABLE;
                    OperRate.pu1_OctetList[CLI_OPER_INDEX_5MB] =
                        CLI_WSSCFG_DISABLE;
                }
                else
                {
                    OperRate.pu1_OctetList[CLI_OPER_INDEX_1MB] =
                        CLI_OPER_RATEB_1MB;
                    OperRate.pu1_OctetList[CLI_OPER_INDEX_2MB] =
                        CLI_OPER_RATEB_2MB;
                    OperRate.pu1_OctetList[CLI_OPER_INDEX_5MB] =
                        CLI_OPER_RATEB_5MB;
                }

                MEMCPY (au1OperRate, OperRate.pu1_OctetList,
                        sizeof (au1OperRate));
                MEMCPY (pWsscfgDot11StationConfigEntry->MibObject.
                        au1Dot11OperationalRateSet, au1OperRate,
                        sizeof (au1OperRate));
                WsscfgIsSetDot11StationConfigEntry.bDot11OperationalRateSet
                    = OSIX_TRUE;
                pWsscfgDot11StationConfigEntry->MibObject.i4IfIndex =
                    i4RadioIfIndex;

                if (WssCfgSetStationConfigTable (pWsscfgDot11StationConfigEntry,
                                                 &WsscfgIsSetDot11StationConfigEntry)
                    != OSIX_SUCCESS)
                {
                    continue;
                    /*Proceed with further radios */
                }
            }
        }
        while (nmhGetNextIndexFsCapwapWirelessBindingTable
               (u4currentProfileId, &u4nextProfileId,
                u4currentBindingId, &u4nextBindingId) == SNMP_SUCCESS);
    }
    MemReleaseMemBlock (WSSCFG_DOT11STATIONCONFIGTABLE_POOLID,
                        (UINT1 *) pWsscfgDot11StationConfigEntry);
    return OSIX_SUCCESS;
}
#endif
/****************************************************************************
 * Function    :  WssCfgSetAuthenticationAlgorithmTable
 * Input       :   pWsscfgDot11AuthenticationAlgorithmsEntry
                   pWsscfgIsSetDot11AuthenticationAlgorithmsEntry
 * Descritpion :  This Routine invoke the WLAN  module to send WLAN Config 
 *                request 
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetAuthenticationAlgorithmTable (tWsscfgDot11AuthenticationAlgorithmsEntry
                                       *
                                       pWsscfgDot11AuthenticationAlgorithmsEntry,
                                       tWsscfgIsSetDot11AuthenticationAlgorithmsEntry
                                       *
                                       pWsscfgIsSetDot11AuthenticationAlgorithmsEntry)
{
    tWssWlanDB         *pWssWlanDB = NULL;
    pWssWlanDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanDB == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pWssWlanDB, 0, sizeof (tWssWlanDB));

#ifdef WLC_WANTED
    CONFIG_LOCK ();
    pWssWlanDB->WssWlanAttributeDB.u4WlanIfIndex =
        (UINT4) (pWsscfgDot11AuthenticationAlgorithmsEntry->
                 MibObject.i4IfIndex);
    pWssWlanDB->WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

    if (pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->
        bDot11AuthenticationAlgorithmsIndex != OSIX_FALSE)
    {
        pWssWlanDB->WssWlanAttributeDB.u1AuthenticationIndex =
            (UINT1) (pWsscfgDot11AuthenticationAlgorithmsEntry->
                     MibObject.i4Dot11AuthenticationAlgorithmsIndex);
        pWssWlanDB->WssWlanIsPresentDB.bAuthenticationIndex = OSIX_TRUE;
    }
    if (pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->
        bDot11AuthenticationAlgorithmsEnable != OSIX_FALSE)
    {
        pWssWlanDB->WssWlanAttributeDB.u1AuthAlgorithm =
            (UINT1) (pWsscfgDot11AuthenticationAlgorithmsEntry->
                     MibObject.i4Dot11AuthenticationAlgorithmsEnable);
        pWssWlanDB->WssWlanIsPresentDB.bAuthAlgorithm = OSIX_TRUE;
    }
    if (pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->
        bDot11AuthenticationAlgorithm != OSIX_FALSE)
    {
        pWssWlanDB->WssWlanAttributeDB.u1AuthMethod =
            (UINT1) (pWsscfgDot11AuthenticationAlgorithmsEntry->
                     MibObject.i4Dot11AuthenticationAlgorithm);
        pWssWlanDB->WssWlanIsPresentDB.bAuthMethod = OSIX_TRUE;
    }
    if (WssWlanSetAuthenticationAlgorithm (pWssWlanDB) != OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
        return OSIX_FAILURE;
    }
    CONFIG_UNLOCK ();
#else
    UNUSED_PARAM (pWsscfgDot11AuthenticationAlgorithmsEntry);
    UNUSED_PARAM (pWsscfgIsSetDot11AuthenticationAlgorithmsEntry);
#endif
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetWEPDefaultKeysTable
 * Input       :   pWsscfgDot11WEPDefaultKeysEntry
                   pWsscfgIsSetDot11WEPDefaultKeysEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetWEPDefaultKeysTable (tWsscfgDot11WEPDefaultKeysEntry *
                              pWsscfgDot11WEPDefaultKeysEntry,
                              tWsscfgIsSetDot11WEPDefaultKeysEntry *
                              pWsscfgIsSetDot11WEPDefaultKeysEntry)
{
    UNUSED_PARAM (pWsscfgDot11WEPDefaultKeysEntry);
    UNUSED_PARAM (pWsscfgIsSetDot11WEPDefaultKeysEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetWEPKeyMappingsTable
 * Input       :   pWsscfgDot11WEPKeyMappingsEntry
                   pWsscfgIsSetDot11WEPKeyMappingsEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetWEPKeyMappingsTable (tWsscfgDot11WEPKeyMappingsEntry *
                              pWsscfgDot11WEPKeyMappingsEntry,
                              tWsscfgIsSetDot11WEPKeyMappingsEntry *
                              pWsscfgIsSetDot11WEPKeyMappingsEntry)
{
    UNUSED_PARAM (pWsscfgDot11WEPKeyMappingsEntry);
    UNUSED_PARAM (pWsscfgIsSetDot11WEPKeyMappingsEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetMultiDomainCapabilityTable
 * Input       :   pWsscfgDot11MultiDomainCapabilityEntry
                   pWsscfgIsSetDot11MultiDomainCapabilityEntry
 * Descritpion :  This Routine invoke the Radio IF module to send Config 
 *                update request 
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetMultiDomainCapabilityTable (tWsscfgDot11MultiDomainCapabilityEntry *
                                     pWsscfgDot11MultiDomainCapabilityEntry,
                                     tWsscfgIsSetDot11MultiDomainCapabilityEntry
                                     *
                                     pWsscfgIsSetDot11MultiDomainCapabilityEntry)
{
    UNUSED_PARAM (pWsscfgDot11MultiDomainCapabilityEntry);
    UNUSED_PARAM (pWsscfgIsSetDot11MultiDomainCapabilityEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetDot11OperationTable
 * Input       :   pWsscfgDot11OperationEntry
                   pWsscfgIsSetDot11OperationEntry
 * Descritpion :  This Routine invoke the Radio IF module to send Config 
 *                update request 
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetDot11OperationTable (tWsscfgDot11OperationEntry *
                              pWsscfgDot11OperationEntry,
                              tWsscfgIsSetDot11OperationEntry *
                              pWsscfgIsSetDot11OperationEntry)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (RadioIfGetDB));

    if ((pWsscfgDot11OperationEntry == NULL) ||
        (pWsscfgIsSetDot11OperationEntry == NULL))
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetMultiDomainCapability : Null Input received \r\n");
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }
    CONFIG_LOCK ();

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        (UINT4) pWsscfgDot11OperationEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;

    if (pWsscfgIsSetDot11OperationEntry->bDot11RTSThreshold != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u2RTSThreshold =
            (UINT2) pWsscfgDot11OperationEntry->MibObject.i4Dot11RTSThreshold;
        RadioIfGetDB.RadioIfIsGetAllDB.bRTSThreshold = OSIX_TRUE;
    }

    if (pWsscfgIsSetDot11OperationEntry->bDot11ShortRetryLimit != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u1ShortRetry =
            (UINT1) pWsscfgDot11OperationEntry->MibObject.
            i4Dot11ShortRetryLimit;
        RadioIfGetDB.RadioIfIsGetAllDB.bShortRetry = OSIX_TRUE;
    }

    if (pWsscfgIsSetDot11OperationEntry->bDot11LongRetryLimit != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u1LongRetry =
            (UINT1) pWsscfgDot11OperationEntry->MibObject.i4Dot11LongRetryLimit;
        RadioIfGetDB.RadioIfIsGetAllDB.bLongRetry = OSIX_TRUE;
    }

    if (pWsscfgIsSetDot11OperationEntry->bDot11FragmentationThreshold !=
        OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u2FragmentationThreshold =
            (UINT2) pWsscfgDot11OperationEntry->MibObject.
            i4Dot11FragmentationThreshold;
        RadioIfGetDB.RadioIfIsGetAllDB.bFragmentationThreshold = OSIX_TRUE;
    }

    if (pWsscfgIsSetDot11OperationEntry->bDot11MaxTransmitMSDULifetime !=
        OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4TxMsduLifetime =
            pWsscfgDot11OperationEntry->MibObject.
            u4Dot11MaxTransmitMSDULifetime;
        RadioIfGetDB.RadioIfIsGetAllDB.bTxMsduLifetime = OSIX_TRUE;
    }

    if (pWsscfgIsSetDot11OperationEntry->bDot11MaxReceiveLifetime != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4RxMsduLifetime =
            pWsscfgDot11OperationEntry->MibObject.u4Dot11MaxReceiveLifetime;
        RadioIfGetDB.RadioIfIsGetAllDB.bRxMsduLifetime = OSIX_TRUE;
    }

    if (RadioIfSetOperationTable (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    CONFIG_UNLOCK ();
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetFsDot11nFsDot11nConfigTable
 * Input       :   pWsscfgFsDot11nConfigEntry
                   pWsscfgIsSetFsDot11nConfigEntry
 * Descritpion :  This Routine invoke the Radio IF module to send Config 
 *                update request 
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetFsDot11nFsDot11nConfigTable (tWsscfgFsDot11nConfigEntry *
                                      pWsscfgFsDot11nConfigEntry,
                                      tWsscfgIsSetFsDot11nConfigEntry *
                                      pWsscfgIsSetFsDot11nConfigEntry)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (RadioIfGetDB));

    if ((pWsscfgFsDot11nConfigEntry == NULL)
        || (pWsscfgIsSetFsDot11nConfigEntry == NULL))
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "WssCfgSetFsDot11nFsDot11nConfigTable:"
                     "Null Input received \r\n");
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }

    CONFIG_LOCK ();

    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        (UINT4) pWsscfgFsDot11nConfigEntry->MibObject.i4IfIndex;

    if (pWsscfgIsSetFsDot11nConfigEntry->bFsDot11nConfigShortGIfor20MHz !=
        OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bShortGi20 = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1ShortGi20 =
            (UINT1) pWsscfgFsDot11nConfigEntry->
            MibObject.i4FsDot11nConfigShortGIfor20MHz;
    }

    if (pWsscfgIsSetFsDot11nConfigEntry->bFsDot11nConfigShortGIfor40MHz !=
        OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bShortGi40 = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1ShortGi40 =
            (UINT1) pWsscfgFsDot11nConfigEntry->
            MibObject.i4FsDot11nConfigShortGIfor40MHz;
    }

    if (pWsscfgIsSetFsDot11nConfigEntry->bFsDot11nConfigChannelWidth !=
        OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bChannelWidth = OSIX_TRUE;
        if (pWsscfgFsDot11nConfigEntry->
            MibObject.i4FsDot11nConfigChannelWidth ==
            CLI_FSDOT11CONFIG_CHANNELWIDTH_40)
        {
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1ChannelWidth =
                (UINT1) 1;
        }
        else
        {
            RadioIfGetDB.RadioIfGetAllDB.Dot11NCapaParams.u1ChannelWidth =
                (UINT1) 0;
        }

    }

    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }
    if (pWsscfgIsSetFsDot11nConfigEntry->bFsDot11nConfigChannelWidth !=
        OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bStaChanWidth = OSIX_TRUE;
        RadioIfGetDB.RadioIfIsGetAllDB.bSecondaryChannel = OSIX_TRUE;
        if (pWsscfgFsDot11nConfigEntry->
            MibObject.i4FsDot11nConfigChannelWidth ==
            CLI_FSDOT11CONFIG_CHANNELWIDTH_40)
        {
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
                u1StaChanWidth = RADIO_VALUE_1;
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SecondaryChannel =
                RADIO_VALUE_1;
        }
        else
        {
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.
                u1StaChanWidth = RADIO_VALUE_0;
            RadioIfGetDB.RadioIfGetAllDB.Dot11NhtOperation.u1SecondaryChannel =
                RADIO_VALUE_0;
        }

    }

    if (RadioIfSetDot11nOperParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    CONFIG_UNLOCK ();
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetFsDot11nMCSDataRateTable
 * Input       :   pWsscfgFsDot11nMCSDataRateEntry
                   pWsscfgIsSetFsDot11nMCSDataRateEntry
 * Descritpion :  This Routine invoke the Radio IF module to send Config 
 *                update request 
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetFsDot11nMCSDataRateTable (tWsscfgFsDot11nMCSDataRateEntry *
                                   pWsscfgFsDot11nMCSDataRateEntry,
                                   tWsscfgIsSetFsDot11nMCSDataRateEntry *
                                   pWsscfgIsSetFsDot11nMCSDataRateEntry)
{
    tRadioIfGetDB       RadioIfGetDB;
    INT1                i1DataRate = 0;
    INT4                i4DataRateIndex = 0;
    INT4                i4ArrayDataRateIndex = 0;

    MEMSET (&RadioIfGetDB, 0, sizeof (RadioIfGetDB));

    if ((pWsscfgFsDot11nMCSDataRateEntry == NULL)
        || (pWsscfgIsSetFsDot11nMCSDataRateEntry == NULL))
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "WssCfgSetFsDot11nMCSDataRateTable : "
                     "Null Input received \r\n");
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }

    CONFIG_LOCK ();

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        (UINT4) pWsscfgFsDot11nMCSDataRateEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;

    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfGetDB)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "WssCfgSetFsDot11nMCSDataRateTable: "
                     "RadioIf DB access failed. \r\n");
        CONFIG_UNLOCK ();
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfIsGetAllDB.bRxMcsBitmask = OSIX_TRUE;
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB_11N, &RadioIfGetDB)
        != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "WssCfgSetFsDot11nMCSDataRateTable: "
                     "RadioIf DB access failed. \r\n");
        CONFIG_UNLOCK ();
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    i1DataRate =
        (UINT1) pWsscfgFsDot11nMCSDataRateEntry->MibObject.
        i4FsDot11nMCSDataRate;
    i4DataRateIndex =
        pWsscfgFsDot11nMCSDataRateEntry->MibObject.i4FsDot11nMCSDataRateIndex %
        8;
    i4ArrayDataRateIndex =
        pWsscfgFsDot11nMCSDataRateEntry->MibObject.i4FsDot11nMCSDataRateIndex /
        8;
    if (i1DataRate == 1)
    {
        i1DataRate = i1DataRate << i4DataRateIndex;
        RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
            au1RxMcsBitmask[i4ArrayDataRateIndex] |= i1DataRate;
    }
    else
    {
        i1DataRate = 1;
        i1DataRate = i1DataRate << i4DataRateIndex;
        i1DataRate = ~i1DataRate;
        RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
            au1RxMcsBitmask[i4ArrayDataRateIndex] &= i1DataRate;
    }
    if (RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.au1RxMcsBitmask[0] == 0
        && RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
        au1RxMcsBitmask[1] == 0
        && RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
        au1RxMcsBitmask[2] == 0
        && RadioIfGetDB.RadioIfGetAllDB.Dot11NsuppMcsParams.
        au1RxMcsBitmask[3] == 0)
    {
        CONFIG_UNLOCK ();
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "WssCfgSetFsDot11nMCSDataRateTable: "
                     "ERROR: Please Select a MCS Index, should not be None \r\n");
        return OSIX_FAILURE;

    }
    RadioIfGetDB.RadioIfIsGetAllDB.bRxMcsBitmask = OSIX_TRUE;
    if (RadioIfSetFsDot11nParams (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    CONFIG_UNLOCK ();
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetEDCATable
 * Input       :   pWsscfgDot11EDCAEntry
                   pWsscfgIsSetDot11EDCAEntry
 * Descritpion :  This Routine checks set value and invoke corresponding 
                  module 
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetEDCATable (tWsscfgDot11EDCAEntry * pWsscfgDot11EDCAEntry,
                    tWsscfgIsSetDot11EDCAEntry * pWsscfgIsSetDot11EDCAEntry)
{
    tRadioIfGetDB       RadioIfGetDB;
    INT4                i4EdcaTableIndex = 1;
    UINT1               u1SendUpdateWlan = OSIX_FALSE;
    CONFIG_LOCK ();

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex
        = (UINT4) pWsscfgDot11EDCAEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;

    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableIndex != OSIX_FALSE)
    {
        i4EdcaTableIndex =
            (pWsscfgDot11EDCAEntry->MibObject.i4Dot11EDCATableIndex);
        RadioIfGetDB.RadioIfGetAllDB.u1QosIndex =
            (UINT1) (pWsscfgDot11EDCAEntry->MibObject.i4Dot11EDCATableIndex);
        RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableCWmin != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.au2CwMin[i4EdcaTableIndex]
            = (UINT2) pWsscfgDot11EDCAEntry->MibObject.i4Dot11EDCATableCWmin;
        RadioIfGetDB.RadioIfIsGetAllDB.bCwMin = OSIX_TRUE;
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableCWmax != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.au2CwMax[i4EdcaTableIndex]
            = (UINT2) pWsscfgDot11EDCAEntry->MibObject.i4Dot11EDCATableCWmax;
        RadioIfGetDB.RadioIfIsGetAllDB.bCwMax = OSIX_TRUE;
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableAIFSN != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.au1Aifsn[i4EdcaTableIndex]
            = (UINT1) pWsscfgDot11EDCAEntry->MibObject.i4Dot11EDCATableAIFSN;
        RadioIfGetDB.RadioIfIsGetAllDB.bAifsn = OSIX_TRUE;
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableTXOPLimit != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.au2TxOpLimit[i4EdcaTableIndex]
            =
            (UINT2) pWsscfgDot11EDCAEntry->MibObject.i4Dot11EDCATableTXOPLimit;
        RadioIfGetDB.RadioIfIsGetAllDB.bTxOpLimit = OSIX_TRUE;
        u1SendUpdateWlan = OSIX_TRUE;
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableMSDULifetime != OSIX_FALSE)
    {
        /* fill the values */
    }
    if (pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableMandatory != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.au1AdmissionControl[i4EdcaTableIndex]
            =
            (UINT1) (pWsscfgDot11EDCAEntry->MibObject.
                     i4Dot11EDCATableMandatory);
        RadioIfGetDB.RadioIfIsGetAllDB.bAdmissionControl = OSIX_TRUE;
        u1SendUpdateWlan = OSIX_TRUE;
    }
#ifdef WLC_WANTED
    if (RadioIfSetEDCATable (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }
/*SEND Update WLAN message for Changes in Admission control and Txoplimit*/
    if (u1SendUpdateWlan == OSIX_TRUE)
    {
        if (WssWlanUpdateEdcaParams
            (RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex,
             (RadioIfGetDB.RadioIfGetAllDB.u1QosIndex - 1)) != OSIX_SUCCESS)
        {
            CONFIG_UNLOCK ();
            return OSIX_FAILURE;
        }
    }
#endif
    CONFIG_UNLOCK ();
    UNUSED_PARAM (RadioIfGetDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetQAPTable
 * Input       :   pWsscfgFsDot11QAPEntry
                   pWsscfgIsSetFsDot11QAPEntry
 * Descritpion :  This Routine checks set value and invoke corresponding 
                  module 
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetQAPTable (tWsscfgFsDot11QAPEntry * pWsscfgFsDot11QAPEntry,
                   tWsscfgIsSetFsDot11QAPEntry * pWsscfgIsSetFsDot11QAPEntry)
{
    tRadioIfGetDB       RadioIfGetDB;
    INT4                i4EdcaTableIndex = 1;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    CONFIG_LOCK ();

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        (UINT4) pWsscfgFsDot11QAPEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    if (pWsscfgIsSetFsDot11QAPEntry->bDot11EDCATableIndex == OSIX_TRUE)
    {
        i4EdcaTableIndex =
            pWsscfgFsDot11QAPEntry->MibObject.i4Dot11EDCATableIndex;
        RadioIfGetDB.RadioIfGetAllDB.u1QosIndex =
            (UINT1) (pWsscfgFsDot11QAPEntry->MibObject.i4Dot11EDCATableIndex);
        RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
    }
    if (pWsscfgIsSetFsDot11QAPEntry->bFsDot11PriorityValue != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.au1QosPrio[i4EdcaTableIndex] =
            (UINT1) pWsscfgFsDot11QAPEntry->MibObject.i4FsDot11PriorityValue;
        RadioIfGetDB.RadioIfIsGetAllDB.bQosPrio = OSIX_TRUE;
    }
    if (pWsscfgIsSetFsDot11QAPEntry->bFsDot11DscpValue != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.au1Dscp[i4EdcaTableIndex] =
            (UINT1) (pWsscfgFsDot11QAPEntry->MibObject.i4FsDot11DscpValue);
        RadioIfGetDB.RadioIfIsGetAllDB.bDscp = OSIX_TRUE;
    }
#ifdef WLC_WANTED
    if (RadioIfSetEDCATable (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }
#endif
    CONFIG_UNLOCK ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetTxPowerTable
 * Input       :   pWsscfgDot11PhyTxPowerEntry
                   pWsscfgIsSetDot11PhyTxPowerEntry
 * Descritpion :  This Routine checks set value and invoke corresponding
                  module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetTxPowerTable (tWsscfgDot11PhyTxPowerEntry *
                       pWsscfgDot11PhyTxPowerEntry,
                       tWsscfgIsSetDot11PhyTxPowerEntry *
                       pWsscfgIsSetDot11PhyTxPowerEntry)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }

    CONFIG_LOCK ();

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        (UINT4) pWsscfgDot11PhyTxPowerEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    RadioIfGetDB.RadioIfIsGetAllDB.bMaxTxPowerLevel = OSIX_TRUE;

    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB,
                                  &RadioIfGetDB) != OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "WssCfgSetTxPowerTable: RadioIf DB access failed. \r\n");
        CONFIG_UNLOCK ();
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    if (pWsscfgIsSetDot11PhyTxPowerEntry->bDot11CurrentTxPowerLevel !=
        OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u2CurrentTxPowerLevel =
            (UINT2) pWsscfgDot11PhyTxPowerEntry->
            MibObject.i4Dot11CurrentTxPowerLevel;
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentTxPowerLevel = OSIX_TRUE;
    }
    if (RadioIfSetTxPowerTable (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }

    CONFIG_UNLOCK ();
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetAntennasList
 * Input       :   pWsscfgDot11AntennasListEntry
                   pWsscfgIsSetDot11AntennasListEntry
 * Descritpion :  This Routine checks set value and invoke the
                  corresponding module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetDot11AntennasList (tWsscfgDot11AntennasListEntry *
                            pWsscfgDot11AntennasListEntry,
                            tWsscfgIsSetDot11AntennasListEntry *
                            pWsscfgIsSetDot11AntennasListEntry)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }

    CONFIG_LOCK ();

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex
        = (UINT4) pWsscfgDot11AntennasListEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;

    if (pWsscfgIsSetDot11AntennasListEntry->bDot11AntennaListIndex !=
        OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u1QosIndex =
            (UINT1) pWsscfgDot11AntennasListEntry->
            MibObject.i4Dot11AntennaListIndex;
        RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
    }
    if (pWsscfgIsSetDot11AntennasListEntry->bDot11DiversitySelectionRx !=
        OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u1DiversitySupport =
            (UINT1) pWsscfgDot11AntennasListEntry->
            MibObject.i4Dot11DiversitySelectionRx;
        RadioIfGetDB.RadioIfIsGetAllDB.bDiversitySupport = OSIX_TRUE;
    }
    if (RadioIfSetAntennasList (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }
    CONFIG_UNLOCK ();
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetDSSSTable
 * Input       :   pWsscfgDot11PhyDSSSEntry
                   pWsscfgIsSetDot11PhyDSSSEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetDSSSTable (tWsscfgDot11PhyDSSSEntry * pWsscfgDot11PhyDSSSEntry,
                    tWsscfgIsSetDot11PhyDSSSEntry *
                    pWsscfgIsSetDot11PhyDSSSEntry)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }

    CONFIG_LOCK ();
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex
        = (UINT4) pWsscfgDot11PhyDSSSEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;

    if (pWsscfgIsSetDot11PhyDSSSEntry->bDot11EDThreshold != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.i4EDThreshold =
            pWsscfgDot11PhyDSSSEntry->MibObject.i4Dot11EDThreshold;
        RadioIfGetDB.RadioIfIsGetAllDB.bEDThreshold = OSIX_TRUE;
    }

    if (pWsscfgIsSetDot11PhyDSSSEntry->bDot11CurrentChannel != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel =
            (UINT1) pWsscfgDot11PhyDSSSEntry->MibObject.i4Dot11CurrentChannel;
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    }

    if (RadioIfSetDSSSTable (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }
    CONFIG_UNLOCK ();
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetOFDMTable
 * Input       :   pWsscfgDot11PhyOFDMEntry
                   pWsscfgIsSetDot11PhyOFDMEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetOFDMTable (tWsscfgDot11PhyOFDMEntry * pWsscfgDot11PhyOFDMEntry,
                    tWsscfgIsSetDot11PhyOFDMEntry *
                    pWsscfgIsSetDot11PhyOFDMEntry)
{
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }

    CONFIG_LOCK ();
    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        (UINT4) pWsscfgDot11PhyOFDMEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;

    if (pWsscfgIsSetDot11PhyOFDMEntry->bDot11CurrentFrequency != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u1CurrentChannel =
            (UINT1) pWsscfgDot11PhyOFDMEntry->MibObject.i4Dot11CurrentFrequency;
        RadioIfGetDB.RadioIfIsGetAllDB.bCurrentChannel = OSIX_TRUE;
    }
    if (pWsscfgIsSetDot11PhyOFDMEntry->bDot11TIThreshold != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u4T1Threshold =
            (UINT4) pWsscfgDot11PhyOFDMEntry->MibObject.i4Dot11TIThreshold;
        RadioIfGetDB.RadioIfIsGetAllDB.bT1Threshold = OSIX_TRUE;
    }
    if (RadioIfSetOFDMTable (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }
    CONFIG_UNLOCK ();
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetWlanProfile
 * Input       :   pWsscfgCapwapDot11WlanEntry
                   pWsscfgIsSetCapwapDot11WlanEntry                    
 * Descritpion :  This Routine checks set value and invoke corresponding 
                  module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetWlanProfile (tWsscfgCapwapDot11WlanEntry * pWsscfgCapwapDot11WlanEntry,
                      tWsscfgIsSetCapwapDot11WlanEntry *
                      pWsscfgIsSetCapwapDot11WlanEntry)
{
#ifdef WLC_WANTED
    tWssWlanDB         *pWssWlanMsgDB = NULL;
    tWsscfgFsDot11CapabilityMappingEntry WsscfgSetFsDot11CapabilityMappingEntry;
    tWsscfgIsSetFsDot11CapabilityMappingEntry
        WsscfgIsSetFsDot11CapabilityMappingEntry;
    tWsscfgFsDot11QosMappingEntry WsscfgSetFsDot11QosMappingEntry;
    tWsscfgIsSetFsDot11QosMappingEntry WsscfgIsSetFsDot11QosMappingEntry;
    tWsscfgFsDot11AuthMappingEntry WsscfgSetFsDot11AuthMappingEntry;
    tWsscfgFsDot11StationConfigEntry WsscfgFsDot11StationConfigEntry;
    tWsscfgIsSetFsDot11AuthMappingEntry WsscfgIsSetFsDot11AuthMappingEntry;

    tWsscfgFsDot11WlanCapabilityProfileEntry
        WsscfgSetFsDot11WlanCapabilityProfileEntry;
    tWsscfgIsSetFsDot11WlanCapabilityProfileEntry
        WsscfgIsSetFsDot11WlanCapabilityProfileEntry;
    tWsscfgFsDot11WlanQosProfileEntry WsscfgSetFsDot11WlanQosProfileEntry;
    tWsscfgIsSetFsDot11WlanQosProfileEntry
        WsscfgIsSetFsDot11WlanQosProfileEntry;
    tWsscfgFsDot11WlanAuthenticationProfileEntry
        WsscfgSetFsDot11WlanAuthenticationProfileEntry;
    tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry
        WsscfgIsSetFsDot11WlanAuthenticationProfileEntry;
    tWssPMInfo          WssPmInfo;
    tWsscfgFsDot11StationConfigEntry *pWsscfgFsDot11StationConfigEntry = NULL;
    pWssWlanMsgDB = (tWssWlanDB *) (VOID *) UtlShMemAllocWlanDbBuf ();
    if (pWssWlanMsgDB == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&WssPmInfo, 0, sizeof (tWssPMInfo));
    MEMSET (pWssWlanMsgDB, 0, sizeof (tWssWlanDB));
    MEMSET (&WsscfgSetFsDot11CapabilityMappingEntry, 0,
            sizeof (tWsscfgFsDot11CapabilityMappingEntry));
    MEMSET (&WsscfgIsSetFsDot11CapabilityMappingEntry, 0,
            sizeof (tWsscfgIsSetFsDot11CapabilityMappingEntry));
    MEMSET (&WsscfgSetFsDot11QosMappingEntry, 0,
            sizeof (tWsscfgFsDot11QosMappingEntry));
    MEMSET (&WsscfgIsSetFsDot11QosMappingEntry, 0,
            sizeof (tWsscfgIsSetFsDot11QosMappingEntry));
    MEMSET (&WsscfgSetFsDot11AuthMappingEntry, 0,
            sizeof (tWsscfgFsDot11AuthMappingEntry));
    MEMSET (&WsscfgIsSetFsDot11AuthMappingEntry, 0,
            sizeof (tWsscfgIsSetFsDot11AuthMappingEntry));

    MEMSET (&WsscfgSetFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanCapabilityProfileEntry));
    MEMSET (&WsscfgIsSetFsDot11WlanCapabilityProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11WlanCapabilityProfileEntry));
    MEMSET (&WsscfgSetFsDot11WlanQosProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanQosProfileEntry));
    MEMSET (&WsscfgIsSetFsDot11WlanQosProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11WlanQosProfileEntry));
    MEMSET (&WsscfgSetFsDot11WlanAuthenticationProfileEntry, 0,
            sizeof (tWsscfgFsDot11WlanAuthenticationProfileEntry));
    MEMSET (&WsscfgIsSetFsDot11WlanAuthenticationProfileEntry, 0,
            sizeof (tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry));

    if ((pWsscfgIsSetCapwapDot11WlanEntry == NULL) ||
        (pWsscfgCapwapDot11WlanEntry == NULL))
    {
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
        return OSIX_FAILURE;
    }

    CONFIG_LOCK ();

    if (pWsscfgCapwapDot11WlanEntry->MibObject.u4CapwapDot11WlanProfileId == 0)
    {
        CONFIG_UNLOCK ();
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
        return OSIX_FAILURE;
    }

    pWssWlanMsgDB->WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    pWssWlanMsgDB->WssWlanAttributeDB.u2WlanProfileId =
        (UINT2) (pWsscfgCapwapDot11WlanEntry->
                 MibObject.u4CapwapDot11WlanProfileId);

    if (pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanMacType != OSIX_FALSE)
    {
        pWssWlanMsgDB->WssWlanIsPresentDB.bWlanMacType = OSIX_TRUE;
        pWssWlanMsgDB->WssWlanAttributeDB.u1MacType =
            (UINT1) (pWsscfgCapwapDot11WlanEntry->
                     MibObject.i4CapwapDot11WlanMacType);
    }
    if (pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanTunnelMode !=
        OSIX_FALSE)
    {
        pWssWlanMsgDB->WssWlanIsPresentDB.bWlanTunnelMode = OSIX_TRUE;
        pWssWlanMsgDB->WssWlanAttributeDB.u1TunnelMode =
            pWsscfgCapwapDot11WlanEntry->
            MibObject.au1CapwapDot11WlanTunnelMode[0];
    }
    if (pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanRowStatus !=
        OSIX_FALSE)
    {
        if (pWsscfgCapwapDot11WlanEntry->MibObject.
            i4CapwapDot11WlanRowStatus == NOT_READY)
        {
            pWssWlanMsgDB->WssWlanIsPresentDB.bRowStatus = OSIX_TRUE;
            pWssWlanMsgDB->WssWlanAttributeDB.u1RowStatus = CREATE_AND_WAIT;
            WssPmInfo.u4FsDot11WlanProfileId =
                pWssWlanMsgDB->WssWlanAttributeDB.u2WlanProfileId;
            if (pmWTPInfoNotify (SSID_ADD, &WssPmInfo) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "Create SSID : Update PM Stats DB Failed \r\n");
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
                CONFIG_UNLOCK ();
                return OSIX_FAILURE;
            }
        }
        else if (pWsscfgCapwapDot11WlanEntry->
                 MibObject.i4CapwapDot11WlanRowStatus == DESTROY)
        {
            pWssWlanMsgDB->WssWlanIsPresentDB.bRowStatus = OSIX_TRUE;
            pWssWlanMsgDB->WssWlanAttributeDB.u1RowStatus =
                (UINT1) (pWsscfgCapwapDot11WlanEntry->
                         MibObject.i4CapwapDot11WlanRowStatus);
            pWssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex =
                (UINT4) (pWsscfgCapwapDot11WlanEntry->
                         MibObject.i4CapwapDot11WlanProfileIfIndex);
        }
        else
        {
            pWssWlanMsgDB->WssWlanIsPresentDB.bRowStatus = OSIX_TRUE;
            pWssWlanMsgDB->WssWlanAttributeDB.u1RowStatus =
                (UINT1) (pWsscfgCapwapDot11WlanEntry->
                         MibObject.i4CapwapDot11WlanRowStatus);
        }
    }

    if (WssWlanSetWlanProfile (pWssWlanMsgDB) != OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
        return OSIX_FAILURE;
    }

    if (pWsscfgCapwapDot11WlanEntry->
        MibObject.i4CapwapDot11WlanRowStatus == DESTROY)
    {
        WssPmInfo.u4FsDot11WlanProfileId =
            pWssWlanMsgDB->WssWlanAttributeDB.u2WlanProfileId;
        if (pmWTPInfoNotify (SSID_DEL, &WssPmInfo) != OSIX_SUCCESS)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "Delete SSID : Update PM Stats DB Failed \r\n");
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
            CONFIG_UNLOCK ();
        }
        /* Delete Entry for the following */
        MEMSET (&WsscfgFsDot11StationConfigEntry, 0,
                sizeof (tWsscfgFsDot11StationConfigEntry));

        WsscfgFsDot11StationConfigEntry.MibObject.i4IfIndex =
            (INT4) (pWssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex);
        pWsscfgFsDot11StationConfigEntry =
            RBTreeGet (gWsscfgGlobals.
                       WsscfgGlbMib.FsDot11StationConfigTable,
                       (tRBElem *) & WsscfgFsDot11StationConfigEntry);

        if (pWsscfgFsDot11StationConfigEntry != NULL)
        {
            RBTreeRem (gWsscfgGlobals.WsscfgGlbMib.FsDot11StationConfigTable,
                       &WsscfgFsDot11StationConfigEntry);
            MemReleaseMemBlock (WSSCFG_FSDOT11STATIONCONFIGTABLE_POOLID,
                                (UINT1 *) pWsscfgFsDot11StationConfigEntry);
        }

        WsscfgSetFsDot11CapabilityMappingEntry.MibObject.i4IfIndex =
            (INT4) (pWssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex);
        WsscfgSetFsDot11CapabilityMappingEntry.
            MibObject.i4FsDot11CapabilityMappingProfileNameLen =
            (INT4) (strlen (WSS_DEF_PROFILE_NAME));
        MEMCPY (WsscfgSetFsDot11CapabilityMappingEntry.
                MibObject.au1FsDot11CapabilityMappingProfileName,
                WSS_DEF_PROFILE_NAME,
                WsscfgSetFsDot11CapabilityMappingEntry.
                MibObject.i4FsDot11CapabilityMappingProfileNameLen);
        WsscfgSetFsDot11CapabilityMappingEntry.
            MibObject.i4FsDot11CapabilityMappingRowStatus = DESTROY;
        WsscfgIsSetFsDot11CapabilityMappingEntry.bIfIndex = OSIX_TRUE;
        WsscfgIsSetFsDot11CapabilityMappingEntry.
            bFsDot11CapabilityMappingProfileName = OSIX_TRUE;
        WsscfgIsSetFsDot11CapabilityMappingEntry.
            bFsDot11CapabilityMappingRowStatus = OSIX_TRUE;

        if (WsscfgSetAllFsDot11CapabilityMappingTable
            (&WsscfgSetFsDot11CapabilityMappingEntry,
             &WsscfgIsSetFsDot11CapabilityMappingEntry, OSIX_TRUE,
             OSIX_TRUE) != OSIX_SUCCESS)
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
            return OSIX_FAILURE;
        }

        WsscfgSetFsDot11AuthMappingEntry.MibObject.i4IfIndex =
            (INT4) (pWssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex);
        WsscfgSetFsDot11AuthMappingEntry.
            MibObject.i4FsDot11AuthMappingProfileNameLen =
            (INT4) (strlen (WSS_DEF_PROFILE_NAME));
        MEMCPY (WsscfgSetFsDot11AuthMappingEntry.
                MibObject.au1FsDot11AuthMappingProfileName,
                WSS_DEF_PROFILE_NAME,
                WsscfgSetFsDot11AuthMappingEntry.MibObject.
                i4FsDot11AuthMappingProfileNameLen);
        WsscfgSetFsDot11AuthMappingEntry.MibObject.
            i4FsDot11AuthMappingRowStatus = DESTROY;
        WsscfgIsSetFsDot11AuthMappingEntry.bIfIndex = OSIX_TRUE;
        WsscfgIsSetFsDot11AuthMappingEntry.
            bFsDot11AuthMappingProfileName = OSIX_TRUE;
        WsscfgIsSetFsDot11AuthMappingEntry.
            bFsDot11AuthMappingRowStatus = OSIX_TRUE;
        if (WsscfgSetAllFsDot11AuthMappingTable
            (&WsscfgSetFsDot11AuthMappingEntry,
             &WsscfgIsSetFsDot11AuthMappingEntry, OSIX_TRUE,
             OSIX_TRUE) != OSIX_SUCCESS)
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
            return OSIX_FAILURE;
        }

        WsscfgSetFsDot11QosMappingEntry.MibObject.i4IfIndex =
            (INT4) (pWssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex);
        WsscfgSetFsDot11QosMappingEntry.
            MibObject.i4FsDot11QosMappingProfileNameLen =
            (INT4) (strlen (WSS_DEF_PROFILE_NAME));
        MEMCPY (WsscfgSetFsDot11QosMappingEntry.
                MibObject.au1FsDot11QosMappingProfileName, WSS_DEF_PROFILE_NAME,
                WsscfgSetFsDot11QosMappingEntry.
                MibObject.i4FsDot11QosMappingProfileNameLen);
        WsscfgSetFsDot11QosMappingEntry.
            MibObject.i4FsDot11QosMappingRowStatus = DESTROY;
        WsscfgIsSetFsDot11QosMappingEntry.bIfIndex = OSIX_TRUE;
        WsscfgIsSetFsDot11QosMappingEntry.
            bFsDot11QosMappingProfileName = OSIX_TRUE;
        WsscfgIsSetFsDot11QosMappingEntry.
            bFsDot11QosMappingRowStatus = OSIX_TRUE;
        if (WsscfgSetAllFsDot11QosMappingTable
            (&WsscfgSetFsDot11QosMappingEntry,
             &WsscfgIsSetFsDot11QosMappingEntry, OSIX_TRUE,
             OSIX_TRUE) != OSIX_SUCCESS)
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
            return OSIX_FAILURE;
        }
    }
    if (pWssWlanMsgDB->WssWlanIsPresentDB.bWlanIfIndex != OSIX_FALSE)
    {
        pWsscfgCapwapDot11WlanEntry->
            MibObject.i4CapwapDot11WlanProfileIfIndex =
            (INT4) (pWssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex);

        /* Create Entry for the following mib tables */
        pWsscfgFsDot11StationConfigEntry =
            (tWsscfgFsDot11StationConfigEntry *)
            MemAllocMemBlk (WSSCFG_FSDOT11STATIONCONFIGTABLE_POOLID);

        if (pWsscfgFsDot11StationConfigEntry != NULL)
        {
            MEMSET (pWsscfgFsDot11StationConfigEntry, 0,
                    sizeof (tWsscfgFsDot11StationConfigEntry));

            pWsscfgFsDot11StationConfigEntry->MibObject.i4IfIndex =
                (INT4) (pWssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex);
            if (RBTreeAdd
                (gWsscfgGlobals.WsscfgGlbMib.FsDot11StationConfigTable,
                 (tRBElem *) pWsscfgFsDot11StationConfigEntry) != RB_SUCCESS)
            {
                CONFIG_UNLOCK ();
                UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
                return OSIX_FAILURE;
            }
        }
        else
        {
            CONFIG_UNLOCK ();
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
            return OSIX_FAILURE;
        }

        WsscfgSetFsDot11CapabilityMappingEntry.MibObject.i4IfIndex =
            (INT4) (pWssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex);
        WsscfgSetFsDot11CapabilityMappingEntry.
            MibObject.i4FsDot11CapabilityMappingProfileNameLen =
            (INT4) (strlen (WSS_DEF_PROFILE_NAME));
        MEMCPY (WsscfgSetFsDot11CapabilityMappingEntry.
                MibObject.au1FsDot11CapabilityMappingProfileName,
                WSS_DEF_PROFILE_NAME,
                WsscfgSetFsDot11CapabilityMappingEntry.
                MibObject.i4FsDot11CapabilityMappingProfileNameLen);
        WsscfgSetFsDot11CapabilityMappingEntry.
            MibObject.i4FsDot11CapabilityMappingRowStatus = CREATE_AND_GO;
        WsscfgIsSetFsDot11CapabilityMappingEntry.bIfIndex = OSIX_TRUE;
        WsscfgIsSetFsDot11CapabilityMappingEntry.
            bFsDot11CapabilityMappingProfileName = OSIX_TRUE;
        WsscfgIsSetFsDot11CapabilityMappingEntry.
            bFsDot11CapabilityMappingRowStatus = OSIX_TRUE;

        if (WsscfgSetAllFsDot11CapabilityMappingTable
            (&WsscfgSetFsDot11CapabilityMappingEntry,
             &WsscfgIsSetFsDot11CapabilityMappingEntry, OSIX_TRUE,
             OSIX_TRUE) != OSIX_SUCCESS)
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
            return OSIX_FAILURE;
        }

        WsscfgSetFsDot11AuthMappingEntry.MibObject.i4IfIndex =
            (INT4) (pWssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex);
        WsscfgSetFsDot11AuthMappingEntry.
            MibObject.i4FsDot11AuthMappingProfileNameLen =
            (INT4) (strlen (WSS_DEF_PROFILE_NAME));
        MEMCPY (WsscfgSetFsDot11AuthMappingEntry.
                MibObject.au1FsDot11AuthMappingProfileName,
                WSS_DEF_PROFILE_NAME,
                WsscfgSetFsDot11AuthMappingEntry.MibObject.
                i4FsDot11AuthMappingProfileNameLen);
        WsscfgSetFsDot11AuthMappingEntry.MibObject.
            i4FsDot11AuthMappingRowStatus = CREATE_AND_GO;
        WsscfgIsSetFsDot11AuthMappingEntry.bIfIndex = OSIX_TRUE;
        WsscfgIsSetFsDot11AuthMappingEntry.
            bFsDot11AuthMappingProfileName = OSIX_TRUE;
        WsscfgIsSetFsDot11AuthMappingEntry.
            bFsDot11AuthMappingRowStatus = OSIX_TRUE;
        if (WsscfgSetAllFsDot11AuthMappingTable
            (&WsscfgSetFsDot11AuthMappingEntry,
             &WsscfgIsSetFsDot11AuthMappingEntry, OSIX_TRUE,
             OSIX_TRUE) != OSIX_SUCCESS)
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
            return OSIX_FAILURE;
        }

        WsscfgSetFsDot11QosMappingEntry.MibObject.i4IfIndex =
            (INT4) (pWssWlanMsgDB->WssWlanAttributeDB.u4WlanIfIndex);
        WsscfgSetFsDot11QosMappingEntry.
            MibObject.i4FsDot11QosMappingProfileNameLen =
            (INT4) (strlen (WSS_DEF_PROFILE_NAME));
        MEMCPY (WsscfgSetFsDot11QosMappingEntry.
                MibObject.au1FsDot11QosMappingProfileName, WSS_DEF_PROFILE_NAME,
                WsscfgSetFsDot11QosMappingEntry.
                MibObject.i4FsDot11QosMappingProfileNameLen);
        WsscfgSetFsDot11QosMappingEntry.
            MibObject.i4FsDot11QosMappingRowStatus = CREATE_AND_GO;
        WsscfgIsSetFsDot11QosMappingEntry.bIfIndex = OSIX_TRUE;
        WsscfgIsSetFsDot11QosMappingEntry.
            bFsDot11QosMappingProfileName = OSIX_TRUE;
        WsscfgIsSetFsDot11QosMappingEntry.
            bFsDot11QosMappingRowStatus = OSIX_TRUE;
        if (WsscfgSetAllFsDot11QosMappingTable
            (&WsscfgSetFsDot11QosMappingEntry,
             &WsscfgIsSetFsDot11QosMappingEntry, OSIX_TRUE,
             OSIX_TRUE) != OSIX_SUCCESS)
        {
            UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
            return OSIX_FAILURE;
        }
    }
    CONFIG_UNLOCK ();
    UtlShMemFreeWlanDbBuf ((UINT1 *) pWssWlanMsgDB);
#else
    UNUSED_PARAM (pWsscfgIsSetCapwapDot11WlanEntry);
    UNUSED_PARAM (pWsscfgCapwapDot11WlanEntry);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgSetFsDot11WlanCapabilityProfile
 * Input       :  pWsscfgCapwapDot11CapabilityProfileEntry 
                  pWsscfgIsSetCapwapDot11CapabilityProfileEntry                
 * Descritpion :  This Routine checks set value and invoke corresponding 
                  module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgSetFsDot11WlanCapabilityProfile
    (tWsscfgFsDot11WlanCapabilityProfileEntry *
     pWsscfgFsDot11WlanCapabilityProfileEntry,
     tWsscfgIsSetFsDot11WlanCapabilityProfileEntry *
     pWsscfgIsSetFsDot11WlanCapabilityProfileEntry)
{
    tWssWlanDB          WssWlanDB;
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));

    if ((pWsscfgFsDot11WlanCapabilityProfileEntry == NULL) ||
        (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry == NULL))
    {
        return OSIX_FAILURE;
    }
#ifdef WLC_WANTED
    /* Lock is taken away since we have already taken lock at WlanCreate */
    if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
        bFsDot11WlanCapabilityRowStatus != OSIX_FALSE)
    {
        if (pWsscfgFsDot11WlanCapabilityProfileEntry->
            MibObject.i4FsDot11WlanCapabilityRowStatus == DESTROY)
        {
            return OSIX_SUCCESS;
        }
        WssWlanDB.WssWlanIsPresentDB.bCfPollable = OSIX_TRUE;
        WssWlanDB.WssWlanAttributeDB.u1CfPollable =
            (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                     MibObject.i4FsDot11WlanCFPollable);

        WssWlanDB.WssWlanIsPresentDB.bCfPollRequest = OSIX_TRUE;
        WssWlanDB.WssWlanAttributeDB.u1CfPollRequest =
            (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                     MibObject.i4FsDot11WlanCFPollRequest);
        WssWlanDB.WssWlanIsPresentDB.bPrivacyOptionImplemented = OSIX_TRUE;
        WssWlanDB.WssWlanAttributeDB.u1PrivacyOptionImplemented =
            (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                     MibObject.i4FsDot11WlanPrivacyOptionImplemented);
        WssWlanDB.WssWlanIsPresentDB.bShortPreambleOptionImplemented =
            OSIX_TRUE;
        WssWlanDB.WssWlanAttributeDB.
            u1ShortPreambleOptionImplemented =
            (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                     MibObject.i4FsDot11WlanShortPreambleOptionImplemented);
        WssWlanDB.WssWlanIsPresentDB.bPBCCOptionImplemented = OSIX_TRUE;
        WssWlanDB.WssWlanAttributeDB.u1PBCCOptionImplemented =
            (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                     MibObject.i4FsDot11WlanPBCCOptionImplemented);
        WssWlanDB.WssWlanIsPresentDB.bChannelAgilityPresent = OSIX_TRUE;
        WssWlanDB.WssWlanAttributeDB.u1ChannelAgilityPresent =
            (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                     MibObject.i4FsDot11WlanChannelAgilityPresent);
        WssWlanDB.WssWlanIsPresentDB.bQosOptionImplemented = OSIX_TRUE;
        WssWlanDB.WssWlanAttributeDB.u1QosOptionImplemented =
            (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                     MibObject.i4FsDot11WlanQosOptionImplemented);
        WssWlanDB.WssWlanIsPresentDB.bSpectrumManagementRequired = OSIX_TRUE;
        WssWlanDB.WssWlanAttributeDB.u1SpectrumManagementRequired =
            (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                     MibObject.i4FsDot11WlanSpectrumManagementRequired);
        WssWlanDB.WssWlanIsPresentDB.bShortSlotTimeOptionImplemented =
            OSIX_TRUE;
        WssWlanDB.WssWlanAttributeDB.
            u1ShortSlotTimeOptionImplemented =
            (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                     MibObject.i4FsDot11WlanShortSlotTimeOptionImplemented);
        WssWlanDB.WssWlanIsPresentDB.bAPSDOptionImplemented = OSIX_TRUE;
        WssWlanDB.WssWlanAttributeDB.u1APSDOptionImplemented =
            (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                     MibObject.i4FsDot11WlanAPSDOptionImplemented);
        WssWlanDB.WssWlanIsPresentDB.bDSSSOFDMOptionEnabled = OSIX_TRUE;
        WssWlanDB.WssWlanAttributeDB.u1DSSSOFDMOptionEnabled =
            (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                     MibObject.i4FsDot11WlanDSSSOFDMOptionEnabled);
        WssWlanDB.WssWlanIsPresentDB.
            bDelayedBlockAckOptionImplemented = OSIX_TRUE;
        WssWlanDB.WssWlanAttributeDB.
            u1DelayedBlockAckOptionImplemented =
            (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                     MibObject.i4FsDot11WlanDelayedBlockAckOptionImplemented);
        WssWlanDB.WssWlanIsPresentDB.
            bImmediateBlockAckOptionImplemented = OSIX_TRUE;
        WssWlanDB.WssWlanAttributeDB.
            u1ImmediateBlockAckOptionImplemented =
            (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                     MibObject.i4FsDot11WlanImmediateBlockAckOptionImplemented);
        WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex =
            (UINT4) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                     MibObject.i4IfIndex);

        WssWlanDB.WssWlanIsPresentDB.bQAckOptionImplemented = OSIX_TRUE;
        WssWlanDB.WssWlanAttributeDB.u1QAckOptionImplemented =
            (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                     MibObject.i4FsDot11WlanQAckOptionImplemented);
        WssWlanDB.WssWlanIsPresentDB.bQueueRequestOptionImplemented = OSIX_TRUE;
        WssWlanDB.WssWlanAttributeDB.u1QueueRequestOptionImplemented =
            (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                     MibObject.i4FsDot11WlanQueueRequestOptionImplemented);
        WssWlanDB.WssWlanIsPresentDB.bTXOPRequestOptionImplemented = OSIX_TRUE;
        WssWlanDB.WssWlanAttributeDB.u1TXOPRequestOptionImplemented =
            (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                     MibObject.i4FsDot11WlanTXOPRequestOptionImplemented);
        WssWlanDB.WssWlanIsPresentDB.bRSNAOptionImplemented = OSIX_TRUE;
        WssWlanDB.WssWlanAttributeDB.u1RSNAOptionImplemented =
            (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                     MibObject.i4FsDot11WlanRSNAOptionImplemented);
        WssWlanDB.WssWlanIsPresentDB.
            bRSNAPreauthenticationImplemented = OSIX_TRUE;
        WssWlanDB.WssWlanAttributeDB.
            u1RSNAPreauthenticationImplemented =
            (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                     MibObject.i4FsDot11WlanRSNAPreauthenticationImplemented);

    }
    else
    {
        WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex =
            (UINT4) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                     MibObject.i4IfIndex);
        if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanCFPollable != OSIX_FALSE)
        {
            WssWlanDB.WssWlanIsPresentDB.bCfPollable = OSIX_TRUE;
            WssWlanDB.WssWlanAttributeDB.u1CfPollable =
                (UINT1)
                (pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.
                 i4FsDot11WlanCFPollable);
        }
        if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanCFPollRequest != OSIX_FALSE)
        {
            WssWlanDB.WssWlanIsPresentDB.bCfPollRequest = OSIX_TRUE;
            WssWlanDB.WssWlanAttributeDB.u1CfPollRequest =
                (UINT1)
                (pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.
                 i4FsDot11WlanCFPollRequest);
        }
        if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanPrivacyOptionImplemented != OSIX_FALSE)
        {
            WssWlanDB.WssWlanIsPresentDB.bPrivacyOptionImplemented = OSIX_TRUE;
            WssWlanDB.WssWlanAttributeDB.u1PrivacyOptionImplemented =
                (UINT1)
                (pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.
                 i4FsDot11WlanPrivacyOptionImplemented);
        }
        if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanShortPreambleOptionImplemented != OSIX_FALSE)
        {
            WssWlanDB.WssWlanIsPresentDB.
                bShortPreambleOptionImplemented = OSIX_TRUE;
            WssWlanDB.WssWlanAttributeDB.u1ShortPreambleOptionImplemented =
                (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.
                         i4FsDot11WlanShortPreambleOptionImplemented);
        }
        if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanPBCCOptionImplemented != OSIX_FALSE)
        {
            WssWlanDB.WssWlanIsPresentDB.bPBCCOptionImplemented = OSIX_TRUE;
            WssWlanDB.WssWlanAttributeDB.u1PBCCOptionImplemented =
                (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.
                         i4FsDot11WlanPBCCOptionImplemented);
        }
        if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanChannelAgilityPresent != OSIX_FALSE)
        {
            WssWlanDB.WssWlanIsPresentDB.bChannelAgilityPresent = OSIX_TRUE;
            WssWlanDB.WssWlanAttributeDB.u1ChannelAgilityPresent =
                (UINT1)
                (pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.
                 i4FsDot11WlanChannelAgilityPresent);
        }
        if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanQosOptionImplemented != OSIX_FALSE)
        {
            WssWlanDB.WssWlanIsPresentDB.bQosOptionImplemented = OSIX_TRUE;
            WssWlanDB.WssWlanAttributeDB.u1QosOptionImplemented =
                (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.
                         i4FsDot11WlanQosOptionImplemented);
        }
        if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanSpectrumManagementRequired != OSIX_FALSE)
        {
            WssWlanDB.WssWlanIsPresentDB.bSpectrumManagementRequired =
                OSIX_TRUE;
            WssWlanDB.WssWlanAttributeDB.u1SpectrumManagementRequired =
                (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.
                         i4FsDot11WlanSpectrumManagementRequired);
        }
        if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanShortSlotTimeOptionImplemented != OSIX_FALSE)
        {
            WssWlanDB.WssWlanIsPresentDB.bShortSlotTimeOptionImplemented =
                OSIX_TRUE;
            WssWlanDB.WssWlanAttributeDB.u1ShortSlotTimeOptionImplemented =
                (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.
                         i4FsDot11WlanShortSlotTimeOptionImplemented);
        }
        if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanAPSDOptionImplemented != OSIX_FALSE)
        {
            WssWlanDB.WssWlanIsPresentDB.bAPSDOptionImplemented = OSIX_TRUE;
            WssWlanDB.WssWlanAttributeDB.u1APSDOptionImplemented =
                (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.
                         i4FsDot11WlanAPSDOptionImplemented);
        }
        if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanDSSSOFDMOptionEnabled != OSIX_FALSE)
        {
            WssWlanDB.WssWlanIsPresentDB.bDSSSOFDMOptionEnabled = OSIX_TRUE;
            WssWlanDB.WssWlanAttributeDB.u1DSSSOFDMOptionEnabled =
                (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.
                         i4FsDot11WlanDSSSOFDMOptionEnabled);
        }
        if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanDelayedBlockAckOptionImplemented != OSIX_FALSE)
        {
            WssWlanDB.WssWlanIsPresentDB.bDelayedBlockAckOptionImplemented =
                OSIX_TRUE;
            WssWlanDB.WssWlanAttributeDB.u1DelayedBlockAckOptionImplemented
                = (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.
                           i4FsDot11WlanDelayedBlockAckOptionImplemented);
        }
        if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanImmediateBlockAckOptionImplemented != OSIX_FALSE)
        {
            WssWlanDB.WssWlanIsPresentDB.bImmediateBlockAckOptionImplemented
                = OSIX_TRUE;
            WssWlanDB.WssWlanAttributeDB.u1ImmediateBlockAckOptionImplemented
                = (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.
                           i4FsDot11WlanImmediateBlockAckOptionImplemented);
        }
        if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanQAckOptionImplemented != OSIX_FALSE)
        {
            WssWlanDB.WssWlanIsPresentDB.bQAckOptionImplemented = OSIX_TRUE;
            WssWlanDB.WssWlanAttributeDB.u1QAckOptionImplemented =
                (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                         MibObject.i4FsDot11WlanQAckOptionImplemented);
        }
        if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanQueueRequestOptionImplemented != OSIX_FALSE)
        {
            WssWlanDB.WssWlanIsPresentDB.bQueueRequestOptionImplemented =
                OSIX_TRUE;
            WssWlanDB.WssWlanAttributeDB.u1QueueRequestOptionImplemented =
                (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                         MibObject.i4FsDot11WlanQueueRequestOptionImplemented);
        }
        if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanTXOPRequestOptionImplemented != OSIX_FALSE)
        {
            WssWlanDB.WssWlanIsPresentDB.bTXOPRequestOptionImplemented =
                OSIX_TRUE;
            WssWlanDB.WssWlanAttributeDB.u1TXOPRequestOptionImplemented =
                (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                         MibObject.i4FsDot11WlanTXOPRequestOptionImplemented);
        }
        if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanRSNAOptionImplemented != OSIX_FALSE)
        {
            WssWlanDB.WssWlanIsPresentDB.bRSNAOptionImplemented = OSIX_TRUE;
            WssWlanDB.WssWlanAttributeDB.u1RSNAOptionImplemented =
                (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                         MibObject.i4FsDot11WlanRSNAOptionImplemented);
        }
        if (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->
            bFsDot11WlanRSNAPreauthenticationImplemented != OSIX_FALSE)
        {
            WssWlanDB.WssWlanIsPresentDB.bRSNAPreauthenticationImplemented =
                OSIX_TRUE;
            WssWlanDB.WssWlanAttributeDB.u1RSNAPreauthenticationImplemented =
                (UINT1) (pWsscfgFsDot11WlanCapabilityProfileEntry->
                         MibObject.
                         i4FsDot11WlanRSNAPreauthenticationImplemented);
        }

    }
    if (WssWlanSetCapabilityProfile (&WssWlanDB) != OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pWsscfgFsDot11WlanCapabilityProfileEntry);
    UNUSED_PARAM (pWsscfgIsSetFsDot11WlanCapabilityProfileEntry);

#endif

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :   WsscfgSetDot11SpectrumManagementTable
 * Input       :  pWsscfgCapwapDot11CapabilityProfileEntry 
                  pWsscfgIsSetCapwapDot11CapabilityProfileEntry                 
 * Descritpion :  This Routine checks set value and invoke corresponding 
                  module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetDot11SpectrumManagementTable (tWsscfgDot11SpectrumManagementEntry
                                       * pWsscfgDot11SpectrumManagementEntry,
                                       tWsscfgIsSetDot11SpectrumManagementEntry
                                       *
                                       pWsscfgIsSetDot11SpectrumManagementEntry)
{
    tWssWlanDB          WssWlanDB;
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));

    if ((pWsscfgDot11SpectrumManagementEntry == NULL) ||
        (pWsscfgIsSetDot11SpectrumManagementEntry == NULL))
    {
        return OSIX_FAILURE;
    }
#ifdef WLC_WANTED
    WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex =
        (UINT4) (pWsscfgDot11SpectrumManagementEntry->MibObject.i4IfIndex);
    if (pWsscfgIsSetDot11SpectrumManagementEntry->bDot11MitigationRequirement !=
        OSIX_FALSE)
    {
        WssWlanDB.WssWlanIsPresentDB.bMitigationRequirement = OSIX_TRUE;
        WssWlanDB.WssWlanAttributeDB.u1MitigationRequirement =
            (UINT1) (pWsscfgDot11SpectrumManagementEntry->
                     MibObject.i4Dot11MitigationRequirement);

    }

    if (WssWlanSetDot11SpectrumManagementTable (&WssWlanDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#endif
    return OSIX_SUCCESS;

}

/****************************************************************************
 * Function    :  WssCfgSetFsDot11WlanQosProfile
 * Input       :  pWsscfgCapwapDot11CapabilityProfileEntry 
                  pWsscfgIsSetCapwapDot11CapabilityProfileEntry                 
 * Descritpion :  This Routine checks set value and invoke corresponding 
                  module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetFsDot11WlanQosProfile (tWsscfgFsDot11WlanQosProfileEntry *
                                pWsscfgFsDot11WlanQosProfileEntry,
                                tWsscfgIsSetFsDot11WlanQosProfileEntry *
                                pWsscfgIsSetFsDot11WlanQosProfileEntry)
{
    tWssWlanDB          WssWlanDB;
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));

    if ((pWsscfgFsDot11WlanQosProfileEntry == NULL) ||
        (pWsscfgIsSetFsDot11WlanQosProfileEntry == NULL))
    {
        return OSIX_FAILURE;
    }
#ifdef WLC_WANTED
    if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
        bFsDot11WlanQosRowStatus != OSIX_FALSE)
    {
        if (pWsscfgFsDot11WlanQosProfileEntry->
            MibObject.i4FsDot11WlanQosRowStatus == DESTROY)
        {
            return OSIX_SUCCESS;
        }
        WssWlanDB.WssWlanAttributeDB.u1QosTraffic =
            (UINT1) ((pWsscfgFsDot11WlanQosProfileEntry->
                      MibObject.i4FsDot11WlanQosTraffic) - 1);

        WssWlanDB.WssWlanIsPresentDB.bQosTraffic = OSIX_TRUE;

        WssWlanDB.WssWlanAttributeDB.u1PassengerTrustMode =
            (UINT1) (pWsscfgFsDot11WlanQosProfileEntry->
                     MibObject.i4FsDot11WlanQosPassengerTrustMode);

        WssWlanDB.WssWlanIsPresentDB.bPassengerTrustMode = OSIX_TRUE;

        WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex =
            (UINT4) (pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4IfIndex);

        WssWlanDB.WssWlanIsPresentDB.bQosRateLimit = OSIX_TRUE;

        WssWlanDB.WssWlanAttributeDB.u1QosRateLimit =
            (UINT1) (pWsscfgFsDot11WlanQosProfileEntry->
                     MibObject.i4FsDot11WlanQosRateLimit);

        WssWlanDB.WssWlanIsPresentDB.bQosRateLimit = OSIX_TRUE;

        WssWlanDB.WssWlanAttributeDB.u4QosUpStreamCIR =
            (UINT4) (pWsscfgFsDot11WlanQosProfileEntry->
                     MibObject.i4FsDot11WlanUpStreamCIR);

        WssWlanDB.WssWlanIsPresentDB.bQosUpStreamCIR = OSIX_TRUE;

        WssWlanDB.WssWlanAttributeDB.u4QosUpStreamCBS =
            (UINT4) (pWsscfgFsDot11WlanQosProfileEntry->
                     MibObject.i4FsDot11WlanUpStreamCBS);

        WssWlanDB.WssWlanIsPresentDB.bQosUpStreamCBS = OSIX_TRUE;

        WssWlanDB.WssWlanAttributeDB.u4QosUpStreamEIR =
            (UINT4) (pWsscfgFsDot11WlanQosProfileEntry->
                     MibObject.i4FsDot11WlanUpStreamEIR);

        WssWlanDB.WssWlanIsPresentDB.bQosUpStreamEIR = OSIX_TRUE;

        WssWlanDB.WssWlanAttributeDB.u4QosUpStreamEBS =
            (UINT4) (pWsscfgFsDot11WlanQosProfileEntry->
                     MibObject.i4FsDot11WlanUpStreamEBS);

        WssWlanDB.WssWlanIsPresentDB.bQosUpStreamEBS = OSIX_TRUE;

        WssWlanDB.WssWlanAttributeDB.u4QosDownStreamCIR =
            (UINT4) (pWsscfgFsDot11WlanQosProfileEntry->
                     MibObject.i4FsDot11WlanDownStreamCIR);

        WssWlanDB.WssWlanIsPresentDB.bQosDownStreamCIR = OSIX_TRUE;

        WssWlanDB.WssWlanAttributeDB.u4QosDownStreamCBS =
            (UINT4) (pWsscfgFsDot11WlanQosProfileEntry->
                     MibObject.i4FsDot11WlanDownStreamCBS);

        WssWlanDB.WssWlanIsPresentDB.bQosDownStreamCBS = OSIX_TRUE;

        WssWlanDB.WssWlanAttributeDB.u4QosDownStreamEIR =
            (UINT4) (pWsscfgFsDot11WlanQosProfileEntry->
                     MibObject.i4FsDot11WlanDownStreamEIR);

        WssWlanDB.WssWlanIsPresentDB.bQosDownStreamEIR = OSIX_TRUE;

        WssWlanDB.WssWlanAttributeDB.u4QosDownStreamEBS =
            (UINT4) (pWsscfgFsDot11WlanQosProfileEntry->
                     MibObject.i4FsDot11WlanDownStreamEBS);

        WssWlanDB.WssWlanIsPresentDB.bQosDownStreamEBS = OSIX_TRUE;

    }
    else
    {

        if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanQosTraffic !=
            OSIX_FALSE)
        {
            WssWlanDB.WssWlanAttributeDB.u1QosTraffic =
                (UINT1) ((pWsscfgFsDot11WlanQosProfileEntry->
                          MibObject.i4FsDot11WlanQosTraffic) - 1);
            WssWlanDB.WssWlanIsPresentDB.bQosTraffic = OSIX_TRUE;

        }
        if (pWsscfgIsSetFsDot11WlanQosProfileEntry->
            bFsDot11WlanQosPassengerTrustMode != OSIX_FALSE)
        {
            WssWlanDB.WssWlanAttributeDB.u1PassengerTrustMode =
                (UINT1) (pWsscfgFsDot11WlanQosProfileEntry->
                         MibObject.i4FsDot11WlanQosPassengerTrustMode);
            WssWlanDB.WssWlanIsPresentDB.bPassengerTrustMode = OSIX_TRUE;
        }
        if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanQosRateLimit !=
            OSIX_FALSE)
        {
            WssWlanDB.WssWlanAttributeDB.u1QosRateLimit =
                (UINT1) (pWsscfgFsDot11WlanQosProfileEntry->
                         MibObject.i4FsDot11WlanQosRateLimit);
            WssWlanDB.WssWlanIsPresentDB.bQosRateLimit = OSIX_TRUE;
        }
        if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanUpStreamCIR !=
            OSIX_FALSE)
        {
            WssWlanDB.WssWlanAttributeDB.u4QosUpStreamCIR =
                (UINT4) (pWsscfgFsDot11WlanQosProfileEntry->
                         MibObject.i4FsDot11WlanUpStreamCIR);
            WssWlanDB.WssWlanIsPresentDB.bQosUpStreamCIR = OSIX_TRUE;
        }
        if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanUpStreamCBS !=
            OSIX_FALSE)
        {
            WssWlanDB.WssWlanAttributeDB.u4QosUpStreamCBS =
                (UINT4) (pWsscfgFsDot11WlanQosProfileEntry->
                         MibObject.i4FsDot11WlanUpStreamCBS);
            WssWlanDB.WssWlanIsPresentDB.bQosUpStreamCBS = OSIX_TRUE;

        }
        if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanUpStreamEIR !=
            OSIX_FALSE)
        {
            WssWlanDB.WssWlanAttributeDB.u4QosUpStreamEIR =
                (UINT4) (pWsscfgFsDot11WlanQosProfileEntry->
                         MibObject.i4FsDot11WlanUpStreamEIR);
            WssWlanDB.WssWlanIsPresentDB.bQosUpStreamEIR = OSIX_TRUE;
        }
        if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanUpStreamEBS !=
            OSIX_FALSE)
        {
            WssWlanDB.WssWlanAttributeDB.u4QosUpStreamEBS =
                (UINT4) (pWsscfgFsDot11WlanQosProfileEntry->
                         MibObject.i4FsDot11WlanUpStreamEBS);
            WssWlanDB.WssWlanIsPresentDB.bQosUpStreamEBS = OSIX_TRUE;
        }
        if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamCIR !=
            OSIX_FALSE)
        {
            WssWlanDB.WssWlanAttributeDB.u4QosDownStreamCIR =
                (UINT4) (pWsscfgFsDot11WlanQosProfileEntry->
                         MibObject.i4FsDot11WlanDownStreamCIR);
            WssWlanDB.WssWlanIsPresentDB.bQosDownStreamCIR = OSIX_TRUE;
        }
        if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamCBS !=
            OSIX_FALSE)
        {
            WssWlanDB.WssWlanAttributeDB.u4QosDownStreamCBS =
                (UINT4) (pWsscfgFsDot11WlanQosProfileEntry->
                         MibObject.i4FsDot11WlanDownStreamCBS);
            WssWlanDB.WssWlanIsPresentDB.bQosDownStreamCBS = OSIX_TRUE;
        }
        if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamEIR !=
            OSIX_FALSE)
        {
            WssWlanDB.WssWlanAttributeDB.u4QosDownStreamEIR =
                (UINT4) (pWsscfgFsDot11WlanQosProfileEntry->
                         MibObject.i4FsDot11WlanDownStreamEIR);
            WssWlanDB.WssWlanIsPresentDB.bQosDownStreamEIR = OSIX_TRUE;
        }
        if (pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamEBS !=
            OSIX_FALSE)
        {
            WssWlanDB.WssWlanAttributeDB.u4QosDownStreamEBS =
                (UINT4) (pWsscfgFsDot11WlanQosProfileEntry->
                         MibObject.i4FsDot11WlanDownStreamEBS);
            WssWlanDB.WssWlanIsPresentDB.bQosDownStreamEBS = OSIX_TRUE;
        }
        WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex =
            (UINT4) (pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4IfIndex);

    }
    if (WssWlanSetQosProfile (&WssWlanDB) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pWsscfgFsDot11WlanQosProfileEntry);
    UNUSED_PARAM (pWsscfgIsSetFsDot11WlanQosProfileEntry);
#endif

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetFsDot11WlanAuthenticationProfile
 * Input       :  pWsscfgCapwapDot11WlanAuthenticationProfileEntry 
                  pWsscfgIsSetCapwapDot11CapabilityProfileEntry                 
 * Descritpion :  This Routine checks set value and invoke corresponding 
                  module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgSetFsDot11WlanAuthenticationProfile
    (tWsscfgFsDot11WlanAuthenticationProfileEntry *
     pWsscfgFsDot11WlanAuthenticationProfileEntry,
     tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry *
     pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry)
{
    tWssWlanDB          WssWlanDB;
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    if ((pWsscfgFsDot11WlanAuthenticationProfileEntry == NULL) ||
        (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry == NULL))
    {
        return OSIX_FAILURE;
    }

#ifdef WLC_WANTED

    if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
        bFsDot11WlanAuthenticationRowStatus != OSIX_FALSE)
    {
        if (pWsscfgFsDot11WlanAuthenticationProfileEntry->
            MibObject.i4FsDot11WlanAuthenticationRowStatus == DESTROY)
        {
            return OSIX_SUCCESS;
        }
        WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex =
            (UINT4) (pWsscfgFsDot11WlanAuthenticationProfileEntry->MibObject.
                     i4IfIndex);

        MEMCPY (WssWlanDB.WssWlanAttributeDB.au1WepKey,
                pWsscfgFsDot11WlanAuthenticationProfileEntry->
                MibObject.au1FsDot11WlanWepKey,
                pWsscfgFsDot11WlanAuthenticationProfileEntry->
                MibObject.i4FsDot11WlanWepKeyLen);
        WssWlanDB.WssWlanIsPresentDB.bWepKey = OSIX_TRUE;

        WssWlanDB.WssWlanAttributeDB.u1AuthMethod =
            (UINT1) (pWsscfgFsDot11WlanAuthenticationProfileEntry->
                     MibObject.i4FsDot11WlanAuthenticationAlgorithm);
        WssWlanDB.WssWlanIsPresentDB.bAuthMethod = OSIX_TRUE;

        WssWlanDB.WssWlanAttributeDB.u1KeyIndex =
            (UINT1) (pWsscfgFsDot11WlanAuthenticationProfileEntry->
                     MibObject.i4FsDot11WlanWepKeyIndex);
        WssWlanDB.WssWlanIsPresentDB.bKeyIndex = OSIX_TRUE;

        WssWlanDB.WssWlanAttributeDB.u1KeyType =
            (UINT1) (pWsscfgFsDot11WlanAuthenticationProfileEntry->
                     MibObject.i4FsDot11WlanWepKeyType);
        WssWlanDB.WssWlanIsPresentDB.bKeyType = OSIX_TRUE;

        WssWlanDB.WssWlanAttributeDB.u1WebAuthStatus =
            (UINT1) (pWsscfgFsDot11WlanAuthenticationProfileEntry->
                     MibObject.i4FsDot11WlanWebAuthentication);
        WssWlanDB.WssWlanIsPresentDB.bWebAuthStatus = OSIX_TRUE;

    }
    else
    {
        WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex =
            (UINT4) (pWsscfgFsDot11WlanAuthenticationProfileEntry->MibObject.
                     i4IfIndex);

        if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
            bFsDot11WlanWepKey != OSIX_FALSE)
        {
            MEMCPY (WssWlanDB.WssWlanAttributeDB.au1WepKey,
                    pWsscfgFsDot11WlanAuthenticationProfileEntry->
                    MibObject.au1FsDot11WlanWepKey,
                    pWsscfgFsDot11WlanAuthenticationProfileEntry->
                    MibObject.i4FsDot11WlanWepKeyLen);
            WssWlanDB.WssWlanIsPresentDB.bWepKey = OSIX_TRUE;
        }
        if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
            bFsDot11WlanAuthenticationAlgorithm != OSIX_FALSE)
        {
            WssWlanDB.WssWlanAttributeDB.u1AuthMethod =
                (UINT1) (pWsscfgFsDot11WlanAuthenticationProfileEntry->
                         MibObject.i4FsDot11WlanAuthenticationAlgorithm);
            WssWlanDB.WssWlanIsPresentDB.bAuthMethod = OSIX_TRUE;
        }
        if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
            bFsDot11WlanWepKeyIndex != OSIX_FALSE)
        {
            WssWlanDB.WssWlanAttributeDB.u1KeyIndex =
                (UINT1) (pWsscfgFsDot11WlanAuthenticationProfileEntry->
                         MibObject.i4FsDot11WlanWepKeyIndex);
            WssWlanDB.WssWlanIsPresentDB.bKeyIndex = OSIX_TRUE;
        }
        if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
            bFsDot11WlanWepKeyType != OSIX_FALSE)
        {
            WssWlanDB.WssWlanAttributeDB.u1KeyType =
                (UINT1) (pWsscfgFsDot11WlanAuthenticationProfileEntry->
                         MibObject.i4FsDot11WlanWepKeyType);
            WssWlanDB.WssWlanIsPresentDB.bKeyType = OSIX_TRUE;
        }
        if (pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->
            bFsDot11WlanWebAuthentication != OSIX_FALSE)
        {

            WssWlanDB.WssWlanAttributeDB.u1WebAuthStatus =
                (UINT1) (pWsscfgFsDot11WlanAuthenticationProfileEntry->
                         MibObject.i4FsDot11WlanWebAuthentication);
            WssWlanDB.WssWlanIsPresentDB.bWebAuthStatus = OSIX_TRUE;
        }
    }

    if (WssWlanSetAuthenticationProfile (&WssWlanDB) != OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetCapabilityProfile
 * Input       :  pWsscfgCapwapDot11CapabilityProfileEntry 
                  pWsscfgIsSetCapwapDot11CapabilityProfileEntry                
 * Descritpion :  This Routine checks set value and invoke corresponding 
                  module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetCapabilityProfile (tWsscfgFsDot11CapabilityProfileEntry *
                            pWsscfgFsDot11CapabilityProfileEntry,
                            tWsscfgIsSetFsDot11CapabilityProfileEntry *
                            pWsscfgIsSetFsDot11CapabilityProfileEntry)
{
    UNUSED_PARAM (pWsscfgFsDot11CapabilityProfileEntry);
    UNUSED_PARAM (pWsscfgIsSetFsDot11CapabilityProfileEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetQosProfile
 * Input       :  pWsscfgCapwapDot11CapabilityProfileEntry 
                  pWsscfgIsSetCapwapDot11CapabilityProfileEntry                
 * Descritpion :  This Routine checks set value and invoke corresponding 
                  module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetQosProfile (tWsscfgFsDot11WlanQosProfileEntry *
                     pWsscfgFsDot11WlanQosProfileEntry,
                     tWsscfgIsSetFsDot11WlanQosProfileEntry *
                     pWsscfgIsSetFsDot11WlanQosProfileEntry)
{
    UNUSED_PARAM (pWsscfgFsDot11WlanQosProfileEntry);
    UNUSED_PARAM (pWsscfgIsSetFsDot11WlanQosProfileEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetWlanBinding
 * Input       :  pWsscfgCapwapDot11WlanBindEntry 
                  pWsscfgIsSetCapwapDot11WlanBindEntry                    
 * Descritpion :  This Routine checks set value and invoke corresponding 
                  module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetWlanBinding (tWsscfgCapwapDot11WlanBindEntry *
                      pWsscfgCapwapDot11WlanBindEntry,
                      tWsscfgIsSetCapwapDot11WlanBindEntry *
                      pWsscfgIsSetCapwapDot11WlanBindEntry)
{
#ifdef WLC_WANTED
    tWssWlanDB          WssWlanMsgDB;
    tWssWlanDB          WssWlanMsgDBGetBssIfIndex;
    tWssPMInfo          WssPmInfo;
    tWssifauthDBMsgStruct *pWssBssIdStnStats = NULL;
#ifdef RFMGMT_WANTED
    tRfMgmtMsgStruct    RfMgmtMsgStruct;
#endif

#ifdef RFMGMT_WANTED
    MEMSET (&RfMgmtMsgStruct, 0, sizeof (tRfMgmtMsgStruct));
#endif
    pWssBssIdStnStats =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pWssBssIdStnStats == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WssCfgSetWlanBinding:- "
                     "UtlShMemAllocWssIfAuthDbBuf returned failure\n"));
        return OSIX_FAILURE;
    }

    MEMSET (&WssWlanMsgDB, 0, sizeof (tWssWlanDB));
    MEMSET (&WssWlanMsgDBGetBssIfIndex, 0, sizeof (tWssWlanDB));

    if ((pWsscfgCapwapDot11WlanBindEntry == NULL) ||
        (pWsscfgIsSetCapwapDot11WlanBindEntry == NULL))
    {
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssBssIdStnStats);
        return OSIX_FAILURE;
    }

    CONFIG_LOCK ();

    if (pWsscfgIsSetCapwapDot11WlanBindEntry->bCapwapDot11WlanBindRowStatus !=
        OSIX_FALSE)
    {
        WssWlanMsgDB.WssWlanIsPresentDB.bRowStatus = OSIX_TRUE;

        if (pWsscfgCapwapDot11WlanBindEntry->
            MibObject.i4CapwapDot11WlanBindRowStatus == NOT_READY)
        {
            WssWlanMsgDB.WssWlanAttributeDB.u1RowStatus = CREATE_AND_WAIT;
        }
        /** added for DESTROY command **/
        else if (pWsscfgCapwapDot11WlanBindEntry->
                 MibObject.i4CapwapDot11WlanBindRowStatus == DESTROY)
        {
            /*  WssWlanMsgDBGetBssIfIndex.WssWlanIsPresentDB.bRowStatus = OSIX_TRUE; */

            WssWlanMsgDBGetBssIfIndex.WssWlanIsPresentDB.bWlanProfileId =
                OSIX_TRUE;
            WssWlanMsgDBGetBssIfIndex.WssWlanAttributeDB.u2WlanProfileId =
                (UINT2) (pWsscfgCapwapDot11WlanBindEntry->MibObject.
                         u4CapwapDot11WlanProfileId);

            WssWlanMsgDBGetBssIfIndex.WssWlanIsPresentDB.bRadioIfIndex =
                OSIX_TRUE;
            WssWlanMsgDBGetBssIfIndex.WssWlanAttributeDB.u4RadioIfIndex =
                (UINT4) (pWsscfgCapwapDot11WlanBindEntry->MibObject.i4IfIndex);

            WssWlanMsgDBGetBssIfIndex.WssWlanIsPresentDB.bRowStatus = OSIX_TRUE;

            if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY,
                                          &WssWlanMsgDBGetBssIfIndex) !=
                OSIX_SUCCESS)
            {
                WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                             "WssCfgSetWlanBinding : WssWLAN DB access\
                    WSS_WLAN_GET_BSS_ENTRY failed. \r\n");
            }
            if (WssWlanMsgDBGetBssIfIndex.WssWlanAttributeDB.
                u1RowStatus == ACTIVE)
            {

                WssWlanMsgDBGetBssIfIndex.WssWlanIsPresentDB.bBssIfIndex =
                    OSIX_TRUE;

                if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_BSS_ENTRY,
                                              &WssWlanMsgDBGetBssIfIndex) !=
                    OSIX_SUCCESS)
                {
                    WSSWLAN_TRC (WSSWLAN_FAILURE_TRC,
                                 "WssCfgSetWlanBinding : WssWLAN DB access\
                        failed to get BssIfIndex. \r\n");
                }
            }
            WssWlanMsgDB.WssWlanAttributeDB.u1RowStatus =
                (UINT1) (pWsscfgCapwapDot11WlanBindEntry->
                         MibObject.i4CapwapDot11WlanBindRowStatus);
        }
        else
        {
            WssWlanMsgDB.WssWlanAttributeDB.u1RowStatus =
                (UINT1) (pWsscfgCapwapDot11WlanBindEntry->
                         MibObject.i4CapwapDot11WlanBindRowStatus);
        }

        WssWlanMsgDB.WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
        WssWlanMsgDB.WssWlanAttributeDB.u2WlanProfileId =
            (UINT2) (pWsscfgCapwapDot11WlanBindEntry->
                     MibObject.u4CapwapDot11WlanProfileId);

        WssWlanMsgDB.WssWlanIsPresentDB.bRadioIfIndex = OSIX_TRUE;
        WssWlanMsgDB.WssWlanAttributeDB.u4RadioIfIndex =
            (UINT4) (pWsscfgCapwapDot11WlanBindEntry->MibObject.i4IfIndex);
    }
    if (WssWlanSetWlanBinding (&WssWlanMsgDB) != OSIX_SUCCESS)
    {
        UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssBssIdStnStats);
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }

    if (WssWlanMsgDB.WssWlanIsPresentDB.bBssIfIndex != OSIX_FALSE)
    {
        pWsscfgCapwapDot11WlanBindEntry->
            MibObject.u4CapwapDot11WlanBindWlanId =
            WssWlanMsgDB.WssWlanAttributeDB.u1WlanId;

        pWsscfgCapwapDot11WlanBindEntry->
            MibObject.i4CapwapDot11WlanBindBssIfIndex =
            (INT4) (WssWlanMsgDB.WssWlanAttributeDB.u4BssIfIndex);
    }

    /* u4BssIfIndex is 0 when WLAN is deleted.
     * PM code to be added for WLAN Delete case
     * and WLAN Enable for the second time or more*/

    if (pWsscfgCapwapDot11WlanBindEntry->MibObject.
        i4CapwapDot11WlanBindRowStatus == ACTIVE)
    {
        if ((WssWlanMsgDB.WssWlanAttributeDB.u4BssIfIndex != 0))

        {
            MEMSET (&WssPmInfo, 0, sizeof (tWssPMInfo));

            WssPmInfo.u2ProfileBSSID =
                WssWlanMsgDB.WssWlanAttributeDB.u4BssIfIndex;

            if (pmWTPInfoNotify (BSSID_ADD, &WssPmInfo) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "Create BSSID : Update PM Stats DB Failed \r\n");
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssBssIdStnStats);
                CONFIG_UNLOCK ();
                return OSIX_FAILURE;
            }

            MEMSET (pWssBssIdStnStats, 0, sizeof (tWssifauthDBMsgStruct));
            pWssBssIdStnStats->WssBssStnStatsDB.u4BssIfIndex =
                WssWlanMsgDB.WssWlanAttributeDB.u4BssIfIndex;
            if ((WssIfProcessWssAuthDBMsg (WSS_AUTH_ADD_BSSSID_STATION_STATS,
                                           pWssBssIdStnStats)) != OSIX_SUCCESS)
            {
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssBssIdStnStats);
                CONFIG_UNLOCK ();
                return OSIX_FAILURE;
            }
        }
    }
    else if ((pWsscfgCapwapDot11WlanBindEntry->MibObject.
              i4CapwapDot11WlanBindRowStatus == DESTROY) ||
             (pWsscfgCapwapDot11WlanBindEntry->MibObject.
              i4CapwapDot11WlanBindRowStatus == NOT_IN_SERVICE))
    {
        if (WssWlanMsgDBGetBssIfIndex.WssWlanAttributeDB.u4BssIfIndex != 0)
        {
            MEMSET (&WssPmInfo, 0, sizeof (tWssPMInfo));

            WssPmInfo.u2ProfileBSSID =
                WssWlanMsgDBGetBssIfIndex.WssWlanAttributeDB.u4BssIfIndex;

            if (pmWTPInfoNotify (BSSID_DEL, &WssPmInfo) != OSIX_SUCCESS)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "Create BSSID : Update PM Stats DB Failed \r\n");
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssBssIdStnStats);
                CONFIG_UNLOCK ();
                return OSIX_FAILURE;
            }

            MEMSET (pWssBssIdStnStats, 0, sizeof (tWssifauthDBMsgStruct));
            pWssBssIdStnStats->WssBssStnStatsDB.u4BssIfIndex =
                WssWlanMsgDBGetBssIfIndex.WssWlanAttributeDB.u4BssIfIndex;
            if ((WssIfProcessWssAuthDBMsg (WSS_AUTH_DEL_BSSSID_STATION_STATS,
                                           pWssBssIdStnStats)) != OSIX_SUCCESS)
            {
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssBssIdStnStats);
                CONFIG_UNLOCK ();
                return OSIX_FAILURE;
            }
#ifdef RFMGMT_WANTED
            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u4RadioIfIndex =
                pWsscfgCapwapDot11WlanBindEntry->MibObject.i4IfIndex;
            RfMgmtMsgStruct.unRfMgmtMsg.RfMgmtIntfConfigReq.u1WlanId =
                pWsscfgCapwapDot11WlanBindEntry->MibObject.
                u4CapwapDot11WlanBindWlanId;

            if (WssIfProcessRfMgmtMsg (RFMGMT_WLAN_DELETION_MSG,
                                       &RfMgmtMsgStruct) != OSIX_TRUE)
            {
                RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                             "Update to RF module failed \r\n");
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssBssIdStnStats);
                CONFIG_UNLOCK ();
                return OSIX_FAILURE;
            }

#endif
        }
    }
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssBssIdStnStats);
    CONFIG_UNLOCK ();

#else
    UNUSED_PARAM (pWsscfgCapwapDot11WlanBindEntry);
    UNUSED_PARAM (pWsscfgIsSetCapwapDot11WlanBindEntry);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetFsDot11StationConfigTable
 * Input       :   pWsscfgFsDot11StationConfigEntry
                   pWsscfgIsSetFsDot11StationConfigEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetFsDot11StationConfigTable (tWsscfgFsDot11StationConfigEntry *
                                    pWsscfgFsDot11StationConfigEntry,
                                    tWsscfgIsSetFsDot11StationConfigEntry *
                                    pWsscfgIsSetFsDot11StationConfigEntry)
{
#ifdef WLC_WANTED
    tWssWlanDB          WssWlanDB;
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));

    if ((pWsscfgFsDot11StationConfigEntry == NULL) ||
        (pWsscfgIsSetFsDot11StationConfigEntry == NULL))
    {
        return OSIX_FAILURE;
    }

    CONFIG_LOCK ();
    if (pWsscfgIsSetFsDot11StationConfigEntry->bFsDot11SupressSSID !=
        OSIX_FALSE)
    {
        WssWlanDB.WssWlanAttributeDB.u1SupressSsid =
            (UINT1) (pWsscfgFsDot11StationConfigEntry->
                     MibObject.i4FsDot11SupressSSID);
        WssWlanDB.WssWlanIsPresentDB.bSupressSsid = OSIX_TRUE;
    }
    if (pWsscfgIsSetFsDot11StationConfigEntry->bFsDot11VlanId != OSIX_FALSE)
    {
        WssWlanDB.WssWlanAttributeDB.u2VlanId =
            (UINT2) (pWsscfgFsDot11StationConfigEntry->
                     MibObject.i4FsDot11VlanId);
        WssWlanDB.WssWlanIsPresentDB.bVlanId = OSIX_TRUE;
    }

    if (pWsscfgIsSetFsDot11StationConfigEntry->bFsDot11BandwidthThresh !=
        OSIX_FALSE)
    {
        WssWlanDB.WssWlanAttributeDB.u4BandwidthThresh =
            (UINT4) (pWsscfgFsDot11StationConfigEntry->
                     MibObject.i4FsDot11BandwidthThresh);
        WssWlanDB.WssWlanIsPresentDB.bBandwidthThresh = OSIX_TRUE;
    }

    WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex
        = (UINT4) (pWsscfgFsDot11StationConfigEntry->MibObject.i4IfIndex);
    WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

    if (WssWlanSetFsDot11StationConfigTable (&WssWlanDB) != OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }
    CONFIG_UNLOCK ();
#else
    UNUSED_PARAM (pWsscfgFsDot11StationConfigEntry);
    UNUSED_PARAM (pWsscfgIsSetFsDot11StationConfigEntry);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :   WssCfgSetFsDot11QAPTable
 * Input       :   pWsscfgFsDot11QAPEntry
                   pWsscfgIsSetFsDot11QAPEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetFsDot11QAPTable (tWsscfgFsDot11QAPEntry * pWsscfgFsDot11QAPEntry,
                          tWsscfgIsSetFsDot11QAPEntry *
                          pWsscfgIsSetFsDot11QAPEntry)
{
    UNUSED_PARAM (pWsscfgFsDot11QAPEntry);
    UNUSED_PARAM (pWsscfgIsSetFsDot11QAPEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WsscfgSetFsDot11AntennasListTable
 * Input       :   pWsscfgFsDot11AntennasListEntry
                   pWsscfgIsSetFsDot11AntennasListEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetFsDot11AntennasListTable (tWsscfgFsDot11AntennasListEntry *
                                   pWsscfgFsDot11AntennasListEntry,
                                   tWsscfgIsSetFsDot11AntennasListEntry *
                                   pWsscfgIsSetFsDot11AntennasListEntry)
{
    UNUSED_PARAM (pWsscfgIsSetFsDot11AntennasListEntry);
    tRadioIfGetDB       RadioIfGetDB;
    tRadioIfGetDB       RadioIfDB;

    MEMSET (&RadioIfDB, 0, sizeof (tRadioIfGetDB));
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));
#ifdef WLC_WANTED
    CONFIG_LOCK ();

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex
        = (UINT4) pWsscfgFsDot11AntennasListEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;

    RadioIfDB.RadioIfGetAllDB.u4RadioIfIndex
        = (UINT4) pWsscfgFsDot11AntennasListEntry->MibObject.i4IfIndex;
    RadioIfDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;
    if (pWsscfgIsSetFsDot11AntennasListEntry->
        bDot11AntennaListIndex != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u1CurrentTxAntenna =
            (UINT1) pWsscfgFsDot11AntennasListEntry->
            MibObject.i4Dot11AntennaListIndex;
        RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;
    }
    if (pWsscfgIsSetFsDot11AntennasListEntry->bFsAntennaMode != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u1AntennaMode =
            (UINT1) pWsscfgFsDot11AntennasListEntry->MibObject.i4FsAntennaMode;
        RadioIfGetDB.RadioIfIsGetAllDB.bAntennaMode = OSIX_TRUE;
    }
    RadioIfDB.RadioIfIsGetAllDB.bCurrentTxAntenna = OSIX_TRUE;
    RadioIfDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }
    /* Get the Radio IF DB info to fill the structure  */
    if (WssIfProcessRadioIfDBMsg (WSS_GET_RADIO_IF_DB, &RadioIfDB) !=
        OSIX_SUCCESS)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetAntennasList : RadioIf DB access failed. \r\n");
        UtlShMemFreeAntennaSelectionBuf (RadioIfDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }
    if (pWsscfgFsDot11AntennasListEntry->MibObject.i4Dot11AntennaListIndex >
        RadioIfDB.RadioIfGetAllDB.u1CurrentTxAntenna)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                     "RadioIfSetAntennasList : Invalid selection count\n");
        CONFIG_UNLOCK ();
        UtlShMemFreeAntennaSelectionBuf (RadioIfDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }
    if (pWsscfgIsSetFsDot11AntennasListEntry->bFsAntennaSelection != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection
            [pWsscfgFsDot11AntennasListEntry->
             MibObject.i4Dot11AntennaListIndex - 1]
            = (UINT1) pWsscfgFsDot11AntennasListEntry->
            MibObject.i4FsAntennaSelection;
        RadioIfGetDB.RadioIfIsGetAllDB.bAntennaSelection
            [pWsscfgFsDot11AntennasListEntry->
             MibObject.i4Dot11AntennaListIndex - 1] = OSIX_TRUE;
    }
    if (RadioIfSetAntennasList (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        UtlShMemFreeAntennaSelectionBuf (RadioIfDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        return OSIX_FAILURE;
    }
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    UtlShMemFreeAntennaSelectionBuf (RadioIfDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    CONFIG_UNLOCK ();
#else
    UNUSED_PARAM (pWsscfgFsDot11AntennasListEntry);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetVlanIsolation
 * Input       :   pWsscfgFsVlanIsolationEntry
                   pWsscfgIsSetFsVlanIsolationEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetVlanIsolation (tWsscfgFsVlanIsolationEntry *
                        pWsscfgFsVlanIsolationEntry,
                        tWsscfgIsSetFsVlanIsolationEntry *
                        pWsscfgIsSetFsVlanIsolationEntry)
{
    tWssWlanDB          WssWlanDB;
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));

#ifdef WLC_WANTED
    CONFIG_LOCK ();
    WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex =
        (UINT4) pWsscfgFsVlanIsolationEntry->MibObject.i4IfIndex;
    WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

    if (pWsscfgIsSetFsVlanIsolationEntry->bFsVlanIsolation != OSIX_FALSE)
    {
        WssWlanDB.WssWlanAttributeDB.u1SsidIsolation =
            (UINT1) pWsscfgFsVlanIsolationEntry->MibObject.i4FsVlanIsolation;
        WssWlanDB.WssWlanIsPresentDB.bSsidIsolation = OSIX_TRUE;
    }

    if (WssWlanSetVlanIsolation (&WssWlanDB) != OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }
    CONFIG_UNLOCK ();
#else
    UNUSED_PARAM (pWsscfgFsVlanIsolationEntry);
    UNUSED_PARAM (pWsscfgIsSetFsVlanIsolationEntry);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgNetworkStatusChange
 * Input       :   i4FsDot11bNetworkEnable
                   u4RadioType
 * Descritpion :  This Routine will invoke the radio if module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgNetworkStatusChange (INT4 i4FsDot11bNetworkEnable, UINT4 u4RadioType)
{
    UINT2               u2WtpProfileId = 0;
    UINT4               u4RadioId = 0;
    UINT4               u4NoOfRadio = 0;
    UINT1               au1ModelNumber[CLI_MODEL_NAME_LEN];
    INT4                i4RadioIfIndex = 0;
    UINT4               u4ErrorCode;
    UINT4               u4GetRadioType = 0;
    tSNMP_OCTET_STRING_TYPE ModelName;

    MEMSET (au1ModelNumber, 0, CLI_MODEL_NAME_LEN);
    MEMSET (&ModelName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    ModelName.pu1_OctetList = au1ModelNumber;

    CONFIG_LOCK ();

    for (u2WtpProfileId = 1; u2WtpProfileId <= CLI_MAX_AP; u2WtpProfileId++)
    {
        if (nmhGetCapwapBaseWtpProfileWtpModelNumber (u2WtpProfileId,
                                                      &ModelName) ==
            SNMP_SUCCESS)
        {
            if (ModelName.pu1_OctetList != NULL)
            {
                if (nmhGetFsNoOfRadio (&ModelName, &u4NoOfRadio) !=
                    SNMP_SUCCESS)
                {
                    continue;
                }
                for (u4RadioId = 1; u4RadioId <= u4NoOfRadio; u4RadioId++)
                {
                    if (nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
                        (u2WtpProfileId, u4RadioId,
                         &i4RadioIfIndex) != SNMP_SUCCESS)
                    {
                        continue;
                    }
                    if (nmhGetFsDot11RadioType (i4RadioIfIndex, &u4GetRadioType)
                        != SNMP_SUCCESS)
                    {
                        continue;
                    }

                    if (u4GetRadioType == u4RadioType)
                    {
                        if (nmhTestv2IfMainAdminStatus (&u4ErrorCode,
                                                        i4RadioIfIndex,
                                                        i4FsDot11bNetworkEnable)
                            != SNMP_SUCCESS)
                        {
                            continue;
                        }
                        if (nmhSetIfMainAdminStatus (i4RadioIfIndex,
                                                     i4FsDot11bNetworkEnable) !=
                            SNMP_SUCCESS)
                        {
                            continue;
                        }
                    }
                }
            }
        }
    }

    CONFIG_UNLOCK ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgGetWlanCount
 * Input       :   u2WlanCount
 * Descritpion :  This Routine will invoke the radio if module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
VOID
WssCfgGetWlanCount (UINT2 *pu2WlanCount)
{
#ifdef WSSWLAN_WANTED
    CONFIG_LOCK ();

    WssWlanGetProfileCount (pu2WlanCount);

    CONFIG_UNLOCK ();
#endif
    return;
}

/****************************************************************************
 * Function    :  WssCfgGetWlanCount
 * Input       :   u2WlanCount
 * Descritpion :  This Routine will invoke the radio if module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
VOID
WssCfgGetWlanProfileId (UINT2 u2WlanId, UINT2 *pu2WlanProfileId)
{
#ifdef WSSWLAN_WANTED
    CONFIG_LOCK ();

    WssWlanGetWlanProfileId (u2WlanId, pu2WlanProfileId);

    CONFIG_UNLOCK ();
#endif

    return;
}

/****************************************************************************
 * Function    :  WssCfgGetWlanInternlId
 * Input       :   u2WlanCount
 * Descritpion :  This Routine will get the Wlan Internal Id
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
UINT1
WssCfgGetWlanInternlId (UINT4 u4WlanProfileId, UINT2 *pu2WlanInternalId)
{
#ifdef WSSWLAN_WANTED
    CONFIG_LOCK ();
    if (WssWlanGetWlanInternalId (u4WlanProfileId, pu2WlanInternalId) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    CONFIG_UNLOCK ();
#endif

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfGetDot11DesiredSSID
 * Input       :   u2WlanCount
 * Descritpion :  This Routine will get the SSID for WLAN module
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
VOID
WssCfGetDot11DesiredSSID (UINT4 u4WlanIfIndex, UINT1 *pu1DesiredSSID)
{
#ifdef WSSWLAN_WANTED
    CONFIG_LOCK ();

    WssWlanGetDot11DesiredSSID (u4WlanIfIndex, pu1DesiredSSID);

    CONFIG_UNLOCK ();
#endif
    return;
}

/****************************************************************************
 * Function    :  WssCfgGetWlanIfIndexfromSSID
 * Input       :  SSID
 * Descritpion :  This Routine will invoke the radio if module
 * Output      :  WlanIfIndex
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
UINT1
WssCfgGetWlanIfIndexfromSSID (UINT1 *pu1SSID, UINT4 *pu4WlanIfIndex)
{
#ifdef WLC_WANTED
    CONFIG_LOCK ();
    if (WssWlanGetWlanIfIndexfromSSID (pu1SSID, pu4WlanIfIndex) != OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }

    CONFIG_UNLOCK ();
#else
    UNUSED_PARAM (pu1SSID);
    UNUSED_PARAM (pu4WlanIfIndex);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgGetWlanProfilefromIndex
 * Input       :  SSID
 * Descritpion :  This Routine will invoke the radio if module
 * Output      :  WlanIfIndex
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
UINT1
WssCfgGetWlanProfilefromIndex (UINT4 u4WlanIfIndex, UINT2 *pu2WlanProfileId)
{
#ifdef WLC_WANTED
    CONFIG_LOCK ();
    if (WssWlanGetWlanIdfromIfIndex (u4WlanIfIndex, pu2WlanProfileId)
        != OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }

    CONFIG_UNLOCK ();
#else
    UNUSED_PARAM (u4WlanIfIndex);
    UNUSED_PARAM (pu2WlanProfileId);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgGetWtpInfofromRadio
 * Input       :  u4IfIndex 
 * Descritpion :  This Routine will invoke the radio if module
 * Output      :  WlanIfIndex
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
UINT1
WssCfgGetWtpIdfromRadio (UINT4 u4RadioIfIndex, UINT2 *pu2WtpInternalId,
                         UINT4 *pu4RadioId)
{
#ifdef WLC_WANTED
    CONFIG_LOCK ();
    if (RadioIfGetWtpInternalId (u4RadioIfIndex, pu2WtpInternalId,
                                 pu4RadioId) != OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }

    CONFIG_UNLOCK ();
#else
    UNUSED_PARAM (u4RadioIfIndex);
    UNUSED_PARAM (pu2WtpInternalId);
    UNUSED_PARAM (pu4RadioId);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetFsDot11RadioConfigTable
 * Input       :   pWsscfgFsDot11RadioConfigEntry
                   pWsscfgIsSetFsDot11RadioConfigEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetFsDot11RadioConfigTable (tWsscfgFsDot11RadioConfigEntry *
                                  pWsscfgFsDot11RadioConfigEntry,
                                  tWsscfgIsSetFsDot11RadioConfigEntry *
                                  pWsscfgIsSetFsDot11RadioConfigEntry)
{
    tRadioIfGetDB       RadioIfGetDB;

    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

#ifdef WLC_WANTED
    CONFIG_LOCK ();

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex =
        (UINT4) (pWsscfgFsDot11RadioConfigEntry->MibObject.i4IfIndex);

    if (pWsscfgIsSetFsDot11RadioConfigEntry->bFsDot11RadioType != OSIX_FALSE)
    {
        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType =
            (UINT4) (pWsscfgFsDot11RadioConfigEntry->MibObject.
                     u4FsDot11RadioType);
    }
    RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
        UtlShMemAllocAntennaSelectionBuf ();

    if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
    {
        RADIOIF_TRC (RADIOIF_FAILURE_TRC, "\r%% Memory allocation for Antenna"
                     " selection failed.\r\n");
        return OSIX_FAILURE;
    }

    if (RadioIfSetDot11RadioType (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }
    UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                     pu1AntennaSelection);
    CONFIG_UNLOCK ();
#else
    UNUSED_PARAM (pWsscfgFsDot11RadioConfigEntry);
    UNUSED_PARAM (pWsscfgIsSetFsDot11RadioConfigEntry);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetDot11Radiotype
 * Input       :   pWsscfgFsDot11RadioConfigEntry
                   pWsscfgIsSetFsDot11RadioConfigEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetDot11Radiotype (tRadioIfGetDB * pRadioIfGetDB)
{
#ifdef WLC_WANTED
    UINT4               u4ErrorCode = 0;
    tWsscfgFsDot11RadioConfigEntry WsscfgFsDot11RadioConfigEntry;
    tWsscfgIsSetFsDot11RadioConfigEntry WsscfgIsSetFsDot11RadioConfigEntry;

    MEMSET (&WsscfgFsDot11RadioConfigEntry, 0,
            sizeof (tWsscfgFsDot11RadioConfigEntry));
    MEMSET (&WsscfgIsSetFsDot11RadioConfigEntry, 0,
            sizeof (tWsscfgIsSetFsDot11RadioConfigEntry));

    WsscfgFsDot11RadioConfigEntry.MibObject.i4IfIndex =
        pRadioIfGetDB->RadioIfGetAllDB.u4RadioIfIndex;
    WsscfgIsSetFsDot11RadioConfigEntry.bIfIndex = OSIX_TRUE;

    if (pRadioIfGetDB->RadioIfIsGetAllDB.bDot11RadioType != OSIX_FALSE)
    {
        if (pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType != 0)
        {
            WsscfgIsSetFsDot11RadioConfigEntry.bFsDot11RadioType = OSIX_TRUE;
            WsscfgFsDot11RadioConfigEntry.MibObject.u4FsDot11RadioType =
                (INT4) (pRadioIfGetDB->RadioIfGetAllDB.u4Dot11RadioType);
        }

    }
    WsscfgIsSetFsDot11RadioConfigEntry.bFsDot11RowStatus =
        pRadioIfGetDB->RadioIfIsGetAllDB.bRowStatus;

    WsscfgFsDot11RadioConfigEntry.MibObject.i4FsDot11RowStatus =
        (INT4) (pRadioIfGetDB->RadioIfGetAllDB.u4RowStatus);

    if (WsscfgTestAllFsDot11RadioConfigTable (&u4ErrorCode,
                                              &WsscfgFsDot11RadioConfigEntry,
                                              &WsscfgIsSetFsDot11RadioConfigEntry,
                                              OSIX_TRUE,
                                              OSIX_TRUE) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (WsscfgSetAllFsDot11RadioConfigTable (&WsscfgFsDot11RadioConfigEntry,
                                             &WsscfgIsSetFsDot11RadioConfigEntry,
                                             OSIX_TRUE,
                                             OSIX_TRUE) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pRadioIfGetDB);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
  * Function    :WssCfgSetFsDot11RadioTypeTable
  * Input       :
  * Descritpion :
  * Output      :  None
  * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WssCfgSetFsDot11RadioTypeTable (UINT4 u4RadioType)
{
#ifdef WLC_WANTED
    tRadioIfGetDB       RadioIfGetDB;
    MEMSET (&RadioIfGetDB, 0, sizeof (tRadioIfGetDB));

    INT4                i4OutCome = 0;
    UINT4               u4IfIndex = 0;
    UINT4               currentProfileId = 0, nextProfileId = 0;
    UINT4               u4currentBindingId = 0, u4nextBindingId = 0;

    CONFIG_LOCK ();

    i4OutCome = nmhGetFirstIndexFsCapwapWirelessBindingTable (&nextProfileId,
                                                              &u4nextBindingId);
    if (i4OutCome == SNMP_FAILURE)
    {
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }

    do
    {
        currentProfileId = nextProfileId;
        u4currentBindingId = u4nextBindingId;

        i4OutCome = nmhGetCapwapBaseWirelessBindingVirtualRadioIfIndex
            (nextProfileId, u4nextBindingId, (INT4 *) &u4IfIndex);

        if (i4OutCome == SNMP_FAILURE)
        {
            CONFIG_UNLOCK ();
            return OSIX_FAILURE;
        }
        RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex = u4IfIndex;
        RadioIfGetDB.RadioIfIsGetAllDB.bDot11RadioType = OSIX_TRUE;
        RadioIfGetDB.RadioIfGetAllDB.u4Dot11RadioType = u4RadioType;

        RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection =
            UtlShMemAllocAntennaSelectionBuf ();

        if (RadioIfGetDB.RadioIfGetAllDB.pu1AntennaSelection == NULL)
        {
            RADIOIF_TRC (RADIOIF_FAILURE_TRC,
                         "\r%% Memory allocation for Antenna"
                         " selection failed.\r\n");
            continue;
        }
        if (RadioIfSetDot11RadioType (&RadioIfGetDB) != OSIX_SUCCESS)
        {
            UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                             pu1AntennaSelection);
            continue;
        }
        UtlShMemFreeAntennaSelectionBuf (RadioIfGetDB.RadioIfGetAllDB.
                                         pu1AntennaSelection);
    }
    while (nmhGetNextIndexFsCapwapWirelessBindingTable (currentProfileId,
                                                        &nextProfileId,
                                                        u4currentBindingId,
                                                        &u4nextBindingId) ==
           SNMP_SUCCESS);

    CONFIG_UNLOCK ();
#else
    UNUSED_PARAM (u4RadioType);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
  * Function    :WsscfgSetManagmentSSID
  * Input       :
  * Descritpion :
  * Output      :  None
  * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
WsscfgSetManagmentSSID (INT4 i4FsDot11ManagmentSSID)
{
#ifdef WLC_WANTED
    tWssWlanDB          WssWlanDB;
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));
    INT4                i4OutCome = 0;
    INT4                i4IfIndex = 0;
    UINT4               currentProfileId = 0, nextProfileId = 0;
    UNUSED_PARAM (i4FsDot11ManagmentSSID);

    CONFIG_LOCK ();

    i4OutCome = nmhGetFirstIndexCapwapDot11WlanTable (&nextProfileId);
    if (i4OutCome == SNMP_FAILURE)
    {
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }
    do
    {
        currentProfileId = nextProfileId;

        if (nmhGetCapwapDot11WlanProfileIfIndex (nextProfileId, &i4IfIndex) !=
            SNMP_SUCCESS)
        {
        }

        WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex = (UINT4) i4IfIndex;
        WssWlanDB.WssWlanIsPresentDB.bManagmentSSID = OSIX_TRUE;

        if (WssWlanSetFsDot11StationConfigTable (&WssWlanDB) != OSIX_SUCCESS)
        {
            CONFIG_UNLOCK ();
            return OSIX_FAILURE;
        }

    }
    while (nmhGetNextIndexCapwapDot11WlanTable
           (currentProfileId, &nextProfileId) == SNMP_SUCCESS);

    CONFIG_UNLOCK ();
#else
    UNUSED_PARAM (i4FsDot11ManagmentSSID);
#endif
    return OSIX_SUCCESS;
}

/* Added for Web Authentication feature */
/****************************************************************************
 * Function    :  WssCfgSetFsSecurityWebAuthGuestInfoTable
 * Input       :   pWsscfgFsSecurityWebAuthGuestInfoEntry
 *                 pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry
 * Descritpion :  This Routine checks set value
 *                 with that of the value in database
 *                and do the necessary protocol operation.
 * Output      : None
 * Returns     : OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/
INT4
WssCfgSetFsSecurityWebAuthGuestInfoTable (tWsscfgFsSecurityWebAuthGuestInfoEntry
                                          *
                                          pWsscfgFsSecurityWebAuthGuestInfoEntry,
                                          tWsscfgIsSetFsSecurityWebAuthGuestInfoEntry
                                          *
                                          pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry)
{
#ifdef WLC_WANTED
    tWssIfIsSetWebAuthDB WssStaWebAuthIsSetDB;
    tWssIfWebAuthDB     WssStaWebAuthDB;
    MEMSET (&WssStaWebAuthIsSetDB, 0, sizeof (tWssIfIsSetWebAuthDB));
    MEMSET (&WssStaWebAuthDB, 0, sizeof (tWssIfWebAuthDB));

    CONFIG_LOCK ();
#ifdef NPAPI_WANTED
    WssStaWebAuthSockInit ();
#endif
    /* Assign the user configured username */
    WssStaWebAuthIsSetDB.bUserName = OSIX_TRUE;
    MEMCPY (WssStaWebAuthDB.au1UserName,
            pWsscfgFsSecurityWebAuthGuestInfoEntry->
            MibObject.au1FsSecurityWebAuthUName, WSS_STA_WEBUSER_LEN_MAX);
    if (pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry->
        bFsSecurityWebAuthUPassword != OSIX_FALSE)
    {
        WssStaWebAuthIsSetDB.bUserPwd = OSIX_TRUE;
        MEMCPY (WssStaWebAuthDB.au1UserPwd,
                pWsscfgFsSecurityWebAuthGuestInfoEntry->
                MibObject.au1FsSecurityWebAuthUPassword,
                WSS_STA_WEBUSER_LEN_MAX);
    }
    if (pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry->
        bFsSecurityWebAuthUserLifetime != OSIX_FALSE)
    {
        WssStaWebAuthIsSetDB.bMaxUserLifetime = OSIX_TRUE;
        WssStaWebAuthDB.u4MaxUserLifetime =
            (UINT4) (pWsscfgFsSecurityWebAuthGuestInfoEntry->
                     MibObject.i4FsSecurityWebAuthUserLifetime);
    }
    if (pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry->bFsSecurityWlanProfileId !=
        OSIX_FALSE)
    {
        WssStaWebAuthIsSetDB.bWlanProfileId = OSIX_TRUE;
        WssStaWebAuthDB.u4WlanId =
            (UINT4) (pWsscfgFsSecurityWebAuthGuestInfoEntry->MibObject.
                     i4FsSecurityWlanProfileId);
    }
    if (pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry->
        bFsSecurityWebAuthGuestInfoRowStatus != OSIX_FALSE)
    {
        WssStaWebAuthIsSetDB.bRowStatus = OSIX_TRUE;
        WssStaWebAuthDB.u1RowStatus =
            (UINT1) (pWsscfgFsSecurityWebAuthGuestInfoEntry->MibObject.
                     i4FsSecurityWebAuthGuestInfoRowStatus);
    }

    if (WssStaWebAuthDbUpdate (&WssStaWebAuthIsSetDB,
                               &WssStaWebAuthDB) != OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }
    CONFIG_UNLOCK ();
#else
    UNUSED_PARAM (pWsscfgFsSecurityWebAuthGuestInfoEntry);
    UNUSED_PARAM (pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgSetRadioQosTable
 * Input       :   WssCfgSetRadioQosTable
 *                 pWsscfgIsSetFsDot11RadioQosEntry
 * Descritpion :  This Routine checks set value
 *                 with that of the value in database
 *                and do the necessary protocol operation.
 * Output      : None
 * Returns     : OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/
INT4
WssCfgSetRadioQosTable (tWsscfgFsDot11RadioQosEntry *
                        pWsscfgFsDot11RadioQosEntry,
                        tWsscfgIsSetFsDot11RadioQosEntry *
                        pWsscfgIsSetFsDot11RadioQosEntry)
{
    tRadioIfGetDB       RadioIfGetDB;

    CONFIG_LOCK ();

    RadioIfGetDB.RadioIfGetAllDB.u4RadioIfIndex
        = (UINT4) pWsscfgFsDot11RadioQosEntry->MibObject.i4IfIndex;
    RadioIfGetDB.RadioIfIsGetAllDB.bRadioIfIndex = OSIX_TRUE;

    RadioIfGetDB.RadioIfGetAllDB.u1QosIndex = 1;
    RadioIfGetDB.RadioIfIsGetAllDB.bQosIndex = OSIX_TRUE;

    if (pWsscfgIsSetFsDot11RadioQosEntry->bFsDot11TaggingPolicy == OSIX_TRUE)
    {
        RadioIfGetDB.RadioIfGetAllDB.u1TaggingPolicy =
            (UINT1) pWsscfgFsDot11RadioQosEntry->
            MibObject.au1FsDot11TaggingPolicy[0];
        RadioIfGetDB.RadioIfIsGetAllDB.bTaggingPolicy = OSIX_TRUE;
    }
#ifdef WLC_WANTED
    if (RadioIfSetEDCATable (&RadioIfGetDB) != OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }
#endif
    CONFIG_UNLOCK ();
    UNUSED_PARAM (RadioIfGetDB);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  WssCfgDeleteWlanBinding     
 * Input       :   WssCfgSetRadioQosTable
 *                 pWsscfgIsSetFsDot11RadioQosEntry
 * Descritpion :  This Routine checks set value
 *                 with that of the value in database
 *                and do the necessary protocol operation.
 * Output      : None
 * Returns     : OSIX_SUCCESS or OSIX_FAILURE
 ****************************************************************************/
UINT4
WssCfgDeleteWlanBinding (UINT4 u4BssIfIndex)
{
    INT4                i4RadioIfIndex;
    INT4                i4NextRadioIfIndex;
    INT4                i4BssIntfIndex;
    UINT4               u4WlanProfileId;
    UINT4               u4NextWlanProfileId;
    UINT4               u4ErrorCode;
    INT4                i4OutCome = 0;
    tWsscfgCapwapDot11WlanBindEntry WsscfgCapwapDot11WlanBindEntry;
    tWsscfgIsSetCapwapDot11WlanBindEntry WsscfgIsSetCapwapDot11WlanBindEntry;

    MEMSET (&WsscfgCapwapDot11WlanBindEntry, 0,
            sizeof (tWsscfgCapwapDot11WlanBindEntry));
    MEMSET (&WsscfgIsSetCapwapDot11WlanBindEntry, 0,
            sizeof (tWsscfgIsSetCapwapDot11WlanBindEntry));

    i4OutCome = nmhGetFirstIndexCapwapDot11WlanBindTable (&i4NextRadioIfIndex,
                                                          &u4NextWlanProfileId);

    if (i4OutCome == SNMP_FAILURE)
    {
        return OSIX_SUCCESS;
    }

    do
    {
        i4RadioIfIndex = i4NextRadioIfIndex;
        u4WlanProfileId = u4NextWlanProfileId;

        if (nmhGetCapwapDot11WlanBindBssIfIndex (i4RadioIfIndex,
                                                 u4WlanProfileId,
                                                 &i4BssIntfIndex) ==
            SNMP_SUCCESS)
        {
            if (i4BssIntfIndex == (INT4) u4BssIfIndex)
            {
                WsscfgIsSetCapwapDot11WlanBindEntry.
                    bCapwapDot11WlanBindRowStatus = OSIX_TRUE;
                WsscfgIsSetCapwapDot11WlanBindEntry.bIfIndex = OSIX_TRUE;
                WsscfgIsSetCapwapDot11WlanBindEntry.
                    bCapwapDot11WlanProfileId = OSIX_TRUE;

                WsscfgCapwapDot11WlanBindEntry.MibObject.
                    i4CapwapDot11WlanBindRowStatus = DESTROY;

                WsscfgCapwapDot11WlanBindEntry.MibObject.
                    u4CapwapDot11WlanProfileId = u4WlanProfileId;
                WsscfgCapwapDot11WlanBindEntry.MibObject.
                    i4IfIndex = i4RadioIfIndex;

                if (WsscfgTestAllCapwapDot11WlanBindTable (&u4ErrorCode,
                                                           &WsscfgCapwapDot11WlanBindEntry,
                                                           &WsscfgIsSetCapwapDot11WlanBindEntry,
                                                           OSIX_TRUE,
                                                           OSIX_TRUE) !=
                    OSIX_SUCCESS)
                {
                    continue;
                }
                if (WsscfgSetAllCapwapDot11WlanBindTable
                    (&WsscfgCapwapDot11WlanBindEntry,
                     &WsscfgIsSetCapwapDot11WlanBindEntry, OSIX_TRUE,
                     OSIX_TRUE) != OSIX_SUCCESS)
                {
                    continue;
                }

            }
        }

    }
    while (nmhGetNextIndexCapwapDot11WlanBindTable (i4RadioIfIndex,
                                                    &i4NextRadioIfIndex,
                                                    u4WlanProfileId,
                                                    &u4NextWlanProfileId) ==
           SNMP_SUCCESS);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  WssCfgDestroyDot11WlanBindTable
 Input       :  pWsscfgSetCapwapDot11WlanBindEntry
                pWsscfgIsSetCapwapDot11WlanBindEntry
 Description :  This fnction will delete the WLAN binding table
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
UINT4
WssCfgDestroyDot11WlanBindTable (UINT4 u4RadioIfIndex, UINT4 u4WlanProfileId)
{
#ifdef WLC_WANTED
    INT4                i4BssIntfIndex = 0;
    tWsscfgIsSetCapwapDot11WlanBindEntry WsscfgIsSetCapwapDot11WlanBindEntry;
    tWsscfgCapwapDot11WlanBindEntry WsscfgCapwapDot11WlanBindEntry;
    tWsscfgCapwapDot11WlanBindEntry *pWsscfgCapwapDot11WlanBindEntry = NULL;
    tWssPMInfo          WssPmInfo;
    tWssifauthDBMsgStruct *pWssBssIdStnStats = NULL;
    pWssBssIdStnStats =
        (tWssifauthDBMsgStruct *) (VOID *) UtlShMemAllocWssIfAuthDbBuf ();
    if (pWssBssIdStnStats == NULL)
    {
        WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                     "WssCfgDestroyDot11WlanBindTable:- "
                     "UtlShMemAllocWssIfAuthDbBuf returned failure\n"));
        return OSIX_FAILURE;
    }
    MEMSET (&WsscfgCapwapDot11WlanBindEntry, 0,
            sizeof (tWsscfgCapwapDot11WlanBindEntry));
    MEMSET (&WsscfgIsSetCapwapDot11WlanBindEntry, 0,
            sizeof (tWsscfgIsSetCapwapDot11WlanBindEntry));

    CONFIG_LOCK ();

    if (nmhGetCapwapDot11WlanBindBssIfIndex ((INT4) u4RadioIfIndex,
                                             u4WlanProfileId,
                                             &i4BssIntfIndex) == SNMP_SUCCESS)
    {
        WsscfgCapwapDot11WlanBindEntry.MibObject.i4IfIndex =
            (INT4) u4RadioIfIndex;

        WsscfgCapwapDot11WlanBindEntry.MibObject.u4CapwapDot11WlanProfileId =
            u4WlanProfileId;

        /* Check whether the node is already present */
        pWsscfgCapwapDot11WlanBindEntry =
            RBTreeGet (gWsscfgGlobals.WsscfgGlbMib.
                       CapwapDot11WlanBindTable,
                       (tRBElem *) & WsscfgCapwapDot11WlanBindEntry);

        if (pWsscfgCapwapDot11WlanBindEntry == NULL)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WssCfgDestroyDot11WlanBindTable:"
                         "No entry present function returns failure.\r\n"));
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssBssIdStnStats);
            CONFIG_UNLOCK ();
            return OSIX_SUCCESS;
        }

        RBTreeRem (gWsscfgGlobals.WsscfgGlbMib.
                   CapwapDot11WlanBindTable, pWsscfgCapwapDot11WlanBindEntry);

        if (WsscfgSetAllCapwapDot11WlanBindTableTrigger
            (&WsscfgCapwapDot11WlanBindEntry,
             &WsscfgIsSetCapwapDot11WlanBindEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                         "WssCfgDestroyDot11WlanBindTable:"
                         "WsscfgSetAllCapwapDot11WlanBindTableTrigger function"
                         "returns failure.\r\n"));
            UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssBssIdStnStats);
            CONFIG_UNLOCK ();
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID,
                            (UINT1 *) pWsscfgCapwapDot11WlanBindEntry);

        if (i4BssIntfIndex != 0)
        {
            MEMSET (&WssPmInfo, 0, sizeof (tWssPMInfo));

            WssPmInfo.u2ProfileBSSID = (UINT2) i4BssIntfIndex;

            if (pmWTPInfoNotify (BSSID_DEL, &WssPmInfo) != OSIX_SUCCESS)
            {
                WSSCFG_TRC ((WSSCFG_UTIL_TRC,
                             "Create BSSID : Update PM Stats DB Failed \r\n"));
                UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssBssIdStnStats);
                CONFIG_UNLOCK ();
                return OSIX_FAILURE;
            }

            MEMSET (pWssBssIdStnStats, 0, sizeof (tWssifauthDBMsgStruct));
            pWssBssIdStnStats->WssBssStnStatsDB.u4BssIfIndex =
                (UINT4) i4BssIntfIndex;
            if ((WssIfProcessWssAuthDBMsg (WSS_AUTH_DEL_BSSSID_STATION_STATS,
                                           pWssBssIdStnStats) != OSIX_SUCCESS))
            {
                return OSIX_FAILURE;
            }
        }
    }
    UtlShMemFreeWssIfAuthDbBuf ((UINT1 *) pWssBssIdStnStats);
    CONFIG_UNLOCK ();
#else
    UNUSED_PARAM (u4RadioIfIndex);
    UNUSED_PARAM (u4WlanProfileId);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    : WssGetCapwapWlanId
 *
 * Description : This function is utility function to give the wlan index
 *               for the profile id
 *
 * Input       : u4ProfileId - WTP  Profile ID
 *               u4WlanId    - radio index
 *
 * Output      : u4WlanId -  Radio Index
 *
 * Returns     : OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/

UINT1
WssGetCapwapWlanId (UINT4 u4profileId, UINT4 *u4WlanId)
{

    tWssWlanDB          wssWlanDB;
    MEMSET (&wssWlanDB, 0, sizeof (tWssWlanDB));
    wssWlanDB.WssWlanAttributeDB.u4WlanIfIndex = (UINT4) u4profileId;
    wssWlanDB.WssWlanIsPresentDB.bWlanProfileId = OSIX_TRUE;
    if (WssIfProcessWssWlanDBMsg (WSS_WLAN_GET_IFINDEX_ENTRY, &wssWlanDB) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    *u4WlanId = wssWlanDB.WssWlanAttributeDB.u2WlanProfileId;

    return OSIX_SUCCESS;
}

#endif

/****************************************************************************
 * Function    :  WssCfgSetFsDot11ExternWebAuthProfileTable
 * Input       :   pWsscfgFsDot11ExternalWebAuthProfileEntry
                   pWsscfgIsSetFsDot11ExternalWebAuthProfileEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WssCfgSetFsDot11ExternalWebAuthProfileTable
    (tWsscfgFsDot11ExternalWebAuthProfileEntry *
     pWsscfgFsDot11ExternalWebAuthProfileEntry,
     tWsscfgIsSetFsDot11ExternalWebAuthProfileEntry *
     pWsscfgIsSetFsDot11ExternalWebAuthProfileEntry)
{
#ifdef WLC_WANTED
    tWssWlanDB          WssWlanDB;
    MEMSET (&WssWlanDB, 0, sizeof (tWssWlanDB));

    if ((pWsscfgFsDot11ExternalWebAuthProfileEntry == NULL) ||
        (pWsscfgIsSetFsDot11ExternalWebAuthProfileEntry == NULL))
    {
        return OSIX_FAILURE;
    }

    CONFIG_LOCK ();
    if (pWsscfgIsSetFsDot11ExternalWebAuthProfileEntry->
        bFsDot11ExternalWebAuthMethod != OSIX_FALSE)
    {
        WssWlanDB.WssWlanAttributeDB.u1ExternalWebAuthMethod =
            (UINT1) (pWsscfgFsDot11ExternalWebAuthProfileEntry->
                     MibObject.i4FsDot11ExternalWebAuthMethod);
        WssWlanDB.WssWlanIsPresentDB.bExternalWebAuthMethod = OSIX_TRUE;
    }
    if (pWsscfgIsSetFsDot11ExternalWebAuthProfileEntry->
        bFsDot11ExternalWebAuthUrl != OSIX_FALSE)
    {
        MEMCPY (WssWlanDB.WssWlanAttributeDB.au1ExternalWebAuthUrl,
                pWsscfgFsDot11ExternalWebAuthProfileEntry->MibObject.
                au1FsDot11ExternalWebAuthUrl,
                STRLEN (pWsscfgFsDot11ExternalWebAuthProfileEntry->MibObject.
                        au1FsDot11ExternalWebAuthUrl));

        WssWlanDB.WssWlanIsPresentDB.bExternalWebAuthUrl = OSIX_TRUE;
    }

    WssWlanDB.WssWlanAttributeDB.u4WlanIfIndex
        =
        (UINT4) (pWsscfgFsDot11ExternalWebAuthProfileEntry->MibObject.
                 i4IfIndex);
    WssWlanDB.WssWlanIsPresentDB.bWlanIfIndex = OSIX_TRUE;

    if (WssWlanSetFsDot11ExternalWebAuthProfileTable (&WssWlanDB) !=
        OSIX_SUCCESS)
    {
        CONFIG_UNLOCK ();
        return OSIX_FAILURE;
    }
    CONFIG_UNLOCK ();
#else
    UNUSED_PARAM (pWsscfgFsDot11ExternalWebAuthProfileEntry);
    UNUSED_PARAM (pWsscfgIsSetFsDot11ExternalWebAuthProfileEntry);
#endif
    return OSIX_SUCCESS;
}
