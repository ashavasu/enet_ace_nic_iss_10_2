/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: wsscfgdefaultg.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ 
*
* Description: This file contains the routines to initialize the
*              mib objects for the module Wsscfg 
*********************************************************************/

#include "wsscfginc.h"
#include "wsscfgcli.h"

/****************************************************************************
* Function    : WsscfgInitializeMibDot11WEPKeyMappingsTable
* Input       : pWsscfgDot11WEPKeyMappingsEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
WsscfgInitializeMibDot11WEPKeyMappingsTable (tWsscfgDot11WEPKeyMappingsEntry *
                                             pWsscfgDot11WEPKeyMappingsEntry)
{

    pWsscfgDot11WEPKeyMappingsEntry->MibObject.i4Dot11WEPKeyMappingStatus = 1;

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeMibDot11GroupAddressesTable
* Input       : pWsscfgDot11GroupAddressesEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
WsscfgInitializeMibDot11GroupAddressesTable (tWsscfgDot11GroupAddressesEntry *
                                             pWsscfgDot11GroupAddressesEntry)
{

    pWsscfgDot11GroupAddressesEntry->MibObject.i4Dot11GroupAddressesStatus = 1;

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeMibCapwapDot11WlanTable
* Input       : pWsscfgCapwapDot11WlanEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
WsscfgInitializeMibCapwapDot11WlanTable (tWsscfgCapwapDot11WlanEntry *
                                         pWsscfgCapwapDot11WlanEntry)
{
    pWsscfgCapwapDot11WlanEntry->MibObject.i4CapwapDot11WlanMacType =
        INVALID_MAC_TYPE;
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeMibCapwapDot11WlanBindTable
* Input       : pWsscfgCapwapDot11WlanBindEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
WsscfgInitializeMibCapwapDot11WlanBindTable (tWsscfgCapwapDot11WlanBindEntry *
                                             pWsscfgCapwapDot11WlanBindEntry)
{
    UNUSED_PARAM (pWsscfgCapwapDot11WlanBindEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    : WsscfgInitializeMibFsQAPProfileTable
 * Input       : pWsscfgFsQAPProfileEntry
 * Description : It Intialize the given structure
 * Output      : None
 * Returns     : OSIX_SUCCESS or OSIX_FAILURE
 * *****************************************************************************/
INT4
WsscfgInitializeMibFsQAPProfileTable (tWsscfgFsQAPProfileEntry *
                                      pWsscfgFsQAPProfileEntry)
{
    pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileCWmin = 7;
    pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileCWmax = 15;
    pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileAIFSN = 3;
    pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileTXOPLimit = 3;
    pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfilePriorityValue = 3;
    pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileDscpValue = 3;
    pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileAdmissionControl = 1;
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeMibFsDot11CapabilityProfileTable
* Input       : pWsscfgFsDot11CapabilityProfileEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgInitializeMibFsDot11CapabilityProfileTable
    (tWsscfgFsDot11CapabilityProfileEntry *
     pWsscfgFsDot11CapabilityProfileEntry)
{
/* UNUSED_PARAM (pWsscfgFsDot11CapabilityProfileEntry); */
    pWsscfgFsDot11CapabilityProfileEntry->MibObject.
        i4FsDot11CFPollable = WSSCFG_DISABLED;
    pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11CFPollRequest = WSSCFG_DISABLED;
    pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11PrivacyOptionImplemented = WSSCFG_DISABLED;
    pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11ShortPreambleOptionImplemented = WSSCFG_DISABLED;
    pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11PBCCOptionImplemented = WSSCFG_DISABLED;
    pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11ChannelAgilityPresent = WSSCFG_DISABLED;
    pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11QosOptionImplemented = WSSCFG_DISABLED;
    pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11SpectrumManagementRequired = WSSCFG_DISABLED;
    pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11ShortSlotTimeOptionImplemented = WSSCFG_DISABLED;
    pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11APSDOptionImplemented = WSSCFG_DISABLED;
    pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11DSSSOFDMOptionEnabled = WSSCFG_DISABLED;
    pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11DelayedBlockAckOptionImplemented = WSSCFG_DISABLED;
    pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11ImmediateBlockAckOptionImplemented = WSSCFG_DISABLED;
    pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11QAckOptionImplemented = WSSCFG_DISABLED;
    pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11QueueRequestOptionImplemented = WSSCFG_DISABLED;
    pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11TXOPRequestOptionImplemented = WSSCFG_DISABLED;
    pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11RSNAOptionImplemented = WSSCFG_DISABLED;
    pWsscfgFsDot11CapabilityProfileEntry->
        MibObject.i4FsDot11RSNAPreauthenticationImplemented = WSSCFG_DISABLED;
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeMibFsDot11AuthenticationProfileTable
* Input       : pWsscfgFsDot11AuthenticationProfileEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgInitializeMibFsDot11AuthenticationProfileTable
    (tWsscfgFsDot11AuthenticationProfileEntry *
     pWsscfgFsDot11AuthenticationProfileEntry)
{
/* UNUSED_PARAM (pWsscfgFsDot11AuthenticationProfileEntry); */
    pWsscfgFsDot11AuthenticationProfileEntry->
        MibObject.i4FsDot11AuthenticationAlgorithm = 2;
    pWsscfgFsDot11AuthenticationProfileEntry->
        MibObject.i4FsDot11WepKeyIndex = 1;
    pWsscfgFsDot11AuthenticationProfileEntry->MibObject.i4FsDot11WepKeyType = 1;
    pWsscfgFsDot11AuthenticationProfileEntry->
        MibObject.i4FsDot11WepKeyLength = 40;
    pWsscfgFsDot11AuthenticationProfileEntry->MibObject.i4FsDot11WepKeyLen = 0;
    pWsscfgFsDot11AuthenticationProfileEntry->
        MibObject.i4FsDot11WebAuthentication = WSS_AUTH_STATUS_DISABLE;
    MEMSET (pWsscfgFsDot11AuthenticationProfileEntry->
            MibObject.au1FsDot11WepKey, 0, 104);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeMibFsDot11RadioConfigTable
* Input       : pWsscfgFsDot11RadioConfigEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
WsscfgInitializeMibFsDot11RadioConfigTable (tWsscfgFsDot11RadioConfigEntry *
                                            pWsscfgFsDot11RadioConfigEntry)
{
    pWsscfgFsDot11RadioConfigEntry->MibObject.u4FsDot11RadioType =
        CLI_RADIO_TYPEB;
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeMibFsDot11QosProfileTable
* Input       : pWsscfgFsDot11QosProfileEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
WsscfgInitializeMibFsDot11QosProfileTable (tWsscfgFsDot11QosProfileEntry *
                                           pWsscfgFsDot11QosProfileEntry)
{
/* UNUSED_PARAM (pWsscfgFsDot11QosProfileEntry); */
    pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11QosTraffic = 0;
    pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11QosPassengerTrustMode = 0;

    pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11QosRateLimit =
        WSSCFG_ENABLED;
    pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamCIR =
        CLI_DEF_CIR_VAL;
    pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamCBS =
        CLI_DEF_CBS_VAL;
    pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamEIR =
        CLI_DEF_EIR_VAL;
    pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamEBS =
        CLI_DEF_EBS_VAL;
    pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11DownStreamCIR =
        CLI_DEF_CIR_VAL;
    pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11DownStreamCBS =
        CLI_DEF_CBS_VAL;
    pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11DownStreamEIR =
        CLI_DEF_EIR_VAL;
    pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11DownStreamEBS =
        CLI_DEF_EBS_VAL;

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeMibFsDot11WlanCapabilityProfileTable
* Input       : pWsscfgFsDot11WlanCapabilityProfileEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgInitializeMibFsDot11WlanCapabilityProfileTable
    (tWsscfgFsDot11WlanCapabilityProfileEntry *
     pWsscfgFsDot11WlanCapabilityProfileEntry)
{
    pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanCFPollable = WSSCFG_ENABLED;
    pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanCFPollRequest = WSSCFG_ENABLED;
    pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanPrivacyOptionImplemented = WSSCFG_DISABLED;
    pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanShortPreambleOptionImplemented = WSSCFG_DISABLED;
    pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanPBCCOptionImplemented = WSSCFG_DISABLED;
    pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanChannelAgilityPresent = WSSCFG_DISABLED;
    pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanQosOptionImplemented = WSSCFG_DISABLED;
    pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanSpectrumManagementRequired = WSSCFG_DISABLED;
    pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanShortSlotTimeOptionImplemented = WSSCFG_DISABLED;
    pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanAPSDOptionImplemented = WSSCFG_DISABLED;
    pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanDSSSOFDMOptionEnabled = WSSCFG_DISABLED;
    pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanDelayedBlockAckOptionImplemented =
        WSSCFG_DISABLED;
    pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanImmediateBlockAckOptionImplemented =
        WSSCFG_DISABLED;
    pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanQAckOptionImplemented = WSSCFG_DISABLED;
    pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanQueueRequestOptionImplemented = WSSCFG_DISABLED;
    pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanTXOPRequestOptionImplemented = WSSCFG_DISABLED;
    pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanRSNAOptionImplemented = WSSCFG_DISABLED;
    pWsscfgFsDot11WlanCapabilityProfileEntry->
        MibObject.i4FsDot11WlanRSNAPreauthenticationImplemented =
        WSSCFG_DISABLED;

    /* UNUSED_PARAM (pWsscfgFsDot11WlanCapabilityProfileEntry); */
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeMibFsDot11WlanAuthenticationProfileTable
* Input       : pWsscfgFsDot11WlanAuthenticationProfileEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgInitializeMibFsDot11WlanAuthenticationProfileTable
    (tWsscfgFsDot11WlanAuthenticationProfileEntry *
     pWsscfgFsDot11WlanAuthenticationProfileEntry)
{
    pWsscfgFsDot11WlanAuthenticationProfileEntry->
        MibObject.i4FsDot11WlanAuthenticationAlgorithm = 2;
    pWsscfgFsDot11WlanAuthenticationProfileEntry->
        MibObject.i4FsDot11WlanWepKeyIndex = 1;
    pWsscfgFsDot11WlanAuthenticationProfileEntry->
        MibObject.i4FsDot11WlanWepKeyType = 1;
    pWsscfgFsDot11WlanAuthenticationProfileEntry->
        MibObject.i4FsDot11WlanWepKeyLength = 40;
    pWsscfgFsDot11WlanAuthenticationProfileEntry->
        MibObject.i4FsDot11WlanWepKeyLen = 0;
    pWsscfgFsDot11WlanAuthenticationProfileEntry->
        MibObject.i4FsDot11WlanWebAuthentication = WSS_AUTH_STATUS_DISABLE;
    MEMSET (pWsscfgFsDot11WlanAuthenticationProfileEntry->
            MibObject.au1FsDot11WlanWepKey, 0, 104);

/* UNUSED_PARAM (pWsscfgFsDot11WlanAuthenticationProfileEntry); */
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeMibFsDot11WlanQosProfileTable
* Input       : pWsscfgFsDot11WlanQosProfileEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
WsscfgInitializeMibFsDot11WlanQosProfileTable (tWsscfgFsDot11WlanQosProfileEntry
                                               *
                                               pWsscfgFsDot11WlanQosProfileEntry)
{
    pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanQosTraffic = 0;
    pWsscfgFsDot11WlanQosProfileEntry->MibObject.
        i4FsDot11WlanQosPassengerTrustMode = 0;

    pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanQosRateLimit = 0;
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeMibFsDot11CapabilityMappingTable
* Input       : pWsscfgFsDot11CapabilityMappingEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4 
     
     
     
     
     
     
     
    WsscfgInitializeMibFsDot11CapabilityMappingTable
    (tWsscfgFsDot11CapabilityMappingEntry *
     pWsscfgFsDot11CapabilityMappingEntry)
{
    UNUSED_PARAM (pWsscfgFsDot11CapabilityMappingEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeMibFsDot11AuthMappingTable
* Input       : pWsscfgFsDot11AuthMappingEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
WsscfgInitializeMibFsDot11AuthMappingTable (tWsscfgFsDot11AuthMappingEntry *
                                            pWsscfgFsDot11AuthMappingEntry)
{
    UNUSED_PARAM (pWsscfgFsDot11AuthMappingEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeMibFsDot11QosMappingTable
* Input       : pWsscfgFsDot11QosMappingEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
WsscfgInitializeMibFsDot11QosMappingTable (tWsscfgFsDot11QosMappingEntry *
                                           pWsscfgFsDot11QosMappingEntry)
{
    UNUSED_PARAM (pWsscfgFsDot11QosMappingEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeMibFsDot11WlanTable
* Input       : pWsscfgFsDot11WlanEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
WsscfgInitializeMibFsDot11WlanTable (tWsscfgFsDot11WlanEntry *
                                     pWsscfgFsDot11WlanEntry)
{
    UNUSED_PARAM (pWsscfgFsDot11WlanEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeMibFsDot11WlanBindTable
* Input       : pWsscfgFsDot11WlanBindEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
WsscfgInitializeMibFsDot11WlanBindTable (tWsscfgFsDot11WlanBindEntry *
                                         pWsscfgFsDot11WlanBindEntry)
{
    UNUSED_PARAM (pWsscfgFsDot11WlanBindEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeMibFsWtpImageUpgradeTable
* Input       : pWsscfgFsWtpImageUpgradeEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
WsscfgInitializeMibFsWtpImageUpgradeTable (tWsscfgFsWtpImageUpgradeEntry *
                                           pWsscfgFsWtpImageUpgradeEntry)
{
    UNUSED_PARAM (pWsscfgFsWtpImageUpgradeEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeMibFsRrmConfigTable
* Input       : pWsscfgFsRrmConfigEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
WsscfgInitializeMibFsRrmConfigTable (tWsscfgFsRrmConfigEntry *
                                     pWsscfgFsRrmConfigEntry)
{

    pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmDcaMode = 3;

    pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmDcaChannelSelectionMode = 3;

    pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmTpcMode = 3;

    pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmTpcSelectionMode = 3;

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : WsscfgInitializeDot11OperationTable
* Input       : pWsscfgDot11OperationEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
WsscfgInitializeMibDot11OperationTable (tWsscfgDot11OperationEntry *
                                        pWsscfgDot11OperationEntry)
{

    pWsscfgDot11OperationEntry->MibObject.i4Dot11RTSThreshold = 3000;
    pWsscfgDot11OperationEntry->MibObject.i4Dot11ShortRetryLimit = 7;
    pWsscfgDot11OperationEntry->MibObject.i4Dot11LongRetryLimit = 4;
    pWsscfgDot11OperationEntry->MibObject.i4Dot11FragmentationThreshold = 3000;

    return OSIX_SUCCESS;
}
