/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
*  $Id: wsscfglw.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $
*
* Description: This file contains some of low level functions used by protocol in Wsscfg module
*********************************************************************/

#include "wsscfginc.h"

/****************************************************************************
 Function    :  nmhValidateIndexInstanceCapwapDot11WlanTable
 Input       :  The Indices
                CapwapDot11WlanProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceCapwapDot11WlanTable (UINT4 u4CapwapDot11WlanProfileId)
{
    UNUSED_PARAM (u4CapwapDot11WlanProfileId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceCapwapDot11WlanBindTable
 Input       :  The Indices
                IfIndex
                CapwapDot11WlanProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceCapwapDot11WlanBindTable (INT4 i4IfIndex,
                                                  UINT4
                                                  u4CapwapDot11WlanProfileId)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4CapwapDot11WlanProfileId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot11StationConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot11StationConfigTable (INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot11CapabilityProfileTable
 Input       :  The Indices
                FsDot11CapabilityProfileName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot11CapabilityProfileTable (tSNMP_OCTET_STRING_TYPE
                                                         *
                                                         pFsDot11CapabilityProfileName)
{
    UNUSED_PARAM (pFsDot11CapabilityProfileName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot11AuthenticationProfileTable
 Input       :  The Indices
                FsDot11AuthenticationProfileName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot11AuthenticationProfileTable
    (tSNMP_OCTET_STRING_TYPE * pFsDot11AuthenticationProfileName)
{
    UNUSED_PARAM (pFsDot11AuthenticationProfileName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsSecurityWebAuthGuestInfoTable
 Input       :  The Indices
                FsSecurityWebAuthUName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsSecurityWebAuthGuestInfoTable (tSNMP_OCTET_STRING_TYPE
                                                         *
                                                         pFsSecurityWebAuthUName)
{
    UNUSED_PARAM (pFsSecurityWebAuthUName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsStationQosParamsTable
 Input       :  The Indices
                IfIndex
                FsStaMacAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsStationQosParamsTable (INT4 i4IfIndex,
                                                 tMacAddr FsStaMacAddr)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (FsStaMacAddr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVlanIsolationTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVlanIsolationTable (INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot11RadioConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot11RadioConfigTable (INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot11QosProfileTable
 Input       :  The Indices
                FsDot11QosProfileName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot11QosProfileTable (tSNMP_OCTET_STRING_TYPE *
                                                pFsDot11QosProfileName)
{
    UNUSED_PARAM (pFsDot11QosProfileName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot11WlanCapabilityProfileTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot11WlanCapabilityProfileTable (INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot11WlanAuthenticationProfileTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot11WlanAuthenticationProfileTable (INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot11WlanQosProfileTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot11WlanQosProfileTable (INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot11RadioQosTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot11RadioQosTable (INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot11QAPTable
 Input       :  The Indices
                IfIndex
                Dot11EDCATableIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot11QAPTable (INT4 i4IfIndex,
                                         INT4 i4Dot11EDCATableIndex)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4Dot11EDCATableIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsQAPProfileTable
 Input       :  The Indices
                FsQAPProfileName
                FsQAPProfileIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsQAPProfileTable (tSNMP_OCTET_STRING_TYPE *
                                           pFsQAPProfileName,
                                           INT4 i4FsQAPProfileIndex)
{
    UNUSED_PARAM (pFsQAPProfileName);
    UNUSED_PARAM (i4FsQAPProfileIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot11CapabilityMappingTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot11CapabilityMappingTable (INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot11AuthMappingTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot11AuthMappingTable (INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot11QosMappingTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot11QosMappingTable (INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot11ClientSummaryTable
 Input       :  The Indices
                FsDot11ClientMacAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot11ClientSummaryTable (tMacAddr
                                                   FsDot11ClientMacAddr)
{
    UNUSED_PARAM (FsDot11ClientMacAddr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot11AntennasListTable
 Input       :  The Indices
                IfIndex
                Dot11AntennaListIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot11AntennasListTable (INT4 i4IfIndex,
                                                  INT4 i4Dot11AntennaListIndex)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4Dot11AntennaListIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot11WlanTable
 Input       :  The Indices
                CapwapDot11WlanProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot11WlanTable (UINT4 u4CapwapDot11WlanProfileId)
{
    UNUSED_PARAM (u4CapwapDot11WlanProfileId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot11WlanBindTable
 Input       :  The Indices
                IfIndex
                CapwapDot11WlanProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot11WlanBindTable (INT4 i4IfIndex,
                                              UINT4 u4CapwapDot11WlanProfileId)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4CapwapDot11WlanProfileId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot11nConfigTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot11nConfigTable (INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot11nMCSDataRateTable
 Input       :  The Indices
                IfIndex
                FsDot11nMCSDataRateIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot11nMCSDataRateTable (INT4 i4IfIndex,
                                                  INT4
                                                  i4FsDot11nMCSDataRateIndex)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4FsDot11nMCSDataRateIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWtpImageUpgradeTable
 Input       :  The Indices
                CapwapBaseWtpProfileWtpModelNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWtpImageUpgradeTable (tSNMP_OCTET_STRING_TYPE *
                                                pCapwapBaseWtpProfileWtpModelNumber)
{
    UNUSED_PARAM (pCapwapBaseWtpProfileWtpModelNumber);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWlanSSIDStatsTable
 Input       :  The Indices
                FsDot11WlanProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWlanSSIDStatsTable (UINT4 u4FsDot11WlanProfileId)
{
    UNUSED_PARAM (u4FsDot11WlanProfileId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWlanClientStatsTable
 Input       :  The Indices
                FsWlanClientStatsMACAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWlanClientStatsTable (tMacAddr
                                                FsWlanClientStatsMACAddress)
{
    UNUSED_PARAM (FsWlanClientStatsMACAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsWlanRadioTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsWlanRadioTable (INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;
}

#ifndef RFMGMT_WANTED
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRrmConfigTable
 Input       :  The Indices
                FsRrmRadioType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRrmConfigTable (INT4 i4FsRrmRadioType)
{
    UNUSED_PARAM (i4FsRrmRadioType);
    return SNMP_SUCCESS;
}
#endif
