/***************************************************************************** 
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved 
 * 
 * $Id: wsscfgtrc.c,v 1.1.1.1 2016/09/20 10:42:40 siva Exp $ 
 * 
 * Description: This file contains tracing utility functions for wsscfg module
 * 
 *****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "wsscfginc.h"

/****************************************************************************
*                                                                           *
* Function     : WsscfgTrcPrint                                               *
*                                                                           *
* Description  :  prints the trace - with filename and line no              *
*                                                                           *
* Input        : fname   - File name                                        *
*                u4Line  - Line no                                          *
*                s       - strong to be printed                             *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

VOID 
WsscfgTrcPrint( const char *fname, UINT4 u4Line, const char *s)
{

    tOsixSysTime sysTime = 0;
    const char *pc1Fname = fname;

    if (s == NULL) {
        return;
    }
    while (*fname != '\0') {
        if (*fname == '/')
            pc1Fname = (fname + 1);
        fname++;
    }
    OsixGetSysTime(&sysTime);
    printf("WSSCFG: %d:%s:%d:   %s", sysTime, pc1Fname, u4Line, s);
}

/****************************************************************************
*                                                                           *
* Function     : WsscfgTrcWrite                                               *
*                                                                           *
* Description  :  prints the trace - without filename and line no ,         *
*                 Useful for dumping packets                                *
*                                                                           *
* Input        : s - string to be printed                                   *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

VOID 
WsscfgTrcWrite( CHR1 * s)
{
    if (s != NULL) {
        printf("%s", s);
    }
}

/****************************************************************************
*                                                                           *
* Function     : WsscfgTrc                                                    *
*                                                                           *
* Description  : converts variable argument in to string depending on flag  *
*                                                                           *
* Input        : u4Flags  - Trace flag                                      *
*                fmt  - format strong, variable argument
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

CHR1 *
WsscfgTrc( UINT4 u4Flags, const char *fmt, ...)
{
    va_list ap;
#define WSSCFG_TRC_BUF_SIZE    2000
    static CHR1 buf[WSSCFG_TRC_BUF_SIZE];

    if ((u4Flags & WSSCFG_TRC_FLAG) > 0) {
        return (NULL);
    }
    va_start(ap, fmt);
    vsprintf(&buf[0], fmt, ap);
    va_end(ap);

    return (&buf[0]);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  wsscfgtrc.c                      */
/*-----------------------------------------------------------------------*/
