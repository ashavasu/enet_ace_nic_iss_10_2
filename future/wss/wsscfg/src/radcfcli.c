/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * 
 *  $Id: radcfcli.c,v 1.2 2017/11/24 10:37:08 siva Exp $
 * 
 *  Description: This file contains the routines for radius
 *          related cli commands.
 * 
 * ********************************************************************/
#ifdef WSSCUST_WANTED
#include "wsscfginc.h"
#include "wsscfgcli.h"
#include "capwapcli.h"
#include "wsscfgprot.h"
#include "wsscfgextn.h"

/****************************************************************************
 *Function    :  cli_process_fswsslr_cmd
 *Description :  This function is exported to CLI module to handle the
 *                     fs11ac cli commands to take the corresponding action.
 *
 *Input       :  Variable arguments
 *
 *Output      :  None
 *
 *Returns     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/

INT4
cli_process_sqlcfg_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[WSSCFG_CLI_MAX_ARGS];
    INT1                i1RetStatus = CLI_SUCCESS;
    INT1                i1argno = 0;
    UINT4               u4IfIndex;
    INT4                i4Inst = 0;
    UINT4               u4ErrCode;
    CliRegisterLock (CliHandle, WsscfgMainTaskLock, WsscfgMainTaskUnLock);
    WSSCFG_LOCK;

    va_start (ap, u4Command);

    /* Walk through the rest of the arguements and store in args array.
     *      * Store RFMGMT_CLI_MAX_ARGS arguements at the max.
     *           */

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }

    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == WSSCFG_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    switch (u4Command)
    {
        case CLI_SQL_CONFIGURE:
            MEMCPY (au1DatabaseName, args[0], sizeof (au1DatabaseName));
            i1RetStatus = FsWssSqlCfg (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_START_EMBEDDED_RADIUS:
            i1RetStatus = WssStartRadiusServer ();
            break;

        case CLI_CREATE_DATABASE:
            i1RetStatus = WssCreateDatabase ((UINT1 *) args[0]);
            break;

        case CLI_DELETE_DATABASE:
            i1RetStatus = WssDeleteDatabase ((UINT1 *) args[0]);
            break;

        case CLI_SHOW_DATABASES:
            i1RetStatus = WssShowDatabases ();
            break;

        case CLI_SHOW_TABLES:
            i1RetStatus = WssShowTables ((UINT1 *) args[0]);
            break;

        case CLI_RADCHECK_INSERT:
            i1RetStatus =
                WssInsertRadCheck ((UINT1 *) args[0], (UINT1 *) args[1],
                                   (UINT1 *) args[2]);
            break;

        case CLI_RADREPLY_SET_VOLUME:
            i1RetStatus =
                WssSetVolume ((UINT1 *) args[0], (UINT1 *) args[1],
                              CLI_PTR_TO_U4 (*args[2]));
            break;

        case CLI_RADREPLY_SET_TIMEOUT:
            i1RetStatus =
                WssSetTimeout ((UINT1 *) args[0], (UINT1 *) args[1],
                               CLI_PTR_TO_U4 (*args[2]));
            break;

        case CLI_RADREPLY_SET_BANDWIDH:
            i1RetStatus =
                WssSetBandwidth ((UINT1 *) args[0], (UINT1 *) args[1],
                                 CLI_PTR_TO_U4 (*args[2]));
            break;

        case CLI_RADREPLY_SET_DOWNLOAD_BANDWIDTH:
            i1RetStatus =
                WssSetDownloadBandwidth ((UINT1 *) args[0], (UINT1 *) args[1],
                                         CLI_PTR_TO_U4 (*args[2]));
            break;

        case CLI_RADREPLY_SET_UPLOAD_BANDWIDTH:
            i1RetStatus =
                WssSetUploadbandwidth ((UINT1 *) args[0], (UINT1 *) args[1],
                                       CLI_PTR_TO_U4 (*args[2]));
            break;

        case CLI_UPDATE_RADCHECK:
            /* if ((UINT1 *) args[0] == NULL && (UINT1 *) args[1] == NULL)
               {
               WssInvalidInput (CliHandle,0);
               } */
            i1RetStatus =
                WssUpdateRadCheck ((UINT1 *) args[0], (UINT1 *) args[1],
                                   (UINT1 *) args[2]);
            break;

        case CLI_RADREPLY_UPDATE_VOLUME:
            if ((UINT1 *) args[0] == NULL && (UINT1 *) args[1] == NULL)
            {
                WssInvalidInput (CliHandle, 0);
            }
            i1RetStatus =
                WssUpdateVolume ((UINT1 *) args[0], (UINT1 *) args[1],
                                 CLI_PTR_TO_U4 (*args[2]));
            break;

        case CLI_RADREPLY_UPDATE_TIMEOUT:
            if ((UINT1 *) args[0] == NULL && (UINT1 *) args[1] == NULL)
            {
                WssInvalidInput (CliHandle, 0);
            }
            i1RetStatus =
                WssUpdateTimeout ((UINT1 *) args[0], (UINT1 *) args[1],
                                  CLI_PTR_TO_U4 (*args[2]));
            break;

        case CLI_RADREPLY_UPDATE_BANDWIDTH:
            if ((UINT1 *) args[0] == NULL && (UINT1 *) args[1] == NULL)
            {
                WssInvalidInput (CliHandle, 0);
            }
            i1RetStatus =
                WssUpdateBandwidth ((UINT1 *) args[0], (UINT1 *) args[1],
                                    CLI_PTR_TO_U4 (*args[2]));
            break;

        case CLI_RADREPLY_UPDATE_UPLOAD_BANDWIDTH:
            if ((UINT1 *) args[0] == NULL && (UINT1 *) args[1] == NULL)
            {
                WssInvalidInput (CliHandle, 0);
            }
            i1RetStatus =
                WssUpdateUploadBandwidth ((UINT1 *) args[0], (UINT1 *) args[1],
                                          CLI_PTR_TO_U4 (*args[2]));
            break;

        case CLI_RADREPLY_UPDATE_DOWNLOAD_BANDWIDTH:
            if ((UINT1 *) args[0] == NULL && (UINT1 *) args[1] == NULL)
            {
                WssInvalidInput (CliHandle, 0);
            }
            i1RetStatus =
                WssUpdateDownloadBandwidth ((UINT1 *) args[0],
                                            (UINT1 *) args[1],
                                            CLI_PTR_TO_U4 (*args[2]));
            break;

        case CLI_RADREPLY_INVALID_INPUT:
            i1RetStatus = WssInvalidInput (CliHandle, *args[0]);
            break;

        case CLI_RADREPLY_DELETE:
            if ((UINT1 *) args[0] == NULL && (UINT1 *) args[1] == NULL
                && (UINT1 *) args[2] == NULL)
            {
                CliPrintf (CliHandle, "%s",
                           "\nWarnig:Either one of user name, SSID or Attribute required\n\n");
            }
            i1RetStatus =
                WssDeleteRadReply ((UINT1 *) args[0], (UINT1 *) args[1],
                                   (UINT1 *) args[2]);
            break;

        case CLI_RADCHECK_DELETE:
            i1RetStatus =
                WssDeleteRadCheck ((UINT1 *) args[0], (UINT1 *) args[1],
                                   (UINT1 *) args[2]);
            break;

        case CLI_RADACCT_DELETE:
            i1RetStatus = WssDeleteRadAcct ();
            break;

        case CLI_EXPORT_DATABASE:
            i1RetStatus = WssExportDatabase ();
            break;

        case CLI_IMPORT_DATABASE:
            i1RetStatus = WssImportDatabase ();
            break;

        case CLI_SHOW_RADREPLY:
            i1RetStatus = WssShowRadreply ((UINT1 *) args[0]);
            break;

        case CLI_SHOW_RADCHECK:
            i1RetStatus = WssShowRadcheck ((UINT1 *) args[0]);
            break;

        case CLI_SHOW_RADACCT:
            i1RetStatus = WssShowRadAcct ((UINT1 *) args[0]);
            break;

        case CLI_DESCRIBE_RADCHECK:
            i1RetStatus = WssDescribeRadCheckAttributes ((UINT1 *) args[0]);
            break;

        case CLI_DESCRIBE_RADREPLY:
            i1RetStatus = WssDescribeRadReplyAttributes ((UINT1 *) args[0]);
            break;

        case CLI_DESCRIBE_RADACCT:
            i1RetStatus = WssDescribeRadAcctAttributes ((UINT1 *) args[0]);
            break;

        case CLI_RADGROUPREPLY_SET_VOLUME:
            i1RetStatus =
                WssGroupSetVolume ((UINT1 *) args[0], (UINT1 *) args[1],
                                   CLI_PTR_TO_U4 (*args[2]));
            break;

        case CLI_RADGROUPREPLY_SET_TIMEOUT:
            i1RetStatus =
                WssGroupSetTimeout ((UINT1 *) args[0], (UINT1 *) args[1],
                                    CLI_PTR_TO_U4 (*args[2]));
            break;

        case CLI_RADGROUPREPLY_SET_BANDWIDH:
            i1RetStatus =
                WssGroupSetBandwidth ((UINT1 *) args[0], (UINT1 *) args[1],
                                      CLI_PTR_TO_U4 (*args[2]));
            break;

        case CLI_RADGROUPREPLY_SET_DOWNLOAD_BANDWIDTH:
            i1RetStatus =
                WssGroupSetDownloadBandwidth ((UINT1 *) args[0],
                                              (UINT1 *) args[1],
                                              CLI_PTR_TO_U4 (*args[2]));
            break;

        case CLI_RADGROUPREPLY_SET_UPLOAD_BANDWIDTH:
            i1RetStatus =
                WssGroupSetUploadbandwidth ((UINT1 *) args[0],
                                            (UINT1 *) args[1],
                                            CLI_PTR_TO_U4 (*args[2]));
            break;

        case CLI_RADGROUPREPLY_UPDATE_VOLUME:
            if ((UINT1 *) args[0] == NULL && (UINT1 *) args[1] == NULL)
            {
                WssInvalidGroupInput (CliHandle, 0);
            }
            i1RetStatus =
                WssGroupUpdateVolume ((UINT1 *) args[0], (UINT1 *) args[1],
                                      CLI_PTR_TO_U4 (*args[2]));
            break;

        case CLI_RADGROUPREPLY_UPDATE_TIMEOUT:
            if ((UINT1 *) args[0] == NULL && (UINT1 *) args[1] == NULL)
            {
                WssInvalidGroupInput (CliHandle, 0);
            }
            i1RetStatus =
                WssGroupUpdateTimeout ((UINT1 *) args[0], (UINT1 *) args[1],
                                       CLI_PTR_TO_U4 (*args[2]));
            break;

        case CLI_RADGROUPREPLY_UPDATE_BANDWIDTH:
            if ((UINT1 *) args[0] == NULL && (UINT1 *) args[1] == NULL)
            {
                WssInvalidGroupInput (CliHandle, 0);
            }
            i1RetStatus =
                WssGroupUpdateBandwidth ((UINT1 *) args[0], (UINT1 *) args[1],
                                         CLI_PTR_TO_U4 (*args[2]));
            break;

        case CLI_RADGROUPREPLY_UPDATE_UPLOAD_BANDWIDTH:
            if ((UINT1 *) args[0] == NULL && (UINT1 *) args[1] == NULL)
            {
                WssInvalidGroupInput (CliHandle, 0);
            }
            i1RetStatus =
                WssGroupUpdateUploadBandwidth ((UINT1 *) args[0],
                                               (UINT1 *) args[1],
                                               CLI_PTR_TO_U4 (*args[2]));
            break;

        case CLI_RADGROUPREPLY_UPDATE_DOWNLOAD_BANDWIDTH:
            if ((UINT1 *) args[0] == NULL && (UINT1 *) args[1] == NULL)
            {
                WssInvalidGroupInput (CliHandle, 0);
            }
            i1RetStatus =
                WssGroupUpdateDownloadBandwidth ((UINT1 *) args[0],
                                                 (UINT1 *) args[1],
                                                 CLI_PTR_TO_U4 (*args[2]));
            break;

        case CLI_RADUSERGROP_INSERT_USER:
            i1RetStatus =
                WssGroupInsertRadUserGroup ((UINT1 *) args[0],
                                            (UINT1 *) args[1],
                                            (UINT1 *) args[2],
                                            CLI_PTR_TO_U4 (*args[3]));
            break;

        case CLI_RADUSERGROP_DELETE_USER:
            i1RetStatus =
                WssDeleteUserFromRadGroupReply ((UINT1 *) args[0],
                                                (UINT1 *) args[1],
                                                (UINT1 *) args[2]);
            break;

        case CLI_RADGROUPREPLY_DELETE:
            if ((UINT1 *) args[0] == NULL && (UINT1 *) args[1] == NULL
                && (UINT1 *) args[2] == NULL)
            {
                CliPrintf (CliHandle, "%s",
                           "\nWarnig: Provide group name and  SSID or all \n\n");
            }
            i1RetStatus =
                WssDeleteRadGroupReply ((UINT1 *) args[0], (UINT1 *) args[1],
                                        (UINT1 *) args[2]);
            break;

        case CLI_SHOW_RADGROUPREPLY:
            i1RetStatus = WssShowRadGroupreply ((UINT1 *) args[0]);
            break;

        case CLI_SHOW_RADUSERGROUP:
            i1RetStatus = WssShowRadUserGroup ((UINT1 *) args[0]);
            break;

        default:
            break;
    }
    if ((i1RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_WSSCFG_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", WsscfgCliErrString[u4ErrCode]);
            CLI_FATAL_ERROR (CliHandle);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i1RetStatus);

    CliUnRegisterLock (CliHandle);
    WSSCFG_UNLOCK;
    UNUSED_PARAM (u4IfIndex);
    return i1RetStatus;
}

/****************************************************************************
 *Function    :  FsWssSqlCfg                                                
 *Description :  This function is to change the configure prompt.           
 *                                                        
 *Input       :  CliHandle,                             
 *         pu1Prompt                                
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS/OSIX_FAILURE                     
 ****************************************************************************/
UINT1
FsWssSqlCfg (tCliHandle CliHandle, UINT1 *pu1Prompt)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    tSNMP_OCTET_STRING_TYPE DatabaseName;

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    MEMSET (&DatabaseName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    DatabaseName.i4_Length = (INT4) (STRLEN (pu1Prompt));

    DatabaseName.pu1_OctetList = pu1Prompt;

    SPRINTF ((CHR1 *) au1Cmd, "%s%s", SQL_CLI_CONFIG_MODE, pu1Prompt);
#ifdef CLI_WANTED
    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "Unable to enter into radius config mode\r\n");
        return OSIX_FAILURE;
    }
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssStartRadiusServer
 *Description :  This function is used to start the radius server           
 *                                                        
 *Input       :  NONE
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssStartRadiusServer (VOID)
{

    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s",
              "service mysqld start");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssCreateDatabase
 *Description :  This function is used to create a new database           
 *                                                        
 *Input       :  pu1DatabaseName
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/

INT1
WssCreateDatabase (UINT1 *pu1Database)
{

    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s",
              "mysql -s -t -e ", "\"create database ", pu1Database, ";\"");

    system (ac1Command);

    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s", "mysql ",
              pu1Database, "< /usr/local/etc/raddb/sql/mysql/schema.sql");
    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssDeleteDatabase
 *Description :  This function is used to delete an existing database           
 *                                                        
 *Input       :  pu1DatabaseName
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/

INT1
WssDeleteDatabase (UINT1 *pu1Database)
{

    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s",
              "mysql -s -t -e ", "\"drop database ", pu1Database, ";\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssShowDatabases 
 *Description :  This function is used to list the available database           
 *                                                        
 *Input       :  NONE
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/

INT1
WssShowDatabases (VOID)
{

    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s",
              "mysql -s -t -e ", "\"show databases;\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssShowTables 
 *Description :  This function is used to list the tables in the database           
 *                                                        
 *Input       :  pu1DatabaseName
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/

INT1
WssShowTables (UINT1 *pu1DatabaseName)
{

    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s",
              "mysql -s -t -e ", "\"use ", pu1DatabaseName, "; show tables;\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssInsertRadCheck
 *Description :  This function is for inserting username, 
 *         SSID and password into Radcheck table           
 *                                                        
 *Input       :  pu1UserName,
 *               pu1SSID,                             
 *         pu1Password                                
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssInsertRadCheck (UINT1 *pu1UserName, UINT1 *pu1SSID, UINT1 *pu1Password)
{
    UINT1               au1Attribute[ATTRIBUTE_SIZE] = PASSWORD;
    UINT1               au1Op[OPERAND_SIZE] = OPERAND;

    /* Delete the duplicate entry before inserting with alreday existing same username and ssid */

    WssDeleteRadCheck (pu1UserName, pu1SSID, au1Attribute);

    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
              "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s", "mysql -s -t -e ",
              "\"use ", au1DatabaseName,
              "; insert into radcheck (username,ssid,attribute,op,value) values (",
              "'", pu1UserName, "',", "'", pu1SSID, "',", "'", au1Attribute,
              "',", "'", au1Op, "',", "'", pu1Password, "'", ");\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssSetVolume
 *Description :  This function is used to insert 
 *          volume into Radreply table           
 *                                                        
 *Input       :  pu1UserName,
 *               pu1SSID,                             
 *         u4Volume                                
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssSetVolume (UINT1 *pu1UserName, UINT1 *pu1SSID, UINT4 u4Volume)
{
    UINT1               au1Attribute[ATTRIBUTE_SIZE] = VOLUME;
    UINT1               au1Op[OPERAND_SIZE] = OPERAND;

    /* Delete the duplicate entry before inserting with alreday existing same username and ssid */

    WssDeleteRadReply (pu1UserName, pu1SSID, au1Attribute);

    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
              "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%d%s%s", "mysql -s -t -e ",
              "\"use ", au1DatabaseName,
              "; insert into radreply (username,ssid,attribute,op,value) values (",
              "'", pu1UserName, "',", "'", pu1SSID, "',", "'", au1Attribute,
              "',", "'", au1Op, "',", "'", u4Volume, "'", ");\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssSetTimeout
 *Description :  This function is used to insert 
 *          Timeout value into Radreply table           
 *                                                        
 *Input       :  pu1UserName,
 *               pu1SSID,                             
 *         u4Timeout                                
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssSetTimeout (UINT1 *pu1UserName, UINT1 *pu1SSID, UINT4 u4Timeout)
{
    UINT1               au1Attribute[ATTRIBUTE_SIZE] = TIMEOUT;
    UINT1               au1Op[OPERAND_SIZE] = OPERAND;

    /* Delete the duplicate entry before inserting with alreday existing same username and ssid */

    WssDeleteRadReply (pu1UserName, pu1SSID, au1Attribute);

    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
              "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%d%s%s", "mysql -s -t -e ",
              "\"use ", au1DatabaseName,
              "; insert into radreply (username,ssid,attribute,op,value) values (",
              "'", pu1UserName, "',", "'", pu1SSID, "',", "'", au1Attribute,
              "',", "'", au1Op, "',", "'", u4Timeout, "'", ");\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssSetBandwidth
 *Description :  This function is used to insert 
 *          Bandwidth value into Radreply table           
 *                                                        
 *Input       :  pu1UserName,
 *               pu1SSID,                             
 *         u4Bandwidth                                
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssSetBandwidth (UINT1 *pu1UserName, UINT1 *pu1SSID, UINT4 u4Bandwidth)
{
    UINT1               au1Attribute[ATTRIBUTE_SIZE] = BANDWIDTH;
    UINT1               au1Op[OPERAND_SIZE] = OPERAND;

    /* Delete the duplicate entry before inserting with alreday existing same username and ssid */

    WssDeleteRadReply (pu1UserName, pu1SSID, au1Attribute);

    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
              "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%d%s%s", "mysql -s -t -e ",
              "\"use ", au1DatabaseName,
              "; insert into radreply (username,ssid,attribute,op,value) values (",
              "'", pu1UserName, "',", "'", pu1SSID, "',", "'", au1Attribute,
              "',", "'", au1Op, "',", "'", u4Bandwidth, "'", ");\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssSetDownloadBandwidth
 *Description :  This function is used to insert 
 *          Download bandwidth value into Radreply table           
 *                                                        
 *Input       :  pu1UserName,
 *               pu1SSID,                             
 *         u4DLBandwidth                                
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssSetDownloadBandwidth (UINT1 *pu1UserName, UINT1 *pu1SSID,
                         UINT4 u4DLBandwidth)
{
    UINT1               au1Attribute[ATTRIBUTE_SIZE] = DOWNLOAD_BANDWIDTH;
    UINT1               au1Op[OPERAND_SIZE] = OPERAND;

    /* Delete the duplicate entry before inserting with alreday existing same username and ssid */

    WssDeleteRadReply (pu1UserName, pu1SSID, au1Attribute);
    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
              "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%d%s%s", "mysql -s -t -e ",
              "\"use ", au1DatabaseName,
              "; insert into radreply (username,ssid,attribute,op,value) values (",
              "'", pu1UserName, "',", "'", pu1SSID, "',", "'", au1Attribute,
              "',", "'", au1Op, "',", "'", u4DLBandwidth, "'", ");\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssSetUploadbandwidth
 *Description :  This function is used to insert 
 *          Upload bandwidth value into Radreply table           
 *                                                        
 *Input       :  pu1UserName,
 *               pu1SSID,                             
 *         u4ULBandwidth                                
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssSetUploadbandwidth (UINT1 *pu1UserName, UINT1 *pu1SSID, UINT4 u4ULBandwidth)
{
    UINT1               au1Attribute[ATTRIBUTE_SIZE] = UPLOAD_BANDWIDTH;
    UINT1               au1Op[OPERAND_SIZE] = OPERAND;

    /* Delete the duplicate entry before inserting with alreday existing same username and ssid */

    WssDeleteRadReply (pu1UserName, pu1SSID, au1Attribute);

    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
              "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%d%s%s", "mysql -s -t -e ",
              "\"use ", au1DatabaseName,
              "; insert into radreply (username,ssid,attribute,op,value) values (",
              "'", pu1UserName, "',", "'", pu1SSID, "',", "'", au1Attribute,
              "',", "'", au1Op, "',", "'", u4ULBandwidth, "'", ");\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssUpdateRadCheck
 *Description :  This function is used to update 
 *          password from Radcheck table           
 *                                                        
 *Input       :  pu1UserName,
 *               pu1SSID,                             
 *         pu1Password       
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssUpdateRadCheck (UINT1 *pu1UserName, UINT1 *pu1SSID, UINT1 *pu1Password)
{
    if (pu1UserName != NULL && pu1SSID != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                  "%s%s%s%s%s%s%s%s%s%s", "mysql -s -t -e ", "\"use ",
                  au1DatabaseName, ";update radcheck set value=\'", pu1Password,
                  "\' where attribute='Cleartext-Password' and username=\'",
                  pu1UserName, "\' and ssid=\'", pu1SSID, "\';\"");

        system (ac1Command);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssUpdateVolume
 *Description :  This function is used to update 
 *          volume value from Radreply table           
 *                                                        
 *Input       :  pu1UserName,
 *               pu1SSID,                             
 *         u4Volume       
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssUpdateVolume (UINT1 *pu1UserName, UINT1 *pu1SSID, UINT4 u4Volume)
{

    if (pu1UserName == NULL && pu1SSID != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%d%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";update radreply set value=\'", u4Volume,
                  "\' where attribute='Volume' and ssid=\'", pu1SSID, "\';\"");

        system (ac1Command);
    }

    if (pu1UserName != NULL && pu1SSID == NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%d%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";update radreply set value=\'", u4Volume,
                  "\' where attribute='Volume' and username=\'", pu1UserName,
                  "\';\"");

        system (ac1Command);
    }

    if (pu1UserName != NULL && pu1SSID != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                  "%s%s%s%s%d%s%s%s%s%s", "mysql -s -t -e ", "\"use ",
                  au1DatabaseName, ";update radreply set value=\'", u4Volume,
                  "\' where attribute='Volume' and username=\'", pu1UserName,
                  "\' and ssid=\'", pu1SSID, "\';\"");

        system (ac1Command);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssUpdateTimeout
 *Description :  This function is used to update 
 *          timeout value from Radreply table           
 *                                                        
 *Input       :  pu1UserName,
 *               pu1SSID,                             
 *         u4Timeout       
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssUpdateTimeout (UINT1 *pu1UserName, UINT1 *pu1SSID, UINT4 u4Timeout)
{

    if (pu1UserName == NULL && pu1SSID != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%d%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";update radreply set value=\'", u4Timeout,
                  "\' where attribute='Timeout' and ssid=\'", pu1SSID, "\';\"");

        system (ac1Command);
    }
    if (pu1UserName != NULL && pu1SSID == NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%d%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";update radreply set value=\'", u4Timeout,
                  "\' where attribute='Timeout' and username=\'", pu1UserName,
                  "\';\"");

        system (ac1Command);
    }

    if (pu1UserName != NULL && pu1SSID != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                  "%s%s%s%s%d%s%s%s%s%s", "mysql -s -t -e ", "\"use ",
                  au1DatabaseName, ";update radreply set value=\'", u4Timeout,
                  "\' where attribute='Timeout' and username=\'", pu1UserName,
                  "\' and ssid=\'", pu1SSID, "\';\"");

        system (ac1Command);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssUpdateBandwidth
 *Description :  This function is used to update 
 *          bandwidth value from Radreply table           
 *                                                        
 *Input       :  pu1UserName,
 *               pu1SSID,                             
 *         u4Bandwidth       
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssUpdateBandwidth (UINT1 *pu1UserName, UINT1 *pu1SSID, UINT4 u4Bandwidth)
{

    if (pu1UserName == NULL && pu1SSID != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%d%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";update radreply set value=\'", u4Bandwidth,
                  "\' where attribute='Bandwidth' and ssid=\'", pu1SSID,
                  "\';\"");

        system (ac1Command);
    }

    if (pu1UserName != NULL && pu1SSID == NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%d%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";update radreply set value=\'", u4Bandwidth,
                  "\' where attribute='Bandwidth' and username=\'", pu1UserName,
                  "\';\"");

        system (ac1Command);
    }

    if (pu1UserName != NULL && pu1SSID != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                  "%s%s%s%s%d%s%s%s%s%s", "mysql -s -t -e ", "\"use ",
                  au1DatabaseName, ";update radreply set value=\'", u4Bandwidth,
                  "\' where attribute='Bandwidth' and username=\'", pu1UserName,
                  "\' and ssid=\'", pu1SSID, "\';\"");

        system (ac1Command);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssUpdateUploadBandwidth
 *Description :  This function is used to update 
 *          upload bandwidth value from Radreply table           
 *                                                        
 *Input       :  pu1UserName,
 *               pu1SSID,                             
 *         u4ULBandwidth       
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssUpdateUploadBandwidth (UINT1 *pu1UserName, UINT1 *pu1SSID,
                          UINT4 u4ULBandwidth)
{

    if (pu1UserName == NULL && pu1SSID != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%d%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";update radreply set value=\'", u4ULBandwidth,
                  "\' where attribute='ULBandwidth' and ssid=\'", pu1SSID,
                  "\';\"");

        system (ac1Command);
    }

    if (pu1UserName != NULL && pu1SSID == NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%d%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";update radreply set value=\'", u4ULBandwidth,
                  "\' where attribute='ULBandwidth' and username=\'",
                  pu1UserName, "\';\"");

        system (ac1Command);
    }

    if (pu1UserName != NULL && pu1SSID != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                  "%s%s%s%s%d%s%s%s%s%s", "mysql -s -t -e ", "\"use ",
                  au1DatabaseName, ";update radreply set value=\'",
                  u4ULBandwidth,
                  "\' where attribute='ULBandwidth' and username=\'",
                  pu1UserName, "\' and ssid=\'", pu1SSID, "\';\"");

        system (ac1Command);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssUpdateDownloadBandwidth
 *Description :  This function is used to update 
 *          download bandwidth value from Radreply table           
 *                                                        
 *Input       :  pu1UserName,
 *               pu1SSID,                             
 *         u4DLBandwidth       
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssUpdateDownloadBandwidth (UINT1 *pu1UserName, UINT1 *pu1SSID,
                            UINT4 u4DLBandwidth)
{

    if (pu1UserName == NULL && pu1SSID != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%d%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";update radreply set value=\'", u4DLBandwidth,
                  "\' where attribute='DLBandwidth' and ssid=\'", pu1SSID,
                  "\';\"");

        system (ac1Command);
    }

    if (pu1UserName != NULL && pu1SSID == NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%d%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";update radreply set value=\'", u4DLBandwidth,
                  "\' where attribute='DLBandwidth' and username=\'",
                  pu1UserName, "\';\"");

        system (ac1Command);
    }

    if (pu1UserName != NULL && pu1SSID != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                  "%s%s%s%s%d%s%s%s%s%s", "mysql -s -t -e ", "\"use ",
                  au1DatabaseName, ";update radreply set value=\'",
                  u4DLBandwidth,
                  "\' where attribute='DLBandwidth' and username=\'",
                  pu1UserName, "\' and ssid=\'", pu1SSID, "\';\"");

        system (ac1Command);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssInvalidInput
 *Description :  This function is used to handle 
 *          invalid CLI command           
 *                                                        
 *Input       :  CliHandle,
 *               u4Invalid
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssInvalidInput (tCliHandle CliHandle, UINT4 u4Invalid)
{
    UNUSED_PARAM (u4Invalid);
    CliPrintf (CliHandle, "\nWarning:Either of user name or ssid required\n\n");

    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssDeleteRadReply
 *Description :  This function is used to delete 
 *          entry from Radreply table           
 *                                                        
 *Input       :  pu1UserName,
 *               pu1SSID,
 *               pu1Attribute
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssDeleteRadReply (UINT1 *pu1UserName, UINT1 *pu1SSID, UINT1 *pu1Attribute)
{

    if (pu1UserName == NULL && pu1SSID == NULL && pu1Attribute != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";delete from radreply where attribute=\'", pu1Attribute,
                  "\';\"");

        system (ac1Command);
    }

    if (pu1UserName == NULL && pu1SSID != NULL && pu1Attribute == NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";delete from radreply where ssid=\'", pu1SSID, "\';\"");

        system (ac1Command);
    }

    if (pu1UserName == NULL && pu1SSID != NULL && pu1Attribute != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%s%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";delete from radreply where attribute=\'", pu1Attribute,
                  "\' and ssid=\'", pu1SSID, "\';\"");

        system (ac1Command);
    }

    if (pu1UserName != NULL && pu1SSID == NULL && pu1Attribute == NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";delete from radreply where username=\'", pu1UserName,
                  "\';\"");

        system (ac1Command);
    }

    if (pu1UserName != NULL && pu1SSID == NULL && pu1Attribute != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%s%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";delete from radreply where username=\'", pu1UserName,
                  "\' and attribute=\'", pu1Attribute, "\';\"");

        system (ac1Command);
    }

    if (pu1UserName != NULL && pu1SSID != NULL && pu1Attribute == NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%s%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";delete from radreply where username=\'", pu1UserName,
                  "\' and ssid=\'", pu1SSID, "\';\"");

        system (ac1Command);
    }

    if (pu1UserName != NULL && pu1SSID != NULL && pu1Attribute != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                  "%s%s%s%s%s%s%s%s%s%s", "mysql -s -t -e ", "\"use ",
                  au1DatabaseName, ";delete from radreply where username=\'",
                  pu1UserName, "\' and ssid=\'", pu1SSID, "\' and attribute=\'",
                  pu1Attribute, "\';\"");

        system (ac1Command);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssDeleteRadCheck
 *Description :  This function is used to delete 
 *          entry from Radcheck table           
 *                                                        
 *Input       :  pu1UserName,
 *               pu1SSID,
 *               pu1Attribute
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssDeleteRadCheck (UINT1 *pu1UserName, UINT1 *pu1SSID, UINT1 *pu1Attribute)
{

    if (pu1UserName == NULL && pu1SSID == NULL && pu1Attribute != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";delete from radcheck where attribute=\'", pu1Attribute,
                  "\';\"");

        system (ac1Command);
    }

    if (pu1UserName == NULL && pu1SSID != NULL && pu1Attribute == NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";delete from radcheck where ssid=\'", pu1SSID, "\';\"");

        system (ac1Command);
    }

    if (pu1UserName == NULL && pu1SSID != NULL && pu1Attribute != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%s%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";delete from radcheck where attribute=\'", pu1Attribute,
                  "\' and ssid=\'", pu1SSID, "\';\"");

        system (ac1Command);
    }

    if (pu1UserName != NULL && pu1SSID == NULL && pu1Attribute == NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";delete from radcheck where username=\'", pu1UserName,
                  "\';\"");

        system (ac1Command);
    }

    if (pu1UserName != NULL && pu1SSID == NULL && pu1Attribute != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%s%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";delete from radcheck where username=\'", pu1UserName,
                  "\' and attribute=\'", pu1Attribute, "\';\"");

        system (ac1Command);
    }

    if (pu1UserName != NULL && pu1SSID != NULL && pu1Attribute == NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%s%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";delete from radcheck where username=\'", pu1UserName,
                  "\' and ssid=\'", pu1SSID, "\';\"");

        system (ac1Command);
    }

    if (pu1UserName != NULL && pu1SSID != NULL && pu1Attribute != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                  "%s%s%s%s%s%s%s%s%s%s", "mysql -s -t -e ", "\"use ",
                  au1DatabaseName, ";delete from radcheck where username=\'",
                  pu1UserName, "\' and ssid=\'", pu1SSID, "\' and attribute=\'",
                  pu1Attribute, "\';\"");

        system (ac1Command);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssExportDatabase
 *Description :  This function is used to export our database           
 *                                                        
 *Input       :  NONE
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssExportDatabase (VOID)
{

    /*  We need to lock the database so we can do a data dump.
     *
     *  Note: Locking your database will block updates to it, so your application will only be able to do read-only
     */

    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s",
              "mysql -s -t -e ",
              "\"FLUSH TABLES WITH READ LOCK;SET GLOBAL read_only = ON;\"");
    system (ac1Command);

    /* Exporting database to current directory */

    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s",
              "mysqldump --lock-all-tables --all-databases > ", FLASH_CUST,
              "mysql_radius_db.sql");
    system (ac1Command);

    /*to unlock */
    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s",
              "mysql -s -t -e ",
              "\"SET GLOBAL read_only = OFF;UNLOCK TABLES;\"");
    system (ac1Command);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssImportDatabase
 *Description :  This function is used to import required database           
 *                                                        
 *Input       :  NONE
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssImportDatabase (VOID)
{

    /* Taking backup of already available database */

    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s",
              "mysqldump --lock-all-tables --all-databases > ", FLASH_CUST,
              "mysql_radius_db_bkp.sql");
    system (ac1Command);

    /*importing new database */

    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s", "mysql < ",
              FLASH_CUST, "mysql_radius_db.sql");
    system (ac1Command);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssDeleteRadAcct
 *Description :  This function is used to delete 
 *          entry from Radacct table           
 *                                                        
 *Input       :  NONE
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssDeleteRadAcct (VOID)
{
    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s",
              "mysql -s -t -e ", "\"use ", au1DatabaseName,
              ";delete from radacct;\"");

    system (ac1Command);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssShowRadreply
 *Description :  This function is used to display 
 *          all the entries from Radreply table           
 *                                                        
 *Input       :  pu1Database,
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssShowRadreply (UINT1 *pu1Database)
{
    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s",
              "mysql -s -t -e ", "\"use ", pu1Database,
              ";select * from radreply;\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssShowRadcheck
 *Description :  This function is used to display 
 *          all the entries from Radcheck table           
 *                                                        
 *Input       :  pu1Database,
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssShowRadcheck (UINT1 *pu1Database)
{
    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s",
              "mysql -s -t -e ", "\"use ", pu1Database,
              ";select * from radcheck;\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssShowRadAcct
 *Description :  This function is used to display 
 *          all the entries from RadAcct table           
 *                                                        
 *Input       :  pu1Database
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssShowRadAcct (UINT1 *pu1Database)
{
    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s",
              "mysql -s -t -e ", "\"use ", pu1Database,
              ";select * from radacct;\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssDescribeRadCheckAttributes
 *Description :  This function is used to describe 
 *          all the attribute from Radcheck table           
 *                                                        
 *Input       :  pu1Database
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssDescribeRadCheckAttributes (UINT1 *pu1Database)
{
    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s",
              "mysql -s -t -e ", "\"use ", pu1Database,
              ";describe radcheck;\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssDescribeRadReplyAttributes
 *Description :  This function is used to describe 
 *          all the attribute from Radreply table           
 *                                                        
 *Input       :  pu1Database
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssDescribeRadReplyAttributes (UINT1 *pu1Database)
{
    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s",
              "mysql -s -t -e ", "\"use ", pu1Database,
              ";describe radreply;\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssDescribeRadAcctAttributes
 *Description :  This function is used to describe 
 *          all the attribute from Radacct table           
 *                                                        
 *Input       :  pu1Database
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssDescribeRadAcctAttributes (UINT1 *pu1Database)
{
    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s",
              "mysql -s -t -e ", "\"use ", pu1Database, ";describe radacct;\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssDeleteUserFromRadGroupReply
 *Description :  This function is used to delete 
 *          entry from Radreply table           
 *                                                        
 *Input       :  pu1GroupName,
 *               pu1SSID,
 *               pu1Attribute
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssDeleteUserFromRadGroupReply (UINT1 *pu1UserName, UINT1 *pu1GroupName,
                                UINT1 *pu1SSID)
{

    if (pu1GroupName != NULL && pu1SSID != NULL && pu1UserName != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                  "%s%s%s%s%s%s%s%s%s%s", "mysql -s -t -e ", "\"use ",
                  au1DatabaseName,
                  ";delete from radusergroup where groupname=\'", pu1GroupName,
                  "\' and ssid=\'", pu1SSID, "\' and username=\'", pu1UserName,
                  "\';\"");
        system (ac1Command);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssDeleteRadGroupReply
 *Description :  This function is used to delete 
 *          entry from Radreply table           
 *                                                        
 *Input       :  pu1GroupName,
 *               pu1SSID,
 *               pu1Attribute
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssDeleteRadGroupReply (UINT1 *pu1GroupName, UINT1 *pu1SSID,
                        UINT1 *pu1Attribute)
{
    if (pu1GroupName != NULL && pu1SSID != NULL && pu1Attribute == NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s%s%s%s%s",
                  "mysql -s -t -e ", "\"use ", au1DatabaseName,
                  ";delete from radgroupreply where groupname=\'", pu1GroupName,
                  "\' and ssid=\'", pu1SSID, "\';\"");

        system (ac1Command);
    }

    if (pu1GroupName != NULL && pu1SSID != NULL && pu1Attribute != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                  "%s%s%s%s%s%s%s%s%s%s", "mysql -s -t -e ", "\"use ",
                  au1DatabaseName,
                  ";delete from radgroupreply where groupname=\'", pu1GroupName,
                  "\' and ssid=\'", pu1SSID, "\' and attribute=\'",
                  pu1Attribute, "\';\"");

        system (ac1Command);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssGroupInsertRadUserGroup
 *Description :  This function is for inserting username, 
 *         SSID into radusergroup  table           
 *                                                        
 *Input       :  pu1UserName,
 *               pu1SSID,                             
 *         pu1GroupName                                
 *               u4priority
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssGroupInsertRadUserGroup (UINT1 *pu1UserName, UINT1 *pu1GroupName,
                            UINT1 *pu1SSID, UINT4 u4Priority)
{
    /* Delete the duplicate entry before inserting with alreday existing same username and ssid and groupname */
    WssDeleteUserFromRadGroupReply (pu1UserName, pu1SSID, pu1GroupName);

    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
              "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%d%s%s", "mysql -s -t -e ", "\"use ",
              au1DatabaseName,
              "; insert into radusergroup (username, groupname, ssid, priority) values (",
              "'", pu1UserName, "',", "'", pu1GroupName, "',", "'", pu1SSID,
              "',", "'", u4Priority, "'", ");\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssGroupSetVolume
 *Description :  This function is used to insert 
 *          volume into Radgroupreply table           
 *                                                        
 *Input       :  pu1GroupName,
 *               pu1SSID,                             
 *         u4Volume                                
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssGroupSetVolume (UINT1 *pu1GroupName, UINT1 *pu1SSID, UINT4 u4Volume)
{
    UINT1               au1Attribute[ATTRIBUTE_SIZE] = VOLUME;
    UINT1               au1Op[OPERAND_SIZE] = OPERAND;

    /* Delete the duplicate entry before inserting with alreday existing same username and ssid */

    WssDeleteRadGroupReply (pu1GroupName, pu1SSID, au1Attribute);

    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
              "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%d%s%s", "mysql -s -t -e ",
              "\"use ", au1DatabaseName,
              "; insert into radgroupreply (groupname,ssid,attribute,op,value) values (",
              "'", pu1GroupName, "',", "'", pu1SSID, "',", "'", au1Attribute,
              "',", "'", au1Op, "',", "'", u4Volume, "'", ");\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssGroupSetTimeout
 *Description :  This function is used to insert 
 *          Timeout value into Radgroupreply table           
 *                                                        
 *Input       :  pu1GroupName,
 *               pu1SSID,                             
 *         u4Timeout                                
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssGroupSetTimeout (UINT1 *pu1GroupName, UINT1 *pu1SSID, UINT4 u4Timeout)
{
    UINT1               au1Attribute[ATTRIBUTE_SIZE] = TIMEOUT;
    UINT1               au1Op[OPERAND_SIZE] = OPERAND;

    /* Delete the duplicate entry before inserting with alreday existing same username and ssid */

    WssDeleteRadGroupReply (pu1GroupName, pu1SSID, au1Attribute);

    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
              "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%d%s%s", "mysql -s -t -e ",
              "\"use ", au1DatabaseName,
              "; insert into radgroupreply (groupname,ssid,attribute,op,value) values (",
              "'", pu1GroupName, "',", "'", pu1SSID, "',", "'", au1Attribute,
              "',", "'", au1Op, "',", "'", u4Timeout, "'", ");\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssGroupSetBandwidth
 *Description :  This function is used to insert 
 *          Bandwidth value into Radgroupreply table           
 *                                                        
 *Input       :  pu1GroupName,
 *               pu1SSID,                             
 *         u4Bandwidth                                
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssGroupSetBandwidth (UINT1 *pu1GroupName, UINT1 *pu1SSID, UINT4 u4Bandwidth)
{
    UINT1               au1Attribute[ATTRIBUTE_SIZE] = BANDWIDTH;
    UINT1               au1Op[OPERAND_SIZE] = OPERAND;

    /* Delete the duplicate entry before inserting with alreday existing same username and ssid */

    WssDeleteRadGroupReply (pu1GroupName, pu1SSID, au1Attribute);

    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
              "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%d%s%s", "mysql -s -t -e ",
              "\"use ", au1DatabaseName,
              "; insert into radgroupreply (groupname,ssid,attribute,op,value) values (",
              "'", pu1GroupName, "',", "'", pu1SSID, "',", "'", au1Attribute,
              "',", "'", au1Op, "',", "'", u4Bandwidth, "'", ");\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssSetGroupDownloadBandwidth
 *Description :  This function is used to insert 
 *          Download bandwidth value into Radgroupreply table           
 *                                                        
 *Input       :  pu1GroupName,
 *               pu1SSID,                             
 *         u4DLBandwidth                                
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssGroupSetDownloadBandwidth (UINT1 *pu1GroupName, UINT1 *pu1SSID,
                              UINT4 u4DLBandwidth)
{
    UINT1               au1Attribute[ATTRIBUTE_SIZE] = DOWNLOAD_BANDWIDTH;
    UINT1               au1Op[OPERAND_SIZE] = OPERAND;

    /* Delete the duplicate entry before inserting with alreday existing same username and ssid */

    WssDeleteRadGroupReply (pu1GroupName, pu1SSID, au1Attribute);
    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
              "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%d%s%s", "mysql -s -t -e ",
              "\"use ", au1DatabaseName,
              "; insert into radgroupreply (groupname,ssid,attribute,op,value) values (",
              "'", pu1GroupName, "',", "'", pu1SSID, "',", "'", au1Attribute,
              "',", "'", au1Op, "',", "'", u4DLBandwidth, "'", ");\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssGroupSetUploadbandwidth
 *Description :  This function is used to insert 
 *          Upload bandwidth value into Radgroupreply table           
 *                                                        
 *Input       :  pu1GroupName,
 *               pu1SSID,                             
 *         u4ULBandwidth                                
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssGroupSetUploadbandwidth (UINT1 *pu1GroupName, UINT1 *pu1SSID,
                            UINT4 u4ULBandwidth)
{
    UINT1               au1Attribute[ATTRIBUTE_SIZE] = UPLOAD_BANDWIDTH;
    UINT1               au1Op[OPERAND_SIZE] = OPERAND;

    /* Delete the duplicate entry before inserting with alreday existing same username and ssid */

    WssDeleteRadGroupReply (pu1GroupName, pu1SSID, au1Attribute);

    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
              "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%d%s%s", "mysql -s -t -e ",
              "\"use ", au1DatabaseName,
              "; insert into radgroupreply (groupname,ssid,attribute,op,value) values (",
              "'", pu1GroupName, "',", "'", pu1SSID, "',", "'", au1Attribute,
              "',", "'", au1Op, "',", "'", u4ULBandwidth, "'", ");\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssInvalidGroupInput
 *Description :  This function is used to handle 
 *          invalid CLI command           
 *                                                        
 *Input       :  CliHandle,
 *               u4Invalid
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssInvalidGroupInput (tCliHandle CliHandle, UINT4 u4Invalid)
{
    UNUSED_PARAM (u4Invalid);
    CliPrintf (CliHandle, "\nWarning:Either of groupname or ssid required\n\n");

    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssGroupUpdateVolume
 *Description :  This function is used to update 
 *          volume value from Radreply table           
 *                                                        
 *Input       :  pu1UserName,
 *               pu1SSID,                             
 *         u4Volume       
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssGroupUpdateVolume (UINT1 *pu1GroupName, UINT1 *pu1SSID, UINT4 u4Volume)
{

    if (pu1GroupName != NULL && pu1SSID != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                  "%s%s%s%s%d%s%s%s%s%s", "mysql -s -t -e ", "\"use ",
                  au1DatabaseName, ";update radgroupreply set value=\'",
                  u4Volume, "\' where attribute='Volume' and groupname=\'",
                  pu1GroupName, "\' and ssid=\'", pu1SSID, "\';\"");

        system (ac1Command);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssGroupUpdateTimeout
 *Description :  This function is used to update 
 *          timeout value from RadGroupreply table           
 *                                                        
 *Input       :  pu1GroupName,
 *               pu1SSID,                             
 *         u4Timeout       
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssGroupUpdateTimeout (UINT1 *pu1GroupName, UINT1 *pu1SSID, UINT4 u4Timeout)
{

    if (pu1GroupName != NULL && pu1SSID != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                  "%s%s%s%s%d%s%s%s%s%s", "mysql -s -t -e ", "\"use ",
                  au1DatabaseName, ";update radgroupreply set value=\'",
                  u4Timeout, "\' where attribute='Timeout' and groupname=\'",
                  pu1GroupName, "\' and ssid=\'", pu1SSID, "\';\"");

        system (ac1Command);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssGroupUpdateBandwidth
 *Description :  This function is used to update 
 *          bandwidth value from RadGroupreply table           
 *                                                        
 *Input       :  pu1GroupName,
 *               pu1SSID,                             
 *         u4Bandwidth       
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssGroupUpdateBandwidth (UINT1 *pu1GroupName, UINT1 *pu1SSID, UINT4 u4Bandwidth)
{

    if (pu1GroupName != NULL && pu1SSID != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                  "%s%s%s%s%d%s%s%s%s%s", "mysql -s -t -e ", "\"use ",
                  au1DatabaseName, ";update radgroupreply set value=\'",
                  u4Bandwidth,
                  "\' where attribute='Bandwidth' and groupname=\'",
                  pu1GroupName, "\' and ssid=\'", pu1SSID, "\';\"");

        system (ac1Command);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssGroupUpdateUploadBandwidth
 *Description :  This function is used to update 
 *          upload bandwidth value from RadGroupreply table           
 *                                                        
 *Input       :  pu1UserName,
 *               pu1SSID,                             
 *         u4ULBandwidth       
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssGroupUpdateUploadBandwidth (UINT1 *pu1GroupName, UINT1 *pu1SSID,
                               UINT4 u4ULBandwidth)
{

    if (pu1GroupName != NULL && pu1SSID != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                  "%s%s%s%s%d%s%s%s%s%s", "mysql -s -t -e ", "\"use ",
                  au1DatabaseName, ";update radgroupreply set value=\'",
                  u4ULBandwidth,
                  "\' where attribute='ULBandwidth' and groupname=\'",
                  pu1GroupName, "\' and ssid=\'", pu1SSID, "\';\"");

        system (ac1Command);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssUpdateDownloadBandwidth
 *Description :  This function is used to update 
 *          download bandwidth value from Radreply table           
 *                                                        
 *Input       :  pu1UserName,
 *               pu1SSID,                             
 *         u4DLBandwidth       
 *                                                
 *Output      :  None                                    
 *                                        
 *Returns     :  OSIX_SUCCESS                     
 ****************************************************************************/
INT1
WssGroupUpdateDownloadBandwidth (UINT1 *pu1GroupName, UINT1 *pu1SSID,
                                 UINT4 u4DLBandwidth)
{

    if (pu1GroupName != NULL && pu1SSID != NULL)
    {
        SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
                  "%s%s%s%s%d%s%s%s%s%s", "mysql -s -t -e ", "\"use ",
                  au1DatabaseName, ";update radgroupreply set value=\'",
                  u4DLBandwidth,
                  "\' where attribute='DLBandwidth' and groupname=\'",
                  pu1GroupName, "\' and ssid=\'", pu1SSID, "\';\"");

        system (ac1Command);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssShowRadGroupreply
 *Description :  This function is used to display
 *               all the entries from RadGroupreply table
 *
 *Input       :  pu1Database,
 *
 *Output      :  None
 *
 *Returns     :  OSIX_SUCCESS
 ****************************************************************************/
INT1
WssShowRadGroupreply (UINT1 *pu1Database)
{
    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s",
              "mysql -s -t -e ", "\"use ", pu1Database,
              ";select * from radgroupreply;\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *Function    :  WssShowRadUserGroup
 *Description :  This function is used to display
 *               all the entries from RadGroupreply table
 *
 *Input       :  pu1Database,
 *
 *Output      :  None
 *
 *Returns     :  OSIX_SUCCESS
 ****************************************************************************/
INT1
WssShowRadUserGroup (UINT1 *pu1Database)
{
    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "%s%s%s%s",
              "mysql -s -t -e ", "\"use ", pu1Database,
              ";select * from radusergroup;\"");

    system (ac1Command);
    return OSIX_SUCCESS;
}

#endif
