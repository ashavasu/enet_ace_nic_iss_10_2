/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
 Aricent Inc . All Rights Reserved
*
* $Id: wsscfgprotg.h,v 1.4 2017/11/24 10:37:08 siva Exp $
*
* This file contains prototypes for functions defined in Wsscfg.
*********************************************************************/
#ifndef _WSSCFGPROTG_
#define _WSSCFGPROTG_

#include "cli.h"
/*#include "wssifinc.h"*/
#include "wssifcapdb.h"
#include "wssifstatdfs.h"

INT4 WsscfgCliSetDot11StationConfigTable(
    tCliHandle CliHandle,
    tWsscfgDot11StationConfigEntry *
    pWsscfgSetDot11StationConfigEntry,
    tWsscfgIsSetDot11StationConfigEntry *
    pWsscfgIsSetDot11StationConfigEntry);

INT4 WsscfgCliSetDot11AuthenticationAlgorithmsTable(
    tCliHandle CliHandle,
    tWsscfgDot11AuthenticationAlgorithmsEntry *
    pWsscfgSetDot11AuthenticationAlgorithmsEntry,
    tWsscfgIsSetDot11AuthenticationAlgorithmsEntry *
    pWsscfgIsSetDot11AuthenticationAlgorithmsEntry);

INT4 WsscfgCliSetWebauthRedirectFile (
    tCliHandle CliHandle, 
    UINT1 *pu1FileName,
    UINT4 u4WlanId);

INT4 WsscfgCliSetWlanLoginAuthentication(
    tCliHandle CliHandle, 
    UINT4 u4LoginMode,
    UINT4 u4WlanId);

INT4 WsscfgCliSetDot11WEPDefaultKeysTable(
    tCliHandle CliHandle,
    tWsscfgDot11WEPDefaultKeysEntry *
    pWsscfgSetDot11WEPDefaultKeysEntry,
    tWsscfgIsSetDot11WEPDefaultKeysEntry *
    pWsscfgIsSetDot11WEPDefaultKeysEntry);

INT4 WsscfgCliSetDot11WEPKeyMappingsTable(
    tCliHandle CliHandle,
    tWsscfgDot11WEPKeyMappingsEntry *
    pWsscfgSetDot11WEPKeyMappingsEntry,
    tWsscfgIsSetDot11WEPKeyMappingsEntry *
    pWsscfgIsSetDot11WEPKeyMappingsEntry);

INT4 WsscfgCliSetDot11PrivacyTable(
    tCliHandle CliHandle,
    tWsscfgDot11PrivacyEntry * pWsscfgSetDot11PrivacyEntry,
    tWsscfgIsSetDot11PrivacyEntry * pWsscfgIsSetDot11PrivacyEntry);

INT4 WsscfgCliSetDot11MultiDomainCapabilityTable(
    tCliHandle CliHandle,
    tWsscfgDot11MultiDomainCapabilityEntry *
    pWsscfgSetDot11MultiDomainCapabilityEntry,
    tWsscfgIsSetDot11MultiDomainCapabilityEntry *
    pWsscfgIsSetDot11MultiDomainCapabilityEntry);

INT4 WsscfgCliSetDot11SpectrumManagementTable(
    tCliHandle CliHandle,
    tWsscfgDot11SpectrumManagementEntry *
    pWsscfgSetDot11SpectrumManagementEntry,
    tWsscfgIsSetDot11SpectrumManagementEntry *
    pWsscfgIsSetDot11SpectrumManagementEntry);

INT4 WsscfgCliSetDot11RegulatoryClassesTable(
    tCliHandle CliHandle,
    tWsscfgDot11RegulatoryClassesEntry *
    pWsscfgSetDot11RegulatoryClassesEntry,
    tWsscfgIsSetDot11RegulatoryClassesEntry *
    pWsscfgIsSetDot11RegulatoryClassesEntry);

INT1
WsscfgCliSetFragmentationThreshold (
            tCliHandle, UINT4,
            UINT4, UINT1 *, UINT4 *);

INT4 WsscfgCliSetDot11OperationTable(
    tCliHandle CliHandle,
    tWsscfgDot11OperationEntry * pWsscfgSetDot11OperationEntry,
    tWsscfgIsSetDot11OperationEntry *
    pWsscfgIsSetDot11OperationEntry);

INT4 WsscfgCliSetDot11GroupAddressesTable(
    tCliHandle CliHandle,
    tWsscfgDot11GroupAddressesEntry *
    pWsscfgSetDot11GroupAddressesEntry,
    tWsscfgIsSetDot11GroupAddressesEntry *
    pWsscfgIsSetDot11GroupAddressesEntry);

INT4 WsscfgCliSetDot11EDCATable(
    tCliHandle CliHandle,
    tWsscfgDot11EDCAEntry * pWsscfgSetDot11EDCAEntry,
    tWsscfgIsSetDot11EDCAEntry * pWsscfgIsSetDot11EDCAEntry);

INT4 WsscfgCliSetDot11QAPEDCATable(
    tCliHandle CliHandle,
    tWsscfgDot11QAPEDCAEntry * pWsscfgSetDot11QAPEDCAEntry,
    tWsscfgIsSetDot11QAPEDCAEntry * pWsscfgIsSetDot11QAPEDCAEntry);

INT4 WsscfgCliSetDot11PhyOperationTable(
    tCliHandle CliHandle,
    tWsscfgDot11PhyOperationEntry * pWsscfgSetDot11PhyOperationEntry,
    tWsscfgIsSetDot11PhyOperationEntry *
    pWsscfgIsSetDot11PhyOperationEntry);

INT4 WsscfgCliSetDot11PhyAntennaTable(
    tCliHandle CliHandle,
    tWsscfgDot11PhyAntennaEntry * pWsscfgSetDot11PhyAntennaEntry,
    tWsscfgIsSetDot11PhyAntennaEntry *
    pWsscfgIsSetDot11PhyAntennaEntry);

INT4 WsscfgCliSetDot11PhyTxPowerTable(
    tCliHandle CliHandle,
    tWsscfgDot11PhyTxPowerEntry * pWsscfgSetDot11PhyTxPowerEntry,
    tWsscfgIsSetDot11PhyTxPowerEntry *
    pWsscfgIsSetDot11PhyTxPowerEntry);

INT4 WsscfgCliSetDot11PhyFHSSTable(
    tCliHandle CliHandle,
    tWsscfgDot11PhyFHSSEntry * pWsscfgSetDot11PhyFHSSEntry,
    tWsscfgIsSetDot11PhyFHSSEntry * pWsscfgIsSetDot11PhyFHSSEntry);

INT4 WsscfgCliSetDot11PhyDSSSTable(
    tCliHandle CliHandle,
    tWsscfgDot11PhyDSSSEntry * pWsscfgSetDot11PhyDSSSEntry,
    tWsscfgIsSetDot11PhyDSSSEntry * pWsscfgIsSetDot11PhyDSSSEntry);

INT4 WsscfgCliSetDot11PhyIRTable(
    tCliHandle CliHandle,
    tWsscfgDot11PhyIREntry * pWsscfgSetDot11PhyIREntry,
    tWsscfgIsSetDot11PhyIREntry * pWsscfgIsSetDot11PhyIREntry);

INT4 WsscfgCliSetDot11AntennasListTable(
    tCliHandle CliHandle,
    tWsscfgDot11AntennasListEntry * pWsscfgSetDot11AntennasListEntry,
    tWsscfgIsSetDot11AntennasListEntry *
    pWsscfgIsSetDot11AntennasListEntry);

INT4 WsscfgCliSetDot11PhyOFDMTable(
    tCliHandle CliHandle,
    tWsscfgDot11PhyOFDMEntry * pWsscfgSetDot11PhyOFDMEntry,
    tWsscfgIsSetDot11PhyOFDMEntry * pWsscfgIsSetDot11PhyOFDMEntry);

INT4 WsscfgCliSetDot11HoppingPatternTable(
    tCliHandle CliHandle,
    tWsscfgDot11HoppingPatternEntry *
    pWsscfgSetDot11HoppingPatternEntry,
    tWsscfgIsSetDot11HoppingPatternEntry *
    pWsscfgIsSetDot11HoppingPatternEntry);

INT4 WsscfgCliSetDot11PhyERPTable(
    tCliHandle CliHandle,
    tWsscfgDot11PhyERPEntry * pWsscfgSetDot11PhyERPEntry,
    tWsscfgIsSetDot11PhyERPEntry * pWsscfgIsSetDot11PhyERPEntry);

INT4 WsscfgCliSetDot11RSNAConfigTable(
    tCliHandle CliHandle,
    tWsscfgDot11RSNAConfigEntry * pWsscfgSetDot11RSNAConfigEntry,
    tWsscfgIsSetDot11RSNAConfigEntry *
    pWsscfgIsSetDot11RSNAConfigEntry);

INT4 WsscfgCliSetDot11RSNAConfigPairwiseCiphersTable(
    tCliHandle CliHandle,
    tWsscfgDot11RSNAConfigPairwiseCiphersEntry *
    pWsscfgSetDot11RSNAConfigPairwiseCiphersEntry,
    tWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry *
    pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry);

INT4 WsscfgCliSetDot11RSNAConfigAuthenticationSuitesTable(
    tCliHandle CliHandle,
    tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *
    pWsscfgSetDot11RSNAConfigAuthenticationSuitesEntry,
    tWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry *
    pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry);

INT4 WsscfgCliSetFsRrmConfigTable(
    tCliHandle CliHandle,
    tWsscfgFsRrmConfigEntry * pWsscfgSetFsRrmConfigEntry,
    tWsscfgIsSetFsRrmConfigEntry * pWsscfgIsSetFsRrmConfigEntry);

INT4 WsscfgCliSetFsWlanStationTrapStatus(tCliHandle CliHandle,
                                    UINT4 *pFsWlanStationTrapStatus);
INT4 WssCfgUtlSetConfigAllowChannelWidth(INT4 i4IfIndex ,
                         INT4 i4SetValFsDot11nConfigAllowChannelWidth);
INT4
WssCfgUtlGetConfigAllowChannelWidth(INT4 i4IfIndex ,
                         INT4 *pi4GetValFsDot11nConfigAllowChannelWidth);


PUBLIC INT4 WsscfgUtlAddDefaultRBTree(
    VOID);
PUBLIC INT4 WsscfgAddDefaultFsRrmConfigTable(
    VOID);

PUBLIC INT4 WsscfgUtlCreateRBTree PROTO(
     (VOID));

PUBLIC INT4 WsscfgLock(
    VOID);

PUBLIC INT4 WsscfgUnLock(
    VOID);
PUBLIC INT4 Dot11StationConfigTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11StationConfigTableCreate PROTO(
     (VOID));

tWsscfgDot11StationConfigEntry *WsscfgDot11StationConfigTableCreateApi
PROTO(
     (tWsscfgDot11StationConfigEntry *));

PUBLIC INT4 Dot11AuthenticationAlgorithmsTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11AuthenticationAlgorithmsTableCreate PROTO(
     (VOID));

tWsscfgDot11AuthenticationAlgorithmsEntry
    * WsscfgDot11AuthenticationAlgorithmsTableCreateApi PROTO((tWsscfgDot11AuthenticationAlgorithmsEntry *));

PUBLIC INT4 Dot11WEPDefaultKeysTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11WEPDefaultKeysTableCreate PROTO(
     (VOID));

tWsscfgDot11WEPDefaultKeysEntry
    *WsscfgDot11WEPDefaultKeysTableCreateApi PROTO(
     (tWsscfgDot11WEPDefaultKeysEntry *));

PUBLIC INT4 Dot11WEPKeyMappingsTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11WEPKeyMappingsTableCreate PROTO(
     (VOID));

tWsscfgDot11WEPKeyMappingsEntry
    *WsscfgDot11WEPKeyMappingsTableCreateApi PROTO(
     (tWsscfgDot11WEPKeyMappingsEntry *));

PUBLIC INT4 Dot11PrivacyTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11PrivacyTableCreate PROTO(
     (VOID));

tWsscfgDot11PrivacyEntry *WsscfgDot11PrivacyTableCreateApi PROTO(
     (tWsscfgDot11PrivacyEntry *));

PUBLIC INT4 Dot11MultiDomainCapabilityTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11MultiDomainCapabilityTableCreate PROTO(
     (VOID));

tWsscfgDot11MultiDomainCapabilityEntry
    * WsscfgDot11MultiDomainCapabilityTableCreateApi PROTO((tWsscfgDot11MultiDomainCapabilityEntry *));

PUBLIC INT4 Dot11SpectrumManagementTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11SpectrumManagementTableCreate PROTO(
     (VOID));

tWsscfgDot11SpectrumManagementEntry
    * WsscfgDot11SpectrumManagementTableCreateApi PROTO((tWsscfgDot11SpectrumManagementEntry *));

PUBLIC INT4 Dot11RegulatoryClassesTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11RegulatoryClassesTableCreate PROTO(
     (VOID));

tWsscfgDot11RegulatoryClassesEntry
    * WsscfgDot11RegulatoryClassesTableCreateApi PROTO((tWsscfgDot11RegulatoryClassesEntry *));

PUBLIC INT4 Dot11OperationTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11OperationTableCreate PROTO(
     (VOID));

tWsscfgDot11OperationEntry *WsscfgDot11OperationTableCreateApi PROTO(
     (tWsscfgDot11OperationEntry *));

PUBLIC INT4 Dot11CountersTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11CountersTableCreate PROTO(
     (VOID));

tWsscfgDot11CountersEntry *WsscfgDot11CountersTableCreateApi PROTO(
     (tWsscfgDot11CountersEntry *));

PUBLIC INT4 Dot11GroupAddressesTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11GroupAddressesTableCreate PROTO(
     (VOID));

tWsscfgDot11GroupAddressesEntry
    *WsscfgDot11GroupAddressesTableCreateApi PROTO(
     (tWsscfgDot11GroupAddressesEntry *));

PUBLIC INT4 Dot11EDCATableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11EDCATableCreate PROTO(
     (VOID));

tWsscfgDot11EDCAEntry *WsscfgDot11EDCATableCreateApi PROTO(
     (tWsscfgDot11EDCAEntry *));

PUBLIC INT4 Dot11QAPEDCATableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11QAPEDCATableCreate PROTO(
     (VOID));

tWsscfgDot11QAPEDCAEntry *WsscfgDot11QAPEDCATableCreateApi PROTO(
     (tWsscfgDot11QAPEDCAEntry *));

PUBLIC INT4 Dot11QosCountersTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11QosCountersTableCreate PROTO(
     (VOID));

tWsscfgDot11QosCountersEntry *WsscfgDot11QosCountersTableCreateApi
PROTO(
     (tWsscfgDot11QosCountersEntry *));

PUBLIC INT4 Dot11ResourceInfoTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11ResourceInfoTableCreate PROTO(
     (VOID));

tWsscfgDot11ResourceInfoEntry *WsscfgDot11ResourceInfoTableCreateApi
PROTO(
     (tWsscfgDot11ResourceInfoEntry *));

PUBLIC INT4 Dot11PhyOperationTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11PhyOperationTableCreate PROTO(
     (VOID));

tWsscfgDot11PhyOperationEntry *WsscfgDot11PhyOperationTableCreateApi
PROTO(
     (tWsscfgDot11PhyOperationEntry *));

PUBLIC INT4 Dot11PhyAntennaTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11PhyAntennaTableCreate PROTO(
     (VOID));

tWsscfgDot11PhyAntennaEntry *WsscfgDot11PhyAntennaTableCreateApi PROTO(
     (tWsscfgDot11PhyAntennaEntry *));

PUBLIC INT4 Dot11PhyTxPowerTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11PhyTxPowerTableCreate PROTO(
     (VOID));

tWsscfgDot11PhyTxPowerEntry *WsscfgDot11PhyTxPowerTableCreateApi PROTO(
     (tWsscfgDot11PhyTxPowerEntry *));

PUBLIC INT4 Dot11PhyFHSSTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11PhyFHSSTableCreate PROTO(
     (VOID));

tWsscfgDot11PhyFHSSEntry *WsscfgDot11PhyFHSSTableCreateApi PROTO(
     (tWsscfgDot11PhyFHSSEntry *));

PUBLIC INT4 Dot11PhyDSSSTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11PhyDSSSTableCreate PROTO(
     (VOID));

tWsscfgDot11PhyDSSSEntry *WsscfgDot11PhyDSSSTableCreateApi PROTO(
     (tWsscfgDot11PhyDSSSEntry *));

PUBLIC INT4 Dot11PhyIRTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11PhyIRTableCreate PROTO(
     (VOID));

tWsscfgDot11PhyIREntry *WsscfgDot11PhyIRTableCreateApi PROTO(
     (tWsscfgDot11PhyIREntry *));

PUBLIC INT4 Dot11RegDomainsSupportedTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11RegDomainsSupportedTableCreate PROTO(
     (VOID));

tWsscfgDot11RegDomainsSupportedEntry
    * WsscfgDot11RegDomainsSupportedTableCreateApi PROTO((tWsscfgDot11RegDomainsSupportedEntry *));

PUBLIC INT4 Dot11AntennasListTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11AntennasListTableCreate PROTO(
     (VOID));

tWsscfgDot11AntennasListEntry *WsscfgDot11AntennasListTableCreateApi
PROTO(
     (tWsscfgDot11AntennasListEntry *));

PUBLIC INT4 Dot11SupportedDataRatesTxTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11SupportedDataRatesTxTableCreate PROTO(
     (VOID));

tWsscfgDot11SupportedDataRatesTxEntry
    * WsscfgDot11SupportedDataRatesTxTableCreateApi PROTO((tWsscfgDot11SupportedDataRatesTxEntry *));

PUBLIC INT4 Dot11SupportedDataRatesRxTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11SupportedDataRatesRxTableCreate PROTO(
     (VOID));

tWsscfgDot11SupportedDataRatesRxEntry
    * WsscfgDot11SupportedDataRatesRxTableCreateApi PROTO((tWsscfgDot11SupportedDataRatesRxEntry *));

PUBLIC INT4 Dot11PhyOFDMTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11PhyOFDMTableCreate PROTO(
     (VOID));

tWsscfgDot11PhyOFDMEntry *WsscfgDot11PhyOFDMTableCreateApi PROTO(
     (tWsscfgDot11PhyOFDMEntry *));

PUBLIC INT4 Dot11PhyHRDSSSTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11PhyHRDSSSTableCreate PROTO(
     (VOID));

tWsscfgDot11PhyHRDSSSEntry *WsscfgDot11PhyHRDSSSTableCreateApi PROTO(
     (tWsscfgDot11PhyHRDSSSEntry *));

PUBLIC INT4 Dot11HoppingPatternTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11HoppingPatternTableCreate PROTO(
     (VOID));

tWsscfgDot11HoppingPatternEntry
    *WsscfgDot11HoppingPatternTableCreateApi PROTO(
     (tWsscfgDot11HoppingPatternEntry *));

PUBLIC INT4 Dot11PhyERPTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11PhyERPTableCreate PROTO(
     (VOID));

tWsscfgDot11PhyERPEntry *WsscfgDot11PhyERPTableCreateApi PROTO(
     (tWsscfgDot11PhyERPEntry *));

PUBLIC INT4 Dot11RSNAConfigTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11RSNAConfigTableCreate PROTO(
     (VOID));

tWsscfgDot11RSNAConfigEntry *WsscfgDot11RSNAConfigTableCreateApi PROTO(
     (tWsscfgDot11RSNAConfigEntry *));

PUBLIC INT4 Dot11RSNAConfigPairwiseCiphersTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11RSNAConfigPairwiseCiphersTableCreate PROTO(
     (VOID));

tWsscfgDot11RSNAConfigPairwiseCiphersEntry
    * WsscfgDot11RSNAConfigPairwiseCiphersTableCreateApi PROTO((tWsscfgDot11RSNAConfigPairwiseCiphersEntry *));

PUBLIC INT4 Dot11RSNAConfigAuthenticationSuitesTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11RSNAConfigAuthenticationSuitesTableCreate PROTO(
     (VOID));

tWsscfgDot11RSNAConfigAuthenticationSuitesEntry
    * WsscfgDot11RSNAConfigAuthenticationSuitesTableCreateApi PROTO((tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *));

PUBLIC INT4 Dot11RSNAStatsTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgDot11RSNAStatsTableCreate PROTO(
     (VOID));

tWsscfgDot11RSNAStatsEntry *WsscfgDot11RSNAStatsTableCreateApi PROTO(
     (tWsscfgDot11RSNAStatsEntry *));
INT4 WsscfgGetAllDot11StationConfigTable PROTO(
     (tWsscfgDot11StationConfigEntry *));
INT4 WsscfgGetAllUtlDot11StationConfigTable PROTO(
     (tWsscfgDot11StationConfigEntry *,
      tWsscfgDot11StationConfigEntry *));
INT4 WsscfgGetAllDot11AuthenticationAlgorithmsTable PROTO(
     (tWsscfgDot11AuthenticationAlgorithmsEntry *));
INT4 WsscfgGetAllUtlDot11AuthenticationAlgorithmsTable PROTO(
     (tWsscfgDot11AuthenticationAlgorithmsEntry *,
      tWsscfgDot11AuthenticationAlgorithmsEntry *));
INT4 WsscfgGetAllDot11WEPDefaultKeysTable PROTO(
     (tWsscfgDot11WEPDefaultKeysEntry *));
INT4 WsscfgGetAllUtlDot11WEPDefaultKeysTable PROTO(
     (tWsscfgDot11WEPDefaultKeysEntry *,
      tWsscfgDot11WEPDefaultKeysEntry *));
INT4 WsscfgGetAllDot11WEPKeyMappingsTable PROTO(
     (tWsscfgDot11WEPKeyMappingsEntry *));
INT4 WsscfgGetAllUtlDot11WEPKeyMappingsTable PROTO(
     (tWsscfgDot11WEPKeyMappingsEntry *,
      tWsscfgDot11WEPKeyMappingsEntry *));
INT4 WsscfgGetAllDot11PrivacyTable PROTO(
     (tWsscfgDot11PrivacyEntry *));
INT4 WsscfgGetAllUtlDot11PrivacyTable PROTO(
     (tWsscfgDot11PrivacyEntry *,
      tWsscfgDot11PrivacyEntry *));
INT4 WsscfgGetAllDot11MultiDomainCapabilityTable PROTO(
     (tWsscfgDot11MultiDomainCapabilityEntry *));
INT4 WsscfgGetAllUtlDot11MultiDomainCapabilityTable PROTO(
     (tWsscfgDot11MultiDomainCapabilityEntry *,
      tWsscfgDot11MultiDomainCapabilityEntry *));
INT4 WsscfgGetAllDot11SpectrumManagementTable PROTO(
     (tWsscfgDot11SpectrumManagementEntry *));
INT4 WsscfgGetAllUtlDot11SpectrumManagementTable PROTO(
     (tWsscfgDot11SpectrumManagementEntry *,
      tWsscfgDot11SpectrumManagementEntry *));
INT4 WsscfgGetAllDot11RegulatoryClassesTable PROTO(
     (tWsscfgDot11RegulatoryClassesEntry *));
INT4 WsscfgGetAllUtlDot11RegulatoryClassesTable PROTO(
     (tWsscfgDot11RegulatoryClassesEntry *,
      tWsscfgDot11RegulatoryClassesEntry *));
INT4 WsscfgGetAllDot11OperationTable PROTO(
     (tWsscfgDot11OperationEntry *));
INT4 WsscfgGetAllUtlDot11OperationTable PROTO(
     (tWsscfgDot11OperationEntry *,
      tWsscfgDot11OperationEntry *));
INT4 WsscfgGetAllDot11CountersTable PROTO(
     (tWsscfgDot11CountersEntry *));
INT4 WsscfgGetAllUtlDot11CountersTable PROTO(
     (tWsscfgDot11CountersEntry *,
      tWsscfgDot11CountersEntry *));
INT4 WsscfgGetAllDot11GroupAddressesTable PROTO(
     (tWsscfgDot11GroupAddressesEntry *));
INT4 WsscfgGetAllUtlDot11GroupAddressesTable PROTO(
     (tWsscfgDot11GroupAddressesEntry *,
      tWsscfgDot11GroupAddressesEntry *));
INT4 WsscfgGetAllDot11EDCATable PROTO(
     (tWsscfgDot11EDCAEntry *));
INT4 WsscfgGetAllUtlDot11EDCATable PROTO(
     (tWsscfgDot11EDCAEntry *,
      tWsscfgDot11EDCAEntry *));
INT4 WsscfgGetAllDot11QAPEDCATable PROTO(
     (tWsscfgDot11QAPEDCAEntry *));
INT4 WsscfgGetAllUtlDot11QAPEDCATable PROTO(
     (tWsscfgDot11QAPEDCAEntry *,
      tWsscfgDot11QAPEDCAEntry *));
INT4 WsscfgGetAllDot11QosCountersTable PROTO(
     (tWsscfgDot11QosCountersEntry *));
INT4 WsscfgGetAllUtlDot11QosCountersTable PROTO(
     (tWsscfgDot11QosCountersEntry *,
      tWsscfgDot11QosCountersEntry *));
INT4 WsscfgGetDot11ResourceTypeIDName PROTO(
     (UINT1 *));
INT4 WsscfgGetAllDot11ResourceInfoTable PROTO(
     (tWsscfgDot11ResourceInfoEntry *));
INT4 WsscfgGetAllUtlDot11ResourceInfoTable PROTO(
     (tWsscfgDot11ResourceInfoEntry *,
      tWsscfgDot11ResourceInfoEntry *));
INT4 WsscfgGetAllDot11PhyOperationTable PROTO(
     (tWsscfgDot11PhyOperationEntry *));
INT4 WsscfgGetAllUtlDot11PhyOperationTable PROTO(
     (tWsscfgDot11PhyOperationEntry *,
      tWsscfgDot11PhyOperationEntry *));
INT4 WsscfgGetAllDot11PhyAntennaTable PROTO(
     (tWsscfgDot11PhyAntennaEntry *));
INT4 WsscfgGetAllUtlDot11PhyAntennaTable PROTO(
     (tWsscfgDot11PhyAntennaEntry *,
      tWsscfgDot11PhyAntennaEntry *));
INT4 WsscfgGetAllDot11PhyTxPowerTable PROTO(
     (tWsscfgDot11PhyTxPowerEntry *));
INT4 WsscfgGetAllUtlDot11PhyTxPowerTable PROTO(
     (tWsscfgDot11PhyTxPowerEntry *,
      tWsscfgDot11PhyTxPowerEntry *));
INT4 WsscfgGetAllDot11PhyFHSSTable PROTO(
     (tWsscfgDot11PhyFHSSEntry *));
INT4 WsscfgGetAllUtlDot11PhyFHSSTable PROTO(
     (tWsscfgDot11PhyFHSSEntry *,
      tWsscfgDot11PhyFHSSEntry *));
INT4 WsscfgGetAllDot11PhyDSSSTable PROTO(
     (tWsscfgDot11PhyDSSSEntry *));
INT4 WsscfgGetAllUtlDot11PhyDSSSTable PROTO(
     (tWsscfgDot11PhyDSSSEntry *,
      tWsscfgDot11PhyDSSSEntry *));
INT4 WsscfgGetAllDot11PhyIRTable PROTO(
     (tWsscfgDot11PhyIREntry *));
INT4 WsscfgGetAllUtlDot11PhyIRTable PROTO(
     (tWsscfgDot11PhyIREntry *,
      tWsscfgDot11PhyIREntry *));
INT4 WsscfgGetAllDot11RegDomainsSupportedTable PROTO(
     (tWsscfgDot11RegDomainsSupportedEntry *));
INT4 WsscfgGetAllUtlDot11RegDomainsSupportedTable PROTO(
     (tWsscfgDot11RegDomainsSupportedEntry *,
      tWsscfgDot11RegDomainsSupportedEntry *));
INT4 WsscfgGetAllDot11AntennasListTable PROTO(
     (tWsscfgDot11AntennasListEntry *));
INT4 WsscfgGetAllUtlDot11AntennasListTable PROTO(
     (tWsscfgDot11AntennasListEntry *,
      tWsscfgDot11AntennasListEntry *));
INT4 WsscfgGetAllDot11SupportedDataRatesTxTable PROTO(
     (tWsscfgDot11SupportedDataRatesTxEntry *));
INT4 WsscfgGetAllUtlDot11SupportedDataRatesTxTable PROTO(
     (tWsscfgDot11SupportedDataRatesTxEntry *,
      tWsscfgDot11SupportedDataRatesTxEntry *));
INT4 WsscfgGetAllDot11SupportedDataRatesRxTable PROTO(
     (tWsscfgDot11SupportedDataRatesRxEntry *));
INT4 WsscfgGetAllUtlDot11SupportedDataRatesRxTable PROTO(
     (tWsscfgDot11SupportedDataRatesRxEntry *,
      tWsscfgDot11SupportedDataRatesRxEntry *));
INT4 WsscfgGetAllDot11PhyOFDMTable PROTO(
     (tWsscfgDot11PhyOFDMEntry *));
INT4 WsscfgGetAllUtlDot11PhyOFDMTable PROTO(
     (tWsscfgDot11PhyOFDMEntry *,
      tWsscfgDot11PhyOFDMEntry *));
INT4 WsscfgGetAllDot11PhyHRDSSSTable PROTO(
     (tWsscfgDot11PhyHRDSSSEntry *));
INT4 WsscfgGetAllUtlDot11PhyHRDSSSTable PROTO(
     (tWsscfgDot11PhyHRDSSSEntry *,
      tWsscfgDot11PhyHRDSSSEntry *));
INT4 WsscfgGetAllDot11HoppingPatternTable PROTO(
     (tWsscfgDot11HoppingPatternEntry *));
INT4 WsscfgGetAllUtlDot11HoppingPatternTable PROTO(
     (tWsscfgDot11HoppingPatternEntry *,
      tWsscfgDot11HoppingPatternEntry *));
INT4 WsscfgGetAllDot11PhyERPTable PROTO(
     (tWsscfgDot11PhyERPEntry *));
INT4 WsscfgGetAllUtlDot11PhyERPTable PROTO(
     (tWsscfgDot11PhyERPEntry *,
      tWsscfgDot11PhyERPEntry *));
INT4 WsscfgGetAllDot11RSNAConfigTable PROTO(
     (tWsscfgDot11RSNAConfigEntry *));
INT4 WsscfgGetAllUtlDot11RSNAConfigTable PROTO(
     (tWsscfgDot11RSNAConfigEntry *,
      tWsscfgDot11RSNAConfigEntry *));
INT4 WsscfgGetAllDot11RSNAConfigPairwiseCiphersTable PROTO(
     (tWsscfgDot11RSNAConfigPairwiseCiphersEntry *));
INT4 WsscfgGetAllUtlDot11RSNAConfigPairwiseCiphersTable PROTO(
     (tWsscfgDot11RSNAConfigPairwiseCiphersEntry *,
      tWsscfgDot11RSNAConfigPairwiseCiphersEntry *));
INT4 WsscfgGetAllDot11RSNAConfigAuthenticationSuitesTable PROTO(
     (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *));
INT4 WsscfgGetAllUtlDot11RSNAConfigAuthenticationSuitesTable PROTO(
     (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *,
      tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *));
INT4 WsscfgGetAllDot11RSNAStatsTable PROTO(
     (tWsscfgDot11RSNAStatsEntry *));
INT4 WsscfgGetAllUtlDot11RSNAStatsTable PROTO(
     (tWsscfgDot11RSNAStatsEntry *,
      tWsscfgDot11RSNAStatsEntry *));
INT4 WsscfgSetAllDot11StationConfigTable PROTO(
     (tWsscfgDot11StationConfigEntry *,
      tWsscfgIsSetDot11StationConfigEntry *));
INT4 WsscfgSetAllDot11AuthenticationAlgorithmsTable PROTO(
     (tWsscfgDot11AuthenticationAlgorithmsEntry *,
      tWsscfgIsSetDot11AuthenticationAlgorithmsEntry *));
INT4 WsscfgSetAllDot11WEPDefaultKeysTable PROTO(
     (tWsscfgDot11WEPDefaultKeysEntry *,
      tWsscfgIsSetDot11WEPDefaultKeysEntry *));
INT4 WsscfgSetAllDot11WEPKeyMappingsTable PROTO(
     (tWsscfgDot11WEPKeyMappingsEntry *,
      tWsscfgIsSetDot11WEPKeyMappingsEntry *,
      INT4,
      INT4));
INT4 WsscfgSetAllDot11PrivacyTable PROTO(
     (tWsscfgDot11PrivacyEntry *,
      tWsscfgIsSetDot11PrivacyEntry *));
INT4 WsscfgSetAllDot11MultiDomainCapabilityTable PROTO(
     (tWsscfgDot11MultiDomainCapabilityEntry *,
      tWsscfgIsSetDot11MultiDomainCapabilityEntry *));
INT4 WsscfgSetAllDot11SpectrumManagementTable PROTO(
     (tWsscfgDot11SpectrumManagementEntry *,
      tWsscfgIsSetDot11SpectrumManagementEntry *));
INT4 WsscfgSetAllDot11RegulatoryClassesTable PROTO(
     (tWsscfgDot11RegulatoryClassesEntry *,
      tWsscfgIsSetDot11RegulatoryClassesEntry *));
INT4 WsscfgSetAllDot11OperationTable PROTO(
     (tWsscfgDot11OperationEntry *,
      tWsscfgIsSetDot11OperationEntry *));
INT4 WsscfgSetAllDot11GroupAddressesTable PROTO(
     (tWsscfgDot11GroupAddressesEntry *,
      tWsscfgIsSetDot11GroupAddressesEntry *,
      INT4,
      INT4));
INT4 WsscfgSetAllDot11EDCATable PROTO(
     (tWsscfgDot11EDCAEntry *,
      tWsscfgIsSetDot11EDCAEntry *));
INT4 WsscfgSetAllDot11QAPEDCATable PROTO(
     (tWsscfgDot11QAPEDCAEntry *,
      tWsscfgIsSetDot11QAPEDCAEntry *));
INT4 WsscfgSetAllDot11PhyOperationTable PROTO(
     (tWsscfgDot11PhyOperationEntry *,
      tWsscfgIsSetDot11PhyOperationEntry *));
INT4 WsscfgSetAllDot11PhyAntennaTable PROTO(
     (tWsscfgDot11PhyAntennaEntry *,
      tWsscfgIsSetDot11PhyAntennaEntry *));
INT4 WsscfgSetAllDot11PhyTxPowerTable PROTO(
     (tWsscfgDot11PhyTxPowerEntry *,
      tWsscfgIsSetDot11PhyTxPowerEntry *));
INT4 WsscfgSetAllDot11PhyFHSSTable PROTO(
     (tWsscfgDot11PhyFHSSEntry *,
      tWsscfgIsSetDot11PhyFHSSEntry *));
INT4 WsscfgSetAllDot11PhyDSSSTable PROTO(
     (tWsscfgDot11PhyDSSSEntry *,
      tWsscfgIsSetDot11PhyDSSSEntry *));
INT4 WsscfgSetAllDot11PhyIRTable PROTO(
     (tWsscfgDot11PhyIREntry *,
      tWsscfgIsSetDot11PhyIREntry *));
INT4 WsscfgSetAllDot11AntennasListTable PROTO(
     (tWsscfgDot11AntennasListEntry *,
      tWsscfgIsSetDot11AntennasListEntry *));
INT4 WsscfgSetAllDot11PhyOFDMTable PROTO(
     (tWsscfgDot11PhyOFDMEntry *,
      tWsscfgIsSetDot11PhyOFDMEntry *));
INT4 WsscfgSetAllDot11HoppingPatternTable PROTO(
     (tWsscfgDot11HoppingPatternEntry *,
      tWsscfgIsSetDot11HoppingPatternEntry *));
INT4 WsscfgSetAllDot11PhyERPTable PROTO(
     (tWsscfgDot11PhyERPEntry *,
      tWsscfgIsSetDot11PhyERPEntry *));
INT4 WsscfgSetAllDot11RSNAConfigTable PROTO(
     (tWsscfgDot11RSNAConfigEntry *,
      tWsscfgIsSetDot11RSNAConfigEntry *));
INT4 WsscfgSetAllDot11RSNAConfigPairwiseCiphersTable PROTO(
     (tWsscfgDot11RSNAConfigPairwiseCiphersEntry *,
      tWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry *));
INT4 WsscfgSetAllDot11RSNAConfigAuthenticationSuitesTable PROTO(
     (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *,
      tWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry *));

INT4 WsscfgInitializeDot11WEPKeyMappingsTable PROTO(
     (tWsscfgDot11WEPKeyMappingsEntry *));

INT4 WsscfgInitializeMibDot11WEPKeyMappingsTable PROTO(
     (tWsscfgDot11WEPKeyMappingsEntry *));

INT4 WsscfgInitializeDot11GroupAddressesTable PROTO(
     (tWsscfgDot11GroupAddressesEntry *));
INT4 WsscfgInitializeFsQAPProfileTable(
    tWsscfgFsQAPProfileEntry * pWsscfgFsQAPProfileEntry);
INT4 WsscfgInitializeMibFsQAPProfileTable(
    tWsscfgFsQAPProfileEntry * pWsscfgFsQAPProfileEntry);

INT4 WebAuthShowRunningConfig(
        tCliHandle CliHandle);
INT4 WssCfgShowCountrySupported(
    tCliHandle CliHandle);
INT4 WssCfgShowCapabilityProfile(
    tCliHandle CliHandle,
    UINT1 * pu1CapwapBaseWtpProfileName);
INT4 WssCfgShowQosProfile(
    tCliHandle CliHandle,
    UINT1 * pu1CapwapBaseWtpProfileName);
INT4 WssCfgShowAuthenticationProfile(
    tCliHandle CliHandle,
    UINT1 * pu1CapwapBaseWtpProfileName);
INT4 WssCfgShowWlanStatistics(
     tCliHandle CliHandle,
     UINT4 * pu4CurrentCapwapDot11WlanProfileId);
INT4 WssCfgShowApWlanStatistics(
     tCliHandle CliHandle,
     UINT1 *pu1CapwapBaseWtpProfileName,
     UINT4 *pu4RadioId, UINT4 *pu4WlanId);
INT4 WssCfgShowDiffServStatistics(
     tCliHandle CliHandle);
INT4 WssCfgShowWlanMulticastConfig(
    tCliHandle CliHandle,
    UINT4 u4WlanProfileId);
INT4 WssCfgShowWlanConfig(
    tCliHandle CliHandle,
    UINT4 u4WlanProfileId);
INT4 WssCfgShowClientSummary(
    tCliHandle CliHandle);
INT4 WssCfgShowWebAuthConfig(
    tCliHandle CliHandle);
INT4 WssCfgShowWlanMulticastSummary(
    tCliHandle CliHandle);    
INT4 WssCfgShowWlanSummary(
    tCliHandle CliHandle);
INT4 WssCfgShowClientStats (
    tCliHandle CliHandle, 
    UINT1  *pu1ClientMacAddr,UINT1);
INT4 WssCfgClientStats(tCliHandle CliHandle,
    tMacAddr staMacAddr);
INT4 WssCfgShowNetUserSummary(
    tCliHandle CliHandle);
INT4 WsscfgWlanAuthDelete(
    tCliHandle CliHandle,
    UINT1 * pu1Prompt);
INT4 WsscfgWlanAuthCreate(
    tCliHandle CliHandle,
    UINT1 * pu1Prompt);
INT4 WsscfgWlanQosDelete(
    tCliHandle CliHandle,
    UINT1 * pu1Prompt);
INT4 WsscfgWlanQosCreate(
    tCliHandle CliHandle,
    UINT1 * pu1Prompt);
INT4 WsscfgWlanCapabilityDelete(
    tCliHandle CliHandle,
    UINT1 * pu1Prompt);
INT4 WsscfgWlanCapabilityCreate(
    tCliHandle CliHandle,
    UINT1 * pu1Prompt);
INT4 WsscfgCliClearFsSecurityWebAuthWebLoadBalInfo(
    tCliHandle CliHandle);
INT4 WsscfgCliClearFsSecurityWebAuthWebButtonText(
    tCliHandle CliHandle);
INT4 WsscfgCliClearFsSecurityWebAuthWebFailMessage(
    tCliHandle CliHandle);
INT4 WsscfgCliClearFsSecurityWebAuthWebSuccMessage(
    tCliHandle CliHandle);
INT4 WsscfgCliClearFsSecurityWebAuthWebMessage(
    tCliHandle CliHandle);
INT4 WsscfgCliClearFsSecurityWebAuthWebTitle(
    tCliHandle CliHandle);
INT4 WsscfgClearClientCounters (
    tCliHandle CliHandle,
    tMacAddr staMacAddr);
INT4
WsscfgClearRadioCounters (tCliHandle CliHandle,
    UINT1 *pu1ProfileName,
    UINT4 *pu4RadioId);
INT4 WsscfgInitializeMibDot11GroupAddressesTable PROTO(
     (tWsscfgDot11GroupAddressesEntry *));
INT4 WsscfgTestAllDot11StationConfigTable PROTO(
     (UINT4 *,
      tWsscfgDot11StationConfigEntry *,
      tWsscfgIsSetDot11StationConfigEntry *));
INT4 WsscfgTestAllDot11AuthenticationAlgorithmsTable PROTO(
     (UINT4 *,
      tWsscfgDot11AuthenticationAlgorithmsEntry *,
      tWsscfgIsSetDot11AuthenticationAlgorithmsEntry *));
INT4 WsscfgTestAllDot11WEPDefaultKeysTable PROTO(
     (UINT4 *,
      tWsscfgDot11WEPDefaultKeysEntry *,
      tWsscfgIsSetDot11WEPDefaultKeysEntry *));
INT4 WsscfgTestAllDot11WEPKeyMappingsTable PROTO(
     (UINT4 *,
      tWsscfgDot11WEPKeyMappingsEntry *,
      tWsscfgIsSetDot11WEPKeyMappingsEntry *,
      INT4,
      INT4));
INT4 WsscfgTestAllDot11PrivacyTable PROTO(
     (UINT4 *,
      tWsscfgDot11PrivacyEntry *,
      tWsscfgIsSetDot11PrivacyEntry *));
INT4 WsscfgTestAllDot11MultiDomainCapabilityTable PROTO(
     (UINT4 *,
      tWsscfgDot11MultiDomainCapabilityEntry *,
      tWsscfgIsSetDot11MultiDomainCapabilityEntry *));
INT4 WsscfgTestAllDot11SpectrumManagementTable PROTO(
     (UINT4 *,
      tWsscfgDot11SpectrumManagementEntry *,
      tWsscfgIsSetDot11SpectrumManagementEntry *));
INT4 WsscfgTestAllDot11RegulatoryClassesTable PROTO(
     (UINT4 *,
      tWsscfgDot11RegulatoryClassesEntry *,
      tWsscfgIsSetDot11RegulatoryClassesEntry *));
INT4 WsscfgTestAllDot11OperationTable PROTO(
     (UINT4 *,
      tWsscfgDot11OperationEntry *,
      tWsscfgIsSetDot11OperationEntry *));
INT4 WsscfgTestAllDot11GroupAddressesTable PROTO(
     (UINT4 *,
      tWsscfgDot11GroupAddressesEntry *,
      tWsscfgIsSetDot11GroupAddressesEntry *,
      INT4,
      INT4));
INT4 WsscfgTestAllDot11EDCATable PROTO(
     (UINT4 *,
      tWsscfgDot11EDCAEntry *,
      tWsscfgIsSetDot11EDCAEntry *));
INT4 WsscfgTestAllDot11QAPEDCATable PROTO(
     (UINT4 *,
      tWsscfgDot11QAPEDCAEntry *,
      tWsscfgIsSetDot11QAPEDCAEntry *));
INT4 WsscfgTestAllDot11PhyOperationTable PROTO(
     (UINT4 *,
      tWsscfgDot11PhyOperationEntry *,
      tWsscfgIsSetDot11PhyOperationEntry *));
INT4 WsscfgTestAllDot11PhyAntennaTable PROTO(
     (UINT4 *,
      tWsscfgDot11PhyAntennaEntry *,
      tWsscfgIsSetDot11PhyAntennaEntry *));
INT4 WsscfgTestAllDot11PhyTxPowerTable PROTO(
     (UINT4 *,
      tWsscfgDot11PhyTxPowerEntry *,
      tWsscfgIsSetDot11PhyTxPowerEntry *));
INT4 WsscfgTestAllDot11PhyFHSSTable PROTO(
     (UINT4 *,
      tWsscfgDot11PhyFHSSEntry *,
      tWsscfgIsSetDot11PhyFHSSEntry *));
INT4 WsscfgTestAllDot11PhyDSSSTable PROTO(
     (UINT4 *,
      tWsscfgDot11PhyDSSSEntry *,
      tWsscfgIsSetDot11PhyDSSSEntry *));
INT4 WsscfgTestAllDot11PhyIRTable PROTO(
     (UINT4 *,
      tWsscfgDot11PhyIREntry *,
      tWsscfgIsSetDot11PhyIREntry *));
INT4 WsscfgTestAllDot11AntennasListTable PROTO(
     (UINT4 *,
      tWsscfgDot11AntennasListEntry *,
      tWsscfgIsSetDot11AntennasListEntry *));
INT4 WsscfgTestAllDot11PhyOFDMTable PROTO(
     (UINT4 *,
      tWsscfgDot11PhyOFDMEntry *,
      tWsscfgIsSetDot11PhyOFDMEntry *));
INT4 WsscfgTestAllDot11HoppingPatternTable PROTO(
     (UINT4 *,
      tWsscfgDot11HoppingPatternEntry *,
      tWsscfgIsSetDot11HoppingPatternEntry *));
INT4 WsscfgTestAllDot11PhyERPTable PROTO(
     (UINT4 *,
      tWsscfgDot11PhyERPEntry *,
      tWsscfgIsSetDot11PhyERPEntry *));
INT4 WsscfgTestAllDot11RSNAConfigTable PROTO(
     (UINT4 *,
      tWsscfgDot11RSNAConfigEntry *,
      tWsscfgIsSetDot11RSNAConfigEntry *));
INT4 WsscfgTestAllDot11RSNAConfigPairwiseCiphersTable PROTO(
     (UINT4 *,
      tWsscfgDot11RSNAConfigPairwiseCiphersEntry *,
      tWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry *));
INT4 WsscfgTestAllDot11RSNAConfigAuthenticationSuitesTable PROTO(
     (UINT4 *,
      tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *,
      tWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry *));
tWsscfgDot11StationConfigEntry *WsscfgGetDot11StationConfigTable PROTO(
     (tWsscfgDot11StationConfigEntry *));
tWsscfgDot11AuthenticationAlgorithmsEntry
    * WsscfgGetDot11AuthenticationAlgorithmsTable PROTO((tWsscfgDot11AuthenticationAlgorithmsEntry *));
tWsscfgDot11WEPDefaultKeysEntry *WsscfgGetDot11WEPDefaultKeysTable
PROTO(
     (tWsscfgDot11WEPDefaultKeysEntry *));
tWsscfgDot11WEPKeyMappingsEntry *WsscfgGetDot11WEPKeyMappingsTable
PROTO(
     (tWsscfgDot11WEPKeyMappingsEntry *));
tWsscfgDot11PrivacyEntry *WsscfgGetDot11PrivacyTable PROTO(
     (tWsscfgDot11PrivacyEntry *));
tWsscfgDot11MultiDomainCapabilityEntry
    * WsscfgGetDot11MultiDomainCapabilityTable PROTO((tWsscfgDot11MultiDomainCapabilityEntry *));
tWsscfgDot11SpectrumManagementEntry
    *WsscfgGetDot11SpectrumManagementTable PROTO(
     (tWsscfgDot11SpectrumManagementEntry *));
tWsscfgDot11RegulatoryClassesEntry
    *WsscfgGetDot11RegulatoryClassesTable PROTO(
     (tWsscfgDot11RegulatoryClassesEntry *));
tWsscfgDot11OperationEntry *WsscfgGetDot11OperationTable PROTO(
     (tWsscfgDot11OperationEntry *));
tWsscfgDot11CountersEntry *WsscfgGetDot11CountersTable PROTO(
     (tWsscfgDot11CountersEntry *));
tWsscfgDot11GroupAddressesEntry *WsscfgGetDot11GroupAddressesTable
PROTO(
     (tWsscfgDot11GroupAddressesEntry *));
tWsscfgDot11EDCAEntry *WsscfgGetDot11EDCATable PROTO(
     (tWsscfgDot11EDCAEntry *));
tWsscfgDot11QAPEDCAEntry *WsscfgGetDot11QAPEDCATable PROTO(
     (tWsscfgDot11QAPEDCAEntry *));
tWsscfgDot11QosCountersEntry *WsscfgGetDot11QosCountersTable PROTO(
     (tWsscfgDot11QosCountersEntry *));
tWsscfgDot11ResourceInfoEntry *WsscfgGetDot11ResourceInfoTable PROTO(
     (tWsscfgDot11ResourceInfoEntry *));
tWsscfgDot11PhyOperationEntry *WsscfgGetDot11PhyOperationTable PROTO(
     (tWsscfgDot11PhyOperationEntry *));
tWsscfgDot11PhyAntennaEntry *WsscfgGetDot11PhyAntennaTable PROTO(
     (tWsscfgDot11PhyAntennaEntry *));
tWsscfgDot11PhyTxPowerEntry *WsscfgGetDot11PhyTxPowerTable PROTO(
     (tWsscfgDot11PhyTxPowerEntry *));
tWsscfgDot11PhyFHSSEntry *WsscfgGetDot11PhyFHSSTable PROTO(
     (tWsscfgDot11PhyFHSSEntry *));
tWsscfgDot11PhyDSSSEntry *WsscfgGetDot11PhyDSSSTable PROTO(
     (tWsscfgDot11PhyDSSSEntry *));
tWsscfgDot11PhyIREntry *WsscfgGetDot11PhyIRTable PROTO(
     (tWsscfgDot11PhyIREntry *));
tWsscfgDot11RegDomainsSupportedEntry
    * WsscfgGetDot11RegDomainsSupportedTable PROTO((tWsscfgDot11RegDomainsSupportedEntry *));
tWsscfgDot11AntennasListEntry *WsscfgGetDot11AntennasListTable PROTO(
     (tWsscfgDot11AntennasListEntry *));
tWsscfgDot11SupportedDataRatesTxEntry
    * WsscfgGetDot11SupportedDataRatesTxTable PROTO((tWsscfgDot11SupportedDataRatesTxEntry *));
tWsscfgDot11SupportedDataRatesRxEntry
    * WsscfgGetDot11SupportedDataRatesRxTable PROTO((tWsscfgDot11SupportedDataRatesRxEntry *));
tWsscfgDot11PhyOFDMEntry *WsscfgGetDot11PhyOFDMTable PROTO(
     (tWsscfgDot11PhyOFDMEntry *));
tWsscfgDot11PhyHRDSSSEntry *WsscfgGetDot11PhyHRDSSSTable PROTO(
     (tWsscfgDot11PhyHRDSSSEntry *));
tWsscfgDot11HoppingPatternEntry *WsscfgGetDot11HoppingPatternTable
PROTO(
     (tWsscfgDot11HoppingPatternEntry *));
tWsscfgDot11PhyERPEntry *WsscfgGetDot11PhyERPTable PROTO(
     (tWsscfgDot11PhyERPEntry *));
tWsscfgDot11RSNAConfigEntry *WsscfgGetDot11RSNAConfigTable PROTO(
     (tWsscfgDot11RSNAConfigEntry *));
tWsscfgDot11RSNAConfigPairwiseCiphersEntry
    * WsscfgGetDot11RSNAConfigPairwiseCiphersTable PROTO((tWsscfgDot11RSNAConfigPairwiseCiphersEntry *));
tWsscfgDot11RSNAConfigAuthenticationSuitesEntry
    * WsscfgGetDot11RSNAConfigAuthenticationSuitesTable PROTO((tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *));
tWsscfgDot11RSNAStatsEntry *WsscfgGetDot11RSNAStatsTable PROTO(
     (tWsscfgDot11RSNAStatsEntry *));
INT1 WsscfgGetFirstDot11StationConfigTable PROTO(
     (INT4 *));
INT1 WsscfgGetFirstDot11AuthenticationAlgorithmsTable PROTO((INT4 *));

tWsscfgDot11WEPDefaultKeysEntry
    *WsscfgGetFirstDot11WEPDefaultKeysTable PROTO(
     (VOID));
tWsscfgDot11WEPKeyMappingsEntry
    *WsscfgGetFirstDot11WEPKeyMappingsTable PROTO(
     (VOID));
tWsscfgDot11PrivacyEntry *WsscfgGetFirstDot11PrivacyTable PROTO(
     (VOID));
INT1 WsscfgGetFirstDot11MultiDomainCapabilityTable (INT4 *, UINT4 *);
INT1 WsscfgGetFirstDot11SpectrumManagementTable PROTO(
     (INT4 *));
tWsscfgDot11RegulatoryClassesEntry
    * WsscfgGetFirstDot11RegulatoryClassesTable PROTO((VOID));
INT1 WsscfgGetFirstDot11OperationTable PROTO(
     (INT4 *));
INT1 WsscfgGetFirstDot11CountersTable PROTO((INT4 *));
tWsscfgDot11GroupAddressesEntry
    *WsscfgGetFirstDot11GroupAddressesTable PROTO(
     (VOID));
INT4 WsscfgGetFirstDot11EDCATable(INT4 *pi4IfIndex, INT4 *pi4Dot11EDCATableIndex
        );

tWsscfgDot11QAPEDCAEntry *WsscfgGetFirstDot11QAPEDCATable PROTO(
     (VOID));
tWsscfgDot11QosCountersEntry *WsscfgGetFirstDot11QosCountersTable
PROTO(
     (VOID));
tWsscfgDot11ResourceInfoEntry *WsscfgGetFirstDot11ResourceInfoTable
PROTO(
     (VOID));
tWsscfgDot11PhyOperationEntry *WsscfgGetFirstDot11PhyOperationTable
PROTO(
     (VOID));
INT4 WsscfgGetFirstDot11PhyAntennaTable PROTO((INT4 *));
INT1 WsscfgGetFirstDot11PhyTxPowerTable PROTO(
     (INT4 *));
tWsscfgDot11PhyFHSSEntry *WsscfgGetFirstDot11PhyFHSSTable PROTO(
     (VOID));
INT4 WsscfgGetFirstDot11PhyDSSSTable PROTO((INT4 *));
tWsscfgDot11PhyIREntry *WsscfgGetFirstDot11PhyIRTable PROTO(
     (VOID));
tWsscfgDot11RegDomainsSupportedEntry
    * WsscfgGetFirstDot11RegDomainsSupportedTable PROTO((VOID));
INT4 WsscfgGetFirstDot11AntennasListTable PROTO((INT4 *, INT4 *));
tWsscfgDot11SupportedDataRatesTxEntry
    * WsscfgGetFirstDot11SupportedDataRatesTxTable PROTO((VOID));
tWsscfgDot11SupportedDataRatesRxEntry
    * WsscfgGetFirstDot11SupportedDataRatesRxTable PROTO((VOID));
INT4 WsscfgGetFirstDot11PhyOFDMTable PROTO((INT4 *));
tWsscfgDot11PhyHRDSSSEntry *WsscfgGetFirstDot11PhyHRDSSSTable PROTO(
     (VOID));
tWsscfgDot11HoppingPatternEntry
    *WsscfgGetFirstDot11HoppingPatternTable PROTO(
     (VOID));
tWsscfgDot11PhyERPEntry *WsscfgGetFirstDot11PhyERPTable PROTO(
     (VOID));
tWsscfgDot11RSNAConfigEntry *WsscfgGetFirstDot11RSNAConfigTable PROTO(
     (VOID));
tWsscfgDot11RSNAConfigPairwiseCiphersEntry
    * WsscfgGetFirstDot11RSNAConfigPairwiseCiphersTable PROTO((VOID));
tWsscfgDot11RSNAConfigAuthenticationSuitesEntry
    * WsscfgGetFirstDot11RSNAConfigAuthenticationSuitesTable PROTO((VOID));
tWsscfgDot11RSNAStatsEntry *WsscfgGetFirstDot11RSNAStatsTable PROTO(
     (VOID));
INT1 WsscfgGetNextDot11StationConfigTable PROTO(
     (INT4,
      INT4 *));
INT1 WsscfgGetNextDot11AuthenticationAlgorithmsTable PROTO(
     (INT4,
      INT4 *));
tWsscfgDot11WEPDefaultKeysEntry *WsscfgGetNextDot11WEPDefaultKeysTable
PROTO(
     (tWsscfgDot11WEPDefaultKeysEntry *));
tWsscfgDot11WEPKeyMappingsEntry *WsscfgGetNextDot11WEPKeyMappingsTable
PROTO(
     (tWsscfgDot11WEPKeyMappingsEntry *));
tWsscfgDot11PrivacyEntry *WsscfgGetNextDot11PrivacyTable PROTO(
     (tWsscfgDot11PrivacyEntry *));
INT1 WsscfgGetNextDot11MultiDomainCapabilityTable PROTO(
     (INT4, INT4 *, UINT4, UINT4 *));
INT1 WsscfgGetNextDot11SpectrumManagementTable PROTO(
     (INT4,
      INT4 *));
tWsscfgDot11RegulatoryClassesEntry
    * WsscfgGetNextDot11RegulatoryClassesTable PROTO((tWsscfgDot11RegulatoryClassesEntry *));
INT1 WsscfgGetNextDot11OperationTable PROTO(
     (INT4,
      INT4 *));
INT1 WsscfgGetNextDot11CountersTable PROTO((INT4,INT4*));
tWsscfgDot11GroupAddressesEntry *WsscfgGetNextDot11GroupAddressesTable
PROTO(
     (tWsscfgDot11GroupAddressesEntry *));
INT4 WsscfgGetNextDot11EDCATable PROTO((INT4, INT4 *, INT4, INT4 *));
tWsscfgDot11QAPEDCAEntry *WsscfgGetNextDot11QAPEDCATable PROTO(
     (tWsscfgDot11QAPEDCAEntry *));
tWsscfgDot11QosCountersEntry *WsscfgGetNextDot11QosCountersTable PROTO(
     (tWsscfgDot11QosCountersEntry *));
tWsscfgDot11ResourceInfoEntry *WsscfgGetNextDot11ResourceInfoTable
PROTO(
     (tWsscfgDot11ResourceInfoEntry *));
tWsscfgDot11PhyOperationEntry *WsscfgGetNextDot11PhyOperationTable
PROTO(
     (tWsscfgDot11PhyOperationEntry *));
INT4 WsscfgGetNextDot11PhyAntennaTable PROTO((INT4, INT4 *));
INT1 WsscfgGetNextDot11PhyTxPowerTable PROTO(
     (INT4,
      INT4 *));
tWsscfgDot11PhyFHSSEntry *WsscfgGetNextDot11PhyFHSSTable PROTO(
     (tWsscfgDot11PhyFHSSEntry *));
INT4 WsscfgGetNextDot11PhyDSSSTable PROTO((INT4, INT4 *));
tWsscfgDot11PhyIREntry *WsscfgGetNextDot11PhyIRTable PROTO(
     (tWsscfgDot11PhyIREntry *));
tWsscfgDot11RegDomainsSupportedEntry
    * WsscfgGetNextDot11RegDomainsSupportedTable PROTO((tWsscfgDot11RegDomainsSupportedEntry *));
INT4 WsscfgGetNextDot11AntennasListTable PROTO((INT4, INT4 *, INT4, INT4 *));
tWsscfgDot11SupportedDataRatesTxEntry
    * WsscfgGetNextDot11SupportedDataRatesTxTable PROTO((tWsscfgDot11SupportedDataRatesTxEntry *));
tWsscfgDot11SupportedDataRatesRxEntry
    * WsscfgGetNextDot11SupportedDataRatesRxTable PROTO((tWsscfgDot11SupportedDataRatesRxEntry *));
INT4 WsscfgGetNextDot11PhyOFDMTable PROTO((INT4, INT4 *));
tWsscfgDot11PhyHRDSSSEntry *WsscfgGetNextDot11PhyHRDSSSTable PROTO(
     (tWsscfgDot11PhyHRDSSSEntry *));
tWsscfgDot11HoppingPatternEntry *WsscfgGetNextDot11HoppingPatternTable
PROTO(
     (tWsscfgDot11HoppingPatternEntry *));
tWsscfgDot11PhyERPEntry *WsscfgGetNextDot11PhyERPTable PROTO(
     (tWsscfgDot11PhyERPEntry *));
tWsscfgDot11RSNAConfigEntry *WsscfgGetNextDot11RSNAConfigTable PROTO(
     (tWsscfgDot11RSNAConfigEntry *));
tWsscfgDot11RSNAConfigPairwiseCiphersEntry
    * WsscfgGetNextDot11RSNAConfigPairwiseCiphersTable PROTO((tWsscfgDot11RSNAConfigPairwiseCiphersEntry *));
tWsscfgDot11RSNAConfigAuthenticationSuitesEntry
    * WsscfgGetNextDot11RSNAConfigAuthenticationSuitesTable PROTO((tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *));
tWsscfgDot11RSNAStatsEntry *WsscfgGetNextDot11RSNAStatsTable PROTO(
     (tWsscfgDot11RSNAStatsEntry *));
INT4 Dot11StationConfigTableFilterInputs PROTO(
     (tWsscfgDot11StationConfigEntry *,
      tWsscfgDot11StationConfigEntry *,
      tWsscfgIsSetDot11StationConfigEntry *));
INT4 Dot11AuthenticationAlgorithmsTableFilterInputs PROTO(
     (tWsscfgDot11AuthenticationAlgorithmsEntry *,
      tWsscfgDot11AuthenticationAlgorithmsEntry *,
      tWsscfgIsSetDot11AuthenticationAlgorithmsEntry *));
INT4 Dot11WEPDefaultKeysTableFilterInputs PROTO(
     (tWsscfgDot11WEPDefaultKeysEntry *,
      tWsscfgDot11WEPDefaultKeysEntry *,
      tWsscfgIsSetDot11WEPDefaultKeysEntry *));
INT4 Dot11WEPKeyMappingsTableFilterInputs PROTO(
     (tWsscfgDot11WEPKeyMappingsEntry *,
      tWsscfgDot11WEPKeyMappingsEntry *,
      tWsscfgIsSetDot11WEPKeyMappingsEntry *));
INT4 Dot11PrivacyTableFilterInputs PROTO(
     (tWsscfgDot11PrivacyEntry *,
      tWsscfgDot11PrivacyEntry *,
      tWsscfgIsSetDot11PrivacyEntry *));
INT4 Dot11MultiDomainCapabilityTableFilterInputs PROTO(
     (tWsscfgDot11MultiDomainCapabilityEntry *,
      tWsscfgDot11MultiDomainCapabilityEntry *,
      tWsscfgIsSetDot11MultiDomainCapabilityEntry *));
INT4 Dot11SpectrumManagementTableFilterInputs PROTO(
     (tWsscfgDot11SpectrumManagementEntry *,
      tWsscfgDot11SpectrumManagementEntry *,
      tWsscfgIsSetDot11SpectrumManagementEntry *));
INT4 Dot11RegulatoryClassesTableFilterInputs PROTO(
     (tWsscfgDot11RegulatoryClassesEntry *,
      tWsscfgDot11RegulatoryClassesEntry *,
      tWsscfgIsSetDot11RegulatoryClassesEntry *));
INT4 Dot11OperationTableFilterInputs PROTO(
     (tWsscfgDot11OperationEntry *,
      tWsscfgDot11OperationEntry *,
      tWsscfgIsSetDot11OperationEntry *));
INT4 Dot11GroupAddressesTableFilterInputs PROTO(
     (tWsscfgDot11GroupAddressesEntry *,
      tWsscfgDot11GroupAddressesEntry *,
      tWsscfgIsSetDot11GroupAddressesEntry *));
INT4 Dot11EDCATableFilterInputs PROTO(
     (tWsscfgDot11EDCAEntry *,
      tWsscfgDot11EDCAEntry *,
      tWsscfgIsSetDot11EDCAEntry *));
INT4 Dot11QAPEDCATableFilterInputs PROTO(
     (tWsscfgDot11QAPEDCAEntry *,
      tWsscfgDot11QAPEDCAEntry *,
      tWsscfgIsSetDot11QAPEDCAEntry *));
INT4 Dot11PhyOperationTableFilterInputs PROTO(
     (tWsscfgDot11PhyOperationEntry *,
      tWsscfgDot11PhyOperationEntry *,
      tWsscfgIsSetDot11PhyOperationEntry *));
INT4 Dot11PhyAntennaTableFilterInputs PROTO(
     (tWsscfgDot11PhyAntennaEntry *,
      tWsscfgDot11PhyAntennaEntry *,
      tWsscfgIsSetDot11PhyAntennaEntry *));
INT4 Dot11PhyTxPowerTableFilterInputs PROTO(
     (tWsscfgDot11PhyTxPowerEntry *,
      tWsscfgDot11PhyTxPowerEntry *,
      tWsscfgIsSetDot11PhyTxPowerEntry *));
INT4 Dot11PhyFHSSTableFilterInputs PROTO(
     (tWsscfgDot11PhyFHSSEntry *,
      tWsscfgDot11PhyFHSSEntry *,
      tWsscfgIsSetDot11PhyFHSSEntry *));
INT4 Dot11PhyDSSSTableFilterInputs PROTO(
     (tWsscfgDot11PhyDSSSEntry *,
      tWsscfgDot11PhyDSSSEntry *,
      tWsscfgIsSetDot11PhyDSSSEntry *));
INT4 Dot11PhyIRTableFilterInputs PROTO(
     (tWsscfgDot11PhyIREntry *,
      tWsscfgDot11PhyIREntry *,
      tWsscfgIsSetDot11PhyIREntry *));
INT4 Dot11AntennasListTableFilterInputs PROTO(
     (tWsscfgDot11AntennasListEntry *,
      tWsscfgDot11AntennasListEntry *,
      tWsscfgIsSetDot11AntennasListEntry *));
INT4 Dot11PhyOFDMTableFilterInputs PROTO(
     (tWsscfgDot11PhyOFDMEntry *,
      tWsscfgDot11PhyOFDMEntry *,
      tWsscfgIsSetDot11PhyOFDMEntry *));
INT4 Dot11HoppingPatternTableFilterInputs PROTO(
     (tWsscfgDot11HoppingPatternEntry *,
      tWsscfgDot11HoppingPatternEntry *,
      tWsscfgIsSetDot11HoppingPatternEntry *));
INT4 Dot11PhyERPTableFilterInputs PROTO(
     (tWsscfgDot11PhyERPEntry *,
      tWsscfgDot11PhyERPEntry *,
      tWsscfgIsSetDot11PhyERPEntry *));
INT4 Dot11RSNAConfigTableFilterInputs PROTO(
     (tWsscfgDot11RSNAConfigEntry *,
      tWsscfgDot11RSNAConfigEntry *,
      tWsscfgIsSetDot11RSNAConfigEntry *));
INT4 Dot11RSNAConfigPairwiseCiphersTableFilterInputs PROTO(
     (tWsscfgDot11RSNAConfigPairwiseCiphersEntry *,
      tWsscfgDot11RSNAConfigPairwiseCiphersEntry *,
      tWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry *));
INT4 Dot11RSNAConfigAuthenticationSuitesTableFilterInputs PROTO(
     (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *,
      tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *,
      tWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry *));
INT4 WsscfgUtilUpdateDot11StationConfigTable PROTO(
     (tWsscfgDot11StationConfigEntry *,
      tWsscfgDot11StationConfigEntry *,
      tWsscfgIsSetDot11StationConfigEntry *));
INT4 WsscfgUtilUpdateDot11AuthenticationAlgorithmsTable PROTO(
     (tWsscfgDot11AuthenticationAlgorithmsEntry *,
      tWsscfgDot11AuthenticationAlgorithmsEntry *,
      tWsscfgIsSetDot11AuthenticationAlgorithmsEntry *));
INT4 WsscfgUtilUpdateDot11WEPDefaultKeysTable PROTO(
     (tWsscfgDot11WEPDefaultKeysEntry *,
      tWsscfgDot11WEPDefaultKeysEntry *,
      tWsscfgIsSetDot11WEPDefaultKeysEntry *));
INT4 WsscfgUtilUpdateDot11WEPKeyMappingsTable PROTO(
     (tWsscfgDot11WEPKeyMappingsEntry *,
      tWsscfgDot11WEPKeyMappingsEntry *,
      tWsscfgIsSetDot11WEPKeyMappingsEntry *));
INT4 WsscfgUtilUpdateDot11PrivacyTable PROTO(
     (tWsscfgDot11PrivacyEntry *,
      tWsscfgDot11PrivacyEntry *,
      tWsscfgIsSetDot11PrivacyEntry *));
INT4 WsscfgUtilUpdateDot11MultiDomainCapabilityTable PROTO(
     (tWsscfgDot11MultiDomainCapabilityEntry *,
      tWsscfgDot11MultiDomainCapabilityEntry *,
      tWsscfgIsSetDot11MultiDomainCapabilityEntry *));
INT4 WsscfgUtilUpdateDot11SpectrumManagementTable PROTO(
     (tWsscfgDot11SpectrumManagementEntry *,
      tWsscfgDot11SpectrumManagementEntry *,
      tWsscfgIsSetDot11SpectrumManagementEntry *));
INT4 WsscfgUtilUpdateDot11RegulatoryClassesTable PROTO(
     (tWsscfgDot11RegulatoryClassesEntry *,
      tWsscfgDot11RegulatoryClassesEntry *,
      tWsscfgIsSetDot11RegulatoryClassesEntry *));
INT4 WsscfgUtilUpdateDot11OperationTable PROTO(
     (tWsscfgDot11OperationEntry *,
      tWsscfgDot11OperationEntry *,
      tWsscfgIsSetDot11OperationEntry *));
INT4 WsscfgUtilUpdateDot11GroupAddressesTable PROTO(
     (tWsscfgDot11GroupAddressesEntry *,
      tWsscfgDot11GroupAddressesEntry *,
      tWsscfgIsSetDot11GroupAddressesEntry *));
INT4 WsscfgUtilUpdateDot11EDCATable PROTO(
     (tWsscfgDot11EDCAEntry *,
      tWsscfgDot11EDCAEntry *,
      tWsscfgIsSetDot11EDCAEntry *));
INT4 WsscfgUtilUpdateDot11QAPEDCATable PROTO(
     (tWsscfgDot11QAPEDCAEntry *,
      tWsscfgDot11QAPEDCAEntry *,
      tWsscfgIsSetDot11QAPEDCAEntry *));
INT4 WsscfgUtilUpdateDot11PhyOperationTable PROTO(
     (tWsscfgDot11PhyOperationEntry *,
      tWsscfgDot11PhyOperationEntry *,
      tWsscfgIsSetDot11PhyOperationEntry *));
INT4 WsscfgUtilUpdateDot11PhyAntennaTable PROTO(
     (tWsscfgDot11PhyAntennaEntry *,
      tWsscfgDot11PhyAntennaEntry *,
      tWsscfgIsSetDot11PhyAntennaEntry *));
INT4 WsscfgUtilUpdateDot11PhyTxPowerTable PROTO(
     (tWsscfgDot11PhyTxPowerEntry *,
      tWsscfgDot11PhyTxPowerEntry *,
      tWsscfgIsSetDot11PhyTxPowerEntry *));
INT4 WsscfgUtilUpdateDot11PhyFHSSTable PROTO(
     (tWsscfgDot11PhyFHSSEntry *,
      tWsscfgDot11PhyFHSSEntry *,
      tWsscfgIsSetDot11PhyFHSSEntry *));
INT4 WsscfgUtilUpdateDot11PhyDSSSTable PROTO(
     (tWsscfgDot11PhyDSSSEntry *,
      tWsscfgDot11PhyDSSSEntry *,
      tWsscfgIsSetDot11PhyDSSSEntry *));
INT4 WsscfgUtilUpdateDot11PhyIRTable PROTO(
     (tWsscfgDot11PhyIREntry *,
      tWsscfgDot11PhyIREntry *,
      tWsscfgIsSetDot11PhyIREntry *));
INT4 WsscfgUtilUpdateDot11AntennasListTable PROTO(
     (tWsscfgDot11AntennasListEntry *,
      tWsscfgDot11AntennasListEntry *,
      tWsscfgIsSetDot11AntennasListEntry *));
INT4 WsscfgUtilUpdateDot11PhyOFDMTable PROTO(
     (tWsscfgDot11PhyOFDMEntry *,
      tWsscfgDot11PhyOFDMEntry *,
      tWsscfgIsSetDot11PhyOFDMEntry *));
INT4 WsscfgUtilUpdateDot11HoppingPatternTable PROTO(
     (tWsscfgDot11HoppingPatternEntry *,
      tWsscfgDot11HoppingPatternEntry *,
      tWsscfgIsSetDot11HoppingPatternEntry *));
INT4 WsscfgUtilUpdateDot11PhyERPTable PROTO(
     (tWsscfgDot11PhyERPEntry *,
      tWsscfgDot11PhyERPEntry *,
      tWsscfgIsSetDot11PhyERPEntry *));
INT4 WsscfgUtilUpdateDot11RSNAConfigTable PROTO(
     (tWsscfgDot11RSNAConfigEntry *,
      tWsscfgDot11RSNAConfigEntry *,
      tWsscfgIsSetDot11RSNAConfigEntry *));
INT4 WsscfgUtilUpdateDot11RSNAConfigPairwiseCiphersTable PROTO(
     (tWsscfgDot11RSNAConfigPairwiseCiphersEntry *,
      tWsscfgDot11RSNAConfigPairwiseCiphersEntry *,
      tWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry *));
INT4 WsscfgUtilUpdateDot11RSNAConfigAuthenticationSuitesTable PROTO(
     (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *,
      tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *,
      tWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry *));
INT4 WsscfgSetAllDot11StationConfigTableTrigger PROTO(
     (tWsscfgDot11StationConfigEntry *,
      tWsscfgIsSetDot11StationConfigEntry *,
      INT4));
INT4 WsscfgSetAllDot11AuthenticationAlgorithmsTableTrigger PROTO(
     (tWsscfgDot11AuthenticationAlgorithmsEntry *,
      tWsscfgIsSetDot11AuthenticationAlgorithmsEntry *,
      INT4));
INT4 WsscfgSetAllDot11WEPDefaultKeysTableTrigger PROTO(
     (tWsscfgDot11WEPDefaultKeysEntry *,
      tWsscfgIsSetDot11WEPDefaultKeysEntry *,
      INT4));
INT4 WsscfgSetAllDot11WEPKeyMappingsTableTrigger PROTO(
     (tWsscfgDot11WEPKeyMappingsEntry *,
      tWsscfgIsSetDot11WEPKeyMappingsEntry *,
      INT4));
INT4 WsscfgSetAllDot11PrivacyTableTrigger PROTO(
     (tWsscfgDot11PrivacyEntry *,
      tWsscfgIsSetDot11PrivacyEntry *,
      INT4));
INT4 WsscfgSetAllDot11MultiDomainCapabilityTableTrigger PROTO(
     (tWsscfgDot11MultiDomainCapabilityEntry *,
      tWsscfgIsSetDot11MultiDomainCapabilityEntry *,
      INT4));
INT4 WsscfgSetAllDot11SpectrumManagementTableTrigger PROTO(
     (tWsscfgDot11SpectrumManagementEntry *,
      tWsscfgIsSetDot11SpectrumManagementEntry *,
      INT4));
INT4 WsscfgSetAllDot11RegulatoryClassesTableTrigger PROTO(
     (tWsscfgDot11RegulatoryClassesEntry *,
      tWsscfgIsSetDot11RegulatoryClassesEntry *,
      INT4));
INT4 WsscfgSetAllDot11OperationTableTrigger PROTO(
     (tWsscfgDot11OperationEntry *,
      tWsscfgIsSetDot11OperationEntry *,
      INT4));
INT4 WsscfgSetAllDot11GroupAddressesTableTrigger PROTO(
     (tWsscfgDot11GroupAddressesEntry *,
      tWsscfgIsSetDot11GroupAddressesEntry *,
      INT4));
INT4 WsscfgSetAllDot11EDCATableTrigger PROTO(
     (tWsscfgDot11EDCAEntry *,
      tWsscfgIsSetDot11EDCAEntry *,
      INT4));
INT4 WsscfgSetAllDot11QAPEDCATableTrigger PROTO(
     (tWsscfgDot11QAPEDCAEntry *,
      tWsscfgIsSetDot11QAPEDCAEntry *,
      INT4));
INT4 WsscfgSetAllDot11PhyOperationTableTrigger PROTO(
     (tWsscfgDot11PhyOperationEntry *,
      tWsscfgIsSetDot11PhyOperationEntry *,
      INT4));
INT4 WsscfgSetAllDot11PhyAntennaTableTrigger PROTO(
     (tWsscfgDot11PhyAntennaEntry *,
      tWsscfgIsSetDot11PhyAntennaEntry *,
      INT4));
INT4 WsscfgSetAllDot11PhyTxPowerTableTrigger PROTO(
     (tWsscfgDot11PhyTxPowerEntry *,
      tWsscfgIsSetDot11PhyTxPowerEntry *,
      INT4));
INT4 WsscfgSetAllDot11PhyFHSSTableTrigger PROTO(
     (tWsscfgDot11PhyFHSSEntry *,
      tWsscfgIsSetDot11PhyFHSSEntry *,
      INT4));
INT4 WsscfgSetAllDot11PhyDSSSTableTrigger PROTO(
     (tWsscfgDot11PhyDSSSEntry *,
      tWsscfgIsSetDot11PhyDSSSEntry *,
      INT4));
INT4 WsscfgSetAllDot11PhyIRTableTrigger PROTO(
     (tWsscfgDot11PhyIREntry *,
      tWsscfgIsSetDot11PhyIREntry *,
      INT4));
INT4 WsscfgSetAllDot11AntennasListTableTrigger PROTO(
     (tWsscfgDot11AntennasListEntry *,
      tWsscfgIsSetDot11AntennasListEntry *,
      INT4));
INT4 WsscfgSetAllDot11PhyOFDMTableTrigger PROTO(
     (tWsscfgDot11PhyOFDMEntry *,
      tWsscfgIsSetDot11PhyOFDMEntry *,
      INT4));
INT4 WsscfgSetAllDot11HoppingPatternTableTrigger PROTO(
     (tWsscfgDot11HoppingPatternEntry *,
      tWsscfgIsSetDot11HoppingPatternEntry *,
      INT4));
INT4 WsscfgSetAllDot11PhyERPTableTrigger PROTO(
     (tWsscfgDot11PhyERPEntry *,
      tWsscfgIsSetDot11PhyERPEntry *,
      INT4));
INT4 WsscfgSetAllDot11RSNAConfigTableTrigger PROTO(
     (tWsscfgDot11RSNAConfigEntry *,
      tWsscfgIsSetDot11RSNAConfigEntry *,
      INT4));
INT4 WsscfgSetAllDot11RSNAConfigPairwiseCiphersTableTrigger PROTO(
     (tWsscfgDot11RSNAConfigPairwiseCiphersEntry *,
      tWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry *,
      INT4));
INT4 WsscfgSetAllDot11RSNAConfigAuthenticationSuitesTableTrigger PROTO(
     (tWsscfgDot11RSNAConfigAuthenticationSuitesEntry *,
      tWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry *,
      INT4));

INT4 WsscfgCliSetCapwapDot11WlanTable(
    tCliHandle CliHandle,
    tWsscfgCapwapDot11WlanEntry * pWsscfgSetCapwapDot11WlanEntry,
    tWsscfgIsSetCapwapDot11WlanEntry *
    pWsscfgIsSetCapwapDot11WlanEntry);

INT4 WsscfgCliSetCapwapDot11WlanBindTable(
    tCliHandle CliHandle,
    tWsscfgCapwapDot11WlanBindEntry *
    pWsscfgSetCapwapDot11WlanBindEntry,
    tWsscfgIsSetCapwapDot11WlanBindEntry *
    pWsscfgIsSetCapwapDot11WlanBindEntry);


PUBLIC INT4 CapwapDot11WlanTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgCapwapDot11WlanTableCreate PROTO(
     (VOID));

tWsscfgCapwapDot11WlanEntry *WsscfgCapwapDot11WlanTableCreateApi PROTO(
     (tWsscfgCapwapDot11WlanEntry *));

PUBLIC INT4 CapwapDot11WlanBindTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgCapwapDot11WlanBindTableCreate PROTO(
     (VOID));

tWsscfgCapwapDot11WlanBindEntry
    *WsscfgCapwapDot11WlanBindTableCreateApi PROTO(
     (tWsscfgCapwapDot11WlanBindEntry *));
INT4 WsscfgGetAllCapwapDot11WlanTable PROTO(
     (tWsscfgCapwapDot11WlanEntry *));
INT4 WsscfgGetAllUtlCapwapDot11WlanTable PROTO(
     (tWsscfgCapwapDot11WlanEntry *,
      tWsscfgCapwapDot11WlanEntry *));
INT4 WsscfgGetAllCapwapDot11WlanBindTable PROTO(
     (tWsscfgCapwapDot11WlanBindEntry *));
INT4 WsscfgGetAllUtlCapwapDot11WlanBindTable PROTO(
     (tWsscfgCapwapDot11WlanBindEntry *,
      tWsscfgCapwapDot11WlanBindEntry *));
INT4 WsscfgSetAllCapwapDot11WlanTable PROTO(
     (tWsscfgCapwapDot11WlanEntry *,
      tWsscfgIsSetCapwapDot11WlanEntry *,
      INT4,
      INT4));
INT4 WsscfgSetAllCapwapDot11WlanBindTable PROTO(
     (tWsscfgCapwapDot11WlanBindEntry *,
      tWsscfgIsSetCapwapDot11WlanBindEntry *,
      INT4,
      INT4));
INT4 WsscfgCliSetFsSecurityWebAuthType(
    tCliHandle CliHandle,
    UINT4 * pFsSecurityWebAuthType,
    UINT4 * pWlanIfIndex);

INT4 WsscfgCliSetFsSecurityWebAuthUrl(
    tCliHandle CliHandle,
    UINT1 * pFsSecurityWebAuthUrl,
    UINT4 * pWlanIfIndex);

INT4 WsscfgCliSetFsSecurityWebAuthRedirectUrl(
    tCliHandle CliHandle,
    UINT4 * pFsSecurityWebAuthRedirectUrl);

INT4 WsscfgCliSetFsSecurityWebAddr(
    tCliHandle CliHandle,
    UINT4 * pFsSecurityWebAddr);

INT4 WsscfgCliSetFsSecurityWebAuthWebTitle(
    tCliHandle CliHandle,
    UINT1 * pFsSecurityWebAuthWebTitle);

INT4 WsscfgCliSetFsSecurityWebAuthWebMessage(
    tCliHandle CliHandle,
    UINT1 * pFsSecurityWebAuthWebMessage);

INT4 WsscfgCliSetFsSecurityWebAuthWebLogoFileName(
    tCliHandle CliHandle,
    UINT4 *,
    UINT1 * pFsSecurityWebAuthWebLogoFileName);

INT4 WsscfgCliSetFsSecurityWebAuthWebSuccMessage(
    tCliHandle CliHandle,
    UINT1 * pFsSecurityWebAuthWebSuccMessage);

INT4 WsscfgCliSetFsSecurityWebAuthWebFailMessage(
    tCliHandle CliHandle,
    UINT1 * pFsSecurityWebAuthWebFailMessage);

INT4 WsscfgCliSetFsSecurityWebAuthWebButtonText(
    tCliHandle CliHandle,
    UINT1 * pFsSecurityWebAuthWebButtonText);

INT4 WsscfgCliSetFsSecurityWebAuthWebLoadBalInfo(
    tCliHandle CliHandle,
    UINT1 * pFsSecurityWebAuthWebLoadBalInfo);

INT4 WsscfgCliSetFsSecurityWebAuthDisplayLang(
    tCliHandle CliHandle,
    UINT4 * pFsSecurityWebAuthDisplayLang);

INT4 WsscfgCliSetFsSecurityWebAuthColor(
    tCliHandle CliHandle,
    UINT4 * pFsSecurityWebAuthColor);







INT4 WsscfgInitializeCapwapDot11WlanTable PROTO(
     (tWsscfgCapwapDot11WlanEntry *));

INT4 WsscfgInitializeMibCapwapDot11WlanTable PROTO(
     (tWsscfgCapwapDot11WlanEntry *));

INT4 WsscfgInitializeCapwapDot11WlanBindTable PROTO(
     (tWsscfgCapwapDot11WlanBindEntry *));

INT4 WsscfgInitializeMibCapwapDot11WlanBindTable PROTO(
     (tWsscfgCapwapDot11WlanBindEntry *));
INT4 WsscfgTestAllCapwapDot11WlanTable PROTO(
     (UINT4 *,
      tWsscfgCapwapDot11WlanEntry *,
      tWsscfgIsSetCapwapDot11WlanEntry *,
      INT4,
      INT4));
INT4 WsscfgTestAllCapwapDot11WlanBindTable PROTO(
     (UINT4 *,
      tWsscfgCapwapDot11WlanBindEntry *,
      tWsscfgIsSetCapwapDot11WlanBindEntry *,
      INT4,
      INT4));
tWsscfgCapwapDot11WlanEntry *WsscfgGetCapwapDot11WlanTable PROTO(
     (tWsscfgCapwapDot11WlanEntry *));
tWsscfgCapwapDot11WlanBindEntry *WsscfgGetCapwapDot11WlanBindTable
PROTO(
     (tWsscfgCapwapDot11WlanBindEntry *));
tWsscfgCapwapDot11WlanEntry *WsscfgGetFirstCapwapDot11WlanTable PROTO(
     (VOID));
tWsscfgCapwapDot11WlanBindEntry
    *WsscfgGetFirstCapwapDot11WlanBindTable PROTO(
     (VOID));
tWsscfgCapwapDot11WlanEntry *WsscfgGetNextCapwapDot11WlanTable PROTO(
     (tWsscfgCapwapDot11WlanEntry *));
tWsscfgCapwapDot11WlanBindEntry *WsscfgGetNextCapwapDot11WlanBindTable
PROTO(
     (tWsscfgCapwapDot11WlanBindEntry *));
INT4 CapwapDot11WlanTableFilterInputs PROTO(
     (tWsscfgCapwapDot11WlanEntry *,
      tWsscfgCapwapDot11WlanEntry *,
      tWsscfgIsSetCapwapDot11WlanEntry *));
INT4 CapwapDot11WlanBindTableFilterInputs PROTO(
     (tWsscfgCapwapDot11WlanBindEntry *,
      tWsscfgCapwapDot11WlanBindEntry *,
      tWsscfgIsSetCapwapDot11WlanBindEntry *));
INT4 WsscfgUtilUpdateCapwapDot11WlanTable PROTO(
     (tWsscfgCapwapDot11WlanEntry *,
      tWsscfgCapwapDot11WlanEntry *,
      tWsscfgIsSetCapwapDot11WlanEntry *));
INT4 WsscfgUtilUpdateCapwapDot11WlanBindTable PROTO(
     (tWsscfgCapwapDot11WlanBindEntry *,
      tWsscfgCapwapDot11WlanBindEntry *,
      tWsscfgIsSetCapwapDot11WlanBindEntry *));
INT4 WsscfgSetAllCapwapDot11WlanTableTrigger PROTO(
     (tWsscfgCapwapDot11WlanEntry *,
      tWsscfgIsSetCapwapDot11WlanEntry *,
      INT4));
INT4 WsscfgSetAllCapwapDot11WlanBindTableTrigger PROTO(
     (tWsscfgCapwapDot11WlanBindEntry *,
      tWsscfgIsSetCapwapDot11WlanBindEntry *,
      INT4));


PUBLIC INT4 FsRrmConfigTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 FsDot11SupportedCountryTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsRrmConfigTableCreate PROTO(
     (VOID));

PUBLIC INT4 WsscfgFsDot11SupportedCountryTableCreate PROTO(
     (VOID));

tWsscfgFsRrmConfigEntry *WsscfgFsRrmConfigTableCreateApi PROTO(
     (tWsscfgFsRrmConfigEntry *));
INT4 WsscfgGetAllFsRrmConfigTable PROTO(
     (tWsscfgFsRrmConfigEntry *));
INT4 WsscfgGetAllUtlFsRrmConfigTable PROTO(
     (tWsscfgFsRrmConfigEntry *,
      tWsscfgFsRrmConfigEntry *));
INT4 WsscfgSetAllFsRrmConfigTable PROTO(
     (tWsscfgFsRrmConfigEntry *,
      tWsscfgIsSetFsRrmConfigEntry *,
      INT4,
      INT4));

INT4 WsscfgInitializeFsRrmConfigTable PROTO(
     (tWsscfgFsRrmConfigEntry *));

INT4 WsscfgInitializeMibFsRrmConfigTable PROTO(
     (tWsscfgFsRrmConfigEntry *));
INT4 WsscfgTestAllFsRrmConfigTable PROTO(
     (UINT4 *,
      tWsscfgFsRrmConfigEntry *,
      tWsscfgIsSetFsRrmConfigEntry *,
      INT4,
      INT4));
tWsscfgFsRrmConfigEntry *WsscfgGetFsRrmConfigTable PROTO(
     (tWsscfgFsRrmConfigEntry *));
tWsscfgFsRrmConfigEntry *WsscfgGetFirstFsRrmConfigTable PROTO(
     (VOID));
tWsscfgFsRrmConfigEntry *WsscfgGetNextFsRrmConfigTable PROTO(
     (tWsscfgFsRrmConfigEntry *));
INT4 FsRrmConfigTableFilterInputs PROTO(
     (tWsscfgFsRrmConfigEntry *,
      tWsscfgFsRrmConfigEntry *,
      tWsscfgIsSetFsRrmConfigEntry *));
INT4 WsscfgUtilUpdateFsRrmConfigTable PROTO(
     (tWsscfgFsRrmConfigEntry *,
      tWsscfgFsRrmConfigEntry *,
      tWsscfgIsSetFsRrmConfigEntry *));
INT4 WsscfgSetAllFsRrmConfigTableTrigger PROTO(
     (tWsscfgFsRrmConfigEntry *,
      tWsscfgIsSetFsRrmConfigEntry *,
      INT4));



INT4 WsscfgCliSetFsDot11aNetworkEnable(
    tCliHandle CliHandle,
    UINT4 * pFsDot11aNetworkEnable);


INT4 WsscfgCliSetFsDot11bNetworkEnable(
    tCliHandle CliHandle,
    UINT4 * pFsDot11bNetworkEnable);


INT4 WsscfgCliSetFsDot11gSupport(
    tCliHandle CliHandle,
    UINT4 * pFsDot11gSupport);


INT4 WsscfgCliSetFsDot11anSupport(
    tCliHandle CliHandle,
    UINT4 * pFsDot11anSupport);


INT4 WsscfgCliSetFsDot11bnSupport(
    tCliHandle CliHandle,
    UINT4 * pFsDot11bnSupport);


INT4 WsscfgCliSetFsDot11ManagmentSSID(
    tCliHandle CliHandle,
    UINT4 * pFsDot11ManagmentSSID);


INT4 WsscfgCliSetFsDot11CountryString(
    tCliHandle CliHandle,
    UINT4 * pFsDot11CountryString);
/* 

INT4 WsscfgCliSetFsSecurityWebAuthType (tCliHandle CliHandle,UINT4 *pFsSecurityWebAuthType);




INT4 WsscfgCliSetFsSecurityWebAuthRedirectUrl (tCliHandle CliHandle,UINT4 *pFsSecurityWebAuthRedirectUrl);


INT4 WsscfgCliSetFsSecurityWebAddr (tCliHandle CliHandle,UINT4 *pFsSecurityWebAddr);

INT4 WsscfgCliSetFsSecurityWebAuthDisplayLang (tCliHandle CliHandle,UINT4 *pFsSecurityWebAuthDisplayLang);


INT4 WsscfgCliSetFsSecurityWebAuthColor (tCliHandle CliHandle,UINT4 *pFsSecurityWebAuthColor); */


INT4 WsscfgCliSetFsDot11StationConfigTable(
    tCliHandle CliHandle,
    tWsscfgFsDot11StationConfigEntry *
    pWsscfgSetFsDot11StationConfigEntry,
    tWsscfgIsSetFsDot11StationConfigEntry *
    pWsscfgIsSetFsDot11StationConfigEntry);

INT4 WsscfgCliSetFsDot11CapabilityProfileTable(
    tCliHandle CliHandle,
    tWsscfgFsDot11CapabilityProfileEntry *
    pWsscfgSetFsDot11CapabilityProfileEntry,
    tWsscfgIsSetFsDot11CapabilityProfileEntry *
    pWsscfgIsSetFsDot11CapabilityProfileEntry);

INT4 WsscfgCliSetFsDot11AuthenticationProfileTable(
    tCliHandle CliHandle,
    tWsscfgFsDot11AuthenticationProfileEntry *
    pWsscfgSetFsDot11AuthenticationProfileEntry,
    tWsscfgIsSetFsDot11AuthenticationProfileEntry *
    pWsscfgIsSetFsDot11AuthenticationProfileEntry);

INT4 WsscfgCliSetFsSecurityWebAuthGuestInfoTable(
    tCliHandle CliHandle,
    tWsscfgFsSecurityWebAuthGuestInfoEntry *
    pWsscfgSetFsSecurityWebAuthGuestInfoEntry,
    tWsscfgIsSetFsSecurityWebAuthGuestInfoEntry *
    pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry);

INT4 WsscfgCliSetFsStationQosParamsTable(
    tCliHandle CliHandle,
    tWsscfgFsStationQosParamsEntry *
    pWsscfgSetFsStationQosParamsEntry,
    tWsscfgIsSetFsStationQosParamsEntry *
    pWsscfgIsSetFsStationQosParamsEntry);

INT4 WsscfgCliSetFsVlanIsolationTable(
    tCliHandle CliHandle,
    tWsscfgFsVlanIsolationEntry * pWsscfgSetFsVlanIsolationEntry,
    tWsscfgIsSetFsVlanIsolationEntry *
    pWsscfgIsSetFsVlanIsolationEntry);

INT4 WsscfgCliSetFsDot11RadioConfigTable(
    tCliHandle CliHandle,
    tWsscfgFsDot11RadioConfigEntry *
    pWsscfgSetFsDot11RadioConfigEntry,
    tWsscfgIsSetFsDot11RadioConfigEntry *
    pWsscfgIsSetFsDot11RadioConfigEntry);

INT4 WsscfgCliSetFsDot11QosProfileTable(
    tCliHandle CliHandle,
    tWsscfgFsDot11QosProfileEntry * pWsscfgSetFsDot11QosProfileEntry,
    tWsscfgIsSetFsDot11QosProfileEntry *
    pWsscfgIsSetFsDot11QosProfileEntry);

INT4 WsscfgCliSetFsDot11WlanCapabilityProfileTable(
    tCliHandle CliHandle,
    tWsscfgFsDot11WlanCapabilityProfileEntry *
    pWsscfgSetFsDot11WlanCapabilityProfileEntry,
    tWsscfgIsSetFsDot11WlanCapabilityProfileEntry *
    pWsscfgIsSetFsDot11WlanCapabilityProfileEntry);

INT4 WsscfgCliSetFsDot11WlanAuthenticationProfileTable(
    tCliHandle CliHandle,
    tWsscfgFsDot11WlanAuthenticationProfileEntry *
    pWsscfgSetFsDot11WlanAuthenticationProfileEntry,
    tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry *
    pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry);

INT4 WsscfgCliSetFsDot11WlanQosProfileTable(
    tCliHandle CliHandle,
    tWsscfgFsDot11WlanQosProfileEntry *
    pWsscfgSetFsDot11WlanQosProfileEntry,
    tWsscfgIsSetFsDot11WlanQosProfileEntry *
    pWsscfgIsSetFsDot11WlanQosProfileEntry);

INT4 WsscfgCliSetFsDot11RadioQosTable(
    tCliHandle CliHandle,
    tWsscfgFsDot11RadioQosEntry * pWsscfgSetFsDot11RadioQosEntry,
    tWsscfgIsSetFsDot11RadioQosEntry *
    pWsscfgIsSetFsDot11RadioQosEntry);

INT4 WsscfgCliSetFsDot11QAPTable(
    tCliHandle CliHandle,
    tWsscfgFsDot11QAPEntry * pWsscfgSetFsDot11QAPEntry,
    tWsscfgIsSetFsDot11QAPEntry * pWsscfgIsSetFsDot11QAPEntry);

INT4 WsscfgCliSetFsQAPProfileTable(
    tCliHandle CliHandle,
    tWsscfgFsQAPProfileEntry * pWsscfgSetFsQAPProfileEntry,
    tWsscfgIsSetFsQAPProfileEntry * pWsscfgIsSetFsQAPProfileEntry);

INT4 WsscfgCliSetFsDot11CapabilityMappingTable(
    tCliHandle CliHandle,
    tWsscfgFsDot11CapabilityMappingEntry *
    pWsscfgSetFsDot11CapabilityMappingEntry,
    tWsscfgIsSetFsDot11CapabilityMappingEntry *
    pWsscfgIsSetFsDot11CapabilityMappingEntry);

INT4 WsscfgCliSetFsDot11AuthMappingTable(
    tCliHandle CliHandle,
    tWsscfgFsDot11AuthMappingEntry *
    pWsscfgSetFsDot11AuthMappingEntry,
    tWsscfgIsSetFsDot11AuthMappingEntry *
    pWsscfgIsSetFsDot11AuthMappingEntry);

INT4 WsscfgCliSetFsDot11QosMappingTable(
    tCliHandle CliHandle,
    tWsscfgFsDot11QosMappingEntry * pWsscfgSetFsDot11QosMappingEntry,
    tWsscfgIsSetFsDot11QosMappingEntry *
    pWsscfgIsSetFsDot11QosMappingEntry);

PUBLIC INT4 FsDot11ClientSummaryTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 WsscfgFsDot11ClientSummaryTableCreate PROTO ((VOID));

INT4 WsscfgGetAllFsDot11ClientSummaryTable PROTO ((tWsscfgFsDot11ClientSummaryEntry *));
INT4 WsscfgGetAllUtlFsDot11ClientSummaryTable PROTO((tWsscfgFsDot11ClientSummaryEntry *, tWsscfgFsDot11ClientSummaryEntry *));
tWsscfgFsDot11ClientSummaryEntry * WsscfgGetFsDot11ClientSummaryTable PROTO ((tWsscfgFsDot11ClientSummaryEntry *));


INT4 WsscfgCliSetFsDot11AntennasListTable(
    tCliHandle CliHandle,
    tWsscfgFsDot11AntennasListEntry *
    pWsscfgSetFsDot11AntennasListEntry,
    tWsscfgIsSetFsDot11AntennasListEntry *
    pWsscfgIsSetFsDot11AntennasListEntry);

INT4 WsscfgCliSetFsDot11WlanTable(
    tCliHandle CliHandle,
    tWsscfgFsDot11WlanEntry * pWsscfgSetFsDot11WlanEntry,
    tWsscfgIsSetFsDot11WlanEntry * pWsscfgIsSetFsDot11WlanEntry);

INT4 WsscfgCliSetFsDot11WlanBindTable(
    tCliHandle CliHandle,
    tWsscfgFsDot11WlanBindEntry * pWsscfgSetFsDot11WlanBindEntry,
    tWsscfgIsSetFsDot11WlanBindEntry *
    pWsscfgIsSetFsDot11WlanBindEntry);

INT4
WssCfgCliSetMaxClientCountPerSSID (tCliHandle CliHandle,
                           UINT4 u4MaxClientCount,
                           UINT4 u4WlanProfileId);
INT4
WssCfgCliSetMaxClientCountPerRadio (tCliHandle CliHandle,
                                    UINT4 u4MaxClientCount,
                                    UINT1 *pu1ProfileName,
                                    UINT4 u4RadioId);
INT4 WsscfgCliSetFsWtpImageUpgradeTable(
    tCliHandle CliHandle,
    tWsscfgFsWtpImageUpgradeEntry * pWsscfgSetFsWtpImageUpgradeEntry,
    tWsscfgIsSetFsWtpImageUpgradeEntry *
    pWsscfgIsSetFsWtpImageUpgradeEntry);

INT4 WsscfgCliShowWtpImageUpgradeTable (
     tCliHandle CliHandle,
     UINT4 u4WtpProfileId,UINT1 *pu1WtpName );

INT4 WsscfgCliWtpModelProfileCmp (UINT4 u4WtpProfileId,
                             tWsscfgFsWtpImageUpgradeEntry *
                             pWsscfgSetFsWtpImageUpgradeEntry);

INT4 WsscfgCliSetFsDot11nConfigTable(
    tCliHandle CliHandle,
    tWsscfgFsDot11nConfigEntry * pWsscfgSetFsDot11nConfigEntry,
    tWsscfgIsSetFsDot11nConfigEntry *
    pWsscfgIsSetFsDot11nConfigEntry);

INT4 WsscfgCliSetFsDot11nMCSDataRateTable(
    tCliHandle CliHandle,
    tWsscfgFsDot11nMCSDataRateEntry *
    pWsscfgSetFsDot11nMCSDataRateEntry,
    tWsscfgIsSetFsDot11nMCSDataRateEntry *
    pWsscfgIsSetFsDot11nMCSDataRateEntry);

extern INT1 nmhGetIfType PROTO ((INT4 , INT4 *));
extern INT1 nmhGetIfName PROTO ((INT4 , tSNMP_OCTET_STRING_TYPE *));

PUBLIC INT4 FsDot11StationConfigTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsDot11StationConfigTableCreate PROTO(
     (VOID));

tWsscfgFsDot11StationConfigEntry
    *WsscfgFsDot11StationConfigTableCreateApi PROTO(
     (tWsscfgFsDot11StationConfigEntry *));

PUBLIC INT4 FsDot11CapabilityProfileTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsDot11CapabilityProfileTableCreate PROTO(
     (VOID));

tWsscfgFsDot11CapabilityProfileEntry
    * WsscfgFsDot11CapabilityProfileTableCreateApi PROTO((tWsscfgFsDot11CapabilityProfileEntry *));

PUBLIC INT4 FsDot11AuthenticationProfileTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsDot11AuthenticationProfileTableCreate PROTO(
     (VOID));

tWsscfgFsDot11AuthenticationProfileEntry
    * WsscfgFsDot11AuthenticationProfileTableCreateApi PROTO((tWsscfgFsDot11AuthenticationProfileEntry *));


PUBLIC INT4 FsStationQosParamsTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsStationQosParamsTableCreate PROTO(
     (VOID));

tWsscfgFsStationQosParamsEntry *WsscfgFsStationQosParamsTableCreateApi
PROTO(
     (tWsscfgFsStationQosParamsEntry *));

PUBLIC INT4 FsVlanIsolationTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsVlanIsolationTableCreate PROTO(
     (VOID));

tWsscfgFsVlanIsolationEntry *WsscfgFsVlanIsolationTableCreateApi PROTO(
     (tWsscfgFsVlanIsolationEntry *));

PUBLIC INT4 FsDot11RadioConfigTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsDot11RadioConfigTableCreate PROTO(
     (VOID));

tWsscfgFsDot11RadioConfigEntry *WsscfgFsDot11RadioConfigTableCreateApi
PROTO(
     (tWsscfgFsDot11RadioConfigEntry *));

PUBLIC INT4 FsDot11QosProfileTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsDot11QosProfileTableCreate PROTO(
     (VOID));

tWsscfgFsDot11QosProfileEntry *WsscfgFsDot11QosProfileTableCreateApi
PROTO(
     (tWsscfgFsDot11QosProfileEntry *));

PUBLIC INT4 FsDot11WlanCapabilityProfileTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsDot11WlanCapabilityProfileTableCreate PROTO(
     (VOID));

tWsscfgFsDot11WlanCapabilityProfileEntry
    * WsscfgFsDot11WlanCapabilityProfileTableCreateApi PROTO((tWsscfgFsDot11WlanCapabilityProfileEntry *));

PUBLIC INT4 FsDot11WlanAuthenticationProfileTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsDot11WlanAuthenticationProfileTableCreate PROTO(
     (VOID));

tWsscfgFsDot11WlanAuthenticationProfileEntry
    * WsscfgFsDot11WlanAuthenticationProfileTableCreateApi PROTO((tWsscfgFsDot11WlanAuthenticationProfileEntry *));

PUBLIC INT4 FsDot11WlanQosProfileTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsDot11WlanQosProfileTableCreate PROTO(
     (VOID));

tWsscfgFsDot11WlanQosProfileEntry
    * WsscfgFsDot11WlanQosProfileTableCreateApi PROTO((tWsscfgFsDot11WlanQosProfileEntry *));

PUBLIC INT4 FsDot11RadioQosTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsDot11RadioQosTableCreate PROTO(
     (VOID));

tWsscfgFsDot11RadioQosEntry *WsscfgFsDot11RadioQosTableCreateApi PROTO(
     (tWsscfgFsDot11RadioQosEntry *));

PUBLIC INT4 FsDot11QAPTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsDot11QAPTableCreate PROTO(
     (VOID));

tWsscfgFsDot11QAPEntry *WsscfgFsDot11QAPTableCreateApi PROTO(
     (tWsscfgFsDot11QAPEntry *));

PUBLIC INT4 FsQAPProfileTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsQAPProfileTableCreate PROTO(
     (VOID));

tWsscfgFsQAPProfileEntry *WsscfgFsQAPProfileTableCreateApi PROTO(
     (tWsscfgFsQAPProfileEntry *));

PUBLIC INT4 FsDot11CapabilityMappingTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsDot11CapabilityMappingTableCreate PROTO(
     (VOID));

tWsscfgFsDot11CapabilityMappingEntry
    * WsscfgFsDot11CapabilityMappingTableCreateApi PROTO((tWsscfgFsDot11CapabilityMappingEntry *));

PUBLIC INT4 FsDot11AuthMappingTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsDot11AuthMappingTableCreate PROTO(
     (VOID));

tWsscfgFsDot11AuthMappingEntry *WsscfgFsDot11AuthMappingTableCreateApi
PROTO(
     (tWsscfgFsDot11AuthMappingEntry *));

PUBLIC INT4 FsDot11QosMappingTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsDot11QosMappingTableCreate PROTO(
     (VOID));

tWsscfgFsDot11QosMappingEntry *WsscfgFsDot11QosMappingTableCreateApi
PROTO(
     (tWsscfgFsDot11QosMappingEntry *));

tWsscfgFsDot11ClientSummaryEntry * WsscfgFsDot11ClientSummaryTableCreateApi 
   PROTO ((tWsscfgFsDot11ClientSummaryEntry *));

PUBLIC INT4 FsDot11AntennasListTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsDot11AntennasListTableCreate PROTO(
     (VOID));

tWsscfgFsDot11AntennasListEntry
    *WsscfgFsDot11AntennasListTableCreateApi PROTO(
     (tWsscfgFsDot11AntennasListEntry *));

PUBLIC INT4 FsDot11WlanTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsDot11WlanTableCreate PROTO(
     (VOID));

tWsscfgFsDot11WlanEntry *WsscfgFsDot11WlanTableCreateApi PROTO(
     (tWsscfgFsDot11WlanEntry *));

PUBLIC INT4 FsDot11WlanBindTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsDot11WlanBindTableCreate PROTO(
     (VOID));

tWsscfgFsDot11WlanBindEntry *WsscfgFsDot11WlanBindTableCreateApi PROTO(
     (tWsscfgFsDot11WlanBindEntry *));

PUBLIC INT4 FsWtpImageUpgradeTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsWtpImageUpgradeTableCreate PROTO(
     (VOID));

tWsscfgFsWtpImageUpgradeEntry *WsscfgFsWtpImageUpgradeTableCreateApi
PROTO(
     (tWsscfgFsWtpImageUpgradeEntry *));

PUBLIC INT4 FsDot11nConfigTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsDot11nConfigTableCreate PROTO(
     (VOID));

tWsscfgFsDot11nConfigEntry *WsscfgFsDot11nConfigTableCreateApi PROTO(
     (tWsscfgFsDot11nConfigEntry *));

PUBLIC INT4 FsDot11nMCSDataRateTableRBCmp PROTO(
     (tRBElem *,
      tRBElem *));

PUBLIC INT4 WsscfgFsDot11nMCSDataRateTableCreate PROTO(
     (VOID));

tWsscfgFsDot11nMCSDataRateEntry
    *WsscfgFsDot11nMCSDataRateTableCreateApi PROTO(
     (tWsscfgFsDot11nMCSDataRateEntry *));

INT4 WsscfgGetFsDot11aNetworkEnable PROTO(
     (INT4 *));
INT4 WsscfgGetFsDot11bNetworkEnable PROTO(
     (INT4 *));
INT4 WsscfgGetFsDot11gSupport PROTO(
     (INT4 *));
INT4 WsscfgGetFsDot11anSupport PROTO(
     (INT4 *));
INT4 WsscfgGetFsDot11bnSupport PROTO(
     (INT4 *));
INT4 WsscfgGetFsDot11ManagmentSSID PROTO(
     (UINT4 *));
INT4 WsscfgGetFsDot11CountryString PROTO(
     (UINT1 *));
INT4 WsscfgGetFsSecurityWebAuthType PROTO(
     (INT4 *));
INT4 WsscfgGetFsSecurityWebAuthUrl PROTO(
     (UINT1 *));
INT4 WsscfgGetFsSecurityWebAuthRedirectUrl PROTO(
     (UINT1 *));
INT4 WsscfgGetFsSecurityWebAddr PROTO(
     (INT4 *));
INT4 WsscfgGetFsSecurityWebAuthWebTitle PROTO(
     (UINT1 *));
INT4 WsscfgGetFsSecurityWebAuthWebMessage PROTO(
     (UINT1 *));
INT4 WsscfgGetFsSecurityWebAuthWebLogoFileName PROTO(
     (UINT1 *));
INT4 WsscfgGetFsSecurityWebAuthWebSuccMessage PROTO(
     (UINT1 *));
INT4 WsscfgGetFsSecurityWebAuthWebFailMessage PROTO(
     (UINT1 *));
INT4 WsscfgGetFsSecurityWebAuthWebButtonText PROTO(
     (UINT1 *));
INT4 WsscfgGetFsSecurityWebAuthWebLoadBalInfo PROTO(
     (UINT1 *));
INT4 WsscfgGetFsSecurityWebAuthDisplayLang PROTO(
     (INT4 *));
INT4 WsscfgGetFsSecurityWebAuthColor PROTO(
     (INT4 *));
INT4 WsscfgGetAllFsDot11StationConfigTable PROTO(
     (tWsscfgFsDot11StationConfigEntry *));
INT4 WsscfgGetAllUtlFsDot11StationConfigTable PROTO(
     (tWsscfgFsDot11StationConfigEntry *,
      tWsscfgFsDot11StationConfigEntry *));
INT4 WsscfgGetAllFsDot11CapabilityProfileTable PROTO(
     (tWsscfgFsDot11CapabilityProfileEntry *));
INT4 WsscfgGetAllUtlFsDot11CapabilityProfileTable PROTO(
     (tWsscfgFsDot11CapabilityProfileEntry *,
      tWsscfgFsDot11CapabilityProfileEntry *));
INT4 WsscfgGetAllFsDot11AuthenticationProfileTable PROTO(
     (tWsscfgFsDot11AuthenticationProfileEntry *));
INT4 WsscfgGetAllUtlFsDot11AuthenticationProfileTable PROTO(
     (tWsscfgFsDot11AuthenticationProfileEntry *,
      tWsscfgFsDot11AuthenticationProfileEntry *));
INT4 WsscfgGetAllFsSecurityWebAuthGuestInfoTable PROTO(
     (tWsscfgFsSecurityWebAuthGuestInfoEntry *));
INT4 WsscfgGetAllUtlFsSecurityWebAuthGuestInfoTable PROTO(
     (tWsscfgFsSecurityWebAuthGuestInfoEntry *,
      tWsscfgFsSecurityWebAuthGuestInfoEntry *));
INT4 WsscfgGetAllFsStationQosParamsTable PROTO(
     (tWsscfgFsStationQosParamsEntry *));
INT4 WsscfgGetAllUtlFsStationQosParamsTable PROTO(
     (tWsscfgFsStationQosParamsEntry *,
      tWsscfgFsStationQosParamsEntry *));
INT4 WsscfgGetAllFsVlanIsolationTable PROTO(
     (tWsscfgFsVlanIsolationEntry *));
INT4 WsscfgGetAllUtlFsVlanIsolationTable PROTO(
     (tWsscfgFsVlanIsolationEntry *,
      tWsscfgFsVlanIsolationEntry *));
INT4 WsscfgGetAllFsDot11RadioConfigTable PROTO(
     (tWsscfgFsDot11RadioConfigEntry *));
INT4 WsscfgGetAllUtlFsDot11RadioConfigTable PROTO(
     (tWsscfgFsDot11RadioConfigEntry *,
      tWsscfgFsDot11RadioConfigEntry *));
INT4 WsscfgGetAllFsDot11QosProfileTable PROTO(
     (tWsscfgFsDot11QosProfileEntry *));
INT4 WsscfgGetAllUtlFsDot11QosProfileTable PROTO(
     (tWsscfgFsDot11QosProfileEntry *,
      tWsscfgFsDot11QosProfileEntry *));
INT4 WsscfgGetAllFsDot11WlanCapabilityProfileTable PROTO(
     (tWsscfgFsDot11WlanCapabilityProfileEntry *));
INT4 WsscfgGetAllUtlFsDot11WlanCapabilityProfileTable PROTO(
     (tWsscfgFsDot11WlanCapabilityProfileEntry *,
      tWsscfgFsDot11WlanCapabilityProfileEntry *));
INT4 WsscfgGetAllFsDot11WlanAuthenticationProfileTable PROTO(
     (tWsscfgFsDot11WlanAuthenticationProfileEntry *));
INT4 WsscfgGetAllUtlFsDot11WlanAuthenticationProfileTable PROTO(
     (tWsscfgFsDot11WlanAuthenticationProfileEntry *,
      tWsscfgFsDot11WlanAuthenticationProfileEntry *));
INT4 WsscfgGetAllFsDot11WlanQosProfileTable PROTO(
     (tWsscfgFsDot11WlanQosProfileEntry *));
INT4 WsscfgGetAllUtlFsDot11WlanQosProfileTable PROTO(
     (tWsscfgFsDot11WlanQosProfileEntry *,
      tWsscfgFsDot11WlanQosProfileEntry *));
INT4 WsscfgGetAllFsDot11RadioQosTable PROTO(
     (tWsscfgFsDot11RadioQosEntry *));
INT4 WsscfgGetAllUtlFsDot11RadioQosTable PROTO(
     (tWsscfgFsDot11RadioQosEntry *,
      tWsscfgFsDot11RadioQosEntry *));
INT4 WsscfgGetAllFsDot11QAPTable PROTO(
     (tWsscfgFsDot11QAPEntry *));
INT4 WsscfgGetAllUtlFsDot11QAPTable PROTO(
     (tWsscfgFsDot11QAPEntry *,
      tWsscfgFsDot11QAPEntry *));
INT4 WsscfgGetAllFsQAPProfileTable PROTO(
     (tWsscfgFsQAPProfileEntry *));
INT4 WsscfgGetAllUtlFsQAPProfileTable PROTO(
     (tWsscfgFsQAPProfileEntry *,
      tWsscfgFsQAPProfileEntry *));
INT4 WsscfgGetAllFsDot11CapabilityMappingTable PROTO(
     (tWsscfgFsDot11CapabilityMappingEntry *));
INT4 WsscfgGetAllUtlFsDot11CapabilityMappingTable PROTO(
     (tWsscfgFsDot11CapabilityMappingEntry *,
      tWsscfgFsDot11CapabilityMappingEntry *));
INT4 WsscfgGetAllFsDot11AuthMappingTable PROTO(
     (tWsscfgFsDot11AuthMappingEntry *));
INT4 WsscfgGetAllUtlFsDot11AuthMappingTable PROTO(
     (tWsscfgFsDot11AuthMappingEntry *,
      tWsscfgFsDot11AuthMappingEntry *));
INT4 WsscfgGetAllFsDot11QosMappingTable PROTO(
     (tWsscfgFsDot11QosMappingEntry *));
INT4 WsscfgGetAllUtlFsDot11QosMappingTable PROTO(
     (tWsscfgFsDot11QosMappingEntry *,
      tWsscfgFsDot11QosMappingEntry *));
INT4 WsscfgGetAllFsDot11AntennasListTable PROTO(
     (tWsscfgFsDot11AntennasListEntry *));
INT4 WsscfgGetAllUtlFsDot11AntennasListTable PROTO(
     (tWsscfgFsDot11AntennasListEntry *,
      tWsscfgFsDot11AntennasListEntry *));
INT4 WsscfgGetAllFsDot11WlanTable PROTO(
     (tWsscfgFsDot11WlanEntry *));
INT4 WsscfgGetAllUtlFsDot11WlanTable PROTO(
     (tWsscfgFsDot11WlanEntry *,
      tWsscfgFsDot11WlanEntry *));
INT4 WsscfgGetAllFsDot11WlanBindTable PROTO(
     (tWsscfgFsDot11WlanBindEntry *));
INT4 WsscfgGetAllUtlFsDot11WlanBindTable PROTO(
     (tWsscfgFsDot11WlanBindEntry *,
      tWsscfgFsDot11WlanBindEntry *));
INT4 WsscfgGetAllFsWtpImageUpgradeTable PROTO(
     (tWsscfgFsWtpImageUpgradeEntry *));
INT4 WsscfgGetAllUtlFsWtpImageUpgradeTable PROTO(
     (tWsscfgFsWtpImageUpgradeEntry *,
      tWsscfgFsWtpImageUpgradeEntry *));
INT4 WsscfgGetAllFsDot11nConfigTable PROTO(
     (tWsscfgFsDot11nConfigEntry *));
INT4 WsscfgGetAllUtlFsDot11nConfigTable PROTO(
     (tWsscfgFsDot11nConfigEntry *,
      tWsscfgFsDot11nConfigEntry *));
INT4 WsscfgGetAllFsDot11nMCSDataRateTable PROTO(
     (tWsscfgFsDot11nMCSDataRateEntry *));
INT4 WsscfgGetAllUtlFsDot11nMCSDataRateTable PROTO(
     (tWsscfgFsDot11nMCSDataRateEntry *,
      tWsscfgFsDot11nMCSDataRateEntry *));

INT4 WsscfgSetFsDot11aNetworkEnable PROTO(
     (INT4));
INT4 WsscfgSetFsDot11bNetworkEnable PROTO(
     (INT4));
INT4 WsscfgSetFsDot11gSupport PROTO(
     (INT4));
INT4 WsscfgSetFsDot11anSupport PROTO(
     (INT4));
INT4 WsscfgSetFsDot11bnSupport PROTO(
     (INT4));
INT4 WsscfgSetFsDot11ManagmentSSID PROTO(
     (UINT4));
INT4 WsscfgSetFsDot11CountryString PROTO(
     (UINT1 *));
INT4 WsscfgSetFsSecurityWebAuthType PROTO(
     (INT4));
INT4 WsscfgSetFsSecurityWebAuthUrl PROTO(
     (tSNMP_OCTET_STRING_TYPE *));
INT4 WsscfgSetFsSecurityWebAuthRedirectUrl PROTO(
     (UINT1 *));
INT4 WsscfgSetFsSecurityWebAddr PROTO(
     (INT4));
INT4 WsscfgSetFsSecurityWebAuthWebMessage PROTO(
     (tSNMP_OCTET_STRING_TYPE *));
INT4 WsscfgSetFsSecurityWebAuthWebTitle PROTO(
     (tSNMP_OCTET_STRING_TYPE *));
INT4 WsscfgSetFsSecurityWebAuthWebLogoFileName PROTO(
     (tSNMP_OCTET_STRING_TYPE *));
INT4 WsscfgSetFsSecurityWebAuthWebSuccMessage PROTO(
     (tSNMP_OCTET_STRING_TYPE *));
INT4 WsscfgSetFsSecurityWebAuthWebFailMessage PROTO(
     (tSNMP_OCTET_STRING_TYPE *));
INT4 WsscfgSetFsSecurityWebAuthWebButtonText PROTO(
     (tSNMP_OCTET_STRING_TYPE *));
INT4 WsscfgSetFsSecurityWebAuthWebLoadBalInfo PROTO(
     (tSNMP_OCTET_STRING_TYPE *));
INT4 WsscfgSetFsSecurityWebAuthDisplayLang PROTO(
     (INT4));
INT4 WsscfgSetFsSecurityWebAuthColor PROTO(
     (INT4));
INT4 WsscfgSetAllFsDot11StationConfigTable PROTO(
     (tWsscfgFsDot11StationConfigEntry *,
      tWsscfgIsSetFsDot11StationConfigEntry *));
INT4 WsscfgSetAllFsDot11CapabilityProfileTable PROTO(
     (tWsscfgFsDot11CapabilityProfileEntry *,
      tWsscfgIsSetFsDot11CapabilityProfileEntry *,
      INT4,
      INT4));
INT4 WsscfgSetAllFsDot11AuthenticationProfileTable PROTO(
     (tWsscfgFsDot11AuthenticationProfileEntry *,
      tWsscfgIsSetFsDot11AuthenticationProfileEntry *,
      INT4,
      INT4));
INT4 WsscfgSetAllFsSecurityWebAuthGuestInfoTable PROTO(
     (tWsscfgFsSecurityWebAuthGuestInfoEntry *,
      tWsscfgIsSetFsSecurityWebAuthGuestInfoEntry *,
      INT4,
      INT4));
INT4 WsscfgSetAllFsStationQosParamsTable PROTO(
     (tWsscfgFsStationQosParamsEntry *,
      tWsscfgIsSetFsStationQosParamsEntry *));
INT4 WsscfgSetAllFsVlanIsolationTable PROTO(
     (tWsscfgFsVlanIsolationEntry *,
      tWsscfgIsSetFsVlanIsolationEntry *));
INT4 WsscfgSetAllFsDot11RadioConfigTable PROTO(
     (tWsscfgFsDot11RadioConfigEntry *,
      tWsscfgIsSetFsDot11RadioConfigEntry *,
      INT4,
      INT4));
INT4 WsscfgSetAllFsDot11QosProfileTable PROTO(
     (tWsscfgFsDot11QosProfileEntry *,
      tWsscfgIsSetFsDot11QosProfileEntry *,
      INT4,
      INT4));
INT4 WsscfgSetAllFsDot11WlanCapabilityProfileTable PROTO(
     (tWsscfgFsDot11WlanCapabilityProfileEntry *,
      tWsscfgIsSetFsDot11WlanCapabilityProfileEntry *,
      INT4,
      INT4));
INT4 WsscfgSetAllFsDot11WlanAuthenticationProfileTable PROTO(
     (tWsscfgFsDot11WlanAuthenticationProfileEntry *,
      tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry *,
      INT4,
      INT4));
INT4 WsscfgSetAllFsDot11WlanQosProfileTable PROTO(
     (tWsscfgFsDot11WlanQosProfileEntry *,
      tWsscfgIsSetFsDot11WlanQosProfileEntry *,
      INT4,
      INT4));
INT4 WsscfgSetAllFsDot11RadioQosTable PROTO(
     (tWsscfgFsDot11RadioQosEntry *,
      tWsscfgIsSetFsDot11RadioQosEntry *));
INT4 WsscfgSetAllFsDot11QAPTable PROTO(
     (tWsscfgFsDot11QAPEntry *,
      tWsscfgIsSetFsDot11QAPEntry *));
INT4 WsscfgSetAllFsQAPProfileTable PROTO(
     (tWsscfgFsQAPProfileEntry *,
      tWsscfgIsSetFsQAPProfileEntry *));
INT4 WsscfgSetAllFsDot11CapabilityMappingTable PROTO(
     (tWsscfgFsDot11CapabilityMappingEntry *,
      tWsscfgIsSetFsDot11CapabilityMappingEntry *,
      INT4,
      INT4));
INT4 WsscfgSetAllFsDot11AuthMappingTable PROTO(
     (tWsscfgFsDot11AuthMappingEntry *,
      tWsscfgIsSetFsDot11AuthMappingEntry *,
      INT4,
      INT4));
INT4 WsscfgSetAllFsDot11QosMappingTable PROTO(
     (tWsscfgFsDot11QosMappingEntry *,
      tWsscfgIsSetFsDot11QosMappingEntry *,
      INT4,
      INT4));
INT4 WsscfgSetAllFsDot11AntennasListTable PROTO(
     (tWsscfgFsDot11AntennasListEntry *,
      tWsscfgIsSetFsDot11AntennasListEntry *));
INT4 WsscfgSetAllFsDot11WlanTable PROTO(
     (tWsscfgFsDot11WlanEntry *,
      tWsscfgIsSetFsDot11WlanEntry *,
      INT4,
      INT4));
INT4 WsscfgSetAllFsDot11WlanBindTable PROTO(
     (tWsscfgFsDot11WlanBindEntry *,
      tWsscfgIsSetFsDot11WlanBindEntry *,
      INT4,
      INT4));
INT4 WsscfgSetAllFsWtpImageUpgradeTable PROTO(
     (tWsscfgFsWtpImageUpgradeEntry *,
      tWsscfgIsSetFsWtpImageUpgradeEntry *,
      INT4,
      INT4));
INT4 WsscfgSetAllFsDot11nConfigTable PROTO(
     (tWsscfgFsDot11nConfigEntry *,
      tWsscfgIsSetFsDot11nConfigEntry *));
INT4 WsscfgSetAllFsDot11nMCSDataRateTable PROTO(
     (tWsscfgFsDot11nMCSDataRateEntry *,
      tWsscfgIsSetFsDot11nMCSDataRateEntry *));

INT4 WsscfgInitializeFsDot11CapabilityProfileTable PROTO(
     (tWsscfgFsDot11CapabilityProfileEntry *));

INT4 WsscfgInitializeMibFsDot11CapabilityProfileTable PROTO(
     (tWsscfgFsDot11CapabilityProfileEntry *));

INT4 WsscfgInitializeFsDot11AuthenticationProfileTable PROTO(
     (tWsscfgFsDot11AuthenticationProfileEntry *));

INT4 WsscfgInitializeMibFsDot11AuthenticationProfileTable PROTO(
     (tWsscfgFsDot11AuthenticationProfileEntry *));

INT4 WsscfgInitializeFsDot11RadioConfigTable PROTO(
     (tWsscfgFsDot11RadioConfigEntry *));

INT4 WsscfgInitializeMibFsDot11RadioConfigTable PROTO(
     (tWsscfgFsDot11RadioConfigEntry *));

INT4 WsscfgInitializeFsDot11QosProfileTable PROTO(
     (tWsscfgFsDot11QosProfileEntry *));

INT4 WsscfgInitializeMibFsDot11QosProfileTable PROTO(
     (tWsscfgFsDot11QosProfileEntry *));

INT4 WsscfgInitializeFsDot11WlanCapabilityProfileTable PROTO(
     (tWsscfgFsDot11WlanCapabilityProfileEntry *));

INT4 WsscfgInitializeMibFsDot11WlanCapabilityProfileTable PROTO(
     (tWsscfgFsDot11WlanCapabilityProfileEntry *));

INT4 WsscfgInitializeFsDot11WlanAuthenticationProfileTable PROTO(
     (tWsscfgFsDot11WlanAuthenticationProfileEntry *));

INT4 WsscfgInitializeMibFsDot11WlanAuthenticationProfileTable PROTO(
     (tWsscfgFsDot11WlanAuthenticationProfileEntry *));

INT4 WsscfgInitializeFsDot11WlanQosProfileTable PROTO(
     (tWsscfgFsDot11WlanQosProfileEntry *));

INT4 WsscfgInitializeMibFsDot11WlanQosProfileTable PROTO(
     (tWsscfgFsDot11WlanQosProfileEntry *));

INT4 WsscfgInitializeFsDot11CapabilityMappingTable PROTO(
     (tWsscfgFsDot11CapabilityMappingEntry *));

INT4 WsscfgInitializeMibFsDot11CapabilityMappingTable PROTO(
     (tWsscfgFsDot11CapabilityMappingEntry *));

INT4 WsscfgInitializeFsDot11AuthMappingTable PROTO(
     (tWsscfgFsDot11AuthMappingEntry *));

INT4 WsscfgInitializeMibFsDot11AuthMappingTable PROTO(
     (tWsscfgFsDot11AuthMappingEntry *));

INT4 WsscfgInitializeFsDot11QosMappingTable PROTO(
     (tWsscfgFsDot11QosMappingEntry *));

INT4 WsscfgInitializeMibFsDot11QosMappingTable PROTO(
     (tWsscfgFsDot11QosMappingEntry *));

INT4 WsscfgInitializeFsDot11WlanTable PROTO(
     (tWsscfgFsDot11WlanEntry *));

INT4 WsscfgInitializeMibFsDot11WlanTable PROTO(
     (tWsscfgFsDot11WlanEntry *));

INT4 WsscfgInitializeFsDot11WlanBindTable PROTO(
     (tWsscfgFsDot11WlanBindEntry *));

INT4 WsscfgInitializeMibFsDot11WlanBindTable PROTO(
     (tWsscfgFsDot11WlanBindEntry *));

INT4 WsscfgInitializeFsWtpImageUpgradeTable PROTO(
     (tWsscfgFsWtpImageUpgradeEntry *));

INT4 WsscfgInitializeMibFsWtpImageUpgradeTable PROTO(
     (tWsscfgFsWtpImageUpgradeEntry *));
INT4 WsscfgTestFsDot11aNetworkEnable PROTO(
     (UINT4 *,
      INT4));
INT4 WsscfgTestFsDot11bNetworkEnable PROTO(
     (UINT4 *,
      INT4));
INT4 WsscfgTestFsDot11gSupport PROTO(
     (UINT4 *,
      INT4));
INT4 WsscfgTestFsDot11anSupport PROTO(
     (UINT4 *,
      INT4));
INT4 WsscfgTestFsDot11bnSupport PROTO(
     (UINT4 *,
      INT4));
INT4 WsscfgTestFsDot11ManagmentSSID PROTO(
     (UINT4 *,
      UINT4));
INT4 WsscfgTestFsDot11CountryString PROTO(
     (UINT4 *,
      UINT1 *));
INT4 WsscfgTestFsSecurityWebAuthType PROTO(
     (UINT4 *,
      INT4));
INT4 WsscfgTestFsSecurityWebAuthUrl PROTO(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *));
INT4 WsscfgTestFsSecurityWebAuthRedirectUrl PROTO(
     (UINT4 *,
      UINT1 *));
INT4 WsscfgTestFsSecurityWebAddr PROTO(
     (UINT4 *,
      INT4));
INT4 WsscfgTestFsSecurityWebAuthWebMessage PROTO(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *));
INT4 WsscfgTestFsSecurityWebAuthWebTitle PROTO(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *));
INT4 WsscfgTestFsSecurityWebAuthWebLogoFileName PROTO(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *));
INT4 WsscfgTestFsSecurityWebAuthWebSuccMessage PROTO(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *));
INT4 WsscfgTestFsSecurityWebAuthWebFailMessage PROTO(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *));
INT4 WsscfgTestFsSecurityWebAuthWebButtonText PROTO(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *));
INT4 WsscfgTestFsSecurityWebAuthWebLoadBalInfo PROTO(
     (UINT4 *,
      tSNMP_OCTET_STRING_TYPE *));
INT4 WsscfgTestFsSecurityWebAuthDisplayLang PROTO(
     (UINT4 *,
      INT4));
INT4 WsscfgTestFsSecurityWebAuthColor PROTO(
     (UINT4 *,
      INT4));
INT4 WsscfgTestAllFsDot11StationConfigTable PROTO(
     (UINT4 *,
      tWsscfgFsDot11StationConfigEntry *,
      tWsscfgIsSetFsDot11StationConfigEntry *));
INT4 WsscfgTestAllFsDot11CapabilityProfileTable PROTO(
     (UINT4 *,
      tWsscfgFsDot11CapabilityProfileEntry *,
      tWsscfgIsSetFsDot11CapabilityProfileEntry *,
      INT4,
      INT4));
INT4 WsscfgTestAllFsDot11AuthenticationProfileTable PROTO(
     (UINT4 *,
      tWsscfgFsDot11AuthenticationProfileEntry *,
      tWsscfgIsSetFsDot11AuthenticationProfileEntry *,
      INT4,
      INT4));
INT4 WsscfgTestAllFsSecurityWebAuthGuestInfoTable PROTO(
     (UINT4 *,
      tWsscfgFsSecurityWebAuthGuestInfoEntry *,
      tWsscfgIsSetFsSecurityWebAuthGuestInfoEntry *,
      INT4,
      INT4));
INT4 WsscfgTestAllFsStationQosParamsTable PROTO(
     (UINT4 *,
      tWsscfgFsStationQosParamsEntry *,
      tWsscfgIsSetFsStationQosParamsEntry *));
INT4 WsscfgTestAllFsVlanIsolationTable PROTO(
     (UINT4 *,
      tWsscfgFsVlanIsolationEntry *,
      tWsscfgIsSetFsVlanIsolationEntry *));
INT4 WsscfgTestAllFsDot11RadioConfigTable PROTO(
     (UINT4 *,
      tWsscfgFsDot11RadioConfigEntry *,
      tWsscfgIsSetFsDot11RadioConfigEntry *,
      INT4,
      INT4));
INT4 WsscfgTestAllFsDot11QosProfileTable PROTO(
     (UINT4 *,
      tWsscfgFsDot11QosProfileEntry *,
      tWsscfgIsSetFsDot11QosProfileEntry *,
      INT4,
      INT4));
INT4 WsscfgTestAllFsDot11WlanCapabilityProfileTable PROTO(
     (UINT4 *,
      tWsscfgFsDot11WlanCapabilityProfileEntry *,
      tWsscfgIsSetFsDot11WlanCapabilityProfileEntry *,
      INT4,
      INT4));
INT4 WsscfgTestAllFsDot11WlanAuthenticationProfileTable PROTO(
     (UINT4 *,
      tWsscfgFsDot11WlanAuthenticationProfileEntry *,
      tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry *,
      INT4,
      INT4));
INT4 WsscfgTestAllFsDot11WlanQosProfileTable PROTO(
     (UINT4 *,
      tWsscfgFsDot11WlanQosProfileEntry *,
      tWsscfgIsSetFsDot11WlanQosProfileEntry *,
      INT4,
      INT4));
INT4 WsscfgTestAllFsDot11RadioQosTable PROTO(
     (UINT4 *,
      tWsscfgFsDot11RadioQosEntry *,
      tWsscfgIsSetFsDot11RadioQosEntry *));
INT4 WsscfgTestAllFsDot11QAPTable PROTO(
     (UINT4 *,
      tWsscfgFsDot11QAPEntry *,
      tWsscfgIsSetFsDot11QAPEntry *));
INT4 WsscfgTestAllFsQAPProfileTable PROTO(
     (UINT4 *,
      tWsscfgFsQAPProfileEntry *,
      tWsscfgIsSetFsQAPProfileEntry *));
INT4 WsscfgTestAllFsDot11CapabilityMappingTable PROTO(
     (UINT4 *,
      tWsscfgFsDot11CapabilityMappingEntry *,
      tWsscfgIsSetFsDot11CapabilityMappingEntry *,
      INT4,
      INT4));
INT4 WsscfgTestAllFsDot11AuthMappingTable PROTO(
     (UINT4 *,
      tWsscfgFsDot11AuthMappingEntry *,
      tWsscfgIsSetFsDot11AuthMappingEntry *,
      INT4,
      INT4));
INT4 WsscfgTestAllFsDot11QosMappingTable PROTO(
     (UINT4 *,
      tWsscfgFsDot11QosMappingEntry *,
      tWsscfgIsSetFsDot11QosMappingEntry *,
      INT4,
      INT4));
INT4 WsscfgTestAllFsDot11AntennasListTable PROTO(
     (UINT4 *,
      tWsscfgFsDot11AntennasListEntry *,
      tWsscfgIsSetFsDot11AntennasListEntry *));
INT4 WsscfgTestAllFsDot11WlanTable PROTO(
     (UINT4 *,
      tWsscfgFsDot11WlanEntry *,
      tWsscfgIsSetFsDot11WlanEntry *,
      INT4,
      INT4));
INT4 WsscfgTestAllFsDot11WlanBindTable PROTO(
     (UINT4 *,
      tWsscfgFsDot11WlanBindEntry *,
      tWsscfgIsSetFsDot11WlanBindEntry *,
      INT4,
      INT4));
INT4 WsscfgTestAllFsWtpImageUpgradeTable PROTO(
     (UINT4 *,
      tWsscfgFsWtpImageUpgradeEntry *,
      tWsscfgIsSetFsWtpImageUpgradeEntry *,
      INT4,
      INT4));
INT4 WsscfgTestAllFsDot11nConfigTable PROTO(
     (UINT4 *,
      tWsscfgFsDot11nConfigEntry *,
      tWsscfgIsSetFsDot11nConfigEntry *));
INT4 WsscfgTestAllFsDot11nMCSDataRateTable PROTO(
     (UINT4 *,
      tWsscfgFsDot11nMCSDataRateEntry *,
      tWsscfgIsSetFsDot11nMCSDataRateEntry *));

tWsscfgFsDot11StationConfigEntry *WsscfgGetFsDot11StationConfigTable
PROTO(
     (tWsscfgFsDot11StationConfigEntry *));
tWsscfgFsDot11CapabilityProfileEntry
    * WsscfgGetFsDot11CapabilityProfileTable PROTO((tWsscfgFsDot11CapabilityProfileEntry *));
tWsscfgFsDot11AuthenticationProfileEntry
    * WsscfgGetFsDot11AuthenticationProfileTable PROTO((tWsscfgFsDot11AuthenticationProfileEntry *));
tWsscfgFsStationQosParamsEntry *WsscfgGetFsStationQosParamsTable PROTO(
     (tWsscfgFsStationQosParamsEntry *));
tWsscfgFsVlanIsolationEntry *WsscfgGetFsVlanIsolationTable PROTO(
     (tWsscfgFsVlanIsolationEntry *));
tWsscfgFsDot11RadioConfigEntry *WsscfgGetFsDot11RadioConfigTable PROTO(
     (tWsscfgFsDot11RadioConfigEntry *));
tWsscfgFsDot11QosProfileEntry *WsscfgGetFsDot11QosProfileTable PROTO(
     (tWsscfgFsDot11QosProfileEntry *));
tWsscfgFsDot11WlanCapabilityProfileEntry
    * WsscfgGetFsDot11WlanCapabilityProfileTable PROTO((tWsscfgFsDot11WlanCapabilityProfileEntry *));
tWsscfgFsDot11WlanAuthenticationProfileEntry
    * WsscfgGetFsDot11WlanAuthenticationProfileTable PROTO((tWsscfgFsDot11WlanAuthenticationProfileEntry *));
tWsscfgFsDot11WlanQosProfileEntry *WsscfgGetFsDot11WlanQosProfileTable
PROTO(
     (tWsscfgFsDot11WlanQosProfileEntry *));
tWsscfgFsDot11RadioQosEntry *WsscfgGetFsDot11RadioQosTable PROTO(
     (tWsscfgFsDot11RadioQosEntry *));
tWsscfgFsDot11QAPEntry *WsscfgGetFsDot11QAPTable PROTO(
     (tWsscfgFsDot11QAPEntry *));
tWsscfgFsQAPProfileEntry *WsscfgGetFsQAPProfileTable PROTO(
     (tWsscfgFsQAPProfileEntry *));
tWsscfgFsDot11CapabilityMappingEntry
    * WsscfgGetFsDot11CapabilityMappingTable PROTO((tWsscfgFsDot11CapabilityMappingEntry *));
tWsscfgFsDot11AuthMappingEntry *WsscfgGetFsDot11AuthMappingTable PROTO(
     (tWsscfgFsDot11AuthMappingEntry *));
tWsscfgFsDot11QosMappingEntry *WsscfgGetFsDot11QosMappingTable PROTO(
     (tWsscfgFsDot11QosMappingEntry *));
tWsscfgFsDot11AntennasListEntry *WsscfgGetFsDot11AntennasListTable
PROTO(
     (tWsscfgFsDot11AntennasListEntry *));
tWsscfgFsDot11WlanEntry *WsscfgGetFsDot11WlanTable PROTO(
     (tWsscfgFsDot11WlanEntry *));
tWsscfgFsDot11WlanBindEntry *WsscfgGetFsDot11WlanBindTable PROTO(
     (tWsscfgFsDot11WlanBindEntry *));
tWsscfgFsWtpImageUpgradeEntry *WsscfgGetFsWtpImageUpgradeTable PROTO(
     (tWsscfgFsWtpImageUpgradeEntry *));
tWsscfgFsDot11nConfigEntry *WsscfgGetFsDot11nConfigTable PROTO(
     (tWsscfgFsDot11nConfigEntry *));
tWsscfgFsDot11nMCSDataRateEntry *WsscfgGetFsDot11nMCSDataRateTable
PROTO(
     (tWsscfgFsDot11nMCSDataRateEntry *));

tWsscfgFsDot11StationConfigEntry
    *WsscfgGetFirstFsDot11StationConfigTable PROTO(
     (VOID));
tWsscfgFsDot11CapabilityProfileEntry
    * WsscfgGetFirstFsDot11CapabilityProfileTable PROTO((VOID));
tWsscfgFsDot11AuthenticationProfileEntry
    * WsscfgGetFirstFsDot11AuthenticationProfileTable PROTO((VOID));

    
INT4 WsscfgGetFirstFsSecurityWebAuthGuestInfoTable PROTO
                  ((tSNMP_OCTET_STRING_TYPE *));
tWsscfgFsStationQosParamsEntry *WsscfgGetFirstFsStationQosParamsTable
PROTO(
     (VOID));
tWsscfgFsVlanIsolationEntry *WsscfgGetFirstFsVlanIsolationTable PROTO(
     (VOID));
INT4 WsscfgGetFirstFsDot11RadioConfigTable PROTO((INT4 *));
tWsscfgFsDot11QosProfileEntry *WsscfgGetFirstFsDot11QosProfileTable
PROTO(
     (VOID));
tWsscfgFsDot11WlanCapabilityProfileEntry
    * WsscfgGetFirstFsDot11WlanCapabilityProfileTable PROTO((VOID));
tWsscfgFsDot11WlanAuthenticationProfileEntry
    * WsscfgGetFirstFsDot11WlanAuthenticationProfileTable PROTO((VOID));
tWsscfgFsDot11WlanQosProfileEntry
    *WsscfgGetFirstFsDot11WlanQosProfileTable PROTO(
     (VOID));
tWssIfAuthStateDB * WsscfgGetFirstFsDot11ClientSummaryTable PROTO((VOID));
INT4 WsscfgGetFirstFsDot11RadioQosTable PROTO((INT4 *));
tWsscfgFsDot11QAPEntry *WsscfgGetFirstFsDot11QAPTable PROTO(
     (VOID));
tWsscfgFsQAPProfileEntry *WsscfgGetFirstFsQAPProfileTable PROTO(
     (VOID));
tWsscfgFsDot11CapabilityMappingEntry
    * WsscfgGetFirstFsDot11CapabilityMappingTable PROTO((VOID));
tWsscfgFsDot11AuthMappingEntry *WsscfgGetFirstFsDot11AuthMappingTable
PROTO(
     (VOID));
tWsscfgFsDot11QosMappingEntry *WsscfgGetFirstFsDot11QosMappingTable
PROTO(
     (VOID));

INT4 WsscfgGetFirstFsDot11AntennasListTable PROTO((INT4 *, INT4 *));
tWsscfgFsDot11WlanEntry *WsscfgGetFirstFsDot11WlanTable PROTO(
     (VOID));
tWsscfgFsDot11WlanBindEntry *WsscfgGetFirstFsDot11WlanBindTable PROTO(
     (VOID));
tWsscfgFsWtpImageUpgradeEntry *WsscfgGetFirstFsWtpImageUpgradeTable
PROTO(
     (VOID));
INT1 WsscfgGetFirstFsDot11nConfigTable PROTO(
     (INT4 *));
INT1 WsscfgGetFirstFsDot11nMCSDataRateTable PROTO(
     (INT4 *));
tWsscfgFsDot11StationConfigEntry
    *WsscfgGetNextFsDot11StationConfigTable PROTO(
     (tWsscfgFsDot11StationConfigEntry *));
tWsscfgFsDot11CapabilityProfileEntry
    * WsscfgGetNextFsDot11CapabilityProfileTable PROTO((tWsscfgFsDot11CapabilityProfileEntry *));
tWsscfgFsDot11AuthenticationProfileEntry
    * WsscfgGetNextFsDot11AuthenticationProfileTable PROTO((tWsscfgFsDot11AuthenticationProfileEntry *));

INT4 WsscfgGetNextFsSecurityWebAuthGuestInfoTable PROTO((tSNMP_OCTET_STRING_TYPE *,
                                                         tSNMP_OCTET_STRING_TYPE *));
tWsscfgFsStationQosParamsEntry *WsscfgGetNextFsStationQosParamsTable
PROTO(
     (tWsscfgFsStationQosParamsEntry *));
tWsscfgFsVlanIsolationEntry *WsscfgGetNextFsVlanIsolationTable PROTO(
     (tWsscfgFsVlanIsolationEntry *));
INT4 WsscfgGetNextFsDot11RadioConfigTable PROTO((INT4, INT4 *));
tWsscfgFsDot11QosProfileEntry *WsscfgGetNextFsDot11QosProfileTable
PROTO(
     (tWsscfgFsDot11QosProfileEntry *));
tWsscfgFsDot11WlanCapabilityProfileEntry
    * WsscfgGetNextFsDot11WlanCapabilityProfileTable PROTO((tWsscfgFsDot11WlanCapabilityProfileEntry *));
tWsscfgFsDot11WlanAuthenticationProfileEntry
    * WsscfgGetNextFsDot11WlanAuthenticationProfileTable PROTO((tWsscfgFsDot11WlanAuthenticationProfileEntry *));
tWsscfgFsDot11WlanQosProfileEntry
    *WsscfgGetNextFsDot11WlanQosProfileTable PROTO(
     (tWsscfgFsDot11WlanQosProfileEntry *));
INT1 WsscfgGetNextFsDot11RadioQosTable PROTO(
     (INT4 ,INT4 * ));
tWsscfgFsDot11QAPEntry *WsscfgGetNextFsDot11QAPTable PROTO(
     (tWsscfgFsDot11QAPEntry *));
tWsscfgFsQAPProfileEntry *WsscfgGetNextFsQAPProfileTable PROTO(
     (tWsscfgFsQAPProfileEntry *));
tWsscfgFsDot11CapabilityMappingEntry
    * WsscfgGetNextFsDot11CapabilityMappingTable PROTO((tWsscfgFsDot11CapabilityMappingEntry *));
tWssIfAuthStateDB * WsscfgGetNextFsDot11ClientSummaryTable PROTO((tWssIfAuthStateDB *));
tWsscfgFsDot11AuthMappingEntry *WsscfgGetNextFsDot11AuthMappingTable
PROTO(
     (tWsscfgFsDot11AuthMappingEntry *));
tWsscfgFsDot11QosMappingEntry *WsscfgGetNextFsDot11QosMappingTable
PROTO(
     (tWsscfgFsDot11QosMappingEntry *));
INT4 WsscfgGetNextFsDot11AntennasListTable
PROTO((INT4, INT4 *, INT4, INT4 *));
     
tWsscfgFsDot11WlanEntry *WsscfgGetNextFsDot11WlanTable PROTO(
     (tWsscfgFsDot11WlanEntry *));
tWsscfgFsDot11WlanBindEntry *WsscfgGetNextFsDot11WlanBindTable PROTO(
     (tWsscfgFsDot11WlanBindEntry *));
tWsscfgFsWtpImageUpgradeEntry *WsscfgGetNextFsWtpImageUpgradeTable
PROTO(
     (tWsscfgFsWtpImageUpgradeEntry *));
INT1 WsscfgGetNextFsDot11nConfigTable PROTO(
     (INT4,
      INT4 *));
INT1 WsscfgGetNextFsDot11nMCSDataRateTable PROTO(
     (INT4,
      INT4 *,
      INT4,
      INT4 *));
INT1
    WsscfgGetNextWlanStatisticsTable PROTO( 
        (INT4 ,INT4 *));
INT1
    WsscfgGetNextWlanSSIDStatsTable PROTO( 
        (UINT4 , UINT4 *));
INT1
    WsscfgGetNextWlanRadioStatsTable PROTO(
        (INT4 ,INT4 *));
INT1
    WsscfgGetNextWlanClientStatsTable PROTO(
        (tMacAddr ,tMacAddr * ));

INT1
    WsscfgGetNextApWlanStatisticsTable PROTO(
        (INT4 ,INT4 * ,UINT4 ,UINT4 *));
    
INT1
    WsscfgGetFirstClientStatsTable PROTO((tMacAddr *));
INT1
    WsscfgGetFirstRadioStatsTable PROTO((INT4 *));
INT1
    WsscfgGetFirstSSIDStatsTable PROTO((UINT4 *));

INT1
    WsscfgGetFirstApWlanStatisticsTable PROTO((INT4 * ,UINT4 *));
INT1
    WsscfgGetFirstWlanStatisticsTable PROTO((INT4 *));

INT4 FsDot11StationConfigTableFilterInputs PROTO(
     (tWsscfgFsDot11StationConfigEntry *,
      tWsscfgFsDot11StationConfigEntry *,
      tWsscfgIsSetFsDot11StationConfigEntry *));
INT4 FsDot11CapabilityProfileTableFilterInputs PROTO(
     (tWsscfgFsDot11CapabilityProfileEntry *,
      tWsscfgFsDot11CapabilityProfileEntry *,
      tWsscfgIsSetFsDot11CapabilityProfileEntry *));
INT4 FsDot11AuthenticationProfileTableFilterInputs PROTO(
     (tWsscfgFsDot11AuthenticationProfileEntry *,
      tWsscfgFsDot11AuthenticationProfileEntry *,
      tWsscfgIsSetFsDot11AuthenticationProfileEntry *));
INT4 FsStationQosParamsTableFilterInputs PROTO(
     (tWsscfgFsStationQosParamsEntry *,
      tWsscfgFsStationQosParamsEntry *,
      tWsscfgIsSetFsStationQosParamsEntry *));
INT4 FsVlanIsolationTableFilterInputs PROTO(
     (tWsscfgFsVlanIsolationEntry *,
      tWsscfgFsVlanIsolationEntry *,
      tWsscfgIsSetFsVlanIsolationEntry *));
INT4 FsDot11RadioConfigTableFilterInputs PROTO(
     (tWsscfgFsDot11RadioConfigEntry *,
      tWsscfgFsDot11RadioConfigEntry *,
      tWsscfgIsSetFsDot11RadioConfigEntry *));
INT4 FsDot11QosProfileTableFilterInputs PROTO(
     (tWsscfgFsDot11QosProfileEntry *,
      tWsscfgFsDot11QosProfileEntry *,
      tWsscfgIsSetFsDot11QosProfileEntry *));
INT4 FsDot11WlanCapabilityProfileTableFilterInputs PROTO(
     (tWsscfgFsDot11WlanCapabilityProfileEntry *,
      tWsscfgFsDot11WlanCapabilityProfileEntry *,
      tWsscfgIsSetFsDot11WlanCapabilityProfileEntry *));
INT4 FsDot11WlanAuthenticationProfileTableFilterInputs PROTO(
     (tWsscfgFsDot11WlanAuthenticationProfileEntry *,
      tWsscfgFsDot11WlanAuthenticationProfileEntry *,
      tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry *));
INT4 FsDot11WlanQosProfileTableFilterInputs PROTO(
     (tWsscfgFsDot11WlanQosProfileEntry *,
      tWsscfgFsDot11WlanQosProfileEntry *,
      tWsscfgIsSetFsDot11WlanQosProfileEntry *));
INT4 FsDot11RadioQosTableFilterInputs PROTO(
     (tWsscfgFsDot11RadioQosEntry *,
      tWsscfgFsDot11RadioQosEntry *,
      tWsscfgIsSetFsDot11RadioQosEntry *));
INT4 FsDot11QAPTableFilterInputs PROTO(
     (tWsscfgFsDot11QAPEntry *,
      tWsscfgFsDot11QAPEntry *,
      tWsscfgIsSetFsDot11QAPEntry *));
INT4 FsQAPProfileTableFilterInputs PROTO(
     (tWsscfgFsQAPProfileEntry *,
      tWsscfgFsQAPProfileEntry *,
      tWsscfgIsSetFsQAPProfileEntry *));
INT4 FsDot11CapabilityMappingTableFilterInputs PROTO(
     (tWsscfgFsDot11CapabilityMappingEntry *,
      tWsscfgFsDot11CapabilityMappingEntry *,
      tWsscfgIsSetFsDot11CapabilityMappingEntry *));
INT4 FsDot11AuthMappingTableFilterInputs PROTO(
     (tWsscfgFsDot11AuthMappingEntry *,
      tWsscfgFsDot11AuthMappingEntry *,
      tWsscfgIsSetFsDot11AuthMappingEntry *));
INT4 FsDot11QosMappingTableFilterInputs PROTO(
     (tWsscfgFsDot11QosMappingEntry *,
      tWsscfgFsDot11QosMappingEntry *,
      tWsscfgIsSetFsDot11QosMappingEntry *));
INT4 FsDot11AntennasListTableFilterInputs PROTO(
     (tWsscfgFsDot11AntennasListEntry *,
      tWsscfgFsDot11AntennasListEntry *,
      tWsscfgIsSetFsDot11AntennasListEntry *));
INT4 FsDot11WlanTableFilterInputs PROTO(
     (tWsscfgFsDot11WlanEntry *,
      tWsscfgFsDot11WlanEntry *,
      tWsscfgIsSetFsDot11WlanEntry *));
INT4 FsDot11WlanBindTableFilterInputs PROTO(
     (tWsscfgFsDot11WlanBindEntry *,
      tWsscfgFsDot11WlanBindEntry *,
      tWsscfgIsSetFsDot11WlanBindEntry *));
INT4 FsWtpImageUpgradeTableFilterInputs PROTO(
     (tWsscfgFsWtpImageUpgradeEntry *,
      tWsscfgFsWtpImageUpgradeEntry *,
      tWsscfgIsSetFsWtpImageUpgradeEntry *));
INT4 FsDot11nConfigTableFilterInputs PROTO(
     (tWsscfgFsDot11nConfigEntry *,
      tWsscfgFsDot11nConfigEntry *,
      tWsscfgIsSetFsDot11nConfigEntry *));
INT4 FsDot11nMCSDataRateTableFilterInputs PROTO(
     (tWsscfgFsDot11nMCSDataRateEntry *,
      tWsscfgFsDot11nMCSDataRateEntry *,
      tWsscfgIsSetFsDot11nMCSDataRateEntry *));

INT4 WsscfgUtilUpdateFsDot11StationConfigTable PROTO(
     (tWsscfgFsDot11StationConfigEntry *,
      tWsscfgFsDot11StationConfigEntry *,
      tWsscfgIsSetFsDot11StationConfigEntry *));

INT4 WsscfgGetAllUtlFsDot11ExternalWebAuthProfileTable PROTO(
        (tWsscfgFsDot11ExternalWebAuthProfileEntry *));

INT4 WsscfgUtilUpdateFsDot11ExternalWebAuthProfileTable PROTO(
        ( tWsscfgFsDot11ExternalWebAuthProfileEntry *,
         tWsscfgIsSetFsDot11ExternalWebAuthProfileEntry *));

INT4 WsscfgUtilUpdateFsDot11CapabilityProfileTable PROTO(
     (tWsscfgFsDot11CapabilityProfileEntry *,
      tWsscfgFsDot11CapabilityProfileEntry *,
      tWsscfgIsSetFsDot11CapabilityProfileEntry *));
INT4 WsscfgUtilUpdateFsDot11AuthenticationProfileTable PROTO(
     (tWsscfgFsDot11AuthenticationProfileEntry *,
      tWsscfgFsDot11AuthenticationProfileEntry *,
      tWsscfgIsSetFsDot11AuthenticationProfileEntry *));
INT4 WsscfgUtilUpdateFsSecurityWebAuthGuestInfoTable PROTO(
     (tWsscfgFsSecurityWebAuthGuestInfoEntry *,
      tWsscfgFsSecurityWebAuthGuestInfoEntry *,
      tWsscfgIsSetFsSecurityWebAuthGuestInfoEntry *));
INT4 WsscfgUtilUpdateFsStationQosParamsTable PROTO(
     (tWsscfgFsStationQosParamsEntry *,
      tWsscfgFsStationQosParamsEntry *,
      tWsscfgIsSetFsStationQosParamsEntry *));
INT4 WsscfgUtilUpdateFsVlanIsolationTable PROTO(
     (tWsscfgFsVlanIsolationEntry *,
      tWsscfgFsVlanIsolationEntry *,
      tWsscfgIsSetFsVlanIsolationEntry *));
INT4 WsscfgUtilUpdateFsDot11RadioConfigTable PROTO(
     (tWsscfgFsDot11RadioConfigEntry *,
      tWsscfgFsDot11RadioConfigEntry *,
      tWsscfgIsSetFsDot11RadioConfigEntry *));
INT4 WsscfgUtilUpdateFsDot11QosProfileTable PROTO(
     (tWsscfgFsDot11QosProfileEntry *,
      tWsscfgFsDot11QosProfileEntry *,
      tWsscfgIsSetFsDot11QosProfileEntry *));
INT4 WsscfgUtilUpdateFsDot11WlanCapabilityProfileTable PROTO(
     (tWsscfgFsDot11WlanCapabilityProfileEntry *,
      tWsscfgFsDot11WlanCapabilityProfileEntry *,
      tWsscfgIsSetFsDot11WlanCapabilityProfileEntry *));
INT4 WsscfgUtilUpdateFsDot11WlanAuthenticationProfileTable PROTO(
     (tWsscfgFsDot11WlanAuthenticationProfileEntry *,
      tWsscfgFsDot11WlanAuthenticationProfileEntry *,
      tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry *));
INT4 WsscfgUtilUpdateFsDot11WlanQosProfileTable PROTO(
     (tWsscfgFsDot11WlanQosProfileEntry *,
      tWsscfgFsDot11WlanQosProfileEntry *,
      tWsscfgIsSetFsDot11WlanQosProfileEntry *));
INT4 WsscfgUtilUpdateFsDot11RadioQosTable PROTO(
     (tWsscfgFsDot11RadioQosEntry *,
      tWsscfgFsDot11RadioQosEntry *,
      tWsscfgIsSetFsDot11RadioQosEntry *));
INT4 WsscfgUtilUpdateFsDot11QAPTable PROTO(
     (tWsscfgFsDot11QAPEntry *,
      tWsscfgFsDot11QAPEntry *,
      tWsscfgIsSetFsDot11QAPEntry *));
INT4 WsscfgUtilUpdateFsQAPProfileTable PROTO(
     (tWsscfgFsQAPProfileEntry *,
      tWsscfgFsQAPProfileEntry *,
      tWsscfgIsSetFsQAPProfileEntry *));
INT4 WsscfgUtilUpdateFsDot11CapabilityMappingTable PROTO(
     (tWsscfgFsDot11CapabilityMappingEntry *,
      tWsscfgFsDot11CapabilityMappingEntry *,
      tWsscfgIsSetFsDot11CapabilityMappingEntry *));
INT4 WsscfgUtilUpdateFsDot11AuthMappingTable PROTO(
     (tWsscfgFsDot11AuthMappingEntry *,
      tWsscfgFsDot11AuthMappingEntry *,
      tWsscfgIsSetFsDot11AuthMappingEntry *));
INT4 WsscfgUtilUpdateFsDot11QosMappingTable PROTO(
     (tWsscfgFsDot11QosMappingEntry *,
      tWsscfgFsDot11QosMappingEntry *,
      tWsscfgIsSetFsDot11QosMappingEntry *));
INT4 WsscfgUtilUpdateFsDot11AntennasListTable PROTO(
     (tWsscfgFsDot11AntennasListEntry *,
      tWsscfgFsDot11AntennasListEntry *,
      tWsscfgIsSetFsDot11AntennasListEntry *));
INT4 WsscfgUtilUpdateFsDot11WlanTable PROTO(
     (tWsscfgFsDot11WlanEntry *,
      tWsscfgFsDot11WlanEntry *,
      tWsscfgIsSetFsDot11WlanEntry *));
INT4 WsscfgUtilUpdateFsDot11WlanBindTable PROTO(
     (tWsscfgFsDot11WlanBindEntry *,
      tWsscfgFsDot11WlanBindEntry *,
      tWsscfgIsSetFsDot11WlanBindEntry *));
INT4 WsscfgUtilUpdateFsWtpImageUpgradeTable PROTO(
     (tWsscfgFsWtpImageUpgradeEntry *,
      tWsscfgFsWtpImageUpgradeEntry *,
      tWsscfgIsSetFsWtpImageUpgradeEntry *));
INT4 WsscfgUtilUpdateFsDot11nConfigTable PROTO(
     (tWsscfgFsDot11nConfigEntry *,
      tWsscfgFsDot11nConfigEntry *,
      tWsscfgIsSetFsDot11nConfigEntry *));
INT4 WsscfgUtilUpdateFsDot11nMCSDataRateTable PROTO(
     (tWsscfgFsDot11nMCSDataRateEntry *,
      tWsscfgFsDot11nMCSDataRateEntry *,
      tWsscfgIsSetFsDot11nMCSDataRateEntry *));

INT4 WsscfgSetAllFsDot11StationConfigTableTrigger PROTO(
     (tWsscfgFsDot11StationConfigEntry *,
      tWsscfgIsSetFsDot11StationConfigEntry *,
      INT4));
INT4 WsscfgSetAllFsDot11CapabilityProfileTableTrigger PROTO(
     (tWsscfgFsDot11CapabilityProfileEntry *,
      tWsscfgIsSetFsDot11CapabilityProfileEntry *,
      INT4));
INT4 WsscfgSetAllFsDot11AuthenticationProfileTableTrigger PROTO(
     (tWsscfgFsDot11AuthenticationProfileEntry *,
      tWsscfgIsSetFsDot11AuthenticationProfileEntry *,
      INT4));
INT4 WsscfgSetAllFsStationQosParamsTableTrigger PROTO(
     (tWsscfgFsStationQosParamsEntry *,
      tWsscfgIsSetFsStationQosParamsEntry *,
      INT4));
INT4 WsscfgSetAllFsVlanIsolationTableTrigger PROTO(
     (tWsscfgFsVlanIsolationEntry *,
      tWsscfgIsSetFsVlanIsolationEntry *,
      INT4));
INT4 WsscfgSetAllFsDot11RadioConfigTableTrigger PROTO(
     (tWsscfgFsDot11RadioConfigEntry *,
      tWsscfgIsSetFsDot11RadioConfigEntry *,
      INT4));
INT4 WsscfgSetAllFsDot11QosProfileTableTrigger PROTO(
     (tWsscfgFsDot11QosProfileEntry *,
      tWsscfgIsSetFsDot11QosProfileEntry *,
      INT4));
INT4 WsscfgSetAllFsDot11WlanCapabilityProfileTableTrigger PROTO(
     (tWsscfgFsDot11WlanCapabilityProfileEntry *,
      tWsscfgIsSetFsDot11WlanCapabilityProfileEntry *,
      INT4));
INT4 WsscfgSetAllFsDot11WlanAuthenticationProfileTableTrigger PROTO(
     (tWsscfgFsDot11WlanAuthenticationProfileEntry *,
      tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry *,
      INT4));
INT4 WsscfgSetAllFsDot11WlanQosProfileTableTrigger PROTO(
     (tWsscfgFsDot11WlanQosProfileEntry *,
      tWsscfgIsSetFsDot11WlanQosProfileEntry *,
      INT4));
INT4 WsscfgSetAllFsDot11RadioQosTableTrigger PROTO(
     (tWsscfgFsDot11RadioQosEntry *,
      tWsscfgIsSetFsDot11RadioQosEntry *,
      INT4));
INT4 WsscfgSetAllFsDot11QAPTableTrigger PROTO(
     (tWsscfgFsDot11QAPEntry *,
      tWsscfgIsSetFsDot11QAPEntry *,
      INT4));
INT4 WsscfgSetAllFsQAPProfileTableTrigger PROTO(
     (tWsscfgFsQAPProfileEntry *,
      tWsscfgIsSetFsQAPProfileEntry *,
      INT4));
INT4 WsscfgSetAllFsDot11CapabilityMappingTableTrigger PROTO(
     (tWsscfgFsDot11CapabilityMappingEntry *,
      tWsscfgIsSetFsDot11CapabilityMappingEntry *,
      INT4));
INT4 WsscfgSetAllFsDot11AuthMappingTableTrigger PROTO(
     (tWsscfgFsDot11AuthMappingEntry *,
      tWsscfgIsSetFsDot11AuthMappingEntry *,
      INT4));
INT4 WsscfgSetAllFsDot11QosMappingTableTrigger PROTO(
     (tWsscfgFsDot11QosMappingEntry *,
      tWsscfgIsSetFsDot11QosMappingEntry *,
      INT4));
INT4 WsscfgSetAllFsDot11AntennasListTableTrigger PROTO(
     (tWsscfgFsDot11AntennasListEntry *,
      tWsscfgIsSetFsDot11AntennasListEntry *,
      INT4));
INT4 WsscfgSetAllFsDot11WlanTableTrigger PROTO(
     (tWsscfgFsDot11WlanEntry *,
      tWsscfgIsSetFsDot11WlanEntry *,
      INT4));
INT4 WsscfgSetAllFsDot11WlanBindTableTrigger PROTO(
     (tWsscfgFsDot11WlanBindEntry *,
      tWsscfgIsSetFsDot11WlanBindEntry *,
      INT4));
INT4 WsscfgSetAllFsDot11nConfigTableTrigger PROTO(
     (tWsscfgFsDot11nConfigEntry *,
      tWsscfgIsSetFsDot11nConfigEntry *,
      INT4));
INT4 WsscfgSetAllFsDot11nMCSDataRateTableTrigger PROTO(
     (tWsscfgFsDot11nMCSDataRateEntry *,
      tWsscfgIsSetFsDot11nMCSDataRateEntry *,
      INT4));

/* WSS Changes Starts */
INT4 WssCfgShowNetworkSummary PROTO(
     (tCliHandle,
      UINT4 *));

INT4 WssCfgShowApConfiguration PROTO(
     (tCliHandle,
      UINT4,
      UINT1 *,
      UINT4));

INT4 WssCfgShowApWlanConfig PROTO(
     (tCliHandle,
      UINT4 *,
      UINT1 *,
      UINT4 *));

INT4 WssCfgShowNetworkConfiguration PROTO(
     (tCliHandle,
      UINT4,
      UINT1 *,
      UINT4));

INT4 WsscfgSetDot11OperationTable PROTO(
     (UINT4,
      UINT1));

INT4 WsscfgInitializeDot11OperationTable PROTO(
     (tWsscfgDot11OperationEntry *));
INT4 WsscfgInitializeMibDot11OperationTable PROTO(
     (tWsscfgDot11OperationEntry *));

INT4 WsscfgSetAllFsWtpImageUpgradeTableTrigger PROTO(
     (tWsscfgFsWtpImageUpgradeEntry *,
      tWsscfgIsSetFsWtpImageUpgradeEntry *,
      INT4));

UINT1 WssCfgSetOperRate PROTO ((INT4 i4RadioIfIndex,
        tSNMP_OCTET_STRING_TYPE *OperRate,
        INT4  i4Rate, INT4 i4Status));
INT4  WssCfgCliSetStatusAllowedChannelWidth(tCliHandle CliHandle, INT4 i4RadioType,
                           UINT1 u1AllowChannelWidth, UINT1 *pu1ProfileName,
                           UINT4 *pu4RadioId);
INT4
WsscfgUtlTestDot11nAllowConfigTable(UINT4 *pu4ErrorCode ,
                                    INT4 i4IfIndex ,
                        INT4 i4TestValFsDot11nConfigAllowChannelWidth);
INT4 WsscfgGetFirstFsDot11ExternalWebAuthProfileTable(VOID); 
INT4 WsscfgGetNextFsDot11ExternalWebAuthProfileTable (tWsscfgFsDot11ExternalWebAuthProfileEntry *); 
INT4 WsscfgGetAllFsDot11ExternalWebAuthProfileTable (tWsscfgFsDot11ExternalWebAuthProfileEntry *);
INT4 WsscfgSetAllFsDot11ExternalWebAuthProfileTable (tWsscfgFsDot11ExternalWebAuthProfileEntry *,
                                       tWsscfgIsSetFsDot11ExternalWebAuthProfileEntry *);
INT4 WsscfgTestAllFsDot11ExternalWebAuthProfileTable (UINT4 *pu4ErrorCode,
                                        tWsscfgFsDot11ExternalWebAuthProfileEntry * ,
                                        tWsscfgIsSetFsDot11ExternalWebAuthProfileEntry *);

INT4
WsscfgCliSetDscp(tCliHandle CliHandle, UINT4 u4RadioId, UINT4 u4WlanId,
                UINT1 *pu1ProfileName,
                UINT4 u4DscpInPriority, UINT4 u4OutDscp);

INT4
WsscfgCliSetDscpTable(tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4WlanProfileId,
                        UINT4 u4DscpInPriority, UINT4 u4OutDscp);

INT1
ApFwlParseIpAddr (UINT1 *, UINT4 *, UINT4 *);

INT1
ApFwlParseMinAndMaxPort (UINT1 *, UINT2 *, UINT2 *);

INT4
ApFwlValidateIpAddress  (UINT4);

INT1
ApFwlValidateProtocol  (INT4);

INT1
ApFwlParseFilterSet (UINT1 *, tWssIfWtpFwlRuleDB *, UINT4);
INT4
WsscfgCliShowLegacyRate PROTO ((tCliHandle CliHandle));
INT4
WlanLegacyRateShowRunningConfig PROTO ((tCliHandle CliHandle));
#ifdef BAND_SELECT_WANTED
INT4
WlanBandSelectShowRunningConfig PROTO ((tCliHandle CliHandle));
INT4
WsscfgCliShowBandSelect PROTO((tCliHandle CliHandle, UINT4 u4WlanProfileId));

INT4
WsscfgCliShowBandSelectProbeDetails PROTO((tCliHandle CliHandle));
INT4
WsscfgCliShowBandSelectAssocDetails PROTO((tCliHandle CliHandle));

INT4
WsscfgCliEnableDebug PROTO((tCliHandle CliHandle, INT4 i4EnableDebug));
INT4
WsscfgCliDisableDebug PROTO((tCliHandle CliHandle, INT4 i4DisableDebug));
#endif

/**AP GROUP **/
INT4
ApGroupAddConfigEntry PROTO((tApGroupConfigEntry *));

INT1
ApGroupGetConfigEntry PROTO ((UINT2, tApGroupConfigEntry **));

INT4
ApGroupCompareConfigData PROTO((tRBElem * e1, tRBElem * e2));

INT1
ApGroupGetFirstConfigEntry PROTO ((tApGroupConfigEntry **));

INT1
ApGroupGetNextConfigEntry PROTO ((tApGroupConfigEntry *,
                                 tApGroupConfigEntry ** ));

INT4
ApGroupAddWTPConfigEntry PROTO((tApGroupConfigWTPEntry *));

INT1
ApGroupGetWTPConfigEntry PROTO ((UINT2, UINT2 ,
                               tApGroupConfigWTPEntry **));

INT4
ApGroupCompareWTPConfigData PROTO((tRBElem * e1, tRBElem * e2));

INT1
ApGroupGetFirstWTPConfigEntry PROTO ((tApGroupConfigWTPEntry **));

INT1
ApGroupGetNextWTPConfigEntry PROTO ((tApGroupConfigWTPEntry *,
                                 tApGroupConfigWTPEntry ** ));
INT4
ApGroupAddWLANConfigEntry PROTO((tApGroupConfigWTPEntry *));

INT1
ApGroupGetWLANConfigEntry PROTO ((UINT2, tApGroupConfigWLANEntry **));

INT4
ApGroupCompareWLANConfigData PROTO((tRBElem * e1, tRBElem * e2));

INT1
ApGroupGetFirstWLANConfigEntry PROTO ((tApGroupConfigWLANEntry **));

INT1
ApGroupGetNextWLANConfigEntry PROTO ((tApGroupConfigWLANEntry *,
                                 tApGroupConfigWLANEntry ** ));

INT1
AddWlanToApGroup PROTO (( UINT4 , UINT4 , UINT4 ));

INT1
DeleteWlanFromApGroup PROTO (( UINT4 , UINT4 , UINT4 ));

INT4
FindWlanInApGroup PROTO (( UINT4 ));


INT4
ApGroupDeleteConfigEntry PROTO ((UINT4));

INT1
ApGroupGetFirstConfigWLANEntry (tApGroupConfigWLANEntry **);

INT4
WssCfgValidateWtpEntry(tApGroupConfigEntry *,
                     tApGroupConfigEntry *, UINT4 );

VOID
WssFindTheApGroupFromWtpProfileId(UINT4 ,tApGroupConfigEntry **,
                                        tApGroupConfigEntry **);

INT4
WsscfgApGroupBinding(UINT4 ,UINT4 ,UINT2 ,UINT4 *);

INT4
WsscfgGetApGroupEnable (UINT4 *);

INT4
WsscfgSetApGroupEnable ( UINT4 );

INT1
WsscfgGetApGroupName ( UINT4 ,tSNMP_OCTET_STRING_TYPE *);

INT1
WsscfgSetApGroupName ( INT4 ,tSNMP_OCTET_STRING_TYPE *);

INT1
WsscfgGetApGroupRowStatus ( INT4 , INT4 *);

INT1
WsscfgGetApGroupWTPRowStatus ( UINT4 , UINT4 ,INT4 *);

INT1
WsscfgGetApGroupWLANRowStatus ( UINT4 , UINT4 ,INT4 *);

INT1
WsscfgDeleteApGroup ( UINT4 );

INT1
WsscfgDeleteApGroupWTP(UINT4,  UINT4 );

INT1
WsscfgDeleteApGroupWLAN(UINT4, UINT4 );

INT1
WsscfgDeleteApGroupInterfaceMapping ( UINT4 );

INT1
WsscfgGetApGroupNameDescription ( UINT4 ,tSNMP_OCTET_STRING_TYPE *);
INT1
WsscfgSetApGroupNameDescription ( UINT4 , tSNMP_OCTET_STRING_TYPE *);

INT1
WsscfgGetApGroupRadioPolicy ( UINT4 ,UINT4 *);
INT1
WsscfgSetApGroupRadioPolicy ( UINT4 ,UINT4 *);

INT1 WsscfgSetApGroupInterfaceVlan (UINT4 ,UINT4 );

INT1 WsscfgGetApGroupInterfaceVlan (UINT4 ,UINT4 *);

UINT1
APGroupDBCreate (VOID);

UINT1
APGroupWTPDBCreate (VOID);

UINT1
APGroupWLANDBCreate (VOID);

INT4
ApGroupCompareConfigWTPData (tRBElem * e1, tRBElem * e2);

INT1
ApGroupGetFirstConfigWTPEntry (tApGroupConfigWTPEntry **pApGroupConfigWTPEntry);

INT1
ApGroupGetNextConfigWTPEntry (tApGroupConfigWTPEntry * pApGroupConfigWTPEntry,
                tApGroupConfigWTPEntry **pNextApGroupConfigWTPEntry);

INT1
ApGroupGetConfigWLANEntry (UINT2 u2ApGroupWLANIndex,UINT2 u2WlanProfileId,
                       tApGroupConfigWLANEntry **pConfigInfo);

INT4
ApGroupAddConfigWLANEntry (tApGroupConfigWLANEntry *pApGroupConfigWLANEntry);

INT4
ApGroupDeleteConfigWLANEntry (UINT4,UINT4);

INT1
ApGroupCliGetWlanIfIndexFromWlanId (UINT4,INT4 *);

INT4 WssCfgShowApGroupSummary (
    tCliHandle CliHandle);

INT4 WssCfgShowApGroupConfig (
    tCliHandle CliHandle,UINT4);


#endif
