/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: wsscfgtdfsg.h,v 1.3 2017/11/24 10:37:08 siva Exp $ 
*
* Description: This file contains data structures defined for Wsscfg module.
*********************************************************************/
/* Structure used by CLI to indicate which 
 all objects to be set in Dot11StationConfigEntry */

#ifndef _WSSCFG_TDFSG_H_
#define _WSSCFG_TDFSG_H_

typedef struct {
    BOOL1 bDot11StationID;
    BOOL1 bDot11MediumOccupancyLimit;
    BOOL1 bDot11CFPPeriod;
    BOOL1 bDot11CFPMaxDuration;
    BOOL1 bDot11AuthenticationResponseTimeOut;
    BOOL1 bDot11PowerManagementMode;
    BOOL1 bDot11DesiredSSID;
    BOOL1 bDot11DesiredBSSType;
    BOOL1 bDot11OperationalRateSet;
    BOOL1 bDot11BeaconPeriod;
    BOOL1 bDot11DTIMPeriod;
    BOOL1 bDot11AssociationResponseTimeOut;
    BOOL1 bDot11MultiDomainCapabilityImplemented;
    BOOL1 bDot11MultiDomainCapabilityEnabled;
    BOOL1 bDot11SpectrumManagementRequired;
    BOOL1 bDot11RegulatoryClassesImplemented;
    BOOL1 bDot11RegulatoryClassesRequired;
    BOOL1 bDot11AssociateinNQBSS;
    BOOL1 bDot11DLSAllowedInQBSS;
    BOOL1 bDot11DLSAllowed;
    BOOL1 bIfIndex;
    UINT1 au1Pad[3];
} tWsscfgIsSetDot11StationConfigEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11AuthenticationAlgorithmsEntry */

typedef struct {
    BOOL1 bDot11AuthenticationAlgorithmsIndex;
    BOOL1 bDot11AuthenticationAlgorithm;
    BOOL1 bDot11AuthenticationAlgorithmsEnable;
    BOOL1 bIfIndex;
} tWsscfgIsSetDot11AuthenticationAlgorithmsEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11WEPDefaultKeysEntry */

typedef struct {
    BOOL1 bDot11WEPDefaultKeyIndex;
    BOOL1 bDot11WEPDefaultKeyValue;
    BOOL1 bIfIndex;
    UINT1 u1Pad;
} tWsscfgIsSetDot11WEPDefaultKeysEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11WEPKeyMappingsEntry */

typedef struct {
    BOOL1 bDot11WEPKeyMappingIndex;
    BOOL1 bDot11WEPKeyMappingAddress;
    BOOL1 bDot11WEPKeyMappingWEPOn;
    BOOL1 bDot11WEPKeyMappingValue;
    BOOL1 bDot11WEPKeyMappingStatus;
    BOOL1 bIfIndex;
    UINT1 au1Pad[2];
} tWsscfgIsSetDot11WEPKeyMappingsEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11PrivacyEntry */

typedef struct {
    BOOL1 bDot11PrivacyInvoked;
    BOOL1 bDot11WEPDefaultKeyID;
    BOOL1 bDot11WEPKeyMappingLength;
    BOOL1 bDot11ExcludeUnencrypted;
    BOOL1 bDot11RSNAEnabled;
    BOOL1 bDot11RSNAPreauthenticationEnabled;
    BOOL1 bIfIndex;
    UINT1 u1Pad;
} tWsscfgIsSetDot11PrivacyEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11MultiDomainCapabilityEntry */

typedef struct {
    BOOL1 bDot11MultiDomainCapabilityIndex;
    BOOL1 bDot11FirstChannelNumber;
    BOOL1 bDot11NumberofChannels;
    BOOL1 bDot11MaximumTransmitPowerLevel;
    BOOL1 bIfIndex;
    UINT1 au1Pad[3];
} tWsscfgIsSetDot11MultiDomainCapabilityEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11SpectrumManagementEntry */

typedef struct {
    BOOL1 bDot11SpectrumManagementIndex;
    BOOL1 bDot11MitigationRequirement;
    BOOL1 bDot11ChannelSwitchTime;
    BOOL1 bIfIndex;
} tWsscfgIsSetDot11SpectrumManagementEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11RegulatoryClassesEntry */

typedef struct {
    BOOL1 bDot11RegulatoryClassesIndex;
    BOOL1 bDot11RegulatoryClass;
    BOOL1 bDot11CoverageClass;
    BOOL1 bIfIndex;
} tWsscfgIsSetDot11RegulatoryClassesEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11OperationEntry */

typedef struct {
    BOOL1 bDot11RTSThreshold;
    BOOL1 bDot11ShortRetryLimit;
    BOOL1 bDot11LongRetryLimit;
    BOOL1 bDot11FragmentationThreshold;
    BOOL1 bDot11MaxTransmitMSDULifetime;
    BOOL1 bDot11MaxReceiveLifetime;
    BOOL1 bDot11CAPLimit;
    BOOL1 bDot11HCCWmin;
    BOOL1 bDot11HCCWmax;
    BOOL1 bDot11HCCAIFSN;
    BOOL1 bDot11ADDBAResponseTimeout;
    BOOL1 bDot11ADDTSResponseTimeout;
    BOOL1 bDot11ChannelUtilizationBeaconInterval;
    BOOL1 bDot11ScheduleTimeout;
    BOOL1 bDot11DLSResponseTimeout;
    BOOL1 bDot11QAPMissingAckRetryLimit;
    BOOL1 bDot11EDCAAveragingPeriod;
    BOOL1 bIfIndex;
    UINT1 au1Pad[2];
} tWsscfgIsSetDot11OperationEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11GroupAddressesEntry */

typedef struct {
    BOOL1 bDot11GroupAddressesIndex;
    BOOL1 bDot11Address;
    BOOL1 bDot11GroupAddressesStatus;
    BOOL1 bIfIndex;
} tWsscfgIsSetDot11GroupAddressesEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11EDCAEntry */

typedef struct {
    BOOL1 bDot11EDCATableIndex;
    BOOL1 bDot11EDCATableCWmin;
    BOOL1 bDot11EDCATableCWmax;
    BOOL1 bDot11EDCATableAIFSN;
    BOOL1 bDot11EDCATableTXOPLimit;
    BOOL1 bDot11EDCATableMSDULifetime;
    BOOL1 bDot11EDCATableMandatory;
    BOOL1 bIfIndex;
} tWsscfgIsSetDot11EDCAEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11QAPEDCAEntry */

typedef struct {
    BOOL1 bDot11QAPEDCATableIndex;
    BOOL1 bDot11QAPEDCATableCWmin;
    BOOL1 bDot11QAPEDCATableCWmax;
    BOOL1 bDot11QAPEDCATableAIFSN;
    BOOL1 bDot11QAPEDCATableTXOPLimit;
    BOOL1 bDot11QAPEDCATableMSDULifetime;
    BOOL1 bDot11QAPEDCATableMandatory;
    BOOL1 bIfIndex;
} tWsscfgIsSetDot11QAPEDCAEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11PhyOperationEntry */

typedef struct {
    BOOL1 bDot11CurrentRegDomain;
    BOOL1 bIfIndex;
    UINT1 au1Pad[2];
} tWsscfgIsSetDot11PhyOperationEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11PhyAntennaEntry */

typedef struct {
    BOOL1 bDot11CurrentTxAntenna;
    BOOL1 bDot11CurrentRxAntenna;
    BOOL1 bIfIndex;
    UINT1 u1Pad;
} tWsscfgIsSetDot11PhyAntennaEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11PhyTxPowerEntry */

typedef struct {
    BOOL1 bDot11CurrentTxPowerLevel;
    BOOL1 bIfIndex;
    UINT1 au1Pad[2];
} tWsscfgIsSetDot11PhyTxPowerEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11PhyFHSSEntry */

typedef struct {
    BOOL1 bDot11CurrentChannelNumber;
    BOOL1 bDot11CurrentDwellTime;
    BOOL1 bDot11CurrentSet;
    BOOL1 bDot11CurrentPattern;
    BOOL1 bDot11CurrentIndex;
    BOOL1 bDot11EHCCPrimeRadix;
    BOOL1 bDot11EHCCNumberofChannelsFamilyIndex;
    BOOL1 bDot11EHCCCapabilityImplemented;
    BOOL1 bDot11EHCCCapabilityEnabled;
    BOOL1 bDot11HopAlgorithmAdopted;
    BOOL1 bDot11RandomTableFlag;
    BOOL1 bDot11HopOffset;
    BOOL1 bIfIndex;
    UINT1 au1Pad[3];
} tWsscfgIsSetDot11PhyFHSSEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11PhyDSSSEntry */

typedef struct {
    BOOL1 bDot11CurrentChannel;
    BOOL1 bDot11CurrentCCAMode;
    BOOL1 bDot11EDThreshold;
    BOOL1 bIfIndex;
} tWsscfgIsSetDot11PhyDSSSEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11PhyIREntry */

typedef struct {
    BOOL1 bDot11CCAWatchdogTimerMax;
    BOOL1 bDot11CCAWatchdogCountMax;
    BOOL1 bDot11CCAWatchdogTimerMin;
    BOOL1 bDot11CCAWatchdogCountMin;
    BOOL1 bIfIndex;
    UINT1 au1Pad[3];
} tWsscfgIsSetDot11PhyIREntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11AntennasListEntry */

typedef struct {
    BOOL1 bDot11AntennaListIndex;
    BOOL1 bDot11SupportedTxAntenna;
    BOOL1 bDot11SupportedRxAntenna;
    BOOL1 bDot11DiversitySelectionRx;
    BOOL1 bIfIndex;
    UINT1 au1Pad[3];
} tWsscfgIsSetDot11AntennasListEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11PhyOFDMEntry */

typedef struct {
    BOOL1 bDot11CurrentFrequency;
    BOOL1 bDot11TIThreshold;
    BOOL1 bDot11ChannelStartingFactor;
    BOOL1 bDot11PhyOFDMChannelWidth;
    BOOL1 bIfIndex;
    UINT1 au1Pad[3];
} tWsscfgIsSetDot11PhyOFDMEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11HoppingPatternEntry */

typedef struct {
    BOOL1 bDot11HoppingPatternIndex;
    BOOL1 bDot11RandomTableFieldNumber;
    BOOL1 bIfIndex;
    UINT1 u1Pad;
} tWsscfgIsSetDot11HoppingPatternEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11PhyERPEntry */

typedef struct {
    BOOL1 bDot11ERPBCCOptionEnabled;
    BOOL1 bDot11DSSSOFDMOptionEnabled;
    BOOL1 bDot11ShortSlotTimeOptionImplemented;
    BOOL1 bDot11ShortSlotTimeOptionEnabled;
    BOOL1 bIfIndex;
    UINT1 au1Pad[3];
} tWsscfgIsSetDot11PhyERPEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11RSNAConfigEntry */

typedef struct {
    BOOL1 bDot11RSNAConfigGroupCipher;
    BOOL1 bDot11RSNAConfigGroupRekeyMethod;
    BOOL1 bDot11RSNAConfigGroupRekeyTime;
    BOOL1 bDot11RSNAConfigGroupRekeyPackets;

    BOOL1 bDot11RSNAConfigGroupRekeyStrict;
    BOOL1 bDot11RSNAConfigPSKValue;
    BOOL1 bDot11RSNAConfigPSKPassPhrase;
    BOOL1 bDot11RSNAConfigGroupUpdateCount;

    BOOL1 bDot11RSNAConfigPairwiseUpdateCount;
    BOOL1 bDot11RSNAConfigPMKLifetime;
    BOOL1 bDot11RSNAConfigPMKReauthThreshold;
    BOOL1 bDot11RSNAConfigSATimeout;

    BOOL1 bDot11RSNATKIPCounterMeasuresInvoked;
    BOOL1 bDot11RSNA4WayHandshakeFailures;
    BOOL1 bDot11RSNAConfigSTKRekeyTime;
    BOOL1 bDot11RSNAConfigSMKUpdateCount;

    BOOL1 bDot11RSNAConfigSMKLifetime;
    BOOL1 bDot11RSNASMKHandshakeFailures;
    BOOL1 bIfIndex;
    UINT1 u1Pad;
} tWsscfgIsSetDot11RSNAConfigEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11RSNAConfigPairwiseCiphersEntry */

typedef struct {
    BOOL1 bDot11RSNAConfigPairwiseCipherIndex;
    BOOL1 bDot11RSNAConfigPairwiseCipherEnabled;
    BOOL1 bIfIndex;
    UINT1 u1Pad;
} tWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in Dot11RSNAConfigAuthenticationSuitesEntry */

typedef struct {
    BOOL1 bDot11RSNAConfigAuthenticationSuiteIndex;
    BOOL1 bDot11RSNAConfigAuthenticationSuiteEnabled;
    UINT1 au1Pad[2];
} tWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry;



/* Structure used by Wsscfg protocol for Dot11StationConfigEntry */

typedef struct {
    tRBNodeEmbd Dot11StationConfigTableNode;
    tMacAddr Dot11StationID;
    tMacAddr Dot11DisassociateStation;
    tMacAddr Dot11DeauthenticateStation;
    tMacAddr Dot11AuthenticateFailStation;
    UINT4 u4Dot11AuthenticationResponseTimeOut;
    UINT4 u4Dot11AssociationResponseTimeOut;
    INT4 i4Dot11MediumOccupancyLimit;
    INT4 i4Dot11CFPollable;
    INT4 i4Dot11CFPPeriod;
    INT4 i4Dot11CFPMaxDuration;
    INT4 i4Dot11PrivacyOptionImplemented;
    INT4 i4Dot11PowerManagementMode;
    INT4 i4Dot11DesiredBSSType;
    INT4 i4Dot11BeaconPeriod;
    INT4 i4Dot11DTIMPeriod;
    INT4 i4Dot11DisassociateReason;
    INT4 i4Dot11DeauthenticateReason;
    INT4 i4Dot11AuthenticateFailStatus;
    INT4 i4Dot11MultiDomainCapabilityImplemented;
    INT4 i4Dot11MultiDomainCapabilityEnabled;
    INT4 i4Dot11SpectrumManagementImplemented;
    INT4 i4Dot11SpectrumManagementRequired;
    INT4 i4Dot11RSNAOptionImplemented;
    INT4 i4Dot11RSNAPreauthenticationImplemented;
    INT4 i4Dot11RegulatoryClassesImplemented;
    INT4 i4Dot11RegulatoryClassesRequired;
    INT4 i4Dot11QosOptionImplemented;
    INT4 i4Dot11ImmediateBlockAckOptionImplemented;
    INT4 i4Dot11DelayedBlockAckOptionImplemented;
    INT4 i4Dot11DirectOptionImplemented;
    INT4 i4Dot11APSDOptionImplemented;
    INT4 i4Dot11QAckOptionImplemented;
    INT4 i4Dot11QBSSLoadOptionImplemented;
    INT4 i4Dot11QueueRequestOptionImplemented;
    INT4 i4Dot11TXOPRequestOptionImplemented;
    INT4 i4Dot11MoreDataAckOptionImplemented;
    INT4 i4Dot11AssociateinNQBSS;
    INT4 i4Dot11DLSAllowedInQBSS;
    INT4 i4Dot11DLSAllowed;
    INT4 i4IfIndex;
    INT4 i4Dot11DesiredSSIDLen;
    INT4 i4Dot11OperationalRateSetLen;
    INT4 i4Dot11CountryStringLen;
    UINT1 au1Dot11DesiredSSID[32];
    UINT1 au1Dot11OperationalRateSet[1024];
    UINT1 au1Dot11CountryString[3];
    UINT1 au1Pad1[1];
    UINT1 au1FsDot11CountryString[3];
    UINT1 au1Pad2[1];
} tWsscfgMibDot11StationConfigEntry;

/* Structure used by Wsscfg protocol for Dot11AuthenticationAlgorithmsEntry */

typedef struct {
    tRBNodeEmbd Dot11AuthenticationAlgorithmsTableNode;
    INT4 i4Dot11AuthenticationAlgorithmsIndex;
    INT4 i4Dot11AuthenticationAlgorithm;
    INT4 i4Dot11AuthenticationAlgorithmsEnable;
    INT4 i4IfIndex;
} tWsscfgMibDot11AuthenticationAlgorithmsEntry;

/* Structure used by Wsscfg protocol for Dot11WEPDefaultKeysEntry */

typedef struct {
    tRBNodeEmbd Dot11WEPDefaultKeysTableNode;
    INT4 i4Dot11WEPDefaultKeyIndex;
    INT4 i4IfIndex;
    INT4 i4Dot11WEPDefaultKeyValueLen;
    UINT1 au1Dot11WEPDefaultKeyValue[256];
} tWsscfgMibDot11WEPDefaultKeysEntry;

/* Structure used by Wsscfg protocol for Dot11WEPKeyMappingsEntry */

typedef struct {
    tRBNodeEmbd Dot11WEPKeyMappingsTableNode;
    tMacAddr Dot11WEPKeyMappingAddress;
    UINT1 au1Pad[2];
    INT4 i4Dot11WEPKeyMappingIndex;
    INT4 i4Dot11WEPKeyMappingWEPOn;
    INT4 i4Dot11WEPKeyMappingStatus;
    INT4 i4IfIndex;
    INT4 i4Dot11WEPKeyMappingValueLen;
    UINT1 au1Dot11WEPKeyMappingValue[256];
} tWsscfgMibDot11WEPKeyMappingsEntry;

/* Structure used by Wsscfg protocol for Dot11PrivacyEntry */

typedef struct {
    tRBNodeEmbd Dot11PrivacyTableNode;
    UINT4 u4Dot11WEPKeyMappingLength;
    UINT4 u4Dot11WEPICVErrorCount;
    UINT4 u4Dot11WEPExcludedCount;
    INT4 i4Dot11PrivacyInvoked;
    INT4 i4Dot11WEPDefaultKeyID;
    INT4 i4Dot11ExcludeUnencrypted;
    INT4 i4Dot11RSNAEnabled;
    INT4 i4Dot11RSNAPreauthenticationEnabled;
    INT4 i4IfIndex;
} tWsscfgMibDot11PrivacyEntry;

/* Structure used by Wsscfg protocol for Dot11MultiDomainCapabilityEntry */

typedef struct {
    tRBNodeEmbd Dot11MultiDomainCapabilityTableNode;
    INT4 i4Dot11MultiDomainCapabilityIndex;
    INT4 i4Dot11FirstChannelNumber;
    INT4 i4Dot11NumberofChannels;
    INT4 i4Dot11MaximumTransmitPowerLevel;
    INT4 i4IfIndex;
} tWsscfgMibDot11MultiDomainCapabilityEntry;

/* Structure used by Wsscfg protocol for Dot11SpectrumManagementEntry */

typedef struct {
    tRBNodeEmbd Dot11SpectrumManagementTableNode;
    INT4 i4Dot11SpectrumManagementIndex;
    INT4 i4Dot11MitigationRequirement;
    INT4 i4Dot11ChannelSwitchTime;
    INT4 i4Dot11PowerCapabilityMax;
    INT4 i4Dot11PowerCapabilityMin;
    INT4 i4IfIndex;
} tWsscfgMibDot11SpectrumManagementEntry;

/* Structure used by Wsscfg protocol for Dot11RegulatoryClassesEntry */

typedef struct {
    tRBNodeEmbd Dot11RegulatoryClassesTableNode;
    INT4 i4Dot11RegulatoryClassesIndex;
    INT4 i4Dot11RegulatoryClass;
    INT4 i4Dot11CoverageClass;
    INT4 i4IfIndex;
} tWsscfgMibDot11RegulatoryClassesEntry;

/* Structure used by Wsscfg protocol for Dot11OperationEntry */

typedef struct {
    tRBNodeEmbd Dot11OperationTableNode;
    tMacAddr Dot11MACAddress;
    UINT1 au1Pad[2];
    UINT4 u4Dot11MaxTransmitMSDULifetime;
    UINT4 u4Dot11MaxReceiveLifetime;
    INT4 i4Dot11RTSThreshold;
    INT4 i4Dot11ShortRetryLimit;
    INT4 i4Dot11LongRetryLimit;
    INT4 i4Dot11FragmentationThreshold;
    INT4 i4Dot11CAPLimit;
    INT4 i4Dot11HCCWmin;
    INT4 i4Dot11HCCWmax;
    INT4 i4Dot11HCCAIFSN;
    INT4 i4Dot11ADDBAResponseTimeout;
    INT4 i4Dot11ADDTSResponseTimeout;
    INT4 i4Dot11ChannelUtilizationBeaconInterval;
    INT4 i4Dot11ScheduleTimeout;
    INT4 i4Dot11DLSResponseTimeout;
    INT4 i4Dot11QAPMissingAckRetryLimit;
    INT4 i4Dot11EDCAAveragingPeriod;
    INT4 i4IfIndex;
    INT4 i4Dot11ManufacturerIDLen;
    INT4 i4Dot11ProductIDLen;
    UINT1 au1Dot11ManufacturerID[128];
    UINT1 au1Dot11ProductID[128];
} tWsscfgMibDot11OperationEntry;

/* Structure used by Wsscfg protocol for Dot11CountersEntry */

typedef struct {
    tRBNodeEmbd Dot11CountersTableNode;
    UINT4 u4Dot11TransmittedFragmentCount;
    UINT4 u4Dot11MulticastTransmittedFrameCount;
    UINT4 u4Dot11FailedCount;
    UINT4 u4Dot11RetryCount;
    UINT4 u4Dot11MultipleRetryCount;
    UINT4 u4Dot11FrameDuplicateCount;
    UINT4 u4Dot11RTSSuccessCount;
    UINT4 u4Dot11RTSFailureCount;
    UINT4 u4Dot11ACKFailureCount;
    UINT4 u4Dot11ReceivedFragmentCount;
    UINT4 u4Dot11MulticastReceivedFrameCount;
    UINT4 u4Dot11FCSErrorCount;
    UINT4 u4Dot11TransmittedFrameCount;
    UINT4 u4Dot11WEPUndecryptableCount;
    UINT4 u4Dot11QosDiscardedFragmentCount;
    UINT4 u4Dot11AssociatedStationCount;
    UINT4 u4Dot11QosCFPollsReceivedCount;
    UINT4 u4Dot11QosCFPollsUnusedCount;
    UINT4 u4Dot11QosCFPollsUnusableCount;
    INT4 i4IfIndex;
} tWsscfgMibDot11CountersEntry;

/* Structure used by Wsscfg protocol for Dot11GroupAddressesEntry */

typedef struct {
    tRBNodeEmbd Dot11GroupAddressesTableNode;
    tMacAddr Dot11Address;
    UINT1 au1Pad[2];
    INT4 i4Dot11GroupAddressesIndex;
    INT4 i4Dot11GroupAddressesStatus;
    INT4 i4IfIndex;
} tWsscfgMibDot11GroupAddressesEntry;

/* Structure used by Wsscfg protocol for Dot11EDCAEntry */

typedef struct {
    tRBNodeEmbd Dot11EDCATableNode;
    INT4 i4Dot11EDCATableIndex;
    INT4 i4Dot11EDCATableCWmin;
    INT4 i4Dot11EDCATableCWmax;
    INT4 i4Dot11EDCATableAIFSN;
    INT4 i4Dot11EDCATableTXOPLimit;
    INT4 i4Dot11EDCATableMSDULifetime;
    INT4 i4Dot11EDCATableMandatory;
    INT4 i4IfIndex;
} tWsscfgMibDot11EDCAEntry;

/* Structure used by Wsscfg protocol for Dot11QAPEDCAEntry */

typedef struct {
    tRBNodeEmbd Dot11QAPEDCATableNode;
    INT4 i4Dot11QAPEDCATableIndex;
    INT4 i4Dot11QAPEDCATableCWmin;
    INT4 i4Dot11QAPEDCATableCWmax;
    INT4 i4Dot11QAPEDCATableAIFSN;
    INT4 i4Dot11QAPEDCATableTXOPLimit;
    INT4 i4Dot11QAPEDCATableMSDULifetime;
    INT4 i4Dot11QAPEDCATableMandatory;
    INT4 i4IfIndex;
} tWsscfgMibDot11QAPEDCAEntry;

/* Structure used by Wsscfg protocol for Dot11QosCountersEntry */

typedef struct {
    tRBNodeEmbd Dot11QosCountersTableNode;
    UINT4 u4Dot11QosTransmittedFragmentCount;
    UINT4 u4Dot11QosFailedCount;
    UINT4 u4Dot11QosRetryCount;
    UINT4 u4Dot11QosMultipleRetryCount;
    UINT4 u4Dot11QosFrameDuplicateCount;
    UINT4 u4Dot11QosRTSSuccessCount;
    UINT4 u4Dot11QosRTSFailureCount;
    UINT4 u4Dot11QosACKFailureCount;
    UINT4 u4Dot11QosReceivedFragmentCount;
    UINT4 u4Dot11QosTransmittedFrameCount;
    UINT4 u4Dot11QosDiscardedFrameCount;
    UINT4 u4Dot11QosMPDUsReceivedCount;
    UINT4 u4Dot11QosRetriesReceivedCount;
    INT4 i4Dot11QosCountersIndex;
    INT4 i4IfIndex;
} tWsscfgMibDot11QosCountersEntry;

/* Structure used by Wsscfg protocol for Dot11ResourceInfoEntry */

typedef struct {
    tRBNodeEmbd Dot11ResourceInfoTableNode;
    INT4 i4IfIndex;
    INT4 i4Dot11manufacturerOUILen;
    INT4 i4Dot11manufacturerNameLen;
    INT4 i4Dot11manufacturerProductNameLen;
    INT4 i4Dot11manufacturerProductVersionLen;
    UINT1 au1Dot11manufacturerOUI[3];
    UINT1 au1Align[1];
    UINT1 au1Dot11manufacturerName[128];
    UINT1 au1Dot11manufacturerProductName[128];
    UINT1 au1Dot11manufacturerProductVersion[128];
} tWsscfgMibDot11ResourceInfoEntry;

/* Structure used by Wsscfg protocol for Dot11PhyOperationEntry */

typedef struct {
    tRBNodeEmbd Dot11PhyOperationTableNode;
    INT4 i4Dot11PHYType;
    INT4 i4Dot11CurrentRegDomain;
    INT4 i4Dot11TempType;
    INT4 i4IfIndex;
} tWsscfgMibDot11PhyOperationEntry;

/* Structure used by Wsscfg protocol for Dot11PhyAntennaEntry */

typedef struct {
    tRBNodeEmbd Dot11PhyAntennaTableNode;
    INT4 i4Dot11CurrentTxAntenna;
    INT4 i4Dot11DiversitySupport;
    INT4 i4Dot11CurrentRxAntenna;
    INT4 i4IfIndex;
} tWsscfgMibDot11PhyAntennaEntry;

/* Structure used by Wsscfg protocol for Dot11PhyTxPowerEntry */

typedef struct {
    tRBNodeEmbd Dot11PhyTxPowerTableNode;
    INT4 i4Dot11NumberSupportedPowerLevels;
    INT4 i4Dot11TxPowerLevel1;
    INT4 i4Dot11TxPowerLevel2;
    INT4 i4Dot11TxPowerLevel3;
    INT4 i4Dot11TxPowerLevel4;
    INT4 i4Dot11TxPowerLevel5;
    INT4 i4Dot11TxPowerLevel6;
    INT4 i4Dot11TxPowerLevel7;
    INT4 i4Dot11TxPowerLevel8;
    INT4 i4Dot11CurrentTxPowerLevel;
    INT4 i4IfIndex;
} tWsscfgMibDot11PhyTxPowerEntry;

/* Structure used by Wsscfg protocol for Dot11PhyFHSSEntry */

typedef struct {
    tRBNodeEmbd Dot11PhyFHSSTableNode;
    INT4 i4Dot11HopTime;
    INT4 i4Dot11CurrentChannelNumber;
    INT4 i4Dot11MaxDwellTime;
    INT4 i4Dot11CurrentDwellTime;
    INT4 i4Dot11CurrentSet;
    INT4 i4Dot11CurrentPattern;
    INT4 i4Dot11CurrentIndex;
    INT4 i4Dot11EHCCPrimeRadix;
    INT4 i4Dot11EHCCNumberofChannelsFamilyIndex;
    INT4 i4Dot11EHCCCapabilityImplemented;
    INT4 i4Dot11EHCCCapabilityEnabled;
    INT4 i4Dot11HopAlgorithmAdopted;
    INT4 i4Dot11RandomTableFlag;
    INT4 i4Dot11NumberofHoppingSets;
    INT4 i4Dot11HopModulus;
    INT4 i4Dot11HopOffset;
    INT4 i4IfIndex;
} tWsscfgMibDot11PhyFHSSEntry;

/* Structure used by Wsscfg protocol for Dot11PhyDSSSEntry */

typedef struct {
    tRBNodeEmbd Dot11PhyDSSSTableNode;
    INT4 i4Dot11CurrentChannel;
    INT4 i4Dot11CCAModeSupported;
    INT4 i4Dot11CurrentCCAMode;
    INT4 i4Dot11EDThreshold;
    INT4 i4IfIndex;
} tWsscfgMibDot11PhyDSSSEntry;

/* Structure used by Wsscfg protocol for Dot11PhyIREntry */

typedef struct {
    tRBNodeEmbd Dot11PhyIRTableNode;
    INT4 i4Dot11CCAWatchdogTimerMax;
    INT4 i4Dot11CCAWatchdogCountMax;
    INT4 i4Dot11CCAWatchdogTimerMin;
    INT4 i4Dot11CCAWatchdogCountMin;
    INT4 i4IfIndex;
} tWsscfgMibDot11PhyIREntry;

/* Structure used by Wsscfg protocol for Dot11RegDomainsSupportedEntry */

typedef struct {
    tRBNodeEmbd Dot11RegDomainsSupportedTableNode;
    INT4 i4Dot11RegDomainsSupportedIndex;
    INT4 i4Dot11RegDomainsSupportedValue;
    INT4 i4IfIndex;
} tWsscfgMibDot11RegDomainsSupportedEntry;

/* Structure used by Wsscfg protocol for Dot11AntennasListEntry */

typedef struct {
    tRBNodeEmbd Dot11AntennasListTableNode;
    INT4 i4Dot11AntennaListIndex;
    INT4 i4Dot11SupportedTxAntenna;
    INT4 i4Dot11SupportedRxAntenna;
    INT4 i4Dot11DiversitySelectionRx;
    INT4 i4IfIndex;
} tWsscfgMibDot11AntennasListEntry;

/* Structure used by Wsscfg protocol for Dot11SupportedDataRatesTxEntry */

typedef struct {
    tRBNodeEmbd Dot11SupportedDataRatesTxTableNode;
    INT4 i4Dot11SupportedDataRatesTxIndex;
    INT4 i4Dot11SupportedDataRatesTxValue;
    INT4 i4IfIndex;
} tWsscfgMibDot11SupportedDataRatesTxEntry;

/* Structure used by Wsscfg protocol for Dot11SupportedDataRatesRxEntry */

typedef struct {
    tRBNodeEmbd Dot11SupportedDataRatesRxTableNode;
    INT4 i4Dot11SupportedDataRatesRxIndex;
    INT4 i4Dot11SupportedDataRatesRxValue;
    INT4 i4IfIndex;
} tWsscfgMibDot11SupportedDataRatesRxEntry;

/* Structure used by Wsscfg protocol for Dot11PhyOFDMEntry */

typedef struct {
    tRBNodeEmbd Dot11PhyOFDMTableNode;
    INT4 i4Dot11CurrentFrequency;
    INT4 i4Dot11TIThreshold;
    INT4 i4Dot11FrequencyBandsSupported;
    INT4 i4Dot11ChannelStartingFactor;
    INT4 i4Dot11FiveMHzOperationImplemented;
    INT4 i4Dot11TenMHzOperationImplemented;
    INT4 i4Dot11TwentyMHzOperationImplemented;
    INT4 i4Dot11PhyOFDMChannelWidth;
    INT4 i4IfIndex;
} tWsscfgMibDot11PhyOFDMEntry;

/* Structure used by Wsscfg protocol for Dot11PhyHRDSSSEntry */

typedef struct {
    tRBNodeEmbd Dot11PhyHRDSSSTableNode;
    INT4 i4Dot11ShortPreambleOptionImplemented;
    INT4 i4Dot11PBCCOptionImplemented;
    INT4 i4Dot11ChannelAgilityPresent;
    INT4 i4Dot11ChannelAgilityEnabled;
    INT4 i4Dot11HRCCAModeSupported;
    INT4 i4IfIndex;
} tWsscfgMibDot11PhyHRDSSSEntry;

/* Structure used by Wsscfg protocol for Dot11HoppingPatternEntry */

typedef struct {
    tRBNodeEmbd Dot11HoppingPatternTableNode;
    INT4 i4Dot11HoppingPatternIndex;
    INT4 i4Dot11RandomTableFieldNumber;
    INT4 i4IfIndex;
} tWsscfgMibDot11HoppingPatternEntry;

/* Structure used by Wsscfg protocol for Dot11PhyERPEntry */

typedef struct {
    tRBNodeEmbd Dot11PhyERPTableNode;
    INT4 i4Dot11ERPPBCCOptionImplemented;
    INT4 i4Dot11ERPBCCOptionEnabled;
    INT4 i4Dot11DSSSOFDMOptionImplemented;
    INT4 i4Dot11DSSSOFDMOptionEnabled;
    INT4 i4Dot11ShortSlotTimeOptionImplemented;
    INT4 i4Dot11ShortSlotTimeOptionEnabled;
    INT4 i4IfIndex;
} tWsscfgMibDot11PhyERPEntry;

/* Structure used by Wsscfg protocol for Dot11RSNAConfigEntry */

typedef struct {
    tRBNodeEmbd Dot11RSNAConfigTableNode;
    UINT4 u4Dot11RSNAConfigPairwiseKeysSupported;
    UINT4 u4Dot11RSNAConfigGroupRekeyTime;
    UINT4 u4Dot11RSNAConfigGroupRekeyPackets;
    UINT4 u4Dot11RSNAConfigGroupUpdateCount;
    UINT4 u4Dot11RSNAConfigPairwiseUpdateCount;
    UINT4 u4Dot11RSNAConfigGroupCipherSize;
    UINT4 u4Dot11RSNAConfigPMKLifetime;
    UINT4 u4Dot11RSNAConfigPMKReauthThreshold;
    UINT4 u4Dot11RSNAConfigSATimeout;
    UINT4 u4Dot11RSNATKIPCounterMeasuresInvoked;
    UINT4 u4Dot11RSNA4WayHandshakeFailures;
    UINT4 u4Dot11RSNAConfigSTKKeysSupported;
    UINT4 u4Dot11RSNAConfigSTKRekeyTime;
    UINT4 u4Dot11RSNAConfigSMKUpdateCount;
    UINT4 u4Dot11RSNAConfigSTKCipherSize;
    UINT4 u4Dot11RSNAConfigSMKLifetime;
    UINT4 u4Dot11RSNAConfigSMKReauthThreshold;
    UINT4 u4Dot11RSNASMKHandshakeFailures;
    INT4 i4Dot11RSNAConfigVersion;
    INT4 i4Dot11RSNAConfigGroupRekeyMethod;
    INT4 i4Dot11RSNAConfigGroupRekeyStrict;
    INT4 i4Dot11RSNAConfigNumberOfPTKSAReplayCounters;
    INT4 i4Dot11RSNAConfigNumberOfGTKSAReplayCounters;
    INT4 i4Dot11RSNAConfigNumberOfSTKSAReplayCounters;
    INT4 i4IfIndex;
    INT4 i4Dot11RSNAConfigGroupCipherLen;
    INT4 i4Dot11RSNAConfigPSKValueLen;
    INT4 i4Dot11RSNAConfigPSKPassPhraseLen;
    INT4 i4Dot11RSNAAuthenticationSuiteSelectedLen;
    INT4 i4Dot11RSNAPairwiseCipherSelectedLen;
    INT4 i4Dot11RSNAGroupCipherSelectedLen;
    INT4 i4Dot11RSNAPMKIDUsedLen;
    INT4 i4Dot11RSNAAuthenticationSuiteRequestedLen;
    INT4 i4Dot11RSNAPairwiseCipherRequestedLen;
    INT4 i4Dot11RSNAGroupCipherRequestedLen;
    INT4 i4Dot11RSNAConfigSTKCipherLen;
    INT4 i4Dot11RSNAPairwiseSTKSelectedLen;
    UINT1 au1Dot11RSNAConfigGroupCipher[4];
    UINT1 au1Dot11RSNAConfigPSKValue[32];
    UINT1 au1Dot11RSNAConfigPSKPassPhrase[256];
    UINT1 au1Dot11RSNAAuthenticationSuiteSelected[4];
    UINT1 au1Dot11RSNAPairwiseCipherSelected[4];
    UINT1 au1Dot11RSNAGroupCipherSelected[4];
    UINT1 au1Dot11RSNAPMKIDUsed[16];
    UINT1 au1Dot11RSNAAuthenticationSuiteRequested[4];
    UINT1 au1Dot11RSNAPairwiseCipherRequested[4];
    UINT1 au1Dot11RSNAGroupCipherRequested[4];
    UINT1 au1Dot11RSNAConfigSTKCipher[4];
    UINT1 au1Dot11RSNAPairwiseSTKSelected[4];
} tWsscfgMibDot11RSNAConfigEntry;

/* Structure used by Wsscfg protocol for Dot11RSNAConfigPairwiseCiphersEntry */

typedef struct {
    tRBNodeEmbd Dot11RSNAConfigPairwiseCiphersTableNode;
    UINT4 u4Dot11RSNAConfigPairwiseCipherIndex;
    UINT4 u4Dot11RSNAConfigPairwiseCipherSize;
    INT4 i4Dot11RSNAConfigPairwiseCipherEnabled;
    INT4 i4IfIndex;
    INT4 i4Dot11RSNAConfigPairwiseCipherLen;
    UINT1 au1Dot11RSNAConfigPairwiseCipher[4];
} tWsscfgMibDot11RSNAConfigPairwiseCiphersEntry;

/* Structure used by Wsscfg protocol for Dot11RSNAConfigAuthenticationSuitesEntry */

typedef struct {
    tRBNodeEmbd Dot11RSNAConfigAuthenticationSuitesTableNode;
    UINT4 u4Dot11RSNAConfigAuthenticationSuiteIndex;
    INT4 i4Dot11RSNAConfigAuthenticationSuiteEnabled;
    INT4 i4Dot11RSNAConfigAuthenticationSuiteLen;
    UINT1 au1Dot11RSNAConfigAuthenticationSuite[4];
} tWsscfgMibDot11RSNAConfigAuthenticationSuitesEntry;

/* Structure used by Wsscfg protocol for Dot11RSNAStatsEntry */

typedef struct {
    tRBNodeEmbd Dot11RSNAStatsTableNode;
    tMacAddr Dot11RSNAStatsSTAAddress;
    UINT1 au1Pad[2];
    UINT4 u4Dot11RSNAStatsIndex;
    UINT4 u4Dot11RSNAStatsVersion;
    UINT4 u4Dot11RSNAStatsTKIPICVErrors;
    UINT4 u4Dot11RSNAStatsTKIPLocalMICFailures;
    UINT4 u4Dot11RSNAStatsTKIPRemoteMICFailures;
    UINT4 u4Dot11RSNAStatsCCMPReplays;
    UINT4 u4Dot11RSNAStatsCCMPDecryptErrors;
    UINT4 u4Dot11RSNAStatsTKIPReplays;
    INT4 i4IfIndex;
    INT4 i4Dot11RSNAStatsSelectedPairwiseCipherLen;
    UINT1 au1Dot11RSNAStatsSelectedPairwiseCipher[4];
} tWsscfgMibDot11RSNAStatsEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in CapwapDot11WlanEntry */

typedef struct {
    BOOL1 bCapwapDot11WlanProfileId;
    BOOL1 bCapwapDot11WlanMacType;
    BOOL1 bCapwapDot11WlanTunnelMode;
    BOOL1 bCapwapDot11WlanRowStatus;
} tWsscfgIsSetCapwapDot11WlanEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in CapwapDot11WlanBindEntry */

typedef struct {
    BOOL1 bCapwapDot11WlanBindRowStatus;
    BOOL1 bIfIndex;
    BOOL1 bCapwapDot11WlanProfileId;
    UINT1 u1Pad;
} tWsscfgIsSetCapwapDot11WlanBindEntry;



/* Structure used by Wsscfg protocol for CapwapDot11WlanEntry */

typedef struct {
    tRBNodeEmbd CapwapDot11WlanTableNode;
    UINT4 u4CapwapDot11WlanProfileId;
    INT4 i4CapwapDot11WlanProfileIfIndex;
    INT4 i4CapwapDot11WlanMacType;
    INT4 i4CapwapDot11WlanRowStatus;
    INT4 i4CapwapDot11WlanTunnelModeLen;
    UINT1 au1CapwapDot11WlanTunnelMode[256];
} tWsscfgMibCapwapDot11WlanEntry;

/* Structure used by Wsscfg protocol for CapwapDot11WlanBindEntry */

typedef struct {
    tRBNodeEmbd CapwapDot11WlanBindTableNode;
    UINT4 u4CapwapDot11WlanBindWlanId;
    UINT4 u4CapwapDot11WlanProfileId;
    INT4 i4CapwapDot11WlanBindBssIfIndex;
    INT4 i4CapwapDot11WlanBindRowStatus;
    INT4 i4IfIndex;
} tWsscfgMibCapwapDot11WlanBindEntry;

/* Structure used by CLI to indicate which 
 all objects to be set in FsDot11StationConfigEntry */
typedef struct {
    BOOL1 bFsDot11SupressSSID;
    BOOL1 bFsDot11VlanId;
    BOOL1 bFsDot11BandwidthThresh;
    BOOL1 bIfIndex;
} tWsscfgIsSetFsDot11StationConfigEntry;


/* Structure used by CLI to indicate which 
 *  all objects to be set in FsDot11ExternalWebAuthProfileEntry */
typedef struct {
    BOOL1 bFsDot11ExternalWebAuthMethod;
    BOOL1 bFsDot11ExternalWebAuthUrl;
    BOOL1 bIfIndex;
    UINT1 au1Pad;
} tWsscfgIsSetFsDot11ExternalWebAuthProfileEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsDot11CapabilityProfileEntry */

typedef struct {
    BOOL1 bFsDot11CapabilityProfileName;
    BOOL1 bFsDot11CFPollable;
    BOOL1 bFsDot11CFPollRequest;
    BOOL1 bFsDot11PrivacyOptionImplemented;
    BOOL1 bFsDot11ShortPreambleOptionImplemented;
    BOOL1 bFsDot11PBCCOptionImplemented;
    BOOL1 bFsDot11ChannelAgilityPresent;
    BOOL1 bFsDot11QosOptionImplemented;
    BOOL1 bFsDot11SpectrumManagementRequired;
    BOOL1 bFsDot11ShortSlotTimeOptionImplemented;
    BOOL1 bFsDot11APSDOptionImplemented;
    BOOL1 bFsDot11DSSSOFDMOptionEnabled;
    BOOL1 bFsDot11DelayedBlockAckOptionImplemented;
    BOOL1 bFsDot11ImmediateBlockAckOptionImplemented;
    BOOL1 bFsDot11QAckOptionImplemented;
    BOOL1 bFsDot11QueueRequestOptionImplemented;
    BOOL1 bFsDot11TXOPRequestOptionImplemented;
    BOOL1 bFsDot11RSNAOptionImplemented;
    BOOL1 bFsDot11RSNAPreauthenticationImplemented;
    BOOL1 bFsDot11CapabilityRowStatus;
} tWsscfgIsSetFsDot11CapabilityProfileEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsDot11AuthenticationProfileEntry */

typedef struct {
    BOOL1 bFsDot11AuthenticationProfileName;
    BOOL1 bFsDot11AuthenticationAlgorithm;
    BOOL1 bFsDot11WepKeyIndex;
    BOOL1 bFsDot11WepKeyType;
    BOOL1 bFsDot11WepKeyLength;
    BOOL1 bFsDot11WepKey;
    BOOL1 bFsDot11WebAuthentication;
    BOOL1 bFsDot11AuthenticationRowStatus;
} tWsscfgIsSetFsDot11AuthenticationProfileEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsSecurityWebAuthGuestInfoEntry */

typedef struct {
    BOOL1 bFsSecurityWebAuthUName;
    BOOL1 bFsSecurityWebAuthUPassword;
    BOOL1 bFsSecurityWlanProfileId;
    BOOL1 bFsSecurityWebAuthUserLifetime;
    BOOL1 bFsSecurityWebAuthGuestInfoRowStatus;
    UINT1 au1Pad[3];
} tWsscfgIsSetFsSecurityWebAuthGuestInfoEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsStationQosParamsEntry */

typedef struct {
    BOOL1 bFsStaMacAddress;
    BOOL1 bFsStaQoSPriority;
    BOOL1 bFsStaQoSDscp;
    BOOL1 bIfIndex;
} tWsscfgIsSetFsStationQosParamsEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsVlanIsolationEntry */

typedef struct {
    BOOL1 bFsVlanIsolation;
    BOOL1 bIfIndex;
    UINT1 au1Pad[2];
} tWsscfgIsSetFsVlanIsolationEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsDot11RadioConfigEntry */

typedef struct {
    BOOL1 bFsDot11RadioType;
    BOOL1 bFsDot11RowStatus;
    BOOL1 bIfIndex;
    UINT1 u1Pad;
} tWsscfgIsSetFsDot11RadioConfigEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsDot11QosProfileEntry */

typedef struct {
    BOOL1 bFsDot11QosProfileName;
    BOOL1 bFsDot11QosTraffic;
    BOOL1 bFsDot11QosPassengerTrustMode;
    BOOL1 bFsDot11QosRateLimit;
    BOOL1 bFsDot11UpStreamCIR;
    BOOL1 bFsDot11UpStreamCBS;
    BOOL1 bFsDot11UpStreamEIR;
    BOOL1 bFsDot11UpStreamEBS;
    BOOL1 bFsDot11DownStreamCIR;
    BOOL1 bFsDot11DownStreamCBS;
    BOOL1 bFsDot11DownStreamEIR;
    BOOL1 bFsDot11DownStreamEBS;
    BOOL1 bFsDot11QosRowStatus;
    UINT1 au1Pad[3];
} tWsscfgIsSetFsDot11QosProfileEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsDot11WlanCapabilityProfileEntry */

typedef struct {
    BOOL1 bFsDot11WlanCFPollable;
    BOOL1 bFsDot11WlanCFPollRequest;
    BOOL1 bFsDot11WlanPrivacyOptionImplemented;
    BOOL1 bFsDot11WlanShortPreambleOptionImplemented;
    BOOL1 bFsDot11WlanPBCCOptionImplemented;
    BOOL1 bFsDot11WlanChannelAgilityPresent;
    BOOL1 bFsDot11WlanQosOptionImplemented;
    BOOL1 bFsDot11WlanSpectrumManagementRequired;
    BOOL1 bFsDot11WlanShortSlotTimeOptionImplemented;
    BOOL1 bFsDot11WlanAPSDOptionImplemented;
    BOOL1 bFsDot11WlanDSSSOFDMOptionEnabled;
    BOOL1 bFsDot11WlanDelayedBlockAckOptionImplemented;
    BOOL1 bFsDot11WlanImmediateBlockAckOptionImplemented;
    BOOL1 bFsDot11WlanQAckOptionImplemented;
    BOOL1 bFsDot11WlanQueueRequestOptionImplemented;
    BOOL1 bFsDot11WlanTXOPRequestOptionImplemented;
    BOOL1 bFsDot11WlanRSNAOptionImplemented;
    BOOL1 bFsDot11WlanRSNAPreauthenticationImplemented;
    BOOL1 bFsDot11WlanCapabilityRowStatus;
    BOOL1 bIfIndex;
} tWsscfgIsSetFsDot11WlanCapabilityProfileEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsDot11WlanAuthenticationProfileEntry */

typedef struct {
    BOOL1 bFsDot11WlanAuthenticationAlgorithm;
    BOOL1 bFsDot11WlanWepKeyIndex;
    BOOL1 bFsDot11WlanWepKeyType;
    BOOL1 bFsDot11WlanWepKeyLength;
    BOOL1 bFsDot11WlanWepKey;
    BOOL1 bFsDot11WlanWebAuthentication;
    BOOL1 bFsDot11WlanAuthenticationRowStatus;
    BOOL1 bIfIndex;
} tWsscfgIsSetFsDot11WlanAuthenticationProfileEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsDot11WlanQosProfileEntry */

typedef struct {
    BOOL1 bFsDot11WlanQosTraffic;
    BOOL1 bFsDot11WlanQosPassengerTrustMode;
    BOOL1 bFsDot11WlanQosRateLimit;
    BOOL1 bFsDot11WlanUpStreamCIR;
    BOOL1 bFsDot11WlanUpStreamCBS;
    BOOL1 bFsDot11WlanUpStreamEIR;
    BOOL1 bFsDot11WlanUpStreamEBS;
    BOOL1 bFsDot11WlanDownStreamCIR;
    BOOL1 bFsDot11WlanDownStreamCBS;
    BOOL1 bFsDot11WlanDownStreamEIR;
    BOOL1 bFsDot11WlanDownStreamEBS;
    BOOL1 bFsDot11WlanQosRowStatus;
    BOOL1 bIfIndex;
    UINT1 au1Pad[3];
} tWsscfgIsSetFsDot11WlanQosProfileEntry;

/* Structure used by Wsscfg protocol for FsDot11ClientSummaryEntry */

typedef struct
{
 tRBNodeEmbd  FsDot11ClientSummaryTableNode;
 tMacAddr FsDot11ClientMacAddress;
 UINT1       au1pad[2];
    UINT4 u4FsDot11WlanProfileId;
 UINT4 u4FsDot11WtpRadioId;
 INT4 i4FsDot11AuthStatus;
 INT4 i4FsDot11AssocStatus;
 INT4 i4FsDot11WtpProfileNameLen;
 UINT1 au1FsDot11WtpProfileName[256];
} tWsscfgMibFsDot11ClientSummaryEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsDot11RadioQosEntry */

typedef struct {
    BOOL1 bFsDot11TaggingPolicy;
    BOOL1 bIfIndex;
    UINT1 au1Pad[2];
} tWsscfgIsSetFsDot11RadioQosEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsDot11QAPEntry */

typedef struct {
    BOOL1 bFsDot11QueueDepth;
    BOOL1 bFsDot11PriorityValue;
    BOOL1 bFsDot11DscpValue;
    BOOL1 bIfIndex;
    BOOL1 bDot11EDCATableIndex;
    UINT1 au1Pad[3];
} tWsscfgIsSetFsDot11QAPEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsQAPProfileEntry */

typedef struct {
    BOOL1 bFsQAPProfileName;
    BOOL1 bFsQAPProfileIndex;
    BOOL1 bFsQAPProfileCWmin;
    BOOL1 bFsQAPProfileCWmax;
    BOOL1 bFsQAPProfileAIFSN;
    BOOL1 bFsQAPProfileTXOPLimit;
    BOOL1 bFsQAPProfileAdmissionControl;
    BOOL1 bFsQAPProfilePriorityValue;
    BOOL1 bFsQAPProfileDscpValue;
    BOOL1 bFsQAPProfileRowStatus;
    UINT1 au1Pad[2];
} tWsscfgIsSetFsQAPProfileEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsDot11CapabilityMappingEntry */

typedef struct {
    BOOL1 bFsDot11CapabilityMappingProfileName;
    BOOL1 bFsDot11CapabilityMappingRowStatus;
    BOOL1 bIfIndex;
    UINT1 u1Pad;
} tWsscfgIsSetFsDot11CapabilityMappingEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsDot11AuthMappingEntry */

typedef struct {
    BOOL1 bFsDot11AuthMappingProfileName;
    BOOL1 bFsDot11AuthMappingRowStatus;
    BOOL1 bIfIndex;
    UINT1 u1Pad;
} tWsscfgIsSetFsDot11AuthMappingEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsDot11QosMappingEntry */

typedef struct {
    BOOL1 bFsDot11QosMappingProfileName;
    BOOL1 bFsDot11QosMappingRowStatus;
    BOOL1 bIfIndex;
    UINT1 u1Pad;
} tWsscfgIsSetFsDot11QosMappingEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsDot11AntennasListEntry */

typedef struct {
    BOOL1 bFsAntennaMode;
    BOOL1 bFsAntennaSelection;
    BOOL1 bIfIndex;
    BOOL1 bDot11AntennaListIndex;
} tWsscfgIsSetFsDot11AntennasListEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsDot11WlanEntry */

typedef struct {
    BOOL1 bFsDot11WlanProfileIfIndex;
    BOOL1 bFsDot11WlanRowStatus;
    BOOL1 bCapwapDot11WlanProfileId;
    UINT1 u1Pad;
} tWsscfgIsSetFsDot11WlanEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsDot11WlanBindEntry */

typedef struct {
    BOOL1 bFsDot11WlanBindWlanId;
    BOOL1 bFsDot11WlanBindBssIfIndex;
    BOOL1 bFsDot11WlanBindRowStatus;
    BOOL1 bIfIndex;
    BOOL1 bCapwapDot11WlanProfileId;
    UINT1 au1Pad[3];
} tWsscfgIsSetFsDot11WlanBindEntry;

/* Structure used by CLI to indicate which 
 *  all objects to be set in FsDot11nConfigEntry */

typedef struct {
    BOOL1 bFsDot11nConfigShortGIfor20MHz;
    BOOL1 bFsDot11nConfigShortGIfor40MHz;
    BOOL1 bFsDot11nConfigChannelWidth;
    BOOL1 bIfIndex;
} tWsscfgIsSetFsDot11nConfigEntry;


/* Structure used by CLI to indicate which 
 *  all objects to be set in FsDot11nMCSDataRateEntry */

typedef struct {
    BOOL1 bFsDot11nMCSDataRateIndex;
    BOOL1 bFsDot11nMCSDataRate;
    BOOL1 bIfIndex;
    UINT1 u1Pad;
} tWsscfgIsSetFsDot11nMCSDataRateEntry;

/* Structure used by CLI to indicate which 
 all objects to be set in FsWtpImageUpgradeEntry */

typedef struct {
    BOOL1 bFsWtpImageVersion;
    BOOL1 bFsWtpUpgradeDev;
    BOOL1 bFsWtpName;
    BOOL1 bFsWtpImageName;
    BOOL1 bFsWtpAddressType;
    BOOL1 bFsWtpServerIP;
    BOOL1 bFsWtpRowStatus;
    BOOL1 bCapwapBaseWtpProfileWtpModelNumber;
} tWsscfgIsSetFsWtpImageUpgradeEntry;



/* Structure used by Wsscfg protocol for FsDot11StationConfigEntry */

typedef struct {
    tRBNodeEmbd FsDot11StationConfigTableNode;
    INT4 i4FsDot11SupressSSID;
    INT4 i4FsDot11VlanId;
    INT4 i4FsDot11BandwidthThresh;
    INT4 i4IfIndex;
    INT4 i4FsDtimPeriod;
} tWsscfgMibFsDot11StationConfigEntry;


/* Structure used by Wsscfg protocol for FsDot11ExternalWebAuthProfileEntry */

typedef struct {
    INT4 i4FsDot11ExternalWebAuthMethod;
    UINT1 au1FsDot11ExternalWebAuthUrl[120];
    INT4 i4IfIndex;
} tWsscfgMibFsDot11ExternalWebAuthProfileEntry;



/* Structure used by Wsscfg protocol for FsDot11CapabilityProfileEntry */

typedef struct {
    tRBNodeEmbd FsDot11CapabilityProfileTableNode;
    INT4 i4FsDot11CFPollable;
    INT4 i4FsDot11CFPollRequest;
    INT4 i4FsDot11PrivacyOptionImplemented;
    INT4 i4FsDot11ShortPreambleOptionImplemented;
    INT4 i4FsDot11PBCCOptionImplemented;
    INT4 i4FsDot11ChannelAgilityPresent;
    INT4 i4FsDot11QosOptionImplemented;
    INT4 i4FsDot11SpectrumManagementRequired;
    INT4 i4FsDot11ShortSlotTimeOptionImplemented;
    INT4 i4FsDot11APSDOptionImplemented;
    INT4 i4FsDot11DSSSOFDMOptionEnabled;
    INT4 i4FsDot11DelayedBlockAckOptionImplemented;
    INT4 i4FsDot11ImmediateBlockAckOptionImplemented;
    INT4 i4FsDot11QAckOptionImplemented;
    INT4 i4FsDot11QueueRequestOptionImplemented;
    INT4 i4FsDot11TXOPRequestOptionImplemented;
    INT4 i4FsDot11RSNAOptionImplemented;
    INT4 i4FsDot11RSNAPreauthenticationImplemented;
    INT4 i4FsDot11CapabilityRowStatus;
    INT4 i4FsDot11CapabilityProfileNameLen;
    UINT1 au1FsDot11CapabilityProfileName[32];
} tWsscfgMibFsDot11CapabilityProfileEntry;

/* Structure used by Wsscfg protocol for FsDot11AuthenticationProfileEntry */

typedef struct {
    tRBNodeEmbd FsDot11AuthenticationProfileTableNode;
    INT4 i4FsDot11AuthenticationAlgorithm;
    INT4 i4FsDot11WepKeyIndex;
    INT4 i4FsDot11WepKeyType;
    INT4 i4FsDot11WepKeyLength;
    INT4 i4FsDot11WebAuthentication;
    INT4 i4FsDot11AuthenticationRowStatus;
    INT4 i4FsDot11AuthenticationProfileNameLen;
    INT4 i4FsDot11WepKeyLen;
    UINT1 au1FsDot11AuthenticationProfileName[32];
    UINT1 au1FsDot11WepKey[104];
} tWsscfgMibFsDot11AuthenticationProfileEntry;

/* Structure used by Wsscfg protocol for FsSecurityWebAuthGuestInfoEntry */

typedef struct {
    tRBNodeEmbd FsSecurityWebAuthGuestInfoTableNode;
    INT4 i4FsSecurityWlanProfileId;
    INT4 i4FsSecurityWebAuthUserLifetime;
    INT4 i4FsSecurityWebAuthGuestInfoRowStatus;
    INT4 i4FsSecurityWebAuthUNameLen;
    INT4 i4FsSecurityWebAuthUPasswordLen;
    INT4 i4FsSecurityWebAuthUserEmailIdLen;
    UINT1 au1FsSecurityWebAuthUName[20];
    UINT1 au1FsSecurityWebAuthUPassword[20];
    UINT1 au1FsSecurityWebAuthUserEmailId[256];
} tWsscfgMibFsSecurityWebAuthGuestInfoEntry;

/* Structure used by Wsscfg protocol for FsStationQosParamsEntry */

typedef struct {
    tRBNodeEmbd FsStationQosParamsTableNode;
    tMacAddr FsStaMacAddress;
    UINT1 au1Pad[2];
    INT4 i4FsStaQoSPriority;
    INT4 i4FsStaQoSDscp;
    INT4 i4IfIndex;
} tWsscfgMibFsStationQosParamsEntry;

/* Structure used by Wsscfg protocol for FsVlanIsolationEntry */

typedef struct {
    tRBNodeEmbd FsVlanIsolationTableNode;
    INT4 i4FsVlanIsolation;
    INT4 i4IfIndex;
} tWsscfgMibFsVlanIsolationEntry;

/* Structure used by Wsscfg protocol for FsDot11RadioConfigEntry */

typedef struct {
    tRBNodeEmbd FsDot11RadioConfigTableNode;
    UINT4 u4FsDot11RadioType;
    INT4 i4FsDot11RadioNoOfBssIdSupported;
    INT4 i4FsDot11RadioAntennaType;
    INT4 i4FsDot11RadioFailureStatus;
    INT4 i4FsDot11RowStatus;
    INT4 i4IfIndex;
} tWsscfgMibFsDot11RadioConfigEntry;

/* Structure used by Wsscfg protocol for FsDot11QosProfileEntry */

typedef struct {
    tRBNodeEmbd FsDot11QosProfileTableNode;
    INT4 i4FsDot11QosTraffic;
    INT4 i4FsDot11QosPassengerTrustMode;
    INT4 i4FsDot11QosRateLimit;
    INT4 i4FsDot11UpStreamCIR;
    INT4 i4FsDot11UpStreamCBS;
    INT4 i4FsDot11UpStreamEIR;
    INT4 i4FsDot11UpStreamEBS;
    INT4 i4FsDot11DownStreamCIR;
    INT4 i4FsDot11DownStreamCBS;
    INT4 i4FsDot11DownStreamEIR;
    INT4 i4FsDot11DownStreamEBS;
    INT4 i4FsDot11QosRowStatus;
    INT4 i4FsDot11QosProfileNameLen;
    UINT1 au1FsDot11QosProfileName[32];
} tWsscfgMibFsDot11QosProfileEntry;

/* Structure used by Wsscfg protocol for FsDot11WlanCapabilityProfileEntry */

typedef struct {
    tRBNodeEmbd FsDot11WlanCapabilityProfileTableNode;
    INT4 i4FsDot11WlanCFPollable;
    INT4 i4FsDot11WlanCFPollRequest;
    INT4 i4FsDot11WlanPrivacyOptionImplemented;
    INT4 i4FsDot11WlanShortPreambleOptionImplemented;
    INT4 i4FsDot11WlanPBCCOptionImplemented;
    INT4 i4FsDot11WlanChannelAgilityPresent;
    INT4 i4FsDot11WlanQosOptionImplemented;
    INT4 i4FsDot11WlanSpectrumManagementRequired;
    INT4 i4FsDot11WlanShortSlotTimeOptionImplemented;
    INT4 i4FsDot11WlanAPSDOptionImplemented;
    INT4 i4FsDot11WlanDSSSOFDMOptionEnabled;
    INT4 i4FsDot11WlanDelayedBlockAckOptionImplemented;
    INT4 i4FsDot11WlanImmediateBlockAckOptionImplemented;
    INT4 i4FsDot11WlanQAckOptionImplemented;
    INT4 i4FsDot11WlanQueueRequestOptionImplemented;
    INT4 i4FsDot11WlanTXOPRequestOptionImplemented;
    INT4 i4FsDot11WlanRSNAOptionImplemented;
    INT4 i4FsDot11WlanRSNAPreauthenticationImplemented;
    INT4 i4FsDot11WlanCapabilityRowStatus;
    INT4 i4IfIndex;
} tWsscfgMibFsDot11WlanCapabilityProfileEntry;

/* Structure used by Wsscfg protocol for FsDot11WlanAuthenticationProfileEntry */

typedef struct {
    tRBNodeEmbd FsDot11WlanAuthenticationProfileTableNode;
    INT4 i4FsDot11WlanAuthenticationAlgorithm;
    INT4 i4FsDot11WlanWepKeyIndex;
    INT4 i4FsDot11WlanWepKeyType;
    INT4 i4FsDot11WlanWepKeyLength;
    INT4 i4FsDot11WlanWebAuthentication;
    INT4 i4FsDot11WlanAuthenticationRowStatus;
    INT4 i4IfIndex;
    INT4 i4FsDot11WlanWepKeyLen;
    UINT1 au1FsDot11WlanWepKey[104];
} tWsscfgMibFsDot11WlanAuthenticationProfileEntry;

/* Structure used by Wsscfg protocol for FsDot11WlanQosProfileEntry */

typedef struct {
    tRBNodeEmbd FsDot11WlanQosProfileTableNode;
    INT4 i4FsDot11WlanQosTraffic;
    INT4 i4FsDot11WlanQosPassengerTrustMode;
    INT4 i4FsDot11WlanQosRateLimit;
    INT4 i4FsDot11WlanUpStreamCIR;
    INT4 i4FsDot11WlanUpStreamCBS;
    INT4 i4FsDot11WlanUpStreamEIR;
    INT4 i4FsDot11WlanUpStreamEBS;
    INT4 i4FsDot11WlanDownStreamCIR;
    INT4 i4FsDot11WlanDownStreamCBS;
    INT4 i4FsDot11WlanDownStreamEIR;
    INT4 i4FsDot11WlanDownStreamEBS;
    INT4 i4FsDot11WlanQosRowStatus;
    INT4 i4IfIndex;
} tWsscfgMibFsDot11WlanQosProfileEntry;

/* Structure used by Wsscfg protocol for FsDot11RadioQosEntry */

typedef struct {
    tRBNodeEmbd FsDot11RadioQosTableNode;
    INT4 i4IfIndex;
    INT4 i4FsDot11TaggingPolicyLen;
    UINT1 au1FsDot11TaggingPolicy[256];
} tWsscfgMibFsDot11RadioQosEntry;

/* Structure used by Wsscfg protocol for FsDot11QAPEntry */

typedef struct {
    tRBNodeEmbd FsDot11QAPTableNode;
    INT4 i4FsDot11QueueDepth;
    INT4 i4FsDot11PriorityValue;
    INT4 i4FsDot11DscpValue;
    INT4 i4IfIndex;
    INT4 i4Dot11EDCATableIndex;
} tWsscfgMibFsDot11QAPEntry;

/* Structure used by Wsscfg protocol for FsQAPProfileEntry */

typedef struct {
    tRBNodeEmbd FsQAPProfileTableNode;
    INT4 i4FsQAPProfileIndex;
    INT4 i4FsQAPProfileCWmin;
    INT4 i4FsQAPProfileCWmax;
    INT4 i4FsQAPProfileAIFSN;
    INT4 i4FsQAPProfileTXOPLimit;
    INT4 i4FsQAPProfileAdmissionControl;
    INT4 i4FsQAPProfilePriorityValue;
    INT4 i4FsQAPProfileDscpValue;
    INT4 i4FsQAPProfileRowStatus;
    INT4 i4FsQAPProfileNameLen;
    UINT1 au1FsQAPProfileName[1024];
} tWsscfgMibFsQAPProfileEntry;

/* Structure used by Wsscfg protocol for FsDot11CapabilityMappingEntry */

typedef struct {
    tRBNodeEmbd FsDot11CapabilityMappingTableNode;
    INT4 i4FsDot11CapabilityMappingRowStatus;
    INT4 i4IfIndex;
    INT4 i4FsDot11CapabilityMappingProfileNameLen;
    UINT1 au1FsDot11CapabilityMappingProfileName[32];
} tWsscfgMibFsDot11CapabilityMappingEntry;

/* Structure used by Wsscfg protocol for FsDot11AuthMappingEntry */

typedef struct {
    tRBNodeEmbd FsDot11AuthMappingTableNode;
    INT4 i4FsDot11AuthMappingRowStatus;
    INT4 i4IfIndex;
    INT4 i4FsDot11AuthMappingProfileNameLen;
    UINT1 au1FsDot11AuthMappingProfileName[32];
} tWsscfgMibFsDot11AuthMappingEntry;

/* Structure used by Wsscfg protocol for FsDot11QosMappingEntry */

typedef struct {
    tRBNodeEmbd FsDot11QosMappingTableNode;
    INT4 i4FsDot11QosMappingRowStatus;
    INT4 i4IfIndex;
    INT4 i4FsDot11QosMappingProfileNameLen;
    UINT1 au1FsDot11QosMappingProfileName[32];
} tWsscfgMibFsDot11QosMappingEntry;

/* Structure used by Wsscfg protocol for FsDot11AntennasListEntry */

typedef struct {
    tRBNodeEmbd FsDot11AntennasListTableNode;
    INT4 i4FsAntennaMode;
    INT4 i4FsAntennaSelection;
    INT4 i4IfIndex;
    INT4 i4Dot11AntennaListIndex;
} tWsscfgMibFsDot11AntennasListEntry;

/* Structure used by Wsscfg protocol for FsDot11WlanEntry */
typedef struct  {
    tRBNodeEmbd APGroupEntryNode;
    INT4  i4ApGroupRowstatus;
    UINT2 u2ApGroupId;
    UINT2 u2RadioPolicy;
    UINT2 u2InterfaceVlan;
    UINT2 u2NoOfWlan;
    UINT1 u4GroupNameLen;
    UINT1 u4GroupNameDesLen;
    UINT1 au1padding[2];
    UINT1 au1ApGroupName[32];
    UINT1 au1ApGroupNameDes[128];
} tApGroupConfigEntry;

typedef struct  {
    tRBNodeEmbd APGroupWTPEntryNode;
    UINT2 u2ApGroupId;
    UINT2 u2ApGroupWTPIndex;
    INT4  i4ApGroupWTPRowstatus;
    UINT1 au1WTPProfile[256];
} tApGroupConfigWTPEntry;

typedef struct  {
    tRBNodeEmbd APGroupWLANEntryNode;
    UINT2 u2ApGroupId;
    UINT2 u2ApGroupWLANIndex;
    UINT4 u4WlanId;
    INT4  i4ApGroupWLANRowstatus;
    UINT1 au1WlanProfileName[32];
} tApGroupConfigWLANEntry;


typedef struct {
    tRBNodeEmbd FsDot11WlanTableNode;
    UINT4 u4CapwapDot11WlanProfileId;
    INT4 i4FsDot11WlanProfileIfIndex;
    INT4 i4FsDot11WlanRowStatus;
} tWsscfgMibFsDot11WlanEntry;

/* Structure used by Wsscfg protocol for FsDot11WlanBindEntry */

typedef struct {
    tRBNodeEmbd FsDot11WlanBindTableNode;
    UINT4 u4FsDot11WlanBindWlanId;
    UINT4 u4CapwapDot11WlanProfileId;
    INT4 i4FsDot11WlanBindBssIfIndex;
    INT4 i4FsDot11WlanBindRowStatus;
    INT4 i4IfIndex;
} tWsscfgMibFsDot11WlanBindEntry;

/* Structure used by Wsscfg protocol for FsDot11nConfigEntry */

typedef struct {
    tRBNodeEmbd FsDot11nConfigTableNode;
    INT4 i4FsDot11nConfigShortGIfor20MHz;
    INT4 i4FsDot11nConfigShortGIfor40MHz;
    INT4 i4FsDot11nConfigChannelWidth;
    INT4 i4IfIndex;
} tWsscfgMibFsDot11nConfigEntry;

/* Structure used by Wsscfg protocol for FsDot11nMCSDataRateEntry */

typedef struct {
    tRBNodeEmbd FsDot11nMCSDataRateTableNode;
    INT4 i4FsDot11nMCSDataRateIndex;
    INT4 i4FsDot11nMCSDataRate;
    INT4 i4IfIndex;
} tWsscfgMibFsDot11nMCSDataRateEntry;

/* Structure used by Wsscfg protocol for FsWtpImageUpgradeEntry */

typedef struct {
    tRBNodeEmbd FsWtpImageUpgradeTableNode;
    INT4 i4FsWtpUpgradeDev;
    INT4 i4FsWtpAddressType;
    INT4 i4FsWtpRowStatus;
    INT4 i4FsWtpImageVersionLen;
    INT4 i4FsWtpNameLen;
    INT4 i4FsWtpImageNameLen;
    INT4 i4FsWtpServerIPLen;
    INT4 i4CapwapBaseWtpProfileWtpModelNumberLen;
    UINT1 au1FsWtpImageVersion[256];
    UINT1 au1FsWtpName[256];
    UINT1 au1FsWtpImageName[256];
    UINT1 au1FsWtpServerIP[16];
    UINT1 au1CapwapBaseWtpProfileWtpModelNumber[256];
} tWsscfgMibFsWtpImageUpgradeEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsRrmConfigEntry */

typedef struct {
    BOOL1 bFsRrmRadioType;
    BOOL1 bFsRrmDcaMode;
    BOOL1 bFsRrmDcaChannelSelectionMode;
    BOOL1 bFsRrmTpcMode;
    BOOL1 bFsRrmTpcSelectionMode;
    BOOL1 bFsRrmRowStatus;
    UINT1 au1Pad[2];
} tWsscfgIsSetFsRrmConfigEntry;



/* Structure used by Wsscfg protocol for FsRrmConfigEntry */

typedef struct {
    tRBNodeEmbd FsRrmConfigTableNode;
    INT4 i4FsRrmRadioType;
    INT4 i4FsRrmDcaMode;
    INT4 i4FsRrmDcaChannelSelectionMode;
    INT4 i4FsRrmTpcMode;
    INT4 i4FsRrmTpcSelectionMode;
    INT4 i4FsRrmRowStatus;
} tWsscfgMibFsRrmConfigEntry;

#endif
