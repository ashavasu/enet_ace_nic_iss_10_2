/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
* $Id: wsscfgtdfs.h,v 1.3 2017/11/24 10:37:08 siva Exp $
* 
*
* Description: This file contains type definitions for Wsscfg module.
*********************************************************************/

#ifndef __WSSCFGTDFS_H__
#define __WSSCFGTDFS_H__

typedef struct {
    tRBTree CapwapDot11WlanTable;
    tRBTree CapwapDot11WlanBindTable;
    tRBTree Dot11StationConfigTable;
    tRBTree Dot11AuthenticationAlgorithmsTable;
    tRBTree Dot11WEPDefaultKeysTable;
    tRBTree Dot11WEPKeyMappingsTable;
    tRBTree Dot11PrivacyTable;
    tRBTree Dot11MultiDomainCapabilityTable;
    tRBTree Dot11SpectrumManagementTable;
    tRBTree Dot11RegulatoryClassesTable;
    tRBTree Dot11OperationTable;
    tRBTree Dot11CountersTable;
    tRBTree Dot11GroupAddressesTable;
    tRBTree Dot11EDCATable;
    tRBTree Dot11QAPEDCATable;
    tRBTree Dot11QosCountersTable;
    tRBTree Dot11ResourceInfoTable;
    tRBTree Dot11PhyOperationTable;
    tRBTree Dot11PhyAntennaTable;
    tRBTree Dot11PhyTxPowerTable;
    tRBTree Dot11PhyFHSSTable;
    tRBTree Dot11PhyDSSSTable;
    tRBTree Dot11PhyIRTable;
    tRBTree Dot11RegDomainsSupportedTable;
    tRBTree Dot11AntennasListTable;
    tRBTree Dot11SupportedDataRatesTxTable;
    tRBTree Dot11SupportedDataRatesRxTable;
    tRBTree Dot11PhyOFDMTable;
    tRBTree Dot11PhyHRDSSSTable;
    tRBTree Dot11HoppingPatternTable;
    tRBTree Dot11PhyERPTable;
    tRBTree Dot11RSNAConfigTable;
    tRBTree Dot11RSNAConfigPairwiseCiphersTable;
    tRBTree Dot11RSNAConfigAuthenticationSuitesTable;
    tRBTree Dot11RSNAStatsTable;
    tRBTree FsDot11StationConfigTable;
    tRBTree FsDot11CapabilityProfileTable;
    tRBTree FsDot11AuthenticationProfileTable;
    tRBTree FsStationQosParamsTable;
    tRBTree FsVlanIsolationTable;
    tRBTree FsDot11RadioConfigTable;
    tRBTree FsDot11QosProfileTable;
    tRBTree FsDot11WlanCapabilityProfileTable;
    tRBTree FsDot11WlanAuthenticationProfileTable;
    tRBTree FsDot11WlanQosProfileTable;
    tRBTree FsDot11RadioQosTable;
    tRBTree FsDot11QAPTable;
    tRBTree FsQAPProfileTable;
    tRBTree FsDot11CapabilityMappingTable;
    tRBTree FsDot11AuthMappingTable;
    tRBTree FsDot11QosMappingTable;
    tRBTree FsDot11ClientSummaryTable;
    tRBTree FsDot11AntennasListTable;
    tRBTree FsDot11WlanTable;
    tRBTree FsDot11WlanBindTable;
    tRBTree FsDot11nConfigTable;
    tRBTree FsDot11nMCSDataRateTable;
    tRBTree FsWtpImageUpgradeTable;
    tRBTree FsWlanClientStatsTable;
    tRBTree FsWlanRadioTable;
    tRBTree FsWlanSSIDStatsTable;
    tRBTree FsRrmConfigTable;

    INT4 i4FsDot11aNetworkEnable;
    INT4 i4FsDot11bNetworkEnable;
    INT4 i4FsDot11gSupport;
    INT4 i4FsDot11anSupport;
    INT4 i4FsDot11bnSupport;
    UINT2 u2FsDot11ManagmentSSID;
    UINT1 au1Pad1[2];
    INT4 i4Dot11ResourceTypeIDNameLen;
    UINT1 au1Dot11ResourceTypeIDName[4];


    INT4 i4FsDot11CountryStringLen;
    INT4 i4Dot11CountryStringLen;
    UINT1 au1FsDot11CountryString[3];
    UINT1 au1Pad2[1];
    INT4 i4FsSecurityWebAuthType;

    INT4 i4FsSecurityWebAuthUrlLen;
    UINT1 au1FsSecurityWebAuthUrl[256];

    INT4 i4FsSecurityWebAuthRedirectUrlLen;
    UINT1 au1FsSecurityWebAuthRedirectUrl[256];
    INT4 i4FsSecurityWebAddr;

    INT4 i4FsSecurityWebAuthWebTitleLen;
    UINT1 au1FsSecurityWebAuthWebTitle[256];

    INT4 i4FsSecurityWebAuthWebMessageLen;
    UINT1 au1FsSecurityWebAuthWebMessage[256];

    INT4 i4FsSecurityWebAuthWebLogoFileNameLen;
    UINT1 au1FsSecurityWebAuthWebLogoFileName[256];

    INT4 i4FsSecurityWebAuthWebSuccMessageLen;
    UINT1 au1FsSecurityWebAuthWebSuccMessage[256];

    INT4 i4FsSecurityWebAuthWebFailMessageLen;
    UINT1 au1FsSecurityWebAuthWebFailMessage[256];

    INT4 i4FsSecurityWebAuthWebButtonTextLen;
    UINT1 au1FsSecurityWebAuthWebButtonText[256];

    INT4 i4FsSecurityWebAuthWebLoadBalInfoLen;
    UINT1 au1FsSecurityWebAuthWebLoadBalInfo[256];
    INT4 i4FsSecurityWebAuthDisplayLang;
    INT4 i4FsSecurityWebAuthColor;
    INT4 i4FsSecurityWebAuthUrlLength;
    INT4 i4FsSecurityWebAuthRedirectUrlLength;
    INT4 i4FsSecurityWebAuthWebTitleLength;
    INT4 i4FsSecurityWebAuthWebMessageLength;
    INT4 i4FsSecurityWebAuthWebLogoFileNameLength;
    INT4 i4FsSecurityWebAuthWebSuccMessageLength;
    INT4 i4FsSecurityWebAuthWebFailMessageLength;
    INT4 i4FsSecurityWebAuthWebButtonTextLength;
    INT4 i4FsSecurityWebAuthWebLoadBalInfoLength;

    UINT1 au1Dot11CountryString[3];
    UINT1 au1Pad3[1];


} tWsscfgGlbMib;

typedef struct {
    tWsscfgMibDot11StationConfigEntry MibObject;
} tWsscfgDot11StationConfigEntry;


typedef struct {
    tWsscfgMibDot11AuthenticationAlgorithmsEntry MibObject;
} tWsscfgDot11AuthenticationAlgorithmsEntry;


typedef struct {
    tWsscfgMibDot11WEPDefaultKeysEntry MibObject;
} tWsscfgDot11WEPDefaultKeysEntry;


typedef struct {
    tWsscfgMibDot11WEPKeyMappingsEntry MibObject;
} tWsscfgDot11WEPKeyMappingsEntry;


typedef struct {
    tWsscfgMibDot11PrivacyEntry MibObject;
} tWsscfgDot11PrivacyEntry;


typedef struct {
    tWsscfgMibDot11MultiDomainCapabilityEntry MibObject;
} tWsscfgDot11MultiDomainCapabilityEntry;


typedef struct {
    tWsscfgMibDot11SpectrumManagementEntry MibObject;
} tWsscfgDot11SpectrumManagementEntry;


typedef struct {
    tWsscfgMibDot11RegulatoryClassesEntry MibObject;
} tWsscfgDot11RegulatoryClassesEntry;


typedef struct {
    tWsscfgMibDot11OperationEntry MibObject;
} tWsscfgDot11OperationEntry;


typedef struct {
    tWsscfgMibDot11CountersEntry MibObject;
} tWsscfgDot11CountersEntry;


typedef struct {
    tWsscfgMibDot11GroupAddressesEntry MibObject;
} tWsscfgDot11GroupAddressesEntry;


typedef struct {
    tWsscfgMibDot11EDCAEntry MibObject;
} tWsscfgDot11EDCAEntry;


typedef struct {
    tWsscfgMibDot11QAPEDCAEntry MibObject;
} tWsscfgDot11QAPEDCAEntry;


typedef struct {
    tWsscfgMibDot11QosCountersEntry MibObject;
} tWsscfgDot11QosCountersEntry;


typedef struct {
    tWsscfgMibDot11ResourceInfoEntry MibObject;
} tWsscfgDot11ResourceInfoEntry;


typedef struct {
    tWsscfgMibDot11PhyOperationEntry MibObject;
} tWsscfgDot11PhyOperationEntry;


typedef struct {
    tWsscfgMibDot11PhyAntennaEntry MibObject;
} tWsscfgDot11PhyAntennaEntry;


typedef struct {
    tWsscfgMibDot11PhyTxPowerEntry MibObject;
} tWsscfgDot11PhyTxPowerEntry;


typedef struct {
    tWsscfgMibDot11PhyFHSSEntry MibObject;
} tWsscfgDot11PhyFHSSEntry;


typedef struct {
    tWsscfgMibDot11PhyDSSSEntry MibObject;
} tWsscfgDot11PhyDSSSEntry;


typedef struct {
    tWsscfgMibDot11PhyIREntry MibObject;
} tWsscfgDot11PhyIREntry;


typedef struct {
    tWsscfgMibDot11RegDomainsSupportedEntry MibObject;
} tWsscfgDot11RegDomainsSupportedEntry;


typedef struct {
    tWsscfgMibDot11AntennasListEntry MibObject;
} tWsscfgDot11AntennasListEntry;


typedef struct {
    tWsscfgMibDot11SupportedDataRatesTxEntry MibObject;
} tWsscfgDot11SupportedDataRatesTxEntry;


typedef struct {
    tWsscfgMibDot11SupportedDataRatesRxEntry MibObject;
} tWsscfgDot11SupportedDataRatesRxEntry;


typedef struct {
    tWsscfgMibDot11PhyOFDMEntry MibObject;
} tWsscfgDot11PhyOFDMEntry;


typedef struct {
    tWsscfgMibDot11PhyHRDSSSEntry MibObject;
} tWsscfgDot11PhyHRDSSSEntry;


typedef struct {
    tWsscfgMibDot11HoppingPatternEntry MibObject;
} tWsscfgDot11HoppingPatternEntry;


typedef struct {
    tWsscfgMibDot11PhyERPEntry MibObject;
} tWsscfgDot11PhyERPEntry;


typedef struct {
    tWsscfgMibDot11RSNAConfigEntry MibObject;
} tWsscfgDot11RSNAConfigEntry;


typedef struct {
    tWsscfgMibDot11RSNAConfigPairwiseCiphersEntry MibObject;
} tWsscfgDot11RSNAConfigPairwiseCiphersEntry;


typedef struct {
    tWsscfgMibDot11RSNAConfigAuthenticationSuitesEntry MibObject;
} tWsscfgDot11RSNAConfigAuthenticationSuitesEntry;


typedef struct {
    tWsscfgMibDot11RSNAStatsEntry MibObject;
} tWsscfgDot11RSNAStatsEntry;

typedef struct {
    tWsscfgMibCapwapDot11WlanEntry MibObject;
} tWsscfgCapwapDot11WlanEntry;


typedef struct {
    tWsscfgMibCapwapDot11WlanBindEntry MibObject;
} tWsscfgCapwapDot11WlanBindEntry;

typedef struct {
    tWsscfgMibFsDot11StationConfigEntry MibObject;
} tWsscfgFsDot11StationConfigEntry;

typedef struct {
    tWsscfgMibFsDot11ExternalWebAuthProfileEntry MibObject;
} tWsscfgFsDot11ExternalWebAuthProfileEntry;

typedef struct {
    tWsscfgMibFsDot11CapabilityProfileEntry MibObject;
} tWsscfgFsDot11CapabilityProfileEntry;


typedef struct {
    tWsscfgMibFsDot11AuthenticationProfileEntry MibObject;
} tWsscfgFsDot11AuthenticationProfileEntry;


typedef struct {
    tWsscfgMibFsSecurityWebAuthGuestInfoEntry MibObject;
} tWsscfgFsSecurityWebAuthGuestInfoEntry;


typedef struct {
    tWsscfgMibFsStationQosParamsEntry MibObject;
} tWsscfgFsStationQosParamsEntry;


typedef struct {
    tWsscfgMibFsVlanIsolationEntry MibObject;
} tWsscfgFsVlanIsolationEntry;


typedef struct {
    tWsscfgMibFsDot11RadioConfigEntry MibObject;
} tWsscfgFsDot11RadioConfigEntry;


typedef struct {
    tWsscfgMibFsDot11QosProfileEntry MibObject;
} tWsscfgFsDot11QosProfileEntry;


typedef struct {
    tWsscfgMibFsDot11WlanCapabilityProfileEntry MibObject;
} tWsscfgFsDot11WlanCapabilityProfileEntry;


typedef struct {
    tWsscfgMibFsDot11WlanAuthenticationProfileEntry MibObject;
} tWsscfgFsDot11WlanAuthenticationProfileEntry;


typedef struct {
    tWsscfgMibFsDot11WlanQosProfileEntry MibObject;
} tWsscfgFsDot11WlanQosProfileEntry;


typedef struct
{
 tWsscfgMibFsDot11ClientSummaryEntry       MibObject;
} tWsscfgFsDot11ClientSummaryEntry;


typedef struct {
    tWsscfgMibFsDot11RadioQosEntry MibObject;
} tWsscfgFsDot11RadioQosEntry;


typedef struct {
    tWsscfgMibFsDot11QAPEntry MibObject;
} tWsscfgFsDot11QAPEntry;


typedef struct {
    tWsscfgMibFsQAPProfileEntry MibObject;
} tWsscfgFsQAPProfileEntry;


typedef struct {
    tWsscfgMibFsDot11CapabilityMappingEntry MibObject;
} tWsscfgFsDot11CapabilityMappingEntry;


typedef struct {
    tWsscfgMibFsDot11AuthMappingEntry MibObject;
} tWsscfgFsDot11AuthMappingEntry;


typedef struct {
    tWsscfgMibFsDot11QosMappingEntry MibObject;
} tWsscfgFsDot11QosMappingEntry;


typedef struct {
    tWsscfgMibFsDot11AntennasListEntry MibObject;
} tWsscfgFsDot11AntennasListEntry;

typedef struct {
    tApGroupConfigEntry MibObject;
} tWsscfgWlanApGroupEntry;

typedef struct {
   tApGroupConfigWTPEntry MibObject;
} tWsscfgWlanApGroupWTPEntry;

typedef struct {
   tApGroupConfigWLANEntry MibObject;
} tWsscfgWlanApGroupWLANEntry;

typedef struct {
   tRBTree    ApGroupConfigEntryData;
   tRBTree    ApGroupConfigWTPEntryData;
   tRBTree    ApGroupConfigWLANEntryData;
   UINT4 u4ApGroupEnableStatus;
   UINT4 u4NoOfAPGroups;
} tApGroupGlobals;


typedef struct {
    tWsscfgMibFsDot11WlanEntry MibObject;
} tWsscfgFsDot11WlanEntry;


typedef struct {
    tWsscfgMibFsDot11WlanBindEntry MibObject;
} tWsscfgFsDot11WlanBindEntry;

typedef struct {
    tWsscfgMibFsDot11nConfigEntry MibObject;
} tWsscfgFsDot11nConfigEntry;


typedef struct {
    tWsscfgMibFsDot11nMCSDataRateEntry MibObject;
} tWsscfgFsDot11nMCSDataRateEntry;

typedef struct {
    tWsscfgMibFsWtpImageUpgradeEntry MibObject;
} tWsscfgFsWtpImageUpgradeEntry;


typedef struct {
    tWsscfgMibFsRrmConfigEntry MibObject;
} tWsscfgFsRrmConfigEntry;

typedef struct {
        tWsscfgMibFsDot11ExternalWebAuthProfileEntry MibObject;
} tWsscfgDot11ExternalWebAuthProfileEntry;



/* -------------------------------------------------------
 *
 *                WSSCFG   Data Structures
 *
 * ------------------------------------------------------*/


typedef struct WSSCFG_GLOBALS {
    tTimerListId wsscfgTmrLst;
    UINT4 u4WsscfgTrc;
    tOsixTaskId wsscfgTaskId;
    UINT1 au1TaskSemName[8];
    tOsixSemId wsscfgTaskSemId;
    tOsixQId wsscfgQueId;
    tWsscfgGlbMib WsscfgGlbMib;
    UINT4 u4StationTrapStatus;
    INT4 i4ClientCount;
} tWsscfgGlobals;



#endif                          /* __WSSCFGTDFS_H__ */
/*-----------------------------------------------------------------------*/
