/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: wsscfgdefn.h,v 1.3 2017/11/24 10:37:07 siva Exp $
 * Description: This file contains definitions for wsscfg module.
 *******************************************************************/

#ifndef __WSSCFGDEFN_H__
#define __WSSCFGDEFN_H__

#define WSSCFG_SUCCESS        (OSIX_SUCCESS)
#define WSSCFG_FAILURE        (OSIX_FAILURE)

#define  WSSCFG_TASK_PRIORITY              100
#define  WSSCFG_TASK_NAME                  "IAP1"
#define  WSSCFG_QUEUE_NAME                 (const UINT1 *) "WSSCFGQ"
#define  WSSCFG_MUT_EXCL_SEM_NAME          (const UINT1 *) "WSSCFGM"
#define  WSSCFG_LOCK_SEM_NAME              (const UINT1 *) "WSSLOCKCFG"
#define  WSSCFG_QUEUE_DEPTH                100
#define  WSSCFG_SEM_CREATE_INIT_CNT        1


#define  WSSCFG_ENABLED                    1
#define  WSSCFG_DISABLED                   0

#define  WSSCFG_TIMER_EVENT             0x00000001 
#define  WSSCFG_QUEUE_EVENT             0x00000002
#define  MAX_WSSCFG_DUMMY                   1

#define  WSSCFG_MAX_OPERATOR_LEN   5
#define  WSSCFG_END_OF_STRING  '\0' 
#define  WSSCFG_NULL_STRING    "\0" 
#define  WSSCFG_SHIFT_VALUE                     24
#define  WSSCFG_MASK_VALUE              0xff000000
#define  WSSCFG_REMAINING_SHIFT_VALUE            8
#define  WSSCFG_ASCII_VALUE                     48
#define WSSCFG_ONE                 1
#define WSSCFG_TWO                 2     
#define WSSCFG_THREE               3    
#define WSSCFG_FOUR                4     
#define WSSCFG_FIVE                5     
#define WSSCFG_SIX                 6     
#define WSSCFG_EIGHT               8     
#define WSSCFG_NINE                9     
#define WSSCFG_TEN                 10  
#define WSSCFG_TWELVE              12   
#define WSSCFG_THIRTEEN            13
#define WSSCFG_FOURTEEN            14  
#define WSSCFG_FIFTEEN             15 
#define WSSCFG_SIXTEEN             16 
#define WSSCFG_TWENTY              20
#define WSSCFG_MAX_ROW_NUM         23
#define WSSCFG_HASH_TABLE_LEN      3
#define WSSCFG_IP_ADDR_LEN         2
#define WSSCFG_PORT_NO_LEN         2


#define WSSCFG_INDEX_0              0
#define WSSCFG_INDEX_1              1
#define WSSCFG_INDEX_2              2
#define WSSCFG_INDEX_3              3
#define WSSCFG_INDEX_4              4
#define WSSCFG_INDEX_5              5
#define WSSCFG_INDEX_6              6
#define WSSCFG_INDEX_7              7
#define WSSCFG_INDEX_8              8
#define WSSCFG_INDEX_9              9
#define WSSCFG_INDEX_10             10
#define WSSCFG_INDEX_11             11
#define WSSCFG_INDEX_12             12
#define WSSCFG_INDEX_13             13
#define WSSCFG_INDEX_14             14
#define WSSCFG_INDEX_15             15
#define WSSCFG_INDEX_16             16
#define WSSCFG_INDEX_20             20
#define WSSCFG_DATE_LEN             32

#define WSSCFG_INDEX_256            256
#define WSSCFG_INDEX_257            257

#define WSSCFG_PROTO_ICMP           1
#define WSSCFG_PROTO_IGMP           2
#define WSSCFG_PROTO_GGP            3
#define WSSCFG_PROTO_IP             4
#define WSSCFG_PROTO_TCP            6
#define WSSCFG_PROTO_EGP            8
#define WSSCFG_PROTO_IGP            9
#define WSSCFG_PROTO_NVP            11
#define WSSCFG_PROTO_UDP            17
#define WSSCFG_PROTO_IRTP           28
#define WSSCFG_PROTO_IDPR           35
#define WSSCFG_PROTO_RSVP           46
#define WSSCFG_PROTO_MHRP           48
#define WSSCFG_PROTO_ICMPV6         58
#define WSSCFG_PROTO_IGRP           88
#define WSSCFG_PROTO_OSPF           89
#define WSSCFG_PROTO_ANY            255
#define WSSCFG_MAX_MASK_VAL                 32
#define WSSCFG_MIN_MASK_VAL                 3

#define WSSCFG_ICMP           1   /*  Internet Control Message Protocol       */
#define WSSCFG_IGMP           2   /*  Internet Group Management Protocol      */
#define WSSCFG_GGP            3   /*  Gateway -To- Gateway Protocol           */
#define WSSCFG_IP             4   /*  Internet Protocol                       */
#define WSSCFG_TCP            6   /*  Transmission Control Protocol           */
#define WSSCFG_EGP            8   /*  Exterior Gateway Protocol               */
#define WSSCFG_IGP            9   /*  Interior Gateway Protocol               */
#define WSSCFG_NVP           11   /*  Network Voice Protocol                  */
#define WSSCFG_UDP           17   /*  User Datagram protocol                  */
#define WSSCFG_IRTP          28   /*  Internet Reliable Transaction Protocol  */
#define WSSCFG_IDPR          35   /*  Inter-Domain Policy Routing Protocol    */
#define WSSCFG_RSVP          46   /*  Reservation Protocol                    */
#define WSSCFG_GRE_PPTP      47   /*  PPTP with GRE header                    */
#define WSSCFG_MHRP          48   /*  Mobile Host Routing Protocol            */
#define WSSCFG_ICMPV6        58   /*  Internet Control Message Protocol v6    */
#define WSSCFG_IGRP          88   /*  Cisco specific                          */
#define WSSCFG_OSPFIGP       89   /*  Open Shortest Path First                */
#define WSSCFG_OSPF          89
#define WSSCFG_HTTP          80   /*  HTTP                                    */
#define WSSCFG_PIM          103   /*  PIM                                     */
#define WSSCFG_ANY          255   /*  Any case                                */
#define WSSCFG_DEFAULT_PROTO             255
#define AP_FWL_MAX_NUM_OF_IF        ( CFA_MAX_INTERFACES_IN_SYS + 1 )
#define AP_FWL_DIRECTION_IN            1
#define AP_FWL_DIRECTION_OUT             2
#define WSSCFG_PERMIT                   1
#define WSSCFG_DENY                     2

#define WSSCFG_CHECK_VAL_9                  9
#define WSSCFG_BROADCAST_ADDR        0xffffffff
#define WSSCFG_DEFAULT_ADDR          0x00000000
#define WSSCFG_32ND_BIT_SET          0x80000000
#define WSSCFG_MAX_ADDR_LEN          85

#define WSSCFG_MAX_PORT_LEN          12
#define WSSCFG_MIN_PORT_LEN          1
#define WSSCFG_DIV_FACTOR_100        100 
#define WSSCFG_DIV_FACTOR_10         10 
#define WSSCFG_MAX_PORT_VALUE        65535
#define WSSCFG_MIN_PORT_VALUE        1       
#define WSSCFG_MAX_PORT              65535 
#define WSSCFG_MIN_PORT              1


#define  COUNTER_INC(var, value)  (var) = (COUNTER32) ((var) + (value));
#define  COUNTER_DEC(var, value)  (var) = (UINT2) ((var) - (value));
#define  COUNTER_DECR(var, value)  (var) = (GAUGE) ((var) - (value));
#define  INC_FILTER_REF_COUNT(pFilter)  \
             COUNTER_INC(pFilter->u1FilterRefCount,1);
#define  DEC_FILTER_REF_COUNT(pFilter) \
             COUNTER_DECR(pFilter->u1FilterRefCount,1);
#define  INC_FILTER_HIT_COUNT(pFilter) \
             COUNTER_INC(pFilter->u4FilterHitCount,1);
#define  INC_RULE_REF_COUNT(pRule) \
             COUNTER_INC(pRule->u1RuleRefCount,1);
#define  DEC_RULE_REF_COUNT(pRule) \
             COUNTER_DECR(pRule->u1RuleRefCount,1);

#define WSSCFG_BCAST_ADDR                               255
#define WSSCFG_MCAST_ADDR                               224
#define WSSCFG_ZERO_NETW_ADDR                             0
#define WSSCFG_ZERO_ADDR                                100
#define WSSCFG_LOOPBACK_ADDR                            127
#define WSSCFG_UCAST_ADDR                                 1
#define WSSCFG_INVALID_ADDR                              -1
#define WSSCFG_CLASS_NETADDR                             10  
#define WSSCFG_CLASS_BCASTADDR                          200  
#define WSSCFG_CLASSE_ADDR                              240

#define WSSCFG_SET_ALL_BITS                         0xffffffff
#define WSSCFG_NET_ADDR_LOOP_BACK                    0xff000000
#define WSSCFG_NET_ADDR_MASK                         0xf0000000
#define WSSCFG_CLASS_B_ADDR                          0x80000000
#define WSSCFG_CLASS_C_ADDR                          0xc0000000
#define WSSCFG_CLASS_D_ADDR                          0xe0000000
#define WSSCFG_CLASS_B_MASK                          0xc0000000
#define WSSCFG_CLASS_C_MASK                          0xe0000000
#define WSSCFG_LOOP_BACK_ADDR                        0x7f000000
#define WSSCFG_CLASS_E_ADDR                          0xf0000000
#define WSSCFG_SET_24_BITS                           0x00ffffff
#define WSSCFG_SET_16_BITS                           0x0000ffff
#define WSSCFG_SET_8_BITS                            0x000000ff
#define WSSCFG_FILTER_AND                                1
#define WSSCFG_FILTER_OR                                 2
/* AP Group */
#define CLI_MAX_GROUPNAME_LEN                            32
#define CLI_MAX_APGROUPS                                   50
#define CLI_MAX_GROUPDESC_LEN                            128
#define APGROUP_GREATER                                  1
#define APGROUP_EQUAL                                    0
#define APGROUP_LESSER                                  -1
#define WSS_APGROUP_DOT                                 '.'
#define WSS_APGROUP_SLASH                               '/'
#define WSS_APGROUP_HIPHEN                              '-'
#define WSS_APGROUP_UNDERSCORE                          '_'

#endif  /* __WSSCFGDEFN_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file wsscfgdefn.h                      */
/*-----------------------------------------------------------------------*/

