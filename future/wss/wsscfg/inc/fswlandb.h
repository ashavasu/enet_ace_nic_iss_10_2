/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fswlandb.h,v 1.3.2.1 2018/03/19 14:21:09 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSWLANDB_H
#define _FSWLANDB_H

UINT1 FsDot11SupportedCountryTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsDot11StationConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot11CapabilityProfileTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsDot11AuthenticationProfileTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsSecurityWebAuthGuestInfoTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsStationQosParamsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsVlanIsolationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot11RadioConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot11QosProfileTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsDot11WlanCapabilityProfileTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot11WlanAuthenticationProfileTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot11WlanQosProfileTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot11CapabilityMappingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot11AuthMappingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot11QosMappingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot11ClientSummaryTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsDot11ExternalWebAuthProfileTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot11RadioQosTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot11QAPTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsQAPProfileTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot11AntennasListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot11WlanTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsDot11WlanBindTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsWtpImageUpgradeTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsWlanMulticastTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsDot11nConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot11nMCSDataRateTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDot11nExtConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsWlanStatisticsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsWlanSSIDStatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsWlanClientStatsTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsWlanRadioTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsApWlanStatisticsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsApGroupTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsApGroupWTPTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsApGroupWLANTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsDot11DscpConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsWlanBandSelectTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsWlanBandSelectClientTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};


UINT4 fswlan [] ={1,3,6,1,4,1,29601,2,83};
tSNMP_OID_TYPE fswlanOID = {9, fswlan};


UINT4 FsDot11aNetworkEnable [ ] ={1,3,6,1,4,1,29601,2,83,1,1};
UINT4 FsDot11bNetworkEnable [ ] ={1,3,6,1,4,1,29601,2,83,1,2};
UINT4 FsDot11gSupport [ ] ={1,3,6,1,4,1,29601,2,83,1,3};
UINT4 FsDot11anSupport [ ] ={1,3,6,1,4,1,29601,2,83,1,4};
UINT4 FsDot11bnSupport [ ] ={1,3,6,1,4,1,29601,2,83,1,5};
UINT4 FsDot11ManagmentSSID [ ] ={1,3,6,1,4,1,29601,2,83,1,6};
UINT4 FsDot11CountryString [ ] ={1,3,6,1,4,1,29601,2,83,1,7};
UINT4 FsDot11SupportedCountryIndex [ ] ={1,3,6,1,4,1,29601,2,83,1,8,1,1};
UINT4 FsDot11CountryCode [ ] ={1,3,6,1,4,1,29601,2,83,1,8,1,2};
UINT4 FsDot11CountryName [ ] ={1,3,6,1,4,1,29601,2,83,1,8,1,3};
UINT4 FsDot11NumericIndex [ ] ={1,3,6,1,4,1,29601,2,83,1,8,1,4};
UINT4 FsSecurityWebAuthType [ ] ={1,3,6,1,4,1,29601,2,83,2,1};
UINT4 FsSecurityWebAuthUrl [ ] ={1,3,6,1,4,1,29601,2,83,2,2};
UINT4 FsSecurityWebAuthRedirectUrl [ ] ={1,3,6,1,4,1,29601,2,83,2,3};
UINT4 FsSecurityWebAddr [ ] ={1,3,6,1,4,1,29601,2,83,2,4};
UINT4 FsSecurityWebAuthWebTitle [ ] ={1,3,6,1,4,1,29601,2,83,2,5};
UINT4 FsSecurityWebAuthWebMessage [ ] ={1,3,6,1,4,1,29601,2,83,2,6};
UINT4 FsSecurityWebAuthWebLogoFileName [ ] ={1,3,6,1,4,1,29601,2,83,2,7};
UINT4 FsSecurityWebAuthWebSuccMessage [ ] ={1,3,6,1,4,1,29601,2,83,2,8};
UINT4 FsSecurityWebAuthWebFailMessage [ ] ={1,3,6,1,4,1,29601,2,83,2,9};
UINT4 FsSecurityWebAuthWebButtonText [ ] ={1,3,6,1,4,1,29601,2,83,2,10};
UINT4 FsSecurityWebAuthWebLoadBalInfo [ ] ={1,3,6,1,4,1,29601,2,83,2,11};
UINT4 FsSecurityWebAuthDisplayLang [ ] ={1,3,6,1,4,1,29601,2,83,2,12};
UINT4 FsSecurityWebAuthColor [ ] ={1,3,6,1,4,1,29601,2,83,2,13};
UINT4 FsDot11StationCount [ ] ={1,3,6,1,4,1,29601,2,83,2,14};
UINT4 FsDot11SupressSSID [ ] ={1,3,6,1,4,1,29601,2,83,3,1,1,1};
UINT4 FsDot11VlanId [ ] ={1,3,6,1,4,1,29601,2,83,3,1,1,2};
UINT4 FsDot11WlanWebAuthRedirectFileName [ ] ={1,3,6,1,4,1,29601,2,83,3,1,1,3};
UINT4 FsDot11WlanLoginAuthentication [ ] ={1,3,6,1,4,1,29601,2,83,3,1,1,4};
UINT4 FsDot11BandwidthThresh [ ] ={1,3,6,1,4,1,29601,2,83,3,1,1,5};
UINT4 FsDot11CapabilityProfileName [ ] ={1,3,6,1,4,1,29601,2,83,3,2,1,1};
UINT4 FsDot11CFPollable [ ] ={1,3,6,1,4,1,29601,2,83,3,2,1,2};
UINT4 FsDot11CFPollRequest [ ] ={1,3,6,1,4,1,29601,2,83,3,2,1,3};
UINT4 FsDot11PrivacyOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,2,1,4};
UINT4 FsDot11ShortPreambleOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,2,1,5};
UINT4 FsDot11PBCCOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,2,1,6};
UINT4 FsDot11ChannelAgilityPresent [ ] ={1,3,6,1,4,1,29601,2,83,3,2,1,7};
UINT4 FsDot11QosOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,2,1,8};
UINT4 FsDot11SpectrumManagementRequired [ ] ={1,3,6,1,4,1,29601,2,83,3,2,1,9};
UINT4 FsDot11ShortSlotTimeOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,2,1,10};
UINT4 FsDot11APSDOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,2,1,11};
UINT4 FsDot11DSSSOFDMOptionEnabled [ ] ={1,3,6,1,4,1,29601,2,83,3,2,1,12};
UINT4 FsDot11DelayedBlockAckOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,2,1,13};
UINT4 FsDot11ImmediateBlockAckOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,2,1,14};
UINT4 FsDot11QAckOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,2,1,15};
UINT4 FsDot11QueueRequestOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,2,1,16};
UINT4 FsDot11TXOPRequestOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,2,1,17};
UINT4 FsDot11RSNAOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,2,1,18};
UINT4 FsDot11RSNAPreauthenticationImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,2,1,19};
UINT4 FsDot11CapabilityRowStatus [ ] ={1,3,6,1,4,1,29601,2,83,3,2,1,20};
UINT4 FsDot11AuthenticationProfileName [ ] ={1,3,6,1,4,1,29601,2,83,3,3,1,1};
UINT4 FsDot11AuthenticationAlgorithm [ ] ={1,3,6,1,4,1,29601,2,83,3,3,1,2};
UINT4 FsDot11WepKeyIndex [ ] ={1,3,6,1,4,1,29601,2,83,3,3,1,3};
UINT4 FsDot11WepKeyType [ ] ={1,3,6,1,4,1,29601,2,83,3,3,1,4};
UINT4 FsDot11WepKeyLength [ ] ={1,3,6,1,4,1,29601,2,83,3,3,1,5};
UINT4 FsDot11WepKey [ ] ={1,3,6,1,4,1,29601,2,83,3,3,1,6};
UINT4 FsDot11WebAuthentication [ ] ={1,3,6,1,4,1,29601,2,83,3,3,1,7};
UINT4 FsDot11AuthenticationRowStatus [ ] ={1,3,6,1,4,1,29601,2,83,3,3,1,8};
UINT4 FsSecurityWebAuthUName [ ] ={1,3,6,1,4,1,29601,2,83,3,4,1,1};
UINT4 FsSecurityWlanProfileId [ ] ={1,3,6,1,4,1,29601,2,83,3,4,1,2};
UINT4 FsSecurityWebAuthUserLifetime [ ] ={1,3,6,1,4,1,29601,2,83,3,4,1,3};
UINT4 FsSecurityWebAuthUserEmailId [ ] ={1,3,6,1,4,1,29601,2,83,3,4,1,4};
UINT4 FsSecurityWebAuthGuestInfoRowStatus [ ] ={1,3,6,1,4,1,29601,2,83,3,4,1,5};
UINT4 FsSecurityWebAuthUPassword [ ] ={1,3,6,1,4,1,29601,2,83,3,4,1,6};
UINT4 FsStaMacAddress [ ] ={1,3,6,1,4,1,29601,2,83,3,5,1,1};
UINT4 FsStaQoSPriority [ ] ={1,3,6,1,4,1,29601,2,83,3,5,1,2};
UINT4 FsStaQoSDscp [ ] ={1,3,6,1,4,1,29601,2,83,3,5,1,3};
UINT4 FsVlanIsolation [ ] ={1,3,6,1,4,1,29601,2,83,3,6,1,1};
UINT4 FsDot11RadioType [ ] ={1,3,6,1,4,1,29601,2,83,3,7,1,1};
UINT4 FsDot11RadioNoOfBssIdSupported [ ] ={1,3,6,1,4,1,29601,2,83,3,7,1,2};
UINT4 FsDot11RadioAntennaType [ ] ={1,3,6,1,4,1,29601,2,83,3,7,1,3};
UINT4 FsDot11RadioFailureStatus [ ] ={1,3,6,1,4,1,29601,2,83,3,7,1,4};
UINT4 FsDot11RowStatus [ ] ={1,3,6,1,4,1,29601,2,83,3,7,1,15};
UINT4 FsDot11QosProfileName [ ] ={1,3,6,1,4,1,29601,2,83,3,8,1,1};
UINT4 FsDot11QosTraffic [ ] ={1,3,6,1,4,1,29601,2,83,3,8,1,2};
UINT4 FsDot11QosPassengerTrustMode [ ] ={1,3,6,1,4,1,29601,2,83,3,8,1,3};
UINT4 FsDot11QosRateLimit [ ] ={1,3,6,1,4,1,29601,2,83,3,8,1,4};
UINT4 FsDot11UpStreamCIR [ ] ={1,3,6,1,4,1,29601,2,83,3,8,1,5};
UINT4 FsDot11UpStreamCBS [ ] ={1,3,6,1,4,1,29601,2,83,3,8,1,6};
UINT4 FsDot11UpStreamEIR [ ] ={1,3,6,1,4,1,29601,2,83,3,8,1,7};
UINT4 FsDot11UpStreamEBS [ ] ={1,3,6,1,4,1,29601,2,83,3,8,1,8};
UINT4 FsDot11DownStreamCIR [ ] ={1,3,6,1,4,1,29601,2,83,3,8,1,9};
UINT4 FsDot11DownStreamCBS [ ] ={1,3,6,1,4,1,29601,2,83,3,8,1,10};
UINT4 FsDot11DownStreamEIR [ ] ={1,3,6,1,4,1,29601,2,83,3,8,1,11};
UINT4 FsDot11DownStreamEBS [ ] ={1,3,6,1,4,1,29601,2,83,3,8,1,12};
UINT4 FsDot11QosRowStatus [ ] ={1,3,6,1,4,1,29601,2,83,3,8,1,13};
UINT4 FsDot11WlanCFPollable [ ] ={1,3,6,1,4,1,29601,2,83,3,9,1,1};
UINT4 FsDot11WlanCFPollRequest [ ] ={1,3,6,1,4,1,29601,2,83,3,9,1,2};
UINT4 FsDot11WlanPrivacyOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,9,1,3};
UINT4 FsDot11WlanShortPreambleOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,9,1,4};
UINT4 FsDot11WlanPBCCOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,9,1,5};
UINT4 FsDot11WlanChannelAgilityPresent [ ] ={1,3,6,1,4,1,29601,2,83,3,9,1,6};
UINT4 FsDot11WlanQosOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,9,1,7};
UINT4 FsDot11WlanSpectrumManagementRequired [ ] ={1,3,6,1,4,1,29601,2,83,3,9,1,8};
UINT4 FsDot11WlanShortSlotTimeOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,9,1,9};
UINT4 FsDot11WlanAPSDOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,9,1,10};
UINT4 FsDot11WlanDSSSOFDMOptionEnabled [ ] ={1,3,6,1,4,1,29601,2,83,3,9,1,11};
UINT4 FsDot11WlanDelayedBlockAckOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,9,1,12};
UINT4 FsDot11WlanImmediateBlockAckOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,9,1,13};
UINT4 FsDot11WlanQAckOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,9,1,14};
UINT4 FsDot11WlanQueueRequestOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,9,1,15};
UINT4 FsDot11WlanTXOPRequestOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,9,1,16};
UINT4 FsDot11WlanRSNAOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,9,1,17};
UINT4 FsDot11WlanRSNAPreauthenticationImplemented [ ] ={1,3,6,1,4,1,29601,2,83,3,9,1,18};
UINT4 FsDot11WlanCapabilityRowStatus [ ] ={1,3,6,1,4,1,29601,2,83,3,9,1,19};
UINT4 FsDot11WlanAuthenticationAlgorithm [ ] ={1,3,6,1,4,1,29601,2,83,3,10,1,1};
UINT4 FsDot11WlanWepKeyIndex [ ] ={1,3,6,1,4,1,29601,2,83,3,10,1,2};
UINT4 FsDot11WlanWepKeyType [ ] ={1,3,6,1,4,1,29601,2,83,3,10,1,3};
UINT4 FsDot11WlanWepKeyLength [ ] ={1,3,6,1,4,1,29601,2,83,3,10,1,4};
UINT4 FsDot11WlanWepKey [ ] ={1,3,6,1,4,1,29601,2,83,3,10,1,5};
UINT4 FsDot11WlanWebAuthentication [ ] ={1,3,6,1,4,1,29601,2,83,3,10,1,6};
UINT4 FsDot11WlanAuthenticationRowStatus [ ] ={1,3,6,1,4,1,29601,2,83,3,10,1,7};
UINT4 FsDot11WlanQosTraffic [ ] ={1,3,6,1,4,1,29601,2,83,3,11,1,1};
UINT4 FsDot11WlanQosPassengerTrustMode [ ] ={1,3,6,1,4,1,29601,2,83,3,11,1,2};
UINT4 FsDot11WlanQosRateLimit [ ] ={1,3,6,1,4,1,29601,2,83,3,11,1,3};
UINT4 FsDot11WlanUpStreamCIR [ ] ={1,3,6,1,4,1,29601,2,83,3,11,1,4};
UINT4 FsDot11WlanUpStreamCBS [ ] ={1,3,6,1,4,1,29601,2,83,3,11,1,5};
UINT4 FsDot11WlanUpStreamEIR [ ] ={1,3,6,1,4,1,29601,2,83,3,11,1,6};
UINT4 FsDot11WlanUpStreamEBS [ ] ={1,3,6,1,4,1,29601,2,83,3,11,1,7};
UINT4 FsDot11WlanDownStreamCIR [ ] ={1,3,6,1,4,1,29601,2,83,3,11,1,8};
UINT4 FsDot11WlanDownStreamCBS [ ] ={1,3,6,1,4,1,29601,2,83,3,11,1,9};
UINT4 FsDot11WlanDownStreamEIR [ ] ={1,3,6,1,4,1,29601,2,83,3,11,1,10};
UINT4 FsDot11WlanDownStreamEBS [ ] ={1,3,6,1,4,1,29601,2,83,3,11,1,11};
UINT4 FsDot11WlanQosRowStatus [ ] ={1,3,6,1,4,1,29601,2,83,3,11,1,12};
UINT4 FsDot11CapabilityMappingProfileName [ ] ={1,3,6,1,4,1,29601,2,83,3,12,1,1};
UINT4 FsDot11CapabilityMappingRowStatus [ ] ={1,3,6,1,4,1,29601,2,83,3,12,1,2};
UINT4 FsDot11AuthMappingProfileName [ ] ={1,3,6,1,4,1,29601,2,83,3,13,1,1};
UINT4 FsDot11AuthMappingRowStatus [ ] ={1,3,6,1,4,1,29601,2,83,3,13,1,2};
UINT4 FsDot11QosMappingProfileName [ ] ={1,3,6,1,4,1,29601,2,83,3,14,1,1};
UINT4 FsDot11QosMappingRowStatus [ ] ={1,3,6,1,4,1,29601,2,83,3,14,1,2};
UINT4 FsDot11ClientMacAddress [ ] ={1,3,6,1,4,1,29601,2,83,3,15,1,1};
UINT4 FsDot11WlanProfileId [ ] ={1,3,6,1,4,1,29601,2,83,3,15,1,2};
UINT4 FsDot11WtpProfileName [ ] ={1,3,6,1,4,1,29601,2,83,3,15,1,3};
UINT4 FsDot11WtpRadioId [ ] ={1,3,6,1,4,1,29601,2,83,3,15,1,4};
UINT4 FsDot11AuthStatus [ ] ={1,3,6,1,4,1,29601,2,83,3,15,1,5};
UINT4 FsDot11AssocStatus [ ] ={1,3,6,1,4,1,29601,2,83,3,15,1,6};
UINT4 FsDot11ExternalWebAuthMethod [ ] ={1,3,6,1,4,1,29601,2,83,3,16,1,1};
UINT4 FsDot11ExternalWebAuthUrl [ ] ={1,3,6,1,4,1,29601,2,83,3,16,1,2};
UINT4 FsDot11TaggingPolicy [ ] ={1,3,6,1,4,1,29601,2,83,4,1,1,1};
UINT4 FsDot11QueueDepth [ ] ={1,3,6,1,4,1,29601,2,83,4,2,1,2};
UINT4 FsDot11PriorityValue [ ] ={1,3,6,1,4,1,29601,2,83,4,2,1,3};
UINT4 FsDot11DscpValue [ ] ={1,3,6,1,4,1,29601,2,83,4,2,1,4};
UINT4 FsQAPProfileName [ ] ={1,3,6,1,4,1,29601,2,83,4,3,1,1};
UINT4 FsQAPProfileIndex [ ] ={1,3,6,1,4,1,29601,2,83,4,3,1,2};
UINT4 FsQAPProfileCWmin [ ] ={1,3,6,1,4,1,29601,2,83,4,3,1,3};
UINT4 FsQAPProfileCWmax [ ] ={1,3,6,1,4,1,29601,2,83,4,3,1,4};
UINT4 FsQAPProfileAIFSN [ ] ={1,3,6,1,4,1,29601,2,83,4,3,1,5};
UINT4 FsQAPProfileTXOPLimit [ ] ={1,3,6,1,4,1,29601,2,83,4,3,1,6};
UINT4 FsQAPProfileQueueDepth [ ] ={1,3,6,1,4,1,29601,2,83,4,3,1,7};
UINT4 FsQAPProfilePriorityValue [ ] ={1,3,6,1,4,1,29601,2,83,4,3,1,8};
UINT4 FsQAPProfileDscpValue [ ] ={1,3,6,1,4,1,29601,2,83,4,3,1,9};
UINT4 FsQAPProfileRowStatus [ ] ={1,3,6,1,4,1,29601,2,83,4,3,1,10};
UINT4 FsAntennaMode [ ] ={1,3,6,1,4,1,29601,2,83,5,1,1,1};
UINT4 FsAntennaSelection [ ] ={1,3,6,1,4,1,29601,2,83,5,1,1,2};
UINT4 FsDot11WlanProfileIfIndex [ ] ={1,3,6,1,4,1,29601,2,83,5,2,1,1};
UINT4 FsDot11WlanRowStatus [ ] ={1,3,6,1,4,1,29601,2,83,5,2,1,2};
UINT4 FsDot11WlanBindWlanId [ ] ={1,3,6,1,4,1,29601,2,83,5,3,1,1};
UINT4 FsDot11WlanBindBssIfIndex [ ] ={1,3,6,1,4,1,29601,2,83,5,3,1,2};
UINT4 FsDot11WlanBindRowStatus [ ] ={1,3,6,1,4,1,29601,2,83,5,3,1,3};
UINT4 FsWtpImageVersion [ ] ={1,3,6,1,4,1,29601,2,83,6,1,1,1};
UINT4 FsWtpUpgradeDev [ ] ={1,3,6,1,4,1,29601,2,83,6,1,1,2};
UINT4 FsWtpName [ ] ={1,3,6,1,4,1,29601,2,83,6,1,1,3};
UINT4 FsWtpImageName [ ] ={1,3,6,1,4,1,29601,2,83,6,1,1,4};
UINT4 FsWtpAddressType [ ] ={1,3,6,1,4,1,29601,2,83,6,1,1,5};
UINT4 FsWtpServerIP [ ] ={1,3,6,1,4,1,29601,2,83,6,1,1,6};
UINT4 FsWtpRowStatus [ ] ={1,3,6,1,4,1,29601,2,83,6,1,1,7};
UINT4 FsWlanMulticastMode [ ] ={1,3,6,1,4,1,29601,2,83,6,2,1,1};
UINT4 FsWlanMulticastSnoopTableLength [ ] ={1,3,6,1,4,1,29601,2,83,6,2,1,2};
UINT4 FsWlanMulticastSnoopTimer [ ] ={1,3,6,1,4,1,29601,2,83,6,2,1,3};
UINT4 FsWlanMulticastSnoopTimeout [ ] ={1,3,6,1,4,1,29601,2,83,6,2,1,4};
UINT4 FsDot11nConfigShortGIfor20MHz [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,1};
UINT4 FsDot11nConfigShortGIfor40MHz [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,2};
UINT4 FsDot11nConfigChannelWidth [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,3};
UINT4 FsDot11nConfigMIMOPowerSave [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,4};
UINT4 FsDot11nConfigDelayedBlockAckOptionActivated [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,5};
UINT4 FsDot11nConfigMaxAMSDULengthConfig [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,6};
UINT4 FsDot11nConfigMaxRxAMPDUFactorConfig [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,7};
UINT4 FsDot11nConfigMinimumMPDUStartSpacingConfig [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,8};
UINT4 FsDot11nConfigPCOOptionActivated [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,9};
UINT4 FsDot11nConfigTransitionTimeConfig [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,10};
UINT4 FsDot11nConfigMCSFeedbackOptionActivated [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,11};
UINT4 FsDot11nConfigHTControlFieldSupported [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,12};
UINT4 FsDot11nConfigRDResponderOptionActivated [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,13};
UINT4 FsDot11nConfigtHTDSSCCKModein40MHz [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,14};
UINT4 FsDot11nConfigtHTDSSCCKModein40MHzConfig [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,15};
UINT4 FsDot11nConfigTxRxMCSSetNotEqual [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,16};
UINT4 FsDot11nConfigTxMaximumNumberSpatialStreamsSupported [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,17};
UINT4 FsDot11nConfigTxUnequalModulationSupported [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,18};
UINT4 FsDot11nConfigImplicitTransmitBeamformingRecvOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,19};
UINT4 FsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,20};
UINT4 FsDot11nConfigReceiveStaggerSoundingOptionActivated [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,21};
UINT4 FsDot11nConfigTransmitStaggerSoundingOptionActivated [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,22};
UINT4 FsDot11nConfigReceiveNDPOptionActivated [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,23};
UINT4 FsDot11nConfigTransmitNDPOptionActivated [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,24};
UINT4 FsDot11nConfigImplicitTransmitBeamformingOptionActivated [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,25};
UINT4 FsDot11nConfigCalibrationOptionActivated [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,26};
UINT4 FsDot11nConfigExplicitCSITransmitBeamformingOptionActivated [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,27};
UINT4 FsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,28};
UINT4 FsDot11nConfigExplicitCompressedBeamformingMatrixOptImplemented [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,29};
UINT4 FsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,30};
UINT4 FsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,31};
UINT4 FsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,32};
UINT4 FsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,33};
UINT4 FsDot11nConfigMinimalGroupingImplemented [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,34};
UINT4 FsDot11nConfigMinimalGroupingActivated [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,35};
UINT4 FsDot11nConfigNumberBeamFormingCSISupportAntenna [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,36};
UINT4 FsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,37};
UINT4 FsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,38};
UINT4 FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaImplemented [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,39};
UINT4 FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,40};
UINT4 FsDot11nConfigChannelEstimationCapabilityImplemented [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,41};
UINT4 FsDot11nConfigChannelEstimationCapabilityActivated [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,42};
UINT4 FsDot11nConfigCurrentPrimaryChannel [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,43};
UINT4 FsDot11nConfigCurrentSecondaryChannel [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,44};
UINT4 FsDot11nConfigSTAChannelWidth [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,45};
UINT4 FsDot11nConfigSTAChannelWidthConfig [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,46};
UINT4 FsDot11nConfigNonGreenfieldHTSTAsPresent [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,47};
UINT4 FsDot11nConfigNonGreenfieldHTSTAsPresentConfig [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,48};
UINT4 FsDot11nConfigOBSSNonHTSTAsPresent [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,49};
UINT4 FsDot11nConfigOBSSNonHTSTAsPresentConfig [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,50};
UINT4 FsDot11nConfigDualBeacon [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,51};
UINT4 FsDot11nConfigDualBeaconConfig [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,52};
UINT4 FsDot11nConfigSTBCBeacon [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,53};
UINT4 FsDot11nConfigSTBCBeaconConfig [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,54};
UINT4 FsDot11nConfigPCOPhase [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,55};
UINT4 FsDot11nConfigPCOPhaseConfig [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,56};
UINT4 FsDot11nConfigAllowChannelWidth [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,57};
UINT4 FsDot11nConfigRxSTBCOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,58};
UINT4 FsDot11nConfigRxSTBCOptionActivated [ ] ={1,3,6,1,4,1,29601,2,83,5,4,1,59};
UINT4 FsDot11nMCSDataRateIndex [ ] ={1,3,6,1,4,1,29601,2,83,5,5,1,1};
UINT4 FsDot11nMCSDataRate [ ] ={1,3,6,1,4,1,29601,2,83,5,5,1,2};
UINT4 FsDot11nAMPDUStatus [ ] ={1,3,6,1,4,1,29601,2,83,5,6,1,1};
UINT4 FsDot11nAMPDUSubFrame [ ] ={1,3,6,1,4,1,29601,2,83,5,6,1,2};
UINT4 FsDot11nAMPDULimit [ ] ={1,3,6,1,4,1,29601,2,83,5,6,1,3};
UINT4 FsDot11nAMSDUStatus [ ] ={1,3,6,1,4,1,29601,2,83,5,6,1,4};
UINT4 FsDot11nAMSDULimit [ ] ={1,3,6,1,4,1,29601,2,83,5,6,1,5};
UINT4 FsWlanBeaconsSentCount [ ] ={1,3,6,1,4,1,29601,2,83,7,1,1,1};
UINT4 FsWlanProbeReqRcvdCount [ ] ={1,3,6,1,4,1,29601,2,83,7,1,1,2};
UINT4 FsWlanProbeRespSentCount [ ] ={1,3,6,1,4,1,29601,2,83,7,1,1,3};
UINT4 FsWlanDataPktRcvdCount [ ] ={1,3,6,1,4,1,29601,2,83,7,1,1,4};
UINT4 FsWlanDataPktSentCount [ ] ={1,3,6,1,4,1,29601,2,83,7,1,1,5};
UINT4 FsWlanSSIDStatsAssocClientCount [ ] ={1,3,6,1,4,1,29601,2,83,7,2,1,1};
UINT4 FsWlanSSIDStatsMaxClientCount [ ] ={1,3,6,1,4,1,29601,2,83,7,2,1,2};
UINT4 FsWlanClientStatsMACAddress [ ] ={1,3,6,1,4,1,29601,2,83,7,3,1,1};
UINT4 FsWlanClientStatsIdleTimeout [ ] ={1,3,6,1,4,1,29601,2,83,7,3,1,2};
UINT4 FsWlanClientStatsBytesReceivedCount [ ] ={1,3,6,1,4,1,29601,2,83,7,3,1,3};
UINT4 FsWlanClientStatsBytesSentCount [ ] ={1,3,6,1,4,1,29601,2,83,7,3,1,4};
UINT4 FsWlanClientStatsAuthState [ ] ={1,3,6,1,4,1,29601,2,83,7,3,1,5};
UINT4 FsWlanClientStatsRadiusUserName [ ] ={1,3,6,1,4,1,29601,2,83,7,3,1,6};
UINT4 FsWlanClientStatsIpAddressType [ ] ={1,3,6,1,4,1,29601,2,83,7,3,1,7};
UINT4 FsWlanClientStatsIpAddress [ ] ={1,3,6,1,4,1,29601,2,83,7,3,1,8};
UINT4 FsWlanClientStatsAssocTime [ ] ={1,3,6,1,4,1,29601,2,83,7,3,1,9};
UINT4 FsWlanClientStatsSessionTimeLeft [ ] ={1,3,6,1,4,1,29601,2,83,7,3,1,10};
UINT4 FsWlanClientStatsConnTime [ ] ={1,3,6,1,4,1,29601,2,83,7,3,1,11};
UINT4 FsWlanClientStatsSSID [ ] ={1,3,6,1,4,1,29601,2,83,7,3,1,12};
UINT4 FsWlanClientStatsToACStatus [ ] ={1,3,6,1,4,1,29601,2,83,7,3,1,13};
UINT4 FsWlanClientStatsClear [ ] ={1,3,6,1,4,1,29601,2,83,7,3,1,14};
UINT4 FsWlanRadioRadiobaseMACAddress [ ] ={1,3,6,1,4,1,29601,2,83,7,4,1,1};
UINT4 FsWlanRadioIfInOctets [ ] ={1,3,6,1,4,1,29601,2,83,7,4,1,2};
UINT4 FsWlanRadioIfOutOctets [ ] ={1,3,6,1,4,1,29601,2,83,7,4,1,3};
UINT4 FsWlanRadioRadioBand [ ] ={1,3,6,1,4,1,29601,2,83,7,4,1,4};
UINT4 FsWlanRadioMaxClientCount [ ] ={1,3,6,1,4,1,29601,2,83,7,4,1,5};
UINT4 FsWlanRadioClearStats [ ] ={1,3,6,1,4,1,29601,2,83,7,4,1,6};
UINT4 FsApWlanBeaconsSentCount [ ] ={1,3,6,1,4,1,29601,2,83,7,5,1,1};
UINT4 FsApWlanProbeReqRcvdCount [ ] ={1,3,6,1,4,1,29601,2,83,7,5,1,2};
UINT4 FsApWlanProbeRespSentCount [ ] ={1,3,6,1,4,1,29601,2,83,7,5,1,3};
UINT4 FsApWlanDataPktRcvdCount [ ] ={1,3,6,1,4,1,29601,2,83,7,5,1,4};
UINT4 FsApWlanDataPktSentCount [ ] ={1,3,6,1,4,1,29601,2,83,7,5,1,5};
UINT4 FsApGroupEnabledStatus [ ] ={1,3,6,1,4,1,29601,2,83,3,17};
UINT4 FsApGroupIndex [ ] ={1,3,6,1,4,1,29601,2,83,3,18,1,1};
UINT4 FsApGroupName [ ] ={1,3,6,1,4,1,29601,2,83,3,18,1,2};
UINT4 FsApGroupNameDescription [ ] ={1,3,6,1,4,1,29601,2,83,3,18,1,3};
UINT4 FsApGroupRadioPolicy [ ] ={1,3,6,1,4,1,29601,2,83,3,18,1,4};
UINT4 FsApGroupInterfaceVlan [ ] ={1,3,6,1,4,1,29601,2,83,3,18,1,5};
UINT4 FsApGroupRowStatus [ ] ={1,3,6,1,4,1,29601,2,83,3,18,1,6};
UINT4 FsApGroupWTPIndex [ ] ={1,3,6,1,4,1,29601,2,83,3,19,1,1};
UINT4 FsApGroupWtpProfileId [ ] ={1,3,6,1,4,1,29601,2,83,3,19,1,2};
UINT4 FsApGroupWTPRowStatus [ ] ={1,3,6,1,4,1,29601,2,83,3,19,1,3};
UINT4 FsApGroupWLANIndex [ ] ={1,3,6,1,4,1,29601,2,83,3,20,1,1};
UINT4 FsApGroupWlanProfileId [ ] ={1,3,6,1,4,1,29601,2,83,3,20,1,2};
UINT4 FsApGroupWLANRowStatus [ ] ={1,3,6,1,4,1,29601,2,83,3,20,1,3};
UINT4 Fsdot1xAuthFailureStatus [ ] ={1,3,6,1,4,1,29601,2,83,8,1,1};
UINT4 FsDot11DisconnectionReason [ ] ={1,3,6,1,4,1,29601,2,83,8,1,2};
UINT4 FsDot11ClientMac [ ] ={1,3,6,1,4,1,29601,2,83,8,1,3};
UINT4 FsWlanStationTrapStatus [ ] ={1,3,6,1,4,1,29601,2,83,10,1};
UINT4 FsDot11DscpInPriority [ ] ={1,3,6,1,4,1,29601,2,83,11,1,1,1};
UINT4 FsDot11OutDscp [ ] ={1,3,6,1,4,1,29601,2,83,11,1,1,2};
UINT4 FsDot11DscpRowStatus [ ] ={1,3,6,1,4,1,29601,2,83,11,1,1,3};
#ifdef BAND_SELECT_WANTED
UINT4 FsBandSelectStatus [ ] ={1,3,6,1,4,1,29601,2,83,12,1};
UINT4 FsBandSelectPerWlanStatus [ ] ={1,3,6,1,4,1,29601,2,83,12,2,1,1};
UINT4 FsBandSelectAssocCount [ ] ={1,3,6,1,4,1,29601,2,83,12,2,1,2};
UINT4 FsBandSelectAgeOutSuppTimer [ ] ={1,3,6,1,4,1,29601,2,83,12,2,1,3};
UINT4 FsBandSelectAssocCountFlushTimer [ ] ={1,3,6,1,4,1,29601,2,83,12,2,1,4};
UINT4 FsWlanBandSelectClientMacAddress [ ] ={1,3,6,1,4,1,29601,2,83,12,3,1,1};
UINT4 FsWlanBandSelectEnabledBSSID [ ] ={1,3,6,1,4,1,29601,2,83,12,3,1,2};
UINT4 FsWlanBandSelectStationActiveStatus [ ] ={1,3,6,1,4,1,29601,2,83,12,3,1,3};
#endif
UINT4 FsStationDebugOption [ ] ={1,3,6,1,4,1,29601,2,83,13,1};
UINT4 FsWlanDebugOption [ ] ={1,3,6,1,4,1,29601,2,83,13,2};
UINT4 FsRadioDebugOption [ ] ={1,3,6,1,4,1,29601,2,83,13,3};
UINT4 FsPmDebugOption [ ] ={1,3,6,1,4,1,29601,2,83,13,4};
UINT4 FsWssIfDebugOption [ ] ={1,3,6,1,4,1,29601,2,83,13,5};
UINT4 FsBandSelectLegacyRate [ ] ={1,3,6,1,4,1,29601,2,83,14,1};




tMbDbEntry fswlanMibEntry[]= {

{{11,FsDot11aNetworkEnable}, NULL, FsDot11aNetworkEnableGet, FsDot11aNetworkEnableSet, FsDot11aNetworkEnableTest, FsDot11aNetworkEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsDot11bNetworkEnable}, NULL, FsDot11bNetworkEnableGet, FsDot11bNetworkEnableSet, FsDot11bNetworkEnableTest, FsDot11bNetworkEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsDot11gSupport}, NULL, FsDot11gSupportGet, FsDot11gSupportSet, FsDot11gSupportTest, FsDot11gSupportDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsDot11anSupport}, NULL, FsDot11anSupportGet, FsDot11anSupportSet, FsDot11anSupportTest, FsDot11anSupportDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsDot11bnSupport}, NULL, FsDot11bnSupportGet, FsDot11bnSupportSet, FsDot11bnSupportTest, FsDot11bnSupportDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsDot11ManagmentSSID}, NULL, FsDot11ManagmentSSIDGet, FsDot11ManagmentSSIDSet, FsDot11ManagmentSSIDTest, FsDot11ManagmentSSIDDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsDot11CountryString}, NULL, FsDot11CountryStringGet, FsDot11CountryStringSet, FsDot11CountryStringTest, FsDot11CountryStringDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{13,FsDot11SupportedCountryIndex}, GetNextIndexFsDot11SupportedCountryTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsDot11SupportedCountryTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11CountryCode}, GetNextIndexFsDot11SupportedCountryTable, FsDot11CountryCodeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsDot11SupportedCountryTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11CountryName}, GetNextIndexFsDot11SupportedCountryTable, FsDot11CountryNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsDot11SupportedCountryTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11NumericIndex}, GetNextIndexFsDot11SupportedCountryTable, FsDot11NumericIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsDot11SupportedCountryTableINDEX, 1, 0, 0, NULL},

{{11,FsSecurityWebAuthType}, NULL, FsSecurityWebAuthTypeGet, FsSecurityWebAuthTypeSet, FsSecurityWebAuthTypeTest, FsSecurityWebAuthTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsSecurityWebAuthUrl}, NULL, FsSecurityWebAuthUrlGet, FsSecurityWebAuthUrlSet, FsSecurityWebAuthUrlTest, FsSecurityWebAuthUrlDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsSecurityWebAuthRedirectUrl}, NULL, FsSecurityWebAuthRedirectUrlGet, FsSecurityWebAuthRedirectUrlSet, FsSecurityWebAuthRedirectUrlTest, FsSecurityWebAuthRedirectUrlDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsSecurityWebAddr}, NULL, FsSecurityWebAddrGet, FsSecurityWebAddrSet, FsSecurityWebAddrTest, FsSecurityWebAddrDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsSecurityWebAuthWebTitle}, NULL, FsSecurityWebAuthWebTitleGet, FsSecurityWebAuthWebTitleSet, FsSecurityWebAuthWebTitleTest, FsSecurityWebAuthWebTitleDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsSecurityWebAuthWebMessage}, NULL, FsSecurityWebAuthWebMessageGet, FsSecurityWebAuthWebMessageSet, FsSecurityWebAuthWebMessageTest, FsSecurityWebAuthWebMessageDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsSecurityWebAuthWebLogoFileName}, NULL, FsSecurityWebAuthWebLogoFileNameGet, FsSecurityWebAuthWebLogoFileNameSet, FsSecurityWebAuthWebLogoFileNameTest, FsSecurityWebAuthWebLogoFileNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsSecurityWebAuthWebSuccMessage}, NULL, FsSecurityWebAuthWebSuccMessageGet, FsSecurityWebAuthWebSuccMessageSet, FsSecurityWebAuthWebSuccMessageTest, FsSecurityWebAuthWebSuccMessageDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsSecurityWebAuthWebFailMessage}, NULL, FsSecurityWebAuthWebFailMessageGet, FsSecurityWebAuthWebFailMessageSet, FsSecurityWebAuthWebFailMessageTest, FsSecurityWebAuthWebFailMessageDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsSecurityWebAuthWebButtonText}, NULL, FsSecurityWebAuthWebButtonTextGet, FsSecurityWebAuthWebButtonTextSet, FsSecurityWebAuthWebButtonTextTest, FsSecurityWebAuthWebButtonTextDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsSecurityWebAuthWebLoadBalInfo}, NULL, FsSecurityWebAuthWebLoadBalInfoGet, FsSecurityWebAuthWebLoadBalInfoSet, FsSecurityWebAuthWebLoadBalInfoTest, FsSecurityWebAuthWebLoadBalInfoDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsSecurityWebAuthDisplayLang}, NULL, FsSecurityWebAuthDisplayLangGet, FsSecurityWebAuthDisplayLangSet, FsSecurityWebAuthDisplayLangTest, FsSecurityWebAuthDisplayLangDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsSecurityWebAuthColor}, NULL, FsSecurityWebAuthColorGet, FsSecurityWebAuthColorSet, FsSecurityWebAuthColorTest, FsSecurityWebAuthColorDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsDot11StationCount}, NULL, FsDot11StationCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, "0"},

{{13,FsDot11SupressSSID}, GetNextIndexFsDot11StationConfigTable, FsDot11SupressSSIDGet, FsDot11SupressSSIDSet, FsDot11SupressSSIDTest, FsDot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11VlanId}, GetNextIndexFsDot11StationConfigTable, FsDot11VlanIdGet, FsDot11VlanIdSet, FsDot11VlanIdTest, FsDot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11StationConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11WlanWebAuthRedirectFileName}, GetNextIndexFsDot11StationConfigTable, FsDot11WlanWebAuthRedirectFileNameGet, FsDot11WlanWebAuthRedirectFileNameSet, FsDot11WlanWebAuthRedirectFileNameTest, FsDot11StationConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11WlanLoginAuthentication}, GetNextIndexFsDot11StationConfigTable, FsDot11WlanLoginAuthenticationGet, FsDot11WlanLoginAuthenticationSet, FsDot11WlanLoginAuthenticationTest, FsDot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11StationConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11BandwidthThresh}, GetNextIndexFsDot11StationConfigTable, FsDot11BandwidthThreshGet, FsDot11BandwidthThreshSet, FsDot11BandwidthThreshTest, FsDot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11StationConfigTableINDEX, 1, 0, 0, "1024"},

{{13,FsDot11CapabilityProfileName}, GetNextIndexFsDot11CapabilityProfileTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsDot11CapabilityProfileTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11CFPollable}, GetNextIndexFsDot11CapabilityProfileTable, FsDot11CFPollableGet, FsDot11CFPollableSet, FsDot11CFPollableTest, FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11CFPollRequest}, GetNextIndexFsDot11CapabilityProfileTable, FsDot11CFPollRequestGet, FsDot11CFPollRequestSet, FsDot11CFPollRequestTest, FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11PrivacyOptionImplemented}, GetNextIndexFsDot11CapabilityProfileTable, FsDot11PrivacyOptionImplementedGet, FsDot11PrivacyOptionImplementedSet, FsDot11PrivacyOptionImplementedTest, FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11ShortPreambleOptionImplemented}, GetNextIndexFsDot11CapabilityProfileTable, FsDot11ShortPreambleOptionImplementedGet, FsDot11ShortPreambleOptionImplementedSet, FsDot11ShortPreambleOptionImplementedTest, FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11PBCCOptionImplemented}, GetNextIndexFsDot11CapabilityProfileTable, FsDot11PBCCOptionImplementedGet, FsDot11PBCCOptionImplementedSet, FsDot11PBCCOptionImplementedTest, FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11ChannelAgilityPresent}, GetNextIndexFsDot11CapabilityProfileTable, FsDot11ChannelAgilityPresentGet, FsDot11ChannelAgilityPresentSet, FsDot11ChannelAgilityPresentTest, FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11QosOptionImplemented}, GetNextIndexFsDot11CapabilityProfileTable, FsDot11QosOptionImplementedGet, FsDot11QosOptionImplementedSet, FsDot11QosOptionImplementedTest, FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11SpectrumManagementRequired}, GetNextIndexFsDot11CapabilityProfileTable, FsDot11SpectrumManagementRequiredGet, FsDot11SpectrumManagementRequiredSet, FsDot11SpectrumManagementRequiredTest, FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11ShortSlotTimeOptionImplemented}, GetNextIndexFsDot11CapabilityProfileTable, FsDot11ShortSlotTimeOptionImplementedGet, FsDot11ShortSlotTimeOptionImplementedSet, FsDot11ShortSlotTimeOptionImplementedTest, FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11APSDOptionImplemented}, GetNextIndexFsDot11CapabilityProfileTable, FsDot11APSDOptionImplementedGet, FsDot11APSDOptionImplementedSet, FsDot11APSDOptionImplementedTest, FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11DSSSOFDMOptionEnabled}, GetNextIndexFsDot11CapabilityProfileTable, FsDot11DSSSOFDMOptionEnabledGet, FsDot11DSSSOFDMOptionEnabledSet, FsDot11DSSSOFDMOptionEnabledTest, FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11DelayedBlockAckOptionImplemented}, GetNextIndexFsDot11CapabilityProfileTable, FsDot11DelayedBlockAckOptionImplementedGet, FsDot11DelayedBlockAckOptionImplementedSet, FsDot11DelayedBlockAckOptionImplementedTest, FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11ImmediateBlockAckOptionImplemented}, GetNextIndexFsDot11CapabilityProfileTable, FsDot11ImmediateBlockAckOptionImplementedGet, FsDot11ImmediateBlockAckOptionImplementedSet, FsDot11ImmediateBlockAckOptionImplementedTest, FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11QAckOptionImplemented}, GetNextIndexFsDot11CapabilityProfileTable, FsDot11QAckOptionImplementedGet, FsDot11QAckOptionImplementedSet, FsDot11QAckOptionImplementedTest, FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11QueueRequestOptionImplemented}, GetNextIndexFsDot11CapabilityProfileTable, FsDot11QueueRequestOptionImplementedGet, FsDot11QueueRequestOptionImplementedSet, FsDot11QueueRequestOptionImplementedTest, FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11TXOPRequestOptionImplemented}, GetNextIndexFsDot11CapabilityProfileTable, FsDot11TXOPRequestOptionImplementedGet, FsDot11TXOPRequestOptionImplementedSet, FsDot11TXOPRequestOptionImplementedTest, FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11RSNAOptionImplemented}, GetNextIndexFsDot11CapabilityProfileTable, FsDot11RSNAOptionImplementedGet, FsDot11RSNAOptionImplementedSet, FsDot11RSNAOptionImplementedTest, FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11RSNAPreauthenticationImplemented}, GetNextIndexFsDot11CapabilityProfileTable, FsDot11RSNAPreauthenticationImplementedGet, FsDot11RSNAPreauthenticationImplementedSet, FsDot11RSNAPreauthenticationImplementedTest, FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11CapabilityRowStatus}, GetNextIndexFsDot11CapabilityProfileTable, FsDot11CapabilityRowStatusGet, FsDot11CapabilityRowStatusSet, FsDot11CapabilityRowStatusTest, FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 1, NULL},

{{13,FsDot11AuthenticationProfileName}, GetNextIndexFsDot11AuthenticationProfileTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsDot11AuthenticationProfileTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11AuthenticationAlgorithm}, GetNextIndexFsDot11AuthenticationProfileTable, FsDot11AuthenticationAlgorithmGet, FsDot11AuthenticationAlgorithmSet, FsDot11AuthenticationAlgorithmTest, FsDot11AuthenticationProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11AuthenticationProfileTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11WepKeyIndex}, GetNextIndexFsDot11AuthenticationProfileTable, FsDot11WepKeyIndexGet, FsDot11WepKeyIndexSet, FsDot11WepKeyIndexTest, FsDot11AuthenticationProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11AuthenticationProfileTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11WepKeyType}, GetNextIndexFsDot11AuthenticationProfileTable, FsDot11WepKeyTypeGet, FsDot11WepKeyTypeSet, FsDot11WepKeyTypeTest, FsDot11AuthenticationProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11AuthenticationProfileTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11WepKeyLength}, GetNextIndexFsDot11AuthenticationProfileTable, FsDot11WepKeyLengthGet, FsDot11WepKeyLengthSet, FsDot11WepKeyLengthTest, FsDot11AuthenticationProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11AuthenticationProfileTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11WepKey}, GetNextIndexFsDot11AuthenticationProfileTable, FsDot11WepKeyGet, FsDot11WepKeySet, FsDot11WepKeyTest, FsDot11AuthenticationProfileTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDot11AuthenticationProfileTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11WebAuthentication}, GetNextIndexFsDot11AuthenticationProfileTable, FsDot11WebAuthenticationGet, FsDot11WebAuthenticationSet, FsDot11WebAuthenticationTest, FsDot11AuthenticationProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11AuthenticationProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11AuthenticationRowStatus}, GetNextIndexFsDot11AuthenticationProfileTable, FsDot11AuthenticationRowStatusGet, FsDot11AuthenticationRowStatusSet, FsDot11AuthenticationRowStatusTest, FsDot11AuthenticationProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11AuthenticationProfileTableINDEX, 1, 0, 1, NULL},

{{13,FsSecurityWebAuthUName}, GetNextIndexFsSecurityWebAuthGuestInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsSecurityWebAuthGuestInfoTableINDEX, 1, 0, 0, NULL},

{{13,FsSecurityWlanProfileId}, GetNextIndexFsSecurityWebAuthGuestInfoTable, FsSecurityWlanProfileIdGet, FsSecurityWlanProfileIdSet, FsSecurityWlanProfileIdTest, FsSecurityWebAuthGuestInfoTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsSecurityWebAuthGuestInfoTableINDEX, 1, 0, 0, NULL},

{{13,FsSecurityWebAuthUserLifetime}, GetNextIndexFsSecurityWebAuthGuestInfoTable, FsSecurityWebAuthUserLifetimeGet, FsSecurityWebAuthUserLifetimeSet, FsSecurityWebAuthUserLifetimeTest, FsSecurityWebAuthGuestInfoTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsSecurityWebAuthGuestInfoTableINDEX, 1, 0, 0, NULL},

{{13,FsSecurityWebAuthUserEmailId}, GetNextIndexFsSecurityWebAuthGuestInfoTable, FsSecurityWebAuthUserEmailIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsSecurityWebAuthGuestInfoTableINDEX, 1, 0, 0, NULL},

{{13,FsSecurityWebAuthGuestInfoRowStatus}, GetNextIndexFsSecurityWebAuthGuestInfoTable, FsSecurityWebAuthGuestInfoRowStatusGet, FsSecurityWebAuthGuestInfoRowStatusSet, FsSecurityWebAuthGuestInfoRowStatusTest, FsSecurityWebAuthGuestInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSecurityWebAuthGuestInfoTableINDEX, 1, 0, 1, NULL},

{{13,FsSecurityWebAuthUPassword}, GetNextIndexFsSecurityWebAuthGuestInfoTable, FsSecurityWebAuthUPasswordGet, FsSecurityWebAuthUPasswordSet, FsSecurityWebAuthUPasswordTest, FsSecurityWebAuthGuestInfoTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsSecurityWebAuthGuestInfoTableINDEX, 1, 0, 0, NULL},

{{13,FsStaMacAddress}, GetNextIndexFsStationQosParamsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsStationQosParamsTableINDEX, 2, 0, 0, NULL},

{{13,FsStaQoSPriority}, GetNextIndexFsStationQosParamsTable, FsStaQoSPriorityGet, FsStaQoSPrioritySet, FsStaQoSPriorityTest, FsStationQosParamsTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsStationQosParamsTableINDEX, 2, 0, 0, NULL},

{{13,FsStaQoSDscp}, GetNextIndexFsStationQosParamsTable, FsStaQoSDscpGet, FsStaQoSDscpSet, FsStaQoSDscpTest, FsStationQosParamsTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsStationQosParamsTableINDEX, 2, 0, 0, NULL},

{{13,FsVlanIsolation}, GetNextIndexFsVlanIsolationTable, FsVlanIsolationGet, FsVlanIsolationSet, FsVlanIsolationTest, FsVlanIsolationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVlanIsolationTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11RadioType}, GetNextIndexFsDot11RadioConfigTable, FsDot11RadioTypeGet, FsDot11RadioTypeSet, FsDot11RadioTypeTest, FsDot11RadioConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11RadioConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11RadioNoOfBssIdSupported}, GetNextIndexFsDot11RadioConfigTable, FsDot11RadioNoOfBssIdSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsDot11RadioConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11RadioAntennaType}, GetNextIndexFsDot11RadioConfigTable, FsDot11RadioAntennaTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot11RadioConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11RadioFailureStatus}, GetNextIndexFsDot11RadioConfigTable, FsDot11RadioFailureStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot11RadioConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11RowStatus}, GetNextIndexFsDot11RadioConfigTable, FsDot11RowStatusGet, FsDot11RowStatusSet, FsDot11RowStatusTest, FsDot11RadioConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11RadioConfigTableINDEX, 1, 0, 1, NULL},

{{13,FsDot11QosProfileName}, GetNextIndexFsDot11QosProfileTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsDot11QosProfileTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11QosTraffic}, GetNextIndexFsDot11QosProfileTable, FsDot11QosTrafficGet, FsDot11QosTrafficSet, FsDot11QosTrafficTest, FsDot11QosProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11QosProfileTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11QosPassengerTrustMode}, GetNextIndexFsDot11QosProfileTable, FsDot11QosPassengerTrustModeGet, FsDot11QosPassengerTrustModeSet, FsDot11QosPassengerTrustModeTest, FsDot11QosProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11QosProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11QosRateLimit}, GetNextIndexFsDot11QosProfileTable, FsDot11QosRateLimitGet, FsDot11QosRateLimitSet, FsDot11QosRateLimitTest, FsDot11QosProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11QosProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11UpStreamCIR}, GetNextIndexFsDot11QosProfileTable, FsDot11UpStreamCIRGet, FsDot11UpStreamCIRSet, FsDot11UpStreamCIRTest, FsDot11QosProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11QosProfileTableINDEX, 1, 0, 0, "100"},

{{13,FsDot11UpStreamCBS}, GetNextIndexFsDot11QosProfileTable, FsDot11UpStreamCBSGet, FsDot11UpStreamCBSSet, FsDot11UpStreamCBSTest, FsDot11QosProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11QosProfileTableINDEX, 1, 0, 0, "1000"},

{{13,FsDot11UpStreamEIR}, GetNextIndexFsDot11QosProfileTable, FsDot11UpStreamEIRGet, FsDot11UpStreamEIRSet, FsDot11UpStreamEIRTest, FsDot11QosProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11QosProfileTableINDEX, 1, 0, 0, "15000"},

{{13,FsDot11UpStreamEBS}, GetNextIndexFsDot11QosProfileTable, FsDot11UpStreamEBSGet, FsDot11UpStreamEBSSet, FsDot11UpStreamEBSTest, FsDot11QosProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11QosProfileTableINDEX, 1, 0, 0, "15000"},

{{13,FsDot11DownStreamCIR}, GetNextIndexFsDot11QosProfileTable, FsDot11DownStreamCIRGet, FsDot11DownStreamCIRSet, FsDot11DownStreamCIRTest, FsDot11QosProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11QosProfileTableINDEX, 1, 0, 0, "100"},

{{13,FsDot11DownStreamCBS}, GetNextIndexFsDot11QosProfileTable, FsDot11DownStreamCBSGet, FsDot11DownStreamCBSSet, FsDot11DownStreamCBSTest, FsDot11QosProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11QosProfileTableINDEX, 1, 0, 0, "1000"},

{{13,FsDot11DownStreamEIR}, GetNextIndexFsDot11QosProfileTable, FsDot11DownStreamEIRGet, FsDot11DownStreamEIRSet, FsDot11DownStreamEIRTest, FsDot11QosProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11QosProfileTableINDEX, 1, 0, 0, "15000"},

{{13,FsDot11DownStreamEBS}, GetNextIndexFsDot11QosProfileTable, FsDot11DownStreamEBSGet, FsDot11DownStreamEBSSet, FsDot11DownStreamEBSTest, FsDot11QosProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11QosProfileTableINDEX, 1, 0, 0, "15000"},

{{13,FsDot11QosRowStatus}, GetNextIndexFsDot11QosProfileTable, FsDot11QosRowStatusGet, FsDot11QosRowStatusSet, FsDot11QosRowStatusTest, FsDot11QosProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11QosProfileTableINDEX, 1, 0, 1, NULL},

{{13,FsDot11WlanCFPollable}, GetNextIndexFsDot11WlanCapabilityProfileTable, FsDot11WlanCFPollableGet, FsDot11WlanCFPollableSet, FsDot11WlanCFPollableTest, FsDot11WlanCapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanCapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11WlanCFPollRequest}, GetNextIndexFsDot11WlanCapabilityProfileTable, FsDot11WlanCFPollRequestGet, FsDot11WlanCFPollRequestSet, FsDot11WlanCFPollRequestTest, FsDot11WlanCapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanCapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11WlanPrivacyOptionImplemented}, GetNextIndexFsDot11WlanCapabilityProfileTable, FsDot11WlanPrivacyOptionImplementedGet, FsDot11WlanPrivacyOptionImplementedSet, FsDot11WlanPrivacyOptionImplementedTest, FsDot11WlanCapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanCapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11WlanShortPreambleOptionImplemented}, GetNextIndexFsDot11WlanCapabilityProfileTable, FsDot11WlanShortPreambleOptionImplementedGet, FsDot11WlanShortPreambleOptionImplementedSet, FsDot11WlanShortPreambleOptionImplementedTest, FsDot11WlanCapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanCapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11WlanPBCCOptionImplemented}, GetNextIndexFsDot11WlanCapabilityProfileTable, FsDot11WlanPBCCOptionImplementedGet, FsDot11WlanPBCCOptionImplementedSet, FsDot11WlanPBCCOptionImplementedTest, FsDot11WlanCapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanCapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11WlanChannelAgilityPresent}, GetNextIndexFsDot11WlanCapabilityProfileTable, FsDot11WlanChannelAgilityPresentGet, FsDot11WlanChannelAgilityPresentSet, FsDot11WlanChannelAgilityPresentTest, FsDot11WlanCapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanCapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11WlanQosOptionImplemented}, GetNextIndexFsDot11WlanCapabilityProfileTable, FsDot11WlanQosOptionImplementedGet, FsDot11WlanQosOptionImplementedSet, FsDot11WlanQosOptionImplementedTest, FsDot11WlanCapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanCapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11WlanSpectrumManagementRequired}, GetNextIndexFsDot11WlanCapabilityProfileTable, FsDot11WlanSpectrumManagementRequiredGet, FsDot11WlanSpectrumManagementRequiredSet, FsDot11WlanSpectrumManagementRequiredTest, FsDot11WlanCapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanCapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11WlanShortSlotTimeOptionImplemented}, GetNextIndexFsDot11WlanCapabilityProfileTable, FsDot11WlanShortSlotTimeOptionImplementedGet, FsDot11WlanShortSlotTimeOptionImplementedSet, FsDot11WlanShortSlotTimeOptionImplementedTest, FsDot11WlanCapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanCapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11WlanAPSDOptionImplemented}, GetNextIndexFsDot11WlanCapabilityProfileTable, FsDot11WlanAPSDOptionImplementedGet, FsDot11WlanAPSDOptionImplementedSet, FsDot11WlanAPSDOptionImplementedTest, FsDot11WlanCapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanCapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11WlanDSSSOFDMOptionEnabled}, GetNextIndexFsDot11WlanCapabilityProfileTable, FsDot11WlanDSSSOFDMOptionEnabledGet, FsDot11WlanDSSSOFDMOptionEnabledSet, FsDot11WlanDSSSOFDMOptionEnabledTest, FsDot11WlanCapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanCapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11WlanDelayedBlockAckOptionImplemented}, GetNextIndexFsDot11WlanCapabilityProfileTable, FsDot11WlanDelayedBlockAckOptionImplementedGet, FsDot11WlanDelayedBlockAckOptionImplementedSet, FsDot11WlanDelayedBlockAckOptionImplementedTest, FsDot11WlanCapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanCapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11WlanImmediateBlockAckOptionImplemented}, GetNextIndexFsDot11WlanCapabilityProfileTable, FsDot11WlanImmediateBlockAckOptionImplementedGet, FsDot11WlanImmediateBlockAckOptionImplementedSet, FsDot11WlanImmediateBlockAckOptionImplementedTest, FsDot11WlanCapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanCapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11WlanQAckOptionImplemented}, GetNextIndexFsDot11WlanCapabilityProfileTable, FsDot11WlanQAckOptionImplementedGet, FsDot11WlanQAckOptionImplementedSet, FsDot11WlanQAckOptionImplementedTest, FsDot11WlanCapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanCapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11WlanQueueRequestOptionImplemented}, GetNextIndexFsDot11WlanCapabilityProfileTable, FsDot11WlanQueueRequestOptionImplementedGet, FsDot11WlanQueueRequestOptionImplementedSet, FsDot11WlanQueueRequestOptionImplementedTest, FsDot11WlanCapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanCapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11WlanTXOPRequestOptionImplemented}, GetNextIndexFsDot11WlanCapabilityProfileTable, FsDot11WlanTXOPRequestOptionImplementedGet, FsDot11WlanTXOPRequestOptionImplementedSet, FsDot11WlanTXOPRequestOptionImplementedTest, FsDot11WlanCapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanCapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11WlanRSNAOptionImplemented}, GetNextIndexFsDot11WlanCapabilityProfileTable, FsDot11WlanRSNAOptionImplementedGet, FsDot11WlanRSNAOptionImplementedSet, FsDot11WlanRSNAOptionImplementedTest, FsDot11WlanCapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanCapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11WlanRSNAPreauthenticationImplemented}, GetNextIndexFsDot11WlanCapabilityProfileTable, FsDot11WlanRSNAPreauthenticationImplementedGet, FsDot11WlanRSNAPreauthenticationImplementedSet, FsDot11WlanRSNAPreauthenticationImplementedTest, FsDot11WlanCapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanCapabilityProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11WlanCapabilityRowStatus}, GetNextIndexFsDot11WlanCapabilityProfileTable, FsDot11WlanCapabilityRowStatusGet, FsDot11WlanCapabilityRowStatusSet, FsDot11WlanCapabilityRowStatusTest, FsDot11WlanCapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanCapabilityProfileTableINDEX, 1, 0, 1, NULL},

{{13,FsDot11WlanAuthenticationAlgorithm}, GetNextIndexFsDot11WlanAuthenticationProfileTable, FsDot11WlanAuthenticationAlgorithmGet, FsDot11WlanAuthenticationAlgorithmSet, FsDot11WlanAuthenticationAlgorithmTest, FsDot11WlanAuthenticationProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanAuthenticationProfileTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11WlanWepKeyIndex}, GetNextIndexFsDot11WlanAuthenticationProfileTable, FsDot11WlanWepKeyIndexGet, FsDot11WlanWepKeyIndexSet, FsDot11WlanWepKeyIndexTest, FsDot11WlanAuthenticationProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11WlanAuthenticationProfileTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11WlanWepKeyType}, GetNextIndexFsDot11WlanAuthenticationProfileTable, FsDot11WlanWepKeyTypeGet, FsDot11WlanWepKeyTypeSet, FsDot11WlanWepKeyTypeTest, FsDot11WlanAuthenticationProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanAuthenticationProfileTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11WlanWepKeyLength}, GetNextIndexFsDot11WlanAuthenticationProfileTable, FsDot11WlanWepKeyLengthGet, FsDot11WlanWepKeyLengthSet, FsDot11WlanWepKeyLengthTest, FsDot11WlanAuthenticationProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11WlanAuthenticationProfileTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11WlanWepKey}, GetNextIndexFsDot11WlanAuthenticationProfileTable, FsDot11WlanWepKeyGet, FsDot11WlanWepKeySet, FsDot11WlanWepKeyTest, FsDot11WlanAuthenticationProfileTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDot11WlanAuthenticationProfileTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11WlanWebAuthentication}, GetNextIndexFsDot11WlanAuthenticationProfileTable, FsDot11WlanWebAuthenticationGet, FsDot11WlanWebAuthenticationSet, FsDot11WlanWebAuthenticationTest, FsDot11WlanAuthenticationProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanAuthenticationProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11WlanAuthenticationRowStatus}, GetNextIndexFsDot11WlanAuthenticationProfileTable, FsDot11WlanAuthenticationRowStatusGet, FsDot11WlanAuthenticationRowStatusSet, FsDot11WlanAuthenticationRowStatusTest, FsDot11WlanAuthenticationProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanAuthenticationProfileTableINDEX, 1, 0, 1, NULL},

{{13,FsDot11WlanQosTraffic}, GetNextIndexFsDot11WlanQosProfileTable, FsDot11WlanQosTrafficGet, FsDot11WlanQosTrafficSet, FsDot11WlanQosTrafficTest, FsDot11WlanQosProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanQosProfileTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11WlanQosPassengerTrustMode}, GetNextIndexFsDot11WlanQosProfileTable, FsDot11WlanQosPassengerTrustModeGet, FsDot11WlanQosPassengerTrustModeSet, FsDot11WlanQosPassengerTrustModeTest, FsDot11WlanQosProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanQosProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11WlanQosRateLimit}, GetNextIndexFsDot11WlanQosProfileTable, FsDot11WlanQosRateLimitGet, FsDot11WlanQosRateLimitSet, FsDot11WlanQosRateLimitTest, FsDot11WlanQosProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanQosProfileTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11WlanUpStreamCIR}, GetNextIndexFsDot11WlanQosProfileTable, FsDot11WlanUpStreamCIRGet, FsDot11WlanUpStreamCIRSet, FsDot11WlanUpStreamCIRTest, FsDot11WlanQosProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11WlanQosProfileTableINDEX, 1, 0, 0, "100"},

{{13,FsDot11WlanUpStreamCBS}, GetNextIndexFsDot11WlanQosProfileTable, FsDot11WlanUpStreamCBSGet, FsDot11WlanUpStreamCBSSet, FsDot11WlanUpStreamCBSTest, FsDot11WlanQosProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11WlanQosProfileTableINDEX, 1, 0, 0, "1000"},

{{13,FsDot11WlanUpStreamEIR}, GetNextIndexFsDot11WlanQosProfileTable, FsDot11WlanUpStreamEIRGet, FsDot11WlanUpStreamEIRSet, FsDot11WlanUpStreamEIRTest, FsDot11WlanQosProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11WlanQosProfileTableINDEX, 1, 0, 0, "15000"},

{{13,FsDot11WlanUpStreamEBS}, GetNextIndexFsDot11WlanQosProfileTable, FsDot11WlanUpStreamEBSGet, FsDot11WlanUpStreamEBSSet, FsDot11WlanUpStreamEBSTest, FsDot11WlanQosProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11WlanQosProfileTableINDEX, 1, 0, 0, "15000"},

{{13,FsDot11WlanDownStreamCIR}, GetNextIndexFsDot11WlanQosProfileTable, FsDot11WlanDownStreamCIRGet, FsDot11WlanDownStreamCIRSet, FsDot11WlanDownStreamCIRTest, FsDot11WlanQosProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11WlanQosProfileTableINDEX, 1, 0, 0, "100"},

{{13,FsDot11WlanDownStreamCBS}, GetNextIndexFsDot11WlanQosProfileTable, FsDot11WlanDownStreamCBSGet, FsDot11WlanDownStreamCBSSet, FsDot11WlanDownStreamCBSTest, FsDot11WlanQosProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11WlanQosProfileTableINDEX, 1, 0, 0, "1000"},

{{13,FsDot11WlanDownStreamEIR}, GetNextIndexFsDot11WlanQosProfileTable, FsDot11WlanDownStreamEIRGet, FsDot11WlanDownStreamEIRSet, FsDot11WlanDownStreamEIRTest, FsDot11WlanQosProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11WlanQosProfileTableINDEX, 1, 0, 0, "15000"},

{{13,FsDot11WlanDownStreamEBS}, GetNextIndexFsDot11WlanQosProfileTable, FsDot11WlanDownStreamEBSGet, FsDot11WlanDownStreamEBSSet, FsDot11WlanDownStreamEBSTest, FsDot11WlanQosProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11WlanQosProfileTableINDEX, 1, 0, 0, "15000"},

{{13,FsDot11WlanQosRowStatus}, GetNextIndexFsDot11WlanQosProfileTable, FsDot11WlanQosRowStatusGet, FsDot11WlanQosRowStatusSet, FsDot11WlanQosRowStatusTest, FsDot11WlanQosProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanQosProfileTableINDEX, 1, 0, 1, NULL},

{{13,FsDot11CapabilityMappingProfileName}, GetNextIndexFsDot11CapabilityMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsDot11CapabilityMappingTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11CapabilityMappingRowStatus}, GetNextIndexFsDot11CapabilityMappingTable, FsDot11CapabilityMappingRowStatusGet, FsDot11CapabilityMappingRowStatusSet, FsDot11CapabilityMappingRowStatusTest, FsDot11CapabilityMappingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11CapabilityMappingTableINDEX, 1, 0, 1, NULL},

{{13,FsDot11AuthMappingProfileName}, GetNextIndexFsDot11AuthMappingTable, FsDot11AuthMappingProfileNameGet, FsDot11AuthMappingProfileNameSet, FsDot11AuthMappingProfileNameTest, FsDot11AuthMappingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDot11AuthMappingTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11AuthMappingRowStatus}, GetNextIndexFsDot11AuthMappingTable, FsDot11AuthMappingRowStatusGet, FsDot11AuthMappingRowStatusSet, FsDot11AuthMappingRowStatusTest, FsDot11AuthMappingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11AuthMappingTableINDEX, 1, 0, 1, NULL},

{{13,FsDot11QosMappingProfileName}, GetNextIndexFsDot11QosMappingTable, FsDot11QosMappingProfileNameGet, FsDot11QosMappingProfileNameSet, FsDot11QosMappingProfileNameTest, FsDot11QosMappingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDot11QosMappingTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11QosMappingRowStatus}, GetNextIndexFsDot11QosMappingTable, FsDot11QosMappingRowStatusGet, FsDot11QosMappingRowStatusSet, FsDot11QosMappingRowStatusTest, FsDot11QosMappingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11QosMappingTableINDEX, 1, 0, 1, NULL},

{{13,FsDot11ClientMacAddress}, GetNextIndexFsDot11ClientSummaryTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsDot11ClientSummaryTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11WlanProfileId}, GetNextIndexFsDot11ClientSummaryTable, FsDot11WlanProfileIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsDot11ClientSummaryTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11WtpProfileName}, GetNextIndexFsDot11ClientSummaryTable, FsDot11WtpProfileNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsDot11ClientSummaryTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11WtpRadioId}, GetNextIndexFsDot11ClientSummaryTable, FsDot11WtpRadioIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsDot11ClientSummaryTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11AuthStatus}, GetNextIndexFsDot11ClientSummaryTable, FsDot11AuthStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot11ClientSummaryTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11AssocStatus}, GetNextIndexFsDot11ClientSummaryTable, FsDot11AssocStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot11ClientSummaryTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11ExternalWebAuthMethod}, GetNextIndexFsDot11ExternalWebAuthProfileTable, FsDot11ExternalWebAuthMethodGet, FsDot11ExternalWebAuthMethodSet, FsDot11ExternalWebAuthMethodTest, FsDot11ExternalWebAuthProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11ExternalWebAuthProfileTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11ExternalWebAuthUrl}, GetNextIndexFsDot11ExternalWebAuthProfileTable, FsDot11ExternalWebAuthUrlGet, FsDot11ExternalWebAuthUrlSet, FsDot11ExternalWebAuthUrlTest, FsDot11ExternalWebAuthProfileTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDot11ExternalWebAuthProfileTableINDEX, 1, 0, 0, NULL},

{{11,FsApGroupEnabledStatus}, NULL, FsApGroupEnabledStatusGet, FsApGroupEnabledStatusSet, FsApGroupEnabledStatusTest, FsApGroupEnabledStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{13,FsApGroupIndex}, GetNextIndexFsApGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsApGroupTableINDEX, 1, 0, 0, NULL},

{{13,FsApGroupName}, GetNextIndexFsApGroupTable, FsApGroupNameGet, FsApGroupNameSet, FsApGroupNameTest, FsApGroupTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsApGroupTableINDEX, 1, 0, 0, NULL},

{{13,FsApGroupNameDescription}, GetNextIndexFsApGroupTable, FsApGroupNameDescriptionGet, FsApGroupNameDescriptionSet, FsApGroupNameDescriptionTest, FsApGroupTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsApGroupTableINDEX, 1, 0, 0, NULL},

{{13,FsApGroupRadioPolicy}, GetNextIndexFsApGroupTable, FsApGroupRadioPolicyGet, FsApGroupRadioPolicySet, FsApGroupRadioPolicyTest, FsApGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsApGroupTableINDEX, 1, 0, 0, NULL},

{{13,FsApGroupInterfaceVlan}, GetNextIndexFsApGroupTable, FsApGroupInterfaceVlanGet, FsApGroupInterfaceVlanSet, FsApGroupInterfaceVlanTest, FsApGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsApGroupTableINDEX, 1, 0, 0, NULL},

{{13,FsApGroupRowStatus}, GetNextIndexFsApGroupTable, FsApGroupRowStatusGet, FsApGroupRowStatusSet, FsApGroupRowStatusTest, FsApGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsApGroupTableINDEX, 1, 0, 1, NULL},

{{13,FsApGroupWTPIndex}, GetNextIndexFsApGroupWTPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsApGroupWTPTableINDEX, 2, 0, 0, NULL},

{{13,FsApGroupWtpProfileId}, GetNextIndexFsApGroupWTPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsApGroupWTPTableINDEX, 2, 0, 0, NULL},

{{13,FsApGroupWTPRowStatus}, GetNextIndexFsApGroupWTPTable, FsApGroupWTPRowStatusGet, FsApGroupWTPRowStatusSet, FsApGroupWTPRowStatusTest, FsApGroupWTPTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsApGroupWTPTableINDEX, 2, 0, 1, NULL},

{{13,FsApGroupWLANIndex}, GetNextIndexFsApGroupWLANTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsApGroupWLANTableINDEX, 2, 0, 0, NULL},

{{13,FsApGroupWlanProfileId}, GetNextIndexFsApGroupWLANTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsApGroupWLANTableINDEX, 2, 0, 0, NULL},

{{13,FsApGroupWLANRowStatus}, GetNextIndexFsApGroupWLANTable, FsApGroupWLANRowStatusGet, FsApGroupWLANRowStatusSet, FsApGroupWLANRowStatusTest, FsApGroupWLANTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsApGroupWLANTableINDEX, 2, 0, 1, NULL},

{{13,FsDot11TaggingPolicy}, GetNextIndexFsDot11RadioQosTable, FsDot11TaggingPolicyGet, FsDot11TaggingPolicySet, FsDot11TaggingPolicyTest, FsDot11RadioQosTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDot11RadioQosTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11QueueDepth}, GetNextIndexFsDot11QAPTable, FsDot11QueueDepthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsDot11QAPTableINDEX, 2, 0, 0, NULL},

{{13,FsDot11PriorityValue}, GetNextIndexFsDot11QAPTable, FsDot11PriorityValueGet, FsDot11PriorityValueSet, FsDot11PriorityValueTest, FsDot11QAPTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11QAPTableINDEX, 2, 0, 0, "0"},

{{13,FsDot11DscpValue}, GetNextIndexFsDot11QAPTable, FsDot11DscpValueGet, FsDot11DscpValueSet, FsDot11DscpValueTest, FsDot11QAPTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11QAPTableINDEX, 2, 0, 0, "0"},

{{13,FsQAPProfileName}, GetNextIndexFsQAPProfileTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsQAPProfileTableINDEX, 2, 0, 0, NULL},

{{13,FsQAPProfileIndex}, GetNextIndexFsQAPProfileTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsQAPProfileTableINDEX, 2, 0, 0, NULL},

{{13,FsQAPProfileCWmin}, GetNextIndexFsQAPProfileTable, FsQAPProfileCWminGet, FsQAPProfileCWminSet, FsQAPProfileCWminTest, FsQAPProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsQAPProfileTableINDEX, 2, 0, 0, NULL},

{{13,FsQAPProfileCWmax}, GetNextIndexFsQAPProfileTable, FsQAPProfileCWmaxGet, FsQAPProfileCWmaxSet, FsQAPProfileCWmaxTest, FsQAPProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsQAPProfileTableINDEX, 2, 0, 0, NULL},

{{13,FsQAPProfileAIFSN}, GetNextIndexFsQAPProfileTable, FsQAPProfileAIFSNGet, FsQAPProfileAIFSNSet, FsQAPProfileAIFSNTest, FsQAPProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsQAPProfileTableINDEX, 2, 0, 0, NULL},

{{13,FsQAPProfileTXOPLimit}, GetNextIndexFsQAPProfileTable, FsQAPProfileTXOPLimitGet, FsQAPProfileTXOPLimitSet, FsQAPProfileTXOPLimitTest, FsQAPProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsQAPProfileTableINDEX, 2, 0, 0, NULL},

{{13,FsQAPProfileQueueDepth}, GetNextIndexFsQAPProfileTable, FsQAPProfileQueueDepthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsQAPProfileTableINDEX, 2, 0, 0, NULL},

{{13,FsQAPProfilePriorityValue}, GetNextIndexFsQAPProfileTable, FsQAPProfilePriorityValueGet, FsQAPProfilePriorityValueSet, FsQAPProfilePriorityValueTest, FsQAPProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsQAPProfileTableINDEX, 2, 0, 0, "0"},

{{13,FsQAPProfileDscpValue}, GetNextIndexFsQAPProfileTable, FsQAPProfileDscpValueGet, FsQAPProfileDscpValueSet, FsQAPProfileDscpValueTest, FsQAPProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsQAPProfileTableINDEX, 2, 0, 0, "0"},

{{13,FsQAPProfileRowStatus}, GetNextIndexFsQAPProfileTable, FsQAPProfileRowStatusGet, FsQAPProfileRowStatusSet, FsQAPProfileRowStatusTest, FsQAPProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQAPProfileTableINDEX, 2, 0, 1, NULL},

{{13,FsAntennaMode}, GetNextIndexFsDot11AntennasListTable, FsAntennaModeGet, FsAntennaModeSet, FsAntennaModeTest, FsDot11AntennasListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11AntennasListTableINDEX, 2, 0, 0, "3"},

{{13,FsAntennaSelection}, GetNextIndexFsDot11AntennasListTable, FsAntennaSelectionGet, FsAntennaSelectionSet, FsAntennaSelectionTest, FsDot11AntennasListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11AntennasListTableINDEX, 2, 0, 0, "1"},

{{13,FsDot11WlanProfileIfIndex}, GetNextIndexFsDot11WlanTable, FsDot11WlanProfileIfIndexGet, FsDot11WlanProfileIfIndexSet, FsDot11WlanProfileIfIndexTest, FsDot11WlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11WlanRowStatus}, GetNextIndexFsDot11WlanTable, FsDot11WlanRowStatusGet, FsDot11WlanRowStatusSet, FsDot11WlanRowStatusTest, FsDot11WlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanTableINDEX, 1, 0, 1, NULL},

{{13,FsDot11WlanBindWlanId}, GetNextIndexFsDot11WlanBindTable, FsDot11WlanBindWlanIdGet, FsDot11WlanBindWlanIdSet, FsDot11WlanBindWlanIdTest, FsDot11WlanBindTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11WlanBindTableINDEX, 2, 0, 0, NULL},

{{13,FsDot11WlanBindBssIfIndex}, GetNextIndexFsDot11WlanBindTable, FsDot11WlanBindBssIfIndexGet, FsDot11WlanBindBssIfIndexSet, FsDot11WlanBindBssIfIndexTest, FsDot11WlanBindTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanBindTableINDEX, 2, 0, 0, NULL},

{{13,FsDot11WlanBindRowStatus}, GetNextIndexFsDot11WlanBindTable, FsDot11WlanBindRowStatusGet, FsDot11WlanBindRowStatusSet, FsDot11WlanBindRowStatusTest, FsDot11WlanBindTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WlanBindTableINDEX, 2, 0, 1, NULL},

{{13,FsDot11nConfigShortGIfor20MHz}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigShortGIfor20MHzGet, FsDot11nConfigShortGIfor20MHzSet, FsDot11nConfigShortGIfor20MHzTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigShortGIfor40MHz}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigShortGIfor40MHzGet, FsDot11nConfigShortGIfor40MHzSet, FsDot11nConfigShortGIfor40MHzTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigChannelWidth}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigChannelWidthGet, FsDot11nConfigChannelWidthSet, FsDot11nConfigChannelWidthTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11nConfigMIMOPowerSave}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigMIMOPowerSaveGet, FsDot11nConfigMIMOPowerSaveSet, FsDot11nConfigMIMOPowerSaveTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "3"},

{{13,FsDot11nConfigDelayedBlockAckOptionActivated}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigDelayedBlockAckOptionActivatedGet, FsDot11nConfigDelayedBlockAckOptionActivatedSet, FsDot11nConfigDelayedBlockAckOptionActivatedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigMaxAMSDULengthConfig}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigMaxAMSDULengthConfigGet, FsDot11nConfigMaxAMSDULengthConfigSet, FsDot11nConfigMaxAMSDULengthConfigTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "3839"},

{{13,FsDot11nConfigMaxRxAMPDUFactorConfig}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigMaxRxAMPDUFactorConfigGet, FsDot11nConfigMaxRxAMPDUFactorConfigSet, FsDot11nConfigMaxRxAMPDUFactorConfigTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11nConfigMinimumMPDUStartSpacingConfig}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigMinimumMPDUStartSpacingConfigGet, FsDot11nConfigMinimumMPDUStartSpacingConfigSet, FsDot11nConfigMinimumMPDUStartSpacingConfigTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11nConfigPCOOptionActivated}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigPCOOptionActivatedGet, FsDot11nConfigPCOOptionActivatedSet, FsDot11nConfigPCOOptionActivatedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigTransitionTimeConfig}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigTransitionTimeConfigGet, FsDot11nConfigTransitionTimeConfigSet, FsDot11nConfigTransitionTimeConfigTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigMCSFeedbackOptionActivated}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigMCSFeedbackOptionActivatedGet, FsDot11nConfigMCSFeedbackOptionActivatedSet, FsDot11nConfigMCSFeedbackOptionActivatedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11nConfigHTControlFieldSupported}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigHTControlFieldSupportedGet, FsDot11nConfigHTControlFieldSupportedSet, FsDot11nConfigHTControlFieldSupportedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigRDResponderOptionActivated}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigRDResponderOptionActivatedGet, FsDot11nConfigRDResponderOptionActivatedSet, FsDot11nConfigRDResponderOptionActivatedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigtHTDSSCCKModein40MHz}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigtHTDSSCCKModein40MHzGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot11nConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11nConfigtHTDSSCCKModein40MHzConfig}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigtHTDSSCCKModein40MHzConfigGet, FsDot11nConfigtHTDSSCCKModein40MHzConfigSet, FsDot11nConfigtHTDSSCCKModein40MHzConfigTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigTxRxMCSSetNotEqual}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigTxRxMCSSetNotEqualGet, FsDot11nConfigTxRxMCSSetNotEqualSet, FsDot11nConfigTxRxMCSSetNotEqualTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigTxMaximumNumberSpatialStreamsSupported}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigTxMaximumNumberSpatialStreamsSupportedGet, FsDot11nConfigTxMaximumNumberSpatialStreamsSupportedSet, FsDot11nConfigTxMaximumNumberSpatialStreamsSupportedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11nConfigTxUnequalModulationSupported}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigTxUnequalModulationSupportedGet, FsDot11nConfigTxUnequalModulationSupportedSet, FsDot11nConfigTxUnequalModulationSupportedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigImplicitTransmitBeamformingRecvOptionImplemented}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigImplicitTransmitBeamformingRecvOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigImplicitTransmitBeamformingRecvOptionActivatedGet, FsDot11nConfigImplicitTransmitBeamformingRecvOptionActivatedSet, FsDot11nConfigImplicitTransmitBeamformingRecvOptionActivatedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigReceiveStaggerSoundingOptionActivated}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigReceiveStaggerSoundingOptionActivatedGet, FsDot11nConfigReceiveStaggerSoundingOptionActivatedSet, FsDot11nConfigReceiveStaggerSoundingOptionActivatedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigTransmitStaggerSoundingOptionActivated}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigTransmitStaggerSoundingOptionActivatedGet, FsDot11nConfigTransmitStaggerSoundingOptionActivatedSet, FsDot11nConfigTransmitStaggerSoundingOptionActivatedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigReceiveNDPOptionActivated}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigReceiveNDPOptionActivatedGet, FsDot11nConfigReceiveNDPOptionActivatedSet, FsDot11nConfigReceiveNDPOptionActivatedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigTransmitNDPOptionActivated}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigTransmitNDPOptionActivatedGet, FsDot11nConfigTransmitNDPOptionActivatedSet, FsDot11nConfigTransmitNDPOptionActivatedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigImplicitTransmitBeamformingOptionActivated}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigImplicitTransmitBeamformingOptionActivatedGet, FsDot11nConfigImplicitTransmitBeamformingOptionActivatedSet, FsDot11nConfigImplicitTransmitBeamformingOptionActivatedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigCalibrationOptionActivated}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigCalibrationOptionActivatedGet, FsDot11nConfigCalibrationOptionActivatedSet, FsDot11nConfigCalibrationOptionActivatedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11nConfigExplicitCSITransmitBeamformingOptionActivated}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigExplicitCSITransmitBeamformingOptionActivatedGet, FsDot11nConfigExplicitCSITransmitBeamformingOptionActivatedSet, FsDot11nConfigExplicitCSITransmitBeamformingOptionActivatedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivatedGet, FsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivatedSet, FsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivatedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigExplicitCompressedBeamformingMatrixOptImplemented}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigExplicitCompressedBeamformingMatrixOptImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivatedGet, FsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivatedSet, FsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivatedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivatedGet, FsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivatedSet, FsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivatedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivatedGet, FsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivatedSet, FsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivatedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivatedGet, FsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivatedSet, FsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivatedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11nConfigMinimalGroupingImplemented}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigMinimalGroupingImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsDot11nConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11nConfigMinimalGroupingActivated}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigMinimalGroupingActivatedGet, FsDot11nConfigMinimalGroupingActivatedSet, FsDot11nConfigMinimalGroupingActivatedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11nConfigNumberBeamFormingCSISupportAntenna}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigNumberBeamFormingCSISupportAntennaGet, FsDot11nConfigNumberBeamFormingCSISupportAntennaSet, FsDot11nConfigNumberBeamFormingCSISupportAntennaTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntennaGet, FsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntennaSet, FsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntennaTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigNumberCompressedBeamformingMatrixSupportAntennaGet, FsDot11nConfigNumberCompressedBeamformingMatrixSupportAntennaSet, FsDot11nConfigNumberCompressedBeamformingMatrixSupportAntennaTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaImplemented}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsDot11nConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivatedGet, FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivatedSet, FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivatedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11nConfigChannelEstimationCapabilityImplemented}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigChannelEstimationCapabilityImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsDot11nConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11nConfigChannelEstimationCapabilityActivated}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigChannelEstimationCapabilityActivatedGet, FsDot11nConfigChannelEstimationCapabilityActivatedSet, FsDot11nConfigChannelEstimationCapabilityActivatedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11nConfigCurrentPrimaryChannel}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigCurrentPrimaryChannelGet, FsDot11nConfigCurrentPrimaryChannelSet, FsDot11nConfigCurrentPrimaryChannelTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11nConfigCurrentSecondaryChannel}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigCurrentSecondaryChannelGet, FsDot11nConfigCurrentSecondaryChannelSet, FsDot11nConfigCurrentSecondaryChannelTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11nConfigSTAChannelWidth}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigSTAChannelWidthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot11nConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11nConfigSTAChannelWidthConfig}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigSTAChannelWidthConfigGet, FsDot11nConfigSTAChannelWidthConfigSet, FsDot11nConfigSTAChannelWidthConfigTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigNonGreenfieldHTSTAsPresent}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigNonGreenfieldHTSTAsPresentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot11nConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11nConfigNonGreenfieldHTSTAsPresentConfig}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigNonGreenfieldHTSTAsPresentConfigGet, FsDot11nConfigNonGreenfieldHTSTAsPresentConfigSet, FsDot11nConfigNonGreenfieldHTSTAsPresentConfigTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigOBSSNonHTSTAsPresent}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigOBSSNonHTSTAsPresentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot11nConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11nConfigOBSSNonHTSTAsPresentConfig}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigOBSSNonHTSTAsPresentConfigGet, FsDot11nConfigOBSSNonHTSTAsPresentConfigSet, FsDot11nConfigOBSSNonHTSTAsPresentConfigTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigDualBeacon}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigDualBeaconGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot11nConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11nConfigDualBeaconConfig}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigDualBeaconConfigGet, FsDot11nConfigDualBeaconConfigSet, FsDot11nConfigDualBeaconConfigTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigSTBCBeacon}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigSTBCBeaconGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigSTBCBeaconConfig}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigSTBCBeaconConfigGet, FsDot11nConfigSTBCBeaconConfigSet, FsDot11nConfigSTBCBeaconConfigTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigPCOPhase}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigPCOPhaseGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot11nConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11nConfigPCOPhaseConfig}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigPCOPhaseConfigGet, FsDot11nConfigPCOPhaseConfigSet, FsDot11nConfigPCOPhaseConfigTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11nConfigAllowChannelWidth}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigAllowChannelWidthGet, FsDot11nConfigAllowChannelWidthSet, FsDot11nConfigAllowChannelWidthTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11nConfigRxSTBCOptionImplemented}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigRxSTBCOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDot11nConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11nConfigRxSTBCOptionActivated}, GetNextIndexFsDot11nConfigTable, FsDot11nConfigRxSTBCOptionActivatedGet, FsDot11nConfigRxSTBCOptionActivatedSet, FsDot11nConfigRxSTBCOptionActivatedTest, FsDot11nConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11nMCSDataRateIndex}, GetNextIndexFsDot11nMCSDataRateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsDot11nMCSDataRateTableINDEX, 2, 0, 0, NULL},

{{13,FsDot11nMCSDataRate}, GetNextIndexFsDot11nMCSDataRateTable, FsDot11nMCSDataRateGet, FsDot11nMCSDataRateSet, FsDot11nMCSDataRateTest, FsDot11nMCSDataRateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nMCSDataRateTableINDEX, 2, 0, 0, "2"},

{{13,FsDot11nAMPDUStatus}, GetNextIndexFsDot11nExtConfigTable, FsDot11nAMPDUStatusGet, FsDot11nAMPDUStatusSet, FsDot11nAMPDUStatusTest, FsDot11nExtConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nExtConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11nAMPDUSubFrame}, GetNextIndexFsDot11nExtConfigTable, FsDot11nAMPDUSubFrameGet, FsDot11nAMPDUSubFrameSet, FsDot11nAMPDUSubFrameTest, FsDot11nExtConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11nExtConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11nAMPDULimit}, GetNextIndexFsDot11nExtConfigTable, FsDot11nAMPDULimitGet, FsDot11nAMPDULimitSet, FsDot11nAMPDULimitTest, FsDot11nExtConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11nExtConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11nAMSDUStatus}, GetNextIndexFsDot11nExtConfigTable, FsDot11nAMSDUStatusGet, FsDot11nAMSDUStatusSet, FsDot11nAMSDUStatusTest, FsDot11nExtConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11nExtConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11nAMSDULimit}, GetNextIndexFsDot11nExtConfigTable, FsDot11nAMSDULimitGet, FsDot11nAMSDULimitSet, FsDot11nAMSDULimitTest, FsDot11nExtConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11nExtConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsWtpImageVersion}, GetNextIndexFsWtpImageUpgradeTable, FsWtpImageVersionGet, FsWtpImageVersionSet, FsWtpImageVersionTest, FsWtpImageUpgradeTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsWtpImageUpgradeTableINDEX, 1, 0, 0, NULL},

{{13,FsWtpUpgradeDev}, GetNextIndexFsWtpImageUpgradeTable, FsWtpUpgradeDevGet, FsWtpUpgradeDevSet, FsWtpUpgradeDevTest, FsWtpImageUpgradeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpImageUpgradeTableINDEX, 1, 0, 0, "1"},

{{13,FsWtpName}, GetNextIndexFsWtpImageUpgradeTable, FsWtpNameGet, FsWtpNameSet, FsWtpNameTest, FsWtpImageUpgradeTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsWtpImageUpgradeTableINDEX, 1, 0, 0, NULL},

{{13,FsWtpImageName}, GetNextIndexFsWtpImageUpgradeTable, FsWtpImageNameGet, FsWtpImageNameSet, FsWtpImageNameTest, FsWtpImageUpgradeTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsWtpImageUpgradeTableINDEX, 1, 0, 0, NULL},

{{13,FsWtpAddressType}, GetNextIndexFsWtpImageUpgradeTable, FsWtpAddressTypeGet, FsWtpAddressTypeSet, FsWtpAddressTypeTest, FsWtpImageUpgradeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpImageUpgradeTableINDEX, 1, 0, 0, "1"},

{{13,FsWtpServerIP}, GetNextIndexFsWtpImageUpgradeTable, FsWtpServerIPGet, FsWtpServerIPSet, FsWtpServerIPTest, FsWtpImageUpgradeTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsWtpImageUpgradeTableINDEX, 1, 0, 0, NULL},

{{13,FsWtpRowStatus}, GetNextIndexFsWtpImageUpgradeTable, FsWtpRowStatusGet, FsWtpRowStatusSet, FsWtpRowStatusTest, FsWtpImageUpgradeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpImageUpgradeTableINDEX, 1, 0, 1, NULL},

{{13,FsWlanMulticastMode}, GetNextIndexFsWlanMulticastTable, FsWlanMulticastModeGet, FsWlanMulticastModeSet, FsWlanMulticastModeTest, FsWlanMulticastTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWlanMulticastTableINDEX, 1, 0, 0, "0"},

{{13,FsWlanMulticastSnoopTableLength}, GetNextIndexFsWlanMulticastTable, FsWlanMulticastSnoopTableLengthGet, FsWlanMulticastSnoopTableLengthSet, FsWlanMulticastSnoopTableLengthTest, FsWlanMulticastTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsWlanMulticastTableINDEX, 1, 0, 0, "64"},

{{13,FsWlanMulticastSnoopTimer}, GetNextIndexFsWlanMulticastTable, FsWlanMulticastSnoopTimerGet, FsWlanMulticastSnoopTimerSet, FsWlanMulticastSnoopTimerTest, FsWlanMulticastTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsWlanMulticastTableINDEX, 1, 0, 0, "30000"},

{{13,FsWlanMulticastSnoopTimeout}, GetNextIndexFsWlanMulticastTable, FsWlanMulticastSnoopTimeoutGet, FsWlanMulticastSnoopTimeoutSet, FsWlanMulticastSnoopTimeoutTest, FsWlanMulticastTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsWlanMulticastTableINDEX, 1, 0, 0, "120000"},

{{13,FsWlanBeaconsSentCount}, GetNextIndexFsWlanStatisticsTable, FsWlanBeaconsSentCountGet, FsWlanBeaconsSentCountSet, FsWlanBeaconsSentCountTest, FsWlanStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsWlanStatisticsTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanProbeReqRcvdCount}, GetNextIndexFsWlanStatisticsTable, FsWlanProbeReqRcvdCountGet, FsWlanProbeReqRcvdCountSet, FsWlanProbeReqRcvdCountTest, FsWlanStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsWlanStatisticsTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanProbeRespSentCount}, GetNextIndexFsWlanStatisticsTable, FsWlanProbeRespSentCountGet, FsWlanProbeRespSentCountSet, FsWlanProbeRespSentCountTest, FsWlanStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsWlanStatisticsTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanDataPktRcvdCount}, GetNextIndexFsWlanStatisticsTable, FsWlanDataPktRcvdCountGet, FsWlanDataPktRcvdCountSet, FsWlanDataPktRcvdCountTest, FsWlanStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsWlanStatisticsTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanDataPktSentCount}, GetNextIndexFsWlanStatisticsTable, FsWlanDataPktSentCountGet, FsWlanDataPktSentCountSet, FsWlanDataPktSentCountTest, FsWlanStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsWlanStatisticsTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanSSIDStatsAssocClientCount}, GetNextIndexFsWlanSSIDStatsTable, FsWlanSSIDStatsAssocClientCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsWlanSSIDStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanSSIDStatsMaxClientCount}, GetNextIndexFsWlanSSIDStatsTable, FsWlanSSIDStatsMaxClientCountGet, FsWlanSSIDStatsMaxClientCountSet, FsWlanSSIDStatsMaxClientCountTest, FsWlanSSIDStatsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsWlanSSIDStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanClientStatsMACAddress}, GetNextIndexFsWlanClientStatsTable, FsWlanClientStatsMACAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsWlanClientStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanClientStatsIdleTimeout}, GetNextIndexFsWlanClientStatsTable, FsWlanClientStatsIdleTimeoutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsWlanClientStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanClientStatsBytesReceivedCount}, GetNextIndexFsWlanClientStatsTable, FsWlanClientStatsBytesReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsWlanClientStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanClientStatsBytesSentCount}, GetNextIndexFsWlanClientStatsTable, FsWlanClientStatsBytesSentCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsWlanClientStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanClientStatsAuthState}, GetNextIndexFsWlanClientStatsTable, FsWlanClientStatsAuthStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsWlanClientStatsTableINDEX, 1, 0, 0, "2"},

{{13,FsWlanClientStatsRadiusUserName}, GetNextIndexFsWlanClientStatsTable, FsWlanClientStatsRadiusUserNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsWlanClientStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanClientStatsIpAddressType}, GetNextIndexFsWlanClientStatsTable, FsWlanClientStatsIpAddressTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsWlanClientStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanClientStatsIpAddress}, GetNextIndexFsWlanClientStatsTable, FsWlanClientStatsIpAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsWlanClientStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanClientStatsAssocTime}, GetNextIndexFsWlanClientStatsTable, FsWlanClientStatsAssocTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsWlanClientStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanClientStatsSessionTimeLeft}, GetNextIndexFsWlanClientStatsTable, FsWlanClientStatsSessionTimeLeftGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsWlanClientStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanClientStatsConnTime}, GetNextIndexFsWlanClientStatsTable, FsWlanClientStatsConnTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsWlanClientStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanClientStatsSSID}, GetNextIndexFsWlanClientStatsTable, FsWlanClientStatsSSIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsWlanClientStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanClientStatsToACStatus}, GetNextIndexFsWlanClientStatsTable, FsWlanClientStatsToACStatusGet, FsWlanClientStatsToACStatusSet, FsWlanClientStatsToACStatusTest, FsWlanClientStatsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsWlanClientStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanClientStatsClear}, GetNextIndexFsWlanClientStatsTable, FsWlanClientStatsClearGet, FsWlanClientStatsClearSet, FsWlanClientStatsClearTest, FsWlanClientStatsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWlanClientStatsTableINDEX, 1, 0, 0, "2"},

{{13,FsWlanRadioRadiobaseMACAddress}, GetNextIndexFsWlanRadioTable, FsWlanRadioRadiobaseMACAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsWlanRadioTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanRadioIfInOctets}, GetNextIndexFsWlanRadioTable, FsWlanRadioIfInOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsWlanRadioTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanRadioIfOutOctets}, GetNextIndexFsWlanRadioTable, FsWlanRadioIfOutOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsWlanRadioTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanRadioRadioBand}, GetNextIndexFsWlanRadioTable, FsWlanRadioRadioBandGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsWlanRadioTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanRadioMaxClientCount}, GetNextIndexFsWlanRadioTable, FsWlanRadioMaxClientCountGet, FsWlanRadioMaxClientCountSet, FsWlanRadioMaxClientCountTest, FsWlanRadioTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsWlanRadioTableINDEX, 1, 0, 0, NULL},

{{13,FsWlanRadioClearStats}, GetNextIndexFsWlanRadioTable, FsWlanRadioClearStatsGet, FsWlanRadioClearStatsSet, FsWlanRadioClearStatsTest, FsWlanRadioTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWlanRadioTableINDEX, 1, 0, 0, "2"},

{{13,FsApWlanBeaconsSentCount}, GetNextIndexFsApWlanStatisticsTable, FsApWlanBeaconsSentCountGet, FsApWlanBeaconsSentCountSet, FsApWlanBeaconsSentCountTest, FsApWlanStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsApWlanStatisticsTableINDEX, 2, 0, 0, NULL},

{{13,FsApWlanProbeReqRcvdCount}, GetNextIndexFsApWlanStatisticsTable, FsApWlanProbeReqRcvdCountGet, FsApWlanProbeReqRcvdCountSet, FsApWlanProbeReqRcvdCountTest, FsApWlanStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsApWlanStatisticsTableINDEX, 2, 0, 0, NULL},

{{13,FsApWlanProbeRespSentCount}, GetNextIndexFsApWlanStatisticsTable, FsApWlanProbeRespSentCountGet, FsApWlanProbeRespSentCountSet, FsApWlanProbeRespSentCountTest, FsApWlanStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsApWlanStatisticsTableINDEX, 2, 0, 0, NULL},

{{13,FsApWlanDataPktRcvdCount}, GetNextIndexFsApWlanStatisticsTable, FsApWlanDataPktRcvdCountGet, FsApWlanDataPktRcvdCountSet, FsApWlanDataPktRcvdCountTest, FsApWlanStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsApWlanStatisticsTableINDEX, 2, 0, 0, NULL},

{{13,FsApWlanDataPktSentCount}, GetNextIndexFsApWlanStatisticsTable, FsApWlanDataPktSentCountGet, FsApWlanDataPktSentCountSet, FsApWlanDataPktSentCountTest, FsApWlanStatisticsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsApWlanStatisticsTableINDEX, 2, 0, 0, NULL},

{{12,Fsdot1xAuthFailureStatus}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{12,FsDot11DisconnectionReason}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{12,FsDot11ClientMac}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FsWlanStationTrapStatus}, NULL, FsWlanStationTrapStatusGet, FsWlanStationTrapStatusSet, FsWlanStationTrapStatusTest, FsWlanStationTrapStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{13,FsDot11DscpInPriority}, GetNextIndexFsDot11DscpConfigTable, FsDot11DscpInPriorityGet, FsDot11DscpInPrioritySet, FsDot11DscpInPriorityTest, FsDot11DscpConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11DscpConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsDot11OutDscp}, GetNextIndexFsDot11DscpConfigTable, FsDot11OutDscpGet, FsDot11OutDscpSet, FsDot11OutDscpTest, FsDot11DscpConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11DscpConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsDot11DscpRowStatus}, GetNextIndexFsDot11DscpConfigTable, FsDot11DscpRowStatusGet, FsDot11DscpRowStatusSet, FsDot11DscpRowStatusTest, FsDot11DscpConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11DscpConfigTableINDEX, 2, 0, 1, NULL},

#ifdef BAND_SELECT_WANTED
{{11,FsBandSelectStatus}, NULL, FsBandSelectStatusGet, FsBandSelectStatusSet, FsBandSelectStatusTest, FsBandSelectStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{13,FsBandSelectPerWlanStatus}, GetNextIndexFsWlanBandSelectTable, FsBandSelectPerWlanStatusGet, FsBandSelectPerWlanStatusSet, FsBandSelectPerWlanStatusTest, FsWlanBandSelectTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWlanBandSelectTableINDEX, 1, 0, 0, "2"},

{{13,FsBandSelectAssocCount}, GetNextIndexFsWlanBandSelectTable, FsBandSelectAssocCountGet, FsBandSelectAssocCountSet, FsBandSelectAssocCountTest, FsWlanBandSelectTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsWlanBandSelectTableINDEX, 1, 0, 0, "5"},

{{13,FsBandSelectAgeOutSuppTimer}, GetNextIndexFsWlanBandSelectTable, FsBandSelectAgeOutSuppTimerGet, FsBandSelectAgeOutSuppTimerSet, FsBandSelectAgeOutSuppTimerTest, FsWlanBandSelectTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsWlanBandSelectTableINDEX, 1, 0, 0, "5"},

{{13,FsBandSelectAssocCountFlushTimer}, GetNextIndexFsWlanBandSelectTable, FsBandSelectAssocCountFlushTimerGet, FsBandSelectAssocCountFlushTimerSet, FsBandSelectAssocCountFlushTimerTest, FsWlanBandSelectTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsWlanBandSelectTableINDEX, 1, 0, 0, "5"},

{{13,FsWlanBandSelectClientMacAddress}, GetNextIndexFsWlanBandSelectClientTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsWlanBandSelectClientTableINDEX, 2, 0, 0, NULL},

{{13,FsWlanBandSelectEnabledBSSID}, GetNextIndexFsWlanBandSelectClientTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsWlanBandSelectClientTableINDEX, 2, 0, 0, NULL},

{{13,FsWlanBandSelectStationActiveStatus}, GetNextIndexFsWlanBandSelectClientTable, FsWlanBandSelectStationActiveStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsWlanBandSelectClientTableINDEX, 2, 0, 0, NULL},
#endif

{{11,FsStationDebugOption}, NULL, FsStationDebugOptionGet, FsStationDebugOptionSet, FsStationDebugOptionTest, FsStationDebugOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsWlanDebugOption}, NULL, FsWlanDebugOptionGet, FsWlanDebugOptionSet, FsWlanDebugOptionTest, FsWlanDebugOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsRadioDebugOption}, NULL, FsRadioDebugOptionGet, FsRadioDebugOptionSet, FsRadioDebugOptionTest, FsRadioDebugOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsPmDebugOption}, NULL, FsPmDebugOptionGet, FsPmDebugOptionSet, FsPmDebugOptionTest, FsPmDebugOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsWssIfDebugOption}, NULL, FsWssIfDebugOptionGet, FsWssIfDebugOptionSet, FsWssIfDebugOptionTest, FsWssIfDebugOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsBandSelectLegacyRate}, NULL, FsBandSelectLegacyRateGet, FsBandSelectLegacyRateSet, FsBandSelectLegacyRateTest, FsBandSelectLegacyRateDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
#ifdef BAND_SELECT_WANTED
tMibData fswlanEntry = { 301, fswlanMibEntry };
#else
tMibData fswlanEntry = { 294, fswlanMibEntry };
#endif

#endif /* _FSWLANDB_H */

