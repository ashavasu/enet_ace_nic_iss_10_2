/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fs11acdb.h,v 1.1.1.1 2016/09/20 10:42:39 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FS11ACDB_H
#define _FS11ACDB_H

UINT1 FsDot11ACConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot11VHTStationConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsDot11PhyVHTTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 fs11ac [] ={1,3,6,1,4,1,29601,2,100};
tSNMP_OID_TYPE fs11acOID = {9, fs11ac};


UINT4 FsDot11ACMaxMPDULength [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,1};
UINT4 FsDot11ACMaxMPDULengthConfig [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,2};
UINT4 FsDot11ACVHTMaxRxAMPDUFactor [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,3};
UINT4 FsDot11ACVHTMaxRxAMPDUFactorConfig [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,4};
UINT4 FsDot11ACVHTControlFieldSupported [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,5};
UINT4 FsDot11ACVHTTXOPPowerSaveOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,6};
UINT4 FsDot11ACVHTRxMCSMap [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,7};
UINT4 FsDot11ACVHTRxHighestDataRateSupported [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,8};
UINT4 FsDot11ACVHTTxMCSMap [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,9};
UINT4 FsDot11ACVHTTxHighestDataRateSupported [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,10};
UINT4 FsDot11ACVHTOBSSScanCount [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,11};
UINT4 FsDot11ACCurrentChannelBandwidth [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,12};
UINT4 FsDot11ACCurrentChannelBandwidthConfig [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,13};
UINT4 FsDot11ACCurrentChannelCenterFrequencyIndex0 [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,14};
UINT4 FsDot11ACCurrentChannelCenterFrequencyIndex0Config [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,15};
UINT4 FsDot11ACCurrentChannelCenterFrequencyIndex1 [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,16};
UINT4 FsDot11ACVHTShortGIOptionIn80Implemented [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,17};
UINT4 FsDot11ACVHTShortGIOptionIn80Activated [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,18};
UINT4 FsDot11ACVHTShortGIOptionIn160and80p80Implemented [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,19};
UINT4 FsDot11ACVHTShortGIOptionIn160and80p80Activated [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,20};
UINT4 FsDot11ACVHTLDPCCodingOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,21};
UINT4 FsDot11ACVHTLDPCCodingOptionActivated [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,22};
UINT4 FsDot11ACVHTTxSTBCOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,23};
UINT4 FsDot11ACVHTTxSTBCOptionActivated [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,24};
UINT4 FsDot11ACVHTRxSTBCOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,25};
UINT4 FsDot11ACVHTRxSTBCOptionActivated [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,26};
UINT4 FsDot11ACVHTMUMaxUsersImplemented [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,27};
UINT4 FsDot11ACVHTMUMaxNSTSPerUserImplemented [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,28};
UINT4 FsDot11ACVHTMUMaxNSTSTotalImplemented [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,29};
UINT4 FsDot11ACSuBeamFormer [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,30};
UINT4 FsDot11ACSuBeamFormee [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,31};
UINT4 FsDot11ACMuBeamFormer [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,32};
UINT4 FsDot11ACMuBeamFormee [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,33};
UINT4 FsDot11ACVHTLinkAdaption [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,34};
UINT4 FsDot11ACRxAntennaPattern [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,35};
UINT4 FsDot11ACTxAntennaPattern [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,36};
UINT4 FsDot11ACBasicMCSMap [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,37};
UINT4 FsDot11ACVHTRxMCSMapConfig [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,38};
UINT4 FsDot11ACVHTTxMCSMapConfig [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,39};
UINT4 FsDot11ACCurrentChannelCenterFrequencyIndex1Config [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,40};
UINT4 FsDot11VHTOptionImplemented [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,41};
UINT4 FsDot11OperatingModeNotificationImplemented [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,42};
UINT4 FsDot11ExtendedChannelSwitchActivated [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,43};
UINT4 FsDot11FragmentationThreshold [ ] ={1,3,6,1,4,1,29601,2,100,1,1,1,44};
UINT4 FsDot11VHTRxHighestDataRateConfig [ ] ={1,3,6,1,4,1,29601,2,100,2,1,1,1};
UINT4 FsDot11VHTTxHighestDataRateConfig [ ] ={1,3,6,1,4,1,29601,2,100,2,1,1,2};
UINT4 FsDot11NumberOfSpatialStreamsImplemented [ ] ={1,3,6,1,4,1,29601,2,100,3,1,1,1};
UINT4 FsDot11NumberOfSpatialStreamsActivated [ ] ={1,3,6,1,4,1,29601,2,100,3,1,1,2};




tMbDbEntry fs11acMibEntry[]= {

{{13,FsDot11ACMaxMPDULength}, GetNextIndexFsDot11ACConfigTable, FsDot11ACMaxMPDULengthGet, FsDot11ACMaxMPDULengthSet, FsDot11ACMaxMPDULengthTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "3895"},

{{13,FsDot11ACMaxMPDULengthConfig}, GetNextIndexFsDot11ACConfigTable, FsDot11ACMaxMPDULengthConfigGet, FsDot11ACMaxMPDULengthConfigSet, FsDot11ACMaxMPDULengthConfigTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "3895"},

{{13,FsDot11ACVHTMaxRxAMPDUFactor}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTMaxRxAMPDUFactorGet, FsDot11ACVHTMaxRxAMPDUFactorSet, FsDot11ACVHTMaxRxAMPDUFactorTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11ACVHTMaxRxAMPDUFactorConfig}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTMaxRxAMPDUFactorConfigGet, FsDot11ACVHTMaxRxAMPDUFactorConfigSet, FsDot11ACVHTMaxRxAMPDUFactorConfigTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11ACVHTControlFieldSupported}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTControlFieldSupportedGet, FsDot11ACVHTControlFieldSupportedSet, FsDot11ACVHTControlFieldSupportedTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11ACVHTTXOPPowerSaveOptionImplemented}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTTXOPPowerSaveOptionImplementedGet, FsDot11ACVHTTXOPPowerSaveOptionImplementedSet, FsDot11ACVHTTXOPPowerSaveOptionImplementedTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11ACVHTRxMCSMap}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTRxMCSMapGet, FsDot11ACVHTRxMCSMapSet, FsDot11ACVHTRxMCSMapTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11ACVHTRxHighestDataRateSupported}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTRxHighestDataRateSupportedGet, FsDot11ACVHTRxHighestDataRateSupportedSet, FsDot11ACVHTRxHighestDataRateSupportedTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11ACVHTTxMCSMap}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTTxMCSMapGet, FsDot11ACVHTTxMCSMapSet, FsDot11ACVHTTxMCSMapTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11ACVHTTxHighestDataRateSupported}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTTxHighestDataRateSupportedGet, FsDot11ACVHTTxHighestDataRateSupportedSet, FsDot11ACVHTTxHighestDataRateSupportedTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11ACVHTOBSSScanCount}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTOBSSScanCountGet, FsDot11ACVHTOBSSScanCountSet, FsDot11ACVHTOBSSScanCountTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "3"},

{{13,FsDot11ACCurrentChannelBandwidth}, GetNextIndexFsDot11ACConfigTable, FsDot11ACCurrentChannelBandwidthGet, FsDot11ACCurrentChannelBandwidthSet, FsDot11ACCurrentChannelBandwidthTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11ACCurrentChannelBandwidthConfig}, GetNextIndexFsDot11ACConfigTable, FsDot11ACCurrentChannelBandwidthConfigGet, FsDot11ACCurrentChannelBandwidthConfigSet, FsDot11ACCurrentChannelBandwidthConfigTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11ACCurrentChannelCenterFrequencyIndex0}, GetNextIndexFsDot11ACConfigTable, FsDot11ACCurrentChannelCenterFrequencyIndex0Get, FsDot11ACCurrentChannelCenterFrequencyIndex0Set, FsDot11ACCurrentChannelCenterFrequencyIndex0Test, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11ACCurrentChannelCenterFrequencyIndex0Config}, GetNextIndexFsDot11ACConfigTable, FsDot11ACCurrentChannelCenterFrequencyIndex0ConfigGet, FsDot11ACCurrentChannelCenterFrequencyIndex0ConfigSet, FsDot11ACCurrentChannelCenterFrequencyIndex0ConfigTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11ACCurrentChannelCenterFrequencyIndex1}, GetNextIndexFsDot11ACConfigTable, FsDot11ACCurrentChannelCenterFrequencyIndex1Get, FsDot11ACCurrentChannelCenterFrequencyIndex1Set, FsDot11ACCurrentChannelCenterFrequencyIndex1Test, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11ACVHTShortGIOptionIn80Implemented}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTShortGIOptionIn80ImplementedGet, FsDot11ACVHTShortGIOptionIn80ImplementedSet, FsDot11ACVHTShortGIOptionIn80ImplementedTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11ACVHTShortGIOptionIn80Activated}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTShortGIOptionIn80ActivatedGet, FsDot11ACVHTShortGIOptionIn80ActivatedSet, FsDot11ACVHTShortGIOptionIn80ActivatedTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11ACVHTShortGIOptionIn160and80p80Implemented}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTShortGIOptionIn160and80p80ImplementedGet, FsDot11ACVHTShortGIOptionIn160and80p80ImplementedSet, FsDot11ACVHTShortGIOptionIn160and80p80ImplementedTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11ACVHTShortGIOptionIn160and80p80Activated}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTShortGIOptionIn160and80p80ActivatedGet, FsDot11ACVHTShortGIOptionIn160and80p80ActivatedSet, FsDot11ACVHTShortGIOptionIn160and80p80ActivatedTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11ACVHTLDPCCodingOptionImplemented}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTLDPCCodingOptionImplementedGet, FsDot11ACVHTLDPCCodingOptionImplementedSet, FsDot11ACVHTLDPCCodingOptionImplementedTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11ACVHTLDPCCodingOptionActivated}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTLDPCCodingOptionActivatedGet, FsDot11ACVHTLDPCCodingOptionActivatedSet, FsDot11ACVHTLDPCCodingOptionActivatedTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11ACVHTTxSTBCOptionImplemented}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTTxSTBCOptionImplementedGet, FsDot11ACVHTTxSTBCOptionImplementedSet, FsDot11ACVHTTxSTBCOptionImplementedTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11ACVHTTxSTBCOptionActivated}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTTxSTBCOptionActivatedGet, FsDot11ACVHTTxSTBCOptionActivatedSet, FsDot11ACVHTTxSTBCOptionActivatedTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11ACVHTRxSTBCOptionImplemented}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTRxSTBCOptionImplementedGet, FsDot11ACVHTRxSTBCOptionImplementedSet, FsDot11ACVHTRxSTBCOptionImplementedTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11ACVHTRxSTBCOptionActivated}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTRxSTBCOptionActivatedGet, FsDot11ACVHTRxSTBCOptionActivatedSet, FsDot11ACVHTRxSTBCOptionActivatedTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11ACVHTMUMaxUsersImplemented}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTMUMaxUsersImplementedGet, FsDot11ACVHTMUMaxUsersImplementedSet, FsDot11ACVHTMUMaxUsersImplementedTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11ACVHTMUMaxNSTSPerUserImplemented}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTMUMaxNSTSPerUserImplementedGet, FsDot11ACVHTMUMaxNSTSPerUserImplementedSet, FsDot11ACVHTMUMaxNSTSPerUserImplementedTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11ACVHTMUMaxNSTSTotalImplemented}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTMUMaxNSTSTotalImplementedGet, FsDot11ACVHTMUMaxNSTSTotalImplementedSet, FsDot11ACVHTMUMaxNSTSTotalImplementedTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11ACSuBeamFormer}, GetNextIndexFsDot11ACConfigTable, FsDot11ACSuBeamFormerGet, FsDot11ACSuBeamFormerSet, FsDot11ACSuBeamFormerTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11ACSuBeamFormee}, GetNextIndexFsDot11ACConfigTable, FsDot11ACSuBeamFormeeGet, FsDot11ACSuBeamFormeeSet, FsDot11ACSuBeamFormeeTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11ACMuBeamFormer}, GetNextIndexFsDot11ACConfigTable, FsDot11ACMuBeamFormerGet, FsDot11ACMuBeamFormerSet, FsDot11ACMuBeamFormerTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11ACMuBeamFormee}, GetNextIndexFsDot11ACConfigTable, FsDot11ACMuBeamFormeeGet, FsDot11ACMuBeamFormeeSet, FsDot11ACMuBeamFormeeTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11ACVHTLinkAdaption}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTLinkAdaptionGet, FsDot11ACVHTLinkAdaptionSet, FsDot11ACVHTLinkAdaptionTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11ACRxAntennaPattern}, GetNextIndexFsDot11ACConfigTable, FsDot11ACRxAntennaPatternGet, FsDot11ACRxAntennaPatternSet, FsDot11ACRxAntennaPatternTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11ACTxAntennaPattern}, GetNextIndexFsDot11ACConfigTable, FsDot11ACTxAntennaPatternGet, FsDot11ACTxAntennaPatternSet, FsDot11ACTxAntennaPatternTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsDot11ACBasicMCSMap}, GetNextIndexFsDot11ACConfigTable, FsDot11ACBasicMCSMapGet, FsDot11ACBasicMCSMapSet, FsDot11ACBasicMCSMapTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11ACVHTRxMCSMapConfig}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTRxMCSMapConfigGet, FsDot11ACVHTRxMCSMapConfigSet, FsDot11ACVHTRxMCSMapConfigTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11ACVHTTxMCSMapConfig}, GetNextIndexFsDot11ACConfigTable, FsDot11ACVHTTxMCSMapConfigGet, FsDot11ACVHTTxMCSMapConfigSet, FsDot11ACVHTTxMCSMapConfigTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11ACCurrentChannelCenterFrequencyIndex1Config}, GetNextIndexFsDot11ACConfigTable, FsDot11ACCurrentChannelCenterFrequencyIndex1ConfigGet, FsDot11ACCurrentChannelCenterFrequencyIndex1ConfigSet, FsDot11ACCurrentChannelCenterFrequencyIndex1ConfigTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11VHTOptionImplemented}, GetNextIndexFsDot11ACConfigTable, FsDot11VHTOptionImplementedGet, FsDot11VHTOptionImplementedSet, FsDot11VHTOptionImplementedTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11OperatingModeNotificationImplemented}, GetNextIndexFsDot11ACConfigTable, FsDot11OperatingModeNotificationImplementedGet, FsDot11OperatingModeNotificationImplementedSet, FsDot11OperatingModeNotificationImplementedTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11ExtendedChannelSwitchActivated}, GetNextIndexFsDot11ACConfigTable, FsDot11ExtendedChannelSwitchActivatedGet, FsDot11ExtendedChannelSwitchActivatedSet, FsDot11ExtendedChannelSwitchActivatedTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsDot11FragmentationThreshold}, GetNextIndexFsDot11ACConfigTable, FsDot11FragmentationThresholdGet, FsDot11FragmentationThresholdSet, FsDot11FragmentationThresholdTest, FsDot11ACConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11ACConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsDot11VHTRxHighestDataRateConfig}, GetNextIndexFsDot11VHTStationConfigTable, FsDot11VHTRxHighestDataRateConfigGet, FsDot11VHTRxHighestDataRateConfigSet, FsDot11VHTRxHighestDataRateConfigTest, FsDot11VHTStationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11VHTStationConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11VHTTxHighestDataRateConfig}, GetNextIndexFsDot11VHTStationConfigTable, FsDot11VHTTxHighestDataRateConfigGet, FsDot11VHTTxHighestDataRateConfigSet, FsDot11VHTTxHighestDataRateConfigTest, FsDot11VHTStationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11VHTStationConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsDot11NumberOfSpatialStreamsImplemented}, GetNextIndexFsDot11PhyVHTTable, FsDot11NumberOfSpatialStreamsImplementedGet, FsDot11NumberOfSpatialStreamsImplementedSet, FsDot11NumberOfSpatialStreamsImplementedTest, FsDot11PhyVHTTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11PhyVHTTableINDEX, 1, 0, 0, "3"},

{{13,FsDot11NumberOfSpatialStreamsActivated}, GetNextIndexFsDot11PhyVHTTable, FsDot11NumberOfSpatialStreamsActivatedGet, FsDot11NumberOfSpatialStreamsActivatedSet, FsDot11NumberOfSpatialStreamsActivatedTest, FsDot11PhyVHTTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsDot11PhyVHTTableINDEX, 1, 0, 0, "3"},
};
tMibData fs11acEntry = { 48, fs11acMibEntry };

#endif /* _FS11ACDB_H */

