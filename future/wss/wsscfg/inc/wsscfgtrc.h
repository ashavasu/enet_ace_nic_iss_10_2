/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved 
 * 
 * $Id: wsscfgtrc.h,v 1.1.1.1 2016/09/20 10:42:39 siva Exp $ 
 *
 * Description:This file contains procedures and definitions 
 *             used for debugging.
 *******************************************************************/


#ifndef __WSSCFGTRC_H__
#define __WSSCFGTRC_H__

#define  WSSCFG_TRC_FLAG  gWsscfgGlobals.u4WsscfgTrc
#define  WSSCFG_NAME      "WSSCFG"

#ifdef WSSCFG_TRACE_WANTED



#define  WSSCFG_TRC(x)       WsscfgTrcPrint( __FILE__, __LINE__, WsscfgTrc x)
#define  WSSCFG_TRC_FUNC(x)  WsscfgTrcPrint( __FILE__, __LINE__, WsscfgTrc x)
#define  WSSCFG_TRC_CRIT(x)  WsscfgTrcPrint( __FILE__, __LINE__, WsscfgTrc x)
#define  WSSCFG_TRC_PKT(x)   WsscfgTrcWrite( WsscfgTrc x)




#else                           /* WSSCFG_TRACE_WANTED */

#define  WSSCFG_TRC(x)
#define  WSSCFG_TRC_FUNC(x)
#define  WSSCFG_TRC_CRIT(x)
#define  WSSCFG_TRC_PKT(x)

#endif                          /* WSSCFG_TRACE_WANTED */


#define  WSSCFG_FN_ENTRY  (0x1 << 9)
#define  WSSCFG_FN_EXIT   (0x1 << 10)
#define  WSSCFG_CLI_TRC   (0x1 << 11)
#define  WSSCFG_MAIN_TRC  (0x1 << 12)
#define  WSSCFG_PKT_TRC   (0x1 << 13)
#define  WSSCFG_QUE_TRC   (0x1 << 14)
#define  WSSCFG_TASK_TRC  (0x1 << 15)
#define  WSSCFG_TMR_TRC   (0x1 << 16)
#define  WSSCFG_UTIL_TRC  (0x1 << 17)
#define  WSSCFG_ALL_TRC   WSSCFG_FN_ENTRY |\
                        WSSCFG_FN_EXIT  |\
                        WSSCFG_CLI_TRC  |\
                        WSSCFG_MAIN_TRC |\
                        WSSCFG_PKT_TRC  |\
                        WSSCFG_QUE_TRC  |\
                        WSSCFG_TASK_TRC |\
                        WSSCFG_TMR_TRC  |\
                        WSSCFG_UTIL_TRC


#endif                          /* _WSSCFGTRC_H */


/*-----------------------------------------------------------------------*/
/*                       End of the file  wsscfgtrc.h                      */
/*-----------------------------------------------------------------------*/
