/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std802db.h,v 1.2 2017/05/23 14:16:55 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STD802DB_H
#define _STD802DB_H

UINT1 Dot11StationConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11AuthenticationAlgorithmsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11WEPDefaultKeysTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11WEPKeyMappingsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11PrivacyTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11MultiDomainCapabilityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11SpectrumManagementTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11RSNAConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11RSNAConfigPairwiseCiphersTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11RSNAConfigAuthenticationSuitesTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11RSNAStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11OperatingClassesTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11FastBSSTransitionConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11LCIDSETableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11HTStationConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11WirelessMgmtOptionsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11LocationServicesTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11WirelessMGTEventTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11RMRequestTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11ChannelLoadReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11NoiseHistogramReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11BeaconReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11FrameReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11STAStatisticsReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11LCIReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11TransmitStreamReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11APChannelReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11RMNeighborReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11WNMRequestTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11WNMVendorSpecificReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11WNMMulticastDiagnosticReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11WNMLocationCivicReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11WNMLocationIdentifierReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11WNMEventTransitReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11WNMEventRsnaReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11WNMEventPeerReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11WNMEventWNMLogReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11WNMDiagMfrInfoReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11WNMDiagConfigProfReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11WNMDiagAssocReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11WNMDiag8021xAuthReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11WNMLocConfigReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11WNMBssTransitReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11WNMColocInterfReportTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11MeshSTAConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11MeshHWMPConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11RSNAConfigPasswordValueTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11RSNAConfigDLCGroupTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11OperationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11CountersTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11GroupAddressesTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11EDCATableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11QAPEDCATableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11QosCountersTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11ResourceInfoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11PhyOperationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11PhyAntennaTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11PhyTxPowerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11PhyFHSSTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11PhyDSSSTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11PhyIRTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11RegDomainsSupportedTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11AntennasListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11SupportedDataRatesTxTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11SupportedDataRatesRxTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11PhyOFDMTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11PhyHRDSSSTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11HoppingPatternTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11PhyERPTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11PhyHTTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11SupportedMCSTxTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11SupportedMCSRxTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11TransmitBeamformingConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11BSSIdTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 Dot11InterworkingTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 Dot11APLCITableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11APCivicLocationTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11RoamingConsortiumTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 Dot11DomainNameTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Dot11GASAdvertisementTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11MACStateConfigTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 Dot11MACStateParameterTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 Dot11MACStateESSLinkDetectedTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};

UINT4 std802 [] ={1,2,840,10036};
tSNMP_OID_TYPE std802OID = {4, std802};


/* Generated OID's for tables */
UINT4 Dot11StationConfigTable [] ={1,2,840,10036,1,1};
tSNMP_OID_TYPE Dot11StationConfigTableOID = {6, Dot11StationConfigTable};


UINT4 Dot11AuthenticationAlgorithmsTable [] ={1,2,840,10036,1,2};
tSNMP_OID_TYPE Dot11AuthenticationAlgorithmsTableOID = {6, Dot11AuthenticationAlgorithmsTable};


UINT4 Dot11WEPDefaultKeysTable [] ={1,2,840,10036,1,3};
tSNMP_OID_TYPE Dot11WEPDefaultKeysTableOID = {6, Dot11WEPDefaultKeysTable};


UINT4 Dot11WEPKeyMappingsTable [] ={1,2,840,10036,1,4};
tSNMP_OID_TYPE Dot11WEPKeyMappingsTableOID = {6, Dot11WEPKeyMappingsTable};


UINT4 Dot11PrivacyTable [] ={1,2,840,10036,1,5};
tSNMP_OID_TYPE Dot11PrivacyTableOID = {6, Dot11PrivacyTable};


UINT4 Dot11MultiDomainCapabilityTable [] ={1,2,840,10036,1,7};
tSNMP_OID_TYPE Dot11MultiDomainCapabilityTableOID = {6, Dot11MultiDomainCapabilityTable};


UINT4 Dot11SpectrumManagementTable [] ={1,2,840,10036,1,8};
tSNMP_OID_TYPE Dot11SpectrumManagementTableOID = {6, Dot11SpectrumManagementTable};


UINT4 Dot11RSNAConfigTable [] ={1,2,840,10036,1,9};
tSNMP_OID_TYPE Dot11RSNAConfigTableOID = {6, Dot11RSNAConfigTable};


UINT4 Dot11RSNAConfigPairwiseCiphersTable [] ={1,2,840,10036,1,10};
tSNMP_OID_TYPE Dot11RSNAConfigPairwiseCiphersTableOID = {6, Dot11RSNAConfigPairwiseCiphersTable};


UINT4 Dot11RSNAConfigAuthenticationSuitesTable [] ={1,2,840,10036,1,11};
tSNMP_OID_TYPE Dot11RSNAConfigAuthenticationSuitesTableOID = {6, Dot11RSNAConfigAuthenticationSuitesTable};


UINT4 Dot11RSNAStatsTable [] ={1,2,840,10036,1,12};
tSNMP_OID_TYPE Dot11RSNAStatsTableOID = {6, Dot11RSNAStatsTable};


UINT4 Dot11OperatingClassesTable [] ={1,2,840,10036,1,13};
tSNMP_OID_TYPE Dot11OperatingClassesTableOID = {6, Dot11OperatingClassesTable};


UINT4 Dot11RMRequestTable [] ={1,2,840,10036,1,14,1,2};
tSNMP_OID_TYPE Dot11RMRequestTableOID = {8, Dot11RMRequestTable};


UINT4 Dot11ChannelLoadReportTable [] ={1,2,840,10036,1,14,2,1};
tSNMP_OID_TYPE Dot11ChannelLoadReportTableOID = {8, Dot11ChannelLoadReportTable};


UINT4 Dot11NoiseHistogramReportTable [] ={1,2,840,10036,1,14,2,2};
tSNMP_OID_TYPE Dot11NoiseHistogramReportTableOID = {8, Dot11NoiseHistogramReportTable};


UINT4 Dot11BeaconReportTable [] ={1,2,840,10036,1,14,2,3};
tSNMP_OID_TYPE Dot11BeaconReportTableOID = {8, Dot11BeaconReportTable};


UINT4 Dot11FrameReportTable [] ={1,2,840,10036,1,14,2,4};
tSNMP_OID_TYPE Dot11FrameReportTableOID = {8, Dot11FrameReportTable};


UINT4 Dot11STAStatisticsReportTable [] ={1,2,840,10036,1,14,2,5};
tSNMP_OID_TYPE Dot11STAStatisticsReportTableOID = {8, Dot11STAStatisticsReportTable};


UINT4 Dot11LCIReportTable [] ={1,2,840,10036,1,14,2,6};
tSNMP_OID_TYPE Dot11LCIReportTableOID = {8, Dot11LCIReportTable};


UINT4 Dot11TransmitStreamReportTable [] ={1,2,840,10036,1,14,2,7};
tSNMP_OID_TYPE Dot11TransmitStreamReportTableOID = {8, Dot11TransmitStreamReportTable};


UINT4 Dot11APChannelReportTable [] ={1,2,840,10036,1,14,3,1};
tSNMP_OID_TYPE Dot11APChannelReportTableOID = {8, Dot11APChannelReportTable};


UINT4 Dot11RMNeighborReportTable [] ={1,2,840,10036,1,14,3,3};
tSNMP_OID_TYPE Dot11RMNeighborReportTableOID = {8, Dot11RMNeighborReportTable};


UINT4 Dot11FastBSSTransitionConfigTable [] ={1,2,840,10036,1,15};
tSNMP_OID_TYPE Dot11FastBSSTransitionConfigTableOID = {6, Dot11FastBSSTransitionConfigTable};


UINT4 Dot11LCIDSETable [] ={1,2,840,10036,1,16};
tSNMP_OID_TYPE Dot11LCIDSETableOID = {6, Dot11LCIDSETable};


UINT4 Dot11HTStationConfigTable [] ={1,2,840,10036,1,17};
tSNMP_OID_TYPE Dot11HTStationConfigTableOID = {6, Dot11HTStationConfigTable};


UINT4 Dot11WirelessMgmtOptionsTable [] ={1,2,840,10036,1,18};
tSNMP_OID_TYPE Dot11WirelessMgmtOptionsTableOID = {6, Dot11WirelessMgmtOptionsTable};


UINT4 Dot11LocationServicesTable [] ={1,2,840,10036,1,20};
tSNMP_OID_TYPE Dot11LocationServicesTableOID = {6, Dot11LocationServicesTable};


UINT4 Dot11WirelessMGTEventTable [] ={1,2,840,10036,1,21};
tSNMP_OID_TYPE Dot11WirelessMGTEventTableOID = {6, Dot11WirelessMGTEventTable};


UINT4 Dot11WNMRequestTable [] ={1,2,840,10036,1,22,1,2};
tSNMP_OID_TYPE Dot11WNMRequestTableOID = {8, Dot11WNMRequestTable};


UINT4 Dot11WNMVendorSpecificReportTable [] ={1,2,840,10036,1,22,2,1};
tSNMP_OID_TYPE Dot11WNMVendorSpecificReportTableOID = {8, Dot11WNMVendorSpecificReportTable};


UINT4 Dot11WNMMulticastDiagnosticReportTable [] ={1,2,840,10036,1,22,2,2};
tSNMP_OID_TYPE Dot11WNMMulticastDiagnosticReportTableOID = {8, Dot11WNMMulticastDiagnosticReportTable};


UINT4 Dot11WNMLocationCivicReportTable [] ={1,2,840,10036,1,22,2,3};
tSNMP_OID_TYPE Dot11WNMLocationCivicReportTableOID = {8, Dot11WNMLocationCivicReportTable};


UINT4 Dot11WNMLocationIdentifierReportTable [] ={1,2,840,10036,1,22,2,4};
tSNMP_OID_TYPE Dot11WNMLocationIdentifierReportTableOID = {8, Dot11WNMLocationIdentifierReportTable};


UINT4 Dot11WNMEventTransitReportTable [] ={1,2,840,10036,1,22,2,5};
tSNMP_OID_TYPE Dot11WNMEventTransitReportTableOID = {8, Dot11WNMEventTransitReportTable};


UINT4 Dot11WNMEventRsnaReportTable [] ={1,2,840,10036,1,22,2,6};
tSNMP_OID_TYPE Dot11WNMEventRsnaReportTableOID = {8, Dot11WNMEventRsnaReportTable};


UINT4 Dot11WNMEventPeerReportTable [] ={1,2,840,10036,1,22,2,7};
tSNMP_OID_TYPE Dot11WNMEventPeerReportTableOID = {8, Dot11WNMEventPeerReportTable};


UINT4 Dot11WNMEventWNMLogReportTable [] ={1,2,840,10036,1,22,2,8};
tSNMP_OID_TYPE Dot11WNMEventWNMLogReportTableOID = {8, Dot11WNMEventWNMLogReportTable};


UINT4 Dot11WNMDiagMfrInfoReportTable [] ={1,2,840,10036,1,22,2,9};
tSNMP_OID_TYPE Dot11WNMDiagMfrInfoReportTableOID = {8, Dot11WNMDiagMfrInfoReportTable};


UINT4 Dot11WNMDiagConfigProfReportTable [] ={1,2,840,10036,1,22,2,10};
tSNMP_OID_TYPE Dot11WNMDiagConfigProfReportTableOID = {8, Dot11WNMDiagConfigProfReportTable};


UINT4 Dot11WNMDiagAssocReportTable [] ={1,2,840,10036,1,22,2,11};
tSNMP_OID_TYPE Dot11WNMDiagAssocReportTableOID = {8, Dot11WNMDiagAssocReportTable};


UINT4 Dot11WNMDiag8021xAuthReportTable [] ={1,2,840,10036,1,22,2,12};
tSNMP_OID_TYPE Dot11WNMDiag8021xAuthReportTableOID = {8, Dot11WNMDiag8021xAuthReportTable};


UINT4 Dot11WNMLocConfigReportTable [] ={1,2,840,10036,1,22,2,13};
tSNMP_OID_TYPE Dot11WNMLocConfigReportTableOID = {8, Dot11WNMLocConfigReportTable};


UINT4 Dot11WNMBssTransitReportTable [] ={1,2,840,10036,1,22,2,14};
tSNMP_OID_TYPE Dot11WNMBssTransitReportTableOID = {8, Dot11WNMBssTransitReportTable};


UINT4 Dot11WNMColocInterfReportTable [] ={1,2,840,10036,1,22,2,16};
tSNMP_OID_TYPE Dot11WNMColocInterfReportTableOID = {8, Dot11WNMColocInterfReportTable};


UINT4 Dot11MeshSTAConfigTable [] ={1,2,840,10036,1,23};
tSNMP_OID_TYPE Dot11MeshSTAConfigTableOID = {6, Dot11MeshSTAConfigTable};


UINT4 Dot11MeshHWMPConfigTable [] ={1,2,840,10036,1,24};
tSNMP_OID_TYPE Dot11MeshHWMPConfigTableOID = {6, Dot11MeshHWMPConfigTable};


UINT4 Dot11RSNAConfigPasswordValueTable [] ={1,2,840,10036,1,25};
tSNMP_OID_TYPE Dot11RSNAConfigPasswordValueTableOID = {6, Dot11RSNAConfigPasswordValueTable};


UINT4 Dot11RSNAConfigDLCGroupTable [] ={1,2,840,10036,1,26};
tSNMP_OID_TYPE Dot11RSNAConfigDLCGroupTableOID = {6, Dot11RSNAConfigDLCGroupTable};


UINT4 Dot11OperationTable [] ={1,2,840,10036,2,1};
tSNMP_OID_TYPE Dot11OperationTableOID = {6, Dot11OperationTable};


UINT4 Dot11CountersTable [] ={1,2,840,10036,2,2};
tSNMP_OID_TYPE Dot11CountersTableOID = {6, Dot11CountersTable};


UINT4 Dot11GroupAddressesTable [] ={1,2,840,10036,2,3};
tSNMP_OID_TYPE Dot11GroupAddressesTableOID = {6, Dot11GroupAddressesTable};


UINT4 Dot11EDCATable [] ={1,2,840,10036,2,4};
tSNMP_OID_TYPE Dot11EDCATableOID = {6, Dot11EDCATable};


UINT4 Dot11QAPEDCATable [] ={1,2,840,10036,2,5};
tSNMP_OID_TYPE Dot11QAPEDCATableOID = {6, Dot11QAPEDCATable};


UINT4 Dot11QosCountersTable [] ={1,2,840,10036,2,6};
tSNMP_OID_TYPE Dot11QosCountersTableOID = {6, Dot11QosCountersTable};


UINT4 Dot11ResourceInfoTable [] ={1,2,840,10036,3,1,2};
tSNMP_OID_TYPE Dot11ResourceInfoTableOID = {7, Dot11ResourceInfoTable};


UINT4 Dot11PhyOperationTable [] ={1,2,840,10036,4,1};
tSNMP_OID_TYPE Dot11PhyOperationTableOID = {6, Dot11PhyOperationTable};


UINT4 Dot11PhyAntennaTable [] ={1,2,840,10036,4,2};
tSNMP_OID_TYPE Dot11PhyAntennaTableOID = {6, Dot11PhyAntennaTable};


UINT4 Dot11PhyTxPowerTable [] ={1,2,840,10036,4,3};
tSNMP_OID_TYPE Dot11PhyTxPowerTableOID = {6, Dot11PhyTxPowerTable};


UINT4 Dot11PhyFHSSTable [] ={1,2,840,10036,4,4};
tSNMP_OID_TYPE Dot11PhyFHSSTableOID = {6, Dot11PhyFHSSTable};


UINT4 Dot11PhyDSSSTable [] ={1,2,840,10036,4,5};
tSNMP_OID_TYPE Dot11PhyDSSSTableOID = {6, Dot11PhyDSSSTable};


UINT4 Dot11PhyIRTable [] ={1,2,840,10036,4,6};
tSNMP_OID_TYPE Dot11PhyIRTableOID = {6, Dot11PhyIRTable};


UINT4 Dot11RegDomainsSupportedTable [] ={1,2,840,10036,4,7};
tSNMP_OID_TYPE Dot11RegDomainsSupportedTableOID = {6, Dot11RegDomainsSupportedTable};


UINT4 Dot11AntennasListTable [] ={1,2,840,10036,4,8};
tSNMP_OID_TYPE Dot11AntennasListTableOID = {6, Dot11AntennasListTable};


UINT4 Dot11SupportedDataRatesTxTable [] ={1,2,840,10036,4,9};
tSNMP_OID_TYPE Dot11SupportedDataRatesTxTableOID = {6, Dot11SupportedDataRatesTxTable};


UINT4 Dot11SupportedDataRatesRxTable [] ={1,2,840,10036,4,10};
tSNMP_OID_TYPE Dot11SupportedDataRatesRxTableOID = {6, Dot11SupportedDataRatesRxTable};


UINT4 Dot11PhyOFDMTable [] ={1,2,840,10036,4,11};
tSNMP_OID_TYPE Dot11PhyOFDMTableOID = {6, Dot11PhyOFDMTable};


UINT4 Dot11PhyHRDSSSTable [] ={1,2,840,10036,4,12};
tSNMP_OID_TYPE Dot11PhyHRDSSSTableOID = {6, Dot11PhyHRDSSSTable};


UINT4 Dot11HoppingPatternTable [] ={1,2,840,10036,4,13};
tSNMP_OID_TYPE Dot11HoppingPatternTableOID = {6, Dot11HoppingPatternTable};


UINT4 Dot11PhyERPTable [] ={1,2,840,10036,4,14};
tSNMP_OID_TYPE Dot11PhyERPTableOID = {6, Dot11PhyERPTable};


UINT4 Dot11PhyHTTable [] ={1,2,840,10036,4,15};
tSNMP_OID_TYPE Dot11PhyHTTableOID = {6, Dot11PhyHTTable};


UINT4 Dot11SupportedMCSTxTable [] ={1,2,840,10036,4,16};
tSNMP_OID_TYPE Dot11SupportedMCSTxTableOID = {6, Dot11SupportedMCSTxTable};


UINT4 Dot11SupportedMCSRxTable [] ={1,2,840,10036,4,17};
tSNMP_OID_TYPE Dot11SupportedMCSRxTableOID = {6, Dot11SupportedMCSRxTable};


UINT4 Dot11TransmitBeamformingConfigTable [] ={1,2,840,10036,4,18};
tSNMP_OID_TYPE Dot11TransmitBeamformingConfigTableOID = {6, Dot11TransmitBeamformingConfigTable};


UINT4 Dot11BSSIdTable [] ={1,2,840,10036,6,1};
tSNMP_OID_TYPE Dot11BSSIdTableOID = {6, Dot11BSSIdTable};


UINT4 Dot11InterworkingTable [] ={1,2,840,10036,6,2};
tSNMP_OID_TYPE Dot11InterworkingTableOID = {6, Dot11InterworkingTable};


UINT4 Dot11APLCITable [] ={1,2,840,10036,6,3};
tSNMP_OID_TYPE Dot11APLCITableOID = {6, Dot11APLCITable};


UINT4 Dot11APCivicLocationTable [] ={1,2,840,10036,6,4};
tSNMP_OID_TYPE Dot11APCivicLocationTableOID = {6, Dot11APCivicLocationTable};


UINT4 Dot11RoamingConsortiumTable [] ={1,2,840,10036,6,5};
tSNMP_OID_TYPE Dot11RoamingConsortiumTableOID = {6, Dot11RoamingConsortiumTable};


UINT4 Dot11DomainNameTable [] ={1,2,840,10036,6,6};
tSNMP_OID_TYPE Dot11DomainNameTableOID = {6, Dot11DomainNameTable};


UINT4 Dot11GASAdvertisementTable [] ={1,2,840,10036,6,7};
tSNMP_OID_TYPE Dot11GASAdvertisementTableOID = {6, Dot11GASAdvertisementTable};


UINT4 Dot11MACStateConfigTable [] ={1,2,840,10036,7,1};
tSNMP_OID_TYPE Dot11MACStateConfigTableOID = {6, Dot11MACStateConfigTable};


UINT4 Dot11MACStateParameterTable [] ={1,2,840,10036,7,2};
tSNMP_OID_TYPE Dot11MACStateParameterTableOID = {6, Dot11MACStateParameterTable};


UINT4 Dot11MACStateESSLinkDetectedTable [] ={1,2,840,10036,7,3};
tSNMP_OID_TYPE Dot11MACStateESSLinkDetectedTableOID = {6, Dot11MACStateESSLinkDetectedTable};




UINT4 Dot11StationID [ ] ={1,2,840,10036,1,1,1,1};
UINT4 Dot11MediumOccupancyLimit [ ] ={1,2,840,10036,1,1,1,2};
UINT4 Dot11CFPollable [ ] ={1,2,840,10036,1,1,1,3};
UINT4 Dot11CFPPeriod [ ] ={1,2,840,10036,1,1,1,4};
UINT4 Dot11CFPMaxDuration [ ] ={1,2,840,10036,1,1,1,5};
UINT4 Dot11AuthenticationResponseTimeOut [ ] ={1,2,840,10036,1,1,1,6};
UINT4 Dot11PrivacyOptionImplemented [ ] ={1,2,840,10036,1,1,1,7};
UINT4 Dot11PowerManagementMode [ ] ={1,2,840,10036,1,1,1,8};
UINT4 Dot11DesiredSSID [ ] ={1,2,840,10036,1,1,1,9};
UINT4 Dot11DesiredBSSType [ ] ={1,2,840,10036,1,1,1,10};
UINT4 Dot11OperationalRateSet [ ] ={1,2,840,10036,1,1,1,11};
UINT4 Dot11BeaconPeriod [ ] ={1,2,840,10036,1,1,1,12};
UINT4 Dot11DTIMPeriod [ ] ={1,2,840,10036,1,1,1,13};
UINT4 Dot11AssociationResponseTimeOut [ ] ={1,2,840,10036,1,1,1,14};
UINT4 Dot11DisassociateReason [ ] ={1,2,840,10036,1,1,1,15};
UINT4 Dot11DisassociateStation [ ] ={1,2,840,10036,1,1,1,16};
UINT4 Dot11DeauthenticateReason [ ] ={1,2,840,10036,1,1,1,17};
UINT4 Dot11DeauthenticateStation [ ] ={1,2,840,10036,1,1,1,18};
UINT4 Dot11AuthenticateFailStatus [ ] ={1,2,840,10036,1,1,1,19};
UINT4 Dot11AuthenticateFailStation [ ] ={1,2,840,10036,1,1,1,20};
UINT4 Dot11MultiDomainCapabilityImplemented [ ] ={1,2,840,10036,1,1,1,21};
UINT4 Dot11MultiDomainCapabilityActivated [ ] ={1,2,840,10036,1,1,1,22};
UINT4 Dot11CountryString [ ] ={1,2,840,10036,1,1,1,23};
UINT4 Dot11SpectrumManagementImplemented [ ] ={1,2,840,10036,1,1,1,24};
UINT4 Dot11SpectrumManagementRequired [ ] ={1,2,840,10036,1,1,1,25};
UINT4 Dot11RSNAOptionImplemented [ ] ={1,2,840,10036,1,1,1,26};
UINT4 Dot11RSNAPreauthenticationImplemented [ ] ={1,2,840,10036,1,1,1,27};
UINT4 Dot11OperatingClassesImplemented [ ] ={1,2,840,10036,1,1,1,28};
UINT4 Dot11OperatingClassesRequired [ ] ={1,2,840,10036,1,1,1,29};
UINT4 Dot11QosOptionImplemented [ ] ={1,2,840,10036,1,1,1,30};
UINT4 Dot11ImmediateBlockAckOptionImplemented [ ] ={1,2,840,10036,1,1,1,31};
UINT4 Dot11DelayedBlockAckOptionImplemented [ ] ={1,2,840,10036,1,1,1,32};
UINT4 Dot11DirectOptionImplemented [ ] ={1,2,840,10036,1,1,1,33};
UINT4 Dot11APSDOptionImplemented [ ] ={1,2,840,10036,1,1,1,34};
UINT4 Dot11QAckOptionImplemented [ ] ={1,2,840,10036,1,1,1,35};
UINT4 Dot11QBSSLoadImplemented [ ] ={1,2,840,10036,1,1,1,36};
UINT4 Dot11QueueRequestOptionImplemented [ ] ={1,2,840,10036,1,1,1,37};
UINT4 Dot11TXOPRequestOptionImplemented [ ] ={1,2,840,10036,1,1,1,38};
UINT4 Dot11MoreDataAckOptionImplemented [ ] ={1,2,840,10036,1,1,1,39};
UINT4 Dot11AssociateInNQBSS [ ] ={1,2,840,10036,1,1,1,40};
UINT4 Dot11DLSAllowedInQBSS [ ] ={1,2,840,10036,1,1,1,41};
UINT4 Dot11DLSAllowed [ ] ={1,2,840,10036,1,1,1,42};
UINT4 Dot11AssociateStation [ ] ={1,2,840,10036,1,1,1,43};
UINT4 Dot11AssociateID [ ] ={1,2,840,10036,1,1,1,44};
UINT4 Dot11AssociateFailStation [ ] ={1,2,840,10036,1,1,1,45};
UINT4 Dot11AssociateFailStatus [ ] ={1,2,840,10036,1,1,1,46};
UINT4 Dot11ReassociateStation [ ] ={1,2,840,10036,1,1,1,47};
UINT4 Dot11ReassociateID [ ] ={1,2,840,10036,1,1,1,48};
UINT4 Dot11ReassociateFailStation [ ] ={1,2,840,10036,1,1,1,49};
UINT4 Dot11ReassociateFailStatus [ ] ={1,2,840,10036,1,1,1,50};
UINT4 Dot11RadioMeasurementImplemented [ ] ={1,2,840,10036,1,1,1,51};
UINT4 Dot11RadioMeasurementActivated [ ] ={1,2,840,10036,1,1,1,52};
UINT4 Dot11RMMeasurementProbeDelay [ ] ={1,2,840,10036,1,1,1,53};
UINT4 Dot11RMMeasurementPilotPeriod [ ] ={1,2,840,10036,1,1,1,54};
UINT4 Dot11RMLinkMeasurementActivated [ ] ={1,2,840,10036,1,1,1,55};
UINT4 Dot11RMNeighborReportActivated [ ] ={1,2,840,10036,1,1,1,56};
UINT4 Dot11RMParallelMeasurementsActivated [ ] ={1,2,840,10036,1,1,1,57};
UINT4 Dot11RMRepeatedMeasurementsActivated [ ] ={1,2,840,10036,1,1,1,58};
UINT4 Dot11RMBeaconPassiveMeasurementActivated [ ] ={1,2,840,10036,1,1,1,59};
UINT4 Dot11RMBeaconActiveMeasurementActivated [ ] ={1,2,840,10036,1,1,1,60};
UINT4 Dot11RMBeaconTableMeasurementActivated [ ] ={1,2,840,10036,1,1,1,61};
UINT4 Dot11RMBeaconMeasurementReportingConditionsActivated [ ] ={1,2,840,10036,1,1,1,62};
UINT4 Dot11RMFrameMeasurementActivated [ ] ={1,2,840,10036,1,1,1,63};
UINT4 Dot11RMChannelLoadMeasurementActivated [ ] ={1,2,840,10036,1,1,1,64};
UINT4 Dot11RMNoiseHistogramMeasurementActivated [ ] ={1,2,840,10036,1,1,1,65};
UINT4 Dot11RMStatisticsMeasurementActivated [ ] ={1,2,840,10036,1,1,1,66};
UINT4 Dot11RMLCIMeasurementActivated [ ] ={1,2,840,10036,1,1,1,67};
UINT4 Dot11RMLCIAzimuthActivated [ ] ={1,2,840,10036,1,1,1,68};
UINT4 Dot11RMTransmitStreamCategoryMeasurementActivated [ ] ={1,2,840,10036,1,1,1,69};
UINT4 Dot11RMTriggeredTransmitStreamCategoryMeasurementActivated [ ] ={1,2,840,10036,1,1,1,70};
UINT4 Dot11RMAPChannelReportActivated [ ] ={1,2,840,10036,1,1,1,71};
UINT4 Dot11RMMIBActivated [ ] ={1,2,840,10036,1,1,1,72};
UINT4 Dot11RMMaxMeasurementDuration [ ] ={1,2,840,10036,1,1,1,73};
UINT4 Dot11RMNonOperatingChannelMaxMeasurementDuration [ ] ={1,2,840,10036,1,1,1,74};
UINT4 Dot11RMMeasurementPilotTransmissionInformationActivated [ ] ={1,2,840,10036,1,1,1,75};
UINT4 Dot11RMMeasurementPilotActivated [ ] ={1,2,840,10036,1,1,1,76};
UINT4 Dot11RMNeighborReportTSFOffsetActivated [ ] ={1,2,840,10036,1,1,1,77};
UINT4 Dot11RMRCPIMeasurementActivated [ ] ={1,2,840,10036,1,1,1,78};
UINT4 Dot11RMRSNIMeasurementActivated [ ] ={1,2,840,10036,1,1,1,79};
UINT4 Dot11RMBSSAverageAccessDelayActivated [ ] ={1,2,840,10036,1,1,1,80};
UINT4 Dot11RMBSSAvailableAdmissionCapacityActivated [ ] ={1,2,840,10036,1,1,1,81};
UINT4 Dot11RMAntennaInformationActivated [ ] ={1,2,840,10036,1,1,1,82};
UINT4 Dot11FastBSSTransitionImplemented [ ] ={1,2,840,10036,1,1,1,83};
UINT4 Dot11LCIDSEImplemented [ ] ={1,2,840,10036,1,1,1,84};
UINT4 Dot11LCIDSERequired [ ] ={1,2,840,10036,1,1,1,85};
UINT4 Dot11DSERequired [ ] ={1,2,840,10036,1,1,1,86};
UINT4 Dot11ExtendedChannelSwitchActivated [ ] ={1,2,840,10036,1,1,1,87};
#ifdef PMF_WANTED
UINT4 Dot11RSNAProtectedManagementFramesActivated [ ] ={1,2,840,10036,1,1,1,88};
UINT4 Dot11RSNAUnprotectedManagementFramesAllowed [ ] ={1,2,840,10036,1,1,1,89};
UINT4 Dot11AssociationSAQueryMaximumTimeout [ ] ={1,2,840,10036,1,1,1,90};
UINT4 Dot11AssociationSAQueryRetryTimeout [ ] ={1,2,840,10036,1,1,1,91};
#endif
UINT4 Dot11HighThroughputOptionImplemented [ ] ={1,2,840,10036,1,1,1,92};
UINT4 Dot11RSNAPBACRequired [ ] ={1,2,840,10036,1,1,1,93};
UINT4 Dot11PSMPOptionImplemented [ ] ={1,2,840,10036,1,1,1,94};
UINT4 Dot11TunneledDirectLinkSetupImplemented [ ] ={1,2,840,10036,1,1,1,95};
UINT4 Dot11TDLSPeerUAPSDBufferSTAActivated [ ] ={1,2,840,10036,1,1,1,96};
UINT4 Dot11TDLSPeerPSMActivated [ ] ={1,2,840,10036,1,1,1,97};
UINT4 Dot11TDLSPeerUAPSDIndicationWindow [ ] ={1,2,840,10036,1,1,1,98};
UINT4 Dot11TDLSChannelSwitchingActivated [ ] ={1,2,840,10036,1,1,1,99};
UINT4 Dot11TDLSPeerSTAMissingAckRetryLimit [ ] ={1,2,840,10036,1,1,1,100};
UINT4 Dot11TDLSResponseTimeout [ ] ={1,2,840,10036,1,1,1,101};
UINT4 Dot11OCBActivated [ ] ={1,2,840,10036,1,1,1,102};
UINT4 Dot11TDLSProbeDelay [ ] ={1,2,840,10036,1,1,1,103};
UINT4 Dot11TDLSDiscoveryRequestWindow [ ] ={1,2,840,10036,1,1,1,104};
UINT4 Dot11TDLSACDeterminationInterval [ ] ={1,2,840,10036,1,1,1,105};
UINT4 Dot11WirelessManagementImplemented [ ] ={1,2,840,10036,1,1,1,106};
UINT4 Dot11BssMaxIdlePeriod [ ] ={1,2,840,10036,1,1,1,107};
UINT4 Dot11BssMaxIdlePeriodOptions [ ] ={1,2,840,10036,1,1,1,108};
UINT4 Dot11TIMBroadcastInterval [ ] ={1,2,840,10036,1,1,1,109};
UINT4 Dot11TIMBroadcastOffset [ ] ={1,2,840,10036,1,1,1,110};
UINT4 Dot11TIMBroadcastHighRateTIMRate [ ] ={1,2,840,10036,1,1,1,111};
UINT4 Dot11TIMBroadcastLowRateTIMRate [ ] ={1,2,840,10036,1,1,1,112};
UINT4 Dot11StatsMinTriggerTimeout [ ] ={1,2,840,10036,1,1,1,113};
UINT4 Dot11RMCivicMeasurementActivated [ ] ={1,2,840,10036,1,1,1,114};
UINT4 Dot11RMIdentifierMeasurementActivated [ ] ={1,2,840,10036,1,1,1,115};
UINT4 Dot11TimeAdvertisementDTIMInterval [ ] ={1,2,840,10036,1,1,1,116};
UINT4 Dot11InterworkingServiceImplemented [ ] ={1,2,840,10036,1,1,1,117};
UINT4 Dot11InterworkingServiceActivated [ ] ={1,2,840,10036,1,1,1,118};
UINT4 Dot11QosMapImplemented [ ] ={1,2,840,10036,1,1,1,119};
UINT4 Dot11QosMapActivated [ ] ={1,2,840,10036,1,1,1,120};
UINT4 Dot11EBRImplemented [ ] ={1,2,840,10036,1,1,1,121};
UINT4 Dot11EBRActivated [ ] ={1,2,840,10036,1,1,1,122};
UINT4 Dot11ESNetwork [ ] ={1,2,840,10036,1,1,1,123};
UINT4 Dot11SSPNInterfaceImplemented [ ] ={1,2,840,10036,1,1,1,124};
UINT4 Dot11SSPNInterfaceActivated [ ] ={1,2,840,10036,1,1,1,125};
UINT4 Dot11HESSID [ ] ={1,2,840,10036,1,1,1,126};
UINT4 Dot11EASImplemented [ ] ={1,2,840,10036,1,1,1,127};
UINT4 Dot11EASActivated [ ] ={1,2,840,10036,1,1,1,128};
UINT4 Dot11MSGCFImplemented [ ] ={1,2,840,10036,1,1,1,129};
UINT4 Dot11MSGCFActivated [ ] ={1,2,840,10036,1,1,1,130};
UINT4 Dot11TimeAdvertisementTimeError [ ] ={1,2,840,10036,1,1,1,131};
UINT4 Dot11TimeAdvertisementTimeValue [ ] ={1,2,840,10036,1,1,1,132};
UINT4 Dot11RM3rdPartyMeasurementActivated [ ] ={1,2,840,10036,1,1,1,133};
UINT4 Dot11RejectUnadmittedTraffic [ ] ={1,2,840,10036,1,1,1,134};
UINT4 Dot11BSSBroadcastNullCount [ ] ={1,2,840,10036,1,1,1,135};
UINT4 Dot11MeshActivated [ ] ={1,2,840,10036,1,1,1,136};
UINT4 Dot11AuthenticationAlgorithmsIndex [ ] ={1,2,840,10036,1,2,1,1};
UINT4 Dot11AuthenticationAlgorithm [ ] ={1,2,840,10036,1,2,1,2};
UINT4 Dot11AuthenticationAlgorithmsActivated [ ] ={1,2,840,10036,1,2,1,3};
UINT4 Dot11WEPDefaultKeyIndex [ ] ={1,2,840,10036,1,3,1,1};
UINT4 Dot11WEPDefaultKeyValue [ ] ={1,2,840,10036,1,3,1,2};
UINT4 Dot11WEPKeyMappingIndex [ ] ={1,2,840,10036,1,4,1,1};
UINT4 Dot11WEPKeyMappingAddress [ ] ={1,2,840,10036,1,4,1,2};
UINT4 Dot11WEPKeyMappingWEPOn [ ] ={1,2,840,10036,1,4,1,3};
UINT4 Dot11WEPKeyMappingValue [ ] ={1,2,840,10036,1,4,1,4};
UINT4 Dot11WEPKeyMappingStatus [ ] ={1,2,840,10036,1,4,1,5};
UINT4 Dot11PrivacyInvoked [ ] ={1,2,840,10036,1,5,1,1};
UINT4 Dot11WEPDefaultKeyID [ ] ={1,2,840,10036,1,5,1,2};
UINT4 Dot11WEPKeyMappingLengthImplemented [ ] ={1,2,840,10036,1,5,1,3};
UINT4 Dot11ExcludeUnencrypted [ ] ={1,2,840,10036,1,5,1,4};
UINT4 Dot11WEPICVErrorCount [ ] ={1,2,840,10036,1,5,1,5};
UINT4 Dot11WEPExcludedCount [ ] ={1,2,840,10036,1,5,1,6};
UINT4 Dot11RSNAActivated [ ] ={1,2,840,10036,1,5,1,7};
UINT4 Dot11RSNAPreauthenticationActivated [ ] ={1,2,840,10036,1,5,1,8};
UINT4 Dot11MultiDomainCapabilityIndex [ ] ={1,2,840,10036,1,7,1,1};
UINT4 Dot11FirstChannelNumber [ ] ={1,2,840,10036,1,7,1,2};
UINT4 Dot11NumberofChannels [ ] ={1,2,840,10036,1,7,1,3};
UINT4 Dot11MaximumTransmitPowerLevel [ ] ={1,2,840,10036,1,7,1,4};
UINT4 Dot11SpectrumManagementIndex [ ] ={1,2,840,10036,1,8,1,1};
UINT4 Dot11MitigationRequirement [ ] ={1,2,840,10036,1,8,1,2};
UINT4 Dot11ChannelSwitchTime [ ] ={1,2,840,10036,1,8,1,3};
UINT4 Dot11PowerCapabilityMaxImplemented [ ] ={1,2,840,10036,1,8,1,4};
UINT4 Dot11PowerCapabilityMinImplemented [ ] ={1,2,840,10036,1,8,1,5};
UINT4 Dot11RSNAConfigVersion [ ] ={1,2,840,10036,1,9,1,2};
UINT4 Dot11RSNAConfigPairwiseKeysImplemented [ ] ={1,2,840,10036,1,9,1,3};
UINT4 Dot11RSNAConfigGroupCipher [ ] ={1,2,840,10036,1,9,1,4};
UINT4 Dot11RSNAConfigGroupRekeyMethod [ ] ={1,2,840,10036,1,9,1,5};
UINT4 Dot11RSNAConfigGroupRekeyTime [ ] ={1,2,840,10036,1,9,1,6};
UINT4 Dot11RSNAConfigGroupRekeyPackets [ ] ={1,2,840,10036,1,9,1,7};
UINT4 Dot11RSNAConfigGroupRekeyStrict [ ] ={1,2,840,10036,1,9,1,8};
UINT4 Dot11RSNAConfigPSKValue [ ] ={1,2,840,10036,1,9,1,9};
UINT4 Dot11RSNAConfigPSKPassPhrase [ ] ={1,2,840,10036,1,9,1,10};
UINT4 Dot11RSNAConfigGroupUpdateCount [ ] ={1,2,840,10036,1,9,1,13};
UINT4 Dot11RSNAConfigPairwiseUpdateCount [ ] ={1,2,840,10036,1,9,1,14};
UINT4 Dot11RSNAConfigGroupCipherSize [ ] ={1,2,840,10036,1,9,1,15};
UINT4 Dot11RSNAConfigPMKLifetime [ ] ={1,2,840,10036,1,9,1,16};
UINT4 Dot11RSNAConfigPMKReauthThreshold [ ] ={1,2,840,10036,1,9,1,17};
UINT4 Dot11RSNAConfigNumberOfPTKSAReplayCountersImplemented [ ] ={1,2,840,10036,1,9,1,18};
UINT4 Dot11RSNAConfigSATimeout [ ] ={1,2,840,10036,1,9,1,19};
UINT4 Dot11RSNAAuthenticationSuiteSelected [ ] ={1,2,840,10036,1,9,1,20};
UINT4 Dot11RSNAPairwiseCipherSelected [ ] ={1,2,840,10036,1,9,1,21};
UINT4 Dot11RSNAGroupCipherSelected [ ] ={1,2,840,10036,1,9,1,22};
UINT4 Dot11RSNAPMKIDUsed [ ] ={1,2,840,10036,1,9,1,23};
UINT4 Dot11RSNAAuthenticationSuiteRequested [ ] ={1,2,840,10036,1,9,1,24};
UINT4 Dot11RSNAPairwiseCipherRequested [ ] ={1,2,840,10036,1,9,1,25};
UINT4 Dot11RSNAGroupCipherRequested [ ] ={1,2,840,10036,1,9,1,26};
UINT4 Dot11RSNATKIPCounterMeasuresInvoked [ ] ={1,2,840,10036,1,9,1,27};
UINT4 Dot11RSNA4WayHandshakeFailures [ ] ={1,2,840,10036,1,9,1,28};
UINT4 Dot11RSNAConfigNumberOfGTKSAReplayCountersImplemented [ ] ={1,2,840,10036,1,9,1,29};
UINT4 Dot11RSNAConfigSTKKeysImplemented [ ] ={1,2,840,10036,1,9,1,30};
UINT4 Dot11RSNAConfigSTKCipher [ ] ={1,2,840,10036,1,9,1,31};
UINT4 Dot11RSNAConfigSTKRekeyTime [ ] ={1,2,840,10036,1,9,1,32};
UINT4 Dot11RSNAConfigSMKUpdateCount [ ] ={1,2,840,10036,1,9,1,33};
UINT4 Dot11RSNAConfigSTKCipherSize [ ] ={1,2,840,10036,1,9,1,34};
UINT4 Dot11RSNAConfigSMKLifetime [ ] ={1,2,840,10036,1,9,1,35};
UINT4 Dot11RSNAConfigSMKReauthThreshold [ ] ={1,2,840,10036,1,9,1,36};
UINT4 Dot11RSNAConfigNumberOfSTKSAReplayCountersImplemented [ ] ={1,2,840,10036,1,9,1,37};
UINT4 Dot11RSNAPairwiseSTKSelected [ ] ={1,2,840,10036,1,9,1,38};
UINT4 Dot11RSNASMKHandshakeFailures [ ] ={1,2,840,10036,1,9,1,39};
UINT4 Dot11RSNASAERetransPeriod [ ] ={1,2,840,10036,1,9,1,40};
UINT4 Dot11RSNASAEAntiCloggingThreshold [ ] ={1,2,840,10036,1,9,1,41};
UINT4 Dot11RSNASAESync [ ] ={1,2,840,10036,1,9,1,42};
UINT4 Dot11RSNAConfigPairwiseCipherIndex [ ] ={1,2,840,10036,1,10,1,1};
UINT4 Dot11RSNAConfigPairwiseCipherImplemented [ ] ={1,2,840,10036,1,10,1,2};
UINT4 Dot11RSNAConfigPairwiseCipherActivated [ ] ={1,2,840,10036,1,10,1,3};
UINT4 Dot11RSNAConfigPairwiseCipherSizeImplemented [ ] ={1,2,840,10036,1,10,1,4};
UINT4 Dot11RSNAConfigAuthenticationSuiteIndex [ ] ={1,2,840,10036,1,11,1,1};
UINT4 Dot11RSNAConfigAuthenticationSuiteImplemented [ ] ={1,2,840,10036,1,11,1,2};
UINT4 Dot11RSNAConfigAuthenticationSuiteActivated [ ] ={1,2,840,10036,1,11,1,3};
UINT4 Dot11RSNAStatsIndex [ ] ={1,2,840,10036,1,12,1,1};
UINT4 Dot11RSNAStatsSTAAddress [ ] ={1,2,840,10036,1,12,1,2};
UINT4 Dot11RSNAStatsVersion [ ] ={1,2,840,10036,1,12,1,3};
UINT4 Dot11RSNAStatsSelectedPairwiseCipher [ ] ={1,2,840,10036,1,12,1,4};
UINT4 Dot11RSNAStatsTKIPICVErrors [ ] ={1,2,840,10036,1,12,1,5};
UINT4 Dot11RSNAStatsTKIPLocalMICFailures [ ] ={1,2,840,10036,1,12,1,6};
UINT4 Dot11RSNAStatsTKIPRemoteMICFailures [ ] ={1,2,840,10036,1,12,1,7};
UINT4 Dot11RSNAStatsCCMPReplays [ ] ={1,2,840,10036,1,12,1,8};
UINT4 Dot11RSNAStatsCCMPDecryptErrors [ ] ={1,2,840,10036,1,12,1,9};
UINT4 Dot11RSNAStatsTKIPReplays [ ] ={1,2,840,10036,1,12,1,10};
UINT4 Dot11RSNAStatsCMACICVErrors [ ] ={1,2,840,10036,1,12,1,11};
UINT4 Dot11RSNAStatsCMACReplays [ ] ={1,2,840,10036,1,12,1,12};
UINT4 Dot11RSNAStatsRobustMgmtCCMPReplays [ ] ={1,2,840,10036,1,12,1,13};
UINT4 Dot11RSNABIPMICErrors [ ] ={1,2,840,10036,1,12,1,14};
UINT4 Dot11OperatingClassesIndex [ ] ={1,2,840,10036,1,13,1,1};
UINT4 Dot11OperatingClass [ ] ={1,2,840,10036,1,13,1,2};
UINT4 Dot11CoverageClass [ ] ={1,2,840,10036,1,13,1,3};
UINT4 Dot11RMRequestNextIndex [ ] ={1,2,840,10036,1,14,1,1};
UINT4 Dot11RMRqstIndex [ ] ={1,2,840,10036,1,14,1,2,1,1};
UINT4 Dot11RMRqstRowStatus [ ] ={1,2,840,10036,1,14,1,2,1,2};
UINT4 Dot11RMRqstToken [ ] ={1,2,840,10036,1,14,1,2,1,3};
UINT4 Dot11RMRqstRepetitions [ ] ={1,2,840,10036,1,14,1,2,1,4};
UINT4 Dot11RMRqstIfIndex [ ] ={1,2,840,10036,1,14,1,2,1,5};
UINT4 Dot11RMRqstType [ ] ={1,2,840,10036,1,14,1,2,1,6};
UINT4 Dot11RMRqstTargetAdd [ ] ={1,2,840,10036,1,14,1,2,1,7};
UINT4 Dot11RMRqstTimeStamp [ ] ={1,2,840,10036,1,14,1,2,1,8};
UINT4 Dot11RMRqstChanNumber [ ] ={1,2,840,10036,1,14,1,2,1,9};
UINT4 Dot11RMRqstOperatingClass [ ] ={1,2,840,10036,1,14,1,2,1,10};
UINT4 Dot11RMRqstRndInterval [ ] ={1,2,840,10036,1,14,1,2,1,11};
UINT4 Dot11RMRqstDuration [ ] ={1,2,840,10036,1,14,1,2,1,12};
UINT4 Dot11RMRqstParallel [ ] ={1,2,840,10036,1,14,1,2,1,13};
UINT4 Dot11RMRqstEnable [ ] ={1,2,840,10036,1,14,1,2,1,14};
UINT4 Dot11RMRqstRequest [ ] ={1,2,840,10036,1,14,1,2,1,15};
UINT4 Dot11RMRqstReport [ ] ={1,2,840,10036,1,14,1,2,1,16};
UINT4 Dot11RMRqstDurationMandatory [ ] ={1,2,840,10036,1,14,1,2,1,17};
UINT4 Dot11RMRqstBeaconRqstMode [ ] ={1,2,840,10036,1,14,1,2,1,18};
UINT4 Dot11RMRqstBeaconRqstDetail [ ] ={1,2,840,10036,1,14,1,2,1,19};
UINT4 Dot11RMRqstFrameRqstType [ ] ={1,2,840,10036,1,14,1,2,1,20};
UINT4 Dot11RMRqstBssid [ ] ={1,2,840,10036,1,14,1,2,1,21};
UINT4 Dot11RMRqstSSID [ ] ={1,2,840,10036,1,14,1,2,1,22};
UINT4 Dot11RMRqstBeaconReportingCondition [ ] ={1,2,840,10036,1,14,1,2,1,23};
UINT4 Dot11RMRqstBeaconThresholdOffset [ ] ={1,2,840,10036,1,14,1,2,1,24};
UINT4 Dot11RMRqstSTAStatRqstGroupID [ ] ={1,2,840,10036,1,14,1,2,1,25};
UINT4 Dot11RMRqstLCIRqstSubject [ ] ={1,2,840,10036,1,14,1,2,1,26};
UINT4 Dot11RMRqstLCILatitudeResolution [ ] ={1,2,840,10036,1,14,1,2,1,27};
UINT4 Dot11RMRqstLCILongitudeResolution [ ] ={1,2,840,10036,1,14,1,2,1,28};
UINT4 Dot11RMRqstLCIAltitudeResolution [ ] ={1,2,840,10036,1,14,1,2,1,29};
UINT4 Dot11RMRqstLCIAzimuthType [ ] ={1,2,840,10036,1,14,1,2,1,30};
UINT4 Dot11RMRqstLCIAzimuthResolution [ ] ={1,2,840,10036,1,14,1,2,1,31};
UINT4 Dot11RMRqstPauseTime [ ] ={1,2,840,10036,1,14,1,2,1,32};
UINT4 Dot11RMRqstTransmitStreamPeerQSTAAddress [ ] ={1,2,840,10036,1,14,1,2,1,33};
UINT4 Dot11RMRqstTransmitStreamTrafficIdentifier [ ] ={1,2,840,10036,1,14,1,2,1,34};
UINT4 Dot11RMRqstTransmitStreamBin0Range [ ] ={1,2,840,10036,1,14,1,2,1,35};
UINT4 Dot11RMRqstTrigdQoSAverageCondition [ ] ={1,2,840,10036,1,14,1,2,1,36};
UINT4 Dot11RMRqstTrigdQoSConsecutiveCondition [ ] ={1,2,840,10036,1,14,1,2,1,37};
UINT4 Dot11RMRqstTrigdQoSDelayCondition [ ] ={1,2,840,10036,1,14,1,2,1,38};
UINT4 Dot11RMRqstTrigdQoSAverageThreshold [ ] ={1,2,840,10036,1,14,1,2,1,39};
UINT4 Dot11RMRqstTrigdQoSConsecutiveThreshold [ ] ={1,2,840,10036,1,14,1,2,1,40};
UINT4 Dot11RMRqstTrigdQoSDelayThresholdRange [ ] ={1,2,840,10036,1,14,1,2,1,41};
UINT4 Dot11RMRqstTrigdQoSDelayThreshold [ ] ={1,2,840,10036,1,14,1,2,1,42};
UINT4 Dot11RMRqstTrigdQoSMeasurementCount [ ] ={1,2,840,10036,1,14,1,2,1,43};
UINT4 Dot11RMRqstTrigdQoSTimeout [ ] ={1,2,840,10036,1,14,1,2,1,44};
UINT4 Dot11RMRqstChannelLoadReportingCondition [ ] ={1,2,840,10036,1,14,1,2,1,45};
UINT4 Dot11RMRqstChannelLoadReference [ ] ={1,2,840,10036,1,14,1,2,1,46};
UINT4 Dot11RMRqstNoiseHistogramReportingCondition [ ] ={1,2,840,10036,1,14,1,2,1,47};
UINT4 Dot11RMRqstAnpiReference [ ] ={1,2,840,10036,1,14,1,2,1,48};
UINT4 Dot11RMRqstAPChannelReport [ ] ={1,2,840,10036,1,14,1,2,1,49};
UINT4 Dot11RMRqstSTAStatPeerSTAAddress [ ] ={1,2,840,10036,1,14,1,2,1,50};
UINT4 Dot11RMRqstFrameTransmitterAddress [ ] ={1,2,840,10036,1,14,1,2,1,51};
UINT4 Dot11RMRqstVendorSpecific [ ] ={1,2,840,10036,1,14,1,2,1,52};
UINT4 Dot11RMRqstSTAStatTrigMeasCount [ ] ={1,2,840,10036,1,14,1,2,1,53};
UINT4 Dot11RMRqstSTAStatTrigTimeout [ ] ={1,2,840,10036,1,14,1,2,1,54};
UINT4 Dot11RMRqstSTAStatTrigCondition [ ] ={1,2,840,10036,1,14,1,2,1,55};
UINT4 Dot11RMRqstSTAStatTrigSTAFailedCntThresh [ ] ={1,2,840,10036,1,14,1,2,1,56};
UINT4 Dot11RMRqstSTAStatTrigSTAFCSErrCntThresh [ ] ={1,2,840,10036,1,14,1,2,1,57};
UINT4 Dot11RMRqstSTAStatTrigSTAMultRetryCntThresh [ ] ={1,2,840,10036,1,14,1,2,1,58};
UINT4 Dot11RMRqstSTAStatTrigSTAFrameDupeCntThresh [ ] ={1,2,840,10036,1,14,1,2,1,59};
UINT4 Dot11RMRqstSTAStatTrigSTARTSFailCntThresh [ ] ={1,2,840,10036,1,14,1,2,1,60};
UINT4 Dot11RMRqstSTAStatTrigSTAAckFailCntThresh [ ] ={1,2,840,10036,1,14,1,2,1,61};
UINT4 Dot11RMRqstSTAStatTrigSTARetryCntThresh [ ] ={1,2,840,10036,1,14,1,2,1,62};
UINT4 Dot11RMRqstSTAStatTrigQoSTrigCondition [ ] ={1,2,840,10036,1,14,1,2,1,63};
UINT4 Dot11RMRqstSTAStatTrigQoSFailedCntThresh [ ] ={1,2,840,10036,1,14,1,2,1,64};
UINT4 Dot11RMRqstSTAStatTrigQoSRetryCntThresh [ ] ={1,2,840,10036,1,14,1,2,1,65};
UINT4 Dot11RMRqstSTAStatTrigQoSMultRetryCntThresh [ ] ={1,2,840,10036,1,14,1,2,1,66};
UINT4 Dot11RMRqstSTAStatTrigQoSFrameDupeCntThresh [ ] ={1,2,840,10036,1,14,1,2,1,67};
UINT4 Dot11RMRqstSTAStatTrigQoSRTSFailCntThresh [ ] ={1,2,840,10036,1,14,1,2,1,68};
UINT4 Dot11RMRqstSTAStatTrigQoSAckFailCntThresh [ ] ={1,2,840,10036,1,14,1,2,1,69};
UINT4 Dot11RMRqstSTAStatTrigQoSDiscardCntThresh [ ] ={1,2,840,10036,1,14,1,2,1,70};
UINT4 Dot11RMRqstSTAStatTrigRsnaTrigCondition [ ] ={1,2,840,10036,1,14,1,2,1,71};
UINT4 Dot11RMRqstSTAStatTrigRsnaCMACICVErrCntThresh [ ] ={1,2,840,10036,1,14,1,2,1,72};
UINT4 Dot11RMRqstSTAStatTrigRsnaCMACReplayCntThresh [ ] ={1,2,840,10036,1,14,1,2,1,73};
UINT4 Dot11RMRqstSTAStatTrigRsnaRobustCCMPReplayCntThresh [ ] ={1,2,840,10036,1,14,1,2,1,74};
UINT4 Dot11RMRqstSTAStatTrigRsnaTKIPICVErrCntThresh [ ] ={1,2,840,10036,1,14,1,2,1,75};
UINT4 Dot11RMRqstSTAStatTrigRsnaTKIPReplayCntThresh [ ] ={1,2,840,10036,1,14,1,2,1,76};
UINT4 Dot11RMRqstSTAStatTrigRsnaCCMPDecryptErrCntThresh [ ] ={1,2,840,10036,1,14,1,2,1,77};
UINT4 Dot11RMRqstSTAStatTrigRsnaCCMPReplayCntThresh [ ] ={1,2,840,10036,1,14,1,2,1,78};
UINT4 Dot11ChannelLoadRprtIndex [ ] ={1,2,840,10036,1,14,2,1,1,1};
UINT4 Dot11ChannelLoadRprtRqstToken [ ] ={1,2,840,10036,1,14,2,1,1,2};
UINT4 Dot11ChannelLoadRprtIfIndex [ ] ={1,2,840,10036,1,14,2,1,1,3};
UINT4 Dot11ChannelLoadMeasuringSTAAddr [ ] ={1,2,840,10036,1,14,2,1,1,4};
UINT4 Dot11ChannelLoadRprtChanNumber [ ] ={1,2,840,10036,1,14,2,1,1,5};
UINT4 Dot11ChannelLoadRprtOperatingClass [ ] ={1,2,840,10036,1,14,2,1,1,6};
UINT4 Dot11ChannelLoadRprtActualStartTime [ ] ={1,2,840,10036,1,14,2,1,1,7};
UINT4 Dot11ChannelLoadRprtMeasurementDuration [ ] ={1,2,840,10036,1,14,2,1,1,8};
UINT4 Dot11ChannelLoadRprtChannelLoad [ ] ={1,2,840,10036,1,14,2,1,1,9};
UINT4 Dot11ChannelLoadRprtVendorSpecific [ ] ={1,2,840,10036,1,14,2,1,1,10};
UINT4 Dot11ChannelLoadRprtMeasurementMode [ ] ={1,2,840,10036,1,14,2,1,1,11};
UINT4 Dot11NoiseHistogramRprtIndex [ ] ={1,2,840,10036,1,14,2,2,1,1};
UINT4 Dot11NoiseHistogramRprtRqstToken [ ] ={1,2,840,10036,1,14,2,2,1,2};
UINT4 Dot11NoiseHistogramRprtIfIndex [ ] ={1,2,840,10036,1,14,2,2,1,3};
UINT4 Dot11NoiseHistogramMeasuringSTAAddr [ ] ={1,2,840,10036,1,14,2,2,1,4};
UINT4 Dot11NoiseHistogramRprtChanNumber [ ] ={1,2,840,10036,1,14,2,2,1,5};
UINT4 Dot11NoiseHistogramRprtOperatingClass [ ] ={1,2,840,10036,1,14,2,2,1,6};
UINT4 Dot11NoiseHistogramRprtActualStartTime [ ] ={1,2,840,10036,1,14,2,2,1,7};
UINT4 Dot11NoiseHistogramRprtMeasurementDuration [ ] ={1,2,840,10036,1,14,2,2,1,8};
UINT4 Dot11NoiseHistogramRprtAntennaID [ ] ={1,2,840,10036,1,14,2,2,1,9};
UINT4 Dot11NoiseHistogramRprtANPI [ ] ={1,2,840,10036,1,14,2,2,1,10};
UINT4 Dot11NoiseHistogramRprtIPIDensity0 [ ] ={1,2,840,10036,1,14,2,2,1,11};
UINT4 Dot11NoiseHistogramRprtIPIDensity1 [ ] ={1,2,840,10036,1,14,2,2,1,12};
UINT4 Dot11NoiseHistogramRprtIPIDensity2 [ ] ={1,2,840,10036,1,14,2,2,1,13};
UINT4 Dot11NoiseHistogramRprtIPIDensity3 [ ] ={1,2,840,10036,1,14,2,2,1,14};
UINT4 Dot11NoiseHistogramRprtIPIDensity4 [ ] ={1,2,840,10036,1,14,2,2,1,15};
UINT4 Dot11NoiseHistogramRprtIPIDensity5 [ ] ={1,2,840,10036,1,14,2,2,1,16};
UINT4 Dot11NoiseHistogramRprtIPIDensity6 [ ] ={1,2,840,10036,1,14,2,2,1,17};
UINT4 Dot11NoiseHistogramRprtIPIDensity7 [ ] ={1,2,840,10036,1,14,2,2,1,18};
UINT4 Dot11NoiseHistogramRprtIPIDensity8 [ ] ={1,2,840,10036,1,14,2,2,1,19};
UINT4 Dot11NoiseHistogramRprtIPIDensity9 [ ] ={1,2,840,10036,1,14,2,2,1,20};
UINT4 Dot11NoiseHistogramRprtIPIDensity10 [ ] ={1,2,840,10036,1,14,2,2,1,21};
UINT4 Dot11NoiseHistogramRprtVendorSpecific [ ] ={1,2,840,10036,1,14,2,2,1,22};
UINT4 Dot11NoiseHistogramRprtMeasurementMode [ ] ={1,2,840,10036,1,14,2,2,1,23};
UINT4 Dot11BeaconRprtIndex [ ] ={1,2,840,10036,1,14,2,3,1,1};
UINT4 Dot11BeaconRprtRqstToken [ ] ={1,2,840,10036,1,14,2,3,1,2};
UINT4 Dot11BeaconRprtIfIndex [ ] ={1,2,840,10036,1,14,2,3,1,3};
UINT4 Dot11BeaconMeasuringSTAAddr [ ] ={1,2,840,10036,1,14,2,3,1,4};
UINT4 Dot11BeaconRprtChanNumber [ ] ={1,2,840,10036,1,14,2,3,1,5};
UINT4 Dot11BeaconRprtOperatingClass [ ] ={1,2,840,10036,1,14,2,3,1,6};
UINT4 Dot11BeaconRprtActualStartTime [ ] ={1,2,840,10036,1,14,2,3,1,7};
UINT4 Dot11BeaconRprtMeasurementDuration [ ] ={1,2,840,10036,1,14,2,3,1,8};
UINT4 Dot11BeaconRprtPhyType [ ] ={1,2,840,10036,1,14,2,3,1,9};
UINT4 Dot11BeaconRprtReportedFrameType [ ] ={1,2,840,10036,1,14,2,3,1,10};
UINT4 Dot11BeaconRprtRCPI [ ] ={1,2,840,10036,1,14,2,3,1,11};
UINT4 Dot11BeaconRprtRSNI [ ] ={1,2,840,10036,1,14,2,3,1,12};
UINT4 Dot11BeaconRprtBSSID [ ] ={1,2,840,10036,1,14,2,3,1,13};
UINT4 Dot11BeaconRprtAntennaID [ ] ={1,2,840,10036,1,14,2,3,1,14};
UINT4 Dot11BeaconRprtParentTSF [ ] ={1,2,840,10036,1,14,2,3,1,15};
UINT4 Dot11BeaconRprtReportedFrameBody [ ] ={1,2,840,10036,1,14,2,3,1,16};
UINT4 Dot11BeaconRprtVendorSpecific [ ] ={1,2,840,10036,1,14,2,3,1,17};
UINT4 Dot11BeaconRprtMeasurementMode [ ] ={1,2,840,10036,1,14,2,3,1,18};
UINT4 Dot11FrameRprtIndex [ ] ={1,2,840,10036,1,14,2,4,1,1};
UINT4 Dot11FrameRprtIfIndex [ ] ={1,2,840,10036,1,14,2,4,1,2};
UINT4 Dot11FrameRprtRqstToken [ ] ={1,2,840,10036,1,14,2,4,1,3};
UINT4 Dot11FrameRprtChanNumber [ ] ={1,2,840,10036,1,14,2,4,1,4};
UINT4 Dot11FrameRprtOperatingClass [ ] ={1,2,840,10036,1,14,2,4,1,5};
UINT4 Dot11FrameRprtActualStartTime [ ] ={1,2,840,10036,1,14,2,4,1,6};
UINT4 Dot11FrameRprtMeasurementDuration [ ] ={1,2,840,10036,1,14,2,4,1,7};
UINT4 Dot11FrameRprtTransmitSTAAddress [ ] ={1,2,840,10036,1,14,2,4,1,8};
UINT4 Dot11FrameRprtBSSID [ ] ={1,2,840,10036,1,14,2,4,1,9};
UINT4 Dot11FrameRprtPhyType [ ] ={1,2,840,10036,1,14,2,4,1,10};
UINT4 Dot11FrameRprtAvgRCPI [ ] ={1,2,840,10036,1,14,2,4,1,11};
UINT4 Dot11FrameRprtLastRSNI [ ] ={1,2,840,10036,1,14,2,4,1,12};
UINT4 Dot11FrameRprtLastRCPI [ ] ={1,2,840,10036,1,14,2,4,1,13};
UINT4 Dot11FrameRprtAntennaID [ ] ={1,2,840,10036,1,14,2,4,1,14};
UINT4 Dot11FrameRprtNumberFrames [ ] ={1,2,840,10036,1,14,2,4,1,15};
UINT4 Dot11FrameRprtVendorSpecific [ ] ={1,2,840,10036,1,14,2,4,1,16};
UINT4 Dot11FrameRptMeasurementMode [ ] ={1,2,840,10036,1,14,2,4,1,17};
UINT4 Dot11STAStatisticsReportIndex [ ] ={1,2,840,10036,1,14,2,5,1,1};
UINT4 Dot11STAStatisticsReportToken [ ] ={1,2,840,10036,1,14,2,5,1,2};
UINT4 Dot11STAStatisticsIfIndex [ ] ={1,2,840,10036,1,14,2,5,1,3};
UINT4 Dot11STAStatisticsSTAAddress [ ] ={1,2,840,10036,1,14,2,5,1,4};
UINT4 Dot11STAStatisticsMeasurementDuration [ ] ={1,2,840,10036,1,14,2,5,1,5};
UINT4 Dot11STAStatisticsGroupID [ ] ={1,2,840,10036,1,14,2,5,1,6};
UINT4 Dot11STAStatisticsTransmittedFragmentCount [ ] ={1,2,840,10036,1,14,2,5,1,7};
UINT4 Dot11STAStatisticsGroupTransmittedFrameCount [ ] ={1,2,840,10036,1,14,2,5,1,8};
UINT4 Dot11STAStatisticsFailedCount [ ] ={1,2,840,10036,1,14,2,5,1,9};
UINT4 Dot11STAStatisticsRetryCount [ ] ={1,2,840,10036,1,14,2,5,1,10};
UINT4 Dot11STAStatisticsMultipleRetryCount [ ] ={1,2,840,10036,1,14,2,5,1,11};
UINT4 Dot11STAStatisticsFrameDuplicateCount [ ] ={1,2,840,10036,1,14,2,5,1,12};
UINT4 Dot11STAStatisticsRTSSuccessCount [ ] ={1,2,840,10036,1,14,2,5,1,13};
UINT4 Dot11STAStatisticsRTSFailureCount [ ] ={1,2,840,10036,1,14,2,5,1,14};
UINT4 Dot11STAStatisticsACKFailureCount [ ] ={1,2,840,10036,1,14,2,5,1,15};
UINT4 Dot11STAStatisticsQosTransmittedFragmentCount [ ] ={1,2,840,10036,1,14,2,5,1,16};
UINT4 Dot11STAStatisticsQosFailedCount [ ] ={1,2,840,10036,1,14,2,5,1,17};
UINT4 Dot11STAStatisticsQosRetryCount [ ] ={1,2,840,10036,1,14,2,5,1,18};
UINT4 Dot11STAStatisticsQosMultipleRetryCount [ ] ={1,2,840,10036,1,14,2,5,1,19};
UINT4 Dot11STAStatisticsQosFrameDuplicateCount [ ] ={1,2,840,10036,1,14,2,5,1,20};
UINT4 Dot11STAStatisticsQosRTSSuccessCount [ ] ={1,2,840,10036,1,14,2,5,1,21};
UINT4 Dot11STAStatisticsQosRTSFailureCount [ ] ={1,2,840,10036,1,14,2,5,1,22};
UINT4 Dot11STAStatisticsQosACKFailureCount [ ] ={1,2,840,10036,1,14,2,5,1,23};
UINT4 Dot11STAStatisticsQosReceivedFragmentCount [ ] ={1,2,840,10036,1,14,2,5,1,24};
UINT4 Dot11STAStatisticsQosTransmittedFrameCount [ ] ={1,2,840,10036,1,14,2,5,1,25};
UINT4 Dot11STAStatisticsQosDiscardedFrameCount [ ] ={1,2,840,10036,1,14,2,5,1,26};
UINT4 Dot11STAStatisticsQosMPDUsReceivedCount [ ] ={1,2,840,10036,1,14,2,5,1,27};
UINT4 Dot11STAStatisticsQosRetriesReceivedCount [ ] ={1,2,840,10036,1,14,2,5,1,28};
UINT4 Dot11STAStatisticsReceivedFragmentCount [ ] ={1,2,840,10036,1,14,2,5,1,29};
UINT4 Dot11STAStatisticsGroupReceivedFrameCount [ ] ={1,2,840,10036,1,14,2,5,1,30};
UINT4 Dot11STAStatisticsFCSErrorCount [ ] ={1,2,840,10036,1,14,2,5,1,31};
UINT4 Dot11STAStatisticsTransmittedFrameCount [ ] ={1,2,840,10036,1,14,2,5,1,32};
UINT4 Dot11STAStatisticsAPAverageAccessDelay [ ] ={1,2,840,10036,1,14,2,5,1,33};
UINT4 Dot11STAStatisticsAverageAccessDelayBestEffort [ ] ={1,2,840,10036,1,14,2,5,1,34};
UINT4 Dot11STAStatisticsAverageAccessDelayBackground [ ] ={1,2,840,10036,1,14,2,5,1,35};
UINT4 Dot11STAStatisticsAverageAccessDelayVideo [ ] ={1,2,840,10036,1,14,2,5,1,36};
UINT4 Dot11STAStatisticsAverageAccessDelayVoice [ ] ={1,2,840,10036,1,14,2,5,1,37};
UINT4 Dot11STAStatisticsStationCount [ ] ={1,2,840,10036,1,14,2,5,1,38};
UINT4 Dot11STAStatisticsChannelUtilization [ ] ={1,2,840,10036,1,14,2,5,1,39};
UINT4 Dot11STAStatisticsVendorSpecific [ ] ={1,2,840,10036,1,14,2,5,1,40};
UINT4 Dot11STAStatisticsRprtMeasurementMode [ ] ={1,2,840,10036,1,14,2,5,1,41};
UINT4 Dot11STAStatisticsRSNAStatsCMACICVErrors [ ] ={1,2,840,10036,1,14,2,5,1,42};
UINT4 Dot11STAStatisticsRSNAStatsCMACReplays [ ] ={1,2,840,10036,1,14,2,5,1,43};
UINT4 Dot11STAStatisticsRSNAStatsRobustMgmtCCMPReplays [ ] ={1,2,840,10036,1,14,2,5,1,44};
UINT4 Dot11STAStatisticsRSNAStatsTKIPICVErrors [ ] ={1,2,840,10036,1,14,2,5,1,45};
UINT4 Dot11STAStatisticsRSNAStatsTKIPReplays [ ] ={1,2,840,10036,1,14,2,5,1,46};
UINT4 Dot11STAStatisticsRSNAStatsCCMPDecryptErrors [ ] ={1,2,840,10036,1,14,2,5,1,47};
UINT4 Dot11STAStatisticsRSNAStatsCCMPReplays [ ] ={1,2,840,10036,1,14,2,5,1,48};
UINT4 Dot11STAStatisticsReportingReasonSTACounters [ ] ={1,2,840,10036,1,14,2,5,1,49};
UINT4 Dot11STAStatisticsReportingReasonQosCounters [ ] ={1,2,840,10036,1,14,2,5,1,50};
UINT4 Dot11STAStatisticsReportingReasonRsnaCounters [ ] ={1,2,840,10036,1,14,2,5,1,51};
UINT4 Dot11STAStatisticsTransmittedAMSDUCount [ ] ={1,2,840,10036,1,14,2,5,1,52};
UINT4 Dot11STAStatisticsFailedAMSDUCount [ ] ={1,2,840,10036,1,14,2,5,1,53};
UINT4 Dot11STAStatisticsRetryAMSDUCount [ ] ={1,2,840,10036,1,14,2,5,1,54};
UINT4 Dot11STAStatisticsMultipleRetryAMSDUCount [ ] ={1,2,840,10036,1,14,2,5,1,55};
UINT4 Dot11STAStatisticsTransmittedOctetsInAMSDUCount [ ] ={1,2,840,10036,1,14,2,5,1,56};
UINT4 Dot11STAStatisticsAMSDUAckFailureCount [ ] ={1,2,840,10036,1,14,2,5,1,57};
UINT4 Dot11STAStatisticsReceivedAMSDUCount [ ] ={1,2,840,10036,1,14,2,5,1,58};
UINT4 Dot11STAStatisticsReceivedOctetsInAMSDUCount [ ] ={1,2,840,10036,1,14,2,5,1,59};
UINT4 Dot11STAStatisticsTransmittedAMPDUCount [ ] ={1,2,840,10036,1,14,2,5,1,60};
UINT4 Dot11STAStatisticsTransmittedMPDUsInAMPDUCount [ ] ={1,2,840,10036,1,14,2,5,1,61};
UINT4 Dot11STAStatisticsTransmittedOctetsInAMPDUCount [ ] ={1,2,840,10036,1,14,2,5,1,62};
UINT4 Dot11STAStatisticsAMPDUReceivedCount [ ] ={1,2,840,10036,1,14,2,5,1,63};
UINT4 Dot11STAStatisticsMPDUInReceivedAMPDUCount [ ] ={1,2,840,10036,1,14,2,5,1,64};
UINT4 Dot11STAStatisticsReceivedOctetsInAMPDUCount [ ] ={1,2,840,10036,1,14,2,5,1,65};
UINT4 Dot11STAStatisticsAMPDUDelimiterCRCErrorCount [ ] ={1,2,840,10036,1,14,2,5,1,66};
UINT4 Dot11STAStatisticsImplicitBARFailureCount [ ] ={1,2,840,10036,1,14,2,5,1,67};
UINT4 Dot11STAStatisticsExplicitBARFailureCount [ ] ={1,2,840,10036,1,14,2,5,1,68};
UINT4 Dot11STAStatisticsChannelWidthSwitchCount [ ] ={1,2,840,10036,1,14,2,5,1,69};
UINT4 Dot11STAStatisticsTwentyMHzFrameTransmittedCount [ ] ={1,2,840,10036,1,14,2,5,1,70};
UINT4 Dot11STAStatisticsFortyMHzFrameTransmittedCount [ ] ={1,2,840,10036,1,14,2,5,1,71};
UINT4 Dot11STAStatisticsTwentyMHzFrameReceivedCount [ ] ={1,2,840,10036,1,14,2,5,1,72};
UINT4 Dot11STAStatisticsFortyMHzFrameReceivedCount [ ] ={1,2,840,10036,1,14,2,5,1,73};
UINT4 Dot11STAStatisticsPSMPUTTGrantDuration [ ] ={1,2,840,10036,1,14,2,5,1,74};
UINT4 Dot11STAStatisticsPSMPUTTUsedDuration [ ] ={1,2,840,10036,1,14,2,5,1,75};
UINT4 Dot11STAStatisticsGrantedRDGUsedCount [ ] ={1,2,840,10036,1,14,2,5,1,76};
UINT4 Dot11STAStatisticsGrantedRDGUnusedCount [ ] ={1,2,840,10036,1,14,2,5,1,77};
UINT4 Dot11STAStatisticsTransmittedFramesInGrantedRDGCount [ ] ={1,2,840,10036,1,14,2,5,1,78};
UINT4 Dot11STAStatisticsTransmittedOctetsInGrantedRDGCount [ ] ={1,2,840,10036,1,14,2,5,1,79};
UINT4 Dot11STAStatisticsDualCTSSuccessCount [ ] ={1,2,840,10036,1,14,2,5,1,80};
UINT4 Dot11STAStatisticsDualCTSFailureCount [ ] ={1,2,840,10036,1,14,2,5,1,81};
UINT4 Dot11STAStatisticsRTSLSIGSuccessCount [ ] ={1,2,840,10036,1,14,2,5,1,82};
UINT4 Dot11STAStatisticsRTSLSIGFailureCount [ ] ={1,2,840,10036,1,14,2,5,1,83};
UINT4 Dot11STAStatisticsBeamformingFrameCount [ ] ={1,2,840,10036,1,14,2,5,1,84};
UINT4 Dot11STAStatisticsSTBCCTSSuccessCount [ ] ={1,2,840,10036,1,14,2,5,1,85};
UINT4 Dot11STAStatisticsSTBCCTSFailureCount [ ] ={1,2,840,10036,1,14,2,5,1,86};
UINT4 Dot11STAStatisticsnonSTBCCTSSuccessCount [ ] ={1,2,840,10036,1,14,2,5,1,87};
UINT4 Dot11STAStatisticsnonSTBCCTSFailureCount [ ] ={1,2,840,10036,1,14,2,5,1,88};
UINT4 Dot11LCIReportIndex [ ] ={1,2,840,10036,1,14,2,6,1,1};
UINT4 Dot11LCIReportToken [ ] ={1,2,840,10036,1,14,2,6,1,2};
UINT4 Dot11LCIIfIndex [ ] ={1,2,840,10036,1,14,2,6,1,3};
UINT4 Dot11LCISTAAddress [ ] ={1,2,840,10036,1,14,2,6,1,4};
UINT4 Dot11LCILatitudeResolution [ ] ={1,2,840,10036,1,14,2,6,1,5};
UINT4 Dot11LCILatitudeInteger [ ] ={1,2,840,10036,1,14,2,6,1,6};
UINT4 Dot11LCILatitudeFraction [ ] ={1,2,840,10036,1,14,2,6,1,7};
UINT4 Dot11LCILongitudeResolution [ ] ={1,2,840,10036,1,14,2,6,1,8};
UINT4 Dot11LCILongitudeInteger [ ] ={1,2,840,10036,1,14,2,6,1,9};
UINT4 Dot11LCILongitudeFraction [ ] ={1,2,840,10036,1,14,2,6,1,10};
UINT4 Dot11LCIAltitudeType [ ] ={1,2,840,10036,1,14,2,6,1,11};
UINT4 Dot11LCIAltitudeResolution [ ] ={1,2,840,10036,1,14,2,6,1,12};
UINT4 Dot11LCIAltitudeInteger [ ] ={1,2,840,10036,1,14,2,6,1,13};
UINT4 Dot11LCIAltitudeFraction [ ] ={1,2,840,10036,1,14,2,6,1,14};
UINT4 Dot11LCIDatum [ ] ={1,2,840,10036,1,14,2,6,1,15};
UINT4 Dot11LCIAzimuthType [ ] ={1,2,840,10036,1,14,2,6,1,16};
UINT4 Dot11LCIAzimuthResolution [ ] ={1,2,840,10036,1,14,2,6,1,17};
UINT4 Dot11LCIAzimuth [ ] ={1,2,840,10036,1,14,2,6,1,18};
UINT4 Dot11LCIVendorSpecific [ ] ={1,2,840,10036,1,14,2,6,1,19};
UINT4 Dot11LCIRprtMeasurementMode [ ] ={1,2,840,10036,1,14,2,6,1,20};
UINT4 Dot11TransmitStreamRprtIndex [ ] ={1,2,840,10036,1,14,2,7,1,1};
UINT4 Dot11TransmitStreamRprtRqstToken [ ] ={1,2,840,10036,1,14,2,7,1,2};
UINT4 Dot11TransmitStreamRprtIfIndex [ ] ={1,2,840,10036,1,14,2,7,1,3};
UINT4 Dot11TransmitStreamMeasuringSTAAddr [ ] ={1,2,840,10036,1,14,2,7,1,4};
UINT4 Dot11TransmitStreamRprtActualStartTime [ ] ={1,2,840,10036,1,14,2,7,1,5};
UINT4 Dot11TransmitStreamRprtMeasurementDuration [ ] ={1,2,840,10036,1,14,2,7,1,6};
UINT4 Dot11TransmitStreamRprtPeerSTAAddress [ ] ={1,2,840,10036,1,14,2,7,1,7};
UINT4 Dot11TransmitStreamRprtTID [ ] ={1,2,840,10036,1,14,2,7,1,8};
UINT4 Dot11TransmitStreamRprtAverageQueueDelay [ ] ={1,2,840,10036,1,14,2,7,1,9};
UINT4 Dot11TransmitStreamRprtAverageTransmitDelay [ ] ={1,2,840,10036,1,14,2,7,1,10};
UINT4 Dot11TransmitStreamRprtTransmittedMSDUCount [ ] ={1,2,840,10036,1,14,2,7,1,11};
UINT4 Dot11TransmitStreamRprtMSDUDiscardedCount [ ] ={1,2,840,10036,1,14,2,7,1,12};
UINT4 Dot11TransmitStreamRprtMSDUFailedCount [ ] ={1,2,840,10036,1,14,2,7,1,13};
UINT4 Dot11TransmitStreamRprtMultipleRetryCount [ ] ={1,2,840,10036,1,14,2,7,1,14};
UINT4 Dot11TransmitStreamRprtCFPollsLostCount [ ] ={1,2,840,10036,1,14,2,7,1,15};
UINT4 Dot11TransmitStreamRprtBin0Range [ ] ={1,2,840,10036,1,14,2,7,1,16};
UINT4 Dot11TransmitStreamRprtDelayHistogram [ ] ={1,2,840,10036,1,14,2,7,1,17};
UINT4 Dot11TransmitStreamRprtReason [ ] ={1,2,840,10036,1,14,2,7,1,18};
UINT4 Dot11TransmitStreamRprtVendorSpecific [ ] ={1,2,840,10036,1,14,2,7,1,19};
UINT4 Dot11TransmitStreamRprtMeasurementMode [ ] ={1,2,840,10036,1,14,2,7,1,20};
UINT4 Dot11APChannelReportIndex [ ] ={1,2,840,10036,1,14,3,1,1,1};
UINT4 Dot11APChannelReportIfIndex [ ] ={1,2,840,10036,1,14,3,1,1,2};
UINT4 Dot11APChannelReportOperatingClass [ ] ={1,2,840,10036,1,14,3,1,1,3};
UINT4 Dot11APChannelReportChannelList [ ] ={1,2,840,10036,1,14,3,1,1,4};
UINT4 Dot11RMNeighborReportNextIndex [ ] ={1,2,840,10036,1,14,3,2};
UINT4 Dot11RMNeighborReportIndex [ ] ={1,2,840,10036,1,14,3,3,1,1};
UINT4 Dot11RMNeighborReportIfIndex [ ] ={1,2,840,10036,1,14,3,3,1,2};
UINT4 Dot11RMNeighborReportBSSID [ ] ={1,2,840,10036,1,14,3,3,1,3};
UINT4 Dot11RMNeighborReportAPReachability [ ] ={1,2,840,10036,1,14,3,3,1,4};
UINT4 Dot11RMNeighborReportSecurity [ ] ={1,2,840,10036,1,14,3,3,1,5};
UINT4 Dot11RMNeighborReportCapSpectrumMgmt [ ] ={1,2,840,10036,1,14,3,3,1,6};
UINT4 Dot11RMNeighborReportCapQoS [ ] ={1,2,840,10036,1,14,3,3,1,7};
UINT4 Dot11RMNeighborReportCapAPSD [ ] ={1,2,840,10036,1,14,3,3,1,8};
UINT4 Dot11RMNeighborReportCapRM [ ] ={1,2,840,10036,1,14,3,3,1,9};
UINT4 Dot11RMNeighborReportCapDelayBlockAck [ ] ={1,2,840,10036,1,14,3,3,1,10};
UINT4 Dot11RMNeighborReportCapImmediateBlockAck [ ] ={1,2,840,10036,1,14,3,3,1,11};
UINT4 Dot11RMNeighborReportKeyScope [ ] ={1,2,840,10036,1,14,3,3,1,12};
UINT4 Dot11RMNeighborReportOperatingClass [ ] ={1,2,840,10036,1,14,3,3,1,13};
UINT4 Dot11RMNeighborReportChannelNumber [ ] ={1,2,840,10036,1,14,3,3,1,14};
UINT4 Dot11RMNeighborReportPhyType [ ] ={1,2,840,10036,1,14,3,3,1,15};
UINT4 Dot11RMNeighborReportNeighborTSFInfo [ ] ={1,2,840,10036,1,14,3,3,1,16};
UINT4 Dot11RMNeighborReportPilotInterval [ ] ={1,2,840,10036,1,14,3,3,1,17};
UINT4 Dot11RMNeighborReportPilotMultipleBSSID [ ] ={1,2,840,10036,1,14,3,3,1,18};
UINT4 Dot11RMNeighborReportRMEnabledCapabilities [ ] ={1,2,840,10036,1,14,3,3,1,19};
UINT4 Dot11RMNeighborReportVendorSpecific [ ] ={1,2,840,10036,1,14,3,3,1,20};
UINT4 Dot11RMNeighborReportRowStatus [ ] ={1,2,840,10036,1,14,3,3,1,21};
UINT4 Dot11RMNeighborReportMobilityDomain [ ] ={1,2,840,10036,1,14,3,3,1,22};
UINT4 Dot11RMNeighborReportCapHT [ ] ={1,2,840,10036,1,14,3,3,1,23};
UINT4 Dot11RMNeighborReportHTLDPCCodingCap [ ] ={1,2,840,10036,1,14,3,3,1,24};
UINT4 Dot11RMNeighborReportHTSupportedChannelWidthSet [ ] ={1,2,840,10036,1,14,3,3,1,25};
UINT4 Dot11RMNeighborReportHTSMPowerSave [ ] ={1,2,840,10036,1,14,3,3,1,26};
UINT4 Dot11RMNeighborReportHTGreenfield [ ] ={1,2,840,10036,1,14,3,3,1,27};
UINT4 Dot11RMNeighborReportHTShortGIfor20MHz [ ] ={1,2,840,10036,1,14,3,3,1,28};
UINT4 Dot11RMNeighborReportHTShortGIfor40MHz [ ] ={1,2,840,10036,1,14,3,3,1,29};
UINT4 Dot11RMNeighborReportHTTxSTBC [ ] ={1,2,840,10036,1,14,3,3,1,30};
UINT4 Dot11RMNeighborReportHTRxSTBC [ ] ={1,2,840,10036,1,14,3,3,1,31};
UINT4 Dot11RMNeighborReportHTDelayedBlockAck [ ] ={1,2,840,10036,1,14,3,3,1,32};
UINT4 Dot11RMNeighborReportHTMaxAMSDULength [ ] ={1,2,840,10036,1,14,3,3,1,33};
UINT4 Dot11RMNeighborReportHTDSSCCKModein40MHz [ ] ={1,2,840,10036,1,14,3,3,1,34};
UINT4 Dot11RMNeighborReportHTFortyMHzIntolerant [ ] ={1,2,840,10036,1,14,3,3,1,35};
UINT4 Dot11RMNeighborReportHTLSIGTXOPProtectionSupport [ ] ={1,2,840,10036,1,14,3,3,1,36};
UINT4 Dot11RMNeighborReportHTMaxAMPDULengthExponent [ ] ={1,2,840,10036,1,14,3,3,1,37};
UINT4 Dot11RMNeighborReportHTMinMPDUStartSpacing [ ] ={1,2,840,10036,1,14,3,3,1,38};
UINT4 Dot11RMNeighborReportHTRxMCSBitMask [ ] ={1,2,840,10036,1,14,3,3,1,39};
UINT4 Dot11RMNeighborReportHTRxHighestSupportedDataRate [ ] ={1,2,840,10036,1,14,3,3,1,40};
UINT4 Dot11RMNeighborReportHTTxMCSSetDefined [ ] ={1,2,840,10036,1,14,3,3,1,41};
UINT4 Dot11RMNeighborReportHTTxRxMCSSetNotEqual [ ] ={1,2,840,10036,1,14,3,3,1,42};
UINT4 Dot11RMNeighborReportHTTxMaxNumberSpatialStreamsSupported [ ] ={1,2,840,10036,1,14,3,3,1,43};
UINT4 Dot11RMNeighborReportHTTxUnequalModulationSupported [ ] ={1,2,840,10036,1,14,3,3,1,44};
UINT4 Dot11RMNeighborReportHTPCO [ ] ={1,2,840,10036,1,14,3,3,1,45};
UINT4 Dot11RMNeighborReportHTPCOTransitionTime [ ] ={1,2,840,10036,1,14,3,3,1,46};
UINT4 Dot11RMNeighborReportHTMCSFeedback [ ] ={1,2,840,10036,1,14,3,3,1,47};
UINT4 Dot11RMNeighborReportHTCSupport [ ] ={1,2,840,10036,1,14,3,3,1,48};
UINT4 Dot11RMNeighborReportHTRDResponder [ ] ={1,2,840,10036,1,14,3,3,1,49};
UINT4 Dot11RMNeighborReportHTImplictTransmitBeamformingReceivingCap [ ] ={1,2,840,10036,1,14,3,3,1,50};
UINT4 Dot11RMNeighborReportHTReceiveStaggeredSoundingCap [ ] ={1,2,840,10036,1,14,3,3,1,51};
UINT4 Dot11RMNeighborReportHTTransmitStaggeredSoundingCap [ ] ={1,2,840,10036,1,14,3,3,1,52};
UINT4 Dot11RMNeighborReportHTReceiveNDPCap [ ] ={1,2,840,10036,1,14,3,3,1,53};
UINT4 Dot11RMNeighborReportHTTransmitNDPCap [ ] ={1,2,840,10036,1,14,3,3,1,54};
UINT4 Dot11RMNeighborReportHTImplicitTransmitBeamformingCap [ ] ={1,2,840,10036,1,14,3,3,1,55};
UINT4 Dot11RMNeighborReportHTTransmitBeamformingCalibration [ ] ={1,2,840,10036,1,14,3,3,1,56};
UINT4 Dot11RMNeighborReportHTExplicitCSITransmitBeamformingCap [ ] ={1,2,840,10036,1,14,3,3,1,57};
UINT4 Dot11RMNeighborReportHTExplicitNonCompressedSteeringCap [ ] ={1,2,840,10036,1,14,3,3,1,58};
UINT4 Dot11RMNeighborReportHTExplicitCompressedSteeringCap [ ] ={1,2,840,10036,1,14,3,3,1,59};
UINT4 Dot11RMNeighborReportHTExplicitTransmitBeamformingFeedback [ ] ={1,2,840,10036,1,14,3,3,1,60};
UINT4 Dot11RMNbRprtHTExplicitNonCompressedBeamformingFeedbackCap [ ] ={1,2,840,10036,1,14,3,3,1,61};
UINT4 Dot11RMNeighborReportHTExplicitCompressedBeamformingFeedbackCap [ ] ={1,2,840,10036,1,14,3,3,1,62};
UINT4 Dot11RMNeighborReportHTTransmitBeamformingMinimalGrouping [ ] ={1,2,840,10036,1,14,3,3,1,63};
UINT4 Dot11RMNbRprtHTCSINumberofTxBeamformingAntennasSuppt [ ] ={1,2,840,10036,1,14,3,3,1,64};
UINT4 Dot11RMNbRprtHTNonCompressedSteeringNumofTxBmfmingAntennasSuppt [ ] ={1,2,840,10036,1,14,3,3,1,65};
UINT4 Dot11RMNbRprtHTCompressedSteeringNumberofTxBmfmingAntennasSuppt [ ] ={1,2,840,10036,1,14,3,3,1,66};
UINT4 Dot11RMNbRprtHTCSIMaxNumberofRowsTxBeamformingSuppt [ ] ={1,2,840,10036,1,14,3,3,1,67};
UINT4 Dot11RMNeighborReportHTTransmitBeamformingChannelEstimationCap [ ] ={1,2,840,10036,1,14,3,3,1,68};
UINT4 Dot11RMNeighborReportHTAntSelectionCap [ ] ={1,2,840,10036,1,14,3,3,1,69};
UINT4 Dot11RMNeighborReportHTExplicitCSIFeedbackBasedTxASELCap [ ] ={1,2,840,10036,1,14,3,3,1,70};
UINT4 Dot11RMNeighborReportHTAntIndicesFeedbackBasedTxASELCap [ ] ={1,2,840,10036,1,14,3,3,1,71};
UINT4 Dot11RMNeighborReportHTExplicitCSIFeedbackBasedCap [ ] ={1,2,840,10036,1,14,3,3,1,72};
UINT4 Dot11RMNeighborReportHTAntIndicesFeedbackCap [ ] ={1,2,840,10036,1,14,3,3,1,73};
UINT4 Dot11RMNeighborReportHTRxASELCap [ ] ={1,2,840,10036,1,14,3,3,1,74};
UINT4 Dot11RMNeighborReportHTTxSoundingPPDUsCap [ ] ={1,2,840,10036,1,14,3,3,1,75};
UINT4 Dot11RMNeighborReportHTInfoPrimaryChannel [ ] ={1,2,840,10036,1,14,3,3,1,76};
UINT4 Dot11RMNeighborReportHTInfoSecChannelOffset [ ] ={1,2,840,10036,1,14,3,3,1,77};
UINT4 Dot11RMNeighborReportHTInfoSTAChannelWidth [ ] ={1,2,840,10036,1,14,3,3,1,78};
UINT4 Dot11RMNeighborReportHTInfoRIFSMode [ ] ={1,2,840,10036,1,14,3,3,1,79};
UINT4 Dot11RMNeighborReportHTInfoProtection [ ] ={1,2,840,10036,1,14,3,3,1,80};
UINT4 Dot11RMNeighborReportHTInfoNonGreenfieldHTSTAsPresent [ ] ={1,2,840,10036,1,14,3,3,1,81};
UINT4 Dot11RMNeighborReportHTInfoOBSSNonHTSTAsPresent [ ] ={1,2,840,10036,1,14,3,3,1,82};
UINT4 Dot11RMNeighborReportHTInfoDualBeacon [ ] ={1,2,840,10036,1,14,3,3,1,83};
UINT4 Dot11RMNeighborReportHTInfoDualCTSProtection [ ] ={1,2,840,10036,1,14,3,3,1,84};
UINT4 Dot11RMNeighborReportHTInfoSTBCBeacon [ ] ={1,2,840,10036,1,14,3,3,1,85};
UINT4 Dot11RMNeighborReportHTInfoLSIGTXOPProtectionSup [ ] ={1,2,840,10036,1,14,3,3,1,86};
UINT4 Dot11RMNeighborReportHTInfoPCOActive [ ] ={1,2,840,10036,1,14,3,3,1,87};
UINT4 Dot11RMNeighborReportHTInfoPCOPhase [ ] ={1,2,840,10036,1,14,3,3,1,88};
UINT4 Dot11RMNeighborReportHTInfoBasicMCSSet [ ] ={1,2,840,10036,1,14,3,3,1,89};
UINT4 Dot11RMNeighborReportHTSecChannelOffset [ ] ={1,2,840,10036,1,14,3,3,1,90};
UINT4 Dot11RMNeighborReportExtCapPSMPSupport [ ] ={1,2,840,10036,1,14,3,3,1,91};
UINT4 Dot11RMNeighborReportExtCapSPSMPSup [ ] ={1,2,840,10036,1,14,3,3,1,92};
UINT4 Dot11RMNeighborReportExtCapServiceIntervalGranularity [ ] ={1,2,840,10036,1,14,3,3,1,93};
UINT4 Dot11RMNeighborReportBSSTransitCandPreference [ ] ={1,2,840,10036,1,14,3,3,1,94};
UINT4 Dot11RMNeighborReportBSSTerminationTSF [ ] ={1,2,840,10036,1,14,3,3,1,95};
UINT4 Dot11RMNeighborReportBSSTerminationDuration [ ] ={1,2,840,10036,1,14,3,3,1,96};
UINT4 Dot11FastBSSTransitionActivated [ ] ={1,2,840,10036,1,15,1,1};
UINT4 Dot11FTMobilityDomainID [ ] ={1,2,840,10036,1,15,1,2};
UINT4 Dot11FTOverDSActivated [ ] ={1,2,840,10036,1,15,1,3};
UINT4 Dot11FTResourceRequestSupported [ ] ={1,2,840,10036,1,15,1,4};
UINT4 Dot11FTR0KeyHolderID [ ] ={1,2,840,10036,1,15,1,5};
UINT4 Dot11FTR0KeyLifetime [ ] ={1,2,840,10036,1,15,1,6};
UINT4 Dot11FTR1KeyHolderID [ ] ={1,2,840,10036,1,15,1,7};
UINT4 Dot11FTReassociationDeadline [ ] ={1,2,840,10036,1,15,1,8};
UINT4 Dot11LCIDSEIndex [ ] ={1,2,840,10036,1,16,1,1};
UINT4 Dot11LCIDSEIfIndex [ ] ={1,2,840,10036,1,16,1,2};
UINT4 Dot11LCIDSECurrentOperatingClass [ ] ={1,2,840,10036,1,16,1,3};
UINT4 Dot11LCIDSELatitudeResolution [ ] ={1,2,840,10036,1,16,1,4};
UINT4 Dot11LCIDSELatitudeInteger [ ] ={1,2,840,10036,1,16,1,5};
UINT4 Dot11LCIDSELatitudeFraction [ ] ={1,2,840,10036,1,16,1,6};
UINT4 Dot11LCIDSELongitudeResolution [ ] ={1,2,840,10036,1,16,1,7};
UINT4 Dot11LCIDSELongitudeInteger [ ] ={1,2,840,10036,1,16,1,8};
UINT4 Dot11LCIDSELongitudeFraction [ ] ={1,2,840,10036,1,16,1,9};
UINT4 Dot11LCIDSEAltitudeType [ ] ={1,2,840,10036,1,16,1,10};
UINT4 Dot11LCIDSEAltitudeResolution [ ] ={1,2,840,10036,1,16,1,11};
UINT4 Dot11LCIDSEAltitudeInteger [ ] ={1,2,840,10036,1,16,1,12};
UINT4 Dot11LCIDSEAltitudeFraction [ ] ={1,2,840,10036,1,16,1,13};
UINT4 Dot11LCIDSEDatum [ ] ={1,2,840,10036,1,16,1,14};
UINT4 Dot11RegLocAgreement [ ] ={1,2,840,10036,1,16,1,15};
UINT4 Dot11RegLocDSE [ ] ={1,2,840,10036,1,16,1,16};
UINT4 Dot11DependentSTA [ ] ={1,2,840,10036,1,16,1,17};
UINT4 Dot11DependentEnablementIdentifier [ ] ={1,2,840,10036,1,16,1,18};
UINT4 Dot11DSEEnablementTimeLimit [ ] ={1,2,840,10036,1,16,1,19};
UINT4 Dot11DSEEnablementFailHoldTime [ ] ={1,2,840,10036,1,16,1,20};
UINT4 Dot11DSERenewalTime [ ] ={1,2,840,10036,1,16,1,21};
UINT4 Dot11DSETransmitDivisor [ ] ={1,2,840,10036,1,16,1,22};
UINT4 Dot11HTOperationalMCSSet [ ] ={1,2,840,10036,1,17,1,1};
UINT4 Dot11MIMOPowerSave [ ] ={1,2,840,10036,1,17,1,2};
UINT4 Dot11NDelayedBlockAckOptionImplemented [ ] ={1,2,840,10036,1,17,1,3};
UINT4 Dot11MaxAMSDULength [ ] ={1,2,840,10036,1,17,1,4};
UINT4 Dot11STBCControlFrameOptionImplemented [ ] ={1,2,840,10036,1,17,1,5};
UINT4 Dot11LsigTxopProtectionOptionImplemented [ ] ={1,2,840,10036,1,17,1,6};
UINT4 Dot11MaxRxAMPDUFactor [ ] ={1,2,840,10036,1,17,1,7};
UINT4 Dot11MinimumMPDUStartSpacing [ ] ={1,2,840,10036,1,17,1,8};
UINT4 Dot11PCOOptionImplemented [ ] ={1,2,840,10036,1,17,1,9};
UINT4 Dot11TransitionTime [ ] ={1,2,840,10036,1,17,1,10};
UINT4 Dot11MCSFeedbackOptionImplemented [ ] ={1,2,840,10036,1,17,1,11};
UINT4 Dot11HTControlFieldSupported [ ] ={1,2,840,10036,1,17,1,12};
UINT4 Dot11RDResponderOptionImplemented [ ] ={1,2,840,10036,1,17,1,13};
UINT4 Dot11SPPAMSDUCapable [ ] ={1,2,840,10036,1,17,1,14};
UINT4 Dot11SPPAMSDURequired [ ] ={1,2,840,10036,1,17,1,15};
UINT4 Dot11FortyMHzOptionImplemented [ ] ={1,2,840,10036,1,17,1,16};
UINT4 Dot11MgmtOptionLocationActivated [ ] ={1,2,840,10036,1,18,1,1};
UINT4 Dot11MgmtOptionFMSImplemented [ ] ={1,2,840,10036,1,18,1,2};
UINT4 Dot11MgmtOptionFMSActivated [ ] ={1,2,840,10036,1,18,1,3};
UINT4 Dot11MgmtOptionEventsActivated [ ] ={1,2,840,10036,1,18,1,4};
UINT4 Dot11MgmtOptionDiagnosticsActivated [ ] ={1,2,840,10036,1,18,1,5};
UINT4 Dot11MgmtOptionMultiBSSIDImplemented [ ] ={1,2,840,10036,1,18,1,6};
UINT4 Dot11MgmtOptionMultiBSSIDActivated [ ] ={1,2,840,10036,1,18,1,7};
UINT4 Dot11MgmtOptionTFSImplemented [ ] ={1,2,840,10036,1,18,1,8};
UINT4 Dot11MgmtOptionTFSActivated [ ] ={1,2,840,10036,1,18,1,9};
UINT4 Dot11MgmtOptionWNMSleepModeImplemented [ ] ={1,2,840,10036,1,18,1,10};
UINT4 Dot11MgmtOptionWNMSleepModeActivated [ ] ={1,2,840,10036,1,18,1,11};
UINT4 Dot11MgmtOptionTIMBroadcastImplemented [ ] ={1,2,840,10036,1,18,1,12};
UINT4 Dot11MgmtOptionTIMBroadcastActivated [ ] ={1,2,840,10036,1,18,1,13};
UINT4 Dot11MgmtOptionProxyARPImplemented [ ] ={1,2,840,10036,1,18,1,14};
UINT4 Dot11MgmtOptionProxyARPActivated [ ] ={1,2,840,10036,1,18,1,15};
UINT4 Dot11MgmtOptionBSSTransitionImplemented [ ] ={1,2,840,10036,1,18,1,16};
UINT4 Dot11MgmtOptionBSSTransitionActivated [ ] ={1,2,840,10036,1,18,1,17};
UINT4 Dot11MgmtOptionQoSTrafficCapabilityImplemented [ ] ={1,2,840,10036,1,18,1,18};
UINT4 Dot11MgmtOptionQoSTrafficCapabilityActivated [ ] ={1,2,840,10036,1,18,1,19};
UINT4 Dot11MgmtOptionACStationCountImplemented [ ] ={1,2,840,10036,1,18,1,20};
UINT4 Dot11MgmtOptionACStationCountActivated [ ] ={1,2,840,10036,1,18,1,21};
UINT4 Dot11MgmtOptionCoLocIntfReportingImplemented [ ] ={1,2,840,10036,1,18,1,22};
UINT4 Dot11MgmtOptionCoLocIntfReportingActivated [ ] ={1,2,840,10036,1,18,1,23};
UINT4 Dot11MgmtOptionMotionDetectionImplemented [ ] ={1,2,840,10036,1,18,1,24};
UINT4 Dot11MgmtOptionMotionDetectionActivated [ ] ={1,2,840,10036,1,18,1,25};
UINT4 Dot11MgmtOptionTODImplemented [ ] ={1,2,840,10036,1,18,1,26};
UINT4 Dot11MgmtOptionTODActivated [ ] ={1,2,840,10036,1,18,1,27};
UINT4 Dot11MgmtOptionTimingMsmtImplemented [ ] ={1,2,840,10036,1,18,1,28};
UINT4 Dot11MgmtOptionTimingMsmtActivated [ ] ={1,2,840,10036,1,18,1,29};
UINT4 Dot11MgmtOptionChannelUsageImplemented [ ] ={1,2,840,10036,1,18,1,30};
UINT4 Dot11MgmtOptionChannelUsageActivated [ ] ={1,2,840,10036,1,18,1,31};
UINT4 Dot11MgmtOptionTriggerSTAStatisticsActivated [ ] ={1,2,840,10036,1,18,1,32};
UINT4 Dot11MgmtOptionSSIDListImplemented [ ] ={1,2,840,10036,1,18,1,33};
UINT4 Dot11MgmtOptionSSIDListActivated [ ] ={1,2,840,10036,1,18,1,34};
UINT4 Dot11MgmtOptionMulticastDiagnosticsActivated [ ] ={1,2,840,10036,1,18,1,35};
UINT4 Dot11MgmtOptionLocationTrackingImplemented [ ] ={1,2,840,10036,1,18,1,36};
UINT4 Dot11MgmtOptionLocationTrackingActivated [ ] ={1,2,840,10036,1,18,1,37};
UINT4 Dot11MgmtOptionDMSImplemented [ ] ={1,2,840,10036,1,18,1,38};
UINT4 Dot11MgmtOptionDMSActivated [ ] ={1,2,840,10036,1,18,1,39};
UINT4 Dot11MgmtOptionUAPSDCoexistenceImplemented [ ] ={1,2,840,10036,1,18,1,40};
UINT4 Dot11MgmtOptionUAPSDCoexistenceActivated [ ] ={1,2,840,10036,1,18,1,41};
UINT4 Dot11MgmtOptionWNMNotificationImplemented [ ] ={1,2,840,10036,1,18,1,42};
UINT4 Dot11MgmtOptionWNMNotificationActivated [ ] ={1,2,840,10036,1,18,1,43};
UINT4 Dot11MgmtOptionUTCTSFOffsetImplemented [ ] ={1,2,840,10036,1,18,1,44};
UINT4 Dot11MgmtOptionUTCTSFOffsetActivated [ ] ={1,2,840,10036,1,18,1,45};
UINT4 Dot11LocationServicesNextIndex [ ] ={1,2,840,10036,1,19};
UINT4 Dot11LocationServicesIndex [ ] ={1,2,840,10036,1,20,1,1};
UINT4 Dot11LocationServicesMACAddress [ ] ={1,2,840,10036,1,20,1,2};
UINT4 Dot11LocationServicesLIPIndicationMulticastAddress [ ] ={1,2,840,10036,1,20,1,3};
UINT4 Dot11LocationServicesLIPReportIntervalUnits [ ] ={1,2,840,10036,1,20,1,4};
UINT4 Dot11LocationServicesLIPNormalReportInterval [ ] ={1,2,840,10036,1,20,1,5};
UINT4 Dot11LocationServicesLIPNormalFramesperChannel [ ] ={1,2,840,10036,1,20,1,6};
UINT4 Dot11LocationServicesLIPInMotionReportInterval [ ] ={1,2,840,10036,1,20,1,7};
UINT4 Dot11LocationServicesLIPInMotionFramesperChannel [ ] ={1,2,840,10036,1,20,1,8};
UINT4 Dot11LocationServicesLIPBurstInterframeInterval [ ] ={1,2,840,10036,1,20,1,9};
UINT4 Dot11LocationServicesLIPTrackingDuration [ ] ={1,2,840,10036,1,20,1,10};
UINT4 Dot11LocationServicesLIPEssDetectionInterval [ ] ={1,2,840,10036,1,20,1,11};
UINT4 Dot11LocationServicesLocationIndicationChannelList [ ] ={1,2,840,10036,1,20,1,12};
UINT4 Dot11LocationServicesLocationIndicationBroadcastDataRate [ ] ={1,2,840,10036,1,20,1,13};
UINT4 Dot11LocationServicesLocationIndicationOptionsUsed [ ] ={1,2,840,10036,1,20,1,14};
UINT4 Dot11LocationServicesLocationIndicationIndicationParameters [ ] ={1,2,840,10036,1,20,1,15};
UINT4 Dot11LocationServicesLocationStatus [ ] ={1,2,840,10036,1,20,1,16};
UINT4 Dot11WirelessMGTEventIndex [ ] ={1,2,840,10036,1,21,1,1};
UINT4 Dot11WirelessMGTEventMACAddress [ ] ={1,2,840,10036,1,21,1,2};
UINT4 Dot11WirelessMGTEventType [ ] ={1,2,840,10036,1,21,1,3};
UINT4 Dot11WirelessMGTEventStatus [ ] ={1,2,840,10036,1,21,1,4};
UINT4 Dot11WirelessMGTEventTSF [ ] ={1,2,840,10036,1,21,1,5};
UINT4 Dot11WirelessMGTEventUTCOffset [ ] ={1,2,840,10036,1,21,1,6};
UINT4 Dot11WirelessMGTEventTimeError [ ] ={1,2,840,10036,1,21,1,7};
UINT4 Dot11WirelessMGTEventTransitionSourceBSSID [ ] ={1,2,840,10036,1,21,1,8};
UINT4 Dot11WirelessMGTEventTransitionTargetBSSID [ ] ={1,2,840,10036,1,21,1,9};
UINT4 Dot11WirelessMGTEventTransitionTime [ ] ={1,2,840,10036,1,21,1,10};
UINT4 Dot11WirelessMGTEventTransitionReason [ ] ={1,2,840,10036,1,21,1,11};
UINT4 Dot11WirelessMGTEventTransitionResult [ ] ={1,2,840,10036,1,21,1,12};
UINT4 Dot11WirelessMGTEventTransitionSourceRCPI [ ] ={1,2,840,10036,1,21,1,13};
UINT4 Dot11WirelessMGTEventTransitionSourceRSNI [ ] ={1,2,840,10036,1,21,1,14};
UINT4 Dot11WirelessMGTEventTransitionTargetRCPI [ ] ={1,2,840,10036,1,21,1,15};
UINT4 Dot11WirelessMGTEventTransitionTargetRSNI [ ] ={1,2,840,10036,1,21,1,16};
UINT4 Dot11WirelessMGTEventRSNATargetBSSID [ ] ={1,2,840,10036,1,21,1,17};
UINT4 Dot11WirelessMGTEventRSNAAuthenticationType [ ] ={1,2,840,10036,1,21,1,18};
UINT4 Dot11WirelessMGTEventRSNAEAPMethod [ ] ={1,2,840,10036,1,21,1,19};
UINT4 Dot11WirelessMGTEventRSNAResult [ ] ={1,2,840,10036,1,21,1,20};
UINT4 Dot11WirelessMGTEventRSNARSNElement [ ] ={1,2,840,10036,1,21,1,21};
UINT4 Dot11WirelessMGTEventPeerSTAAddress [ ] ={1,2,840,10036,1,21,1,22};
UINT4 Dot11WirelessMGTEventPeerOperatingClass [ ] ={1,2,840,10036,1,21,1,23};
UINT4 Dot11WirelessMGTEventPeerChannelNumber [ ] ={1,2,840,10036,1,21,1,24};
UINT4 Dot11WirelessMGTEventPeerSTATxPower [ ] ={1,2,840,10036,1,21,1,25};
UINT4 Dot11WirelessMGTEventPeerConnectionTime [ ] ={1,2,840,10036,1,21,1,26};
UINT4 Dot11WirelessMGTEventPeerPeerStatus [ ] ={1,2,840,10036,1,21,1,27};
UINT4 Dot11WirelessMGTEventWNMLog [ ] ={1,2,840,10036,1,21,1,28};
UINT4 Dot11WNMRequestNextIndex [ ] ={1,2,840,10036,1,22,1,1};
UINT4 Dot11WNMRqstIndex [ ] ={1,2,840,10036,1,22,1,2,1,1};
UINT4 Dot11WNMRqstRowStatus [ ] ={1,2,840,10036,1,22,1,2,1,2};
UINT4 Dot11WNMRqstToken [ ] ={1,2,840,10036,1,22,1,2,1,3};
UINT4 Dot11WNMRqstIfIndex [ ] ={1,2,840,10036,1,22,1,2,1,4};
UINT4 Dot11WNMRqstType [ ] ={1,2,840,10036,1,22,1,2,1,5};
UINT4 Dot11WNMRqstTargetAdd [ ] ={1,2,840,10036,1,22,1,2,1,6};
UINT4 Dot11WNMRqstTimeStamp [ ] ={1,2,840,10036,1,22,1,2,1,7};
UINT4 Dot11WNMRqstRndInterval [ ] ={1,2,840,10036,1,22,1,2,1,8};
UINT4 Dot11WNMRqstDuration [ ] ={1,2,840,10036,1,22,1,2,1,9};
UINT4 Dot11WNMRqstMcstGroup [ ] ={1,2,840,10036,1,22,1,2,1,10};
UINT4 Dot11WNMRqstMcstTrigCon [ ] ={1,2,840,10036,1,22,1,2,1,11};
UINT4 Dot11WNMRqstMcstTrigInactivityTimeout [ ] ={1,2,840,10036,1,22,1,2,1,12};
UINT4 Dot11WNMRqstMcstTrigReactDelay [ ] ={1,2,840,10036,1,22,1,2,1,13};
UINT4 Dot11WNMRqstLCRRqstSubject [ ] ={1,2,840,10036,1,22,1,2,1,14};
UINT4 Dot11WNMRqstLCRIntervalUnits [ ] ={1,2,840,10036,1,22,1,2,1,15};
UINT4 Dot11WNMRqstLCRServiceInterval [ ] ={1,2,840,10036,1,22,1,2,1,16};
UINT4 Dot11WNMRqstLIRRqstSubject [ ] ={1,2,840,10036,1,22,1,2,1,17};
UINT4 Dot11WNMRqstLIRIntervalUnits [ ] ={1,2,840,10036,1,22,1,2,1,18};
UINT4 Dot11WNMRqstLIRServiceInterval [ ] ={1,2,840,10036,1,22,1,2,1,19};
UINT4 Dot11WNMRqstEventToken [ ] ={1,2,840,10036,1,22,1,2,1,20};
UINT4 Dot11WNMRqstEventType [ ] ={1,2,840,10036,1,22,1,2,1,21};
UINT4 Dot11WNMRqstEventResponseLimit [ ] ={1,2,840,10036,1,22,1,2,1,22};
UINT4 Dot11WNMRqstEventTargetBssid [ ] ={1,2,840,10036,1,22,1,2,1,23};
UINT4 Dot11WNMRqstEventSourceBssid [ ] ={1,2,840,10036,1,22,1,2,1,24};
UINT4 Dot11WNMRqstEventTransitTimeThresh [ ] ={1,2,840,10036,1,22,1,2,1,25};
UINT4 Dot11WNMRqstEventTransitMatchValue [ ] ={1,2,840,10036,1,22,1,2,1,26};
UINT4 Dot11WNMRqstEventFreqTransitCountThresh [ ] ={1,2,840,10036,1,22,1,2,1,27};
UINT4 Dot11WNMRqstEventFreqTransitInterval [ ] ={1,2,840,10036,1,22,1,2,1,28};
UINT4 Dot11WNMRqstEventRsnaAuthType [ ] ={1,2,840,10036,1,22,1,2,1,29};
UINT4 Dot11WNMRqstEapType [ ] ={1,2,840,10036,1,22,1,2,1,30};
UINT4 Dot11WNMRqstEapVendorId [ ] ={1,2,840,10036,1,22,1,2,1,31};
UINT4 Dot11WNMRqstEapVendorType [ ] ={1,2,840,10036,1,22,1,2,1,32};
UINT4 Dot11WNMRqstEventRsnaMatchValue [ ] ={1,2,840,10036,1,22,1,2,1,33};
UINT4 Dot11WNMRqstEventPeerMacAddress [ ] ={1,2,840,10036,1,22,1,2,1,34};
UINT4 Dot11WNMRqstOperatingClass [ ] ={1,2,840,10036,1,22,1,2,1,35};
UINT4 Dot11WNMRqstChanNumber [ ] ={1,2,840,10036,1,22,1,2,1,36};
UINT4 Dot11WNMRqstDiagToken [ ] ={1,2,840,10036,1,22,1,2,1,37};
UINT4 Dot11WNMRqstDiagType [ ] ={1,2,840,10036,1,22,1,2,1,38};
UINT4 Dot11WNMRqstDiagTimeout [ ] ={1,2,840,10036,1,22,1,2,1,39};
UINT4 Dot11WNMRqstDiagBssid [ ] ={1,2,840,10036,1,22,1,2,1,40};
UINT4 Dot11WNMRqstDiagProfileId [ ] ={1,2,840,10036,1,22,1,2,1,41};
UINT4 Dot11WNMRqstDiagCredentials [ ] ={1,2,840,10036,1,22,1,2,1,42};
UINT4 Dot11WNMRqstLocConfigLocIndParams [ ] ={1,2,840,10036,1,22,1,2,1,43};
UINT4 Dot11WNMRqstLocConfigChanList [ ] ={1,2,840,10036,1,22,1,2,1,44};
UINT4 Dot11WNMRqstLocConfigBcastRate [ ] ={1,2,840,10036,1,22,1,2,1,45};
UINT4 Dot11WNMRqstLocConfigOptions [ ] ={1,2,840,10036,1,22,1,2,1,46};
UINT4 Dot11WNMRqstBssTransitQueryReason [ ] ={1,2,840,10036,1,22,1,2,1,47};
UINT4 Dot11WNMRqstBssTransitReqMode [ ] ={1,2,840,10036,1,22,1,2,1,48};
UINT4 Dot11WNMRqstBssTransitDisocTimer [ ] ={1,2,840,10036,1,22,1,2,1,49};
UINT4 Dot11WNMRqstBssTransitSessInfoURL [ ] ={1,2,840,10036,1,22,1,2,1,50};
UINT4 Dot11WNMRqstBssTransitCandidateList [ ] ={1,2,840,10036,1,22,1,2,1,51};
UINT4 Dot11WNMRqstColocInterfAutoEnable [ ] ={1,2,840,10036,1,22,1,2,1,52};
UINT4 Dot11WNMRqstColocInterfRptTimeout [ ] ={1,2,840,10036,1,22,1,2,1,53};
UINT4 Dot11WNMRqstVendorSpecific [ ] ={1,2,840,10036,1,22,1,2,1,54};
UINT4 Dot11WNMRqstDestinationURI [ ] ={1,2,840,10036,1,22,1,2,1,55};
UINT4 Dot11WNMVendorSpecificRprtIndex [ ] ={1,2,840,10036,1,22,2,1,1,1};
UINT4 Dot11WNMVendorSpecificRprtRqstToken [ ] ={1,2,840,10036,1,22,2,1,1,2};
UINT4 Dot11WNMVendorSpecificRprtIfIndex [ ] ={1,2,840,10036,1,22,2,1,1,3};
UINT4 Dot11WNMVendorSpecificRprtContent [ ] ={1,2,840,10036,1,22,2,1,1,4};
UINT4 Dot11WNMMulticastDiagnosticRprtIndex [ ] ={1,2,840,10036,1,22,2,2,1,1};
UINT4 Dot11WNMMulticastDiagnosticRprtRqstToken [ ] ={1,2,840,10036,1,22,2,2,1,2};
UINT4 Dot11WNMMulticastDiagnosticRprtIfIndex [ ] ={1,2,840,10036,1,22,2,2,1,3};
UINT4 Dot11WNMMulticastDiagnosticRprtMeasurementTime [ ] ={1,2,840,10036,1,22,2,2,1,4};
UINT4 Dot11WNMMulticastDiagnosticRprtDuration [ ] ={1,2,840,10036,1,22,2,2,1,5};
UINT4 Dot11WNMMulticastDiagnosticRprtMcstGroup [ ] ={1,2,840,10036,1,22,2,2,1,6};
UINT4 Dot11WNMMulticastDiagnosticRprtReason [ ] ={1,2,840,10036,1,22,2,2,1,7};
UINT4 Dot11WNMMulticastDiagnosticRprtRcvdMsduCount [ ] ={1,2,840,10036,1,22,2,2,1,8};
UINT4 Dot11WNMMulticastDiagnosticRprtFirstSeqNumber [ ] ={1,2,840,10036,1,22,2,2,1,9};
UINT4 Dot11WNMMulticastDiagnosticRprtLastSeqNumber [ ] ={1,2,840,10036,1,22,2,2,1,10};
UINT4 Dot11WNMMulticastDiagnosticRprtMcstRate [ ] ={1,2,840,10036,1,22,2,2,1,11};
UINT4 Dot11WNMLocationCivicRprtIndex [ ] ={1,2,840,10036,1,22,2,3,1,1};
UINT4 Dot11WNMLocationCivicRprtRqstToken [ ] ={1,2,840,10036,1,22,2,3,1,2};
UINT4 Dot11WNMLocationCivicRprtIfIndex [ ] ={1,2,840,10036,1,22,2,3,1,3};
UINT4 Dot11WNMLocationCivicRprtCivicLocation [ ] ={1,2,840,10036,1,22,2,3,1,4};
UINT4 Dot11WNMLocationIdentifierRprtIndex [ ] ={1,2,840,10036,1,22,2,4,1,1};
UINT4 Dot11WNMLocationIdentifierRprtRqstToken [ ] ={1,2,840,10036,1,22,2,4,1,2};
UINT4 Dot11WNMLocationIdentifierRprtIfIndex [ ] ={1,2,840,10036,1,22,2,4,1,3};
UINT4 Dot11WNMLocationIdentifierRprtExpirationTSF [ ] ={1,2,840,10036,1,22,2,4,1,4};
UINT4 Dot11WNMLocationIdentifierRprtPublicIdUri [ ] ={1,2,840,10036,1,22,2,4,1,5};
UINT4 Dot11WNMEventTransitRprtIndex [ ] ={1,2,840,10036,1,22,2,5,1,1};
UINT4 Dot11WNMEventTransitRprtRqstToken [ ] ={1,2,840,10036,1,22,2,5,1,2};
UINT4 Dot11WNMEventTransitRprtIfIndex [ ] ={1,2,840,10036,1,22,2,5,1,3};
UINT4 Dot11WNMEventTransitRprtEventStatus [ ] ={1,2,840,10036,1,22,2,5,1,4};
UINT4 Dot11WNMEventTransitRprtEventTSF [ ] ={1,2,840,10036,1,22,2,5,1,5};
UINT4 Dot11WNMEventTransitRprtUTCOffset [ ] ={1,2,840,10036,1,22,2,5,1,6};
UINT4 Dot11WNMEventTransitRprtTimeError [ ] ={1,2,840,10036,1,22,2,5,1,7};
UINT4 Dot11WNMEventTransitRprtSourceBssid [ ] ={1,2,840,10036,1,22,2,5,1,8};
UINT4 Dot11WNMEventTransitRprtTargetBssid [ ] ={1,2,840,10036,1,22,2,5,1,9};
UINT4 Dot11WNMEventTransitRprtTransitTime [ ] ={1,2,840,10036,1,22,2,5,1,10};
UINT4 Dot11WNMEventTransitRprtTransitReason [ ] ={1,2,840,10036,1,22,2,5,1,11};
UINT4 Dot11WNMEventTransitRprtTransitResult [ ] ={1,2,840,10036,1,22,2,5,1,12};
UINT4 Dot11WNMEventTransitRprtSourceRCPI [ ] ={1,2,840,10036,1,22,2,5,1,13};
UINT4 Dot11WNMEventTransitRprtSourceRSNI [ ] ={1,2,840,10036,1,22,2,5,1,14};
UINT4 Dot11WNMEventTransitRprtTargetRCPI [ ] ={1,2,840,10036,1,22,2,5,1,15};
UINT4 Dot11WNMEventTransitRprtTargetRSNI [ ] ={1,2,840,10036,1,22,2,5,1,16};
UINT4 Dot11WNMEventRsnaRprtIndex [ ] ={1,2,840,10036,1,22,2,6,1,1};
UINT4 Dot11WNMEventRsnaRprtRqstToken [ ] ={1,2,840,10036,1,22,2,6,1,2};
UINT4 Dot11WNMEventRsnaRprtIfIndex [ ] ={1,2,840,10036,1,22,2,6,1,3};
UINT4 Dot11WNMEventRsnaRprtEventStatus [ ] ={1,2,840,10036,1,22,2,6,1,4};
UINT4 Dot11WNMEventRsnaRprtEventTSF [ ] ={1,2,840,10036,1,22,2,6,1,5};
UINT4 Dot11WNMEventRsnaRprtUTCOffset [ ] ={1,2,840,10036,1,22,2,6,1,6};
UINT4 Dot11WNMEventRsnaRprtTimeError [ ] ={1,2,840,10036,1,22,2,6,1,7};
UINT4 Dot11WNMEventRsnaRprtTargetBssid [ ] ={1,2,840,10036,1,22,2,6,1,8};
UINT4 Dot11WNMEventRsnaRprtAuthType [ ] ={1,2,840,10036,1,22,2,6,1,9};
UINT4 Dot11WNMEventRsnaRprtEapMethod [ ] ={1,2,840,10036,1,22,2,6,1,10};
UINT4 Dot11WNMEventRsnaRprtResult [ ] ={1,2,840,10036,1,22,2,6,1,11};
UINT4 Dot11WNMEventRsnaRprtRsnElement [ ] ={1,2,840,10036,1,22,2,6,1,12};
UINT4 Dot11WNMEventPeerRprtIndex [ ] ={1,2,840,10036,1,22,2,7,1,1};
UINT4 Dot11WNMEventPeerRprtRqstToken [ ] ={1,2,840,10036,1,22,2,7,1,2};
UINT4 Dot11WNMEventPeerRprtIfIndex [ ] ={1,2,840,10036,1,22,2,7,1,3};
UINT4 Dot11WNMEventPeerRprtEventStatus [ ] ={1,2,840,10036,1,22,2,7,1,4};
UINT4 Dot11WNMEventPeerRprtEventTSF [ ] ={1,2,840,10036,1,22,2,7,1,5};
UINT4 Dot11WNMEventPeerRprtUTCOffset [ ] ={1,2,840,10036,1,22,2,7,1,6};
UINT4 Dot11WNMEventPeerRprtTimeError [ ] ={1,2,840,10036,1,22,2,7,1,7};
UINT4 Dot11WNMEventPeerRprtPeerMacAddress [ ] ={1,2,840,10036,1,22,2,7,1,8};
UINT4 Dot11WNMEventPeerRprtOperatingClass [ ] ={1,2,840,10036,1,22,2,7,1,9};
UINT4 Dot11WNMEventPeerRprtChanNumber [ ] ={1,2,840,10036,1,22,2,7,1,10};
UINT4 Dot11WNMEventPeerRprtStaTxPower [ ] ={1,2,840,10036,1,22,2,7,1,11};
UINT4 Dot11WNMEventPeerRprtConnTime [ ] ={1,2,840,10036,1,22,2,7,1,12};
UINT4 Dot11WNMEventPeerRprtPeerStatus [ ] ={1,2,840,10036,1,22,2,7,1,13};
UINT4 Dot11WNMEventWNMLogRprtIndex [ ] ={1,2,840,10036,1,22,2,8,1,1};
UINT4 Dot11WNMEventWNMLogRprtRqstToken [ ] ={1,2,840,10036,1,22,2,8,1,2};
UINT4 Dot11WNMEventWNMLogRprtIfIndex [ ] ={1,2,840,10036,1,22,2,8,1,3};
UINT4 Dot11WNMEventWNMLogRprtEventStatus [ ] ={1,2,840,10036,1,22,2,8,1,4};
UINT4 Dot11WNMEventWNMLogRprtEventTSF [ ] ={1,2,840,10036,1,22,2,8,1,5};
UINT4 Dot11WNMEventWNMLogRprtUTCOffset [ ] ={1,2,840,10036,1,22,2,8,1,6};
UINT4 Dot11WNMEventWNMLogRprtTimeError [ ] ={1,2,840,10036,1,22,2,8,1,7};
UINT4 Dot11WNMEventWNMLogRprtContent [ ] ={1,2,840,10036,1,22,2,8,1,8};
UINT4 Dot11WNMDiagMfrInfoRprtIndex [ ] ={1,2,840,10036,1,22,2,9,1,1};
UINT4 Dot11WNMDiagMfrInfoRprtRqstToken [ ] ={1,2,840,10036,1,22,2,9,1,2};
UINT4 Dot11WNMDiagMfrInfoRprtIfIndex [ ] ={1,2,840,10036,1,22,2,9,1,3};
UINT4 Dot11WNMDiagMfrInfoRprtEventStatus [ ] ={1,2,840,10036,1,22,2,9,1,4};
UINT4 Dot11WNMDiagMfrInfoRprtMfrOi [ ] ={1,2,840,10036,1,22,2,9,1,5};
UINT4 Dot11WNMDiagMfrInfoRprtMfrIdString [ ] ={1,2,840,10036,1,22,2,9,1,6};
UINT4 Dot11WNMDiagMfrInfoRprtMfrModelString [ ] ={1,2,840,10036,1,22,2,9,1,7};
UINT4 Dot11WNMDiagMfrInfoRprtMfrSerialNumberString [ ] ={1,2,840,10036,1,22,2,9,1,8};
UINT4 Dot11WNMDiagMfrInfoRprtMfrFirmwareVersion [ ] ={1,2,840,10036,1,22,2,9,1,9};
UINT4 Dot11WNMDiagMfrInfoRprtMfrAntennaType [ ] ={1,2,840,10036,1,22,2,9,1,10};
UINT4 Dot11WNMDiagMfrInfoRprtCollocRadioType [ ] ={1,2,840,10036,1,22,2,9,1,11};
UINT4 Dot11WNMDiagMfrInfoRprtDeviceType [ ] ={1,2,840,10036,1,22,2,9,1,12};
UINT4 Dot11WNMDiagMfrInfoRprtCertificateID [ ] ={1,2,840,10036,1,22,2,9,1,13};
UINT4 Dot11WNMDiagConfigProfRprtIndex [ ] ={1,2,840,10036,1,22,2,10,1,1};
UINT4 Dot11WNMDiagConfigProfRprtRqstToken [ ] ={1,2,840,10036,1,22,2,10,1,2};
UINT4 Dot11WNMDiagConfigProfRprtIfIndex [ ] ={1,2,840,10036,1,22,2,10,1,3};
UINT4 Dot11WNMDiagConfigProfRprtEventStatus [ ] ={1,2,840,10036,1,22,2,10,1,4};
UINT4 Dot11WNMDiagConfigProfRprtProfileId [ ] ={1,2,840,10036,1,22,2,10,1,5};
UINT4 Dot11WNMDiagConfigProfRprtSupportedOperatingClasses [ ] ={1,2,840,10036,1,22,2,10,1,6};
UINT4 Dot11WNMDiagConfigProfRprtTxPowerMode [ ] ={1,2,840,10036,1,22,2,10,1,7};
UINT4 Dot11WNMDiagConfigProfRprtTxPowerLevels [ ] ={1,2,840,10036,1,22,2,10,1,8};
UINT4 Dot11WNMDiagConfigProfRprtCipherSuite [ ] ={1,2,840,10036,1,22,2,10,1,9};
UINT4 Dot11WNMDiagConfigProfRprtAkmSuite [ ] ={1,2,840,10036,1,22,2,10,1,10};
UINT4 Dot11WNMDiagConfigProfRprtEapType [ ] ={1,2,840,10036,1,22,2,10,1,11};
UINT4 Dot11WNMDiagConfigProfRprtEapVendorID [ ] ={1,2,840,10036,1,22,2,10,1,12};
UINT4 Dot11WNMDiagConfigProfRprtEapVendorType [ ] ={1,2,840,10036,1,22,2,10,1,13};
UINT4 Dot11WNMDiagConfigProfRprtCredentialType [ ] ={1,2,840,10036,1,22,2,10,1,14};
UINT4 Dot11WNMDiagConfigProfRprtSSID [ ] ={1,2,840,10036,1,22,2,10,1,15};
UINT4 Dot11WNMDiagConfigProfRprtPowerSaveMode [ ] ={1,2,840,10036,1,22,2,10,1,16};
UINT4 Dot11WNMDiagAssocRprtIndex [ ] ={1,2,840,10036,1,22,2,11,1,1};
UINT4 Dot11WNMDiagAssocRprtRqstToken [ ] ={1,2,840,10036,1,22,2,11,1,2};
UINT4 Dot11WNMDiagAssocRprtIfIndex [ ] ={1,2,840,10036,1,22,2,11,1,3};
UINT4 Dot11WNMDiagAssocRprtEventStatus [ ] ={1,2,840,10036,1,22,2,11,1,4};
UINT4 Dot11WNMDiagAssocRprtBssid [ ] ={1,2,840,10036,1,22,2,11,1,5};
UINT4 Dot11WNMDiagAssocRprtOperatingClass [ ] ={1,2,840,10036,1,22,2,11,1,6};
UINT4 Dot11WNMDiagAssocRprtChannelNumber [ ] ={1,2,840,10036,1,22,2,11,1,7};
UINT4 Dot11WNMDiagAssocRprtStatusCode [ ] ={1,2,840,10036,1,22,2,11,1,8};
UINT4 Dot11WNMDiag8021xAuthRprtIndex [ ] ={1,2,840,10036,1,22,2,12,1,1};
UINT4 Dot11WNMDiag8021xAuthRprtRqstToken [ ] ={1,2,840,10036,1,22,2,12,1,2};
UINT4 Dot11WNMDiag8021xAuthRprtIfIndex [ ] ={1,2,840,10036,1,22,2,12,1,3};
UINT4 Dot11WNMDiag8021xAuthRprtEventStatus [ ] ={1,2,840,10036,1,22,2,12,1,4};
UINT4 Dot11WNMDiag8021xAuthRprtBssid [ ] ={1,2,840,10036,1,22,2,12,1,5};
UINT4 Dot11WNMDiag8021xAuthRprtOperatingClass [ ] ={1,2,840,10036,1,22,2,12,1,6};
UINT4 Dot11WNMDiag8021xAuthRprtChannelNumber [ ] ={1,2,840,10036,1,22,2,12,1,7};
UINT4 Dot11WNMDiag8021xAuthRprtEapType [ ] ={1,2,840,10036,1,22,2,12,1,8};
UINT4 Dot11WNMDiag8021xAuthRprtEapVendorID [ ] ={1,2,840,10036,1,22,2,12,1,9};
UINT4 Dot11WNMDiag8021xAuthRprtEapVendorType [ ] ={1,2,840,10036,1,22,2,12,1,10};
UINT4 Dot11WNMDiag8021xAuthRprtCredentialType [ ] ={1,2,840,10036,1,22,2,12,1,11};
UINT4 Dot11WNMDiag8021xAuthRprtStatusCode [ ] ={1,2,840,10036,1,22,2,12,1,12};
UINT4 Dot11WNMLocConfigRprtIndex [ ] ={1,2,840,10036,1,22,2,13,1,1};
UINT4 Dot11WNMLocConfigRprtRqstToken [ ] ={1,2,840,10036,1,22,2,13,1,2};
UINT4 Dot11WNMLocConfigRprtIfIndex [ ] ={1,2,840,10036,1,22,2,13,1,3};
UINT4 Dot11WNMLocConfigRprtLocIndParams [ ] ={1,2,840,10036,1,22,2,13,1,4};
UINT4 Dot11WNMLocConfigRprtLocIndChanList [ ] ={1,2,840,10036,1,22,2,13,1,5};
UINT4 Dot11WNMLocConfigRprtLocIndBcastRate [ ] ={1,2,840,10036,1,22,2,13,1,6};
UINT4 Dot11WNMLocConfigRprtLocIndOptions [ ] ={1,2,840,10036,1,22,2,13,1,7};
UINT4 Dot11WNMLocConfigRprtStatusConfigSubelemId [ ] ={1,2,840,10036,1,22,2,13,1,8};
UINT4 Dot11WNMLocConfigRprtStatusResult [ ] ={1,2,840,10036,1,22,2,13,1,9};
UINT4 Dot11WNMLocConfigRprtVendorSpecificRprtContent [ ] ={1,2,840,10036,1,22,2,13,1,10};
UINT4 Dot11WNMBssTransitRprtIndex [ ] ={1,2,840,10036,1,22,2,14,1,1};
UINT4 Dot11WNMBssTransitRprtRqstToken [ ] ={1,2,840,10036,1,22,2,14,1,2};
UINT4 Dot11WNMBssTransitRprtIfIndex [ ] ={1,2,840,10036,1,22,2,14,1,3};
UINT4 Dot11WNMBssTransitRprtStatusCode [ ] ={1,2,840,10036,1,22,2,14,1,4};
UINT4 Dot11WNMBssTransitRprtBSSTerminationDelay [ ] ={1,2,840,10036,1,22,2,14,1,5};
UINT4 Dot11WNMBssTransitRprtTargetBssid [ ] ={1,2,840,10036,1,22,2,14,1,6};
UINT4 Dot11WNMBssTransitRprtCandidateList [ ] ={1,2,840,10036,1,22,2,14,1,7};
UINT4 Dot11WNMColocInterfRprtIndex [ ] ={1,2,840,10036,1,22,2,16,1,1};
UINT4 Dot11WNMColocInterfRprtRqstToken [ ] ={1,2,840,10036,1,22,2,16,1,2};
UINT4 Dot11WNMColocInterfRprtIfIndex [ ] ={1,2,840,10036,1,22,2,16,1,3};
UINT4 Dot11WNMColocInterfRprtPeriod [ ] ={1,2,840,10036,1,22,2,16,1,4};
UINT4 Dot11WNMColocInterfRprtInterfLevel [ ] ={1,2,840,10036,1,22,2,16,1,5};
UINT4 Dot11WNMColocInterfRprtInterfAccuracy [ ] ={1,2,840,10036,1,22,2,16,1,6};
UINT4 Dot11WNMColocInterfRprtInterfIndex [ ] ={1,2,840,10036,1,22,2,16,1,7};
UINT4 Dot11WNMColocInterfRprtInterfInterval [ ] ={1,2,840,10036,1,22,2,16,1,8};
UINT4 Dot11WNMColocInterfRprtInterfBurstLength [ ] ={1,2,840,10036,1,22,2,16,1,9};
UINT4 Dot11WNMColocInterfRprtInterfStartTime [ ] ={1,2,840,10036,1,22,2,16,1,10};
UINT4 Dot11WNMColocInterfRprtInterfCenterFreq [ ] ={1,2,840,10036,1,22,2,16,1,11};
UINT4 Dot11WNMColocInterfRprtInterfBandwidth [ ] ={1,2,840,10036,1,22,2,16,1,12};
UINT4 Dot11MeshID [ ] ={1,2,840,10036,1,23,1,1};
UINT4 Dot11MeshNumberOfPeerings [ ] ={1,2,840,10036,1,23,1,2};
UINT4 Dot11MeshAcceptingAdditionalPeerings [ ] ={1,2,840,10036,1,23,1,3};
UINT4 Dot11MeshConnectedToMeshGate [ ] ={1,2,840,10036,1,23,1,4};
UINT4 Dot11MeshSecurityActivated [ ] ={1,2,840,10036,1,23,1,5};
UINT4 Dot11MeshActiveAuthenticationProtocol [ ] ={1,2,840,10036,1,23,1,6};
UINT4 Dot11MeshMaxRetries [ ] ={1,2,840,10036,1,23,1,7};
UINT4 Dot11MeshRetryTimeout [ ] ={1,2,840,10036,1,23,1,8};
UINT4 Dot11MeshConfirmTimeout [ ] ={1,2,840,10036,1,23,1,9};
UINT4 Dot11MeshHoldingTimeout [ ] ={1,2,840,10036,1,23,1,10};
UINT4 Dot11MeshConfigGroupUpdateCount [ ] ={1,2,840,10036,1,23,1,11};
UINT4 Dot11MeshActivePathSelectionProtocol [ ] ={1,2,840,10036,1,23,1,12};
UINT4 Dot11MeshActivePathSelectionMetric [ ] ={1,2,840,10036,1,23,1,13};
UINT4 Dot11MeshForwarding [ ] ={1,2,840,10036,1,23,1,14};
UINT4 Dot11MeshTTL [ ] ={1,2,840,10036,1,23,1,15};
UINT4 Dot11MeshGateAnnouncements [ ] ={1,2,840,10036,1,23,1,16};
UINT4 Dot11MeshGateAnnouncementInterval [ ] ={1,2,840,10036,1,23,1,17};
UINT4 Dot11MeshActiveCongestionControlMode [ ] ={1,2,840,10036,1,23,1,18};
UINT4 Dot11MeshActiveSynchronizationMethod [ ] ={1,2,840,10036,1,23,1,19};
UINT4 Dot11MeshNbrOffsetMaxNeighbor [ ] ={1,2,840,10036,1,23,1,20};
UINT4 Dot11MBCAActivated [ ] ={1,2,840,10036,1,23,1,21};
UINT4 Dot11MeshBeaconTimingReportInterval [ ] ={1,2,840,10036,1,23,1,22};
UINT4 Dot11MeshBeaconTimingReportMaxNum [ ] ={1,2,840,10036,1,23,1,23};
UINT4 Dot11MeshDelayedBeaconTxInterval [ ] ={1,2,840,10036,1,23,1,24};
UINT4 Dot11MeshDelayedBeaconTxMaxDelay [ ] ={1,2,840,10036,1,23,1,25};
UINT4 Dot11MeshDelayedBeaconTxMinDelay [ ] ={1,2,840,10036,1,23,1,26};
UINT4 Dot11MeshAverageBeaconFrameDuration [ ] ={1,2,840,10036,1,23,1,27};
UINT4 Dot11MeshSTAMissingAckRetryLimit [ ] ={1,2,840,10036,1,23,1,28};
UINT4 Dot11MeshAwakeWindowDuration [ ] ={1,2,840,10036,1,23,1,29};
UINT4 Dot11MCCAImplemented [ ] ={1,2,840,10036,1,23,1,30};
UINT4 Dot11MCCAActivated [ ] ={1,2,840,10036,1,23,1,31};
UINT4 Dot11MAFlimit [ ] ={1,2,840,10036,1,23,1,32};
UINT4 Dot11MCCAScanDuration [ ] ={1,2,840,10036,1,23,1,33};
UINT4 Dot11MCCAAdvertPeriodMax [ ] ={1,2,840,10036,1,23,1,34};
UINT4 Dot11MCCAMinTrackStates [ ] ={1,2,840,10036,1,23,1,35};
UINT4 Dot11MCCAMaxTrackStates [ ] ={1,2,840,10036,1,23,1,36};
UINT4 Dot11MCCAOPtimeout [ ] ={1,2,840,10036,1,23,1,37};
UINT4 Dot11MCCACWmin [ ] ={1,2,840,10036,1,23,1,38};
UINT4 Dot11MCCACWmax [ ] ={1,2,840,10036,1,23,1,39};
UINT4 Dot11MCCAAIFSN [ ] ={1,2,840,10036,1,23,1,40};
UINT4 Dot11MeshHWMPmaxPREQretries [ ] ={1,2,840,10036,1,24,1,1};
UINT4 Dot11MeshHWMPnetDiameter [ ] ={1,2,840,10036,1,24,1,2};
UINT4 Dot11MeshHWMPnetDiameterTraversalTime [ ] ={1,2,840,10036,1,24,1,3};
UINT4 Dot11MeshHWMPpreqMinInterval [ ] ={1,2,840,10036,1,24,1,4};
UINT4 Dot11MeshHWMPperrMinInterval [ ] ={1,2,840,10036,1,24,1,5};
UINT4 Dot11MeshHWMPactivePathToRootTimeout [ ] ={1,2,840,10036,1,24,1,6};
UINT4 Dot11MeshHWMPactivePathTimeout [ ] ={1,2,840,10036,1,24,1,7};
UINT4 Dot11MeshHWMProotMode [ ] ={1,2,840,10036,1,24,1,8};
UINT4 Dot11MeshHWMProotInterval [ ] ={1,2,840,10036,1,24,1,9};
UINT4 Dot11MeshHWMPrannInterval [ ] ={1,2,840,10036,1,24,1,10};
UINT4 Dot11MeshHWMPtargetOnly [ ] ={1,2,840,10036,1,24,1,11};
UINT4 Dot11MeshHWMPmaintenanceInterval [ ] ={1,2,840,10036,1,24,1,12};
UINT4 Dot11MeshHWMPconfirmationInterval [ ] ={1,2,840,10036,1,24,1,13};
UINT4 Dot11RSNAConfigPasswordValueIndex [ ] ={1,2,840,10036,1,25,1,1};
UINT4 Dot11RSNAConfigPasswordCredential [ ] ={1,2,840,10036,1,25,1,2};
UINT4 Dot11RSNAConfigPasswordPeerMac [ ] ={1,2,840,10036,1,25,1,3};
UINT4 Dot11RSNAConfigDLCGroupIndex [ ] ={1,2,840,10036,1,26,1,1};
UINT4 Dot11RSNAConfigDLCGroupIdentifier [ ] ={1,2,840,10036,1,26,1,2};
UINT4 Dot11MACAddress [ ] ={1,2,840,10036,2,1,1,1};
UINT4 Dot11RTSThreshold [ ] ={1,2,840,10036,2,1,1,2};
UINT4 Dot11ShortRetryLimit [ ] ={1,2,840,10036,2,1,1,3};
UINT4 Dot11LongRetryLimit [ ] ={1,2,840,10036,2,1,1,4};
UINT4 Dot11FragmentationThreshold [ ] ={1,2,840,10036,2,1,1,5};
UINT4 Dot11MaxTransmitMSDULifetime [ ] ={1,2,840,10036,2,1,1,6};
UINT4 Dot11MaxReceiveLifetime [ ] ={1,2,840,10036,2,1,1,7};
UINT4 Dot11ManufacturerID [ ] ={1,2,840,10036,2,1,1,8};
UINT4 Dot11ProductID [ ] ={1,2,840,10036,2,1,1,9};
UINT4 Dot11CAPLimit [ ] ={1,2,840,10036,2,1,1,10};
UINT4 Dot11HCCWmin [ ] ={1,2,840,10036,2,1,1,11};
UINT4 Dot11HCCWmax [ ] ={1,2,840,10036,2,1,1,12};
UINT4 Dot11HCCAIFSN [ ] ={1,2,840,10036,2,1,1,13};
UINT4 Dot11ADDBAResponseTimeout [ ] ={1,2,840,10036,2,1,1,14};
UINT4 Dot11ADDTSResponseTimeout [ ] ={1,2,840,10036,2,1,1,15};
UINT4 Dot11ChannelUtilizationBeaconInterval [ ] ={1,2,840,10036,2,1,1,16};
UINT4 Dot11ScheduleTimeout [ ] ={1,2,840,10036,2,1,1,17};
UINT4 Dot11DLSResponseTimeout [ ] ={1,2,840,10036,2,1,1,18};
UINT4 Dot11QAPMissingAckRetryLimit [ ] ={1,2,840,10036,2,1,1,19};
UINT4 Dot11EDCAAveragingPeriod [ ] ={1,2,840,10036,2,1,1,20};
UINT4 Dot11HTProtection [ ] ={1,2,840,10036,2,1,1,21};
UINT4 Dot11RIFSMode [ ] ={1,2,840,10036,2,1,1,22};
UINT4 Dot11PSMPControlledAccess [ ] ={1,2,840,10036,2,1,1,23};
UINT4 Dot11ServiceIntervalGranularity [ ] ={1,2,840,10036,2,1,1,24};
UINT4 Dot11DualCTSProtection [ ] ={1,2,840,10036,2,1,1,25};
UINT4 Dot11LSIGTXOPFullProtectionActivated [ ] ={1,2,840,10036,2,1,1,26};
UINT4 Dot11NonGFEntitiesPresent [ ] ={1,2,840,10036,2,1,1,27};
UINT4 Dot11PCOActivated [ ] ={1,2,840,10036,2,1,1,28};
UINT4 Dot11PCOFortyMaxDuration [ ] ={1,2,840,10036,2,1,1,29};
UINT4 Dot11PCOTwentyMaxDuration [ ] ={1,2,840,10036,2,1,1,30};
UINT4 Dot11PCOFortyMinDuration [ ] ={1,2,840,10036,2,1,1,31};
UINT4 Dot11PCOTwentyMinDuration [ ] ={1,2,840,10036,2,1,1,32};
UINT4 Dot11FortyMHzIntolerant [ ] ={1,2,840,10036,2,1,1,33};
UINT4 Dot11BSSWidthTriggerScanInterval [ ] ={1,2,840,10036,2,1,1,34};
UINT4 Dot11BSSWidthChannelTransitionDelayFactor [ ] ={1,2,840,10036,2,1,1,35};
UINT4 Dot11OBSSScanPassiveDwell [ ] ={1,2,840,10036,2,1,1,36};
UINT4 Dot11OBSSScanActiveDwell [ ] ={1,2,840,10036,2,1,1,37};
UINT4 Dot11OBSSScanPassiveTotalPerChannel [ ] ={1,2,840,10036,2,1,1,38};
UINT4 Dot11OBSSScanActiveTotalPerChannel [ ] ={1,2,840,10036,2,1,1,39};
UINT4 Dot112040BSSCoexistenceManagementSupport [ ] ={1,2,840,10036,2,1,1,40};
UINT4 Dot11OBSSScanActivityThreshold [ ] ={1,2,840,10036,2,1,1,41};
UINT4 Dot11TransmittedFragmentCount [ ] ={1,2,840,10036,2,2,1,1};
UINT4 Dot11GroupTransmittedFrameCount [ ] ={1,2,840,10036,2,2,1,2};
UINT4 Dot11FailedCount [ ] ={1,2,840,10036,2,2,1,3};
UINT4 Dot11RetryCount [ ] ={1,2,840,10036,2,2,1,4};
UINT4 Dot11MultipleRetryCount [ ] ={1,2,840,10036,2,2,1,5};
UINT4 Dot11FrameDuplicateCount [ ] ={1,2,840,10036,2,2,1,6};
UINT4 Dot11RTSSuccessCount [ ] ={1,2,840,10036,2,2,1,7};
UINT4 Dot11RTSFailureCount [ ] ={1,2,840,10036,2,2,1,8};
UINT4 Dot11ACKFailureCount [ ] ={1,2,840,10036,2,2,1,9};
UINT4 Dot11ReceivedFragmentCount [ ] ={1,2,840,10036,2,2,1,10};
UINT4 Dot11GroupReceivedFrameCount [ ] ={1,2,840,10036,2,2,1,11};
UINT4 Dot11FCSErrorCount [ ] ={1,2,840,10036,2,2,1,12};
UINT4 Dot11TransmittedFrameCount [ ] ={1,2,840,10036,2,2,1,13};
UINT4 Dot11WEPUndecryptableCount [ ] ={1,2,840,10036,2,2,1,14};
UINT4 Dot11QosDiscardedFragmentCount [ ] ={1,2,840,10036,2,2,1,15};
UINT4 Dot11AssociatedStationCount [ ] ={1,2,840,10036,2,2,1,16};
UINT4 Dot11QosCFPollsReceivedCount [ ] ={1,2,840,10036,2,2,1,17};
UINT4 Dot11QosCFPollsUnusedCount [ ] ={1,2,840,10036,2,2,1,18};
UINT4 Dot11QosCFPollsUnusableCount [ ] ={1,2,840,10036,2,2,1,19};
UINT4 Dot11QosCFPollsLostCount [ ] ={1,2,840,10036,2,2,1,20};
UINT4 Dot11TransmittedAMSDUCount [ ] ={1,2,840,10036,2,2,1,21};
UINT4 Dot11FailedAMSDUCount [ ] ={1,2,840,10036,2,2,1,22};
UINT4 Dot11RetryAMSDUCount [ ] ={1,2,840,10036,2,2,1,23};
UINT4 Dot11MultipleRetryAMSDUCount [ ] ={1,2,840,10036,2,2,1,24};
UINT4 Dot11TransmittedOctetsInAMSDUCount [ ] ={1,2,840,10036,2,2,1,25};
UINT4 Dot11AMSDUAckFailureCount [ ] ={1,2,840,10036,2,2,1,26};
UINT4 Dot11ReceivedAMSDUCount [ ] ={1,2,840,10036,2,2,1,27};
UINT4 Dot11ReceivedOctetsInAMSDUCount [ ] ={1,2,840,10036,2,2,1,28};
UINT4 Dot11TransmittedAMPDUCount [ ] ={1,2,840,10036,2,2,1,29};
UINT4 Dot11TransmittedMPDUsInAMPDUCount [ ] ={1,2,840,10036,2,2,1,30};
UINT4 Dot11TransmittedOctetsInAMPDUCount [ ] ={1,2,840,10036,2,2,1,31};
UINT4 Dot11AMPDUReceivedCount [ ] ={1,2,840,10036,2,2,1,32};
UINT4 Dot11MPDUInReceivedAMPDUCount [ ] ={1,2,840,10036,2,2,1,33};
UINT4 Dot11ReceivedOctetsInAMPDUCount [ ] ={1,2,840,10036,2,2,1,34};
UINT4 Dot11AMPDUDelimiterCRCErrorCount [ ] ={1,2,840,10036,2,2,1,35};
UINT4 Dot11ImplicitBARFailureCount [ ] ={1,2,840,10036,2,2,1,36};
UINT4 Dot11ExplicitBARFailureCount [ ] ={1,2,840,10036,2,2,1,37};
UINT4 Dot11ChannelWidthSwitchCount [ ] ={1,2,840,10036,2,2,1,38};
UINT4 Dot11TwentyMHzFrameTransmittedCount [ ] ={1,2,840,10036,2,2,1,39};
UINT4 Dot11FortyMHzFrameTransmittedCount [ ] ={1,2,840,10036,2,2,1,40};
UINT4 Dot11TwentyMHzFrameReceivedCount [ ] ={1,2,840,10036,2,2,1,41};
UINT4 Dot11FortyMHzFrameReceivedCount [ ] ={1,2,840,10036,2,2,1,42};
UINT4 Dot11PSMPUTTGrantDuration [ ] ={1,2,840,10036,2,2,1,43};
UINT4 Dot11PSMPUTTUsedDuration [ ] ={1,2,840,10036,2,2,1,44};
UINT4 Dot11GrantedRDGUsedCount [ ] ={1,2,840,10036,2,2,1,45};
UINT4 Dot11GrantedRDGUnusedCount [ ] ={1,2,840,10036,2,2,1,46};
UINT4 Dot11TransmittedFramesInGrantedRDGCount [ ] ={1,2,840,10036,2,2,1,47};
UINT4 Dot11TransmittedOctetsInGrantedRDGCount [ ] ={1,2,840,10036,2,2,1,48};
UINT4 Dot11BeamformingFrameCount [ ] ={1,2,840,10036,2,2,1,49};
UINT4 Dot11DualCTSSuccessCount [ ] ={1,2,840,10036,2,2,1,50};
UINT4 Dot11DualCTSFailureCount [ ] ={1,2,840,10036,2,2,1,51};
UINT4 Dot11STBCCTSSuccessCount [ ] ={1,2,840,10036,2,2,1,52};
UINT4 Dot11STBCCTSFailureCount [ ] ={1,2,840,10036,2,2,1,53};
UINT4 Dot11nonSTBCCTSSuccessCount [ ] ={1,2,840,10036,2,2,1,54};
UINT4 Dot11nonSTBCCTSFailureCount [ ] ={1,2,840,10036,2,2,1,55};
UINT4 Dot11RTSLSIGSuccessCount [ ] ={1,2,840,10036,2,2,1,56};
UINT4 Dot11RTSLSIGFailureCount [ ] ={1,2,840,10036,2,2,1,57};
UINT4 Dot11PBACErrors [ ] ={1,2,840,10036,2,2,1,58};
UINT4 Dot11DeniedAssociationCounterDueToBSSLoad [ ] ={1,2,840,10036,2,2,1,59};
UINT4 Dot11GroupAddressesIndex [ ] ={1,2,840,10036,2,3,1,1};
UINT4 Dot11Address [ ] ={1,2,840,10036,2,3,1,2};
UINT4 Dot11GroupAddressesStatus [ ] ={1,2,840,10036,2,3,1,3};
UINT4 Dot11EDCATableIndex [ ] ={1,2,840,10036,2,4,1,1};
UINT4 Dot11EDCATableCWmin [ ] ={1,2,840,10036,2,4,1,2};
UINT4 Dot11EDCATableCWmax [ ] ={1,2,840,10036,2,4,1,3};
UINT4 Dot11EDCATableAIFSN [ ] ={1,2,840,10036,2,4,1,4};
UINT4 Dot11EDCATableTXOPLimit [ ] ={1,2,840,10036,2,4,1,5};
UINT4 Dot11EDCATableMSDULifetime [ ] ={1,2,840,10036,2,4,1,6};
UINT4 Dot11EDCATableMandatory [ ] ={1,2,840,10036,2,4,1,7};
UINT4 Dot11QAPEDCATableIndex [ ] ={1,2,840,10036,2,5,1,1};
UINT4 Dot11QAPEDCATableCWmin [ ] ={1,2,840,10036,2,5,1,2};
UINT4 Dot11QAPEDCATableCWmax [ ] ={1,2,840,10036,2,5,1,3};
UINT4 Dot11QAPEDCATableAIFSN [ ] ={1,2,840,10036,2,5,1,4};
UINT4 Dot11QAPEDCATableTXOPLimit [ ] ={1,2,840,10036,2,5,1,5};
UINT4 Dot11QAPEDCATableMSDULifetime [ ] ={1,2,840,10036,2,5,1,6};
UINT4 Dot11QAPEDCATableMandatory [ ] ={1,2,840,10036,2,5,1,7};
UINT4 Dot11QosCountersIndex [ ] ={1,2,840,10036,2,6,1,1};
UINT4 Dot11QosTransmittedFragmentCount [ ] ={1,2,840,10036,2,6,1,2};
UINT4 Dot11QosFailedCount [ ] ={1,2,840,10036,2,6,1,3};
UINT4 Dot11QosRetryCount [ ] ={1,2,840,10036,2,6,1,4};
UINT4 Dot11QosMultipleRetryCount [ ] ={1,2,840,10036,2,6,1,5};
UINT4 Dot11QosFrameDuplicateCount [ ] ={1,2,840,10036,2,6,1,6};
UINT4 Dot11QosRTSSuccessCount [ ] ={1,2,840,10036,2,6,1,7};
UINT4 Dot11QosRTSFailureCount [ ] ={1,2,840,10036,2,6,1,8};
UINT4 Dot11QosACKFailureCount [ ] ={1,2,840,10036,2,6,1,9};
UINT4 Dot11QosReceivedFragmentCount [ ] ={1,2,840,10036,2,6,1,10};
UINT4 Dot11QosTransmittedFrameCount [ ] ={1,2,840,10036,2,6,1,11};
UINT4 Dot11QosDiscardedFrameCount [ ] ={1,2,840,10036,2,6,1,12};
UINT4 Dot11QosMPDUsReceivedCount [ ] ={1,2,840,10036,2,6,1,13};
UINT4 Dot11QosRetriesReceivedCount [ ] ={1,2,840,10036,2,6,1,14};
UINT4 Dot11ResourceTypeIDName [ ] ={1,2,840,10036,3,1,1};
UINT4 Dot11manufacturerOUI [ ] ={1,2,840,10036,3,1,2,1,1};
UINT4 Dot11manufacturerName [ ] ={1,2,840,10036,3,1,2,1,2};
UINT4 Dot11manufacturerProductName [ ] ={1,2,840,10036,3,1,2,1,3};
UINT4 Dot11manufacturerProductVersion [ ] ={1,2,840,10036,3,1,2,1,4};
UINT4 Dot11PHYType [ ] ={1,2,840,10036,4,1,1,1};
UINT4 Dot11CurrentRegDomain [ ] ={1,2,840,10036,4,1,1,2};
UINT4 Dot11TempType [ ] ={1,2,840,10036,4,1,1,3};
UINT4 Dot11CurrentTxAntenna [ ] ={1,2,840,10036,4,2,1,1};
UINT4 Dot11DiversitySupportImplemented [ ] ={1,2,840,10036,4,2,1,2};
UINT4 Dot11CurrentRxAntenna [ ] ={1,2,840,10036,4,2,1,3};
UINT4 Dot11AntennaSelectionOptionImplemented [ ] ={1,2,840,10036,4,2,1,4};
UINT4 Dot11TransmitExplicitCSIFeedbackASOptionImplemented [ ] ={1,2,840,10036,4,2,1,5};
UINT4 Dot11TransmitIndicesFeedbackASOptionImplemented [ ] ={1,2,840,10036,4,2,1,6};
UINT4 Dot11ExplicitCSIFeedbackASOptionImplemented [ ] ={1,2,840,10036,4,2,1,7};
UINT4 Dot11TransmitIndicesComputationASOptionImplemented [ ] ={1,2,840,10036,4,2,1,8};
UINT4 Dot11ReceiveAntennaSelectionOptionImplemented [ ] ={1,2,840,10036,4,2,1,9};
UINT4 Dot11TransmitSoundingPPDUOptionImplemented [ ] ={1,2,840,10036,4,2,1,10};
UINT4 Dot11NumberOfActiveRxAntennas [ ] ={1,2,840,10036,4,2,1,11};
UINT4 Dot11NumberSupportedPowerLevelsImplemented [ ] ={1,2,840,10036,4,3,1,1};
UINT4 Dot11TxPowerLevel1 [ ] ={1,2,840,10036,4,3,1,2};
UINT4 Dot11TxPowerLevel2 [ ] ={1,2,840,10036,4,3,1,3};
UINT4 Dot11TxPowerLevel3 [ ] ={1,2,840,10036,4,3,1,4};
UINT4 Dot11TxPowerLevel4 [ ] ={1,2,840,10036,4,3,1,5};
UINT4 Dot11TxPowerLevel5 [ ] ={1,2,840,10036,4,3,1,6};
UINT4 Dot11TxPowerLevel6 [ ] ={1,2,840,10036,4,3,1,7};
UINT4 Dot11TxPowerLevel7 [ ] ={1,2,840,10036,4,3,1,8};
UINT4 Dot11TxPowerLevel8 [ ] ={1,2,840,10036,4,3,1,9};
UINT4 Dot11CurrentTxPowerLevel [ ] ={1,2,840,10036,4,3,1,10};
UINT4 Dot11HopTime [ ] ={1,2,840,10036,4,4,1,1};
UINT4 Dot11CurrentChannelNumber [ ] ={1,2,840,10036,4,4,1,2};
UINT4 Dot11MaxDwellTime [ ] ={1,2,840,10036,4,4,1,3};
UINT4 Dot11CurrentDwellTime [ ] ={1,2,840,10036,4,4,1,4};
UINT4 Dot11CurrentSet [ ] ={1,2,840,10036,4,4,1,5};
UINT4 Dot11CurrentPattern [ ] ={1,2,840,10036,4,4,1,6};
UINT4 Dot11CurrentIndex [ ] ={1,2,840,10036,4,4,1,7};
UINT4 Dot11EHCCPrimeRadix [ ] ={1,2,840,10036,4,4,1,8};
UINT4 Dot11EHCCNumberofChannelsFamilyIndex [ ] ={1,2,840,10036,4,4,1,9};
UINT4 Dot11EHCCCapabilityImplemented [ ] ={1,2,840,10036,4,4,1,10};
UINT4 Dot11EHCCCapabilityActivated [ ] ={1,2,840,10036,4,4,1,11};
UINT4 Dot11HopAlgorithmAdopted [ ] ={1,2,840,10036,4,4,1,12};
UINT4 Dot11RandomTableFlag [ ] ={1,2,840,10036,4,4,1,13};
UINT4 Dot11NumberofHoppingSets [ ] ={1,2,840,10036,4,4,1,14};
UINT4 Dot11HopModulus [ ] ={1,2,840,10036,4,4,1,15};
UINT4 Dot11HopOffset [ ] ={1,2,840,10036,4,4,1,16};
UINT4 Dot11CurrentChannel [ ] ={1,2,840,10036,4,5,1,1};
UINT4 Dot11CCAModeSupported [ ] ={1,2,840,10036,4,5,1,2};
UINT4 Dot11CurrentCCAMode [ ] ={1,2,840,10036,4,5,1,3};
UINT4 Dot11EDThreshold [ ] ={1,2,840,10036,4,5,1,4};
UINT4 Dot11CCAWatchdogTimerMax [ ] ={1,2,840,10036,4,6,1,1};
UINT4 Dot11CCAWatchdogCountMax [ ] ={1,2,840,10036,4,6,1,2};
UINT4 Dot11CCAWatchdogTimerMin [ ] ={1,2,840,10036,4,6,1,3};
UINT4 Dot11CCAWatchdogCountMin [ ] ={1,2,840,10036,4,6,1,4};
UINT4 Dot11RegDomainsSupportedIndex [ ] ={1,2,840,10036,4,7,1,1};
UINT4 Dot11RegDomainsImplementedValue [ ] ={1,2,840,10036,4,7,1,2};
UINT4 Dot11AntennaListIndex [ ] ={1,2,840,10036,4,8,1,1};
UINT4 Dot11TxAntennaImplemented [ ] ={1,2,840,10036,4,8,1,2};
UINT4 Dot11RxAntennaImplemented [ ] ={1,2,840,10036,4,8,1,3};
UINT4 Dot11DiversitySelectionRxImplemented [ ] ={1,2,840,10036,4,8,1,4};
UINT4 Dot11SupportedDataRatesTxIndex [ ] ={1,2,840,10036,4,9,1,1};
UINT4 Dot11ImplementedDataRatesTxValue [ ] ={1,2,840,10036,4,9,1,2};
UINT4 Dot11SupportedDataRatesRxIndex [ ] ={1,2,840,10036,4,10,1,1};
UINT4 Dot11ImplementedDataRatesRxValue [ ] ={1,2,840,10036,4,10,1,2};
UINT4 Dot11CurrentFrequency [ ] ={1,2,840,10036,4,11,1,1};
UINT4 Dot11TIThreshold [ ] ={1,2,840,10036,4,11,1,2};
UINT4 Dot11FrequencyBandsImplemented [ ] ={1,2,840,10036,4,11,1,3};
UINT4 Dot11ChannelStartingFactor [ ] ={1,2,840,10036,4,11,1,4};
UINT4 Dot11FiveMHzOperationImplemented [ ] ={1,2,840,10036,4,11,1,5};
UINT4 Dot11TenMHzOperationImplemented [ ] ={1,2,840,10036,4,11,1,6};
UINT4 Dot11TwentyMHzOperationImplemented [ ] ={1,2,840,10036,4,11,1,7};
UINT4 Dot11PhyOFDMChannelWidth [ ] ={1,2,840,10036,4,11,1,8};
UINT4 Dot11OFDMCCAEDImplemented [ ] ={1,2,840,10036,4,11,1,9};
UINT4 Dot11OFDMCCAEDRequired [ ] ={1,2,840,10036,4,11,1,10};
UINT4 Dot11OFDMEDThreshold [ ] ={1,2,840,10036,4,11,1,11};
UINT4 Dot11STATransmitPowerClass [ ] ={1,2,840,10036,4,11,1,12};
UINT4 Dot11ACRType [ ] ={1,2,840,10036,4,11,1,13};
UINT4 Dot11ShortPreambleOptionImplemented [ ] ={1,2,840,10036,4,12,1,1};
UINT4 Dot11PBCCOptionImplemented [ ] ={1,2,840,10036,4,12,1,2};
UINT4 Dot11ChannelAgilityPresent [ ] ={1,2,840,10036,4,12,1,3};
UINT4 Dot11ChannelAgilityActivated [ ] ={1,2,840,10036,4,12,1,4};
UINT4 Dot11HRCCAModeImplemented [ ] ={1,2,840,10036,4,12,1,5};
UINT4 Dot11HoppingPatternIndex [ ] ={1,2,840,10036,4,13,1,1};
UINT4 Dot11RandomTableFieldNumber [ ] ={1,2,840,10036,4,13,1,2};
UINT4 Dot11ERPPBCCOptionImplemented [ ] ={1,2,840,10036,4,14,1,1};
UINT4 Dot11ERPBCCOptionActivated [ ] ={1,2,840,10036,4,14,1,2};
UINT4 Dot11DSSSOFDMOptionImplemented [ ] ={1,2,840,10036,4,14,1,3};
UINT4 Dot11DSSSOFDMOptionActivated [ ] ={1,2,840,10036,4,14,1,4};
UINT4 Dot11ShortSlotTimeOptionImplemented [ ] ={1,2,840,10036,4,14,1,5};
UINT4 Dot11ShortSlotTimeOptionActivated [ ] ={1,2,840,10036,4,14,1,6};
UINT4 Dot11FortyMHzOperationImplemented [ ] ={1,2,840,10036,4,15,1,1};
UINT4 Dot11FortyMHzOperationActivated [ ] ={1,2,840,10036,4,15,1,2};
UINT4 Dot11CurrentPrimaryChannel [ ] ={1,2,840,10036,4,15,1,3};
UINT4 Dot11CurrentSecondaryChannel [ ] ={1,2,840,10036,4,15,1,4};
UINT4 Dot11NumberOfSpatialStreamsImplemented [ ] ={1,2,840,10036,4,15,1,5};
UINT4 Dot11NumberOfSpatialStreamsActivated [ ] ={1,2,840,10036,4,15,1,6};
UINT4 Dot11HTGreenfieldOptionImplemented [ ] ={1,2,840,10036,4,15,1,7};
UINT4 Dot11HTGreenfieldOptionActivated [ ] ={1,2,840,10036,4,15,1,8};
UINT4 Dot11ShortGIOptionInTwentyImplemented [ ] ={1,2,840,10036,4,15,1,9};
UINT4 Dot11ShortGIOptionInTwentyActivated [ ] ={1,2,840,10036,4,15,1,10};
UINT4 Dot11ShortGIOptionInFortyImplemented [ ] ={1,2,840,10036,4,15,1,11};
UINT4 Dot11ShortGIOptionInFortyActivated [ ] ={1,2,840,10036,4,15,1,12};
UINT4 Dot11LDPCCodingOptionImplemented [ ] ={1,2,840,10036,4,15,1,13};
UINT4 Dot11LDPCCodingOptionActivated [ ] ={1,2,840,10036,4,15,1,14};
UINT4 Dot11TxSTBCOptionImplemented [ ] ={1,2,840,10036,4,15,1,15};
UINT4 Dot11TxSTBCOptionActivated [ ] ={1,2,840,10036,4,15,1,16};
UINT4 Dot11RxSTBCOptionImplemented [ ] ={1,2,840,10036,4,15,1,17};
UINT4 Dot11RxSTBCOptionActivated [ ] ={1,2,840,10036,4,15,1,18};
UINT4 Dot11BeamFormingOptionImplemented [ ] ={1,2,840,10036,4,15,1,19};
UINT4 Dot11BeamFormingOptionActivated [ ] ={1,2,840,10036,4,15,1,20};
UINT4 Dot11HighestSupportedDataRate [ ] ={1,2,840,10036,4,15,1,21};
UINT4 Dot11TxMCSSetDefined [ ] ={1,2,840,10036,4,15,1,22};
UINT4 Dot11TxRxMCSSetNotEqual [ ] ={1,2,840,10036,4,15,1,23};
UINT4 Dot11TxMaximumNumberSpatialStreamsSupported [ ] ={1,2,840,10036,4,15,1,24};
UINT4 Dot11TxUnequalModulationSupported [ ] ={1,2,840,10036,4,15,1,25};
UINT4 Dot11SupportedMCSTxIndex [ ] ={1,2,840,10036,4,16,1,1};
UINT4 Dot11SupportedMCSTxValue [ ] ={1,2,840,10036,4,16,1,2};
UINT4 Dot11SupportedMCSRxIndex [ ] ={1,2,840,10036,4,17,1,1};
UINT4 Dot11SupportedMCSRxValue [ ] ={1,2,840,10036,4,17,1,2};
UINT4 Dot11ReceiveStaggerSoundingOptionImplemented [ ] ={1,2,840,10036,4,18,1,1};
UINT4 Dot11TransmitStaggerSoundingOptionImplemented [ ] ={1,2,840,10036,4,18,1,2};
UINT4 Dot11ReceiveNDPOptionImplemented [ ] ={1,2,840,10036,4,18,1,3};
UINT4 Dot11TransmitNDPOptionImplemented [ ] ={1,2,840,10036,4,18,1,4};
UINT4 Dot11ImplicitTransmitBeamformingOptionImplemented [ ] ={1,2,840,10036,4,18,1,5};
UINT4 Dot11CalibrationOptionImplemented [ ] ={1,2,840,10036,4,18,1,6};
UINT4 Dot11ExplicitCSITransmitBeamformingOptionImplemented [ ] ={1,2,840,10036,4,18,1,7};
UINT4 Dot11ExplicitNonCompressedBeamformingMatrixOptionImplemented [ ] ={1,2,840,10036,4,18,1,8};
UINT4 Dot11ExplicitTransmitBeamformingCSIFeedbackOptionImplemented [ ] ={1,2,840,10036,4,18,1,9};
UINT4 Dot11ExplicitNonCompressedBeamformingFeedbackOptionImplemented [ ] ={1,2,840,10036,4,18,1,10};
UINT4 Dot11ExplicitCompressedBeamformingFeedbackOptionImplemented [ ] ={1,2,840,10036,4,18,1,11};
UINT4 Dot11NumberBeamFormingCSISupportAntenna [ ] ={1,2,840,10036,4,18,1,12};
UINT4 Dot11NumberNonCompressedBeamformingMatrixSupportAntenna [ ] ={1,2,840,10036,4,18,1,13};
UINT4 Dot11NumberCompressedBeamformingMatrixSupportAntenna [ ] ={1,2,840,10036,4,18,1,14};
UINT4 Dot11APMacAddress [ ] ={1,2,840,10036,6,1,1,1};
UINT4 Dot11NonAPStationMacAddress [ ] ={1,2,840,10036,6,2,1,1};
UINT4 Dot11NonAPStationUserIdentity [ ] ={1,2,840,10036,6,2,1,2};
UINT4 Dot11NonAPStationInterworkingCapability [ ] ={1,2,840,10036,6,2,1,3};
UINT4 Dot11NonAPStationAssociatedSSID [ ] ={1,2,840,10036,6,2,1,4};
UINT4 Dot11NonAPStationUnicastCipherSuite [ ] ={1,2,840,10036,6,2,1,5};
UINT4 Dot11NonAPStationBroadcastCipherSuite [ ] ={1,2,840,10036,6,2,1,6};
UINT4 Dot11NonAPStationAuthAccessCategories [ ] ={1,2,840,10036,6,2,1,7};
UINT4 Dot11NonAPStationAuthMaxVoiceRate [ ] ={1,2,840,10036,6,2,1,8};
UINT4 Dot11NonAPStationAuthMaxVideoRate [ ] ={1,2,840,10036,6,2,1,9};
UINT4 Dot11NonAPStationAuthMaxBestEffortRate [ ] ={1,2,840,10036,6,2,1,10};
UINT4 Dot11NonAPStationAuthMaxBackgroundRate [ ] ={1,2,840,10036,6,2,1,11};
UINT4 Dot11NonAPStationAuthMaxVoiceOctets [ ] ={1,2,840,10036,6,2,1,12};
UINT4 Dot11NonAPStationAuthMaxVideoOctets [ ] ={1,2,840,10036,6,2,1,13};
UINT4 Dot11NonAPStationAuthMaxBestEffortOctets [ ] ={1,2,840,10036,6,2,1,14};
UINT4 Dot11NonAPStationAuthMaxBackgroundOctets [ ] ={1,2,840,10036,6,2,1,15};
UINT4 Dot11NonAPStationAuthMaxHCCAHEMMOctets [ ] ={1,2,840,10036,6,2,1,16};
UINT4 Dot11NonAPStationAuthMaxTotalOctets [ ] ={1,2,840,10036,6,2,1,17};
UINT4 Dot11NonAPStationAuthHCCAHEMM [ ] ={1,2,840,10036,6,2,1,18};
UINT4 Dot11NonAPStationAuthMaxHCCAHEMMRate [ ] ={1,2,840,10036,6,2,1,19};
UINT4 Dot11NonAPStationAuthHCCAHEMMDelay [ ] ={1,2,840,10036,6,2,1,20};
UINT4 Dot11NonAPStationAuthSourceMulticast [ ] ={1,2,840,10036,6,2,1,21};
UINT4 Dot11NonAPStationAuthMaxSourceMulticastRate [ ] ={1,2,840,10036,6,2,1,22};
UINT4 Dot11NonAPStationVoiceMSDUCount [ ] ={1,2,840,10036,6,2,1,23};
UINT4 Dot11NonAPStationDroppedVoiceMSDUCount [ ] ={1,2,840,10036,6,2,1,24};
UINT4 Dot11NonAPStationVoiceOctetCount [ ] ={1,2,840,10036,6,2,1,25};
UINT4 Dot11NonAPStationDroppedVoiceOctetCount [ ] ={1,2,840,10036,6,2,1,26};
UINT4 Dot11NonAPStationVideoMSDUCount [ ] ={1,2,840,10036,6,2,1,27};
UINT4 Dot11NonAPStationDroppedVideoMSDUCount [ ] ={1,2,840,10036,6,2,1,28};
UINT4 Dot11NonAPStationVideoOctetCount [ ] ={1,2,840,10036,6,2,1,29};
UINT4 Dot11NonAPStationDroppedVideoOctetCount [ ] ={1,2,840,10036,6,2,1,30};
UINT4 Dot11NonAPStationBestEffortMSDUCount [ ] ={1,2,840,10036,6,2,1,31};
UINT4 Dot11NonAPStationDroppedBestEffortMSDUCount [ ] ={1,2,840,10036,6,2,1,32};
UINT4 Dot11NonAPStationBestEffortOctetCount [ ] ={1,2,840,10036,6,2,1,33};
UINT4 Dot11NonAPStationDroppedBestEffortOctetCount [ ] ={1,2,840,10036,6,2,1,34};
UINT4 Dot11NonAPStationBackgroundMSDUCount [ ] ={1,2,840,10036,6,2,1,35};
UINT4 Dot11NonAPStationDroppedBackgroundMSDUCount [ ] ={1,2,840,10036,6,2,1,36};
UINT4 Dot11NonAPStationBackgroundOctetCount [ ] ={1,2,840,10036,6,2,1,37};
UINT4 Dot11NonAPStationDroppedBackgroundOctetCount [ ] ={1,2,840,10036,6,2,1,38};
UINT4 Dot11NonAPStationHCCAHEMMMSDUCount [ ] ={1,2,840,10036,6,2,1,39};
UINT4 Dot11NonAPStationDroppedHCCAHEMMMSDUCount [ ] ={1,2,840,10036,6,2,1,40};
UINT4 Dot11NonAPStationHCCAHEMMOctetCount [ ] ={1,2,840,10036,6,2,1,41};
UINT4 Dot11NonAPStationDroppedHCCAHEMMOctetCount [ ] ={1,2,840,10036,6,2,1,42};
UINT4 Dot11NonAPStationMulticastMSDUCount [ ] ={1,2,840,10036,6,2,1,43};
UINT4 Dot11NonAPStationDroppedMulticastMSDUCount [ ] ={1,2,840,10036,6,2,1,44};
UINT4 Dot11NonAPStationMulticastOctetCount [ ] ={1,2,840,10036,6,2,1,45};
UINT4 Dot11NonAPStationDroppedMulticastOctetCount [ ] ={1,2,840,10036,6,2,1,46};
UINT4 Dot11NonAPStationPowerManagementMode [ ] ={1,2,840,10036,6,2,1,47};
UINT4 Dot11NonAPStationAuthDls [ ] ={1,2,840,10036,6,2,1,48};
UINT4 Dot11NonAPStationVLANId [ ] ={1,2,840,10036,6,2,1,49};
UINT4 Dot11NonAPStationVLANName [ ] ={1,2,840,10036,6,2,1,50};
UINT4 Dot11NonAPStationAddtsResultCode [ ] ={1,2,840,10036,6,2,1,51};
UINT4 Dot11APLCIIndex [ ] ={1,2,840,10036,6,3,1,1};
UINT4 Dot11APLCILatitudeResolution [ ] ={1,2,840,10036,6,3,1,2};
UINT4 Dot11APLCILatitudeInteger [ ] ={1,2,840,10036,6,3,1,3};
UINT4 Dot11APLCILatitudeFraction [ ] ={1,2,840,10036,6,3,1,4};
UINT4 Dot11APLCILongitudeResolution [ ] ={1,2,840,10036,6,3,1,5};
UINT4 Dot11APLCILongitudeInteger [ ] ={1,2,840,10036,6,3,1,6};
UINT4 Dot11APLCILongitudeFraction [ ] ={1,2,840,10036,6,3,1,7};
UINT4 Dot11APLCIAltitudeType [ ] ={1,2,840,10036,6,3,1,8};
UINT4 Dot11APLCIAltitudeResolution [ ] ={1,2,840,10036,6,3,1,9};
UINT4 Dot11APLCIAltitudeInteger [ ] ={1,2,840,10036,6,3,1,10};
UINT4 Dot11APLCIAltitudeFraction [ ] ={1,2,840,10036,6,3,1,11};
UINT4 Dot11APLCIDatum [ ] ={1,2,840,10036,6,3,1,12};
UINT4 Dot11APLCIAzimuthType [ ] ={1,2,840,10036,6,3,1,13};
UINT4 Dot11APLCIAzimuthResolution [ ] ={1,2,840,10036,6,3,1,14};
UINT4 Dot11APLCIAzimuth [ ] ={1,2,840,10036,6,3,1,15};
UINT4 Dot11APCivicLocationIndex [ ] ={1,2,840,10036,6,4,1,1};
UINT4 Dot11APCivicLocationCountry [ ] ={1,2,840,10036,6,4,1,2};
UINT4 Dot11APCivicLocationA1 [ ] ={1,2,840,10036,6,4,1,3};
UINT4 Dot11APCivicLocationA2 [ ] ={1,2,840,10036,6,4,1,4};
UINT4 Dot11APCivicLocationA3 [ ] ={1,2,840,10036,6,4,1,5};
UINT4 Dot11APCivicLocationA4 [ ] ={1,2,840,10036,6,4,1,6};
UINT4 Dot11APCivicLocationA5 [ ] ={1,2,840,10036,6,4,1,7};
UINT4 Dot11APCivicLocationA6 [ ] ={1,2,840,10036,6,4,1,8};
UINT4 Dot11APCivicLocationPrd [ ] ={1,2,840,10036,6,4,1,9};
UINT4 Dot11APCivicLocationPod [ ] ={1,2,840,10036,6,4,1,10};
UINT4 Dot11APCivicLocationSts [ ] ={1,2,840,10036,6,4,1,11};
UINT4 Dot11APCivicLocationHno [ ] ={1,2,840,10036,6,4,1,12};
UINT4 Dot11APCivicLocationHns [ ] ={1,2,840,10036,6,4,1,13};
UINT4 Dot11APCivicLocationLmk [ ] ={1,2,840,10036,6,4,1,14};
UINT4 Dot11APCivicLocationLoc [ ] ={1,2,840,10036,6,4,1,15};
UINT4 Dot11APCivicLocationNam [ ] ={1,2,840,10036,6,4,1,16};
UINT4 Dot11APCivicLocationPc [ ] ={1,2,840,10036,6,4,1,17};
UINT4 Dot11APCivicLocationBld [ ] ={1,2,840,10036,6,4,1,18};
UINT4 Dot11APCivicLocationUnit [ ] ={1,2,840,10036,6,4,1,19};
UINT4 Dot11APCivicLocationFlr [ ] ={1,2,840,10036,6,4,1,20};
UINT4 Dot11APCivicLocationRoom [ ] ={1,2,840,10036,6,4,1,21};
UINT4 Dot11APCivicLocationPlc [ ] ={1,2,840,10036,6,4,1,22};
UINT4 Dot11APCivicLocationPcn [ ] ={1,2,840,10036,6,4,1,23};
UINT4 Dot11APCivicLocationPobox [ ] ={1,2,840,10036,6,4,1,24};
UINT4 Dot11APCivicLocationAddcode [ ] ={1,2,840,10036,6,4,1,25};
UINT4 Dot11APCivicLocationSeat [ ] ={1,2,840,10036,6,4,1,26};
UINT4 Dot11APCivicLocationRd [ ] ={1,2,840,10036,6,4,1,27};
UINT4 Dot11APCivicLocationRdsec [ ] ={1,2,840,10036,6,4,1,28};
UINT4 Dot11APCivicLocationRdbr [ ] ={1,2,840,10036,6,4,1,29};
UINT4 Dot11APCivicLocationRdsubbr [ ] ={1,2,840,10036,6,4,1,30};
UINT4 Dot11APCivicLocationPrm [ ] ={1,2,840,10036,6,4,1,31};
UINT4 Dot11APCivicLocationPom [ ] ={1,2,840,10036,6,4,1,32};
UINT4 Dot11RoamingConsortiumOI [ ] ={1,2,840,10036,6,5,1,1};
UINT4 Dot11RoamingConsortiumRowStatus [ ] ={1,2,840,10036,6,5,1,2};
UINT4 Dot11DomainName [ ] ={1,2,840,10036,6,6,1,1};
UINT4 Dot11DomainNameRowStatus [ ] ={1,2,840,10036,6,6,1,2};
UINT4 Dot11DomainNameOui [ ] ={1,2,840,10036,6,6,1,3};
UINT4 Dot11GASAdvertisementId [ ] ={1,2,840,10036,6,7,1,1};
UINT4 Dot11GASPauseForServerResponse [ ] ={1,2,840,10036,6,7,1,2};
UINT4 Dot11GASResponseTimeout [ ] ={1,2,840,10036,6,7,1,3};
UINT4 Dot11GASComebackDelay [ ] ={1,2,840,10036,6,7,1,4};
UINT4 Dot11GASResponseBufferingTime [ ] ={1,2,840,10036,6,7,1,5};
UINT4 Dot11GASQueryResponseLengthLimit [ ] ={1,2,840,10036,6,7,1,6};
UINT4 Dot11GASQueries [ ] ={1,2,840,10036,6,7,1,7};
UINT4 Dot11GASQueryRate [ ] ={1,2,840,10036,6,7,1,8};
UINT4 Dot11GASResponses [ ] ={1,2,840,10036,6,7,1,9};
UINT4 Dot11GASResponseRate [ ] ={1,2,840,10036,6,7,1,10};
UINT4 Dot11GASTransmittedFragmentCount [ ] ={1,2,840,10036,6,7,1,11};
UINT4 Dot11GASReceivedFragmentCount [ ] ={1,2,840,10036,6,7,1,12};
UINT4 Dot11GASNoRequestOutstanding [ ] ={1,2,840,10036,6,7,1,13};
UINT4 Dot11GASResponsesDiscarded [ ] ={1,2,840,10036,6,7,1,14};
UINT4 Dot11GASFailedResponses [ ] ={1,2,840,10036,6,7,1,15};
UINT4 Dot11ESSDisconnectFilterInterval [ ] ={1,2,840,10036,7,1,1,1};
UINT4 Dot11ESSLinkDetectionHoldInterval [ ] ={1,2,840,10036,7,1,1,2};
UINT4 Dot11MSCEESSLinkIdentifier [ ] ={1,2,840,10036,7,1,1,3};
UINT4 Dot11MSCENonAPStationMacAddress [ ] ={1,2,840,10036,7,1,1,4};
UINT4 Dot11ESSLinkDownTimeInterval [ ] ={1,2,840,10036,7,2,1,2};
UINT4 Dot11ESSLinkRssiDataThreshold [ ] ={1,2,840,10036,7,2,1,3};
UINT4 Dot11ESSLinkRssiBeaconThreshold [ ] ={1,2,840,10036,7,2,1,4};
UINT4 Dot11ESSLinkBeaconSnrThreshold [ ] ={1,2,840,10036,7,2,1,5};
UINT4 Dot11ESSLinkDataSnrThreshold [ ] ={1,2,840,10036,7,2,1,6};
UINT4 Dot11ESSLinkBeaconFrameErrorRateThresholdInteger [ ] ={1,2,840,10036,7,2,1,7};
UINT4 Dot11ESSLinkBeaconFrameErrorRateThresholdFraction [ ] ={1,2,840,10036,7,2,1,8};
UINT4 Dot11ESSLinkBeaconFrameErrorRateThresholdExponent [ ] ={1,2,840,10036,7,2,1,9};
UINT4 Dot11ESSLinkFrameErrorRateThresholdInteger [ ] ={1,2,840,10036,7,2,1,10};
UINT4 Dot11ESSLinkFrameErrorRateThresholdFraction [ ] ={1,2,840,10036,7,2,1,11};
UINT4 Dot11ESSLinkFrameErrorRateThresholdExponent [ ] ={1,2,840,10036,7,2,1,12};
UINT4 Dot11PeakOperationalRate [ ] ={1,2,840,10036,7,2,1,13};
UINT4 Dot11MinimumOperationalRate [ ] ={1,2,840,10036,7,2,1,14};
UINT4 Dot11ESSLinkDataThroughputInteger [ ] ={1,2,840,10036,7,2,1,15};
UINT4 Dot11ESSLinkDataThroughputFraction [ ] ={1,2,840,10036,7,2,1,16};
UINT4 Dot11ESSLinkDataThroughputExponent [ ] ={1,2,840,10036,7,2,1,17};
UINT4 Dot11MSPEESSLinkIdentifier [ ] ={1,2,840,10036,7,2,1,18};
UINT4 Dot11MSPENonAPStationMacAddress [ ] ={1,2,840,10036,7,2,1,19};
UINT4 Dot11ESSLinkDetectedIndex [ ] ={1,2,840,10036,7,3,1,1};
UINT4 Dot11ESSLinkDetectedNetworkId [ ] ={1,2,840,10036,7,3,1,2};
UINT4 Dot11ESSLinkDetectedNetworkDetectTime [ ] ={1,2,840,10036,7,3,1,4};
UINT4 Dot11ESSLinkDetectedNetworkModifiedTime [ ] ={1,2,840,10036,7,3,1,5};
UINT4 Dot11ESSLinkDetectedNetworkMIHCapabilities [ ] ={1,2,840,10036,7,3,1,6};
UINT4 Dot11MSELDEESSLinkIdentifier [ ] ={1,2,840,10036,7,3,1,7};
UINT4 Dot11MSELDENonAPStationMacAddress [ ] ={1,2,840,10036,7,3,1,8};


tSNMP_OID_TYPE Dot11RMRequestNextIndexOID = {8, Dot11RMRequestNextIndex};


tSNMP_OID_TYPE Dot11LocationServicesNextIndexOID = {6, Dot11LocationServicesNextIndex};


tSNMP_OID_TYPE Dot11WNMRequestNextIndexOID = {8, Dot11WNMRequestNextIndex};


tSNMP_OID_TYPE Dot11ResourceTypeIDNameOID = {7, Dot11ResourceTypeIDName};




tMbDbEntry Dot11StationConfigTableMibEntry[]= {

{{8,Dot11StationID}, GetNextIndexDot11StationConfigTable, Dot11StationIDGet, Dot11StationIDSet, Dot11StationIDTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MediumOccupancyLimit}, GetNextIndexDot11StationConfigTable, Dot11MediumOccupancyLimitGet, Dot11MediumOccupancyLimitSet, Dot11MediumOccupancyLimitTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "100"},

{{8,Dot11CFPollable}, GetNextIndexDot11StationConfigTable, Dot11CFPollableGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CFPPeriod}, GetNextIndexDot11StationConfigTable, Dot11CFPPeriodGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CFPMaxDuration}, GetNextIndexDot11StationConfigTable, Dot11CFPMaxDurationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11AuthenticationResponseTimeOut}, GetNextIndexDot11StationConfigTable, Dot11AuthenticationResponseTimeOutGet, Dot11AuthenticationResponseTimeOutSet, Dot11AuthenticationResponseTimeOutTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11PrivacyOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11PrivacyOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11PowerManagementMode}, GetNextIndexDot11StationConfigTable, Dot11PowerManagementModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DesiredSSID}, GetNextIndexDot11StationConfigTable, Dot11DesiredSSIDGet, Dot11DesiredSSIDSet, Dot11DesiredSSIDTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DesiredBSSType}, GetNextIndexDot11StationConfigTable, Dot11DesiredBSSTypeGet, Dot11DesiredBSSTypeSet, Dot11DesiredBSSTypeTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11OperationalRateSet}, GetNextIndexDot11StationConfigTable, Dot11OperationalRateSetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11BeaconPeriod}, GetNextIndexDot11StationConfigTable, Dot11BeaconPeriodGet, Dot11BeaconPeriodSet, Dot11BeaconPeriodTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DTIMPeriod}, GetNextIndexDot11StationConfigTable, Dot11DTIMPeriodGet, Dot11DTIMPeriodSet, Dot11DTIMPeriodTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11AssociationResponseTimeOut}, GetNextIndexDot11StationConfigTable, Dot11AssociationResponseTimeOutGet, Dot11AssociationResponseTimeOutSet, Dot11AssociationResponseTimeOutTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DisassociateReason}, GetNextIndexDot11StationConfigTable, Dot11DisassociateReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DisassociateStation}, GetNextIndexDot11StationConfigTable, Dot11DisassociateStationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DeauthenticateReason}, GetNextIndexDot11StationConfigTable, Dot11DeauthenticateReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DeauthenticateStation}, GetNextIndexDot11StationConfigTable, Dot11DeauthenticateStationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11AuthenticateFailStatus}, GetNextIndexDot11StationConfigTable, Dot11AuthenticateFailStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11AuthenticateFailStation}, GetNextIndexDot11StationConfigTable, Dot11AuthenticateFailStationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MultiDomainCapabilityImplemented}, GetNextIndexDot11StationConfigTable, Dot11MultiDomainCapabilityImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MultiDomainCapabilityActivated}, GetNextIndexDot11StationConfigTable, Dot11MultiDomainCapabilityActivatedGet, Dot11MultiDomainCapabilityActivatedSet, Dot11MultiDomainCapabilityActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11CountryString}, GetNextIndexDot11StationConfigTable, Dot11CountryStringGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11SpectrumManagementImplemented}, GetNextIndexDot11StationConfigTable, Dot11SpectrumManagementImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11SpectrumManagementRequired}, GetNextIndexDot11StationConfigTable, Dot11SpectrumManagementRequiredGet, Dot11SpectrumManagementRequiredSet, Dot11SpectrumManagementRequiredTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RSNAOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11RSNAOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAPreauthenticationImplemented}, GetNextIndexDot11StationConfigTable, Dot11RSNAPreauthenticationImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11OperatingClassesImplemented}, GetNextIndexDot11StationConfigTable, Dot11OperatingClassesImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11OperatingClassesRequired}, GetNextIndexDot11StationConfigTable, Dot11OperatingClassesRequiredGet, Dot11OperatingClassesRequiredSet, Dot11OperatingClassesRequiredTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11QosOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11QosOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11ImmediateBlockAckOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11ImmediateBlockAckOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11DelayedBlockAckOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11DelayedBlockAckOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11DirectOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11DirectOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11APSDOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11APSDOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11QAckOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11QAckOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11QBSSLoadImplemented}, GetNextIndexDot11StationConfigTable, Dot11QBSSLoadImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11QueueRequestOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11QueueRequestOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TXOPRequestOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11TXOPRequestOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MoreDataAckOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11MoreDataAckOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11AssociateInNQBSS}, GetNextIndexDot11StationConfigTable, Dot11AssociateInNQBSSGet, Dot11AssociateInNQBSSSet, Dot11AssociateInNQBSSTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "1"},

{{8,Dot11DLSAllowedInQBSS}, GetNextIndexDot11StationConfigTable, Dot11DLSAllowedInQBSSGet, Dot11DLSAllowedInQBSSSet, Dot11DLSAllowedInQBSSTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "1"},

{{8,Dot11DLSAllowed}, GetNextIndexDot11StationConfigTable, Dot11DLSAllowedGet, Dot11DLSAllowedSet, Dot11DLSAllowedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "1"},

{{8,Dot11AssociateStation}, GetNextIndexDot11StationConfigTable, Dot11AssociateStationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11AssociateID}, GetNextIndexDot11StationConfigTable, Dot11AssociateIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11AssociateFailStation}, GetNextIndexDot11StationConfigTable, Dot11AssociateFailStationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11AssociateFailStatus}, GetNextIndexDot11StationConfigTable, Dot11AssociateFailStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ReassociateStation}, GetNextIndexDot11StationConfigTable, Dot11ReassociateStationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ReassociateID}, GetNextIndexDot11StationConfigTable, Dot11ReassociateIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ReassociateFailStation}, GetNextIndexDot11StationConfigTable, Dot11ReassociateFailStationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ReassociateFailStatus}, GetNextIndexDot11StationConfigTable, Dot11ReassociateFailStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RadioMeasurementImplemented}, GetNextIndexDot11StationConfigTable, Dot11RadioMeasurementImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RadioMeasurementActivated}, GetNextIndexDot11StationConfigTable, Dot11RadioMeasurementActivatedGet, Dot11RadioMeasurementActivatedSet, Dot11RadioMeasurementActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMMeasurementProbeDelay}, GetNextIndexDot11StationConfigTable, Dot11RMMeasurementProbeDelayGet, Dot11RMMeasurementProbeDelaySet, Dot11RMMeasurementProbeDelayTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RMMeasurementPilotPeriod}, GetNextIndexDot11StationConfigTable, Dot11RMMeasurementPilotPeriodGet, Dot11RMMeasurementPilotPeriodSet, Dot11RMMeasurementPilotPeriodTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RMLinkMeasurementActivated}, GetNextIndexDot11StationConfigTable, Dot11RMLinkMeasurementActivatedGet, Dot11RMLinkMeasurementActivatedSet, Dot11RMLinkMeasurementActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMNeighborReportActivated}, GetNextIndexDot11StationConfigTable, Dot11RMNeighborReportActivatedGet, Dot11RMNeighborReportActivatedSet, Dot11RMNeighborReportActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMParallelMeasurementsActivated}, GetNextIndexDot11StationConfigTable, Dot11RMParallelMeasurementsActivatedGet, Dot11RMParallelMeasurementsActivatedSet, Dot11RMParallelMeasurementsActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMRepeatedMeasurementsActivated}, GetNextIndexDot11StationConfigTable, Dot11RMRepeatedMeasurementsActivatedGet, Dot11RMRepeatedMeasurementsActivatedSet, Dot11RMRepeatedMeasurementsActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMBeaconPassiveMeasurementActivated}, GetNextIndexDot11StationConfigTable, Dot11RMBeaconPassiveMeasurementActivatedGet, Dot11RMBeaconPassiveMeasurementActivatedSet, Dot11RMBeaconPassiveMeasurementActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMBeaconActiveMeasurementActivated}, GetNextIndexDot11StationConfigTable, Dot11RMBeaconActiveMeasurementActivatedGet, Dot11RMBeaconActiveMeasurementActivatedSet, Dot11RMBeaconActiveMeasurementActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMBeaconTableMeasurementActivated}, GetNextIndexDot11StationConfigTable, Dot11RMBeaconTableMeasurementActivatedGet, Dot11RMBeaconTableMeasurementActivatedSet, Dot11RMBeaconTableMeasurementActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMBeaconMeasurementReportingConditionsActivated}, GetNextIndexDot11StationConfigTable, Dot11RMBeaconMeasurementReportingConditionsActivatedGet, Dot11RMBeaconMeasurementReportingConditionsActivatedSet, Dot11RMBeaconMeasurementReportingConditionsActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMFrameMeasurementActivated}, GetNextIndexDot11StationConfigTable, Dot11RMFrameMeasurementActivatedGet, Dot11RMFrameMeasurementActivatedSet, Dot11RMFrameMeasurementActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMChannelLoadMeasurementActivated}, GetNextIndexDot11StationConfigTable, Dot11RMChannelLoadMeasurementActivatedGet, Dot11RMChannelLoadMeasurementActivatedSet, Dot11RMChannelLoadMeasurementActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMNoiseHistogramMeasurementActivated}, GetNextIndexDot11StationConfigTable, Dot11RMNoiseHistogramMeasurementActivatedGet, Dot11RMNoiseHistogramMeasurementActivatedSet, Dot11RMNoiseHistogramMeasurementActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMStatisticsMeasurementActivated}, GetNextIndexDot11StationConfigTable, Dot11RMStatisticsMeasurementActivatedGet, Dot11RMStatisticsMeasurementActivatedSet, Dot11RMStatisticsMeasurementActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMLCIMeasurementActivated}, GetNextIndexDot11StationConfigTable, Dot11RMLCIMeasurementActivatedGet, Dot11RMLCIMeasurementActivatedSet, Dot11RMLCIMeasurementActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMLCIAzimuthActivated}, GetNextIndexDot11StationConfigTable, Dot11RMLCIAzimuthActivatedGet, Dot11RMLCIAzimuthActivatedSet, Dot11RMLCIAzimuthActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMTransmitStreamCategoryMeasurementActivated}, GetNextIndexDot11StationConfigTable, Dot11RMTransmitStreamCategoryMeasurementActivatedGet, Dot11RMTransmitStreamCategoryMeasurementActivatedSet, Dot11RMTransmitStreamCategoryMeasurementActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMTriggeredTransmitStreamCategoryMeasurementActivated}, GetNextIndexDot11StationConfigTable, Dot11RMTriggeredTransmitStreamCategoryMeasurementActivatedGet, Dot11RMTriggeredTransmitStreamCategoryMeasurementActivatedSet, Dot11RMTriggeredTransmitStreamCategoryMeasurementActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMAPChannelReportActivated}, GetNextIndexDot11StationConfigTable, Dot11RMAPChannelReportActivatedGet, Dot11RMAPChannelReportActivatedSet, Dot11RMAPChannelReportActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMMIBActivated}, GetNextIndexDot11StationConfigTable, Dot11RMMIBActivatedGet, Dot11RMMIBActivatedSet, Dot11RMMIBActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMMaxMeasurementDuration}, GetNextIndexDot11StationConfigTable, Dot11RMMaxMeasurementDurationGet, Dot11RMMaxMeasurementDurationSet, Dot11RMMaxMeasurementDurationTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RMNonOperatingChannelMaxMeasurementDuration}, GetNextIndexDot11StationConfigTable, Dot11RMNonOperatingChannelMaxMeasurementDurationGet, Dot11RMNonOperatingChannelMaxMeasurementDurationSet, Dot11RMNonOperatingChannelMaxMeasurementDurationTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RMMeasurementPilotTransmissionInformationActivated}, GetNextIndexDot11StationConfigTable, Dot11RMMeasurementPilotTransmissionInformationActivatedGet, Dot11RMMeasurementPilotTransmissionInformationActivatedSet, Dot11RMMeasurementPilotTransmissionInformationActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMMeasurementPilotActivated}, GetNextIndexDot11StationConfigTable, Dot11RMMeasurementPilotActivatedGet, Dot11RMMeasurementPilotActivatedSet, Dot11RMMeasurementPilotActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "0"},

{{8,Dot11RMNeighborReportTSFOffsetActivated}, GetNextIndexDot11StationConfigTable, Dot11RMNeighborReportTSFOffsetActivatedGet, Dot11RMNeighborReportTSFOffsetActivatedSet, Dot11RMNeighborReportTSFOffsetActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMRCPIMeasurementActivated}, GetNextIndexDot11StationConfigTable, Dot11RMRCPIMeasurementActivatedGet, Dot11RMRCPIMeasurementActivatedSet, Dot11RMRCPIMeasurementActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMRSNIMeasurementActivated}, GetNextIndexDot11StationConfigTable, Dot11RMRSNIMeasurementActivatedGet, Dot11RMRSNIMeasurementActivatedSet, Dot11RMRSNIMeasurementActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMBSSAverageAccessDelayActivated}, GetNextIndexDot11StationConfigTable, Dot11RMBSSAverageAccessDelayActivatedGet, Dot11RMBSSAverageAccessDelayActivatedSet, Dot11RMBSSAverageAccessDelayActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMBSSAvailableAdmissionCapacityActivated}, GetNextIndexDot11StationConfigTable, Dot11RMBSSAvailableAdmissionCapacityActivatedGet, Dot11RMBSSAvailableAdmissionCapacityActivatedSet, Dot11RMBSSAvailableAdmissionCapacityActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMAntennaInformationActivated}, GetNextIndexDot11StationConfigTable, Dot11RMAntennaInformationActivatedGet, Dot11RMAntennaInformationActivatedSet, Dot11RMAntennaInformationActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11FastBSSTransitionImplemented}, GetNextIndexDot11StationConfigTable, Dot11FastBSSTransitionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11LCIDSEImplemented}, GetNextIndexDot11StationConfigTable, Dot11LCIDSEImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11LCIDSERequired}, GetNextIndexDot11StationConfigTable, Dot11LCIDSERequiredGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11DSERequired}, GetNextIndexDot11StationConfigTable, Dot11DSERequiredGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11ExtendedChannelSwitchActivated}, GetNextIndexDot11StationConfigTable, Dot11ExtendedChannelSwitchActivatedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},
#ifdef PMF_WANTED
{{8,Dot11RSNAProtectedManagementFramesActivated}, GetNextIndexDot11StationConfigTable, Dot11RSNAProtectedManagementFramesActivatedGet, Dot11RSNAProtectedManagementFramesActivatedSet, Dot11RSNAProtectedManagementFramesActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RSNAUnprotectedManagementFramesAllowed}, GetNextIndexDot11StationConfigTable, Dot11RSNAUnprotectedManagementFramesAllowedGet, Dot11RSNAUnprotectedManagementFramesAllowedSet, Dot11RSNAUnprotectedManagementFramesAllowedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "1"},

{{8,Dot11AssociationSAQueryMaximumTimeout}, GetNextIndexDot11StationConfigTable, Dot11AssociationSAQueryMaximumTimeoutGet, Dot11AssociationSAQueryMaximumTimeoutSet, Dot11AssociationSAQueryMaximumTimeoutTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "1000"},

{{8,Dot11AssociationSAQueryRetryTimeout}, GetNextIndexDot11StationConfigTable, Dot11AssociationSAQueryRetryTimeoutGet, Dot11AssociationSAQueryRetryTimeoutSet, Dot11AssociationSAQueryRetryTimeoutTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "201"},
#endif
{{8,Dot11HighThroughputOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11HighThroughputOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAPBACRequired}, GetNextIndexDot11StationConfigTable, Dot11RSNAPBACRequiredGet, Dot11RSNAPBACRequiredSet, Dot11RSNAPBACRequiredTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11PSMPOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11PSMPOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TunneledDirectLinkSetupImplemented}, GetNextIndexDot11StationConfigTable, Dot11TunneledDirectLinkSetupImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TDLSPeerUAPSDBufferSTAActivated}, GetNextIndexDot11StationConfigTable, Dot11TDLSPeerUAPSDBufferSTAActivatedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TDLSPeerPSMActivated}, GetNextIndexDot11StationConfigTable, Dot11TDLSPeerPSMActivatedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TDLSPeerUAPSDIndicationWindow}, GetNextIndexDot11StationConfigTable, Dot11TDLSPeerUAPSDIndicationWindowGet, Dot11TDLSPeerUAPSDIndicationWindowSet, Dot11TDLSPeerUAPSDIndicationWindowTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "1"},

{{8,Dot11TDLSChannelSwitchingActivated}, GetNextIndexDot11StationConfigTable, Dot11TDLSChannelSwitchingActivatedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TDLSPeerSTAMissingAckRetryLimit}, GetNextIndexDot11StationConfigTable, Dot11TDLSPeerSTAMissingAckRetryLimitGet, Dot11TDLSPeerSTAMissingAckRetryLimitSet, Dot11TDLSPeerSTAMissingAckRetryLimitTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "3"},

{{8,Dot11TDLSResponseTimeout}, GetNextIndexDot11StationConfigTable, Dot11TDLSResponseTimeoutGet, Dot11TDLSResponseTimeoutSet, Dot11TDLSResponseTimeoutTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "5"},

{{8,Dot11OCBActivated}, GetNextIndexDot11StationConfigTable, Dot11OCBActivatedGet, Dot11OCBActivatedSet, Dot11OCBActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TDLSProbeDelay}, GetNextIndexDot11StationConfigTable, Dot11TDLSProbeDelayGet, Dot11TDLSProbeDelaySet, Dot11TDLSProbeDelayTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "1000"},

{{8,Dot11TDLSDiscoveryRequestWindow}, GetNextIndexDot11StationConfigTable, Dot11TDLSDiscoveryRequestWindowGet, Dot11TDLSDiscoveryRequestWindowSet, Dot11TDLSDiscoveryRequestWindowTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TDLSACDeterminationInterval}, GetNextIndexDot11StationConfigTable, Dot11TDLSACDeterminationIntervalGet, Dot11TDLSACDeterminationIntervalSet, Dot11TDLSACDeterminationIntervalTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "1"},

{{8,Dot11WirelessManagementImplemented}, GetNextIndexDot11StationConfigTable, Dot11WirelessManagementImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11BssMaxIdlePeriod}, GetNextIndexDot11StationConfigTable, Dot11BssMaxIdlePeriodGet, Dot11BssMaxIdlePeriodSet, Dot11BssMaxIdlePeriodTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11BssMaxIdlePeriodOptions}, GetNextIndexDot11StationConfigTable, Dot11BssMaxIdlePeriodOptionsGet, Dot11BssMaxIdlePeriodOptionsSet, Dot11BssMaxIdlePeriodOptionsTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TIMBroadcastInterval}, GetNextIndexDot11StationConfigTable, Dot11TIMBroadcastIntervalGet, Dot11TIMBroadcastIntervalSet, Dot11TIMBroadcastIntervalTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "0"},

{{8,Dot11TIMBroadcastOffset}, GetNextIndexDot11StationConfigTable, Dot11TIMBroadcastOffsetGet, Dot11TIMBroadcastOffsetSet, Dot11TIMBroadcastOffsetTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "0"},

{{8,Dot11TIMBroadcastHighRateTIMRate}, GetNextIndexDot11StationConfigTable, Dot11TIMBroadcastHighRateTIMRateGet, Dot11TIMBroadcastHighRateTIMRateSet, Dot11TIMBroadcastHighRateTIMRateTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "0"},

{{8,Dot11TIMBroadcastLowRateTIMRate}, GetNextIndexDot11StationConfigTable, Dot11TIMBroadcastLowRateTIMRateGet, Dot11TIMBroadcastLowRateTIMRateSet, Dot11TIMBroadcastLowRateTIMRateTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "0"},

{{8,Dot11StatsMinTriggerTimeout}, GetNextIndexDot11StationConfigTable, Dot11StatsMinTriggerTimeoutGet, Dot11StatsMinTriggerTimeoutSet, Dot11StatsMinTriggerTimeoutTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "10"},

{{8,Dot11RMCivicMeasurementActivated}, GetNextIndexDot11StationConfigTable, Dot11RMCivicMeasurementActivatedGet, Dot11RMCivicMeasurementActivatedSet, Dot11RMCivicMeasurementActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RMIdentifierMeasurementActivated}, GetNextIndexDot11StationConfigTable, Dot11RMIdentifierMeasurementActivatedGet, Dot11RMIdentifierMeasurementActivatedSet, Dot11RMIdentifierMeasurementActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TimeAdvertisementDTIMInterval}, GetNextIndexDot11StationConfigTable, Dot11TimeAdvertisementDTIMIntervalGet, Dot11TimeAdvertisementDTIMIntervalSet, Dot11TimeAdvertisementDTIMIntervalTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "1"},

{{8,Dot11InterworkingServiceImplemented}, GetNextIndexDot11StationConfigTable, Dot11InterworkingServiceImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11InterworkingServiceActivated}, GetNextIndexDot11StationConfigTable, Dot11InterworkingServiceActivatedGet, Dot11InterworkingServiceActivatedSet, Dot11InterworkingServiceActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11QosMapImplemented}, GetNextIndexDot11StationConfigTable, Dot11QosMapImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11QosMapActivated}, GetNextIndexDot11StationConfigTable, Dot11QosMapActivatedGet, Dot11QosMapActivatedSet, Dot11QosMapActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11EBRImplemented}, GetNextIndexDot11StationConfigTable, Dot11EBRImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11EBRActivated}, GetNextIndexDot11StationConfigTable, Dot11EBRActivatedGet, Dot11EBRActivatedSet, Dot11EBRActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11ESNetwork}, GetNextIndexDot11StationConfigTable, Dot11ESNetworkGet, Dot11ESNetworkSet, Dot11ESNetworkTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11SSPNInterfaceImplemented}, GetNextIndexDot11StationConfigTable, Dot11SSPNInterfaceImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11SSPNInterfaceActivated}, GetNextIndexDot11StationConfigTable, Dot11SSPNInterfaceActivatedGet, Dot11SSPNInterfaceActivatedSet, Dot11SSPNInterfaceActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11HESSID}, GetNextIndexDot11StationConfigTable, Dot11HESSIDGet, Dot11HESSIDSet, Dot11HESSIDTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11EASImplemented}, GetNextIndexDot11StationConfigTable, Dot11EASImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11EASActivated}, GetNextIndexDot11StationConfigTable, Dot11EASActivatedGet, Dot11EASActivatedSet, Dot11EASActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MSGCFImplemented}, GetNextIndexDot11StationConfigTable, Dot11MSGCFImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MSGCFActivated}, GetNextIndexDot11StationConfigTable, Dot11MSGCFActivatedGet, Dot11MSGCFActivatedSet, Dot11MSGCFActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TimeAdvertisementTimeError}, GetNextIndexDot11StationConfigTable, Dot11TimeAdvertisementTimeErrorGet, Dot11TimeAdvertisementTimeErrorSet, Dot11TimeAdvertisementTimeErrorTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TimeAdvertisementTimeValue}, GetNextIndexDot11StationConfigTable, Dot11TimeAdvertisementTimeValueGet, Dot11TimeAdvertisementTimeValueSet, Dot11TimeAdvertisementTimeValueTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RM3rdPartyMeasurementActivated}, GetNextIndexDot11StationConfigTable, Dot11RM3rdPartyMeasurementActivatedGet, Dot11RM3rdPartyMeasurementActivatedSet, Dot11RM3rdPartyMeasurementActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RejectUnadmittedTraffic}, GetNextIndexDot11StationConfigTable, Dot11RejectUnadmittedTrafficGet, Dot11RejectUnadmittedTrafficSet, Dot11RejectUnadmittedTrafficTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11BSSBroadcastNullCount}, GetNextIndexDot11StationConfigTable, Dot11BSSBroadcastNullCountGet, Dot11BSSBroadcastNullCountSet, Dot11BSSBroadcastNullCountTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, "3"},

{{8,Dot11MeshActivated}, GetNextIndexDot11StationConfigTable, Dot11MeshActivatedGet, Dot11MeshActivatedSet, Dot11MeshActivatedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},
};
#ifdef PMF_WANTED
tMibData Dot11StationConfigTableEntry = { 136, Dot11StationConfigTableMibEntry };
#else
tMibData Dot11StationConfigTableEntry = { 132, Dot11StationConfigTableMibEntry };
#endif

tMbDbEntry Dot11AuthenticationAlgorithmsTableMibEntry[]= {

{{8,Dot11AuthenticationAlgorithmsIndex}, GetNextIndexDot11AuthenticationAlgorithmsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11AuthenticationAlgorithmsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11AuthenticationAlgorithm}, GetNextIndexDot11AuthenticationAlgorithmsTable, Dot11AuthenticationAlgorithmGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11AuthenticationAlgorithmsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11AuthenticationAlgorithmsActivated}, GetNextIndexDot11AuthenticationAlgorithmsTable, Dot11AuthenticationAlgorithmsActivatedGet, Dot11AuthenticationAlgorithmsActivatedSet, Dot11AuthenticationAlgorithmsActivatedTest, Dot11AuthenticationAlgorithmsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11AuthenticationAlgorithmsTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11AuthenticationAlgorithmsTableEntry = { 3, Dot11AuthenticationAlgorithmsTableMibEntry };

tMbDbEntry Dot11WEPDefaultKeysTableMibEntry[]= {

{{8,Dot11WEPDefaultKeyIndex}, GetNextIndexDot11WEPDefaultKeysTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11WEPDefaultKeysTableINDEX, 2, 0, 0, NULL},

{{8,Dot11WEPDefaultKeyValue}, GetNextIndexDot11WEPDefaultKeysTable, Dot11WEPDefaultKeyValueGet, Dot11WEPDefaultKeyValueSet, Dot11WEPDefaultKeyValueTest, Dot11WEPDefaultKeysTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11WEPDefaultKeysTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11WEPDefaultKeysTableEntry = { 2, Dot11WEPDefaultKeysTableMibEntry };

tMbDbEntry Dot11WEPKeyMappingsTableMibEntry[]= {

{{8,Dot11WEPKeyMappingIndex}, GetNextIndexDot11WEPKeyMappingsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11WEPKeyMappingsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11WEPKeyMappingAddress}, GetNextIndexDot11WEPKeyMappingsTable, Dot11WEPKeyMappingAddressGet, Dot11WEPKeyMappingAddressSet, Dot11WEPKeyMappingAddressTest, Dot11WEPKeyMappingsTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot11WEPKeyMappingsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11WEPKeyMappingWEPOn}, GetNextIndexDot11WEPKeyMappingsTable, Dot11WEPKeyMappingWEPOnGet, Dot11WEPKeyMappingWEPOnSet, Dot11WEPKeyMappingWEPOnTest, Dot11WEPKeyMappingsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WEPKeyMappingsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11WEPKeyMappingValue}, GetNextIndexDot11WEPKeyMappingsTable, Dot11WEPKeyMappingValueGet, Dot11WEPKeyMappingValueSet, Dot11WEPKeyMappingValueTest, Dot11WEPKeyMappingsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11WEPKeyMappingsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11WEPKeyMappingStatus}, GetNextIndexDot11WEPKeyMappingsTable, Dot11WEPKeyMappingStatusGet, Dot11WEPKeyMappingStatusSet, Dot11WEPKeyMappingStatusTest, Dot11WEPKeyMappingsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WEPKeyMappingsTableINDEX, 2, 0, 1, "1"},
};
tMibData Dot11WEPKeyMappingsTableEntry = { 5, Dot11WEPKeyMappingsTableMibEntry };

tMbDbEntry Dot11PrivacyTableMibEntry[]= {

{{8,Dot11PrivacyInvoked}, GetNextIndexDot11PrivacyTable, Dot11PrivacyInvokedGet, Dot11PrivacyInvokedSet, Dot11PrivacyInvokedTest, Dot11PrivacyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PrivacyTableINDEX, 1, 0, 0, "2"},

{{8,Dot11WEPDefaultKeyID}, GetNextIndexDot11PrivacyTable, Dot11WEPDefaultKeyIDGet, Dot11WEPDefaultKeyIDSet, Dot11WEPDefaultKeyIDTest, Dot11PrivacyTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11PrivacyTableINDEX, 1, 0, 0, "0"},

{{8,Dot11WEPKeyMappingLengthImplemented}, GetNextIndexDot11PrivacyTable, Dot11WEPKeyMappingLengthImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PrivacyTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ExcludeUnencrypted}, GetNextIndexDot11PrivacyTable, Dot11ExcludeUnencryptedGet, Dot11ExcludeUnencryptedSet, Dot11ExcludeUnencryptedTest, Dot11PrivacyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PrivacyTableINDEX, 1, 0, 0, "2"},

{{8,Dot11WEPICVErrorCount}, GetNextIndexDot11PrivacyTable, Dot11WEPICVErrorCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11PrivacyTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WEPExcludedCount}, GetNextIndexDot11PrivacyTable, Dot11WEPExcludedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11PrivacyTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAActivated}, GetNextIndexDot11PrivacyTable, Dot11RSNAActivatedGet, Dot11RSNAActivatedSet, Dot11RSNAActivatedTest, Dot11PrivacyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PrivacyTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAPreauthenticationActivated}, GetNextIndexDot11PrivacyTable, Dot11RSNAPreauthenticationActivatedGet, Dot11RSNAPreauthenticationActivatedSet, Dot11RSNAPreauthenticationActivatedTest, Dot11PrivacyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PrivacyTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11PrivacyTableEntry = { 8, Dot11PrivacyTableMibEntry };

tMbDbEntry Dot11MultiDomainCapabilityTableMibEntry[]= {

{{8,Dot11MultiDomainCapabilityIndex}, GetNextIndexDot11MultiDomainCapabilityTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11MultiDomainCapabilityTableINDEX, 2, 0, 0, NULL},

{{8,Dot11FirstChannelNumber}, GetNextIndexDot11MultiDomainCapabilityTable, Dot11FirstChannelNumberGet, Dot11FirstChannelNumberSet, Dot11FirstChannelNumberTest, Dot11MultiDomainCapabilityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MultiDomainCapabilityTableINDEX, 2, 0, 0, "0"},

{{8,Dot11NumberofChannels}, GetNextIndexDot11MultiDomainCapabilityTable, Dot11NumberofChannelsGet, Dot11NumberofChannelsSet, Dot11NumberofChannelsTest, Dot11MultiDomainCapabilityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MultiDomainCapabilityTableINDEX, 2, 0, 0, "0"},

{{8,Dot11MaximumTransmitPowerLevel}, GetNextIndexDot11MultiDomainCapabilityTable, Dot11MaximumTransmitPowerLevelGet, Dot11MaximumTransmitPowerLevelSet, Dot11MaximumTransmitPowerLevelTest, Dot11MultiDomainCapabilityTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11MultiDomainCapabilityTableINDEX, 2, 0, 0, "0"},
};
tMibData Dot11MultiDomainCapabilityTableEntry = { 4, Dot11MultiDomainCapabilityTableMibEntry };

tMbDbEntry Dot11SpectrumManagementTableMibEntry[]= {

{{8,Dot11SpectrumManagementIndex}, GetNextIndexDot11SpectrumManagementTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11SpectrumManagementTableINDEX, 2, 0, 0, NULL},

{{8,Dot11MitigationRequirement}, GetNextIndexDot11SpectrumManagementTable, Dot11MitigationRequirementGet, Dot11MitigationRequirementSet, Dot11MitigationRequirementTest, Dot11SpectrumManagementTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11SpectrumManagementTableINDEX, 2, 0, 0, "3"},

{{8,Dot11ChannelSwitchTime}, GetNextIndexDot11SpectrumManagementTable, Dot11ChannelSwitchTimeGet, Dot11ChannelSwitchTimeSet, Dot11ChannelSwitchTimeTest, Dot11SpectrumManagementTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11SpectrumManagementTableINDEX, 2, 0, 0, "2"},

{{8,Dot11PowerCapabilityMaxImplemented}, GetNextIndexDot11SpectrumManagementTable, Dot11PowerCapabilityMaxImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11SpectrumManagementTableINDEX, 2, 0, 0, "0"},

{{8,Dot11PowerCapabilityMinImplemented}, GetNextIndexDot11SpectrumManagementTable, Dot11PowerCapabilityMinImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11SpectrumManagementTableINDEX, 2, 0, 0, "-100"},
};
tMibData Dot11SpectrumManagementTableEntry = { 5, Dot11SpectrumManagementTableMibEntry };

tMbDbEntry Dot11RSNAConfigTableMibEntry[]= {

{{8,Dot11RSNAConfigVersion}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigPairwiseKeysImplemented}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigPairwiseKeysImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigGroupCipher}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigGroupCipherGet, Dot11RSNAConfigGroupCipherSet, Dot11RSNAConfigGroupCipherTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigGroupRekeyMethod}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigGroupRekeyMethodGet, Dot11RSNAConfigGroupRekeyMethodSet, Dot11RSNAConfigGroupRekeyMethodTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RSNAConfigGroupRekeyTime}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigGroupRekeyTimeGet, Dot11RSNAConfigGroupRekeyTimeSet, Dot11RSNAConfigGroupRekeyTimeTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, "86400"},

{{8,Dot11RSNAConfigGroupRekeyPackets}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigGroupRekeyPacketsGet, Dot11RSNAConfigGroupRekeyPacketsSet, Dot11RSNAConfigGroupRekeyPacketsTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigGroupRekeyStrict}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigGroupRekeyStrictGet, Dot11RSNAConfigGroupRekeyStrictSet, Dot11RSNAConfigGroupRekeyStrictTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigPSKValue}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigPSKValueGet, Dot11RSNAConfigPSKValueSet, Dot11RSNAConfigPSKValueTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigPSKPassPhrase}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigPSKPassPhraseGet, Dot11RSNAConfigPSKPassPhraseSet, Dot11RSNAConfigPSKPassPhraseTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigGroupUpdateCount}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigGroupUpdateCountGet, Dot11RSNAConfigGroupUpdateCountSet, Dot11RSNAConfigGroupUpdateCountTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, "3"},

{{8,Dot11RSNAConfigPairwiseUpdateCount}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigPairwiseUpdateCountGet, Dot11RSNAConfigPairwiseUpdateCountSet, Dot11RSNAConfigPairwiseUpdateCountTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, "3"},

{{8,Dot11RSNAConfigGroupCipherSize}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigGroupCipherSizeGet, Dot11RSNAConfigGroupCipherSizeSet, Dot11RSNAConfigGroupCipherSizeTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigPMKLifetime}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigPMKLifetimeGet, Dot11RSNAConfigPMKLifetimeSet, Dot11RSNAConfigPMKLifetimeTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, "43200"},

{{8,Dot11RSNAConfigPMKReauthThreshold}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigPMKReauthThresholdGet, Dot11RSNAConfigPMKReauthThresholdSet, Dot11RSNAConfigPMKReauthThresholdTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, "70"},

{{8,Dot11RSNAConfigNumberOfPTKSAReplayCountersImplemented}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigNumberOfPTKSAReplayCountersImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, "3"},

{{8,Dot11RSNAConfigSATimeout}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigSATimeoutGet, Dot11RSNAConfigSATimeoutSet, Dot11RSNAConfigSATimeoutTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, "60"},

{{8,Dot11RSNAAuthenticationSuiteSelected}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAAuthenticationSuiteSelectedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAPairwiseCipherSelected}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAPairwiseCipherSelectedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAGroupCipherSelected}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAGroupCipherSelectedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAPMKIDUsed}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAPMKIDUsedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAAuthenticationSuiteRequested}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAAuthenticationSuiteRequestedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAPairwiseCipherRequested}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAPairwiseCipherRequestedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAGroupCipherRequested}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAGroupCipherRequestedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNATKIPCounterMeasuresInvoked}, GetNextIndexDot11RSNAConfigTable, Dot11RSNATKIPCounterMeasuresInvokedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNA4WayHandshakeFailures}, GetNextIndexDot11RSNAConfigTable, Dot11RSNA4WayHandshakeFailuresGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigNumberOfGTKSAReplayCountersImplemented}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigNumberOfGTKSAReplayCountersImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, "3"},

{{8,Dot11RSNAConfigSTKKeysImplemented}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigSTKKeysImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigSTKCipher}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigSTKCipherGet, Dot11RSNAConfigSTKCipherSet, Dot11RSNAConfigSTKCipherTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigSTKRekeyTime}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigSTKRekeyTimeGet, Dot11RSNAConfigSTKRekeyTimeSet, Dot11RSNAConfigSTKRekeyTimeTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, "86400"},

{{8,Dot11RSNAConfigSMKUpdateCount}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigSMKUpdateCountGet, Dot11RSNAConfigSMKUpdateCountSet, Dot11RSNAConfigSMKUpdateCountTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, "3"},

{{8,Dot11RSNAConfigSTKCipherSize}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigSTKCipherSizeGet, Dot11RSNAConfigSTKCipherSizeSet, Dot11RSNAConfigSTKCipherSizeTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigSMKLifetime}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigSMKLifetimeGet, Dot11RSNAConfigSMKLifetimeSet, Dot11RSNAConfigSMKLifetimeTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, "43200"},

{{8,Dot11RSNAConfigSMKReauthThreshold}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigSMKReauthThresholdGet, Dot11RSNAConfigSMKReauthThresholdSet, Dot11RSNAConfigSMKReauthThresholdTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigNumberOfSTKSAReplayCountersImplemented}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigNumberOfSTKSAReplayCountersImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, "3"},

{{8,Dot11RSNAPairwiseSTKSelected}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAPairwiseSTKSelectedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNASMKHandshakeFailures}, GetNextIndexDot11RSNAConfigTable, Dot11RSNASMKHandshakeFailuresGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNASAERetransPeriod}, GetNextIndexDot11RSNAConfigTable, Dot11RSNASAERetransPeriodGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, "40"},

{{8,Dot11RSNASAEAntiCloggingThreshold}, GetNextIndexDot11RSNAConfigTable, Dot11RSNASAEAntiCloggingThresholdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, "5"},

{{8,Dot11RSNASAESync}, GetNextIndexDot11RSNAConfigTable, Dot11RSNASAESyncGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, "5"},
};
tMibData Dot11RSNAConfigTableEntry = { 39, Dot11RSNAConfigTableMibEntry };

tMbDbEntry Dot11RSNAConfigPairwiseCiphersTableMibEntry[]= {

{{8,Dot11RSNAConfigPairwiseCipherIndex}, GetNextIndexDot11RSNAConfigPairwiseCiphersTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11RSNAConfigPairwiseCiphersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAConfigPairwiseCipherImplemented}, GetNextIndexDot11RSNAConfigPairwiseCiphersTable, Dot11RSNAConfigPairwiseCipherImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAConfigPairwiseCiphersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAConfigPairwiseCipherActivated}, GetNextIndexDot11RSNAConfigPairwiseCiphersTable, Dot11RSNAConfigPairwiseCipherActivatedGet, Dot11RSNAConfigPairwiseCipherActivatedSet, Dot11RSNAConfigPairwiseCipherActivatedTest, Dot11RSNAConfigPairwiseCiphersTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RSNAConfigPairwiseCiphersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAConfigPairwiseCipherSizeImplemented}, GetNextIndexDot11RSNAConfigPairwiseCiphersTable, Dot11RSNAConfigPairwiseCipherSizeImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11RSNAConfigPairwiseCiphersTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11RSNAConfigPairwiseCiphersTableEntry = { 4, Dot11RSNAConfigPairwiseCiphersTableMibEntry };

tMbDbEntry Dot11RSNAConfigAuthenticationSuitesTableMibEntry[]= {

{{8,Dot11RSNAConfigAuthenticationSuiteIndex}, GetNextIndexDot11RSNAConfigAuthenticationSuitesTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11RSNAConfigAuthenticationSuitesTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigAuthenticationSuiteImplemented}, GetNextIndexDot11RSNAConfigAuthenticationSuitesTable, Dot11RSNAConfigAuthenticationSuiteImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAConfigAuthenticationSuitesTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigAuthenticationSuiteActivated}, GetNextIndexDot11RSNAConfigAuthenticationSuitesTable, Dot11RSNAConfigAuthenticationSuiteActivatedGet, Dot11RSNAConfigAuthenticationSuiteActivatedSet, Dot11RSNAConfigAuthenticationSuiteActivatedTest, Dot11RSNAConfigAuthenticationSuitesTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RSNAConfigAuthenticationSuitesTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11RSNAConfigAuthenticationSuitesTableEntry = { 3, Dot11RSNAConfigAuthenticationSuitesTableMibEntry };

tMbDbEntry Dot11RSNAStatsTableMibEntry[]= {

{{8,Dot11RSNAStatsIndex}, GetNextIndexDot11RSNAStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAStatsSTAAddress}, GetNextIndexDot11RSNAStatsTable, Dot11RSNAStatsSTAAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAStatsVersion}, GetNextIndexDot11RSNAStatsTable, Dot11RSNAStatsVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAStatsSelectedPairwiseCipher}, GetNextIndexDot11RSNAStatsTable, Dot11RSNAStatsSelectedPairwiseCipherGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAStatsTKIPICVErrors}, GetNextIndexDot11RSNAStatsTable, Dot11RSNAStatsTKIPICVErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAStatsTKIPLocalMICFailures}, GetNextIndexDot11RSNAStatsTable, Dot11RSNAStatsTKIPLocalMICFailuresGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAStatsTKIPRemoteMICFailures}, GetNextIndexDot11RSNAStatsTable, Dot11RSNAStatsTKIPRemoteMICFailuresGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAStatsCCMPReplays}, GetNextIndexDot11RSNAStatsTable, Dot11RSNAStatsCCMPReplaysGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAStatsCCMPDecryptErrors}, GetNextIndexDot11RSNAStatsTable, Dot11RSNAStatsCCMPDecryptErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAStatsTKIPReplays}, GetNextIndexDot11RSNAStatsTable, Dot11RSNAStatsTKIPReplaysGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAStatsCMACICVErrors}, GetNextIndexDot11RSNAStatsTable, Dot11RSNAStatsCMACICVErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAStatsCMACReplays}, GetNextIndexDot11RSNAStatsTable, Dot11RSNAStatsCMACReplaysGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAStatsRobustMgmtCCMPReplays}, GetNextIndexDot11RSNAStatsTable, Dot11RSNAStatsRobustMgmtCCMPReplaysGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNABIPMICErrors}, GetNextIndexDot11RSNAStatsTable, Dot11RSNABIPMICErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11RSNAStatsTableEntry = { 14, Dot11RSNAStatsTableMibEntry };

tMbDbEntry Dot11OperatingClassesTableMibEntry[]= {

{{8,Dot11OperatingClassesIndex}, GetNextIndexDot11OperatingClassesTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11OperatingClassesTableINDEX, 2, 0, 0, NULL},

{{8,Dot11OperatingClass}, GetNextIndexDot11OperatingClassesTable, Dot11OperatingClassGet, Dot11OperatingClassSet, Dot11OperatingClassTest, Dot11OperatingClassesTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperatingClassesTableINDEX, 2, 0, 0, "0"},

{{8,Dot11CoverageClass}, GetNextIndexDot11OperatingClassesTable, Dot11CoverageClassGet, Dot11CoverageClassSet, Dot11CoverageClassTest, Dot11OperatingClassesTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperatingClassesTableINDEX, 2, 0, 0, "0"},
};
tMibData Dot11OperatingClassesTableEntry = { 3, Dot11OperatingClassesTableMibEntry };

tMbDbEntry Dot11RMRequestNextIndexMibEntry[]= {

{{8,Dot11RMRequestNextIndex}, NULL, Dot11RMRequestNextIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData Dot11RMRequestNextIndexEntry = { 1, Dot11RMRequestNextIndexMibEntry };

tMbDbEntry Dot11RMRequestTableMibEntry[]= {

{{10,Dot11RMRqstIndex}, GetNextIndexDot11RMRequestTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11RMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMRqstRowStatus}, GetNextIndexDot11RMRequestTable, Dot11RMRqstRowStatusGet, Dot11RMRqstRowStatusSet, Dot11RMRqstRowStatusTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 1, NULL},

{{10,Dot11RMRqstToken}, GetNextIndexDot11RMRequestTable, Dot11RMRqstTokenGet, Dot11RMRqstTokenSet, Dot11RMRqstTokenTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, ""},

{{10,Dot11RMRqstRepetitions}, GetNextIndexDot11RMRequestTable, Dot11RMRqstRepetitionsGet, Dot11RMRqstRepetitionsSet, Dot11RMRqstRepetitionsTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMRqstIfIndex}, GetNextIndexDot11RMRequestTable, Dot11RMRqstIfIndexGet, Dot11RMRqstIfIndexSet, Dot11RMRqstIfIndexTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMRqstType}, GetNextIndexDot11RMRequestTable, Dot11RMRqstTypeGet, Dot11RMRqstTypeSet, Dot11RMRqstTypeTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMRqstTargetAdd}, GetNextIndexDot11RMRequestTable, Dot11RMRqstTargetAddGet, Dot11RMRqstTargetAddSet, Dot11RMRqstTargetAddTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMRqstTimeStamp}, GetNextIndexDot11RMRequestTable, Dot11RMRqstTimeStampGet, Dot11RMRqstTimeStampSet, Dot11RMRqstTimeStampTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMRqstChanNumber}, GetNextIndexDot11RMRequestTable, Dot11RMRqstChanNumberGet, Dot11RMRqstChanNumberSet, Dot11RMRqstChanNumberTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMRqstOperatingClass}, GetNextIndexDot11RMRequestTable, Dot11RMRqstOperatingClassGet, Dot11RMRqstOperatingClassSet, Dot11RMRqstOperatingClassTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMRqstRndInterval}, GetNextIndexDot11RMRequestTable, Dot11RMRqstRndIntervalGet, Dot11RMRqstRndIntervalSet, Dot11RMRqstRndIntervalTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstDuration}, GetNextIndexDot11RMRequestTable, Dot11RMRqstDurationGet, Dot11RMRqstDurationSet, Dot11RMRqstDurationTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstParallel}, GetNextIndexDot11RMRequestTable, Dot11RMRqstParallelGet, Dot11RMRqstParallelSet, Dot11RMRqstParallelTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "2"},

{{10,Dot11RMRqstEnable}, GetNextIndexDot11RMRequestTable, Dot11RMRqstEnableGet, Dot11RMRqstEnableSet, Dot11RMRqstEnableTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "2"},

{{10,Dot11RMRqstRequest}, GetNextIndexDot11RMRequestTable, Dot11RMRqstRequestGet, Dot11RMRqstRequestSet, Dot11RMRqstRequestTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "2"},

{{10,Dot11RMRqstReport}, GetNextIndexDot11RMRequestTable, Dot11RMRqstReportGet, Dot11RMRqstReportSet, Dot11RMRqstReportTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "2"},

{{10,Dot11RMRqstDurationMandatory}, GetNextIndexDot11RMRequestTable, Dot11RMRqstDurationMandatoryGet, Dot11RMRqstDurationMandatorySet, Dot11RMRqstDurationMandatoryTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "2"},

{{10,Dot11RMRqstBeaconRqstMode}, GetNextIndexDot11RMRequestTable, Dot11RMRqstBeaconRqstModeGet, Dot11RMRqstBeaconRqstModeSet, Dot11RMRqstBeaconRqstModeTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstBeaconRqstDetail}, GetNextIndexDot11RMRequestTable, Dot11RMRqstBeaconRqstDetailGet, Dot11RMRqstBeaconRqstDetailSet, Dot11RMRqstBeaconRqstDetailTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "2"},

{{10,Dot11RMRqstFrameRqstType}, GetNextIndexDot11RMRequestTable, Dot11RMRqstFrameRqstTypeGet, Dot11RMRqstFrameRqstTypeSet, Dot11RMRqstFrameRqstTypeTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "1"},

{{10,Dot11RMRqstBssid}, GetNextIndexDot11RMRequestTable, Dot11RMRqstBssidGet, Dot11RMRqstBssidSet, Dot11RMRqstBssidTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "FFFFFFFFFFFF"},

{{10,Dot11RMRqstSSID}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSSIDGet, Dot11RMRqstSSIDSet, Dot11RMRqstSSIDTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstBeaconReportingCondition}, GetNextIndexDot11RMRequestTable, Dot11RMRqstBeaconReportingConditionGet, Dot11RMRqstBeaconReportingConditionSet, Dot11RMRqstBeaconReportingConditionTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstBeaconThresholdOffset}, GetNextIndexDot11RMRequestTable, Dot11RMRqstBeaconThresholdOffsetGet, Dot11RMRqstBeaconThresholdOffsetSet, Dot11RMRqstBeaconThresholdOffsetTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatRqstGroupID}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatRqstGroupIDGet, Dot11RMRqstSTAStatRqstGroupIDSet, Dot11RMRqstSTAStatRqstGroupIDTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstLCIRqstSubject}, GetNextIndexDot11RMRequestTable, Dot11RMRqstLCIRqstSubjectGet, Dot11RMRqstLCIRqstSubjectSet, Dot11RMRqstLCIRqstSubjectTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstLCILatitudeResolution}, GetNextIndexDot11RMRequestTable, Dot11RMRqstLCILatitudeResolutionGet, Dot11RMRqstLCILatitudeResolutionSet, Dot11RMRqstLCILatitudeResolutionTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMRqstLCILongitudeResolution}, GetNextIndexDot11RMRequestTable, Dot11RMRqstLCILongitudeResolutionGet, Dot11RMRqstLCILongitudeResolutionSet, Dot11RMRqstLCILongitudeResolutionTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMRqstLCIAltitudeResolution}, GetNextIndexDot11RMRequestTable, Dot11RMRqstLCIAltitudeResolutionGet, Dot11RMRqstLCIAltitudeResolutionSet, Dot11RMRqstLCIAltitudeResolutionTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMRqstLCIAzimuthType}, GetNextIndexDot11RMRequestTable, Dot11RMRqstLCIAzimuthTypeGet, Dot11RMRqstLCIAzimuthTypeSet, Dot11RMRqstLCIAzimuthTypeTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstLCIAzimuthResolution}, GetNextIndexDot11RMRequestTable, Dot11RMRqstLCIAzimuthResolutionGet, Dot11RMRqstLCIAzimuthResolutionSet, Dot11RMRqstLCIAzimuthResolutionTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMRqstPauseTime}, GetNextIndexDot11RMRequestTable, Dot11RMRqstPauseTimeGet, Dot11RMRqstPauseTimeSet, Dot11RMRqstPauseTimeTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstTransmitStreamPeerQSTAAddress}, GetNextIndexDot11RMRequestTable, Dot11RMRqstTransmitStreamPeerQSTAAddressGet, Dot11RMRqstTransmitStreamPeerQSTAAddressSet, Dot11RMRqstTransmitStreamPeerQSTAAddressTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMRqstTransmitStreamTrafficIdentifier}, GetNextIndexDot11RMRequestTable, Dot11RMRqstTransmitStreamTrafficIdentifierGet, Dot11RMRqstTransmitStreamTrafficIdentifierSet, Dot11RMRqstTransmitStreamTrafficIdentifierTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMRqstTransmitStreamBin0Range}, GetNextIndexDot11RMRequestTable, Dot11RMRqstTransmitStreamBin0RangeGet, Dot11RMRqstTransmitStreamBin0RangeSet, Dot11RMRqstTransmitStreamBin0RangeTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMRqstTrigdQoSAverageCondition}, GetNextIndexDot11RMRequestTable, Dot11RMRqstTrigdQoSAverageConditionGet, Dot11RMRqstTrigdQoSAverageConditionSet, Dot11RMRqstTrigdQoSAverageConditionTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "2"},

{{10,Dot11RMRqstTrigdQoSConsecutiveCondition}, GetNextIndexDot11RMRequestTable, Dot11RMRqstTrigdQoSConsecutiveConditionGet, Dot11RMRqstTrigdQoSConsecutiveConditionSet, Dot11RMRqstTrigdQoSConsecutiveConditionTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "2"},

{{10,Dot11RMRqstTrigdQoSDelayCondition}, GetNextIndexDot11RMRequestTable, Dot11RMRqstTrigdQoSDelayConditionGet, Dot11RMRqstTrigdQoSDelayConditionSet, Dot11RMRqstTrigdQoSDelayConditionTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "2"},

{{10,Dot11RMRqstTrigdQoSAverageThreshold}, GetNextIndexDot11RMRequestTable, Dot11RMRqstTrigdQoSAverageThresholdGet, Dot11RMRqstTrigdQoSAverageThresholdSet, Dot11RMRqstTrigdQoSAverageThresholdTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "10"},

{{10,Dot11RMRqstTrigdQoSConsecutiveThreshold}, GetNextIndexDot11RMRequestTable, Dot11RMRqstTrigdQoSConsecutiveThresholdGet, Dot11RMRqstTrigdQoSConsecutiveThresholdSet, Dot11RMRqstTrigdQoSConsecutiveThresholdTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "5"},

{{10,Dot11RMRqstTrigdQoSDelayThresholdRange}, GetNextIndexDot11RMRequestTable, Dot11RMRqstTrigdQoSDelayThresholdRangeGet, Dot11RMRqstTrigdQoSDelayThresholdRangeSet, Dot11RMRqstTrigdQoSDelayThresholdRangeTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "1"},

{{10,Dot11RMRqstTrigdQoSDelayThreshold}, GetNextIndexDot11RMRequestTable, Dot11RMRqstTrigdQoSDelayThresholdGet, Dot11RMRqstTrigdQoSDelayThresholdSet, Dot11RMRqstTrigdQoSDelayThresholdTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "20"},

{{10,Dot11RMRqstTrigdQoSMeasurementCount}, GetNextIndexDot11RMRequestTable, Dot11RMRqstTrigdQoSMeasurementCountGet, Dot11RMRqstTrigdQoSMeasurementCountSet, Dot11RMRqstTrigdQoSMeasurementCountTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "100"},

{{10,Dot11RMRqstTrigdQoSTimeout}, GetNextIndexDot11RMRequestTable, Dot11RMRqstTrigdQoSTimeoutGet, Dot11RMRqstTrigdQoSTimeoutSet, Dot11RMRqstTrigdQoSTimeoutTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "20"},

{{10,Dot11RMRqstChannelLoadReportingCondition}, GetNextIndexDot11RMRequestTable, Dot11RMRqstChannelLoadReportingConditionGet, Dot11RMRqstChannelLoadReportingConditionSet, Dot11RMRqstChannelLoadReportingConditionTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstChannelLoadReference}, GetNextIndexDot11RMRequestTable, Dot11RMRqstChannelLoadReferenceGet, Dot11RMRqstChannelLoadReferenceSet, Dot11RMRqstChannelLoadReferenceTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "5"},

{{10,Dot11RMRqstNoiseHistogramReportingCondition}, GetNextIndexDot11RMRequestTable, Dot11RMRqstNoiseHistogramReportingConditionGet, Dot11RMRqstNoiseHistogramReportingConditionSet, Dot11RMRqstNoiseHistogramReportingConditionTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstAnpiReference}, GetNextIndexDot11RMRequestTable, Dot11RMRqstAnpiReferenceGet, Dot11RMRqstAnpiReferenceSet, Dot11RMRqstAnpiReferenceTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "5"},

{{10,Dot11RMRqstAPChannelReport}, GetNextIndexDot11RMRequestTable, Dot11RMRqstAPChannelReportGet, Dot11RMRqstAPChannelReportSet, Dot11RMRqstAPChannelReportTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatPeerSTAAddress}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatPeerSTAAddressGet, Dot11RMRqstSTAStatPeerSTAAddressSet, Dot11RMRqstSTAStatPeerSTAAddressTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMRqstFrameTransmitterAddress}, GetNextIndexDot11RMRequestTable, Dot11RMRqstFrameTransmitterAddressGet, Dot11RMRqstFrameTransmitterAddressSet, Dot11RMRqstFrameTransmitterAddressTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMRqstVendorSpecific}, GetNextIndexDot11RMRequestTable, Dot11RMRqstVendorSpecificGet, Dot11RMRqstVendorSpecificSet, Dot11RMRqstVendorSpecificTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigMeasCount}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigMeasCountGet, Dot11RMRqstSTAStatTrigMeasCountSet, Dot11RMRqstSTAStatTrigMeasCountTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigTimeout}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigTimeoutGet, Dot11RMRqstSTAStatTrigTimeoutSet, Dot11RMRqstSTAStatTrigTimeoutTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigCondition}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigConditionGet, Dot11RMRqstSTAStatTrigConditionSet, Dot11RMRqstSTAStatTrigConditionTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMRqstSTAStatTrigSTAFailedCntThresh}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigSTAFailedCntThreshGet, Dot11RMRqstSTAStatTrigSTAFailedCntThreshSet, Dot11RMRqstSTAStatTrigSTAFailedCntThreshTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigSTAFCSErrCntThresh}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigSTAFCSErrCntThreshGet, Dot11RMRqstSTAStatTrigSTAFCSErrCntThreshSet, Dot11RMRqstSTAStatTrigSTAFCSErrCntThreshTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigSTAMultRetryCntThresh}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigSTAMultRetryCntThreshGet, Dot11RMRqstSTAStatTrigSTAMultRetryCntThreshSet, Dot11RMRqstSTAStatTrigSTAMultRetryCntThreshTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigSTAFrameDupeCntThresh}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigSTAFrameDupeCntThreshGet, Dot11RMRqstSTAStatTrigSTAFrameDupeCntThreshSet, Dot11RMRqstSTAStatTrigSTAFrameDupeCntThreshTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigSTARTSFailCntThresh}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigSTARTSFailCntThreshGet, Dot11RMRqstSTAStatTrigSTARTSFailCntThreshSet, Dot11RMRqstSTAStatTrigSTARTSFailCntThreshTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigSTAAckFailCntThresh}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigSTAAckFailCntThreshGet, Dot11RMRqstSTAStatTrigSTAAckFailCntThreshSet, Dot11RMRqstSTAStatTrigSTAAckFailCntThreshTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigSTARetryCntThresh}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigSTARetryCntThreshGet, Dot11RMRqstSTAStatTrigSTARetryCntThreshSet, Dot11RMRqstSTAStatTrigSTARetryCntThreshTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigQoSTrigCondition}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigQoSTrigConditionGet, Dot11RMRqstSTAStatTrigQoSTrigConditionSet, Dot11RMRqstSTAStatTrigQoSTrigConditionTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMRqstSTAStatTrigQoSFailedCntThresh}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigQoSFailedCntThreshGet, Dot11RMRqstSTAStatTrigQoSFailedCntThreshSet, Dot11RMRqstSTAStatTrigQoSFailedCntThreshTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigQoSRetryCntThresh}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigQoSRetryCntThreshGet, Dot11RMRqstSTAStatTrigQoSRetryCntThreshSet, Dot11RMRqstSTAStatTrigQoSRetryCntThreshTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigQoSMultRetryCntThresh}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigQoSMultRetryCntThreshGet, Dot11RMRqstSTAStatTrigQoSMultRetryCntThreshSet, Dot11RMRqstSTAStatTrigQoSMultRetryCntThreshTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigQoSFrameDupeCntThresh}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigQoSFrameDupeCntThreshGet, Dot11RMRqstSTAStatTrigQoSFrameDupeCntThreshSet, Dot11RMRqstSTAStatTrigQoSFrameDupeCntThreshTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigQoSRTSFailCntThresh}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigQoSRTSFailCntThreshGet, Dot11RMRqstSTAStatTrigQoSRTSFailCntThreshSet, Dot11RMRqstSTAStatTrigQoSRTSFailCntThreshTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigQoSAckFailCntThresh}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigQoSAckFailCntThreshGet, Dot11RMRqstSTAStatTrigQoSAckFailCntThreshSet, Dot11RMRqstSTAStatTrigQoSAckFailCntThreshTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigQoSDiscardCntThresh}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigQoSDiscardCntThreshGet, Dot11RMRqstSTAStatTrigQoSDiscardCntThreshSet, Dot11RMRqstSTAStatTrigQoSDiscardCntThreshTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigRsnaTrigCondition}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigRsnaTrigConditionGet, Dot11RMRqstSTAStatTrigRsnaTrigConditionSet, Dot11RMRqstSTAStatTrigRsnaTrigConditionTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMRqstSTAStatTrigRsnaCMACICVErrCntThresh}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigRsnaCMACICVErrCntThreshGet, Dot11RMRqstSTAStatTrigRsnaCMACICVErrCntThreshSet, Dot11RMRqstSTAStatTrigRsnaCMACICVErrCntThreshTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigRsnaCMACReplayCntThresh}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigRsnaCMACReplayCntThreshGet, Dot11RMRqstSTAStatTrigRsnaCMACReplayCntThreshSet, Dot11RMRqstSTAStatTrigRsnaCMACReplayCntThreshTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigRsnaRobustCCMPReplayCntThresh}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigRsnaRobustCCMPReplayCntThreshGet, Dot11RMRqstSTAStatTrigRsnaRobustCCMPReplayCntThreshSet, Dot11RMRqstSTAStatTrigRsnaRobustCCMPReplayCntThreshTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigRsnaTKIPICVErrCntThresh}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigRsnaTKIPICVErrCntThreshGet, Dot11RMRqstSTAStatTrigRsnaTKIPICVErrCntThreshSet, Dot11RMRqstSTAStatTrigRsnaTKIPICVErrCntThreshTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigRsnaTKIPReplayCntThresh}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigRsnaTKIPReplayCntThreshGet, Dot11RMRqstSTAStatTrigRsnaTKIPReplayCntThreshSet, Dot11RMRqstSTAStatTrigRsnaTKIPReplayCntThreshTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigRsnaCCMPDecryptErrCntThresh}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigRsnaCCMPDecryptErrCntThreshGet, Dot11RMRqstSTAStatTrigRsnaCCMPDecryptErrCntThreshSet, Dot11RMRqstSTAStatTrigRsnaCCMPDecryptErrCntThreshTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMRqstSTAStatTrigRsnaCCMPReplayCntThresh}, GetNextIndexDot11RMRequestTable, Dot11RMRqstSTAStatTrigRsnaCCMPReplayCntThreshGet, Dot11RMRqstSTAStatTrigRsnaCCMPReplayCntThreshSet, Dot11RMRqstSTAStatTrigRsnaCCMPReplayCntThreshTest, Dot11RMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMRequestTableINDEX, 1, 0, 0, "0"},
};
tMibData Dot11RMRequestTableEntry = { 78, Dot11RMRequestTableMibEntry };

tMbDbEntry Dot11ChannelLoadReportTableMibEntry[]= {

{{10,Dot11ChannelLoadRprtIndex}, GetNextIndexDot11ChannelLoadReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11ChannelLoadReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11ChannelLoadRprtRqstToken}, GetNextIndexDot11ChannelLoadReportTable, Dot11ChannelLoadRprtRqstTokenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11ChannelLoadReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11ChannelLoadRprtIfIndex}, GetNextIndexDot11ChannelLoadReportTable, Dot11ChannelLoadRprtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11ChannelLoadReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11ChannelLoadMeasuringSTAAddr}, GetNextIndexDot11ChannelLoadReportTable, Dot11ChannelLoadMeasuringSTAAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11ChannelLoadReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11ChannelLoadRprtChanNumber}, GetNextIndexDot11ChannelLoadReportTable, Dot11ChannelLoadRprtChanNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11ChannelLoadReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11ChannelLoadRprtOperatingClass}, GetNextIndexDot11ChannelLoadReportTable, Dot11ChannelLoadRprtOperatingClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11ChannelLoadReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11ChannelLoadRprtActualStartTime}, GetNextIndexDot11ChannelLoadReportTable, Dot11ChannelLoadRprtActualStartTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11ChannelLoadReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11ChannelLoadRprtMeasurementDuration}, GetNextIndexDot11ChannelLoadReportTable, Dot11ChannelLoadRprtMeasurementDurationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11ChannelLoadReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11ChannelLoadRprtChannelLoad}, GetNextIndexDot11ChannelLoadReportTable, Dot11ChannelLoadRprtChannelLoadGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11ChannelLoadReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11ChannelLoadRprtVendorSpecific}, GetNextIndexDot11ChannelLoadReportTable, Dot11ChannelLoadRprtVendorSpecificGet, Dot11ChannelLoadRprtVendorSpecificSet, Dot11ChannelLoadRprtVendorSpecificTest, Dot11ChannelLoadReportTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11ChannelLoadReportTableINDEX, 1, 0, 0, "0"},

{{10,Dot11ChannelLoadRprtMeasurementMode}, GetNextIndexDot11ChannelLoadReportTable, Dot11ChannelLoadRprtMeasurementModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11ChannelLoadReportTableINDEX, 1, 0, 0, "0"},
};
tMibData Dot11ChannelLoadReportTableEntry = { 11, Dot11ChannelLoadReportTableMibEntry };

tMbDbEntry Dot11NoiseHistogramReportTableMibEntry[]= {

{{10,Dot11NoiseHistogramRprtIndex}, GetNextIndexDot11NoiseHistogramReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11NoiseHistogramRprtRqstToken}, GetNextIndexDot11NoiseHistogramReportTable, Dot11NoiseHistogramRprtRqstTokenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11NoiseHistogramRprtIfIndex}, GetNextIndexDot11NoiseHistogramReportTable, Dot11NoiseHistogramRprtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11NoiseHistogramMeasuringSTAAddr}, GetNextIndexDot11NoiseHistogramReportTable, Dot11NoiseHistogramMeasuringSTAAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11NoiseHistogramRprtChanNumber}, GetNextIndexDot11NoiseHistogramReportTable, Dot11NoiseHistogramRprtChanNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11NoiseHistogramRprtOperatingClass}, GetNextIndexDot11NoiseHistogramReportTable, Dot11NoiseHistogramRprtOperatingClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11NoiseHistogramRprtActualStartTime}, GetNextIndexDot11NoiseHistogramReportTable, Dot11NoiseHistogramRprtActualStartTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11NoiseHistogramRprtMeasurementDuration}, GetNextIndexDot11NoiseHistogramReportTable, Dot11NoiseHistogramRprtMeasurementDurationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11NoiseHistogramRprtAntennaID}, GetNextIndexDot11NoiseHistogramReportTable, Dot11NoiseHistogramRprtAntennaIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11NoiseHistogramRprtANPI}, GetNextIndexDot11NoiseHistogramReportTable, Dot11NoiseHistogramRprtANPIGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11NoiseHistogramRprtIPIDensity0}, GetNextIndexDot11NoiseHistogramReportTable, Dot11NoiseHistogramRprtIPIDensity0Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11NoiseHistogramRprtIPIDensity1}, GetNextIndexDot11NoiseHistogramReportTable, Dot11NoiseHistogramRprtIPIDensity1Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11NoiseHistogramRprtIPIDensity2}, GetNextIndexDot11NoiseHistogramReportTable, Dot11NoiseHistogramRprtIPIDensity2Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11NoiseHistogramRprtIPIDensity3}, GetNextIndexDot11NoiseHistogramReportTable, Dot11NoiseHistogramRprtIPIDensity3Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11NoiseHistogramRprtIPIDensity4}, GetNextIndexDot11NoiseHistogramReportTable, Dot11NoiseHistogramRprtIPIDensity4Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11NoiseHistogramRprtIPIDensity5}, GetNextIndexDot11NoiseHistogramReportTable, Dot11NoiseHistogramRprtIPIDensity5Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11NoiseHistogramRprtIPIDensity6}, GetNextIndexDot11NoiseHistogramReportTable, Dot11NoiseHistogramRprtIPIDensity6Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11NoiseHistogramRprtIPIDensity7}, GetNextIndexDot11NoiseHistogramReportTable, Dot11NoiseHistogramRprtIPIDensity7Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11NoiseHistogramRprtIPIDensity8}, GetNextIndexDot11NoiseHistogramReportTable, Dot11NoiseHistogramRprtIPIDensity8Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11NoiseHistogramRprtIPIDensity9}, GetNextIndexDot11NoiseHistogramReportTable, Dot11NoiseHistogramRprtIPIDensity9Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11NoiseHistogramRprtIPIDensity10}, GetNextIndexDot11NoiseHistogramReportTable, Dot11NoiseHistogramRprtIPIDensity10Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11NoiseHistogramRprtVendorSpecific}, GetNextIndexDot11NoiseHistogramReportTable, Dot11NoiseHistogramRprtVendorSpecificGet, Dot11NoiseHistogramRprtVendorSpecificSet, Dot11NoiseHistogramRprtVendorSpecificTest, Dot11NoiseHistogramReportTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, "0"},

{{10,Dot11NoiseHistogramRprtMeasurementMode}, GetNextIndexDot11NoiseHistogramReportTable, Dot11NoiseHistogramRprtMeasurementModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11NoiseHistogramReportTableINDEX, 1, 0, 0, "0"},
};
tMibData Dot11NoiseHistogramReportTableEntry = { 23, Dot11NoiseHistogramReportTableMibEntry };

tMbDbEntry Dot11BeaconReportTableMibEntry[]= {

{{10,Dot11BeaconRprtIndex}, GetNextIndexDot11BeaconReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11BeaconReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11BeaconRprtRqstToken}, GetNextIndexDot11BeaconReportTable, Dot11BeaconRprtRqstTokenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11BeaconReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11BeaconRprtIfIndex}, GetNextIndexDot11BeaconReportTable, Dot11BeaconRprtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11BeaconReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11BeaconMeasuringSTAAddr}, GetNextIndexDot11BeaconReportTable, Dot11BeaconMeasuringSTAAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11BeaconReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11BeaconRprtChanNumber}, GetNextIndexDot11BeaconReportTable, Dot11BeaconRprtChanNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11BeaconReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11BeaconRprtOperatingClass}, GetNextIndexDot11BeaconReportTable, Dot11BeaconRprtOperatingClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11BeaconReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11BeaconRprtActualStartTime}, GetNextIndexDot11BeaconReportTable, Dot11BeaconRprtActualStartTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11BeaconReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11BeaconRprtMeasurementDuration}, GetNextIndexDot11BeaconReportTable, Dot11BeaconRprtMeasurementDurationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11BeaconReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11BeaconRprtPhyType}, GetNextIndexDot11BeaconReportTable, Dot11BeaconRprtPhyTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11BeaconReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11BeaconRprtReportedFrameType}, GetNextIndexDot11BeaconReportTable, Dot11BeaconRprtReportedFrameTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11BeaconReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11BeaconRprtRCPI}, GetNextIndexDot11BeaconReportTable, Dot11BeaconRprtRCPIGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11BeaconReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11BeaconRprtRSNI}, GetNextIndexDot11BeaconReportTable, Dot11BeaconRprtRSNIGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11BeaconReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11BeaconRprtBSSID}, GetNextIndexDot11BeaconReportTable, Dot11BeaconRprtBSSIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11BeaconReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11BeaconRprtAntennaID}, GetNextIndexDot11BeaconReportTable, Dot11BeaconRprtAntennaIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11BeaconReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11BeaconRprtParentTSF}, GetNextIndexDot11BeaconReportTable, Dot11BeaconRprtParentTSFGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11BeaconReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11BeaconRprtReportedFrameBody}, GetNextIndexDot11BeaconReportTable, Dot11BeaconRprtReportedFrameBodyGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11BeaconReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11BeaconRprtVendorSpecific}, GetNextIndexDot11BeaconReportTable, Dot11BeaconRprtVendorSpecificGet, Dot11BeaconRprtVendorSpecificSet, Dot11BeaconRprtVendorSpecificTest, Dot11BeaconReportTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11BeaconReportTableINDEX, 1, 0, 0, "0"},

{{10,Dot11BeaconRprtMeasurementMode}, GetNextIndexDot11BeaconReportTable, Dot11BeaconRprtMeasurementModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11BeaconReportTableINDEX, 1, 0, 0, "0"},
};
tMibData Dot11BeaconReportTableEntry = { 18, Dot11BeaconReportTableMibEntry };

tMbDbEntry Dot11FrameReportTableMibEntry[]= {

{{10,Dot11FrameRprtIndex}, GetNextIndexDot11FrameReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11FrameReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11FrameRprtIfIndex}, GetNextIndexDot11FrameReportTable, Dot11FrameRprtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11FrameReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11FrameRprtRqstToken}, GetNextIndexDot11FrameReportTable, Dot11FrameRprtRqstTokenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11FrameReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11FrameRprtChanNumber}, GetNextIndexDot11FrameReportTable, Dot11FrameRprtChanNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11FrameReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11FrameRprtOperatingClass}, GetNextIndexDot11FrameReportTable, Dot11FrameRprtOperatingClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11FrameReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11FrameRprtActualStartTime}, GetNextIndexDot11FrameReportTable, Dot11FrameRprtActualStartTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11FrameReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11FrameRprtMeasurementDuration}, GetNextIndexDot11FrameReportTable, Dot11FrameRprtMeasurementDurationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11FrameReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11FrameRprtTransmitSTAAddress}, GetNextIndexDot11FrameReportTable, Dot11FrameRprtTransmitSTAAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11FrameReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11FrameRprtBSSID}, GetNextIndexDot11FrameReportTable, Dot11FrameRprtBSSIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11FrameReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11FrameRprtPhyType}, GetNextIndexDot11FrameReportTable, Dot11FrameRprtPhyTypeGet, Dot11FrameRprtPhyTypeSet, Dot11FrameRprtPhyTypeTest, Dot11FrameReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11FrameReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11FrameRprtAvgRCPI}, GetNextIndexDot11FrameReportTable, Dot11FrameRprtAvgRCPIGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11FrameReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11FrameRprtLastRSNI}, GetNextIndexDot11FrameReportTable, Dot11FrameRprtLastRSNIGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11FrameReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11FrameRprtLastRCPI}, GetNextIndexDot11FrameReportTable, Dot11FrameRprtLastRCPIGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11FrameReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11FrameRprtAntennaID}, GetNextIndexDot11FrameReportTable, Dot11FrameRprtAntennaIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11FrameReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11FrameRprtNumberFrames}, GetNextIndexDot11FrameReportTable, Dot11FrameRprtNumberFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11FrameReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11FrameRprtVendorSpecific}, GetNextIndexDot11FrameReportTable, Dot11FrameRprtVendorSpecificGet, Dot11FrameRprtVendorSpecificSet, Dot11FrameRprtVendorSpecificTest, Dot11FrameReportTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11FrameReportTableINDEX, 1, 0, 0, "0"},

{{10,Dot11FrameRptMeasurementMode}, GetNextIndexDot11FrameReportTable, Dot11FrameRptMeasurementModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11FrameReportTableINDEX, 1, 0, 0, "0"},
};
tMibData Dot11FrameReportTableEntry = { 17, Dot11FrameReportTableMibEntry };

tMbDbEntry Dot11STAStatisticsReportTableMibEntry[]= {

{{10,Dot11STAStatisticsReportIndex}, GetNextIndexDot11STAStatisticsReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsReportToken}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsReportTokenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsIfIndex}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsSTAAddress}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsSTAAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsMeasurementDuration}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsMeasurementDurationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsGroupID}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsGroupIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, "0"},

{{10,Dot11STAStatisticsTransmittedFragmentCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsTransmittedFragmentCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsGroupTransmittedFrameCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsGroupTransmittedFrameCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsFailedCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsFailedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsRetryCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsRetryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsMultipleRetryCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsMultipleRetryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsFrameDuplicateCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsFrameDuplicateCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsRTSSuccessCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsRTSSuccessCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsRTSFailureCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsRTSFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsACKFailureCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsACKFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsQosTransmittedFragmentCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsQosTransmittedFragmentCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsQosFailedCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsQosFailedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsQosRetryCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsQosRetryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsQosMultipleRetryCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsQosMultipleRetryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsQosFrameDuplicateCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsQosFrameDuplicateCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsQosRTSSuccessCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsQosRTSSuccessCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsQosRTSFailureCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsQosRTSFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsQosACKFailureCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsQosACKFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsQosReceivedFragmentCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsQosReceivedFragmentCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsQosTransmittedFrameCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsQosTransmittedFrameCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsQosDiscardedFrameCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsQosDiscardedFrameCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsQosMPDUsReceivedCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsQosMPDUsReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsQosRetriesReceivedCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsQosRetriesReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsReceivedFragmentCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsReceivedFragmentCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsGroupReceivedFrameCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsGroupReceivedFrameCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsFCSErrorCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsFCSErrorCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsTransmittedFrameCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsTransmittedFrameCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsAPAverageAccessDelay}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsAPAverageAccessDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsAverageAccessDelayBestEffort}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsAverageAccessDelayBestEffortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsAverageAccessDelayBackground}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsAverageAccessDelayBackgroundGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsAverageAccessDelayVideo}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsAverageAccessDelayVideoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsAverageAccessDelayVoice}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsAverageAccessDelayVoiceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsStationCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsStationCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsChannelUtilization}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsChannelUtilizationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsVendorSpecific}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsVendorSpecificGet, Dot11STAStatisticsVendorSpecificSet, Dot11STAStatisticsVendorSpecificTest, Dot11STAStatisticsReportTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, "0"},

{{10,Dot11STAStatisticsRprtMeasurementMode}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsRprtMeasurementModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, "0"},

{{10,Dot11STAStatisticsRSNAStatsCMACICVErrors}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsRSNAStatsCMACICVErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsRSNAStatsCMACReplays}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsRSNAStatsCMACReplaysGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsRSNAStatsRobustMgmtCCMPReplays}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsRSNAStatsRobustMgmtCCMPReplaysGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsRSNAStatsTKIPICVErrors}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsRSNAStatsTKIPICVErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsRSNAStatsTKIPReplays}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsRSNAStatsTKIPReplaysGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsRSNAStatsCCMPDecryptErrors}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsRSNAStatsCCMPDecryptErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsRSNAStatsCCMPReplays}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsRSNAStatsCCMPReplaysGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsReportingReasonSTACounters}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsReportingReasonSTACountersGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsReportingReasonQosCounters}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsReportingReasonQosCountersGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsReportingReasonRsnaCounters}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsReportingReasonRsnaCountersGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsTransmittedAMSDUCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsTransmittedAMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsFailedAMSDUCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsFailedAMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsRetryAMSDUCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsRetryAMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsMultipleRetryAMSDUCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsMultipleRetryAMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsTransmittedOctetsInAMSDUCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsTransmittedOctetsInAMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsAMSDUAckFailureCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsAMSDUAckFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsReceivedAMSDUCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsReceivedAMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsReceivedOctetsInAMSDUCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsReceivedOctetsInAMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsTransmittedAMPDUCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsTransmittedAMPDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsTransmittedMPDUsInAMPDUCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsTransmittedMPDUsInAMPDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsTransmittedOctetsInAMPDUCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsTransmittedOctetsInAMPDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsAMPDUReceivedCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsAMPDUReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsMPDUInReceivedAMPDUCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsMPDUInReceivedAMPDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsReceivedOctetsInAMPDUCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsReceivedOctetsInAMPDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsAMPDUDelimiterCRCErrorCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsAMPDUDelimiterCRCErrorCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsImplicitBARFailureCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsImplicitBARFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsExplicitBARFailureCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsExplicitBARFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsChannelWidthSwitchCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsChannelWidthSwitchCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsTwentyMHzFrameTransmittedCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsTwentyMHzFrameTransmittedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsFortyMHzFrameTransmittedCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsFortyMHzFrameTransmittedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsTwentyMHzFrameReceivedCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsTwentyMHzFrameReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsFortyMHzFrameReceivedCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsFortyMHzFrameReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsPSMPUTTGrantDuration}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsPSMPUTTGrantDurationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsPSMPUTTUsedDuration}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsPSMPUTTUsedDurationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsGrantedRDGUsedCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsGrantedRDGUsedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsGrantedRDGUnusedCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsGrantedRDGUnusedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsTransmittedFramesInGrantedRDGCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsTransmittedFramesInGrantedRDGCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsTransmittedOctetsInGrantedRDGCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsTransmittedOctetsInGrantedRDGCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsDualCTSSuccessCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsDualCTSSuccessCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsDualCTSFailureCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsDualCTSFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsRTSLSIGSuccessCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsRTSLSIGSuccessCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsRTSLSIGFailureCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsRTSLSIGFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsBeamformingFrameCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsBeamformingFrameCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsSTBCCTSSuccessCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsSTBCCTSSuccessCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsSTBCCTSFailureCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsSTBCCTSFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsnonSTBCCTSSuccessCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsnonSTBCCTSSuccessCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11STAStatisticsnonSTBCCTSFailureCount}, GetNextIndexDot11STAStatisticsReportTable, Dot11STAStatisticsnonSTBCCTSFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11STAStatisticsReportTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11STAStatisticsReportTableEntry = { 88, Dot11STAStatisticsReportTableMibEntry };

tMbDbEntry Dot11LCIReportTableMibEntry[]= {

{{10,Dot11LCIReportIndex}, GetNextIndexDot11LCIReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11LCIReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11LCIReportToken}, GetNextIndexDot11LCIReportTable, Dot11LCIReportTokenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11LCIReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11LCIIfIndex}, GetNextIndexDot11LCIReportTable, Dot11LCIIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11LCIReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11LCISTAAddress}, GetNextIndexDot11LCIReportTable, Dot11LCISTAAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11LCIReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11LCILatitudeResolution}, GetNextIndexDot11LCIReportTable, Dot11LCILatitudeResolutionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11LCIReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11LCILatitudeInteger}, GetNextIndexDot11LCIReportTable, Dot11LCILatitudeIntegerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11LCIReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11LCILatitudeFraction}, GetNextIndexDot11LCIReportTable, Dot11LCILatitudeFractionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11LCIReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11LCILongitudeResolution}, GetNextIndexDot11LCIReportTable, Dot11LCILongitudeResolutionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11LCIReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11LCILongitudeInteger}, GetNextIndexDot11LCIReportTable, Dot11LCILongitudeIntegerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11LCIReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11LCILongitudeFraction}, GetNextIndexDot11LCIReportTable, Dot11LCILongitudeFractionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11LCIReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11LCIAltitudeType}, GetNextIndexDot11LCIReportTable, Dot11LCIAltitudeTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11LCIReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11LCIAltitudeResolution}, GetNextIndexDot11LCIReportTable, Dot11LCIAltitudeResolutionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11LCIReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11LCIAltitudeInteger}, GetNextIndexDot11LCIReportTable, Dot11LCIAltitudeIntegerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11LCIReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11LCIAltitudeFraction}, GetNextIndexDot11LCIReportTable, Dot11LCIAltitudeFractionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11LCIReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11LCIDatum}, GetNextIndexDot11LCIReportTable, Dot11LCIDatumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11LCIReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11LCIAzimuthType}, GetNextIndexDot11LCIReportTable, Dot11LCIAzimuthTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11LCIReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11LCIAzimuthResolution}, GetNextIndexDot11LCIReportTable, Dot11LCIAzimuthResolutionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11LCIReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11LCIAzimuth}, GetNextIndexDot11LCIReportTable, Dot11LCIAzimuthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11LCIReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11LCIVendorSpecific}, GetNextIndexDot11LCIReportTable, Dot11LCIVendorSpecificGet, Dot11LCIVendorSpecificSet, Dot11LCIVendorSpecificTest, Dot11LCIReportTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11LCIReportTableINDEX, 1, 0, 0, "0"},

{{10,Dot11LCIRprtMeasurementMode}, GetNextIndexDot11LCIReportTable, Dot11LCIRprtMeasurementModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11LCIReportTableINDEX, 1, 0, 0, "0"},
};
tMibData Dot11LCIReportTableEntry = { 20, Dot11LCIReportTableMibEntry };

tMbDbEntry Dot11TransmitStreamReportTableMibEntry[]= {

{{10,Dot11TransmitStreamRprtIndex}, GetNextIndexDot11TransmitStreamReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11TransmitStreamReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11TransmitStreamRprtRqstToken}, GetNextIndexDot11TransmitStreamReportTable, Dot11TransmitStreamRprtRqstTokenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11TransmitStreamReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11TransmitStreamRprtIfIndex}, GetNextIndexDot11TransmitStreamReportTable, Dot11TransmitStreamRprtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11TransmitStreamReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11TransmitStreamMeasuringSTAAddr}, GetNextIndexDot11TransmitStreamReportTable, Dot11TransmitStreamMeasuringSTAAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11TransmitStreamReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11TransmitStreamRprtActualStartTime}, GetNextIndexDot11TransmitStreamReportTable, Dot11TransmitStreamRprtActualStartTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11TransmitStreamReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11TransmitStreamRprtMeasurementDuration}, GetNextIndexDot11TransmitStreamReportTable, Dot11TransmitStreamRprtMeasurementDurationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11TransmitStreamReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11TransmitStreamRprtPeerSTAAddress}, GetNextIndexDot11TransmitStreamReportTable, Dot11TransmitStreamRprtPeerSTAAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11TransmitStreamReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11TransmitStreamRprtTID}, GetNextIndexDot11TransmitStreamReportTable, Dot11TransmitStreamRprtTIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11TransmitStreamReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11TransmitStreamRprtAverageQueueDelay}, GetNextIndexDot11TransmitStreamReportTable, Dot11TransmitStreamRprtAverageQueueDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11TransmitStreamReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11TransmitStreamRprtAverageTransmitDelay}, GetNextIndexDot11TransmitStreamReportTable, Dot11TransmitStreamRprtAverageTransmitDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11TransmitStreamReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11TransmitStreamRprtTransmittedMSDUCount}, GetNextIndexDot11TransmitStreamReportTable, Dot11TransmitStreamRprtTransmittedMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11TransmitStreamReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11TransmitStreamRprtMSDUDiscardedCount}, GetNextIndexDot11TransmitStreamReportTable, Dot11TransmitStreamRprtMSDUDiscardedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11TransmitStreamReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11TransmitStreamRprtMSDUFailedCount}, GetNextIndexDot11TransmitStreamReportTable, Dot11TransmitStreamRprtMSDUFailedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11TransmitStreamReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11TransmitStreamRprtMultipleRetryCount}, GetNextIndexDot11TransmitStreamReportTable, Dot11TransmitStreamRprtMultipleRetryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11TransmitStreamReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11TransmitStreamRprtCFPollsLostCount}, GetNextIndexDot11TransmitStreamReportTable, Dot11TransmitStreamRprtCFPollsLostCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11TransmitStreamReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11TransmitStreamRprtBin0Range}, GetNextIndexDot11TransmitStreamReportTable, Dot11TransmitStreamRprtBin0RangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11TransmitStreamReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11TransmitStreamRprtDelayHistogram}, GetNextIndexDot11TransmitStreamReportTable, Dot11TransmitStreamRprtDelayHistogramGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11TransmitStreamReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11TransmitStreamRprtReason}, GetNextIndexDot11TransmitStreamReportTable, Dot11TransmitStreamRprtReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11TransmitStreamReportTableINDEX, 1, 0, 0, "0"},

{{10,Dot11TransmitStreamRprtVendorSpecific}, GetNextIndexDot11TransmitStreamReportTable, Dot11TransmitStreamRprtVendorSpecificGet, Dot11TransmitStreamRprtVendorSpecificSet, Dot11TransmitStreamRprtVendorSpecificTest, Dot11TransmitStreamReportTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11TransmitStreamReportTableINDEX, 1, 0, 0, "0"},

{{10,Dot11TransmitStreamRprtMeasurementMode}, GetNextIndexDot11TransmitStreamReportTable, Dot11TransmitStreamRprtMeasurementModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11TransmitStreamReportTableINDEX, 1, 0, 0, "0"},
};
tMibData Dot11TransmitStreamReportTableEntry = { 20, Dot11TransmitStreamReportTableMibEntry };

tMbDbEntry Dot11APChannelReportTableMibEntry[]= {

{{10,Dot11APChannelReportIndex}, GetNextIndexDot11APChannelReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11APChannelReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11APChannelReportIfIndex}, GetNextIndexDot11APChannelReportTable, Dot11APChannelReportIfIndexGet, Dot11APChannelReportIfIndexSet, Dot11APChannelReportIfIndexTest, Dot11APChannelReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11APChannelReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11APChannelReportOperatingClass}, GetNextIndexDot11APChannelReportTable, Dot11APChannelReportOperatingClassGet, Dot11APChannelReportOperatingClassSet, Dot11APChannelReportOperatingClassTest, Dot11APChannelReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11APChannelReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11APChannelReportChannelList}, GetNextIndexDot11APChannelReportTable, Dot11APChannelReportChannelListGet, Dot11APChannelReportChannelListSet, Dot11APChannelReportChannelListTest, Dot11APChannelReportTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APChannelReportTableINDEX, 1, 0, 0, "0"},
};
tMibData Dot11APChannelReportTableEntry = { 4, Dot11APChannelReportTableMibEntry };

tMbDbEntry Dot11RMNeighborReportNextIndexMibEntry[]= {

{{8,Dot11RMNeighborReportNextIndex}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, NULL, 0, 0, 0, NULL},
};
tMibData Dot11RMNeighborReportNextIndexEntry = { 1, Dot11RMNeighborReportNextIndexMibEntry };

tMbDbEntry Dot11RMNeighborReportTableMibEntry[]= {

{{10,Dot11RMNeighborReportIndex}, GetNextIndexDot11RMNeighborReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportIfIndex}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportIfIndexGet, Dot11RMNeighborReportIfIndexSet, Dot11RMNeighborReportIfIndexTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportBSSID}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportBSSIDGet, Dot11RMNeighborReportBSSIDSet, Dot11RMNeighborReportBSSIDTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportAPReachability}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportAPReachabilityGet, Dot11RMNeighborReportAPReachabilitySet, Dot11RMNeighborReportAPReachabilityTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportSecurity}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportSecurityGet, Dot11RMNeighborReportSecuritySet, Dot11RMNeighborReportSecurityTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportCapSpectrumMgmt}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportCapSpectrumMgmtGet, Dot11RMNeighborReportCapSpectrumMgmtSet, Dot11RMNeighborReportCapSpectrumMgmtTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportCapQoS}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportCapQoSGet, Dot11RMNeighborReportCapQoSSet, Dot11RMNeighborReportCapQoSTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportCapAPSD}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportCapAPSDGet, Dot11RMNeighborReportCapAPSDSet, Dot11RMNeighborReportCapAPSDTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportCapRM}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportCapRMGet, Dot11RMNeighborReportCapRMSet, Dot11RMNeighborReportCapRMTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportCapDelayBlockAck}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportCapDelayBlockAckGet, Dot11RMNeighborReportCapDelayBlockAckSet, Dot11RMNeighborReportCapDelayBlockAckTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportCapImmediateBlockAck}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportCapImmediateBlockAckGet, Dot11RMNeighborReportCapImmediateBlockAckSet, Dot11RMNeighborReportCapImmediateBlockAckTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportKeyScope}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportKeyScopeGet, Dot11RMNeighborReportKeyScopeSet, Dot11RMNeighborReportKeyScopeTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportOperatingClass}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportOperatingClassGet, Dot11RMNeighborReportOperatingClassSet, Dot11RMNeighborReportOperatingClassTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportChannelNumber}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportChannelNumberGet, Dot11RMNeighborReportChannelNumberSet, Dot11RMNeighborReportChannelNumberTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportPhyType}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportPhyTypeGet, Dot11RMNeighborReportPhyTypeSet, Dot11RMNeighborReportPhyTypeTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportNeighborTSFInfo}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportNeighborTSFInfoGet, Dot11RMNeighborReportNeighborTSFInfoSet, Dot11RMNeighborReportNeighborTSFInfoTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportPilotInterval}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportPilotIntervalGet, Dot11RMNeighborReportPilotIntervalSet, Dot11RMNeighborReportPilotIntervalTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportPilotMultipleBSSID}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportPilotMultipleBSSIDGet, Dot11RMNeighborReportPilotMultipleBSSIDSet, Dot11RMNeighborReportPilotMultipleBSSIDTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportRMEnabledCapabilities}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportRMEnabledCapabilitiesGet, Dot11RMNeighborReportRMEnabledCapabilitiesSet, Dot11RMNeighborReportRMEnabledCapabilitiesTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportVendorSpecific}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportVendorSpecificGet, Dot11RMNeighborReportVendorSpecificSet, Dot11RMNeighborReportVendorSpecificTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, "0"},

{{10,Dot11RMNeighborReportRowStatus}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportRowStatusGet, Dot11RMNeighborReportRowStatusSet, Dot11RMNeighborReportRowStatusTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 1, NULL},

{{10,Dot11RMNeighborReportMobilityDomain}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportMobilityDomainGet, Dot11RMNeighborReportMobilityDomainSet, Dot11RMNeighborReportMobilityDomainTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportCapHT}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportCapHTGet, Dot11RMNeighborReportCapHTSet, Dot11RMNeighborReportCapHTTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTLDPCCodingCap}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTLDPCCodingCapGet, Dot11RMNeighborReportHTLDPCCodingCapSet, Dot11RMNeighborReportHTLDPCCodingCapTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTSupportedChannelWidthSet}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTSupportedChannelWidthSetGet, Dot11RMNeighborReportHTSupportedChannelWidthSetSet, Dot11RMNeighborReportHTSupportedChannelWidthSetTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTSMPowerSave}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTSMPowerSaveGet, Dot11RMNeighborReportHTSMPowerSaveSet, Dot11RMNeighborReportHTSMPowerSaveTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTGreenfield}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTGreenfieldGet, Dot11RMNeighborReportHTGreenfieldSet, Dot11RMNeighborReportHTGreenfieldTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTShortGIfor20MHz}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTShortGIfor20MHzGet, Dot11RMNeighborReportHTShortGIfor20MHzSet, Dot11RMNeighborReportHTShortGIfor20MHzTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTShortGIfor40MHz}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTShortGIfor40MHzGet, Dot11RMNeighborReportHTShortGIfor40MHzSet, Dot11RMNeighborReportHTShortGIfor40MHzTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTTxSTBC}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTTxSTBCGet, Dot11RMNeighborReportHTTxSTBCSet, Dot11RMNeighborReportHTTxSTBCTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTRxSTBC}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTRxSTBCGet, Dot11RMNeighborReportHTRxSTBCSet, Dot11RMNeighborReportHTRxSTBCTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTDelayedBlockAck}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTDelayedBlockAckGet, Dot11RMNeighborReportHTDelayedBlockAckSet, Dot11RMNeighborReportHTDelayedBlockAckTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTMaxAMSDULength}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTMaxAMSDULengthGet, Dot11RMNeighborReportHTMaxAMSDULengthSet, Dot11RMNeighborReportHTMaxAMSDULengthTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTDSSCCKModein40MHz}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTDSSCCKModein40MHzGet, Dot11RMNeighborReportHTDSSCCKModein40MHzSet, Dot11RMNeighborReportHTDSSCCKModein40MHzTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTFortyMHzIntolerant}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTFortyMHzIntolerantGet, Dot11RMNeighborReportHTFortyMHzIntolerantSet, Dot11RMNeighborReportHTFortyMHzIntolerantTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTLSIGTXOPProtectionSupport}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTLSIGTXOPProtectionSupportGet, Dot11RMNeighborReportHTLSIGTXOPProtectionSupportSet, Dot11RMNeighborReportHTLSIGTXOPProtectionSupportTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTMaxAMPDULengthExponent}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTMaxAMPDULengthExponentGet, Dot11RMNeighborReportHTMaxAMPDULengthExponentSet, Dot11RMNeighborReportHTMaxAMPDULengthExponentTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTMinMPDUStartSpacing}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTMinMPDUStartSpacingGet, Dot11RMNeighborReportHTMinMPDUStartSpacingSet, Dot11RMNeighborReportHTMinMPDUStartSpacingTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTRxMCSBitMask}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTRxMCSBitMaskGet, Dot11RMNeighborReportHTRxMCSBitMaskSet, Dot11RMNeighborReportHTRxMCSBitMaskTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTRxHighestSupportedDataRate}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTRxHighestSupportedDataRateGet, Dot11RMNeighborReportHTRxHighestSupportedDataRateSet, Dot11RMNeighborReportHTRxHighestSupportedDataRateTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTTxMCSSetDefined}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTTxMCSSetDefinedGet, Dot11RMNeighborReportHTTxMCSSetDefinedSet, Dot11RMNeighborReportHTTxMCSSetDefinedTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTTxRxMCSSetNotEqual}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTTxRxMCSSetNotEqualGet, Dot11RMNeighborReportHTTxRxMCSSetNotEqualSet, Dot11RMNeighborReportHTTxRxMCSSetNotEqualTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTTxMaxNumberSpatialStreamsSupported}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTTxMaxNumberSpatialStreamsSupportedGet, Dot11RMNeighborReportHTTxMaxNumberSpatialStreamsSupportedSet, Dot11RMNeighborReportHTTxMaxNumberSpatialStreamsSupportedTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTTxUnequalModulationSupported}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTTxUnequalModulationSupportedGet, Dot11RMNeighborReportHTTxUnequalModulationSupportedSet, Dot11RMNeighborReportHTTxUnequalModulationSupportedTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTPCO}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTPCOGet, Dot11RMNeighborReportHTPCOSet, Dot11RMNeighborReportHTPCOTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTPCOTransitionTime}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTPCOTransitionTimeGet, Dot11RMNeighborReportHTPCOTransitionTimeSet, Dot11RMNeighborReportHTPCOTransitionTimeTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTMCSFeedback}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTMCSFeedbackGet, Dot11RMNeighborReportHTMCSFeedbackSet, Dot11RMNeighborReportHTMCSFeedbackTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTCSupport}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTCSupportGet, Dot11RMNeighborReportHTCSupportSet, Dot11RMNeighborReportHTCSupportTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTRDResponder}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTRDResponderGet, Dot11RMNeighborReportHTRDResponderSet, Dot11RMNeighborReportHTRDResponderTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTImplictTransmitBeamformingReceivingCap}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTImplictTransmitBeamformingReceivingCapGet, Dot11RMNeighborReportHTImplictTransmitBeamformingReceivingCapSet, Dot11RMNeighborReportHTImplictTransmitBeamformingReceivingCapTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTReceiveStaggeredSoundingCap}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTReceiveStaggeredSoundingCapGet, Dot11RMNeighborReportHTReceiveStaggeredSoundingCapSet, Dot11RMNeighborReportHTReceiveStaggeredSoundingCapTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTTransmitStaggeredSoundingCap}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTTransmitStaggeredSoundingCapGet, Dot11RMNeighborReportHTTransmitStaggeredSoundingCapSet, Dot11RMNeighborReportHTTransmitStaggeredSoundingCapTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTReceiveNDPCap}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTReceiveNDPCapGet, Dot11RMNeighborReportHTReceiveNDPCapSet, Dot11RMNeighborReportHTReceiveNDPCapTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTTransmitNDPCap}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTTransmitNDPCapGet, Dot11RMNeighborReportHTTransmitNDPCapSet, Dot11RMNeighborReportHTTransmitNDPCapTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTImplicitTransmitBeamformingCap}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTImplicitTransmitBeamformingCapGet, Dot11RMNeighborReportHTImplicitTransmitBeamformingCapSet, Dot11RMNeighborReportHTImplicitTransmitBeamformingCapTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTTransmitBeamformingCalibration}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTTransmitBeamformingCalibrationGet, Dot11RMNeighborReportHTTransmitBeamformingCalibrationSet, Dot11RMNeighborReportHTTransmitBeamformingCalibrationTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTExplicitCSITransmitBeamformingCap}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTExplicitCSITransmitBeamformingCapGet, Dot11RMNeighborReportHTExplicitCSITransmitBeamformingCapSet, Dot11RMNeighborReportHTExplicitCSITransmitBeamformingCapTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTExplicitNonCompressedSteeringCap}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTExplicitNonCompressedSteeringCapGet, Dot11RMNeighborReportHTExplicitNonCompressedSteeringCapSet, Dot11RMNeighborReportHTExplicitNonCompressedSteeringCapTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTExplicitCompressedSteeringCap}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTExplicitCompressedSteeringCapGet, Dot11RMNeighborReportHTExplicitCompressedSteeringCapSet, Dot11RMNeighborReportHTExplicitCompressedSteeringCapTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTExplicitTransmitBeamformingFeedback}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTExplicitTransmitBeamformingFeedbackGet, Dot11RMNeighborReportHTExplicitTransmitBeamformingFeedbackSet, Dot11RMNeighborReportHTExplicitTransmitBeamformingFeedbackTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNbRprtHTExplicitNonCompressedBeamformingFeedbackCap}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNbRprtHTExplicitNonCompressedBeamformingFeedbackCapGet, Dot11RMNbRprtHTExplicitNonCompressedBeamformingFeedbackCapSet, Dot11RMNbRprtHTExplicitNonCompressedBeamformingFeedbackCapTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTExplicitCompressedBeamformingFeedbackCap}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTExplicitCompressedBeamformingFeedbackCapGet, Dot11RMNeighborReportHTExplicitCompressedBeamformingFeedbackCapSet, Dot11RMNeighborReportHTExplicitCompressedBeamformingFeedbackCapTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTTransmitBeamformingMinimalGrouping}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTTransmitBeamformingMinimalGroupingGet, Dot11RMNeighborReportHTTransmitBeamformingMinimalGroupingSet, Dot11RMNeighborReportHTTransmitBeamformingMinimalGroupingTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNbRprtHTCSINumberofTxBeamformingAntennasSuppt}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNbRprtHTCSINumberofTxBeamformingAntennasSupptGet, Dot11RMNbRprtHTCSINumberofTxBeamformingAntennasSupptSet, Dot11RMNbRprtHTCSINumberofTxBeamformingAntennasSupptTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNbRprtHTNonCompressedSteeringNumofTxBmfmingAntennasSuppt}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNbRprtHTNonCompressedSteeringNumofTxBmfmingAntennasSupptGet, Dot11RMNbRprtHTNonCompressedSteeringNumofTxBmfmingAntennasSupptSet, Dot11RMNbRprtHTNonCompressedSteeringNumofTxBmfmingAntennasSupptTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNbRprtHTCompressedSteeringNumberofTxBmfmingAntennasSuppt}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNbRprtHTCompressedSteeringNumberofTxBmfmingAntennasSupptGet, Dot11RMNbRprtHTCompressedSteeringNumberofTxBmfmingAntennasSupptSet, Dot11RMNbRprtHTCompressedSteeringNumberofTxBmfmingAntennasSupptTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNbRprtHTCSIMaxNumberofRowsTxBeamformingSuppt}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNbRprtHTCSIMaxNumberofRowsTxBeamformingSupptGet, Dot11RMNbRprtHTCSIMaxNumberofRowsTxBeamformingSupptSet, Dot11RMNbRprtHTCSIMaxNumberofRowsTxBeamformingSupptTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTTransmitBeamformingChannelEstimationCap}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTTransmitBeamformingChannelEstimationCapGet, Dot11RMNeighborReportHTTransmitBeamformingChannelEstimationCapSet, Dot11RMNeighborReportHTTransmitBeamformingChannelEstimationCapTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTAntSelectionCap}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTAntSelectionCapGet, Dot11RMNeighborReportHTAntSelectionCapSet, Dot11RMNeighborReportHTAntSelectionCapTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTExplicitCSIFeedbackBasedTxASELCap}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTExplicitCSIFeedbackBasedTxASELCapGet, Dot11RMNeighborReportHTExplicitCSIFeedbackBasedTxASELCapSet, Dot11RMNeighborReportHTExplicitCSIFeedbackBasedTxASELCapTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTAntIndicesFeedbackBasedTxASELCap}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTAntIndicesFeedbackBasedTxASELCapGet, Dot11RMNeighborReportHTAntIndicesFeedbackBasedTxASELCapSet, Dot11RMNeighborReportHTAntIndicesFeedbackBasedTxASELCapTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTExplicitCSIFeedbackBasedCap}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTExplicitCSIFeedbackBasedCapGet, Dot11RMNeighborReportHTExplicitCSIFeedbackBasedCapSet, Dot11RMNeighborReportHTExplicitCSIFeedbackBasedCapTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTAntIndicesFeedbackCap}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTAntIndicesFeedbackCapGet, Dot11RMNeighborReportHTAntIndicesFeedbackCapSet, Dot11RMNeighborReportHTAntIndicesFeedbackCapTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTRxASELCap}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTRxASELCapGet, Dot11RMNeighborReportHTRxASELCapSet, Dot11RMNeighborReportHTRxASELCapTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTTxSoundingPPDUsCap}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTTxSoundingPPDUsCapGet, Dot11RMNeighborReportHTTxSoundingPPDUsCapSet, Dot11RMNeighborReportHTTxSoundingPPDUsCapTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTInfoPrimaryChannel}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTInfoPrimaryChannelGet, Dot11RMNeighborReportHTInfoPrimaryChannelSet, Dot11RMNeighborReportHTInfoPrimaryChannelTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTInfoSecChannelOffset}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTInfoSecChannelOffsetGet, Dot11RMNeighborReportHTInfoSecChannelOffsetSet, Dot11RMNeighborReportHTInfoSecChannelOffsetTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTInfoSTAChannelWidth}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTInfoSTAChannelWidthGet, Dot11RMNeighborReportHTInfoSTAChannelWidthSet, Dot11RMNeighborReportHTInfoSTAChannelWidthTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTInfoRIFSMode}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTInfoRIFSModeGet, Dot11RMNeighborReportHTInfoRIFSModeSet, Dot11RMNeighborReportHTInfoRIFSModeTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTInfoProtection}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTInfoProtectionGet, Dot11RMNeighborReportHTInfoProtectionSet, Dot11RMNeighborReportHTInfoProtectionTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTInfoNonGreenfieldHTSTAsPresent}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTInfoNonGreenfieldHTSTAsPresentGet, Dot11RMNeighborReportHTInfoNonGreenfieldHTSTAsPresentSet, Dot11RMNeighborReportHTInfoNonGreenfieldHTSTAsPresentTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTInfoOBSSNonHTSTAsPresent}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTInfoOBSSNonHTSTAsPresentGet, Dot11RMNeighborReportHTInfoOBSSNonHTSTAsPresentSet, Dot11RMNeighborReportHTInfoOBSSNonHTSTAsPresentTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTInfoDualBeacon}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTInfoDualBeaconGet, Dot11RMNeighborReportHTInfoDualBeaconSet, Dot11RMNeighborReportHTInfoDualBeaconTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTInfoDualCTSProtection}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTInfoDualCTSProtectionGet, Dot11RMNeighborReportHTInfoDualCTSProtectionSet, Dot11RMNeighborReportHTInfoDualCTSProtectionTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTInfoSTBCBeacon}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTInfoSTBCBeaconGet, Dot11RMNeighborReportHTInfoSTBCBeaconSet, Dot11RMNeighborReportHTInfoSTBCBeaconTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTInfoLSIGTXOPProtectionSup}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTInfoLSIGTXOPProtectionSupGet, Dot11RMNeighborReportHTInfoLSIGTXOPProtectionSupSet, Dot11RMNeighborReportHTInfoLSIGTXOPProtectionSupTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTInfoPCOActive}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTInfoPCOActiveGet, Dot11RMNeighborReportHTInfoPCOActiveSet, Dot11RMNeighborReportHTInfoPCOActiveTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTInfoPCOPhase}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTInfoPCOPhaseGet, Dot11RMNeighborReportHTInfoPCOPhaseSet, Dot11RMNeighborReportHTInfoPCOPhaseTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTInfoBasicMCSSet}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTInfoBasicMCSSetGet, Dot11RMNeighborReportHTInfoBasicMCSSetSet, Dot11RMNeighborReportHTInfoBasicMCSSetTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportHTSecChannelOffset}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportHTSecChannelOffsetGet, Dot11RMNeighborReportHTSecChannelOffsetSet, Dot11RMNeighborReportHTSecChannelOffsetTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportExtCapPSMPSupport}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportExtCapPSMPSupportGet, Dot11RMNeighborReportExtCapPSMPSupportSet, Dot11RMNeighborReportExtCapPSMPSupportTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportExtCapSPSMPSup}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportExtCapSPSMPSupGet, Dot11RMNeighborReportExtCapSPSMPSupSet, Dot11RMNeighborReportExtCapSPSMPSupTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportExtCapServiceIntervalGranularity}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportExtCapServiceIntervalGranularityGet, Dot11RMNeighborReportExtCapServiceIntervalGranularitySet, Dot11RMNeighborReportExtCapServiceIntervalGranularityTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportBSSTransitCandPreference}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportBSSTransitCandPreferenceGet, Dot11RMNeighborReportBSSTransitCandPreferenceSet, Dot11RMNeighborReportBSSTransitCandPreferenceTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportBSSTerminationTSF}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportBSSTerminationTSFGet, Dot11RMNeighborReportBSSTerminationTSFSet, Dot11RMNeighborReportBSSTerminationTSFTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11RMNeighborReportBSSTerminationDuration}, GetNextIndexDot11RMNeighborReportTable, Dot11RMNeighborReportBSSTerminationDurationGet, Dot11RMNeighborReportBSSTerminationDurationSet, Dot11RMNeighborReportBSSTerminationDurationTest, Dot11RMNeighborReportTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RMNeighborReportTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11RMNeighborReportTableEntry = { 96, Dot11RMNeighborReportTableMibEntry };

tMbDbEntry Dot11FastBSSTransitionConfigTableMibEntry[]= {

{{8,Dot11FastBSSTransitionActivated}, GetNextIndexDot11FastBSSTransitionConfigTable, Dot11FastBSSTransitionActivatedGet, Dot11FastBSSTransitionActivatedSet, Dot11FastBSSTransitionActivatedTest, Dot11FastBSSTransitionConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11FastBSSTransitionConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11FTMobilityDomainID}, GetNextIndexDot11FastBSSTransitionConfigTable, Dot11FTMobilityDomainIDGet, Dot11FTMobilityDomainIDSet, Dot11FTMobilityDomainIDTest, Dot11FastBSSTransitionConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11FastBSSTransitionConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11FTOverDSActivated}, GetNextIndexDot11FastBSSTransitionConfigTable, Dot11FTOverDSActivatedGet, Dot11FTOverDSActivatedSet, Dot11FTOverDSActivatedTest, Dot11FastBSSTransitionConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11FastBSSTransitionConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11FTResourceRequestSupported}, GetNextIndexDot11FastBSSTransitionConfigTable, Dot11FTResourceRequestSupportedGet, Dot11FTResourceRequestSupportedSet, Dot11FTResourceRequestSupportedTest, Dot11FastBSSTransitionConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11FastBSSTransitionConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11FTR0KeyHolderID}, GetNextIndexDot11FastBSSTransitionConfigTable, Dot11FTR0KeyHolderIDGet, Dot11FTR0KeyHolderIDSet, Dot11FTR0KeyHolderIDTest, Dot11FastBSSTransitionConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11FastBSSTransitionConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11FTR0KeyLifetime}, GetNextIndexDot11FastBSSTransitionConfigTable, Dot11FTR0KeyLifetimeGet, Dot11FTR0KeyLifetimeSet, Dot11FTR0KeyLifetimeTest, Dot11FastBSSTransitionConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11FastBSSTransitionConfigTableINDEX, 1, 0, 0, "1209600"},

{{8,Dot11FTR1KeyHolderID}, GetNextIndexDot11FastBSSTransitionConfigTable, Dot11FTR1KeyHolderIDGet, Dot11FTR1KeyHolderIDSet, Dot11FTR1KeyHolderIDTest, Dot11FastBSSTransitionConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11FastBSSTransitionConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11FTReassociationDeadline}, GetNextIndexDot11FastBSSTransitionConfigTable, Dot11FTReassociationDeadlineGet, Dot11FTReassociationDeadlineSet, Dot11FTReassociationDeadlineTest, Dot11FastBSSTransitionConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11FastBSSTransitionConfigTableINDEX, 1, 0, 0, "1000"},
};
tMibData Dot11FastBSSTransitionConfigTableEntry = { 8, Dot11FastBSSTransitionConfigTableMibEntry };

tMbDbEntry Dot11LCIDSETableMibEntry[]= {

{{8,Dot11LCIDSEIndex}, GetNextIndexDot11LCIDSETable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11LCIDSETableINDEX, 1, 0, 0, NULL},

{{8,Dot11LCIDSEIfIndex}, GetNextIndexDot11LCIDSETable, Dot11LCIDSEIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11LCIDSETableINDEX, 1, 0, 0, NULL},

{{8,Dot11LCIDSECurrentOperatingClass}, GetNextIndexDot11LCIDSETable, Dot11LCIDSECurrentOperatingClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11LCIDSETableINDEX, 1, 0, 0, NULL},

{{8,Dot11LCIDSELatitudeResolution}, GetNextIndexDot11LCIDSETable, Dot11LCIDSELatitudeResolutionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11LCIDSETableINDEX, 1, 0, 0, NULL},

{{8,Dot11LCIDSELatitudeInteger}, GetNextIndexDot11LCIDSETable, Dot11LCIDSELatitudeIntegerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11LCIDSETableINDEX, 1, 0, 0, NULL},

{{8,Dot11LCIDSELatitudeFraction}, GetNextIndexDot11LCIDSETable, Dot11LCIDSELatitudeFractionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11LCIDSETableINDEX, 1, 0, 0, NULL},

{{8,Dot11LCIDSELongitudeResolution}, GetNextIndexDot11LCIDSETable, Dot11LCIDSELongitudeResolutionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11LCIDSETableINDEX, 1, 0, 0, NULL},

{{8,Dot11LCIDSELongitudeInteger}, GetNextIndexDot11LCIDSETable, Dot11LCIDSELongitudeIntegerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11LCIDSETableINDEX, 1, 0, 0, NULL},

{{8,Dot11LCIDSELongitudeFraction}, GetNextIndexDot11LCIDSETable, Dot11LCIDSELongitudeFractionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11LCIDSETableINDEX, 1, 0, 0, NULL},

{{8,Dot11LCIDSEAltitudeType}, GetNextIndexDot11LCIDSETable, Dot11LCIDSEAltitudeTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11LCIDSETableINDEX, 1, 0, 0, "3"},

{{8,Dot11LCIDSEAltitudeResolution}, GetNextIndexDot11LCIDSETable, Dot11LCIDSEAltitudeResolutionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11LCIDSETableINDEX, 1, 0, 0, NULL},

{{8,Dot11LCIDSEAltitudeInteger}, GetNextIndexDot11LCIDSETable, Dot11LCIDSEAltitudeIntegerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11LCIDSETableINDEX, 1, 0, 0, NULL},

{{8,Dot11LCIDSEAltitudeFraction}, GetNextIndexDot11LCIDSETable, Dot11LCIDSEAltitudeFractionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11LCIDSETableINDEX, 1, 0, 0, NULL},

{{8,Dot11LCIDSEDatum}, GetNextIndexDot11LCIDSETable, Dot11LCIDSEDatumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11LCIDSETableINDEX, 1, 0, 0, "1"},

{{8,Dot11RegLocAgreement}, GetNextIndexDot11LCIDSETable, Dot11RegLocAgreementGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11LCIDSETableINDEX, 1, 0, 0, "2"},

{{8,Dot11RegLocDSE}, GetNextIndexDot11LCIDSETable, Dot11RegLocDSEGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11LCIDSETableINDEX, 1, 0, 0, "2"},

{{8,Dot11DependentSTA}, GetNextIndexDot11LCIDSETable, Dot11DependentSTAGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11LCIDSETableINDEX, 1, 0, 0, "1"},

{{8,Dot11DependentEnablementIdentifier}, GetNextIndexDot11LCIDSETable, Dot11DependentEnablementIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11LCIDSETableINDEX, 1, 0, 0, "0"},

{{8,Dot11DSEEnablementTimeLimit}, GetNextIndexDot11LCIDSETable, Dot11DSEEnablementTimeLimitGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11LCIDSETableINDEX, 1, 0, 0, "32"},

{{8,Dot11DSEEnablementFailHoldTime}, GetNextIndexDot11LCIDSETable, Dot11DSEEnablementFailHoldTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11LCIDSETableINDEX, 1, 0, 0, "512"},

{{8,Dot11DSERenewalTime}, GetNextIndexDot11LCIDSETable, Dot11DSERenewalTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11LCIDSETableINDEX, 1, 0, 0, "60"},

{{8,Dot11DSETransmitDivisor}, GetNextIndexDot11LCIDSETable, Dot11DSETransmitDivisorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11LCIDSETableINDEX, 1, 0, 0, "256"},
};
tMibData Dot11LCIDSETableEntry = { 22, Dot11LCIDSETableMibEntry };

tMbDbEntry Dot11HTStationConfigTableMibEntry[]= {

{{8,Dot11HTOperationalMCSSet}, GetNextIndexDot11HTStationConfigTable, Dot11HTOperationalMCSSetGet, Dot11HTOperationalMCSSetSet, Dot11HTOperationalMCSSetTest, Dot11HTStationConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11HTStationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MIMOPowerSave}, GetNextIndexDot11HTStationConfigTable, Dot11MIMOPowerSaveGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11HTStationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11NDelayedBlockAckOptionImplemented}, GetNextIndexDot11HTStationConfigTable, Dot11NDelayedBlockAckOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11HTStationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MaxAMSDULength}, GetNextIndexDot11HTStationConfigTable, Dot11MaxAMSDULengthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11HTStationConfigTableINDEX, 1, 0, 0, "3839"},

{{8,Dot11STBCControlFrameOptionImplemented}, GetNextIndexDot11HTStationConfigTable, Dot11STBCControlFrameOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11HTStationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11LsigTxopProtectionOptionImplemented}, GetNextIndexDot11HTStationConfigTable, Dot11LsigTxopProtectionOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11HTStationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MaxRxAMPDUFactor}, GetNextIndexDot11HTStationConfigTable, Dot11MaxRxAMPDUFactorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11HTStationConfigTableINDEX, 1, 0, 0, "0"},

{{8,Dot11MinimumMPDUStartSpacing}, GetNextIndexDot11HTStationConfigTable, Dot11MinimumMPDUStartSpacingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11HTStationConfigTableINDEX, 1, 0, 0, "0"},

{{8,Dot11PCOOptionImplemented}, GetNextIndexDot11HTStationConfigTable, Dot11PCOOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11HTStationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TransitionTime}, GetNextIndexDot11HTStationConfigTable, Dot11TransitionTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11HTStationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MCSFeedbackOptionImplemented}, GetNextIndexDot11HTStationConfigTable, Dot11MCSFeedbackOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11HTStationConfigTableINDEX, 1, 0, 0, "0"},

{{8,Dot11HTControlFieldSupported}, GetNextIndexDot11HTStationConfigTable, Dot11HTControlFieldSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11HTStationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RDResponderOptionImplemented}, GetNextIndexDot11HTStationConfigTable, Dot11RDResponderOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11HTStationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11SPPAMSDUCapable}, GetNextIndexDot11HTStationConfigTable, Dot11SPPAMSDUCapableGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11HTStationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11SPPAMSDURequired}, GetNextIndexDot11HTStationConfigTable, Dot11SPPAMSDURequiredGet, Dot11SPPAMSDURequiredSet, Dot11SPPAMSDURequiredTest, Dot11HTStationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11HTStationConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11FortyMHzOptionImplemented}, GetNextIndexDot11HTStationConfigTable, Dot11FortyMHzOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11HTStationConfigTableINDEX, 1, 0, 0, "2"},
};
tMibData Dot11HTStationConfigTableEntry = { 16, Dot11HTStationConfigTableMibEntry };

tMbDbEntry Dot11WirelessMgmtOptionsTableMibEntry[]= {

{{8,Dot11MgmtOptionLocationActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionLocationActivatedGet, Dot11MgmtOptionLocationActivatedSet, Dot11MgmtOptionLocationActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionFMSImplemented}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionFMSImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MgmtOptionFMSActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionFMSActivatedGet, Dot11MgmtOptionFMSActivatedSet, Dot11MgmtOptionFMSActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionEventsActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionEventsActivatedGet, Dot11MgmtOptionEventsActivatedSet, Dot11MgmtOptionEventsActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionDiagnosticsActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionDiagnosticsActivatedGet, Dot11MgmtOptionDiagnosticsActivatedSet, Dot11MgmtOptionDiagnosticsActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionMultiBSSIDImplemented}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionMultiBSSIDImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MgmtOptionMultiBSSIDActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionMultiBSSIDActivatedGet, Dot11MgmtOptionMultiBSSIDActivatedSet, Dot11MgmtOptionMultiBSSIDActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionTFSImplemented}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionTFSImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MgmtOptionTFSActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionTFSActivatedGet, Dot11MgmtOptionTFSActivatedSet, Dot11MgmtOptionTFSActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionWNMSleepModeImplemented}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionWNMSleepModeImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MgmtOptionWNMSleepModeActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionWNMSleepModeActivatedGet, Dot11MgmtOptionWNMSleepModeActivatedSet, Dot11MgmtOptionWNMSleepModeActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionTIMBroadcastImplemented}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionTIMBroadcastImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MgmtOptionTIMBroadcastActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionTIMBroadcastActivatedGet, Dot11MgmtOptionTIMBroadcastActivatedSet, Dot11MgmtOptionTIMBroadcastActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionProxyARPImplemented}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionProxyARPImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MgmtOptionProxyARPActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionProxyARPActivatedGet, Dot11MgmtOptionProxyARPActivatedSet, Dot11MgmtOptionProxyARPActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionBSSTransitionImplemented}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionBSSTransitionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MgmtOptionBSSTransitionActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionBSSTransitionActivatedGet, Dot11MgmtOptionBSSTransitionActivatedSet, Dot11MgmtOptionBSSTransitionActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionQoSTrafficCapabilityImplemented}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionQoSTrafficCapabilityImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MgmtOptionQoSTrafficCapabilityActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionQoSTrafficCapabilityActivatedGet, Dot11MgmtOptionQoSTrafficCapabilityActivatedSet, Dot11MgmtOptionQoSTrafficCapabilityActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionACStationCountImplemented}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionACStationCountImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MgmtOptionACStationCountActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionACStationCountActivatedGet, Dot11MgmtOptionACStationCountActivatedSet, Dot11MgmtOptionACStationCountActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionCoLocIntfReportingImplemented}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionCoLocIntfReportingImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MgmtOptionCoLocIntfReportingActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionCoLocIntfReportingActivatedGet, Dot11MgmtOptionCoLocIntfReportingActivatedSet, Dot11MgmtOptionCoLocIntfReportingActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionMotionDetectionImplemented}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionMotionDetectionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MgmtOptionMotionDetectionActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionMotionDetectionActivatedGet, Dot11MgmtOptionMotionDetectionActivatedSet, Dot11MgmtOptionMotionDetectionActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionTODImplemented}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionTODImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MgmtOptionTODActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionTODActivatedGet, Dot11MgmtOptionTODActivatedSet, Dot11MgmtOptionTODActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionTimingMsmtImplemented}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionTimingMsmtImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MgmtOptionTimingMsmtActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionTimingMsmtActivatedGet, Dot11MgmtOptionTimingMsmtActivatedSet, Dot11MgmtOptionTimingMsmtActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionChannelUsageImplemented}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionChannelUsageImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MgmtOptionChannelUsageActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionChannelUsageActivatedGet, Dot11MgmtOptionChannelUsageActivatedSet, Dot11MgmtOptionChannelUsageActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionTriggerSTAStatisticsActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionTriggerSTAStatisticsActivatedGet, Dot11MgmtOptionTriggerSTAStatisticsActivatedSet, Dot11MgmtOptionTriggerSTAStatisticsActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionSSIDListImplemented}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionSSIDListImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MgmtOptionSSIDListActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionSSIDListActivatedGet, Dot11MgmtOptionSSIDListActivatedSet, Dot11MgmtOptionSSIDListActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionMulticastDiagnosticsActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionMulticastDiagnosticsActivatedGet, Dot11MgmtOptionMulticastDiagnosticsActivatedSet, Dot11MgmtOptionMulticastDiagnosticsActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionLocationTrackingImplemented}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionLocationTrackingImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MgmtOptionLocationTrackingActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionLocationTrackingActivatedGet, Dot11MgmtOptionLocationTrackingActivatedSet, Dot11MgmtOptionLocationTrackingActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionDMSImplemented}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionDMSImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MgmtOptionDMSActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionDMSActivatedGet, Dot11MgmtOptionDMSActivatedSet, Dot11MgmtOptionDMSActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionUAPSDCoexistenceImplemented}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionUAPSDCoexistenceImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MgmtOptionUAPSDCoexistenceActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionUAPSDCoexistenceActivatedGet, Dot11MgmtOptionUAPSDCoexistenceActivatedSet, Dot11MgmtOptionUAPSDCoexistenceActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionWNMNotificationImplemented}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionWNMNotificationImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MgmtOptionWNMNotificationActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionWNMNotificationActivatedGet, Dot11MgmtOptionWNMNotificationActivatedSet, Dot11MgmtOptionWNMNotificationActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MgmtOptionUTCTSFOffsetImplemented}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionUTCTSFOffsetImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MgmtOptionUTCTSFOffsetActivated}, GetNextIndexDot11WirelessMgmtOptionsTable, Dot11MgmtOptionUTCTSFOffsetActivatedGet, Dot11MgmtOptionUTCTSFOffsetActivatedSet, Dot11MgmtOptionUTCTSFOffsetActivatedTest, Dot11WirelessMgmtOptionsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WirelessMgmtOptionsTableINDEX, 1, 0, 0, "2"},
};
tMibData Dot11WirelessMgmtOptionsTableEntry = { 45, Dot11WirelessMgmtOptionsTableMibEntry };

tMbDbEntry Dot11LocationServicesNextIndexMibEntry[]= {

{{6,Dot11LocationServicesNextIndex}, NULL, Dot11LocationServicesNextIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData Dot11LocationServicesNextIndexEntry = { 1, Dot11LocationServicesNextIndexMibEntry };

tMbDbEntry Dot11LocationServicesTableMibEntry[]= {

{{8,Dot11LocationServicesIndex}, GetNextIndexDot11LocationServicesTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11LocationServicesTableINDEX, 1, 0, 0, NULL},

{{8,Dot11LocationServicesMACAddress}, GetNextIndexDot11LocationServicesTable, Dot11LocationServicesMACAddressGet, Dot11LocationServicesMACAddressSet, Dot11LocationServicesMACAddressTest, Dot11LocationServicesTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot11LocationServicesTableINDEX, 1, 0, 0, NULL},

{{8,Dot11LocationServicesLIPIndicationMulticastAddress}, GetNextIndexDot11LocationServicesTable, Dot11LocationServicesLIPIndicationMulticastAddressGet, Dot11LocationServicesLIPIndicationMulticastAddressSet, Dot11LocationServicesLIPIndicationMulticastAddressTest, Dot11LocationServicesTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot11LocationServicesTableINDEX, 1, 0, 0, NULL},

{{8,Dot11LocationServicesLIPReportIntervalUnits}, GetNextIndexDot11LocationServicesTable, Dot11LocationServicesLIPReportIntervalUnitsGet, Dot11LocationServicesLIPReportIntervalUnitsSet, Dot11LocationServicesLIPReportIntervalUnitsTest, Dot11LocationServicesTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11LocationServicesTableINDEX, 1, 0, 0, NULL},

{{8,Dot11LocationServicesLIPNormalReportInterval}, GetNextIndexDot11LocationServicesTable, Dot11LocationServicesLIPNormalReportIntervalGet, Dot11LocationServicesLIPNormalReportIntervalSet, Dot11LocationServicesLIPNormalReportIntervalTest, Dot11LocationServicesTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11LocationServicesTableINDEX, 1, 0, 0, NULL},

{{8,Dot11LocationServicesLIPNormalFramesperChannel}, GetNextIndexDot11LocationServicesTable, Dot11LocationServicesLIPNormalFramesperChannelGet, Dot11LocationServicesLIPNormalFramesperChannelSet, Dot11LocationServicesLIPNormalFramesperChannelTest, Dot11LocationServicesTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11LocationServicesTableINDEX, 1, 0, 0, NULL},

{{8,Dot11LocationServicesLIPInMotionReportInterval}, GetNextIndexDot11LocationServicesTable, Dot11LocationServicesLIPInMotionReportIntervalGet, Dot11LocationServicesLIPInMotionReportIntervalSet, Dot11LocationServicesLIPInMotionReportIntervalTest, Dot11LocationServicesTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11LocationServicesTableINDEX, 1, 0, 0, NULL},

{{8,Dot11LocationServicesLIPInMotionFramesperChannel}, GetNextIndexDot11LocationServicesTable, Dot11LocationServicesLIPInMotionFramesperChannelGet, Dot11LocationServicesLIPInMotionFramesperChannelSet, Dot11LocationServicesLIPInMotionFramesperChannelTest, Dot11LocationServicesTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11LocationServicesTableINDEX, 1, 0, 0, NULL},

{{8,Dot11LocationServicesLIPBurstInterframeInterval}, GetNextIndexDot11LocationServicesTable, Dot11LocationServicesLIPBurstInterframeIntervalGet, Dot11LocationServicesLIPBurstInterframeIntervalSet, Dot11LocationServicesLIPBurstInterframeIntervalTest, Dot11LocationServicesTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11LocationServicesTableINDEX, 1, 0, 0, NULL},

{{8,Dot11LocationServicesLIPTrackingDuration}, GetNextIndexDot11LocationServicesTable, Dot11LocationServicesLIPTrackingDurationGet, Dot11LocationServicesLIPTrackingDurationSet, Dot11LocationServicesLIPTrackingDurationTest, Dot11LocationServicesTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11LocationServicesTableINDEX, 1, 0, 0, NULL},

{{8,Dot11LocationServicesLIPEssDetectionInterval}, GetNextIndexDot11LocationServicesTable, Dot11LocationServicesLIPEssDetectionIntervalGet, Dot11LocationServicesLIPEssDetectionIntervalSet, Dot11LocationServicesLIPEssDetectionIntervalTest, Dot11LocationServicesTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11LocationServicesTableINDEX, 1, 0, 0, NULL},

{{8,Dot11LocationServicesLocationIndicationChannelList}, GetNextIndexDot11LocationServicesTable, Dot11LocationServicesLocationIndicationChannelListGet, Dot11LocationServicesLocationIndicationChannelListSet, Dot11LocationServicesLocationIndicationChannelListTest, Dot11LocationServicesTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11LocationServicesTableINDEX, 1, 0, 0, NULL},

{{8,Dot11LocationServicesLocationIndicationBroadcastDataRate}, GetNextIndexDot11LocationServicesTable, Dot11LocationServicesLocationIndicationBroadcastDataRateGet, Dot11LocationServicesLocationIndicationBroadcastDataRateSet, Dot11LocationServicesLocationIndicationBroadcastDataRateTest, Dot11LocationServicesTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11LocationServicesTableINDEX, 1, 0, 0, "0"},

{{8,Dot11LocationServicesLocationIndicationOptionsUsed}, GetNextIndexDot11LocationServicesTable, Dot11LocationServicesLocationIndicationOptionsUsedGet, Dot11LocationServicesLocationIndicationOptionsUsedSet, Dot11LocationServicesLocationIndicationOptionsUsedTest, Dot11LocationServicesTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11LocationServicesTableINDEX, 1, 0, 0, NULL},

{{8,Dot11LocationServicesLocationIndicationIndicationParameters}, GetNextIndexDot11LocationServicesTable, Dot11LocationServicesLocationIndicationIndicationParametersGet, Dot11LocationServicesLocationIndicationIndicationParametersSet, Dot11LocationServicesLocationIndicationIndicationParametersTest, Dot11LocationServicesTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11LocationServicesTableINDEX, 1, 0, 0, NULL},

{{8,Dot11LocationServicesLocationStatus}, GetNextIndexDot11LocationServicesTable, Dot11LocationServicesLocationStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11LocationServicesTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11LocationServicesTableEntry = { 16, Dot11LocationServicesTableMibEntry };

tMbDbEntry Dot11WirelessMGTEventTableMibEntry[]= {

{{8,Dot11WirelessMGTEventIndex}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventMACAddress}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventMACAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventType}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventStatus}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventTSF}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventTSFGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventUTCOffset}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventUTCOffsetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventTimeError}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventTimeErrorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventTransitionSourceBSSID}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventTransitionSourceBSSIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventTransitionTargetBSSID}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventTransitionTargetBSSIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventTransitionTime}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventTransitionTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventTransitionReason}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventTransitionReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventTransitionResult}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventTransitionResultGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventTransitionSourceRCPI}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventTransitionSourceRCPIGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventTransitionSourceRSNI}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventTransitionSourceRSNIGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventTransitionTargetRCPI}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventTransitionTargetRCPIGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventTransitionTargetRSNI}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventTransitionTargetRSNIGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventRSNATargetBSSID}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventRSNATargetBSSIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventRSNAAuthenticationType}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventRSNAAuthenticationTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventRSNAEAPMethod}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventRSNAEAPMethodGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventRSNAResult}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventRSNAResultGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventRSNARSNElement}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventRSNARSNElementGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventPeerSTAAddress}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventPeerSTAAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventPeerOperatingClass}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventPeerOperatingClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventPeerChannelNumber}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventPeerChannelNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventPeerSTATxPower}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventPeerSTATxPowerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventPeerConnectionTime}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventPeerConnectionTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventPeerPeerStatus}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventPeerPeerStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WirelessMGTEventWNMLog}, GetNextIndexDot11WirelessMGTEventTable, Dot11WirelessMGTEventWNMLogGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WirelessMGTEventTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11WirelessMGTEventTableEntry = { 28, Dot11WirelessMGTEventTableMibEntry };

tMbDbEntry Dot11WNMRequestNextIndexMibEntry[]= {

{{8,Dot11WNMRequestNextIndex}, NULL, Dot11WNMRequestNextIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData Dot11WNMRequestNextIndexEntry = { 1, Dot11WNMRequestNextIndexMibEntry };

tMbDbEntry Dot11WNMRequestTableMibEntry[]= {

{{10,Dot11WNMRqstIndex}, GetNextIndexDot11WNMRequestTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstRowStatus}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstRowStatusGet, Dot11WNMRqstRowStatusSet, Dot11WNMRqstRowStatusTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 1, NULL},

{{10,Dot11WNMRqstToken}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstTokenGet, Dot11WNMRqstTokenSet, Dot11WNMRqstTokenTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstIfIndex}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstIfIndexGet, Dot11WNMRqstIfIndexSet, Dot11WNMRqstIfIndexTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstType}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstTypeGet, Dot11WNMRqstTypeSet, Dot11WNMRqstTypeTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstTargetAdd}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstTargetAddGet, Dot11WNMRqstTargetAddSet, Dot11WNMRqstTargetAddTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstTimeStamp}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstTimeStampGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstRndInterval}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstRndIntervalGet, Dot11WNMRqstRndIntervalSet, Dot11WNMRqstRndIntervalTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11WNMRqstDuration}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstDurationGet, Dot11WNMRqstDurationSet, Dot11WNMRqstDurationTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11WNMRqstMcstGroup}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstMcstGroupGet, Dot11WNMRqstMcstGroupSet, Dot11WNMRqstMcstGroupTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, "FFFFFFFFFFFF"},

{{10,Dot11WNMRqstMcstTrigCon}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstMcstTrigConGet, Dot11WNMRqstMcstTrigConSet, Dot11WNMRqstMcstTrigConTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstMcstTrigInactivityTimeout}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstMcstTrigInactivityTimeoutGet, Dot11WNMRqstMcstTrigInactivityTimeoutSet, Dot11WNMRqstMcstTrigInactivityTimeoutTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstMcstTrigReactDelay}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstMcstTrigReactDelayGet, Dot11WNMRqstMcstTrigReactDelaySet, Dot11WNMRqstMcstTrigReactDelayTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstLCRRqstSubject}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstLCRRqstSubjectGet, Dot11WNMRqstLCRRqstSubjectSet, Dot11WNMRqstLCRRqstSubjectTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11WNMRqstLCRIntervalUnits}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstLCRIntervalUnitsGet, Dot11WNMRqstLCRIntervalUnitsSet, Dot11WNMRqstLCRIntervalUnitsTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstLCRServiceInterval}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstLCRServiceIntervalGet, Dot11WNMRqstLCRServiceIntervalSet, Dot11WNMRqstLCRServiceIntervalTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstLIRRqstSubject}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstLIRRqstSubjectGet, Dot11WNMRqstLIRRqstSubjectSet, Dot11WNMRqstLIRRqstSubjectTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11WNMRqstLIRIntervalUnits}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstLIRIntervalUnitsGet, Dot11WNMRqstLIRIntervalUnitsSet, Dot11WNMRqstLIRIntervalUnitsTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstLIRServiceInterval}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstLIRServiceIntervalGet, Dot11WNMRqstLIRServiceIntervalSet, Dot11WNMRqstLIRServiceIntervalTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstEventToken}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstEventTokenGet, Dot11WNMRqstEventTokenSet, Dot11WNMRqstEventTokenTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstEventType}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstEventTypeGet, Dot11WNMRqstEventTypeSet, Dot11WNMRqstEventTypeTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstEventResponseLimit}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstEventResponseLimitGet, Dot11WNMRqstEventResponseLimitSet, Dot11WNMRqstEventResponseLimitTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstEventTargetBssid}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstEventTargetBssidGet, Dot11WNMRqstEventTargetBssidSet, Dot11WNMRqstEventTargetBssidTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, "FFFFFFFFFFFF"},

{{10,Dot11WNMRqstEventSourceBssid}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstEventSourceBssidGet, Dot11WNMRqstEventSourceBssidSet, Dot11WNMRqstEventSourceBssidTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, "FFFFFFFFFFFF"},

{{10,Dot11WNMRqstEventTransitTimeThresh}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstEventTransitTimeThreshGet, Dot11WNMRqstEventTransitTimeThreshSet, Dot11WNMRqstEventTransitTimeThreshTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstEventTransitMatchValue}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstEventTransitMatchValueGet, Dot11WNMRqstEventTransitMatchValueSet, Dot11WNMRqstEventTransitMatchValueTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstEventFreqTransitCountThresh}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstEventFreqTransitCountThreshGet, Dot11WNMRqstEventFreqTransitCountThreshSet, Dot11WNMRqstEventFreqTransitCountThreshTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstEventFreqTransitInterval}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstEventFreqTransitIntervalGet, Dot11WNMRqstEventFreqTransitIntervalSet, Dot11WNMRqstEventFreqTransitIntervalTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstEventRsnaAuthType}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstEventRsnaAuthTypeGet, Dot11WNMRqstEventRsnaAuthTypeSet, Dot11WNMRqstEventRsnaAuthTypeTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstEapType}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstEapTypeGet, Dot11WNMRqstEapTypeSet, Dot11WNMRqstEapTypeTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstEapVendorId}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstEapVendorIdGet, Dot11WNMRqstEapVendorIdSet, Dot11WNMRqstEapVendorIdTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstEapVendorType}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstEapVendorTypeGet, Dot11WNMRqstEapVendorTypeSet, Dot11WNMRqstEapVendorTypeTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstEventRsnaMatchValue}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstEventRsnaMatchValueGet, Dot11WNMRqstEventRsnaMatchValueSet, Dot11WNMRqstEventRsnaMatchValueTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstEventPeerMacAddress}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstEventPeerMacAddressGet, Dot11WNMRqstEventPeerMacAddressSet, Dot11WNMRqstEventPeerMacAddressTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, "FFFFFFFFFFFF"},

{{10,Dot11WNMRqstOperatingClass}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstOperatingClassGet, Dot11WNMRqstOperatingClassSet, Dot11WNMRqstOperatingClassTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstChanNumber}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstChanNumberGet, Dot11WNMRqstChanNumberSet, Dot11WNMRqstChanNumberTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstDiagToken}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstDiagTokenGet, Dot11WNMRqstDiagTokenSet, Dot11WNMRqstDiagTokenTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstDiagType}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstDiagTypeGet, Dot11WNMRqstDiagTypeSet, Dot11WNMRqstDiagTypeTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstDiagTimeout}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstDiagTimeoutGet, Dot11WNMRqstDiagTimeoutSet, Dot11WNMRqstDiagTimeoutTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstDiagBssid}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstDiagBssidGet, Dot11WNMRqstDiagBssidSet, Dot11WNMRqstDiagBssidTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, "FFFFFFFFFFFF"},

{{10,Dot11WNMRqstDiagProfileId}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstDiagProfileIdGet, Dot11WNMRqstDiagProfileIdSet, Dot11WNMRqstDiagProfileIdTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstDiagCredentials}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstDiagCredentialsGet, Dot11WNMRqstDiagCredentialsSet, Dot11WNMRqstDiagCredentialsTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstLocConfigLocIndParams}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstLocConfigLocIndParamsGet, Dot11WNMRqstLocConfigLocIndParamsSet, Dot11WNMRqstLocConfigLocIndParamsTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstLocConfigChanList}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstLocConfigChanListGet, Dot11WNMRqstLocConfigChanListSet, Dot11WNMRqstLocConfigChanListTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11WNMRqstLocConfigBcastRate}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstLocConfigBcastRateGet, Dot11WNMRqstLocConfigBcastRateSet, Dot11WNMRqstLocConfigBcastRateTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstLocConfigOptions}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstLocConfigOptionsGet, Dot11WNMRqstLocConfigOptionsSet, Dot11WNMRqstLocConfigOptionsTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11WNMRqstBssTransitQueryReason}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstBssTransitQueryReasonGet, Dot11WNMRqstBssTransitQueryReasonSet, Dot11WNMRqstBssTransitQueryReasonTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstBssTransitReqMode}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstBssTransitReqModeGet, Dot11WNMRqstBssTransitReqModeSet, Dot11WNMRqstBssTransitReqModeTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstBssTransitDisocTimer}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstBssTransitDisocTimerGet, Dot11WNMRqstBssTransitDisocTimerSet, Dot11WNMRqstBssTransitDisocTimerTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstBssTransitSessInfoURL}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstBssTransitSessInfoURLGet, Dot11WNMRqstBssTransitSessInfoURLSet, Dot11WNMRqstBssTransitSessInfoURLTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstBssTransitCandidateList}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstBssTransitCandidateListGet, Dot11WNMRqstBssTransitCandidateListSet, Dot11WNMRqstBssTransitCandidateListTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstColocInterfAutoEnable}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstColocInterfAutoEnableGet, Dot11WNMRqstColocInterfAutoEnableSet, Dot11WNMRqstColocInterfAutoEnableTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstColocInterfRptTimeout}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstColocInterfRptTimeoutGet, Dot11WNMRqstColocInterfRptTimeoutSet, Dot11WNMRqstColocInterfRptTimeoutTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMRqstVendorSpecific}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstVendorSpecificGet, Dot11WNMRqstVendorSpecificSet, Dot11WNMRqstVendorSpecificTest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, "0"},

{{10,Dot11WNMRqstDestinationURI}, GetNextIndexDot11WNMRequestTable, Dot11WNMRqstDestinationURIGet, Dot11WNMRqstDestinationURISet, Dot11WNMRqstDestinationURITest, Dot11WNMRequestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11WNMRequestTableINDEX, 1, 0, 0, "0"},
};
tMibData Dot11WNMRequestTableEntry = { 55, Dot11WNMRequestTableMibEntry };

tMbDbEntry Dot11WNMVendorSpecificReportTableMibEntry[]= {

{{10,Dot11WNMVendorSpecificRprtIndex}, GetNextIndexDot11WNMVendorSpecificReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11WNMVendorSpecificReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMVendorSpecificRprtRqstToken}, GetNextIndexDot11WNMVendorSpecificReportTable, Dot11WNMVendorSpecificRprtRqstTokenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMVendorSpecificReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMVendorSpecificRprtIfIndex}, GetNextIndexDot11WNMVendorSpecificReportTable, Dot11WNMVendorSpecificRprtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMVendorSpecificReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMVendorSpecificRprtContent}, GetNextIndexDot11WNMVendorSpecificReportTable, Dot11WNMVendorSpecificRprtContentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMVendorSpecificReportTableINDEX, 1, 0, 0, "0"},
};
tMibData Dot11WNMVendorSpecificReportTableEntry = { 4, Dot11WNMVendorSpecificReportTableMibEntry };

tMbDbEntry Dot11WNMMulticastDiagnosticReportTableMibEntry[]= {

{{10,Dot11WNMMulticastDiagnosticRprtIndex}, GetNextIndexDot11WNMMulticastDiagnosticReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11WNMMulticastDiagnosticReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMMulticastDiagnosticRprtRqstToken}, GetNextIndexDot11WNMMulticastDiagnosticReportTable, Dot11WNMMulticastDiagnosticRprtRqstTokenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMMulticastDiagnosticReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMMulticastDiagnosticRprtIfIndex}, GetNextIndexDot11WNMMulticastDiagnosticReportTable, Dot11WNMMulticastDiagnosticRprtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMMulticastDiagnosticReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMMulticastDiagnosticRprtMeasurementTime}, GetNextIndexDot11WNMMulticastDiagnosticReportTable, Dot11WNMMulticastDiagnosticRprtMeasurementTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMMulticastDiagnosticReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMMulticastDiagnosticRprtDuration}, GetNextIndexDot11WNMMulticastDiagnosticReportTable, Dot11WNMMulticastDiagnosticRprtDurationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMMulticastDiagnosticReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMMulticastDiagnosticRprtMcstGroup}, GetNextIndexDot11WNMMulticastDiagnosticReportTable, Dot11WNMMulticastDiagnosticRprtMcstGroupGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11WNMMulticastDiagnosticReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMMulticastDiagnosticRprtReason}, GetNextIndexDot11WNMMulticastDiagnosticReportTable, Dot11WNMMulticastDiagnosticRprtReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMMulticastDiagnosticReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMMulticastDiagnosticRprtRcvdMsduCount}, GetNextIndexDot11WNMMulticastDiagnosticReportTable, Dot11WNMMulticastDiagnosticRprtRcvdMsduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMMulticastDiagnosticReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMMulticastDiagnosticRprtFirstSeqNumber}, GetNextIndexDot11WNMMulticastDiagnosticReportTable, Dot11WNMMulticastDiagnosticRprtFirstSeqNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMMulticastDiagnosticReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMMulticastDiagnosticRprtLastSeqNumber}, GetNextIndexDot11WNMMulticastDiagnosticReportTable, Dot11WNMMulticastDiagnosticRprtLastSeqNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMMulticastDiagnosticReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMMulticastDiagnosticRprtMcstRate}, GetNextIndexDot11WNMMulticastDiagnosticReportTable, Dot11WNMMulticastDiagnosticRprtMcstRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMMulticastDiagnosticReportTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11WNMMulticastDiagnosticReportTableEntry = { 11, Dot11WNMMulticastDiagnosticReportTableMibEntry };

tMbDbEntry Dot11WNMLocationCivicReportTableMibEntry[]= {

{{10,Dot11WNMLocationCivicRprtIndex}, GetNextIndexDot11WNMLocationCivicReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11WNMLocationCivicReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMLocationCivicRprtRqstToken}, GetNextIndexDot11WNMLocationCivicReportTable, Dot11WNMLocationCivicRprtRqstTokenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMLocationCivicReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMLocationCivicRprtIfIndex}, GetNextIndexDot11WNMLocationCivicReportTable, Dot11WNMLocationCivicRprtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMLocationCivicReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMLocationCivicRprtCivicLocation}, GetNextIndexDot11WNMLocationCivicReportTable, Dot11WNMLocationCivicRprtCivicLocationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMLocationCivicReportTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11WNMLocationCivicReportTableEntry = { 4, Dot11WNMLocationCivicReportTableMibEntry };

tMbDbEntry Dot11WNMLocationIdentifierReportTableMibEntry[]= {

{{10,Dot11WNMLocationIdentifierRprtIndex}, GetNextIndexDot11WNMLocationIdentifierReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11WNMLocationIdentifierReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMLocationIdentifierRprtRqstToken}, GetNextIndexDot11WNMLocationIdentifierReportTable, Dot11WNMLocationIdentifierRprtRqstTokenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMLocationIdentifierReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMLocationIdentifierRprtIfIndex}, GetNextIndexDot11WNMLocationIdentifierReportTable, Dot11WNMLocationIdentifierRprtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMLocationIdentifierReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMLocationIdentifierRprtExpirationTSF}, GetNextIndexDot11WNMLocationIdentifierReportTable, Dot11WNMLocationIdentifierRprtExpirationTSFGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMLocationIdentifierReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMLocationIdentifierRprtPublicIdUri}, GetNextIndexDot11WNMLocationIdentifierReportTable, Dot11WNMLocationIdentifierRprtPublicIdUriGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMLocationIdentifierReportTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11WNMLocationIdentifierReportTableEntry = { 5, Dot11WNMLocationIdentifierReportTableMibEntry };

tMbDbEntry Dot11WNMEventTransitReportTableMibEntry[]= {

{{10,Dot11WNMEventTransitRprtIndex}, GetNextIndexDot11WNMEventTransitReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11WNMEventTransitReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventTransitRprtRqstToken}, GetNextIndexDot11WNMEventTransitReportTable, Dot11WNMEventTransitRprtRqstTokenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMEventTransitReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventTransitRprtIfIndex}, GetNextIndexDot11WNMEventTransitReportTable, Dot11WNMEventTransitRprtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMEventTransitReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventTransitRprtEventStatus}, GetNextIndexDot11WNMEventTransitReportTable, Dot11WNMEventTransitRprtEventStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMEventTransitReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventTransitRprtEventTSF}, GetNextIndexDot11WNMEventTransitReportTable, Dot11WNMEventTransitRprtEventTSFGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMEventTransitReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventTransitRprtUTCOffset}, GetNextIndexDot11WNMEventTransitReportTable, Dot11WNMEventTransitRprtUTCOffsetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMEventTransitReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventTransitRprtTimeError}, GetNextIndexDot11WNMEventTransitReportTable, Dot11WNMEventTransitRprtTimeErrorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMEventTransitReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventTransitRprtSourceBssid}, GetNextIndexDot11WNMEventTransitReportTable, Dot11WNMEventTransitRprtSourceBssidGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11WNMEventTransitReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventTransitRprtTargetBssid}, GetNextIndexDot11WNMEventTransitReportTable, Dot11WNMEventTransitRprtTargetBssidGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11WNMEventTransitReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventTransitRprtTransitTime}, GetNextIndexDot11WNMEventTransitReportTable, Dot11WNMEventTransitRprtTransitTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMEventTransitReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventTransitRprtTransitReason}, GetNextIndexDot11WNMEventTransitReportTable, Dot11WNMEventTransitRprtTransitReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMEventTransitReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventTransitRprtTransitResult}, GetNextIndexDot11WNMEventTransitReportTable, Dot11WNMEventTransitRprtTransitResultGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMEventTransitReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventTransitRprtSourceRCPI}, GetNextIndexDot11WNMEventTransitReportTable, Dot11WNMEventTransitRprtSourceRCPIGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMEventTransitReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventTransitRprtSourceRSNI}, GetNextIndexDot11WNMEventTransitReportTable, Dot11WNMEventTransitRprtSourceRSNIGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMEventTransitReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventTransitRprtTargetRCPI}, GetNextIndexDot11WNMEventTransitReportTable, Dot11WNMEventTransitRprtTargetRCPIGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMEventTransitReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventTransitRprtTargetRSNI}, GetNextIndexDot11WNMEventTransitReportTable, Dot11WNMEventTransitRprtTargetRSNIGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMEventTransitReportTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11WNMEventTransitReportTableEntry = { 16, Dot11WNMEventTransitReportTableMibEntry };

tMbDbEntry Dot11WNMEventRsnaReportTableMibEntry[]= {

{{10,Dot11WNMEventRsnaRprtIndex}, GetNextIndexDot11WNMEventRsnaReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11WNMEventRsnaReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventRsnaRprtRqstToken}, GetNextIndexDot11WNMEventRsnaReportTable, Dot11WNMEventRsnaRprtRqstTokenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMEventRsnaReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventRsnaRprtIfIndex}, GetNextIndexDot11WNMEventRsnaReportTable, Dot11WNMEventRsnaRprtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMEventRsnaReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventRsnaRprtEventStatus}, GetNextIndexDot11WNMEventRsnaReportTable, Dot11WNMEventRsnaRprtEventStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMEventRsnaReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventRsnaRprtEventTSF}, GetNextIndexDot11WNMEventRsnaReportTable, Dot11WNMEventRsnaRprtEventTSFGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMEventRsnaReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventRsnaRprtUTCOffset}, GetNextIndexDot11WNMEventRsnaReportTable, Dot11WNMEventRsnaRprtUTCOffsetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMEventRsnaReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventRsnaRprtTimeError}, GetNextIndexDot11WNMEventRsnaReportTable, Dot11WNMEventRsnaRprtTimeErrorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMEventRsnaReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventRsnaRprtTargetBssid}, GetNextIndexDot11WNMEventRsnaReportTable, Dot11WNMEventRsnaRprtTargetBssidGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11WNMEventRsnaReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventRsnaRprtAuthType}, GetNextIndexDot11WNMEventRsnaReportTable, Dot11WNMEventRsnaRprtAuthTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMEventRsnaReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventRsnaRprtEapMethod}, GetNextIndexDot11WNMEventRsnaReportTable, Dot11WNMEventRsnaRprtEapMethodGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMEventRsnaReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventRsnaRprtResult}, GetNextIndexDot11WNMEventRsnaReportTable, Dot11WNMEventRsnaRprtResultGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMEventRsnaReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventRsnaRprtRsnElement}, GetNextIndexDot11WNMEventRsnaReportTable, Dot11WNMEventRsnaRprtRsnElementGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMEventRsnaReportTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11WNMEventRsnaReportTableEntry = { 12, Dot11WNMEventRsnaReportTableMibEntry };

tMbDbEntry Dot11WNMEventPeerReportTableMibEntry[]= {

{{10,Dot11WNMEventPeerRprtIndex}, GetNextIndexDot11WNMEventPeerReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11WNMEventPeerReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventPeerRprtRqstToken}, GetNextIndexDot11WNMEventPeerReportTable, Dot11WNMEventPeerRprtRqstTokenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMEventPeerReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventPeerRprtIfIndex}, GetNextIndexDot11WNMEventPeerReportTable, Dot11WNMEventPeerRprtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMEventPeerReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventPeerRprtEventStatus}, GetNextIndexDot11WNMEventPeerReportTable, Dot11WNMEventPeerRprtEventStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMEventPeerReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventPeerRprtEventTSF}, GetNextIndexDot11WNMEventPeerReportTable, Dot11WNMEventPeerRprtEventTSFGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMEventPeerReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventPeerRprtUTCOffset}, GetNextIndexDot11WNMEventPeerReportTable, Dot11WNMEventPeerRprtUTCOffsetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMEventPeerReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventPeerRprtTimeError}, GetNextIndexDot11WNMEventPeerReportTable, Dot11WNMEventPeerRprtTimeErrorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMEventPeerReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventPeerRprtPeerMacAddress}, GetNextIndexDot11WNMEventPeerReportTable, Dot11WNMEventPeerRprtPeerMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11WNMEventPeerReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventPeerRprtOperatingClass}, GetNextIndexDot11WNMEventPeerReportTable, Dot11WNMEventPeerRprtOperatingClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMEventPeerReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventPeerRprtChanNumber}, GetNextIndexDot11WNMEventPeerReportTable, Dot11WNMEventPeerRprtChanNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMEventPeerReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventPeerRprtStaTxPower}, GetNextIndexDot11WNMEventPeerReportTable, Dot11WNMEventPeerRprtStaTxPowerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11WNMEventPeerReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventPeerRprtConnTime}, GetNextIndexDot11WNMEventPeerReportTable, Dot11WNMEventPeerRprtConnTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMEventPeerReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventPeerRprtPeerStatus}, GetNextIndexDot11WNMEventPeerReportTable, Dot11WNMEventPeerRprtPeerStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMEventPeerReportTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11WNMEventPeerReportTableEntry = { 13, Dot11WNMEventPeerReportTableMibEntry };

tMbDbEntry Dot11WNMEventWNMLogReportTableMibEntry[]= {

{{10,Dot11WNMEventWNMLogRprtIndex}, GetNextIndexDot11WNMEventWNMLogReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11WNMEventWNMLogReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventWNMLogRprtRqstToken}, GetNextIndexDot11WNMEventWNMLogReportTable, Dot11WNMEventWNMLogRprtRqstTokenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMEventWNMLogReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventWNMLogRprtIfIndex}, GetNextIndexDot11WNMEventWNMLogReportTable, Dot11WNMEventWNMLogRprtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMEventWNMLogReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventWNMLogRprtEventStatus}, GetNextIndexDot11WNMEventWNMLogReportTable, Dot11WNMEventWNMLogRprtEventStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMEventWNMLogReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventWNMLogRprtEventTSF}, GetNextIndexDot11WNMEventWNMLogReportTable, Dot11WNMEventWNMLogRprtEventTSFGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMEventWNMLogReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventWNMLogRprtUTCOffset}, GetNextIndexDot11WNMEventWNMLogReportTable, Dot11WNMEventWNMLogRprtUTCOffsetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMEventWNMLogReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventWNMLogRprtTimeError}, GetNextIndexDot11WNMEventWNMLogReportTable, Dot11WNMEventWNMLogRprtTimeErrorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMEventWNMLogReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMEventWNMLogRprtContent}, GetNextIndexDot11WNMEventWNMLogReportTable, Dot11WNMEventWNMLogRprtContentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMEventWNMLogReportTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11WNMEventWNMLogReportTableEntry = { 8, Dot11WNMEventWNMLogReportTableMibEntry };

tMbDbEntry Dot11WNMDiagMfrInfoReportTableMibEntry[]= {

{{10,Dot11WNMDiagMfrInfoRprtIndex}, GetNextIndexDot11WNMDiagMfrInfoReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11WNMDiagMfrInfoReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagMfrInfoRprtRqstToken}, GetNextIndexDot11WNMDiagMfrInfoReportTable, Dot11WNMDiagMfrInfoRprtRqstTokenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMDiagMfrInfoReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagMfrInfoRprtIfIndex}, GetNextIndexDot11WNMDiagMfrInfoReportTable, Dot11WNMDiagMfrInfoRprtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMDiagMfrInfoReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagMfrInfoRprtEventStatus}, GetNextIndexDot11WNMDiagMfrInfoReportTable, Dot11WNMDiagMfrInfoRprtEventStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMDiagMfrInfoReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagMfrInfoRprtMfrOi}, GetNextIndexDot11WNMDiagMfrInfoReportTable, Dot11WNMDiagMfrInfoRprtMfrOiGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMDiagMfrInfoReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagMfrInfoRprtMfrIdString}, GetNextIndexDot11WNMDiagMfrInfoReportTable, Dot11WNMDiagMfrInfoRprtMfrIdStringGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMDiagMfrInfoReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagMfrInfoRprtMfrModelString}, GetNextIndexDot11WNMDiagMfrInfoReportTable, Dot11WNMDiagMfrInfoRprtMfrModelStringGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMDiagMfrInfoReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagMfrInfoRprtMfrSerialNumberString}, GetNextIndexDot11WNMDiagMfrInfoReportTable, Dot11WNMDiagMfrInfoRprtMfrSerialNumberStringGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMDiagMfrInfoReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagMfrInfoRprtMfrFirmwareVersion}, GetNextIndexDot11WNMDiagMfrInfoReportTable, Dot11WNMDiagMfrInfoRprtMfrFirmwareVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMDiagMfrInfoReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagMfrInfoRprtMfrAntennaType}, GetNextIndexDot11WNMDiagMfrInfoReportTable, Dot11WNMDiagMfrInfoRprtMfrAntennaTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMDiagMfrInfoReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagMfrInfoRprtCollocRadioType}, GetNextIndexDot11WNMDiagMfrInfoReportTable, Dot11WNMDiagMfrInfoRprtCollocRadioTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMDiagMfrInfoReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagMfrInfoRprtDeviceType}, GetNextIndexDot11WNMDiagMfrInfoReportTable, Dot11WNMDiagMfrInfoRprtDeviceTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMDiagMfrInfoReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagMfrInfoRprtCertificateID}, GetNextIndexDot11WNMDiagMfrInfoReportTable, Dot11WNMDiagMfrInfoRprtCertificateIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMDiagMfrInfoReportTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11WNMDiagMfrInfoReportTableEntry = { 13, Dot11WNMDiagMfrInfoReportTableMibEntry };

tMbDbEntry Dot11WNMDiagConfigProfReportTableMibEntry[]= {

{{10,Dot11WNMDiagConfigProfRprtIndex}, GetNextIndexDot11WNMDiagConfigProfReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11WNMDiagConfigProfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagConfigProfRprtRqstToken}, GetNextIndexDot11WNMDiagConfigProfReportTable, Dot11WNMDiagConfigProfRprtRqstTokenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMDiagConfigProfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagConfigProfRprtIfIndex}, GetNextIndexDot11WNMDiagConfigProfReportTable, Dot11WNMDiagConfigProfRprtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMDiagConfigProfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagConfigProfRprtEventStatus}, GetNextIndexDot11WNMDiagConfigProfReportTable, Dot11WNMDiagConfigProfRprtEventStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMDiagConfigProfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagConfigProfRprtProfileId}, GetNextIndexDot11WNMDiagConfigProfReportTable, Dot11WNMDiagConfigProfRprtProfileIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMDiagConfigProfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagConfigProfRprtSupportedOperatingClasses}, GetNextIndexDot11WNMDiagConfigProfReportTable, Dot11WNMDiagConfigProfRprtSupportedOperatingClassesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMDiagConfigProfReportTableINDEX, 1, 0, 0, "0"},

{{10,Dot11WNMDiagConfigProfRprtTxPowerMode}, GetNextIndexDot11WNMDiagConfigProfReportTable, Dot11WNMDiagConfigProfRprtTxPowerModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMDiagConfigProfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagConfigProfRprtTxPowerLevels}, GetNextIndexDot11WNMDiagConfigProfReportTable, Dot11WNMDiagConfigProfRprtTxPowerLevelsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMDiagConfigProfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagConfigProfRprtCipherSuite}, GetNextIndexDot11WNMDiagConfigProfReportTable, Dot11WNMDiagConfigProfRprtCipherSuiteGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMDiagConfigProfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagConfigProfRprtAkmSuite}, GetNextIndexDot11WNMDiagConfigProfReportTable, Dot11WNMDiagConfigProfRprtAkmSuiteGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMDiagConfigProfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagConfigProfRprtEapType}, GetNextIndexDot11WNMDiagConfigProfReportTable, Dot11WNMDiagConfigProfRprtEapTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMDiagConfigProfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagConfigProfRprtEapVendorID}, GetNextIndexDot11WNMDiagConfigProfReportTable, Dot11WNMDiagConfigProfRprtEapVendorIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMDiagConfigProfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagConfigProfRprtEapVendorType}, GetNextIndexDot11WNMDiagConfigProfReportTable, Dot11WNMDiagConfigProfRprtEapVendorTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMDiagConfigProfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagConfigProfRprtCredentialType}, GetNextIndexDot11WNMDiagConfigProfReportTable, Dot11WNMDiagConfigProfRprtCredentialTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMDiagConfigProfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagConfigProfRprtSSID}, GetNextIndexDot11WNMDiagConfigProfReportTable, Dot11WNMDiagConfigProfRprtSSIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMDiagConfigProfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagConfigProfRprtPowerSaveMode}, GetNextIndexDot11WNMDiagConfigProfReportTable, Dot11WNMDiagConfigProfRprtPowerSaveModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMDiagConfigProfReportTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11WNMDiagConfigProfReportTableEntry = { 16, Dot11WNMDiagConfigProfReportTableMibEntry };

tMbDbEntry Dot11WNMDiagAssocReportTableMibEntry[]= {

{{10,Dot11WNMDiagAssocRprtIndex}, GetNextIndexDot11WNMDiagAssocReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11WNMDiagAssocReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagAssocRprtRqstToken}, GetNextIndexDot11WNMDiagAssocReportTable, Dot11WNMDiagAssocRprtRqstTokenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMDiagAssocReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagAssocRprtIfIndex}, GetNextIndexDot11WNMDiagAssocReportTable, Dot11WNMDiagAssocRprtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMDiagAssocReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagAssocRprtEventStatus}, GetNextIndexDot11WNMDiagAssocReportTable, Dot11WNMDiagAssocRprtEventStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMDiagAssocReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagAssocRprtBssid}, GetNextIndexDot11WNMDiagAssocReportTable, Dot11WNMDiagAssocRprtBssidGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11WNMDiagAssocReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagAssocRprtOperatingClass}, GetNextIndexDot11WNMDiagAssocReportTable, Dot11WNMDiagAssocRprtOperatingClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMDiagAssocReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagAssocRprtChannelNumber}, GetNextIndexDot11WNMDiagAssocReportTable, Dot11WNMDiagAssocRprtChannelNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMDiagAssocReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiagAssocRprtStatusCode}, GetNextIndexDot11WNMDiagAssocReportTable, Dot11WNMDiagAssocRprtStatusCodeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMDiagAssocReportTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11WNMDiagAssocReportTableEntry = { 8, Dot11WNMDiagAssocReportTableMibEntry };

tMbDbEntry Dot11WNMDiag8021xAuthReportTableMibEntry[]= {

{{10,Dot11WNMDiag8021xAuthRprtIndex}, GetNextIndexDot11WNMDiag8021xAuthReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11WNMDiag8021xAuthReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiag8021xAuthRprtRqstToken}, GetNextIndexDot11WNMDiag8021xAuthReportTable, Dot11WNMDiag8021xAuthRprtRqstTokenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMDiag8021xAuthReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiag8021xAuthRprtIfIndex}, GetNextIndexDot11WNMDiag8021xAuthReportTable, Dot11WNMDiag8021xAuthRprtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMDiag8021xAuthReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiag8021xAuthRprtEventStatus}, GetNextIndexDot11WNMDiag8021xAuthReportTable, Dot11WNMDiag8021xAuthRprtEventStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMDiag8021xAuthReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiag8021xAuthRprtBssid}, GetNextIndexDot11WNMDiag8021xAuthReportTable, Dot11WNMDiag8021xAuthRprtBssidGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11WNMDiag8021xAuthReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiag8021xAuthRprtOperatingClass}, GetNextIndexDot11WNMDiag8021xAuthReportTable, Dot11WNMDiag8021xAuthRprtOperatingClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMDiag8021xAuthReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiag8021xAuthRprtChannelNumber}, GetNextIndexDot11WNMDiag8021xAuthReportTable, Dot11WNMDiag8021xAuthRprtChannelNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMDiag8021xAuthReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiag8021xAuthRprtEapType}, GetNextIndexDot11WNMDiag8021xAuthReportTable, Dot11WNMDiag8021xAuthRprtEapTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMDiag8021xAuthReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiag8021xAuthRprtEapVendorID}, GetNextIndexDot11WNMDiag8021xAuthReportTable, Dot11WNMDiag8021xAuthRprtEapVendorIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMDiag8021xAuthReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiag8021xAuthRprtEapVendorType}, GetNextIndexDot11WNMDiag8021xAuthReportTable, Dot11WNMDiag8021xAuthRprtEapVendorTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMDiag8021xAuthReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiag8021xAuthRprtCredentialType}, GetNextIndexDot11WNMDiag8021xAuthReportTable, Dot11WNMDiag8021xAuthRprtCredentialTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMDiag8021xAuthReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMDiag8021xAuthRprtStatusCode}, GetNextIndexDot11WNMDiag8021xAuthReportTable, Dot11WNMDiag8021xAuthRprtStatusCodeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMDiag8021xAuthReportTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11WNMDiag8021xAuthReportTableEntry = { 12, Dot11WNMDiag8021xAuthReportTableMibEntry };

tMbDbEntry Dot11WNMLocConfigReportTableMibEntry[]= {

{{10,Dot11WNMLocConfigRprtIndex}, GetNextIndexDot11WNMLocConfigReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11WNMLocConfigReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMLocConfigRprtRqstToken}, GetNextIndexDot11WNMLocConfigReportTable, Dot11WNMLocConfigRprtRqstTokenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMLocConfigReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMLocConfigRprtIfIndex}, GetNextIndexDot11WNMLocConfigReportTable, Dot11WNMLocConfigRprtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMLocConfigReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMLocConfigRprtLocIndParams}, GetNextIndexDot11WNMLocConfigReportTable, Dot11WNMLocConfigRprtLocIndParamsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMLocConfigReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMLocConfigRprtLocIndChanList}, GetNextIndexDot11WNMLocConfigReportTable, Dot11WNMLocConfigRprtLocIndChanListGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMLocConfigReportTableINDEX, 1, 0, 0, "0"},

{{10,Dot11WNMLocConfigRprtLocIndBcastRate}, GetNextIndexDot11WNMLocConfigReportTable, Dot11WNMLocConfigRprtLocIndBcastRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMLocConfigReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMLocConfigRprtLocIndOptions}, GetNextIndexDot11WNMLocConfigReportTable, Dot11WNMLocConfigRprtLocIndOptionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMLocConfigReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMLocConfigRprtStatusConfigSubelemId}, GetNextIndexDot11WNMLocConfigReportTable, Dot11WNMLocConfigRprtStatusConfigSubelemIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMLocConfigReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMLocConfigRprtStatusResult}, GetNextIndexDot11WNMLocConfigReportTable, Dot11WNMLocConfigRprtStatusResultGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMLocConfigReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMLocConfigRprtVendorSpecificRprtContent}, GetNextIndexDot11WNMLocConfigReportTable, Dot11WNMLocConfigRprtVendorSpecificRprtContentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMLocConfigReportTableINDEX, 1, 0, 0, "0"},
};
tMibData Dot11WNMLocConfigReportTableEntry = { 10, Dot11WNMLocConfigReportTableMibEntry };

tMbDbEntry Dot11WNMBssTransitReportTableMibEntry[]= {

{{10,Dot11WNMBssTransitRprtIndex}, GetNextIndexDot11WNMBssTransitReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11WNMBssTransitReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMBssTransitRprtRqstToken}, GetNextIndexDot11WNMBssTransitReportTable, Dot11WNMBssTransitRprtRqstTokenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMBssTransitReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMBssTransitRprtIfIndex}, GetNextIndexDot11WNMBssTransitReportTable, Dot11WNMBssTransitRprtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMBssTransitReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMBssTransitRprtStatusCode}, GetNextIndexDot11WNMBssTransitReportTable, Dot11WNMBssTransitRprtStatusCodeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMBssTransitReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMBssTransitRprtBSSTerminationDelay}, GetNextIndexDot11WNMBssTransitReportTable, Dot11WNMBssTransitRprtBSSTerminationDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMBssTransitReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMBssTransitRprtTargetBssid}, GetNextIndexDot11WNMBssTransitReportTable, Dot11WNMBssTransitRprtTargetBssidGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11WNMBssTransitReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMBssTransitRprtCandidateList}, GetNextIndexDot11WNMBssTransitReportTable, Dot11WNMBssTransitRprtCandidateListGet, Dot11WNMBssTransitRprtCandidateListSet, Dot11WNMBssTransitRprtCandidateListTest, Dot11WNMBssTransitReportTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11WNMBssTransitReportTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11WNMBssTransitReportTableEntry = { 7, Dot11WNMBssTransitReportTableMibEntry };

tMbDbEntry Dot11WNMColocInterfReportTableMibEntry[]= {

{{10,Dot11WNMColocInterfRprtIndex}, GetNextIndexDot11WNMColocInterfReportTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11WNMColocInterfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMColocInterfRprtRqstToken}, GetNextIndexDot11WNMColocInterfReportTable, Dot11WNMColocInterfRprtRqstTokenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11WNMColocInterfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMColocInterfRprtIfIndex}, GetNextIndexDot11WNMColocInterfReportTable, Dot11WNMColocInterfRprtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11WNMColocInterfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMColocInterfRprtPeriod}, GetNextIndexDot11WNMColocInterfReportTable, Dot11WNMColocInterfRprtPeriodGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMColocInterfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMColocInterfRprtInterfLevel}, GetNextIndexDot11WNMColocInterfReportTable, Dot11WNMColocInterfRprtInterfLevelGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11WNMColocInterfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMColocInterfRprtInterfAccuracy}, GetNextIndexDot11WNMColocInterfReportTable, Dot11WNMColocInterfRprtInterfAccuracyGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMColocInterfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMColocInterfRprtInterfIndex}, GetNextIndexDot11WNMColocInterfReportTable, Dot11WNMColocInterfRprtInterfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMColocInterfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMColocInterfRprtInterfInterval}, GetNextIndexDot11WNMColocInterfReportTable, Dot11WNMColocInterfRprtInterfIntervalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11WNMColocInterfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMColocInterfRprtInterfBurstLength}, GetNextIndexDot11WNMColocInterfReportTable, Dot11WNMColocInterfRprtInterfBurstLengthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11WNMColocInterfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMColocInterfRprtInterfStartTime}, GetNextIndexDot11WNMColocInterfReportTable, Dot11WNMColocInterfRprtInterfStartTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11WNMColocInterfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMColocInterfRprtInterfCenterFreq}, GetNextIndexDot11WNMColocInterfReportTable, Dot11WNMColocInterfRprtInterfCenterFreqGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11WNMColocInterfReportTableINDEX, 1, 0, 0, NULL},

{{10,Dot11WNMColocInterfRprtInterfBandwidth}, GetNextIndexDot11WNMColocInterfReportTable, Dot11WNMColocInterfRprtInterfBandwidthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11WNMColocInterfReportTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11WNMColocInterfReportTableEntry = { 12, Dot11WNMColocInterfReportTableMibEntry };

tMbDbEntry Dot11MeshSTAConfigTableMibEntry[]= {

{{8,Dot11MeshID}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshIDGet, Dot11MeshIDSet, Dot11MeshIDTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MeshNumberOfPeerings}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshNumberOfPeeringsGet, Dot11MeshNumberOfPeeringsSet, Dot11MeshNumberOfPeeringsTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MeshAcceptingAdditionalPeerings}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshAcceptingAdditionalPeeringsGet, Dot11MeshAcceptingAdditionalPeeringsSet, Dot11MeshAcceptingAdditionalPeeringsTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MeshConnectedToMeshGate}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshConnectedToMeshGateGet, Dot11MeshConnectedToMeshGateSet, Dot11MeshConnectedToMeshGateTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MeshSecurityActivated}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshSecurityActivatedGet, Dot11MeshSecurityActivatedSet, Dot11MeshSecurityActivatedTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MeshActiveAuthenticationProtocol}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshActiveAuthenticationProtocolGet, Dot11MeshActiveAuthenticationProtocolSet, Dot11MeshActiveAuthenticationProtocolTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "0"},

{{8,Dot11MeshMaxRetries}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshMaxRetriesGet, Dot11MeshMaxRetriesSet, Dot11MeshMaxRetriesTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MeshRetryTimeout}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshRetryTimeoutGet, Dot11MeshRetryTimeoutSet, Dot11MeshRetryTimeoutTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "40"},

{{8,Dot11MeshConfirmTimeout}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshConfirmTimeoutGet, Dot11MeshConfirmTimeoutSet, Dot11MeshConfirmTimeoutTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "40"},

{{8,Dot11MeshHoldingTimeout}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshHoldingTimeoutGet, Dot11MeshHoldingTimeoutSet, Dot11MeshHoldingTimeoutTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "40"},

{{8,Dot11MeshConfigGroupUpdateCount}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshConfigGroupUpdateCountGet, Dot11MeshConfigGroupUpdateCountSet, Dot11MeshConfigGroupUpdateCountTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "3"},

{{8,Dot11MeshActivePathSelectionProtocol}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshActivePathSelectionProtocolGet, Dot11MeshActivePathSelectionProtocolSet, Dot11MeshActivePathSelectionProtocolTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "1"},

{{8,Dot11MeshActivePathSelectionMetric}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshActivePathSelectionMetricGet, Dot11MeshActivePathSelectionMetricSet, Dot11MeshActivePathSelectionMetricTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "1"},

{{8,Dot11MeshForwarding}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshForwardingGet, Dot11MeshForwardingSet, Dot11MeshForwardingTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "1"},

{{8,Dot11MeshTTL}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshTTLGet, Dot11MeshTTLSet, Dot11MeshTTLTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "31"},

{{8,Dot11MeshGateAnnouncements}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshGateAnnouncementsGet, Dot11MeshGateAnnouncementsSet, Dot11MeshGateAnnouncementsTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MeshGateAnnouncementInterval}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshGateAnnouncementIntervalGet, Dot11MeshGateAnnouncementIntervalSet, Dot11MeshGateAnnouncementIntervalTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "10"},

{{8,Dot11MeshActiveCongestionControlMode}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshActiveCongestionControlModeGet, Dot11MeshActiveCongestionControlModeSet, Dot11MeshActiveCongestionControlModeTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "0"},

{{8,Dot11MeshActiveSynchronizationMethod}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshActiveSynchronizationMethodGet, Dot11MeshActiveSynchronizationMethodSet, Dot11MeshActiveSynchronizationMethodTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "1"},

{{8,Dot11MeshNbrOffsetMaxNeighbor}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshNbrOffsetMaxNeighborGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "50"},

{{8,Dot11MBCAActivated}, GetNextIndexDot11MeshSTAConfigTable, Dot11MBCAActivatedGet, Dot11MBCAActivatedSet, Dot11MBCAActivatedTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MeshBeaconTimingReportInterval}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshBeaconTimingReportIntervalGet, Dot11MeshBeaconTimingReportIntervalSet, Dot11MeshBeaconTimingReportIntervalTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "4"},

{{8,Dot11MeshBeaconTimingReportMaxNum}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshBeaconTimingReportMaxNumGet, Dot11MeshBeaconTimingReportMaxNumSet, Dot11MeshBeaconTimingReportMaxNumTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "16"},

{{8,Dot11MeshDelayedBeaconTxInterval}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshDelayedBeaconTxIntervalGet, Dot11MeshDelayedBeaconTxIntervalSet, Dot11MeshDelayedBeaconTxIntervalTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "0"},

{{8,Dot11MeshDelayedBeaconTxMaxDelay}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshDelayedBeaconTxMaxDelayGet, Dot11MeshDelayedBeaconTxMaxDelaySet, Dot11MeshDelayedBeaconTxMaxDelayTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "2048"},

{{8,Dot11MeshDelayedBeaconTxMinDelay}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshDelayedBeaconTxMinDelayGet, Dot11MeshDelayedBeaconTxMinDelaySet, Dot11MeshDelayedBeaconTxMinDelayTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "0"},

{{8,Dot11MeshAverageBeaconFrameDuration}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshAverageBeaconFrameDurationGet, Dot11MeshAverageBeaconFrameDurationSet, Dot11MeshAverageBeaconFrameDurationTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MeshSTAMissingAckRetryLimit}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshSTAMissingAckRetryLimitGet, Dot11MeshSTAMissingAckRetryLimitSet, Dot11MeshSTAMissingAckRetryLimitTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MeshAwakeWindowDuration}, GetNextIndexDot11MeshSTAConfigTable, Dot11MeshAwakeWindowDurationGet, Dot11MeshAwakeWindowDurationSet, Dot11MeshAwakeWindowDurationTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MCCAImplemented}, GetNextIndexDot11MeshSTAConfigTable, Dot11MCCAImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MCCAActivated}, GetNextIndexDot11MeshSTAConfigTable, Dot11MCCAActivatedGet, Dot11MCCAActivatedSet, Dot11MCCAActivatedTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11MAFlimit}, GetNextIndexDot11MeshSTAConfigTable, Dot11MAFlimitGet, Dot11MAFlimitSet, Dot11MAFlimitTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "128"},

{{8,Dot11MCCAScanDuration}, GetNextIndexDot11MeshSTAConfigTable, Dot11MCCAScanDurationGet, Dot11MCCAScanDurationSet, Dot11MCCAScanDurationTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "3200"},

{{8,Dot11MCCAAdvertPeriodMax}, GetNextIndexDot11MeshSTAConfigTable, Dot11MCCAAdvertPeriodMaxGet, Dot11MCCAAdvertPeriodMaxSet, Dot11MCCAAdvertPeriodMaxTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "1"},

{{8,Dot11MCCAMinTrackStates}, GetNextIndexDot11MeshSTAConfigTable, Dot11MCCAMinTrackStatesGet, Dot11MCCAMinTrackStatesSet, Dot11MCCAMinTrackStatesTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "83"},

{{8,Dot11MCCAMaxTrackStates}, GetNextIndexDot11MeshSTAConfigTable, Dot11MCCAMaxTrackStatesGet, Dot11MCCAMaxTrackStatesSet, Dot11MCCAMaxTrackStatesTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "83"},

{{8,Dot11MCCAOPtimeout}, GetNextIndexDot11MeshSTAConfigTable, Dot11MCCAOPtimeoutGet, Dot11MCCAOPtimeoutSet, Dot11MCCAOPtimeoutTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "10000"},

{{8,Dot11MCCACWmin}, GetNextIndexDot11MeshSTAConfigTable, Dot11MCCACWminGet, Dot11MCCACWminSet, Dot11MCCACWminTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "0"},

{{8,Dot11MCCACWmax}, GetNextIndexDot11MeshSTAConfigTable, Dot11MCCACWmaxGet, Dot11MCCACWmaxSet, Dot11MCCACWmaxTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "31"},

{{8,Dot11MCCAAIFSN}, GetNextIndexDot11MeshSTAConfigTable, Dot11MCCAAIFSNGet, Dot11MCCAAIFSNSet, Dot11MCCAAIFSNTest, Dot11MeshSTAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshSTAConfigTableINDEX, 1, 0, 0, "1"},
};
tMibData Dot11MeshSTAConfigTableEntry = { 40, Dot11MeshSTAConfigTableMibEntry };

tMbDbEntry Dot11MeshHWMPConfigTableMibEntry[]= {

{{8,Dot11MeshHWMPmaxPREQretries}, GetNextIndexDot11MeshHWMPConfigTable, Dot11MeshHWMPmaxPREQretriesGet, Dot11MeshHWMPmaxPREQretriesSet, Dot11MeshHWMPmaxPREQretriesTest, Dot11MeshHWMPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshHWMPConfigTableINDEX, 1, 0, 0, "3"},

{{8,Dot11MeshHWMPnetDiameter}, GetNextIndexDot11MeshHWMPConfigTable, Dot11MeshHWMPnetDiameterGet, Dot11MeshHWMPnetDiameterSet, Dot11MeshHWMPnetDiameterTest, Dot11MeshHWMPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshHWMPConfigTableINDEX, 1, 0, 0, "31"},

{{8,Dot11MeshHWMPnetDiameterTraversalTime}, GetNextIndexDot11MeshHWMPConfigTable, Dot11MeshHWMPnetDiameterTraversalTimeGet, Dot11MeshHWMPnetDiameterTraversalTimeSet, Dot11MeshHWMPnetDiameterTraversalTimeTest, Dot11MeshHWMPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshHWMPConfigTableINDEX, 1, 0, 0, "500"},

{{8,Dot11MeshHWMPpreqMinInterval}, GetNextIndexDot11MeshHWMPConfigTable, Dot11MeshHWMPpreqMinIntervalGet, Dot11MeshHWMPpreqMinIntervalSet, Dot11MeshHWMPpreqMinIntervalTest, Dot11MeshHWMPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshHWMPConfigTableINDEX, 1, 0, 0, "100"},

{{8,Dot11MeshHWMPperrMinInterval}, GetNextIndexDot11MeshHWMPConfigTable, Dot11MeshHWMPperrMinIntervalGet, Dot11MeshHWMPperrMinIntervalSet, Dot11MeshHWMPperrMinIntervalTest, Dot11MeshHWMPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshHWMPConfigTableINDEX, 1, 0, 0, "100"},

{{8,Dot11MeshHWMPactivePathToRootTimeout}, GetNextIndexDot11MeshHWMPConfigTable, Dot11MeshHWMPactivePathToRootTimeoutGet, Dot11MeshHWMPactivePathToRootTimeoutSet, Dot11MeshHWMPactivePathToRootTimeoutTest, Dot11MeshHWMPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshHWMPConfigTableINDEX, 1, 0, 0, "5000"},

{{8,Dot11MeshHWMPactivePathTimeout}, GetNextIndexDot11MeshHWMPConfigTable, Dot11MeshHWMPactivePathTimeoutGet, Dot11MeshHWMPactivePathTimeoutSet, Dot11MeshHWMPactivePathTimeoutTest, Dot11MeshHWMPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshHWMPConfigTableINDEX, 1, 0, 0, "5000"},

{{8,Dot11MeshHWMProotMode}, GetNextIndexDot11MeshHWMPConfigTable, Dot11MeshHWMProotModeGet, Dot11MeshHWMProotModeSet, Dot11MeshHWMProotModeTest, Dot11MeshHWMPConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11MeshHWMPConfigTableINDEX, 1, 0, 0, "0"},

{{8,Dot11MeshHWMProotInterval}, GetNextIndexDot11MeshHWMPConfigTable, Dot11MeshHWMProotIntervalGet, Dot11MeshHWMProotIntervalSet, Dot11MeshHWMProotIntervalTest, Dot11MeshHWMPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshHWMPConfigTableINDEX, 1, 0, 0, "2000"},

{{8,Dot11MeshHWMPrannInterval}, GetNextIndexDot11MeshHWMPConfigTable, Dot11MeshHWMPrannIntervalGet, Dot11MeshHWMPrannIntervalSet, Dot11MeshHWMPrannIntervalTest, Dot11MeshHWMPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshHWMPConfigTableINDEX, 1, 0, 0, "2000"},

{{8,Dot11MeshHWMPtargetOnly}, GetNextIndexDot11MeshHWMPConfigTable, Dot11MeshHWMPtargetOnlyGet, Dot11MeshHWMPtargetOnlySet, Dot11MeshHWMPtargetOnlyTest, Dot11MeshHWMPConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11MeshHWMPConfigTableINDEX, 1, 0, 0, "1"},

{{8,Dot11MeshHWMPmaintenanceInterval}, GetNextIndexDot11MeshHWMPConfigTable, Dot11MeshHWMPmaintenanceIntervalGet, Dot11MeshHWMPmaintenanceIntervalSet, Dot11MeshHWMPmaintenanceIntervalTest, Dot11MeshHWMPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshHWMPConfigTableINDEX, 1, 0, 0, "2000"},

{{8,Dot11MeshHWMPconfirmationInterval}, GetNextIndexDot11MeshHWMPConfigTable, Dot11MeshHWMPconfirmationIntervalGet, Dot11MeshHWMPconfirmationIntervalSet, Dot11MeshHWMPconfirmationIntervalTest, Dot11MeshHWMPConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MeshHWMPConfigTableINDEX, 1, 0, 0, "2000"},
};
tMibData Dot11MeshHWMPConfigTableEntry = { 13, Dot11MeshHWMPConfigTableMibEntry };

tMbDbEntry Dot11RSNAConfigPasswordValueTableMibEntry[]= {

{{8,Dot11RSNAConfigPasswordValueIndex}, GetNextIndexDot11RSNAConfigPasswordValueTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11RSNAConfigPasswordValueTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigPasswordCredential}, GetNextIndexDot11RSNAConfigPasswordValueTable, Dot11RSNAConfigPasswordCredentialGet, Dot11RSNAConfigPasswordCredentialSet, Dot11RSNAConfigPasswordCredentialTest, Dot11RSNAConfigPasswordValueTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11RSNAConfigPasswordValueTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigPasswordPeerMac}, GetNextIndexDot11RSNAConfigPasswordValueTable, Dot11RSNAConfigPasswordPeerMacGet, Dot11RSNAConfigPasswordPeerMacSet, Dot11RSNAConfigPasswordPeerMacTest, Dot11RSNAConfigPasswordValueTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot11RSNAConfigPasswordValueTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11RSNAConfigPasswordValueTableEntry = { 3, Dot11RSNAConfigPasswordValueTableMibEntry };

tMbDbEntry Dot11RSNAConfigDLCGroupTableMibEntry[]= {

{{8,Dot11RSNAConfigDLCGroupIndex}, GetNextIndexDot11RSNAConfigDLCGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11RSNAConfigDLCGroupTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigDLCGroupIdentifier}, GetNextIndexDot11RSNAConfigDLCGroupTable, Dot11RSNAConfigDLCGroupIdentifierGet, Dot11RSNAConfigDLCGroupIdentifierSet, Dot11RSNAConfigDLCGroupIdentifierTest, Dot11RSNAConfigDLCGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigDLCGroupTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11RSNAConfigDLCGroupTableEntry = { 2, Dot11RSNAConfigDLCGroupTableMibEntry };

tMbDbEntry Dot11OperationTableMibEntry[]= {

{{8,Dot11MACAddress}, GetNextIndexDot11OperationTable, Dot11MACAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RTSThreshold}, GetNextIndexDot11OperationTable, Dot11RTSThresholdGet, Dot11RTSThresholdSet, Dot11RTSThresholdTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "65536"},

{{8,Dot11ShortRetryLimit}, GetNextIndexDot11OperationTable, Dot11ShortRetryLimitGet, Dot11ShortRetryLimitSet, Dot11ShortRetryLimitTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "7"},

{{8,Dot11LongRetryLimit}, GetNextIndexDot11OperationTable, Dot11LongRetryLimitGet, Dot11LongRetryLimitSet, Dot11LongRetryLimitTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "4"},

{{8,Dot11FragmentationThreshold}, GetNextIndexDot11OperationTable, Dot11FragmentationThresholdGet, Dot11FragmentationThresholdSet, Dot11FragmentationThresholdTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MaxTransmitMSDULifetime}, GetNextIndexDot11OperationTable, Dot11MaxTransmitMSDULifetimeGet, Dot11MaxTransmitMSDULifetimeSet, Dot11MaxTransmitMSDULifetimeTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "512"},

{{8,Dot11MaxReceiveLifetime}, GetNextIndexDot11OperationTable, Dot11MaxReceiveLifetimeGet, Dot11MaxReceiveLifetimeSet, Dot11MaxReceiveLifetimeTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "512"},

{{8,Dot11ManufacturerID}, GetNextIndexDot11OperationTable, Dot11ManufacturerIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ProductID}, GetNextIndexDot11OperationTable, Dot11ProductIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CAPLimit}, GetNextIndexDot11OperationTable, Dot11CAPLimitGet, Dot11CAPLimitSet, Dot11CAPLimitTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11HCCWmin}, GetNextIndexDot11OperationTable, Dot11HCCWminGet, Dot11HCCWminSet, Dot11HCCWminTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "0"},

{{8,Dot11HCCWmax}, GetNextIndexDot11OperationTable, Dot11HCCWmaxGet, Dot11HCCWmaxSet, Dot11HCCWmaxTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "0"},

{{8,Dot11HCCAIFSN}, GetNextIndexDot11OperationTable, Dot11HCCAIFSNGet, Dot11HCCAIFSNSet, Dot11HCCAIFSNTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "1"},

{{8,Dot11ADDBAResponseTimeout}, GetNextIndexDot11OperationTable, Dot11ADDBAResponseTimeoutGet, Dot11ADDBAResponseTimeoutSet, Dot11ADDBAResponseTimeoutTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "1"},

{{8,Dot11ADDTSResponseTimeout}, GetNextIndexDot11OperationTable, Dot11ADDTSResponseTimeoutGet, Dot11ADDTSResponseTimeoutSet, Dot11ADDTSResponseTimeoutTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "1"},

{{8,Dot11ChannelUtilizationBeaconInterval}, GetNextIndexDot11OperationTable, Dot11ChannelUtilizationBeaconIntervalGet, Dot11ChannelUtilizationBeaconIntervalSet, Dot11ChannelUtilizationBeaconIntervalTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "50"},

{{8,Dot11ScheduleTimeout}, GetNextIndexDot11OperationTable, Dot11ScheduleTimeoutGet, Dot11ScheduleTimeoutSet, Dot11ScheduleTimeoutTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "10"},

{{8,Dot11DLSResponseTimeout}, GetNextIndexDot11OperationTable, Dot11DLSResponseTimeoutGet, Dot11DLSResponseTimeoutSet, Dot11DLSResponseTimeoutTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "10"},

{{8,Dot11QAPMissingAckRetryLimit}, GetNextIndexDot11OperationTable, Dot11QAPMissingAckRetryLimitGet, Dot11QAPMissingAckRetryLimitSet, Dot11QAPMissingAckRetryLimitTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11EDCAAveragingPeriod}, GetNextIndexDot11OperationTable, Dot11EDCAAveragingPeriodGet, Dot11EDCAAveragingPeriodSet, Dot11EDCAAveragingPeriodTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "5"},

{{8,Dot11HTProtection}, GetNextIndexDot11OperationTable, Dot11HTProtectionGet, Dot11HTProtectionSet, Dot11HTProtectionTest, Dot11OperationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "0"},

{{8,Dot11RIFSMode}, GetNextIndexDot11OperationTable, Dot11RIFSModeGet, Dot11RIFSModeSet, Dot11RIFSModeTest, Dot11OperationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "2"},

{{8,Dot11PSMPControlledAccess}, GetNextIndexDot11OperationTable, Dot11PSMPControlledAccessGet, Dot11PSMPControlledAccessSet, Dot11PSMPControlledAccessTest, Dot11OperationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "2"},

{{8,Dot11ServiceIntervalGranularity}, GetNextIndexDot11OperationTable, Dot11ServiceIntervalGranularityGet, Dot11ServiceIntervalGranularitySet, Dot11ServiceIntervalGranularityTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "0"},

{{8,Dot11DualCTSProtection}, GetNextIndexDot11OperationTable, Dot11DualCTSProtectionGet, Dot11DualCTSProtectionSet, Dot11DualCTSProtectionTest, Dot11OperationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "2"},

{{8,Dot11LSIGTXOPFullProtectionActivated}, GetNextIndexDot11OperationTable, Dot11LSIGTXOPFullProtectionActivatedGet, Dot11LSIGTXOPFullProtectionActivatedSet, Dot11LSIGTXOPFullProtectionActivatedTest, Dot11OperationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "2"},

{{8,Dot11NonGFEntitiesPresent}, GetNextIndexDot11OperationTable, Dot11NonGFEntitiesPresentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11OperationTableINDEX, 1, 0, 0, "2"},

{{8,Dot11PCOActivated}, GetNextIndexDot11OperationTable, Dot11PCOActivatedGet, Dot11PCOActivatedSet, Dot11PCOActivatedTest, Dot11OperationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "2"},

{{8,Dot11PCOFortyMaxDuration}, GetNextIndexDot11OperationTable, Dot11PCOFortyMaxDurationGet, Dot11PCOFortyMaxDurationSet, Dot11PCOFortyMaxDurationTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "30"},

{{8,Dot11PCOTwentyMaxDuration}, GetNextIndexDot11OperationTable, Dot11PCOTwentyMaxDurationGet, Dot11PCOTwentyMaxDurationSet, Dot11PCOTwentyMaxDurationTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "30"},

{{8,Dot11PCOFortyMinDuration}, GetNextIndexDot11OperationTable, Dot11PCOFortyMinDurationGet, Dot11PCOFortyMinDurationSet, Dot11PCOFortyMinDurationTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "20"},

{{8,Dot11PCOTwentyMinDuration}, GetNextIndexDot11OperationTable, Dot11PCOTwentyMinDurationGet, Dot11PCOTwentyMinDurationSet, Dot11PCOTwentyMinDurationTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "20"},

{{8,Dot11FortyMHzIntolerant}, GetNextIndexDot11OperationTable, Dot11FortyMHzIntolerantGet, Dot11FortyMHzIntolerantSet, Dot11FortyMHzIntolerantTest, Dot11OperationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "2"},

{{8,Dot11BSSWidthTriggerScanInterval}, GetNextIndexDot11OperationTable, Dot11BSSWidthTriggerScanIntervalGet, Dot11BSSWidthTriggerScanIntervalSet, Dot11BSSWidthTriggerScanIntervalTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "300"},

{{8,Dot11BSSWidthChannelTransitionDelayFactor}, GetNextIndexDot11OperationTable, Dot11BSSWidthChannelTransitionDelayFactorGet, Dot11BSSWidthChannelTransitionDelayFactorSet, Dot11BSSWidthChannelTransitionDelayFactorTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "5"},

{{8,Dot11OBSSScanPassiveDwell}, GetNextIndexDot11OperationTable, Dot11OBSSScanPassiveDwellGet, Dot11OBSSScanPassiveDwellSet, Dot11OBSSScanPassiveDwellTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "20"},

{{8,Dot11OBSSScanActiveDwell}, GetNextIndexDot11OperationTable, Dot11OBSSScanActiveDwellGet, Dot11OBSSScanActiveDwellSet, Dot11OBSSScanActiveDwellTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "10"},

{{8,Dot11OBSSScanPassiveTotalPerChannel}, GetNextIndexDot11OperationTable, Dot11OBSSScanPassiveTotalPerChannelGet, Dot11OBSSScanPassiveTotalPerChannelSet, Dot11OBSSScanPassiveTotalPerChannelTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "200"},

{{8,Dot11OBSSScanActiveTotalPerChannel}, GetNextIndexDot11OperationTable, Dot11OBSSScanActiveTotalPerChannelGet, Dot11OBSSScanActiveTotalPerChannelSet, Dot11OBSSScanActiveTotalPerChannelTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "20"},

{{8,Dot112040BSSCoexistenceManagementSupport}, GetNextIndexDot11OperationTable, Dot112040BSSCoexistenceManagementSupportGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11OperationTableINDEX, 1, 0, 0, "2"},

{{8,Dot11OBSSScanActivityThreshold}, GetNextIndexDot11OperationTable, Dot11OBSSScanActivityThresholdGet, Dot11OBSSScanActivityThresholdSet, Dot11OBSSScanActivityThresholdTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, "25"},
};
tMibData Dot11OperationTableEntry = { 41, Dot11OperationTableMibEntry };

tMbDbEntry Dot11CountersTableMibEntry[]= {

{{8,Dot11TransmittedFragmentCount}, GetNextIndexDot11CountersTable, Dot11TransmittedFragmentCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11GroupTransmittedFrameCount}, GetNextIndexDot11CountersTable, Dot11GroupTransmittedFrameCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11FailedCount}, GetNextIndexDot11CountersTable, Dot11FailedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RetryCount}, GetNextIndexDot11CountersTable, Dot11RetryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MultipleRetryCount}, GetNextIndexDot11CountersTable, Dot11MultipleRetryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11FrameDuplicateCount}, GetNextIndexDot11CountersTable, Dot11FrameDuplicateCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RTSSuccessCount}, GetNextIndexDot11CountersTable, Dot11RTSSuccessCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RTSFailureCount}, GetNextIndexDot11CountersTable, Dot11RTSFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ACKFailureCount}, GetNextIndexDot11CountersTable, Dot11ACKFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ReceivedFragmentCount}, GetNextIndexDot11CountersTable, Dot11ReceivedFragmentCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11GroupReceivedFrameCount}, GetNextIndexDot11CountersTable, Dot11GroupReceivedFrameCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11FCSErrorCount}, GetNextIndexDot11CountersTable, Dot11FCSErrorCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TransmittedFrameCount}, GetNextIndexDot11CountersTable, Dot11TransmittedFrameCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WEPUndecryptableCount}, GetNextIndexDot11CountersTable, Dot11WEPUndecryptableCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11QosDiscardedFragmentCount}, GetNextIndexDot11CountersTable, Dot11QosDiscardedFragmentCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11AssociatedStationCount}, GetNextIndexDot11CountersTable, Dot11AssociatedStationCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11QosCFPollsReceivedCount}, GetNextIndexDot11CountersTable, Dot11QosCFPollsReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11QosCFPollsUnusedCount}, GetNextIndexDot11CountersTable, Dot11QosCFPollsUnusedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11QosCFPollsUnusableCount}, GetNextIndexDot11CountersTable, Dot11QosCFPollsUnusableCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11QosCFPollsLostCount}, GetNextIndexDot11CountersTable, Dot11QosCFPollsLostCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TransmittedAMSDUCount}, GetNextIndexDot11CountersTable, Dot11TransmittedAMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11FailedAMSDUCount}, GetNextIndexDot11CountersTable, Dot11FailedAMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RetryAMSDUCount}, GetNextIndexDot11CountersTable, Dot11RetryAMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MultipleRetryAMSDUCount}, GetNextIndexDot11CountersTable, Dot11MultipleRetryAMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TransmittedOctetsInAMSDUCount}, GetNextIndexDot11CountersTable, Dot11TransmittedOctetsInAMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11AMSDUAckFailureCount}, GetNextIndexDot11CountersTable, Dot11AMSDUAckFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ReceivedAMSDUCount}, GetNextIndexDot11CountersTable, Dot11ReceivedAMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ReceivedOctetsInAMSDUCount}, GetNextIndexDot11CountersTable, Dot11ReceivedOctetsInAMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TransmittedAMPDUCount}, GetNextIndexDot11CountersTable, Dot11TransmittedAMPDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TransmittedMPDUsInAMPDUCount}, GetNextIndexDot11CountersTable, Dot11TransmittedMPDUsInAMPDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TransmittedOctetsInAMPDUCount}, GetNextIndexDot11CountersTable, Dot11TransmittedOctetsInAMPDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11AMPDUReceivedCount}, GetNextIndexDot11CountersTable, Dot11AMPDUReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MPDUInReceivedAMPDUCount}, GetNextIndexDot11CountersTable, Dot11MPDUInReceivedAMPDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ReceivedOctetsInAMPDUCount}, GetNextIndexDot11CountersTable, Dot11ReceivedOctetsInAMPDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11AMPDUDelimiterCRCErrorCount}, GetNextIndexDot11CountersTable, Dot11AMPDUDelimiterCRCErrorCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ImplicitBARFailureCount}, GetNextIndexDot11CountersTable, Dot11ImplicitBARFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ExplicitBARFailureCount}, GetNextIndexDot11CountersTable, Dot11ExplicitBARFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ChannelWidthSwitchCount}, GetNextIndexDot11CountersTable, Dot11ChannelWidthSwitchCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TwentyMHzFrameTransmittedCount}, GetNextIndexDot11CountersTable, Dot11TwentyMHzFrameTransmittedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11FortyMHzFrameTransmittedCount}, GetNextIndexDot11CountersTable, Dot11FortyMHzFrameTransmittedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TwentyMHzFrameReceivedCount}, GetNextIndexDot11CountersTable, Dot11TwentyMHzFrameReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11FortyMHzFrameReceivedCount}, GetNextIndexDot11CountersTable, Dot11FortyMHzFrameReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11PSMPUTTGrantDuration}, GetNextIndexDot11CountersTable, Dot11PSMPUTTGrantDurationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11PSMPUTTUsedDuration}, GetNextIndexDot11CountersTable, Dot11PSMPUTTUsedDurationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11GrantedRDGUsedCount}, GetNextIndexDot11CountersTable, Dot11GrantedRDGUsedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11GrantedRDGUnusedCount}, GetNextIndexDot11CountersTable, Dot11GrantedRDGUnusedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TransmittedFramesInGrantedRDGCount}, GetNextIndexDot11CountersTable, Dot11TransmittedFramesInGrantedRDGCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TransmittedOctetsInGrantedRDGCount}, GetNextIndexDot11CountersTable, Dot11TransmittedOctetsInGrantedRDGCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11BeamformingFrameCount}, GetNextIndexDot11CountersTable, Dot11BeamformingFrameCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DualCTSSuccessCount}, GetNextIndexDot11CountersTable, Dot11DualCTSSuccessCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DualCTSFailureCount}, GetNextIndexDot11CountersTable, Dot11DualCTSFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11STBCCTSSuccessCount}, GetNextIndexDot11CountersTable, Dot11STBCCTSSuccessCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11STBCCTSFailureCount}, GetNextIndexDot11CountersTable, Dot11STBCCTSFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11nonSTBCCTSSuccessCount}, GetNextIndexDot11CountersTable, Dot11nonSTBCCTSSuccessCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11nonSTBCCTSFailureCount}, GetNextIndexDot11CountersTable, Dot11nonSTBCCTSFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RTSLSIGSuccessCount}, GetNextIndexDot11CountersTable, Dot11RTSLSIGSuccessCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RTSLSIGFailureCount}, GetNextIndexDot11CountersTable, Dot11RTSLSIGFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11PBACErrors}, GetNextIndexDot11CountersTable, Dot11PBACErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DeniedAssociationCounterDueToBSSLoad}, GetNextIndexDot11CountersTable, Dot11DeniedAssociationCounterDueToBSSLoadGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11CountersTableEntry = { 59, Dot11CountersTableMibEntry };

tMbDbEntry Dot11GroupAddressesTableMibEntry[]= {

{{8,Dot11GroupAddressesIndex}, GetNextIndexDot11GroupAddressesTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot11GroupAddressesTableINDEX, 2, 0, 0, NULL},

{{8,Dot11Address}, GetNextIndexDot11GroupAddressesTable, Dot11AddressGet, Dot11AddressSet, Dot11AddressTest, Dot11GroupAddressesTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot11GroupAddressesTableINDEX, 2, 0, 0, NULL},

{{8,Dot11GroupAddressesStatus}, GetNextIndexDot11GroupAddressesTable, Dot11GroupAddressesStatusGet, Dot11GroupAddressesStatusSet, Dot11GroupAddressesStatusTest, Dot11GroupAddressesTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11GroupAddressesTableINDEX, 2, 0, 1, "1"},
};
tMibData Dot11GroupAddressesTableEntry = { 3, Dot11GroupAddressesTableMibEntry };

tMbDbEntry Dot11EDCATableMibEntry[]= {

{{8,Dot11EDCATableIndex}, GetNextIndexDot11EDCATable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11EDCATableINDEX, 2, 0, 0, NULL},

{{8,Dot11EDCATableCWmin}, GetNextIndexDot11EDCATable, Dot11EDCATableCWminGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11EDCATableINDEX, 2, 0, 0, NULL},

{{8,Dot11EDCATableCWmax}, GetNextIndexDot11EDCATable, Dot11EDCATableCWmaxGet, Dot11EDCATableCWmaxSet, Dot11EDCATableCWmaxTest, Dot11EDCATableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11EDCATableINDEX, 2, 0, 0, NULL},

{{8,Dot11EDCATableAIFSN}, GetNextIndexDot11EDCATable, Dot11EDCATableAIFSNGet, Dot11EDCATableAIFSNSet, Dot11EDCATableAIFSNTest, Dot11EDCATableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11EDCATableINDEX, 2, 0, 0, NULL},

{{8,Dot11EDCATableTXOPLimit}, GetNextIndexDot11EDCATable, Dot11EDCATableTXOPLimitGet, Dot11EDCATableTXOPLimitSet, Dot11EDCATableTXOPLimitTest, Dot11EDCATableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11EDCATableINDEX, 2, 0, 0, NULL},

{{8,Dot11EDCATableMSDULifetime}, GetNextIndexDot11EDCATable, Dot11EDCATableMSDULifetimeGet, Dot11EDCATableMSDULifetimeSet, Dot11EDCATableMSDULifetimeTest, Dot11EDCATableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11EDCATableINDEX, 2, 0, 0, "500"},

{{8,Dot11EDCATableMandatory}, GetNextIndexDot11EDCATable, Dot11EDCATableMandatoryGet, Dot11EDCATableMandatorySet, Dot11EDCATableMandatoryTest, Dot11EDCATableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11EDCATableINDEX, 2, 0, 0, "2"},
};
tMibData Dot11EDCATableEntry = { 7, Dot11EDCATableMibEntry };

tMbDbEntry Dot11QAPEDCATableMibEntry[]= {

{{8,Dot11QAPEDCATableIndex}, GetNextIndexDot11QAPEDCATable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11QAPEDCATableINDEX, 2, 0, 0, NULL},

{{8,Dot11QAPEDCATableCWmin}, GetNextIndexDot11QAPEDCATable, Dot11QAPEDCATableCWminGet, Dot11QAPEDCATableCWminSet, Dot11QAPEDCATableCWminTest, Dot11QAPEDCATableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11QAPEDCATableINDEX, 2, 0, 0, NULL},

{{8,Dot11QAPEDCATableCWmax}, GetNextIndexDot11QAPEDCATable, Dot11QAPEDCATableCWmaxGet, Dot11QAPEDCATableCWmaxSet, Dot11QAPEDCATableCWmaxTest, Dot11QAPEDCATableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11QAPEDCATableINDEX, 2, 0, 0, NULL},

{{8,Dot11QAPEDCATableAIFSN}, GetNextIndexDot11QAPEDCATable, Dot11QAPEDCATableAIFSNGet, Dot11QAPEDCATableAIFSNSet, Dot11QAPEDCATableAIFSNTest, Dot11QAPEDCATableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11QAPEDCATableINDEX, 2, 0, 0, NULL},

{{8,Dot11QAPEDCATableTXOPLimit}, GetNextIndexDot11QAPEDCATable, Dot11QAPEDCATableTXOPLimitGet, Dot11QAPEDCATableTXOPLimitSet, Dot11QAPEDCATableTXOPLimitTest, Dot11QAPEDCATableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11QAPEDCATableINDEX, 2, 0, 0, NULL},

{{8,Dot11QAPEDCATableMSDULifetime}, GetNextIndexDot11QAPEDCATable, Dot11QAPEDCATableMSDULifetimeGet, Dot11QAPEDCATableMSDULifetimeSet, Dot11QAPEDCATableMSDULifetimeTest, Dot11QAPEDCATableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11QAPEDCATableINDEX, 2, 0, 0, "500"},

{{8,Dot11QAPEDCATableMandatory}, GetNextIndexDot11QAPEDCATable, Dot11QAPEDCATableMandatoryGet, Dot11QAPEDCATableMandatorySet, Dot11QAPEDCATableMandatoryTest, Dot11QAPEDCATableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11QAPEDCATableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11QAPEDCATableEntry = { 7, Dot11QAPEDCATableMibEntry };

tMbDbEntry Dot11QosCountersTableMibEntry[]= {

{{8,Dot11QosCountersIndex}, GetNextIndexDot11QosCountersTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosTransmittedFragmentCount}, GetNextIndexDot11QosCountersTable, Dot11QosTransmittedFragmentCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosFailedCount}, GetNextIndexDot11QosCountersTable, Dot11QosFailedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosRetryCount}, GetNextIndexDot11QosCountersTable, Dot11QosRetryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosMultipleRetryCount}, GetNextIndexDot11QosCountersTable, Dot11QosMultipleRetryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosFrameDuplicateCount}, GetNextIndexDot11QosCountersTable, Dot11QosFrameDuplicateCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosRTSSuccessCount}, GetNextIndexDot11QosCountersTable, Dot11QosRTSSuccessCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosRTSFailureCount}, GetNextIndexDot11QosCountersTable, Dot11QosRTSFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosACKFailureCount}, GetNextIndexDot11QosCountersTable, Dot11QosACKFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosReceivedFragmentCount}, GetNextIndexDot11QosCountersTable, Dot11QosReceivedFragmentCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosTransmittedFrameCount}, GetNextIndexDot11QosCountersTable, Dot11QosTransmittedFrameCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosDiscardedFrameCount}, GetNextIndexDot11QosCountersTable, Dot11QosDiscardedFrameCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosMPDUsReceivedCount}, GetNextIndexDot11QosCountersTable, Dot11QosMPDUsReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosRetriesReceivedCount}, GetNextIndexDot11QosCountersTable, Dot11QosRetriesReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11QosCountersTableEntry = { 14, Dot11QosCountersTableMibEntry };

tMbDbEntry Dot11ResourceTypeIDNameMibEntry[]= {

{{7,Dot11ResourceTypeIDName}, NULL, Dot11ResourceTypeIDNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, "RTID"},
};
tMibData Dot11ResourceTypeIDNameEntry = { 1, Dot11ResourceTypeIDNameMibEntry };

tMbDbEntry Dot11ResourceInfoTableMibEntry[]= {

{{9,Dot11manufacturerOUI}, GetNextIndexDot11ResourceInfoTable, Dot11manufacturerOUIGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11ResourceInfoTableINDEX, 1, 0, 0, NULL},

{{9,Dot11manufacturerName}, GetNextIndexDot11ResourceInfoTable, Dot11manufacturerNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11ResourceInfoTableINDEX, 1, 0, 0, NULL},

{{9,Dot11manufacturerProductName}, GetNextIndexDot11ResourceInfoTable, Dot11manufacturerProductNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11ResourceInfoTableINDEX, 1, 0, 0, NULL},

{{9,Dot11manufacturerProductVersion}, GetNextIndexDot11ResourceInfoTable, Dot11manufacturerProductVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11ResourceInfoTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11ResourceInfoTableEntry = { 4, Dot11ResourceInfoTableMibEntry };

tMbDbEntry Dot11PhyOperationTableMibEntry[]= {

{{8,Dot11PHYType}, GetNextIndexDot11PhyOperationTable, Dot11PHYTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyOperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CurrentRegDomain}, GetNextIndexDot11PhyOperationTable, Dot11CurrentRegDomainGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyOperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TempType}, GetNextIndexDot11PhyOperationTable, Dot11TempTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyOperationTableINDEX, 1, 1, 0, NULL},
};
tMibData Dot11PhyOperationTableEntry = { 3, Dot11PhyOperationTableMibEntry };

tMbDbEntry Dot11PhyAntennaTableMibEntry[]= {

{{8,Dot11CurrentTxAntenna}, GetNextIndexDot11PhyAntennaTable, Dot11CurrentTxAntennaGet, Dot11CurrentTxAntennaSet, Dot11CurrentTxAntennaTest, Dot11PhyAntennaTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11PhyAntennaTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DiversitySupportImplemented}, GetNextIndexDot11PhyAntennaTable, Dot11DiversitySupportImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyAntennaTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CurrentRxAntenna}, GetNextIndexDot11PhyAntennaTable, Dot11CurrentRxAntennaGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyAntennaTableINDEX, 1, 0, 0, NULL},

{{8,Dot11AntennaSelectionOptionImplemented}, GetNextIndexDot11PhyAntennaTable, Dot11AntennaSelectionOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyAntennaTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TransmitExplicitCSIFeedbackASOptionImplemented}, GetNextIndexDot11PhyAntennaTable, Dot11TransmitExplicitCSIFeedbackASOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyAntennaTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TransmitIndicesFeedbackASOptionImplemented}, GetNextIndexDot11PhyAntennaTable, Dot11TransmitIndicesFeedbackASOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyAntennaTableINDEX, 1, 0, 0, "2"},

{{8,Dot11ExplicitCSIFeedbackASOptionImplemented}, GetNextIndexDot11PhyAntennaTable, Dot11ExplicitCSIFeedbackASOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyAntennaTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TransmitIndicesComputationASOptionImplemented}, GetNextIndexDot11PhyAntennaTable, Dot11TransmitIndicesComputationASOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyAntennaTableINDEX, 1, 0, 0, "2"},

{{8,Dot11ReceiveAntennaSelectionOptionImplemented}, GetNextIndexDot11PhyAntennaTable, Dot11ReceiveAntennaSelectionOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyAntennaTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TransmitSoundingPPDUOptionImplemented}, GetNextIndexDot11PhyAntennaTable, Dot11TransmitSoundingPPDUOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyAntennaTableINDEX, 1, 0, 0, "2"},

{{8,Dot11NumberOfActiveRxAntennas}, GetNextIndexDot11PhyAntennaTable, Dot11NumberOfActiveRxAntennasGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyAntennaTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11PhyAntennaTableEntry = { 11, Dot11PhyAntennaTableMibEntry };

tMbDbEntry Dot11PhyTxPowerTableMibEntry[]= {

{{8,Dot11NumberSupportedPowerLevelsImplemented}, GetNextIndexDot11PhyTxPowerTable, Dot11NumberSupportedPowerLevelsImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyTxPowerTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TxPowerLevel1}, GetNextIndexDot11PhyTxPowerTable, Dot11TxPowerLevel1Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyTxPowerTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TxPowerLevel2}, GetNextIndexDot11PhyTxPowerTable, Dot11TxPowerLevel2Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyTxPowerTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TxPowerLevel3}, GetNextIndexDot11PhyTxPowerTable, Dot11TxPowerLevel3Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyTxPowerTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TxPowerLevel4}, GetNextIndexDot11PhyTxPowerTable, Dot11TxPowerLevel4Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyTxPowerTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TxPowerLevel5}, GetNextIndexDot11PhyTxPowerTable, Dot11TxPowerLevel5Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyTxPowerTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TxPowerLevel6}, GetNextIndexDot11PhyTxPowerTable, Dot11TxPowerLevel6Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyTxPowerTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TxPowerLevel7}, GetNextIndexDot11PhyTxPowerTable, Dot11TxPowerLevel7Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyTxPowerTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TxPowerLevel8}, GetNextIndexDot11PhyTxPowerTable, Dot11TxPowerLevel8Get, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyTxPowerTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CurrentTxPowerLevel}, GetNextIndexDot11PhyTxPowerTable, Dot11CurrentTxPowerLevelGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyTxPowerTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11PhyTxPowerTableEntry = { 10, Dot11PhyTxPowerTableMibEntry };

tMbDbEntry Dot11PhyFHSSTableMibEntry[]= {

{{8,Dot11HopTime}, GetNextIndexDot11PhyFHSSTable, Dot11HopTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CurrentChannelNumber}, GetNextIndexDot11PhyFHSSTable, Dot11CurrentChannelNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MaxDwellTime}, GetNextIndexDot11PhyFHSSTable, Dot11MaxDwellTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CurrentDwellTime}, GetNextIndexDot11PhyFHSSTable, Dot11CurrentDwellTimeGet, Dot11CurrentDwellTimeSet, Dot11CurrentDwellTimeTest, Dot11PhyFHSSTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11PhyFHSSTableINDEX, 1, 0, 0, "19"},

{{8,Dot11CurrentSet}, GetNextIndexDot11PhyFHSSTable, Dot11CurrentSetGet, Dot11CurrentSetSet, Dot11CurrentSetTest, Dot11PhyFHSSTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CurrentPattern}, GetNextIndexDot11PhyFHSSTable, Dot11CurrentPatternGet, Dot11CurrentPatternSet, Dot11CurrentPatternTest, Dot11PhyFHSSTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CurrentIndex}, GetNextIndexDot11PhyFHSSTable, Dot11CurrentIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11EHCCPrimeRadix}, GetNextIndexDot11PhyFHSSTable, Dot11EHCCPrimeRadixGet, Dot11EHCCPrimeRadixSet, Dot11EHCCPrimeRadixTest, Dot11PhyFHSSTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11EHCCNumberofChannelsFamilyIndex}, GetNextIndexDot11PhyFHSSTable, Dot11EHCCNumberofChannelsFamilyIndexGet, Dot11EHCCNumberofChannelsFamilyIndexSet, Dot11EHCCNumberofChannelsFamilyIndexTest, Dot11PhyFHSSTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11EHCCCapabilityImplemented}, GetNextIndexDot11PhyFHSSTable, Dot11EHCCCapabilityImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyFHSSTableINDEX, 1, 0, 0, "2"},

{{8,Dot11EHCCCapabilityActivated}, GetNextIndexDot11PhyFHSSTable, Dot11EHCCCapabilityActivatedGet, Dot11EHCCCapabilityActivatedSet, Dot11EHCCCapabilityActivatedTest, Dot11PhyFHSSTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyFHSSTableINDEX, 1, 0, 0, "2"},

{{8,Dot11HopAlgorithmAdopted}, GetNextIndexDot11PhyFHSSTable, Dot11HopAlgorithmAdoptedGet, Dot11HopAlgorithmAdoptedSet, Dot11HopAlgorithmAdoptedTest, Dot11PhyFHSSTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RandomTableFlag}, GetNextIndexDot11PhyFHSSTable, Dot11RandomTableFlagGet, Dot11RandomTableFlagSet, Dot11RandomTableFlagTest, Dot11PhyFHSSTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyFHSSTableINDEX, 1, 0, 0, "1"},

{{8,Dot11NumberofHoppingSets}, GetNextIndexDot11PhyFHSSTable, Dot11NumberofHoppingSetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11HopModulus}, GetNextIndexDot11PhyFHSSTable, Dot11HopModulusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11HopOffset}, GetNextIndexDot11PhyFHSSTable, Dot11HopOffsetGet, Dot11HopOffsetSet, Dot11HopOffsetTest, Dot11PhyFHSSTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11PhyFHSSTableEntry = { 16, Dot11PhyFHSSTableMibEntry };

tMbDbEntry Dot11PhyDSSSTableMibEntry[]= {

{{8,Dot11CurrentChannel}, GetNextIndexDot11PhyDSSSTable, Dot11CurrentChannelGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyDSSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CCAModeSupported}, GetNextIndexDot11PhyDSSSTable, Dot11CCAModeSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyDSSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CurrentCCAMode}, GetNextIndexDot11PhyDSSSTable, Dot11CurrentCCAModeGet, Dot11CurrentCCAModeSet, Dot11CurrentCCAModeTest, Dot11PhyDSSSTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyDSSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11EDThreshold}, GetNextIndexDot11PhyDSSSTable, Dot11EDThresholdGet, Dot11EDThresholdSet, Dot11EDThresholdTest, Dot11PhyDSSSTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11PhyDSSSTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11PhyDSSSTableEntry = { 4, Dot11PhyDSSSTableMibEntry };

tMbDbEntry Dot11PhyIRTableMibEntry[]= {

{{8,Dot11CCAWatchdogTimerMax}, GetNextIndexDot11PhyIRTable, Dot11CCAWatchdogTimerMaxGet, Dot11CCAWatchdogTimerMaxSet, Dot11CCAWatchdogTimerMaxTest, Dot11PhyIRTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11PhyIRTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CCAWatchdogCountMax}, GetNextIndexDot11PhyIRTable, Dot11CCAWatchdogCountMaxGet, Dot11CCAWatchdogCountMaxSet, Dot11CCAWatchdogCountMaxTest, Dot11PhyIRTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11PhyIRTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CCAWatchdogTimerMin}, GetNextIndexDot11PhyIRTable, Dot11CCAWatchdogTimerMinGet, Dot11CCAWatchdogTimerMinSet, Dot11CCAWatchdogTimerMinTest, Dot11PhyIRTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11PhyIRTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CCAWatchdogCountMin}, GetNextIndexDot11PhyIRTable, Dot11CCAWatchdogCountMinGet, Dot11CCAWatchdogCountMinSet, Dot11CCAWatchdogCountMinTest, Dot11PhyIRTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11PhyIRTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11PhyIRTableEntry = { 4, Dot11PhyIRTableMibEntry };

tMbDbEntry Dot11RegDomainsSupportedTableMibEntry[]= {

{{8,Dot11RegDomainsSupportedIndex}, GetNextIndexDot11RegDomainsSupportedTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11RegDomainsSupportedTableINDEX, 2, 1, 0, NULL},

{{8,Dot11RegDomainsImplementedValue}, GetNextIndexDot11RegDomainsSupportedTable, Dot11RegDomainsImplementedValueGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11RegDomainsSupportedTableINDEX, 2, 1, 0, NULL},
};
tMibData Dot11RegDomainsSupportedTableEntry = { 2, Dot11RegDomainsSupportedTableMibEntry };

tMbDbEntry Dot11AntennasListTableMibEntry[]= {

{{8,Dot11AntennaListIndex}, GetNextIndexDot11AntennasListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11AntennasListTableINDEX, 2, 0, 0, NULL},

{{8,Dot11TxAntennaImplemented}, GetNextIndexDot11AntennasListTable, Dot11TxAntennaImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11AntennasListTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RxAntennaImplemented}, GetNextIndexDot11AntennasListTable, Dot11RxAntennaImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11AntennasListTableINDEX, 2, 0, 0, NULL},

{{8,Dot11DiversitySelectionRxImplemented}, GetNextIndexDot11AntennasListTable, Dot11DiversitySelectionRxImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11AntennasListTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11AntennasListTableEntry = { 4, Dot11AntennasListTableMibEntry };

tMbDbEntry Dot11SupportedDataRatesTxTableMibEntry[]= {

{{8,Dot11SupportedDataRatesTxIndex}, GetNextIndexDot11SupportedDataRatesTxTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11SupportedDataRatesTxTableINDEX, 2, 0, 0, NULL},

{{8,Dot11ImplementedDataRatesTxValue}, GetNextIndexDot11SupportedDataRatesTxTable, Dot11ImplementedDataRatesTxValueGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11SupportedDataRatesTxTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11SupportedDataRatesTxTableEntry = { 2, Dot11SupportedDataRatesTxTableMibEntry };

tMbDbEntry Dot11SupportedDataRatesRxTableMibEntry[]= {

{{8,Dot11SupportedDataRatesRxIndex}, GetNextIndexDot11SupportedDataRatesRxTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11SupportedDataRatesRxTableINDEX, 2, 0, 0, NULL},

{{8,Dot11ImplementedDataRatesRxValue}, GetNextIndexDot11SupportedDataRatesRxTable, Dot11ImplementedDataRatesRxValueGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11SupportedDataRatesRxTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11SupportedDataRatesRxTableEntry = { 2, Dot11SupportedDataRatesRxTableMibEntry };

tMbDbEntry Dot11PhyOFDMTableMibEntry[]= {

{{8,Dot11CurrentFrequency}, GetNextIndexDot11PhyOFDMTable, Dot11CurrentFrequencyGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyOFDMTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TIThreshold}, GetNextIndexDot11PhyOFDMTable, Dot11TIThresholdGet, Dot11TIThresholdSet, Dot11TIThresholdTest, Dot11PhyOFDMTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11PhyOFDMTableINDEX, 1, 1, 0, NULL},

{{8,Dot11FrequencyBandsImplemented}, GetNextIndexDot11PhyOFDMTable, Dot11FrequencyBandsImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyOFDMTableINDEX, 1, 1, 0, NULL},

{{8,Dot11ChannelStartingFactor}, GetNextIndexDot11PhyOFDMTable, Dot11ChannelStartingFactorGet, Dot11ChannelStartingFactorSet, Dot11ChannelStartingFactorTest, Dot11PhyOFDMTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11PhyOFDMTableINDEX, 1, 0, 0, "10000"},

{{8,Dot11FiveMHzOperationImplemented}, GetNextIndexDot11PhyOFDMTable, Dot11FiveMHzOperationImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyOFDMTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TenMHzOperationImplemented}, GetNextIndexDot11PhyOFDMTable, Dot11TenMHzOperationImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyOFDMTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TwentyMHzOperationImplemented}, GetNextIndexDot11PhyOFDMTable, Dot11TwentyMHzOperationImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyOFDMTableINDEX, 1, 0, 0, "1"},

{{8,Dot11PhyOFDMChannelWidth}, GetNextIndexDot11PhyOFDMTable, Dot11PhyOFDMChannelWidthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyOFDMTableINDEX, 1, 0, 0, NULL},

{{8,Dot11OFDMCCAEDImplemented}, GetNextIndexDot11PhyOFDMTable, Dot11OFDMCCAEDImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyOFDMTableINDEX, 1, 0, 0, NULL},

{{8,Dot11OFDMCCAEDRequired}, GetNextIndexDot11PhyOFDMTable, Dot11OFDMCCAEDRequiredGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyOFDMTableINDEX, 1, 0, 0, NULL},

{{8,Dot11OFDMEDThreshold}, GetNextIndexDot11PhyOFDMTable, Dot11OFDMEDThresholdGet, Dot11OFDMEDThresholdSet, Dot11OFDMEDThresholdTest, Dot11PhyOFDMTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11PhyOFDMTableINDEX, 1, 0, 0, NULL},

{{8,Dot11STATransmitPowerClass}, GetNextIndexDot11PhyOFDMTable, Dot11STATransmitPowerClassGet, Dot11STATransmitPowerClassSet, Dot11STATransmitPowerClassTest, Dot11PhyOFDMTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyOFDMTableINDEX, 1, 0, 0, "1"},

{{8,Dot11ACRType}, GetNextIndexDot11PhyOFDMTable, Dot11ACRTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyOFDMTableINDEX, 1, 0, 0, "1"},
};
tMibData Dot11PhyOFDMTableEntry = { 13, Dot11PhyOFDMTableMibEntry };

tMbDbEntry Dot11PhyHRDSSSTableMibEntry[]= {

{{8,Dot11ShortPreambleOptionImplemented}, GetNextIndexDot11PhyHRDSSSTable, Dot11ShortPreambleOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyHRDSSSTableINDEX, 1, 0, 0, "2"},

{{8,Dot11PBCCOptionImplemented}, GetNextIndexDot11PhyHRDSSSTable, Dot11PBCCOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyHRDSSSTableINDEX, 1, 0, 0, "2"},

{{8,Dot11ChannelAgilityPresent}, GetNextIndexDot11PhyHRDSSSTable, Dot11ChannelAgilityPresentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyHRDSSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ChannelAgilityActivated}, GetNextIndexDot11PhyHRDSSSTable, Dot11ChannelAgilityActivatedGet, Dot11ChannelAgilityActivatedSet, Dot11ChannelAgilityActivatedTest, Dot11PhyHRDSSSTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyHRDSSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11HRCCAModeImplemented}, GetNextIndexDot11PhyHRDSSSTable, Dot11HRCCAModeImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyHRDSSSTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11PhyHRDSSSTableEntry = { 5, Dot11PhyHRDSSSTableMibEntry };

tMbDbEntry Dot11HoppingPatternTableMibEntry[]= {

{{8,Dot11HoppingPatternIndex}, GetNextIndexDot11HoppingPatternTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11HoppingPatternTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RandomTableFieldNumber}, GetNextIndexDot11HoppingPatternTable, Dot11RandomTableFieldNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11HoppingPatternTableINDEX, 2, 0, 0, "0"},
};
tMibData Dot11HoppingPatternTableEntry = { 2, Dot11HoppingPatternTableMibEntry };

tMbDbEntry Dot11PhyERPTableMibEntry[]= {

{{8,Dot11ERPPBCCOptionImplemented}, GetNextIndexDot11PhyERPTable, Dot11ERPPBCCOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyERPTableINDEX, 1, 0, 0, "2"},

{{8,Dot11ERPBCCOptionActivated}, GetNextIndexDot11PhyERPTable, Dot11ERPBCCOptionActivatedGet, Dot11ERPBCCOptionActivatedSet, Dot11ERPBCCOptionActivatedTest, Dot11PhyERPTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyERPTableINDEX, 1, 0, 0, "2"},

{{8,Dot11DSSSOFDMOptionImplemented}, GetNextIndexDot11PhyERPTable, Dot11DSSSOFDMOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyERPTableINDEX, 1, 0, 0, "2"},

{{8,Dot11DSSSOFDMOptionActivated}, GetNextIndexDot11PhyERPTable, Dot11DSSSOFDMOptionActivatedGet, Dot11DSSSOFDMOptionActivatedSet, Dot11DSSSOFDMOptionActivatedTest, Dot11PhyERPTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyERPTableINDEX, 1, 0, 0, "2"},

{{8,Dot11ShortSlotTimeOptionImplemented}, GetNextIndexDot11PhyERPTable, Dot11ShortSlotTimeOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyERPTableINDEX, 1, 0, 0, "2"},

{{8,Dot11ShortSlotTimeOptionActivated}, GetNextIndexDot11PhyERPTable, Dot11ShortSlotTimeOptionActivatedGet, Dot11ShortSlotTimeOptionActivatedSet, Dot11ShortSlotTimeOptionActivatedTest, Dot11PhyERPTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyERPTableINDEX, 1, 0, 0, "2"},
};
tMibData Dot11PhyERPTableEntry = { 6, Dot11PhyERPTableMibEntry };

tMbDbEntry Dot11PhyHTTableMibEntry[]= {

{{8,Dot11FortyMHzOperationImplemented}, GetNextIndexDot11PhyHTTable, Dot11FortyMHzOperationImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyHTTableINDEX, 1, 0, 0, "2"},

{{8,Dot11FortyMHzOperationActivated}, GetNextIndexDot11PhyHTTable, Dot11FortyMHzOperationActivatedGet, Dot11FortyMHzOperationActivatedSet, Dot11FortyMHzOperationActivatedTest, Dot11PhyHTTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyHTTableINDEX, 1, 0, 0, "2"},

{{8,Dot11CurrentPrimaryChannel}, GetNextIndexDot11PhyHTTable, Dot11CurrentPrimaryChannelGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyHTTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CurrentSecondaryChannel}, GetNextIndexDot11PhyHTTable, Dot11CurrentSecondaryChannelGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyHTTableINDEX, 1, 0, 0, NULL},

{{8,Dot11NumberOfSpatialStreamsImplemented}, GetNextIndexDot11PhyHTTable, Dot11NumberOfSpatialStreamsImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyHTTableINDEX, 1, 0, 0, "2"},

{{8,Dot11NumberOfSpatialStreamsActivated}, GetNextIndexDot11PhyHTTable, Dot11NumberOfSpatialStreamsActivatedGet, Dot11NumberOfSpatialStreamsActivatedSet, Dot11NumberOfSpatialStreamsActivatedTest, Dot11PhyHTTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11PhyHTTableINDEX, 1, 0, 0, "2"},

{{8,Dot11HTGreenfieldOptionImplemented}, GetNextIndexDot11PhyHTTable, Dot11HTGreenfieldOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyHTTableINDEX, 1, 0, 0, "2"},

{{8,Dot11HTGreenfieldOptionActivated}, GetNextIndexDot11PhyHTTable, Dot11HTGreenfieldOptionActivatedGet, Dot11HTGreenfieldOptionActivatedSet, Dot11HTGreenfieldOptionActivatedTest, Dot11PhyHTTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyHTTableINDEX, 1, 0, 0, "2"},

{{8,Dot11ShortGIOptionInTwentyImplemented}, GetNextIndexDot11PhyHTTable, Dot11ShortGIOptionInTwentyImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyHTTableINDEX, 1, 0, 0, "2"},

{{8,Dot11ShortGIOptionInTwentyActivated}, GetNextIndexDot11PhyHTTable, Dot11ShortGIOptionInTwentyActivatedGet, Dot11ShortGIOptionInTwentyActivatedSet, Dot11ShortGIOptionInTwentyActivatedTest, Dot11PhyHTTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyHTTableINDEX, 1, 0, 0, "2"},

{{8,Dot11ShortGIOptionInFortyImplemented}, GetNextIndexDot11PhyHTTable, Dot11ShortGIOptionInFortyImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyHTTableINDEX, 1, 0, 0, "2"},

{{8,Dot11ShortGIOptionInFortyActivated}, GetNextIndexDot11PhyHTTable, Dot11ShortGIOptionInFortyActivatedGet, Dot11ShortGIOptionInFortyActivatedSet, Dot11ShortGIOptionInFortyActivatedTest, Dot11PhyHTTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyHTTableINDEX, 1, 0, 0, "2"},

{{8,Dot11LDPCCodingOptionImplemented}, GetNextIndexDot11PhyHTTable, Dot11LDPCCodingOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyHTTableINDEX, 1, 0, 0, "2"},

{{8,Dot11LDPCCodingOptionActivated}, GetNextIndexDot11PhyHTTable, Dot11LDPCCodingOptionActivatedGet, Dot11LDPCCodingOptionActivatedSet, Dot11LDPCCodingOptionActivatedTest, Dot11PhyHTTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyHTTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TxSTBCOptionImplemented}, GetNextIndexDot11PhyHTTable, Dot11TxSTBCOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyHTTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TxSTBCOptionActivated}, GetNextIndexDot11PhyHTTable, Dot11TxSTBCOptionActivatedGet, Dot11TxSTBCOptionActivatedSet, Dot11TxSTBCOptionActivatedTest, Dot11PhyHTTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyHTTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RxSTBCOptionImplemented}, GetNextIndexDot11PhyHTTable, Dot11RxSTBCOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyHTTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RxSTBCOptionActivated}, GetNextIndexDot11PhyHTTable, Dot11RxSTBCOptionActivatedGet, Dot11RxSTBCOptionActivatedSet, Dot11RxSTBCOptionActivatedTest, Dot11PhyHTTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyHTTableINDEX, 1, 0, 0, "2"},

{{8,Dot11BeamFormingOptionImplemented}, GetNextIndexDot11PhyHTTable, Dot11BeamFormingOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyHTTableINDEX, 1, 0, 0, "2"},

{{8,Dot11BeamFormingOptionActivated}, GetNextIndexDot11PhyHTTable, Dot11BeamFormingOptionActivatedGet, Dot11BeamFormingOptionActivatedSet, Dot11BeamFormingOptionActivatedTest, Dot11PhyHTTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyHTTableINDEX, 1, 0, 0, "2"},

{{8,Dot11HighestSupportedDataRate}, GetNextIndexDot11PhyHTTable, Dot11HighestSupportedDataRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyHTTableINDEX, 1, 0, 0, "0"},

{{8,Dot11TxMCSSetDefined}, GetNextIndexDot11PhyHTTable, Dot11TxMCSSetDefinedGet, Dot11TxMCSSetDefinedSet, Dot11TxMCSSetDefinedTest, Dot11PhyHTTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyHTTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TxRxMCSSetNotEqual}, GetNextIndexDot11PhyHTTable, Dot11TxRxMCSSetNotEqualGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyHTTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TxMaximumNumberSpatialStreamsSupported}, GetNextIndexDot11PhyHTTable, Dot11TxMaximumNumberSpatialStreamsSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11PhyHTTableINDEX, 1, 0, 0, "0"},

{{8,Dot11TxUnequalModulationSupported}, GetNextIndexDot11PhyHTTable, Dot11TxUnequalModulationSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyHTTableINDEX, 1, 0, 0, "2"},
};
tMibData Dot11PhyHTTableEntry = { 25, Dot11PhyHTTableMibEntry };

tMbDbEntry Dot11SupportedMCSTxTableMibEntry[]= {

{{8,Dot11SupportedMCSTxIndex}, GetNextIndexDot11SupportedMCSTxTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11SupportedMCSTxTableINDEX, 2, 0, 0, NULL},

{{8,Dot11SupportedMCSTxValue}, GetNextIndexDot11SupportedMCSTxTable, Dot11SupportedMCSTxValueGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11SupportedMCSTxTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11SupportedMCSTxTableEntry = { 2, Dot11SupportedMCSTxTableMibEntry };

tMbDbEntry Dot11SupportedMCSRxTableMibEntry[]= {

{{8,Dot11SupportedMCSRxIndex}, GetNextIndexDot11SupportedMCSRxTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11SupportedMCSRxTableINDEX, 2, 0, 0, NULL},

{{8,Dot11SupportedMCSRxValue}, GetNextIndexDot11SupportedMCSRxTable, Dot11SupportedMCSRxValueGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11SupportedMCSRxTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11SupportedMCSRxTableEntry = { 2, Dot11SupportedMCSRxTableMibEntry };

tMbDbEntry Dot11TransmitBeamformingConfigTableMibEntry[]= {

{{8,Dot11ReceiveStaggerSoundingOptionImplemented}, GetNextIndexDot11TransmitBeamformingConfigTable, Dot11ReceiveStaggerSoundingOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11TransmitBeamformingConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TransmitStaggerSoundingOptionImplemented}, GetNextIndexDot11TransmitBeamformingConfigTable, Dot11TransmitStaggerSoundingOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11TransmitBeamformingConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11ReceiveNDPOptionImplemented}, GetNextIndexDot11TransmitBeamformingConfigTable, Dot11ReceiveNDPOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11TransmitBeamformingConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11TransmitNDPOptionImplemented}, GetNextIndexDot11TransmitBeamformingConfigTable, Dot11TransmitNDPOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11TransmitBeamformingConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11ImplicitTransmitBeamformingOptionImplemented}, GetNextIndexDot11TransmitBeamformingConfigTable, Dot11ImplicitTransmitBeamformingOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11TransmitBeamformingConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11CalibrationOptionImplemented}, GetNextIndexDot11TransmitBeamformingConfigTable, Dot11CalibrationOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11TransmitBeamformingConfigTableINDEX, 1, 0, 0, "0"},

{{8,Dot11ExplicitCSITransmitBeamformingOptionImplemented}, GetNextIndexDot11TransmitBeamformingConfigTable, Dot11ExplicitCSITransmitBeamformingOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11TransmitBeamformingConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11ExplicitNonCompressedBeamformingMatrixOptionImplemented}, GetNextIndexDot11TransmitBeamformingConfigTable, Dot11ExplicitNonCompressedBeamformingMatrixOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11TransmitBeamformingConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11ExplicitTransmitBeamformingCSIFeedbackOptionImplemented}, GetNextIndexDot11TransmitBeamformingConfigTable, Dot11ExplicitTransmitBeamformingCSIFeedbackOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11TransmitBeamformingConfigTableINDEX, 1, 0, 0, "0"},

{{8,Dot11ExplicitNonCompressedBeamformingFeedbackOptionImplemented}, GetNextIndexDot11TransmitBeamformingConfigTable, Dot11ExplicitNonCompressedBeamformingFeedbackOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11TransmitBeamformingConfigTableINDEX, 1, 0, 0, "0"},

{{8,Dot11ExplicitCompressedBeamformingFeedbackOptionImplemented}, GetNextIndexDot11TransmitBeamformingConfigTable, Dot11ExplicitCompressedBeamformingFeedbackOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11TransmitBeamformingConfigTableINDEX, 1, 0, 0, "0"},

{{8,Dot11NumberBeamFormingCSISupportAntenna}, GetNextIndexDot11TransmitBeamformingConfigTable, Dot11NumberBeamFormingCSISupportAntennaGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11TransmitBeamformingConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11NumberNonCompressedBeamformingMatrixSupportAntenna}, GetNextIndexDot11TransmitBeamformingConfigTable, Dot11NumberNonCompressedBeamformingMatrixSupportAntennaGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11TransmitBeamformingConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11NumberCompressedBeamformingMatrixSupportAntenna}, GetNextIndexDot11TransmitBeamformingConfigTable, Dot11NumberCompressedBeamformingMatrixSupportAntennaGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11TransmitBeamformingConfigTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11TransmitBeamformingConfigTableEntry = { 14, Dot11TransmitBeamformingConfigTableMibEntry };

tMbDbEntry Dot11BSSIdTableMibEntry[]= {

{{8,Dot11APMacAddress}, GetNextIndexDot11BSSIdTable, Dot11APMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11BSSIdTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11BSSIdTableEntry = { 1, Dot11BSSIdTableMibEntry };

tMbDbEntry Dot11InterworkingTableMibEntry[]= {

{{8,Dot11NonAPStationMacAddress}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationUserIdentity}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationUserIdentityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationInterworkingCapability}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationInterworkingCapabilityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationAssociatedSSID}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationAssociatedSSIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationUnicastCipherSuite}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationUnicastCipherSuiteGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationBroadcastCipherSuite}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationBroadcastCipherSuiteGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationAuthAccessCategories}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationAuthAccessCategoriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationAuthMaxVoiceRate}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationAuthMaxVoiceRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, "2147483647"},

{{8,Dot11NonAPStationAuthMaxVideoRate}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationAuthMaxVideoRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, "2147483647"},

{{8,Dot11NonAPStationAuthMaxBestEffortRate}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationAuthMaxBestEffortRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, "2147483647"},

{{8,Dot11NonAPStationAuthMaxBackgroundRate}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationAuthMaxBackgroundRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, "2147483647"},

{{8,Dot11NonAPStationAuthMaxVoiceOctets}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationAuthMaxVoiceOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, "0"},

{{8,Dot11NonAPStationAuthMaxVideoOctets}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationAuthMaxVideoOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, "0"},

{{8,Dot11NonAPStationAuthMaxBestEffortOctets}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationAuthMaxBestEffortOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, "0"},

{{8,Dot11NonAPStationAuthMaxBackgroundOctets}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationAuthMaxBackgroundOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, "0"},

{{8,Dot11NonAPStationAuthMaxHCCAHEMMOctets}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationAuthMaxHCCAHEMMOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, "0"},

{{8,Dot11NonAPStationAuthMaxTotalOctets}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationAuthMaxTotalOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, "0"},

{{8,Dot11NonAPStationAuthHCCAHEMM}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationAuthHCCAHEMMGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, "1"},

{{8,Dot11NonAPStationAuthMaxHCCAHEMMRate}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationAuthMaxHCCAHEMMRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, "2147483647"},

{{8,Dot11NonAPStationAuthHCCAHEMMDelay}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationAuthHCCAHEMMDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, "2147483647"},

{{8,Dot11NonAPStationAuthSourceMulticast}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationAuthSourceMulticastGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, "1"},

{{8,Dot11NonAPStationAuthMaxSourceMulticastRate}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationAuthMaxSourceMulticastRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, "2147483647"},

{{8,Dot11NonAPStationVoiceMSDUCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationVoiceMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationDroppedVoiceMSDUCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationDroppedVoiceMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationVoiceOctetCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationVoiceOctetCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationDroppedVoiceOctetCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationDroppedVoiceOctetCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationVideoMSDUCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationVideoMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationDroppedVideoMSDUCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationDroppedVideoMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationVideoOctetCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationVideoOctetCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationDroppedVideoOctetCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationDroppedVideoOctetCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationBestEffortMSDUCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationBestEffortMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationDroppedBestEffortMSDUCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationDroppedBestEffortMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationBestEffortOctetCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationBestEffortOctetCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationDroppedBestEffortOctetCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationDroppedBestEffortOctetCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationBackgroundMSDUCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationBackgroundMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationDroppedBackgroundMSDUCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationDroppedBackgroundMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationBackgroundOctetCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationBackgroundOctetCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationDroppedBackgroundOctetCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationDroppedBackgroundOctetCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationHCCAHEMMMSDUCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationHCCAHEMMMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationDroppedHCCAHEMMMSDUCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationDroppedHCCAHEMMMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationHCCAHEMMOctetCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationHCCAHEMMOctetCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationDroppedHCCAHEMMOctetCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationDroppedHCCAHEMMOctetCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationMulticastMSDUCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationMulticastMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationDroppedMulticastMSDUCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationDroppedMulticastMSDUCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationMulticastOctetCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationMulticastOctetCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationDroppedMulticastOctetCount}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationDroppedMulticastOctetCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationPowerManagementMode}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationPowerManagementModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationAuthDls}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationAuthDlsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, "1"},

{{8,Dot11NonAPStationVLANId}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationVLANIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationVLANName}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationVLANNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NonAPStationAddtsResultCode}, GetNextIndexDot11InterworkingTable, Dot11NonAPStationAddtsResultCodeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11InterworkingTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11InterworkingTableEntry = { 51, Dot11InterworkingTableMibEntry };

tMbDbEntry Dot11APLCITableMibEntry[]= {

{{8,Dot11APLCIIndex}, GetNextIndexDot11APLCITable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11APLCITableINDEX, 1, 0, 0, NULL},

{{8,Dot11APLCILatitudeResolution}, GetNextIndexDot11APLCITable, Dot11APLCILatitudeResolutionGet, Dot11APLCILatitudeResolutionSet, Dot11APLCILatitudeResolutionTest, Dot11APLCITableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11APLCITableINDEX, 1, 0, 0, NULL},

{{8,Dot11APLCILatitudeInteger}, GetNextIndexDot11APLCITable, Dot11APLCILatitudeIntegerGet, Dot11APLCILatitudeIntegerSet, Dot11APLCILatitudeIntegerTest, Dot11APLCITableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11APLCITableINDEX, 1, 0, 0, NULL},

{{8,Dot11APLCILatitudeFraction}, GetNextIndexDot11APLCITable, Dot11APLCILatitudeFractionGet, Dot11APLCILatitudeFractionSet, Dot11APLCILatitudeFractionTest, Dot11APLCITableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11APLCITableINDEX, 1, 0, 0, NULL},

{{8,Dot11APLCILongitudeResolution}, GetNextIndexDot11APLCITable, Dot11APLCILongitudeResolutionGet, Dot11APLCILongitudeResolutionSet, Dot11APLCILongitudeResolutionTest, Dot11APLCITableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11APLCITableINDEX, 1, 0, 0, NULL},

{{8,Dot11APLCILongitudeInteger}, GetNextIndexDot11APLCITable, Dot11APLCILongitudeIntegerGet, Dot11APLCILongitudeIntegerSet, Dot11APLCILongitudeIntegerTest, Dot11APLCITableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11APLCITableINDEX, 1, 0, 0, NULL},

{{8,Dot11APLCILongitudeFraction}, GetNextIndexDot11APLCITable, Dot11APLCILongitudeFractionGet, Dot11APLCILongitudeFractionSet, Dot11APLCILongitudeFractionTest, Dot11APLCITableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11APLCITableINDEX, 1, 0, 0, NULL},

{{8,Dot11APLCIAltitudeType}, GetNextIndexDot11APLCITable, Dot11APLCIAltitudeTypeGet, Dot11APLCIAltitudeTypeSet, Dot11APLCIAltitudeTypeTest, Dot11APLCITableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11APLCITableINDEX, 1, 0, 0, NULL},

{{8,Dot11APLCIAltitudeResolution}, GetNextIndexDot11APLCITable, Dot11APLCIAltitudeResolutionGet, Dot11APLCIAltitudeResolutionSet, Dot11APLCIAltitudeResolutionTest, Dot11APLCITableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11APLCITableINDEX, 1, 0, 0, NULL},

{{8,Dot11APLCIAltitudeInteger}, GetNextIndexDot11APLCITable, Dot11APLCIAltitudeIntegerGet, Dot11APLCIAltitudeIntegerSet, Dot11APLCIAltitudeIntegerTest, Dot11APLCITableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11APLCITableINDEX, 1, 0, 0, NULL},

{{8,Dot11APLCIAltitudeFraction}, GetNextIndexDot11APLCITable, Dot11APLCIAltitudeFractionGet, Dot11APLCIAltitudeFractionSet, Dot11APLCIAltitudeFractionTest, Dot11APLCITableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11APLCITableINDEX, 1, 0, 0, NULL},

{{8,Dot11APLCIDatum}, GetNextIndexDot11APLCITable, Dot11APLCIDatumGet, Dot11APLCIDatumSet, Dot11APLCIDatumTest, Dot11APLCITableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11APLCITableINDEX, 1, 0, 0, NULL},

{{8,Dot11APLCIAzimuthType}, GetNextIndexDot11APLCITable, Dot11APLCIAzimuthTypeGet, Dot11APLCIAzimuthTypeSet, Dot11APLCIAzimuthTypeTest, Dot11APLCITableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11APLCITableINDEX, 1, 0, 0, NULL},

{{8,Dot11APLCIAzimuthResolution}, GetNextIndexDot11APLCITable, Dot11APLCIAzimuthResolutionGet, Dot11APLCIAzimuthResolutionSet, Dot11APLCIAzimuthResolutionTest, Dot11APLCITableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11APLCITableINDEX, 1, 0, 0, NULL},

{{8,Dot11APLCIAzimuth}, GetNextIndexDot11APLCITable, Dot11APLCIAzimuthGet, Dot11APLCIAzimuthSet, Dot11APLCIAzimuthTest, Dot11APLCITableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11APLCITableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11APLCITableEntry = { 15, Dot11APLCITableMibEntry };

tMbDbEntry Dot11APCivicLocationTableMibEntry[]= {

{{8,Dot11APCivicLocationIndex}, GetNextIndexDot11APCivicLocationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationCountry}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationCountryGet, Dot11APCivicLocationCountrySet, Dot11APCivicLocationCountryTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationA1}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationA1Get, Dot11APCivicLocationA1Set, Dot11APCivicLocationA1Test, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationA2}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationA2Get, Dot11APCivicLocationA2Set, Dot11APCivicLocationA2Test, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationA3}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationA3Get, Dot11APCivicLocationA3Set, Dot11APCivicLocationA3Test, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationA4}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationA4Get, Dot11APCivicLocationA4Set, Dot11APCivicLocationA4Test, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationA5}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationA5Get, Dot11APCivicLocationA5Set, Dot11APCivicLocationA5Test, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationA6}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationA6Get, Dot11APCivicLocationA6Set, Dot11APCivicLocationA6Test, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationPrd}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationPrdGet, Dot11APCivicLocationPrdSet, Dot11APCivicLocationPrdTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationPod}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationPodGet, Dot11APCivicLocationPodSet, Dot11APCivicLocationPodTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationSts}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationStsGet, Dot11APCivicLocationStsSet, Dot11APCivicLocationStsTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationHno}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationHnoGet, Dot11APCivicLocationHnoSet, Dot11APCivicLocationHnoTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationHns}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationHnsGet, Dot11APCivicLocationHnsSet, Dot11APCivicLocationHnsTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationLmk}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationLmkGet, Dot11APCivicLocationLmkSet, Dot11APCivicLocationLmkTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationLoc}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationLocGet, Dot11APCivicLocationLocSet, Dot11APCivicLocationLocTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationNam}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationNamGet, Dot11APCivicLocationNamSet, Dot11APCivicLocationNamTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationPc}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationPcGet, Dot11APCivicLocationPcSet, Dot11APCivicLocationPcTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationBld}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationBldGet, Dot11APCivicLocationBldSet, Dot11APCivicLocationBldTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationUnit}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationUnitGet, Dot11APCivicLocationUnitSet, Dot11APCivicLocationUnitTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationFlr}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationFlrGet, Dot11APCivicLocationFlrSet, Dot11APCivicLocationFlrTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationRoom}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationRoomGet, Dot11APCivicLocationRoomSet, Dot11APCivicLocationRoomTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationPlc}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationPlcGet, Dot11APCivicLocationPlcSet, Dot11APCivicLocationPlcTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationPcn}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationPcnGet, Dot11APCivicLocationPcnSet, Dot11APCivicLocationPcnTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationPobox}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationPoboxGet, Dot11APCivicLocationPoboxSet, Dot11APCivicLocationPoboxTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationAddcode}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationAddcodeGet, Dot11APCivicLocationAddcodeSet, Dot11APCivicLocationAddcodeTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationSeat}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationSeatGet, Dot11APCivicLocationSeatSet, Dot11APCivicLocationSeatTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationRd}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationRdGet, Dot11APCivicLocationRdSet, Dot11APCivicLocationRdTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationRdsec}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationRdsecGet, Dot11APCivicLocationRdsecSet, Dot11APCivicLocationRdsecTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationRdbr}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationRdbrGet, Dot11APCivicLocationRdbrSet, Dot11APCivicLocationRdbrTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationRdsubbr}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationRdsubbrGet, Dot11APCivicLocationRdsubbrSet, Dot11APCivicLocationRdsubbrTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationPrm}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationPrmGet, Dot11APCivicLocationPrmSet, Dot11APCivicLocationPrmTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APCivicLocationPom}, GetNextIndexDot11APCivicLocationTable, Dot11APCivicLocationPomGet, Dot11APCivicLocationPomSet, Dot11APCivicLocationPomTest, Dot11APCivicLocationTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11APCivicLocationTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11APCivicLocationTableEntry = { 32, Dot11APCivicLocationTableMibEntry };

tMbDbEntry Dot11RoamingConsortiumTableMibEntry[]= {

{{8,Dot11RoamingConsortiumOI}, GetNextIndexDot11RoamingConsortiumTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Dot11RoamingConsortiumTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RoamingConsortiumRowStatus}, GetNextIndexDot11RoamingConsortiumTable, Dot11RoamingConsortiumRowStatusGet, Dot11RoamingConsortiumRowStatusSet, Dot11RoamingConsortiumRowStatusTest, Dot11RoamingConsortiumTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RoamingConsortiumTableINDEX, 1, 0, 1, NULL},
};
tMibData Dot11RoamingConsortiumTableEntry = { 2, Dot11RoamingConsortiumTableMibEntry };

tMbDbEntry Dot11DomainNameTableMibEntry[]= {

{{8,Dot11DomainName}, GetNextIndexDot11DomainNameTable, Dot11DomainNameGet, Dot11DomainNameSet, Dot11DomainNameTest, Dot11DomainNameTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11DomainNameTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DomainNameRowStatus}, GetNextIndexDot11DomainNameTable, Dot11DomainNameRowStatusGet, Dot11DomainNameRowStatusSet, Dot11DomainNameRowStatusTest, Dot11DomainNameTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11DomainNameTableINDEX, 1, 0, 1, NULL},

{{8,Dot11DomainNameOui}, GetNextIndexDot11DomainNameTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Dot11DomainNameTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11DomainNameTableEntry = { 3, Dot11DomainNameTableMibEntry };

tMbDbEntry Dot11GASAdvertisementTableMibEntry[]= {

{{8,Dot11GASAdvertisementId}, GetNextIndexDot11GASAdvertisementTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11GASAdvertisementTableINDEX, 1, 0, 0, NULL},

{{8,Dot11GASPauseForServerResponse}, GetNextIndexDot11GASAdvertisementTable, Dot11GASPauseForServerResponseGet, Dot11GASPauseForServerResponseSet, Dot11GASPauseForServerResponseTest, Dot11GASAdvertisementTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11GASAdvertisementTableINDEX, 1, 0, 0, NULL},

{{8,Dot11GASResponseTimeout}, GetNextIndexDot11GASAdvertisementTable, Dot11GASResponseTimeoutGet, Dot11GASResponseTimeoutSet, Dot11GASResponseTimeoutTest, Dot11GASAdvertisementTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11GASAdvertisementTableINDEX, 1, 0, 0, "5000"},

{{8,Dot11GASComebackDelay}, GetNextIndexDot11GASAdvertisementTable, Dot11GASComebackDelayGet, Dot11GASComebackDelaySet, Dot11GASComebackDelayTest, Dot11GASAdvertisementTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11GASAdvertisementTableINDEX, 1, 0, 0, "1000"},

{{8,Dot11GASResponseBufferingTime}, GetNextIndexDot11GASAdvertisementTable, Dot11GASResponseBufferingTimeGet, Dot11GASResponseBufferingTimeSet, Dot11GASResponseBufferingTimeTest, Dot11GASAdvertisementTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11GASAdvertisementTableINDEX, 1, 0, 0, "1000"},

{{8,Dot11GASQueryResponseLengthLimit}, GetNextIndexDot11GASAdvertisementTable, Dot11GASQueryResponseLengthLimitGet, Dot11GASQueryResponseLengthLimitSet, Dot11GASQueryResponseLengthLimitTest, Dot11GASAdvertisementTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11GASAdvertisementTableINDEX, 1, 0, 0, NULL},

{{8,Dot11GASQueries}, GetNextIndexDot11GASAdvertisementTable, Dot11GASQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11GASAdvertisementTableINDEX, 1, 0, 0, NULL},

{{8,Dot11GASQueryRate}, GetNextIndexDot11GASAdvertisementTable, Dot11GASQueryRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, Dot11GASAdvertisementTableINDEX, 1, 0, 0, NULL},

{{8,Dot11GASResponses}, GetNextIndexDot11GASAdvertisementTable, Dot11GASResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11GASAdvertisementTableINDEX, 1, 0, 0, NULL},

{{8,Dot11GASResponseRate}, GetNextIndexDot11GASAdvertisementTable, Dot11GASResponseRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, Dot11GASAdvertisementTableINDEX, 1, 0, 0, NULL},

{{8,Dot11GASTransmittedFragmentCount}, GetNextIndexDot11GASAdvertisementTable, Dot11GASTransmittedFragmentCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11GASAdvertisementTableINDEX, 1, 0, 0, NULL},

{{8,Dot11GASReceivedFragmentCount}, GetNextIndexDot11GASAdvertisementTable, Dot11GASReceivedFragmentCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11GASAdvertisementTableINDEX, 1, 0, 0, NULL},

{{8,Dot11GASNoRequestOutstanding}, GetNextIndexDot11GASAdvertisementTable, Dot11GASNoRequestOutstandingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11GASAdvertisementTableINDEX, 1, 0, 0, NULL},

{{8,Dot11GASResponsesDiscarded}, GetNextIndexDot11GASAdvertisementTable, Dot11GASResponsesDiscardedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11GASAdvertisementTableINDEX, 1, 0, 0, NULL},

{{8,Dot11GASFailedResponses}, GetNextIndexDot11GASAdvertisementTable, Dot11GASFailedResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11GASAdvertisementTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11GASAdvertisementTableEntry = { 15, Dot11GASAdvertisementTableMibEntry };

tMbDbEntry Dot11MACStateConfigTableMibEntry[]= {

{{8,Dot11ESSDisconnectFilterInterval}, GetNextIndexDot11MACStateConfigTable, Dot11ESSDisconnectFilterIntervalGet, Dot11ESSDisconnectFilterIntervalSet, Dot11ESSDisconnectFilterIntervalTest, Dot11MACStateConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MACStateConfigTableINDEX, 2, 0, 0, NULL},

{{8,Dot11ESSLinkDetectionHoldInterval}, GetNextIndexDot11MACStateConfigTable, Dot11ESSLinkDetectionHoldIntervalGet, Dot11ESSLinkDetectionHoldIntervalSet, Dot11ESSLinkDetectionHoldIntervalTest, Dot11MACStateConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MACStateConfigTableINDEX, 2, 0, 0, NULL},

{{8,Dot11MSCEESSLinkIdentifier}, GetNextIndexDot11MACStateConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Dot11MACStateConfigTableINDEX, 2, 0, 0, NULL},

{{8,Dot11MSCENonAPStationMacAddress}, GetNextIndexDot11MACStateConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, Dot11MACStateConfigTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11MACStateConfigTableEntry = { 4, Dot11MACStateConfigTableMibEntry };

tMbDbEntry Dot11MACStateParameterTableMibEntry[]= {

{{8,Dot11ESSLinkDownTimeInterval}, GetNextIndexDot11MACStateParameterTable, Dot11ESSLinkDownTimeIntervalGet, Dot11ESSLinkDownTimeIntervalSet, Dot11ESSLinkDownTimeIntervalTest, Dot11MACStateParameterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MACStateParameterTableINDEX, 2, 0, 0, NULL},

{{8,Dot11ESSLinkRssiDataThreshold}, GetNextIndexDot11MACStateParameterTable, Dot11ESSLinkRssiDataThresholdGet, Dot11ESSLinkRssiDataThresholdSet, Dot11ESSLinkRssiDataThresholdTest, Dot11MACStateParameterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MACStateParameterTableINDEX, 2, 0, 0, NULL},

{{8,Dot11ESSLinkRssiBeaconThreshold}, GetNextIndexDot11MACStateParameterTable, Dot11ESSLinkRssiBeaconThresholdGet, Dot11ESSLinkRssiBeaconThresholdSet, Dot11ESSLinkRssiBeaconThresholdTest, Dot11MACStateParameterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MACStateParameterTableINDEX, 2, 0, 0, NULL},

{{8,Dot11ESSLinkBeaconSnrThreshold}, GetNextIndexDot11MACStateParameterTable, Dot11ESSLinkBeaconSnrThresholdGet, Dot11ESSLinkBeaconSnrThresholdSet, Dot11ESSLinkBeaconSnrThresholdTest, Dot11MACStateParameterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MACStateParameterTableINDEX, 2, 0, 0, NULL},

{{8,Dot11ESSLinkDataSnrThreshold}, GetNextIndexDot11MACStateParameterTable, Dot11ESSLinkDataSnrThresholdGet, Dot11ESSLinkDataSnrThresholdSet, Dot11ESSLinkDataSnrThresholdTest, Dot11MACStateParameterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MACStateParameterTableINDEX, 2, 0, 0, NULL},

{{8,Dot11ESSLinkBeaconFrameErrorRateThresholdInteger}, GetNextIndexDot11MACStateParameterTable, Dot11ESSLinkBeaconFrameErrorRateThresholdIntegerGet, Dot11ESSLinkBeaconFrameErrorRateThresholdIntegerSet, Dot11ESSLinkBeaconFrameErrorRateThresholdIntegerTest, Dot11MACStateParameterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MACStateParameterTableINDEX, 2, 0, 0, NULL},

{{8,Dot11ESSLinkBeaconFrameErrorRateThresholdFraction}, GetNextIndexDot11MACStateParameterTable, Dot11ESSLinkBeaconFrameErrorRateThresholdFractionGet, Dot11ESSLinkBeaconFrameErrorRateThresholdFractionSet, Dot11ESSLinkBeaconFrameErrorRateThresholdFractionTest, Dot11MACStateParameterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MACStateParameterTableINDEX, 2, 0, 0, NULL},

{{8,Dot11ESSLinkBeaconFrameErrorRateThresholdExponent}, GetNextIndexDot11MACStateParameterTable, Dot11ESSLinkBeaconFrameErrorRateThresholdExponentGet, Dot11ESSLinkBeaconFrameErrorRateThresholdExponentSet, Dot11ESSLinkBeaconFrameErrorRateThresholdExponentTest, Dot11MACStateParameterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MACStateParameterTableINDEX, 2, 0, 0, NULL},

{{8,Dot11ESSLinkFrameErrorRateThresholdInteger}, GetNextIndexDot11MACStateParameterTable, Dot11ESSLinkFrameErrorRateThresholdIntegerGet, Dot11ESSLinkFrameErrorRateThresholdIntegerSet, Dot11ESSLinkFrameErrorRateThresholdIntegerTest, Dot11MACStateParameterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MACStateParameterTableINDEX, 2, 0, 0, NULL},

{{8,Dot11ESSLinkFrameErrorRateThresholdFraction}, GetNextIndexDot11MACStateParameterTable, Dot11ESSLinkFrameErrorRateThresholdFractionGet, Dot11ESSLinkFrameErrorRateThresholdFractionSet, Dot11ESSLinkFrameErrorRateThresholdFractionTest, Dot11MACStateParameterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MACStateParameterTableINDEX, 2, 0, 0, NULL},

{{8,Dot11ESSLinkFrameErrorRateThresholdExponent}, GetNextIndexDot11MACStateParameterTable, Dot11ESSLinkFrameErrorRateThresholdExponentGet, Dot11ESSLinkFrameErrorRateThresholdExponentSet, Dot11ESSLinkFrameErrorRateThresholdExponentTest, Dot11MACStateParameterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MACStateParameterTableINDEX, 2, 0, 0, NULL},

{{8,Dot11PeakOperationalRate}, GetNextIndexDot11MACStateParameterTable, Dot11PeakOperationalRateGet, Dot11PeakOperationalRateSet, Dot11PeakOperationalRateTest, Dot11MACStateParameterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MACStateParameterTableINDEX, 2, 0, 0, NULL},

{{8,Dot11MinimumOperationalRate}, GetNextIndexDot11MACStateParameterTable, Dot11MinimumOperationalRateGet, Dot11MinimumOperationalRateSet, Dot11MinimumOperationalRateTest, Dot11MACStateParameterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MACStateParameterTableINDEX, 2, 0, 0, NULL},

{{8,Dot11ESSLinkDataThroughputInteger}, GetNextIndexDot11MACStateParameterTable, Dot11ESSLinkDataThroughputIntegerGet, Dot11ESSLinkDataThroughputIntegerSet, Dot11ESSLinkDataThroughputIntegerTest, Dot11MACStateParameterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MACStateParameterTableINDEX, 2, 0, 0, NULL},

{{8,Dot11ESSLinkDataThroughputFraction}, GetNextIndexDot11MACStateParameterTable, Dot11ESSLinkDataThroughputFractionGet, Dot11ESSLinkDataThroughputFractionSet, Dot11ESSLinkDataThroughputFractionTest, Dot11MACStateParameterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MACStateParameterTableINDEX, 2, 0, 0, NULL},

{{8,Dot11ESSLinkDataThroughputExponent}, GetNextIndexDot11MACStateParameterTable, Dot11ESSLinkDataThroughputExponentGet, Dot11ESSLinkDataThroughputExponentSet, Dot11ESSLinkDataThroughputExponentTest, Dot11MACStateParameterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11MACStateParameterTableINDEX, 2, 0, 0, NULL},

{{8,Dot11MSPEESSLinkIdentifier}, GetNextIndexDot11MACStateParameterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Dot11MACStateParameterTableINDEX, 2, 0, 0, NULL},

{{8,Dot11MSPENonAPStationMacAddress}, GetNextIndexDot11MACStateParameterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, Dot11MACStateParameterTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11MACStateParameterTableEntry = { 18, Dot11MACStateParameterTableMibEntry };

tMbDbEntry Dot11MACStateESSLinkDetectedTableMibEntry[]= {

{{8,Dot11ESSLinkDetectedIndex}, GetNextIndexDot11MACStateESSLinkDetectedTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11MACStateESSLinkDetectedTableINDEX, 2, 0, 0, NULL},

{{8,Dot11ESSLinkDetectedNetworkId}, GetNextIndexDot11MACStateESSLinkDetectedTable, Dot11ESSLinkDetectedNetworkIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11MACStateESSLinkDetectedTableINDEX, 2, 0, 0, NULL},

{{8,Dot11ESSLinkDetectedNetworkDetectTime}, GetNextIndexDot11MACStateESSLinkDetectedTable, Dot11ESSLinkDetectedNetworkDetectTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11MACStateESSLinkDetectedTableINDEX, 2, 0, 0, NULL},

{{8,Dot11ESSLinkDetectedNetworkModifiedTime}, GetNextIndexDot11MACStateESSLinkDetectedTable, Dot11ESSLinkDetectedNetworkModifiedTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11MACStateESSLinkDetectedTableINDEX, 2, 0, 0, NULL},

{{8,Dot11ESSLinkDetectedNetworkMIHCapabilities}, GetNextIndexDot11MACStateESSLinkDetectedTable, Dot11ESSLinkDetectedNetworkMIHCapabilitiesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11MACStateESSLinkDetectedTableINDEX, 2, 0, 0, NULL},

{{8,Dot11MSELDEESSLinkIdentifier}, GetNextIndexDot11MACStateESSLinkDetectedTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Dot11MACStateESSLinkDetectedTableINDEX, 2, 0, 0, NULL},

{{8,Dot11MSELDENonAPStationMacAddress}, GetNextIndexDot11MACStateESSLinkDetectedTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, Dot11MACStateESSLinkDetectedTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11MACStateESSLinkDetectedTableEntry = { 7, Dot11MACStateESSLinkDetectedTableMibEntry };

#endif /* _STD802DB_H */

