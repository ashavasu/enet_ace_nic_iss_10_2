/* $Id: wsscfglock.h,v 1.1.1.1 2016/09/20 10:42:39 siva Exp $*/

#ifndef __WSSCFG_DEF_H__
#define __WSSCFG_DEF_H__


#define CONFIG_LOCK() WssCfgLock ()
#define CONFIG_UNLOCK() WssCfgUnLock ()

#endif

