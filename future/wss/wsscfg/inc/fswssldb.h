#ifndef _FSWSSLDB_H
#define _FSWSSLDB_H

UINT1 FsWlanRadioIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsDot11WtpDhcpSrvSubnetPoolConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsWtpNATTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsWtpDhcpConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsWtpFirewallConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsWtpIpRouteConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsWtpFwlFilterTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsWtpFwlRuleTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsWtpFwlAclTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsWtpNatDynamicTransTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsWtpNatGlobalAddressTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsWtpNatLocalAddressTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsWtpNatStaticTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsWtpNatStaticNaptTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsWtpNatIfTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsWtpVlanStaticTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsWtpVlanStaticPortConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsWtpIfMainTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};

UINT4 fswssl [] ={1,3,6,1,4,1,29601,2,109};
tSNMP_OID_TYPE fswsslOID = {9, fswssl};


UINT4 FsWlanDhcpServerStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,1};
UINT4 FsWlanDhcpRelayStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,2};
UINT4 FsWlanDhcpNextSrvIpAddr [ ] ={1,3,6,1,4,1,29601,2,109,1,3};
UINT4 FsWlanFirewallStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,4};
UINT4 FsWlanNATStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,5};
UINT4 FsWlanDNSServerIpAddr [ ] ={1,3,6,1,4,1,29601,2,109,1,6};
UINT4 FsWlanDefaultRouterIpAddr [ ] ={1,3,6,1,4,1,29601,2,109,1,7};
UINT4 FsWlanRadioIfIpAddr [ ] ={1,3,6,1,4,1,29601,2,109,1,8,1,1};
UINT4 FsWlanRadioIfIpSubnetMask [ ] ={1,3,6,1,4,1,29601,2,109,1,8,1,2};
UINT4 FsWlanRadioIfAdminStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,8,1,3};
UINT4 FsWlanRadioIfRowStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,8,1,4};
UINT4 FsDot11WtpDhcpSrvSubnetPoolIndex [ ] ={1,3,6,1,4,1,29601,2,109,1,9,1,1};
UINT4 FsDot11WtpDhcpSrvSubnetSubnet [ ] ={1,3,6,1,4,1,29601,2,109,1,9,1,2};
UINT4 FsDot11WtpDhcpSrvSubnetMask [ ] ={1,3,6,1,4,1,29601,2,109,1,9,1,3};
UINT4 FsDot11WtpDhcpSrvSubnetStartIpAddress [ ] ={1,3,6,1,4,1,29601,2,109,1,9,1,4};
UINT4 FsDot11WtpDhcpSrvSubnetEndIpAddress [ ] ={1,3,6,1,4,1,29601,2,109,1,9,1,5};
UINT4 FsDot11WtpDhcpSrvSubnetLeaseTime [ ] ={1,3,6,1,4,1,29601,2,109,1,9,1,6};
UINT4 FsDot11WtpDhcpSrvDefaultRouter [ ] ={1,3,6,1,4,1,29601,2,109,1,9,1,7};
UINT4 FsDot11WtpDhcpSrvDnsServerIp [ ] ={1,3,6,1,4,1,29601,2,109,1,9,1,8};
UINT4 FsDot11WtpDhcpSrvSubnetPoolRowStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,9,1,9};
UINT4 FsWtpNATConfigIndex [ ] ={1,3,6,1,4,1,29601,2,109,1,10,1,1};
UINT4 FsWtpIfMainWanType [ ] ={1,3,6,1,4,1,29601,2,109,1,10,1,2};
UINT4 FsWtpNATConfigRowStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,10,1,3};
UINT4 FsWtpDhcpServerStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,11,1,1};
UINT4 FsWtpDhcpRelayStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,11,1,2};
UINT4 FsWtpFirewallStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,12,1,1};
UINT4 FsWtpFirewallRowStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,12,1,2};
UINT4 FsWtpIpRouteConfigSubnet [ ] ={1,3,6,1,4,1,29601,2,109,1,13,1,1};
UINT4 FsWtpIpRouteConfigNetMask [ ] ={1,3,6,1,4,1,29601,2,109,1,13,1,2};
UINT4 FsWtpIpRouteConfigGateway [ ] ={1,3,6,1,4,1,29601,2,109,1,13,1,3};
UINT4 FsWtpIpRouteConfigRowStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,13,1,4};
UINT4 FsWtpFwlFilterName [ ] ={1,3,6,1,4,1,29601,2,109,1,14,1,1};
UINT4 FsWtpFwlFilterSrcAddress [ ] ={1,3,6,1,4,1,29601,2,109,1,14,1,2};
UINT4 FsWtpFwlFilterDestAddress [ ] ={1,3,6,1,4,1,29601,2,109,1,14,1,3};
UINT4 FsWtpFwlFilterProtocol [ ] ={1,3,6,1,4,1,29601,2,109,1,14,1,4};
UINT4 FsWtpFwlFilterSrcPort [ ] ={1,3,6,1,4,1,29601,2,109,1,14,1,5};
UINT4 FsWtpFwlFilterDestPort [ ] ={1,3,6,1,4,1,29601,2,109,1,14,1,6};
UINT4 FsWtpFwlFilterTos [ ] ={1,3,6,1,4,1,29601,2,109,1,14,1,7};
UINT4 FsWtpFwlFilterAccounting [ ] ={1,3,6,1,4,1,29601,2,109,1,14,1,8};
UINT4 FsWtpFwlFilterHitClear [ ] ={1,3,6,1,4,1,29601,2,109,1,14,1,9};
UINT4 FsWtpFwlFilterHitsCount [ ] ={1,3,6,1,4,1,29601,2,109,1,14,1,10};
UINT4 FsWtpFwlFilterAddrType [ ] ={1,3,6,1,4,1,29601,2,109,1,14,1,11};
UINT4 FsWtpFwlFilterFlowId [ ] ={1,3,6,1,4,1,29601,2,109,1,14,1,12};
UINT4 FsWtpFwlFilterDscp [ ] ={1,3,6,1,4,1,29601,2,109,1,14,1,13};
UINT4 FsWtpFwlFilterRowStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,14,1,14};
UINT4 FsWtpFwlRuleName [ ] ={1,3,6,1,4,1,29601,2,109,1,15,1,1};
UINT4 FsWtpFwlRuleFilterSet [ ] ={1,3,6,1,4,1,29601,2,109,1,15,1,2};
UINT4 FsWtpFwlRuleRowStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,15,1,3};
UINT4 FsWtpFwlAclIfIndex [ ] ={1,3,6,1,4,1,29601,2,109,1,16,1,1};
UINT4 FsWtpFwlAclName [ ] ={1,3,6,1,4,1,29601,2,109,1,16,1,2};
UINT4 FsWtpFwlAclDirection [ ] ={1,3,6,1,4,1,29601,2,109,1,16,1,3};
UINT4 FsWtpFwlAclAction [ ] ={1,3,6,1,4,1,29601,2,109,1,16,1,4};
UINT4 FsWtpFwlAclSequenceNumber [ ] ={1,3,6,1,4,1,29601,2,109,1,16,1,5};
UINT4 FsWtpFwlAclLogTrigger [ ] ={1,3,6,1,4,1,29601,2,109,1,16,1,6};
UINT4 FsWtpFwlAclFragAction [ ] ={1,3,6,1,4,1,29601,2,109,1,16,1,7};
UINT4 FsWtpFwlAclRowStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,16,1,8};
UINT4 FsWtpNatDynamicTransInterfaceNum [ ] ={1,3,6,1,4,1,29601,2,109,1,17,1,1};
UINT4 FsWtpNatDynamicTransLocalIp [ ] ={1,3,6,1,4,1,29601,2,109,1,17,1,2};
UINT4 FsWtpNatDynamicTransTranslatedLocalIp [ ] ={1,3,6,1,4,1,29601,2,109,1,17,1,3};
UINT4 FsWtpNatDynamicTransLocalPort [ ] ={1,3,6,1,4,1,29601,2,109,1,17,1,4};
UINT4 FsWtpNatDynamicTransTranslatedLocalPort [ ] ={1,3,6,1,4,1,29601,2,109,1,17,1,5};
UINT4 FsWtpNatDynamicTransOutsideIp [ ] ={1,3,6,1,4,1,29601,2,109,1,17,1,6};
UINT4 FsWtpNatDynamicTransOutsidePort [ ] ={1,3,6,1,4,1,29601,2,109,1,17,1,7};
UINT4 FsWtpNatDynamicTransLastUseTime [ ] ={1,3,6,1,4,1,29601,2,109,1,17,1,8};
UINT4 FsWtpNatGlobalAddressInterfaceNum [ ] ={1,3,6,1,4,1,29601,2,109,1,18,1,1};
UINT4 FsWtpNatGlobalAddressTranslatedLocalIp [ ] ={1,3,6,1,4,1,29601,2,109,1,18,1,2};
UINT4 FsWtpNatGlobalAddressMask [ ] ={1,3,6,1,4,1,29601,2,109,1,18,1,3};
UINT4 FsWtpNatGlobalAddressEntryStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,18,1,4};
UINT4 FsWtpNatLocalAddressInterfaceNumber [ ] ={1,3,6,1,4,1,29601,2,109,1,19,1,1};
UINT4 FsWtpNatLocalAddressLocalIp [ ] ={1,3,6,1,4,1,29601,2,109,1,19,1,2};
UINT4 FsWtpNatLocalAddressMask [ ] ={1,3,6,1,4,1,29601,2,109,1,19,1,3};
UINT4 FsWtpNatLocalAddressEntryStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,19,1,4};
UINT4 FsWtpNatStaticInterfaceNum [ ] ={1,3,6,1,4,1,29601,2,109,1,20,1,1};
UINT4 FsWtpNatStaticLocalIp [ ] ={1,3,6,1,4,1,29601,2,109,1,20,1,2};
UINT4 FsWtpNatStaticTranslatedLocalIp [ ] ={1,3,6,1,4,1,29601,2,109,1,20,1,3};
UINT4 FsWtpNatStaticEntryStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,20,1,4};
UINT4 FsWtpNatStaticNaptInterfaceNum [ ] ={1,3,6,1,4,1,29601,2,109,1,21,1,1};
UINT4 FsWtpNatStaticNaptLocalIp [ ] ={1,3,6,1,4,1,29601,2,109,1,21,1,2};
UINT4 FsWtpNatStaticNaptStartLocalPort [ ] ={1,3,6,1,4,1,29601,2,109,1,21,1,3};
UINT4 FsWtpNatStaticNaptEndLocalPort [ ] ={1,3,6,1,4,1,29601,2,109,1,21,1,4};
UINT4 FsWtpNatStaticNaptProtocolNumber [ ] ={1,3,6,1,4,1,29601,2,109,1,21,1,5};
UINT4 FsWtpNatStaticNaptTranslatedLocalIp [ ] ={1,3,6,1,4,1,29601,2,109,1,21,1,6};
UINT4 FsWtpNatStaticNaptTranslatedLocalPort [ ] ={1,3,6,1,4,1,29601,2,109,1,21,1,7};
UINT4 FsWtpNatStaticNaptDescription [ ] ={1,3,6,1,4,1,29601,2,109,1,21,1,8};
UINT4 FsWtpNatStaticNaptEntryStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,21,1,9};
UINT4 FsWtpNatIfInterfaceNumber [ ] ={1,3,6,1,4,1,29601,2,109,1,22,1,1};
UINT4 FsWtpNatIfNat [ ] ={1,3,6,1,4,1,29601,2,109,1,22,1,2};
UINT4 FsWtpNatIfNapt [ ] ={1,3,6,1,4,1,29601,2,109,1,22,1,3};
UINT4 FsWtpNatIfTwoWayNat [ ] ={1,3,6,1,4,1,29601,2,109,1,22,1,4};
UINT4 FsWtpNatIfEntryStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,22,1,5};
UINT4 FsWtpVlanContextId [ ] ={1,3,6,1,4,1,29601,2,109,1,23,1,1};
UINT4 FsWtpVlanIndex [ ] ={1,3,6,1,4,1,29601,2,109,1,23,1,2};
UINT4 FsWtpVlanStaticName [ ] ={1,3,6,1,4,1,29601,2,109,1,23,1,3};
UINT4 FsWtpVlanStaticRowStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,23,1,4};
UINT4 FsWtpTpPort [ ] ={1,3,6,1,4,1,29601,2,109,1,24,1,1};
UINT4 FsWtpVlanStaticPort [ ] ={1,3,6,1,4,1,29601,2,109,1,24,1,2};
UINT4 FsWtpIfMainIndex [ ] ={1,3,6,1,4,1,29601,2,109,1,25,1,1};
UINT4 FsWtpIfMainType [ ] ={1,3,6,1,4,1,29601,2,109,1,25,1,2};
UINT4 FsWtpIfMainAdminStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,25,1,3};
UINT4 FsWtpIfMainOperStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,25,1,4};
UINT4 FsWtpIfMainRowStatus [ ] ={1,3,6,1,4,1,29601,2,109,1,25,1,5};
UINT4 FsWtpIfMainNetworkType [ ] ={1,3,6,1,4,1,29601,2,109,1,25,1,6};
UINT4 FsWtpIfMainEncapDot1qVlanId [ ] ={1,3,6,1,4,1,29601,2,109,1,25,1,7};
UINT4 FsWtpIfIpAddr [ ] ={1,3,6,1,4,1,29601,2,109,1,25,1,8};
UINT4 FsWtpIfIpSubnetMask [ ] ={1,3,6,1,4,1,29601,2,109,1,25,1,9};
UINT4 FsWtpIfIpBroadcastAddr [ ] ={1,3,6,1,4,1,29601,2,109,1,25,1,10};
UINT4 FsWtpIfMainPhyPort [ ] ={1,3,6,1,4,1,29601,2,109,1,25,1,11};




tMbDbEntry fswsslMibEntry[]= {

{{11,FsWlanDhcpServerStatus}, NULL, FsWlanDhcpServerStatusGet, FsWlanDhcpServerStatusSet, FsWlanDhcpServerStatusTest, FsWlanDhcpServerStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsWlanDhcpRelayStatus}, NULL, FsWlanDhcpRelayStatusGet, FsWlanDhcpRelayStatusSet, FsWlanDhcpRelayStatusTest, FsWlanDhcpRelayStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsWlanDhcpNextSrvIpAddr}, NULL, FsWlanDhcpNextSrvIpAddrGet, FsWlanDhcpNextSrvIpAddrSet, FsWlanDhcpNextSrvIpAddrTest, FsWlanDhcpNextSrvIpAddrDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsWlanFirewallStatus}, NULL, FsWlanFirewallStatusGet, FsWlanFirewallStatusSet, FsWlanFirewallStatusTest, FsWlanFirewallStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsWlanNATStatus}, NULL, FsWlanNATStatusGet, FsWlanNATStatusSet, FsWlanNATStatusTest, FsWlanNATStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsWlanDNSServerIpAddr}, NULL, FsWlanDNSServerIpAddrGet, FsWlanDNSServerIpAddrSet, FsWlanDNSServerIpAddrTest, FsWlanDNSServerIpAddrDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsWlanDefaultRouterIpAddr}, NULL, FsWlanDefaultRouterIpAddrGet, FsWlanDefaultRouterIpAddrSet, FsWlanDefaultRouterIpAddrTest, FsWlanDefaultRouterIpAddrDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{13,FsWlanRadioIfIpAddr}, GetNextIndexFsWlanRadioIfTable, FsWlanRadioIfIpAddrGet, FsWlanRadioIfIpAddrSet, FsWlanRadioIfIpAddrTest, FsWlanRadioIfTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsWlanRadioIfTableINDEX, 2, 0, 0, "0"},

{{13,FsWlanRadioIfIpSubnetMask}, GetNextIndexFsWlanRadioIfTable, FsWlanRadioIfIpSubnetMaskGet, FsWlanRadioIfIpSubnetMaskSet, FsWlanRadioIfIpSubnetMaskTest, FsWlanRadioIfTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsWlanRadioIfTableINDEX, 2, 0, 0, NULL},

{{13,FsWlanRadioIfAdminStatus}, GetNextIndexFsWlanRadioIfTable, FsWlanRadioIfAdminStatusGet, FsWlanRadioIfAdminStatusSet, FsWlanRadioIfAdminStatusTest, FsWlanRadioIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWlanRadioIfTableINDEX, 2, 0, 0, "2"},

{{13,FsWlanRadioIfRowStatus}, GetNextIndexFsWlanRadioIfTable, FsWlanRadioIfRowStatusGet, FsWlanRadioIfRowStatusSet, FsWlanRadioIfRowStatusTest, FsWlanRadioIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWlanRadioIfTableINDEX, 2, 0, 1, NULL},

{{13,FsDot11WtpDhcpSrvSubnetPoolIndex}, GetNextIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsDot11WtpDhcpSrvSubnetPoolConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsDot11WtpDhcpSrvSubnetSubnet}, GetNextIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable, FsDot11WtpDhcpSrvSubnetSubnetGet, FsDot11WtpDhcpSrvSubnetSubnetSet, FsDot11WtpDhcpSrvSubnetSubnetTest, FsDot11WtpDhcpSrvSubnetPoolConfigTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsDot11WtpDhcpSrvSubnetPoolConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsDot11WtpDhcpSrvSubnetMask}, GetNextIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable, FsDot11WtpDhcpSrvSubnetMaskGet, FsDot11WtpDhcpSrvSubnetMaskSet, FsDot11WtpDhcpSrvSubnetMaskTest, FsDot11WtpDhcpSrvSubnetPoolConfigTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsDot11WtpDhcpSrvSubnetPoolConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsDot11WtpDhcpSrvSubnetStartIpAddress}, GetNextIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable, FsDot11WtpDhcpSrvSubnetStartIpAddressGet, FsDot11WtpDhcpSrvSubnetStartIpAddressSet, FsDot11WtpDhcpSrvSubnetStartIpAddressTest, FsDot11WtpDhcpSrvSubnetPoolConfigTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsDot11WtpDhcpSrvSubnetPoolConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsDot11WtpDhcpSrvSubnetEndIpAddress}, GetNextIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable, FsDot11WtpDhcpSrvSubnetEndIpAddressGet, FsDot11WtpDhcpSrvSubnetEndIpAddressSet, FsDot11WtpDhcpSrvSubnetEndIpAddressTest, FsDot11WtpDhcpSrvSubnetPoolConfigTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsDot11WtpDhcpSrvSubnetPoolConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsDot11WtpDhcpSrvSubnetLeaseTime}, GetNextIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable, FsDot11WtpDhcpSrvSubnetLeaseTimeGet, FsDot11WtpDhcpSrvSubnetLeaseTimeSet, FsDot11WtpDhcpSrvSubnetLeaseTimeTest, FsDot11WtpDhcpSrvSubnetPoolConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WtpDhcpSrvSubnetPoolConfigTableINDEX, 2, 0, 0, "3600"},

{{13,FsDot11WtpDhcpSrvDefaultRouter}, GetNextIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable, FsDot11WtpDhcpSrvDefaultRouterGet, FsDot11WtpDhcpSrvDefaultRouterSet, FsDot11WtpDhcpSrvDefaultRouterTest, FsDot11WtpDhcpSrvSubnetPoolConfigTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsDot11WtpDhcpSrvSubnetPoolConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsDot11WtpDhcpSrvDnsServerIp}, GetNextIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable, FsDot11WtpDhcpSrvDnsServerIpGet, FsDot11WtpDhcpSrvDnsServerIpSet, FsDot11WtpDhcpSrvDnsServerIpTest, FsDot11WtpDhcpSrvSubnetPoolConfigTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsDot11WtpDhcpSrvSubnetPoolConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsDot11WtpDhcpSrvSubnetPoolRowStatus}, GetNextIndexFsDot11WtpDhcpSrvSubnetPoolConfigTable, FsDot11WtpDhcpSrvSubnetPoolRowStatusGet, FsDot11WtpDhcpSrvSubnetPoolRowStatusSet, FsDot11WtpDhcpSrvSubnetPoolRowStatusTest, FsDot11WtpDhcpSrvSubnetPoolConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDot11WtpDhcpSrvSubnetPoolConfigTableINDEX, 2, 0, 1, NULL},

{{13,FsWtpNATConfigIndex}, GetNextIndexFsWtpNATTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsWtpNATTableINDEX, 2, 0, 0, NULL},

{{13,FsWtpIfMainWanType}, GetNextIndexFsWtpNATTable, FsWtpIfMainWanTypeGet, FsWtpIfMainWanTypeSet, FsWtpIfMainWanTypeTest, FsWtpNATTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpNATTableINDEX, 2, 0, 0, NULL},

{{13,FsWtpNATConfigRowStatus}, GetNextIndexFsWtpNATTable, FsWtpNATConfigRowStatusGet, FsWtpNATConfigRowStatusSet, FsWtpNATConfigRowStatusTest, FsWtpNATTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpNATTableINDEX, 2, 0, 1, NULL},

{{13,FsWtpDhcpServerStatus}, GetNextIndexFsWtpDhcpConfigTable, FsWtpDhcpServerStatusGet, FsWtpDhcpServerStatusSet, FsWtpDhcpServerStatusTest, FsWtpDhcpConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpDhcpConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsWtpDhcpRelayStatus}, GetNextIndexFsWtpDhcpConfigTable, FsWtpDhcpRelayStatusGet, FsWtpDhcpRelayStatusSet, FsWtpDhcpRelayStatusTest, FsWtpDhcpConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpDhcpConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsWtpFirewallStatus}, GetNextIndexFsWtpFirewallConfigTable, FsWtpFirewallStatusGet, FsWtpFirewallStatusSet, FsWtpFirewallStatusTest, FsWtpFirewallConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpFirewallConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsWtpFirewallRowStatus}, GetNextIndexFsWtpFirewallConfigTable, FsWtpFirewallRowStatusGet, FsWtpFirewallRowStatusSet, FsWtpFirewallRowStatusTest, FsWtpFirewallConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpFirewallConfigTableINDEX, 1, 0, 1, NULL},

{{13,FsWtpIpRouteConfigSubnet}, GetNextIndexFsWtpIpRouteConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsWtpIpRouteConfigTableINDEX, 4, 0, 0, NULL},

{{13,FsWtpIpRouteConfigNetMask}, GetNextIndexFsWtpIpRouteConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsWtpIpRouteConfigTableINDEX, 4, 0, 0, NULL},

{{13,FsWtpIpRouteConfigGateway}, GetNextIndexFsWtpIpRouteConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsWtpIpRouteConfigTableINDEX, 4, 0, 0, NULL},

{{13,FsWtpIpRouteConfigRowStatus}, GetNextIndexFsWtpIpRouteConfigTable, FsWtpIpRouteConfigRowStatusGet, FsWtpIpRouteConfigRowStatusSet, FsWtpIpRouteConfigRowStatusTest, FsWtpIpRouteConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpIpRouteConfigTableINDEX, 4, 0, 1, NULL},

{{13,FsWtpFwlFilterName}, GetNextIndexFsWtpFwlFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsWtpFwlFilterTableINDEX, 2, 0, 0, NULL},

{{13,FsWtpFwlFilterSrcAddress}, GetNextIndexFsWtpFwlFilterTable, FsWtpFwlFilterSrcAddressGet, FsWtpFwlFilterSrcAddressSet, FsWtpFwlFilterSrcAddressTest, FsWtpFwlFilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsWtpFwlFilterTableINDEX, 2, 0, 0, "0"},

{{13,FsWtpFwlFilterDestAddress}, GetNextIndexFsWtpFwlFilterTable, FsWtpFwlFilterDestAddressGet, FsWtpFwlFilterDestAddressSet, FsWtpFwlFilterDestAddressTest, FsWtpFwlFilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsWtpFwlFilterTableINDEX, 2, 0, 0, "0"},

{{13,FsWtpFwlFilterProtocol}, GetNextIndexFsWtpFwlFilterTable, FsWtpFwlFilterProtocolGet, FsWtpFwlFilterProtocolSet, FsWtpFwlFilterProtocolTest, FsWtpFwlFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpFwlFilterTableINDEX, 2, 0, 0, "255"},

{{13,FsWtpFwlFilterSrcPort}, GetNextIndexFsWtpFwlFilterTable, FsWtpFwlFilterSrcPortGet, FsWtpFwlFilterSrcPortSet, FsWtpFwlFilterSrcPortTest, FsWtpFwlFilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsWtpFwlFilterTableINDEX, 2, 0, 0, "0"},

{{13,FsWtpFwlFilterDestPort}, GetNextIndexFsWtpFwlFilterTable, FsWtpFwlFilterDestPortGet, FsWtpFwlFilterDestPortSet, FsWtpFwlFilterDestPortTest, FsWtpFwlFilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsWtpFwlFilterTableINDEX, 2, 0, 0, "0"},

{{13,FsWtpFwlFilterTos}, GetNextIndexFsWtpFwlFilterTable, FsWtpFwlFilterTosGet, FsWtpFwlFilterTosSet, FsWtpFwlFilterTosTest, FsWtpFwlFilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsWtpFwlFilterTableINDEX, 2, 0, 0, "0"},

{{13,FsWtpFwlFilterAccounting}, GetNextIndexFsWtpFwlFilterTable, FsWtpFwlFilterAccountingGet, FsWtpFwlFilterAccountingSet, FsWtpFwlFilterAccountingTest, FsWtpFwlFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpFwlFilterTableINDEX, 2, 0, 0, "2"},

{{13,FsWtpFwlFilterHitClear}, GetNextIndexFsWtpFwlFilterTable, FsWtpFwlFilterHitClearGet, FsWtpFwlFilterHitClearSet, FsWtpFwlFilterHitClearTest, FsWtpFwlFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpFwlFilterTableINDEX, 2, 0, 0, "2"},

{{13,FsWtpFwlFilterHitsCount}, GetNextIndexFsWtpFwlFilterTable, FsWtpFwlFilterHitsCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsWtpFwlFilterTableINDEX, 2, 0, 0, NULL},

{{13,FsWtpFwlFilterAddrType}, GetNextIndexFsWtpFwlFilterTable, FsWtpFwlFilterAddrTypeGet, FsWtpFwlFilterAddrTypeSet, FsWtpFwlFilterAddrTypeTest, FsWtpFwlFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpFwlFilterTableINDEX, 2, 0, 0, NULL},

{{13,FsWtpFwlFilterFlowId}, GetNextIndexFsWtpFwlFilterTable, FsWtpFwlFilterFlowIdGet, FsWtpFwlFilterFlowIdSet, FsWtpFwlFilterFlowIdTest, FsWtpFwlFilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsWtpFwlFilterTableINDEX, 2, 0, 0, "0"},

{{13,FsWtpFwlFilterDscp}, GetNextIndexFsWtpFwlFilterTable, FsWtpFwlFilterDscpGet, FsWtpFwlFilterDscpSet, FsWtpFwlFilterDscpTest, FsWtpFwlFilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsWtpFwlFilterTableINDEX, 2, 0, 0, "0"},

{{13,FsWtpFwlFilterRowStatus}, GetNextIndexFsWtpFwlFilterTable, FsWtpFwlFilterRowStatusGet, FsWtpFwlFilterRowStatusSet, FsWtpFwlFilterRowStatusTest, FsWtpFwlFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpFwlFilterTableINDEX, 2, 0, 1, NULL},

{{13,FsWtpFwlRuleName}, GetNextIndexFsWtpFwlRuleTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsWtpFwlRuleTableINDEX, 2, 0, 0, NULL},

{{13,FsWtpFwlRuleFilterSet}, GetNextIndexFsWtpFwlRuleTable, FsWtpFwlRuleFilterSetGet, FsWtpFwlRuleFilterSetSet, FsWtpFwlRuleFilterSetTest, FsWtpFwlRuleTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsWtpFwlRuleTableINDEX, 2, 0, 0, NULL},

{{13,FsWtpFwlRuleRowStatus}, GetNextIndexFsWtpFwlRuleTable, FsWtpFwlRuleRowStatusGet, FsWtpFwlRuleRowStatusSet, FsWtpFwlRuleRowStatusTest, FsWtpFwlRuleTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpFwlRuleTableINDEX, 2, 0, 1, NULL},

{{13,FsWtpFwlAclIfIndex}, GetNextIndexFsWtpFwlAclTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsWtpFwlAclTableINDEX, 4, 0, 0, NULL},

{{13,FsWtpFwlAclName}, GetNextIndexFsWtpFwlAclTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsWtpFwlAclTableINDEX, 4, 0, 0, NULL},

{{13,FsWtpFwlAclDirection}, GetNextIndexFsWtpFwlAclTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsWtpFwlAclTableINDEX, 4, 0, 0, NULL},

{{13,FsWtpFwlAclAction}, GetNextIndexFsWtpFwlAclTable, FsWtpFwlAclActionGet, FsWtpFwlAclActionSet, FsWtpFwlAclActionTest, FsWtpFwlAclTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpFwlAclTableINDEX, 4, 0, 0, NULL},

{{13,FsWtpFwlAclSequenceNumber}, GetNextIndexFsWtpFwlAclTable, FsWtpFwlAclSequenceNumberGet, FsWtpFwlAclSequenceNumberSet, FsWtpFwlAclSequenceNumberTest, FsWtpFwlAclTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsWtpFwlAclTableINDEX, 4, 0, 0, NULL},

{{13,FsWtpFwlAclLogTrigger}, GetNextIndexFsWtpFwlAclTable, FsWtpFwlAclLogTriggerGet, FsWtpFwlAclLogTriggerSet, FsWtpFwlAclLogTriggerTest, FsWtpFwlAclTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpFwlAclTableINDEX, 4, 0, 0, "1"},

{{13,FsWtpFwlAclFragAction}, GetNextIndexFsWtpFwlAclTable, FsWtpFwlAclFragActionGet, FsWtpFwlAclFragActionSet, FsWtpFwlAclFragActionTest, FsWtpFwlAclTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpFwlAclTableINDEX, 4, 0, 0, NULL},

{{13,FsWtpFwlAclRowStatus}, GetNextIndexFsWtpFwlAclTable, FsWtpFwlAclRowStatusGet, FsWtpFwlAclRowStatusSet, FsWtpFwlAclRowStatusTest, FsWtpFwlAclTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpFwlAclTableINDEX, 4, 0, 1, NULL},

{{13,FsWtpNatDynamicTransInterfaceNum}, GetNextIndexFsWtpNatDynamicTransTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsWtpNatDynamicTransTableINDEX, 6, 0, 0, NULL},

{{13,FsWtpNatDynamicTransLocalIp}, GetNextIndexFsWtpNatDynamicTransTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsWtpNatDynamicTransTableINDEX, 6, 0, 0, NULL},

{{13,FsWtpNatDynamicTransTranslatedLocalIp}, GetNextIndexFsWtpNatDynamicTransTable, FsWtpNatDynamicTransTranslatedLocalIpGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsWtpNatDynamicTransTableINDEX, 6, 0, 0, NULL},

{{13,FsWtpNatDynamicTransLocalPort}, GetNextIndexFsWtpNatDynamicTransTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsWtpNatDynamicTransTableINDEX, 6, 0, 0, NULL},

{{13,FsWtpNatDynamicTransTranslatedLocalPort}, GetNextIndexFsWtpNatDynamicTransTable, FsWtpNatDynamicTransTranslatedLocalPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsWtpNatDynamicTransTableINDEX, 6, 0, 0, NULL},

{{13,FsWtpNatDynamicTransOutsideIp}, GetNextIndexFsWtpNatDynamicTransTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsWtpNatDynamicTransTableINDEX, 6, 0, 0, NULL},

{{13,FsWtpNatDynamicTransOutsidePort}, GetNextIndexFsWtpNatDynamicTransTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsWtpNatDynamicTransTableINDEX, 6, 0, 0, NULL},

{{13,FsWtpNatDynamicTransLastUseTime}, GetNextIndexFsWtpNatDynamicTransTable, FsWtpNatDynamicTransLastUseTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsWtpNatDynamicTransTableINDEX, 6, 0, 0, NULL},

{{13,FsWtpNatGlobalAddressInterfaceNum}, GetNextIndexFsWtpNatGlobalAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsWtpNatGlobalAddressTableINDEX, 3, 0, 0, NULL},

{{13,FsWtpNatGlobalAddressTranslatedLocalIp}, GetNextIndexFsWtpNatGlobalAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsWtpNatGlobalAddressTableINDEX, 3, 0, 0, NULL},

{{13,FsWtpNatGlobalAddressMask}, GetNextIndexFsWtpNatGlobalAddressTable, FsWtpNatGlobalAddressMaskGet, FsWtpNatGlobalAddressMaskSet, FsWtpNatGlobalAddressMaskTest, FsWtpNatGlobalAddressTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsWtpNatGlobalAddressTableINDEX, 3, 0, 0, NULL},

{{13,FsWtpNatGlobalAddressEntryStatus}, GetNextIndexFsWtpNatGlobalAddressTable, FsWtpNatGlobalAddressEntryStatusGet, FsWtpNatGlobalAddressEntryStatusSet, FsWtpNatGlobalAddressEntryStatusTest, FsWtpNatGlobalAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpNatGlobalAddressTableINDEX, 3, 0, 1, NULL},

{{13,FsWtpNatLocalAddressInterfaceNumber}, GetNextIndexFsWtpNatLocalAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsWtpNatLocalAddressTableINDEX, 3, 0, 0, NULL},

{{13,FsWtpNatLocalAddressLocalIp}, GetNextIndexFsWtpNatLocalAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsWtpNatLocalAddressTableINDEX, 3, 0, 0, NULL},

{{13,FsWtpNatLocalAddressMask}, GetNextIndexFsWtpNatLocalAddressTable, FsWtpNatLocalAddressMaskGet, FsWtpNatLocalAddressMaskSet, FsWtpNatLocalAddressMaskTest, FsWtpNatLocalAddressTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsWtpNatLocalAddressTableINDEX, 3, 0, 0, NULL},

{{13,FsWtpNatLocalAddressEntryStatus}, GetNextIndexFsWtpNatLocalAddressTable, FsWtpNatLocalAddressEntryStatusGet, FsWtpNatLocalAddressEntryStatusSet, FsWtpNatLocalAddressEntryStatusTest, FsWtpNatLocalAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpNatLocalAddressTableINDEX, 3, 0, 1, NULL},

{{13,FsWtpNatStaticInterfaceNum}, GetNextIndexFsWtpNatStaticTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsWtpNatStaticTableINDEX, 3, 0, 0, NULL},

{{13,FsWtpNatStaticLocalIp}, GetNextIndexFsWtpNatStaticTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsWtpNatStaticTableINDEX, 3, 0, 0, NULL},

{{13,FsWtpNatStaticTranslatedLocalIp}, GetNextIndexFsWtpNatStaticTable, FsWtpNatStaticTranslatedLocalIpGet, FsWtpNatStaticTranslatedLocalIpSet, FsWtpNatStaticTranslatedLocalIpTest, FsWtpNatStaticTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsWtpNatStaticTableINDEX, 3, 0, 0, NULL},

{{13,FsWtpNatStaticEntryStatus}, GetNextIndexFsWtpNatStaticTable, FsWtpNatStaticEntryStatusGet, FsWtpNatStaticEntryStatusSet, FsWtpNatStaticEntryStatusTest, FsWtpNatStaticTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpNatStaticTableINDEX, 3, 0, 1, NULL},

{{13,FsWtpNatStaticNaptInterfaceNum}, GetNextIndexFsWtpNatStaticNaptTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsWtpNatStaticNaptTableINDEX, 6, 0, 0, NULL},

{{13,FsWtpNatStaticNaptLocalIp}, GetNextIndexFsWtpNatStaticNaptTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsWtpNatStaticNaptTableINDEX, 6, 0, 0, NULL},

{{13,FsWtpNatStaticNaptStartLocalPort}, GetNextIndexFsWtpNatStaticNaptTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsWtpNatStaticNaptTableINDEX, 6, 0, 0, NULL},

{{13,FsWtpNatStaticNaptEndLocalPort}, GetNextIndexFsWtpNatStaticNaptTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsWtpNatStaticNaptTableINDEX, 6, 0, 0, NULL},

{{13,FsWtpNatStaticNaptProtocolNumber}, GetNextIndexFsWtpNatStaticNaptTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsWtpNatStaticNaptTableINDEX, 6, 0, 0, NULL},

{{13,FsWtpNatStaticNaptTranslatedLocalIp}, GetNextIndexFsWtpNatStaticNaptTable, FsWtpNatStaticNaptTranslatedLocalIpGet, FsWtpNatStaticNaptTranslatedLocalIpSet, FsWtpNatStaticNaptTranslatedLocalIpTest, FsWtpNatStaticNaptTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsWtpNatStaticNaptTableINDEX, 6, 0, 0, NULL},

{{13,FsWtpNatStaticNaptTranslatedLocalPort}, GetNextIndexFsWtpNatStaticNaptTable, FsWtpNatStaticNaptTranslatedLocalPortGet, FsWtpNatStaticNaptTranslatedLocalPortSet, FsWtpNatStaticNaptTranslatedLocalPortTest, FsWtpNatStaticNaptTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsWtpNatStaticNaptTableINDEX, 6, 0, 0, NULL},

{{13,FsWtpNatStaticNaptDescription}, GetNextIndexFsWtpNatStaticNaptTable, FsWtpNatStaticNaptDescriptionGet, FsWtpNatStaticNaptDescriptionSet, FsWtpNatStaticNaptDescriptionTest, FsWtpNatStaticNaptTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsWtpNatStaticNaptTableINDEX, 6, 0, 0, NULL},

{{13,FsWtpNatStaticNaptEntryStatus}, GetNextIndexFsWtpNatStaticNaptTable, FsWtpNatStaticNaptEntryStatusGet, FsWtpNatStaticNaptEntryStatusSet, FsWtpNatStaticNaptEntryStatusTest, FsWtpNatStaticNaptTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpNatStaticNaptTableINDEX, 6, 0, 1, NULL},

{{13,FsWtpNatIfInterfaceNumber}, GetNextIndexFsWtpNatIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsWtpNatIfTableINDEX, 2, 0, 0, NULL},

{{13,FsWtpNatIfNat}, GetNextIndexFsWtpNatIfTable, FsWtpNatIfNatGet, FsWtpNatIfNatSet, FsWtpNatIfNatTest, FsWtpNatIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpNatIfTableINDEX, 2, 0, 0, "2"},

{{13,FsWtpNatIfNapt}, GetNextIndexFsWtpNatIfTable, FsWtpNatIfNaptGet, FsWtpNatIfNaptSet, FsWtpNatIfNaptTest, FsWtpNatIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpNatIfTableINDEX, 2, 0, 0, "2"},

{{13,FsWtpNatIfTwoWayNat}, GetNextIndexFsWtpNatIfTable, FsWtpNatIfTwoWayNatGet, FsWtpNatIfTwoWayNatSet, FsWtpNatIfTwoWayNatTest, FsWtpNatIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpNatIfTableINDEX, 2, 0, 0, "2"},

{{13,FsWtpNatIfEntryStatus}, GetNextIndexFsWtpNatIfTable, FsWtpNatIfEntryStatusGet, FsWtpNatIfEntryStatusSet, FsWtpNatIfEntryStatusTest, FsWtpNatIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpNatIfTableINDEX, 2, 0, 1, NULL},

{{13,FsWtpVlanContextId}, GetNextIndexFsWtpVlanStaticTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsWtpVlanStaticTableINDEX, 3, 0, 0, NULL},

{{13,FsWtpVlanIndex}, GetNextIndexFsWtpVlanStaticTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsWtpVlanStaticTableINDEX, 3, 0, 0, NULL},

{{13,FsWtpVlanStaticName}, GetNextIndexFsWtpVlanStaticTable, FsWtpVlanStaticNameGet, FsWtpVlanStaticNameSet, FsWtpVlanStaticNameTest, FsWtpVlanStaticTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsWtpVlanStaticTableINDEX, 3, 0, 0, NULL},

{{13,FsWtpVlanStaticRowStatus}, GetNextIndexFsWtpVlanStaticTable, FsWtpVlanStaticRowStatusGet, FsWtpVlanStaticRowStatusSet, FsWtpVlanStaticRowStatusTest, FsWtpVlanStaticTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpVlanStaticTableINDEX, 3, 0, 1, NULL},

{{13,FsWtpTpPort}, GetNextIndexFsWtpVlanStaticPortConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsWtpVlanStaticPortConfigTableINDEX, 4, 0, 0, NULL},

{{13,FsWtpVlanStaticPort}, GetNextIndexFsWtpVlanStaticPortConfigTable, FsWtpVlanStaticPortGet, FsWtpVlanStaticPortSet, FsWtpVlanStaticPortTest, FsWtpVlanStaticPortConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpVlanStaticPortConfigTableINDEX, 4, 0, 0, NULL},

{{13,FsWtpIfMainIndex}, GetNextIndexFsWtpIfMainTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsWtpIfMainTableINDEX, 2, 0, 0, NULL},

{{13,FsWtpIfMainType}, GetNextIndexFsWtpIfMainTable, FsWtpIfMainTypeGet, FsWtpIfMainTypeSet, FsWtpIfMainTypeTest, FsWtpIfMainTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpIfMainTableINDEX, 2, 0, 0, NULL},

{{13,FsWtpIfMainAdminStatus}, GetNextIndexFsWtpIfMainTable, FsWtpIfMainAdminStatusGet, FsWtpIfMainAdminStatusSet, FsWtpIfMainAdminStatusTest, FsWtpIfMainTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpIfMainTableINDEX, 2, 0, 0, "2"},

{{13,FsWtpIfMainOperStatus}, GetNextIndexFsWtpIfMainTable, FsWtpIfMainOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsWtpIfMainTableINDEX, 2, 0, 0, NULL},

{{13,FsWtpIfMainRowStatus}, GetNextIndexFsWtpIfMainTable, FsWtpIfMainRowStatusGet, FsWtpIfMainRowStatusSet, FsWtpIfMainRowStatusTest, FsWtpIfMainTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpIfMainTableINDEX, 2, 0, 1, NULL},

{{13,FsWtpIfMainNetworkType}, GetNextIndexFsWtpIfMainTable, FsWtpIfMainNetworkTypeGet, FsWtpIfMainNetworkTypeSet, FsWtpIfMainNetworkTypeTest, FsWtpIfMainTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpIfMainTableINDEX, 2, 0, 0, NULL},

{{13,FsWtpIfMainEncapDot1qVlanId}, GetNextIndexFsWtpIfMainTable, FsWtpIfMainEncapDot1qVlanIdGet, FsWtpIfMainEncapDot1qVlanIdSet, FsWtpIfMainEncapDot1qVlanIdTest, FsWtpIfMainTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsWtpIfMainTableINDEX, 2, 0, 0, "0"},

{{13,FsWtpIfIpAddr}, GetNextIndexFsWtpIfMainTable, FsWtpIfIpAddrGet, FsWtpIfIpAddrSet, FsWtpIfIpAddrTest, FsWtpIfMainTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsWtpIfMainTableINDEX, 2, 0, 0, "0"},

{{13,FsWtpIfIpSubnetMask}, GetNextIndexFsWtpIfMainTable, FsWtpIfIpSubnetMaskGet, FsWtpIfIpSubnetMaskSet, FsWtpIfIpSubnetMaskTest, FsWtpIfMainTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsWtpIfMainTableINDEX, 2, 0, 0, NULL},

{{13,FsWtpIfIpBroadcastAddr}, GetNextIndexFsWtpIfMainTable, FsWtpIfIpBroadcastAddrGet, FsWtpIfIpBroadcastAddrSet, FsWtpIfIpBroadcastAddrTest, FsWtpIfMainTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsWtpIfMainTableINDEX, 2, 0, 0, NULL},

{{13,FsWtpIfMainPhyPort}, GetNextIndexFsWtpIfMainTable, FsWtpIfMainPhyPortGet, FsWtpIfMainPhyPortSet, FsWtpIfMainPhyPortTest, FsWtpIfMainTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsWtpIfMainTableINDEX, 2, 0, 0, NULL},
};
tMibData fswsslEntry = { 107, fswsslMibEntry };

#endif /* _FSWSSLDB_H */

