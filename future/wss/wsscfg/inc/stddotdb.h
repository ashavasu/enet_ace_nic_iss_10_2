/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stddotdb.h,v 1.1.1.1 2016/09/20 10:42:39 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDDOTDB_H
#define _STDDOTDB_H

UINT1 Dot11StationConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11AuthenticationAlgorithmsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot11WEPDefaultKeysTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11WEPKeyMappingsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot11PrivacyTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11MultiDomainCapabilityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot11SpectrumManagementTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot11RegulatoryClassesTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot11OperationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11CountersTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11GroupAddressesTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot11EDCATableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11QAPEDCATableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11QosCountersTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11ResourceInfoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11PhyOperationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11PhyAntennaTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11PhyTxPowerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11PhyFHSSTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11PhyDSSSTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11PhyIRTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11RegDomainsSupportedTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot11AntennasListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot11SupportedDataRatesTxTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot11SupportedDataRatesRxTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot11PhyOFDMTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11PhyHRDSSSTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11HoppingPatternTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot11PhyERPTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11RSNAConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Dot11RSNAConfigPairwiseCiphersTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11RSNAConfigAuthenticationSuitesTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Dot11RSNAStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 stddot11 [] ={1,2,840,10036};
tSNMP_OID_TYPE stddot11OID = {4, stddot11};


/* Generated OID's for tables */
UINT4 Dot11StationConfigTable [] ={1,2,840,10036,1,1};
tSNMP_OID_TYPE Dot11StationConfigTableOID = {6, Dot11StationConfigTable};


UINT4 Dot11AuthenticationAlgorithmsTable [] ={1,2,840,10036,1,2};
tSNMP_OID_TYPE Dot11AuthenticationAlgorithmsTableOID = {6, Dot11AuthenticationAlgorithmsTable};


UINT4 Dot11WEPDefaultKeysTable [] ={1,2,840,10036,1,3};
tSNMP_OID_TYPE Dot11WEPDefaultKeysTableOID = {6, Dot11WEPDefaultKeysTable};


UINT4 Dot11WEPKeyMappingsTable [] ={1,2,840,10036,1,4};
tSNMP_OID_TYPE Dot11WEPKeyMappingsTableOID = {6, Dot11WEPKeyMappingsTable};


UINT4 Dot11PrivacyTable [] ={1,2,840,10036,1,5};
tSNMP_OID_TYPE Dot11PrivacyTableOID = {6, Dot11PrivacyTable};


UINT4 Dot11MultiDomainCapabilityTable [] ={1,2,840,10036,1,7};
tSNMP_OID_TYPE Dot11MultiDomainCapabilityTableOID = {6, Dot11MultiDomainCapabilityTable};


UINT4 Dot11SpectrumManagementTable [] ={1,2,840,10036,1,8};
tSNMP_OID_TYPE Dot11SpectrumManagementTableOID = {6, Dot11SpectrumManagementTable};


UINT4 Dot11RegulatoryClassesTable [] ={1,2,840,10036,1,13};
tSNMP_OID_TYPE Dot11RegulatoryClassesTableOID = {6, Dot11RegulatoryClassesTable};


UINT4 Dot11OperationTable [] ={1,2,840,10036,2,1};
tSNMP_OID_TYPE Dot11OperationTableOID = {6, Dot11OperationTable};


UINT4 Dot11CountersTable [] ={1,2,840,10036,2,2};
tSNMP_OID_TYPE Dot11CountersTableOID = {6, Dot11CountersTable};


UINT4 Dot11GroupAddressesTable [] ={1,2,840,10036,2,3};
tSNMP_OID_TYPE Dot11GroupAddressesTableOID = {6, Dot11GroupAddressesTable};


UINT4 Dot11EDCATable [] ={1,2,840,10036,2,4};
tSNMP_OID_TYPE Dot11EDCATableOID = {6, Dot11EDCATable};


UINT4 Dot11QAPEDCATable [] ={1,2,840,10036,2,5};
tSNMP_OID_TYPE Dot11QAPEDCATableOID = {6, Dot11QAPEDCATable};


UINT4 Dot11QosCountersTable [] ={1,2,840,10036,2,6};
tSNMP_OID_TYPE Dot11QosCountersTableOID = {6, Dot11QosCountersTable};


UINT4 Dot11ResourceInfoTable [] ={1,2,840,10036,3,1,2};
tSNMP_OID_TYPE Dot11ResourceInfoTableOID = {7, Dot11ResourceInfoTable};


UINT4 Dot11PhyOperationTable [] ={1,2,840,10036,4,1};
tSNMP_OID_TYPE Dot11PhyOperationTableOID = {6, Dot11PhyOperationTable};


UINT4 Dot11PhyAntennaTable [] ={1,2,840,10036,4,2};
tSNMP_OID_TYPE Dot11PhyAntennaTableOID = {6, Dot11PhyAntennaTable};


UINT4 Dot11PhyTxPowerTable [] ={1,2,840,10036,4,3};
tSNMP_OID_TYPE Dot11PhyTxPowerTableOID = {6, Dot11PhyTxPowerTable};


UINT4 Dot11PhyFHSSTable [] ={1,2,840,10036,4,4};
tSNMP_OID_TYPE Dot11PhyFHSSTableOID = {6, Dot11PhyFHSSTable};


UINT4 Dot11PhyDSSSTable [] ={1,2,840,10036,4,5};
tSNMP_OID_TYPE Dot11PhyDSSSTableOID = {6, Dot11PhyDSSSTable};


UINT4 Dot11PhyIRTable [] ={1,2,840,10036,4,6};
tSNMP_OID_TYPE Dot11PhyIRTableOID = {6, Dot11PhyIRTable};


UINT4 Dot11RegDomainsSupportedTable [] ={1,2,840,10036,4,7};
tSNMP_OID_TYPE Dot11RegDomainsSupportedTableOID = {6, Dot11RegDomainsSupportedTable};


UINT4 Dot11AntennasListTable [] ={1,2,840,10036,4,8};
tSNMP_OID_TYPE Dot11AntennasListTableOID = {6, Dot11AntennasListTable};


UINT4 Dot11SupportedDataRatesTxTable [] ={1,2,840,10036,4,9};
tSNMP_OID_TYPE Dot11SupportedDataRatesTxTableOID = {6, Dot11SupportedDataRatesTxTable};


UINT4 Dot11SupportedDataRatesRxTable [] ={1,2,840,10036,4,10};
tSNMP_OID_TYPE Dot11SupportedDataRatesRxTableOID = {6, Dot11SupportedDataRatesRxTable};


UINT4 Dot11PhyOFDMTable [] ={1,2,840,10036,4,11};
tSNMP_OID_TYPE Dot11PhyOFDMTableOID = {6, Dot11PhyOFDMTable};


UINT4 Dot11PhyHRDSSSTable [] ={1,2,840,10036,4,12};
tSNMP_OID_TYPE Dot11PhyHRDSSSTableOID = {6, Dot11PhyHRDSSSTable};


UINT4 Dot11HoppingPatternTable [] ={1,2,840,10036,4,13};
tSNMP_OID_TYPE Dot11HoppingPatternTableOID = {6, Dot11HoppingPatternTable};


UINT4 Dot11PhyERPTable [] ={1,2,840,10036,4,14};
tSNMP_OID_TYPE Dot11PhyERPTableOID = {6, Dot11PhyERPTable};


UINT4 Dot11RSNAConfigTable [] ={1,2,840,10036,1,9};
tSNMP_OID_TYPE Dot11RSNAConfigTableOID = {6, Dot11RSNAConfigTable};


UINT4 Dot11RSNAConfigPairwiseCiphersTable [] ={1,2,840,10036,1,10};
tSNMP_OID_TYPE Dot11RSNAConfigPairwiseCiphersTableOID = {6, Dot11RSNAConfigPairwiseCiphersTable};


UINT4 Dot11RSNAConfigAuthenticationSuitesTable [] ={1,2,840,10036,1,11};
tSNMP_OID_TYPE Dot11RSNAConfigAuthenticationSuitesTableOID = {6, Dot11RSNAConfigAuthenticationSuitesTable};


UINT4 Dot11RSNAStatsTable [] ={1,2,840,10036,1,12};
tSNMP_OID_TYPE Dot11RSNAStatsTableOID = {6, Dot11RSNAStatsTable};




UINT4 Dot11StationID [ ] ={1,2,840,10036,1,1,1,1};
UINT4 Dot11MediumOccupancyLimit [ ] ={1,2,840,10036,1,1,1,2};
UINT4 Dot11CFPollable [ ] ={1,2,840,10036,1,1,1,3};
UINT4 Dot11CFPPeriod [ ] ={1,2,840,10036,1,1,1,4};
UINT4 Dot11CFPMaxDuration [ ] ={1,2,840,10036,1,1,1,5};
UINT4 Dot11AuthenticationResponseTimeOut [ ] ={1,2,840,10036,1,1,1,6};
UINT4 Dot11PrivacyOptionImplemented [ ] ={1,2,840,10036,1,1,1,7};
UINT4 Dot11PowerManagementMode [ ] ={1,2,840,10036,1,1,1,8};
UINT4 Dot11DesiredSSID [ ] ={1,2,840,10036,1,1,1,9};
UINT4 Dot11DesiredBSSType [ ] ={1,2,840,10036,1,1,1,10};
UINT4 Dot11OperationalRateSet [ ] ={1,2,840,10036,1,1,1,11};
UINT4 Dot11BeaconPeriod [ ] ={1,2,840,10036,1,1,1,12};
UINT4 Dot11DTIMPeriod [ ] ={1,2,840,10036,1,1,1,13};
UINT4 Dot11AssociationResponseTimeOut [ ] ={1,2,840,10036,1,1,1,14};
UINT4 Dot11DisassociateReason [ ] ={1,2,840,10036,1,1,1,15};
UINT4 Dot11DisassociateStation [ ] ={1,2,840,10036,1,1,1,16};
UINT4 Dot11DeauthenticateReason [ ] ={1,2,840,10036,1,1,1,17};
UINT4 Dot11DeauthenticateStation [ ] ={1,2,840,10036,1,1,1,18};
UINT4 Dot11AuthenticateFailStatus [ ] ={1,2,840,10036,1,1,1,19};
UINT4 Dot11AuthenticateFailStation [ ] ={1,2,840,10036,1,1,1,20};
UINT4 Dot11MultiDomainCapabilityImplemented [ ] ={1,2,840,10036,1,1,1,21};
UINT4 Dot11MultiDomainCapabilityEnabled [ ] ={1,2,840,10036,1,1,1,22};
UINT4 Dot11CountryString [ ] ={1,2,840,10036,1,1,1,23};
UINT4 Dot11SpectrumManagementImplemented [ ] ={1,2,840,10036,1,1,1,24};
UINT4 Dot11SpectrumManagementRequired [ ] ={1,2,840,10036,1,1,1,25};
UINT4 Dot11RSNAOptionImplemented [ ] ={1,2,840,10036,1,1,1,26};
UINT4 Dot11RSNAPreauthenticationImplemented [ ] ={1,2,840,10036,1,1,1,27};
UINT4 Dot11RegulatoryClassesImplemented [ ] ={1,2,840,10036,1,1,1,28};
UINT4 Dot11RegulatoryClassesRequired [ ] ={1,2,840,10036,1,1,1,29};
UINT4 Dot11QosOptionImplemented [ ] ={1,2,840,10036,1,1,1,30};
UINT4 Dot11ImmediateBlockAckOptionImplemented [ ] ={1,2,840,10036,1,1,1,31};
UINT4 Dot11DelayedBlockAckOptionImplemented [ ] ={1,2,840,10036,1,1,1,32};
UINT4 Dot11DirectOptionImplemented [ ] ={1,2,840,10036,1,1,1,33};
UINT4 Dot11APSDOptionImplemented [ ] ={1,2,840,10036,1,1,1,34};
UINT4 Dot11QAckOptionImplemented [ ] ={1,2,840,10036,1,1,1,35};
UINT4 Dot11QBSSLoadOptionImplemented [ ] ={1,2,840,10036,1,1,1,36};
UINT4 Dot11QueueRequestOptionImplemented [ ] ={1,2,840,10036,1,1,1,37};
UINT4 Dot11TXOPRequestOptionImplemented [ ] ={1,2,840,10036,1,1,1,38};
UINT4 Dot11MoreDataAckOptionImplemented [ ] ={1,2,840,10036,1,1,1,39};
UINT4 Dot11AssociateinNQBSS [ ] ={1,2,840,10036,1,1,1,40};
UINT4 Dot11DLSAllowedInQBSS [ ] ={1,2,840,10036,1,1,1,41};
UINT4 Dot11DLSAllowed [ ] ={1,2,840,10036,1,1,1,42};
UINT4 Dot11AuthenticationAlgorithmsIndex [ ] ={1,2,840,10036,1,2,1,1};
UINT4 Dot11AuthenticationAlgorithm [ ] ={1,2,840,10036,1,2,1,2};
UINT4 Dot11AuthenticationAlgorithmsEnable [ ] ={1,2,840,10036,1,2,1,3};
UINT4 Dot11WEPDefaultKeyIndex [ ] ={1,2,840,10036,1,3,1,1};
UINT4 Dot11WEPDefaultKeyValue [ ] ={1,2,840,10036,1,3,1,2};
UINT4 Dot11WEPKeyMappingIndex [ ] ={1,2,840,10036,1,4,1,1};
UINT4 Dot11WEPKeyMappingAddress [ ] ={1,2,840,10036,1,4,1,2};
UINT4 Dot11WEPKeyMappingWEPOn [ ] ={1,2,840,10036,1,4,1,3};
UINT4 Dot11WEPKeyMappingValue [ ] ={1,2,840,10036,1,4,1,4};
UINT4 Dot11WEPKeyMappingStatus [ ] ={1,2,840,10036,1,4,1,5};
UINT4 Dot11PrivacyInvoked [ ] ={1,2,840,10036,1,5,1,1};
UINT4 Dot11WEPDefaultKeyID [ ] ={1,2,840,10036,1,5,1,2};
UINT4 Dot11WEPKeyMappingLength [ ] ={1,2,840,10036,1,5,1,3};
UINT4 Dot11ExcludeUnencrypted [ ] ={1,2,840,10036,1,5,1,4};
UINT4 Dot11WEPICVErrorCount [ ] ={1,2,840,10036,1,5,1,5};
UINT4 Dot11WEPExcludedCount [ ] ={1,2,840,10036,1,5,1,6};
UINT4 Dot11RSNAEnabled [ ] ={1,2,840,10036,1,5,1,7};
UINT4 Dot11RSNAPreauthenticationEnabled [ ] ={1,2,840,10036,1,5,1,8};
UINT4 Dot11MultiDomainCapabilityIndex [ ] ={1,2,840,10036,1,7,1,1};
UINT4 Dot11FirstChannelNumber [ ] ={1,2,840,10036,1,7,1,2};
UINT4 Dot11NumberofChannels [ ] ={1,2,840,10036,1,7,1,3};
UINT4 Dot11MaximumTransmitPowerLevel [ ] ={1,2,840,10036,1,7,1,4};
UINT4 Dot11SpectrumManagementIndex [ ] ={1,2,840,10036,1,8,1,1};
UINT4 Dot11MitigationRequirement [ ] ={1,2,840,10036,1,8,1,2};
UINT4 Dot11ChannelSwitchTime [ ] ={1,2,840,10036,1,8,1,3};
UINT4 Dot11PowerCapabilityMax [ ] ={1,2,840,10036,1,8,1,4};
UINT4 Dot11PowerCapabilityMin [ ] ={1,2,840,10036,1,8,1,5};
UINT4 Dot11RegulatoryClassesIndex [ ] ={1,2,840,10036,1,13,1,1};
UINT4 Dot11RegulatoryClass [ ] ={1,2,840,10036,1,13,1,2};
UINT4 Dot11CoverageClass [ ] ={1,2,840,10036,1,13,1,3};
UINT4 Dot11MACAddress [ ] ={1,2,840,10036,2,1,1,1};
UINT4 Dot11RTSThreshold [ ] ={1,2,840,10036,2,1,1,2};
UINT4 Dot11ShortRetryLimit [ ] ={1,2,840,10036,2,1,1,3};
UINT4 Dot11LongRetryLimit [ ] ={1,2,840,10036,2,1,1,4};
UINT4 Dot11FragmentationThreshold [ ] ={1,2,840,10036,2,1,1,5};
UINT4 Dot11MaxTransmitMSDULifetime [ ] ={1,2,840,10036,2,1,1,6};
UINT4 Dot11MaxReceiveLifetime [ ] ={1,2,840,10036,2,1,1,7};
UINT4 Dot11ManufacturerID [ ] ={1,2,840,10036,2,1,1,8};
UINT4 Dot11ProductID [ ] ={1,2,840,10036,2,1,1,9};
UINT4 Dot11CAPLimit [ ] ={1,2,840,10036,2,1,1,10};
UINT4 Dot11HCCWmin [ ] ={1,2,840,10036,2,1,1,11};
UINT4 Dot11HCCWmax [ ] ={1,2,840,10036,2,1,1,12};
UINT4 Dot11HCCAIFSN [ ] ={1,2,840,10036,2,1,1,13};
UINT4 Dot11ADDBAResponseTimeout [ ] ={1,2,840,10036,2,1,1,14};
UINT4 Dot11ADDTSResponseTimeout [ ] ={1,2,840,10036,2,1,1,15};
UINT4 Dot11ChannelUtilizationBeaconInterval [ ] ={1,2,840,10036,2,1,1,16};
UINT4 Dot11ScheduleTimeout [ ] ={1,2,840,10036,2,1,1,17};
UINT4 Dot11DLSResponseTimeout [ ] ={1,2,840,10036,2,1,1,18};
UINT4 Dot11QAPMissingAckRetryLimit [ ] ={1,2,840,10036,2,1,1,19};
UINT4 Dot11EDCAAveragingPeriod [ ] ={1,2,840,10036,2,1,1,20};
UINT4 Dot11TransmittedFragmentCount [ ] ={1,2,840,10036,2,2,1,1};
UINT4 Dot11MulticastTransmittedFrameCount [ ] ={1,2,840,10036,2,2,1,2};
UINT4 Dot11FailedCount [ ] ={1,2,840,10036,2,2,1,3};
UINT4 Dot11RetryCount [ ] ={1,2,840,10036,2,2,1,4};
UINT4 Dot11MultipleRetryCount [ ] ={1,2,840,10036,2,2,1,5};
UINT4 Dot11FrameDuplicateCount [ ] ={1,2,840,10036,2,2,1,6};
UINT4 Dot11RTSSuccessCount [ ] ={1,2,840,10036,2,2,1,7};
UINT4 Dot11RTSFailureCount [ ] ={1,2,840,10036,2,2,1,8};
UINT4 Dot11ACKFailureCount [ ] ={1,2,840,10036,2,2,1,9};
UINT4 Dot11ReceivedFragmentCount [ ] ={1,2,840,10036,2,2,1,10};
UINT4 Dot11MulticastReceivedFrameCount [ ] ={1,2,840,10036,2,2,1,11};
UINT4 Dot11FCSErrorCount [ ] ={1,2,840,10036,2,2,1,12};
UINT4 Dot11TransmittedFrameCount [ ] ={1,2,840,10036,2,2,1,13};
UINT4 Dot11WEPUndecryptableCount [ ] ={1,2,840,10036,2,2,1,14};
UINT4 Dot11QosDiscardedFragmentCount [ ] ={1,2,840,10036,2,2,1,15};
UINT4 Dot11AssociatedStationCount [ ] ={1,2,840,10036,2,2,1,16};
UINT4 Dot11QosCFPollsReceivedCount [ ] ={1,2,840,10036,2,2,1,17};
UINT4 Dot11QosCFPollsUnusedCount [ ] ={1,2,840,10036,2,2,1,18};
UINT4 Dot11QosCFPollsUnusableCount [ ] ={1,2,840,10036,2,2,1,19};
UINT4 Dot11GroupAddressesIndex [ ] ={1,2,840,10036,2,3,1,1};
UINT4 Dot11Address [ ] ={1,2,840,10036,2,3,1,2};
UINT4 Dot11GroupAddressesStatus [ ] ={1,2,840,10036,2,3,1,3};
UINT4 Dot11EDCATableIndex [ ] ={1,2,840,10036,2,4,1,1};
UINT4 Dot11EDCATableCWmin [ ] ={1,2,840,10036,2,4,1,2};
UINT4 Dot11EDCATableCWmax [ ] ={1,2,840,10036,2,4,1,3};
UINT4 Dot11EDCATableAIFSN [ ] ={1,2,840,10036,2,4,1,4};
UINT4 Dot11EDCATableTXOPLimit [ ] ={1,2,840,10036,2,4,1,5};
UINT4 Dot11EDCATableMSDULifetime [ ] ={1,2,840,10036,2,4,1,6};
UINT4 Dot11EDCATableMandatory [ ] ={1,2,840,10036,2,4,1,7};
UINT4 Dot11QAPEDCATableIndex [ ] ={1,2,840,10036,2,5,1,1};
UINT4 Dot11QAPEDCATableCWmin [ ] ={1,2,840,10036,2,5,1,2};
UINT4 Dot11QAPEDCATableCWmax [ ] ={1,2,840,10036,2,5,1,3};
UINT4 Dot11QAPEDCATableAIFSN [ ] ={1,2,840,10036,2,5,1,4};
UINT4 Dot11QAPEDCATableTXOPLimit [ ] ={1,2,840,10036,2,5,1,5};
UINT4 Dot11QAPEDCATableMSDULifetime [ ] ={1,2,840,10036,2,5,1,6};
UINT4 Dot11QAPEDCATableMandatory [ ] ={1,2,840,10036,2,5,1,7};
UINT4 Dot11QosCountersIndex [ ] ={1,2,840,10036,2,6,1,1};
UINT4 Dot11QosTransmittedFragmentCount [ ] ={1,2,840,10036,2,6,1,2};
UINT4 Dot11QosFailedCount [ ] ={1,2,840,10036,2,6,1,3};
UINT4 Dot11QosRetryCount [ ] ={1,2,840,10036,2,6,1,4};
UINT4 Dot11QosMultipleRetryCount [ ] ={1,2,840,10036,2,6,1,5};
UINT4 Dot11QosFrameDuplicateCount [ ] ={1,2,840,10036,2,6,1,6};
UINT4 Dot11QosRTSSuccessCount [ ] ={1,2,840,10036,2,6,1,7};
UINT4 Dot11QosRTSFailureCount [ ] ={1,2,840,10036,2,6,1,8};
UINT4 Dot11QosACKFailureCount [ ] ={1,2,840,10036,2,6,1,9};
UINT4 Dot11QosReceivedFragmentCount [ ] ={1,2,840,10036,2,6,1,10};
UINT4 Dot11QosTransmittedFrameCount [ ] ={1,2,840,10036,2,6,1,11};
UINT4 Dot11QosDiscardedFrameCount [ ] ={1,2,840,10036,2,6,1,12};
UINT4 Dot11QosMPDUsReceivedCount [ ] ={1,2,840,10036,2,6,1,13};
UINT4 Dot11QosRetriesReceivedCount [ ] ={1,2,840,10036,2,6,1,14};
UINT4 Dot11ResourceTypeIDName [ ] ={1,2,840,10036,3,1,1};
UINT4 Dot11manufacturerOUI [ ] ={1,2,840,10036,3,1,2,1,1};
UINT4 Dot11manufacturerName [ ] ={1,2,840,10036,3,1,2,1,2};
UINT4 Dot11manufacturerProductName [ ] ={1,2,840,10036,3,1,2,1,3};
UINT4 Dot11manufacturerProductVersion [ ] ={1,2,840,10036,3,1,2,1,4};
UINT4 Dot11PHYType [ ] ={1,2,840,10036,4,1,1,1};
UINT4 Dot11CurrentRegDomain [ ] ={1,2,840,10036,4,1,1,2};
UINT4 Dot11TempType [ ] ={1,2,840,10036,4,1,1,3};
UINT4 Dot11CurrentTxAntenna [ ] ={1,2,840,10036,4,2,1,1};
UINT4 Dot11DiversitySupport [ ] ={1,2,840,10036,4,2,1,2};
UINT4 Dot11CurrentRxAntenna [ ] ={1,2,840,10036,4,2,1,3};
UINT4 Dot11NumberSupportedPowerLevels [ ] ={1,2,840,10036,4,3,1,1};
UINT4 Dot11TxPowerLevel1 [ ] ={1,2,840,10036,4,3,1,2};
UINT4 Dot11TxPowerLevel2 [ ] ={1,2,840,10036,4,3,1,3};
UINT4 Dot11TxPowerLevel3 [ ] ={1,2,840,10036,4,3,1,4};
UINT4 Dot11TxPowerLevel4 [ ] ={1,2,840,10036,4,3,1,5};
UINT4 Dot11TxPowerLevel5 [ ] ={1,2,840,10036,4,3,1,6};
UINT4 Dot11TxPowerLevel6 [ ] ={1,2,840,10036,4,3,1,7};
UINT4 Dot11TxPowerLevel7 [ ] ={1,2,840,10036,4,3,1,8};
UINT4 Dot11TxPowerLevel8 [ ] ={1,2,840,10036,4,3,1,9};
UINT4 Dot11CurrentTxPowerLevel [ ] ={1,2,840,10036,4,3,1,10};
UINT4 Dot11HopTime [ ] ={1,2,840,10036,4,4,1,1};
UINT4 Dot11CurrentChannelNumber [ ] ={1,2,840,10036,4,4,1,2};
UINT4 Dot11MaxDwellTime [ ] ={1,2,840,10036,4,4,1,3};
UINT4 Dot11CurrentDwellTime [ ] ={1,2,840,10036,4,4,1,4};
UINT4 Dot11CurrentSet [ ] ={1,2,840,10036,4,4,1,5};
UINT4 Dot11CurrentPattern [ ] ={1,2,840,10036,4,4,1,6};
UINT4 Dot11CurrentIndex [ ] ={1,2,840,10036,4,4,1,7};
UINT4 Dot11EHCCPrimeRadix [ ] ={1,2,840,10036,4,4,1,8};
UINT4 Dot11EHCCNumberofChannelsFamilyIndex [ ] ={1,2,840,10036,4,4,1,9};
UINT4 Dot11EHCCCapabilityImplemented [ ] ={1,2,840,10036,4,4,1,10};
UINT4 Dot11EHCCCapabilityEnabled [ ] ={1,2,840,10036,4,4,1,11};
UINT4 Dot11HopAlgorithmAdopted [ ] ={1,2,840,10036,4,4,1,12};
UINT4 Dot11RandomTableFlag [ ] ={1,2,840,10036,4,4,1,13};
UINT4 Dot11NumberofHoppingSets [ ] ={1,2,840,10036,4,4,1,14};
UINT4 Dot11HopModulus [ ] ={1,2,840,10036,4,4,1,15};
UINT4 Dot11HopOffset [ ] ={1,2,840,10036,4,4,1,16};
UINT4 Dot11CurrentChannel [ ] ={1,2,840,10036,4,5,1,1};
UINT4 Dot11CCAModeSupported [ ] ={1,2,840,10036,4,5,1,2};
UINT4 Dot11CurrentCCAMode [ ] ={1,2,840,10036,4,5,1,3};
UINT4 Dot11EDThreshold [ ] ={1,2,840,10036,4,5,1,4};
UINT4 Dot11CCAWatchdogTimerMax [ ] ={1,2,840,10036,4,6,1,1};
UINT4 Dot11CCAWatchdogCountMax [ ] ={1,2,840,10036,4,6,1,2};
UINT4 Dot11CCAWatchdogTimerMin [ ] ={1,2,840,10036,4,6,1,3};
UINT4 Dot11CCAWatchdogCountMin [ ] ={1,2,840,10036,4,6,1,4};
UINT4 Dot11RegDomainsSupportedIndex [ ] ={1,2,840,10036,4,7,1,1};
UINT4 Dot11RegDomainsSupportedValue [ ] ={1,2,840,10036,4,7,1,2};
UINT4 Dot11AntennaListIndex [ ] ={1,2,840,10036,4,8,1,1};
UINT4 Dot11SupportedTxAntenna [ ] ={1,2,840,10036,4,8,1,2};
UINT4 Dot11SupportedRxAntenna [ ] ={1,2,840,10036,4,8,1,3};
UINT4 Dot11DiversitySelectionRx [ ] ={1,2,840,10036,4,8,1,4};
UINT4 Dot11SupportedDataRatesTxIndex [ ] ={1,2,840,10036,4,9,1,1};
UINT4 Dot11SupportedDataRatesTxValue [ ] ={1,2,840,10036,4,9,1,2};
UINT4 Dot11SupportedDataRatesRxIndex [ ] ={1,2,840,10036,4,10,1,1};
UINT4 Dot11SupportedDataRatesRxValue [ ] ={1,2,840,10036,4,10,1,2};
UINT4 Dot11CurrentFrequency [ ] ={1,2,840,10036,4,11,1,1};
UINT4 Dot11TIThreshold [ ] ={1,2,840,10036,4,11,1,2};
UINT4 Dot11FrequencyBandsSupported [ ] ={1,2,840,10036,4,11,1,3};
UINT4 Dot11ChannelStartingFactor [ ] ={1,2,840,10036,4,11,1,4};
UINT4 Dot11FiveMHzOperationImplemented [ ] ={1,2,840,10036,4,11,1,5};
UINT4 Dot11TenMHzOperationImplemented [ ] ={1,2,840,10036,4,11,1,6};
UINT4 Dot11TwentyMHzOperationImplemented [ ] ={1,2,840,10036,4,11,1,7};
UINT4 Dot11PhyOFDMChannelWidth [ ] ={1,2,840,10036,4,11,1,8};
UINT4 Dot11ShortPreambleOptionImplemented [ ] ={1,2,840,10036,4,12,1,1};
UINT4 Dot11PBCCOptionImplemented [ ] ={1,2,840,10036,4,12,1,2};
UINT4 Dot11ChannelAgilityPresent [ ] ={1,2,840,10036,4,12,1,3};
UINT4 Dot11ChannelAgilityEnabled [ ] ={1,2,840,10036,4,12,1,4};
UINT4 Dot11HRCCAModeSupported [ ] ={1,2,840,10036,4,12,1,5};
UINT4 Dot11HoppingPatternIndex [ ] ={1,2,840,10036,4,13,1,1};
UINT4 Dot11RandomTableFieldNumber [ ] ={1,2,840,10036,4,13,1,2};
UINT4 Dot11ERPPBCCOptionImplemented [ ] ={1,2,840,10036,4,14,1,1};
UINT4 Dot11ERPBCCOptionEnabled [ ] ={1,2,840,10036,4,14,1,2};
UINT4 Dot11DSSSOFDMOptionImplemented [ ] ={1,2,840,10036,4,14,1,3};
UINT4 Dot11DSSSOFDMOptionEnabled [ ] ={1,2,840,10036,4,14,1,4};
UINT4 Dot11ShortSlotTimeOptionImplemented [ ] ={1,2,840,10036,4,14,1,5};
UINT4 Dot11ShortSlotTimeOptionEnabled [ ] ={1,2,840,10036,4,14,1,6};
UINT4 Dot11RSNAConfigVersion [ ] ={1,2,840,10036,1,9,1,2};
UINT4 Dot11RSNAConfigPairwiseKeysSupported [ ] ={1,2,840,10036,1,9,1,3};
UINT4 Dot11RSNAConfigGroupCipher [ ] ={1,2,840,10036,1,9,1,4};
UINT4 Dot11RSNAConfigGroupRekeyMethod [ ] ={1,2,840,10036,1,9,1,5};
UINT4 Dot11RSNAConfigGroupRekeyTime [ ] ={1,2,840,10036,1,9,1,6};
UINT4 Dot11RSNAConfigGroupRekeyPackets [ ] ={1,2,840,10036,1,9,1,7};
UINT4 Dot11RSNAConfigGroupRekeyStrict [ ] ={1,2,840,10036,1,9,1,8};
UINT4 Dot11RSNAConfigPSKValue [ ] ={1,2,840,10036,1,9,1,9};
UINT4 Dot11RSNAConfigPSKPassPhrase [ ] ={1,2,840,10036,1,9,1,10};
UINT4 Dot11RSNAConfigGroupUpdateCount [ ] ={1,2,840,10036,1,9,1,13};
UINT4 Dot11RSNAConfigPairwiseUpdateCount [ ] ={1,2,840,10036,1,9,1,14};
UINT4 Dot11RSNAConfigGroupCipherSize [ ] ={1,2,840,10036,1,9,1,15};
UINT4 Dot11RSNAConfigPMKLifetime [ ] ={1,2,840,10036,1,9,1,16};
UINT4 Dot11RSNAConfigPMKReauthThreshold [ ] ={1,2,840,10036,1,9,1,17};
UINT4 Dot11RSNAConfigNumberOfPTKSAReplayCounters [ ] ={1,2,840,10036,1,9,1,18};
UINT4 Dot11RSNAConfigSATimeout [ ] ={1,2,840,10036,1,9,1,19};
UINT4 Dot11RSNAAuthenticationSuiteSelected [ ] ={1,2,840,10036,1,9,1,20};
UINT4 Dot11RSNAPairwiseCipherSelected [ ] ={1,2,840,10036,1,9,1,21};
UINT4 Dot11RSNAGroupCipherSelected [ ] ={1,2,840,10036,1,9,1,22};
UINT4 Dot11RSNAPMKIDUsed [ ] ={1,2,840,10036,1,9,1,23};
UINT4 Dot11RSNAAuthenticationSuiteRequested [ ] ={1,2,840,10036,1,9,1,24};
UINT4 Dot11RSNAPairwiseCipherRequested [ ] ={1,2,840,10036,1,9,1,25};
UINT4 Dot11RSNAGroupCipherRequested [ ] ={1,2,840,10036,1,9,1,26};
UINT4 Dot11RSNATKIPCounterMeasuresInvoked [ ] ={1,2,840,10036,1,9,1,27};
UINT4 Dot11RSNA4WayHandshakeFailures [ ] ={1,2,840,10036,1,9,1,28};
UINT4 Dot11RSNAConfigNumberOfGTKSAReplayCounters [ ] ={1,2,840,10036,1,9,1,29};
UINT4 Dot11RSNAConfigSTKKeysSupported [ ] ={1,2,840,10036,1,9,1,30};
UINT4 Dot11RSNAConfigSTKCipher [ ] ={1,2,840,10036,1,9,1,31};
UINT4 Dot11RSNAConfigSTKRekeyTime [ ] ={1,2,840,10036,1,9,1,32};
UINT4 Dot11RSNAConfigSMKUpdateCount [ ] ={1,2,840,10036,1,9,1,33};
UINT4 Dot11RSNAConfigSTKCipherSize [ ] ={1,2,840,10036,1,9,1,34};
UINT4 Dot11RSNAConfigSMKLifetime [ ] ={1,2,840,10036,1,9,1,35};
UINT4 Dot11RSNAConfigSMKReauthThreshold [ ] ={1,2,840,10036,1,9,1,36};
UINT4 Dot11RSNAConfigNumberOfSTKSAReplayCounters [ ] ={1,2,840,10036,1,9,1,37};
UINT4 Dot11RSNAPairwiseSTKSelected [ ] ={1,2,840,10036,1,9,1,38};
UINT4 Dot11RSNASMKHandshakeFailures [ ] ={1,2,840,10036,1,9,1,39};
UINT4 Dot11RSNAConfigPairwiseCipherIndex [ ] ={1,2,840,10036,1,10,1,1};
UINT4 Dot11RSNAConfigPairwiseCipher [ ] ={1,2,840,10036,1,10,1,2};
UINT4 Dot11RSNAConfigPairwiseCipherEnabled [ ] ={1,2,840,10036,1,10,1,3};
UINT4 Dot11RSNAConfigPairwiseCipherSize [ ] ={1,2,840,10036,1,10,1,4};
UINT4 Dot11RSNAConfigAuthenticationSuiteIndex [ ] ={1,2,840,10036,1,11,1,1};
UINT4 Dot11RSNAConfigAuthenticationSuite [ ] ={1,2,840,10036,1,11,1,2};
UINT4 Dot11RSNAConfigAuthenticationSuiteEnabled [ ] ={1,2,840,10036,1,11,1,3};
UINT4 Dot11RSNAStatsIndex [ ] ={1,2,840,10036,1,12,1,1};
UINT4 Dot11RSNAStatsSTAAddress [ ] ={1,2,840,10036,1,12,1,2};
UINT4 Dot11RSNAStatsVersion [ ] ={1,2,840,10036,1,12,1,3};
UINT4 Dot11RSNAStatsSelectedPairwiseCipher [ ] ={1,2,840,10036,1,12,1,4};
UINT4 Dot11RSNAStatsTKIPICVErrors [ ] ={1,2,840,10036,1,12,1,5};
UINT4 Dot11RSNAStatsTKIPLocalMICFailures [ ] ={1,2,840,10036,1,12,1,6};
UINT4 Dot11RSNAStatsTKIPRemoteMICFailures [ ] ={1,2,840,10036,1,12,1,7};
UINT4 Dot11RSNAStatsCCMPReplays [ ] ={1,2,840,10036,1,12,1,8};
UINT4 Dot11RSNAStatsCCMPDecryptErrors [ ] ={1,2,840,10036,1,12,1,9};
UINT4 Dot11RSNAStatsTKIPReplays [ ] ={1,2,840,10036,1,12,1,10};


tSNMP_OID_TYPE Dot11ResourceTypeIDNameOID = {7, Dot11ResourceTypeIDName};




tMbDbEntry Dot11StationConfigTableMibEntry[]= {

{{8,Dot11StationID}, GetNextIndexDot11StationConfigTable, Dot11StationIDGet, Dot11StationIDSet, Dot11StationIDTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 1, 0, NULL},

{{8,Dot11MediumOccupancyLimit}, GetNextIndexDot11StationConfigTable, Dot11MediumOccupancyLimitGet, Dot11MediumOccupancyLimitSet, Dot11MediumOccupancyLimitTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CFPollable}, GetNextIndexDot11StationConfigTable, Dot11CFPollableGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CFPPeriod}, GetNextIndexDot11StationConfigTable, Dot11CFPPeriodGet, Dot11CFPPeriodSet, Dot11CFPPeriodTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CFPMaxDuration}, GetNextIndexDot11StationConfigTable, Dot11CFPMaxDurationGet, Dot11CFPMaxDurationSet, Dot11CFPMaxDurationTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11AuthenticationResponseTimeOut}, GetNextIndexDot11StationConfigTable, Dot11AuthenticationResponseTimeOutGet, Dot11AuthenticationResponseTimeOutSet, Dot11AuthenticationResponseTimeOutTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11PrivacyOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11PrivacyOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11PowerManagementMode}, GetNextIndexDot11StationConfigTable, Dot11PowerManagementModeGet, Dot11PowerManagementModeSet, Dot11PowerManagementModeTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DesiredSSID}, GetNextIndexDot11StationConfigTable, Dot11DesiredSSIDGet, Dot11DesiredSSIDSet, Dot11DesiredSSIDTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DesiredBSSType}, GetNextIndexDot11StationConfigTable, Dot11DesiredBSSTypeGet, Dot11DesiredBSSTypeSet, Dot11DesiredBSSTypeTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11OperationalRateSet}, GetNextIndexDot11StationConfigTable, Dot11OperationalRateSetGet, Dot11OperationalRateSetSet, Dot11OperationalRateSetTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11BeaconPeriod}, GetNextIndexDot11StationConfigTable, Dot11BeaconPeriodGet, Dot11BeaconPeriodSet, Dot11BeaconPeriodTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DTIMPeriod}, GetNextIndexDot11StationConfigTable, Dot11DTIMPeriodGet, Dot11DTIMPeriodSet, Dot11DTIMPeriodTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11AssociationResponseTimeOut}, GetNextIndexDot11StationConfigTable, Dot11AssociationResponseTimeOutGet, Dot11AssociationResponseTimeOutSet, Dot11AssociationResponseTimeOutTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DisassociateReason}, GetNextIndexDot11StationConfigTable, Dot11DisassociateReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DisassociateStation}, GetNextIndexDot11StationConfigTable, Dot11DisassociateStationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DeauthenticateReason}, GetNextIndexDot11StationConfigTable, Dot11DeauthenticateReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DeauthenticateStation}, GetNextIndexDot11StationConfigTable, Dot11DeauthenticateStationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11AuthenticateFailStatus}, GetNextIndexDot11StationConfigTable, Dot11AuthenticateFailStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11AuthenticateFailStation}, GetNextIndexDot11StationConfigTable, Dot11AuthenticateFailStationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MultiDomainCapabilityImplemented}, GetNextIndexDot11StationConfigTable, Dot11MultiDomainCapabilityImplementedGet, Dot11MultiDomainCapabilityImplementedSet, Dot11MultiDomainCapabilityImplementedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MultiDomainCapabilityEnabled}, GetNextIndexDot11StationConfigTable, Dot11MultiDomainCapabilityEnabledGet, Dot11MultiDomainCapabilityEnabledSet, Dot11MultiDomainCapabilityEnabledTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CountryString}, GetNextIndexDot11StationConfigTable, Dot11CountryStringGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11SpectrumManagementImplemented}, GetNextIndexDot11StationConfigTable, Dot11SpectrumManagementImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11SpectrumManagementRequired}, GetNextIndexDot11StationConfigTable, Dot11SpectrumManagementRequiredGet, Dot11SpectrumManagementRequiredSet, Dot11SpectrumManagementRequiredTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11RSNAOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAPreauthenticationImplemented}, GetNextIndexDot11StationConfigTable, Dot11RSNAPreauthenticationImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RegulatoryClassesImplemented}, GetNextIndexDot11StationConfigTable, Dot11RegulatoryClassesImplementedGet, Dot11RegulatoryClassesImplementedSet, Dot11RegulatoryClassesImplementedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RegulatoryClassesRequired}, GetNextIndexDot11StationConfigTable, Dot11RegulatoryClassesRequiredGet, Dot11RegulatoryClassesRequiredSet, Dot11RegulatoryClassesRequiredTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11QosOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11QosOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ImmediateBlockAckOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11ImmediateBlockAckOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DelayedBlockAckOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11DelayedBlockAckOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DirectOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11DirectOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11APSDOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11APSDOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11QAckOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11QAckOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11QBSSLoadOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11QBSSLoadOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11QueueRequestOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11QueueRequestOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TXOPRequestOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11TXOPRequestOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MoreDataAckOptionImplemented}, GetNextIndexDot11StationConfigTable, Dot11MoreDataAckOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11AssociateinNQBSS}, GetNextIndexDot11StationConfigTable, Dot11AssociateinNQBSSGet, Dot11AssociateinNQBSSSet, Dot11AssociateinNQBSSTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DLSAllowedInQBSS}, GetNextIndexDot11StationConfigTable, Dot11DLSAllowedInQBSSGet, Dot11DLSAllowedInQBSSSet, Dot11DLSAllowedInQBSSTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DLSAllowed}, GetNextIndexDot11StationConfigTable, Dot11DLSAllowedGet, Dot11DLSAllowedSet, Dot11DLSAllowedTest, Dot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11StationConfigTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11StationConfigTableEntry = { 42, Dot11StationConfigTableMibEntry };

tMbDbEntry Dot11AuthenticationAlgorithmsTableMibEntry[]= {

{{8,Dot11AuthenticationAlgorithmsIndex}, GetNextIndexDot11AuthenticationAlgorithmsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot11AuthenticationAlgorithmsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11AuthenticationAlgorithm}, GetNextIndexDot11AuthenticationAlgorithmsTable, Dot11AuthenticationAlgorithmGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11AuthenticationAlgorithmsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11AuthenticationAlgorithmsEnable}, GetNextIndexDot11AuthenticationAlgorithmsTable, Dot11AuthenticationAlgorithmsEnableGet, Dot11AuthenticationAlgorithmsEnableSet, Dot11AuthenticationAlgorithmsEnableTest, Dot11AuthenticationAlgorithmsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11AuthenticationAlgorithmsTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11AuthenticationAlgorithmsTableEntry = { 3, Dot11AuthenticationAlgorithmsTableMibEntry };

tMbDbEntry Dot11WEPDefaultKeysTableMibEntry[]= {

{{8,Dot11WEPDefaultKeyIndex}, GetNextIndexDot11WEPDefaultKeysTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot11WEPDefaultKeysTableINDEX, 2, 0, 0, NULL},

{{8,Dot11WEPDefaultKeyValue}, GetNextIndexDot11WEPDefaultKeysTable, Dot11WEPDefaultKeyValueGet, Dot11WEPDefaultKeyValueSet, Dot11WEPDefaultKeyValueTest, Dot11WEPDefaultKeysTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11WEPDefaultKeysTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11WEPDefaultKeysTableEntry = { 2, Dot11WEPDefaultKeysTableMibEntry };

tMbDbEntry Dot11WEPKeyMappingsTableMibEntry[]= {

{{8,Dot11WEPKeyMappingIndex}, GetNextIndexDot11WEPKeyMappingsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot11WEPKeyMappingsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11WEPKeyMappingAddress}, GetNextIndexDot11WEPKeyMappingsTable, Dot11WEPKeyMappingAddressGet, Dot11WEPKeyMappingAddressSet, Dot11WEPKeyMappingAddressTest, Dot11WEPKeyMappingsTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot11WEPKeyMappingsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11WEPKeyMappingWEPOn}, GetNextIndexDot11WEPKeyMappingsTable, Dot11WEPKeyMappingWEPOnGet, Dot11WEPKeyMappingWEPOnSet, Dot11WEPKeyMappingWEPOnTest, Dot11WEPKeyMappingsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WEPKeyMappingsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11WEPKeyMappingValue}, GetNextIndexDot11WEPKeyMappingsTable, Dot11WEPKeyMappingValueGet, Dot11WEPKeyMappingValueSet, Dot11WEPKeyMappingValueTest, Dot11WEPKeyMappingsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11WEPKeyMappingsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11WEPKeyMappingStatus}, GetNextIndexDot11WEPKeyMappingsTable, Dot11WEPKeyMappingStatusGet, Dot11WEPKeyMappingStatusSet, Dot11WEPKeyMappingStatusTest, Dot11WEPKeyMappingsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11WEPKeyMappingsTableINDEX, 2, 0, 1, "1"},
};
tMibData Dot11WEPKeyMappingsTableEntry = { 5, Dot11WEPKeyMappingsTableMibEntry };

tMbDbEntry Dot11PrivacyTableMibEntry[]= {

{{8,Dot11PrivacyInvoked}, GetNextIndexDot11PrivacyTable, Dot11PrivacyInvokedGet, Dot11PrivacyInvokedSet, Dot11PrivacyInvokedTest, Dot11PrivacyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PrivacyTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WEPDefaultKeyID}, GetNextIndexDot11PrivacyTable, Dot11WEPDefaultKeyIDGet, Dot11WEPDefaultKeyIDSet, Dot11WEPDefaultKeyIDTest, Dot11PrivacyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PrivacyTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WEPKeyMappingLength}, GetNextIndexDot11PrivacyTable, Dot11WEPKeyMappingLengthGet, Dot11WEPKeyMappingLengthSet, Dot11WEPKeyMappingLengthTest, Dot11PrivacyTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11PrivacyTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ExcludeUnencrypted}, GetNextIndexDot11PrivacyTable, Dot11ExcludeUnencryptedGet, Dot11ExcludeUnencryptedSet, Dot11ExcludeUnencryptedTest, Dot11PrivacyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PrivacyTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WEPICVErrorCount}, GetNextIndexDot11PrivacyTable, Dot11WEPICVErrorCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11PrivacyTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WEPExcludedCount}, GetNextIndexDot11PrivacyTable, Dot11WEPExcludedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11PrivacyTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAEnabled}, GetNextIndexDot11PrivacyTable, Dot11RSNAEnabledGet, Dot11RSNAEnabledSet, Dot11RSNAEnabledTest, Dot11PrivacyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PrivacyTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAPreauthenticationEnabled}, GetNextIndexDot11PrivacyTable, Dot11RSNAPreauthenticationEnabledGet, Dot11RSNAPreauthenticationEnabledSet, Dot11RSNAPreauthenticationEnabledTest, Dot11PrivacyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PrivacyTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11PrivacyTableEntry = { 8, Dot11PrivacyTableMibEntry };

tMbDbEntry Dot11MultiDomainCapabilityTableMibEntry[]= {

{{8,Dot11MultiDomainCapabilityIndex}, GetNextIndexDot11MultiDomainCapabilityTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot11MultiDomainCapabilityTableINDEX, 2, 0, 0, NULL},

{{8,Dot11FirstChannelNumber}, GetNextIndexDot11MultiDomainCapabilityTable, Dot11FirstChannelNumberGet, Dot11FirstChannelNumberSet, Dot11FirstChannelNumberTest, Dot11MultiDomainCapabilityTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11MultiDomainCapabilityTableINDEX, 2, 0, 0, NULL},

{{8,Dot11NumberofChannels}, GetNextIndexDot11MultiDomainCapabilityTable, Dot11NumberofChannelsGet, Dot11NumberofChannelsSet, Dot11NumberofChannelsTest, Dot11MultiDomainCapabilityTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11MultiDomainCapabilityTableINDEX, 2, 0, 0, NULL},

{{8,Dot11MaximumTransmitPowerLevel}, GetNextIndexDot11MultiDomainCapabilityTable, Dot11MaximumTransmitPowerLevelGet, Dot11MaximumTransmitPowerLevelSet, Dot11MaximumTransmitPowerLevelTest, Dot11MultiDomainCapabilityTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11MultiDomainCapabilityTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11MultiDomainCapabilityTableEntry = { 4, Dot11MultiDomainCapabilityTableMibEntry };

tMbDbEntry Dot11SpectrumManagementTableMibEntry[]= {

{{8,Dot11SpectrumManagementIndex}, GetNextIndexDot11SpectrumManagementTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot11SpectrumManagementTableINDEX, 2, 0, 0, NULL},

{{8,Dot11MitigationRequirement}, GetNextIndexDot11SpectrumManagementTable, Dot11MitigationRequirementGet, Dot11MitigationRequirementSet, Dot11MitigationRequirementTest, Dot11SpectrumManagementTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11SpectrumManagementTableINDEX, 2, 0, 0, NULL},

{{8,Dot11ChannelSwitchTime}, GetNextIndexDot11SpectrumManagementTable, Dot11ChannelSwitchTimeGet, Dot11ChannelSwitchTimeSet, Dot11ChannelSwitchTimeTest, Dot11SpectrumManagementTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11SpectrumManagementTableINDEX, 2, 0, 0, NULL},

{{8,Dot11PowerCapabilityMax}, GetNextIndexDot11SpectrumManagementTable, Dot11PowerCapabilityMaxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11SpectrumManagementTableINDEX, 2, 0, 0, NULL},

{{8,Dot11PowerCapabilityMin}, GetNextIndexDot11SpectrumManagementTable, Dot11PowerCapabilityMinGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11SpectrumManagementTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11SpectrumManagementTableEntry = { 5, Dot11SpectrumManagementTableMibEntry };

tMbDbEntry Dot11RSNAConfigTableMibEntry[]= {

{{8,Dot11RSNAConfigVersion}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigPairwiseKeysSupported}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigPairwiseKeysSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigGroupCipher}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigGroupCipherGet, Dot11RSNAConfigGroupCipherSet, Dot11RSNAConfigGroupCipherTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigGroupRekeyMethod}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigGroupRekeyMethodGet, Dot11RSNAConfigGroupRekeyMethodSet, Dot11RSNAConfigGroupRekeyMethodTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, "2"},

{{8,Dot11RSNAConfigGroupRekeyTime}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigGroupRekeyTimeGet, Dot11RSNAConfigGroupRekeyTimeSet, Dot11RSNAConfigGroupRekeyTimeTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, "86400"},

{{8,Dot11RSNAConfigGroupRekeyPackets}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigGroupRekeyPacketsGet, Dot11RSNAConfigGroupRekeyPacketsSet, Dot11RSNAConfigGroupRekeyPacketsTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigGroupRekeyStrict}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigGroupRekeyStrictGet, Dot11RSNAConfigGroupRekeyStrictSet, Dot11RSNAConfigGroupRekeyStrictTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigPSKValue}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigPSKValueGet, Dot11RSNAConfigPSKValueSet, Dot11RSNAConfigPSKValueTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigPSKPassPhrase}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigPSKPassPhraseGet, Dot11RSNAConfigPSKPassPhraseSet, Dot11RSNAConfigPSKPassPhraseTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigGroupUpdateCount}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigGroupUpdateCountGet, Dot11RSNAConfigGroupUpdateCountSet, Dot11RSNAConfigGroupUpdateCountTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, "3"},

{{8,Dot11RSNAConfigPairwiseUpdateCount}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigPairwiseUpdateCountGet, Dot11RSNAConfigPairwiseUpdateCountSet, Dot11RSNAConfigPairwiseUpdateCountTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, "3"},

{{8,Dot11RSNAConfigGroupCipherSize}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigGroupCipherSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigPMKLifetime}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigPMKLifetimeGet, Dot11RSNAConfigPMKLifetimeSet, Dot11RSNAConfigPMKLifetimeTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, "43200"},

{{8,Dot11RSNAConfigPMKReauthThreshold}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigPMKReauthThresholdGet, Dot11RSNAConfigPMKReauthThresholdSet, Dot11RSNAConfigPMKReauthThresholdTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, "70"},

{{8,Dot11RSNAConfigNumberOfPTKSAReplayCounters}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigNumberOfPTKSAReplayCountersGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigSATimeout}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigSATimeoutGet, Dot11RSNAConfigSATimeoutSet, Dot11RSNAConfigSATimeoutTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, "60"},

{{8,Dot11RSNAAuthenticationSuiteSelected}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAAuthenticationSuiteSelectedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAPairwiseCipherSelected}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAPairwiseCipherSelectedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAGroupCipherSelected}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAGroupCipherSelectedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAPMKIDUsed}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAPMKIDUsedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAAuthenticationSuiteRequested}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAAuthenticationSuiteRequestedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAPairwiseCipherRequested}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAPairwiseCipherRequestedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAGroupCipherRequested}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAGroupCipherRequestedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNATKIPCounterMeasuresInvoked}, GetNextIndexDot11RSNAConfigTable, Dot11RSNATKIPCounterMeasuresInvokedGet, Dot11RSNATKIPCounterMeasuresInvokedSet, Dot11RSNATKIPCounterMeasuresInvokedTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNA4WayHandshakeFailures}, GetNextIndexDot11RSNAConfigTable, Dot11RSNA4WayHandshakeFailuresGet, Dot11RSNA4WayHandshakeFailuresSet, Dot11RSNA4WayHandshakeFailuresTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigNumberOfGTKSAReplayCounters}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigNumberOfGTKSAReplayCountersGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigSTKKeysSupported}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigSTKKeysSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigSTKCipher}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigSTKCipherGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigSTKRekeyTime}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigSTKRekeyTimeGet, Dot11RSNAConfigSTKRekeyTimeSet, Dot11RSNAConfigSTKRekeyTimeTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, "86400"},

{{8,Dot11RSNAConfigSMKUpdateCount}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigSMKUpdateCountGet, Dot11RSNAConfigSMKUpdateCountSet, Dot11RSNAConfigSMKUpdateCountTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, "3"},

{{8,Dot11RSNAConfigSTKCipherSize}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigSTKCipherSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigSMKLifetime}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigSMKLifetimeGet, Dot11RSNAConfigSMKLifetimeSet, Dot11RSNAConfigSMKLifetimeTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, "43200"},

{{8,Dot11RSNAConfigSMKReauthThreshold}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigSMKReauthThresholdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigNumberOfSTKSAReplayCounters}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAConfigNumberOfSTKSAReplayCountersGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAPairwiseSTKSelected}, GetNextIndexDot11RSNAConfigTable, Dot11RSNAPairwiseSTKSelectedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNASMKHandshakeFailures}, GetNextIndexDot11RSNAConfigTable, Dot11RSNASMKHandshakeFailuresGet, Dot11RSNASMKHandshakeFailuresSet, Dot11RSNASMKHandshakeFailuresTest, Dot11RSNAConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11RSNAConfigTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11RSNAConfigTableEntry = { 36, Dot11RSNAConfigTableMibEntry };

tMbDbEntry Dot11RSNAConfigPairwiseCiphersTableMibEntry[]= {

{{8,Dot11RSNAConfigPairwiseCipherIndex}, GetNextIndexDot11RSNAConfigPairwiseCiphersTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11RSNAConfigPairwiseCiphersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAConfigPairwiseCipher}, GetNextIndexDot11RSNAConfigPairwiseCiphersTable, Dot11RSNAConfigPairwiseCipherGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAConfigPairwiseCiphersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAConfigPairwiseCipherEnabled}, GetNextIndexDot11RSNAConfigPairwiseCiphersTable, Dot11RSNAConfigPairwiseCipherEnabledGet, Dot11RSNAConfigPairwiseCipherEnabledSet, Dot11RSNAConfigPairwiseCipherEnabledTest, Dot11RSNAConfigPairwiseCiphersTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RSNAConfigPairwiseCiphersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAConfigPairwiseCipherSize}, GetNextIndexDot11RSNAConfigPairwiseCiphersTable, Dot11RSNAConfigPairwiseCipherSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11RSNAConfigPairwiseCiphersTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11RSNAConfigPairwiseCiphersTableEntry = { 4, Dot11RSNAConfigPairwiseCiphersTableMibEntry };

tMbDbEntry Dot11RSNAConfigAuthenticationSuitesTableMibEntry[]= {

{{8,Dot11RSNAConfigAuthenticationSuiteIndex}, GetNextIndexDot11RSNAConfigAuthenticationSuitesTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11RSNAConfigAuthenticationSuitesTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigAuthenticationSuite}, GetNextIndexDot11RSNAConfigAuthenticationSuitesTable, Dot11RSNAConfigAuthenticationSuiteGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAConfigAuthenticationSuitesTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RSNAConfigAuthenticationSuiteEnabled}, GetNextIndexDot11RSNAConfigAuthenticationSuitesTable, Dot11RSNAConfigAuthenticationSuiteEnabledGet, Dot11RSNAConfigAuthenticationSuiteEnabledSet, Dot11RSNAConfigAuthenticationSuiteEnabledTest, Dot11RSNAConfigAuthenticationSuitesTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RSNAConfigAuthenticationSuitesTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11RSNAConfigAuthenticationSuitesTableEntry = { 3, Dot11RSNAConfigAuthenticationSuitesTableMibEntry };

tMbDbEntry Dot11RSNAStatsTableMibEntry[]= {

{{8,Dot11RSNAStatsIndex}, GetNextIndexDot11RSNAStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAStatsSTAAddress}, GetNextIndexDot11RSNAStatsTable, Dot11RSNAStatsSTAAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAStatsVersion}, GetNextIndexDot11RSNAStatsTable, Dot11RSNAStatsVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAStatsSelectedPairwiseCipher}, GetNextIndexDot11RSNAStatsTable, Dot11RSNAStatsSelectedPairwiseCipherGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAStatsTKIPICVErrors}, GetNextIndexDot11RSNAStatsTable, Dot11RSNAStatsTKIPICVErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAStatsTKIPLocalMICFailures}, GetNextIndexDot11RSNAStatsTable, Dot11RSNAStatsTKIPLocalMICFailuresGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAStatsTKIPRemoteMICFailures}, GetNextIndexDot11RSNAStatsTable, Dot11RSNAStatsTKIPRemoteMICFailuresGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAStatsCCMPReplays}, GetNextIndexDot11RSNAStatsTable, Dot11RSNAStatsCCMPReplaysGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAStatsCCMPDecryptErrors}, GetNextIndexDot11RSNAStatsTable, Dot11RSNAStatsCCMPDecryptErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RSNAStatsTKIPReplays}, GetNextIndexDot11RSNAStatsTable, Dot11RSNAStatsTKIPReplaysGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11RSNAStatsTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11RSNAStatsTableEntry = { 10, Dot11RSNAStatsTableMibEntry };

tMbDbEntry Dot11RegulatoryClassesTableMibEntry[]= {

{{8,Dot11RegulatoryClassesIndex}, GetNextIndexDot11RegulatoryClassesTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot11RegulatoryClassesTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RegulatoryClass}, GetNextIndexDot11RegulatoryClassesTable, Dot11RegulatoryClassGet, Dot11RegulatoryClassSet, Dot11RegulatoryClassTest, Dot11RegulatoryClassesTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RegulatoryClassesTableINDEX, 2, 0, 0, NULL},

{{8,Dot11CoverageClass}, GetNextIndexDot11RegulatoryClassesTable, Dot11CoverageClassGet, Dot11CoverageClassSet, Dot11CoverageClassTest, Dot11RegulatoryClassesTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11RegulatoryClassesTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11RegulatoryClassesTableEntry = { 3, Dot11RegulatoryClassesTableMibEntry };

tMbDbEntry Dot11OperationTableMibEntry[]= {

{{8,Dot11MACAddress}, GetNextIndexDot11OperationTable, Dot11MACAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RTSThreshold}, GetNextIndexDot11OperationTable, Dot11RTSThresholdGet, Dot11RTSThresholdSet, Dot11RTSThresholdTest, Dot11OperationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ShortRetryLimit}, GetNextIndexDot11OperationTable, Dot11ShortRetryLimitGet, Dot11ShortRetryLimitSet, Dot11ShortRetryLimitTest, Dot11OperationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11LongRetryLimit}, GetNextIndexDot11OperationTable, Dot11LongRetryLimitGet, Dot11LongRetryLimitSet, Dot11LongRetryLimitTest, Dot11OperationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11FragmentationThreshold}, GetNextIndexDot11OperationTable, Dot11FragmentationThresholdGet, Dot11FragmentationThresholdSet, Dot11FragmentationThresholdTest, Dot11OperationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MaxTransmitMSDULifetime}, GetNextIndexDot11OperationTable, Dot11MaxTransmitMSDULifetimeGet, Dot11MaxTransmitMSDULifetimeSet, Dot11MaxTransmitMSDULifetimeTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MaxReceiveLifetime}, GetNextIndexDot11OperationTable, Dot11MaxReceiveLifetimeGet, Dot11MaxReceiveLifetimeSet, Dot11MaxReceiveLifetimeTest, Dot11OperationTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ManufacturerID}, GetNextIndexDot11OperationTable, Dot11ManufacturerIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ProductID}, GetNextIndexDot11OperationTable, Dot11ProductIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CAPLimit}, GetNextIndexDot11OperationTable, Dot11CAPLimitGet, Dot11CAPLimitSet, Dot11CAPLimitTest, Dot11OperationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11HCCWmin}, GetNextIndexDot11OperationTable, Dot11HCCWminGet, Dot11HCCWminSet, Dot11HCCWminTest, Dot11OperationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11HCCWmax}, GetNextIndexDot11OperationTable, Dot11HCCWmaxGet, Dot11HCCWmaxSet, Dot11HCCWmaxTest, Dot11OperationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11HCCAIFSN}, GetNextIndexDot11OperationTable, Dot11HCCAIFSNGet, Dot11HCCAIFSNSet, Dot11HCCAIFSNTest, Dot11OperationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ADDBAResponseTimeout}, GetNextIndexDot11OperationTable, Dot11ADDBAResponseTimeoutGet, Dot11ADDBAResponseTimeoutSet, Dot11ADDBAResponseTimeoutTest, Dot11OperationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ADDTSResponseTimeout}, GetNextIndexDot11OperationTable, Dot11ADDTSResponseTimeoutGet, Dot11ADDTSResponseTimeoutSet, Dot11ADDTSResponseTimeoutTest, Dot11OperationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ChannelUtilizationBeaconInterval}, GetNextIndexDot11OperationTable, Dot11ChannelUtilizationBeaconIntervalGet, Dot11ChannelUtilizationBeaconIntervalSet, Dot11ChannelUtilizationBeaconIntervalTest, Dot11OperationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ScheduleTimeout}, GetNextIndexDot11OperationTable, Dot11ScheduleTimeoutGet, Dot11ScheduleTimeoutSet, Dot11ScheduleTimeoutTest, Dot11OperationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DLSResponseTimeout}, GetNextIndexDot11OperationTable, Dot11DLSResponseTimeoutGet, Dot11DLSResponseTimeoutSet, Dot11DLSResponseTimeoutTest, Dot11OperationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11QAPMissingAckRetryLimit}, GetNextIndexDot11OperationTable, Dot11QAPMissingAckRetryLimitGet, Dot11QAPMissingAckRetryLimitSet, Dot11QAPMissingAckRetryLimitTest, Dot11OperationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11EDCAAveragingPeriod}, GetNextIndexDot11OperationTable, Dot11EDCAAveragingPeriodGet, Dot11EDCAAveragingPeriodSet, Dot11EDCAAveragingPeriodTest, Dot11OperationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11OperationTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11OperationTableEntry = { 20, Dot11OperationTableMibEntry };

tMbDbEntry Dot11CountersTableMibEntry[]= {

{{8,Dot11TransmittedFragmentCount}, GetNextIndexDot11CountersTable, Dot11TransmittedFragmentCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MulticastTransmittedFrameCount}, GetNextIndexDot11CountersTable, Dot11MulticastTransmittedFrameCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11FailedCount}, GetNextIndexDot11CountersTable, Dot11FailedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RetryCount}, GetNextIndexDot11CountersTable, Dot11RetryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MultipleRetryCount}, GetNextIndexDot11CountersTable, Dot11MultipleRetryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11FrameDuplicateCount}, GetNextIndexDot11CountersTable, Dot11FrameDuplicateCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RTSSuccessCount}, GetNextIndexDot11CountersTable, Dot11RTSSuccessCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RTSFailureCount}, GetNextIndexDot11CountersTable, Dot11RTSFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ACKFailureCount}, GetNextIndexDot11CountersTable, Dot11ACKFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ReceivedFragmentCount}, GetNextIndexDot11CountersTable, Dot11ReceivedFragmentCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MulticastReceivedFrameCount}, GetNextIndexDot11CountersTable, Dot11MulticastReceivedFrameCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11FCSErrorCount}, GetNextIndexDot11CountersTable, Dot11FCSErrorCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TransmittedFrameCount}, GetNextIndexDot11CountersTable, Dot11TransmittedFrameCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11WEPUndecryptableCount}, GetNextIndexDot11CountersTable, Dot11WEPUndecryptableCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11QosDiscardedFragmentCount}, GetNextIndexDot11CountersTable, Dot11QosDiscardedFragmentCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11AssociatedStationCount}, GetNextIndexDot11CountersTable, Dot11AssociatedStationCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11QosCFPollsReceivedCount}, GetNextIndexDot11CountersTable, Dot11QosCFPollsReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11QosCFPollsUnusedCount}, GetNextIndexDot11CountersTable, Dot11QosCFPollsUnusedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},

{{8,Dot11QosCFPollsUnusableCount}, GetNextIndexDot11CountersTable, Dot11QosCFPollsUnusableCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11CountersTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11CountersTableEntry = { 19, Dot11CountersTableMibEntry };

tMbDbEntry Dot11GroupAddressesTableMibEntry[]= {

{{8,Dot11GroupAddressesIndex}, GetNextIndexDot11GroupAddressesTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot11GroupAddressesTableINDEX, 2, 0, 0, NULL},

{{8,Dot11Address}, GetNextIndexDot11GroupAddressesTable, Dot11AddressGet, Dot11AddressSet, Dot11AddressTest, Dot11GroupAddressesTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot11GroupAddressesTableINDEX, 2, 0, 0, NULL},

{{8,Dot11GroupAddressesStatus}, GetNextIndexDot11GroupAddressesTable, Dot11GroupAddressesStatusGet, Dot11GroupAddressesStatusSet, Dot11GroupAddressesStatusTest, Dot11GroupAddressesTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11GroupAddressesTableINDEX, 2, 0, 1, "1"},
};
tMibData Dot11GroupAddressesTableEntry = { 3, Dot11GroupAddressesTableMibEntry };

tMbDbEntry Dot11EDCATableMibEntry[]= {

{{8,Dot11EDCATableIndex}, GetNextIndexDot11EDCATable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot11EDCATableINDEX, 2, 0, 0, NULL},

{{8,Dot11EDCATableCWmin}, GetNextIndexDot11EDCATable, Dot11EDCATableCWminGet, Dot11EDCATableCWminSet, Dot11EDCATableCWminTest, Dot11EDCATableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11EDCATableINDEX, 2, 0, 0, NULL},

{{8,Dot11EDCATableCWmax}, GetNextIndexDot11EDCATable, Dot11EDCATableCWmaxGet, Dot11EDCATableCWmaxSet, Dot11EDCATableCWmaxTest, Dot11EDCATableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11EDCATableINDEX, 2, 0, 0, NULL},

{{8,Dot11EDCATableAIFSN}, GetNextIndexDot11EDCATable, Dot11EDCATableAIFSNGet, Dot11EDCATableAIFSNSet, Dot11EDCATableAIFSNTest, Dot11EDCATableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11EDCATableINDEX, 2, 0, 0, NULL},

{{8,Dot11EDCATableTXOPLimit}, GetNextIndexDot11EDCATable, Dot11EDCATableTXOPLimitGet, Dot11EDCATableTXOPLimitSet, Dot11EDCATableTXOPLimitTest, Dot11EDCATableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11EDCATableINDEX, 2, 0, 0, NULL},

{{8,Dot11EDCATableMSDULifetime}, GetNextIndexDot11EDCATable, Dot11EDCATableMSDULifetimeGet, Dot11EDCATableMSDULifetimeSet, Dot11EDCATableMSDULifetimeTest, Dot11EDCATableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11EDCATableINDEX, 2, 0, 0, NULL},

{{8,Dot11EDCATableMandatory}, GetNextIndexDot11EDCATable, Dot11EDCATableMandatoryGet, Dot11EDCATableMandatorySet, Dot11EDCATableMandatoryTest, Dot11EDCATableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11EDCATableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11EDCATableEntry = { 7, Dot11EDCATableMibEntry };

tMbDbEntry Dot11QAPEDCATableMibEntry[]= {

{{8,Dot11QAPEDCATableIndex}, GetNextIndexDot11QAPEDCATable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot11QAPEDCATableINDEX, 2, 0, 0, NULL},

{{8,Dot11QAPEDCATableCWmin}, GetNextIndexDot11QAPEDCATable, Dot11QAPEDCATableCWminGet, Dot11QAPEDCATableCWminSet, Dot11QAPEDCATableCWminTest, Dot11QAPEDCATableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11QAPEDCATableINDEX, 2, 0, 0, NULL},

{{8,Dot11QAPEDCATableCWmax}, GetNextIndexDot11QAPEDCATable, Dot11QAPEDCATableCWmaxGet, Dot11QAPEDCATableCWmaxSet, Dot11QAPEDCATableCWmaxTest, Dot11QAPEDCATableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11QAPEDCATableINDEX, 2, 0, 0, NULL},

{{8,Dot11QAPEDCATableAIFSN}, GetNextIndexDot11QAPEDCATable, Dot11QAPEDCATableAIFSNGet, Dot11QAPEDCATableAIFSNSet, Dot11QAPEDCATableAIFSNTest, Dot11QAPEDCATableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11QAPEDCATableINDEX, 2, 0, 0, NULL},

{{8,Dot11QAPEDCATableTXOPLimit}, GetNextIndexDot11QAPEDCATable, Dot11QAPEDCATableTXOPLimitGet, Dot11QAPEDCATableTXOPLimitSet, Dot11QAPEDCATableTXOPLimitTest, Dot11QAPEDCATableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11QAPEDCATableINDEX, 2, 0, 0, NULL},

{{8,Dot11QAPEDCATableMSDULifetime}, GetNextIndexDot11QAPEDCATable, Dot11QAPEDCATableMSDULifetimeGet, Dot11QAPEDCATableMSDULifetimeSet, Dot11QAPEDCATableMSDULifetimeTest, Dot11QAPEDCATableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11QAPEDCATableINDEX, 2, 0, 0, NULL},

{{8,Dot11QAPEDCATableMandatory}, GetNextIndexDot11QAPEDCATable, Dot11QAPEDCATableMandatoryGet, Dot11QAPEDCATableMandatorySet, Dot11QAPEDCATableMandatoryTest, Dot11QAPEDCATableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11QAPEDCATableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11QAPEDCATableEntry = { 7, Dot11QAPEDCATableMibEntry };

tMbDbEntry Dot11QosCountersTableMibEntry[]= {

{{8,Dot11QosCountersIndex}, GetNextIndexDot11QosCountersTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosTransmittedFragmentCount}, GetNextIndexDot11QosCountersTable, Dot11QosTransmittedFragmentCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosFailedCount}, GetNextIndexDot11QosCountersTable, Dot11QosFailedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosRetryCount}, GetNextIndexDot11QosCountersTable, Dot11QosRetryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosMultipleRetryCount}, GetNextIndexDot11QosCountersTable, Dot11QosMultipleRetryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosFrameDuplicateCount}, GetNextIndexDot11QosCountersTable, Dot11QosFrameDuplicateCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosRTSSuccessCount}, GetNextIndexDot11QosCountersTable, Dot11QosRTSSuccessCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosRTSFailureCount}, GetNextIndexDot11QosCountersTable, Dot11QosRTSFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosACKFailureCount}, GetNextIndexDot11QosCountersTable, Dot11QosACKFailureCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosReceivedFragmentCount}, GetNextIndexDot11QosCountersTable, Dot11QosReceivedFragmentCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosTransmittedFrameCount}, GetNextIndexDot11QosCountersTable, Dot11QosTransmittedFrameCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosDiscardedFrameCount}, GetNextIndexDot11QosCountersTable, Dot11QosDiscardedFrameCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosMPDUsReceivedCount}, GetNextIndexDot11QosCountersTable, Dot11QosMPDUsReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},

{{8,Dot11QosRetriesReceivedCount}, GetNextIndexDot11QosCountersTable, Dot11QosRetriesReceivedCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot11QosCountersTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11QosCountersTableEntry = { 14, Dot11QosCountersTableMibEntry };

tMbDbEntry Dot11ResourceTypeIDNameMibEntry[]= {

{{7,Dot11ResourceTypeIDName}, NULL, Dot11ResourceTypeIDNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, "RTID"},
};
tMibData Dot11ResourceTypeIDNameEntry = { 1, Dot11ResourceTypeIDNameMibEntry };

tMbDbEntry Dot11ResourceInfoTableMibEntry[]= {

{{9,Dot11manufacturerOUI}, GetNextIndexDot11ResourceInfoTable, Dot11manufacturerOUIGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11ResourceInfoTableINDEX, 1, 0, 0, NULL},

{{9,Dot11manufacturerName}, GetNextIndexDot11ResourceInfoTable, Dot11manufacturerNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11ResourceInfoTableINDEX, 1, 0, 0, NULL},

{{9,Dot11manufacturerProductName}, GetNextIndexDot11ResourceInfoTable, Dot11manufacturerProductNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11ResourceInfoTableINDEX, 1, 0, 0, NULL},

{{9,Dot11manufacturerProductVersion}, GetNextIndexDot11ResourceInfoTable, Dot11manufacturerProductVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot11ResourceInfoTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11ResourceInfoTableEntry = { 4, Dot11ResourceInfoTableMibEntry };

tMbDbEntry Dot11PhyOperationTableMibEntry[]= {

{{8,Dot11PHYType}, GetNextIndexDot11PhyOperationTable, Dot11PHYTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyOperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CurrentRegDomain}, GetNextIndexDot11PhyOperationTable, Dot11CurrentRegDomainGet, Dot11CurrentRegDomainSet, Dot11CurrentRegDomainTest, Dot11PhyOperationTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11PhyOperationTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TempType}, GetNextIndexDot11PhyOperationTable, Dot11TempTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyOperationTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11PhyOperationTableEntry = { 3, Dot11PhyOperationTableMibEntry };

tMbDbEntry Dot11PhyAntennaTableMibEntry[]= {

{{8,Dot11CurrentTxAntenna}, GetNextIndexDot11PhyAntennaTable, Dot11CurrentTxAntennaGet, Dot11CurrentTxAntennaSet, Dot11CurrentTxAntennaTest, Dot11PhyAntennaTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11PhyAntennaTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DiversitySupport}, GetNextIndexDot11PhyAntennaTable, Dot11DiversitySupportGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyAntennaTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CurrentRxAntenna}, GetNextIndexDot11PhyAntennaTable, Dot11CurrentRxAntennaGet, Dot11CurrentRxAntennaSet, Dot11CurrentRxAntennaTest, Dot11PhyAntennaTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11PhyAntennaTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11PhyAntennaTableEntry = { 3, Dot11PhyAntennaTableMibEntry };

tMbDbEntry Dot11PhyTxPowerTableMibEntry[]= {

{{8,Dot11NumberSupportedPowerLevels}, GetNextIndexDot11PhyTxPowerTable, Dot11NumberSupportedPowerLevelsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyTxPowerTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TxPowerLevel1}, GetNextIndexDot11PhyTxPowerTable, Dot11TxPowerLevel1Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyTxPowerTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TxPowerLevel2}, GetNextIndexDot11PhyTxPowerTable, Dot11TxPowerLevel2Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyTxPowerTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TxPowerLevel3}, GetNextIndexDot11PhyTxPowerTable, Dot11TxPowerLevel3Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyTxPowerTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TxPowerLevel4}, GetNextIndexDot11PhyTxPowerTable, Dot11TxPowerLevel4Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyTxPowerTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TxPowerLevel5}, GetNextIndexDot11PhyTxPowerTable, Dot11TxPowerLevel5Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyTxPowerTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TxPowerLevel6}, GetNextIndexDot11PhyTxPowerTable, Dot11TxPowerLevel6Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyTxPowerTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TxPowerLevel7}, GetNextIndexDot11PhyTxPowerTable, Dot11TxPowerLevel7Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyTxPowerTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TxPowerLevel8}, GetNextIndexDot11PhyTxPowerTable, Dot11TxPowerLevel8Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyTxPowerTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CurrentTxPowerLevel}, GetNextIndexDot11PhyTxPowerTable, Dot11CurrentTxPowerLevelGet, Dot11CurrentTxPowerLevelSet, Dot11CurrentTxPowerLevelTest, Dot11PhyTxPowerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyTxPowerTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11PhyTxPowerTableEntry = { 10, Dot11PhyTxPowerTableMibEntry };

tMbDbEntry Dot11PhyFHSSTableMibEntry[]= {

{{8,Dot11HopTime}, GetNextIndexDot11PhyFHSSTable, Dot11HopTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CurrentChannelNumber}, GetNextIndexDot11PhyFHSSTable, Dot11CurrentChannelNumberGet, Dot11CurrentChannelNumberSet, Dot11CurrentChannelNumberTest, Dot11PhyFHSSTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11MaxDwellTime}, GetNextIndexDot11PhyFHSSTable, Dot11MaxDwellTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CurrentDwellTime}, GetNextIndexDot11PhyFHSSTable, Dot11CurrentDwellTimeGet, Dot11CurrentDwellTimeSet, Dot11CurrentDwellTimeTest, Dot11PhyFHSSTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CurrentSet}, GetNextIndexDot11PhyFHSSTable, Dot11CurrentSetGet, Dot11CurrentSetSet, Dot11CurrentSetTest, Dot11PhyFHSSTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CurrentPattern}, GetNextIndexDot11PhyFHSSTable, Dot11CurrentPatternGet, Dot11CurrentPatternSet, Dot11CurrentPatternTest, Dot11PhyFHSSTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CurrentIndex}, GetNextIndexDot11PhyFHSSTable, Dot11CurrentIndexGet, Dot11CurrentIndexSet, Dot11CurrentIndexTest, Dot11PhyFHSSTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11EHCCPrimeRadix}, GetNextIndexDot11PhyFHSSTable, Dot11EHCCPrimeRadixGet, Dot11EHCCPrimeRadixSet, Dot11EHCCPrimeRadixTest, Dot11PhyFHSSTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11EHCCNumberofChannelsFamilyIndex}, GetNextIndexDot11PhyFHSSTable, Dot11EHCCNumberofChannelsFamilyIndexGet, Dot11EHCCNumberofChannelsFamilyIndexSet, Dot11EHCCNumberofChannelsFamilyIndexTest, Dot11PhyFHSSTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11EHCCCapabilityImplemented}, GetNextIndexDot11PhyFHSSTable, Dot11EHCCCapabilityImplementedGet, Dot11EHCCCapabilityImplementedSet, Dot11EHCCCapabilityImplementedTest, Dot11PhyFHSSTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11EHCCCapabilityEnabled}, GetNextIndexDot11PhyFHSSTable, Dot11EHCCCapabilityEnabledGet, Dot11EHCCCapabilityEnabledSet, Dot11EHCCCapabilityEnabledTest, Dot11PhyFHSSTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11HopAlgorithmAdopted}, GetNextIndexDot11PhyFHSSTable, Dot11HopAlgorithmAdoptedGet, Dot11HopAlgorithmAdoptedSet, Dot11HopAlgorithmAdoptedTest, Dot11PhyFHSSTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11RandomTableFlag}, GetNextIndexDot11PhyFHSSTable, Dot11RandomTableFlagGet, Dot11RandomTableFlagSet, Dot11RandomTableFlagTest, Dot11PhyFHSSTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11NumberofHoppingSets}, GetNextIndexDot11PhyFHSSTable, Dot11NumberofHoppingSetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11HopModulus}, GetNextIndexDot11PhyFHSSTable, Dot11HopModulusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11HopOffset}, GetNextIndexDot11PhyFHSSTable, Dot11HopOffsetGet, Dot11HopOffsetSet, Dot11HopOffsetTest, Dot11PhyFHSSTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11PhyFHSSTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11PhyFHSSTableEntry = { 16, Dot11PhyFHSSTableMibEntry };

tMbDbEntry Dot11PhyDSSSTableMibEntry[]= {

{{8,Dot11CurrentChannel}, GetNextIndexDot11PhyDSSSTable, Dot11CurrentChannelGet, Dot11CurrentChannelSet, Dot11CurrentChannelTest, Dot11PhyDSSSTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyDSSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CCAModeSupported}, GetNextIndexDot11PhyDSSSTable, Dot11CCAModeSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyDSSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CurrentCCAMode}, GetNextIndexDot11PhyDSSSTable, Dot11CurrentCCAModeGet, Dot11CurrentCCAModeSet, Dot11CurrentCCAModeTest, Dot11PhyDSSSTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyDSSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11EDThreshold}, GetNextIndexDot11PhyDSSSTable, Dot11EDThresholdGet, Dot11EDThresholdSet, Dot11EDThresholdTest, Dot11PhyDSSSTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11PhyDSSSTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11PhyDSSSTableEntry = { 4, Dot11PhyDSSSTableMibEntry };

tMbDbEntry Dot11PhyIRTableMibEntry[]= {

{{8,Dot11CCAWatchdogTimerMax}, GetNextIndexDot11PhyIRTable, Dot11CCAWatchdogTimerMaxGet, Dot11CCAWatchdogTimerMaxSet, Dot11CCAWatchdogTimerMaxTest, Dot11PhyIRTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11PhyIRTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CCAWatchdogCountMax}, GetNextIndexDot11PhyIRTable, Dot11CCAWatchdogCountMaxGet, Dot11CCAWatchdogCountMaxSet, Dot11CCAWatchdogCountMaxTest, Dot11PhyIRTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11PhyIRTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CCAWatchdogTimerMin}, GetNextIndexDot11PhyIRTable, Dot11CCAWatchdogTimerMinGet, Dot11CCAWatchdogTimerMinSet, Dot11CCAWatchdogTimerMinTest, Dot11PhyIRTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11PhyIRTableINDEX, 1, 0, 0, NULL},

{{8,Dot11CCAWatchdogCountMin}, GetNextIndexDot11PhyIRTable, Dot11CCAWatchdogCountMinGet, Dot11CCAWatchdogCountMinSet, Dot11CCAWatchdogCountMinTest, Dot11PhyIRTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11PhyIRTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11PhyIRTableEntry = { 4, Dot11PhyIRTableMibEntry };

tMbDbEntry Dot11RegDomainsSupportedTableMibEntry[]= {

{{8,Dot11RegDomainsSupportedIndex}, GetNextIndexDot11RegDomainsSupportedTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot11RegDomainsSupportedTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RegDomainsSupportedValue}, GetNextIndexDot11RegDomainsSupportedTable, Dot11RegDomainsSupportedValueGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11RegDomainsSupportedTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11RegDomainsSupportedTableEntry = { 2, Dot11RegDomainsSupportedTableMibEntry };

tMbDbEntry Dot11AntennasListTableMibEntry[]= {

{{8,Dot11AntennaListIndex}, GetNextIndexDot11AntennasListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot11AntennasListTableINDEX, 2, 0, 0, NULL},

{{8,Dot11SupportedTxAntenna}, GetNextIndexDot11AntennasListTable, Dot11SupportedTxAntennaGet, Dot11SupportedTxAntennaSet, Dot11SupportedTxAntennaTest, Dot11AntennasListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11AntennasListTableINDEX, 2, 0, 0, NULL},

{{8,Dot11SupportedRxAntenna}, GetNextIndexDot11AntennasListTable, Dot11SupportedRxAntennaGet, Dot11SupportedRxAntennaSet, Dot11SupportedRxAntennaTest, Dot11AntennasListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11AntennasListTableINDEX, 2, 0, 0, NULL},

{{8,Dot11DiversitySelectionRx}, GetNextIndexDot11AntennasListTable, Dot11DiversitySelectionRxGet, Dot11DiversitySelectionRxSet, Dot11DiversitySelectionRxTest, Dot11AntennasListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11AntennasListTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11AntennasListTableEntry = { 4, Dot11AntennasListTableMibEntry };

tMbDbEntry Dot11SupportedDataRatesTxTableMibEntry[]= {

{{8,Dot11SupportedDataRatesTxIndex}, GetNextIndexDot11SupportedDataRatesTxTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot11SupportedDataRatesTxTableINDEX, 2, 0, 0, NULL},

{{8,Dot11SupportedDataRatesTxValue}, GetNextIndexDot11SupportedDataRatesTxTable, Dot11SupportedDataRatesTxValueGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11SupportedDataRatesTxTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11SupportedDataRatesTxTableEntry = { 2, Dot11SupportedDataRatesTxTableMibEntry };

tMbDbEntry Dot11SupportedDataRatesRxTableMibEntry[]= {

{{8,Dot11SupportedDataRatesRxIndex}, GetNextIndexDot11SupportedDataRatesRxTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot11SupportedDataRatesRxTableINDEX, 2, 0, 0, NULL},

{{8,Dot11SupportedDataRatesRxValue}, GetNextIndexDot11SupportedDataRatesRxTable, Dot11SupportedDataRatesRxValueGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot11SupportedDataRatesRxTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11SupportedDataRatesRxTableEntry = { 2, Dot11SupportedDataRatesRxTableMibEntry };

tMbDbEntry Dot11PhyOFDMTableMibEntry[]= {

{{8,Dot11CurrentFrequency}, GetNextIndexDot11PhyOFDMTable, Dot11CurrentFrequencyGet, Dot11CurrentFrequencySet, Dot11CurrentFrequencyTest, Dot11PhyOFDMTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyOFDMTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TIThreshold}, GetNextIndexDot11PhyOFDMTable, Dot11TIThresholdGet, Dot11TIThresholdSet, Dot11TIThresholdTest, Dot11PhyOFDMTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11PhyOFDMTableINDEX, 1, 1, 0, NULL},

{{8,Dot11FrequencyBandsSupported}, GetNextIndexDot11PhyOFDMTable, Dot11FrequencyBandsSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyOFDMTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ChannelStartingFactor}, GetNextIndexDot11PhyOFDMTable, Dot11ChannelStartingFactorGet, Dot11ChannelStartingFactorSet, Dot11ChannelStartingFactorTest, Dot11PhyOFDMTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11PhyOFDMTableINDEX, 1, 0, 0, NULL},

{{8,Dot11FiveMHzOperationImplemented}, GetNextIndexDot11PhyOFDMTable, Dot11FiveMHzOperationImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyOFDMTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TenMHzOperationImplemented}, GetNextIndexDot11PhyOFDMTable, Dot11TenMHzOperationImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyOFDMTableINDEX, 1, 0, 0, NULL},

{{8,Dot11TwentyMHzOperationImplemented}, GetNextIndexDot11PhyOFDMTable, Dot11TwentyMHzOperationImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyOFDMTableINDEX, 1, 0, 0, NULL},

{{8,Dot11PhyOFDMChannelWidth}, GetNextIndexDot11PhyOFDMTable, Dot11PhyOFDMChannelWidthGet, Dot11PhyOFDMChannelWidthSet, Dot11PhyOFDMChannelWidthTest, Dot11PhyOFDMTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyOFDMTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11PhyOFDMTableEntry = { 8, Dot11PhyOFDMTableMibEntry };

tMbDbEntry Dot11PhyHRDSSSTableMibEntry[]= {

{{8,Dot11ShortPreambleOptionImplemented}, GetNextIndexDot11PhyHRDSSSTable, Dot11ShortPreambleOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyHRDSSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11PBCCOptionImplemented}, GetNextIndexDot11PhyHRDSSSTable, Dot11PBCCOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyHRDSSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ChannelAgilityPresent}, GetNextIndexDot11PhyHRDSSSTable, Dot11ChannelAgilityPresentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyHRDSSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ChannelAgilityEnabled}, GetNextIndexDot11PhyHRDSSSTable, Dot11ChannelAgilityEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyHRDSSSTableINDEX, 1, 0, 0, NULL},

{{8,Dot11HRCCAModeSupported}, GetNextIndexDot11PhyHRDSSSTable, Dot11HRCCAModeSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyHRDSSSTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11PhyHRDSSSTableEntry = { 5, Dot11PhyHRDSSSTableMibEntry };

tMbDbEntry Dot11HoppingPatternTableMibEntry[]= {

{{8,Dot11HoppingPatternIndex}, GetNextIndexDot11HoppingPatternTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot11HoppingPatternTableINDEX, 2, 0, 0, NULL},

{{8,Dot11RandomTableFieldNumber}, GetNextIndexDot11HoppingPatternTable, Dot11RandomTableFieldNumberGet, Dot11RandomTableFieldNumberSet, Dot11RandomTableFieldNumberTest, Dot11HoppingPatternTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot11HoppingPatternTableINDEX, 2, 0, 0, NULL},
};
tMibData Dot11HoppingPatternTableEntry = { 2, Dot11HoppingPatternTableMibEntry };

tMbDbEntry Dot11PhyERPTableMibEntry[]= {

{{8,Dot11ERPPBCCOptionImplemented}, GetNextIndexDot11PhyERPTable, Dot11ERPPBCCOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyERPTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ERPBCCOptionEnabled}, GetNextIndexDot11PhyERPTable, Dot11ERPBCCOptionEnabledGet, Dot11ERPBCCOptionEnabledSet, Dot11ERPBCCOptionEnabledTest, Dot11PhyERPTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyERPTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DSSSOFDMOptionImplemented}, GetNextIndexDot11PhyERPTable, Dot11DSSSOFDMOptionImplementedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot11PhyERPTableINDEX, 1, 0, 0, NULL},

{{8,Dot11DSSSOFDMOptionEnabled}, GetNextIndexDot11PhyERPTable, Dot11DSSSOFDMOptionEnabledGet, Dot11DSSSOFDMOptionEnabledSet, Dot11DSSSOFDMOptionEnabledTest, Dot11PhyERPTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyERPTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ShortSlotTimeOptionImplemented}, GetNextIndexDot11PhyERPTable, Dot11ShortSlotTimeOptionImplementedGet, Dot11ShortSlotTimeOptionImplementedSet, Dot11ShortSlotTimeOptionImplementedTest, Dot11PhyERPTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyERPTableINDEX, 1, 0, 0, NULL},

{{8,Dot11ShortSlotTimeOptionEnabled}, GetNextIndexDot11PhyERPTable, Dot11ShortSlotTimeOptionEnabledGet, Dot11ShortSlotTimeOptionEnabledSet, Dot11ShortSlotTimeOptionEnabledTest, Dot11PhyERPTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot11PhyERPTableINDEX, 1, 0, 0, NULL},
};
tMibData Dot11PhyERPTableEntry = { 6, Dot11PhyERPTableMibEntry };

#endif /* _STDDOTDB_H */

