/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std802lw.h,v 1.1.1.1 2016/09/20 10:42:39 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for Dot11StationConfigTable. */
INT1
nmhValidateIndexInstanceDot11StationConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11StationConfigTable  */

INT1
nmhGetFirstIndexDot11StationConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11StationConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11StationID ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetDot11MediumOccupancyLimit ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11CFPollable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11CFPPeriod ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11CFPMaxDuration ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11AuthenticationResponseTimeOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11PrivacyOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11PowerManagementMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11DesiredSSID ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11DesiredBSSType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11OperationalRateSet ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11BeaconPeriod ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11DTIMPeriod ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11AssociationResponseTimeOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11DisassociateReason ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11DisassociateStation ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetDot11DeauthenticateReason ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11DeauthenticateStation ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetDot11AuthenticateFailStatus ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11AuthenticateFailStation ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetDot11MultiDomainCapabilityImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MultiDomainCapabilityActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11CountryString ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11SpectrumManagementImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11SpectrumManagementRequired ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RSNAOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RSNAPreauthenticationImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11OperatingClassesImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11OperatingClassesRequired ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11QosOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ImmediateBlockAckOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11DelayedBlockAckOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11DirectOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11APSDOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11QAckOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11QBSSLoadImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11QueueRequestOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TXOPRequestOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MoreDataAckOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11AssociateInNQBSS ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11DLSAllowedInQBSS ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11DLSAllowed ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11AssociateStation ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetDot11AssociateID ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11AssociateFailStation ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetDot11AssociateFailStatus ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11ReassociateStation ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetDot11ReassociateID ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11ReassociateFailStation ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetDot11ReassociateFailStatus ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RadioMeasurementImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RadioMeasurementActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMMeasurementProbeDelay ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RMMeasurementPilotPeriod ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RMLinkMeasurementActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMParallelMeasurementsActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMRepeatedMeasurementsActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMBeaconPassiveMeasurementActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMBeaconActiveMeasurementActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMBeaconTableMeasurementActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMBeaconMeasurementReportingConditionsActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMFrameMeasurementActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMChannelLoadMeasurementActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMNoiseHistogramMeasurementActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMStatisticsMeasurementActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMLCIMeasurementActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMLCIAzimuthActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMTransmitStreamCategoryMeasurementActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMTriggeredTransmitStreamCategoryMeasurementActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMAPChannelReportActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMMIBActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMMaxMeasurementDuration ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RMNonOperatingChannelMaxMeasurementDuration ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RMMeasurementPilotTransmissionInformationActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMMeasurementPilotActivated ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportTSFOffsetActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMRCPIMeasurementActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMRSNIMeasurementActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMBSSAverageAccessDelayActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMBSSAvailableAdmissionCapacityActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMAntennaInformationActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11FastBSSTransitionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11LCIDSEImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11LCIDSERequired ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11DSERequired ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ExtendedChannelSwitchActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RSNAProtectedManagementFramesActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RSNAUnprotectedManagementFramesAllowed ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11AssociationSAQueryMaximumTimeout ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11AssociationSAQueryRetryTimeout ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11HighThroughputOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RSNAPBACRequired ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11PSMPOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TunneledDirectLinkSetupImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TDLSPeerUAPSDBufferSTAActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TDLSPeerPSMActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TDLSPeerUAPSDIndicationWindow ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TDLSChannelSwitchingActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TDLSPeerSTAMissingAckRetryLimit ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TDLSResponseTimeout ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11OCBActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TDLSProbeDelay ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TDLSDiscoveryRequestWindow ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TDLSACDeterminationInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11WirelessManagementImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11BssMaxIdlePeriod ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11BssMaxIdlePeriodOptions ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11TIMBroadcastInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TIMBroadcastOffset ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TIMBroadcastHighRateTIMRate ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TIMBroadcastLowRateTIMRate ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11StatsMinTriggerTimeout ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RMCivicMeasurementActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RMIdentifierMeasurementActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TimeAdvertisementDTIMInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11InterworkingServiceImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11InterworkingServiceActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11QosMapImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11QosMapActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11EBRImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11EBRActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ESNetwork ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11SSPNInterfaceImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11SSPNInterfaceActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11HESSID ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetDot11EASImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11EASActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MSGCFImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MSGCFActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TimeAdvertisementTimeError ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11TimeAdvertisementTimeValue ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RM3rdPartyMeasurementActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RejectUnadmittedTraffic ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11BSSBroadcastNullCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshActivated ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11StationID ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetDot11MediumOccupancyLimit ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11AuthenticationResponseTimeOut ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11DesiredSSID ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11DesiredBSSType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11BeaconPeriod ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11DTIMPeriod ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11AssociationResponseTimeOut ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MultiDomainCapabilityActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11SpectrumManagementRequired ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11OperatingClassesRequired ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11AssociateInNQBSS ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11DLSAllowedInQBSS ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11DLSAllowed ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RadioMeasurementActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMMeasurementProbeDelay ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11RMMeasurementPilotPeriod ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11RMLinkMeasurementActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMParallelMeasurementsActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMRepeatedMeasurementsActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMBeaconPassiveMeasurementActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMBeaconActiveMeasurementActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMBeaconTableMeasurementActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMBeaconMeasurementReportingConditionsActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMFrameMeasurementActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMChannelLoadMeasurementActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMNoiseHistogramMeasurementActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMStatisticsMeasurementActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMLCIMeasurementActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMLCIAzimuthActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMTransmitStreamCategoryMeasurementActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMTriggeredTransmitStreamCategoryMeasurementActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMAPChannelReportActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMMIBActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMMaxMeasurementDuration ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11RMNonOperatingChannelMaxMeasurementDuration ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11RMMeasurementPilotTransmissionInformationActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMMeasurementPilotActivated ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportTSFOffsetActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMRCPIMeasurementActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMRSNIMeasurementActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMBSSAverageAccessDelayActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMBSSAvailableAdmissionCapacityActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMAntennaInformationActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RSNAProtectedManagementFramesActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RSNAUnprotectedManagementFramesAllowed ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11AssociationSAQueryMaximumTimeout ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11AssociationSAQueryRetryTimeout ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11RSNAPBACRequired ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11TDLSPeerUAPSDIndicationWindow ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11TDLSPeerSTAMissingAckRetryLimit ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11TDLSResponseTimeout ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11OCBActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11TDLSProbeDelay ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11TDLSDiscoveryRequestWindow ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11TDLSACDeterminationInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11BssMaxIdlePeriod ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11BssMaxIdlePeriodOptions ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11TIMBroadcastInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11TIMBroadcastOffset ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11TIMBroadcastHighRateTIMRate ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11TIMBroadcastLowRateTIMRate ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11StatsMinTriggerTimeout ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11RMCivicMeasurementActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RMIdentifierMeasurementActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11TimeAdvertisementDTIMInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11InterworkingServiceActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11QosMapActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11EBRActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11ESNetwork ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11SSPNInterfaceActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11HESSID ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetDot11EASActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MSGCFActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11TimeAdvertisementTimeError ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11TimeAdvertisementTimeValue ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11RM3rdPartyMeasurementActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RejectUnadmittedTraffic ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11BSSBroadcastNullCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshActivated ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11StationID ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2Dot11MediumOccupancyLimit ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11AuthenticationResponseTimeOut ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11DesiredSSID ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11DesiredBSSType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11BeaconPeriod ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11DTIMPeriod ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11AssociationResponseTimeOut ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MultiDomainCapabilityActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11SpectrumManagementRequired ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11OperatingClassesRequired ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11AssociateInNQBSS ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11DLSAllowedInQBSS ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11DLSAllowed ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RadioMeasurementActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMMeasurementProbeDelay ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMMeasurementPilotPeriod ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMLinkMeasurementActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMParallelMeasurementsActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRepeatedMeasurementsActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMBeaconPassiveMeasurementActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMBeaconActiveMeasurementActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMBeaconTableMeasurementActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMBeaconMeasurementReportingConditionsActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMFrameMeasurementActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMChannelLoadMeasurementActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNoiseHistogramMeasurementActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMStatisticsMeasurementActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMLCIMeasurementActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMLCIAzimuthActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMTransmitStreamCategoryMeasurementActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMTriggeredTransmitStreamCategoryMeasurementActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMAPChannelReportActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMMIBActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMMaxMeasurementDuration ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNonOperatingChannelMaxMeasurementDuration ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMMeasurementPilotTransmissionInformationActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMMeasurementPilotActivated ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportTSFOffsetActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRCPIMeasurementActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRSNIMeasurementActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMBSSAverageAccessDelayActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMBSSAvailableAdmissionCapacityActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMAntennaInformationActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RSNAProtectedManagementFramesActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RSNAUnprotectedManagementFramesAllowed ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11AssociationSAQueryMaximumTimeout ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11AssociationSAQueryRetryTimeout ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11RSNAPBACRequired ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11TDLSPeerUAPSDIndicationWindow ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11TDLSPeerSTAMissingAckRetryLimit ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11TDLSResponseTimeout ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11OCBActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11TDLSProbeDelay ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11TDLSDiscoveryRequestWindow ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11TDLSACDeterminationInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11BssMaxIdlePeriod ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11BssMaxIdlePeriodOptions ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11TIMBroadcastInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11TIMBroadcastOffset ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11TIMBroadcastHighRateTIMRate ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11TIMBroadcastLowRateTIMRate ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11StatsMinTriggerTimeout ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMCivicMeasurementActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RMIdentifierMeasurementActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11TimeAdvertisementDTIMInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11InterworkingServiceActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11QosMapActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11EBRActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11ESNetwork ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11SSPNInterfaceActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11HESSID ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2Dot11EASActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MSGCFActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11TimeAdvertisementTimeError ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11TimeAdvertisementTimeValue ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11RM3rdPartyMeasurementActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RejectUnadmittedTraffic ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11BSSBroadcastNullCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11StationConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11AuthenticationAlgorithmsTable. */
INT1
nmhValidateIndexInstanceDot11AuthenticationAlgorithmsTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11AuthenticationAlgorithmsTable  */

INT1
nmhGetFirstIndexDot11AuthenticationAlgorithmsTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11AuthenticationAlgorithmsTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11AuthenticationAlgorithm ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetDot11AuthenticationAlgorithmsActivated ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11AuthenticationAlgorithmsActivated ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11AuthenticationAlgorithmsActivated ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11AuthenticationAlgorithmsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11WEPDefaultKeysTable. */
INT1
nmhValidateIndexInstanceDot11WEPDefaultKeysTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11WEPDefaultKeysTable  */

INT1
nmhGetFirstIndexDot11WEPDefaultKeysTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11WEPDefaultKeysTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11WEPDefaultKeyValue ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11WEPDefaultKeyValue ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11WEPDefaultKeyValue ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11WEPDefaultKeysTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11WEPKeyMappingsTable. */
INT1
nmhValidateIndexInstanceDot11WEPKeyMappingsTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11WEPKeyMappingsTable  */

INT1
nmhGetFirstIndexDot11WEPKeyMappingsTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11WEPKeyMappingsTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11WEPKeyMappingAddress ARG_LIST((INT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetDot11WEPKeyMappingWEPOn ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetDot11WEPKeyMappingValue ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WEPKeyMappingStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11WEPKeyMappingAddress ARG_LIST((INT4  , UINT4  ,tMacAddr ));

INT1
nmhSetDot11WEPKeyMappingWEPOn ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetDot11WEPKeyMappingValue ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11WEPKeyMappingStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11WEPKeyMappingAddress ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2Dot11WEPKeyMappingWEPOn ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot11WEPKeyMappingValue ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11WEPKeyMappingStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11WEPKeyMappingsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11PrivacyTable. */
INT1
nmhValidateIndexInstanceDot11PrivacyTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11PrivacyTable  */

INT1
nmhGetFirstIndexDot11PrivacyTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11PrivacyTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11PrivacyInvoked ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11WEPDefaultKeyID ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11WEPKeyMappingLengthImplemented ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11ExcludeUnencrypted ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11WEPICVErrorCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11WEPExcludedCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNAActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RSNAPreauthenticationActivated ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11PrivacyInvoked ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11WEPDefaultKeyID ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11ExcludeUnencrypted ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RSNAActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RSNAPreauthenticationActivated ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11PrivacyInvoked ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11WEPDefaultKeyID ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11ExcludeUnencrypted ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RSNAActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RSNAPreauthenticationActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11PrivacyTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11MultiDomainCapabilityTable. */
INT1
nmhValidateIndexInstanceDot11MultiDomainCapabilityTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11MultiDomainCapabilityTable  */

INT1
nmhGetFirstIndexDot11MultiDomainCapabilityTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11MultiDomainCapabilityTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11FirstChannelNumber ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11NumberofChannels ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11MaximumTransmitPowerLevel ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11FirstChannelNumber ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetDot11NumberofChannels ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetDot11MaximumTransmitPowerLevel ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11FirstChannelNumber ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11NumberofChannels ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11MaximumTransmitPowerLevel ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11MultiDomainCapabilityTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11SpectrumManagementTable. */
INT1
nmhValidateIndexInstanceDot11SpectrumManagementTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11SpectrumManagementTable  */

INT1
nmhGetFirstIndexDot11SpectrumManagementTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11SpectrumManagementTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11MitigationRequirement ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetDot11ChannelSwitchTime ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11PowerCapabilityMaxImplemented ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetDot11PowerCapabilityMinImplemented ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11MitigationRequirement ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetDot11ChannelSwitchTime ARG_LIST((INT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11MitigationRequirement ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Dot11ChannelSwitchTime ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11SpectrumManagementTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11RSNAConfigTable. */
INT1
nmhValidateIndexInstanceDot11RSNAConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11RSNAConfigTable  */

INT1
nmhGetFirstIndexDot11RSNAConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11RSNAConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11RSNAConfigVersion ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNAConfigPairwiseKeysImplemented ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNAConfigGroupCipher ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RSNAConfigGroupRekeyMethod ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RSNAConfigGroupRekeyTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNAConfigGroupRekeyPackets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNAConfigGroupRekeyStrict ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RSNAConfigPSKValue ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RSNAConfigPSKPassPhrase ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RSNAConfigGroupUpdateCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNAConfigPairwiseUpdateCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNAConfigGroupCipherSize ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNAConfigPMKLifetime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNAConfigPMKReauthThreshold ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNAConfigNumberOfPTKSAReplayCountersImplemented ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNAConfigSATimeout ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNAAuthenticationSuiteSelected ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RSNAPairwiseCipherSelected ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RSNAGroupCipherSelected ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RSNAPMKIDUsed ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RSNAAuthenticationSuiteRequested ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RSNAPairwiseCipherRequested ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RSNAGroupCipherRequested ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RSNATKIPCounterMeasuresInvoked ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNA4WayHandshakeFailures ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNAConfigNumberOfGTKSAReplayCountersImplemented ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNAConfigSTKKeysImplemented ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNAConfigSTKCipher ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RSNAConfigSTKRekeyTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNAConfigSMKUpdateCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNAConfigSTKCipherSize ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNAConfigSMKLifetime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNAConfigSMKReauthThreshold ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNAConfigNumberOfSTKSAReplayCountersImplemented ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNAPairwiseSTKSelected ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RSNASMKHandshakeFailures ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNASAERetransPeriod ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNASAEAntiCloggingThreshold ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RSNASAESync ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11RSNAConfigGroupCipher ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11RSNAConfigGroupRekeyMethod ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RSNAConfigGroupRekeyTime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11RSNAConfigGroupRekeyPackets ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11RSNAConfigGroupRekeyStrict ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RSNAConfigPSKValue ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11RSNAConfigPSKPassPhrase ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11RSNAConfigGroupUpdateCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11RSNAConfigPairwiseUpdateCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11RSNAConfigGroupCipherSize ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11RSNAConfigPMKLifetime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11RSNAConfigPMKReauthThreshold ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11RSNAConfigSATimeout ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11RSNAConfigSTKCipher ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11RSNAConfigSTKRekeyTime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11RSNAConfigSMKUpdateCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11RSNAConfigSTKCipherSize ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11RSNAConfigSMKLifetime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11RSNAConfigSMKReauthThreshold ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11RSNAConfigGroupCipher ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11RSNAConfigGroupRekeyMethod ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RSNAConfigGroupRekeyTime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11RSNAConfigGroupRekeyPackets ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11RSNAConfigGroupRekeyStrict ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RSNAConfigPSKValue ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11RSNAConfigPSKPassPhrase ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11RSNAConfigGroupUpdateCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11RSNAConfigPairwiseUpdateCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11RSNAConfigGroupCipherSize ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11RSNAConfigPMKLifetime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11RSNAConfigPMKReauthThreshold ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11RSNAConfigSATimeout ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11RSNAConfigSTKCipher ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11RSNAConfigSTKRekeyTime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11RSNAConfigSMKUpdateCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11RSNAConfigSTKCipherSize ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11RSNAConfigSMKLifetime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11RSNAConfigSMKReauthThreshold ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11RSNAConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11RSNAConfigPairwiseCiphersTable. */
INT1
nmhValidateIndexInstanceDot11RSNAConfigPairwiseCiphersTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11RSNAConfigPairwiseCiphersTable  */

INT1
nmhGetFirstIndexDot11RSNAConfigPairwiseCiphersTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11RSNAConfigPairwiseCiphersTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11RSNAConfigPairwiseCipherImplemented ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RSNAConfigPairwiseCipherActivated ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetDot11RSNAConfigPairwiseCipherSizeImplemented ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11RSNAConfigPairwiseCipherActivated ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11RSNAConfigPairwiseCipherActivated ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11RSNAConfigPairwiseCiphersTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11RSNAConfigAuthenticationSuitesTable. */
INT1
nmhValidateIndexInstanceDot11RSNAConfigAuthenticationSuitesTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11RSNAConfigAuthenticationSuitesTable  */

INT1
nmhGetFirstIndexDot11RSNAConfigAuthenticationSuitesTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11RSNAConfigAuthenticationSuitesTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11RSNAConfigAuthenticationSuiteImplemented ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RSNAConfigAuthenticationSuiteActivated ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11RSNAConfigAuthenticationSuiteActivated ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11RSNAConfigAuthenticationSuiteActivated ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11RSNAConfigAuthenticationSuitesTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11RSNAStatsTable. */
INT1
nmhValidateIndexInstanceDot11RSNAStatsTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11RSNAStatsTable  */

INT1
nmhGetFirstIndexDot11RSNAStatsTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11RSNAStatsTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11RSNAStatsSTAAddress ARG_LIST((INT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetDot11RSNAStatsVersion ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11RSNAStatsSelectedPairwiseCipher ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RSNAStatsTKIPICVErrors ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11RSNAStatsTKIPLocalMICFailures ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11RSNAStatsTKIPRemoteMICFailures ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11RSNAStatsCCMPReplays ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11RSNAStatsCCMPDecryptErrors ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11RSNAStatsTKIPReplays ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11RSNAStatsCMACICVErrors ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11RSNAStatsCMACReplays ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11RSNAStatsRobustMgmtCCMPReplays ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11RSNABIPMICErrors ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for Dot11OperatingClassesTable. */
INT1
nmhValidateIndexInstanceDot11OperatingClassesTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11OperatingClassesTable  */

INT1
nmhGetFirstIndexDot11OperatingClassesTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11OperatingClassesTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11OperatingClass ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11CoverageClass ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11OperatingClass ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetDot11CoverageClass ARG_LIST((INT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11OperatingClass ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11CoverageClass ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11OperatingClassesTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11FastBSSTransitionConfigTable. */
INT1
nmhValidateIndexInstanceDot11FastBSSTransitionConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11FastBSSTransitionConfigTable  */

INT1
nmhGetFirstIndexDot11FastBSSTransitionConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11FastBSSTransitionConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11FastBSSTransitionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11FTMobilityDomainID ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11FTOverDSActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11FTResourceRequestSupported ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11FTR0KeyHolderID ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11FTR0KeyLifetime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11FTR1KeyHolderID ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11FTReassociationDeadline ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11FastBSSTransitionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11FTMobilityDomainID ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11FTOverDSActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11FTResourceRequestSupported ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11FTR0KeyHolderID ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11FTR0KeyLifetime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11FTR1KeyHolderID ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11FTReassociationDeadline ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11FastBSSTransitionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11FTMobilityDomainID ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11FTOverDSActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11FTResourceRequestSupported ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11FTR0KeyHolderID ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11FTR0KeyLifetime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11FTR1KeyHolderID ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11FTReassociationDeadline ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11FastBSSTransitionConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11LCIDSETable. */
INT1
nmhValidateIndexInstanceDot11LCIDSETable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11LCIDSETable  */

INT1
nmhGetFirstIndexDot11LCIDSETable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11LCIDSETable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11LCIDSEIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11LCIDSECurrentOperatingClass ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11LCIDSELatitudeResolution ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11LCIDSELatitudeInteger ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11LCIDSELatitudeFraction ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11LCIDSELongitudeResolution ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11LCIDSELongitudeInteger ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11LCIDSELongitudeFraction ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11LCIDSEAltitudeType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11LCIDSEAltitudeResolution ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11LCIDSEAltitudeInteger ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11LCIDSEAltitudeFraction ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11LCIDSEDatum ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RegLocAgreement ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RegLocDSE ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11DependentSTA ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11DependentEnablementIdentifier ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11DSEEnablementTimeLimit ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11DSEEnablementFailHoldTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11DSERenewalTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11DSETransmitDivisor ARG_LIST((UINT4 ,UINT4 *));

/* Proto Validate Index Instance for Dot11HTStationConfigTable. */
INT1
nmhValidateIndexInstanceDot11HTStationConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11HTStationConfigTable  */

INT1
nmhGetFirstIndexDot11HTStationConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11HTStationConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11HTOperationalMCSSet ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11MIMOPowerSave ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11NDelayedBlockAckOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MaxAMSDULength ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11STBCControlFrameOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11LsigTxopProtectionOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MaxRxAMPDUFactor ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MinimumMPDUStartSpacing ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11PCOOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TransitionTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MCSFeedbackOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11HTControlFieldSupported ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RDResponderOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11SPPAMSDUCapable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11SPPAMSDURequired ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11FortyMHzOptionImplemented ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11HTOperationalMCSSet ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11SPPAMSDURequired ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11HTOperationalMCSSet ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11SPPAMSDURequired ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11HTStationConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11WirelessMgmtOptionsTable. */
INT1
nmhValidateIndexInstanceDot11WirelessMgmtOptionsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11WirelessMgmtOptionsTable  */

INT1
nmhGetFirstIndexDot11WirelessMgmtOptionsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11WirelessMgmtOptionsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11MgmtOptionLocationActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionFMSImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionFMSActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionEventsActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionDiagnosticsActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionMultiBSSIDImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionMultiBSSIDActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionTFSImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionTFSActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionWNMSleepModeImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionWNMSleepModeActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionTIMBroadcastImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionTIMBroadcastActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionProxyARPImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionProxyARPActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionBSSTransitionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionBSSTransitionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionQoSTrafficCapabilityImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionQoSTrafficCapabilityActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionACStationCountImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionACStationCountActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionCoLocIntfReportingImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionCoLocIntfReportingActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionMotionDetectionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionMotionDetectionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionTODImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionTODActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionTimingMsmtImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionTimingMsmtActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionChannelUsageImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionChannelUsageActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionTriggerSTAStatisticsActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionSSIDListImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionSSIDListActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionMulticastDiagnosticsActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionLocationTrackingImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionLocationTrackingActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionDMSImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionDMSActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionUAPSDCoexistenceImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionUAPSDCoexistenceActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionWNMNotificationImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionWNMNotificationActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionUTCTSFOffsetImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MgmtOptionUTCTSFOffsetActivated ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11MgmtOptionLocationActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionFMSActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionEventsActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionDiagnosticsActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionMultiBSSIDActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionTFSActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionWNMSleepModeActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionTIMBroadcastActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionProxyARPActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionBSSTransitionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionQoSTrafficCapabilityActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionACStationCountActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionCoLocIntfReportingActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionMotionDetectionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionTODActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionTimingMsmtActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionChannelUsageActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionTriggerSTAStatisticsActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionSSIDListActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionMulticastDiagnosticsActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionLocationTrackingActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionDMSActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionUAPSDCoexistenceActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionWNMNotificationActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MgmtOptionUTCTSFOffsetActivated ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11MgmtOptionLocationActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionFMSActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionEventsActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionDiagnosticsActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionMultiBSSIDActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionTFSActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionWNMSleepModeActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionTIMBroadcastActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionProxyARPActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionBSSTransitionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionQoSTrafficCapabilityActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionACStationCountActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionCoLocIntfReportingActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionMotionDetectionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionTODActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionTimingMsmtActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionChannelUsageActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionTriggerSTAStatisticsActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionSSIDListActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionMulticastDiagnosticsActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionLocationTrackingActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionDMSActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionUAPSDCoexistenceActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionWNMNotificationActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MgmtOptionUTCTSFOffsetActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11WirelessMgmtOptionsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11LocationServicesNextIndex ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for Dot11LocationServicesTable. */
INT1
nmhValidateIndexInstanceDot11LocationServicesTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11LocationServicesTable  */

INT1
nmhGetFirstIndexDot11LocationServicesTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11LocationServicesTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11LocationServicesMACAddress ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11LocationServicesLIPIndicationMulticastAddress ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11LocationServicesLIPReportIntervalUnits ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11LocationServicesLIPNormalReportInterval ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11LocationServicesLIPNormalFramesperChannel ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11LocationServicesLIPInMotionReportInterval ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11LocationServicesLIPInMotionFramesperChannel ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11LocationServicesLIPBurstInterframeInterval ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11LocationServicesLIPTrackingDuration ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11LocationServicesLIPEssDetectionInterval ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11LocationServicesLocationIndicationChannelList ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11LocationServicesLocationIndicationBroadcastDataRate ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11LocationServicesLocationIndicationOptionsUsed ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11LocationServicesLocationIndicationIndicationParameters ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11LocationServicesLocationStatus ARG_LIST((UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11LocationServicesMACAddress ARG_LIST((UINT4  ,tMacAddr ));

INT1
nmhSetDot11LocationServicesLIPIndicationMulticastAddress ARG_LIST((UINT4  ,tMacAddr ));

INT1
nmhSetDot11LocationServicesLIPReportIntervalUnits ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11LocationServicesLIPNormalReportInterval ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11LocationServicesLIPNormalFramesperChannel ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11LocationServicesLIPInMotionReportInterval ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11LocationServicesLIPInMotionFramesperChannel ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11LocationServicesLIPBurstInterframeInterval ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11LocationServicesLIPTrackingDuration ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11LocationServicesLIPEssDetectionInterval ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11LocationServicesLocationIndicationChannelList ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11LocationServicesLocationIndicationBroadcastDataRate ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11LocationServicesLocationIndicationOptionsUsed ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11LocationServicesLocationIndicationIndicationParameters ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11LocationServicesMACAddress ARG_LIST((UINT4 *  ,UINT4  ,tMacAddr ));

INT1
nmhTestv2Dot11LocationServicesLIPIndicationMulticastAddress ARG_LIST((UINT4 *  ,UINT4  ,tMacAddr ));

INT1
nmhTestv2Dot11LocationServicesLIPReportIntervalUnits ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11LocationServicesLIPNormalReportInterval ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11LocationServicesLIPNormalFramesperChannel ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11LocationServicesLIPInMotionReportInterval ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11LocationServicesLIPInMotionFramesperChannel ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11LocationServicesLIPBurstInterframeInterval ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11LocationServicesLIPTrackingDuration ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11LocationServicesLIPEssDetectionInterval ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11LocationServicesLocationIndicationChannelList ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11LocationServicesLocationIndicationBroadcastDataRate ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11LocationServicesLocationIndicationOptionsUsed ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11LocationServicesLocationIndicationIndicationParameters ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11LocationServicesTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11WirelessMGTEventTable. */
INT1
nmhValidateIndexInstanceDot11WirelessMGTEventTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11WirelessMGTEventTable  */

INT1
nmhGetFirstIndexDot11WirelessMGTEventTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11WirelessMGTEventTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11WirelessMGTEventMACAddress ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11WirelessMGTEventType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WirelessMGTEventStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WirelessMGTEventTSF ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WirelessMGTEventUTCOffset ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WirelessMGTEventTimeError ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WirelessMGTEventTransitionSourceBSSID ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11WirelessMGTEventTransitionTargetBSSID ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11WirelessMGTEventTransitionTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WirelessMGTEventTransitionReason ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WirelessMGTEventTransitionResult ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WirelessMGTEventTransitionSourceRCPI ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WirelessMGTEventTransitionSourceRSNI ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WirelessMGTEventTransitionTargetRCPI ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WirelessMGTEventTransitionTargetRSNI ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WirelessMGTEventRSNATargetBSSID ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11WirelessMGTEventRSNAAuthenticationType ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WirelessMGTEventRSNAEAPMethod ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WirelessMGTEventRSNAResult ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WirelessMGTEventRSNARSNElement ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WirelessMGTEventPeerSTAAddress ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11WirelessMGTEventPeerOperatingClass ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WirelessMGTEventPeerChannelNumber ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WirelessMGTEventPeerSTATxPower ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WirelessMGTEventPeerConnectionTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WirelessMGTEventPeerPeerStatus ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WirelessMGTEventWNMLog ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11RMRequestNextIndex ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for Dot11RMRequestTable. */
INT1
nmhValidateIndexInstanceDot11RMRequestTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11RMRequestTable  */

INT1
nmhGetFirstIndexDot11RMRequestTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11RMRequestTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11RMRqstRowStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMRqstToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RMRqstRepetitions ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMRqstType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMRqstTargetAdd ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11RMRqstTimeStamp ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstChanNumber ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstOperatingClass ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstRndInterval ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstDuration ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstParallel ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMRqstEnable ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMRqstRequest ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMRqstReport ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMRqstDurationMandatory ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMRqstBeaconRqstMode ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMRqstBeaconRqstDetail ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMRqstFrameRqstType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMRqstBssid ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11RMRqstSSID ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RMRqstBeaconReportingCondition ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMRqstBeaconThresholdOffset ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMRqstSTAStatRqstGroupID ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMRqstLCIRqstSubject ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMRqstLCILatitudeResolution ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstLCILongitudeResolution ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstLCIAltitudeResolution ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstLCIAzimuthType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMRqstLCIAzimuthResolution ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstPauseTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstTransmitStreamPeerQSTAAddress ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11RMRqstTransmitStreamTrafficIdentifier ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstTransmitStreamBin0Range ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstTrigdQoSAverageCondition ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMRqstTrigdQoSConsecutiveCondition ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMRqstTrigdQoSDelayCondition ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMRqstTrigdQoSAverageThreshold ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstTrigdQoSConsecutiveThreshold ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstTrigdQoSDelayThresholdRange ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstTrigdQoSDelayThreshold ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstTrigdQoSMeasurementCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstTrigdQoSTimeout ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstChannelLoadReportingCondition ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMRqstChannelLoadReference ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstNoiseHistogramReportingCondition ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMRqstAnpiReference ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstAPChannelReport ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RMRqstSTAStatPeerSTAAddress ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11RMRqstFrameTransmitterAddress ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11RMRqstVendorSpecific ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RMRqstSTAStatTrigMeasCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstSTAStatTrigTimeout ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstSTAStatTrigCondition ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RMRqstSTAStatTrigSTAFailedCntThresh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstSTAStatTrigSTAFCSErrCntThresh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstSTAStatTrigSTAMultRetryCntThresh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstSTAStatTrigSTAFrameDupeCntThresh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstSTAStatTrigSTARTSFailCntThresh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstSTAStatTrigSTAAckFailCntThresh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstSTAStatTrigSTARetryCntThresh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstSTAStatTrigQoSTrigCondition ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RMRqstSTAStatTrigQoSFailedCntThresh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstSTAStatTrigQoSRetryCntThresh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstSTAStatTrigQoSMultRetryCntThresh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstSTAStatTrigQoSFrameDupeCntThresh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstSTAStatTrigQoSRTSFailCntThresh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstSTAStatTrigQoSAckFailCntThresh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstSTAStatTrigQoSDiscardCntThresh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstSTAStatTrigRsnaTrigCondition ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RMRqstSTAStatTrigRsnaCMACICVErrCntThresh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstSTAStatTrigRsnaCMACReplayCntThresh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstSTAStatTrigRsnaRobustCCMPReplayCntThresh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstSTAStatTrigRsnaTKIPICVErrCntThresh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstSTAStatTrigRsnaTKIPReplayCntThresh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstSTAStatTrigRsnaCCMPDecryptErrCntThresh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMRqstSTAStatTrigRsnaCCMPReplayCntThresh ARG_LIST((UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11RMRqstRowStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMRqstToken ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11RMRqstRepetitions ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstIfIndex ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMRqstType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMRqstTargetAdd ARG_LIST((UINT4  ,tMacAddr ));

INT1
nmhSetDot11RMRqstTimeStamp ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstChanNumber ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstOperatingClass ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstRndInterval ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstDuration ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstParallel ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMRqstEnable ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMRqstRequest ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMRqstReport ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMRqstDurationMandatory ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMRqstBeaconRqstMode ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMRqstBeaconRqstDetail ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMRqstFrameRqstType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMRqstBssid ARG_LIST((UINT4  ,tMacAddr ));

INT1
nmhSetDot11RMRqstSSID ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11RMRqstBeaconReportingCondition ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMRqstBeaconThresholdOffset ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMRqstSTAStatRqstGroupID ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMRqstLCIRqstSubject ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMRqstLCILatitudeResolution ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstLCILongitudeResolution ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstLCIAltitudeResolution ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstLCIAzimuthType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMRqstLCIAzimuthResolution ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstPauseTime ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstTransmitStreamPeerQSTAAddress ARG_LIST((UINT4  ,tMacAddr ));

INT1
nmhSetDot11RMRqstTransmitStreamTrafficIdentifier ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstTransmitStreamBin0Range ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstTrigdQoSAverageCondition ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMRqstTrigdQoSConsecutiveCondition ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMRqstTrigdQoSDelayCondition ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMRqstTrigdQoSAverageThreshold ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstTrigdQoSConsecutiveThreshold ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstTrigdQoSDelayThresholdRange ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstTrigdQoSDelayThreshold ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstTrigdQoSMeasurementCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstTrigdQoSTimeout ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstChannelLoadReportingCondition ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMRqstChannelLoadReference ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstNoiseHistogramReportingCondition ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMRqstAnpiReference ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstAPChannelReport ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11RMRqstSTAStatPeerSTAAddress ARG_LIST((UINT4  ,tMacAddr ));

INT1
nmhSetDot11RMRqstFrameTransmitterAddress ARG_LIST((UINT4  ,tMacAddr ));

INT1
nmhSetDot11RMRqstVendorSpecific ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11RMRqstSTAStatTrigMeasCount ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstSTAStatTrigTimeout ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstSTAStatTrigCondition ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11RMRqstSTAStatTrigSTAFailedCntThresh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstSTAStatTrigSTAFCSErrCntThresh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstSTAStatTrigSTAMultRetryCntThresh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstSTAStatTrigSTAFrameDupeCntThresh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstSTAStatTrigSTARTSFailCntThresh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstSTAStatTrigSTAAckFailCntThresh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstSTAStatTrigSTARetryCntThresh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstSTAStatTrigQoSTrigCondition ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11RMRqstSTAStatTrigQoSFailedCntThresh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstSTAStatTrigQoSRetryCntThresh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstSTAStatTrigQoSMultRetryCntThresh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstSTAStatTrigQoSFrameDupeCntThresh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstSTAStatTrigQoSRTSFailCntThresh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstSTAStatTrigQoSAckFailCntThresh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstSTAStatTrigQoSDiscardCntThresh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstSTAStatTrigRsnaTrigCondition ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11RMRqstSTAStatTrigRsnaCMACICVErrCntThresh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstSTAStatTrigRsnaCMACReplayCntThresh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstSTAStatTrigRsnaRobustCCMPReplayCntThresh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstSTAStatTrigRsnaTKIPICVErrCntThresh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstSTAStatTrigRsnaTKIPReplayCntThresh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstSTAStatTrigRsnaCCMPDecryptErrCntThresh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMRqstSTAStatTrigRsnaCCMPReplayCntThresh ARG_LIST((UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11RMRqstRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRqstToken ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11RMRqstRepetitions ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstIfIndex ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRqstType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRqstTargetAdd ARG_LIST((UINT4 *  ,UINT4  ,tMacAddr ));

INT1
nmhTestv2Dot11RMRqstTimeStamp ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstChanNumber ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstOperatingClass ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstRndInterval ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstDuration ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstParallel ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRqstEnable ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRqstRequest ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRqstReport ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRqstDurationMandatory ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRqstBeaconRqstMode ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRqstBeaconRqstDetail ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRqstFrameRqstType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRqstBssid ARG_LIST((UINT4 *  ,UINT4  ,tMacAddr ));

INT1
nmhTestv2Dot11RMRqstSSID ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11RMRqstBeaconReportingCondition ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRqstBeaconThresholdOffset ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatRqstGroupID ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRqstLCIRqstSubject ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRqstLCILatitudeResolution ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstLCILongitudeResolution ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstLCIAltitudeResolution ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstLCIAzimuthType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRqstLCIAzimuthResolution ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstPauseTime ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstTransmitStreamPeerQSTAAddress ARG_LIST((UINT4 *  ,UINT4  ,tMacAddr ));

INT1
nmhTestv2Dot11RMRqstTransmitStreamTrafficIdentifier ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstTransmitStreamBin0Range ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstTrigdQoSAverageCondition ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRqstTrigdQoSConsecutiveCondition ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRqstTrigdQoSDelayCondition ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRqstTrigdQoSAverageThreshold ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstTrigdQoSConsecutiveThreshold ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstTrigdQoSDelayThresholdRange ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstTrigdQoSDelayThreshold ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstTrigdQoSMeasurementCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstTrigdQoSTimeout ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstChannelLoadReportingCondition ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRqstChannelLoadReference ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstNoiseHistogramReportingCondition ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMRqstAnpiReference ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstAPChannelReport ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11RMRqstSTAStatPeerSTAAddress ARG_LIST((UINT4 *  ,UINT4  ,tMacAddr ));

INT1
nmhTestv2Dot11RMRqstFrameTransmitterAddress ARG_LIST((UINT4 *  ,UINT4  ,tMacAddr ));

INT1
nmhTestv2Dot11RMRqstVendorSpecific ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigMeasCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigTimeout ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigCondition ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigSTAFailedCntThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigSTAFCSErrCntThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigSTAMultRetryCntThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigSTAFrameDupeCntThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigSTARTSFailCntThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigSTAAckFailCntThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigSTARetryCntThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigQoSTrigCondition ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigQoSFailedCntThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigQoSRetryCntThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigQoSMultRetryCntThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigQoSFrameDupeCntThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigQoSRTSFailCntThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigQoSAckFailCntThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigQoSDiscardCntThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigRsnaTrigCondition ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigRsnaCMACICVErrCntThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigRsnaCMACReplayCntThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigRsnaRobustCCMPReplayCntThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigRsnaTKIPICVErrCntThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigRsnaTKIPReplayCntThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigRsnaCCMPDecryptErrCntThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMRqstSTAStatTrigRsnaCCMPReplayCntThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11RMRequestTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11ChannelLoadReportTable. */
INT1
nmhValidateIndexInstanceDot11ChannelLoadReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11ChannelLoadReportTable  */

INT1
nmhGetFirstIndexDot11ChannelLoadReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11ChannelLoadReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11ChannelLoadRprtRqstToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11ChannelLoadRprtIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11ChannelLoadMeasuringSTAAddr ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11ChannelLoadRprtChanNumber ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11ChannelLoadRprtOperatingClass ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11ChannelLoadRprtActualStartTime ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11ChannelLoadRprtMeasurementDuration ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11ChannelLoadRprtChannelLoad ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11ChannelLoadRprtVendorSpecific ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11ChannelLoadRprtMeasurementMode ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11ChannelLoadRprtVendorSpecific ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11ChannelLoadRprtVendorSpecific ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11ChannelLoadReportTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11NoiseHistogramReportTable. */
INT1
nmhValidateIndexInstanceDot11NoiseHistogramReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11NoiseHistogramReportTable  */

INT1
nmhGetFirstIndexDot11NoiseHistogramReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11NoiseHistogramReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11NoiseHistogramRprtRqstToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11NoiseHistogramRprtIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11NoiseHistogramMeasuringSTAAddr ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11NoiseHistogramRprtChanNumber ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11NoiseHistogramRprtOperatingClass ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11NoiseHistogramRprtActualStartTime ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11NoiseHistogramRprtMeasurementDuration ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11NoiseHistogramRprtAntennaID ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11NoiseHistogramRprtANPI ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11NoiseHistogramRprtIPIDensity0 ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11NoiseHistogramRprtIPIDensity1 ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11NoiseHistogramRprtIPIDensity2 ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11NoiseHistogramRprtIPIDensity3 ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11NoiseHistogramRprtIPIDensity4 ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11NoiseHistogramRprtIPIDensity5 ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11NoiseHistogramRprtIPIDensity6 ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11NoiseHistogramRprtIPIDensity7 ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11NoiseHistogramRprtIPIDensity8 ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11NoiseHistogramRprtIPIDensity9 ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11NoiseHistogramRprtIPIDensity10 ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11NoiseHistogramRprtVendorSpecific ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11NoiseHistogramRprtMeasurementMode ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11NoiseHistogramRprtVendorSpecific ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11NoiseHistogramRprtVendorSpecific ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11NoiseHistogramReportTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11BeaconReportTable. */
INT1
nmhValidateIndexInstanceDot11BeaconReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11BeaconReportTable  */

INT1
nmhGetFirstIndexDot11BeaconReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11BeaconReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11BeaconRprtRqstToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11BeaconRprtIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11BeaconMeasuringSTAAddr ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11BeaconRprtChanNumber ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11BeaconRprtOperatingClass ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11BeaconRprtActualStartTime ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11BeaconRprtMeasurementDuration ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11BeaconRprtPhyType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11BeaconRprtReportedFrameType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11BeaconRprtRCPI ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11BeaconRprtRSNI ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11BeaconRprtBSSID ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11BeaconRprtAntennaID ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11BeaconRprtParentTSF ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11BeaconRprtReportedFrameBody ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11BeaconRprtVendorSpecific ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11BeaconRprtMeasurementMode ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11BeaconRprtVendorSpecific ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11BeaconRprtVendorSpecific ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11BeaconReportTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11FrameReportTable. */
INT1
nmhValidateIndexInstanceDot11FrameReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11FrameReportTable  */

INT1
nmhGetFirstIndexDot11FrameReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11FrameReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11FrameRprtIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11FrameRprtRqstToken ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11FrameRprtChanNumber ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11FrameRprtOperatingClass ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11FrameRprtActualStartTime ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11FrameRprtMeasurementDuration ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11FrameRprtTransmitSTAAddress ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11FrameRprtBSSID ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11FrameRprtPhyType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11FrameRprtAvgRCPI ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11FrameRprtLastRSNI ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11FrameRprtLastRCPI ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11FrameRprtAntennaID ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11FrameRprtNumberFrames ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11FrameRprtVendorSpecific ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11FrameRptMeasurementMode ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11FrameRprtPhyType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11FrameRprtVendorSpecific ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11FrameRprtPhyType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11FrameRprtVendorSpecific ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11FrameReportTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11STAStatisticsReportTable. */
INT1
nmhValidateIndexInstanceDot11STAStatisticsReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11STAStatisticsReportTable  */

INT1
nmhGetFirstIndexDot11STAStatisticsReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11STAStatisticsReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11STAStatisticsReportToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11STAStatisticsIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11STAStatisticsSTAAddress ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11STAStatisticsMeasurementDuration ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsGroupID ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11STAStatisticsTransmittedFragmentCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsGroupTransmittedFrameCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsFailedCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsRetryCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsMultipleRetryCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsFrameDuplicateCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsRTSSuccessCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsRTSFailureCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsACKFailureCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsQosTransmittedFragmentCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsQosFailedCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsQosRetryCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsQosMultipleRetryCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsQosFrameDuplicateCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsQosRTSSuccessCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsQosRTSFailureCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsQosACKFailureCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsQosReceivedFragmentCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsQosTransmittedFrameCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsQosDiscardedFrameCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsQosMPDUsReceivedCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsQosRetriesReceivedCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsReceivedFragmentCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsGroupReceivedFrameCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsFCSErrorCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsTransmittedFrameCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsAPAverageAccessDelay ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsAverageAccessDelayBestEffort ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsAverageAccessDelayBackground ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsAverageAccessDelayVideo ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsAverageAccessDelayVoice ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsStationCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsChannelUtilization ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsVendorSpecific ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11STAStatisticsRprtMeasurementMode ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11STAStatisticsRSNAStatsCMACICVErrors ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsRSNAStatsCMACReplays ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsRSNAStatsRobustMgmtCCMPReplays ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsRSNAStatsTKIPICVErrors ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsRSNAStatsTKIPReplays ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsRSNAStatsCCMPDecryptErrors ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsRSNAStatsCCMPReplays ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsReportingReasonSTACounters ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11STAStatisticsReportingReasonQosCounters ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11STAStatisticsReportingReasonRsnaCounters ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11STAStatisticsTransmittedAMSDUCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsFailedAMSDUCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsRetryAMSDUCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsMultipleRetryAMSDUCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsTransmittedOctetsInAMSDUCount ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDot11STAStatisticsAMSDUAckFailureCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsReceivedAMSDUCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsReceivedOctetsInAMSDUCount ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDot11STAStatisticsTransmittedAMPDUCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsTransmittedMPDUsInAMPDUCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsTransmittedOctetsInAMPDUCount ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDot11STAStatisticsAMPDUReceivedCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsMPDUInReceivedAMPDUCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsReceivedOctetsInAMPDUCount ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDot11STAStatisticsAMPDUDelimiterCRCErrorCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsImplicitBARFailureCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsExplicitBARFailureCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsChannelWidthSwitchCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsTwentyMHzFrameTransmittedCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsFortyMHzFrameTransmittedCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsTwentyMHzFrameReceivedCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsFortyMHzFrameReceivedCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsPSMPUTTGrantDuration ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsPSMPUTTUsedDuration ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsGrantedRDGUsedCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsGrantedRDGUnusedCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsTransmittedFramesInGrantedRDGCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsTransmittedOctetsInGrantedRDGCount ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDot11STAStatisticsDualCTSSuccessCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsDualCTSFailureCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsRTSLSIGSuccessCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsRTSLSIGFailureCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsBeamformingFrameCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsSTBCCTSSuccessCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsSTBCCTSFailureCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsnonSTBCCTSSuccessCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11STAStatisticsnonSTBCCTSFailureCount ARG_LIST((UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11STAStatisticsVendorSpecific ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11STAStatisticsVendorSpecific ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11STAStatisticsReportTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11LCIReportTable. */
INT1
nmhValidateIndexInstanceDot11LCIReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11LCIReportTable  */

INT1
nmhGetFirstIndexDot11LCIReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11LCIReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11LCIReportToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11LCIIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11LCISTAAddress ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11LCILatitudeResolution ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11LCILatitudeInteger ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11LCILatitudeFraction ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11LCILongitudeResolution ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11LCILongitudeInteger ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11LCILongitudeFraction ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11LCIAltitudeType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11LCIAltitudeResolution ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11LCIAltitudeInteger ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11LCIAltitudeFraction ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11LCIDatum ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11LCIAzimuthType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11LCIAzimuthResolution ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11LCIAzimuth ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11LCIVendorSpecific ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11LCIRprtMeasurementMode ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11LCIVendorSpecific ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11LCIVendorSpecific ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11LCIReportTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11TransmitStreamReportTable. */
INT1
nmhValidateIndexInstanceDot11TransmitStreamReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11TransmitStreamReportTable  */

INT1
nmhGetFirstIndexDot11TransmitStreamReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11TransmitStreamReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11TransmitStreamRprtRqstToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11TransmitStreamRprtIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11TransmitStreamMeasuringSTAAddr ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11TransmitStreamRprtActualStartTime ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11TransmitStreamRprtMeasurementDuration ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11TransmitStreamRprtPeerSTAAddress ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11TransmitStreamRprtTID ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11TransmitStreamRprtAverageQueueDelay ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11TransmitStreamRprtAverageTransmitDelay ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11TransmitStreamRprtTransmittedMSDUCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11TransmitStreamRprtMSDUDiscardedCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11TransmitStreamRprtMSDUFailedCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11TransmitStreamRprtMultipleRetryCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11TransmitStreamRprtCFPollsLostCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11TransmitStreamRprtBin0Range ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11TransmitStreamRprtDelayHistogram ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11TransmitStreamRprtReason ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11TransmitStreamRprtVendorSpecific ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11TransmitStreamRprtMeasurementMode ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11TransmitStreamRprtVendorSpecific ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11TransmitStreamRprtVendorSpecific ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11TransmitStreamReportTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11APChannelReportTable. */
INT1
nmhValidateIndexInstanceDot11APChannelReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11APChannelReportTable  */

INT1
nmhGetFirstIndexDot11APChannelReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11APChannelReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11APChannelReportIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11APChannelReportOperatingClass ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11APChannelReportChannelList ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11APChannelReportIfIndex ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11APChannelReportOperatingClass ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11APChannelReportChannelList ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11APChannelReportIfIndex ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11APChannelReportOperatingClass ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11APChannelReportChannelList ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11APChannelReportTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11RMNeighborReportTable. */
INT1
nmhValidateIndexInstanceDot11RMNeighborReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11RMNeighborReportTable  */

INT1
nmhGetFirstIndexDot11RMNeighborReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11RMNeighborReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11RMNeighborReportIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportBSSID ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11RMNeighborReportAPReachability ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportSecurity ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportCapSpectrumMgmt ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportCapQoS ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportCapAPSD ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportCapRM ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportCapDelayBlockAck ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportCapImmediateBlockAck ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportKeyScope ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportOperatingClass ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportChannelNumber ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportPhyType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportNeighborTSFInfo ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RMNeighborReportPilotInterval ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportPilotMultipleBSSID ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RMNeighborReportRMEnabledCapabilities ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RMNeighborReportVendorSpecific ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RMNeighborReportRowStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportMobilityDomain ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportCapHT ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTLDPCCodingCap ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTSupportedChannelWidthSet ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTSMPowerSave ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportHTGreenfield ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTShortGIfor20MHz ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTShortGIfor40MHz ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTTxSTBC ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTRxSTBC ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportHTDelayedBlockAck ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTMaxAMSDULength ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTDSSCCKModein40MHz ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTFortyMHzIntolerant ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTLSIGTXOPProtectionSupport ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTMaxAMPDULengthExponent ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportHTMinMPDUStartSpacing ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportHTRxMCSBitMask ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RMNeighborReportHTRxHighestSupportedDataRate ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportHTTxMCSSetDefined ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTTxRxMCSSetNotEqual ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTTxMaxNumberSpatialStreamsSupported ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportHTTxUnequalModulationSupported ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTPCO ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTPCOTransitionTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportHTMCSFeedback ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportHTCSupport ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTRDResponder ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTImplictTransmitBeamformingReceivingCap ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTReceiveStaggeredSoundingCap ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTTransmitStaggeredSoundingCap ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTReceiveNDPCap ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTTransmitNDPCap ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTImplicitTransmitBeamformingCap ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTTransmitBeamformingCalibration ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportHTExplicitCSITransmitBeamformingCap ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTExplicitNonCompressedSteeringCap ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTExplicitCompressedSteeringCap ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTExplicitTransmitBeamformingFeedback ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNbRprtHTExplicitNonCompressedBeamformingFeedbackCap ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportHTExplicitCompressedBeamformingFeedbackCap ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportHTTransmitBeamformingMinimalGrouping ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNbRprtHTCSINumberofTxBeamformingAntennasSuppt ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNbRprtHTNonCompressedSteeringNumofTxBmfmingAntennasSuppt ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNbRprtHTCompressedSteeringNumberofTxBmfmingAntennasSuppt ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNbRprtHTCSIMaxNumberofRowsTxBeamformingSuppt ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportHTTransmitBeamformingChannelEstimationCap ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportHTAntSelectionCap ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTExplicitCSIFeedbackBasedTxASELCap ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTAntIndicesFeedbackBasedTxASELCap ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTExplicitCSIFeedbackBasedCap ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTAntIndicesFeedbackCap ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTRxASELCap ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTTxSoundingPPDUsCap ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTInfoPrimaryChannel ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportHTInfoSecChannelOffset ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportHTInfoSTAChannelWidth ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTInfoRIFSMode ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTInfoProtection ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportHTInfoNonGreenfieldHTSTAsPresent ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTInfoOBSSNonHTSTAsPresent ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTInfoDualBeacon ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTInfoDualCTSProtection ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTInfoSTBCBeacon ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTInfoLSIGTXOPProtectionSup ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTInfoPCOActive ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTInfoPCOPhase ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportHTInfoBasicMCSSet ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RMNeighborReportHTSecChannelOffset ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportExtCapPSMPSupport ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportExtCapSPSMPSup ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11RMNeighborReportExtCapServiceIntervalGranularity ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportBSSTransitCandPreference ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11RMNeighborReportBSSTerminationTSF ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RMNeighborReportBSSTerminationDuration ARG_LIST((UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11RMNeighborReportIfIndex ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportBSSID ARG_LIST((UINT4  ,tMacAddr ));

INT1
nmhSetDot11RMNeighborReportAPReachability ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportSecurity ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportCapSpectrumMgmt ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportCapQoS ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportCapAPSD ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportCapRM ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportCapDelayBlockAck ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportCapImmediateBlockAck ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportKeyScope ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportOperatingClass ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportChannelNumber ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportPhyType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportNeighborTSFInfo ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11RMNeighborReportPilotInterval ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportPilotMultipleBSSID ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11RMNeighborReportRMEnabledCapabilities ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11RMNeighborReportVendorSpecific ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11RMNeighborReportRowStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportMobilityDomain ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportCapHT ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTLDPCCodingCap ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTSupportedChannelWidthSet ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTSMPowerSave ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportHTGreenfield ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTShortGIfor20MHz ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTShortGIfor40MHz ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTTxSTBC ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTRxSTBC ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportHTDelayedBlockAck ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTMaxAMSDULength ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTDSSCCKModein40MHz ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTFortyMHzIntolerant ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTLSIGTXOPProtectionSupport ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTMaxAMPDULengthExponent ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportHTMinMPDUStartSpacing ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportHTRxMCSBitMask ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11RMNeighborReportHTRxHighestSupportedDataRate ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportHTTxMCSSetDefined ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTTxRxMCSSetNotEqual ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTTxMaxNumberSpatialStreamsSupported ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportHTTxUnequalModulationSupported ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTPCO ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTPCOTransitionTime ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportHTMCSFeedback ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportHTCSupport ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTRDResponder ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTImplictTransmitBeamformingReceivingCap ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTReceiveStaggeredSoundingCap ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTTransmitStaggeredSoundingCap ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTReceiveNDPCap ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTTransmitNDPCap ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTImplicitTransmitBeamformingCap ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTTransmitBeamformingCalibration ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportHTExplicitCSITransmitBeamformingCap ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTExplicitNonCompressedSteeringCap ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTExplicitCompressedSteeringCap ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTExplicitTransmitBeamformingFeedback ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNbRprtHTExplicitNonCompressedBeamformingFeedbackCap ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportHTExplicitCompressedBeamformingFeedbackCap ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportHTTransmitBeamformingMinimalGrouping ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNbRprtHTCSINumberofTxBeamformingAntennasSuppt ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNbRprtHTNonCompressedSteeringNumofTxBmfmingAntennasSuppt ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNbRprtHTCompressedSteeringNumberofTxBmfmingAntennasSuppt ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNbRprtHTCSIMaxNumberofRowsTxBeamformingSuppt ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportHTTransmitBeamformingChannelEstimationCap ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportHTAntSelectionCap ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTExplicitCSIFeedbackBasedTxASELCap ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTAntIndicesFeedbackBasedTxASELCap ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTExplicitCSIFeedbackBasedCap ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTAntIndicesFeedbackCap ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTRxASELCap ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTTxSoundingPPDUsCap ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTInfoPrimaryChannel ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportHTInfoSecChannelOffset ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportHTInfoSTAChannelWidth ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTInfoRIFSMode ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTInfoProtection ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportHTInfoNonGreenfieldHTSTAsPresent ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTInfoOBSSNonHTSTAsPresent ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTInfoDualBeacon ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTInfoDualCTSProtection ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTInfoSTBCBeacon ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTInfoLSIGTXOPProtectionSup ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTInfoPCOActive ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTInfoPCOPhase ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportHTInfoBasicMCSSet ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11RMNeighborReportHTSecChannelOffset ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportExtCapPSMPSupport ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportExtCapSPSMPSup ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11RMNeighborReportExtCapServiceIntervalGranularity ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportBSSTransitCandPreference ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11RMNeighborReportBSSTerminationTSF ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11RMNeighborReportBSSTerminationDuration ARG_LIST((UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11RMNeighborReportIfIndex ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportBSSID ARG_LIST((UINT4 *  ,UINT4  ,tMacAddr ));

INT1
nmhTestv2Dot11RMNeighborReportAPReachability ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportSecurity ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportCapSpectrumMgmt ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportCapQoS ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportCapAPSD ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportCapRM ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportCapDelayBlockAck ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportCapImmediateBlockAck ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportKeyScope ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportOperatingClass ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportChannelNumber ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportPhyType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportNeighborTSFInfo ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11RMNeighborReportPilotInterval ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportPilotMultipleBSSID ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11RMNeighborReportRMEnabledCapabilities ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11RMNeighborReportVendorSpecific ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11RMNeighborReportRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportMobilityDomain ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportCapHT ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTLDPCCodingCap ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTSupportedChannelWidthSet ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTSMPowerSave ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTGreenfield ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTShortGIfor20MHz ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTShortGIfor40MHz ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTTxSTBC ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTRxSTBC ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTDelayedBlockAck ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTMaxAMSDULength ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTDSSCCKModein40MHz ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTFortyMHzIntolerant ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTLSIGTXOPProtectionSupport ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTMaxAMPDULengthExponent ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTMinMPDUStartSpacing ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTRxMCSBitMask ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11RMNeighborReportHTRxHighestSupportedDataRate ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTTxMCSSetDefined ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTTxRxMCSSetNotEqual ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTTxMaxNumberSpatialStreamsSupported ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTTxUnequalModulationSupported ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTPCO ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTPCOTransitionTime ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTMCSFeedback ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTCSupport ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTRDResponder ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTImplictTransmitBeamformingReceivingCap ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTReceiveStaggeredSoundingCap ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTTransmitStaggeredSoundingCap ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTReceiveNDPCap ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTTransmitNDPCap ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTImplicitTransmitBeamformingCap ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTTransmitBeamformingCalibration ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTExplicitCSITransmitBeamformingCap ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTExplicitNonCompressedSteeringCap ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTExplicitCompressedSteeringCap ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTExplicitTransmitBeamformingFeedback ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNbRprtHTExplicitNonCompressedBeamformingFeedbackCap ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTExplicitCompressedBeamformingFeedbackCap ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTTransmitBeamformingMinimalGrouping ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNbRprtHTCSINumberofTxBeamformingAntennasSuppt ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNbRprtHTNonCompressedSteeringNumofTxBmfmingAntennasSuppt ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNbRprtHTCompressedSteeringNumberofTxBmfmingAntennasSuppt ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNbRprtHTCSIMaxNumberofRowsTxBeamformingSuppt ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTTransmitBeamformingChannelEstimationCap ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTAntSelectionCap ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTExplicitCSIFeedbackBasedTxASELCap ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTAntIndicesFeedbackBasedTxASELCap ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTExplicitCSIFeedbackBasedCap ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTAntIndicesFeedbackCap ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTRxASELCap ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTTxSoundingPPDUsCap ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTInfoPrimaryChannel ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTInfoSecChannelOffset ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTInfoSTAChannelWidth ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTInfoRIFSMode ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTInfoProtection ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTInfoNonGreenfieldHTSTAsPresent ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTInfoOBSSNonHTSTAsPresent ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTInfoDualBeacon ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTInfoDualCTSProtection ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTInfoSTBCBeacon ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTInfoLSIGTXOPProtectionSup ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTInfoPCOActive ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTInfoPCOPhase ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportHTInfoBasicMCSSet ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11RMNeighborReportHTSecChannelOffset ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportExtCapPSMPSupport ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportExtCapSPSMPSup ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11RMNeighborReportExtCapServiceIntervalGranularity ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportBSSTransitCandPreference ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11RMNeighborReportBSSTerminationTSF ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11RMNeighborReportBSSTerminationDuration ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11RMNeighborReportTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11WNMRequestNextIndex ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for Dot11WNMRequestTable. */
INT1
nmhValidateIndexInstanceDot11WNMRequestTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11WNMRequestTable  */

INT1
nmhGetFirstIndexDot11WNMRequestTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11WNMRequestTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11WNMRqstRowStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMRqstToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMRqstIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMRqstType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMRqstTargetAdd ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11WNMRqstTimeStamp ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMRqstRndInterval ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMRqstDuration ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMRqstMcstGroup ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11WNMRqstMcstTrigCon ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMRqstMcstTrigInactivityTimeout ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMRqstMcstTrigReactDelay ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMRqstLCRRqstSubject ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMRqstLCRIntervalUnits ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMRqstLCRServiceInterval ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMRqstLIRRqstSubject ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMRqstLIRIntervalUnits ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMRqstLIRServiceInterval ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMRqstEventToken ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMRqstEventType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMRqstEventResponseLimit ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMRqstEventTargetBssid ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11WNMRqstEventSourceBssid ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11WNMRqstEventTransitTimeThresh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMRqstEventTransitMatchValue ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMRqstEventFreqTransitCountThresh ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMRqstEventFreqTransitInterval ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMRqstEventRsnaAuthType ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMRqstEapType ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMRqstEapVendorId ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMRqstEapVendorType ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMRqstEventRsnaMatchValue ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMRqstEventPeerMacAddress ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11WNMRqstOperatingClass ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMRqstChanNumber ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMRqstDiagToken ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMRqstDiagType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMRqstDiagTimeout ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMRqstDiagBssid ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11WNMRqstDiagProfileId ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMRqstDiagCredentials ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMRqstLocConfigLocIndParams ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMRqstLocConfigChanList ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMRqstLocConfigBcastRate ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMRqstLocConfigOptions ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMRqstBssTransitQueryReason ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMRqstBssTransitReqMode ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMRqstBssTransitDisocTimer ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMRqstBssTransitSessInfoURL ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMRqstBssTransitCandidateList ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMRqstColocInterfAutoEnable ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMRqstColocInterfRptTimeout ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMRqstVendorSpecific ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMRqstDestinationURI ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11WNMRqstRowStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11WNMRqstToken ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11WNMRqstIfIndex ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11WNMRqstType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11WNMRqstTargetAdd ARG_LIST((UINT4  ,tMacAddr ));

INT1
nmhSetDot11WNMRqstRndInterval ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11WNMRqstDuration ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11WNMRqstMcstGroup ARG_LIST((UINT4  ,tMacAddr ));

INT1
nmhSetDot11WNMRqstMcstTrigCon ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11WNMRqstMcstTrigInactivityTimeout ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11WNMRqstMcstTrigReactDelay ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11WNMRqstLCRRqstSubject ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11WNMRqstLCRIntervalUnits ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11WNMRqstLCRServiceInterval ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11WNMRqstLIRRqstSubject ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11WNMRqstLIRIntervalUnits ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11WNMRqstLIRServiceInterval ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11WNMRqstEventToken ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11WNMRqstEventType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11WNMRqstEventResponseLimit ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11WNMRqstEventTargetBssid ARG_LIST((UINT4  ,tMacAddr ));

INT1
nmhSetDot11WNMRqstEventSourceBssid ARG_LIST((UINT4  ,tMacAddr ));

INT1
nmhSetDot11WNMRqstEventTransitTimeThresh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11WNMRqstEventTransitMatchValue ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11WNMRqstEventFreqTransitCountThresh ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11WNMRqstEventFreqTransitInterval ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11WNMRqstEventRsnaAuthType ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11WNMRqstEapType ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11WNMRqstEapVendorId ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11WNMRqstEapVendorType ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11WNMRqstEventRsnaMatchValue ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11WNMRqstEventPeerMacAddress ARG_LIST((UINT4  ,tMacAddr ));

INT1
nmhSetDot11WNMRqstOperatingClass ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11WNMRqstChanNumber ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11WNMRqstDiagToken ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11WNMRqstDiagType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11WNMRqstDiagTimeout ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11WNMRqstDiagBssid ARG_LIST((UINT4  ,tMacAddr ));

INT1
nmhSetDot11WNMRqstDiagProfileId ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11WNMRqstDiagCredentials ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11WNMRqstLocConfigLocIndParams ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11WNMRqstLocConfigChanList ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11WNMRqstLocConfigBcastRate ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11WNMRqstLocConfigOptions ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11WNMRqstBssTransitQueryReason ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11WNMRqstBssTransitReqMode ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11WNMRqstBssTransitDisocTimer ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11WNMRqstBssTransitSessInfoURL ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11WNMRqstBssTransitCandidateList ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11WNMRqstColocInterfAutoEnable ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11WNMRqstColocInterfRptTimeout ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11WNMRqstVendorSpecific ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11WNMRqstDestinationURI ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11WNMRqstRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11WNMRqstToken ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11WNMRqstIfIndex ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11WNMRqstType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11WNMRqstTargetAdd ARG_LIST((UINT4 *  ,UINT4  ,tMacAddr ));

INT1
nmhTestv2Dot11WNMRqstRndInterval ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11WNMRqstDuration ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11WNMRqstMcstGroup ARG_LIST((UINT4 *  ,UINT4  ,tMacAddr ));

INT1
nmhTestv2Dot11WNMRqstMcstTrigCon ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11WNMRqstMcstTrigInactivityTimeout ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11WNMRqstMcstTrigReactDelay ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11WNMRqstLCRRqstSubject ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11WNMRqstLCRIntervalUnits ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11WNMRqstLCRServiceInterval ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11WNMRqstLIRRqstSubject ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11WNMRqstLIRIntervalUnits ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11WNMRqstLIRServiceInterval ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11WNMRqstEventToken ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11WNMRqstEventType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11WNMRqstEventResponseLimit ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11WNMRqstEventTargetBssid ARG_LIST((UINT4 *  ,UINT4  ,tMacAddr ));

INT1
nmhTestv2Dot11WNMRqstEventSourceBssid ARG_LIST((UINT4 *  ,UINT4  ,tMacAddr ));

INT1
nmhTestv2Dot11WNMRqstEventTransitTimeThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11WNMRqstEventTransitMatchValue ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11WNMRqstEventFreqTransitCountThresh ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11WNMRqstEventFreqTransitInterval ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11WNMRqstEventRsnaAuthType ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11WNMRqstEapType ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11WNMRqstEapVendorId ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11WNMRqstEapVendorType ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11WNMRqstEventRsnaMatchValue ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11WNMRqstEventPeerMacAddress ARG_LIST((UINT4 *  ,UINT4  ,tMacAddr ));

INT1
nmhTestv2Dot11WNMRqstOperatingClass ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11WNMRqstChanNumber ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11WNMRqstDiagToken ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11WNMRqstDiagType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11WNMRqstDiagTimeout ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11WNMRqstDiagBssid ARG_LIST((UINT4 *  ,UINT4  ,tMacAddr ));

INT1
nmhTestv2Dot11WNMRqstDiagProfileId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11WNMRqstDiagCredentials ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11WNMRqstLocConfigLocIndParams ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11WNMRqstLocConfigChanList ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11WNMRqstLocConfigBcastRate ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11WNMRqstLocConfigOptions ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11WNMRqstBssTransitQueryReason ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11WNMRqstBssTransitReqMode ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11WNMRqstBssTransitDisocTimer ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11WNMRqstBssTransitSessInfoURL ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11WNMRqstBssTransitCandidateList ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11WNMRqstColocInterfAutoEnable ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11WNMRqstColocInterfRptTimeout ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11WNMRqstVendorSpecific ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11WNMRqstDestinationURI ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11WNMRequestTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11WNMVendorSpecificReportTable. */
INT1
nmhValidateIndexInstanceDot11WNMVendorSpecificReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11WNMVendorSpecificReportTable  */

INT1
nmhGetFirstIndexDot11WNMVendorSpecificReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11WNMVendorSpecificReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11WNMVendorSpecificRprtRqstToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMVendorSpecificRprtIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMVendorSpecificRprtContent ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for Dot11WNMMulticastDiagnosticReportTable. */
INT1
nmhValidateIndexInstanceDot11WNMMulticastDiagnosticReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11WNMMulticastDiagnosticReportTable  */

INT1
nmhGetFirstIndexDot11WNMMulticastDiagnosticReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11WNMMulticastDiagnosticReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11WNMMulticastDiagnosticRprtRqstToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMMulticastDiagnosticRprtIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMMulticastDiagnosticRprtMeasurementTime ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMMulticastDiagnosticRprtDuration ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMMulticastDiagnosticRprtMcstGroup ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11WNMMulticastDiagnosticRprtReason ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMMulticastDiagnosticRprtRcvdMsduCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMMulticastDiagnosticRprtFirstSeqNumber ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMMulticastDiagnosticRprtLastSeqNumber ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMMulticastDiagnosticRprtMcstRate ARG_LIST((UINT4 ,UINT4 *));

/* Proto Validate Index Instance for Dot11WNMLocationCivicReportTable. */
INT1
nmhValidateIndexInstanceDot11WNMLocationCivicReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11WNMLocationCivicReportTable  */

INT1
nmhGetFirstIndexDot11WNMLocationCivicReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11WNMLocationCivicReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11WNMLocationCivicRprtRqstToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMLocationCivicRprtIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMLocationCivicRprtCivicLocation ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for Dot11WNMLocationIdentifierReportTable. */
INT1
nmhValidateIndexInstanceDot11WNMLocationIdentifierReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11WNMLocationIdentifierReportTable  */

INT1
nmhGetFirstIndexDot11WNMLocationIdentifierReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11WNMLocationIdentifierReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11WNMLocationIdentifierRprtRqstToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMLocationIdentifierRprtIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMLocationIdentifierRprtExpirationTSF ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMLocationIdentifierRprtPublicIdUri ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for Dot11WNMEventTransitReportTable. */
INT1
nmhValidateIndexInstanceDot11WNMEventTransitReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11WNMEventTransitReportTable  */

INT1
nmhGetFirstIndexDot11WNMEventTransitReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11WNMEventTransitReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11WNMEventTransitRprtRqstToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMEventTransitRprtIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMEventTransitRprtEventStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMEventTransitRprtEventTSF ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMEventTransitRprtUTCOffset ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMEventTransitRprtTimeError ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMEventTransitRprtSourceBssid ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11WNMEventTransitRprtTargetBssid ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11WNMEventTransitRprtTransitTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMEventTransitRprtTransitReason ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMEventTransitRprtTransitResult ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMEventTransitRprtSourceRCPI ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMEventTransitRprtSourceRSNI ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMEventTransitRprtTargetRCPI ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMEventTransitRprtTargetRSNI ARG_LIST((UINT4 ,UINT4 *));

/* Proto Validate Index Instance for Dot11WNMEventRsnaReportTable. */
INT1
nmhValidateIndexInstanceDot11WNMEventRsnaReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11WNMEventRsnaReportTable  */

INT1
nmhGetFirstIndexDot11WNMEventRsnaReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11WNMEventRsnaReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11WNMEventRsnaRprtRqstToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMEventRsnaRprtIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMEventRsnaRprtEventStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMEventRsnaRprtEventTSF ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMEventRsnaRprtUTCOffset ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMEventRsnaRprtTimeError ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMEventRsnaRprtTargetBssid ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11WNMEventRsnaRprtAuthType ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMEventRsnaRprtEapMethod ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMEventRsnaRprtResult ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMEventRsnaRprtRsnElement ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for Dot11WNMEventPeerReportTable. */
INT1
nmhValidateIndexInstanceDot11WNMEventPeerReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11WNMEventPeerReportTable  */

INT1
nmhGetFirstIndexDot11WNMEventPeerReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11WNMEventPeerReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11WNMEventPeerRprtRqstToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMEventPeerRprtIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMEventPeerRprtEventStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMEventPeerRprtEventTSF ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMEventPeerRprtUTCOffset ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMEventPeerRprtTimeError ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMEventPeerRprtPeerMacAddress ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11WNMEventPeerRprtOperatingClass ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMEventPeerRprtChanNumber ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMEventPeerRprtStaTxPower ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMEventPeerRprtConnTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMEventPeerRprtPeerStatus ARG_LIST((UINT4 ,INT4 *));

/* Proto Validate Index Instance for Dot11WNMEventWNMLogReportTable. */
INT1
nmhValidateIndexInstanceDot11WNMEventWNMLogReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11WNMEventWNMLogReportTable  */

INT1
nmhGetFirstIndexDot11WNMEventWNMLogReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11WNMEventWNMLogReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11WNMEventWNMLogRprtRqstToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMEventWNMLogRprtIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMEventWNMLogRprtEventStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMEventWNMLogRprtEventTSF ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMEventWNMLogRprtUTCOffset ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMEventWNMLogRprtTimeError ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMEventWNMLogRprtContent ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for Dot11WNMDiagMfrInfoReportTable. */
INT1
nmhValidateIndexInstanceDot11WNMDiagMfrInfoReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11WNMDiagMfrInfoReportTable  */

INT1
nmhGetFirstIndexDot11WNMDiagMfrInfoReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11WNMDiagMfrInfoReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11WNMDiagMfrInfoRprtRqstToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMDiagMfrInfoRprtIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMDiagMfrInfoRprtEventStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMDiagMfrInfoRprtMfrOi ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMDiagMfrInfoRprtMfrIdString ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMDiagMfrInfoRprtMfrModelString ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMDiagMfrInfoRprtMfrSerialNumberString ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMDiagMfrInfoRprtMfrFirmwareVersion ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMDiagMfrInfoRprtMfrAntennaType ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMDiagMfrInfoRprtCollocRadioType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMDiagMfrInfoRprtDeviceType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMDiagMfrInfoRprtCertificateID ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for Dot11WNMDiagConfigProfReportTable. */
INT1
nmhValidateIndexInstanceDot11WNMDiagConfigProfReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11WNMDiagConfigProfReportTable  */

INT1
nmhGetFirstIndexDot11WNMDiagConfigProfReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11WNMDiagConfigProfReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11WNMDiagConfigProfRprtRqstToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMDiagConfigProfRprtIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMDiagConfigProfRprtEventStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMDiagConfigProfRprtProfileId ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMDiagConfigProfRprtSupportedOperatingClasses ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMDiagConfigProfRprtTxPowerMode ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMDiagConfigProfRprtTxPowerLevels ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMDiagConfigProfRprtCipherSuite ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMDiagConfigProfRprtAkmSuite ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMDiagConfigProfRprtEapType ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMDiagConfigProfRprtEapVendorID ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMDiagConfigProfRprtEapVendorType ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMDiagConfigProfRprtCredentialType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMDiagConfigProfRprtSSID ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMDiagConfigProfRprtPowerSaveMode ARG_LIST((UINT4 ,INT4 *));

/* Proto Validate Index Instance for Dot11WNMDiagAssocReportTable. */
INT1
nmhValidateIndexInstanceDot11WNMDiagAssocReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11WNMDiagAssocReportTable  */

INT1
nmhGetFirstIndexDot11WNMDiagAssocReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11WNMDiagAssocReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11WNMDiagAssocRprtRqstToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMDiagAssocRprtIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMDiagAssocRprtEventStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMDiagAssocRprtBssid ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11WNMDiagAssocRprtOperatingClass ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMDiagAssocRprtChannelNumber ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMDiagAssocRprtStatusCode ARG_LIST((UINT4 ,UINT4 *));

/* Proto Validate Index Instance for Dot11WNMDiag8021xAuthReportTable. */
INT1
nmhValidateIndexInstanceDot11WNMDiag8021xAuthReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11WNMDiag8021xAuthReportTable  */

INT1
nmhGetFirstIndexDot11WNMDiag8021xAuthReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11WNMDiag8021xAuthReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11WNMDiag8021xAuthRprtRqstToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMDiag8021xAuthRprtIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMDiag8021xAuthRprtEventStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMDiag8021xAuthRprtBssid ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11WNMDiag8021xAuthRprtOperatingClass ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMDiag8021xAuthRprtChannelNumber ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMDiag8021xAuthRprtEapType ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMDiag8021xAuthRprtEapVendorID ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMDiag8021xAuthRprtEapVendorType ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMDiag8021xAuthRprtCredentialType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMDiag8021xAuthRprtStatusCode ARG_LIST((UINT4 ,UINT4 *));

/* Proto Validate Index Instance for Dot11WNMLocConfigReportTable. */
INT1
nmhValidateIndexInstanceDot11WNMLocConfigReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11WNMLocConfigReportTable  */

INT1
nmhGetFirstIndexDot11WNMLocConfigReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11WNMLocConfigReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11WNMLocConfigRprtRqstToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMLocConfigRprtIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMLocConfigRprtLocIndParams ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMLocConfigRprtLocIndChanList ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMLocConfigRprtLocIndBcastRate ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMLocConfigRprtLocIndOptions ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMLocConfigRprtStatusConfigSubelemId ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMLocConfigRprtStatusResult ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMLocConfigRprtVendorSpecificRprtContent ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for Dot11WNMBssTransitReportTable. */
INT1
nmhValidateIndexInstanceDot11WNMBssTransitReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11WNMBssTransitReportTable  */

INT1
nmhGetFirstIndexDot11WNMBssTransitReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11WNMBssTransitReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11WNMBssTransitRprtRqstToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMBssTransitRprtIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMBssTransitRprtStatusCode ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMBssTransitRprtBSSTerminationDelay ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMBssTransitRprtTargetBssid ARG_LIST((UINT4 ,tMacAddr * ));

INT1
nmhGetDot11WNMBssTransitRprtCandidateList ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11WNMBssTransitRprtCandidateList ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11WNMBssTransitRprtCandidateList ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11WNMBssTransitReportTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11WNMColocInterfReportTable. */
INT1
nmhValidateIndexInstanceDot11WNMColocInterfReportTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11WNMColocInterfReportTable  */

INT1
nmhGetFirstIndexDot11WNMColocInterfReportTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11WNMColocInterfReportTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11WNMColocInterfRprtRqstToken ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11WNMColocInterfRprtIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMColocInterfRprtPeriod ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMColocInterfRprtInterfLevel ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMColocInterfRprtInterfAccuracy ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMColocInterfRprtInterfIndex ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11WNMColocInterfRprtInterfInterval ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMColocInterfRprtInterfBurstLength ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMColocInterfRprtInterfStartTime ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMColocInterfRprtInterfCenterFreq ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11WNMColocInterfRprtInterfBandwidth ARG_LIST((UINT4 ,UINT4 *));

/* Proto Validate Index Instance for Dot11MeshSTAConfigTable. */
INT1
nmhValidateIndexInstanceDot11MeshSTAConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11MeshSTAConfigTable  */

INT1
nmhGetFirstIndexDot11MeshSTAConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11MeshSTAConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11MeshID ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11MeshNumberOfPeerings ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshAcceptingAdditionalPeerings ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MeshConnectedToMeshGate ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MeshSecurityActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MeshActiveAuthenticationProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MeshMaxRetries ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshRetryTimeout ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshConfirmTimeout ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshHoldingTimeout ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshConfigGroupUpdateCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshActivePathSelectionProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MeshActivePathSelectionMetric ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MeshForwarding ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MeshTTL ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshGateAnnouncements ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MeshGateAnnouncementInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshActiveCongestionControlMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MeshActiveSynchronizationMethod ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MeshNbrOffsetMaxNeighbor ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MBCAActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MeshBeaconTimingReportInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshBeaconTimingReportMaxNum ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshDelayedBeaconTxInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshDelayedBeaconTxMaxDelay ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshDelayedBeaconTxMinDelay ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshAverageBeaconFrameDuration ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshSTAMissingAckRetryLimit ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshAwakeWindowDuration ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MCCAImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MCCAActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MAFlimit ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MCCAScanDuration ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MCCAAdvertPeriodMax ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MCCAMinTrackStates ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MCCAMaxTrackStates ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MCCAOPtimeout ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MCCACWmin ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MCCACWmax ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MCCAAIFSN ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11MeshID ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11MeshNumberOfPeerings ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshAcceptingAdditionalPeerings ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MeshConnectedToMeshGate ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MeshSecurityActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MeshActiveAuthenticationProtocol ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MeshMaxRetries ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshRetryTimeout ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshConfirmTimeout ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshHoldingTimeout ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshConfigGroupUpdateCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshActivePathSelectionProtocol ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MeshActivePathSelectionMetric ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MeshForwarding ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MeshTTL ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshGateAnnouncements ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MeshGateAnnouncementInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshActiveCongestionControlMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MeshActiveSynchronizationMethod ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MBCAActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MeshBeaconTimingReportInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshBeaconTimingReportMaxNum ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshDelayedBeaconTxInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshDelayedBeaconTxMaxDelay ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshDelayedBeaconTxMinDelay ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshAverageBeaconFrameDuration ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshSTAMissingAckRetryLimit ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshAwakeWindowDuration ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MCCAActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MAFlimit ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MCCAScanDuration ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MCCAAdvertPeriodMax ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MCCAMinTrackStates ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MCCAMaxTrackStates ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MCCAOPtimeout ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MCCACWmin ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MCCACWmax ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MCCAAIFSN ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11MeshID ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11MeshNumberOfPeerings ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshAcceptingAdditionalPeerings ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MeshConnectedToMeshGate ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MeshSecurityActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MeshActiveAuthenticationProtocol ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MeshMaxRetries ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshRetryTimeout ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshConfirmTimeout ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshHoldingTimeout ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshConfigGroupUpdateCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshActivePathSelectionProtocol ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MeshActivePathSelectionMetric ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MeshForwarding ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MeshTTL ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshGateAnnouncements ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MeshGateAnnouncementInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshActiveCongestionControlMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MeshActiveSynchronizationMethod ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MBCAActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MeshBeaconTimingReportInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshBeaconTimingReportMaxNum ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshDelayedBeaconTxInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshDelayedBeaconTxMaxDelay ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshDelayedBeaconTxMinDelay ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshAverageBeaconFrameDuration ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshSTAMissingAckRetryLimit ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshAwakeWindowDuration ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MCCAActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MAFlimit ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MCCAScanDuration ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MCCAAdvertPeriodMax ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MCCAMinTrackStates ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MCCAMaxTrackStates ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MCCAOPtimeout ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MCCACWmin ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MCCACWmax ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MCCAAIFSN ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11MeshSTAConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11MeshHWMPConfigTable. */
INT1
nmhValidateIndexInstanceDot11MeshHWMPConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11MeshHWMPConfigTable  */

INT1
nmhGetFirstIndexDot11MeshHWMPConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11MeshHWMPConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11MeshHWMPmaxPREQretries ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshHWMPnetDiameter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshHWMPnetDiameterTraversalTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshHWMPpreqMinInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshHWMPperrMinInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshHWMPactivePathToRootTimeout ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshHWMPactivePathTimeout ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshHWMProotMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MeshHWMProotInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshHWMPrannInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshHWMPtargetOnly ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11MeshHWMPmaintenanceInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MeshHWMPconfirmationInterval ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11MeshHWMPmaxPREQretries ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshHWMPnetDiameter ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshHWMPnetDiameterTraversalTime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshHWMPpreqMinInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshHWMPperrMinInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshHWMPactivePathToRootTimeout ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshHWMPactivePathTimeout ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshHWMProotMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MeshHWMProotInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshHWMPrannInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshHWMPtargetOnly ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11MeshHWMPmaintenanceInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MeshHWMPconfirmationInterval ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11MeshHWMPmaxPREQretries ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshHWMPnetDiameter ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshHWMPnetDiameterTraversalTime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshHWMPpreqMinInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshHWMPperrMinInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshHWMPactivePathToRootTimeout ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshHWMPactivePathTimeout ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshHWMProotMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MeshHWMProotInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshHWMPrannInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshHWMPtargetOnly ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11MeshHWMPmaintenanceInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MeshHWMPconfirmationInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11MeshHWMPConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11RSNAConfigPasswordValueTable. */
INT1
nmhValidateIndexInstanceDot11RSNAConfigPasswordValueTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11RSNAConfigPasswordValueTable  */

INT1
nmhGetFirstIndexDot11RSNAConfigPasswordValueTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11RSNAConfigPasswordValueTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11RSNAConfigPasswordCredential ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11RSNAConfigPasswordPeerMac ARG_LIST((UINT4 ,tMacAddr * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11RSNAConfigPasswordCredential ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11RSNAConfigPasswordPeerMac ARG_LIST((UINT4  ,tMacAddr ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11RSNAConfigPasswordCredential ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11RSNAConfigPasswordPeerMac ARG_LIST((UINT4 *  ,UINT4  ,tMacAddr ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11RSNAConfigPasswordValueTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11RSNAConfigDLCGroupTable. */
INT1
nmhValidateIndexInstanceDot11RSNAConfigDLCGroupTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11RSNAConfigDLCGroupTable  */

INT1
nmhGetFirstIndexDot11RSNAConfigDLCGroupTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11RSNAConfigDLCGroupTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11RSNAConfigDLCGroupIdentifier ARG_LIST((UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11RSNAConfigDLCGroupIdentifier ARG_LIST((UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11RSNAConfigDLCGroupIdentifier ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11RSNAConfigDLCGroupTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11OperationTable. */
INT1
nmhValidateIndexInstanceDot11OperationTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11OperationTable  */

INT1
nmhGetFirstIndexDot11OperationTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11OperationTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11MACAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetDot11RTSThreshold ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11ShortRetryLimit ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11LongRetryLimit ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11FragmentationThreshold ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MaxTransmitMSDULifetime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MaxReceiveLifetime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11ManufacturerID ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11ProductID ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11CAPLimit ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11HCCWmin ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11HCCWmax ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11HCCAIFSN ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11ADDBAResponseTimeout ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11ADDTSResponseTimeout ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11ChannelUtilizationBeaconInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11ScheduleTimeout ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11DLSResponseTimeout ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11QAPMissingAckRetryLimit ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11EDCAAveragingPeriod ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11HTProtection ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RIFSMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11PSMPControlledAccess ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ServiceIntervalGranularity ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11DualCTSProtection ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11LSIGTXOPFullProtectionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11NonGFEntitiesPresent ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11PCOActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11PCOFortyMaxDuration ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11PCOTwentyMaxDuration ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11PCOFortyMinDuration ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11PCOTwentyMinDuration ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11FortyMHzIntolerant ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11BSSWidthTriggerScanInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11BSSWidthChannelTransitionDelayFactor ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11OBSSScanPassiveDwell ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11OBSSScanActiveDwell ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11OBSSScanPassiveTotalPerChannel ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11OBSSScanActiveTotalPerChannel ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot112040BSSCoexistenceManagementSupport ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11OBSSScanActivityThreshold ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11RTSThreshold ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11ShortRetryLimit ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11LongRetryLimit ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11FragmentationThreshold ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MaxTransmitMSDULifetime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11MaxReceiveLifetime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11CAPLimit ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11HCCWmin ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11HCCWmax ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11HCCAIFSN ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11ADDBAResponseTimeout ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11ADDTSResponseTimeout ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11ChannelUtilizationBeaconInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11ScheduleTimeout ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11DLSResponseTimeout ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11QAPMissingAckRetryLimit ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11EDCAAveragingPeriod ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11HTProtection ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RIFSMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11PSMPControlledAccess ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11ServiceIntervalGranularity ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11DualCTSProtection ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11LSIGTXOPFullProtectionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11PCOActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11PCOFortyMaxDuration ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11PCOTwentyMaxDuration ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11PCOFortyMinDuration ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11PCOTwentyMinDuration ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11FortyMHzIntolerant ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11BSSWidthTriggerScanInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11BSSWidthChannelTransitionDelayFactor ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11OBSSScanPassiveDwell ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11OBSSScanActiveDwell ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11OBSSScanPassiveTotalPerChannel ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11OBSSScanActiveTotalPerChannel ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11OBSSScanActivityThreshold ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11RTSThreshold ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11ShortRetryLimit ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11LongRetryLimit ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11FragmentationThreshold ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MaxTransmitMSDULifetime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11MaxReceiveLifetime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11CAPLimit ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11HCCWmin ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11HCCWmax ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11HCCAIFSN ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11ADDBAResponseTimeout ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11ADDTSResponseTimeout ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11ChannelUtilizationBeaconInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11ScheduleTimeout ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11DLSResponseTimeout ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11QAPMissingAckRetryLimit ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11EDCAAveragingPeriod ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11HTProtection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RIFSMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11PSMPControlledAccess ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11ServiceIntervalGranularity ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11DualCTSProtection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11LSIGTXOPFullProtectionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11PCOActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11PCOFortyMaxDuration ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11PCOTwentyMaxDuration ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11PCOFortyMinDuration ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11PCOTwentyMinDuration ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11FortyMHzIntolerant ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11BSSWidthTriggerScanInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11BSSWidthChannelTransitionDelayFactor ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11OBSSScanPassiveDwell ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11OBSSScanActiveDwell ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11OBSSScanPassiveTotalPerChannel ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11OBSSScanActiveTotalPerChannel ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11OBSSScanActivityThreshold ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11OperationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11CountersTable. */
INT1
nmhValidateIndexInstanceDot11CountersTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11CountersTable  */

INT1
nmhGetFirstIndexDot11CountersTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11CountersTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11TransmittedFragmentCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11GroupTransmittedFrameCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11FailedCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RetryCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MultipleRetryCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11FrameDuplicateCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RTSSuccessCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RTSFailureCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11ACKFailureCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11ReceivedFragmentCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11GroupReceivedFrameCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11FCSErrorCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TransmittedFrameCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11WEPUndecryptableCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11QosDiscardedFragmentCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11AssociatedStationCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11QosCFPollsReceivedCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11QosCFPollsUnusedCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11QosCFPollsUnusableCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11QosCFPollsLostCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TransmittedAMSDUCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11FailedAMSDUCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RetryAMSDUCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MultipleRetryAMSDUCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TransmittedOctetsInAMSDUCount ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDot11AMSDUAckFailureCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11ReceivedAMSDUCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11ReceivedOctetsInAMSDUCount ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDot11TransmittedAMPDUCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TransmittedMPDUsInAMPDUCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TransmittedOctetsInAMPDUCount ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDot11AMPDUReceivedCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MPDUInReceivedAMPDUCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11ReceivedOctetsInAMPDUCount ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDot11AMPDUDelimiterCRCErrorCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11ImplicitBARFailureCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11ExplicitBARFailureCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11ChannelWidthSwitchCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TwentyMHzFrameTransmittedCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11FortyMHzFrameTransmittedCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TwentyMHzFrameReceivedCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11FortyMHzFrameReceivedCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11PSMPUTTGrantDuration ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11PSMPUTTUsedDuration ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11GrantedRDGUsedCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11GrantedRDGUnusedCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TransmittedFramesInGrantedRDGCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TransmittedOctetsInGrantedRDGCount ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDot11BeamformingFrameCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11DualCTSSuccessCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11DualCTSFailureCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11STBCCTSSuccessCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11STBCCTSFailureCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11nonSTBCCTSSuccessCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11nonSTBCCTSFailureCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RTSLSIGSuccessCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11RTSLSIGFailureCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11PBACErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11DeniedAssociationCounterDueToBSSLoad ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for Dot11GroupAddressesTable. */
INT1
nmhValidateIndexInstanceDot11GroupAddressesTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11GroupAddressesTable  */

INT1
nmhGetFirstIndexDot11GroupAddressesTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11GroupAddressesTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11Address ARG_LIST((INT4  , INT4 ,tMacAddr * ));

INT1
nmhGetDot11GroupAddressesStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11Address ARG_LIST((INT4  , INT4  ,tMacAddr ));

INT1
nmhSetDot11GroupAddressesStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11Address ARG_LIST((UINT4 *  ,INT4  , INT4  ,tMacAddr ));

INT1
nmhTestv2Dot11GroupAddressesStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11GroupAddressesTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11EDCATable. */
INT1
nmhValidateIndexInstanceDot11EDCATable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11EDCATable  */

INT1
nmhGetFirstIndexDot11EDCATable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11EDCATable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11EDCATableCWmin ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11EDCATableCWmax ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11EDCATableAIFSN ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11EDCATableTXOPLimit ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11EDCATableMSDULifetime ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11EDCATableMandatory ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11EDCATableCWmax ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetDot11EDCATableAIFSN ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetDot11EDCATableTXOPLimit ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetDot11EDCATableMSDULifetime ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetDot11EDCATableMandatory ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11EDCATableCWmax ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11EDCATableAIFSN ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11EDCATableTXOPLimit ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11EDCATableMSDULifetime ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11EDCATableMandatory ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11EDCATable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11QAPEDCATable. */
INT1
nmhValidateIndexInstanceDot11QAPEDCATable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11QAPEDCATable  */

INT1
nmhGetFirstIndexDot11QAPEDCATable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11QAPEDCATable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11QAPEDCATableCWmin ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11QAPEDCATableCWmax ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11QAPEDCATableAIFSN ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11QAPEDCATableTXOPLimit ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11QAPEDCATableMSDULifetime ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11QAPEDCATableMandatory ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11QAPEDCATableCWmin ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetDot11QAPEDCATableCWmax ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetDot11QAPEDCATableAIFSN ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetDot11QAPEDCATableTXOPLimit ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetDot11QAPEDCATableMSDULifetime ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetDot11QAPEDCATableMandatory ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11QAPEDCATableCWmin ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11QAPEDCATableCWmax ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11QAPEDCATableAIFSN ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11QAPEDCATableTXOPLimit ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11QAPEDCATableMSDULifetime ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11QAPEDCATableMandatory ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11QAPEDCATable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11QosCountersTable. */
INT1
nmhValidateIndexInstanceDot11QosCountersTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11QosCountersTable  */

INT1
nmhGetFirstIndexDot11QosCountersTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11QosCountersTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11QosTransmittedFragmentCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11QosFailedCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11QosRetryCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11QosMultipleRetryCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11QosFrameDuplicateCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11QosRTSSuccessCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11QosRTSFailureCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11QosACKFailureCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11QosReceivedFragmentCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11QosTransmittedFrameCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11QosDiscardedFrameCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11QosMPDUsReceivedCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetDot11QosRetriesReceivedCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11ResourceTypeIDName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for Dot11ResourceInfoTable. */
INT1
nmhValidateIndexInstanceDot11ResourceInfoTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11ResourceInfoTable  */

INT1
nmhGetFirstIndexDot11ResourceInfoTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11ResourceInfoTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11manufacturerOUI ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11manufacturerName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11manufacturerProductName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11manufacturerProductVersion ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for Dot11PhyOperationTable. */
INT1
nmhValidateIndexInstanceDot11PhyOperationTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11PhyOperationTable  */

INT1
nmhGetFirstIndexDot11PhyOperationTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11PhyOperationTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11PHYType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11CurrentRegDomain ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TempType ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for Dot11PhyAntennaTable. */
INT1
nmhValidateIndexInstanceDot11PhyAntennaTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11PhyAntennaTable  */

INT1
nmhGetFirstIndexDot11PhyAntennaTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11PhyAntennaTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11CurrentTxAntenna ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11DiversitySupportImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11CurrentRxAntenna ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11AntennaSelectionOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TransmitExplicitCSIFeedbackASOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TransmitIndicesFeedbackASOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ExplicitCSIFeedbackASOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TransmitIndicesComputationASOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ReceiveAntennaSelectionOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TransmitSoundingPPDUOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11NumberOfActiveRxAntennas ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11CurrentTxAntenna ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11CurrentTxAntenna ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11PhyAntennaTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11PhyTxPowerTable. */
INT1
nmhValidateIndexInstanceDot11PhyTxPowerTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11PhyTxPowerTable  */

INT1
nmhGetFirstIndexDot11PhyTxPowerTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11PhyTxPowerTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11NumberSupportedPowerLevelsImplemented ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TxPowerLevel1 ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TxPowerLevel2 ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TxPowerLevel3 ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TxPowerLevel4 ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TxPowerLevel5 ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TxPowerLevel6 ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TxPowerLevel7 ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TxPowerLevel8 ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11CurrentTxPowerLevel ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for Dot11PhyFHSSTable. */
INT1
nmhValidateIndexInstanceDot11PhyFHSSTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11PhyFHSSTable  */

INT1
nmhGetFirstIndexDot11PhyFHSSTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11PhyFHSSTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11HopTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11CurrentChannelNumber ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11MaxDwellTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11CurrentDwellTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11CurrentSet ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11CurrentPattern ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11CurrentIndex ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11EHCCPrimeRadix ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11EHCCNumberofChannelsFamilyIndex ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11EHCCCapabilityImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11EHCCCapabilityActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11HopAlgorithmAdopted ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RandomTableFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11NumberofHoppingSets ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11HopModulus ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11HopOffset ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11CurrentDwellTime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11CurrentSet ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11CurrentPattern ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11EHCCPrimeRadix ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11EHCCNumberofChannelsFamilyIndex ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11EHCCCapabilityActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11HopAlgorithmAdopted ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RandomTableFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11HopOffset ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11CurrentDwellTime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11CurrentSet ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11CurrentPattern ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11EHCCPrimeRadix ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11EHCCNumberofChannelsFamilyIndex ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11EHCCCapabilityActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11HopAlgorithmAdopted ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RandomTableFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11HopOffset ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11PhyFHSSTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11PhyDSSSTable. */
INT1
nmhValidateIndexInstanceDot11PhyDSSSTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11PhyDSSSTable  */

INT1
nmhGetFirstIndexDot11PhyDSSSTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11PhyDSSSTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11CurrentChannel ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11CCAModeSupported ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11CurrentCCAMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11EDThreshold ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11CurrentCCAMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11EDThreshold ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11CurrentCCAMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11EDThreshold ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11PhyDSSSTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11PhyIRTable. */
INT1
nmhValidateIndexInstanceDot11PhyIRTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11PhyIRTable  */

INT1
nmhGetFirstIndexDot11PhyIRTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11PhyIRTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11CCAWatchdogTimerMax ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11CCAWatchdogCountMax ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11CCAWatchdogTimerMin ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11CCAWatchdogCountMin ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11CCAWatchdogTimerMax ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11CCAWatchdogCountMax ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11CCAWatchdogTimerMin ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11CCAWatchdogCountMin ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11CCAWatchdogTimerMax ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11CCAWatchdogCountMax ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11CCAWatchdogTimerMin ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11CCAWatchdogCountMin ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11PhyIRTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11RegDomainsSupportedTable. */
INT1
nmhValidateIndexInstanceDot11RegDomainsSupportedTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11RegDomainsSupportedTable  */

INT1
nmhGetFirstIndexDot11RegDomainsSupportedTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11RegDomainsSupportedTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11RegDomainsImplementedValue ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for Dot11AntennasListTable. */
INT1
nmhValidateIndexInstanceDot11AntennasListTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11AntennasListTable  */

INT1
nmhGetFirstIndexDot11AntennasListTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11AntennasListTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11TxAntennaImplemented ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetDot11RxAntennaImplemented ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetDot11DiversitySelectionRxImplemented ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for Dot11SupportedDataRatesTxTable. */
INT1
nmhValidateIndexInstanceDot11SupportedDataRatesTxTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11SupportedDataRatesTxTable  */

INT1
nmhGetFirstIndexDot11SupportedDataRatesTxTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11SupportedDataRatesTxTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11ImplementedDataRatesTxValue ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for Dot11SupportedDataRatesRxTable. */
INT1
nmhValidateIndexInstanceDot11SupportedDataRatesRxTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11SupportedDataRatesRxTable  */

INT1
nmhGetFirstIndexDot11SupportedDataRatesRxTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11SupportedDataRatesRxTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11ImplementedDataRatesRxValue ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for Dot11PhyOFDMTable. */
INT1
nmhValidateIndexInstanceDot11PhyOFDMTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11PhyOFDMTable  */

INT1
nmhGetFirstIndexDot11PhyOFDMTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11PhyOFDMTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11CurrentFrequency ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TIThreshold ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11FrequencyBandsImplemented ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11ChannelStartingFactor ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11FiveMHzOperationImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TenMHzOperationImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TwentyMHzOperationImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11PhyOFDMChannelWidth ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11OFDMCCAEDImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11OFDMCCAEDRequired ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11OFDMEDThreshold ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11STATransmitPowerClass ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ACRType ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11TIThreshold ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11ChannelStartingFactor ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11OFDMEDThreshold ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11STATransmitPowerClass ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11TIThreshold ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11ChannelStartingFactor ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11OFDMEDThreshold ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11STATransmitPowerClass ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11PhyOFDMTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11PhyHRDSSSTable. */
INT1
nmhValidateIndexInstanceDot11PhyHRDSSSTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11PhyHRDSSSTable  */

INT1
nmhGetFirstIndexDot11PhyHRDSSSTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11PhyHRDSSSTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11ShortPreambleOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11PBCCOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ChannelAgilityPresent ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ChannelAgilityActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11HRCCAModeImplemented ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11ChannelAgilityActivated ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11ChannelAgilityActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11PhyHRDSSSTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11HoppingPatternTable. */
INT1
nmhValidateIndexInstanceDot11HoppingPatternTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11HoppingPatternTable  */

INT1
nmhGetFirstIndexDot11HoppingPatternTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11HoppingPatternTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11RandomTableFieldNumber ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for Dot11PhyERPTable. */
INT1
nmhValidateIndexInstanceDot11PhyERPTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11PhyERPTable  */

INT1
nmhGetFirstIndexDot11PhyERPTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11PhyERPTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11ERPPBCCOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ERPBCCOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11DSSSOFDMOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11DSSSOFDMOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ShortSlotTimeOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ShortSlotTimeOptionActivated ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11ERPBCCOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11DSSSOFDMOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11ShortSlotTimeOptionActivated ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11ERPBCCOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11DSSSOFDMOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11ShortSlotTimeOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11PhyERPTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11PhyHTTable. */
INT1
nmhValidateIndexInstanceDot11PhyHTTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11PhyHTTable  */

INT1
nmhGetFirstIndexDot11PhyHTTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11PhyHTTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11FortyMHzOperationImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11FortyMHzOperationActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11CurrentPrimaryChannel ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11CurrentSecondaryChannel ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11NumberOfSpatialStreamsImplemented ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11NumberOfSpatialStreamsActivated ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11HTGreenfieldOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11HTGreenfieldOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ShortGIOptionInTwentyImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ShortGIOptionInTwentyActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ShortGIOptionInFortyImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ShortGIOptionInFortyActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11LDPCCodingOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11LDPCCodingOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TxSTBCOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TxSTBCOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RxSTBCOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11RxSTBCOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11BeamFormingOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11BeamFormingOptionActivated ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11HighestSupportedDataRate ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TxMCSSetDefined ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TxRxMCSSetNotEqual ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TxMaximumNumberSpatialStreamsSupported ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11TxUnequalModulationSupported ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11FortyMHzOperationActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11NumberOfSpatialStreamsActivated ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetDot11HTGreenfieldOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11ShortGIOptionInTwentyActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11ShortGIOptionInFortyActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11LDPCCodingOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11TxSTBCOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11RxSTBCOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11BeamFormingOptionActivated ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot11TxMCSSetDefined ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11FortyMHzOperationActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11NumberOfSpatialStreamsActivated ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Dot11HTGreenfieldOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11ShortGIOptionInTwentyActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11ShortGIOptionInFortyActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11LDPCCodingOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11TxSTBCOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11RxSTBCOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11BeamFormingOptionActivated ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot11TxMCSSetDefined ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11PhyHTTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11SupportedMCSTxTable. */
INT1
nmhValidateIndexInstanceDot11SupportedMCSTxTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11SupportedMCSTxTable  */

INT1
nmhGetFirstIndexDot11SupportedMCSTxTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11SupportedMCSTxTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11SupportedMCSTxValue ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for Dot11SupportedMCSRxTable. */
INT1
nmhValidateIndexInstanceDot11SupportedMCSRxTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11SupportedMCSRxTable  */

INT1
nmhGetFirstIndexDot11SupportedMCSRxTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11SupportedMCSRxTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11SupportedMCSRxValue ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for Dot11TransmitBeamformingConfigTable. */
INT1
nmhValidateIndexInstanceDot11TransmitBeamformingConfigTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11TransmitBeamformingConfigTable  */

INT1
nmhGetFirstIndexDot11TransmitBeamformingConfigTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11TransmitBeamformingConfigTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11ReceiveStaggerSoundingOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TransmitStaggerSoundingOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ReceiveNDPOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11TransmitNDPOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ImplicitTransmitBeamformingOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11CalibrationOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ExplicitCSITransmitBeamformingOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ExplicitNonCompressedBeamformingMatrixOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ExplicitTransmitBeamformingCSIFeedbackOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ExplicitNonCompressedBeamformingFeedbackOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11ExplicitCompressedBeamformingFeedbackOptionImplemented ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot11NumberBeamFormingCSISupportAntenna ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11NumberNonCompressedBeamformingMatrixSupportAntenna ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot11NumberCompressedBeamformingMatrixSupportAntenna ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for Dot11BSSIdTable. */
INT1
nmhValidateIndexInstanceDot11BSSIdTable ARG_LIST((tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for Dot11BSSIdTable  */

INT1
nmhGetFirstIndexDot11BSSIdTable ARG_LIST((tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11BSSIdTable ARG_LIST((tMacAddr , tMacAddr * ));

/* Proto Validate Index Instance for Dot11InterworkingTable. */
INT1
nmhValidateIndexInstanceDot11InterworkingTable ARG_LIST((tMacAddr  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for Dot11InterworkingTable  */

INT1
nmhGetFirstIndexDot11InterworkingTable ARG_LIST((tMacAddr *  , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11InterworkingTable ARG_LIST((tMacAddr , tMacAddr *  , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11NonAPStationUserIdentity ARG_LIST((tMacAddr  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11NonAPStationInterworkingCapability ARG_LIST((tMacAddr  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11NonAPStationAssociatedSSID ARG_LIST((tMacAddr  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11NonAPStationUnicastCipherSuite ARG_LIST((tMacAddr  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11NonAPStationBroadcastCipherSuite ARG_LIST((tMacAddr  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11NonAPStationAuthAccessCategories ARG_LIST((tMacAddr  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11NonAPStationAuthMaxVoiceRate ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationAuthMaxVideoRate ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationAuthMaxBestEffortRate ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationAuthMaxBackgroundRate ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationAuthMaxVoiceOctets ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationAuthMaxVideoOctets ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationAuthMaxBestEffortOctets ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationAuthMaxBackgroundOctets ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationAuthMaxHCCAHEMMOctets ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationAuthMaxTotalOctets ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationAuthHCCAHEMM ARG_LIST((tMacAddr  , tMacAddr ,INT4 *));

INT1
nmhGetDot11NonAPStationAuthMaxHCCAHEMMRate ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationAuthHCCAHEMMDelay ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationAuthSourceMulticast ARG_LIST((tMacAddr  , tMacAddr ,INT4 *));

INT1
nmhGetDot11NonAPStationAuthMaxSourceMulticastRate ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationVoiceMSDUCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationDroppedVoiceMSDUCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationVoiceOctetCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationDroppedVoiceOctetCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationVideoMSDUCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationDroppedVideoMSDUCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationVideoOctetCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationDroppedVideoOctetCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationBestEffortMSDUCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationDroppedBestEffortMSDUCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationBestEffortOctetCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationDroppedBestEffortOctetCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationBackgroundMSDUCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationDroppedBackgroundMSDUCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationBackgroundOctetCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationDroppedBackgroundOctetCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationHCCAHEMMMSDUCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationDroppedHCCAHEMMMSDUCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationHCCAHEMMOctetCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationDroppedHCCAHEMMOctetCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationMulticastMSDUCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationDroppedMulticastMSDUCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationMulticastOctetCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationDroppedMulticastOctetCount ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationPowerManagementMode ARG_LIST((tMacAddr  , tMacAddr ,INT4 *));

INT1
nmhGetDot11NonAPStationAuthDls ARG_LIST((tMacAddr  , tMacAddr ,INT4 *));

INT1
nmhGetDot11NonAPStationVLANId ARG_LIST((tMacAddr  , tMacAddr ,UINT4 *));

INT1
nmhGetDot11NonAPStationVLANName ARG_LIST((tMacAddr  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11NonAPStationAddtsResultCode ARG_LIST((tMacAddr  , tMacAddr ,INT4 *));

/* Proto Validate Index Instance for Dot11APLCITable. */
INT1
nmhValidateIndexInstanceDot11APLCITable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11APLCITable  */

INT1
nmhGetFirstIndexDot11APLCITable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11APLCITable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11APLCILatitudeResolution ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11APLCILatitudeInteger ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11APLCILatitudeFraction ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11APLCILongitudeResolution ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11APLCILongitudeInteger ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11APLCILongitudeFraction ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11APLCIAltitudeType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11APLCIAltitudeResolution ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11APLCIAltitudeInteger ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11APLCIAltitudeFraction ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11APLCIDatum ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11APLCIAzimuthType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11APLCIAzimuthResolution ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11APLCIAzimuth ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11APLCILatitudeResolution ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11APLCILatitudeInteger ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11APLCILatitudeFraction ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11APLCILongitudeResolution ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11APLCILongitudeInteger ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11APLCILongitudeFraction ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11APLCIAltitudeType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11APLCIAltitudeResolution ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11APLCIAltitudeInteger ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11APLCIAltitudeFraction ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11APLCIDatum ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11APLCIAzimuthType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11APLCIAzimuthResolution ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11APLCIAzimuth ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11APLCILatitudeResolution ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11APLCILatitudeInteger ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11APLCILatitudeFraction ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11APLCILongitudeResolution ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11APLCILongitudeInteger ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11APLCILongitudeFraction ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11APLCIAltitudeType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11APLCIAltitudeResolution ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11APLCIAltitudeInteger ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11APLCIAltitudeFraction ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11APLCIDatum ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11APLCIAzimuthType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11APLCIAzimuthResolution ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11APLCIAzimuth ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11APLCITable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11APCivicLocationTable. */
INT1
nmhValidateIndexInstanceDot11APCivicLocationTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11APCivicLocationTable  */

INT1
nmhGetFirstIndexDot11APCivicLocationTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11APCivicLocationTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11APCivicLocationCountry ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationA1 ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationA2 ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationA3 ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationA4 ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationA5 ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationA6 ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationPrd ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationPod ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationSts ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationHno ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationHns ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationLmk ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationLoc ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationNam ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationPc ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationBld ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationUnit ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationFlr ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationRoom ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationPlc ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationPcn ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationPobox ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationAddcode ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationSeat ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationRd ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationRdsec ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationRdbr ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationRdsubbr ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationPrm ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11APCivicLocationPom ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11APCivicLocationCountry ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationA1 ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationA2 ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationA3 ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationA4 ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationA5 ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationA6 ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationPrd ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationPod ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationSts ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationHno ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationHns ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationLmk ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationLoc ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationNam ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationPc ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationBld ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationUnit ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationFlr ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationRoom ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationPlc ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationPcn ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationPobox ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationAddcode ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationSeat ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationRd ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationRdsec ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationRdbr ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationRdsubbr ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationPrm ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11APCivicLocationPom ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11APCivicLocationCountry ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationA1 ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationA2 ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationA3 ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationA4 ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationA5 ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationA6 ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationPrd ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationPod ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationSts ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationHno ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationHns ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationLmk ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationLoc ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationNam ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationPc ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationBld ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationUnit ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationFlr ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationRoom ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationPlc ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationPcn ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationPobox ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationAddcode ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationSeat ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationRd ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationRdsec ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationRdbr ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationRdsubbr ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationPrm ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11APCivicLocationPom ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11APCivicLocationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11RoamingConsortiumTable. */
INT1
nmhValidateIndexInstanceDot11RoamingConsortiumTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Dot11RoamingConsortiumTable  */

INT1
nmhGetFirstIndexDot11RoamingConsortiumTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11RoamingConsortiumTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11RoamingConsortiumRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11RoamingConsortiumRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11RoamingConsortiumRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11RoamingConsortiumTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11DomainNameTable. */
INT1
nmhValidateIndexInstanceDot11DomainNameTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Dot11DomainNameTable  */

INT1
nmhGetFirstIndexDot11DomainNameTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11DomainNameTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11DomainName ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11DomainNameRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11DomainName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot11DomainNameRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11DomainName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot11DomainNameRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11DomainNameTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11GASAdvertisementTable. */
INT1
nmhValidateIndexInstanceDot11GASAdvertisementTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot11GASAdvertisementTable  */

INT1
nmhGetFirstIndexDot11GASAdvertisementTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11GASAdvertisementTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11GASPauseForServerResponse ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDot11GASResponseTimeout ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11GASComebackDelay ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11GASResponseBufferingTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11GASQueryResponseLengthLimit ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11GASQueries ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11GASQueryRate ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11GASResponses ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11GASResponseRate ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11GASTransmittedFragmentCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11GASReceivedFragmentCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11GASNoRequestOutstanding ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11GASResponsesDiscarded ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDot11GASFailedResponses ARG_LIST((UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11GASPauseForServerResponse ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDot11GASResponseTimeout ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11GASComebackDelay ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11GASResponseBufferingTime ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDot11GASQueryResponseLengthLimit ARG_LIST((UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11GASPauseForServerResponse ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Dot11GASResponseTimeout ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11GASComebackDelay ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11GASResponseBufferingTime ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Dot11GASQueryResponseLengthLimit ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11GASAdvertisementTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11MACStateConfigTable. */
INT1
nmhValidateIndexInstanceDot11MACStateConfigTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for Dot11MACStateConfigTable  */

INT1
nmhGetFirstIndexDot11MACStateConfigTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11MACStateConfigTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11ESSDisconnectFilterInterval ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetDot11ESSLinkDetectionHoldInterval ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11ESSDisconnectFilterInterval ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhSetDot11ESSLinkDetectionHoldInterval ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11ESSDisconnectFilterInterval ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhTestv2Dot11ESSLinkDetectionHoldInterval ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11MACStateConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11MACStateParameterTable. */
INT1
nmhValidateIndexInstanceDot11MACStateParameterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for Dot11MACStateParameterTable  */

INT1
nmhGetFirstIndexDot11MACStateParameterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11MACStateParameterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11ESSLinkDownTimeInterval ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetDot11ESSLinkRssiDataThreshold ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetDot11ESSLinkRssiBeaconThreshold ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetDot11ESSLinkBeaconSnrThreshold ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetDot11ESSLinkDataSnrThreshold ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetDot11ESSLinkBeaconFrameErrorRateThresholdInteger ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetDot11ESSLinkBeaconFrameErrorRateThresholdFraction ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetDot11ESSLinkBeaconFrameErrorRateThresholdExponent ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetDot11ESSLinkFrameErrorRateThresholdInteger ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetDot11ESSLinkFrameErrorRateThresholdFraction ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetDot11ESSLinkFrameErrorRateThresholdExponent ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetDot11PeakOperationalRate ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetDot11MinimumOperationalRate ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetDot11ESSLinkDataThroughputInteger ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetDot11ESSLinkDataThroughputFraction ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetDot11ESSLinkDataThroughputExponent ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot11ESSLinkDownTimeInterval ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhSetDot11ESSLinkRssiDataThreshold ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhSetDot11ESSLinkRssiBeaconThreshold ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhSetDot11ESSLinkBeaconSnrThreshold ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhSetDot11ESSLinkDataSnrThreshold ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhSetDot11ESSLinkBeaconFrameErrorRateThresholdInteger ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhSetDot11ESSLinkBeaconFrameErrorRateThresholdFraction ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhSetDot11ESSLinkBeaconFrameErrorRateThresholdExponent ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhSetDot11ESSLinkFrameErrorRateThresholdInteger ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhSetDot11ESSLinkFrameErrorRateThresholdFraction ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhSetDot11ESSLinkFrameErrorRateThresholdExponent ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhSetDot11PeakOperationalRate ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhSetDot11MinimumOperationalRate ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhSetDot11ESSLinkDataThroughputInteger ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhSetDot11ESSLinkDataThroughputFraction ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhSetDot11ESSLinkDataThroughputExponent ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot11ESSLinkDownTimeInterval ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhTestv2Dot11ESSLinkRssiDataThreshold ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhTestv2Dot11ESSLinkRssiBeaconThreshold ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhTestv2Dot11ESSLinkBeaconSnrThreshold ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhTestv2Dot11ESSLinkDataSnrThreshold ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhTestv2Dot11ESSLinkBeaconFrameErrorRateThresholdInteger ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhTestv2Dot11ESSLinkBeaconFrameErrorRateThresholdFraction ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhTestv2Dot11ESSLinkBeaconFrameErrorRateThresholdExponent ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhTestv2Dot11ESSLinkFrameErrorRateThresholdInteger ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhTestv2Dot11ESSLinkFrameErrorRateThresholdFraction ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhTestv2Dot11ESSLinkFrameErrorRateThresholdExponent ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhTestv2Dot11PeakOperationalRate ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhTestv2Dot11MinimumOperationalRate ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhTestv2Dot11ESSLinkDataThroughputInteger ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhTestv2Dot11ESSLinkDataThroughputFraction ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

INT1
nmhTestv2Dot11ESSLinkDataThroughputExponent ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tMacAddr  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Dot11MACStateParameterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Dot11MACStateESSLinkDetectedTable. */
INT1
nmhValidateIndexInstanceDot11MACStateESSLinkDetectedTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for Dot11MACStateESSLinkDetectedTable  */

INT1
nmhGetFirstIndexDot11MACStateESSLinkDetectedTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot11MACStateESSLinkDetectedTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot11ESSLinkDetectedNetworkId ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot11ESSLinkDetectedNetworkDetectTime ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetDot11ESSLinkDetectedNetworkModifiedTime ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,UINT4 *));

INT1
nmhGetDot11ESSLinkDetectedNetworkMIHCapabilities ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));
