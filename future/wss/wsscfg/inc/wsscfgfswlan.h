/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: wsscfgfswlan.h,v 1.2 2017/11/24 10:37:07 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSWLANDB_H
#define _FSWLANDB_H

UINT1 FsDot11StationConfigTableINDEX[] = { SNMP_DATA_TYPE_INTEGER };
UINT1 FsDot11CapabilityProfileTableINDEX[] =
    { SNMP_DATA_TYPE_OCTET_PRIM };
UINT1 FsDot11AuthenticationProfileTableINDEX[] =
    { SNMP_DATA_TYPE_OCTET_PRIM };
UINT1 FsSecurityWebAuthParamsTableINDEX[] =
    { SNMP_DATA_TYPE_INTEGER };
UINT1 FsSecurityWebAuthCredentialsTableINDEX[] =
    { SNMP_DATA_TYPE_INTEGER };
UINT1 FsStationQosParamsTableINDEX[] =
    { SNMP_DATA_TYPE_INTEGER, SNMP_FIXED_LENGTH_OCTET_STRING, 6 };
UINT1 FsDot11QAPTableINDEX[] = { SNMP_DATA_TYPE_INTEGER };
UINT1 FsDot11AntennasListTableINDEX[] =
    { SNMP_DATA_TYPE_INTEGER, SNMP_DATA_TYPE_INTEGER32 };
UINT1 FsVlanIsolationTableINDEX[] = { SNMP_DATA_TYPE_INTEGER };
UINT1 FsDot11RadioConfigTableINDEX[] = { SNMP_DATA_TYPE_INTEGER };
UINT1 FsDot11QosProfileTableINDEX[] = { SNMP_DATA_TYPE_OCTET_PRIM };

UINT4 fswlan[] = { 1, 3, 6, 1, 4, 1, 2076, 102 };
tSNMP_OID_TYPE fswlanOID = { 8, fswlan };


UINT4 FsDot11aNetworkEnable[] = { 1, 3, 6, 1, 4, 1, 2076, 102, 1, 1 };
UINT4 FsDot11bNetworkEnable[] = { 1, 3, 6, 1, 4, 1, 2076, 102, 1, 2 };
UINT4 FsDot11gSupport[] = { 1, 3, 6, 1, 4, 1, 2076, 102, 1, 3 };
UINT4 FsDot11anSupport[] = { 1, 3, 6, 1, 4, 1, 2076, 102, 1, 4 };
UINT4 FsDot11bnSupport[] = { 1, 3, 6, 1, 4, 1, 2076, 102, 1, 5 };
UINT4 FsEncryptDecryptCapab[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 1, 1, 1 };
UINT4 FsDot11SupressSSID[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 1, 1, 2 };
UINT4 FsDot11VlanId[] = { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 1, 1, 3 };
UINT4 FsDot11BandwidthThresh[] = { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 1, 1, 4 };
UINT4 FsDot11CapabilityProfileName[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 2, 1, 1 };
UINT4 FsDot11CFPollable[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 2, 1, 2 };
UINT4 FsDot11CFPollRequest[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 2, 1, 3 };
UINT4 FsDot11PrivacyOptionImplemented[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 2, 1, 4 };
UINT4 FsDot11ShortPreambleOptionImplemented[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 2, 1, 5 };
UINT4 FsDot11PBCCOptionImplemented[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 2, 1, 6 };
UINT4 FsDot11ChannelAgilityPresent[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 2, 1, 7 };
UINT4 FsDot11QosOptionImplemented[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 2, 1, 8 };
UINT4 FsDot11SpectrumManagementRequired[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 2, 1, 9 };
UINT4 FsDot11ShortSlotTimeOptionImplemented[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 2, 1, 10 };
UINT4 FsDot11APSDOptionImplemented[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 2, 1, 11 };
UINT4 FsDot11DSSSOFDMOptionEnabled[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 2, 1, 12 };
UINT4 FsDot11DelayedBlockAckOptionImplemented[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 2, 1, 13 };
UINT4 FsDot11ImmediateBlockAckOptionImplemented[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 2, 1, 14 };
UINT4 FsDot11QAckOptionImplemented[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 2, 1, 15 };
UINT4 FsDot11QueueRequestOptionImplemented[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 2, 1, 16 };
UINT4 FsDot11TXOPRequestOptionImplemented[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 2, 1, 17 };
UINT4 FsDot11RSNAOptionImplemented[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 2, 1, 18 };
UINT4 FsDot11RSNAPreauthenticationImplemented[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 2, 1, 19 };
UINT4 FsDot11CapabilityRowStatus[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 2, 1, 20 };
UINT4 FsDot11AuthenticationProfileName[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 3, 1, 1 };
UINT4 FsDot11AuthenticationAlgorithm[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 3, 1, 2 };
UINT4 FsDot11WepKeyIndex[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 3, 1, 3 };
UINT4 FsDot11WepKey[] = { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 3, 1, 4 };
UINT4 FsDot11AuthenticationRowStatus[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 3, 1, 5 };
UINT4 FsSecurityWebAuthType[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 4, 1, 1 };
UINT4 FsSecurityWebAuthUrl[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 4, 1, 2 };
UINT4 FsSecurityWebAuthRedirectUrl[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 4, 1, 3 };
UINT4 FsSecurityWebAddr[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 4, 1, 4 };
UINT4 FsSecurityWebAuthMode[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 4, 1, 5 };
UINT4 FsSecurityWebAuthMaxDbEntry[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 4, 1, 6 };
UINT4 FsSecurityWebAuthWebTitle[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 4, 1, 7 };
UINT4 FsSecurityWebAuthWebMessage[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 4, 1, 8 };
UINT4 FsSecurityWebAuthWebLogo[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 4, 1, 9 };
UINT4 FsSecurityWebAuthUName[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 5, 1, 1 };
UINT4 FsSecurityWebAuthPasswd[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 5, 1, 2 };
UINT4 FsSecurityWebAuthUserLifetime[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 5, 1, 3 };
UINT4 FsSecurityWebAuthUserEmailId[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 5, 1, 4 };
UINT4 FsStaMacAddress[] = { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 6, 1, 1 };
UINT4 FsStaQoSPriority[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 6, 1, 2 };
UINT4 FsStaQoSDscp[] = { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 6, 1, 3 };
UINT4 FsDot11TaggingPolicy[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 3, 1, 1, 1 };
UINT4 FsDot11QueueDepth[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 3, 1, 1, 2 };
UINT4 FsDot11PriorityValue[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 3, 1, 1, 3 };
UINT4 FsDot11DscpValue[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 3, 1, 1, 4 };
UINT4 FsAntennaMode[] = { 1, 3, 6, 1, 4, 1, 2076, 102, 4, 1, 1, 1 };
UINT4 FsAntennaSelection[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 4, 1, 1, 2 };
UINT4 FsVlanIsolation[] = { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 7, 1, 1 };
UINT4 FsDot11RadioType[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 8, 1, 1 };
UINT4 FsDot11RadioNoOfBssIdSupported[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 8, 1, 2 };
UINT4 FsDot11RadioAntennaType[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 8, 1, 3 };
UINT4 FsDot11RadioFailureStatus[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 8, 1, 4 };
UINT4 FsDot11QosProfileName[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 9, 1, 1 };
UINT4 FsDot11QosTraffic[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 9, 1, 2 };
UINT4 FsDot11QosPassengerTrustMode[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 9, 1, 3 };
UINT4 FsDot11QosRowStatus[] =
    { 1, 3, 6, 1, 4, 1, 2076, 102, 2, 9, 1, 5 };




tMbDbEntry fswlanMibEntry[] = {

    {{10, FsDot11aNetworkEnable}, NULL, FsDot11aNetworkEnableGet,
     FsDot11aNetworkEnableSet, FsDot11aNetworkEnableTest,
     FsDot11aNetworkEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
     NULL, 0, 0, 0, "1"},

    {{10, FsDot11bNetworkEnable}, NULL, FsDot11bNetworkEnableGet,
     FsDot11bNetworkEnableSet, FsDot11bNetworkEnableTest,
     FsDot11bNetworkEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
     NULL, 0, 0, 0, "1"},

    {{10, FsDot11gSupport}, NULL, FsDot11gSupportGet,
     FsDot11gSupportSet,
     FsDot11gSupportTest, FsDot11gSupportDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE, NULL, 0, 0, 0, "1"},

    {{10, FsDot11anSupport}, NULL, FsDot11anSupportGet,
     FsDot11anSupportSet, FsDot11anSupportTest, FsDot11anSupportDep,
     SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

    {{10, FsDot11bnSupport}, NULL, FsDot11bnSupportGet,
     FsDot11bnSupportSet, FsDot11bnSupportTest, FsDot11bnSupportDep,
     SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

    {{12, FsEncryptDecryptCapab},
     GetNextIndexFsDot11StationConfigTable,
     FsEncryptDecryptCapabGet, FsEncryptDecryptCapabSet,
     FsEncryptDecryptCapabTest, FsDot11StationConfigTableDep,
     SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
     FsDot11StationConfigTableINDEX, 1, 0, 0, NULL},

    {{12, FsDot11SupressSSID}, GetNextIndexFsDot11StationConfigTable,
     FsDot11SupressSSIDGet, FsDot11SupressSSIDSet,
     FsDot11SupressSSIDTest,
     FsDot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE,
     FsDot11StationConfigTableINDEX, 1, 0, 0, NULL},

    {{12, FsDot11VlanId}, GetNextIndexFsDot11StationConfigTable,
     FsDot11VlanIdGet, FsDot11VlanIdSet, FsDot11VlanIdTest,
     FsDot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER32,
     SNMP_READWRITE, FsDot11StationConfigTableINDEX, 1, 0, 0, NULL},

    {{12, FsDot11BandwidthThresh}, GetNextIndexFsDot11StationConfigTable,
     FsDot11BandwidthThreshGet, FsDot11BandwidthThreshSet, FsDot11BandwidthThreshTest,
     FsDot11StationConfigTableDep, SNMP_DATA_TYPE_INTEGER32,
     SNMP_READWRITE, FsDot11StationConfigTableINDEX, 1, 0, 0, NULL},

    {{12, FsDot11CapabilityProfileName},
     GetNextIndexFsDot11CapabilityProfileTable, NULL, NULL, NULL,
     NULL,
     SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS,
     FsDot11CapabilityProfileTableINDEX, 1, 0, 0, NULL},

    {{12, FsDot11CFPollable},
     GetNextIndexFsDot11CapabilityProfileTable,
     FsDot11CFPollableGet, FsDot11CFPollableSet,
     FsDot11CFPollableTest,
     FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0,
     NULL},

    {{12, FsDot11CFPollRequest},
     GetNextIndexFsDot11CapabilityProfileTable,
     FsDot11CFPollRequestGet,
     FsDot11CFPollRequestSet, FsDot11CFPollRequestTest,
     FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0,
     NULL},

    {{12, FsDot11PrivacyOptionImplemented},
     GetNextIndexFsDot11CapabilityProfileTable,
     FsDot11PrivacyOptionImplementedGet,
     FsDot11PrivacyOptionImplementedSet,
     FsDot11PrivacyOptionImplementedTest,
     FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0,
     NULL},

    {{12, FsDot11ShortPreambleOptionImplemented},
     GetNextIndexFsDot11CapabilityProfileTable,
     FsDot11ShortPreambleOptionImplementedGet,
     FsDot11ShortPreambleOptionImplementedSet,
     FsDot11ShortPreambleOptionImplementedTest,
     FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0,
     NULL},

    {{12, FsDot11PBCCOptionImplemented},
     GetNextIndexFsDot11CapabilityProfileTable,
     FsDot11PBCCOptionImplementedGet, FsDot11PBCCOptionImplementedSet,
     FsDot11PBCCOptionImplementedTest,
     FsDot11CapabilityProfileTableDep,
     SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
     FsDot11CapabilityProfileTableINDEX, 1, 0, 0, NULL},

    {{12, FsDot11ChannelAgilityPresent},
     GetNextIndexFsDot11CapabilityProfileTable,
     FsDot11ChannelAgilityPresentGet, FsDot11ChannelAgilityPresentSet,
     FsDot11ChannelAgilityPresentTest,
     FsDot11CapabilityProfileTableDep,
     SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
     FsDot11CapabilityProfileTableINDEX, 1, 0, 0, NULL},

    {{12, FsDot11QosOptionImplemented},
     GetNextIndexFsDot11CapabilityProfileTable,
     FsDot11QosOptionImplementedGet, FsDot11QosOptionImplementedSet,
     FsDot11QosOptionImplementedTest,
     FsDot11CapabilityProfileTableDep,
     SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
     FsDot11CapabilityProfileTableINDEX, 1, 0, 0, NULL},

    {{12, FsDot11SpectrumManagementRequired},
     GetNextIndexFsDot11CapabilityProfileTable,
     FsDot11SpectrumManagementRequiredGet,
     FsDot11SpectrumManagementRequiredSet,
     FsDot11SpectrumManagementRequiredTest,
     FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0,
     NULL},

    {{12, FsDot11ShortSlotTimeOptionImplemented},
     GetNextIndexFsDot11CapabilityProfileTable,
     FsDot11ShortSlotTimeOptionImplementedGet,
     FsDot11ShortSlotTimeOptionImplementedSet,
     FsDot11ShortSlotTimeOptionImplementedTest,
     FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0,
     NULL},

    {{12, FsDot11APSDOptionImplemented},
     GetNextIndexFsDot11CapabilityProfileTable,
     FsDot11APSDOptionImplementedGet, FsDot11APSDOptionImplementedSet,
     FsDot11APSDOptionImplementedTest,
     FsDot11CapabilityProfileTableDep,
     SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
     FsDot11CapabilityProfileTableINDEX, 1, 0, 0, NULL},

    {{12, FsDot11DSSSOFDMOptionEnabled},
     GetNextIndexFsDot11CapabilityProfileTable,
     FsDot11DSSSOFDMOptionEnabledGet, FsDot11DSSSOFDMOptionEnabledSet,
     FsDot11DSSSOFDMOptionEnabledTest,
     FsDot11CapabilityProfileTableDep,
     SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
     FsDot11CapabilityProfileTableINDEX, 1, 0, 0, NULL},

    {{12, FsDot11DelayedBlockAckOptionImplemented},
     GetNextIndexFsDot11CapabilityProfileTable,
     FsDot11DelayedBlockAckOptionImplementedGet,
     FsDot11DelayedBlockAckOptionImplementedSet,
     FsDot11DelayedBlockAckOptionImplementedTest,
     FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0,
     NULL},

    {{12, FsDot11ImmediateBlockAckOptionImplemented},
     GetNextIndexFsDot11CapabilityProfileTable,
     FsDot11ImmediateBlockAckOptionImplementedGet,
     FsDot11ImmediateBlockAckOptionImplementedSet,
     FsDot11ImmediateBlockAckOptionImplementedTest,
     FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0,
     NULL},

    {{12, FsDot11QAckOptionImplemented},
     GetNextIndexFsDot11CapabilityProfileTable,
     FsDot11QAckOptionImplementedGet, FsDot11QAckOptionImplementedSet,
     FsDot11QAckOptionImplementedTest,
     FsDot11CapabilityProfileTableDep,
     SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
     FsDot11CapabilityProfileTableINDEX, 1, 0, 0, NULL},

    {{12, FsDot11QueueRequestOptionImplemented},
     GetNextIndexFsDot11CapabilityProfileTable,
     FsDot11QueueRequestOptionImplementedGet,
     FsDot11QueueRequestOptionImplementedSet,
     FsDot11QueueRequestOptionImplementedTest,
     FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0,
     NULL},

    {{12, FsDot11TXOPRequestOptionImplemented},
     GetNextIndexFsDot11CapabilityProfileTable,
     FsDot11TXOPRequestOptionImplementedGet,
     FsDot11TXOPRequestOptionImplementedSet,
     FsDot11TXOPRequestOptionImplementedTest,
     FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0,
     NULL},

    {{12, FsDot11RSNAOptionImplemented},
     GetNextIndexFsDot11CapabilityProfileTable,
     FsDot11RSNAOptionImplementedGet, FsDot11RSNAOptionImplementedSet,
     FsDot11RSNAOptionImplementedTest,
     FsDot11CapabilityProfileTableDep,
     SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
     FsDot11CapabilityProfileTableINDEX, 1, 0, 0, NULL},

    {{12, FsDot11RSNAPreauthenticationImplemented},
     GetNextIndexFsDot11CapabilityProfileTable,
     FsDot11RSNAPreauthenticationImplementedGet,
     FsDot11RSNAPreauthenticationImplementedSet,
     FsDot11RSNAPreauthenticationImplementedTest,
     FsDot11CapabilityProfileTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE, FsDot11CapabilityProfileTableINDEX, 1, 0, 0,
     NULL},

    {{12, FsDot11CapabilityRowStatus},
     GetNextIndexFsDot11CapabilityProfileTable,
     FsDot11CapabilityRowStatusGet, FsDot11CapabilityRowStatusSet,
     FsDot11CapabilityRowStatusTest,
     FsDot11CapabilityProfileTableDep,
     SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
     FsDot11CapabilityProfileTableINDEX, 1, 0, 1, NULL},

    {{12, FsDot11AuthenticationProfileName},
     GetNextIndexFsDot11AuthenticationProfileTable, NULL, NULL, NULL,
     NULL,
     SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS,
     FsDot11AuthenticationProfileTableINDEX, 1, 0, 0, NULL},

    {{12, FsDot11AuthenticationAlgorithm},
     GetNextIndexFsDot11AuthenticationProfileTable,
     FsDot11AuthenticationAlgorithmGet,
     FsDot11AuthenticationAlgorithmSet,
     FsDot11AuthenticationAlgorithmTest,
     FsDot11AuthenticationProfileTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE, FsDot11AuthenticationProfileTableINDEX, 1, 0, 0,
     NULL},

    {{12, FsDot11WepKeyIndex},
     GetNextIndexFsDot11AuthenticationProfileTable, NULL, NULL, NULL,
     NULL,
     SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS,
     FsDot11AuthenticationProfileTableINDEX, 1, 0, 0, NULL},

    {{12, FsDot11WepKey},
     GetNextIndexFsDot11AuthenticationProfileTable,
     FsDot11WepKeyGet, FsDot11WepKeySet, FsDot11WepKeyTest,
     FsDot11AuthenticationProfileTableDep, SNMP_DATA_TYPE_OCTET_PRIM,
     SNMP_READWRITE, FsDot11AuthenticationProfileTableINDEX, 1, 0, 0,
     NULL},

    {{12, FsDot11AuthenticationRowStatus},
     GetNextIndexFsDot11AuthenticationProfileTable,
     FsDot11AuthenticationRowStatusGet,
     FsDot11AuthenticationRowStatusSet,
     FsDot11AuthenticationRowStatusTest,
     FsDot11AuthenticationProfileTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE, FsDot11AuthenticationProfileTableINDEX, 1, 0, 1,
     NULL},

    {{12, FsSecurityWebAuthType},
     GetNextIndexFsSecurityWebAuthParamsTable,
     FsSecurityWebAuthTypeGet, FsSecurityWebAuthTypeSet,
     FsSecurityWebAuthTypeTest, FsSecurityWebAuthParamsTableDep,
     SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
     FsSecurityWebAuthParamsTableINDEX, 1, 0, 0, NULL},

    {{12, FsSecurityWebAuthUrl},
     GetNextIndexFsSecurityWebAuthParamsTable,
     FsSecurityWebAuthUrlGet, FsSecurityWebAuthUrlSet,
     FsSecurityWebAuthUrlTest, FsSecurityWebAuthParamsTableDep,
     SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE,
     FsSecurityWebAuthParamsTableINDEX, 1, 0, 0, NULL},

    {{12, FsSecurityWebAuthRedirectUrl},
     GetNextIndexFsSecurityWebAuthParamsTable,
     FsSecurityWebAuthRedirectUrlGet, FsSecurityWebAuthRedirectUrlSet,
     FsSecurityWebAuthRedirectUrlTest,
     FsSecurityWebAuthParamsTableDep,
     SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE,
     FsSecurityWebAuthParamsTableINDEX, 1, 0, 0, NULL},

    {{12, FsSecurityWebAddr},
     GetNextIndexFsSecurityWebAuthParamsTable,
     FsSecurityWebAddrGet, FsSecurityWebAddrSet,
     FsSecurityWebAddrTest,
     FsSecurityWebAuthParamsTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE, FsSecurityWebAuthParamsTableINDEX, 1, 0, 0,
     NULL},

    {{12, FsSecurityWebAuthMode},
     GetNextIndexFsSecurityWebAuthParamsTable,
     FsSecurityWebAuthModeGet, FsSecurityWebAuthModeSet,
     FsSecurityWebAuthModeTest, FsSecurityWebAuthParamsTableDep,
     SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
     FsSecurityWebAuthParamsTableINDEX, 1, 0, 0, NULL},

    {{12, FsSecurityWebAuthMaxDbEntry},
     GetNextIndexFsSecurityWebAuthParamsTable,
     FsSecurityWebAuthMaxDbEntryGet, FsSecurityWebAuthMaxDbEntrySet,
     FsSecurityWebAuthMaxDbEntryTest, FsSecurityWebAuthParamsTableDep,
     SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
     FsSecurityWebAuthParamsTableINDEX, 1, 0, 0, NULL},

    {{12, FsSecurityWebAuthWebTitle},
     GetNextIndexFsSecurityWebAuthParamsTable,
     FsSecurityWebAuthWebTitleGet, FsSecurityWebAuthWebTitleSet,
     FsSecurityWebAuthWebTitleTest, FsSecurityWebAuthParamsTableDep,
     SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE,
     FsSecurityWebAuthParamsTableINDEX, 1, 0, 0, NULL},

    {{12, FsSecurityWebAuthWebMessage},
     GetNextIndexFsSecurityWebAuthParamsTable,
     FsSecurityWebAuthWebMessageGet, FsSecurityWebAuthWebMessageSet,
     FsSecurityWebAuthWebMessageTest, FsSecurityWebAuthParamsTableDep,
     SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE,
     FsSecurityWebAuthParamsTableINDEX, 1, 0, 0, NULL},

    {{12, FsSecurityWebAuthWebLogo},
     GetNextIndexFsSecurityWebAuthParamsTable,
     FsSecurityWebAuthWebLogoGet,
     FsSecurityWebAuthWebLogoSet, FsSecurityWebAuthWebLogoTest,
     FsSecurityWebAuthParamsTableDep, SNMP_DATA_TYPE_OCTET_PRIM,
     SNMP_READWRITE, FsSecurityWebAuthParamsTableINDEX, 1, 0, 0,
     NULL},

    {{12, FsSecurityWebAuthUName},
     GetNextIndexFsSecurityWebAuthCredentialsTable,
     FsSecurityWebAuthUNameGet, FsSecurityWebAuthUNameSet,
     FsSecurityWebAuthUNameTest, FsSecurityWebAuthCredentialsTableDep,
     SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE,
     FsSecurityWebAuthCredentialsTableINDEX, 1, 0, 0, NULL},

    {{12, FsSecurityWebAuthPasswd},
     GetNextIndexFsSecurityWebAuthCredentialsTable,
     FsSecurityWebAuthPasswdGet, FsSecurityWebAuthPasswdSet,
     FsSecurityWebAuthPasswdTest,
     FsSecurityWebAuthCredentialsTableDep,
     SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE,
     FsSecurityWebAuthCredentialsTableINDEX, 1, 0, 0, NULL},

    {{12, FsSecurityWebAuthUserLifetime},
     GetNextIndexFsSecurityWebAuthCredentialsTable,
     FsSecurityWebAuthUserLifetimeGet,
     FsSecurityWebAuthUserLifetimeSet,
     FsSecurityWebAuthUserLifetimeTest,
     FsSecurityWebAuthCredentialsTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE, FsSecurityWebAuthCredentialsTableINDEX, 1, 0, 0,
     NULL},

    {{12, FsSecurityWebAuthUserEmailId},
     GetNextIndexFsSecurityWebAuthCredentialsTable,
     FsSecurityWebAuthUserEmailIdGet, NULL, NULL, NULL,
     SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY,
     FsSecurityWebAuthCredentialsTableINDEX, 1, 0, 0, NULL},

    {{12, FsStaMacAddress}, GetNextIndexFsStationQosParamsTable, NULL,
     NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS,
     FsStationQosParamsTableINDEX, 2, 0, 0, NULL},

    {{12, FsStaQoSPriority}, GetNextIndexFsStationQosParamsTable,
     FsStaQoSPriorityGet, FsStaQoSPrioritySet, FsStaQoSPriorityTest,
     FsStationQosParamsTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE,
     FsStationQosParamsTableINDEX, 2, 0, 0, NULL},

    {{12, FsStaQoSDscp}, GetNextIndexFsStationQosParamsTable,
     FsStaQoSDscpGet, FsStaQoSDscpSet, FsStaQoSDscpTest,
     FsStationQosParamsTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE,
     FsStationQosParamsTableINDEX, 2, 0, 0, NULL},

    {{12, FsVlanIsolation}, GetNextIndexFsVlanIsolationTable,
     FsVlanIsolationGet, FsVlanIsolationSet, FsVlanIsolationTest,
     FsVlanIsolationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
     FsVlanIsolationTableINDEX, 1, 0, 0, NULL},

    {{12, FsDot11RadioType}, GetNextIndexFsDot11RadioConfigTable,
     FsDot11RadioTypeGet, FsDot11RadioTypeSet, FsDot11RadioTypeTest,
     FsDot11RadioConfigTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE,
     FsDot11RadioConfigTableINDEX, 1, 0, 0, NULL},

    {{12, FsDot11RadioNoOfBssIdSupported},
     GetNextIndexFsDot11RadioConfigTable,
     FsDot11RadioNoOfBssIdSupportedGet, NULL, NULL, NULL,
     SNMP_DATA_TYPE_INTEGER, SNMP_READONLY,
     FsDot11RadioConfigTableINDEX,
     1, 0, 0, NULL},

    {{12, FsDot11RadioAntennaType},
     GetNextIndexFsDot11RadioConfigTable,
     FsDot11RadioAntennaTypeGet, NULL, NULL, NULL,
     SNMP_DATA_TYPE_INTEGER,
     SNMP_READONLY, FsDot11RadioConfigTableINDEX, 1, 0, 0, NULL},

    {{12, FsDot11RadioFailureStatus},
     GetNextIndexFsDot11RadioConfigTable,
     FsDot11RadioFailureStatusGet, NULL, NULL, NULL,
     SNMP_DATA_TYPE_INTEGER, SNMP_READONLY,
     FsDot11RadioConfigTableINDEX,
     1, 0, 0, NULL},

    {{12, FsDot11QosProfileName}, GetNextIndexFsDot11QosProfileTable,
     NULL,
     NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS,
     FsDot11QosProfileTableINDEX, 1, 0, 0, NULL},

    {{12, FsDot11QosTraffic}, GetNextIndexFsDot11QosProfileTable,
     FsDot11QosTrafficGet, FsDot11QosTrafficSet,
     FsDot11QosTrafficTest,
     FsDot11QosProfileTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE,
     FsDot11QosProfileTableINDEX, 1, 0, 0, NULL},

    {{12, FsDot11QosPassengerTrustMode},
     GetNextIndexFsDot11QosProfileTable,
     FsDot11QosPassengerTrustModeGet,
     FsDot11QosPassengerTrustModeSet,
     FsDot11QosPassengerTrustModeTest,
     FsDot11QosProfileTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE,
     FsDot11QosProfileTableINDEX, 1, 0, 0, NULL},

    {{12, FsDot11QosRowStatus}, GetNextIndexFsDot11QosProfileTable,
     FsDot11QosRowStatusGet, FsDot11QosRowStatusSet,
     FsDot11QosRowStatusTest, FsDot11QosProfileTableDep,
     SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
     FsDot11QosProfileTableINDEX,
     1, 0, 1, NULL},

    {{12, FsDot11TaggingPolicy}, GetNextIndexFsDot11QAPTable,
     FsDot11TaggingPolicyGet, FsDot11TaggingPolicySet,
     FsDot11TaggingPolicyTest, FsDot11QAPTableDep,
     SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsDot11QAPTableINDEX,
     1, 0,
     0, NULL},

    {{12, FsDot11QueueDepth}, GetNextIndexFsDot11QAPTable,
     FsDot11QueueDepthGet, FsDot11QueueDepthSet,
     FsDot11QueueDepthTest,
     FsDot11QAPTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE,
     FsDot11QAPTableINDEX, 1, 0, 0, NULL},

    {{12, FsDot11PriorityValue}, GetNextIndexFsDot11QAPTable,
     FsDot11PriorityValueGet, FsDot11PriorityValueSet,
     FsDot11PriorityValueTest, FsDot11QAPTableDep,
     SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsDot11QAPTableINDEX,
     1, 0,
     0, NULL},

    {{12, FsDot11DscpValue}, GetNextIndexFsDot11QAPTable,
     FsDot11DscpValueGet, FsDot11DscpValueSet, FsDot11DscpValueTest,
     FsDot11QAPTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE,
     FsDot11QAPTableINDEX, 1, 0, 0, NULL},

    {{12, FsAntennaMode}, GetNextIndexFsDot11AntennasListTable,
     FsAntennaModeGet, FsAntennaModeSet, FsAntennaModeTest,
     FsDot11AntennasListTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE,
     FsDot11AntennasListTableINDEX, 2, 0, 0, NULL},

    {{12, FsAntennaSelection}, GetNextIndexFsDot11AntennasListTable,
     FsAntennaSelectionGet, FsAntennaSelectionSet,
     FsAntennaSelectionTest,
     FsDot11AntennasListTableDep, SNMP_DATA_TYPE_INTEGER,
     SNMP_READWRITE,
     FsDot11AntennasListTableINDEX, 2, 0, 0, NULL},
};
tMibData fswlanEntry = { 64, fswlanMibEntry };

#endif                          /* _FSWLANDB_H */
