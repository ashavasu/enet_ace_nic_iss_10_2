/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: wsscfgstdwla.h,v 1.1.1.1 2016/09/20 10:42:39 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDWLADB_H
#define _STDWLADB_H

UINT1 CapwapDot11WlanTableINDEX[] = { SNMP_DATA_TYPE_UNSIGNED32 };
UINT1 CapwapDot11WlanBindTableINDEX[] =
    { SNMP_DATA_TYPE_INTEGER, SNMP_DATA_TYPE_UNSIGNED32 };

UINT4 stdwla[] = { 1, 3, 6, 1, 2, 1, 195 };
tSNMP_OID_TYPE stdwlaOID = { 7, stdwla };


UINT4 CapwapDot11WlanProfileId[] =
    { 1, 3, 6, 1, 2, 1, 195, 1, 1, 1, 1 };
UINT4 CapwapDot11WlanProfileIfIndex[] =
    { 1, 3, 6, 1, 2, 1, 195, 1, 1, 1, 2 };
UINT4 CapwapDot11WlanMacType[] =
    { 1, 3, 6, 1, 2, 1, 195, 1, 1, 1, 3 };
UINT4 CapwapDot11WlanTunnelMode[] =
    { 1, 3, 6, 1, 2, 1, 195, 1, 1, 1, 4 };
UINT4 CapwapDot11WlanRowStatus[] =
    { 1, 3, 6, 1, 2, 1, 195, 1, 1, 1, 5 };
UINT4 CapwapDot11WlanBindWlanId[] =
    { 1, 3, 6, 1, 2, 1, 195, 1, 2, 1, 1 };
UINT4 CapwapDot11WlanBindBssIfIndex[] =
    { 1, 3, 6, 1, 2, 1, 195, 1, 2, 1, 2 };
UINT4 CapwapDot11WlanBindRowStatus[] =
    { 1, 3, 6, 1, 2, 1, 195, 1, 2, 1, 3 };




tMbDbEntry stdwlaMibEntry[] = {

    {{11, CapwapDot11WlanProfileId}
     , GetNextIndexCapwapDot11WlanTable, NULL, NULL, NULL, NULL,
     SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS,
     CapwapDot11WlanTableINDEX,
     1, 0, 0, NULL}
    ,

    {{11, CapwapDot11WlanProfileIfIndex}
     , GetNextIndexCapwapDot11WlanTable,
     CapwapDot11WlanProfileIfIndexGet,
     NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY,
     CapwapDot11WlanTableINDEX, 1, 0, 0, NULL}
    ,

    {{11, CapwapDot11WlanMacType}
     , GetNextIndexCapwapDot11WlanTable, CapwapDot11WlanMacTypeGet,
     CapwapDot11WlanMacTypeSet, CapwapDot11WlanMacTypeTest,
     CapwapDot11WlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
     CapwapDot11WlanTableINDEX, 1, 0, 0, NULL}
    ,

    {{11, CapwapDot11WlanTunnelMode}
     , GetNextIndexCapwapDot11WlanTable, CapwapDot11WlanTunnelModeGet,
     CapwapDot11WlanTunnelModeSet, CapwapDot11WlanTunnelModeTest,
     CapwapDot11WlanTableDep, SNMP_DATA_TYPE_OCTET_PRIM,
     SNMP_READWRITE,
     CapwapDot11WlanTableINDEX, 1, 0, 0, NULL}
    ,

    {{11, CapwapDot11WlanRowStatus}
     , GetNextIndexCapwapDot11WlanTable, CapwapDot11WlanRowStatusGet,
     CapwapDot11WlanRowStatusSet, CapwapDot11WlanRowStatusTest,
     CapwapDot11WlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
     CapwapDot11WlanTableINDEX, 1, 0, 1, NULL}
    ,

    {{11, CapwapDot11WlanBindWlanId}
     , GetNextIndexCapwapDot11WlanBindTable,
     CapwapDot11WlanBindWlanIdGet,
     NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY,
     CapwapDot11WlanBindTableINDEX, 2, 0, 0, NULL}
    ,

    {{11, CapwapDot11WlanBindBssIfIndex}
     , GetNextIndexCapwapDot11WlanBindTable,
     CapwapDot11WlanBindBssIfIndexGet, NULL, NULL, NULL,
     SNMP_DATA_TYPE_INTEGER, SNMP_READONLY,
     CapwapDot11WlanBindTableINDEX,
     2, 0, 0, NULL}
    ,

    {{11, CapwapDot11WlanBindRowStatus}
     , GetNextIndexCapwapDot11WlanBindTable,
     CapwapDot11WlanBindRowStatusGet, CapwapDot11WlanBindRowStatusSet,
     CapwapDot11WlanBindRowStatusTest, CapwapDot11WlanBindTableDep,
     SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE,
     CapwapDot11WlanBindTableINDEX,
     2, 0, 1, NULL}
    ,
};
tMibData stdwlaEntry = { 8, stdwlaMibEntry };

#endif                          /* _STDWLADB_H */
