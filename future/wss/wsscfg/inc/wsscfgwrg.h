/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
* $Id: wsscfgwrg.h,v 1.3.2.1 2018/03/19 14:21:09 siva Exp $
* 
* Description: This file contains the prototype(wr) for Wsscfg 
*********************************************************************/
#ifndef _WSSCFGWR_H
#define _WSSCFGWR_H

INT4 GetNextIndexCapwapDot11WlanTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexCapwapDot11WlanBindTable(
    tSnmpIndex *,
    tSnmpIndex *);
INT4 CapwapDot11WlanProfileIfIndexGet(
    tSnmpIndex *,
    tRetVal *);
INT4 CapwapDot11WlanMacTypeGet(
    tSnmpIndex *,
    tRetVal *);
INT4 CapwapDot11WlanTunnelModeGet(
    tSnmpIndex *,
    tRetVal *);
INT4 CapwapDot11WlanRowStatusGet(
    tSnmpIndex *,
    tRetVal *);
INT4 CapwapDot11WlanBindWlanIdGet(
    tSnmpIndex *,
    tRetVal *);
INT4 CapwapDot11WlanBindBssIfIndexGet(
    tSnmpIndex *,
    tRetVal *);
INT4 CapwapDot11WlanBindRowStatusGet(
    tSnmpIndex *,
    tRetVal *);
INT4 CapwapDot11WlanMacTypeTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 CapwapDot11WlanTunnelModeTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 CapwapDot11WlanRowStatusTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 CapwapDot11WlanBindRowStatusTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 CapwapDot11WlanMacTypeSet(
    tSnmpIndex *,
    tRetVal *);
INT4 CapwapDot11WlanTunnelModeSet(
    tSnmpIndex *,
    tRetVal *);
INT4 CapwapDot11WlanRowStatusSet(
    tSnmpIndex *,
    tRetVal *);
INT4 CapwapDot11WlanBindRowStatusSet(
    tSnmpIndex *,
    tRetVal *);
INT4 CapwapDot11WlanTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 CapwapDot11WlanBindTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);

VOID RegisterSTDWLA PROTO(
     (VOID));
VOID UnRegisterSTDWLA PROTO(
     (VOID));


INT4 GetNextIndexFsDot11StationConfigTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsDot11CapabilityProfileTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsDot11AuthenticationProfileTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsSecurityWebAuthGuestInfoTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsStationQosParamsTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsVlanIsolationTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsDot11RadioConfigTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsDot11QosProfileTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsDot11WlanCapabilityProfileTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsDot11WlanAuthenticationProfileTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsDot11WlanQosProfileTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsDot11ClientSummaryTable(
    tSnmpIndex *, 
    tSnmpIndex *);

INT4 GetNextIndexFsDot11ExternalWebAuthProfileTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot11ExternalWebAuthMethodGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11ExternalWebAuthUrlGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11ExternalWebAuthMethodSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11ExternalWebAuthUrlSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11ExternalWebAuthMethodTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11ExternalWebAuthUrlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11ExternalWebAuthProfileTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexFsDot11RadioQosTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsDot11QAPTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsQAPProfileTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsDot11CapabilityMappingTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsDot11AuthMappingTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsDot11QosMappingTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsDot11AntennasListTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsDot11WlanTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsDot11WlanBindTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsDot11nConfigTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsDot11nMCSDataRateTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsWtpImageUpgradeTable(
    tSnmpIndex *,
    tSnmpIndex *);

INT4 GetNextIndexFsWlanStatisticsTable(
     tSnmpIndex *, 
     tSnmpIndex *);

INT4 FsDot11aNetworkEnableGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11bNetworkEnableGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11gSupportGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11anSupportGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11bnSupportGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11ManagmentSSIDGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11CountryStringGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthTypeGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthUrlGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthRedirectUrlGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAddrGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthWebTitleGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthWebMessageGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthWebLogoFileNameGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthWebSuccMessageGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthWebFailMessageGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthWebButtonTextGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthWebLoadBalInfoGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthDisplayLangGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthColorGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11StationCountGet(
    tSnmpIndex *, 
    tRetVal *);
INT4 FsDot11SupressSSIDGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11VlanIdGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanWebAuthRedirectFileNameGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11WlanLoginAuthenticationGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11BandwidthThreshGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11CFPollableGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11CFPollRequestGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11PrivacyOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11ShortPreambleOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11PBCCOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11ChannelAgilityPresentGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QosOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11SpectrumManagementRequiredGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11ShortSlotTimeOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11APSDOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11DSSSOFDMOptionEnabledGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11DelayedBlockAckOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11ImmediateBlockAckOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QAckOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QueueRequestOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11TXOPRequestOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11RSNAOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11RSNAPreauthenticationImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11CapabilityRowStatusGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11AuthenticationAlgorithmGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WepKeyIndexGet(
    tSnmpIndex * , 
    tRetVal * );
INT4 FsDot11WepKeyTypeGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WepKeyLengthGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WepKeyGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WebAuthenticationGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11AuthenticationRowStatusGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWlanProfileIdGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthUserLifetimeGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthUserEmailIdGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthGuestInfoRowStatusGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthUPasswordGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsStaQoSPriorityGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsStaQoSDscpGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsVlanIsolationGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11RadioTypeGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11RadioNoOfBssIdSupportedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11RadioAntennaTypeGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11RadioFailureStatusGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11RowStatusGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QosTrafficGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QosPassengerTrustModeGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QosRateLimitGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11UpStreamCIRGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11UpStreamCBSGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11UpStreamEIRGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11UpStreamEBSGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11DownStreamCIRGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11DownStreamCBSGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11DownStreamEIRGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11DownStreamEBSGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QosRowStatusGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanCFPollableGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanCFPollRequestGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanPrivacyOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanShortPreambleOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanPBCCOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanChannelAgilityPresentGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanQosOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanSpectrumManagementRequiredGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanShortSlotTimeOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanAPSDOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanDSSSOFDMOptionEnabledGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanDelayedBlockAckOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanImmediateBlockAckOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanQAckOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanQueueRequestOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanTXOPRequestOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanRSNAOptionImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanRSNAPreauthenticationImplementedGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanCapabilityRowStatusGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanAuthenticationAlgorithmGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanWepKeyIndexGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanWepKeyTypeGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanWepKeyLengthGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanWepKeyGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanWebAuthenticationGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanAuthenticationRowStatusGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanQosTrafficGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanQosPassengerTrustModeGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanQosRateLimitGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanUpStreamCIRGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanUpStreamCBSGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanUpStreamEIRGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanUpStreamEBSGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanDownStreamCIRGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanDownStreamCBSGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanDownStreamEIRGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanDownStreamEBSGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanQosRowStatusGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11TaggingPolicyGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QueueDepthGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11PriorityValueGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11DscpValueGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfileCWminGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfileCWmaxGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfileAIFSNGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfileTXOPLimitGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfileQueueDepthGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfilePriorityValueGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfileDscpValueGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfileRowStatusGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11CapabilityMappingProfileNameGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11CapabilityMappingRowStatusGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11AuthMappingProfileNameGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11AuthMappingRowStatusGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QosMappingProfileNameGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QosMappingRowStatusGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanProfileIdGet(
    tSnmpIndex *, 
    tRetVal *);
INT4 FsDot11WtpProfileNameGet(
    tSnmpIndex *, 
    tRetVal *);
INT4 FsDot11WtpRadioIdGet(
    tSnmpIndex *, 
    tRetVal *);
INT4 FsDot11AuthStatusGet(
    tSnmpIndex *, 
    tRetVal *);
INT4 FsDot11AssocStatusGet(
    tSnmpIndex *, 
    tRetVal *);
INT4 FsAntennaModeGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsAntennaSelectionGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanProfileIfIndexGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanRowStatusGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanBindWlanIdGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanBindBssIfIndexGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanBindRowStatusGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11nConfigShortGIfor20MHzGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11nConfigShortGIfor40MHzGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11nConfigChannelWidthGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11nConfigAllowChannelWidthGet(
    tSnmpIndex *, 
    tRetVal *);
INT4 FsDot11nMCSDataRateGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsWtpImageVersionGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsWtpUpgradeDevGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsWtpNameGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsWtpImageNameGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsWtpAddressTypeGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsWtpServerIPGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsWtpRowStatusGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsWlanBeaconsSentCountGet(
     tSnmpIndex *, 
     tRetVal *);
INT4 FsWlanProbeReqRcvdCountGet(
     tSnmpIndex *, 
     tRetVal *);
INT4 FsWlanProbeRespSentCountGet(
     tSnmpIndex *, 
     tRetVal *);
INT4 FsWlanDataPktRcvdCountGet(
        tSnmpIndex *, 
        tRetVal *);
INT4 FsWlanDataPktSentCountGet(
     tSnmpIndex *, 
     tRetVal *);

INT4 FsDot11aNetworkEnableTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11bNetworkEnableTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11gSupportTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11anSupportTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11bnSupportTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11ManagmentSSIDTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11CountryStringTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthTypeTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthUrlTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthRedirectUrlTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAddrTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthWebTitleTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthWebMessageTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthWebLogoFileNameTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthWebSuccMessageTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthWebFailMessageTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthWebButtonTextTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthWebLoadBalInfoTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthDisplayLangTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthColorTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11SupressSSIDTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11VlanIdTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanWebAuthRedirectFileNameSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11WlanLoginAuthenticationSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11BandwidthThreshTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11CFPollableTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11CFPollRequestTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11PrivacyOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11ShortPreambleOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11PBCCOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11ChannelAgilityPresentTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QosOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11SpectrumManagementRequiredTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11ShortSlotTimeOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11APSDOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11DSSSOFDMOptionEnabledTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11DelayedBlockAckOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11ImmediateBlockAckOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QAckOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QueueRequestOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11TXOPRequestOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11RSNAOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11RSNAPreauthenticationImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11CapabilityRowStatusTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11AuthenticationAlgorithmTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WepKeyIndexTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WepKeyTypeTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WepKeyLengthTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WepKeyTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WebAuthenticationTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11AuthenticationRowStatusTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWlanProfileIdTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthUserLifetimeTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthGuestInfoRowStatusTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthUPasswordTest(
    UINT4 *, 
    tSnmpIndex *,
    tRetVal *);
INT4 FsStaQoSPriorityTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsStaQoSDscpTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsVlanIsolationTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11RadioTypeTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11RowStatusTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QosTrafficTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QosPassengerTrustModeTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QosRateLimitTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11UpStreamCIRTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11UpStreamCBSTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11UpStreamEIRTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11UpStreamEBSTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11DownStreamCIRTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11DownStreamCBSTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11DownStreamEIRTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11DownStreamEBSTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QosRowStatusTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanCFPollableTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanCFPollRequestTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanPrivacyOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanShortPreambleOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanPBCCOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanChannelAgilityPresentTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanQosOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanSpectrumManagementRequiredTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanShortSlotTimeOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanAPSDOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanDSSSOFDMOptionEnabledTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanDelayedBlockAckOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanImmediateBlockAckOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanQAckOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanQueueRequestOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanTXOPRequestOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanRSNAOptionImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanRSNAPreauthenticationImplementedTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanCapabilityRowStatusTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanAuthenticationAlgorithmTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanWepKeyIndexTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanWepKeyTypeTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanWepKeyLengthTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanWepKeyTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanWebAuthenticationTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanAuthenticationRowStatusTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanQosTrafficTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanQosPassengerTrustModeTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanQosRateLimitTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanUpStreamCIRTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanUpStreamCBSTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanUpStreamEIRTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanUpStreamEBSTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanDownStreamCIRTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanDownStreamCBSTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanDownStreamEIRTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanDownStreamEBSTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanQosRowStatusTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11TaggingPolicyTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QueueDepthTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11PriorityValueTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11DscpValueTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfileCWminTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfileCWmaxTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfileAIFSNTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfileTXOPLimitTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfileQueueDepthTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfilePriorityValueTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfileDscpValueTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfileRowStatusTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11CapabilityMappingProfileNameTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11CapabilityMappingRowStatusTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11AuthMappingProfileNameTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11AuthMappingRowStatusTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QosMappingProfileNameTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QosMappingRowStatusTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsAntennaModeTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsAntennaSelectionTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanProfileIfIndexTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanRowStatusTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanBindWlanIdTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanBindBssIfIndexTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanBindRowStatusTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11nConfigShortGIfor20MHzTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11nConfigShortGIfor40MHzTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11nConfigChannelWidthTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11nConfigAllowChannelWidthTest(
    UINT4 *, 
    tSnmpIndex *, 
    tRetVal *);
INT4 FsDot11nMCSDataRateTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsWtpImageVersionTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsWtpUpgradeDevTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsWtpNameTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsWtpImageNameTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsWtpAddressTypeTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsWtpServerIPTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsWtpRowStatusTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsWlanBeaconsSentCountTest(
    UINT4 *, 
    tSnmpIndex *, 
    tRetVal *);
INT4 FsWlanProbeReqRcvdCountTest(
     UINT4 *, 
     tSnmpIndex *, 
     tRetVal *);
INT4 FsWlanProbeRespSentCountTest(
     UINT4 *, 
     tSnmpIndex *, 
     tRetVal *);
INT4 FsWlanDataPktRcvdCountTest(
     UINT4 *, 
     tSnmpIndex *, 
     tRetVal *);
INT4 FsWlanDataPktSentCountTest(
     UINT4 *, 
     tSnmpIndex *, 
     tRetVal *);

INT4 FsDot11aNetworkEnableSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11bNetworkEnableSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11gSupportSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11anSupportSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11bnSupportSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11ManagmentSSIDSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11CountryStringSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthTypeSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthUrlSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthRedirectUrlSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAddrSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthWebTitleSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthWebMessageSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthWebLogoFileNameSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthWebSuccMessageSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthWebFailMessageSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthWebButtonTextSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthWebLoadBalInfoSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthDisplayLangSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthColorSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11SupressSSIDSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11VlanIdSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanWebAuthRedirectFileNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11WlanLoginAuthenticationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11BandwidthThreshSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11CFPollableSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11CFPollRequestSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11PrivacyOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11ShortPreambleOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11PBCCOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11ChannelAgilityPresentSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QosOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11SpectrumManagementRequiredSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11ShortSlotTimeOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11APSDOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11DSSSOFDMOptionEnabledSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11DelayedBlockAckOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11ImmediateBlockAckOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QAckOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QueueRequestOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11TXOPRequestOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11RSNAOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11RSNAPreauthenticationImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11CapabilityRowStatusSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11AuthenticationAlgorithmSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WepKeyIndexSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WepKeyTypeSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WepKeyLengthSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WepKeySet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WebAuthenticationSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11AuthenticationRowStatusSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWlanProfileIdSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthUserLifetimeSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthGuestInfoRowStatusSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsSecurityWebAuthUPasswordSet(
    tSnmpIndex *, 
    tRetVal *);
INT4 FsStaQoSPrioritySet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsStaQoSDscpSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsVlanIsolationSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11RadioTypeSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11RowStatusSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QosTrafficSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QosPassengerTrustModeSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QosRateLimitSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11UpStreamCIRSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11UpStreamCBSSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11UpStreamEIRSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11UpStreamEBSSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11DownStreamCIRSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11DownStreamCBSSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11DownStreamEIRSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11DownStreamEBSSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QosRowStatusSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanCFPollableSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanCFPollRequestSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanPrivacyOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanShortPreambleOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanPBCCOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanChannelAgilityPresentSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanQosOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanSpectrumManagementRequiredSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanShortSlotTimeOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanAPSDOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanDSSSOFDMOptionEnabledSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanDelayedBlockAckOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanImmediateBlockAckOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanQAckOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanQueueRequestOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanTXOPRequestOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanRSNAOptionImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanRSNAPreauthenticationImplementedSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanCapabilityRowStatusSet(
    tSnmpIndex *,
    tRetVal *);


INT4 FsDot11WlanWepKeyIndexSe(
    tSnmpIndex * pMultiIndex,
    tRetVal * pMultiData);
INT4 FsDot11WlanAuthenticationAlgorithmSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanWepKeyIndexSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanWepKeyTypeSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanWepKeyLengthSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanWepKeySet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanWebAuthenticationSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanAuthenticationRowStatusSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanQosTrafficSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanQosPassengerTrustModeSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanQosRateLimitSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanUpStreamCIRSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanUpStreamCBSSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanUpStreamEIRSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanUpStreamEBSSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanDownStreamCIRSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanDownStreamCBSSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanDownStreamEIRSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanDownStreamEBSSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanQosRowStatusSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11TaggingPolicySet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QueueDepthSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11PriorityValueSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11DscpValueSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfileCWminSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfileCWmaxSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfileAIFSNSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfileTXOPLimitSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfileQueueDepthSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfilePriorityValueSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfileDscpValueSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsQAPProfileRowStatusSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11CapabilityMappingProfileNameSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11CapabilityMappingRowStatusSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11AuthMappingProfileNameSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11AuthMappingRowStatusSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QosMappingProfileNameSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11QosMappingRowStatusSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsAntennaModeSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsAntennaSelectionSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanProfileIfIndexSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanRowStatusSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanBindWlanIdSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanBindBssIfIndexSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11WlanBindRowStatusSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11nConfigShortGIfor20MHzSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11nConfigShortGIfor40MHzSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11nConfigChannelWidthSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11nConfigAllowChannelWidthSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsDot11nMCSDataRateSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsWtpImageVersionSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsWtpUpgradeDevSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsWtpNameSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsWtpImageNameSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsWtpAddressTypeSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsWtpServerIPSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsWtpRowStatusSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsWlanBeaconsSentCountSet(
     tSnmpIndex *, 
     tRetVal *);
INT4 FsWlanProbeReqRcvdCountSet(
     tSnmpIndex *, 
     tRetVal *);
INT4 FsWlanProbeRespSentCountSet(
     tSnmpIndex *, 
     tRetVal *);
INT4 FsWlanDataPktRcvdCountSet(
     tSnmpIndex *, 
     tRetVal *);
INT4 FsWlanDataPktSentCountSet(
     tSnmpIndex *, 
     tRetVal *);

INT4 FsDot11aNetworkEnableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11bNetworkEnableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11gSupportDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11anSupportDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11bnSupportDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11ManagmentSSIDDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11CountryStringDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 GetNextIndexFsDot11SupportedCountryTable(
        tSnmpIndex *, 
        tSnmpIndex *);
INT4 FsDot11CountryCodeGet(
        tSnmpIndex *, 
        tRetVal *);
INT4 FsDot11CountryNameGet(
        tSnmpIndex *, 
        tRetVal *);
INT4 FsDot11NumericIndexGet(
        tSnmpIndex *, 
        tRetVal *);
INT4 FsSecurityWebAuthTypeDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsSecurityWebAuthUrlDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsSecurityWebAuthRedirectUrlDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsSecurityWebAddrDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsSecurityWebAuthWebTitleDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsSecurityWebAuthWebMessageDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsSecurityWebAuthWebLogoFileNameDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsSecurityWebAuthWebSuccMessageDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsSecurityWebAuthWebFailMessageDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsSecurityWebAuthWebButtonTextDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsSecurityWebAuthWebLoadBalInfoDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsSecurityWebAuthDisplayLangDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsSecurityWebAuthColorDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11StationConfigTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11CapabilityProfileTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11AuthenticationProfileTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsSecurityWebAuthGuestInfoTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsStationQosParamsTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsVlanIsolationTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11RadioConfigTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11QosProfileTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11WlanCapabilityProfileTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11WlanAuthenticationProfileTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11WlanQosProfileTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11RadioQosTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11QAPTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsQAPProfileTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11CapabilityMappingTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11AuthMappingTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11QosMappingTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11AntennasListTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11WlanTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11WlanBindTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11nConfigTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsDot11nMCSDataRateTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsWtpImageUpgradeTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);
INT4 FsWlanStatisticsTableDep(
     UINT4 *, 
     tSnmpIndexList*, 
     tSNMP_VAR_BIND*);
INT4 GetNextIndexFsWlanSSIDStatsTable(
     tSnmpIndex *, 
     tSnmpIndex *);
INT4 FsWlanSSIDStatsAssocClientCountGet(
     tSnmpIndex *, 
     tRetVal *);
INT4 GetNextIndexFsDot11nExtConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot11nAMPDUStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nAMPDUSubFrameGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nAMPDULimitGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nAMSDUStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nAMSDULimitGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nAMPDUStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nAMPDUSubFrameSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nAMPDULimitSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nAMSDUStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nAMSDULimitSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nAMPDUStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nAMPDUSubFrameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nAMPDULimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nAMSDUStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nAMSDULimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nExtConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsWlanSSIDStatsMaxClientCountGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanSSIDStatsMaxClientCountSet(tSnmpIndex *, tRetVal *);
INT4 FsWlanSSIDStatsMaxClientCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWlanSSIDStatsTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsWlanClientStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsWlanClientStatsMACAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanClientStatsIdleTimeoutGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanClientStatsBytesReceivedCountGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanClientStatsBytesSentCountGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanClientStatsAuthStateGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanClientStatsRadiusUserNameGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanClientStatsIpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanClientStatsIpAddressTypeGet(tSnmpIndex *,tRetVal *);
INT4 FsWlanClientStatsAssocTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanClientStatsSessionTimeLeftGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanClientStatsConnTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanClientStatsSSIDGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanClientStatsToACStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanClientStatsClearGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanClientStatsToACStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsWlanClientStatsClearSet(tSnmpIndex *, tRetVal *);
INT4 FsWlanClientStatsToACStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWlanClientStatsClearTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWlanClientStatsTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsWlanRadioTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsWlanRadioRadiobaseMACAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanRadioIfInOctetsGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanRadioIfOutOctetsGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanRadioRadioBandGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanRadioMaxClientCountGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanRadioClearStatsGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanRadioMaxClientCountSet(tSnmpIndex *, tRetVal *);
INT4 FsWlanRadioClearStatsSet(tSnmpIndex *, tRetVal *);
INT4 FsWlanRadioMaxClientCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWlanRadioClearStatsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWlanRadioTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsApWlanStatisticsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsApWlanBeaconsSentCountGet(tSnmpIndex *, tRetVal *);
INT4 FsApWlanProbeReqRcvdCountGet(tSnmpIndex *, tRetVal *);
INT4 FsApWlanProbeRespSentCountGet(tSnmpIndex *, tRetVal *);
INT4 FsApWlanDataPktRcvdCountGet(tSnmpIndex *, tRetVal *);
INT4 FsApWlanDataPktSentCountGet(tSnmpIndex *, tRetVal *);
INT4 FsApWlanBeaconsSentCountSet(tSnmpIndex *, tRetVal *);
INT4 FsApWlanProbeReqRcvdCountSet(tSnmpIndex *, tRetVal *);
INT4 FsApWlanProbeRespSentCountSet(tSnmpIndex *, tRetVal *);
INT4 FsApWlanDataPktRcvdCountSet(tSnmpIndex *, tRetVal *);
INT4 FsApWlanDataPktSentCountSet(tSnmpIndex *, tRetVal *);
INT4 FsApWlanBeaconsSentCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsApWlanProbeReqRcvdCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsApWlanProbeRespSentCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsApWlanDataPktRcvdCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsApWlanDataPktSentCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsApWlanStatisticsTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsApGroupEnabledStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsApGroupEnabledStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsApGroupEnabledStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsApGroupEnabledStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsApGroupTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsApGroupNameGet(tSnmpIndex *, tRetVal *);
INT4 FsApGroupNameDescriptionGet(tSnmpIndex *, tRetVal *);
INT4 FsApGroupRadioPolicyGet(tSnmpIndex *, tRetVal *);
INT4 FsApGroupInterfaceVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsApGroupRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsApGroupNameSet(tSnmpIndex *, tRetVal *);
INT4 FsApGroupNameDescriptionSet(tSnmpIndex *, tRetVal *);
INT4 FsApGroupRadioPolicySet(tSnmpIndex *, tRetVal *);
INT4 FsApGroupInterfaceVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsApGroupRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsApGroupNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsApGroupNameDescriptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsApGroupRadioPolicyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsApGroupInterfaceVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsApGroupRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsApGroupTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsApGroupWTPTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsApGroupWTPRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsApGroupWTPRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsApGroupWTPRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsApGroupWTPTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsApGroupWLANTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsApGroupWLANRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsApGroupWLANRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsApGroupWLANRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsApGroupWLANTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

#ifndef RFMGMT_WANTED
INT4 GetNextIndexFsRrmConfigTable(
    tSnmpIndex *,
    tSnmpIndex *);
INT4 FsRrmDcaModeGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsRrmDcaChannelSelectionModeGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsRrmTpcModeGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsRrmTpcSelectionModeGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsRrmRowStatusGet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsRrmDcaModeTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsRrmDcaChannelSelectionModeTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsRrmTpcModeTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsRrmTpcSelectionModeTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsRrmRowStatusTest(
    UINT4 *,
    tSnmpIndex *,
    tRetVal *);
INT4 FsRrmDcaModeSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsRrmDcaChannelSelectionModeSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsRrmTpcModeSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsRrmTpcSelectionModeSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsRrmRowStatusSet(
    tSnmpIndex *,
    tRetVal *);
INT4 FsRrmConfigTableDep(
    UINT4 *,
    tSnmpIndexList *,
    tSNMP_VAR_BIND *);

VOID RegisterFSRRM PROTO(
     (VOID));
VOID UnRegisterFSRRM PROTO(
     (VOID));
#endif
VOID RegisterFSWLAN PROTO(
     (VOID));
VOID UnRegisterFSWLAN PROTO(
     (VOID));
INT4 FsWlanStationTrapStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanStationTrapStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsWlanStationTrapStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWlanStationTrapStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsWlanMulticastTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsWlanMulticastModeGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanMulticastSnoopTableLengthGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanMulticastSnoopTimerGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanMulticastSnoopTimeoutGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanMulticastModeSet(tSnmpIndex *, tRetVal *);
INT4 FsWlanMulticastSnoopTableLengthSet(tSnmpIndex *, tRetVal *);
INT4 FsWlanMulticastSnoopTimerSet(tSnmpIndex *, tRetVal *);
INT4 FsWlanMulticastSnoopTimeoutSet(tSnmpIndex *, tRetVal *);
INT4 FsWlanMulticastModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWlanMulticastSnoopTableLengthTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWlanMulticastSnoopTimerTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWlanMulticastSnoopTimeoutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWlanMulticastTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#ifdef BAND_SELECT_WANTED
INT4 FsBandSelectStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsBandSelectStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsBandSelectStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsBandSelectStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsWlanBandSelectTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsBandSelectPerWlanStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsBandSelectAssocCountGet(tSnmpIndex *, tRetVal *);
INT4 FsBandSelectAgeOutSuppTimerGet(tSnmpIndex *, tRetVal *);
INT4 FsBandSelectAssocCountFlushTimerGet(tSnmpIndex *, tRetVal *);
INT4 FsBandSelectPerWlanStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsBandSelectAssocCountSet(tSnmpIndex *, tRetVal *);
INT4 FsBandSelectAgeOutSuppTimerSet(tSnmpIndex *, tRetVal *);
INT4 FsBandSelectAssocCountFlushTimerSet(tSnmpIndex *, tRetVal *);
INT4 FsBandSelectPerWlanStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsBandSelectAssocCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsBandSelectAgeOutSuppTimerTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsBandSelectAssocCountFlushTimerTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWlanBandSelectTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsWlanBandSelectClientTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsWlanBandSelectStationActiveStatusGet(tSnmpIndex *, tRetVal *);
#endif
INT4 FsStationDebugOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsWlanDebugOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsRadioDebugOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsPmDebugOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsWssIfDebugOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsStationDebugOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsWlanDebugOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsRadioDebugOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsPmDebugOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsWssIfDebugOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsStationDebugOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWlanDebugOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRadioDebugOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPmDebugOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsWssIfDebugOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsStationDebugOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsWlanDebugOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRadioDebugOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPmDebugOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsWssIfDebugOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsBandSelectLegacyRateGet(tSnmpIndex *, tRetVal *);
INT4 FsBandSelectLegacyRateSet(tSnmpIndex *, tRetVal *);
INT4 FsBandSelectLegacyRateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsBandSelectLegacyRateDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 FsDot11nConfigMIMOPowerSaveGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigDelayedBlockAckOptionActivatedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigMaxAMSDULengthConfigGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigMaxRxAMPDUFactorConfigGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigMinimumMPDUStartSpacingConfigGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigPCOOptionActivatedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigTransitionTimeConfigGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigMCSFeedbackOptionActivatedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigHTControlFieldSupportedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigRDResponderOptionActivatedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigtHTDSSCCKModein40MHzGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigtHTDSSCCKModein40MHzConfigGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigTxRxMCSSetNotEqualGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigTxMaximumNumberSpatialStreamsSupportedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigTxUnequalModulationSupportedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigImplicitTransmitBeamformingRecvOptionImplementedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigImplicitTransmitBeamformingRecvOptionActivatedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigReceiveStaggerSoundingOptionActivatedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigTransmitStaggerSoundingOptionActivatedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigReceiveNDPOptionActivatedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigTransmitNDPOptionActivatedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigImplicitTransmitBeamformingOptionActivatedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigCalibrationOptionActivatedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigExplicitCSITransmitBeamformingOptionActivatedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivatedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigExplicitCompressedBeamformingMatrixOptImplementedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivatedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivatedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivatedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivatedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigMinimalGroupingImplementedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigMinimalGroupingActivatedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigNumberBeamFormingCSISupportAntennaGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntennaGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigNumberCompressedBeamformingMatrixSupportAntennaGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaImplementedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivatedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigChannelEstimationCapabilityImplementedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigChannelEstimationCapabilityActivatedGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigCurrentPrimaryChannelGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigCurrentSecondaryChannelGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigSTAChannelWidthGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigSTAChannelWidthConfigGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigNonGreenfieldHTSTAsPresentGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigNonGreenfieldHTSTAsPresentConfigGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigOBSSNonHTSTAsPresentGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigOBSSNonHTSTAsPresentConfigGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigDualBeaconGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigDualBeaconConfigGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigSTBCBeaconGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigSTBCBeaconConfigGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigPCOPhaseGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigPCOPhaseConfigGet(tSnmpIndex *, tRetVal *);

INT4 FsDot11nConfigMIMOPowerSaveSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigDelayedBlockAckOptionActivatedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigMaxAMSDULengthConfigSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigMaxRxAMPDUFactorConfigSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigMinimumMPDUStartSpacingConfigSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigPCOOptionActivatedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigTransitionTimeConfigSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigMCSFeedbackOptionActivatedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigHTControlFieldSupportedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigRDResponderOptionActivatedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigtHTDSSCCKModein40MHzConfigSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigTxRxMCSSetNotEqualSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigTxMaximumNumberSpatialStreamsSupportedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigTxUnequalModulationSupportedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigImplicitTransmitBeamformingRecvOptionActivatedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigReceiveStaggerSoundingOptionActivatedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigTransmitStaggerSoundingOptionActivatedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigReceiveNDPOptionActivatedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigTransmitNDPOptionActivatedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigImplicitTransmitBeamformingOptionActivatedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigCalibrationOptionActivatedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigExplicitCSITransmitBeamformingOptionActivatedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivatedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivatedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivatedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivatedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivatedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigMinimalGroupingActivatedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigNumberBeamFormingCSISupportAntennaSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntennaSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigNumberCompressedBeamformingMatrixSupportAntennaSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivatedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigChannelEstimationCapabilityActivatedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigCurrentPrimaryChannelSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigCurrentSecondaryChannelSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigSTAChannelWidthConfigSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigNonGreenfieldHTSTAsPresentConfigSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigOBSSNonHTSTAsPresentConfigSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigDualBeaconConfigSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigSTBCBeaconConfigSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigPCOPhaseConfigSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigRxSTBCOptionActivatedSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigRxSTBCOptionActivatedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigMIMOPowerSaveTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigDelayedBlockAckOptionActivatedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigMaxAMSDULengthConfigTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigMaxRxAMPDUFactorConfigTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigMinimumMPDUStartSpacingConfigTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigPCOOptionActivatedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigRxSTBCOptionImplementedGet(tSnmpIndex *, tRetVal *); 
INT4 FsDot11nConfigRxSTBCOptionActivatedGet(tSnmpIndex *, tRetVal *); 
INT4 FsDot11nConfigTransitionTimeConfigTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigMCSFeedbackOptionActivatedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigHTControlFieldSupportedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigRDResponderOptionActivatedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigtHTDSSCCKModein40MHzConfigTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigTxRxMCSSetNotEqualTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigTxMaximumNumberSpatialStreamsSupportedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigTxUnequalModulationSupportedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigImplicitTransmitBeamformingRecvOptionActivatedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigReceiveStaggerSoundingOptionActivatedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigTransmitStaggerSoundingOptionActivatedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigReceiveNDPOptionActivatedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigTransmitNDPOptionActivatedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigImplicitTransmitBeamformingOptionActivatedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigCalibrationOptionActivatedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigExplicitCSITransmitBeamformingOptionActivatedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivatedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivatedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivatedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivatedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivatedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigMinimalGroupingActivatedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigNumberBeamFormingCSISupportAntennaTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntennaTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigNumberCompressedBeamformingMatrixSupportAntennaTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivatedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigChannelEstimationCapabilityActivatedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigCurrentPrimaryChannelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigCurrentSecondaryChannelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigSTAChannelWidthConfigTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigNonGreenfieldHTSTAsPresentConfigTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigOBSSNonHTSTAsPresentConfigTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigDualBeaconConfigTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigSTBCBeaconConfigTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11nConfigPCOPhaseConfigTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsDot11DscpConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDot11DscpInPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11OutDscpGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11DscpRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDot11DscpInPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsDot11OutDscpSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11DscpRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDot11DscpInPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11OutDscpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11DscpRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDot11DscpConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

#endif
