/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: wsscfgprot.h,v 1.3 2017/11/24 10:37:08 siva Exp $
 *
 * Description: This file contains declaration of function 
 *              prototypes of wsscfg module.
 *******************************************************************/

#ifndef __WSSCFGPROT_H__
#define __WSSCFGPROT_H__
#include "wsscfgtmr.h"
#include "wssifpmdb.h"
#include "wssifinc.h"
/* Add Prototypes here */

/**************************wsscfgmain.c*******************************/
PUBLIC UINT4 WsscfgMainTask PROTO(
     (VOID));
PUBLIC UINT4 WsscfgMainTaskInit PROTO(
     (VOID));
PUBLIC VOID WsscfgMainDeInit PROTO(
     (VOID));
PUBLIC INT4 WsscfgMainTaskLock PROTO(
     (VOID));
PUBLIC INT4 WsscfgMainTaskUnLock PROTO(
     (VOID));

VOID WsscfgInitProfileTables(
    VOID);
INT4 WsscfgInitQAPProfileTable(
    VOID);



INT4 WssCfgLock(
    VOID);
INT4 WssCfgUnLock(
    VOID);


/**************************wsscfgque.c********************************/
PUBLIC VOID WsscfgQueProcessMsgs PROTO(
     (VOID));

/**************************wsscfgpkt.c*******************************/
/**************************wsscfgtmr.c*******************************/
PUBLIC VOID WsscfgTmrInitTmrDesc PROTO(
     (VOID));
PUBLIC VOID WsscfgTmrStartTmr PROTO(
     (tWsscfgTmr * pWsscfgTmr,
      enWsscfgTmrId eWsscfgTmrId,
      UINT4 u4Secs));
PUBLIC VOID WsscfgTmrRestartTmr PROTO(
     (tWsscfgTmr * pWsscfgTmr,
      enWsscfgTmrId eWsscfgTmrId,
      UINT4 u4Secs));
PUBLIC VOID WsscfgTmrStopTmr PROTO(
     (tWsscfgTmr * pWsscfgTmr));
PUBLIC VOID WsscfgTmrHandleExpiry PROTO(
     (VOID));

/**************************wsscfgtrc.c*******************************/


CHR1 *WsscfgTrc PROTO(
     (UINT4 u4Trc,
      const char *fmt,
      ...));
VOID WsscfgTrcPrint PROTO(
     (const char *fname,
      UINT4 u4Line,
      const char *s));
VOID WsscfgTrcWrite PROTO(
     (CHR1 * s));

/**************************wsscfgtask.c*******************************/

PUBLIC VOID WsscfgTaskSpawnWsscfgTask PROTO(
     (INT1 * pi1Arg));
PUBLIC INT4 WsscfgTaskRegisterWsscfgMibs(
    VOID);

INT4 WssCfgGetWtpDot11StatsEntry(
    tWssIfPMDB * pWssIfPMDB);

INT4 WssCfgGetSSIDStatsEntry(
    tWssIfPMDB * pWssIfPMDB);

INT4 WssCfgGetRadioStatsEntry(
    tWssIfPMDB * pWssIfPMDB);

INT4 WssCfgGetClientStatsEntry(
    tWssIfPMDB * pWssIfPMDB);

INT4 WssCfgGetWlanStatistics(UINT2,
    tWssIfPMDB * pWssIfPMDB);
INT4
WssCfgMaxClientCountCheck (UINT4 u4RadioIndex , UINT4 u4FsWlanRadioMaxClientCount);
INT4
WssCfgMaxClientCountCheckForSSID (UINT2 u2WlanProfileId , UINT4 u4FsWlanMaxClientCount);
INT4
WsscfgGetWebAuthRedirectFileName(tWssWlanDB *pwssWlanDB);

VOID
WssCfgSendDeAuthForRadio(UINT1 * au1StaMac, UINT4 u4RadioIfIndex);
INT4
WsscfgSetWebAuthRedirectFileName(tWssWlanDB *pwssWlanDB);

INT4 WsscfgGetLoginAuthentication(tWssWlanDB *pwssWlanDB);
INT4 WsscfgSetLoginAuthentication(tWssWlanDB *pwssWlanDB);

#endif                          /* __WSSCFGPROT_H__ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  wsscfgprot.h                     */
/*-----------------------------------------------------------------------*/
