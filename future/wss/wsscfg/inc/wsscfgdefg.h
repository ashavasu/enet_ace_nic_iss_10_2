/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
 *  $Id: wsscfgdefg.h,v 1.3 2017/11/24 10:37:07 siva Exp $
*
* Description: Macros used to fill the CLI structure and 
              index value in the respective structure.
*********************************************************************/


#define WSSCFG_DOT11STATIONCONFIGTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11STATIONCONFIGTABLE_SIZING_ID]


#define WSSCFG_DOT11STATIONCONFIGTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11STATIONCONFIGTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11AUTHENTICATIONALGORITHMSTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11AUTHENTICATIONALGORITHMSTABLE_SIZING_ID]


#define WSSCFG_DOT11AUTHENTICATIONALGORITHMSTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11AUTHENTICATIONALGORITHMSTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11WEPDEFAULTKEYSTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11WEPDEFAULTKEYSTABLE_SIZING_ID]


#define WSSCFG_DOT11WEPDEFAULTKEYSTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11WEPDEFAULTKEYSTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11WEPKEYMAPPINGSTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11WEPKEYMAPPINGSTABLE_SIZING_ID]


#define WSSCFG_DOT11WEPKEYMAPPINGSTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11WEPKEYMAPPINGSTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11PRIVACYTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11PRIVACYTABLE_SIZING_ID]


#define WSSCFG_DOT11PRIVACYTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11PRIVACYTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11MULTIDOMAINCAPABILITYTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11MULTIDOMAINCAPABILITYTABLE_SIZING_ID]


#define WSSCFG_DOT11MULTIDOMAINCAPABILITYTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11MULTIDOMAINCAPABILITYTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11SPECTRUMMANAGEMENTTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11SPECTRUMMANAGEMENTTABLE_SIZING_ID]


#define WSSCFG_DOT11SPECTRUMMANAGEMENTTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11SPECTRUMMANAGEMENTTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11REGULATORYCLASSESTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11REGULATORYCLASSESTABLE_SIZING_ID]


#define WSSCFG_DOT11REGULATORYCLASSESTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11REGULATORYCLASSESTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11OPERATIONTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11OPERATIONTABLE_SIZING_ID]


#define WSSCFG_DOT11OPERATIONTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11OPERATIONTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11COUNTERSTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11COUNTERSTABLE_SIZING_ID]


#define WSSCFG_DOT11COUNTERSTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11COUNTERSTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11GROUPADDRESSESTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11GROUPADDRESSESTABLE_SIZING_ID]


#define WSSCFG_DOT11GROUPADDRESSESTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11GROUPADDRESSESTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11EDCATABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11EDCATABLE_SIZING_ID]


#define WSSCFG_DOT11EDCATABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11EDCATABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11QAPEDCATABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11QAPEDCATABLE_SIZING_ID]


#define WSSCFG_DOT11QAPEDCATABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11QAPEDCATABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11QOSCOUNTERSTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11QOSCOUNTERSTABLE_SIZING_ID]


#define WSSCFG_DOT11QOSCOUNTERSTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11QOSCOUNTERSTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11RESOURCEINFOTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11RESOURCEINFOTABLE_SIZING_ID]


#define WSSCFG_DOT11RESOURCEINFOTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11RESOURCEINFOTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11PHYOPERATIONTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11PHYOPERATIONTABLE_SIZING_ID]


#define WSSCFG_DOT11PHYOPERATIONTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11PHYOPERATIONTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11PHYANTENNATABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11PHYANTENNATABLE_SIZING_ID]


#define WSSCFG_DOT11PHYANTENNATABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11PHYANTENNATABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11PHYTXPOWERTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11PHYTXPOWERTABLE_SIZING_ID]


#define WSSCFG_DOT11PHYTXPOWERTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11PHYTXPOWERTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11PHYFHSSTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11PHYFHSSTABLE_SIZING_ID]


#define WSSCFG_DOT11PHYFHSSTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11PHYFHSSTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11PHYDSSSTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11PHYDSSSTABLE_SIZING_ID]


#define WSSCFG_DOT11PHYDSSSTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11PHYDSSSTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11PHYIRTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11PHYIRTABLE_SIZING_ID]


#define WSSCFG_DOT11PHYIRTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11PHYIRTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11REGDOMAINSSUPPORTEDTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11REGDOMAINSSUPPORTEDTABLE_SIZING_ID]


#define WSSCFG_DOT11REGDOMAINSSUPPORTEDTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11REGDOMAINSSUPPORTEDTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11ANTENNASLISTTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11ANTENNASLISTTABLE_SIZING_ID]


#define WSSCFG_DOT11ANTENNASLISTTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11ANTENNASLISTTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11SUPPORTEDDATARATESTXTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11SUPPORTEDDATARATESTXTABLE_SIZING_ID]


#define WSSCFG_DOT11SUPPORTEDDATARATESTXTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11SUPPORTEDDATARATESTXTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11SUPPORTEDDATARATESRXTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11SUPPORTEDDATARATESRXTABLE_SIZING_ID]


#define WSSCFG_DOT11SUPPORTEDDATARATESRXTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11SUPPORTEDDATARATESRXTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11PHYOFDMTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11PHYOFDMTABLE_SIZING_ID]


#define WSSCFG_DOT11PHYOFDMTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11PHYOFDMTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11PHYHRDSSSTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11PHYHRDSSSTABLE_SIZING_ID]


#define WSSCFG_DOT11PHYHRDSSSTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11PHYHRDSSSTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11HOPPINGPATTERNTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11HOPPINGPATTERNTABLE_SIZING_ID]


#define WSSCFG_DOT11HOPPINGPATTERNTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11HOPPINGPATTERNTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11PHYERPTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11PHYERPTABLE_SIZING_ID]


#define WSSCFG_DOT11PHYERPTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11PHYERPTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11RSNACONFIGTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11RSNACONFIGTABLE_SIZING_ID]


#define WSSCFG_DOT11RSNACONFIGTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11RSNACONFIGTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11RSNACONFIGPAIRWISECIPHERSTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11RSNACONFIGPAIRWISECIPHERSTABLE_SIZING_ID]


#define WSSCFG_DOT11RSNACONFIGPAIRWISECIPHERSTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11RSNACONFIGPAIRWISECIPHERSTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11RSNACONFIGAUTHENTICATIONSUITESTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11RSNACONFIGAUTHENTICATIONSUITESTABLE_SIZING_ID]


#define WSSCFG_DOT11RSNACONFIGAUTHENTICATIONSUITESTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11RSNACONFIGAUTHENTICATIONSUITESTABLE_ISSET_SIZING_ID]


#define WSSCFG_DOT11RSNASTATSTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11RSNASTATSTABLE_SIZING_ID]


#define WSSCFG_DOT11RSNASTATSTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_DOT11RSNASTATSTABLE_ISSET_SIZING_ID]


#define WSSCFG_CAPWAPDOT11WLANTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_CAPWAPDOT11WLANTABLE_SIZING_ID]


#define WSSCFG_CAPWAPDOT11WLANTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_CAPWAPDOT11WLANTABLE_ISSET_SIZING_ID]


#define WSSCFG_CAPWAPDOT11WLANBINDTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_CAPWAPDOT11WLANBINDTABLE_SIZING_ID]


#define WSSCFG_CAPWAPDOT11WLANBINDTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_CAPWAPDOT11WLANBINDTABLE_ISSET_SIZING_ID]

#define WSSCFG_FSDOT11STATIONCONFIGTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11STATIONCONFIGTABLE_SIZING_ID]


#define WSSCFG_FSDOT11STATIONCONFIGTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11STATIONCONFIGTABLE_ISSET_SIZING_ID]
#define WSSCFG_FSDOT11EXTERNALWEBAUTHPROFILETABLE_POOLID WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11EXTERNALWEBAUTHPROFILETABLE_SIZING_ID]

#define WSSCFG_FSDOT11EXTERNALWEBAUTHPROFILETABLE_ISSET_POOLID WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11EXTERNALWEBAUTHPROFILETABLE_ISSET_SIZING_ID]
#define WSSCFG_FSDOT11CAPABILITYPROFILETABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11CAPABILITYPROFILETABLE_SIZING_ID]


#define WSSCFG_FSDOT11CAPABILITYPROFILETABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11CAPABILITYPROFILETABLE_ISSET_SIZING_ID]


#define WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_SIZING_ID]


#define WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11AUTHENTICATIONPROFILETABLE_ISSET_SIZING_ID]


#define WSSCFG_FSSTATIONQOSPARAMSTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSSTATIONQOSPARAMSTABLE_SIZING_ID]


#define WSSCFG_FSSTATIONQOSPARAMSTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSSTATIONQOSPARAMSTABLE_ISSET_SIZING_ID]


#define WSSCFG_FSVLANISOLATIONTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSVLANISOLATIONTABLE_SIZING_ID]


#define WSSCFG_FSVLANISOLATIONTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSVLANISOLATIONTABLE_ISSET_SIZING_ID]


#define WSSCFG_FSDOT11RADIOCONFIGTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11RADIOCONFIGTABLE_SIZING_ID]


#define WSSCFG_FSDOT11RADIOCONFIGTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11RADIOCONFIGTABLE_ISSET_SIZING_ID]


#define WSSCFG_FSDOT11QOSPROFILETABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11QOSPROFILETABLE_SIZING_ID]


#define WSSCFG_FSDOT11QOSPROFILETABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11QOSPROFILETABLE_ISSET_SIZING_ID]


#define WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_SIZING_ID]


#define WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11WLANCAPABILITYPROFILETABLE_ISSET_SIZING_ID]


#define WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_SIZING_ID]


#define WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11WLANAUTHENTICATIONPROFILETABLE_ISSET_SIZING_ID]


#define WSSCFG_FSDOT11WLANQOSPROFILETABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11WLANQOSPROFILETABLE_SIZING_ID]


#define WSSCFG_FSDOT11WLANQOSPROFILETABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11WLANQOSPROFILETABLE_ISSET_SIZING_ID]


#define WSSCFG_FSDOT11RADIOQOSTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11RADIOQOSTABLE_SIZING_ID]


#define WSSCFG_FSDOT11RADIOQOSTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11RADIOQOSTABLE_ISSET_SIZING_ID]


#define WSSCFG_FSDOT11QAPTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11QAPTABLE_SIZING_ID]


#define WSSCFG_FSDOT11QAPTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11QAPTABLE_ISSET_SIZING_ID]


#define WSSCFG_FSQAPPROFILETABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSQAPPROFILETABLE_SIZING_ID]


#define WSSCFG_FSQAPPROFILETABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSQAPPROFILETABLE_ISSET_SIZING_ID]


#define WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_SIZING_ID]


#define WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11CAPABILITYMAPPINGTABLE_ISSET_SIZING_ID]


#define WSSCFG_FSDOT11AUTHMAPPINGTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11AUTHMAPPINGTABLE_SIZING_ID]


#define WSSCFG_FSDOT11AUTHMAPPINGTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11AUTHMAPPINGTABLE_ISSET_SIZING_ID]


#define WSSCFG_FSDOT11QOSMAPPINGTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11QOSMAPPINGTABLE_SIZING_ID]


#define WSSCFG_FSDOT11QOSMAPPINGTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11QOSMAPPINGTABLE_ISSET_SIZING_ID]


#define WSSCFG_FSDOT11CLIENTSUMMARYTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11CLIENTSUMMARYTABLE_SIZING_ID]


#define WSSCFG_FSDOT11CLIENTSUMMARYTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11CLIENTSUMMARYTABLE_ISSET_SIZING_ID]


#define WSSCFG_FSDOT11ANTENNASLISTTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11ANTENNASLISTTABLE_SIZING_ID]


#define WSSCFG_FSDOT11ANTENNASLISTTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11ANTENNASLISTTABLE_ISSET_SIZING_ID]


#define WSSCFG_FSDOT11WLANTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11WLANTABLE_SIZING_ID]


#define WSSCFG_FSDOT11WLANTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11WLANTABLE_ISSET_SIZING_ID]


#define WSSCFG_FSDOT11WLANBINDTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11WLANBINDTABLE_SIZING_ID]


#define WSSCFG_FSDOT11WLANBINDTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11WLANBINDTABLE_ISSET_SIZING_ID]


#define WSSCFG_FSDOT11NCONFIGTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11NCONFIGTABLE_SIZING_ID]


#define WSSCFG_FSDOT11NCONFIGTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11NCONFIGTABLE_ISSET_SIZING_ID]


#define WSSCFG_FSDOT11NMCSDATARATETABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11NMCSDATARATETABLE_SIZING_ID]


#define WSSCFG_FSDOT11NMCSDATARATETABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSDOT11NMCSDATARATETABLE_ISSET_SIZING_ID]


#define WSSCFG_FSWTPIMAGEUPGRADETABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSWTPIMAGEUPGRADETABLE_SIZING_ID]


#define WSSCFG_FSWTPIMAGEUPGRADETABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSWTPIMAGEUPGRADETABLE_ISSET_SIZING_ID]

#define WSSCFG_FSRRMCONFIGTABLE_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSRRMCONFIGTABLE_SIZING_ID]


#define WSSCFG_FSRRMCONFIGTABLE_ISSET_POOLID   WSSCFGMemPoolIds[MAX_WSSCFG_FSRRMCONFIGTABLE_ISSET_SIZING_ID]

/*AP Group*/
#define MAX_AP_GROUP_WLAN  8
#define APGROUP_CONFIG_ENTRY_MEMPOOL_ID   WSSCFGMemPoolIds[ MAX_WSSCFG_APGROUPTABLE_SIZING_ID]

#define APGROUP_MEMORY_TYPE      0x00
#define APGROUP_CREATE_MEM_POOL(x,y,pPoolId)\
           MemCreateMemPool(x,y,APGROUP_MEMORY_TYPE,(tMemPoolId*)pPoolId)

#define APGROUP_DELETE_MEM_POOL(pPoolId)\
           MemDeleteMemPool((tMemPoolId) pPoolId)

#define APGROUP_MEM_ALLOCATE_MEM_BLK(APGROUP_CONFIG_ENTRY_MEMPOOL_ID)\
           MemAllocMemBlk(APGROUP_CONFIG_ENTRY_MEMPOOL_ID)

/*Macros for releasing memory block*/

#define APGROUP_MEM_RELEASE_MEM_BLK(APGROUP_CONFIG_ENTRY_MEMPOOL_ID,pNode)\
          MemReleaseMemBlock(APGROUP_CONFIG_ENTRY_MEMPOOL_ID,\
                             (UINT1 *)pNode)
/*ApGroupWTP Table */
#define APGROUP_CONFIG_WTP_ENTRY_MEMPOOL_ID WSSCFGMemPoolIds[MAX_WSSCFG_APGROUPWTPTABLE_SIZING_ID]

#define APGROUPWTP_MEMORY_TYPE      0x00
#define APGROUPWTP_CREATE_MEM_POOL(x,y,pPoolId)\
           MemCreateMemPool(x,y,APGROUPWTP_MEMORY_TYPE,(tMemPoolId*)pPoolId)

#define APGROUPWTP_DELETE_MEM_POOL(pPoolId)\
           MemDeleteMemPool((tMemPoolId) pPoolId)

#define APGROUPWTP_MEM_ALLOCATE_MEM_BLK(APGROUP_CONFIG_WTP_ENTRY_MEMPOOL_ID)\
           MemAllocMemBlk(APGROUP_CONFIG_WTP_ENTRY_MEMPOOL_ID)

/*Macros for releasing memory block*/

#define APGROUPWTP_MEM_RELEASE_MEM_BLK(APGROUP_CONFIG_WTP_ENTRY_MEMPOOL_ID,pNode)\
          MemReleaseMemBlock(APGROUP_CONFIG_WTP_ENTRY_MEMPOOL_ID,\
                             (UINT1 *)pNode)
/*ApGroupWLAN Table*/
#define APGROUP_CONFIG_WLAN_ENTRY_MEMPOOL_ID WSSCFGMemPoolIds[MAX_WSSCFG_APGROUPWLANTABLE_SIZING_ID]

#define APGROUPWLAN_MEMORY_TYPE      0x00
#define APGROUPWLAN_CREATE_MEM_POOL(x,y,pPoolId)\
           MemCreateMemPool(x,y,APGROUPWLAN_MEMORY_TYPE,(tMemPoolId*)pPoolId)

#define APGROUPWLAN_DELETE_MEM_POOL(pPoolId)\
           MemDeleteMemPool((tMemPoolId) pPoolId)

#define APGROUPWLAN_MEM_ALLOCATE_MEM_BLK(APGROUP_CONFIG_WLAN_ENTRY_MEMPOOL_ID)\
           MemAllocMemBlk(APGROUP_CONFIG_WLAN_ENTRY_MEMPOOL_ID)

/*Macros for releasing memory block*/

#define APGROUPWLAN_MEM_RELEASE_MEM_BLK(APGROUP_CONFIG_WLAN_ENTRY_MEMPOOL_ID,pNode)\
          MemReleaseMemBlock(APGROUP_CONFIG_WLAN_ENTRY_MEMPOOL_ID,\
                             (UINT1 *)pNode)
/* Macro used to fill the CLI structure for Dot11StationConfigEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11STATIONCONFIGTABLE_ARGS(pWsscfgDot11StationConfigEntry,\
   pWsscfgIsSetDot11StationConfigEntry,\
   pau1Dot11StationID,\
   pau1Dot11MediumOccupancyLimit,\
   pau1Dot11CFPPeriod,\
   pau1Dot11CFPMaxDuration,\
   pau1Dot11AuthenticationResponseTimeOut,\
   pau1Dot11PowerManagementMode,\
   pau1Dot11DesiredSSID,\
   pau1Dot11DesiredSSIDLen,\
   pau1Dot11DesiredBSSType,\
   pau1Dot11OperationalRateSet,\
   pau1Dot11OperationalRateSetLen,\
   pau1Dot11BeaconPeriod,\
   pau1Dot11DTIMPeriod,\
   pau1Dot11AssociationResponseTimeOut,\
   pau1Dot11MultiDomainCapabilityImplemented,\
   pau1Dot11MultiDomainCapabilityEnabled,\
   pau1Dot11SpectrumManagementRequired,\
   pau1Dot11RegulatoryClassesImplemented,\
   pau1Dot11RegulatoryClassesRequired,\
   pau1Dot11AssociateinNQBSS,\
   pau1Dot11DLSAllowedInQBSS,\
   pau1Dot11DLSAllowed,\
   pau1IfIndex)\
  {\
  if (pau1Dot11StationID != NULL)\
  {\
   StrToMac ((UINT1 *) &pau1Dot11StationID, pWsscfgDot11StationConfigEntry->MibObject.Dot11StationID);\
   pWsscfgIsSetDot11StationConfigEntry->bDot11StationID = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11StationConfigEntry->bDot11StationID = OSIX_FALSE;\
  }\
  if (pau1Dot11MediumOccupancyLimit != NULL)\
  {\
   pWsscfgDot11StationConfigEntry->MibObject.i4Dot11MediumOccupancyLimit = *(INT4 *) (pau1Dot11MediumOccupancyLimit);\
   pWsscfgIsSetDot11StationConfigEntry->bDot11MediumOccupancyLimit = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11StationConfigEntry->bDot11MediumOccupancyLimit = OSIX_FALSE;\
  }\
  if (pau1Dot11CFPPeriod != NULL)\
  {\
   pWsscfgDot11StationConfigEntry->MibObject.i4Dot11CFPPeriod = *(INT4 *) (pau1Dot11CFPPeriod);\
   pWsscfgIsSetDot11StationConfigEntry->bDot11CFPPeriod = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11StationConfigEntry->bDot11CFPPeriod = OSIX_FALSE;\
  }\
  if (pau1Dot11CFPMaxDuration != NULL)\
  {\
   pWsscfgDot11StationConfigEntry->MibObject.i4Dot11CFPMaxDuration = *(INT4 *) (pau1Dot11CFPMaxDuration);\
   pWsscfgIsSetDot11StationConfigEntry->bDot11CFPMaxDuration = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11StationConfigEntry->bDot11CFPMaxDuration = OSIX_FALSE;\
  }\
  if (pau1Dot11AuthenticationResponseTimeOut != NULL)\
  {\
   pWsscfgDot11StationConfigEntry->MibObject.u4Dot11AuthenticationResponseTimeOut = *(UINT4 *) (pau1Dot11AuthenticationResponseTimeOut);\
   pWsscfgIsSetDot11StationConfigEntry->bDot11AuthenticationResponseTimeOut = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11StationConfigEntry->bDot11AuthenticationResponseTimeOut = OSIX_FALSE;\
  }\
  if (pau1Dot11PowerManagementMode != NULL)\
  {\
   pWsscfgDot11StationConfigEntry->MibObject.i4Dot11PowerManagementMode = *(INT4 *) (pau1Dot11PowerManagementMode);\
   pWsscfgIsSetDot11StationConfigEntry->bDot11PowerManagementMode = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11StationConfigEntry->bDot11PowerManagementMode = OSIX_FALSE;\
  }\
  if (pau1Dot11DesiredSSID != NULL)\
  {\
   MEMCPY (pWsscfgDot11StationConfigEntry->MibObject.au1Dot11DesiredSSID, pau1Dot11DesiredSSID, *(INT4 *)pau1Dot11DesiredSSIDLen);\
   pWsscfgDot11StationConfigEntry->MibObject.i4Dot11DesiredSSIDLen = *(INT4 *)pau1Dot11DesiredSSIDLen;\
   pWsscfgIsSetDot11StationConfigEntry->bDot11DesiredSSID = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11StationConfigEntry->bDot11DesiredSSID = OSIX_FALSE;\
  }\
  if (pau1Dot11DesiredBSSType != NULL)\
  {\
   pWsscfgDot11StationConfigEntry->MibObject.i4Dot11DesiredBSSType = *(INT4 *) (pau1Dot11DesiredBSSType);\
   pWsscfgIsSetDot11StationConfigEntry->bDot11DesiredBSSType = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11StationConfigEntry->bDot11DesiredBSSType = OSIX_FALSE;\
  }\
                if (pau1Dot11OperationalRateSetLen != NULL)\
                {\
              if (pau1Dot11OperationalRateSet != NULL)\
       {\
   MEMCPY (pWsscfgDot11StationConfigEntry->MibObject.au1Dot11OperationalRateSet, pau1Dot11OperationalRateSet, *(INT4 *)pau1Dot11OperationalRateSetLen);\
   pWsscfgDot11StationConfigEntry->MibObject.i4Dot11OperationalRateSetLen = *(INT4 *)pau1Dot11OperationalRateSetLen;\
   pWsscfgIsSetDot11StationConfigEntry->bDot11OperationalRateSet = OSIX_TRUE;\
                     }\
  }\
  else\
  {\
   pWsscfgIsSetDot11StationConfigEntry->bDot11OperationalRateSet = OSIX_FALSE;\
  }\
  if (pau1Dot11BeaconPeriod != NULL)\
  {\
   pWsscfgDot11StationConfigEntry->MibObject.i4Dot11BeaconPeriod = *(INT4 *) (pau1Dot11BeaconPeriod);\
   pWsscfgIsSetDot11StationConfigEntry->bDot11BeaconPeriod = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11StationConfigEntry->bDot11BeaconPeriod = OSIX_FALSE;\
  }\
  if (pau1Dot11DTIMPeriod != NULL)\
  {\
   pWsscfgDot11StationConfigEntry->MibObject.i4Dot11DTIMPeriod = *(INT4 *) (pau1Dot11DTIMPeriod);\
   pWsscfgIsSetDot11StationConfigEntry->bDot11DTIMPeriod = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11StationConfigEntry->bDot11DTIMPeriod = OSIX_FALSE;\
  }\
  if (pau1Dot11AssociationResponseTimeOut != NULL)\
  {\
   pWsscfgDot11StationConfigEntry->MibObject.u4Dot11AssociationResponseTimeOut = *(UINT4 *) (pau1Dot11AssociationResponseTimeOut);\
   pWsscfgIsSetDot11StationConfigEntry->bDot11AssociationResponseTimeOut = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11StationConfigEntry->bDot11AssociationResponseTimeOut = OSIX_FALSE;\
  }\
  if (pau1Dot11MultiDomainCapabilityImplemented != NULL)\
  {\
   pWsscfgDot11StationConfigEntry->MibObject.i4Dot11MultiDomainCapabilityImplemented = *(INT4 *) (pau1Dot11MultiDomainCapabilityImplemented);\
   pWsscfgIsSetDot11StationConfigEntry->bDot11MultiDomainCapabilityImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11StationConfigEntry->bDot11MultiDomainCapabilityImplemented = OSIX_FALSE;\
  }\
  if (pau1Dot11MultiDomainCapabilityEnabled != NULL)\
  {\
   pWsscfgDot11StationConfigEntry->MibObject.i4Dot11MultiDomainCapabilityEnabled = *(INT4 *) (pau1Dot11MultiDomainCapabilityEnabled);\
   pWsscfgIsSetDot11StationConfigEntry->bDot11MultiDomainCapabilityEnabled = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11StationConfigEntry->bDot11MultiDomainCapabilityEnabled = OSIX_FALSE;\
  }\
  if (pau1Dot11SpectrumManagementRequired != NULL)\
  {\
   pWsscfgDot11StationConfigEntry->MibObject.i4Dot11SpectrumManagementRequired = *(INT4 *) (pau1Dot11SpectrumManagementRequired);\
   pWsscfgIsSetDot11StationConfigEntry->bDot11SpectrumManagementRequired = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11StationConfigEntry->bDot11SpectrumManagementRequired = OSIX_FALSE;\
  }\
  if (pau1Dot11RegulatoryClassesImplemented != NULL)\
  {\
   pWsscfgDot11StationConfigEntry->MibObject.i4Dot11RegulatoryClassesImplemented = *(INT4 *) (pau1Dot11RegulatoryClassesImplemented);\
   pWsscfgIsSetDot11StationConfigEntry->bDot11RegulatoryClassesImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11StationConfigEntry->bDot11RegulatoryClassesImplemented = OSIX_FALSE;\
  }\
  if (pau1Dot11RegulatoryClassesRequired != NULL)\
  {\
   pWsscfgDot11StationConfigEntry->MibObject.i4Dot11RegulatoryClassesRequired = *(INT4 *) (pau1Dot11RegulatoryClassesRequired);\
   pWsscfgIsSetDot11StationConfigEntry->bDot11RegulatoryClassesRequired = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11StationConfigEntry->bDot11RegulatoryClassesRequired = OSIX_FALSE;\
  }\
  if (pau1Dot11AssociateinNQBSS != NULL)\
  {\
   pWsscfgDot11StationConfigEntry->MibObject.i4Dot11AssociateinNQBSS = *(INT4 *) (pau1Dot11AssociateinNQBSS);\
   pWsscfgIsSetDot11StationConfigEntry->bDot11AssociateinNQBSS = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11StationConfigEntry->bDot11AssociateinNQBSS = OSIX_FALSE;\
  }\
  if (pau1Dot11DLSAllowedInQBSS != NULL)\
  {\
   pWsscfgDot11StationConfigEntry->MibObject.i4Dot11DLSAllowedInQBSS = *(INT4 *) (pau1Dot11DLSAllowedInQBSS);\
   pWsscfgIsSetDot11StationConfigEntry->bDot11DLSAllowedInQBSS = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11StationConfigEntry->bDot11DLSAllowedInQBSS = OSIX_FALSE;\
  }\
  if (pau1Dot11DLSAllowed != NULL)\
  {\
   pWsscfgDot11StationConfigEntry->MibObject.i4Dot11DLSAllowed = *(INT4 *) (pau1Dot11DLSAllowed);\
   pWsscfgIsSetDot11StationConfigEntry->bDot11DLSAllowed = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11StationConfigEntry->bDot11DLSAllowed = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11StationConfigEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11StationConfigEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11StationConfigEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11AuthenticationAlgorithmsEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11AUTHENTICATIONALGORITHMSTABLE_ARGS(pWsscfgDot11AuthenticationAlgorithmsEntry,\
   pWsscfgIsSetDot11AuthenticationAlgorithmsEntry,\
   pau1Dot11AuthenticationAlgorithmsIndex,\
            pau1Dot11AuthenticationAlgorithm,\
   pau1Dot11AuthenticationAlgorithmsEnable,\
   pau1IfIndex)\
  {\
  if (pau1Dot11AuthenticationAlgorithmsIndex != NULL)\
  {\
   pWsscfgDot11AuthenticationAlgorithmsEntry->MibObject.i4Dot11AuthenticationAlgorithmsIndex = *(INT4 *) (pau1Dot11AuthenticationAlgorithmsIndex);\
   pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->bDot11AuthenticationAlgorithmsIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->bDot11AuthenticationAlgorithmsIndex = OSIX_FALSE;\
  }\
  if (pau1Dot11AuthenticationAlgorithm != NULL)\
  {\
   pWsscfgDot11AuthenticationAlgorithmsEntry->MibObject.i4Dot11AuthenticationAlgorithm = *(INT4 *) (pau1Dot11AuthenticationAlgorithm);\
   pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->bDot11AuthenticationAlgorithm = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->bDot11AuthenticationAlgorithm = OSIX_FALSE;\
  }\
  if (pau1Dot11AuthenticationAlgorithmsEnable != NULL)\
  {\
   pWsscfgDot11AuthenticationAlgorithmsEntry->MibObject.i4Dot11AuthenticationAlgorithmsEnable = *(INT4 *) (pau1Dot11AuthenticationAlgorithmsEnable);\
   pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->bDot11AuthenticationAlgorithmsEnable = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->bDot11AuthenticationAlgorithmsEnable = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11AuthenticationAlgorithmsEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11AuthenticationAlgorithmsEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11WEPDefaultKeysEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11WEPDEFAULTKEYSTABLE_ARGS(pWsscfgDot11WEPDefaultKeysEntry,\
   pWsscfgIsSetDot11WEPDefaultKeysEntry,\
   pau1Dot11WEPDefaultKeyIndex,\
   pau1Dot11WEPDefaultKeyValue,\
   pau1Dot11WEPDefaultKeyValueLen,\
   pau1IfIndex)\
  {\
  if (pau1Dot11WEPDefaultKeyIndex != NULL)\
  {\
   pWsscfgDot11WEPDefaultKeysEntry->MibObject.i4Dot11WEPDefaultKeyIndex = *(INT4 *) (pau1Dot11WEPDefaultKeyIndex);\
   pWsscfgIsSetDot11WEPDefaultKeysEntry->bDot11WEPDefaultKeyIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11WEPDefaultKeysEntry->bDot11WEPDefaultKeyIndex = OSIX_FALSE;\
  }\
  if (pau1Dot11WEPDefaultKeyValue != NULL)\
  {\
   MEMCPY (pWsscfgDot11WEPDefaultKeysEntry->MibObject.au1Dot11WEPDefaultKeyValue, pau1Dot11WEPDefaultKeyValue, *(INT4 *)pau1Dot11WEPDefaultKeyValueLen);\
   pWsscfgDot11WEPDefaultKeysEntry->MibObject.i4Dot11WEPDefaultKeyValueLen = *(INT4 *)pau1Dot11WEPDefaultKeyValueLen;\
   pWsscfgIsSetDot11WEPDefaultKeysEntry->bDot11WEPDefaultKeyValue = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11WEPDefaultKeysEntry->bDot11WEPDefaultKeyValue = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11WEPDefaultKeysEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11WEPDefaultKeysEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11WEPDefaultKeysEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11WEPKeyMappingsEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11WEPKEYMAPPINGSTABLE_ARGS(pWsscfgDot11WEPKeyMappingsEntry,\
   pWsscfgIsSetDot11WEPKeyMappingsEntry,\
   pau1Dot11WEPKeyMappingIndex,\
   pau1Dot11WEPKeyMappingAddress,\
   pau1Dot11WEPKeyMappingWEPOn,\
   pau1Dot11WEPKeyMappingValue,\
   pau1Dot11WEPKeyMappingValueLen,\
   pau1Dot11WEPKeyMappingStatus,\
   pau1IfIndex)\
  {\
  if (pau1Dot11WEPKeyMappingIndex != NULL)\
  {\
   pWsscfgDot11WEPKeyMappingsEntry->MibObject.i4Dot11WEPKeyMappingIndex = *(INT4 *) (pau1Dot11WEPKeyMappingIndex);\
   pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingIndex = OSIX_FALSE;\
  }\
  if (pau1Dot11WEPKeyMappingAddress != NULL)\
  {\
   StrToMac ((UINT1 *) &pau1Dot11WEPKeyMappingAddress, pWsscfgDot11WEPKeyMappingsEntry->MibObject.Dot11WEPKeyMappingAddress);\
   pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingAddress = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingAddress = OSIX_FALSE;\
  }\
  if (pau1Dot11WEPKeyMappingWEPOn != NULL)\
  {\
   pWsscfgDot11WEPKeyMappingsEntry->MibObject.i4Dot11WEPKeyMappingWEPOn = *(INT4 *) (pau1Dot11WEPKeyMappingWEPOn);\
   pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingWEPOn = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingWEPOn = OSIX_FALSE;\
  }\
  if (pau1Dot11WEPKeyMappingValue != NULL)\
  {\
   MEMCPY (pWsscfgDot11WEPKeyMappingsEntry->MibObject.au1Dot11WEPKeyMappingValue, pau1Dot11WEPKeyMappingValue, *(INT4 *)pau1Dot11WEPKeyMappingValueLen);\
   pWsscfgDot11WEPKeyMappingsEntry->MibObject.i4Dot11WEPKeyMappingValueLen = *(INT4 *)pau1Dot11WEPKeyMappingValueLen;\
   pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingValue = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingValue = OSIX_FALSE;\
  }\
  if (pau1Dot11WEPKeyMappingStatus != NULL)\
  {\
   pWsscfgDot11WEPKeyMappingsEntry->MibObject.i4Dot11WEPKeyMappingStatus = *(INT4 *) (pau1Dot11WEPKeyMappingStatus);\
   pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11WEPKeyMappingsEntry->bDot11WEPKeyMappingStatus = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11WEPKeyMappingsEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11WEPKeyMappingsEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11WEPKeyMappingsEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11PrivacyEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11PRIVACYTABLE_ARGS(pWsscfgDot11PrivacyEntry,\
   pWsscfgIsSetDot11PrivacyEntry,\
   pau1Dot11PrivacyInvoked,\
   pau1Dot11WEPDefaultKeyID,\
   pau1Dot11WEPKeyMappingLength,\
   pau1Dot11ExcludeUnencrypted,\
   pau1Dot11RSNAEnabled,\
   pau1Dot11RSNAPreauthenticationEnabled,\
   pau1IfIndex)\
  {\
  if (pau1Dot11PrivacyInvoked != NULL)\
  {\
   pWsscfgDot11PrivacyEntry->MibObject.i4Dot11PrivacyInvoked = *(INT4 *) (pau1Dot11PrivacyInvoked);\
   pWsscfgIsSetDot11PrivacyEntry->bDot11PrivacyInvoked = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PrivacyEntry->bDot11PrivacyInvoked = OSIX_FALSE;\
  }\
  if (pau1Dot11WEPDefaultKeyID != NULL)\
  {\
   pWsscfgDot11PrivacyEntry->MibObject.i4Dot11WEPDefaultKeyID = *(INT4 *) (pau1Dot11WEPDefaultKeyID);\
   pWsscfgIsSetDot11PrivacyEntry->bDot11WEPDefaultKeyID = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PrivacyEntry->bDot11WEPDefaultKeyID = OSIX_FALSE;\
  }\
  if (pau1Dot11WEPKeyMappingLength != NULL)\
  {\
   pWsscfgDot11PrivacyEntry->MibObject.u4Dot11WEPKeyMappingLength = *(UINT4 *) (pau1Dot11WEPKeyMappingLength);\
   pWsscfgIsSetDot11PrivacyEntry->bDot11WEPKeyMappingLength = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PrivacyEntry->bDot11WEPKeyMappingLength = OSIX_FALSE;\
  }\
  if (pau1Dot11ExcludeUnencrypted != NULL)\
  {\
   pWsscfgDot11PrivacyEntry->MibObject.i4Dot11ExcludeUnencrypted = *(INT4 *) (pau1Dot11ExcludeUnencrypted);\
   pWsscfgIsSetDot11PrivacyEntry->bDot11ExcludeUnencrypted = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PrivacyEntry->bDot11ExcludeUnencrypted = OSIX_FALSE;\
  }\
  if (pau1Dot11RSNAEnabled != NULL)\
  {\
   pWsscfgDot11PrivacyEntry->MibObject.i4Dot11RSNAEnabled = *(INT4 *) (pau1Dot11RSNAEnabled);\
   pWsscfgIsSetDot11PrivacyEntry->bDot11RSNAEnabled = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PrivacyEntry->bDot11RSNAEnabled = OSIX_FALSE;\
  }\
  if (pau1Dot11RSNAPreauthenticationEnabled != NULL)\
  {\
   pWsscfgDot11PrivacyEntry->MibObject.i4Dot11RSNAPreauthenticationEnabled = *(INT4 *) (pau1Dot11RSNAPreauthenticationEnabled);\
   pWsscfgIsSetDot11PrivacyEntry->bDot11RSNAPreauthenticationEnabled = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PrivacyEntry->bDot11RSNAPreauthenticationEnabled = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11PrivacyEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11PrivacyEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PrivacyEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11MultiDomainCapabilityEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11MULTIDOMAINCAPABILITYTABLE_ARGS(pWsscfgDot11MultiDomainCapabilityEntry,\
   pWsscfgIsSetDot11MultiDomainCapabilityEntry,\
   pau1Dot11MultiDomainCapabilityIndex,\
   pau1Dot11FirstChannelNumber,\
   pau1Dot11NumberofChannels,\
   pau1Dot11MaximumTransmitPowerLevel,\
   pau1IfIndex)\
  {\
  if (pau1Dot11MultiDomainCapabilityIndex != NULL)\
  {\
   pWsscfgDot11MultiDomainCapabilityEntry->MibObject.i4Dot11MultiDomainCapabilityIndex = *(INT4 *) (pau1Dot11MultiDomainCapabilityIndex);\
   pWsscfgIsSetDot11MultiDomainCapabilityEntry->bDot11MultiDomainCapabilityIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11MultiDomainCapabilityEntry->bDot11MultiDomainCapabilityIndex = OSIX_FALSE;\
  }\
  if (pau1Dot11FirstChannelNumber != NULL)\
  {\
   pWsscfgDot11MultiDomainCapabilityEntry->MibObject.i4Dot11FirstChannelNumber = *(INT4 *) (pau1Dot11FirstChannelNumber);\
   pWsscfgIsSetDot11MultiDomainCapabilityEntry->bDot11FirstChannelNumber = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11MultiDomainCapabilityEntry->bDot11FirstChannelNumber = OSIX_FALSE;\
  }\
  if (pau1Dot11NumberofChannels != NULL)\
  {\
   pWsscfgDot11MultiDomainCapabilityEntry->MibObject.i4Dot11NumberofChannels = *(INT4 *) (pau1Dot11NumberofChannels);\
   pWsscfgIsSetDot11MultiDomainCapabilityEntry->bDot11NumberofChannels = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11MultiDomainCapabilityEntry->bDot11NumberofChannels = OSIX_FALSE;\
  }\
  if (pau1Dot11MaximumTransmitPowerLevel != NULL)\
  {\
   pWsscfgDot11MultiDomainCapabilityEntry->MibObject.i4Dot11MaximumTransmitPowerLevel = *(INT4 *) (pau1Dot11MaximumTransmitPowerLevel);\
   pWsscfgIsSetDot11MultiDomainCapabilityEntry->bDot11MaximumTransmitPowerLevel = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11MultiDomainCapabilityEntry->bDot11MaximumTransmitPowerLevel = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11MultiDomainCapabilityEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11MultiDomainCapabilityEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11MultiDomainCapabilityEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11SpectrumManagementEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11SPECTRUMMANAGEMENTTABLE_ARGS(pWsscfgDot11SpectrumManagementEntry,\
   pWsscfgIsSetDot11SpectrumManagementEntry,\
   pau1Dot11SpectrumManagementIndex,\
   pau1Dot11MitigationRequirement,\
   pau1Dot11ChannelSwitchTime,\
   pau1IfIndex)\
  {\
  if (pau1Dot11SpectrumManagementIndex != NULL)\
  {\
   pWsscfgDot11SpectrumManagementEntry->MibObject.i4Dot11SpectrumManagementIndex = *(INT4 *) (pau1Dot11SpectrumManagementIndex);\
   pWsscfgIsSetDot11SpectrumManagementEntry->bDot11SpectrumManagementIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11SpectrumManagementEntry->bDot11SpectrumManagementIndex = OSIX_FALSE;\
  }\
  if (pau1Dot11MitigationRequirement != NULL)\
  {\
   pWsscfgDot11SpectrumManagementEntry->MibObject.i4Dot11MitigationRequirement = *(INT4 *) (pau1Dot11MitigationRequirement);\
   pWsscfgIsSetDot11SpectrumManagementEntry->bDot11MitigationRequirement = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11SpectrumManagementEntry->bDot11MitigationRequirement = OSIX_FALSE;\
  }\
  if (pau1Dot11ChannelSwitchTime != NULL)\
  {\
   pWsscfgDot11SpectrumManagementEntry->MibObject.i4Dot11ChannelSwitchTime = *(INT4 *) (pau1Dot11ChannelSwitchTime);\
   pWsscfgIsSetDot11SpectrumManagementEntry->bDot11ChannelSwitchTime = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11SpectrumManagementEntry->bDot11ChannelSwitchTime = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11SpectrumManagementEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11SpectrumManagementEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11SpectrumManagementEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11RegulatoryClassesEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11REGULATORYCLASSESTABLE_ARGS(pWsscfgDot11RegulatoryClassesEntry,\
   pWsscfgIsSetDot11RegulatoryClassesEntry,\
   pau1Dot11RegulatoryClassesIndex,\
   pau1Dot11RegulatoryClass,\
   pau1Dot11CoverageClass,\
   pau1IfIndex)\
  {\
  if (pau1Dot11RegulatoryClassesIndex != NULL)\
  {\
   pWsscfgDot11RegulatoryClassesEntry->MibObject.i4Dot11RegulatoryClassesIndex = *(INT4 *) (pau1Dot11RegulatoryClassesIndex);\
   pWsscfgIsSetDot11RegulatoryClassesEntry->bDot11RegulatoryClassesIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RegulatoryClassesEntry->bDot11RegulatoryClassesIndex = OSIX_FALSE;\
  }\
  if (pau1Dot11RegulatoryClass != NULL)\
  {\
   pWsscfgDot11RegulatoryClassesEntry->MibObject.i4Dot11RegulatoryClass = *(INT4 *) (pau1Dot11RegulatoryClass);\
   pWsscfgIsSetDot11RegulatoryClassesEntry->bDot11RegulatoryClass = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RegulatoryClassesEntry->bDot11RegulatoryClass = OSIX_FALSE;\
  }\
  if (pau1Dot11CoverageClass != NULL)\
  {\
   pWsscfgDot11RegulatoryClassesEntry->MibObject.i4Dot11CoverageClass = *(INT4 *) (pau1Dot11CoverageClass);\
   pWsscfgIsSetDot11RegulatoryClassesEntry->bDot11CoverageClass = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RegulatoryClassesEntry->bDot11CoverageClass = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11RegulatoryClassesEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11RegulatoryClassesEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RegulatoryClassesEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11OperationEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11OPERATIONTABLE_ARGS(pWsscfgDot11OperationEntry,\
   pWsscfgIsSetDot11OperationEntry,\
   pau1Dot11RTSThreshold,\
   pau1Dot11ShortRetryLimit,\
   pau1Dot11LongRetryLimit,\
   pau1Dot11FragmentationThreshold,\
   pau1Dot11MaxTransmitMSDULifetime,\
   pau1Dot11MaxReceiveLifetime,\
   pau1Dot11CAPLimit,\
   pau1Dot11HCCWmin,\
   pau1Dot11HCCWmax,\
   pau1Dot11HCCAIFSN,\
   pau1Dot11ADDBAResponseTimeout,\
   pau1Dot11ADDTSResponseTimeout,\
   pau1Dot11ChannelUtilizationBeaconInterval,\
   pau1Dot11ScheduleTimeout,\
   pau1Dot11DLSResponseTimeout,\
   pau1Dot11QAPMissingAckRetryLimit,\
   pau1Dot11EDCAAveragingPeriod,\
   pau1IfIndex)\
  {\
  if (pau1Dot11RTSThreshold != NULL)\
  {\
   pWsscfgDot11OperationEntry->MibObject.i4Dot11RTSThreshold = *(INT4 *) (pau1Dot11RTSThreshold);\
   pWsscfgIsSetDot11OperationEntry->bDot11RTSThreshold = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11OperationEntry->bDot11RTSThreshold = OSIX_FALSE;\
  }\
  if (pau1Dot11ShortRetryLimit != NULL)\
  {\
   pWsscfgDot11OperationEntry->MibObject.i4Dot11ShortRetryLimit = *(INT4 *) (pau1Dot11ShortRetryLimit);\
   pWsscfgIsSetDot11OperationEntry->bDot11ShortRetryLimit = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11OperationEntry->bDot11ShortRetryLimit = OSIX_FALSE;\
  }\
  if (pau1Dot11LongRetryLimit != NULL)\
  {\
   pWsscfgDot11OperationEntry->MibObject.i4Dot11LongRetryLimit = *(INT4 *) (pau1Dot11LongRetryLimit);\
   pWsscfgIsSetDot11OperationEntry->bDot11LongRetryLimit = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11OperationEntry->bDot11LongRetryLimit = OSIX_FALSE;\
  }\
  if (pau1Dot11FragmentationThreshold != NULL)\
  {\
   pWsscfgDot11OperationEntry->MibObject.i4Dot11FragmentationThreshold = *(INT4 *) (pau1Dot11FragmentationThreshold);\
   pWsscfgIsSetDot11OperationEntry->bDot11FragmentationThreshold = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11OperationEntry->bDot11FragmentationThreshold = OSIX_FALSE;\
  }\
  if (pau1Dot11MaxTransmitMSDULifetime != NULL)\
  {\
   pWsscfgDot11OperationEntry->MibObject.u4Dot11MaxTransmitMSDULifetime = *(UINT4 *) (pau1Dot11MaxTransmitMSDULifetime);\
   pWsscfgIsSetDot11OperationEntry->bDot11MaxTransmitMSDULifetime = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11OperationEntry->bDot11MaxTransmitMSDULifetime = OSIX_FALSE;\
  }\
  if (pau1Dot11MaxReceiveLifetime != NULL)\
  {\
   pWsscfgDot11OperationEntry->MibObject.u4Dot11MaxReceiveLifetime = *(UINT4 *) (pau1Dot11MaxReceiveLifetime);\
   pWsscfgIsSetDot11OperationEntry->bDot11MaxReceiveLifetime = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11OperationEntry->bDot11MaxReceiveLifetime = OSIX_FALSE;\
  }\
  if (pau1Dot11CAPLimit != NULL)\
  {\
   pWsscfgDot11OperationEntry->MibObject.i4Dot11CAPLimit = *(INT4 *) (pau1Dot11CAPLimit);\
   pWsscfgIsSetDot11OperationEntry->bDot11CAPLimit = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11OperationEntry->bDot11CAPLimit = OSIX_FALSE;\
  }\
  if (pau1Dot11HCCWmin != NULL)\
  {\
   pWsscfgDot11OperationEntry->MibObject.i4Dot11HCCWmin = *(INT4 *) (pau1Dot11HCCWmin);\
   pWsscfgIsSetDot11OperationEntry->bDot11HCCWmin = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11OperationEntry->bDot11HCCWmin = OSIX_FALSE;\
  }\
  if (pau1Dot11HCCWmax != NULL)\
  {\
   pWsscfgDot11OperationEntry->MibObject.i4Dot11HCCWmax = *(INT4 *) (pau1Dot11HCCWmax);\
   pWsscfgIsSetDot11OperationEntry->bDot11HCCWmax = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11OperationEntry->bDot11HCCWmax = OSIX_FALSE;\
  }\
  if (pau1Dot11HCCAIFSN != NULL)\
  {\
   pWsscfgDot11OperationEntry->MibObject.i4Dot11HCCAIFSN = *(INT4 *) (pau1Dot11HCCAIFSN);\
   pWsscfgIsSetDot11OperationEntry->bDot11HCCAIFSN = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11OperationEntry->bDot11HCCAIFSN = OSIX_FALSE;\
  }\
  if (pau1Dot11ADDBAResponseTimeout != NULL)\
  {\
   pWsscfgDot11OperationEntry->MibObject.i4Dot11ADDBAResponseTimeout = *(INT4 *) (pau1Dot11ADDBAResponseTimeout);\
   pWsscfgIsSetDot11OperationEntry->bDot11ADDBAResponseTimeout = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11OperationEntry->bDot11ADDBAResponseTimeout = OSIX_FALSE;\
  }\
  if (pau1Dot11ADDTSResponseTimeout != NULL)\
  {\
   pWsscfgDot11OperationEntry->MibObject.i4Dot11ADDTSResponseTimeout = *(INT4 *) (pau1Dot11ADDTSResponseTimeout);\
   pWsscfgIsSetDot11OperationEntry->bDot11ADDTSResponseTimeout = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11OperationEntry->bDot11ADDTSResponseTimeout = OSIX_FALSE;\
  }\
  if (pau1Dot11ChannelUtilizationBeaconInterval != NULL)\
  {\
   pWsscfgDot11OperationEntry->MibObject.i4Dot11ChannelUtilizationBeaconInterval = *(INT4 *) (pau1Dot11ChannelUtilizationBeaconInterval);\
   pWsscfgIsSetDot11OperationEntry->bDot11ChannelUtilizationBeaconInterval = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11OperationEntry->bDot11ChannelUtilizationBeaconInterval = OSIX_FALSE;\
  }\
  if (pau1Dot11ScheduleTimeout != NULL)\
  {\
   pWsscfgDot11OperationEntry->MibObject.i4Dot11ScheduleTimeout = *(INT4 *) (pau1Dot11ScheduleTimeout);\
   pWsscfgIsSetDot11OperationEntry->bDot11ScheduleTimeout = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11OperationEntry->bDot11ScheduleTimeout = OSIX_FALSE;\
  }\
  if (pau1Dot11DLSResponseTimeout != NULL)\
  {\
   pWsscfgDot11OperationEntry->MibObject.i4Dot11DLSResponseTimeout = *(INT4 *) (pau1Dot11DLSResponseTimeout);\
   pWsscfgIsSetDot11OperationEntry->bDot11DLSResponseTimeout = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11OperationEntry->bDot11DLSResponseTimeout = OSIX_FALSE;\
  }\
  if (pau1Dot11QAPMissingAckRetryLimit != NULL)\
  {\
   pWsscfgDot11OperationEntry->MibObject.i4Dot11QAPMissingAckRetryLimit = *(INT4 *) (pau1Dot11QAPMissingAckRetryLimit);\
   pWsscfgIsSetDot11OperationEntry->bDot11QAPMissingAckRetryLimit = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11OperationEntry->bDot11QAPMissingAckRetryLimit = OSIX_FALSE;\
  }\
  if (pau1Dot11EDCAAveragingPeriod != NULL)\
  {\
   pWsscfgDot11OperationEntry->MibObject.i4Dot11EDCAAveragingPeriod = *(INT4 *) (pau1Dot11EDCAAveragingPeriod);\
   pWsscfgIsSetDot11OperationEntry->bDot11EDCAAveragingPeriod = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11OperationEntry->bDot11EDCAAveragingPeriod = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11OperationEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11OperationEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11OperationEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11GroupAddressesEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11GROUPADDRESSESTABLE_ARGS(pWsscfgDot11GroupAddressesEntry,\
   pWsscfgIsSetDot11GroupAddressesEntry,\
   pau1Dot11GroupAddressesIndex,\
   pau1Dot11Address,\
   pau1Dot11GroupAddressesStatus,\
   pau1IfIndex)\
  {\
  if (pau1Dot11GroupAddressesIndex != NULL)\
  {\
   pWsscfgDot11GroupAddressesEntry->MibObject.i4Dot11GroupAddressesIndex = *(INT4 *) (pau1Dot11GroupAddressesIndex);\
   pWsscfgIsSetDot11GroupAddressesEntry->bDot11GroupAddressesIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11GroupAddressesEntry->bDot11GroupAddressesIndex = OSIX_FALSE;\
  }\
  if (pau1Dot11Address != NULL)\
  {\
   StrToMac ((UINT1 *) &pau1Dot11Address, pWsscfgDot11GroupAddressesEntry->MibObject.Dot11Address);\
   pWsscfgIsSetDot11GroupAddressesEntry->bDot11Address = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11GroupAddressesEntry->bDot11Address = OSIX_FALSE;\
  }\
  if (pau1Dot11GroupAddressesStatus != NULL)\
  {\
   pWsscfgDot11GroupAddressesEntry->MibObject.i4Dot11GroupAddressesStatus = *(INT4 *) (pau1Dot11GroupAddressesStatus);\
   pWsscfgIsSetDot11GroupAddressesEntry->bDot11GroupAddressesStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11GroupAddressesEntry->bDot11GroupAddressesStatus = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11GroupAddressesEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11GroupAddressesEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11GroupAddressesEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11EDCAEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11EDCATABLE_ARGS(pWsscfgDot11EDCAEntry,\
   pWsscfgIsSetDot11EDCAEntry,\
   pau1Dot11EDCATableIndex,\
   pau1Dot11EDCATableCWmin,\
   pau1Dot11EDCATableCWmax,\
   pau1Dot11EDCATableAIFSN,\
   pau1Dot11EDCATableTXOPLimit,\
   pau1Dot11EDCATableMSDULifetime,\
   pau1Dot11EDCATableMandatory,\
   pau1IfIndex)\
  {\
  if (pau1Dot11EDCATableIndex != NULL)\
  {\
   pWsscfgDot11EDCAEntry->MibObject.i4Dot11EDCATableIndex = *(INT4 *) (pau1Dot11EDCATableIndex);\
   pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableIndex = OSIX_FALSE;\
  }\
  if (pau1Dot11EDCATableCWmin != NULL)\
  {\
   pWsscfgDot11EDCAEntry->MibObject.i4Dot11EDCATableCWmin = *(INT4 *) (pau1Dot11EDCATableCWmin);\
   pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableCWmin = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableCWmin = OSIX_FALSE;\
  }\
  if (pau1Dot11EDCATableCWmax != NULL)\
  {\
   pWsscfgDot11EDCAEntry->MibObject.i4Dot11EDCATableCWmax = *(INT4 *) (pau1Dot11EDCATableCWmax);\
   pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableCWmax = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableCWmax = OSIX_FALSE;\
  }\
  if (pau1Dot11EDCATableAIFSN != NULL)\
  {\
   pWsscfgDot11EDCAEntry->MibObject.i4Dot11EDCATableAIFSN = *(INT4 *) (pau1Dot11EDCATableAIFSN);\
   pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableAIFSN = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableAIFSN = OSIX_FALSE;\
  }\
  if (pau1Dot11EDCATableTXOPLimit != NULL)\
  {\
   pWsscfgDot11EDCAEntry->MibObject.i4Dot11EDCATableTXOPLimit = *(INT4 *) (pau1Dot11EDCATableTXOPLimit);\
   pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableTXOPLimit = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableTXOPLimit = OSIX_FALSE;\
  }\
  if (pau1Dot11EDCATableMSDULifetime != NULL)\
  {\
   pWsscfgDot11EDCAEntry->MibObject.i4Dot11EDCATableMSDULifetime = *(INT4 *) (pau1Dot11EDCATableMSDULifetime);\
   pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableMSDULifetime = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableMSDULifetime = OSIX_FALSE;\
  }\
  if (pau1Dot11EDCATableMandatory != NULL)\
  {\
   pWsscfgDot11EDCAEntry->MibObject.i4Dot11EDCATableMandatory = *(INT4 *) (pau1Dot11EDCATableMandatory);\
   pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableMandatory = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11EDCAEntry->bDot11EDCATableMandatory = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11EDCAEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11EDCAEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11EDCAEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11QAPEDCAEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11QAPEDCATABLE_ARGS(pWsscfgDot11QAPEDCAEntry,\
   pWsscfgIsSetDot11QAPEDCAEntry,\
   pau1Dot11QAPEDCATableIndex,\
   pau1Dot11QAPEDCATableCWmin,\
   pau1Dot11QAPEDCATableCWmax,\
   pau1Dot11QAPEDCATableAIFSN,\
   pau1Dot11QAPEDCATableTXOPLimit,\
   pau1Dot11QAPEDCATableMSDULifetime,\
   pau1Dot11QAPEDCATableMandatory,\
   pau1IfIndex)\
  {\
  if (pau1Dot11QAPEDCATableIndex != NULL)\
  {\
   pWsscfgDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableIndex = *(INT4 *) (pau1Dot11QAPEDCATableIndex);\
   pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableIndex = OSIX_FALSE;\
  }\
  if (pau1Dot11QAPEDCATableCWmin != NULL)\
  {\
   pWsscfgDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableCWmin = *(INT4 *) (pau1Dot11QAPEDCATableCWmin);\
   pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableCWmin = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableCWmin = OSIX_FALSE;\
  }\
  if (pau1Dot11QAPEDCATableCWmax != NULL)\
  {\
   pWsscfgDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableCWmax = *(INT4 *) (pau1Dot11QAPEDCATableCWmax);\
   pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableCWmax = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableCWmax = OSIX_FALSE;\
  }\
  if (pau1Dot11QAPEDCATableAIFSN != NULL)\
  {\
   pWsscfgDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableAIFSN = *(INT4 *) (pau1Dot11QAPEDCATableAIFSN);\
   pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableAIFSN = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableAIFSN = OSIX_FALSE;\
  }\
  if (pau1Dot11QAPEDCATableTXOPLimit != NULL)\
  {\
   pWsscfgDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableTXOPLimit = *(INT4 *) (pau1Dot11QAPEDCATableTXOPLimit);\
   pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableTXOPLimit = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableTXOPLimit = OSIX_FALSE;\
  }\
  if (pau1Dot11QAPEDCATableMSDULifetime != NULL)\
  {\
   pWsscfgDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableMSDULifetime = *(INT4 *) (pau1Dot11QAPEDCATableMSDULifetime);\
   pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableMSDULifetime = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableMSDULifetime = OSIX_FALSE;\
  }\
  if (pau1Dot11QAPEDCATableMandatory != NULL)\
  {\
   pWsscfgDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableMandatory = *(INT4 *) (pau1Dot11QAPEDCATableMandatory);\
   pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableMandatory = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11QAPEDCAEntry->bDot11QAPEDCATableMandatory = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11QAPEDCAEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11QAPEDCAEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11QAPEDCAEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11PhyOperationEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11PHYOPERATIONTABLE_ARGS(pWsscfgDot11PhyOperationEntry,\
   pWsscfgIsSetDot11PhyOperationEntry,\
   pau1Dot11CurrentRegDomain,\
   pau1IfIndex)\
  {\
  if (pau1Dot11CurrentRegDomain != NULL)\
  {\
   pWsscfgDot11PhyOperationEntry->MibObject.i4Dot11CurrentRegDomain = *(INT4 *) (pau1Dot11CurrentRegDomain);\
   pWsscfgIsSetDot11PhyOperationEntry->bDot11CurrentRegDomain = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyOperationEntry->bDot11CurrentRegDomain = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11PhyOperationEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11PhyOperationEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyOperationEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11PhyAntennaEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11PHYANTENNATABLE_ARGS(pWsscfgDot11PhyAntennaEntry,\
   pWsscfgIsSetDot11PhyAntennaEntry,\
   pau1Dot11CurrentTxAntenna,\
   pau1Dot11CurrentRxAntenna,\
   pau1IfIndex)\
  {\
  if (pau1Dot11CurrentTxAntenna != NULL)\
  {\
   pWsscfgDot11PhyAntennaEntry->MibObject.i4Dot11CurrentTxAntenna = *(INT4 *) (pau1Dot11CurrentTxAntenna);\
   pWsscfgIsSetDot11PhyAntennaEntry->bDot11CurrentTxAntenna = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyAntennaEntry->bDot11CurrentTxAntenna = OSIX_FALSE;\
  }\
  if (pau1Dot11CurrentRxAntenna != NULL)\
  {\
   pWsscfgDot11PhyAntennaEntry->MibObject.i4Dot11CurrentRxAntenna = *(INT4 *) (pau1Dot11CurrentRxAntenna);\
   pWsscfgIsSetDot11PhyAntennaEntry->bDot11CurrentRxAntenna = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyAntennaEntry->bDot11CurrentRxAntenna = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11PhyAntennaEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11PhyAntennaEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyAntennaEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11PhyTxPowerEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11PHYTXPOWERTABLE_ARGS(pWsscfgDot11PhyTxPowerEntry,\
   pWsscfgIsSetDot11PhyTxPowerEntry,\
   pau1Dot11CurrentTxPowerLevel,\
   pau1IfIndex)\
  {\
  if (pau1Dot11CurrentTxPowerLevel != NULL)\
  {\
   pWsscfgDot11PhyTxPowerEntry->MibObject.i4Dot11CurrentTxPowerLevel = *(INT4 *) (pau1Dot11CurrentTxPowerLevel);\
   pWsscfgIsSetDot11PhyTxPowerEntry->bDot11CurrentTxPowerLevel = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyTxPowerEntry->bDot11CurrentTxPowerLevel = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11PhyTxPowerEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11PhyTxPowerEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyTxPowerEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11PhyFHSSEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11PHYFHSSTABLE_ARGS(pWsscfgDot11PhyFHSSEntry,\
   pWsscfgIsSetDot11PhyFHSSEntry,\
   pau1Dot11CurrentChannelNumber,\
   pau1Dot11CurrentDwellTime,\
   pau1Dot11CurrentSet,\
   pau1Dot11CurrentPattern,\
   pau1Dot11CurrentIndex,\
   pau1Dot11EHCCPrimeRadix,\
   pau1Dot11EHCCNumberofChannelsFamilyIndex,\
   pau1Dot11EHCCCapabilityImplemented,\
   pau1Dot11EHCCCapabilityEnabled,\
   pau1Dot11HopAlgorithmAdopted,\
   pau1Dot11RandomTableFlag,\
   pau1Dot11HopOffset,\
   pau1IfIndex)\
  {\
  if (pau1Dot11CurrentChannelNumber != NULL)\
  {\
   pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11CurrentChannelNumber = *(INT4 *) (pau1Dot11CurrentChannelNumber);\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentChannelNumber = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentChannelNumber = OSIX_FALSE;\
  }\
  if (pau1Dot11CurrentDwellTime != NULL)\
  {\
   pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11CurrentDwellTime = *(INT4 *) (pau1Dot11CurrentDwellTime);\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentDwellTime = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentDwellTime = OSIX_FALSE;\
  }\
  if (pau1Dot11CurrentSet != NULL)\
  {\
   pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11CurrentSet = *(INT4 *) (pau1Dot11CurrentSet);\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentSet = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentSet = OSIX_FALSE;\
  }\
  if (pau1Dot11CurrentPattern != NULL)\
  {\
   pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11CurrentPattern = *(INT4 *) (pau1Dot11CurrentPattern);\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentPattern = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentPattern = OSIX_FALSE;\
  }\
  if (pau1Dot11CurrentIndex != NULL)\
  {\
   pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11CurrentIndex = *(INT4 *) (pau1Dot11CurrentIndex);\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11CurrentIndex = OSIX_FALSE;\
  }\
  if (pau1Dot11EHCCPrimeRadix != NULL)\
  {\
   pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11EHCCPrimeRadix = *(INT4 *) (pau1Dot11EHCCPrimeRadix);\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCPrimeRadix = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCPrimeRadix = OSIX_FALSE;\
  }\
  if (pau1Dot11EHCCNumberofChannelsFamilyIndex != NULL)\
  {\
   pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11EHCCNumberofChannelsFamilyIndex = *(INT4 *) (pau1Dot11EHCCNumberofChannelsFamilyIndex);\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCNumberofChannelsFamilyIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCNumberofChannelsFamilyIndex = OSIX_FALSE;\
  }\
  if (pau1Dot11EHCCCapabilityImplemented != NULL)\
  {\
   pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11EHCCCapabilityImplemented = *(INT4 *) (pau1Dot11EHCCCapabilityImplemented);\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCCapabilityImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCCapabilityImplemented = OSIX_FALSE;\
  }\
  if (pau1Dot11EHCCCapabilityEnabled != NULL)\
  {\
   pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11EHCCCapabilityEnabled = *(INT4 *) (pau1Dot11EHCCCapabilityEnabled);\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCCapabilityEnabled = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11EHCCCapabilityEnabled = OSIX_FALSE;\
  }\
  if (pau1Dot11HopAlgorithmAdopted != NULL)\
  {\
   pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11HopAlgorithmAdopted = *(INT4 *) (pau1Dot11HopAlgorithmAdopted);\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11HopAlgorithmAdopted = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11HopAlgorithmAdopted = OSIX_FALSE;\
  }\
  if (pau1Dot11RandomTableFlag != NULL)\
  {\
   pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11RandomTableFlag = *(INT4 *) (pau1Dot11RandomTableFlag);\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11RandomTableFlag = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11RandomTableFlag = OSIX_FALSE;\
  }\
  if (pau1Dot11HopOffset != NULL)\
  {\
   pWsscfgDot11PhyFHSSEntry->MibObject.i4Dot11HopOffset = *(INT4 *) (pau1Dot11HopOffset);\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11HopOffset = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyFHSSEntry->bDot11HopOffset = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11PhyFHSSEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11PhyFHSSEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyFHSSEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11PhyDSSSEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11PHYDSSSTABLE_ARGS(pWsscfgDot11PhyDSSSEntry,\
   pWsscfgIsSetDot11PhyDSSSEntry,\
   pau1Dot11CurrentChannel,\
   pau1Dot11CurrentCCAMode,\
   pau1Dot11EDThreshold,\
   pau1IfIndex)\
  {\
  if (pau1Dot11CurrentChannel != NULL)\
  {\
   pWsscfgDot11PhyDSSSEntry->MibObject.i4Dot11CurrentChannel = *(INT4 *) (pau1Dot11CurrentChannel);\
   pWsscfgIsSetDot11PhyDSSSEntry->bDot11CurrentChannel = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyDSSSEntry->bDot11CurrentChannel = OSIX_FALSE;\
  }\
  if (pau1Dot11CurrentCCAMode != NULL)\
  {\
   pWsscfgDot11PhyDSSSEntry->MibObject.i4Dot11CurrentCCAMode = *(INT4 *) (pau1Dot11CurrentCCAMode);\
   pWsscfgIsSetDot11PhyDSSSEntry->bDot11CurrentCCAMode = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyDSSSEntry->bDot11CurrentCCAMode = OSIX_FALSE;\
  }\
  if (pau1Dot11EDThreshold != NULL)\
  {\
   pWsscfgDot11PhyDSSSEntry->MibObject.i4Dot11EDThreshold = *(INT4 *) (pau1Dot11EDThreshold);\
   pWsscfgIsSetDot11PhyDSSSEntry->bDot11EDThreshold = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyDSSSEntry->bDot11EDThreshold = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11PhyDSSSEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11PhyDSSSEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyDSSSEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11PhyIREntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11PHYIRTABLE_ARGS(pWsscfgDot11PhyIREntry,\
   pWsscfgIsSetDot11PhyIREntry,\
   pau1Dot11CCAWatchdogTimerMax,\
   pau1Dot11CCAWatchdogCountMax,\
   pau1Dot11CCAWatchdogTimerMin,\
   pau1Dot11CCAWatchdogCountMin,\
   pau1IfIndex)\
  {\
  if (pau1Dot11CCAWatchdogTimerMax != NULL)\
  {\
   pWsscfgDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogTimerMax = *(INT4 *) (pau1Dot11CCAWatchdogTimerMax);\
   pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogTimerMax = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogTimerMax = OSIX_FALSE;\
  }\
  if (pau1Dot11CCAWatchdogCountMax != NULL)\
  {\
   pWsscfgDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogCountMax = *(INT4 *) (pau1Dot11CCAWatchdogCountMax);\
   pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogCountMax = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogCountMax = OSIX_FALSE;\
  }\
  if (pau1Dot11CCAWatchdogTimerMin != NULL)\
  {\
   pWsscfgDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogTimerMin = *(INT4 *) (pau1Dot11CCAWatchdogTimerMin);\
   pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogTimerMin = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogTimerMin = OSIX_FALSE;\
  }\
  if (pau1Dot11CCAWatchdogCountMin != NULL)\
  {\
   pWsscfgDot11PhyIREntry->MibObject.i4Dot11CCAWatchdogCountMin = *(INT4 *) (pau1Dot11CCAWatchdogCountMin);\
   pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogCountMin = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyIREntry->bDot11CCAWatchdogCountMin = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11PhyIREntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11PhyIREntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyIREntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11AntennasListEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11ANTENNASLISTTABLE_ARGS(pWsscfgDot11AntennasListEntry,\
   pWsscfgIsSetDot11AntennasListEntry,\
   pau1Dot11AntennaListIndex,\
   pau1Dot11SupportedTxAntenna,\
   pau1Dot11SupportedRxAntenna,\
   pau1Dot11DiversitySelectionRx,\
   pau1IfIndex)\
  {\
  if (pau1Dot11AntennaListIndex != NULL)\
  {\
   pWsscfgDot11AntennasListEntry->MibObject.i4Dot11AntennaListIndex = *(INT4 *) (pau1Dot11AntennaListIndex);\
   pWsscfgIsSetDot11AntennasListEntry->bDot11AntennaListIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11AntennasListEntry->bDot11AntennaListIndex = OSIX_FALSE;\
  }\
  if (pau1Dot11SupportedTxAntenna != NULL)\
  {\
   pWsscfgDot11AntennasListEntry->MibObject.i4Dot11SupportedTxAntenna = *(INT4 *) (pau1Dot11SupportedTxAntenna);\
   pWsscfgIsSetDot11AntennasListEntry->bDot11SupportedTxAntenna = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11AntennasListEntry->bDot11SupportedTxAntenna = OSIX_FALSE;\
  }\
  if (pau1Dot11SupportedRxAntenna != NULL)\
  {\
   pWsscfgDot11AntennasListEntry->MibObject.i4Dot11SupportedRxAntenna = *(INT4 *) (pau1Dot11SupportedRxAntenna);\
   pWsscfgIsSetDot11AntennasListEntry->bDot11SupportedRxAntenna = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11AntennasListEntry->bDot11SupportedRxAntenna = OSIX_FALSE;\
  }\
  if (pau1Dot11DiversitySelectionRx != NULL)\
  {\
   pWsscfgDot11AntennasListEntry->MibObject.i4Dot11DiversitySelectionRx = *(INT4 *) (pau1Dot11DiversitySelectionRx);\
   pWsscfgIsSetDot11AntennasListEntry->bDot11DiversitySelectionRx = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11AntennasListEntry->bDot11DiversitySelectionRx = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11AntennasListEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11AntennasListEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11AntennasListEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11PhyOFDMEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11PHYOFDMTABLE_ARGS(pWsscfgDot11PhyOFDMEntry,\
   pWsscfgIsSetDot11PhyOFDMEntry,\
   pau1Dot11CurrentFrequency,\
   pau1Dot11TIThreshold,\
   pau1Dot11ChannelStartingFactor,\
   pau1Dot11PhyOFDMChannelWidth,\
   pau1IfIndex)\
  {\
  if (pau1Dot11CurrentFrequency != NULL)\
  {\
   pWsscfgDot11PhyOFDMEntry->MibObject.i4Dot11CurrentFrequency = *(INT4 *) (pau1Dot11CurrentFrequency);\
   pWsscfgIsSetDot11PhyOFDMEntry->bDot11CurrentFrequency = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyOFDMEntry->bDot11CurrentFrequency = OSIX_FALSE;\
  }\
  if (pau1Dot11TIThreshold != NULL)\
  {\
   pWsscfgDot11PhyOFDMEntry->MibObject.i4Dot11TIThreshold = *(INT4 *) (pau1Dot11TIThreshold);\
   pWsscfgIsSetDot11PhyOFDMEntry->bDot11TIThreshold = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyOFDMEntry->bDot11TIThreshold = OSIX_FALSE;\
  }\
  if (pau1Dot11ChannelStartingFactor != NULL)\
  {\
   pWsscfgDot11PhyOFDMEntry->MibObject.i4Dot11ChannelStartingFactor = *(INT4 *) (pau1Dot11ChannelStartingFactor);\
   pWsscfgIsSetDot11PhyOFDMEntry->bDot11ChannelStartingFactor = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyOFDMEntry->bDot11ChannelStartingFactor = OSIX_FALSE;\
  }\
  if (pau1Dot11PhyOFDMChannelWidth != NULL)\
  {\
   pWsscfgDot11PhyOFDMEntry->MibObject.i4Dot11PhyOFDMChannelWidth = *(INT4 *) (pau1Dot11PhyOFDMChannelWidth);\
   pWsscfgIsSetDot11PhyOFDMEntry->bDot11PhyOFDMChannelWidth = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyOFDMEntry->bDot11PhyOFDMChannelWidth = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11PhyOFDMEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11PhyOFDMEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyOFDMEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11HoppingPatternEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11HOPPINGPATTERNTABLE_ARGS(pWsscfgDot11HoppingPatternEntry,\
   pWsscfgIsSetDot11HoppingPatternEntry,\
   pau1Dot11HoppingPatternIndex,\
   pau1Dot11RandomTableFieldNumber,\
   pau1IfIndex)\
  {\
  if (pau1Dot11HoppingPatternIndex != NULL)\
  {\
   pWsscfgDot11HoppingPatternEntry->MibObject.i4Dot11HoppingPatternIndex = *(INT4 *) (pau1Dot11HoppingPatternIndex);\
   pWsscfgIsSetDot11HoppingPatternEntry->bDot11HoppingPatternIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11HoppingPatternEntry->bDot11HoppingPatternIndex = OSIX_FALSE;\
  }\
  if (pau1Dot11RandomTableFieldNumber != NULL)\
  {\
   pWsscfgDot11HoppingPatternEntry->MibObject.i4Dot11RandomTableFieldNumber = *(INT4 *) (pau1Dot11RandomTableFieldNumber);\
   pWsscfgIsSetDot11HoppingPatternEntry->bDot11RandomTableFieldNumber = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11HoppingPatternEntry->bDot11RandomTableFieldNumber = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11HoppingPatternEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11HoppingPatternEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11HoppingPatternEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11PhyERPEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11PHYERPTABLE_ARGS(pWsscfgDot11PhyERPEntry,\
   pWsscfgIsSetDot11PhyERPEntry,\
   pau1Dot11ERPBCCOptionEnabled,\
   pau1Dot11DSSSOFDMOptionEnabled,\
   pau1Dot11ShortSlotTimeOptionImplemented,\
   pau1Dot11ShortSlotTimeOptionEnabled,\
   pau1IfIndex)\
  {\
  if (pau1Dot11ERPBCCOptionEnabled != NULL)\
  {\
   pWsscfgDot11PhyERPEntry->MibObject.i4Dot11ERPBCCOptionEnabled = *(INT4 *) (pau1Dot11ERPBCCOptionEnabled);\
   pWsscfgIsSetDot11PhyERPEntry->bDot11ERPBCCOptionEnabled = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyERPEntry->bDot11ERPBCCOptionEnabled = OSIX_FALSE;\
  }\
  if (pau1Dot11DSSSOFDMOptionEnabled != NULL)\
  {\
   pWsscfgDot11PhyERPEntry->MibObject.i4Dot11DSSSOFDMOptionEnabled = *(INT4 *) (pau1Dot11DSSSOFDMOptionEnabled);\
   pWsscfgIsSetDot11PhyERPEntry->bDot11DSSSOFDMOptionEnabled = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyERPEntry->bDot11DSSSOFDMOptionEnabled = OSIX_FALSE;\
  }\
  if (pau1Dot11ShortSlotTimeOptionImplemented != NULL)\
  {\
   pWsscfgDot11PhyERPEntry->MibObject.i4Dot11ShortSlotTimeOptionImplemented = *(INT4 *) (pau1Dot11ShortSlotTimeOptionImplemented);\
   pWsscfgIsSetDot11PhyERPEntry->bDot11ShortSlotTimeOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyERPEntry->bDot11ShortSlotTimeOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1Dot11ShortSlotTimeOptionEnabled != NULL)\
  {\
   pWsscfgDot11PhyERPEntry->MibObject.i4Dot11ShortSlotTimeOptionEnabled = *(INT4 *) (pau1Dot11ShortSlotTimeOptionEnabled);\
   pWsscfgIsSetDot11PhyERPEntry->bDot11ShortSlotTimeOptionEnabled = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyERPEntry->bDot11ShortSlotTimeOptionEnabled = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11PhyERPEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11PhyERPEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11PhyERPEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11RSNAConfigEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11RSNACONFIGTABLE_ARGS(pWsscfgDot11RSNAConfigEntry,\
   pWsscfgIsSetDot11RSNAConfigEntry,\
   pau1Dot11RSNAConfigGroupCipher,\
   pau1Dot11RSNAConfigGroupCipherLen,\
   pau1Dot11RSNAConfigGroupRekeyMethod,\
   pau1Dot11RSNAConfigGroupRekeyTime,\
   pau1Dot11RSNAConfigGroupRekeyPackets,\
   pau1Dot11RSNAConfigGroupRekeyStrict,\
   pau1Dot11RSNAConfigPSKValue,\
   pau1Dot11RSNAConfigPSKValueLen,\
   pau1Dot11RSNAConfigPSKPassPhrase,\
   pau1Dot11RSNAConfigPSKPassPhraseLen,\
   pau1Dot11RSNAConfigGroupUpdateCount,\
   pau1Dot11RSNAConfigPairwiseUpdateCount,\
   pau1Dot11RSNAConfigPMKLifetime,\
   pau1Dot11RSNAConfigPMKReauthThreshold,\
   pau1Dot11RSNAConfigSATimeout,\
   pau1Dot11RSNATKIPCounterMeasuresInvoked,\
   pau1Dot11RSNA4WayHandshakeFailures,\
   pau1Dot11RSNAConfigSTKRekeyTime,\
   pau1Dot11RSNAConfigSMKUpdateCount,\
   pau1Dot11RSNAConfigSMKLifetime,\
   pau1Dot11RSNASMKHandshakeFailures,\
   pau1IfIndex)\
  {\
  if (pau1Dot11RSNAConfigGroupCipher != NULL)\
  {\
   MEMCPY (pWsscfgDot11RSNAConfigEntry->MibObject.au1Dot11RSNAConfigGroupCipher, pau1Dot11RSNAConfigGroupCipher, *(INT4 *)pau1Dot11RSNAConfigGroupCipherLen);\
   pWsscfgDot11RSNAConfigEntry->MibObject.i4Dot11RSNAConfigGroupCipherLen = *(INT4 *)pau1Dot11RSNAConfigGroupCipherLen;\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupCipher = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupCipher = OSIX_FALSE;\
  }\
  if (pau1Dot11RSNAConfigGroupRekeyMethod != NULL)\
  {\
   pWsscfgDot11RSNAConfigEntry->MibObject.i4Dot11RSNAConfigGroupRekeyMethod = *(INT4 *) (pau1Dot11RSNAConfigGroupRekeyMethod);\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyMethod = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyMethod = OSIX_FALSE;\
  }\
  if (pau1Dot11RSNAConfigGroupRekeyTime != NULL)\
  {\
   pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNAConfigGroupRekeyTime = *(UINT4 *) (pau1Dot11RSNAConfigGroupRekeyTime);\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyTime = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyTime = OSIX_FALSE;\
  }\
  if (pau1Dot11RSNAConfigGroupRekeyPackets != NULL)\
  {\
   pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNAConfigGroupRekeyPackets = *(UINT4 *) (pau1Dot11RSNAConfigGroupRekeyPackets);\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyPackets = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyPackets = OSIX_FALSE;\
  }\
  if (pau1Dot11RSNAConfigGroupRekeyStrict != NULL)\
  {\
   pWsscfgDot11RSNAConfigEntry->MibObject.i4Dot11RSNAConfigGroupRekeyStrict = *(INT4 *) (pau1Dot11RSNAConfigGroupRekeyStrict);\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyStrict = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupRekeyStrict = OSIX_FALSE;\
  }\
  if (pau1Dot11RSNAConfigPSKValue != NULL)\
  {\
   MEMCPY (pWsscfgDot11RSNAConfigEntry->MibObject.au1Dot11RSNAConfigPSKValue, pau1Dot11RSNAConfigPSKValue, *(INT4 *)pau1Dot11RSNAConfigPSKValueLen);\
   pWsscfgDot11RSNAConfigEntry->MibObject.i4Dot11RSNAConfigPSKValueLen = *(INT4 *)pau1Dot11RSNAConfigPSKValueLen;\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPSKValue = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPSKValue = OSIX_FALSE;\
  }\
  if (pau1Dot11RSNAConfigPSKPassPhrase != NULL)\
  {\
   MEMCPY (pWsscfgDot11RSNAConfigEntry->MibObject.au1Dot11RSNAConfigPSKPassPhrase, pau1Dot11RSNAConfigPSKPassPhrase, *(INT4 *)pau1Dot11RSNAConfigPSKPassPhraseLen);\
   pWsscfgDot11RSNAConfigEntry->MibObject.i4Dot11RSNAConfigPSKPassPhraseLen = *(INT4 *)pau1Dot11RSNAConfigPSKPassPhraseLen;\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPSKPassPhrase = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPSKPassPhrase = OSIX_FALSE;\
  }\
  if (pau1Dot11RSNAConfigGroupUpdateCount != NULL)\
  {\
   pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNAConfigGroupUpdateCount = *(UINT4 *) (pau1Dot11RSNAConfigGroupUpdateCount);\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupUpdateCount = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigGroupUpdateCount = OSIX_FALSE;\
  }\
  if (pau1Dot11RSNAConfigPairwiseUpdateCount != NULL)\
  {\
   pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNAConfigPairwiseUpdateCount = *(UINT4 *) (pau1Dot11RSNAConfigPairwiseUpdateCount);\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPairwiseUpdateCount = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPairwiseUpdateCount = OSIX_FALSE;\
  }\
  if (pau1Dot11RSNAConfigPMKLifetime != NULL)\
  {\
   pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNAConfigPMKLifetime = *(UINT4 *) (pau1Dot11RSNAConfigPMKLifetime);\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPMKLifetime = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPMKLifetime = OSIX_FALSE;\
  }\
  if (pau1Dot11RSNAConfigPMKReauthThreshold != NULL)\
  {\
   pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNAConfigPMKReauthThreshold = *(UINT4 *) (pau1Dot11RSNAConfigPMKReauthThreshold);\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPMKReauthThreshold = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigPMKReauthThreshold = OSIX_FALSE;\
  }\
  if (pau1Dot11RSNAConfigSATimeout != NULL)\
  {\
   pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNAConfigSATimeout = *(UINT4 *) (pau1Dot11RSNAConfigSATimeout);\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigSATimeout = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigSATimeout = OSIX_FALSE;\
  }\
  if (pau1Dot11RSNATKIPCounterMeasuresInvoked != NULL)\
  {\
   pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNATKIPCounterMeasuresInvoked = *(UINT4 *) (pau1Dot11RSNATKIPCounterMeasuresInvoked);\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNATKIPCounterMeasuresInvoked = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNATKIPCounterMeasuresInvoked = OSIX_FALSE;\
  }\
  if (pau1Dot11RSNA4WayHandshakeFailures != NULL)\
  {\
   pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNA4WayHandshakeFailures = *(UINT4 *) (pau1Dot11RSNA4WayHandshakeFailures);\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNA4WayHandshakeFailures = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNA4WayHandshakeFailures = OSIX_FALSE;\
  }\
  if (pau1Dot11RSNAConfigSTKRekeyTime != NULL)\
  {\
   pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNAConfigSTKRekeyTime = *(UINT4 *) (pau1Dot11RSNAConfigSTKRekeyTime);\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigSTKRekeyTime = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigSTKRekeyTime = OSIX_FALSE;\
  }\
  if (pau1Dot11RSNAConfigSMKUpdateCount != NULL)\
  {\
   pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNAConfigSMKUpdateCount = *(UINT4 *) (pau1Dot11RSNAConfigSMKUpdateCount);\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigSMKUpdateCount = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigSMKUpdateCount = OSIX_FALSE;\
  }\
  if (pau1Dot11RSNAConfigSMKLifetime != NULL)\
  {\
   pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNAConfigSMKLifetime = *(UINT4 *) (pau1Dot11RSNAConfigSMKLifetime);\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigSMKLifetime = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNAConfigSMKLifetime = OSIX_FALSE;\
  }\
  if (pau1Dot11RSNASMKHandshakeFailures != NULL)\
  {\
   pWsscfgDot11RSNAConfigEntry->MibObject.u4Dot11RSNASMKHandshakeFailures = *(UINT4 *) (pau1Dot11RSNASMKHandshakeFailures);\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNASMKHandshakeFailures = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigEntry->bDot11RSNASMKHandshakeFailures = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11RSNAConfigEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11RSNAConfigEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11RSNAConfigPairwiseCiphersEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11RSNACONFIGPAIRWISECIPHERSTABLE_ARGS(pWsscfgDot11RSNAConfigPairwiseCiphersEntry,\
   pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry,\
   pau1Dot11RSNAConfigPairwiseCipherIndex,\
   pau1Dot11RSNAConfigPairwiseCipherEnabled,\
   pau1IfIndex)\
  {\
  if (pau1Dot11RSNAConfigPairwiseCipherIndex != NULL)\
  {\
   pWsscfgDot11RSNAConfigPairwiseCiphersEntry->MibObject.u4Dot11RSNAConfigPairwiseCipherIndex = *(UINT4 *) (pau1Dot11RSNAConfigPairwiseCipherIndex);\
   pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry->bDot11RSNAConfigPairwiseCipherIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry->bDot11RSNAConfigPairwiseCipherIndex = OSIX_FALSE;\
  }\
  if (pau1Dot11RSNAConfigPairwiseCipherEnabled != NULL)\
  {\
   pWsscfgDot11RSNAConfigPairwiseCiphersEntry->MibObject.i4Dot11RSNAConfigPairwiseCipherEnabled = *(INT4 *) (pau1Dot11RSNAConfigPairwiseCipherEnabled);\
   pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry->bDot11RSNAConfigPairwiseCipherEnabled = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry->bDot11RSNAConfigPairwiseCipherEnabled = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11RSNAConfigPairwiseCiphersEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigPairwiseCiphersEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for Dot11RSNAConfigAuthenticationSuitesEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11RSNACONFIGAUTHENTICATIONSUITESTABLE_ARGS(pWsscfgDot11RSNAConfigAuthenticationSuitesEntry,\
   pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry,\
   pau1Dot11RSNAConfigAuthenticationSuiteIndex,\
   pau1Dot11RSNAConfigAuthenticationSuiteEnabled)\
  {\
  if (pau1Dot11RSNAConfigAuthenticationSuiteIndex != NULL)\
  {\
   pWsscfgDot11RSNAConfigAuthenticationSuitesEntry->MibObject.u4Dot11RSNAConfigAuthenticationSuiteIndex = *(UINT4 *) (pau1Dot11RSNAConfigAuthenticationSuiteIndex);\
   pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry->bDot11RSNAConfigAuthenticationSuiteIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry->bDot11RSNAConfigAuthenticationSuiteIndex = OSIX_FALSE;\
  }\
  if (pau1Dot11RSNAConfigAuthenticationSuiteEnabled != NULL)\
  {\
   pWsscfgDot11RSNAConfigAuthenticationSuitesEntry->MibObject.i4Dot11RSNAConfigAuthenticationSuiteEnabled = *(INT4 *) (pau1Dot11RSNAConfigAuthenticationSuiteEnabled);\
   pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry->bDot11RSNAConfigAuthenticationSuiteEnabled = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetDot11RSNAConfigAuthenticationSuitesEntry->bDot11RSNAConfigAuthenticationSuiteEnabled = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the index values in  structure for Dot11StationConfigEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11STATIONCONFIGTABLE_INDEX(pWsscfgDot11StationConfigEntry,\
   pau1IfIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11StationConfigEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11AuthenticationAlgorithmsEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11AUTHENTICATIONALGORITHMSTABLE_INDEX(pWsscfgDot11AuthenticationAlgorithmsEntry,\
   pau1IfIndex,\
   pau1Dot11AuthenticationAlgorithmsIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11AuthenticationAlgorithmsEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
  if (pau1Dot11AuthenticationAlgorithmsIndex != NULL)\
  {\
   pWsscfgDot11AuthenticationAlgorithmsEntry->MibObject.i4Dot11AuthenticationAlgorithmsIndex = *(INT4 *) (pau1Dot11AuthenticationAlgorithmsIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11WEPDefaultKeysEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11WEPDEFAULTKEYSTABLE_INDEX(pWsscfgDot11WEPDefaultKeysEntry,\
   pau1IfIndex,\
   pau1Dot11WEPDefaultKeyIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11WEPDefaultKeysEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
  if (pau1Dot11WEPDefaultKeyIndex != NULL)\
  {\
   pWsscfgDot11WEPDefaultKeysEntry->MibObject.i4Dot11WEPDefaultKeyIndex = *(INT4 *) (pau1Dot11WEPDefaultKeyIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11WEPKeyMappingsEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11WEPKEYMAPPINGSTABLE_INDEX(pWsscfgDot11WEPKeyMappingsEntry,\
   pau1IfIndex,\
   pau1Dot11WEPKeyMappingIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11WEPKeyMappingsEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
  if (pau1Dot11WEPKeyMappingIndex != NULL)\
  {\
   pWsscfgDot11WEPKeyMappingsEntry->MibObject.i4Dot11WEPKeyMappingIndex = *(INT4 *) (pau1Dot11WEPKeyMappingIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11PrivacyEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11PRIVACYTABLE_INDEX(pWsscfgDot11PrivacyEntry,\
   pau1IfIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11PrivacyEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11MultiDomainCapabilityEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11MULTIDOMAINCAPABILITYTABLE_INDEX(pWsscfgDot11MultiDomainCapabilityEntry,\
   pau1IfIndex,\
   pau1Dot11MultiDomainCapabilityIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11MultiDomainCapabilityEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
  if (pau1Dot11MultiDomainCapabilityIndex != NULL)\
  {\
   pWsscfgDot11MultiDomainCapabilityEntry->MibObject.i4Dot11MultiDomainCapabilityIndex = *(INT4 *) (pau1Dot11MultiDomainCapabilityIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11SpectrumManagementEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11SPECTRUMMANAGEMENTTABLE_INDEX(pWsscfgDot11SpectrumManagementEntry,\
   pau1IfIndex,\
   pau1Dot11SpectrumManagementIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11SpectrumManagementEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
  if (pau1Dot11SpectrumManagementIndex != NULL)\
  {\
   pWsscfgDot11SpectrumManagementEntry->MibObject.i4Dot11SpectrumManagementIndex = *(INT4 *) (pau1Dot11SpectrumManagementIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11RegulatoryClassesEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11REGULATORYCLASSESTABLE_INDEX(pWsscfgDot11RegulatoryClassesEntry,\
   pau1IfIndex,\
   pau1Dot11RegulatoryClassesIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11RegulatoryClassesEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
  if (pau1Dot11RegulatoryClassesIndex != NULL)\
  {\
   pWsscfgDot11RegulatoryClassesEntry->MibObject.i4Dot11RegulatoryClassesIndex = *(INT4 *) (pau1Dot11RegulatoryClassesIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11OperationEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11OPERATIONTABLE_INDEX(pWsscfgDot11OperationEntry,\
   pau1IfIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11OperationEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11GroupAddressesEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11GROUPADDRESSESTABLE_INDEX(pWsscfgDot11GroupAddressesEntry,\
   pau1IfIndex,\
   pau1Dot11GroupAddressesIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11GroupAddressesEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
  if (pau1Dot11GroupAddressesIndex != NULL)\
  {\
   pWsscfgDot11GroupAddressesEntry->MibObject.i4Dot11GroupAddressesIndex = *(INT4 *) (pau1Dot11GroupAddressesIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11EDCAEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11EDCATABLE_INDEX(pWsscfgDot11EDCAEntry,\
   pau1IfIndex,\
   pau1Dot11EDCATableIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11EDCAEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
  if (pau1Dot11EDCATableIndex != NULL)\
  {\
   pWsscfgDot11EDCAEntry->MibObject.i4Dot11EDCATableIndex = *(INT4 *) (pau1Dot11EDCATableIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11QAPEDCAEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11QAPEDCATABLE_INDEX(pWsscfgDot11QAPEDCAEntry,\
   pau1IfIndex,\
   pau1Dot11QAPEDCATableIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11QAPEDCAEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
  if (pau1Dot11QAPEDCATableIndex != NULL)\
  {\
   pWsscfgDot11QAPEDCAEntry->MibObject.i4Dot11QAPEDCATableIndex = *(INT4 *) (pau1Dot11QAPEDCATableIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11PhyOperationEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11PHYOPERATIONTABLE_INDEX(pWsscfgDot11PhyOperationEntry,\
   pau1IfIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11PhyOperationEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11PhyAntennaEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11PHYANTENNATABLE_INDEX(pWsscfgDot11PhyAntennaEntry,\
   pau1IfIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11PhyAntennaEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11PhyTxPowerEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11PHYTXPOWERTABLE_INDEX(pWsscfgDot11PhyTxPowerEntry,\
   pau1IfIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11PhyTxPowerEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11PhyFHSSEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11PHYFHSSTABLE_INDEX(pWsscfgDot11PhyFHSSEntry,\
   pau1IfIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11PhyFHSSEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11PhyDSSSEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11PHYDSSSTABLE_INDEX(pWsscfgDot11PhyDSSSEntry,\
   pau1IfIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11PhyDSSSEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11PhyIREntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11PHYIRTABLE_INDEX(pWsscfgDot11PhyIREntry,\
   pau1IfIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11PhyIREntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11AntennasListEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11ANTENNASLISTTABLE_INDEX(pWsscfgDot11AntennasListEntry,\
   pau1IfIndex,\
   pau1Dot11AntennaListIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11AntennasListEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
  if (pau1Dot11AntennaListIndex != NULL)\
  {\
   pWsscfgDot11AntennasListEntry->MibObject.i4Dot11AntennaListIndex = *(INT4 *) (pau1Dot11AntennaListIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11PhyOFDMEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11PHYOFDMTABLE_INDEX(pWsscfgDot11PhyOFDMEntry,\
   pau1IfIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11PhyOFDMEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11HoppingPatternEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11HOPPINGPATTERNTABLE_INDEX(pWsscfgDot11HoppingPatternEntry,\
   pau1IfIndex,\
   pau1Dot11HoppingPatternIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11HoppingPatternEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
  if (pau1Dot11HoppingPatternIndex != NULL)\
  {\
   pWsscfgDot11HoppingPatternEntry->MibObject.i4Dot11HoppingPatternIndex = *(INT4 *) (pau1Dot11HoppingPatternIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11PhyERPEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11PHYERPTABLE_INDEX(pWsscfgDot11PhyERPEntry,\
   pau1IfIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11PhyERPEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11RSNAConfigEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11RSNACONFIGTABLE_INDEX(pWsscfgDot11RSNAConfigEntry,\
   pau1IfIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11RSNAConfigEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11RSNAConfigPairwiseCiphersEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11RSNACONFIGPAIRWISECIPHERSTABLE_INDEX(pWsscfgDot11RSNAConfigPairwiseCiphersEntry,\
   pau1IfIndex,\
   pau1Dot11RSNAConfigPairwiseCipherIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgDot11RSNAConfigPairwiseCiphersEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
  if (pau1Dot11RSNAConfigPairwiseCipherIndex != NULL)\
  {\
   pWsscfgDot11RSNAConfigPairwiseCiphersEntry->MibObject.u4Dot11RSNAConfigPairwiseCipherIndex = *(UINT4 *) (pau1Dot11RSNAConfigPairwiseCipherIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for Dot11RSNAConfigAuthenticationSuitesEntry 
 using the input given in def file */

#define  WSSCFG_FILL_DOT11RSNACONFIGAUTHENTICATIONSUITESTABLE_INDEX(pWsscfgDot11RSNAConfigAuthenticationSuitesEntry,\
   pau1Dot11RSNAConfigAuthenticationSuiteIndex)\
  {\
  if (pau1Dot11RSNAConfigAuthenticationSuiteIndex != NULL)\
  {\
   pWsscfgDot11RSNAConfigAuthenticationSuitesEntry->MibObject.u4Dot11RSNAConfigAuthenticationSuiteIndex = *(UINT4 *) (pau1Dot11RSNAConfigAuthenticationSuiteIndex);\
  }\
 }

/* Macro used to fill the CLI structure for CapwapDot11WlanEntry 
 using the input given in def file */

#define  WSSCFG_FILL_CAPWAPDOT11WLANTABLE_ARGS(pWsscfgCapwapDot11WlanEntry,\
   pWsscfgIsSetCapwapDot11WlanEntry,\
   pau1CapwapDot11WlanProfileId,\
   pau1CapwapDot11WlanMacType,\
   pau1CapwapDot11WlanTunnelMode,\
   pau1CapwapDot11WlanTunnelModeLen,\
   pau1CapwapDot11WlanRowStatus)\
  {\
  if (pau1CapwapDot11WlanProfileId != NULL)\
  {\
   pWsscfgCapwapDot11WlanEntry->MibObject.u4CapwapDot11WlanProfileId = *(UINT4 *) (pau1CapwapDot11WlanProfileId);\
   pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanProfileId = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanProfileId = OSIX_FALSE;\
  }\
  if (pau1CapwapDot11WlanMacType != NULL)\
  {\
   pWsscfgCapwapDot11WlanEntry->MibObject.i4CapwapDot11WlanMacType = *(INT4 *) (pau1CapwapDot11WlanMacType);\
   pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanMacType = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanMacType = OSIX_FALSE;\
  }\
  if (pau1CapwapDot11WlanTunnelMode != NULL)\
  {\
   MEMCPY (pWsscfgCapwapDot11WlanEntry->MibObject.au1CapwapDot11WlanTunnelMode, pau1CapwapDot11WlanTunnelMode, *(INT4 *)pau1CapwapDot11WlanTunnelModeLen);\
   pWsscfgCapwapDot11WlanEntry->MibObject.i4CapwapDot11WlanTunnelModeLen = *(INT4 *)pau1CapwapDot11WlanTunnelModeLen;\
   pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanTunnelMode = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanTunnelMode = OSIX_FALSE;\
  }\
  if (pau1CapwapDot11WlanRowStatus != NULL)\
  {\
   pWsscfgCapwapDot11WlanEntry->MibObject.i4CapwapDot11WlanRowStatus = *(INT4 *) (pau1CapwapDot11WlanRowStatus);\
   pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetCapwapDot11WlanEntry->bCapwapDot11WlanRowStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for CapwapDot11WlanBindEntry 
 using the input given in def file */

#define  WSSCFG_FILL_CAPWAPDOT11WLANBINDTABLE_ARGS(pWsscfgCapwapDot11WlanBindEntry,\
   pWsscfgIsSetCapwapDot11WlanBindEntry,\
   pau1CapwapDot11WlanBindRowStatus,\
   pau1IfIndex,\
   pau1CapwapDot11WlanProfileId)\
  {\
  if (pau1CapwapDot11WlanBindRowStatus != NULL)\
  {\
   pWsscfgCapwapDot11WlanBindEntry->MibObject.i4CapwapDot11WlanBindRowStatus = *(INT4 *) (pau1CapwapDot11WlanBindRowStatus);\
   pWsscfgIsSetCapwapDot11WlanBindEntry->bCapwapDot11WlanBindRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetCapwapDot11WlanBindEntry->bCapwapDot11WlanBindRowStatus = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgCapwapDot11WlanBindEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetCapwapDot11WlanBindEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetCapwapDot11WlanBindEntry->bIfIndex = OSIX_FALSE;\
  }\
  if (pau1CapwapDot11WlanProfileId != NULL)\
  {\
   pWsscfgCapwapDot11WlanBindEntry->MibObject.u4CapwapDot11WlanProfileId = *(UINT4 *) (pau1CapwapDot11WlanProfileId);\
   pWsscfgIsSetCapwapDot11WlanBindEntry->bCapwapDot11WlanProfileId = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetCapwapDot11WlanBindEntry->bCapwapDot11WlanProfileId = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the index values in  structure for CapwapDot11WlanEntry 
 using the input given in def file */

#define  WSSCFG_FILL_CAPWAPDOT11WLANTABLE_INDEX(pWsscfgCapwapDot11WlanEntry,\
   pau1CapwapDot11WlanProfileId)\
  {\
  if (pau1CapwapDot11WlanProfileId != NULL)\
  {\
   pWsscfgCapwapDot11WlanEntry->MibObject.u4CapwapDot11WlanProfileId = *(UINT4 *) (pau1CapwapDot11WlanProfileId);\
  }\
 }
/* Macro used to fill the index values in  structure for CapwapDot11WlanBindEntry 
 using the input given in def file */

#define  WSSCFG_FILL_CAPWAPDOT11WLANBINDTABLE_INDEX(pWsscfgCapwapDot11WlanBindEntry,\
   pau1IfIndex,\
   pau1CapwapDot11WlanProfileId)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgCapwapDot11WlanBindEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
  if (pau1CapwapDot11WlanProfileId != NULL)\
  {\
   pWsscfgCapwapDot11WlanBindEntry->MibObject.u4CapwapDot11WlanProfileId = *(UINT4 *) (pau1CapwapDot11WlanProfileId);\
  }\
 }

/* Macro used to fill the CLI structure for FsDot11CapabilityProfileEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11STATIONCONFIGTABLE_ARGS(pWsscfgFsDot11StationConfigEntry,\
   pWsscfgIsSetFsDot11StationConfigEntry,\
   pau1FsDot11SupressSSID,\
   pau1FsDot11VlanId,\
   pau1FsDot11BandwidthThresh,\
   pau1IfIndex)\
  {\
  if (pau1FsDot11SupressSSID != NULL)\
  {\
   pWsscfgFsDot11StationConfigEntry->MibObject.i4FsDot11SupressSSID = *(INT4 *) (pau1FsDot11SupressSSID);\
   pWsscfgIsSetFsDot11StationConfigEntry->bFsDot11SupressSSID = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11StationConfigEntry->bFsDot11SupressSSID = OSIX_FALSE;\
  }\
  if (pau1FsDot11VlanId != NULL)\
  {\
   pWsscfgFsDot11StationConfigEntry->MibObject.i4FsDot11VlanId = *(INT4 *) (pau1FsDot11VlanId);\
   pWsscfgIsSetFsDot11StationConfigEntry->bFsDot11VlanId = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11StationConfigEntry->bFsDot11VlanId = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11StationConfigEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetFsDot11StationConfigEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11StationConfigEntry->bIfIndex = OSIX_FALSE;\
  }\
  if (pau1FsDot11BandwidthThresh != NULL)\
  {\
   pWsscfgFsDot11StationConfigEntry->MibObject.i4FsDot11BandwidthThresh = *(INT4 *) (pau1FsDot11BandwidthThresh);\
   pWsscfgIsSetFsDot11StationConfigEntry->bFsDot11BandwidthThresh = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11StationConfigEntry->bFsDot11BandwidthThresh = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsDot11CapabilityProfileEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11CAPABILITYPROFILETABLE_ARGS(pWsscfgFsDot11CapabilityProfileEntry,\
   pWsscfgIsSetFsDot11CapabilityProfileEntry,\
   pau1FsDot11CapabilityProfileName,\
   pau1FsDot11CapabilityProfileNameLen,\
   pau1FsDot11CFPollable,\
   pau1FsDot11CFPollRequest,\
   pau1FsDot11PrivacyOptionImplemented,\
   pau1FsDot11ShortPreambleOptionImplemented,\
   pau1FsDot11PBCCOptionImplemented,\
   pau1FsDot11ChannelAgilityPresent,\
   pau1FsDot11QosOptionImplemented,\
   pau1FsDot11SpectrumManagementRequired,\
   pau1FsDot11ShortSlotTimeOptionImplemented,\
   pau1FsDot11APSDOptionImplemented,\
   pau1FsDot11DSSSOFDMOptionEnabled,\
   pau1FsDot11DelayedBlockAckOptionImplemented,\
   pau1FsDot11ImmediateBlockAckOptionImplemented,\
   pau1FsDot11QAckOptionImplemented,\
   pau1FsDot11QueueRequestOptionImplemented,\
   pau1FsDot11TXOPRequestOptionImplemented,\
   pau1FsDot11RSNAOptionImplemented,\
   pau1FsDot11RSNAPreauthenticationImplemented,\
   pau1FsDot11CapabilityRowStatus)\
  {\
  if (pau1FsDot11CapabilityProfileName != NULL)\
  {\
   MEMCPY (pWsscfgFsDot11CapabilityProfileEntry->MibObject.au1FsDot11CapabilityProfileName, pau1FsDot11CapabilityProfileName, *(INT4 *)pau1FsDot11CapabilityProfileNameLen);\
   pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11CapabilityProfileNameLen = *(INT4 *)pau1FsDot11CapabilityProfileNameLen;\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11CapabilityProfileName = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11CapabilityProfileName = OSIX_FALSE;\
  }\
  if (pau1FsDot11CFPollable != NULL)\
  {\
   pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11CFPollable = *(INT4 *) (pau1FsDot11CFPollable);\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11CFPollable = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11CFPollable = OSIX_FALSE;\
  }\
  if (pau1FsDot11CFPollRequest != NULL)\
  {\
   pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11CFPollRequest = *(INT4 *) (pau1FsDot11CFPollRequest);\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11CFPollRequest = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11CFPollRequest = OSIX_FALSE;\
  }\
  if (pau1FsDot11PrivacyOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11PrivacyOptionImplemented = *(INT4 *) (pau1FsDot11PrivacyOptionImplemented);\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11PrivacyOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11PrivacyOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11ShortPreambleOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11ShortPreambleOptionImplemented = *(INT4 *) (pau1FsDot11ShortPreambleOptionImplemented);\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11ShortPreambleOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11ShortPreambleOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11PBCCOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11PBCCOptionImplemented = *(INT4 *) (pau1FsDot11PBCCOptionImplemented);\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11PBCCOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11PBCCOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11ChannelAgilityPresent != NULL)\
  {\
   pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11ChannelAgilityPresent = *(INT4 *) (pau1FsDot11ChannelAgilityPresent);\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11ChannelAgilityPresent = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11ChannelAgilityPresent = OSIX_FALSE;\
  }\
  if (pau1FsDot11QosOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11QosOptionImplemented = *(INT4 *) (pau1FsDot11QosOptionImplemented);\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11QosOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11QosOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11SpectrumManagementRequired != NULL)\
  {\
   pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11SpectrumManagementRequired = *(INT4 *) (pau1FsDot11SpectrumManagementRequired);\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11SpectrumManagementRequired = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11SpectrumManagementRequired = OSIX_FALSE;\
  }\
  if (pau1FsDot11ShortSlotTimeOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11ShortSlotTimeOptionImplemented = *(INT4 *) (pau1FsDot11ShortSlotTimeOptionImplemented);\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11ShortSlotTimeOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11ShortSlotTimeOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11APSDOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11APSDOptionImplemented = *(INT4 *) (pau1FsDot11APSDOptionImplemented);\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11APSDOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11APSDOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11DSSSOFDMOptionEnabled != NULL)\
  {\
   pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11DSSSOFDMOptionEnabled = *(INT4 *) (pau1FsDot11DSSSOFDMOptionEnabled);\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11DSSSOFDMOptionEnabled = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11DSSSOFDMOptionEnabled = OSIX_FALSE;\
  }\
  if (pau1FsDot11DelayedBlockAckOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11DelayedBlockAckOptionImplemented = *(INT4 *) (pau1FsDot11DelayedBlockAckOptionImplemented);\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11DelayedBlockAckOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11DelayedBlockAckOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11ImmediateBlockAckOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11ImmediateBlockAckOptionImplemented = *(INT4 *) (pau1FsDot11ImmediateBlockAckOptionImplemented);\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11ImmediateBlockAckOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11ImmediateBlockAckOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11QAckOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11QAckOptionImplemented = *(INT4 *) (pau1FsDot11QAckOptionImplemented);\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11QAckOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11QAckOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11QueueRequestOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11QueueRequestOptionImplemented = *(INT4 *) (pau1FsDot11QueueRequestOptionImplemented);\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11QueueRequestOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11QueueRequestOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11TXOPRequestOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11TXOPRequestOptionImplemented = *(INT4 *) (pau1FsDot11TXOPRequestOptionImplemented);\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11TXOPRequestOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11TXOPRequestOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11RSNAOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11RSNAOptionImplemented = *(INT4 *) (pau1FsDot11RSNAOptionImplemented);\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11RSNAOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11RSNAOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11RSNAPreauthenticationImplemented != NULL)\
  {\
   pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11RSNAPreauthenticationImplemented = *(INT4 *) (pau1FsDot11RSNAPreauthenticationImplemented);\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11RSNAPreauthenticationImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11RSNAPreauthenticationImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11CapabilityRowStatus != NULL)\
  {\
   pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11CapabilityRowStatus = *(INT4 *) (pau1FsDot11CapabilityRowStatus);\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11CapabilityRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityProfileEntry->bFsDot11CapabilityRowStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsDot11AuthenticationProfileEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11AUTHENTICATIONPROFILETABLE_ARGS(pWsscfgFsDot11AuthenticationProfileEntry,\
   pWsscfgIsSetFsDot11AuthenticationProfileEntry,\
   pau1FsDot11AuthenticationProfileName,\
   pau1FsDot11AuthenticationProfileNameLen,\
   pau1FsDot11AuthenticationAlgorithm,\
   pau1FsDot11WepKeyIndex,\
   pau1FsDot11WepKeyType,\
   pau1FsDot11WepKeyLength,\
   pau1FsDot11WepKey,\
   pau1FsDot11WepKeyLen,\
   pau1FsDot11WebAuthentication,\
   pau1FsDot11AuthenticationRowStatus)\
  {\
  if (pau1FsDot11AuthenticationProfileName != NULL)\
  {\
   MEMCPY (pWsscfgFsDot11AuthenticationProfileEntry->MibObject.au1FsDot11AuthenticationProfileName, pau1FsDot11AuthenticationProfileName, *(INT4 *)pau1FsDot11AuthenticationProfileNameLen);\
   pWsscfgFsDot11AuthenticationProfileEntry->MibObject.i4FsDot11AuthenticationProfileNameLen = *(INT4 *)pau1FsDot11AuthenticationProfileNameLen;\
   pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11AuthenticationProfileName = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11AuthenticationProfileName = OSIX_FALSE;\
  }\
  if (pau1FsDot11AuthenticationAlgorithm != NULL)\
  {\
   pWsscfgFsDot11AuthenticationProfileEntry->MibObject.i4FsDot11AuthenticationAlgorithm = *(INT4 *) (pau1FsDot11AuthenticationAlgorithm);\
   pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11AuthenticationAlgorithm = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11AuthenticationAlgorithm = OSIX_FALSE;\
  }\
  if (pau1FsDot11WepKeyIndex != NULL)\
  {\
   pWsscfgFsDot11AuthenticationProfileEntry->MibObject.i4FsDot11WepKeyIndex = *(INT4 *) (pau1FsDot11WepKeyIndex);\
   pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKeyIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKeyIndex = OSIX_FALSE;\
  }\
  if (pau1FsDot11WepKeyType != NULL)\
  {\
   pWsscfgFsDot11AuthenticationProfileEntry->MibObject.i4FsDot11WepKeyType = *(INT4 *) (pau1FsDot11WepKeyType);\
   pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKeyType = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKeyType = OSIX_FALSE;\
  }\
  if (pau1FsDot11WepKeyLength != NULL)\
  {\
   pWsscfgFsDot11AuthenticationProfileEntry->MibObject.i4FsDot11WepKeyLength = *(INT4 *) (pau1FsDot11WepKeyLength);\
   pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKeyLength = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKeyLength = OSIX_FALSE;\
  }\
  if (pau1FsDot11WepKey != NULL)\
  {\
   MEMCPY (pWsscfgFsDot11AuthenticationProfileEntry->MibObject.au1FsDot11WepKey, pau1FsDot11WepKey, *(INT4 *)pau1FsDot11WepKeyLen);\
   pWsscfgFsDot11AuthenticationProfileEntry->MibObject.i4FsDot11WepKeyLen = *(INT4 *)pau1FsDot11WepKeyLen;\
   pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKey = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WepKey = OSIX_FALSE;\
  }\
  if (pau1FsDot11WebAuthentication != NULL)\
  {\
   pWsscfgFsDot11AuthenticationProfileEntry->MibObject.i4FsDot11WebAuthentication = *(INT4 *) (pau1FsDot11WebAuthentication);\
   pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WebAuthentication = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11WebAuthentication = OSIX_FALSE;\
  }\
  if (pau1FsDot11AuthenticationRowStatus != NULL)\
  {\
   pWsscfgFsDot11AuthenticationProfileEntry->MibObject.i4FsDot11AuthenticationRowStatus = *(INT4 *) (pau1FsDot11AuthenticationRowStatus);\
   pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11AuthenticationRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11AuthenticationProfileEntry->bFsDot11AuthenticationRowStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsSecurityWebAuthGuestInfoEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSSECURITYWEBAUTHGUESTINFOTABLE_ARGS(pWsscfgFsSecurityWebAuthGuestInfoEntry,\
   pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry,\
   pau1FsSecurityWebAuthUName,\
   pau1FsSecurityWebAuthUNameLen,\
   pau1FsSecurityWebAuthUPassword,\
   pau1FsSecurityWebAuthUPasswordLen,\
   pau1FsSecurityWlanProfileId,\
   pau1FsSecurityWebAuthUserLifetime,\
   pau1FsSecurityWebAuthGuestInfoRowStatus)\
  {\
  if (pau1FsSecurityWebAuthUName != NULL)\
  {\
   MEMCPY (pWsscfgFsSecurityWebAuthGuestInfoEntry->MibObject.au1FsSecurityWebAuthUName, pau1FsSecurityWebAuthUName, *(INT4 *)pau1FsSecurityWebAuthUNameLen);\
   pWsscfgFsSecurityWebAuthGuestInfoEntry->MibObject.i4FsSecurityWebAuthUNameLen = *(INT4 *)pau1FsSecurityWebAuthUNameLen;\
   pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry->bFsSecurityWebAuthUName = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry->bFsSecurityWebAuthUName = OSIX_FALSE;\
  }\
  if (pau1FsSecurityWlanProfileId != NULL)\
  {\
   pWsscfgFsSecurityWebAuthGuestInfoEntry->MibObject.i4FsSecurityWlanProfileId = *(INT4 *) (pau1FsSecurityWlanProfileId);\
   pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry->bFsSecurityWlanProfileId = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry->bFsSecurityWlanProfileId = OSIX_FALSE;\
  }\
  if (pau1FsSecurityWebAuthUserLifetime != NULL)\
  {\
   pWsscfgFsSecurityWebAuthGuestInfoEntry->MibObject.i4FsSecurityWebAuthUserLifetime = *(INT4 *) (pau1FsSecurityWebAuthUserLifetime);\
   pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry->bFsSecurityWebAuthUserLifetime = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry->bFsSecurityWebAuthUserLifetime = OSIX_FALSE;\
  }\
  if (pau1FsSecurityWebAuthGuestInfoRowStatus != NULL)\
  {\
   pWsscfgFsSecurityWebAuthGuestInfoEntry->MibObject.i4FsSecurityWebAuthGuestInfoRowStatus = *(INT4 *) (pau1FsSecurityWebAuthGuestInfoRowStatus);\
   pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry->bFsSecurityWebAuthGuestInfoRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry->bFsSecurityWebAuthGuestInfoRowStatus = OSIX_FALSE;\
  }\
  if (pau1FsSecurityWebAuthUPassword != NULL)\
  {\
   MEMCPY (pWsscfgFsSecurityWebAuthGuestInfoEntry->MibObject.au1FsSecurityWebAuthUPassword, pau1FsSecurityWebAuthUPassword, *(INT4 *)pau1FsSecurityWebAuthUPasswordLen);\
   pWsscfgFsSecurityWebAuthGuestInfoEntry->MibObject.i4FsSecurityWebAuthUPasswordLen = *(INT4 *)pau1FsSecurityWebAuthUPasswordLen;\
   pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry->bFsSecurityWebAuthUPassword = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsSecurityWebAuthGuestInfoEntry->bFsSecurityWebAuthUPassword = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsStationQosParamsEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSSTATIONQOSPARAMSTABLE_ARGS(pWsscfgFsStationQosParamsEntry,\
   pWsscfgIsSetFsStationQosParamsEntry,\
   pau1FsStaMacAddress,\
   pau1FsStaQoSPriority,\
   pau1FsStaQoSDscp,\
   pau1IfIndex)\
  {\
  if (pau1FsStaMacAddress != NULL)\
  {\
   StrToMac ((UINT1 *) &pau1FsStaMacAddress, pWsscfgFsStationQosParamsEntry->MibObject.FsStaMacAddress);\
   pWsscfgIsSetFsStationQosParamsEntry->bFsStaMacAddress = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsStationQosParamsEntry->bFsStaMacAddress = OSIX_FALSE;\
  }\
  if (pau1FsStaQoSPriority != NULL)\
  {\
   pWsscfgFsStationQosParamsEntry->MibObject.i4FsStaQoSPriority = *(INT4 *) (pau1FsStaQoSPriority);\
   pWsscfgIsSetFsStationQosParamsEntry->bFsStaQoSPriority = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsStationQosParamsEntry->bFsStaQoSPriority = OSIX_FALSE;\
  }\
  if (pau1FsStaQoSDscp != NULL)\
  {\
   pWsscfgFsStationQosParamsEntry->MibObject.i4FsStaQoSDscp = *(INT4 *) (pau1FsStaQoSDscp);\
   pWsscfgIsSetFsStationQosParamsEntry->bFsStaQoSDscp = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsStationQosParamsEntry->bFsStaQoSDscp = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsStationQosParamsEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetFsStationQosParamsEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsStationQosParamsEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsVlanIsolationEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSVLANISOLATIONTABLE_ARGS(pWsscfgFsVlanIsolationEntry,\
   pWsscfgIsSetFsVlanIsolationEntry,\
   pau1FsVlanIsolation,\
   pau1IfIndex)\
  {\
  if (pau1FsVlanIsolation != NULL)\
  {\
   pWsscfgFsVlanIsolationEntry->MibObject.i4FsVlanIsolation = *(INT4 *) (pau1FsVlanIsolation);\
   pWsscfgIsSetFsVlanIsolationEntry->bFsVlanIsolation = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsVlanIsolationEntry->bFsVlanIsolation = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsVlanIsolationEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetFsVlanIsolationEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsVlanIsolationEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsDot11RadioConfigEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11RADIOCONFIGTABLE_ARGS(pWsscfgFsDot11RadioConfigEntry,\
   pWsscfgIsSetFsDot11RadioConfigEntry,\
   pau1FsDot11RadioType,\
   pau1FsDot11RowStatus,\
   pau1IfIndex)\
  {\
  if (pau1FsDot11RadioType != NULL)\
  {\
   pWsscfgFsDot11RadioConfigEntry->MibObject.u4FsDot11RadioType = *(UINT4 *) (pau1FsDot11RadioType);\
   pWsscfgIsSetFsDot11RadioConfigEntry->bFsDot11RadioType = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11RadioConfigEntry->bFsDot11RadioType = OSIX_FALSE;\
  }\
  if (pau1FsDot11RowStatus != NULL)\
  {\
   pWsscfgFsDot11RadioConfigEntry->MibObject.i4FsDot11RowStatus = *(INT4 *) (pau1FsDot11RowStatus);\
   pWsscfgIsSetFsDot11RadioConfigEntry->bFsDot11RowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11RadioConfigEntry->bFsDot11RowStatus = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11RadioConfigEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetFsDot11RadioConfigEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11RadioConfigEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsDot11QosProfileEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11QOSPROFILETABLE_ARGS(pWsscfgFsDot11QosProfileEntry,\
   pWsscfgIsSetFsDot11QosProfileEntry,\
   pau1FsDot11QosProfileName,\
   pau1FsDot11QosProfileNameLen,\
   pau1FsDot11QosTraffic,\
   pau1FsDot11QosPassengerTrustMode,\
            pau1FsDot11QosRateLimit,\
            pau1FsDot11UpStreamCIR,\
            pau1FsDot11UpStreamCBS,\
            pau1FsDot11UpStreamEIR,\
            pau1FsDot11UpStreamEBS,\
            pau1FsDot11DownStreamCIR,\
            pau1FsDot11DownStreamCBS,\
            pau1FsDot11DownStreamEIR,\
            pau1FsDot11DownStreamEBS,\
   pau1FsDot11QosRowStatus)\
  {\
  if (pau1FsDot11QosProfileName != NULL)\
  {\
   MEMCPY (pWsscfgFsDot11QosProfileEntry->MibObject.au1FsDot11QosProfileName, pau1FsDot11QosProfileName, *(INT4 *)pau1FsDot11QosProfileNameLen);\
   pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11QosProfileNameLen = *(INT4 *)pau1FsDot11QosProfileNameLen;\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosProfileName = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosProfileName = OSIX_FALSE;\
  }\
  if (pau1FsDot11QosTraffic != NULL)\
  {\
   pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11QosTraffic = *(INT4 *) (pau1FsDot11QosTraffic);\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosTraffic = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosTraffic = OSIX_FALSE;\
  }\
  if (pau1FsDot11QosPassengerTrustMode != NULL)\
  {\
   pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11QosPassengerTrustMode = *(INT4 *) (pau1FsDot11QosPassengerTrustMode);\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosPassengerTrustMode = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosPassengerTrustMode = OSIX_FALSE;\
  }\
  if (pau1FsDot11QosRateLimit != NULL)\
  {\
   pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11QosRateLimit = *(INT4 *) (pau1FsDot11QosRateLimit);\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosRateLimit = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosRateLimit = OSIX_FALSE;\
  }\
  if (pau1FsDot11UpStreamCIR != NULL)\
  {\
   pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamCIR = *(INT4 *) (pau1FsDot11UpStreamCIR);\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamCIR = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamCIR = OSIX_FALSE;\
  }\
  if (pau1FsDot11UpStreamCBS != NULL)\
  {\
   pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamCBS = *(INT4 *) (pau1FsDot11UpStreamCBS);\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamCBS = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamCBS = OSIX_FALSE;\
  }\
  if (pau1FsDot11UpStreamEIR != NULL)\
  {\
   pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamEIR = *(INT4 *) (pau1FsDot11UpStreamEIR);\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamEIR = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamEIR = OSIX_FALSE;\
  }\
  if (pau1FsDot11UpStreamEBS != NULL)\
  {\
   pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11UpStreamEBS = *(INT4 *) (pau1FsDot11UpStreamEBS);\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamEBS = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11UpStreamEBS = OSIX_FALSE;\
  }\
  if (pau1FsDot11DownStreamCIR != NULL)\
  {\
   pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11DownStreamCIR = *(INT4 *) (pau1FsDot11DownStreamCIR);\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamCIR = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamCIR = OSIX_FALSE;\
  }\
  if (pau1FsDot11DownStreamCBS != NULL)\
  {\
   pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11DownStreamCBS = *(INT4 *) (pau1FsDot11DownStreamCBS);\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamCBS = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamCBS = OSIX_FALSE;\
  }\
  if (pau1FsDot11DownStreamEIR != NULL)\
  {\
   pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11DownStreamEIR = *(INT4 *) (pau1FsDot11DownStreamEIR);\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamEIR = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamEIR = OSIX_FALSE;\
  }\
  if (pau1FsDot11DownStreamEBS != NULL)\
  {\
   pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11DownStreamEBS = *(INT4 *) (pau1FsDot11DownStreamEBS);\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamEBS = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11DownStreamEBS = OSIX_FALSE;\
  }\
  if (pau1FsDot11QosRowStatus != NULL)\
  {\
   pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11QosRowStatus = *(INT4 *) (pau1FsDot11QosRowStatus);\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11QosProfileEntry->bFsDot11QosRowStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsDot11WlanCapabilityProfileEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11WLANCAPABILITYPROFILETABLE_ARGS(pWsscfgFsDot11WlanCapabilityProfileEntry,\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry,\
   pau1FsDot11WlanCFPollable,\
   pau1FsDot11WlanCFPollRequest,\
   pau1FsDot11WlanPrivacyOptionImplemented,\
   pau1FsDot11WlanShortPreambleOptionImplemented,\
   pau1FsDot11WlanPBCCOptionImplemented,\
   pau1FsDot11WlanChannelAgilityPresent,\
   pau1FsDot11WlanQosOptionImplemented,\
   pau1FsDot11WlanSpectrumManagementRequired,\
   pau1FsDot11WlanShortSlotTimeOptionImplemented,\
   pau1FsDot11WlanAPSDOptionImplemented,\
   pau1FsDot11WlanDSSSOFDMOptionEnabled,\
   pau1FsDot11WlanDelayedBlockAckOptionImplemented,\
   pau1FsDot11WlanImmediateBlockAckOptionImplemented,\
   pau1FsDot11WlanQAckOptionImplemented,\
   pau1FsDot11WlanQueueRequestOptionImplemented,\
   pau1FsDot11WlanTXOPRequestOptionImplemented,\
   pau1FsDot11WlanRSNAOptionImplemented,\
   pau1FsDot11WlanRSNAPreauthenticationImplemented,\
   pau1FsDot11WlanCapabilityRowStatus,\
   pau1IfIndex)\
  {\
  if (pau1FsDot11WlanCFPollable != NULL)\
  {\
   pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.i4FsDot11WlanCFPollable = *(INT4 *) (pau1FsDot11WlanCFPollable);\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanCFPollable = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanCFPollable = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanCFPollRequest != NULL)\
  {\
   pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.i4FsDot11WlanCFPollRequest = *(INT4 *) (pau1FsDot11WlanCFPollRequest);\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanCFPollRequest = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanCFPollRequest = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanPrivacyOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.i4FsDot11WlanPrivacyOptionImplemented = *(INT4 *) (pau1FsDot11WlanPrivacyOptionImplemented);\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanPrivacyOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanPrivacyOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanShortPreambleOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.i4FsDot11WlanShortPreambleOptionImplemented = *(INT4 *) (pau1FsDot11WlanShortPreambleOptionImplemented);\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanShortPreambleOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanShortPreambleOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanPBCCOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.i4FsDot11WlanPBCCOptionImplemented = *(INT4 *) (pau1FsDot11WlanPBCCOptionImplemented);\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanPBCCOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanPBCCOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanChannelAgilityPresent != NULL)\
  {\
   pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.i4FsDot11WlanChannelAgilityPresent = *(INT4 *) (pau1FsDot11WlanChannelAgilityPresent);\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanChannelAgilityPresent = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanChannelAgilityPresent = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanQosOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.i4FsDot11WlanQosOptionImplemented = *(INT4 *) (pau1FsDot11WlanQosOptionImplemented);\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanQosOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanQosOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanSpectrumManagementRequired != NULL)\
  {\
   pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.i4FsDot11WlanSpectrumManagementRequired = *(INT4 *) (pau1FsDot11WlanSpectrumManagementRequired);\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanSpectrumManagementRequired = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanSpectrumManagementRequired = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanShortSlotTimeOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.i4FsDot11WlanShortSlotTimeOptionImplemented = *(INT4 *) (pau1FsDot11WlanShortSlotTimeOptionImplemented);\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanShortSlotTimeOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanShortSlotTimeOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanAPSDOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.i4FsDot11WlanAPSDOptionImplemented = *(INT4 *) (pau1FsDot11WlanAPSDOptionImplemented);\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanAPSDOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanAPSDOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanDSSSOFDMOptionEnabled != NULL)\
  {\
   pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.i4FsDot11WlanDSSSOFDMOptionEnabled = *(INT4 *) (pau1FsDot11WlanDSSSOFDMOptionEnabled);\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanDSSSOFDMOptionEnabled = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanDSSSOFDMOptionEnabled = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanDelayedBlockAckOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.i4FsDot11WlanDelayedBlockAckOptionImplemented = *(INT4 *) (pau1FsDot11WlanDelayedBlockAckOptionImplemented);\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanDelayedBlockAckOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanDelayedBlockAckOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanImmediateBlockAckOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.i4FsDot11WlanImmediateBlockAckOptionImplemented = *(INT4 *) (pau1FsDot11WlanImmediateBlockAckOptionImplemented);\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanImmediateBlockAckOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanImmediateBlockAckOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanQAckOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.i4FsDot11WlanQAckOptionImplemented = *(INT4 *) (pau1FsDot11WlanQAckOptionImplemented);\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanQAckOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanQAckOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanQueueRequestOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.i4FsDot11WlanQueueRequestOptionImplemented = *(INT4 *) (pau1FsDot11WlanQueueRequestOptionImplemented);\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanQueueRequestOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanQueueRequestOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanTXOPRequestOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.i4FsDot11WlanTXOPRequestOptionImplemented = *(INT4 *) (pau1FsDot11WlanTXOPRequestOptionImplemented);\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanTXOPRequestOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanTXOPRequestOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanRSNAOptionImplemented != NULL)\
  {\
   pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.i4FsDot11WlanRSNAOptionImplemented = *(INT4 *) (pau1FsDot11WlanRSNAOptionImplemented);\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanRSNAOptionImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanRSNAOptionImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanRSNAPreauthenticationImplemented != NULL)\
  {\
   pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.i4FsDot11WlanRSNAPreauthenticationImplemented = *(INT4 *) (pau1FsDot11WlanRSNAPreauthenticationImplemented);\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanRSNAPreauthenticationImplemented = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanRSNAPreauthenticationImplemented = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanCapabilityRowStatus != NULL)\
  {\
   pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.i4FsDot11WlanCapabilityRowStatus = *(INT4 *) (pau1FsDot11WlanCapabilityRowStatus);\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanCapabilityRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bFsDot11WlanCapabilityRowStatus = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanCapabilityProfileEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsDot11WlanAuthenticationProfileEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11WLANAUTHENTICATIONPROFILETABLE_ARGS(pWsscfgFsDot11WlanAuthenticationProfileEntry,\
   pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry,\
   pau1FsDot11WlanAuthenticationAlgorithm,\
   pau1FsDot11WlanWepKeyIndex,\
   pau1FsDot11WlanWepKeyType,\
   pau1FsDot11WlanWepKeyLength,\
   pau1FsDot11WlanWepKey,\
   pau1FsDot11WlanWepKeyLen,\
   pau1FsDot11WlanWebAuthentication,\
   pau1FsDot11WlanAuthenticationRowStatus,\
   pau1IfIndex)\
  {\
  if (pau1FsDot11WlanAuthenticationAlgorithm != NULL)\
  {\
   pWsscfgFsDot11WlanAuthenticationProfileEntry->MibObject.i4FsDot11WlanAuthenticationAlgorithm = *(INT4 *) (pau1FsDot11WlanAuthenticationAlgorithm);\
   pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bFsDot11WlanAuthenticationAlgorithm = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bFsDot11WlanAuthenticationAlgorithm = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanWepKeyIndex != NULL)\
  {\
   pWsscfgFsDot11WlanAuthenticationProfileEntry->MibObject.i4FsDot11WlanWepKeyIndex = *(INT4 *) (pau1FsDot11WlanWepKeyIndex);\
   pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bFsDot11WlanWepKeyIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bFsDot11WlanWepKeyIndex = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanWepKeyType != NULL)\
  {\
   pWsscfgFsDot11WlanAuthenticationProfileEntry->MibObject.i4FsDot11WlanWepKeyType = *(INT4 *) (pau1FsDot11WlanWepKeyType);\
   pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bFsDot11WlanWepKeyType = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bFsDot11WlanWepKeyType = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanWepKeyLength != NULL)\
  {\
   pWsscfgFsDot11WlanAuthenticationProfileEntry->MibObject.i4FsDot11WlanWepKeyLength = *(INT4 *) (pau1FsDot11WlanWepKeyLength);\
   pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bFsDot11WlanWepKeyLength = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bFsDot11WlanWepKeyLength = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanWepKey != NULL)\
  {\
   MEMCPY (pWsscfgFsDot11WlanAuthenticationProfileEntry->MibObject.au1FsDot11WlanWepKey, pau1FsDot11WlanWepKey, *(INT4 *)pau1FsDot11WlanWepKeyLen);\
   pWsscfgFsDot11WlanAuthenticationProfileEntry->MibObject.i4FsDot11WlanWepKeyLen = *(INT4 *)pau1FsDot11WlanWepKeyLen;\
   pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bFsDot11WlanWepKey = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bFsDot11WlanWepKey = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanWebAuthentication != NULL)\
  {\
   pWsscfgFsDot11WlanAuthenticationProfileEntry->MibObject.i4FsDot11WlanWebAuthentication = *(INT4 *) (pau1FsDot11WlanWebAuthentication);\
   pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bFsDot11WlanWebAuthentication = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bFsDot11WlanWebAuthentication = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanAuthenticationRowStatus != NULL)\
  {\
   pWsscfgFsDot11WlanAuthenticationProfileEntry->MibObject.i4FsDot11WlanAuthenticationRowStatus = *(INT4 *) (pau1FsDot11WlanAuthenticationRowStatus);\
   pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bFsDot11WlanAuthenticationRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bFsDot11WlanAuthenticationRowStatus = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11WlanAuthenticationProfileEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanAuthenticationProfileEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsDot11WlanQosProfileEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11WLANQOSPROFILETABLE_ARGS(pWsscfgFsDot11WlanQosProfileEntry,\
   pWsscfgIsSetFsDot11WlanQosProfileEntry,\
   pau1FsDot11WlanQosTraffic,\
   pau1FsDot11WlanQosPassengerTrustMode,\
            pau1FsDot11WlanQosRateLimit,\
            pau1FsDot11WlanUpStreamCIR,\
            pau1FsDot11WlanUpStreamCBS,\
            pau1FsDot11WlanUpStreamEIR,\
            pau1FsDot11WlanUpStreamEBS,\
            pau1FsDot11WlanDownStreamCIR,\
            pau1FsDot11WlanDownStreamCBS,\
            pau1FsDot11WlanDownStreamEIR,\
            pau1FsDot11WlanDownStreamEBS,\
   pau1FsDot11WlanQosRowStatus,\
   pau1IfIndex)\
  {\
  if (pau1FsDot11WlanQosTraffic != NULL)\
  {\
   pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanQosTraffic = *(INT4 *) (pau1FsDot11WlanQosTraffic);\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanQosTraffic = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanQosTraffic = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanQosPassengerTrustMode != NULL)\
  {\
   pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanQosPassengerTrustMode = *(INT4 *) (pau1FsDot11WlanQosPassengerTrustMode);\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanQosPassengerTrustMode = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanQosPassengerTrustMode = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanQosRateLimit != NULL)\
  {\
   pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanQosRateLimit = *(INT4 *) (pau1FsDot11WlanQosRateLimit);\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanQosRateLimit = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanQosRateLimit = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanUpStreamCIR != NULL)\
  {\
   pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanUpStreamCIR = *(INT4 *) (pau1FsDot11WlanUpStreamCIR);\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanUpStreamCIR = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanUpStreamCIR = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanUpStreamCBS != NULL)\
  {\
   pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanUpStreamCBS = *(INT4 *) (pau1FsDot11WlanUpStreamCBS);\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanUpStreamCBS = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanUpStreamCBS = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanUpStreamEIR != NULL)\
  {\
   pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanUpStreamEIR = *(INT4 *) (pau1FsDot11WlanUpStreamEIR);\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanUpStreamEIR = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanUpStreamEIR = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanUpStreamEBS != NULL)\
  {\
   pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanUpStreamEBS = *(INT4 *) (pau1FsDot11WlanUpStreamEBS);\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanUpStreamEBS = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanUpStreamEBS = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanDownStreamCIR != NULL)\
  {\
   pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanDownStreamCIR = *(INT4 *) (pau1FsDot11WlanDownStreamCIR);\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamCIR = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamCIR = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanDownStreamCBS != NULL)\
  {\
   pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanDownStreamCBS = *(INT4 *) (pau1FsDot11WlanDownStreamCBS);\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamCBS = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamCBS = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanDownStreamEIR != NULL)\
  {\
   pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanDownStreamEIR = *(INT4 *) (pau1FsDot11WlanDownStreamEIR);\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamEIR = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamEIR = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanDownStreamEBS != NULL)\
  {\
   pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanDownStreamEBS = *(INT4 *) (pau1FsDot11WlanDownStreamEBS);\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamEBS = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanDownStreamEBS = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanQosRowStatus != NULL)\
  {\
   pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4FsDot11WlanQosRowStatus = *(INT4 *) (pau1FsDot11WlanQosRowStatus);\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanQosRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanQosProfileEntry->bFsDot11WlanQosRowStatus = OSIX_FALSE;\
  }\
                if (pau1IfIndex != NULL)\
  {\
                        pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
                        pWsscfgIsSetFsDot11WlanQosProfileEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
                        pWsscfgIsSetFsDot11WlanQosProfileEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsDot11RadioQosEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11RADIOQOSTABLE_ARGS(pWsscfgFsDot11RadioQosEntry,\
   pWsscfgIsSetFsDot11RadioQosEntry,\
   pau1FsDot11TaggingPolicy,\
   pau1FsDot11TaggingPolicyLen,\
   pau1IfIndex)\
  {\
  if (pau1FsDot11TaggingPolicy != NULL)\
  {\
   MEMCPY (pWsscfgFsDot11RadioQosEntry->MibObject.au1FsDot11TaggingPolicy, pau1FsDot11TaggingPolicy, *(INT4 *)pau1FsDot11TaggingPolicyLen);\
   pWsscfgFsDot11RadioQosEntry->MibObject.i4FsDot11TaggingPolicyLen = *(INT4 *)pau1FsDot11TaggingPolicyLen;\
   pWsscfgIsSetFsDot11RadioQosEntry->bFsDot11TaggingPolicy = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11RadioQosEntry->bFsDot11TaggingPolicy = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11RadioQosEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetFsDot11RadioQosEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11RadioQosEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsDot11QAPEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11QAPTABLE_ARGS(pWsscfgFsDot11QAPEntry,\
   pWsscfgIsSetFsDot11QAPEntry,\
   pau1FsDot11QueueDepth,\
   pau1FsDot11PriorityValue,\
   pau1FsDot11DscpValue,\
   pau1IfIndex,\
   pau1Dot11EDCATableIndex)\
  {\
  if (pau1FsDot11QueueDepth != NULL)\
  {\
   pWsscfgFsDot11QAPEntry->MibObject.i4FsDot11QueueDepth = *(INT4 *) (pau1FsDot11QueueDepth);\
   pWsscfgIsSetFsDot11QAPEntry->bFsDot11QueueDepth = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11QAPEntry->bFsDot11QueueDepth = OSIX_FALSE;\
  }\
  if (pau1FsDot11PriorityValue != NULL)\
  {\
   pWsscfgFsDot11QAPEntry->MibObject.i4FsDot11PriorityValue = *(INT4 *) (pau1FsDot11PriorityValue);\
   pWsscfgIsSetFsDot11QAPEntry->bFsDot11PriorityValue = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11QAPEntry->bFsDot11PriorityValue = OSIX_FALSE;\
  }\
  if (pau1FsDot11DscpValue != NULL)\
  {\
   pWsscfgFsDot11QAPEntry->MibObject.i4FsDot11DscpValue = *(INT4 *) (pau1FsDot11DscpValue);\
   pWsscfgIsSetFsDot11QAPEntry->bFsDot11DscpValue = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11QAPEntry->bFsDot11DscpValue = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11QAPEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetFsDot11QAPEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11QAPEntry->bIfIndex = OSIX_FALSE;\
  }\
  if (pau1Dot11EDCATableIndex != NULL)\
  {\
   pWsscfgFsDot11QAPEntry->MibObject.i4Dot11EDCATableIndex = *(INT4 *) (pau1Dot11EDCATableIndex);\
   pWsscfgIsSetFsDot11QAPEntry->bDot11EDCATableIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11QAPEntry->bDot11EDCATableIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsQAPProfileEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSQAPPROFILETABLE_ARGS(pWsscfgFsQAPProfileEntry,\
   pWsscfgIsSetFsQAPProfileEntry,\
   pau1FsQAPProfileName,\
   pau1FsQAPProfileNameLen,\
   pau1FsQAPProfileIndex,\
   pau1FsQAPProfileCWmin,\
   pau1FsQAPProfileCWmax,\
   pau1FsQAPProfileAIFSN,\
   pau1FsQAPProfileTXOPLimit,\
   pau1FsQAPProfileAdmissionControl,\
   pau1FsQAPProfilePriorityValue,\
   pau1FsQAPProfileDscpValue,\
   pau1FsQAPProfileRowStatus)\
  {\
  if (pau1FsQAPProfileName != NULL)\
  {\
   MEMCPY (pWsscfgFsQAPProfileEntry->MibObject.au1FsQAPProfileName, pau1FsQAPProfileName, *(INT4 *)pau1FsQAPProfileNameLen);\
   pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileNameLen = *(INT4 *)pau1FsQAPProfileNameLen;\
   pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileName = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileName = OSIX_FALSE;\
  }\
  if (pau1FsQAPProfileIndex != NULL)\
  {\
   pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileIndex = *(INT4 *) (pau1FsQAPProfileIndex);\
   pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileIndex = OSIX_FALSE;\
  }\
  if (pau1FsQAPProfileCWmin != NULL)\
  {\
   pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileCWmin = *(INT4 *) (pau1FsQAPProfileCWmin);\
   pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileCWmin = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileCWmin = OSIX_FALSE;\
  }\
  if (pau1FsQAPProfileCWmax != NULL)\
  {\
   pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileCWmax = *(INT4 *) (pau1FsQAPProfileCWmax);\
   pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileCWmax = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileCWmax = OSIX_FALSE;\
  }\
  if (pau1FsQAPProfileAIFSN != NULL)\
  {\
   pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileAIFSN = *(INT4 *) (pau1FsQAPProfileAIFSN);\
   pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileAIFSN = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileAIFSN = OSIX_FALSE;\
  }\
  if (pau1FsQAPProfileTXOPLimit != NULL)\
  {\
   pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileTXOPLimit = *(INT4 *) (pau1FsQAPProfileTXOPLimit);\
   pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileTXOPLimit = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileTXOPLimit = OSIX_FALSE;\
  }\
  if (pau1FsQAPProfileAdmissionControl != NULL)\
  {\
   pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileAdmissionControl = *(INT4 *) (pau1FsQAPProfileAdmissionControl);\
   pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileAdmissionControl = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileAdmissionControl = OSIX_FALSE;\
  }\
  if (pau1FsQAPProfilePriorityValue != NULL)\
  {\
   pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfilePriorityValue = *(INT4 *) (pau1FsQAPProfilePriorityValue);\
   pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfilePriorityValue = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfilePriorityValue = OSIX_FALSE;\
  }\
  if (pau1FsQAPProfileDscpValue != NULL)\
  {\
   pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileDscpValue = *(INT4 *) (pau1FsQAPProfileDscpValue);\
   pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileDscpValue = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileDscpValue = OSIX_FALSE;\
  }\
  if (pau1FsQAPProfileRowStatus != NULL)\
  {\
   pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileRowStatus = *(INT4 *) (pau1FsQAPProfileRowStatus);\
   pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsQAPProfileEntry->bFsQAPProfileRowStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsDot11CapabilityMappingEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11CAPABILITYMAPPINGTABLE_ARGS(pWsscfgFsDot11CapabilityMappingEntry,\
   pWsscfgIsSetFsDot11CapabilityMappingEntry,\
   pau1FsDot11CapabilityMappingProfileName,\
   pau1FsDot11CapabilityMappingProfileNameLen,\
   pau1FsDot11CapabilityMappingRowStatus,\
   pau1IfIndex)\
  {\
  if (pau1FsDot11CapabilityMappingProfileName != NULL)\
  {\
   MEMCPY (pWsscfgFsDot11CapabilityMappingEntry->MibObject.au1FsDot11CapabilityMappingProfileName, pau1FsDot11CapabilityMappingProfileName, *(INT4 *)pau1FsDot11CapabilityMappingProfileNameLen);\
   pWsscfgFsDot11CapabilityMappingEntry->MibObject.i4FsDot11CapabilityMappingProfileNameLen = *(INT4 *)pau1FsDot11CapabilityMappingProfileNameLen;\
   pWsscfgIsSetFsDot11CapabilityMappingEntry->bFsDot11CapabilityMappingProfileName = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityMappingEntry->bFsDot11CapabilityMappingProfileName = OSIX_FALSE;\
  }\
  if (pau1FsDot11CapabilityMappingRowStatus != NULL)\
  {\
   pWsscfgFsDot11CapabilityMappingEntry->MibObject.i4FsDot11CapabilityMappingRowStatus = *(INT4 *) (pau1FsDot11CapabilityMappingRowStatus);\
   pWsscfgIsSetFsDot11CapabilityMappingEntry->bFsDot11CapabilityMappingRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityMappingEntry->bFsDot11CapabilityMappingRowStatus = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11CapabilityMappingEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetFsDot11CapabilityMappingEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11CapabilityMappingEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsDot11AuthMappingEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11AUTHMAPPINGTABLE_ARGS(pWsscfgFsDot11AuthMappingEntry,\
   pWsscfgIsSetFsDot11AuthMappingEntry,\
   pau1FsDot11AuthMappingProfileName,\
   pau1FsDot11AuthMappingProfileNameLen,\
   pau1FsDot11AuthMappingRowStatus,\
   pau1IfIndex)\
  {\
  if (pau1FsDot11AuthMappingProfileName != NULL)\
  {\
   MEMCPY (pWsscfgFsDot11AuthMappingEntry->MibObject.au1FsDot11AuthMappingProfileName, pau1FsDot11AuthMappingProfileName, *(INT4 *)pau1FsDot11AuthMappingProfileNameLen);\
   pWsscfgFsDot11AuthMappingEntry->MibObject.i4FsDot11AuthMappingProfileNameLen = *(INT4 *)pau1FsDot11AuthMappingProfileNameLen;\
   pWsscfgIsSetFsDot11AuthMappingEntry->bFsDot11AuthMappingProfileName = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11AuthMappingEntry->bFsDot11AuthMappingProfileName = OSIX_FALSE;\
  }\
  if (pau1FsDot11AuthMappingRowStatus != NULL)\
  {\
   pWsscfgFsDot11AuthMappingEntry->MibObject.i4FsDot11AuthMappingRowStatus = *(INT4 *) (pau1FsDot11AuthMappingRowStatus);\
   pWsscfgIsSetFsDot11AuthMappingEntry->bFsDot11AuthMappingRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11AuthMappingEntry->bFsDot11AuthMappingRowStatus = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11AuthMappingEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetFsDot11AuthMappingEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11AuthMappingEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsDot11QosMappingEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11QOSMAPPINGTABLE_ARGS(pWsscfgFsDot11QosMappingEntry,\
   pWsscfgIsSetFsDot11QosMappingEntry,\
   pau1FsDot11QosMappingProfileName,\
   pau1FsDot11QosMappingProfileNameLen,\
   pau1FsDot11QosMappingRowStatus,\
   pau1IfIndex)\
  {\
  if (pau1FsDot11QosMappingProfileName != NULL)\
  {\
   MEMCPY (pWsscfgFsDot11QosMappingEntry->MibObject.au1FsDot11QosMappingProfileName, pau1FsDot11QosMappingProfileName, *(INT4 *)pau1FsDot11QosMappingProfileNameLen);\
   pWsscfgFsDot11QosMappingEntry->MibObject.i4FsDot11QosMappingProfileNameLen = *(INT4 *)pau1FsDot11QosMappingProfileNameLen;\
   pWsscfgIsSetFsDot11QosMappingEntry->bFsDot11QosMappingProfileName = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11QosMappingEntry->bFsDot11QosMappingProfileName = OSIX_FALSE;\
  }\
  if (pau1FsDot11QosMappingRowStatus != NULL)\
  {\
   pWsscfgFsDot11QosMappingEntry->MibObject.i4FsDot11QosMappingRowStatus = *(INT4 *) (pau1FsDot11QosMappingRowStatus);\
   pWsscfgIsSetFsDot11QosMappingEntry->bFsDot11QosMappingRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11QosMappingEntry->bFsDot11QosMappingRowStatus = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11QosMappingEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetFsDot11QosMappingEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11QosMappingEntry->bIfIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsDot11AntennasListEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11ANTENNASLISTTABLE_ARGS(pWsscfgFsDot11AntennasListEntry,\
   pWsscfgIsSetFsDot11AntennasListEntry,\
   pau1FsAntennaMode,\
   pau1FsAntennaSelection,\
   pau1IfIndex,\
   pau1Dot11AntennaListIndex)\
  {\
  if (pau1FsAntennaMode != NULL)\
  {\
   pWsscfgFsDot11AntennasListEntry->MibObject.i4FsAntennaMode = *(INT4 *) (pau1FsAntennaMode);\
   pWsscfgIsSetFsDot11AntennasListEntry->bFsAntennaMode = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11AntennasListEntry->bFsAntennaMode = OSIX_FALSE;\
  }\
  if (pau1FsAntennaSelection != NULL)\
  {\
   pWsscfgFsDot11AntennasListEntry->MibObject.i4FsAntennaSelection = *(INT4 *) (pau1FsAntennaSelection);\
   pWsscfgIsSetFsDot11AntennasListEntry->bFsAntennaSelection = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11AntennasListEntry->bFsAntennaSelection = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11AntennasListEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetFsDot11AntennasListEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11AntennasListEntry->bIfIndex = OSIX_FALSE;\
  }\
  if (pau1Dot11AntennaListIndex != NULL)\
  {\
   pWsscfgFsDot11AntennasListEntry->MibObject.i4Dot11AntennaListIndex = *(INT4 *) (pau1Dot11AntennaListIndex);\
   pWsscfgIsSetFsDot11AntennasListEntry->bDot11AntennaListIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11AntennasListEntry->bDot11AntennaListIndex = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsDot11WlanEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11WLANTABLE_ARGS(pWsscfgFsDot11WlanEntry,\
   pWsscfgIsSetFsDot11WlanEntry,\
   pau1FsDot11WlanProfileIfIndex,\
   pau1FsDot11WlanRowStatus,\
   pau1CapwapDot11WlanProfileId)\
  {\
  if (pau1FsDot11WlanProfileIfIndex != NULL)\
  {\
   pWsscfgFsDot11WlanEntry->MibObject.i4FsDot11WlanProfileIfIndex = *(INT4 *) (pau1FsDot11WlanProfileIfIndex);\
   pWsscfgIsSetFsDot11WlanEntry->bFsDot11WlanProfileIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanEntry->bFsDot11WlanProfileIfIndex = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanRowStatus != NULL)\
  {\
   pWsscfgFsDot11WlanEntry->MibObject.i4FsDot11WlanRowStatus = *(INT4 *) (pau1FsDot11WlanRowStatus);\
   pWsscfgIsSetFsDot11WlanEntry->bFsDot11WlanRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanEntry->bFsDot11WlanRowStatus = OSIX_FALSE;\
  }\
  if (pau1CapwapDot11WlanProfileId != NULL)\
  {\
   pWsscfgFsDot11WlanEntry->MibObject.u4CapwapDot11WlanProfileId = *(UINT4 *) (pau1CapwapDot11WlanProfileId);\
   pWsscfgIsSetFsDot11WlanEntry->bCapwapDot11WlanProfileId = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanEntry->bCapwapDot11WlanProfileId = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsDot11WlanBindEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11WLANBINDTABLE_ARGS(pWsscfgFsDot11WlanBindEntry,\
   pWsscfgIsSetFsDot11WlanBindEntry,\
   pau1FsDot11WlanBindWlanId,\
   pau1FsDot11WlanBindBssIfIndex,\
   pau1FsDot11WlanBindRowStatus,\
   pau1IfIndex,\
   pau1CapwapDot11WlanProfileId)\
  {\
  if (pau1FsDot11WlanBindWlanId != NULL)\
  {\
   pWsscfgFsDot11WlanBindEntry->MibObject.u4FsDot11WlanBindWlanId = *(UINT4 *) (pau1FsDot11WlanBindWlanId);\
   pWsscfgIsSetFsDot11WlanBindEntry->bFsDot11WlanBindWlanId = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanBindEntry->bFsDot11WlanBindWlanId = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanBindBssIfIndex != NULL)\
  {\
   pWsscfgFsDot11WlanBindEntry->MibObject.i4FsDot11WlanBindBssIfIndex = *(INT4 *) (pau1FsDot11WlanBindBssIfIndex);\
   pWsscfgIsSetFsDot11WlanBindEntry->bFsDot11WlanBindBssIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanBindEntry->bFsDot11WlanBindBssIfIndex = OSIX_FALSE;\
  }\
  if (pau1FsDot11WlanBindRowStatus != NULL)\
  {\
   pWsscfgFsDot11WlanBindEntry->MibObject.i4FsDot11WlanBindRowStatus = *(INT4 *) (pau1FsDot11WlanBindRowStatus);\
   pWsscfgIsSetFsDot11WlanBindEntry->bFsDot11WlanBindRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanBindEntry->bFsDot11WlanBindRowStatus = OSIX_FALSE;\
  }\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11WlanBindEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
   pWsscfgIsSetFsDot11WlanBindEntry->bIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanBindEntry->bIfIndex = OSIX_FALSE;\
  }\
  if (pau1CapwapDot11WlanProfileId != NULL)\
  {\
   pWsscfgFsDot11WlanBindEntry->MibObject.u4CapwapDot11WlanProfileId = *(UINT4 *) (pau1CapwapDot11WlanProfileId);\
   pWsscfgIsSetFsDot11WlanBindEntry->bCapwapDot11WlanProfileId = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsDot11WlanBindEntry->bCapwapDot11WlanProfileId = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsWtpImageUpgradeEntry 
 *  using the input given in def file */

#define  WSSCFG_FILL_FSWTPIMAGEUPGRADETABLE_ARGS(pWsscfgFsWtpImageUpgradeEntry,\
                    pWsscfgIsSetFsWtpImageUpgradeEntry,\
                    pau1FsWtpImageVersion,\
                    pau1FsWtpImageVersionLen,\
                    pau1FsWtpUpgradeDev,\
                    pau1FsWtpName,\
                    pau1FsWtpNameLen,\
                    pau1FsWtpImageName,\
                    pau1FsWtpImageNameLen,\
                    pau1FsWtpAddressType,\
                    pau1FsWtpServerIP,\
                    pau1FsWtpServerIPLen,\
                    pau1FsWtpRowStatus,\
                    pau1CapwapBaseWtpProfileWtpModelNumber,\
                    pau1CapwapBaseWtpProfileWtpModelNumberLen)\
     {\
                if (pau1FsWtpImageVersion != NULL)\
                {\
                                MEMCPY (pWsscfgFsWtpImageUpgradeEntry->MibObject.au1FsWtpImageVersion, pau1FsWtpImageVersion, *(INT4 *)pau1FsWtpImageVersionLen);\
                                pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpImageVersionLen = *(INT4 *)pau1FsWtpImageVersionLen;\
                                pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpImageVersion = OSIX_TRUE;\
                            }\
                else\
                {\
                                pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpImageVersion = OSIX_FALSE;\
                            }\
                if (pau1FsWtpUpgradeDev != NULL)\
                {\
                                pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpUpgradeDev = *(INT4 *) (pau1FsWtpUpgradeDev);\
                                pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpUpgradeDev = OSIX_TRUE;\
                            }\
                else\
                {\
                                pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpUpgradeDev = OSIX_FALSE;\
                            }\
                if (pau1FsWtpName != NULL)\
                {\
                                MEMCPY (pWsscfgFsWtpImageUpgradeEntry->MibObject.au1FsWtpName, pau1FsWtpName, *(INT4 *)pau1FsWtpNameLen);\
                                pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpNameLen = *(INT4 *)pau1FsWtpNameLen;\
                                pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpName = OSIX_TRUE;\
                            }\
                else\
                {\
                                pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpName = OSIX_FALSE;\
                            }\
                if (pau1FsWtpImageName != NULL)\
                {\
                                MEMCPY (pWsscfgFsWtpImageUpgradeEntry->MibObject.au1FsWtpImageName, pau1FsWtpImageName, *(INT4 *)pau1FsWtpImageNameLen);\
                                pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpImageNameLen = *(INT4 *)pau1FsWtpImageNameLen;\
                                pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpImageName = OSIX_TRUE;\
                            }\
                else\
                {\
                                pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpImageName = OSIX_FALSE;\
                            }\
                if (pau1FsWtpAddressType != NULL)\
                {\
                                pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpAddressType = *(INT4 *) (pau1FsWtpAddressType);\
                                pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpAddressType = OSIX_TRUE;\
                            }\
                else\
                {\
                                pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpAddressType = OSIX_FALSE;\
                            }\
                if (pau1FsWtpServerIP != NULL)\
                {\
                                MEMCPY (pWsscfgFsWtpImageUpgradeEntry->MibObject.au1FsWtpServerIP, pau1FsWtpServerIP, *(INT4 *)pau1FsWtpServerIPLen);\
                                pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpServerIPLen = *(INT4 *)pau1FsWtpServerIPLen;\
                                pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpServerIP = OSIX_TRUE;\
                            }\
                else\
                {\
                                pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpServerIP = OSIX_FALSE;\
                            }\
                if (pau1FsWtpRowStatus != NULL)\
                {\
                                pWsscfgFsWtpImageUpgradeEntry->MibObject.i4FsWtpRowStatus = *(INT4 *) (pau1FsWtpRowStatus);\
                                pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpRowStatus = OSIX_TRUE;\
                            }\
                else\
                {\
                                pWsscfgIsSetFsWtpImageUpgradeEntry->bFsWtpRowStatus = OSIX_FALSE;\
                            }\
                if (pau1CapwapBaseWtpProfileWtpModelNumber != NULL)\
                {\
                                MEMCPY (pWsscfgFsWtpImageUpgradeEntry->MibObject.au1CapwapBaseWtpProfileWtpModelNumber, pau1CapwapBaseWtpProfileWtpModelNumber, *(INT4 *)pau1CapwapBaseWtpProfileWtpModelNumberLen);\
                                pWsscfgFsWtpImageUpgradeEntry->MibObject.i4CapwapBaseWtpProfileWtpModelNumberLen = *(INT4 *)pau1CapwapBaseWtpProfileWtpModelNumberLen;\
                                pWsscfgIsSetFsWtpImageUpgradeEntry->bCapwapBaseWtpProfileWtpModelNumber = OSIX_TRUE;\
                            }\
                else\
                {\
                                pWsscfgIsSetFsWtpImageUpgradeEntry->bCapwapBaseWtpProfileWtpModelNumber = OSIX_FALSE;\
                            }\
                }
/* Macro used to fill the index values in  structure for FsDot11StationConfigEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11STATIONCONFIGTABLE_INDEX(pWsscfgFsDot11StationConfigEntry,\
   pau1IfIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11StationConfigEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for FsDot11CapabilityProfileEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11CAPABILITYPROFILETABLE_INDEX(pWsscfgFsDot11CapabilityProfileEntry,\
   pau1FsDot11CapabilityProfileName)\
  {\
  if (pau1FsDot11CapabilityProfileName != NULL)\
  {\
   MEMCPY (pWsscfgFsDot11CapabilityProfileEntry->MibObject.au1FsDot11CapabilityProfileName, pau1FsDot11CapabilityProfileName, *(INT4 *)pau1FsDot11CapabilityProfileNameLen);\
   pWsscfgFsDot11CapabilityProfileEntry->MibObject.i4FsDot11CapabilityProfileNameLen = *(INT4 *)pau1FsDot11CapabilityProfileNameLen;\
  }\
 }
/* Macro used to fill the index values in  structure for FsDot11AuthenticationProfileEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11AUTHENTICATIONPROFILETABLE_INDEX(pWsscfgFsDot11AuthenticationProfileEntry,\
   pau1FsDot11AuthenticationProfileName)\
  {\
  if (pau1FsDot11AuthenticationProfileName != NULL)\
  {\
   MEMCPY (pWsscfgFsDot11AuthenticationProfileEntry->MibObject.au1FsDot11AuthenticationProfileName, pau1FsDot11AuthenticationProfileName, *(INT4 *)pau1FsDot11AuthenticationProfileNameLen);\
   pWsscfgFsDot11AuthenticationProfileEntry->MibObject.i4FsDot11AuthenticationProfileNameLen = *(INT4 *)pau1FsDot11AuthenticationProfileNameLen;\
  }\
 }
/* Macro used to fill the index values in  structure for FsSecurityWebAuthGuestInfoEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSSECURITYWEBAUTHGUESTINFOTABLE_INDEX(pWsscfgFsSecurityWebAuthGuestInfoEntry,\
   pau1FsSecurityWebAuthUName)\
  {\
  if (pau1FsSecurityWebAuthUName != NULL)\
  {\
   MEMCPY (pWsscfgFsSecurityWebAuthGuestInfoEntry->MibObject.au1FsSecurityWebAuthUName, pau1FsSecurityWebAuthUName, *(INT4 *)pau1FsSecurityWebAuthUNameLen);\
   pWsscfgFsSecurityWebAuthGuestInfoEntry->MibObject.i4FsSecurityWebAuthUNameLen = *(INT4 *)pau1FsSecurityWebAuthUNameLen;\
  }\
 }
/* Macro used to fill the index values in  structure for FsStationQosParamsEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSSTATIONQOSPARAMSTABLE_INDEX(pWsscfgFsStationQosParamsEntry,\
   pau1IfIndex,\
   pau1FsStaMacAddress)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsStationQosParamsEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
  if (pau1FsStaMacAddress != NULL)\
  {\
   StrToMac (pau1FsStaMacAddress, pWsscfgFsStationQosParamsEntry->MibObject.FsStaMacAddress);\
  }\
 }
/* Macro used to fill the index values in  structure for FsVlanIsolationEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSVLANISOLATIONTABLE_INDEX(pWsscfgFsVlanIsolationEntry,\
   pau1IfIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsVlanIsolationEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for FsDot11RadioConfigEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11RADIOCONFIGTABLE_INDEX(pWsscfgFsDot11RadioConfigEntry,\
   pau1IfIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11RadioConfigEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for FsDot11QosProfileEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11QOSPROFILETABLE_INDEX(pWsscfgFsDot11QosProfileEntry,\
   pau1FsDot11QosProfileName)\
  {\
  if (pau1FsDot11QosProfileName != NULL)\
  {\
   MEMCPY (pWsscfgFsDot11QosProfileEntry->MibObject.au1FsDot11QosProfileName, pau1FsDot11QosProfileName, *(INT4 *)pau1FsDot11QosProfileNameLen);\
   pWsscfgFsDot11QosProfileEntry->MibObject.i4FsDot11QosProfileNameLen = *(INT4 *)pau1FsDot11QosProfileNameLen;\
  }\
 }
/* Macro used to fill the index values in  structure for FsDot11WlanCapabilityProfileEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11WLANCAPABILITYPROFILETABLE_INDEX(pWsscfgFsDot11WlanCapabilityProfileEntry,\
   pau1IfIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11WlanCapabilityProfileEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for FsDot11WlanAuthenticationProfileEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11WLANAUTHENTICATIONPROFILETABLE_INDEX(pWsscfgFsDot11WlanAuthenticationProfileEntry,\
   pau1IfIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11WlanAuthenticationProfileEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for FsDot11WlanQosProfileEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11WLANQOSPROFILETABLE_INDEX(pWsscfgFsDot11WlanQosProfileEntry,\
   pau1IfIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11WlanQosProfileEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for FsDot11RadioQosEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11RADIOQOSTABLE_INDEX(pWsscfgFsDot11RadioQosEntry,\
   pau1IfIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11RadioQosEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for FsDot11QAPEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11QAPTABLE_INDEX(pWsscfgFsDot11QAPEntry,\
   pau1IfIndex,\
   pau1Dot11EDCATableIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11QAPEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
  if (pau1Dot11EDCATableIndex != NULL)\
  {\
   pWsscfgFsDot11QAPEntry->MibObject.i4Dot11EDCATableIndex = *(INT4 *) (pau1Dot11EDCATableIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for FsQAPProfileEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSQAPPROFILETABLE_INDEX(pWsscfgFsQAPProfileEntry,\
   pau1FsQAPProfileName,\
   pau1FsQAPProfileIndex)\
  {\
  if (pau1FsQAPProfileName != NULL)\
  {\
   MEMCPY (pWsscfgFsQAPProfileEntry->MibObject.au1FsQAPProfileName, pau1FsQAPProfileName, *(INT4 *)pau1FsQAPProfileNameLen);\
   pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileNameLen = *(INT4 *)pau1FsQAPProfileNameLen;\
  }\
  if (pau1FsQAPProfileIndex != NULL)\
  {\
   pWsscfgFsQAPProfileEntry->MibObject.i4FsQAPProfileIndex = *(INT4 *) (pau1FsQAPProfileIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for FsDot11CapabilityMappingEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11CAPABILITYMAPPINGTABLE_INDEX(pWsscfgFsDot11CapabilityMappingEntry,\
   pau1IfIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11CapabilityMappingEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for FsDot11AuthMappingEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11AUTHMAPPINGTABLE_INDEX(pWsscfgFsDot11AuthMappingEntry,\
   pau1IfIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11AuthMappingEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for FsDot11QosMappingEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11QOSMAPPINGTABLE_INDEX(pWsscfgFsDot11QosMappingEntry,\
   pau1IfIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11QosMappingEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for FsDot11AntennasListEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11ANTENNASLISTTABLE_INDEX(pWsscfgFsDot11AntennasListEntry,\
   pau1IfIndex,\
   pau1Dot11AntennaListIndex)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11AntennasListEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
  if (pau1Dot11AntennaListIndex != NULL)\
  {\
   pWsscfgFsDot11AntennasListEntry->MibObject.i4Dot11AntennaListIndex = *(INT4 *) (pau1Dot11AntennaListIndex);\
  }\
 }
/* Macro used to fill the index values in  structure for FsDot11WlanEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11WLANTABLE_INDEX(pWsscfgFsDot11WlanEntry,\
   pau1CapwapDot11WlanProfileId)\
  {\
  if (pau1CapwapDot11WlanProfileId != NULL)\
  {\
   pWsscfgFsDot11WlanEntry->MibObject.u4CapwapDot11WlanProfileId = *(UINT4 *) (pau1CapwapDot11WlanProfileId);\
  }\
 }
/* Macro used to fill the index values in  structure for FsDot11WlanBindEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSDOT11WLANBINDTABLE_INDEX(pWsscfgFsDot11WlanBindEntry,\
   pau1IfIndex,\
   pau1CapwapDot11WlanProfileId)\
  {\
  if (pau1IfIndex != NULL)\
  {\
   pWsscfgFsDot11WlanBindEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
  }\
  if (pau1CapwapDot11WlanProfileId != NULL)\
  {\
   pWsscfgFsDot11WlanBindEntry->MibObject.u4CapwapDot11WlanProfileId = *(UINT4 *) (pau1CapwapDot11WlanProfileId);\
  }\
 }


/* Macro used to fill the index values in  structure for FsDot11nConfigEntry 
 *  using the input given in def file */

#define  WSSCFG_FILL_FSDOT11NCONFIGTABLE_INDEX(pWsscfgFsDot11nConfigEntry,\
                    pau1IfIndex)\
     {\
                if (pau1IfIndex != NULL)\
                {\
                                pWsscfgFsDot11nConfigEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
                            }\
            }
/* Macro used to fill the index values in  structure for
 * FsDot11nMCSDataRateEntry 
 *  using the input given in def file */

#define  WSSCFG_FILL_FSDOT11NMCSDATARATETABLE_INDEX(pWsscfgFsDot11nMCSDataRateEntry,\
                    pau1IfIndex,\
                    pau1FsDot11nMCSDataRateIndex)\
     {\
                if (pau1IfIndex != NULL)\
                {\
                                pWsscfgFsDot11nMCSDataRateEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
                            }\
                if (pau1FsDot11nMCSDataRateIndex != NULL)\
                {\
                                pWsscfgFsDot11nMCSDataRateEntry->MibObject.i4FsDot11nMCSDataRateIndex = *(INT4 *) (pau1FsDot11nMCSDataRateIndex);\
                            }\
            }

/* Macro used to fill the index values in  structure for FsWtpImageUpgradeEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSWTPIMAGEUPGRADETABLE_INDEX(pWsscfgFsWtpImageUpgradeEntry,\
   pau1CapwapBaseWtpProfileWtpModelNumber)\
  {\
  if (pau1CapwapBaseWtpProfileWtpModelNumber != NULL)\
  {\
   MEMCPY (pWsscfgFsWtpImageUpgradeEntry->MibObject.au1CapwapBaseWtpProfileWtpModelNumber, pau1CapwapBaseWtpProfileWtpModelNumber, *(INT4 *)pau1CapwapBaseWtpProfileWtpModelNumberLen);\
   pWsscfgFsWtpImageUpgradeEntry->MibObject.i4CapwapBaseWtpProfileWtpModelNumberLen = *(INT4 *)pau1CapwapBaseWtpProfileWtpModelNumberLen;\
  }\
 }
/* Macro used to convert the input 
  to required datatype for FsDot11aNetworkEnable*/

#define  WSSCFG_FILL_FSDOT11ANETWORKENABLE(i4FsDot11aNetworkEnable,\
   pau1FsDot11aNetworkEnable)\
  {\
  if (pau1FsDot11aNetworkEnable != NULL)\
  {\
   i4FsDot11aNetworkEnable = *(INT4 *) (pau1FsDot11aNetworkEnable);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsDot11bNetworkEnable*/

#define  WSSCFG_FILL_FSDOT11BNETWORKENABLE(i4FsDot11bNetworkEnable,\
   pau1FsDot11bNetworkEnable)\
  {\
  if (pau1FsDot11bNetworkEnable != NULL)\
  {\
   i4FsDot11bNetworkEnable = *(INT4 *) (pau1FsDot11bNetworkEnable);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsDot11gSupport*/

#define  WSSCFG_FILL_FSDOT11GSUPPORT(i4FsDot11gSupport,\
   pau1FsDot11gSupport)\
  {\
  if (pau1FsDot11gSupport != NULL)\
  {\
   i4FsDot11gSupport = *(INT4 *) (pau1FsDot11gSupport);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsDot11anSupport*/

#define  WSSCFG_FILL_FSDOT11ANSUPPORT(i4FsDot11anSupport,\
   pau1FsDot11anSupport)\
  {\
  if (pau1FsDot11anSupport != NULL)\
  {\
   i4FsDot11anSupport = *(INT4 *) (pau1FsDot11anSupport);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsDot11bnSupport*/

#define  WSSCFG_FILL_FSDOT11BNSUPPORT(i4FsDot11bnSupport,\
   pau1FsDot11bnSupport)\
  {\
  if (pau1FsDot11bnSupport != NULL)\
  {\
   i4FsDot11bnSupport = *(INT4 *) (pau1FsDot11bnSupport);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsDot11ManagmentSSID*/

#define  WSSCFG_FILL_FSDOT11MANAGMENTSSID(u4FsDot11ManagmentSSID,\
   pau1FsDot11ManagmentSSID)\
  {\
  if (pau1FsDot11ManagmentSSID != NULL)\
  {\
   u4FsDot11ManagmentSSID = *(UINT4 *) (pau1FsDot11ManagmentSSID);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsDot11CountryString*/


#define  WSSCFG_FILL_FSDOT11COUNTRYSTRING(pFsDot11CountryString,\
   pau1FsDot11CountryString)\
  {\
  if (pau1FsDot11CountryString != NULL && STRLEN(pau1FsDot11CountryString) < 3)\
  {\
   MEMCPY(pFsDot11CountryString,pau1FsDot11CountryString,STRLEN(pau1FsDot11CountryString));\
  }\
  }


/* Macro used to fill the CLI structure for FsDot11nConfigEntry 
 *  using the input given in def file */

#define  WSSCFG_FILL_FSDOT11NCONFIGTABLE_ARGS(pWsscfgFsDot11nConfigEntry,\
                    pWsscfgIsSetFsDot11nConfigEntry,\
                    pau1FsDot11nConfigShortGIfor20MHz,\
                    pau1FsDot11nConfigShortGIfor40MHz,\
                    pau1FsDot11nConfigChannelWidth,\
                    pau1IfIndex)\
     {\
                if (pau1FsDot11nConfigShortGIfor20MHz != NULL)\
                {\
                                pWsscfgFsDot11nConfigEntry->MibObject.i4FsDot11nConfigShortGIfor20MHz = *(INT4 *) (pau1FsDot11nConfigShortGIfor20MHz);\
                                pWsscfgIsSetFsDot11nConfigEntry->bFsDot11nConfigShortGIfor20MHz = OSIX_TRUE;\
                            }\
                else\
                {\
                                pWsscfgIsSetFsDot11nConfigEntry->bFsDot11nConfigShortGIfor20MHz = OSIX_FALSE;\
                            }\
                if (pau1FsDot11nConfigShortGIfor40MHz != NULL)\
                {\
                                pWsscfgFsDot11nConfigEntry->MibObject.i4FsDot11nConfigShortGIfor40MHz = *(INT4 *) (pau1FsDot11nConfigShortGIfor40MHz);\
                                pWsscfgIsSetFsDot11nConfigEntry->bFsDot11nConfigShortGIfor40MHz = OSIX_TRUE;\
                            }\
                else\
                {\
                                pWsscfgIsSetFsDot11nConfigEntry->bFsDot11nConfigShortGIfor40MHz = OSIX_FALSE;\
                            }\
                if (pau1FsDot11nConfigChannelWidth != NULL)\
                {\
                                pWsscfgFsDot11nConfigEntry->MibObject.i4FsDot11nConfigChannelWidth = *(INT4 *) (pau1FsDot11nConfigChannelWidth);\
                                pWsscfgIsSetFsDot11nConfigEntry->bFsDot11nConfigChannelWidth = OSIX_TRUE;\
                            }\
                else\
                {\
                                pWsscfgIsSetFsDot11nConfigEntry->bFsDot11nConfigChannelWidth = OSIX_FALSE;\
                            }\
                if (pau1IfIndex != NULL)\
                {\
                                pWsscfgFsDot11nConfigEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
                                pWsscfgIsSetFsDot11nConfigEntry->bIfIndex = OSIX_TRUE;\
                            }\
                else\
                {\
                                pWsscfgIsSetFsDot11nConfigEntry->bIfIndex = OSIX_FALSE;\
                            }\
                }
/* Macro used to fill the CLI structure for FsDot11nMCSDataRateEntry 
 *  using the input given in def file */

#define  WSSCFG_FILL_FSDOT11NMCSDATARATETABLE_ARGS(pWsscfgFsDot11nMCSDataRateEntry,\
                    pWsscfgIsSetFsDot11nMCSDataRateEntry,\
                    pau1FsDot11nMCSDataRateIndex,\
                    pau1FsDot11nMCSDataRate,\
                    pau1IfIndex)\
     {\
                if (pau1FsDot11nMCSDataRateIndex != NULL)\
                {\
                                pWsscfgFsDot11nMCSDataRateEntry->MibObject.i4FsDot11nMCSDataRateIndex = *(INT4 *) (pau1FsDot11nMCSDataRateIndex);\
                                pWsscfgIsSetFsDot11nMCSDataRateEntry->bFsDot11nMCSDataRateIndex = OSIX_TRUE;\
                            }\
                else\
                {\
                                pWsscfgIsSetFsDot11nMCSDataRateEntry->bFsDot11nMCSDataRateIndex = OSIX_FALSE;\
                            }\
                if (pau1FsDot11nMCSDataRate != NULL)\
                {\
                                pWsscfgFsDot11nMCSDataRateEntry->MibObject.i4FsDot11nMCSDataRate = *(INT4 *) (pau1FsDot11nMCSDataRate);\
                                pWsscfgIsSetFsDot11nMCSDataRateEntry->bFsDot11nMCSDataRate = OSIX_TRUE;\
                            }\
                else\
                {\
                                pWsscfgIsSetFsDot11nMCSDataRateEntry->bFsDot11nMCSDataRate = OSIX_FALSE;\
                            }\
                if (pau1IfIndex != NULL)\
                {\
                                pWsscfgFsDot11nMCSDataRateEntry->MibObject.i4IfIndex = *(INT4 *) (pau1IfIndex);\
                                pWsscfgIsSetFsDot11nMCSDataRateEntry->bIfIndex = OSIX_TRUE;\
                            }\
                else\
                {\
                                pWsscfgIsSetFsDot11nMCSDataRateEntry->bIfIndex = OSIX_FALSE;\
                            }\
                }



/* Macro used to convert the input 
  to required datatype for FsSecurityWebAuthType*/
#define  WSSCFG_FILL_FSSECURITYWEBAUTHTYPE(i4FsSecurityWebAuthType,\
   pau1FsSecurityWebAuthType)\
  {\
  if (pau1FsSecurityWebAuthType != NULL)\
  {\
   i4FsSecurityWebAuthType = *(INT4 *) (pau1FsSecurityWebAuthType);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsSecurityWebAuthUrl*/

#define  WSSCFG_FILL_FSSECURITYWEBAUTHURL(pFsSecurityWebAuthUrl,\
   pau1FsSecurityWebAuthUrl)\
  {\
  if (pau1FsSecurityWebAuthUrl != NULL)\
  {\
   STRNCPY (pFsSecurityWebAuthUrl->pu1_OctetList, pau1FsSecurityWebAuthUrl, pFsSecurityWebAuthUrl->i4_Length);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsSecurityWebAuthRedirectUrl*/

#define  WSSCFG_FILL_FSSECURITYWEBAUTHREDIRECTURL(pFsSecurityWebAuthRedirectUrl,\
   pau1FsSecurityWebAuthRedirectUrl)\
  {\
  if (pau1FsSecurityWebAuthRedirectUrl != NULL)\
  {\
   STRNCPY (pFsSecurityWebAuthRedirectUrl, pau1FsSecurityWebAuthRedirectUrl, MEM_MAX_BYTES(STRLEN(pau1FsSecurityWebAuthRedirectUrl), sizeof(pFsSecurityWebAuthRedirectUrl)));\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsSecurityWebAddr*/

#define  WSSCFG_FILL_FSSECURITYWEBADDR(i4FsSecurityWebAddr,\
   pau1FsSecurityWebAddr)\
  {\
  if (pau1FsSecurityWebAddr != NULL)\
  {\
   i4FsSecurityWebAddr = *(INT4 *) (pau1FsSecurityWebAddr);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsSecurityWebAuthWebTitle*/

#define  WSSCFG_FILL_FSSECURITYWEBAUTHWEBTITLE(pFsSecurityWebAuthWebTitle,\
   pau1FsSecurityWebAuthWebTitle)\
  {\
  if (pau1FsSecurityWebAuthWebTitle != NULL)\
  {\
   STRCPY (pFsSecurityWebAuthWebTitle, pau1FsSecurityWebAuthWebTitle);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsSecurityWebAuthWebMessage*/

#define  WSSCFG_FILL_FSSECURITYWEBAUTHWEBMESSAGE(pFsSecurityWebAuthWebMessage,\
   pau1FsSecurityWebAuthWebMessage)\
  {\
  if (pau1FsSecurityWebAuthWebMessage != NULL)\
  {\
   STRCPY (pFsSecurityWebAuthWebMessage, pau1FsSecurityWebAuthWebMessage);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsSecurityWebAuthWebLogoFileName*/

#define  WSSCFG_FILL_FSSECURITYWEBAUTHWEBLOGOFILENAME(pFsSecurityWebAuthWebLogoFileName,\
   pau1FsSecurityWebAuthWebLogoFileName)\
  {\
  if (pau1FsSecurityWebAuthWebLogoFileName != NULL)\
  {\
   STRCPY (pFsSecurityWebAuthWebLogoFileName, pau1FsSecurityWebAuthWebLogoFileName);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsSecurityWebAuthWebSuccMessage*/

#define  WSSCFG_FILL_FSSECURITYWEBAUTHWEBSUCCMESSAGE(pFsSecurityWebAuthWebSuccMessage,\
   pau1FsSecurityWebAuthWebSuccMessage)\
  {\
  if (pau1FsSecurityWebAuthWebSuccMessage != NULL)\
  {\
   STRCPY (pFsSecurityWebAuthWebSuccMessage, pau1FsSecurityWebAuthWebSuccMessage);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsSecurityWebAuthWebFailMessage*/

#define  WSSCFG_FILL_FSSECURITYWEBAUTHWEBFAILMESSAGE(pFsSecurityWebAuthWebFailMessage,\
   pau1FsSecurityWebAuthWebFailMessage)\
  {\
  if (pau1FsSecurityWebAuthWebFailMessage != NULL)\
  {\
   STRCPY (pFsSecurityWebAuthWebFailMessage, pau1FsSecurityWebAuthWebFailMessage);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsSecurityWebAuthWebButtonText*/

#define  WSSCFG_FILL_FSSECURITYWEBAUTHWEBBUTTONTEXT(pFsSecurityWebAuthWebButtonText,\
   pau1FsSecurityWebAuthWebButtonText)\
  {\
  if (pau1FsSecurityWebAuthWebButtonText != NULL)\
  {\
   STRCPY (pFsSecurityWebAuthWebButtonText, pau1FsSecurityWebAuthWebButtonText);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsSecurityWebAuthWebLoadBalInfo*/

#define  WSSCFG_FILL_FSSECURITYWEBAUTHWEBLOADBALINFO(pFsSecurityWebAuthWebLoadBalInfo,\
   pau1FsSecurityWebAuthWebLoadBalInfo)\
  {\
  if (pau1FsSecurityWebAuthWebLoadBalInfo != NULL)\
  {\
   STRCPY (pFsSecurityWebAuthWebLoadBalInfo, pau1FsSecurityWebAuthWebLoadBalInfo);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsSecurityWebAuthDisplayLang*/

#define  WSSCFG_FILL_FSSECURITYWEBAUTHDISPLAYLANG(i4FsSecurityWebAuthDisplayLang,\
   pau1FsSecurityWebAuthDisplayLang)\
  {\
  if (pau1FsSecurityWebAuthDisplayLang != NULL)\
  {\
   i4FsSecurityWebAuthDisplayLang = *(INT4 *) (pau1FsSecurityWebAuthDisplayLang);\
  }\
  }


/* Macro used to convert the input 
  to required datatype for FsSecurityWebAuthColor*/

#define  WSSCFG_FILL_FSSECURITYWEBAUTHCOLOR(i4FsSecurityWebAuthColor,\
   pau1FsSecurityWebAuthColor)\
  {\
  if (pau1FsSecurityWebAuthColor != NULL)\
  {\
   i4FsSecurityWebAuthColor = *(INT4 *) (pau1FsSecurityWebAuthColor);\
  }\
  }


/* Macro used to fill the CLI structure for FsRrmConfigEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSRRMCONFIGTABLE_ARGS(pWsscfgFsRrmConfigEntry,\
   pWsscfgIsSetFsRrmConfigEntry,\
   pau1FsRrmRadioType,\
   pau1FsRrmDcaMode,\
   pau1FsRrmDcaChannelSelectionMode,\
   pau1FsRrmTpcMode,\
   pau1FsRrmTpcSelectionMode,\
   pau1FsRrmRowStatus)\
  {\
  if (pau1FsRrmRadioType != NULL)\
  {\
   pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmRadioType = *(INT4 *) (pau1FsRrmRadioType);\
   pWsscfgIsSetFsRrmConfigEntry->bFsRrmRadioType = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsRrmConfigEntry->bFsRrmRadioType = OSIX_FALSE;\
  }\
  if (pau1FsRrmDcaMode != NULL)\
  {\
   pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmDcaMode = *(INT4 *) (pau1FsRrmDcaMode);\
   pWsscfgIsSetFsRrmConfigEntry->bFsRrmDcaMode = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsRrmConfigEntry->bFsRrmDcaMode = OSIX_FALSE;\
  }\
  if (pau1FsRrmDcaChannelSelectionMode != NULL)\
  {\
   pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmDcaChannelSelectionMode = *(INT4 *) (pau1FsRrmDcaChannelSelectionMode);\
   pWsscfgIsSetFsRrmConfigEntry->bFsRrmDcaChannelSelectionMode = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsRrmConfigEntry->bFsRrmDcaChannelSelectionMode = OSIX_FALSE;\
  }\
  if (pau1FsRrmTpcMode != NULL)\
  {\
   pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmTpcMode = *(INT4 *) (pau1FsRrmTpcMode);\
   pWsscfgIsSetFsRrmConfigEntry->bFsRrmTpcMode = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsRrmConfigEntry->bFsRrmTpcMode = OSIX_FALSE;\
  }\
  if (pau1FsRrmTpcSelectionMode != NULL)\
  {\
   pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmTpcSelectionMode = *(INT4 *) (pau1FsRrmTpcSelectionMode);\
   pWsscfgIsSetFsRrmConfigEntry->bFsRrmTpcSelectionMode = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsRrmConfigEntry->bFsRrmTpcSelectionMode = OSIX_FALSE;\
  }\
  if (pau1FsRrmRowStatus != NULL)\
  {\
   pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmRowStatus = *(INT4 *) (pau1FsRrmRowStatus);\
   pWsscfgIsSetFsRrmConfigEntry->bFsRrmRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pWsscfgIsSetFsRrmConfigEntry->bFsRrmRowStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the index values in  structure for FsRrmConfigEntry 
 using the input given in def file */

#define  WSSCFG_FILL_FSRRMCONFIGTABLE_INDEX(pWsscfgFsRrmConfigEntry,\
   pau1FsRrmRadioType)\
  {\
  if (pau1FsRrmRadioType != NULL)\
  {\
   pWsscfgFsRrmConfigEntry->MibObject.i4FsRrmRadioType = *(INT4 *) (pau1FsRrmRadioType);\
  }\
 }
