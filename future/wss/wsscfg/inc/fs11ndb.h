/********************************************************************
 * * Copyright (C) 20132006 Aricent Inc . All Rights Reserved
 * * $Id: fs11ndb.h,v 1.2 2017/05/23 14:16:55 siva Exp $
 * * * Description: Proto types for Low Level  Routines
 * *********************************************************************/

#ifndef _FS11NDB_H
#define _FS11NDB_H

INT1 wsscfgGetDot11LDPCCodingOptionImplemented(INT4 , INT4 *);

INT1 wsscfgGetDot11LDPCCodingOptionActivated(INT4 , INT4 *);

INT1 wsscfgGetDot11MIMOPowerSave(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigMIMOPowerSave(INT4 , INT4 *);

INT1 wsscfgGetDot11HTGreenfieldOptionImplemented(INT4 , INT4 *);

INT1 wsscfgGetDot11HTGreenfieldOptionActivated(INT4 , INT4 *);

INT1 wsscfgGetDot11ShortGIOptionInTwentyImplemented(INT4 , INT4 *);

INT1 wsscfgGetDot11ShortGIOptionInTwentyActivated(INT4 , INT4 *);

INT1 wsscfgGetDot11ShortGIOptionInFortyImplemented(INT4 , INT4 *);

INT1 wsscfgGetDot11ShortGIOptionInFortyActivated(INT4 , INT4 *);

INT1 wsscfgGetDot11TxSTBCOptionImplemented(INT4 , INT4 *);

INT1 wsscfgGetDot11TxSTBCOptionActivated(INT4 , INT4 *);

INT1 wsscfgGetDot11RxSTBCOptionImplemented(INT4 , INT4 *);

INT1 wsscfgGetDot11RxSTBCOptionActivated(INT4 , INT4 *);

INT1 wsscfgGetDot11NDelayedBlockAckOptionImplemented(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigDelayedBlockAckOptionActivated(INT4 , INT4 *);

INT1 wsscfgGetDot11MaxAMSDULength(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigMaxAMSDULengthConfig(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigtHTDSSCCKModein40MHz(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigtHTDSSCCKModein40MHzConfig(INT4 , INT4 *);

INT1 wsscfgGetDot11FortyMHzIntolerant(INT4 , INT4 *);

INT1 wsscfgGetDot11LsigTxopProtectionOptionImplemented(INT4 , INT4 *);

INT1 wsscfgGetDot11LSIGTXOPFullProtectionActivated(INT4 , INT4 *);

INT1 wsscfgGetDot11MaxRxAMPDUFactor(INT4 , UINT4 *);

INT1 wsscfgGetFsDot11nConfigMaxRxAMPDUFactorConfig(INT4 , UINT4 *); 

INT1 wsscfgGetDot11MinimumMPDUStartSpacing(INT4 , UINT4 *);

INT1 wsscfgGetFsDot11nConfigMinimumMPDUStartSpacingConfig(INT4 , UINT4 *);

INT1 wsscfgGetDot11PCOOptionImplemented(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigPCOOptionActivated(INT4 , INT4 *);

INT1 wsscfgGetDot11TransitionTime(INT4 , UINT4 *);

INT1 wsscfgGetFsDot11nConfigTransitionTimeConfig(INT4 , UINT4 *);

INT1 wsscfgGetDot11MCSFeedbackOptionImplemented(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigMCSFeedbackOptionActivated(INT4, INT4 *);

INT1 wsscfgGetDot11HTControlFieldSupported(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigHTControlFieldSupported(INT4 , INT4 *);

INT1 wsscfgGetDot11RDResponderOptionImplemented(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigRDResponderOptionActivated(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigImplicitTransmitBeamformingRecvOptionImplemented(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated(INT4 , INT4 *);

INT1 wsscfgGetDot11ReceiveStaggerSoundingOptionImplemented(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigReceiveStaggerSoundingOptionActivated(INT4 , INT4 *);

INT1 wsscfgGetDot11TransmitStaggerSoundingOptionImplemented(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigTransmitStaggerSoundingOptionActivated(INT4 , INT4 *);

INT1 wsscfgGetDot11ReceiveNDPOptionImplemented(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigReceiveNDPOptionActivated(INT4 , INT4 *);

INT1 wsscfgGetDot11TransmitNDPOptionImplemented(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigTransmitNDPOptionActivated(INT4 , INT4 *);

INT1 wsscfgGetDot11ImplicitTransmitBeamformingOptionImplemented(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigImplicitTransmitBeamformingOptionActivated(INT4 , INT4 *);

INT1 wsscfgGetDot11CalibrationOptionImplemented(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigCalibrationOptionActivated(INT4 , INT4 *);

INT1 wsscfgGetDot11ExplicitCSITransmitBeamformingOptionImplemented(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated(INT4 , INT4 *);

INT1 wsscfgGetDot11ExplicitNonCompressedBeamformingMatrixOptionImplemented(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigExplicitCompressedBeamformingMatrixOptImplemented(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated(INT4 , INT4 *);

INT1 wsscfgGetDot11ExplicitTransmitBeamformingCSIFeedbackOptionImplemented(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated(INT4 , INT4 *);

INT1 wsscfgGetDot11ExplicitNonCompressedBeamformingFeedbackOptionImplemented(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated(INT4 , INT4 *);

INT1 wsscfgGetDot11ExplicitCompressedBeamformingFeedbackOptionImplemented(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigMinimalGroupingImplemented(INT4 , UINT4 *);

INT1 wsscfgGetFsDot11nConfigMinimalGroupingActivated(INT4 , UINT4 *);

INT1 wsscfgGetDot11NumberBeamFormingCSISupportAntenna(INT4 , UINT4 *);

INT1 wsscfgGetFsDot11nConfigNumberBeamFormingCSISupportAntenna(INT4 , UINT4 *);

INT1 wsscfgGetDot11NumberNonCompressedBeamformingMatrixSupportAntenna(INT4 , UINT4 *);

INT1 wsscfgGetFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna(INT4 , UINT4 *);

INT1 wsscfgGetDot11NumberCompressedBeamformingMatrixSupportAntenna(INT4 , UINT4 *);

INT1 wsscfgGetFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna(INT4 , UINT4 *);

INT1 wsscfgGetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaImplemented(INT4 , UINT4 *);

INT1 wsscfgGetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated(INT4 , UINT4 *);

INT1 wsscfgGetFsDot11nConfigChannelEstimationCapabilityImplemented(INT4 , UINT4 *);

INT1 wsscfgGetFsDot11nConfigChannelEstimationCapabilityActivated(INT4 , UINT4 *);

INT1 wsscfgGetDot11CurrentPrimaryChannel(INT4 , UINT4 *);

INT1 wsscfgGetFsDot11nConfigCurrentPrimaryChannel(INT4 , UINT4 *);

INT1 wsscfgGetDot11CurrentSecondaryChannel(INT4 , UINT4 *);

INT1 wsscfgGetFsDot11nConfigCurrentSecondaryChannel(INT4 , UINT4 *);

INT1 wsscfgGetFsDot11nConfigSTAChannelWidth(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigSTAChannelWidthConfig(INT4 , INT4 *);

INT1 wsscfgGetDot11RIFSMode(INT4 , INT4 *);

INT1 wsscfgGetDot11HTProtection(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigNonGreenfieldHTSTAsPresent(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigNonGreenfieldHTSTAsPresentConfig(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigOBSSNonHTSTAsPresent(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigOBSSNonHTSTAsPresentConfig(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigDualBeacon(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigDualBeaconConfig(INT4 , INT4 *);

INT1 wsscfgGetDot11DualCTSProtection(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigSTBCBeacon(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigSTBCBeaconConfig(INT4 , INT4 *);

INT1 wsscfgGetDot11PCOActivated(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigPCOPhase(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigPCOPhaseConfig(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigRxSTBCOptionActivated(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigRxSTBCOptionImplemented(INT4 , INT4 *);

INT1 wsscfgGetDot11TxMCSSetDefined(INT4 , INT4 *);

INT1 wsscfgGetDot11TxRxMCSSetNotEqual(INT4 , INT4 *);

INT1 wsscfgGetFsDot11nConfigTxRxMCSSetNotEqual(INT4 , INT4 *);

INT1 wsscfgSetDot11LDPCCodingOptionActivated(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigMIMOPowerSave(INT4 , INT4 );

INT1 wsscfgSetDot11HTGreenfieldOptionActivated(INT4 , INT4 );

INT1 wsscfgSetDot11ShortGIOptionInTwentyActivated(INT4 , INT4 );

INT1 wsscfgSetDot11ShortGIOptionInFortyActivated(INT4 , INT4 );

INT1 wsscfgSetDot11TxSTBCOptionActivated(INT4 , INT4 );

INT1 wsscfgSetDot11RxSTBCOptionActivated(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigDelayedBlockAckOptionActivated(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigMaxAMSDULengthConfig(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigtHTDSSCCKModein40MHzConfig(INT4 , INT4 );

INT1 wsscfgSetDot11FortyMHzIntolerant(INT4 , INT4 );

INT1 wsscfgSetDot11LSIGTXOPFullProtectionActivated(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigMaxRxAMPDUFactorConfig(INT4 , UINT4 );

INT1 wsscfgSetFsDot11nConfigMinimumMPDUStartSpacingConfig(INT4 , UINT4 );

INT1 wsscfgSetFsDot11nConfigPCOOptionActivated(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigTransitionTimeConfig(INT4 , UINT4 );

INT1 wsscfgSetFsDot11nConfigMCSFeedbackOptionActivated(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigHTControlFieldSupported(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigRDResponderOptionActivated(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigReceiveStaggerSoundingOptionActivated(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigTransmitStaggerSoundingOptionActivated(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigReceiveNDPOptionActivated(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigTransmitNDPOptionActivated(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigImplicitTransmitBeamformingOptionActivated(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigCalibrationOptionActivated(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigExplicitCSITransmitBeamformingOptionActivated(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigMinimalGroupingActivated(INT4 , UINT4 );

INT1 wsscfgSetFsDot11nConfigNumberBeamFormingCSISupportAntenna(INT4 , UINT4 );

INT1 wsscfgSetFsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna(INT4 , UINT4 );

INT1 wsscfgSetFsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna(INT4 , UINT4 );

INT1 wsscfgSetFsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated(INT4 , UINT4 );

INT1 wsscfgSetFsDot11nConfigChannelEstimationCapabilityActivated(INT4 , UINT4 );

INT1 wsscfgSetFsDot11nConfigCurrentPrimaryChannel(INT4 , UINT4 );

INT1 wsscfgSetFsDot11nConfigCurrentSecondaryChannel(INT4 , UINT4 );

INT1 wsscfgSetFsDot11nConfigSTAChannelWidthConfig(INT4 , INT4 );

INT1 wsscfgSetDot11RIFSMode(INT4 , INT4 );

INT1 wsscfgSetDot11HTProtection(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigNonGreenfieldHTSTAsPresentConfig(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigOBSSNonHTSTAsPresentConfig(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigDualBeaconConfig(INT4 , INT4 );

INT1 wsscfgSetDot11DualCTSProtection(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigSTBCBeaconConfig(INT4 , INT4 );

INT1 wsscfgSetDot11PCOActivated(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigPCOPhaseConfig(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigRxSTBCOptionActivated(INT4 , INT4 );

INT1 wsscfgSetDot11TxMCSSetDefined(INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigTxRxMCSSetNotEqual(INT4 , INT4 );

INT1 wsscfgTestv2Dot11LDPCCodingOptionActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigMIMOPowerSave(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2Dot11HTGreenfieldOptionActivated(UINT4 *, INT4 , INT4 ); 

INT1 wsscfgTestv2Dot11ShortGIOptionInTwentyActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2Dot11ShortGIOptionInFortyActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2Dot11TxSTBCOptionActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2Dot11RxSTBCOptionActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigDelayedBlockAckOptionActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigMaxAMSDULengthConfig(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigtHTDSSCCKModein40MHzConfig(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2Dot11FortyMHzIntolerant(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2Dot11LSIGTXOPFullProtectionActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigMaxRxAMPDUFactorConfig(UINT4 *, INT4 , UINT4 );

INT1 wsscfgTestv2FsDot11nConfigMinimumMPDUStartSpacingConfig(UINT4 *, INT4 , UINT4 );

INT1 wsscfgTestv2FsDot11nConfigPCOOptionActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigTransitionTimeConfig(UINT4 *, INT4 , UINT4 );

INT1 wsscfgTestv2FsDot11nConfigMCSFeedbackOptionActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigHTControlFieldSupported(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigRDResponderOptionActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigImplicitTransmitBeamformingRecvOptionActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigReceiveStaggerSoundingOptionActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigTransmitStaggerSoundingOptionActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigReceiveNDPOptionActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigTransmitNDPOptionActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigImplicitTransmitBeamformingOptionActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigCalibrationOptionActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigExplicitCSITransmitBeamformingOptionActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigExplicitNonCompressedBeamformingMatrixOptActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigExplicitCompressedBeamformingMatrixOptionActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigExplicitTransmitBeamformingCSIFeedbackOptActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigExplicitNonCompBeamformingFeedbackOptActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigExplicitCompressedBeamformingFeedbackOptActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigMinimalGroupingActivated(UINT4 *, INT4 , UINT4 );

INT1 wsscfgTestv2FsDot11nConfigNumberBeamFormingCSISupportAntenna(UINT4 *, INT4 , UINT4 );

INT1 wsscfgTestv2FsDot11nConfigNumberNonCompressedBeamformingMatrixSupportAntenna(UINT4 *, INT4 , UINT4 );

INT1 wsscfgTestv2FsDot11nConfigNumberCompressedBeamformingMatrixSupportAntenna(UINT4 *, INT4 , UINT4 );

INT1 wsscfgTestv2FsDot11nConfigMaxNumberBeamFormingCSISupportAntennaActivated(UINT4 *, INT4 , UINT4 );

INT1 wsscfgTestv2FsDot11nConfigChannelEstimationCapabilityActivated(UINT4 *, INT4 , UINT4 );

INT1 wsscfgTestv2FsDot11nConfigCurrentPrimaryChannel(UINT4 *, INT4 , UINT4 );

INT1 wsscfgTestv2FsDot11nConfigCurrentSecondaryChannel(UINT4 *, INT4 , UINT4 );

INT1 wsscfgTestv2FsDot11nConfigSTAChannelWidthConfig(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2Dot11RIFSMode(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2Dot11HTProtection(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigNonGreenfieldHTSTAsPresentConfig(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigOBSSNonHTSTAsPresentConfig(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigDualBeaconConfig(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2Dot11DualCTSProtection(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigSTBCBeaconConfig(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2Dot11PCOActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigPCOPhaseConfig(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigRxSTBCOptionActivated(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2Dot11TxMCSSetDefined(UINT4 *, INT4 , INT4 );

INT1 wsscfgTestv2FsDot11nConfigTxRxMCSSetNotEqual(UINT4 *, INT4 , INT4 );

INT1 wsscfgSetFsDot11nConfigAmpduStatus (INT4 i4IfIndex , UINT1 );

INT1 wsscfgSetFsDot11nConfigAmpduSubFrame (INT4 i4IfIndex , UINT1 );

INT1 wsscfgSetFsDot11nConfigAmpduLimit (INT4 i4IfIndex , UINT2 );

INT1 wsscfgSetFsDot11nConfigAmsduStatus (INT4 i4IfIndex , UINT1 );

INT1 wsscfgSetFsDot11nConfigAmsduLimit (INT4 i4IfIndex , UINT2 );

INT1 wsscfgGetFsDot11nConfigAmpduStatus (INT4 i4IfIndex , INT4 *);

INT1 wsscfgGetFsDot11nConfigAmpduSubFrame (INT4 i4IfIndex , UINT4 *);

INT1 wsscfgGetFsDot11nConfigAmpduLimit (INT4 i4IfIndex , UINT4 *);

INT1 wsscfgGetFsDot11nConfigAmsduStatus (INT4 i4IfIndex , INT4 *);

INT1 wsscfgGetFsDot11nConfigAmsduLimit (INT4 i4IfIndex , UINT4 *);

INT1 wsscfgTestFsDot11nConfigAmpduStatus (UINT4 * , INT4  , INT4 );

INT1 wsscfgTestFsDot11nConfigAmpduSubFrame (UINT4 * , INT4  , INT4 );

INT1 wsscfgTestFsDot11nConfigAmpduLimit (UINT4 * , INT4 , INT4 );

INT1 wsscfgTestFsDot11nConfigAmsduStatus (UINT4 * , INT4  , INT4 );

INT1 wsscfgTestFsDot11nConfigAmsduLimit (UINT4 * , INT4  , INT4 );




#endif
