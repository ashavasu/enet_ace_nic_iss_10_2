/********************************************************************
 *  Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 *  $Id: fs1acwrp.h,v 1.1.1.1 2016/09/20 10:42:39 siva Exp $
 *
 *  Description: This file contains the prototypes for wrapper routines
 **********************************************************************/

#ifndef _WSSCFGWRAP_PROTO_H
#define _WSSCFGWRAP_PROTO_H

#include "lr.h"

INT1 
wsscfgGetFsDot11VHTOptionImplemented (INT4, INT4 *);

INT1
wsscfgGetFsDot11NumberOfSpatialStreamsImplemented (INT4, UINT4 *);

INT1
wsscfgGetFsDot11NumberOfSpatialStreamsActivated(INT4, UINT4 *);

INT1
wsscfgGetFsDot11ACVHTRxHighestDataRateSupported(INT4, UINT4 *);

INT1
wsscfgGetFsDot11VHTRxHighestDataRateConfig(INT4, UINT4 *);

INT1
wsscfgGetFsDot11ACVHTTxHighestDataRateSupported(INT4, UINT4 *);

INT1
wsscfgGetFsDot11VHTTxHighestDataRateConfig(INT4, UINT4 *);


INT1
wsscfgGetDot11HighThroughputOptionImplemented(INT4, INT4 *);

INT1
wsscfgGetFsDot11FragmentationThreshold(INT4, UINT4 *);

INT1
wsscfgGetFsDot11ACVHTTxSTBCOptionImplemented (INT4, INT4 *);

INT1
wsscfgGetFsDot11ACVHTTxSTBCOptionActivated (INT4, INT4 *);

INT1
wsscfgGetFsDot11ACVHTRxSTBCOptionImplemented (INT4, INT4 *);

INT1
wsscfgGetFsDot11ACVHTRxSTBCOptionActivated (INT4, INT4 *);

INT1
wsscfgSetFsDot11ACVHTTxSTBCOptionActivated (INT4, INT4);

INT1
wsscfgSetFsDot11ACVHTRxSTBCOptionActivated (INT4, INT4);

INT1
wsscfgTestv2FsDot11ACVHTRxSTBCOptionActivated (UINT4 *, INT4, INT4);

INT1
wsscfgTestv2FsDot11ACVHTTxSTBCOptionActivated (UINT4 *, INT4, INT4);

INT1 
wsscfgSetFsDot11FragmentationThreshold(INT4, UINT4);


INT1 
wsscfgTestv2FsDot11FragmentationThreshold(UINT4 *, INT4, UINT4);

INT1
wsscfgGetFsDot11OperatingModeNotificationImplemented(INT4, INT4 *);

INT1
wsscfgSetFsDot11NumberOfSpatialStreamsActivated (INT4, UINT4);

INT1
wsscfgTestv2FsDot11NumberOfSpatialStreamsActivated (UINT4 *, INT4, UINT4);
#define RADIO_TYPE_AC_MINRANGE  256
#define RADIO_TYPE_AC_MAXRANGE  11500
#define RADIO_TYPE_N_MINRANGE   256
#define RADIO_TYPE_N_MAXRANGE   8000
#define RADIO_TYPE_OTHER_MINRANGE   450
#define RADIO_TYPE_OTHER_MAXRANGE   3000
#endif
